﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using Avigma.Repository.Lib;
using IPL.Controllers;
using IPL.Models;
using IPL.Repository.data;
using IPLApp.Models;
using IPLApp.Models.data;
using IPLApp.Repository.data;
using Newtonsoft.Json;
using System.Text;
using IPL.Repository;

namespace IPLApp.Controllers
{
    [RoutePrefix("api/RESTIPL")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class RESTIPLController : BaseController
    {

        workOrderxDTO workOrder = new workOrderxDTO();

        GetWorkOrders GetWorkOrdersobj = new GetWorkOrders();
        DropDownData dropDownData = new DropDownData();
        IPLCityData iplCityData = new IPLCityData();
        WorkOrderMasterData WorkOrderMasterDataOBJ = new WorkOrderMasterData();
        ClientCompanyMasterData clientComapanyMasterData = new ClientCompanyMasterData();
        WorkOrderMasterData workOrderMasterData = new WorkOrderMasterData();
        UserMasterData userMasterData = new UserMasterData();
        IPLStateData iPLStateData = new IPLStateData();
        WorkTypeMasterData workTypeMasterData = new WorkTypeMasterData();
        WorkTypeCategoryData workTypeCategoryData = new WorkTypeCategoryData();
        WorkTypeAutoInvItemData workTypeAutoInvItemData = new WorkTypeAutoInvItemData();
        GroupMastersData groupMastersData = new GroupMastersData();
        MenuMasterData menuMasterData = new MenuMasterData();
        GroupMenuRelationData groupMenuRelationData = new GroupMenuRelationData();
        InvoiceItemMasterData invoiceItemMasterData = new InvoiceItemMasterData();
        CustomerMasterData customerMasterData = new CustomerMasterData();
        LoanTypeMasterData loanTypeMasterData = new LoanTypeMasterData();
        SystemOfRecordData systemOfRecordData = new SystemOfRecordData();
        MainInvoiceItemMasterData mainInvoiceItemMasterData = new MainInvoiceItemMasterData();
        WorkOrderColumnData workOrderColumnData = new WorkOrderColumnData();
        AppCompanyMasterData appCompanyMasterData = new AppCompanyMasterData();
        RushesMasterData rushesMasterData = new RushesMasterData();
        ClientMasterData CclientMasterData = new ClientMasterData();
        MainCategoryData mainCategoryData = new MainCategoryData();
        NewWorkOrderSettingData newWorkOrderSettingData = new NewWorkOrderSettingData();
        GeneralWorkOrderSettingData generalWorkOrderSettingData = new GeneralWorkOrderSettingData();
        CustomerNumberData customerNumberData = new CustomerNumberData();
        Task_MasterData Task_MasterDataObj = new Task_MasterData();
        TaskSettingChildData taskSettingChildData = new TaskSettingChildData();
        WorkTypeTaskChildData workTypeTaskChildData = new WorkTypeTaskChildData();
        DamageMasterData damageMasterData = new DamageMasterData();

        WorkOrderImport_MasterData workOrderImport_MasterData = new WorkOrderImport_MasterData();
        Import_Queue_TransData import_Queue_TransData = new Import_Queue_TransData();
        WorkOrderHistoryData workOrderHistoryData = new WorkOrderHistoryData();
        WorkOrder_Column_Data workOrder_Column_Data = new WorkOrder_Column_Data();
        UpdateWorkOrderMAsterStatusData updateWorkOrderMAsterStatusData = new UpdateWorkOrderMAsterStatusData();
        WorkOrderMessageMasterData workOrderMessageMasterData = new WorkOrderMessageMasterData();
        Client_Result_PhotoData client_Result_PhotoData = new Client_Result_PhotoData();

        UserAddressData userAddressData = new UserAddressData();
        Task_Master_Files_Data task_Master_Files_Data = new Task_Master_Files_Data();
        Contractor_Invoice_PaymentData contractor_Invoice_PaymentData = new Contractor_Invoice_PaymentData();
        Client_Invoice_PatymentData client_Invoice_PatymentData = new Client_Invoice_PatymentData();
        UpdateTaskBidStatusData updateTaskBidStatusData = new UpdateTaskBidStatusData();
        ScoreCard_DataVal_ChildData scoreCard_DataVal_ChildData = new ScoreCard_DataVal_ChildData();
        ScoreCard_Data scoreCard_Data = new ScoreCard_Data();
        Import_WorkOrder_Excel_MasterData import_WorkOrder_Excel_MasterData = new Import_WorkOrder_Excel_MasterData();
        InstructionDocumentData instructionDocumentData = new InstructionDocumentData();
        IPL_Company_MasterData iPL_Company_MasterData = new IPL_Company_MasterData();
        ContractorCoverageAreaData contractorCoverageAreaData = new ContractorCoverageAreaData();
        ContractorCategoryData contractorCategoryData = new ContractorCategoryData();
        ContractorMapData contractorMapData = new ContractorMapData();
        GetWorkOrder_Puran_Master getWorkOrder_Puran_Master = new GetWorkOrder_Puran_Master();
        WorkOrder_Office_DocumentData workOrder_Office_DocumentData = new WorkOrder_Office_DocumentData();
        Contractor_ScoreCard_SettingData contractor_ScoreCard_SettingData = new Contractor_ScoreCard_SettingData();
        Task_ConfigurationData task_ConfigurationData = new Task_ConfigurationData();
        WorkType_ConfigurationData workType_ConfigurationData = new WorkType_ConfigurationData();
        MainCategory_ConfigurationData mainCategory_ConfigurationData = new MainCategory_ConfigurationData();
        LiveUserLocationData liveUserLocationData = new LiveUserLocationData();
        Ipl_Company_RegisterData ipl_Company_RegisterData = new Ipl_Company_RegisterData();
        BackgroundCheckinProvider_Data backgroundCheckinProviderData = new BackgroundCheckinProvider_Data();
        User_AccessBy_CompanyID_Data user_AccessBy_CompanyID_Data = new User_AccessBy_CompanyID_Data();

        ECD_Notes_Data ecdNoteData = new ECD_Notes_Data();

        Log log = new Log();

        Occupancy_Status_MasterData Occupancy_Status_MasterData = new Occupancy_Status_MasterData();
        Loan_Status_MasterData Loan_Status_MasterData = new Loan_Status_MasterData();
        Property_Alert_MasterData Property_Alert_MasterData = new Property_Alert_MasterData();
        Property_Type_MasterData property_Type_MasterData = new Property_Type_MasterData();

        Filter_Admin_ImportData filter_Admin_ImportData = new Filter_Admin_ImportData();
        Filter_Admin_LoanData filter_Admin_LoanData = new Filter_Admin_LoanData();
        Filter_Admin_DamageData filter_Admin_DamageData = new Filter_Admin_DamageData();
        Allowables_Category_MasterData allowables_Category_MasterData = new Allowables_Category_MasterData();
        Filter_Admin_Common_Master_Data filter_Admin_Common_Master_Data = new Filter_Admin_Common_Master_Data();

        LotPricingFilterMasterData _lotPricingFilter = new LotPricingFilterMasterData();

        ProfessionalServiceMasterData professionalServiceMasterData = new ProfessionalServiceMasterData();
        ContactTypeMasterData contactTypeMasterData = new ContactTypeMasterData();
        PropertyLockReasonMasterData propertyLockReasonMasterData = new PropertyLockReasonMasterData();


        // Its for User login if user put on the UsserName and Password correctly Then Enter The Our Site.
        //if User Put wrong UserName And Password Then User Can not Enter Our Site that time we have show error message (please Enter User Name or Password Correctly)
        [Authorize]
        [HttpPost]
        [Route("PostAddLoginData")]
        public async Task<List<dynamic>> PostAddLoginData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<RootLoginDTO>(s);

                LoginDTO loginDTO = new LoginDTO();
                loginDTO.username = Data.username;
                loginDTO.token = Data.token;

                var Getinvestorlogin = await Task.Run(() => GetWorkOrdersobj.GetWorkOdersDetails(loginDTO, 1));
                return Getinvestorlogin;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        //get dropdown data
        // Its For New Work Order Page all Dropdown. This API Give All DropDown Value
        [Authorize]
        [HttpPost]
        [Route("GetDropDownData")]
        public async Task<List<dynamic>> GetDropDownData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                DropDownMasterDTO dropDownMasterDTO = new DropDownMasterDTO();
                dropDownMasterDTO.UserID = LoggedInUSerId;
                dropDownMasterDTO.Type = 1;

                var Getdropdown = await Task.Run(() => dropDownData.GetDropdownWorkOrder(dropDownMasterDTO));
                // var Getdropdown = await Task.Run(() => dropDownData.GetDropdownWorkOrdernew(dropDownMasterDTO)); // Uncomment it when implemented new from Angular
                return Getdropdown;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        [Authorize]
        [HttpPost]
        [Route("GetCountyByStateData")]
        public async Task<List<dynamic>> GetCountyByStateData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<StateData>(s);
                Data.Type = 2;
                Data.UserID = LoggedInUSerId;

                var Getdropdown = await Task.Run(() => dropDownData.GetCountyByStateData(Data));
                return Getdropdown;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }



        // post Work Order Data
        //This is For Create New Work Order  If user Want to Create New Work Order that Time this API Responsiable. this API Store The Details In Database.
        [Authorize]
        [HttpPost]
        [Route("PostWorkOrderData")]
        public async Task<List<dynamic>> PostWorkOrderData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();


                var Data = JsonConvert.DeserializeObject<RootworkOrder>(s);

                workOrderxDTO workOrderDTO = new workOrderxDTO();

                workOrderDTO.address1 = Data.address1;
                workOrderDTO.address2 = Data.address2;
                workOrderDTO.city = Data.city;
                workOrderDTO.clientDueDate = Data.clientDueDate;
                workOrderDTO.clientStatus = Data.clientStatus;
                workOrderDTO.country = Data.country;
                workOrderDTO.currUserId = Data.currUserId;
                workOrderDTO.currUserId = Data.currUserId;

                workOrderDTO.dueDate = Data.dueDate;

                workOrderDTO.gpsLatitude = Data.gpsLatitude;
                workOrderDTO.gpsLongitude = Data.gpsLongitude;

                workOrderDTO.IsActive = Data.IsActive;

                workOrderDTO.startDate = Data.startDate;
                workOrderDTO.state = Data.state;
                workOrderDTO.state_name = Data.state_name;
                workOrderDTO.status = Data.status;
                workOrderDTO.Type = Data.Type;
                workOrderDTO.workOrderInfo = Data.workOrderInfo;
                workOrderDTO.workOrderNumber = Data.workOrderNumber;
                workOrderDTO.workOrder_ID = Data.workOrder_ID;
                workOrderDTO.zip = Data.zip;

                workOrderDTO.WorkType = Data.WorkType;
                workOrderDTO.Company = Data.Company;
                workOrderDTO.Com_Name = Data.Com_Name;
                workOrderDTO.Com_Phone = Data.Com_Phone;
                workOrderDTO.Com_Email = Data.Com_Email;
                workOrderDTO.Contractor = Data.Contractor;
                workOrderDTO.Received_Date = Data.Received_Date;
                workOrderDTO.Complete_Date = Data.Complete_Date;
                workOrderDTO.Cancel_Date = Data.Cancel_Date;
                workOrderDTO.IPLNO = Data.IPLNO;

                workOrderDTO.Mortgagor = Data.Mortgagor;
                workOrderDTO.Category = Data.Category;
                workOrderDTO.Loan_Info = Data.Loan_Info;
                workOrderDTO.Customer_Number = Data.Customer_Number;
                workOrderDTO.Cordinator = Data.Cordinator;
                workOrderDTO.Processor = Data.Processor;
                workOrderDTO.BATF = Data.BATF;
                workOrderDTO.ISInspection = Data.ISInspection;
                workOrderDTO.Lotsize = Data.Lotsize;
                workOrderDTO.Rush = Data.Rush;
                workOrderDTO.Lock_Code = Data.Lock_Code;
                workOrderDTO.Broker_Info = Data.Broker_Info;
                workOrderDTO.Comments = Data.Comments;
                workOrderDTO.Lock_Location = Data.Lock_Location;
                workOrderDTO.Key_Code = Data.Key_Code;
                workOrderDTO.Gate_Code = Data.Gate_Code;
                workOrderDTO.Loan_Number = Data.Loan_Number;
                workOrderDTO.IsDelete = Data.IsDelete;
                workOrderDTO.currUserId = Convert.ToInt32(LoggedInUSerId);
                workOrderDTO.DateCreated = Data.DateCreated;
                workOrderDTO.EstimatedDate = Data.EstimatedDate;

                workOrderDTO.Recurring = Data.Recurring;
                workOrderDTO.Recurs_Day = Data.Recurs_Day;
                workOrderDTO.Recurs_Limit = Data.Recurs_Limit;
                workOrderDTO.Recurs_Period = Data.Recurs_Period;
                workOrderDTO.Recurs_CutOffDate = Data.Recurs_CutOffDate;

                workOrderDTO.Recurs_ReceivedDateArray = Data.Recurs_ReceivedDateArray;
                workOrderDTO.Recurs_DueDateArray = Data.Recurs_DueDateArray;
                workOrderDTO.IsEdit = Data.IsEdit;
                workOrderDTO.Background_Provider = Data.Background_Provider;
                workOrderDTO.UserID = LoggedInUSerId;
                var returnVal = await Task.Run(() => WorkOrderMasterDataOBJ.AddWorkorderDataAsync(workOrderDTO));
                return returnVal;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        // Delete WorkOrder
        [Authorize]
        [HttpPost]
        [Route("PostdeleteWorkOrderData")]
        public async Task<List<dynamic>> PostdeleteWorkOrderData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();


                var Data = JsonConvert.DeserializeObject<RootworkOrder>(s);

                workOrderxDTO workOrderDTO = new workOrderxDTO();


                workOrderDTO.workOrder_ID = Data.workOrder_ID;
                workOrderDTO.IPLNO = Data.IPLNO;
                workOrderDTO.Type = 4;
                workOrderDTO.UserID = LoggedInUSerId;
                workOrderDTO.currUserId = Convert.ToInt32(LoggedInUSerId);


                var returndelVal = await Task.Run(() => WorkOrderMasterDataOBJ.AddWorkorderDataAsync(workOrderDTO));
                return returndelVal;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }




        // This Api Responsiable For Showing the all WorkOrder Details In Grid Formate .
        //get work order data
        [Authorize]
        [HttpPost]
        [Route("GetWorkOrderData")]
        public async Task<List<dynamic>> GetWorkOrderData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<workOrderxDTO>(s);
                workOrder.workOrder_ID = Data.workOrder_ID;
                workOrder.Type = Data.Type;
                workOrder.UserID = LoggedInUSerId;
                var Getworkorder = await Task.Run(() => workOrderMasterData.GetWorkOrderData(workOrder));
                //return new JsonResult
                //{
                //    ContentEncoding = Encoding.Default,
                //    ContentType = "application/json",
                //    Data = Data,
                //    JsonRequestBehavior = JsonRequestBehavior.AllowGet,
                //    MaxJsonLength = int.MaxValue
                //};


                return Getworkorder;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        //This API used For Fetch Record From Pruvan Api And Store On Our Databse. If UserName or Token Wrong Then We Can't fetch records from pruvan Api.
        //get work order data
        [Authorize]
        [HttpPost]
        [Route("GetWorkOrder")]
        public async Task<List<dynamic>> GetWorkOrder()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            LoginDTO loginDTO = new LoginDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<LoginDTO>(s);
                loginDTO.username = Data.username;
                loginDTO.token = Data.token;




                var Getorder = await Task.Run(() => GetWorkOrdersobj.GetWorkOdersDetails(loginDTO, 1));
                return Getorder;


            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        //Here We Are Create new User this API responciable For Add User Details store in our database. Address, zip, city, state, etc. we are Store on different table.
        //Add Update User Data
        [Authorize]
        [HttpPost]
        [Route("PostUserData")]
        public async Task<List<dynamic>> PostUserData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();


                var Data = JsonConvert.DeserializeObject<UserMasterDTO>(s);

                UserMasterDTO userMasterDTO = new UserMasterDTO();

                userMasterDTO.User_pkeyID = Data.User_pkeyID;
                userMasterDTO.User_FirstName = Data.User_FirstName;
                userMasterDTO.User_LastName = Data.User_LastName;
                userMasterDTO.User_Address = Data.User_Address;
                userMasterDTO.User_City = Data.User_City;
                userMasterDTO.User_State = Data.User_State;
                userMasterDTO.User_Zip = Data.User_Zip;
                userMasterDTO.User_CellNumber = Data.User_CellNumber;
                userMasterDTO.User_CompanyName = Data.User_CompanyName;
                userMasterDTO.User_Sys_Record = Data.User_Sys_Record;
                userMasterDTO.User_LoginName = Data.User_LoginName;
                userMasterDTO.User_Password = Data.User_Password;
                userMasterDTO.User_Email = Data.User_Email;
                userMasterDTO.User_Group = Data.User_Group;
                userMasterDTO.User_Contractor = Data.User_Contractor;
                userMasterDTO.User_Cordinator = Data.User_Cordinator;
                userMasterDTO.User_Processor = Data.User_Processor;
                userMasterDTO.User_OpenOrderDisCriteria = Data.User_OpenOrderDisCriteria;
                userMasterDTO.User_PastWorkOrder = Data.User_PastWorkOrder;
                userMasterDTO.User_PastOrderDisCriteria = Data.User_PastOrderDisCriteria;
                userMasterDTO.User_SelectOrderDisCriteria = Data.User_SelectOrderDisCriteria;
                userMasterDTO.User_BackgroundCheckProvider = Data.User_BackgroundCheckProvider;
                userMasterDTO.User_BackgroundCheckId = Data.User_BackgroundCheckId;
                userMasterDTO.User_BackgroundDocPath = Data.User_BackgroundDocPath;
                userMasterDTO.User_BackgroundDocName = Data.User_BackgroundDocName;
                userMasterDTO.User_Assi_Admin = Data.User_Assi_Admin;
                userMasterDTO.User_Active = Data.User_Active;
                userMasterDTO.User_WorkOrder = Data.User_WorkOrder;
                userMasterDTO.User_Wo_History = Data.User_Wo_History;
                userMasterDTO.User_Disc_percentage = Data.User_Disc_percentage;
                userMasterDTO.User_Tme_Zone = Data.User_Tme_Zone;
                userMasterDTO.User_Auto_Assign = Data.User_Auto_Assign;
                userMasterDTO.User_Leg_FirstName = Data.User_Leg_FirstName;
                userMasterDTO.User_Leg_LastName = Data.User_Leg_LastName;
                userMasterDTO.User_Leg_CellPhone = Data.User_Leg_CellPhone;
                userMasterDTO.User_Leg_Address = Data.User_Leg_Address;
                userMasterDTO.User_Leg_Address1 = Data.User_Leg_Address1;
                userMasterDTO.User_Leg_City = Data.User_Leg_City;
                userMasterDTO.User_Leg_State = Data.User_Leg_State;
                userMasterDTO.User_Leg_Notes = Data.User_Leg_Notes;
                userMasterDTO.User_Email_Note = Data.User_Email_Note;
                userMasterDTO.User_Emai_Reminders = Data.User_Emai_Reminders;
                userMasterDTO.User_Email_New_Wo = Data.User_Email_New_Wo;
                userMasterDTO.User_Email_UnAssigned_Wo = Data.User_Email_UnAssigned_Wo;
                userMasterDTO.User_Email_FollowUp = Data.User_Email_FollowUp;
                userMasterDTO.User_Text_Note = Data.User_Text_Note;
                userMasterDTO.User_Text_Reminders = Data.User_Text_Reminders;
                userMasterDTO.User_Text_New_Wo = Data.User_Text_New_Wo;
                userMasterDTO.User_Text_UnAssigned_Wo = Data.User_Text_UnAssigned_Wo;
                userMasterDTO.User_Text_FollowUp = Data.User_Text_FollowUp;
                userMasterDTO.User_Alert_EmailReply = Data.User_Alert_EmailReply;
                userMasterDTO.User_Alert_Ready_Office = Data.User_Alert_Ready_Office;
                userMasterDTO.User_Misc_Contractor_Score = Data.User_Misc_Contractor_Score;
                userMasterDTO.User_Misc_Insurance_Expire = Data.User_Misc_Insurance_Expire;
                userMasterDTO.User_Misc_Pruvan_Username = Data.User_Misc_Pruvan_Username;
                userMasterDTO.User_Misc_PushKey = Data.User_Misc_PushKey;
                userMasterDTO.User_Misc_StartDate = Data.User_Misc_StartDate;
                userMasterDTO.User_Misc_Device_Id = Data.User_Misc_Device_Id;
                userMasterDTO.User_Misc_ABC = Data.User_Misc_ABC;
                userMasterDTO.User_Misc_Service_Id = Data.User_Misc_Service_Id;
                userMasterDTO.User_BusinessID_Social_No = Data.User_BusinessID_Social_No;
                userMasterDTO.User_Track_Payment = Data.User_Track_Payment;
                userMasterDTO.User_Default_Expence_Account_Id = Data.User_Default_Expence_Account_Id;
                userMasterDTO.User_Source = Data.User_Source;
                if (Data.Type == 2 || Data.Type == 3)
                {
                    userMasterDTO.User_IsActive = Data.User_IsActive;
                }
                else if (Data.Type == 3)
                {
                    userMasterDTO.User_IsActive = false;
                }
                else
                {
                    userMasterDTO.User_IsActive = true;
                }

                userMasterDTO.User_IsDelete = Data.User_IsDelete;
                userMasterDTO.User_Con_Cat_Id = Data.User_Con_Cat_Id;
                userMasterDTO.Type = Data.Type;


                // new add
                userMasterDTO.User_Email_Cancelled = Data.User_Email_Cancelled;
                userMasterDTO.User_Email_New_Message = Data.User_Email_New_Message;
                userMasterDTO.User_Email_Field_Complete = Data.User_Email_Field_Complete;
                userMasterDTO.User_Email_Daily_Digest = Data.User_Email_Daily_Digest;

                userMasterDTO.User_Text_Cancelled = Data.User_Text_Cancelled;
                userMasterDTO.User_Text_New_Message = Data.User_Text_New_Message;
                userMasterDTO.User_Text_Field_Complete = Data.User_Text_Field_Complete;

                userMasterDTO.StrAddressArray = Data.StrAddressArray;
                userMasterDTO.User_AssignClient = Data.User_AssignClient;
                userMasterDTO.User_Tracking = Data.User_Tracking;
                userMasterDTO.User_Tracking_Time = Data.User_Tracking_Time;



                userMasterDTO.User_Comments = Data.User_Comments;
                userMasterDTO.UserDocumentArray = Data.UserDocumentArray;
                userMasterDTO.UserID = LoggedInUSerId;




                var returnVal = await Task.Run(() => userMasterData.AddUserMasterdetails(userMasterDTO));

                return returnVal;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        // If we want to check how many user in our table/grid that time this api are used. if we are fire this method then we have all record fetch on grid.
        //get User Detaails
        [Authorize]
        [HttpPost]
        [Route("GetUserOrder")] //this api get user data
        public async Task<List<dynamic>> GetUserOrder()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            UserMasterDTO userMasterDTO = new UserMasterDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<UserMasterDTO>(s);
                userMasterDTO.User_pkeyID = Data.User_pkeyID;
                userMasterDTO.Type = Data.Type;



                var GetUser = await Task.Run(() => userMasterData.GetUserDetails(userMasterDTO));
                return GetUser;


            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        //this api responsiable for store client company details. here some field are required like company name Discount% and active if you dont fill these field you cant save your details.
        //create update client company master
        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("PostClientCompanyDetails")]
        public async Task<List<dynamic>> PostClientCompanyDetails()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            ClientCompanyMasterDTO clientCompanyMasterDTO = new ClientCompanyMasterDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<ClientCompanyMasterDTO>(s);
                clientCompanyMasterDTO.Client_pkeyID = Data.Client_pkeyID;
                clientCompanyMasterDTO.Client_Company_Name = Data.Client_Company_Name;
                clientCompanyMasterDTO.Client_Discount = Data.Client_Discount;
                clientCompanyMasterDTO.Client_Contractor_Discount = Data.Client_Contractor_Discount;
                clientCompanyMasterDTO.Client_Billing_Address = Data.Client_Billing_Address;
                clientCompanyMasterDTO.Client_City = Data.Client_City;
                clientCompanyMasterDTO.Client_StateId = Data.Client_StateId;
                clientCompanyMasterDTO.Client_ZipCode = Data.Client_ZipCode;
                clientCompanyMasterDTO.Client_DateTimeOverlay = Data.Client_DateTimeOverlay;
                clientCompanyMasterDTO.Client_BackgroundProvider = Data.Client_BackgroundProvider;
                clientCompanyMasterDTO.Client_ContactType = Data.Client_ContactType;
                clientCompanyMasterDTO.Client_ContactName = Data.Client_ContactName;
                clientCompanyMasterDTO.Client_ContactEmail = Data.Client_ContactEmail;
                clientCompanyMasterDTO.Client_ContactPhone = Data.Client_ContactPhone;
                clientCompanyMasterDTO.Client_Comments = Data.Client_Comments;
                clientCompanyMasterDTO.Client_Due_Date_Offset = Data.Client_Due_Date_Offset;
                clientCompanyMasterDTO.Client_Photo_Resize_width = Data.Client_Photo_Resize_width;
                clientCompanyMasterDTO.Client_Photo_Resize_height = Data.Client_Photo_Resize_height;
                clientCompanyMasterDTO.Client_Invoice_Total = Data.Client_Invoice_Total;
                clientCompanyMasterDTO.Client_Login = Data.Client_Login;
                clientCompanyMasterDTO.Client_Login_Id = Data.Client_Login_Id;
                clientCompanyMasterDTO.Client_Password = Data.Client_Password;
                clientCompanyMasterDTO.Client_Rep_Id = Data.Client_Rep_Id;
                clientCompanyMasterDTO.Client_Lock_Order = Data.Client_Lock_Order;
                clientCompanyMasterDTO.Client_Lock_Order_Reason = Data.Client_Lock_Order_Reason;
                clientCompanyMasterDTO.Client_IPL_Mobile = Data.Client_IPL_Mobile;
                clientCompanyMasterDTO.Client_Provider = Data.Client_Provider;
                clientCompanyMasterDTO.Client_Active = Data.Client_Active;
                clientCompanyMasterDTO.Client_ClientPhone = Data.Client_ClientPhone;
                clientCompanyMasterDTO.Client_FaxNumbar = Data.Client_FaxNumbar;
                clientCompanyMasterDTO.Client_Website_Link = Data.Client_Website_Link;
                clientCompanyMasterDTO.Client_IsActive = Data.Client_IsActive;
                clientCompanyMasterDTO.Client_IsDelete = Data.Client_IsDelete;
                clientCompanyMasterDTO.Client_DateTimeOverlay = Data.Client_DateTimeOverlay;
                clientCompanyMasterDTO.UserID = LoggedInUSerId;
                clientCompanyMasterDTO.Type = Data.Type;


                objdynamicobj = await Task.Run(() => clientComapanyMasterData.AddClientCompanyData(clientCompanyMasterDTO));

                if (objdynamicobj[0].Status != "0")
                {

                    ClientContactListDTO clientContactListDTO = new ClientContactListDTO();

                    // here multiple ClientContact
                    if (Data.ClientContactList != null && Data.ClientContactList.Count > 0)
                    {
                        for (int i = 0; i < Data.ClientContactList.Count; i++)
                        {
                            clientContactListDTO.Clnt_Con_List_ClientComID = Convert.ToInt64(objdynamicobj[0].Client_pkeyID);
                            clientContactListDTO.Clnt_Con_List_pkeyID = Data.ClientContactList[i].Clnt_Con_List_pkeyID;
                            clientContactListDTO.Clnt_Con_List_TypeName = Data.ClientContactList[i].Clnt_Con_List_TypeName;
                            clientContactListDTO.Clnt_Con_List_Name = Data.ClientContactList[i].Clnt_Con_List_Name;
                            clientContactListDTO.Clnt_Con_List_Email = Data.ClientContactList[i].Clnt_Con_List_Email;
                            clientContactListDTO.Clnt_Con_List_Phone = Data.ClientContactList[i].Clnt_Con_List_Phone;
                            clientContactListDTO.Clnt_Con_List_IsActive = Data.ClientContactList[i].Clnt_Con_List_IsActive;
                            clientContactListDTO.Clnt_Con_List_IsDelete = Data.ClientContactList[i].Clnt_Con_List_IsDelete;
                            clientContactListDTO.UserID = LoggedInUSerId;
                            if (Data.ClientContactList[i].Clnt_Con_List_pkeyID != 0)
                            {
                                clientContactListDTO.Type = 2;
                            }
                            else
                            {
                                clientContactListDTO.Type = 1;
                            }
                            var returnPkey = clientComapanyMasterData.AddClientContactListData(clientContactListDTO);
                        }
                    }
                }
                return objdynamicobj;
            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        // this api for showing all records. we are fetch all client company list on grid
        //get client Details
        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("GetClientCompany")]
        public async Task<List<dynamic>> GetClientCompany()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            ClientCompanyMasterDTO clientCompanyMasterDTO = new ClientCompanyMasterDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<ClientCompanyMasterDTO>(s);
                clientCompanyMasterDTO.Client_pkeyID = Data.Client_pkeyID;
                clientCompanyMasterDTO.Type = Data.Type;



                var GetClientCompany = await Task.Run(() => clientComapanyMasterData.GetClientCompanyMasterDetails(clientCompanyMasterDTO));
                return GetClientCompany;


            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        // this api used for fetch records for dropdown from the database .
        //get clientCompany table data using WhereClause
        [Authorize]
        [HttpPost]
        [Route("GetClientCompanyList")]
        public async Task<List<dynamic>> GetClientCompanyList()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            ClientCompanyDTO clientCompanyDTO = new ClientCompanyDTO();
            SearchMasterDTO searchMasterDTO = new SearchMasterDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<ClientCompanyDTO>(s);
                Data.UserId = LoggedInUSerId;
                if (Data.SearchMaster != null)
                {
                    Data.SearchMaster.UserID = LoggedInUSerId;
                }


                switch (Data.Type)
                {
                    case 2:
                        {

                            ClientCompanyMasterDTO ClientCompanyMasterDTOObj = new ClientCompanyMasterDTO();
                            ClientCompanyMasterDTOObj.Type = 2;
                            ClientCompanyMasterDTOObj.Client_pkeyID = Data.Client_pkeyID;
                            ClientCompanyMasterDTOObj.UserID = LoggedInUSerId;


                            objdynamicobj = await Task.Run(() => clientComapanyMasterData.GetClientCompanyMasterDetails(ClientCompanyMasterDTOObj));
                            break;
                        }
                    case 3:
                        {
                            objdynamicobj = await Task.Run(() => clientComapanyMasterData.GetClientContactlstMasterDetails(Data));

                            break;
                        }
                    case 4:
                        {
                            objdynamicobj = await Task.Run(() => clientComapanyMasterData.GetClientCompanyFilterDetails(Data));

                            break;
                        }
                    default:
                        {
                            objdynamicobj = await Task.Run(() => clientComapanyMasterData.GetDefaultClientDataForDropDown(Data));
                            break;
                        }
                }

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

            return objdynamicobj;

        }
        //get clientComapany dropdown
        [Authorize]
        [HttpPost]
        [Route("GetClientCompanydropdownlst")]
        public async Task<List<dynamic>> GetClientCompanydropdownlst()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<ClientCompanyMasterDTO>(s);





                var GetClientdrp = await Task.Run(() => clientComapanyMasterData.GetClientCompanydropdown());
                return GetClientdrp;


            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        //this api responsiable for add city and state. we are store the city state data on our database
        //create update City master
        [Authorize]
        [HttpPost]
        [Route("PostCityDetails")]
        public async Task<List<dynamic>> PostCityDetails()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            IPLCityDTO iplCityDTO = new IPLCityDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<IPLCityDTO>(s);
                iplCityDTO.IPL_CityID = Data.IPL_CityID;
                iplCityDTO.IPL_CityName = Data.IPL_CityName;
                iplCityDTO.IPL_StateID = Data.IPL_StateID;
                iplCityDTO.IPL_IsActive = true;
                iplCityDTO.UserID = LoggedInUSerId;
                iplCityDTO.Type = Data.Type;



                var AddCity = await Task.Run(() => iplCityData.AddCityData(iplCityDTO));
                return AddCity;


            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        // this api for fetch record if we want to edit and update that time it will show on our textbox after we are change here also.
        //get city master
        [Authorize]
        [HttpPost]
        [Route("GetCityDetails")]
        public async Task<List<dynamic>> GetCityDetails()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            IPLCityDTO iplCityDTO = new IPLCityDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<IPLCityDTO>(s);
                iplCityDTO.IPL_CityID = Data.IPL_CityID;
                iplCityDTO.Type = Data.Type;



                var AddCity = await Task.Run(() => iplCityData.GetCityDetails(iplCityDTO));
                return AddCity;


            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        //this api fetch the state records from the database and show on our all state dropdown value
        //get State master
        [Authorize]
        [HttpPost]
        [Route("GetStateDetail")]
        public async Task<List<dynamic>> GetStateDetail()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            IPLStateDTO iPLStateDTO = new IPLStateDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<IPLStateDTO>(s);
                Data.UserID = LoggedInUSerId;

                switch (Data.Type)
                {
                    case 2:
                        {
                            IPLStateDTO IPLStateDTO = new IPLStateDTO();
                            iPLStateDTO.IPL_StateID = Data.IPL_StateID;
                            iPLStateDTO.Type = 2;
                            iPLStateDTO.UserID = LoggedInUSerId;

                            objdynamicobj = await Task.Run(() => iPLStateData.GetStateDetails(iPLStateDTO));
                            break;
                        }
                    case 1:
                        {
                            IPLStateDTO IPLStateDTO = new IPLStateDTO();
                            iPLStateDTO.IPL_StateID = Data.IPL_StateID;
                            iPLStateDTO.WhereClause = "";
                            iPLStateDTO.Type = 1;
                            iPLStateDTO.UserID = LoggedInUSerId;
                            objdynamicobj = await Task.Run(() => iPLStateData.GetStateDetails(iPLStateDTO));

                            break;
                        }
                    case 3:
                        {
                            objdynamicobj = await Task.Run(() => iPLStateData.GetStateFilterDetails(Data));

                            break;
                        }
                }





            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
            return objdynamicobj;
        }
        //this api are used to store the work type details on our database. if we are create new work type it will store the details with uniq key .
        //create update WorkType details master
        [Authorize]
        [HttpPost]
        [Route("PostWorkTypeDetails")]
        public async Task<List<dynamic>> PostWorkTypeDetails()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            WorkTypeMasterDTO workTypeMasterDTO = new WorkTypeMasterDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<WorkTypeMasterDTO>(s);
                workTypeMasterDTO.WT_pkeyID = Data.WT_pkeyID;
                workTypeMasterDTO.WT_WorkType = Data.WT_WorkType;
                workTypeMasterDTO.WT_CategoryID = Data.WT_CategoryID;
                workTypeMasterDTO.WT_YardMaintenance = Data.WT_YardMaintenance;
                workTypeMasterDTO.WT_Active = Data.WT_Active;
                workTypeMasterDTO.WT_Always_recurring = Data.WT_Always_recurring;
                workTypeMasterDTO.WT_Recurs_Every = Data.WT_Recurs_Every;
                workTypeMasterDTO.WT_Recurs_WeekID = Data.WT_Recurs_WeekID;
                workTypeMasterDTO.WT_Limit_to = Data.WT_Limit_to;
                workTypeMasterDTO.WT_Cutoff_Date = Data.WT_Cutoff_Date;
                workTypeMasterDTO.WT_WO_ItemID = Data.WT_WO_ItemID;
                workTypeMasterDTO.WT_Contractor_AssignmentID = Data.WT_Contractor_AssignmentID;
                workTypeMasterDTO.WT_Ready_for_FieldID = Data.WT_Ready_for_FieldID;
                workTypeMasterDTO.WT_IsInspection = Data.WT_IsInspection;
                workTypeMasterDTO.WT_AutoInvoice = Data.WT_AutoInvoice;
                workTypeMasterDTO.WT_IsActive = Data.WT_IsActive;
                //workTypeMasterDTO.WT_Ready_Quantity = Data.WT_Ready_Quantity;
                workTypeMasterDTO.UserID = LoggedInUSerId;
                workTypeMasterDTO.Type = Data.Type;
                workTypeMasterDTO.Client_Work_Type_Name = Data.Client_Work_Type_Name;

                workTypeMasterDTO.WT_assign_upon_comple = Data.WT_assign_upon_comple;
                workTypeMasterDTO.WT_Template_Id = Data.WT_Template_Id;
                workTypeMasterDTO.WT_CategoryMultiple = Data.WT_CategoryMultiple;

                //if have multiple selection the Category id =1 other wise 0
                if (workTypeMasterDTO.Type != 6 && workTypeMasterDTO.WT_CategoryMultiple.Count > 0)
                    workTypeMasterDTO.WT_CategoryID = 1;



                var AddWork = await Task.Run(() => workTypeMasterData.AddWorkTypeData(workTypeMasterDTO));
                return AddWork;


            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        //this api are used to fetch record from database if user want to edit or upadte some details that time this api are wok and if you want to show details on grid that time also used this api .
        //Get WorkType details master
        [Authorize]
        [HttpPost]
        [Route("GetWorkTypeDetails")]
        public async Task<List<dynamic>> GetWorkTypeDetails()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<WorkTypeMasterDTO>(s);
                //    workTypeMasterDTO.WT_pkeyID = Data.WT_pkeyID;
                // workTypeMasterDTO.Type = Data.Type;
                Data.UserID = LoggedInUSerId;
                if (Data.SearchMaster != null)
                {
                    Data.SearchMaster.UserID = LoggedInUSerId;
                }

                var GetWork = await Task.Run(() => workTypeMasterData.GetWorkTypeDetails(Data));
                return GetWork;


            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        // here we are created category for work type . first we created category and use this category particular work type details .
        //create update WorkType Category  master
        [Authorize]
        [HttpPost]
        [Route("PostWorkTypecatDetails")]
        public async Task<List<dynamic>> PostWorkTypecatDetails()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            WorkTypeCategoryDTO workTypeCategoryDTO = new WorkTypeCategoryDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<WorkTypeCategoryDTO>(s);

                if (Data.Work_Type_NameArr != null)
                {
                    for (int i = 0; i < Data.Work_Type_NameArr.Count; i++)
                    {
                        workTypeCategoryDTO.Work_Type_Cat_pkeyID = Data.Work_Type_NameArr[i].Work_Type_Cat_pkeyID;
                        workTypeCategoryDTO.Work_Type_Name = Data.Work_Type_NameArr[i].Work_Type_Name;
                        workTypeCategoryDTO.Work_Type_IsActive = Data.Work_Type_IsActive;
                        workTypeCategoryDTO.Work_Type_Client_pkeyID = Data.Work_Type_Client_pkeyID;
                        workTypeCategoryDTO.UserID = LoggedInUSerId;
                        if (Data.Work_Type_NameArr[i].Work_Type_Cat_pkeyID == 0)
                        {
                            workTypeCategoryDTO.Type = 1;
                        }
                        else
                        {
                            workTypeCategoryDTO.Type = 2;
                        }

                        workTypeCategoryDTO.UserID = LoggedInUSerId;
                        objdynamicobj = await Task.Run(() => workTypeCategoryData.AddWorkTypeCatData(workTypeCategoryDTO));
                        //return objdynamicobj;

                    }
                }
                else
                {

                    workTypeCategoryDTO.Work_Type_Cat_pkeyID = Data.Work_Type_Cat_pkeyID;
                    workTypeCategoryDTO.Work_Type_Name = Data.Work_Type_Name;
                    workTypeCategoryDTO.Work_Type_IsActive = Data.Work_Type_IsActive;
                    workTypeCategoryDTO.Work_Type_Client_pkeyID = Data.Work_Type_Client_pkeyID;
                    workTypeCategoryDTO.UserID = LoggedInUSerId;
                    if (Data.Work_Type_Cat_pkeyID == 0)
                    {
                        workTypeCategoryDTO.Type = 1;
                    }
                    else
                    {
                        workTypeCategoryDTO.Type = 2;
                    }

                    workTypeCategoryDTO.UserID = LoggedInUSerId;
                    objdynamicobj = await Task.Run(() => workTypeCategoryData.AddWorkTypeCatData(workTypeCategoryDTO));
                }

                WorkTypeCategoryDTO workTypeCategoryDTO1 = new WorkTypeCategoryDTO();
                workTypeCategoryDTO1.Type = 1;
                workTypeCategoryDTO1.UserID = LoggedInUSerId;
                objdynamicobj = await Task.Run(() => workTypeCategoryData.GetWorkTypeCategoryDetails(workTypeCategoryDTO1));
                return objdynamicobj;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        // delete work type for manage
        [Authorize]
        [HttpPost]
        [Route("DeleteWorkTypecatDetails")]
        public async Task<List<dynamic>> DeleteWorkTypecatDetails()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            WorkTypeCategoryDTO workTypeCategoryDTO = new WorkTypeCategoryDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<WorkTypeCategoryDTO>(s);
                workTypeCategoryDTO.Work_Type_Cat_pkeyID = Data.Work_Type_Cat_pkeyID;
                workTypeCategoryDTO.Work_Type_IsActive = Data.Work_Type_IsActive;
                workTypeCategoryDTO.Type = Data.Type;



                var deleteWorkCat = await Task.Run(() => workTypeCategoryData.AddWorkTypeCatData(workTypeCategoryDTO));



                return deleteWorkCat;


            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //if we want to change work type category then we need to use this api this api are fetch all category what we created .
        //Get WorkType Category  master
        [Authorize]
        [HttpPost]
        [Route("GetWorkTypecatDetails")]
        public async Task<List<dynamic>> GetWorkTypecatDetails()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            WorkTypeCategoryDTO workTypeCategoryDTO = new WorkTypeCategoryDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<WorkTypeCategoryDTO>(s);
                workTypeCategoryDTO.Work_Type_Cat_pkeyID = Data.Work_Type_Cat_pkeyID;
                workTypeCategoryDTO.Type = Data.Type;
                workTypeCategoryDTO.UserID = LoggedInUSerId;


                var GetWorkCat = await Task.Run(() => workTypeCategoryData.GetWorkTypeCategoryDetails(workTypeCategoryDTO));
                return GetWorkCat;


            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        // this api for create Auto invoice item its belong to work type details page we are created work type auto invoice item and store the details with paticular worke type
        //create update WorkType Invoice Item  master
        [Authorize]
        [HttpPost]
        [Route("PostWorkTypeInvDetails")]
        public async Task<List<dynamic>> PostWorkTypeInvDetails()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            WorkTypeAutoInvItemDTO workTypeAutoInvItemDTO = new WorkTypeAutoInvItemDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<WorkTypeAutoInvItemDTO>(s);
                for (int i = 0; i < Data.Auto_Inv_Itm_StringArr.Count; i++)
                {

                    workTypeAutoInvItemDTO.Auto_Inv_Itm_PkeyID = Data.Auto_Inv_Itm_PkeyID;
                    workTypeAutoInvItemDTO.Auto_Inv_Itm_ID = Data.Auto_Inv_Itm_ID;
                    workTypeAutoInvItemDTO.Auto_Inv_Itm_Quantity = Data.Auto_Inv_Itm_StringArr[i].Quntity;
                    workTypeAutoInvItemDTO.Auto_Inv_Itm_Flag = Data.Auto_Inv_Itm_StringArr[i].Name;
                    workTypeAutoInvItemDTO.Auto_Inv_Itm_WorkTypeID = Data.Auto_Inv_Itm_WorkTypeID;
                    workTypeAutoInvItemDTO.Auto_Inv_Itm_IsActive = Data.Auto_Inv_Itm_IsActive;
                    workTypeAutoInvItemDTO.UserID = LoggedInUSerId;
                    workTypeAutoInvItemDTO.Type = Data.Type;



                    objdynamicobj = await Task.Run(() => workTypeAutoInvItemData.AddWorkTypeAutoInvItemeData(workTypeAutoInvItemDTO));
                }

                return objdynamicobj;


            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        // if user want to edit or update record that time this api fetch the work type auto invoice details for worktype page.
        //Get WorkType InvItem  master
        [Authorize]
        [HttpPost]
        [Route("GettWorkTypeInvDetails")]
        public async Task<List<dynamic>> GettWorkTypeInvDetails()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            WorkTypeAutoInvItemDTO workTypeAutoInvItemDTO = new WorkTypeAutoInvItemDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<WorkTypeAutoInvItemDTO>(s);
                workTypeAutoInvItemDTO.Auto_Inv_Itm_PkeyID = Data.Auto_Inv_Itm_PkeyID;
                workTypeAutoInvItemDTO.Type = Data.Type;



                var GetWorkinvitem = await Task.Run(() => workTypeAutoInvItemData.GetWorkTypeAutoInvItemDetails(workTypeAutoInvItemDTO));
                return GetWorkinvitem;


            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        //here we are create a new group  and store the group value on our database .
        //create update Group  master
        [Authorize]
        [HttpPost]
        [Route("PostGroupDetails")]
        public async Task<List<dynamic>> PostGroupDetails()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            GroupMastersDTO groupMastersDTO = new GroupMastersDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Group_Masters_MenuDTO>(s);
                Data.UserID = Convert.ToInt32(LoggedInUSerId);



                var AddGroup = await Task.Run(() => groupMastersData.AddGroupMenuData(Data));
                return AddGroup;


            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        // if we want to edit or upadate the group that time this api fetch the records on our datatable and also
        //Get Group master
        [Authorize]
        [HttpPost]
        [Route("GetGroupDetails")]
        public async Task<List<dynamic>> GetGroupDetails()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            //GroupMastersDTO groupMastersDTO = new GroupMastersDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<GroupMastersDTO>(s);
                Data.UserID = LoggedInUSerId;
                if (Data.SearchMaster != null)
                {
                    Data.SearchMaster.UserID = LoggedInUSerId;
                }
                //groupMastersDTO.Grp_pkeyID = Data.Grp_pkeyID;
                //groupMastersDTO.Type = Data.Type;



                var Getgroup = await Task.Run(() => groupMastersData.GetGroupDetails(Data));
                return Getgroup;


            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        //here we are creating menu like ,new work oder, admin , work type , etc. if we want to create more menu that time this api we are used.
        //create update Menu  master
        [Authorize]
        [HttpPost]
        [Route("PostMenuDetails")]
        public async Task<List<dynamic>> PostMenuDetails()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            MenuMasterDTO menuMasterDTO = new MenuMasterDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<MenuMasterDTO>(s);
                menuMasterDTO.Ipre_MenuID = Data.Ipre_MenuID;
                menuMasterDTO.Ipre_MenuName = Data.Ipre_MenuName;
                menuMasterDTO.Ipre_PageName = Data.Ipre_PageName;
                menuMasterDTO.Ipre_PageUrl = Data.Ipre_PageUrl;
                menuMasterDTO.Ipre_IsActive = Data.Ipre_IsActive;
                menuMasterDTO.UserID = LoggedInUSerId;
                menuMasterDTO.Type = Data.Type;



                var AddMenu = await Task.Run(() => menuMasterData.AddMenuData(menuMasterDTO));
                return AddMenu;


            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        // this api fetch all the menu list and it will show the menu on our grid or where we want to implement for exmaple in project have menu list what admin want to assinge menu for particular user
        //Get Menu  master
        [Authorize]
        [HttpPost]
        [Route("GetMenuDetails")]
        public async Task<List<dynamic>> GetMenuDetails()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            MenuMasterDTO menuMasterDTO = new MenuMasterDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<MenuMasterDTO>(s);
                menuMasterDTO.UserID = LoggedInUSerId;
                menuMasterDTO.Ipre_MenuID = Data.Ipre_MenuID;
                menuMasterDTO.Mgr_Group_Id = Data.Mgr_Group_Id;
                menuMasterDTO.Type = Data.Type;


                var GetMenu = await Task.Run(() => menuMasterData.MenuSelectForRole(menuMasterDTO));
                return GetMenu;


            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        // this api store the details  for exmaple in IPL project have menu list what admin want to assinge menu for particular user
        //create update GroupMenuRelation  master
        [Authorize]
        [HttpPost]
        [Route("PostGroupMenuRelation")]
        public async Task<List<dynamic>> PostGroupMenuRelation()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            GroupMenuRelationDTO groupMenuRelationDTO = new GroupMenuRelationDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<GroupMenuRelationDTO>(s);
                groupMenuRelationDTO.GroupMenuRelationID = Data.GroupMenuRelationID;
                groupMenuRelationDTO.GroupID = Data.GroupID;
                groupMenuRelationDTO.MenuID = Data.MenuID;
                groupMenuRelationDTO.IsActive = Data.IsActive;
                groupMenuRelationDTO.UserID = LoggedInUSerId;
                groupMenuRelationDTO.Type = Data.Type;



                var AddGroupMenu = await Task.Run(() => groupMenuRelationData.AddGroupMenuRelationData(groupMenuRelationDTO));
                return AddGroupMenu;


            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        //if admin want to change add our upadte that time firstly fetch record from database and after we are update the record
        //Get GroupMenuRelation  master
        [Authorize]
        [HttpPost]
        [Route("GetGroupMenuRelation")]
        public async Task<List<dynamic>> GetGroupMenuRelation()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            GroupMenuRelationDTO groupMenuRelationDTO = new GroupMenuRelationDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<GroupMenuRelationDTO>(s);
                groupMenuRelationDTO.GroupMenuRelationID = Data.GroupMenuRelationID;
                groupMenuRelationDTO.Type = Data.Type;



                var GetGroupMenu = await Task.Run(() => groupMenuRelationData.GetGroupMenuRelationDetails(groupMenuRelationDTO));
                return GetGroupMenu;


            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        //this is for Work type invoice line item dropdown  if you want to create more work type invoice line item that time it will work
        //Create Update Invoice item master
        [Authorize]
        [HttpPost]
        [Route("PostInvoiceitem")]
        public async Task<List<dynamic>> PostInvoiceitem()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            InvoiceItemMasterDTO invoiceItemMasterDTO = new InvoiceItemMasterDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<InvoiceItemMasterDTO>(s);
                invoiceItemMasterDTO.Inv_Itm_pkeyID = Data.Inv_Itm_pkeyID;
                invoiceItemMasterDTO.Inv_Itm_Name = Data.Inv_Itm_Name;
                invoiceItemMasterDTO.Inv_Itm_IsActive = Data.Inv_Itm_IsActive;
                invoiceItemMasterDTO.UserID = LoggedInUSerId;
                invoiceItemMasterDTO.Type = Data.Type;



                var AddInvoiceItem = await Task.Run(() => invoiceItemMasterData.AddInvoiceItemData(invoiceItemMasterDTO));
                return AddInvoiceItem;


            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        // this api show the dropdown value for work type invoice line item
        //Get Invoice item master
        [Authorize]
        [HttpPost]
        [Route("GetInvoiceitem")]
        public async Task<List<dynamic>> GetInvoiceitem()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            InvoiceItemMasterDTO invoiceItemMasterDTO = new InvoiceItemMasterDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<InvoiceItemMasterDTO>(s);
                invoiceItemMasterDTO.Inv_Itm_pkeyID = Data.Inv_Itm_pkeyID;
                invoiceItemMasterDTO.Type = Data.Type;



                var GetInvoiceItem = await Task.Run(() => invoiceItemMasterData.GetInvoiceItemDetails(invoiceItemMasterDTO));
                return GetInvoiceItem;


            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        // it is for invoice  items page here customers dropdown. here we are creating new customer
        //Create Update Customer master
        [Authorize]
        [HttpPost]
        [Route("PostCustomerData")]
        public async Task<List<dynamic>> PostCustomerData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            CustomerMasterDTO customerMasterDTO = new CustomerMasterDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<CustomerMasterDTO>(s);
                customerMasterDTO.Cust_pkeyId = Data.Cust_pkeyId;
                customerMasterDTO.Cust_Name = Data.Cust_Name;
                customerMasterDTO.Cust_IsActive = Data.Cust_IsActive;
                customerMasterDTO.UserID = LoggedInUSerId;
                customerMasterDTO.Type = Data.Type;



                var AddCustomer = await Task.Run(() => customerMasterData.AddCustomeMasyerData(customerMasterDTO));
                return AddCustomer;


            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        // get customer list for dropdown invoice item page
        //Get Customer master
        [Authorize]
        [HttpPost]
        [Route("GetCustomerData")]
        public async Task<List<dynamic>> GetCustomerData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            CustomerMasterDTO customerMasterDTO = new CustomerMasterDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<CustomerMasterDTO>(s);
                customerMasterDTO.Cust_pkeyId = Data.Cust_pkeyId;
                customerMasterDTO.Type = Data.Type;



                var GetCustomer = await Task.Run(() => customerMasterData.GetCustomerMasterDetails(customerMasterDTO));
                return GetCustomer;


            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        // it is for invoice  items page here  LoanType dropdown. here we are creating new  LoanType
        //Create Update LoanType master
        [Authorize]
        [HttpPost]
        [Route("PostLoanTypeData")]
        public async Task<List<dynamic>> PostLoanTypeData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            LoanTypeMasterDTO loanTypeMasterDTO = new LoanTypeMasterDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<LoanTypeMasterDTO>(s);
                loanTypeMasterDTO.Loan_pkeyId = Data.Loan_pkeyId;
                loanTypeMasterDTO.Loan_Type = Data.Loan_Type;
                loanTypeMasterDTO.Loan_IsActive = Data.Loan_IsActive;
                loanTypeMasterDTO.UserID = LoggedInUSerId;
                loanTypeMasterDTO.Type = Data.Type;



                var AddLoanType = await Task.Run(() => loanTypeMasterData.AddLoanTypeMasyerData(loanTypeMasterDTO));
                return AddLoanType;


            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        // get customer list for dropdown invoice item page
        //Get LoanType master
        [Authorize]
        [HttpPost]
        [Route("GetLoanTypeData")]
        public async Task<List<dynamic>> GetLoanTypeData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            LoanTypeMasterDTO loanTypeMasterDTO = new LoanTypeMasterDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<LoanTypeMasterDTO>(s);

                Data.UserID = LoggedInUSerId;

                switch (Data.Type)
                {
                    case 1:
                        {

                            objdynamicobj = await Task.Run(() => loanTypeMasterData.GetLoanTypeMasterDetails(Data));
                            break;
                        }

                    case 3:
                        {
                            objdynamicobj = await Task.Run(() => loanTypeMasterData.GetLoanFilterDetails(Data));

                            break;
                        }

                }

                return objdynamicobj;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        // this api creating for system of records data it will used on user page .
        //Create Update SystemOfRecords master
        [Authorize]
        [HttpPost]
        [Route("PostSystemOfRecordsData")]
        public async Task<List<dynamic>> PostSystemOfRecordsData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            SystemOfRecordDTO systemOfRecordDTO = new SystemOfRecordDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<SystemOfRecordDTO>(s);
                systemOfRecordDTO.Sys_Of_Rcd_PkeyID = Data.Sys_Of_Rcd_PkeyID;
                systemOfRecordDTO.Sys_Of_Rcd_Name = Data.Sys_Of_Rcd_Name;
                systemOfRecordDTO.Sys_Of_Rcd_IsActive = Data.Sys_Of_Rcd_IsActive;
                systemOfRecordDTO.UserID = LoggedInUSerId;
                systemOfRecordDTO.Type = Data.Type;



                var AddSystem = await Task.Run(() => systemOfRecordData.AddSystemRecordData(systemOfRecordDTO));
                return AddSystem;


            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        // this api fetch record from databse for system of records dropdown it will used on user page .
        //Get SystemOfRecords master
        [Authorize]
        [HttpPost]
        [Route("GetSystemOfRecordsData")]
        public async Task<List<dynamic>> GetSystemOfRecordsData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            SystemOfRecordDTO systemOfRecordDTO = new SystemOfRecordDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<SystemOfRecordDTO>(s);
                systemOfRecordDTO.Sys_Of_Rcd_PkeyID = Data.Sys_Of_Rcd_PkeyID;
                systemOfRecordDTO.Type = Data.Type;



                var GetSystem = await Task.Run(() => systemOfRecordData.GetSystemRecordMasterDetails(systemOfRecordDTO));
                return GetSystem;


            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        // this api store the new invoice items details page . here we can create and update records
        //Create Update MainInvoiceItem master
        [Authorize]
        [HttpPost]
        [Route("PostMainInvoiceItemData")]
        public async Task<List<dynamic>> PostMainInvoiceItemData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            MainInvoiceItemMasterDTO mainInvoiceItemMasterDTO = new MainInvoiceItemMasterDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<MainInvoiceItemMasterDTO>(s);
                mainInvoiceItemMasterDTO.Item_pkeyID = Data.Item_pkeyID;
                mainInvoiceItemMasterDTO.Item_Name = Data.Item_Name;
                mainInvoiceItemMasterDTO.Item_Bid = Data.Item_Bid;
                mainInvoiceItemMasterDTO.Item_AlwaysShowClientWo = Data.Item_AlwaysShowClientWo;
                mainInvoiceItemMasterDTO.Item_RequiredClientWO = Data.Item_RequiredClientWO;
                mainInvoiceItemMasterDTO.Item_LotSize = Data.Item_LotSize;
                mainInvoiceItemMasterDTO.Item_Through = Data.Item_Through;
                mainInvoiceItemMasterDTO.Item_AutoInvoiceClient = Data.Item_AutoInvoiceClient;
                mainInvoiceItemMasterDTO.Item_Active = Data.Item_Active;
                mainInvoiceItemMasterDTO.Item_IsActive = Data.Item_IsActive;
                mainInvoiceItemMasterDTO.UserID = LoggedInUSerId;
                mainInvoiceItemMasterDTO.Type = Data.Type;



                var AddMainInvoice = await Task.Run(() => mainInvoiceItemMasterData.AddMainInvoiceItemData(mainInvoiceItemMasterDTO));
                return AddMainInvoice;


            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        // this api fetch all  invoice items records for edit and update .
        //Get MainInvoiceItem master
        [Authorize]
        [HttpPost]
        [Route("GetMainInvoiceItemData")]
        public async Task<List<dynamic>> GetMainInvoiceItemData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            MainInvoiceItemMasterDTO mainInvoiceItemMasterDTO = new MainInvoiceItemMasterDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<MainInvoiceItemMasterDTO>(s);
                mainInvoiceItemMasterDTO.Item_pkeyID = Data.Item_pkeyID;
                mainInvoiceItemMasterDTO.Type = Data.Type;



                var getMainInvoice = await Task.Run(() => mainInvoiceItemMasterData.GetMainInvoiceItemDetails(mainInvoiceItemMasterDTO));
                return getMainInvoice;


            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        // this api fetch all  Column  records it will show on model here we can active or inactive menu .
        //Get WorkOrderColumn master
        [Authorize]
        [HttpPost]
        [Route("GetWorkOrderColumnData")]
        public async Task<List<dynamic>> GetWorkOrderColumnData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<WorkOrderColumnDTO>(s);




                var GetWorkOrderColumn = await Task.Run(() => workOrderColumnData.GetWorkOrderColumnDetails());
                return GetWorkOrderColumn;


            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }



        // this api for Create and update Comapny Info // save to dBase // return response status code :)

        // this api store the new your Company details page . here we can create and update records
        //Create Update AppCompanyInfo  master

        [Authorize]
        [HttpPost]
        [Route("PostAppCompanyInfoData")]
        public async Task<List<dynamic>> PostAppCompanyInfoData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            AppCompanyMasterDTO appCompanyMasterDTO = new AppCompanyMasterDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<AppCompanyMasterDTO>(s);
                appCompanyMasterDTO.YR_Company_pkeyID = Data.YR_Company_pkeyID;
                appCompanyMasterDTO.YR_Company_Name = Data.YR_Company_Name;
                appCompanyMasterDTO.YR_Company_Con_Name = Data.YR_Company_Con_Name;
                appCompanyMasterDTO.YR_Company_Email = Data.YR_Company_Email;
                appCompanyMasterDTO.YR_Company_Phone = Data.YR_Company_Phone;
                appCompanyMasterDTO.YR_Company_Address = Data.YR_Company_Address;
                appCompanyMasterDTO.YR_Company_City = Data.YR_Company_City;
                appCompanyMasterDTO.YR_Company_State = Data.YR_Company_State;
                appCompanyMasterDTO.YR_Company_Zip = Data.YR_Company_Zip;
                appCompanyMasterDTO.YR_Company_Logo = Data.YR_Company_Logo;
                appCompanyMasterDTO.YR_Company_App_logo = Data.YR_Company_App_logo;
                appCompanyMasterDTO.YR_Company_Support_Email = Data.YR_Company_Support_Email;
                appCompanyMasterDTO.YR_Company_Support_Phone = Data.YR_Company_Support_Phone;
                appCompanyMasterDTO.YR_Company_PDF_Heading = Data.YR_Company_PDF_Heading;
                appCompanyMasterDTO.YR_Company_IsActive = Data.YR_Company_IsActive;
                appCompanyMasterDTO.YR_Company_IsDelete = Data.YR_Company_IsDelete;
                appCompanyMasterDTO.UserID = LoggedInUSerId;
                appCompanyMasterDTO.Type = Data.Type;
                appCompanyMasterDTO.YR_Company_UserID = LoggedInUSerId;

                var AddAppComapany = await Task.Run(() => appCompanyMasterData.AddAppCompanyData(appCompanyMasterDTO));
                return AddAppComapany;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        // this api fetch all  AppCompanyInfo records for edit and update .
        //Get AppCompanyInfo  master
        [Authorize]
        [HttpPost]
        [Route("GetAppCompanyInfoData")]
        public async Task<List<dynamic>> GetAppCompanyInfoData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            AppCompanyMasterDTO appCompanyMasterDTO = new AppCompanyMasterDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<AppCompanyMasterDTO>(s);
                appCompanyMasterDTO.YR_Company_pkeyID = Data.YR_Company_pkeyID;
                appCompanyMasterDTO.Type = Data.Type;

                var GetAppComapany = await Task.Run(() => appCompanyMasterData.GetAppCompanyMasterDetails(appCompanyMasterDTO));
                return GetAppComapany;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //get AppCompany table data using WhereClause
        [Authorize]
        [HttpPost]
        [Route("GetAppCompanyList")]
        public async Task<List<dynamic>> GetAppCompanyList()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<AppCompanyMasterDTO>(s);

                if (Data.Type == 1)
                {
                    Data.UserID = LoggedInUSerId;
                    var GetClientCompanylst = await Task.Run(() => appCompanyMasterData.GetAppCompanyMasterDetails(Data));
                    return GetClientCompanylst;

                }
                else
                {

                    Data.UserID = LoggedInUSerId;
                    var GetAppCompanylst = await Task.Run(() => appCompanyMasterData.GetClientCompanyFilterDetails(Data));
                    return GetAppCompanylst;
                }



            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        // get User Master Filter Data
        //get AppCompany table data using WhereClause
        [Authorize]
        [HttpPost]
        [Route("GetUserFilterList")]
        public async Task<List<dynamic>> GetUserFilterList()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<UserFiltermasterDTO>(s);
                Data.UserID = LoggedInUSerId;
                if (Data.SearchMaster != null)
                {
                    Data.SearchMaster.UserID = LoggedInUSerId;
                }


                switch (Data.Type)
                {

                    case 2:
                        {
                            UserMasterDTO userMasterDTO = new UserMasterDTO();
                            userMasterDTO.User_pkeyID = Data.User_pkeyID;
                            userMasterDTO.Type = 2;

                            objdynamicobj = await Task.Run(() => userMasterData.GetUserDetails(userMasterDTO));
                            break;
                        }

                    case 3:
                        {


                            objdynamicobj = await Task.Run(() => userMasterData.GetUserfilterDetails(Data));
                            break;
                        }
                    case 4:
                        {

                            objdynamicobj = await Task.Run(() => userMasterData.GetUserMasterFilterDetails(Data));
                            break;
                        }
                    case 7:
                        {

                            objdynamicobj = await Task.Run(() => userMasterData.GetUserfilterDetails(Data));
                            break;
                        }
                }



            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

            return objdynamicobj;

        }


        /// <summary>
        /// This API is used to Get Client Data For Drop Down
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        [Route("GetClientDataForDropDown")]
        public async Task<List<dynamic>> GetClientDataForDropDown()

        {
            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                ClientCompanyDTO clientCompanyDTO = new ClientCompanyDTO();
                clientCompanyDTO.UserId = LoggedInUSerId;
                clientCompanyDTO.Type = 5;
                var Getuserlst = await Task.Run(() => clientComapanyMasterData.GetClientDataForDropDown(clientCompanyDTO));
                return Getuserlst;


            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }



        // Add IPL State Details
        [Authorize]
        [HttpPost]
        [Route("PostStateData")]
        public async Task<List<dynamic>> PostStateData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            IPLStateDTO iPLStateDTO = new IPLStateDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<IPLStateDTO>(s);
                iPLStateDTO.IPL_StateID = Data.IPL_StateID;
                iPLStateDTO.IPL_StateName = Data.IPL_StateName;
                iPLStateDTO.IPL_State_IsActive = Data.IPL_State_IsActive;
                iPLStateDTO.IPL_IsDelete = Data.IPL_IsDelete;
                iPLStateDTO.UserID = LoggedInUSerId;
                iPLStateDTO.Type = Data.Type;

                var AddState = await Task.Run(() => iPLStateData.AddSateData(iPLStateDTO));
                return AddState;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        // Add Rushes Details
        [Authorize]
        [HttpPost]
        [Route("PostRushesData")]
        public async Task<List<dynamic>> PostRushesData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            RushesMasterDTO rushesMasterDTO = new RushesMasterDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<RushesMasterDTO>(s);
                rushesMasterDTO.rus_pkeyID = Data.rus_pkeyID;
                rushesMasterDTO.rus_Name = Data.rus_Name;
                rushesMasterDTO.rus_Active = Data.rus_Active;
                rushesMasterDTO.rus_IsActive = Data.rus_IsActive;
                rushesMasterDTO.rus_IsDelete = Data.rus_IsDelete;
                rushesMasterDTO.UserID = LoggedInUSerId;
                rushesMasterDTO.Type = Data.Type;

                var AddRushes = await Task.Run(() => rushesMasterData.AddRushesData(rushesMasterDTO));
                return AddRushes;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        // get Rushes Details
        [Authorize]
        [HttpPost]
        [Route("GetRushesData")]
        public async Task<List<dynamic>> GetRushesData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            RushesMasterDTO rushesMasterDTO = new RushesMasterDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<RushesMasterDTO>(s);
                Data.UserID = LoggedInUSerId;

                switch (Data.Type)
                {
                    case 1:
                        {

                            objdynamicobj = await Task.Run(() => rushesMasterData.GetRushesDetails(Data));
                            break;
                        }
                    //case 2:
                    //    {

                    //        objdynamicobj = await Task.Run(() => rushesMasterData.GetRushesDetails(Data));
                    //        break;
                    //    }
                    case 3:
                        {
                            objdynamicobj = await Task.Run(() => rushesMasterData.GetRushFilterDetails(Data));

                            break;
                        }

                }

                return objdynamicobj;


            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        // get new work oder user meta data
        [Authorize]
        [HttpPost]
        [Route("GetUserMetaData")]
        public async Task<List<dynamic>> GetUserMetaData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            ClientMasterDTO clientMasterDTO = new ClientMasterDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<ClientMasterDTO>(s);
                clientMasterDTO.client_pkyeId = Data.client_pkyeId;
                clientMasterDTO.Type = Data.Type;

                var GetRushes = await Task.Run(() => CclientMasterData.GetClientMasterDetails(clientMasterDTO));
                return GetRushes;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }




        // Add Main Category Details
        [Authorize]
        [HttpPost]
        [Route("PostMainCateData")]
        public async Task<List<dynamic>> PostMainCateData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            MainCategoryDTO mainCategoryDTO = new MainCategoryDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<MainCategoryDTO>(s);
                mainCategoryDTO.Main_Cat_pkeyID = Data.Main_Cat_pkeyID;
                mainCategoryDTO.Main_Cat_Name = Data.Main_Cat_Name;
                // mainCategoryDTO.Main_Cat_Back_Color = Data.Main_Cat_Back_Color;
                if (Data.Main_Cat_Back_Color != null)
                {
                    string color = Data.Main_Cat_Back_Color;
                    var colorList = color.Split('#');
                    mainCategoryDTO.Main_Cat_Back_Color = colorList[1];
                }

                mainCategoryDTO.Main_Cat_Active = Data.Main_Cat_Active;
                mainCategoryDTO.Main_Cat_Con_Icon = Data.Main_Cat_Con_Icon;
                mainCategoryDTO.Main_Cat_IsActive = Data.Main_Cat_IsActive;
                mainCategoryDTO.Main_Cat_IsDelete = Data.Main_Cat_IsDelete;
                mainCategoryDTO.UserID = LoggedInUSerId;
                mainCategoryDTO.Type = Data.Type;

                var AddmainCate = await Task.Run(() => mainCategoryData.AddMainCategoryData(mainCategoryDTO));
                return AddmainCate;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //delete main category
        [Authorize]
        [HttpPost]
        [Route("DeleteMainCateData")]
        public async Task<List<dynamic>> DeleteMainCateData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            MainCategoryDTO mainCategoryDTO = new MainCategoryDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<MainCategoryDTO>(s);
                mainCategoryDTO.Main_Cat_pkeyID = Data.Main_Cat_pkeyID;
                mainCategoryDTO.Main_Cat_Active = Data.Main_Cat_Active;
                mainCategoryDTO.Main_Cat_IsActive = Data.Main_Cat_IsActive;
                mainCategoryDTO.Main_Cat_IsDelete = Data.Main_Cat_IsDelete;
                mainCategoryDTO.Type = Data.Type;
                mainCategoryDTO.UserID = LoggedInUSerId;
                var delCate = await Task.Run(() => mainCategoryData.AddMainCategoryData(mainCategoryDTO));
                return delCate;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }



        // Get Main Category Details
        [Authorize]
        [HttpPost]
        [Route("GetMainCateData")]
        public async Task<List<dynamic>> GetMainCateData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            MainCategoryDTO mainCategoryDTO = new MainCategoryDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<MainCategoryDTO>(s);
                mainCategoryDTO.Main_Cat_pkeyID = Data.Main_Cat_pkeyID;
                mainCategoryDTO.UserID = LoggedInUSerId;
                mainCategoryDTO.Type = Data.Type;

                var GetmainCate = await Task.Run(() => mainCategoryData.GetMainCategoryDetails(mainCategoryDTO));
                return GetmainCate;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //filter data
        [Authorize]
        [HttpPost]
        [Route("GetCatefilterData")]
        public async Task<List<dynamic>> GetCatefilterData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            MainCategoryDTO mainCategoryDTO = new MainCategoryDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<MainCategoryDTO>(s);
                Data.UserID = LoggedInUSerId;

                var GetfilterCate = await Task.Run(() => mainCategoryData.GetCategoryFilterDetails(Data));
                return GetfilterCate;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        // Add New Work Order setting Details
        [Authorize]
        [HttpPost]
        [Route("PostWorkOrderSettingData")]
        public async Task<List<dynamic>> PostWorkOrderSettingData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            NewWorkOrderSettingDTO newWorkOrderSettingDTO = new NewWorkOrderSettingDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<NewWorkOrderSettingDTO>(s);

                newWorkOrderSettingDTO.WO_Sett_pkeyID = Data.WO_Sett_pkeyID;
                newWorkOrderSettingDTO.WO_Sett_CompanyID = Data.WO_Sett_CompanyID;
                newWorkOrderSettingDTO.WO_Sett_Allow_Dup_Num = Data.WO_Sett_Allow_Dup_Num;
                newWorkOrderSettingDTO.WO_Sett_Auto_Inc_GoBack = Data.WO_Sett_Auto_Inc_GoBack;
                newWorkOrderSettingDTO.WO_Sett_Auto_Inc_NeedInfo = Data.WO_Sett_Auto_Inc_NeedInfo;
                newWorkOrderSettingDTO.WO_Sett_Auto_Inc_Dup = Data.WO_Sett_Auto_Inc_Dup;
                newWorkOrderSettingDTO.WO_Sett_Auto_Inc_Recurring = Data.WO_Sett_Auto_Inc_Recurring;
                newWorkOrderSettingDTO.WO_Sett_Auto_Assign = Data.WO_Sett_Auto_Assign;
                newWorkOrderSettingDTO.WO_Sett_Detect_Pricing = Data.WO_Sett_Detect_Pricing;
                newWorkOrderSettingDTO.WO_Sett_Remove_Doller = Data.WO_Sett_Remove_Doller;
                newWorkOrderSettingDTO.WO_Sett_IsDelete = Data.WO_Sett_IsDelete;
                newWorkOrderSettingDTO.WO_Sett_IsActive = Data.WO_Sett_IsActive;
                newWorkOrderSettingDTO.UserID = LoggedInUSerId;
                newWorkOrderSettingDTO.Wo_Sett_Comapny_SAlert = Data.Wo_Sett_Comapny_SAlert;
                newWorkOrderSettingDTO.Wo_Sett_Custom_Titlebar = Data.Wo_Sett_Custom_Titlebar;
                newWorkOrderSettingDTO.Wo_Sett_Default_Time = Data.Wo_Sett_Default_Time;
                newWorkOrderSettingDTO.Type = Data.Type;
                newWorkOrderSettingDTO.WO_Sett_UserId = LoggedInUSerId;

                var Addwoordersetting = await Task.Run(() => newWorkOrderSettingData.AddNewWorkOrderSettingData(newWorkOrderSettingDTO));
                return Addwoordersetting;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        // Get New Work Order setting Details
        [Authorize]
        [HttpPost]
        [Route("GetWorkOrderSettingData")]
        public async Task<List<dynamic>> GetWorkOrderSettingData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            NewWorkOrderSettingDTO newWorkOrderSettingDTO = new NewWorkOrderSettingDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<NewWorkOrderSettingDTO>(s);

                newWorkOrderSettingDTO.WO_Sett_pkeyID = Data.WO_Sett_pkeyID;
                newWorkOrderSettingDTO.Type = Data.Type;
                newWorkOrderSettingDTO.WO_Sett_UserId = LoggedInUSerId;

                var Getwoordersetting = await Task.Run(() => newWorkOrderSettingData.GetNewworkordersettingDetails(newWorkOrderSettingDTO));
                return Getwoordersetting;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        // Add General Work Order setting Details
        [Authorize]
        [HttpPost]
        [Route("PostGeneralWorkOrderSettingData")]
        public async Task<List<dynamic>> PostGeneralWorkOrderSettingData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            GeneralWorkOrderSettingDTO generalWorkOrderSettingDTO = new GeneralWorkOrderSettingDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<GeneralWorkOrderSettingDTO>(s);

                generalWorkOrderSettingDTO.GW_Sett_pkeyID = Data.GW_Sett_pkeyID;
                generalWorkOrderSettingDTO.GW_Sett_CompanyID = Data.GW_Sett_CompanyID;
                generalWorkOrderSettingDTO.GW_Sett_Field_Complete = Data.GW_Sett_Field_Complete;
                generalWorkOrderSettingDTO.GW_Sett_Allow_Contractor = Data.GW_Sett_Allow_Contractor;
                generalWorkOrderSettingDTO.GW_Sett_Assigned_Unread = Data.GW_Sett_Assigned_Unread;
                generalWorkOrderSettingDTO.GW_Sett_Allow_Estimated = Data.GW_Sett_Allow_Estimated;
                generalWorkOrderSettingDTO.GW_Sett_Require_Estimated = Data.GW_Sett_Require_Estimated;
                generalWorkOrderSettingDTO.GW_Sett_Sent_Ass_Cooradinator = Data.GW_Sett_Sent_Ass_Cooradinator;
                generalWorkOrderSettingDTO.GW_Sett_Sent_Ass_Processor = Data.GW_Sett_Sent_Ass_Processor;
                generalWorkOrderSettingDTO.GW_Sett_Sent_Email_Multiple = Data.GW_Sett_Sent_Email_Multiple;
                generalWorkOrderSettingDTO.GW_Sett_StaffName = Data.GW_Sett_StaffName;
                generalWorkOrderSettingDTO.GW_Sett_IsDelete = Data.GW_Sett_IsDelete;
                generalWorkOrderSettingDTO.GW_Sett_IsActive = Data.GW_Sett_IsActive;
                generalWorkOrderSettingDTO.UserID = LoggedInUSerId;
                generalWorkOrderSettingDTO.Type = Data.Type;
                generalWorkOrderSettingDTO.GW_Sett_UserID = LoggedInUSerId;

                var AddGenwoordersetting = await Task.Run(() => generalWorkOrderSettingData.AddGeneralWorkOrderSettingData(generalWorkOrderSettingDTO));
                return AddGenwoordersetting;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        // Get General Work Order setting Details
        [Authorize]
        [HttpPost]
        [Route("GetGeneralWorkOrderSettingData")]
        public async Task<List<dynamic>> GetGeneralWorkOrderSettingData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            GeneralWorkOrderSettingDTO generalWorkOrderSettingDTO = new GeneralWorkOrderSettingDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<GeneralWorkOrderSettingDTO>(s);

                generalWorkOrderSettingDTO.GW_Sett_pkeyID = Data.GW_Sett_pkeyID;
                generalWorkOrderSettingDTO.Type = Data.Type;
                generalWorkOrderSettingDTO.GW_Sett_UserID = LoggedInUSerId;
                var GetGenwoordersetting = await Task.Run(() => generalWorkOrderSettingData.GetGeneralworkordersettingDetails(generalWorkOrderSettingDTO));
                return GetGenwoordersetting;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        // Add Customer Number Details
        [Authorize]
        [HttpPost]
        [Route("PostCustomerNumberData")]
        public async Task<List<dynamic>> PostCustomerNumberData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            CustomerNumberDTO customerNumberDTO = new CustomerNumberDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<CustomerNumberDTO>(s);

                customerNumberDTO.Cust_Num_pkeyId = Data.Cust_Num_pkeyId;
                customerNumberDTO.Cust_Num_Number = Data.Cust_Num_Number;
                customerNumberDTO.Cust_Num_Active = Data.Cust_Num_Active;
                customerNumberDTO.Cust_Num_IsDelete = Data.Cust_Num_IsDelete;
                customerNumberDTO.Cust_Num_IsActive = Data.Cust_Num_IsActive;
                customerNumberDTO.UserID = LoggedInUSerId;
                customerNumberDTO.Type = Data.Type;

                var AddCustNum = await Task.Run(() => customerNumberData.AddCustomerNumberData(customerNumberDTO));
                return AddCustNum;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        // Get Customer Number Details
        [Authorize]
        [HttpPost]
        [Route("GetCustomerNumberData")]
        public async Task<List<dynamic>> GetCustomerNumberData()

        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            CustomerNumberDTO customerNumberDTO = new CustomerNumberDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<CustomerNumberDTO>(s);
                Data.UserID = LoggedInUSerId;

                switch (Data.Type)
                {
                    case 1:
                        {

                            customerNumberDTO.Cust_Num_pkeyId = Data.Cust_Num_pkeyId;
                            customerNumberDTO.Type = Data.Type;
                            customerNumberDTO.UserID = LoggedInUSerId;
                            objdynamicobj = await Task.Run(() => customerNumberData.GetCustomerNumberDetails(customerNumberDTO));
                            break;
                        }
                    case 2:
                        {

                            customerNumberDTO.Cust_Num_pkeyId = Data.Cust_Num_pkeyId;
                            customerNumberDTO.Type = Data.Type;
                            customerNumberDTO.UserID = LoggedInUSerId;

                            objdynamicobj = await Task.Run(() => customerNumberData.GetCustomerNumberDetails(customerNumberDTO));
                            break;
                        }
                    case 3:
                        {
                            objdynamicobj = await Task.Run(() => customerNumberData.GetCustomerNumberFilterDetails(Data));

                            break;
                        }

                }

                return objdynamicobj;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        //get AppCompany table data Single data
        [Authorize]
        [HttpPost]
        [Route("GetAppCompanySingle")]
        public async Task<List<dynamic>> GetAppCompanySingle()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            GetAppCompanyMaster getAppCompanyMaster = new GetAppCompanyMaster();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<AppCompanyMasterDTO>(s);
                Data.UserID = LoggedInUSerId;
                var GetClientCompanylst = await Task.Run(() => getAppCompanyMaster.GetSingleAppCompanyMasterDetails(Data));
                return GetClientCompanylst;



            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }



        // post task group popup // type 4 for delete particular records
        [Authorize]
        [HttpPost]
        [Route("PostTaskGroupDetails")]
        public async Task<List<dynamic>> PostTaskGroupDetails()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            TaskGroupMasterDTO taskGroupMasterDTO = new TaskGroupMasterDTO();

            TaskGroupMaster taskGroupMaster = new TaskGroupMaster();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<TaskGroupMasterDTO>(s);
                Data.UserID = LoggedInUSerId;

                if (Data.Type == 4)
                {
                    var data = taskGroupMaster.AddTaskGroup(Data);
                }
                else
                {
                    var data = taskGroupMaster.AddTaskGroupData(Data);
                }


                taskGroupMasterDTO.Type = 1;
                taskGroupMasterDTO.UserID = LoggedInUSerId;
                objdynamicobj = await Task.Run(() => taskGroupMaster.GetAppCompanyMasterDetails(taskGroupMasterDTO));
                return objdynamicobj;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        // get task group list
        [Authorize]
        [HttpPost]
        [Route("GetTaskGroupDetails")]
        public async Task<List<dynamic>> GetTaskGroupDetails()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            TaskGroupMasterDTO taskGroupMasterDTO = new TaskGroupMasterDTO();

            TaskGroupMaster taskGroupMaster = new TaskGroupMaster();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                taskGroupMasterDTO.Type = 1;
                taskGroupMasterDTO.UserID = LoggedInUSerId;
                objdynamicobj = await Task.Run(() => taskGroupMaster.GetAppCompanyMasterDetails(taskGroupMasterDTO));
                return objdynamicobj;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }





        // post Task Master
        [Authorize]
        [HttpPost]
        [Route("PostTaskMaster")]
        public async Task<List<dynamic>> PostTaskMaster()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            TaskMasterDTO taskMasterDTO = new TaskMasterDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<TaskMasterDTO>(s);
                Data.UserID = Convert.ToInt32(LoggedInUSerId);

                taskMasterDTO.Task_pkeyID = Data.Task_pkeyID;
                taskMasterDTO.Task_Name = Data.Task_Name;
                taskMasterDTO.Task_Type = Data.Task_Type;
                taskMasterDTO.Task_Group = Data.Task_Group;
                taskMasterDTO.Task_Contractor_UnitPrice = Data.Task_Contractor_UnitPrice;
                taskMasterDTO.Task_UOM = Data.Task_UOM;
                taskMasterDTO.Task_Client_UnitPrice = Data.Task_Client_UnitPrice;
                taskMasterDTO.Task_Photo_Label_Name = Data.Task_Photo_Label_Name;
                taskMasterDTO.Task_IsActive = Data.Task_IsActive;
                taskMasterDTO.Type = Data.Type;
                taskMasterDTO.Task_AutoInvoiceComplete = Data.Task_AutoInvoiceComplete;
                taskMasterDTO.UserID = Data.UserID;
                taskMasterDTO.Task_Flat_Free = Data.Task_Flat_Free;
                taskMasterDTO.Task_Price_Edit = Data.Task_Price_Edit;
                taskMasterDTO.Task_Disable_Default = Data.Task_Disable_Default;
                taskMasterDTO.Task_Auto_Assign = Data.Task_Auto_Assign;
                var GetCustNum = await Task.Run(() => Task_MasterDataObj.AddTaskMasterData(taskMasterDTO));
                return GetCustNum;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }




        // get task master List
        [Authorize]
        [HttpPost]
        [Route("GetTaskMasterDetails")]
        public async Task<List<dynamic>> GetTaskMasterDetails()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();


            TaskMasterDTO taskMasterDTO = new TaskMasterDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<TaskMasterDTO>(s);
                Data.UserID = Convert.ToInt32(LoggedInUSerId);
                switch (Data.Type)
                {
                    case 1:
                        {
                            objdynamicobj = await Task.Run(() => Task_MasterDataObj.GetTaskMasterrDetails(Data));
                            break;
                        }
                    case 2:
                        {
                            objdynamicobj = await Task.Run(() => Task_MasterDataObj.GetTaskMasterrDetails(Data));
                            break;
                        }
                    case 3:
                        {
                            objdynamicobj = await Task.Run(() => Task_MasterDataObj.GetTaskFilterDetails(Data));
                            break;
                        }
                    case 4:
                        {
                            objdynamicobj = await Task.Run(() => Task_MasterDataObj.GetTaskFilterDetails(Data));
                            break;
                        }
                }


                return objdynamicobj;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }



        // post Task Setting child Master
        [Authorize]
        [HttpPost]
        [Route("PostTaskSettingChildMaster")]
        public async Task<List<dynamic>> PostTaskSettingChildMaster()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            TaskSettingChildDTO taskSettingChildDTO = new TaskSettingChildDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<TaskSettingChildDTO>(s);

                taskSettingChildDTO.Task_sett_pkeyID = Data.Task_sett_pkeyID;
                taskSettingChildDTO.Task_sett_ID = Data.Task_sett_ID;
                taskSettingChildDTO.Task_sett_Company = Data.Task_sett_Company;
                taskSettingChildDTO.Task_sett_State = Data.Task_sett_State;
                taskSettingChildDTO.Task_sett_Country = Data.Task_sett_Country;
                taskSettingChildDTO.Task_sett_Zip = Data.Task_sett_Zip;
                taskSettingChildDTO.Task_sett_Contractor = Data.Task_sett_Contractor;
                taskSettingChildDTO.Task_sett_Customer = Data.Task_sett_Customer;
                taskSettingChildDTO.Task_sett_Lone = Data.Task_sett_Lone;
                taskSettingChildDTO.Task_sett_Con_Unit_Price = Data.Task_sett_Con_Unit_Price;
                taskSettingChildDTO.Task_sett_CLI_Unit_Price = Data.Task_sett_CLI_Unit_Price;
                taskSettingChildDTO.Task_sett_Flat_Free = Data.Task_sett_Flat_Free;
                taskSettingChildDTO.Task_sett_Price_Edit = Data.Task_sett_Price_Edit;
                taskSettingChildDTO.Task_sett_IsActive = Data.Task_sett_IsActive;
                taskSettingChildDTO.Task_sett_IsDelete = Data.Task_sett_IsDelete;
                taskSettingChildDTO.UserID = LoggedInUSerId;
                taskSettingChildDTO.Type = Data.Type;
                taskSettingChildDTO.Task_sett_Disable_Default = Data.Task_sett_Disable_Default;
                taskSettingChildDTO.Task_sett_WorkType = Data.Task_sett_WorkType;
                taskSettingChildDTO.Task_sett_LOT_Min = Data.Task_sett_LOT_Min;
                taskSettingChildDTO.Task_sett_LOT_Max = Data.Task_sett_LOT_Max;

                var Addtaskchild = await Task.Run(() => taskSettingChildData.AddTaskSettingChildData(taskSettingChildDTO));
                return Addtaskchild;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        // DeleteTask Setting child Master
        [Authorize]
        [HttpPost]
        [Route("DeleteTaskSettingChildMaster")]
        public async Task<List<dynamic>> DeleteTaskSettingChildMaster()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            TaskSettingChildDTO taskSettingChildDTO = new TaskSettingChildDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<TaskSettingChildDTO>(s);

                taskSettingChildDTO.Task_sett_pkeyID = Data.Task_sett_pkeyID;
                taskSettingChildDTO.Type = Data.Type;
                taskSettingChildDTO.UserID = LoggedInUSerId;
                var deltaskchild = await Task.Run(() => taskSettingChildData.AddTaskSettingChildData(taskSettingChildDTO));
                return deltaskchild;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        // Get Task Setting child Master
        [Authorize]
        [HttpPost]
        [Route("GetTaskSettingChildMaster")]
        public async Task<List<dynamic>> GetTaskSettingChildMaster()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            TaskSettingChildDTO taskSettingChildDTO = new TaskSettingChildDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<TaskSettingChildDTO>(s);

                taskSettingChildDTO.Task_sett_pkeyID = Data.Task_sett_pkeyID;
                taskSettingChildDTO.Task_sett_ID = Data.Task_sett_ID;
                taskSettingChildDTO.Type = Data.Type;

                var Gettaskchild = await Task.Run(() => taskSettingChildData.GetTaskSettingChildDetails(taskSettingChildDTO));
                return Gettaskchild;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        // post Work Type Task Setting child Master
        [Authorize]
        [HttpPost]
        [Route("PostWorkTypeTaskChildMaster")]
        public async Task<List<dynamic>> PostWorkTypeTaskChildMaster()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            WorkTypeTaskChildDTO workTypeTaskChildDTO = new WorkTypeTaskChildDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<WorkTypeTaskChildDTO>(s);

                workTypeTaskChildDTO.WT_Task_pkeyID = Data.WT_Task_pkeyID;
                workTypeTaskChildDTO.WT_Task_ID = Data.WT_Task_ID;
                workTypeTaskChildDTO.WT_Task_Company = Data.WT_Task_Company;
                workTypeTaskChildDTO.WT_Task_State = Data.WT_Task_State;
                workTypeTaskChildDTO.WT_Task_Customer = Data.WT_Task_Customer;
                workTypeTaskChildDTO.WT_Task_LoneType = Data.WT_Task_LoneType;
                workTypeTaskChildDTO.WT_Task_WorkType = Data.WT_Task_WorkType;
                workTypeTaskChildDTO.WT_Task_ClientDueDateTo = Data.WT_Task_ClientDueDateTo;
                workTypeTaskChildDTO.WT_Task_ClientDueDateFrom = Data.WT_Task_ClientDueDateFrom;
                workTypeTaskChildDTO.WT_Task_IsActive = Data.WT_Task_IsActive;
                workTypeTaskChildDTO.WT_Task_IsDelete = Data.WT_Task_IsDelete;
                workTypeTaskChildDTO.UserID = LoggedInUSerId;
                workTypeTaskChildDTO.Type = Data.Type;

                var Addwttaskchild = await Task.Run(() => workTypeTaskChildData.AddWorkTypeTaskChildData(workTypeTaskChildDTO));
                return Addwttaskchild;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        // delete Work Type Task Setting child Master
        [Authorize]
        [HttpPost]
        [Route("DeleteWorkTypeTaskChildMaster")]
        public async Task<List<dynamic>> DeleteWorkTypeTaskChildMaster()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            WorkTypeTaskChildDTO workTypeTaskChildDTO = new WorkTypeTaskChildDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<WorkTypeTaskChildDTO>(s);

                workTypeTaskChildDTO.WT_Task_pkeyID = Data.WT_Task_pkeyID;
                workTypeTaskChildDTO.Type = Data.Type;
                workTypeTaskChildDTO.UserID = LoggedInUSerId;
                var delwttaskchild = await Task.Run(() => workTypeTaskChildData.AddWorkTypeTaskChildData(workTypeTaskChildDTO));
                return delwttaskchild;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        // Get Work Type Task Setting child Master
        [Authorize]
        [HttpPost]
        [Route("GetWorkTypeTaskChildMaster")]
        public async Task<List<dynamic>> GetWorkTypeTaskChildMaster()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            WorkTypeTaskChildDTO workTypeTaskChildDTO = new WorkTypeTaskChildDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<WorkTypeTaskChildDTO>(s);

                workTypeTaskChildDTO.WT_Task_pkeyID = Data.WT_Task_pkeyID;
                workTypeTaskChildDTO.WT_Task_ID = Data.WT_Task_ID;
                workTypeTaskChildDTO.Type = Data.Type;

                var Getwttaskchild = await Task.Run(() => workTypeTaskChildData.GetWorkTypeTaskChildDetails(workTypeTaskChildDTO));
                return Getwttaskchild;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }









        // Post task Filter data all mutiple fields
        [Authorize]
        [HttpPost]
        [Route("PostTaskMasterFilterData")]
        public async Task<List<dynamic>> PostTaskMasterFilterData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            TaskFilterDTO taskFilterDTO = new TaskFilterDTO();

            TaskFilterMaster taskFilterMaster = new TaskFilterMaster();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<TaskFilterDTO>(s);
                Data.UserID = Convert.ToInt32(LoggedInUSerId);

                var Returnkey = await Task.Run(() => taskFilterMaster.AddTaskSettingCustomPriceFilter(Data));
                return Returnkey;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        // Gettask Filter data all mutiple fields
        [Authorize]
        [HttpPost]
        [Route("GetTaskMasterFilterData")]
        public async Task<List<dynamic>> GetTaskMasterFilterData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            TaskFilterDTO taskFilterDTO = new TaskFilterDTO();

            TaskFilterMaster taskFilterMaster = new TaskFilterMaster();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<TaskFilterDTO>(s);


                var Returnkey = await Task.Run(() => taskFilterMaster.GetTaskFilterData(Data));
                return Returnkey;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        // This Api Responsiable For Showing the work order id wise client result .
        //get work order client result data
        [Authorize]
        [HttpPost]
        [Route("GetWorkOrderDataClientResult")]
        public async Task<List<dynamic>> GetWorkOrderDataClientResult()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<workOrderxDTO>(s);
                workOrder.workOrder_ID = Data.workOrder_ID;
                workOrder.Type = Data.Type;
                workOrder.UserID = LoggedInUSerId;


                var Getworkorder = await Task.Run(() => workOrderMasterData.GetWorkOrderDataClientResult(workOrder));
                return Getworkorder;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }



        // post Damage Master data
        [Authorize]
        [HttpPost]
        [Route("PostDamageMaster")]
        public async Task<List<dynamic>> PostDamageMaster()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            DamageMasterDTO damageMasterDTO = new DamageMasterDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<DamageMasterDTO>(s);

                damageMasterDTO.Damage_pkeyID = Data.Damage_pkeyID;
                damageMasterDTO.Damage_Type = Data.Damage_Type;
                damageMasterDTO.Damage_Int = Data.Damage_Int;
                damageMasterDTO.Damage_Location = Data.Damage_Location;
                damageMasterDTO.Damage_Qty = Data.Damage_Qty;
                damageMasterDTO.Damage_Estimate = Data.Damage_Estimate;
                damageMasterDTO.Damage_Disc = Data.Damage_Disc;
                damageMasterDTO.Damage_IsActive = Data.Damage_IsActive;
                damageMasterDTO.Damage_IsDelete = Data.Damage_IsDelete;
                damageMasterDTO.UserID = LoggedInUSerId;
                damageMasterDTO.Type = Data.Type;

                var GetCustNum = await Task.Run(() => damageMasterData.AddDamageMasterData(damageMasterDTO));
                return GetCustNum;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        //get Damage Master data
        [Authorize]
        [HttpPost]
        [Route("GetDamageMaster")]
        public async Task<List<dynamic>> GetDamageMaster()

        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            DamageMasterDTO damageMasterDTO = new DamageMasterDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<DamageMasterDTO>(s);
                Data.UserID = LoggedInUSerId;

                switch (Data.Type)
                {
                    case 1:
                        {
                            objdynamicobj = await Task.Run(() => damageMasterData.GetDamageMasterDetails(Data));
                            break;
                        }
                    case 2:
                        {

                            objdynamicobj = await Task.Run(() => damageMasterData.GetDamageMasterDetails(Data));
                            break;
                        }
                    case 3:
                        {
                            objdynamicobj = await Task.Run(() => damageMasterData.GetDamageFilterDetails(Data));

                            break;
                        }

                }

                return objdynamicobj;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }




        //delete preset text
        //why separate becoz they lot of code inser qyery slow performance app
        [Authorize]
        [HttpPost]
        [Route("DeletePresetTextTaskMasterFilterData")]
        public async Task<List<dynamic>> DeletePresetTextTaskMasterFilterData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            TaskPresetDTO taskPresetDTO = new TaskPresetDTO();

            TaskPresetData taskPresetData = new TaskPresetData();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<TaskPresetDTO>(s);
                Data.UserID = LoggedInUSerId;
                taskPresetDTO.Task_Preset_pkeyId = Data.Task_Preset_pkeyId;
                taskPresetDTO.UserID = Data.UserID;
                taskPresetDTO.Type = 4;

                var Returnkey = await Task.Run(() => taskPresetData.AddTaskPresetChildData(taskPresetDTO));
                return Returnkey;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        // for Work Order Import Details
        [Authorize]
        [HttpPost]
        [Route("PostWorkOrderImportMasterDetails")]
        public async Task<List<dynamic>> PostWorkOrderImportMasterDetails()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<WorkOrderImport_MasterData_DTO>(s);
                WorkOrderImport_MasterData_DTO workOrderImport_MasterData_DTO = new WorkOrderImport_MasterData_DTO();

                workOrderImport_MasterData_DTO.WI_Pkey_ID = Data.WI_Pkey_ID;
                workOrderImport_MasterData_DTO.WI_ImportFrom = Data.WI_ImportFrom;
                workOrderImport_MasterData_DTO.WI_SetClientCompany = Data.WI_SetClientCompany;
                workOrderImport_MasterData_DTO.WI_LoginName = Data.WI_LoginName;
                workOrderImport_MasterData_DTO.WI_Password = Data.WI_Password;
                workOrderImport_MasterData_DTO.WI_AlertEmail = Data.WI_AlertEmail;
                workOrderImport_MasterData_DTO.WI_FriendlyName = Data.WI_FriendlyName;
                workOrderImport_MasterData_DTO.WI_SkipComments = Data.WI_SkipComments;
                workOrderImport_MasterData_DTO.WI_SkipLineItems = Data.WI_SkipLineItems;
                workOrderImport_MasterData_DTO.WI_SetCategory = Data.WI_SetCategory;
                workOrderImport_MasterData_DTO.WI_StateFilter = Data.WI_StateFilter;
                workOrderImport_MasterData_DTO.WI_Discount_Import = Data.WI_Discount_Import;
                workOrderImport_MasterData_DTO.WI_IsActive = Data.WI_IsActive;
                workOrderImport_MasterData_DTO.WI_IsDeleted = Data.WI_IsDeleted;
                workOrderImport_MasterData_DTO.WI_Processor = Data.WI_Processor;
                workOrderImport_MasterData_DTO.WI_Coordinator = Data.WI_Coordinator;
                workOrderImport_MasterData_DTO.WI_FB_LoginName = Data.WI_FB_LoginName;
                workOrderImport_MasterData_DTO.WI_FB_Password = Data.WI_FB_Password;
                workOrderImport_MasterData_DTO.WI_Res_Code = Data.WI_Res_Code;

                workOrderImport_MasterData_DTO.Type = Data.Type;
                workOrderImport_MasterData_DTO.UserId = Convert.ToInt32(LoggedInUSerId);

                var PostWorkOrderImportMasterDetails = await Task.Run(() => workOrderImport_MasterData.WorkOrderImportMasterDetails(workOrderImport_MasterData_DTO));
                return PostWorkOrderImportMasterDetails;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [Authorize]
        [HttpPost]
        [Route("GetWorkOrderImportDetails")]
        public async Task<List<dynamic>> GetWorkOrderImportDetails()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<WorkOrderImport_MasterData_DTO>(s);
                Data.UserId = LoggedInUSerId;
                WorkOrderImport_MasterData_DTO workOrderImport_MasterData_DTO = new WorkOrderImport_MasterData_DTO();

                switch (Data.Type)
                {
                    case 1:
                        {
                            objdynamicobj = await Task.Run(() => workOrderImport_MasterData.GetWorkOrderImportDetails(Data));
                            break;
                        }
                    case 2:
                        {

                            objdynamicobj = await Task.Run(() => workOrderImport_MasterData.GetWorkOrderImportDetails(Data));
                            break;
                        }
                    case 4:
                        {
                            objdynamicobj = await Task.Run(() => workOrderImport_MasterData.GetDamageFilterDetails(Data));

                            break;
                        }
                    case 7:
                        {
                            objdynamicobj = await Task.Run(() => workOrderImport_MasterData.GetWorkOrderImportDetails(Data));
                            break;
                        }

                }

                return objdynamicobj;


            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        // for Work Order Import Queue Transaction detais
        [Authorize]
        [HttpPost]
        [Route("PostWorkOrderImportQueueTrans")]
        public async Task<List<dynamic>> PostWorkOrderImportQueueTrans()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Import_Queue_Trans_DTO>(s);
                Import_Queue_Trans_DTO import_Queue_Trans_DTO = new Import_Queue_Trans_DTO();

                import_Queue_Trans_DTO.Imrt_PkeyId = Data.Imrt_PkeyId;
                import_Queue_Trans_DTO.Imrt_Wo_ID = Data.Imrt_Wo_ID;
                import_Queue_Trans_DTO.Imrt_Wo_Import_ID = Data.Imrt_Wo_Import_ID;
                import_Queue_Trans_DTO.Imrt_Wo_Count = Data.Imrt_Wo_Count;
                import_Queue_Trans_DTO.Imrt_Last_Run = Data.Imrt_Last_Run;
                import_Queue_Trans_DTO.Imrt_Next_Run = Data.Imrt_Next_Run;
                import_Queue_Trans_DTO.Imrt_Status_Msg = Data.Imrt_Status_Msg;
                import_Queue_Trans_DTO.Imrt_State_Filter = Data.Imrt_State_Filter;
                import_Queue_Trans_DTO.Imrt_Active = Data.Imrt_Active;
                import_Queue_Trans_DTO.Imrt_IsDelete = Data.Imrt_IsDelete;
                import_Queue_Trans_DTO.Type = Data.Type;
                import_Queue_Trans_DTO.UserID = LoggedInUSerId;
                import_Queue_Trans_DTO.Imrt_Import_From_ID = Data.Imrt_Import_From_ID;

                var ImportQueueTransDetails = await Task.Run(() => import_Queue_TransData.AddImportQueueTransData(import_Queue_Trans_DTO, 1));
                return ImportQueueTransDetails;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //Get Work Order Import Details
        [Authorize]
        [HttpPost]
        [Route("GetWorkOrderImportQueueTrans")]
        public async Task<List<dynamic>> GetWorkOrderImportQueueTrans()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Import_Queue_Trans_DTO>(s);


                var GetImportQueueTransData = await Task.Run(() => import_Queue_TransData.GetImportQueueTransDetails(Data));
                return GetImportQueueTransData;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //Delete Work Order Import Details
        [Authorize]
        [HttpPost]
        [Route("DeleteWorkOrderImportQueueTrans")]
        public async Task<List<dynamic>> DeleteWorkOrderImportQueueTrans()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Import_Queue_Trans_DTO>(s);
                Import_Queue_Trans_DTO import_Queue_Trans_DTO = new Import_Queue_Trans_DTO();

                import_Queue_Trans_DTO.Imrt_PkeyId = Data.Imrt_PkeyId;
                import_Queue_Trans_DTO.Type = Data.Type;
                import_Queue_Trans_DTO.UserID = LoggedInUSerId;

                var DelImportQueueTransData = await Task.Run(() => import_Queue_TransData.AddImportQueueTransData(import_Queue_Trans_DTO, 0));
                return DelImportQueueTransData;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("GetPastWorkOrderHistory")]
        public async Task<List<dynamic>> GetPastWorkOrderHistory()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<WorkOrderHistoryDTO>(s);
                WorkOrderHistoryDTO workOrderHistoryDTO = new WorkOrderHistoryDTO();
                PastWorkOrderHistoryData pastWorkOrderHistoryData = new PastWorkOrderHistoryData();
                workOrderHistoryDTO.workOrder_ID = Data.workOrder_ID;
                workOrderHistoryDTO.UserID = LoggedInUSerId;
                workOrderHistoryDTO.Type = Data.Type;


                var GetpastWoHistory = await Task.Run(() => pastWorkOrderHistoryData.Get_Past_WorkOrderHistoryDetails(workOrderHistoryDTO));
                return GetpastWoHistory;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("GetWorkOrderHistory")]
        public async Task<List<dynamic>> GetWorkOrderHistory()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<WorkOrderHistoryDTO>(s);
                WorkOrderHistoryDTO workOrderHistoryDTO = new WorkOrderHistoryDTO();

                workOrderHistoryDTO.workOrder_ID = Data.workOrder_ID;
                workOrderHistoryDTO.Type = Data.Type;


                var GetWoHistory = await Task.Run(() => workOrderHistoryData.Get_WorkOrderHistoryDetails(workOrderHistoryDTO));
                return GetWoHistory;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        // get user menu access
        [Authorize]
        [HttpPost]
        [Route("GetUserMenuAccess")]
        public async Task<List<dynamic>> GetUserMenuAccess()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<UserMenuAcessDTO>(s);
                Data.UserID = LoggedInUSerId;

                var GetUserAcess = await Task.Run(() => groupMenuRelationData.GetUserAcessDetails(Data));
                return GetUserAcess;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        //get column for workOrder
        [Authorize]
        [HttpPost]
        [Route("GetColumnWorkOrder")]
        public async Task<List<dynamic>> GetColumnWorkOrder()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<WorkOrder_Column_DTO>(s);
                //WorkOrder_Column_DTO workOrder_Column_DTO = new WorkOrder_Column_DTO();
                //workOrder_Column_DTO.Type = 1;

                var GetColumn = await Task.Run(() => workOrder_Column_Data.GetWorkOrderData(Data));
                return GetColumn;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //post column for workOrder
        [Authorize]
        [HttpPost]
        [Route("postColumnWorkOrder")]
        public async Task<List<dynamic>> postColumnWorkOrder()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<WorkOrder_Column_Json_DTO>(s);
                Data.UserID = LoggedInUSerId;
                Data.WC_UserId = LoggedInUSerId;

                var postColumn = await Task.Run(() => workOrder_Column_Data.AddWorkorderColumnJsonData(Data));
                return postColumn;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //get drd group role

        // [Authorize]
        [HttpPost]
        [Route("GetGroupRoleDRD")]
        public async Task<List<dynamic>> GetGroupRoleDRD()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<GroupRoleDRDMaster>(s);
                Data.UserId = LoggedInUSerId;

                var getdrd = await Task.Run(() => groupMastersData.GetGrouproleMasterDetails(Data));
                return getdrd;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        //get column for workOrder json data & side listbox single data
        [Authorize]
        [HttpPost]
        [Route("GetJsonColumnWorkOrder")]
        public async Task<List<dynamic>> GetJsonColumnWorkOrder()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<WorkOrder_Column_Json_DTO>(s);
                Data.UserID = LoggedInUSerId;
                Data.WC_UserId = LoggedInUSerId;

                var GetColumn = await Task.Run(() => workOrder_Column_Data.GetColumnJsonDetails(Data));
                return GetColumn;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("UpadateWorkOrderStatusDetails")]
        public async Task<List<dynamic>> UpadateWorkOrderStatusDetails()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<WorkOrderUpdateStatusDTO>(s);
                Data.UserId = LoggedInUSerId;
                var Updatedata = await Task.Run(() => updateWorkOrderMAsterStatusData.UpdateWorkOrderStatus(Data));
                return Updatedata;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //filder workOrder

        [Authorize]
        [HttpPost]
        [Route("FilterWorkOrderStatus")]
        public async Task<List<dynamic>> FilterWorkOrderStatus()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<StatusDetailsDTO>(s);

                Data.UserID = LoggedInUSerId;

                var filterdata = await Task.Run(() => workOrderMasterData.GetWorkOrderFilterData(Data));
                return filterdata;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //get Action WorkOrder Details Data
        //[Authorize]
        [HttpPost]
        [Route("GetWorkOrderActionDetail")]
        public async Task<List<dynamic>> GetWorkOrderActionDetail()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<WorkOrder_Column_DTO>(s);
                WorkOrderActionsDTO workOrderActionsDTO = new WorkOrderActionsDTO();
                WorkOrder_Column_DTO workOrder_Column_DTO = new WorkOrder_Column_DTO();
                workOrder_Column_DTO.Type = 2;
                Data.UserID = LoggedInUSerId;

                var filterdata = await Task.Run(() => workOrder_Column_Data.GetWorkOrderActionData(Data));
                return filterdata;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //post Action WorkOrder
        [Authorize]
        [HttpPost]
        [Route("PostWorkOrderActionDetail")]
        public async Task<List<dynamic>> PostWorkOrderActionDetail()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<ActionResultsWorkOderDTO>(s);
                ActionResultsWorkOderDTO actionResultsWorkOderDTO = new ActionResultsWorkOderDTO();

                Data.UserId = LoggedInUSerId;

                var filterdata = await Task.Run(() => workOrder_Column_Data.PostWorkOrderActionData(Data));
                return filterdata;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //message  details
        [Authorize]
        [HttpPost]
        [Route("PostUserMessageDetail")]
        public async Task<List<dynamic>> PostUserMessageDetail()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<WorkOrderMessageMasterDTO>(s);

                Data.UserID = LoggedInUSerId;

                var msg = await Task.Run(() => workOrderMessageMasterData.AddMessageDetailsData(Data));
                return msg;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("GetUserAddressDetail")]
        public async Task<List<dynamic>> GetUserAddressDetail()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<UserAddressDTO>(s);


                var useradd = await Task.Run(() => userAddressData.GetUserAddressDetails(Data));
                return useradd;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        // for delete client photo results
        [Authorize]
        [HttpPost]
        [Route("PostDeleteClientResultsPhotos")]
        public async Task<List<dynamic>> PostDeleteClientResultsPhotos()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<Client_Result_PhotoDTO>(s);

                Client_Result_PhotoDTO client_Result_PhotoDTO = new Client_Result_PhotoDTO();


                client_Result_PhotoDTO.Client_Result_Photo_ID = Data.Client_Result_Photo_ID;
                client_Result_PhotoDTO.UserID = LoggedInUSerId;
                client_Result_PhotoDTO.Type = 4;

                var DeletePhoto = await Task.Run(() => client_Result_PhotoData.AddClientResultPhotoData(client_Result_PhotoDTO));

                return DeletePhoto;



            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //task according photo delete
        [Authorize]
        [HttpPost]
        [Route("PostTaskDeleteClientResultsPhotos")]
        public async Task<List<dynamic>> PostTaskDeleteClientResultsPhotos()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<ClientResultPhotoRef>(s);
                Data.UserID = LoggedInUSerId;
                var taskDeletePhoto = await Task.Run(() => client_Result_PhotoData.DeleteClientResultPhotoData(Data));

                return taskDeletePhoto;



            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("TaskDocumentAssineInst")]
        public async Task<List<dynamic>> TaskDocumentAssineInst()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<Task_Master_Files_DTO>(s);

                var taskdoc = await Task.Run(() => task_Master_Files_Data.AddTaskMasterDOcument(Data));

                return taskdoc;



            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //multidelete workorder
        [Authorize]
        [HttpPost]
        [Route("MultiActionsWorkOrder")]
        public async Task<List<dynamic>> MultiActionsWorkOrder()
        {

            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<WorkoderActionItems>(s);
                Data.UserId = LoggedInUSerId;

                var deletewo = await Task.Run(() => workOrderMasterData.MultiActions_WorkOrder(Data));

                return deletewo;



            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        [Authorize]
        [HttpPost]
        [Route("ContractorPaymentData")]
        public async Task<List<dynamic>> ContractorPaymentData()
        {

            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<Contractor_Invoice_PaymentDTO>(s);
                Data.UserID = LoggedInUSerId;

                var AddConPay = await Task.Run(() => contractor_Invoice_PaymentData.AddContractorPaymentData(Data));

                return AddConPay;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("GetContractorPaymentData")]
        public async Task<List<dynamic>> GetContractorPaymentData()
        {

            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<Contractor_Invoice_PaymentDTO>(s);

                Data.UserID = LoggedInUSerId;

                var GetConPay = await Task.Run(() => contractor_Invoice_PaymentData.GetContraactorPaymentDetails(Data));

                return GetConPay;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("ClientPaymentData")]
        public async Task<List<dynamic>> ClientPaymentData()
        {

            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<Client_Invoice_PatymentDTO>(s);
                Data.UserID = LoggedInUSerId;

                var AddclientPay = await Task.Run(() => client_Invoice_PatymentData.AddClientPaymentData(Data));

                return AddclientPay;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("GetClientPaymentData")]
        public async Task<List<dynamic>> GetClientPaymentData()
        {

            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<Client_Invoice_PatymentDTO>(s);
                Data.UserID = LoggedInUSerId;

                var GetclientPay = await Task.Run(() => client_Invoice_PatymentData.GetClientPaymentDetails(Data));

                return GetclientPay;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //map data for office result page
        [Authorize]
        [HttpPost]
        [Route("GetMapOfficeData")]
        public async Task<List<dynamic>> GetMapOfficeData()
        {
            HeaderMapData headerMapData = new HeaderMapData();
            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<HeaderMapDTO>(s);

                var GetMap = await Task.Run(() => headerMapData.GetOfficeMapDetails(Data));

                return GetMap;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        ///update bid status data
        [Authorize]
        [HttpPost]
        [Route("UpdateBidStatusDetails")]
        public async Task<List<dynamic>> UpdateBidStatusDetails()
        {

            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<UpdateTaskBidstatusDTO>(s);
                Data.UserID = LoggedInUSerId;
                var GetMap = await Task.Run(() => updateTaskBidStatusData.UpdateTaskBidstatus(Data));

                return GetMap;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        // score card data
        [Authorize]
        [HttpPost]
        [Route("PostScoreCard_DataData")]
        public async Task<List<dynamic>> PostScoreCard_DataData()
        {

            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<ScoreCard_DataDTO>(s);
                Data.UserID = LoggedInUSerId;
                var PostScorecard = await Task.Run(() => scoreCard_Data.AddScoreCardDetails(Data));

                return PostScorecard;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //get score card
        [Authorize]
        [HttpPost]
        [Route("GetScoreCard_DataData")]
        public async Task<List<dynamic>> GetScoreCard_DataData()
        {

            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<ScoreCard_DataDTO>(s);

                var GetScorecard = await Task.Run(() => scoreCard_Data.GetScoreCardDetails(Data));
                return GetScorecard;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //get score Name
        [Authorize]
        [HttpPost]
        [Route("GetScoreCardName")]
        public async Task<List<dynamic>> GetScoreCardName()
        {

            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<ScoreCard_Name_MasterDTO>(s);

                var GetScorecardName = await Task.Run(() => scoreCard_DataVal_ChildData.GetScoreCard_Name_MasterDetails(Data));
                return GetScorecardName;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //add excel import wo data

        [Authorize]
        [HttpPost]
        [Route("PostImportWorkOrderExcel")]
        public async Task<List<dynamic>> PostImportWorkOrderExcel()
        {

            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<ExcelDataDTO>(s);
                Data.UserID = LoggedInUSerId;
                var AddExcel = await Task.Run(() => import_WorkOrder_Excel_MasterData.AddWorkOrderExcelDetails(Data));
                return AddExcel;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [Authorize]
        [HttpPost]
        [Route("GetImportWorkOrderExcel")]
        public async Task<List<dynamic>> GetImportWorkOrderExcel()
        {

            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<Import_WorkOrder_Excel_MasterDTO>(s);
                Data.UserID = LoggedInUSerId;
                var GetExcel = await Task.Run(() => import_WorkOrder_Excel_MasterData.GetImportWororderExcelDetails(Data));
                return GetExcel;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //common details
        [Authorize]
        [HttpPost]
        [Route("GetWorkOrdercommonmethod")]
        public async Task<List<dynamic>> GetWorkOrdercommonmethod()
        {

            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<WorkOrderActionsDTO>(s);
                Data.UserID = LoggedInUSerId;
                var Getcommon = await Task.Run(() => workOrder_Column_Data.GetCommonMethodDetails(Data));
                return Getcommon;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //delete instruction
        [Authorize]
        [HttpPost]
        [Route("DeleteInstructionFile")]
        public async Task<List<dynamic>> DeleteInstructionFile()
        {

            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<Client_Result_PhotoDTO>(s);
                Data.UserID = LoggedInUSerId;
                var deletefile = await Task.Run(() => instructionDocumentData.DeleteInstructionFile(Data));
                return deletefile;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        // save filters

        [Authorize]
        [HttpPost]
        [Route("postSavefilter")]
        public async Task<List<dynamic>> postSavefilter()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<WorkOrderFilterDTO>(s);
                Data.UserID = LoggedInUSerId;
                if (Data.Type == 5)
                {
                    Data.WF_IsLoad = true;
                }
                var postColumn = await Task.Run(() => workOrder_Column_Data.SaveFiltersDataNew(Data));
                return postColumn;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [Authorize]
        [HttpPost]
        [Route("postSavePagesize")]
        public async Task<List<dynamic>> postSavePagesize()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<WorkOrder_Column_Json_DTO>(s);
                Data.UserID = LoggedInUSerId;

                var postpage = await Task.Run(() => workOrder_Column_Data.SaveFiltersData(Data));
                return postpage;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //ipl company details

        [Authorize]
        [HttpPost]
        [Route("postiplcompany")]
        public async Task<List<dynamic>> postiplcompany()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<IPL_Company_MasterDTO>(s);
                Data.UserID = LoggedInUSerId;

                var postcompany = await Task.Run(() => iPL_Company_MasterData.Add_IPL_Company_Data(Data));
                return postcompany;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        //get ipl company
        [Authorize]
        [HttpPost]
        [Route("getiplcompany")]
        public async Task<List<dynamic>> getiplcompany()

        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<IPL_Company_MasterDTO>(s);
                Data.UserID = LoggedInUSerId;

                var getcompany = await Task.Run(() => iPL_Company_MasterData.Get_IPL_Company_Master_Details(Data));
                return getcompany;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("UserCoveragedetail")]
        public async Task<List<dynamic>> UserCoveragedetail()

        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<ContractorCoverageAreaDTO>(s);
                Data.UserID = LoggedInUSerId;

                var addressarr = await Task.Run(() => userMasterData.AddContractorMapAddress(Data));
                return addressarr;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }



        [Authorize]
        [HttpPost]
        [Route("PastContractorMapAddress")]
        public async Task<List<dynamic>> PastContractorMapAddress()

        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<ContractorCoverageAreaDTO>(s);
                Data.UserID = LoggedInUSerId;

                var Addaddress = await Task.Run(() => contractorCoverageAreaData.AddContractorCoverageAreaData(Data));
                return Addaddress;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [Authorize]
        [HttpPost]
        [Route("GetContractorMapAddress")]
        public async Task<List<dynamic>> GetContractorMapAddress()

        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<ContractorCoverageAreaDTO>(s);
                Data.UserID = LoggedInUSerId;

                var Getaddress = await Task.Run(() => contractorCoverageAreaData.GetContractorCoverageAreaDetails(Data));
                return Getaddress;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("DeleteContractorMapAddress")]
        public async Task<List<dynamic>> DeleteContractorMapAddress()

        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<ContractorCoverageAreaDTO>(s);
                Data.UserID = LoggedInUSerId;

                var deladdress = await Task.Run(() => contractorCoverageAreaData.AddContractorCoverageAreaData(Data));
                return deladdress;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("PostContractorCateData")]
        public async Task<List<dynamic>> PostContractorCateData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            ContractorCategoryDTO contractorCategoryDTO = new ContractorCategoryDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<ContractorCategoryDTO>(s);
                contractorCategoryDTO.Con_Cat_PkeyID = Data.Con_Cat_PkeyID;
                contractorCategoryDTO.Con_Cat_Name = Data.Con_Cat_Name;

                if (Data.Con_Cat_Back_Color != null)
                {
                    string color = Data.Con_Cat_Back_Color;
                    var colorList = color.Split('#');
                    contractorCategoryDTO.Con_Cat_Back_Color = colorList[1];
                }

                contractorCategoryDTO.Con_Cat_Icon = Data.Con_Cat_Icon;
                contractorCategoryDTO.Con_Cat_IsActive = Data.Con_Cat_IsActive;
                contractorCategoryDTO.Con_Cat_IsDelete = Data.Con_Cat_IsDelete;
                contractorCategoryDTO.UserID = LoggedInUSerId;
                contractorCategoryDTO.Type = Data.Type;

                var AddConCate = await Task.Run(() => contractorCategoryData.AddContractorCategoryData(contractorCategoryDTO));
                return AddConCate;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("DeleteContractorCateData")]
        public async Task<List<dynamic>> DeleteContractorCateData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<ContractorCategoryDTO>(s);
                Data.UserID = LoggedInUSerId;


                var DelConCate = await Task.Run(() => contractorCategoryData.AddContractorCategoryData(Data));
                return DelConCate;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("FilterContractorCateData")]
        public async Task<List<dynamic>> FilterContractorCateData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<ContractorCategoryDTO>(s);
                Data.UserID = LoggedInUSerId;

                var filterConCate = await Task.Run(() => contractorCategoryData.GetContractorCategoryFilterDetails(Data));
                return filterConCate;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("GetContractorCateData")]
        public async Task<List<dynamic>> GetContractorCateData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<ContractorCategoryDTO>(s);
                Data.UserID = LoggedInUSerId;

                var GetConCate = await Task.Run(() => contractorCategoryData.GetContractorCategoryDetails(Data));
                return GetConCate;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("GetContractorMapData")]
        public async Task<List<dynamic>> GetContractorMapData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<ContractorMapDTO>(s);

                Data.UserID = LoggedInUSerId;
                Data.Type = 3;

                var GetConmap = await Task.Run(() => contractorMapData.GetContractorMapDetails(Data));
                return GetConmap;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("GetContractorMaplatlongData")]
        public async Task<List<dynamic>> GetContractorMaplatlongData()
        {
            ContractorMapLatLondData contractorMapLatLondData = new ContractorMapLatLondData();
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<ContractorMapLatLondDTO>(s);

                Data.UserID = LoggedInUSerId;
                Data.Type = 1;

                var GetConmaplat = await Task.Run(() => contractorMapLatLondData.GetContractorMaplatlongDetails(Data));
                return GetConmaplat;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("GetCountyZipChange")]
        public async Task<List<dynamic>> GetCountyZipChange()
        {
            CountyZipChangeData countyZipChangeData = new CountyZipChangeData();
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<CountyZipChangeDTO>(s);


                Data.Type = 1;

                var GetConmaplat = await Task.Run(() => countyZipChangeData.Get_CountyZip_Details(Data));
                return GetConmaplat;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("AddZipCode")]
        public async Task<List<dynamic>> AddZipCode()
        {
            ContractorCoverageAreaData contractorCoverageAreaData = new ContractorCoverageAreaData();
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<ZipMasterDTO>(s);

                Data.UserID = LoggedInUSerId;
                Data.Type = 1;

                var AddZip = await Task.Run(() => contractorCoverageAreaData.AddUserZipData(Data));
                return AddZip;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        // [Authorize]
        [HttpPost]
        [Route("GetWorkOrderPuran")]
        public async Task<List<dynamic>> GetWorkOrderPuran()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            LoginDTO loginDTO = new LoginDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<LoginDTO>(s);
                loginDTO.username = Data.username;
                loginDTO.token = Data.token;

                var GetorderPuran = await Task.Run(() => getWorkOrder_Puran_Master.GetWorkOdersPuranDetails(loginDTO));
                return GetorderPuran;


            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //Get office document ------ Added by dipali
        [Authorize]
        [HttpPost]
        [Route("GetOfficeDocument")]
        public async Task<List<dynamic>> GetOfficeDocument()
        {

            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<WorkOrder_Office_DocumentDTO>(s);
                Data.UserID = LoggedInUSerId;
                var officeDocumentList = await Task.Run(() => workOrder_Office_DocumentData.GetWorkOrderOfficeDocumentDetails(Data));
                return officeDocumentList;
            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

        //delete office document ------ Added by dipali
        [Authorize]
        [HttpPost]
        [Route("DeleteOfficeDocument")]
        public async Task<List<dynamic>> DeleteOfficeDocument()
        {

            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<WorkOrder_Office_DocumentDTO>(s);
                Data.UserID = LoggedInUSerId;
                var deletefile = await Task.Run(() => workOrder_Office_DocumentData.DeleteOfficeDocument(Data));
                return deletefile;
            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

        //final score card
        [Authorize]
        [HttpPost]
        [Route("FinalScoreCardDetail")]
        public async Task<List<dynamic>> FinalScoreCardDetail()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<FinalScrorCardDTO>(s);
                Data.UserID = LoggedInUSerId;

                var GetConmaplat = await Task.Run(() => contractor_ScoreCard_SettingData.GetFinalScoreCardFilterDetails(Data));
                return GetConmaplat;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [Authorize]
        [HttpPost]
        [Route("FinalScoreCardDropDown")]
        public async Task<List<dynamic>> FinalScoreCardDropDown()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var drd = await Task.Run(() => contractor_ScoreCard_SettingData.GetContractorScoreCardDropdown());
                return drd;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("CurrentScoreCardDetail")]
        public async Task<List<dynamic>> CurrentScoreCardDetail()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<FinalScrorCardDTO>(s);
                Data.UserID = LoggedInUSerId;


                var getcurrent = await Task.Run(() => contractor_ScoreCard_SettingData.GetCurrentScoreCardDetails(Data));
                return getcurrent;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [Authorize]
        [HttpPost]
        [Route("getScoreChildData")]
        public async Task<List<dynamic>> getScoreChildData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<ChildScorecardDTO>(s);

                Data.Type = 1;

                var Child = await Task.Run(() => contractor_ScoreCard_SettingData.GetChildscoreDetails(Data));
                return Child;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("GetWorkOrderPastData")]
        public async Task<List<dynamic>> GetWorkOrderPastData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<WorkOrder_HistoryData_DTO>(s);
                Data.UserID = LoggedInUSerId;
                Data.Type = 1;

                var getcurrent = await Task.Run(() => client_Result_PhotoData.Get_WorkOrderPastData(Data));
                return getcurrent;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        // get task configuration List
        [Authorize]
        [HttpPost]
        [Route("GetTaskConfigurationMasterDetails")]
        public async Task<List<dynamic>> GetTaskConfigurationMasterDetails()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();


            TaskMasterDTO taskMasterDTO = new TaskMasterDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Task_Configuration_MasterDTO>(s);
                Data.UserID = Convert.ToInt32(LoggedInUSerId);

                objdynamicobj = await Task.Run(() => task_ConfigurationData.GetTask_ConfigurationMasterDetails(Data));
                return objdynamicobj;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        // post task configuration List
        [Authorize]
        [HttpPost]
        [Route("PostTaskConfigurationMasterDetails")]
        public async Task<List<dynamic>> PostTaskConfigurationMasterDetails()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();


            TaskMasterDTO taskMasterDTO = new TaskMasterDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Task_Configuration_ListDTO>(s);
                Data.Task_Configuration_List.ForEach(d => d.UserID = Convert.ToInt32(LoggedInUSerId));

                objdynamicobj = await Task.Run(() => task_ConfigurationData.PostTaskConfigurationMasterDetails(Data));
                return objdynamicobj;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        // get workType configuration List
        [Authorize]
        [HttpPost]
        [Route("GetWorkTypeConfigurationDetails")]
        public async Task<List<dynamic>> GetWorkTypeConfigurationDetails()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();


            TaskMasterDTO taskMasterDTO = new TaskMasterDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<WorkType_Configuration_MasterDTO>(s);
                Data.UserID = Convert.ToInt32(LoggedInUSerId);

                objdynamicobj = await Task.Run(() => workType_ConfigurationData.GetWorkType_ConfigurationMasterDetails(Data));
                return objdynamicobj;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        // post workType configuration List
        [Authorize]
        [HttpPost]
        [Route("PostWorkTypeConfigurationDetails")]
        public async Task<List<dynamic>> PostWorkTypeConfigurationDetails()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<WorkType_Configuration_ListDTO>(s);
                Data.WorkType_Configuration_List.ForEach(d => d.UserID = Convert.ToInt32(LoggedInUSerId));

                objdynamicobj = await Task.Run(() => workType_ConfigurationData.PostWorkTypeConfigurationMasterDetails(Data));
                return objdynamicobj;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        // get mainCategory configuration List
        [Authorize]
        [HttpPost]
        [Route("GetMainCategoroyConfigurationDetails")]
        public async Task<List<dynamic>> GetMainCategoroyConfigurationDetails()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<MainCategoroy_Configuration_MasterDTO>(s);
                Data.UserID = Convert.ToInt32(LoggedInUSerId);

                objdynamicobj = await Task.Run(() => mainCategory_ConfigurationData.GetMainCategoroy_ConfigurationMasterDetails(Data));
                return objdynamicobj;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        // post mainCategory configuration List
        [Authorize]
        [HttpPost]
        [Route("PostMainCategoroyConfigurationDetails")]
        public async Task<List<dynamic>> PostMainCategoroyConfigurationDetails()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<MainCategoroy_Configuration_ListDTO>(s);
                Data.MainCategoroy_Configuration_List.ForEach(d => d.UserID = Convert.ToInt32(LoggedInUSerId));

                objdynamicobj = await Task.Run(() => mainCategory_ConfigurationData.PostMainCategoroyConfigurationMasterDetails(Data));
                return objdynamicobj;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]

        [Route("AddUpdateFilterAdminclient")]
        public async Task<List<dynamic>> AddUpdateFilterAdminclient([FromBody] Filter_Admin_Client_MasterDTO model)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            Filter_Admin_Client_MasterData filter_Admin_Client_MasterData = new Filter_Admin_Client_MasterData();
            try
            {
                model.UserId = LoggedInUSerId;
                var UpdateFile = await Task.Run(() => filter_Admin_Client_MasterData.AddUpdate_Filter_Admin_Client(model));
                return UpdateFile;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

        [Route("AddUpdateFilterAdminImport")]
        public async Task<List<dynamic>> AddUpdateFilterAdminImport([FromBody] Filter_Admin_Import_MasterDTO model)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                model.UserId = LoggedInUSerId;
                if (model.Import_Filter_ImpFromID == "Select")
                    model.Import_Filter_ImpFromID = string.Empty;

                var UpdateFile = await Task.Run(() => filter_Admin_ImportData.AddUpdate_Filter_Admin_Import(model));

                return UpdateFile;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

        [Authorize]
        [HttpPost]
        [Route("AddUpdateFilterAdminUser")]
        public async Task<List<dynamic>> AddUpdateFilterAdminUser([FromBody] Filter_Admin_User_MasterDTO model)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            Filter_Admin_User_MasterDatacs filter_Admin_User_MasterDatacs = new Filter_Admin_User_MasterDatacs();
            try
            {
                model.UserId = LoggedInUSerId;
                var UpdateFile = await Task.Run(() => filter_Admin_User_MasterDatacs.AddUpdate_Filter_Admin_User(model));
                return UpdateFile;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }


        [Authorize]
        [HttpPost]
        [Route("AddUpdateFilterAdminLoan")]
        public async Task<List<dynamic>> AddUpdateFilterAdminLoan([FromBody] Filter_Admin_Loan_MasterDTO model)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                model.UserId = LoggedInUSerId;

                var UpdateFile = await Task.Run(() => filter_Admin_LoanData.AddUpdate_Filter_Admin_Loan(model));

                return UpdateFile;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

        [Authorize]
        [HttpPost]
        [Route("AddUpdateFilterAdminDamage")]
        public async Task<List<dynamic>> AddUpdateFilterAdminDamage([FromBody] Filter_Admin_Damage_MasterDTO model)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                model.UserId = LoggedInUSerId;

                var UpdateFile = await Task.Run(() => filter_Admin_DamageData.AddUpdate_Filter_Admin_Damage(model));
                return UpdateFile;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }
        //group save filter
        [Authorize]
        [HttpPost]
        [Route("AddUpdateFilterAdminGroup")]
        public async Task<List<dynamic>> AddUpdateFilterAdminGroup([FromBody] Filter_Admin_Group_MasterDTO model)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            Filter_Admin_Group_MasterDatacs filter_Admin_Group_MasterDatacs = new Filter_Admin_Group_MasterDatacs();

            try
            {
                model.UserId = LoggedInUSerId;
                var Updategroup = await Task.Run(() => filter_Admin_Group_MasterDatacs.AddUpdate_Filter_Admin_Group(model));

                return Updategroup;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }
        [Authorize]
        [HttpPost]
        [Route("AddUpdateFilterAdminConCat")]
        public async Task<List<dynamic>> AddUpdateFilterAdminConCat([FromBody] Filter_Admin_Contractor_Cat_MasterDTO model)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            Filter_Admin_Contractor_Cat_MasterData filter_Admin_Contractor_Cat_MasterData = new Filter_Admin_Contractor_Cat_MasterData();
            try
            {
                model.UserId = LoggedInUSerId;

                var Updateconcat = await Task.Run(() => filter_Admin_Contractor_Cat_MasterData.AddUpdate_Filter_Admin_Concategory(model));

                return Updateconcat;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }
        [Authorize]
        [HttpPost]
        [Route("AddUpdateFilterAdminCutomer")]
        public async Task<List<dynamic>> AddUpdateFilterAdminCutomer([FromBody] Filter_Admin_Customer_MasterDTO model)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            Filter_Admin_Customer_MasterData filter_Admin_Customer_MasterData = new Filter_Admin_Customer_MasterData();
            try
            {
                model.UserId = LoggedInUSerId;

                var Updatecustomer = await Task.Run(() => filter_Admin_Customer_MasterData.AddUpdate_Filter_Admin_Customer(model));

                return Updatecustomer;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

        [Authorize]
        [HttpPost]
        [Route("AddUpdateFilterAdminState")]
        public async Task<List<dynamic>> AddUpdateFilterAdminState([FromBody] Filter_Admin_State_MasterDTO model)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            Filter_Admin_State_MasterData filter_Admin_State_MasterData = new Filter_Admin_State_MasterData();
            try
            {
                model.UserId = LoggedInUSerId;

                var Updatestate = await Task.Run(() => filter_Admin_State_MasterData.AddUpdate_Filter_Admin_State(model));

                return Updatestate;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

        [Authorize]
        [HttpPost]
        [Route("AddUpdateFilterAdminWorkType")]
        public async Task<List<dynamic>> AddUpdateFilterAdminWorkType([FromBody] Filter_Admin_WorkType_MasterDTO model)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            Filter_Admin_WorkType_MasterData filter_Admin_WorkType_MasterData = new Filter_Admin_WorkType_MasterData();
            try
            {
                model.UserId = LoggedInUSerId;

                var Updatework = await Task.Run(() => filter_Admin_WorkType_MasterData.AddUpdate_Filter_Admin_WorkType(model));

                return Updatework;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

        // get firebase location List
        [Authorize]
        [HttpPost]
        [Route("GetFirebaseLocation")]
        public async Task<List<dynamic>> GetFirebaseLocationDetails()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<FirebaseUserDTO>(s);
                Data.UserID = LoggedInUSerId;
                objdynamicobj = await Task.Run(() => liveUserLocationData.GetFirebaseLocation(Data));
                return objdynamicobj;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        // delete firebase location item
        [Authorize]
        [HttpPost]
        [Route("DeleteFirebaseLocation")]
        public async Task<List<dynamic>> DeleteFirebaseLocationDetails()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<LiveUserLocationDTO>(s);
                Data.UserID = LoggedInUSerId;
                objdynamicobj = await Task.Run(() => liveUserLocationData.DeleteFirebaseLocation(Data));
                return objdynamicobj;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        // delete firebase location List
        [Authorize]
        [HttpPost]
        [Route("DeleteFirebaseLocationList")]
        public async Task<List<dynamic>> DeleteFirebaseLocationList()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<LiveUserLocationListDTO>(s);

                objdynamicobj = await Task.Run(() => liveUserLocationData.DeleteFirebaseLocationList(Data.LiveUserLocationArray, LoggedInUSerId));
                return objdynamicobj;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [HttpPost]
        [Route("PostRegisterCompany")]
        public async Task<List<dynamic>> PostRegisterCompany()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<RegisterCompanyDTO>(s);


                var RegisterCompany = await Task.Run(() => ipl_Company_RegisterData.Register_IPL_Company_Data(Data));
                return RegisterCompany;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("GetMultiClientPaymentData")]
        public async Task<List<dynamic>> GetMultiClientPaymentData()
        {

            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<Client_Invoice_MultiPatymentDTO>(s);


                var GetclientPay = await Task.Run(() => client_Invoice_PatymentData.GetClientMultiPaymentDetails(Data));

                return GetclientPay;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("AddClientResultPropertyInfoData")]
        public async Task<List<dynamic>> AddClientResultPropertyInfoData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            ClientResult_PropertyInfo_MasterData clientResult_PropertyInfo_MasterData = new ClientResult_PropertyInfo_MasterData();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<ClientResult_PropertyInfo_MasterDTO>(s);
                Data.UserID = LoggedInUSerId;
                var piInfo = await Task.Run(() => clientResult_PropertyInfo_MasterData.AddClientResult_PropertyInfoData(Data));
                return piInfo;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [Authorize]
        [HttpPost]
        [Route("AddClientResultPropertySettingData")]
        public async Task<List<dynamic>> AddClientResultPropertySettingData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            ClientResult_PropertySettings_MasterData clientResult_PropertySetting_MasterData = new ClientResult_PropertySettings_MasterData();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<ClientResult_PropertySettings_MasterDTO>(s);
                Data.UserID = LoggedInUSerId;
                var piInfo = await Task.Run(() => clientResult_PropertySetting_MasterData.AddClientResult_PropertySettingsData(Data));
                return piInfo;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [Authorize]
        [HttpPost]
        [Route("AddClientResultPropertyServiceDatesData")]
        public async Task<List<dynamic>> AddClientResultPropertyServiceDatesData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            ClientResult_PropertyServiceDates_MasterData clientResult_PropertySetting_MasterData = new ClientResult_PropertyServiceDates_MasterData();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<ClientResult_PropertyServiceDates_MasterDTO>(s);
                Data.UserID = LoggedInUSerId;
                var piInfo = await Task.Run(() => clientResult_PropertySetting_MasterData.AddClientResultPropertyServiceDatesData(Data));
                return piInfo;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [Authorize]
        [HttpPost]
        [Route("AddClientResultPropertyTeamData")]
        public async Task<List<dynamic>> AddClientResultPropertyTeamData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            ClientResult_PropertyTeam_MasterData clientResult_PropertyTeam_MasterData = new ClientResult_PropertyTeam_MasterData();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<ClientResult_PropertyTeam_MasterDTO>(s);
                Data.UserID = LoggedInUSerId;
                var piInfo = await Task.Run(() => clientResult_PropertyTeam_MasterData.AddClientResult_PropertyTeamData(Data));
                return piInfo;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [Authorize]
        [HttpPost]
        [Route("AddClientResultPropertyLoanSettingsData")]
        public async Task<List<dynamic>> AddClientResultPropertyLoanSettingsData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            ClientResult_PropertyLoanSettings_MasterData clientResult_PropertyLoanSettings_MasterData = new ClientResult_PropertyLoanSettings_MasterData();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<ClientResult_PropertyLoanSettings_MasterDTO>(s);
                Data.UserID = LoggedInUSerId;
                var piInfo = await Task.Run(() => clientResult_PropertyLoanSettings_MasterData.AddClientResult_PropertyLoanSettingsData(Data));
                return piInfo;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [Authorize]
        [HttpPost]
        [Route("GetClientResultPropertyInfoData")]
        public async Task<List<dynamic>> GetClientResultPropertyInfoData()
        {

            List<dynamic> objdynamicobj = new List<dynamic>();
            ClientResult_PropertyInfo_MasterData clientResult_PropertyInfo_MasterData = new ClientResult_PropertyInfo_MasterData();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<ClientResult_PropertyInfo_MasterDTO>(s);
                Data.UserID = LoggedInUSerId;
                var GetclientPay = await Task.Run(() => clientResult_PropertyInfo_MasterData.GetClientResult_PropertyInfoData(Data));

                return GetclientPay;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [Authorize]
        [HttpPost]
        [Route("GetClientResultPropertySettingsData")]
        public async Task<List<dynamic>> GetClientResultPropertySettingsData()
        {

            List<dynamic> objdynamicobj = new List<dynamic>();
            ClientResult_PropertySettings_MasterData clientResult_PropertySettings_MasterData = new ClientResult_PropertySettings_MasterData();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<ClientResult_PropertySettings_MasterDTO>(s);
                Data.UserID = LoggedInUSerId;
                var GetclientPay = await Task.Run(() => clientResult_PropertySettings_MasterData.GetClientResultPropertySettingsData(Data));

                return GetclientPay;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [Authorize]
        [HttpPost]
        [Route("GetClientResultPropertyTeamData")]
        public async Task<List<dynamic>> GetClientResultPropertyTeamData()
        {

            List<dynamic> objdynamicobj = new List<dynamic>();
            ClientResult_PropertyTeam_MasterData clientResult_PropertyTeam_MasterData = new ClientResult_PropertyTeam_MasterData();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<ClientResult_PropertyTeam_MasterDTO>(s);
                Data.UserID = LoggedInUSerId;
                var GetclientPay = await Task.Run(() => clientResult_PropertyTeam_MasterData.GetClientResultPropertyTeamData(Data));

                return GetclientPay;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [Authorize]
        [HttpPost]
        [Route("GetClientResultPropertyLoanSettingsData")]
        public async Task<List<dynamic>> GetClientResultPropertyLoanSettingsData()
        {

            List<dynamic> objdynamicobj = new List<dynamic>();
            ClientResult_PropertyLoanSettings_MasterData clientResult_PropertyLoanSettings_MasterData = new ClientResult_PropertyLoanSettings_MasterData();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<ClientResult_PropertyLoanSettings_MasterDTO>(s);
                Data.UserID = LoggedInUSerId;
                var GetclientPay = await Task.Run(() => clientResult_PropertyLoanSettings_MasterData.GetClientResultPropertyLoanSettingsData(Data));

                return GetclientPay;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [Authorize]
        [HttpPost]
        [Route("GetClientResultPropertyServiceDatesData")]
        public async Task<List<dynamic>> GetClientResultPropertyServiceDatesData()
        {

            List<dynamic> objdynamicobj = new List<dynamic>();
            ClientResult_PropertyServiceDates_MasterData clientResult_PropertyServiceDates_MasterData = new ClientResult_PropertyServiceDates_MasterData();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<ClientResult_PropertyServiceDates_MasterDTO>(s);
                Data.UserID = LoggedInUSerId;
                var GetclientPay = await Task.Run(() => clientResult_PropertyServiceDates_MasterData.GetClientResultPropertyServiceDatesData(Data));

                return GetclientPay;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [Authorize]
        [HttpPost]
        [Route("GetLiveRouteLocation")]
        public async Task<List<dynamic>> GetLiveRouteLocation()
        {

            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<WorkoderActionItems>(s);
                Data.UserId = LoggedInUSerId;

                var location = await Task.Run(() => liveUserLocationData.GetFirebaseRouteLocation(Data));

                return location;



            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("GetClientResultPhotoHistoryData")]
        public async Task<List<dynamic>> GetClientResultPhotoHistoryData()
        {

            List<dynamic> objdynamicobj = new List<dynamic>();
            ClientResult_PropertyInfo_MasterData clientResult_PropertyInfo_MasterData = new ClientResult_PropertyInfo_MasterData();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<Client_Result_PhotoDTO>(s);
                Data.UserID = LoggedInUSerId;
                var GetclientPay = await Task.Run(() => client_Result_PhotoData.GetClientResult_PhotoHistoryData(Data));

                return GetclientPay;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("PostRecuredWorkOrderData")]
        public async Task<List<dynamic>> RecuredWorkOrderData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<List<ActionRecurringModel>>(s);
                Data.ForEach(d => d.UserID = LoggedInUSerId);

                var returnVal = await Task.Run(() => WorkOrderMasterDataOBJ.RecuredWorkOrderData(Data));
                return returnVal;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("GetZipAddress")]
        public async Task<List<dynamic>> GetZipAddressDetails()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<ZipMasterDTO>(s);
                Data.UserID = LoggedInUSerId;

                var Getaddress = await Task.Run(() => contractorCoverageAreaData.GetZipAddressDetails(Data));
                return Getaddress;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("GetWorkorderNotificationDetail")]
        public async Task<List<dynamic>> GetWorkorderNotificationDetail()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Workorder_Notification_MasterDTO>(s);
                Workorder_Notification_MasterData workorder_Notification_MasterData = new Workorder_Notification_MasterData();
                Data.UserID = LoggedInUSerId;
                Data.WN_UserId = LoggedInUSerId;

                var dataList = await Task.Run(() => workorder_Notification_MasterData.GetWorkorderNotificationDetails(Data));
                return dataList;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("AddWorkorderNotification")]
        public async Task<List<dynamic>> AddWorkorderNotification()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Workorder_Notification_MasterDTO>(s);
                Workorder_Notification_MasterData workorder_Notification_MasterData = new Workorder_Notification_MasterData();
                Data.UserID = LoggedInUSerId;





                var dataList = await Task.Run(() => workorder_Notification_MasterData.AddWorkorderNotificationMaster(Data));
                return dataList;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        [Authorize]
        [HttpPost]
        [Route("GetPhotosDownloadNotification")]
        public async Task<List<dynamic>> GetPhotosDownloadNotification()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<ClientResult_Photos_Download_Notification_MasterDTO>(s);
                ClientResult_Photos_Download_Notification_MasterData Photos_Download_Notification_MasterData = new ClientResult_Photos_Download_Notification_MasterData();
                Data.PN_UserId = LoggedInUSerId;
                Data.Type = 1;

                var dataList = await Task.Run(() => Photos_Download_Notification_MasterData.GetDownloadPhotosNotificationDetails(Data));
                return dataList;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("AddPhotosDownloadNotification")]
        public async Task<List<dynamic>> AddPhotosDownloadNotification()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Workorder_Notification_MasterDTO>(s);
                Workorder_Notification_MasterData workorder_Notification_MasterData = new Workorder_Notification_MasterData();
                Data.UserID = LoggedInUSerId;





                var dataList = await Task.Run(() => workorder_Notification_MasterData.AddWorkorderNotificationMaster(Data));
                return dataList;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("GetClientForImport")]
        public async Task<List<dynamic>> GetClientForImport()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<ImportClient>(s);
                DropDownData dropDownData = new DropDownData();
                Data.UserID = LoggedInUSerId;
                var dataList = await Task.Run(() => dropDownData.GetImportClientDrdDetails(Data));
                return dataList;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("CreaterBackgroundCheckinProvider")]
        public async Task<List<dynamic>> CreaterBackgroundCheckinProvider()
        {

            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<BackgroundCheckinProvider_DTO>(s);
                Data.UserID = LoggedInUSerId;

                var BackgroundCheckinProviderDetails = await Task.Run(() => backgroundCheckinProviderData.Update_BackgroundCheckinProvider(Data));

                return BackgroundCheckinProviderDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage("CreaterBackgroundCheckinProvider");
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }


        [Authorize]
        [HttpPost]
        [Route("GetBackgroundCheckinProvider")]
        public async Task<List<dynamic>> GetBackgroundCheckinProvider()
        {

            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<BackgroundCheckinProvider_DTO>(s);
                Data.UserID = LoggedInUSerId;

                var BackgroundCheckinProviderDetails = await Task.Run(() => backgroundCheckinProviderData.Get_BackgroundCheckinProviderDetails(Data));

                return BackgroundCheckinProviderDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage("GetBackgroundCheckinProvider");
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }
        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("GetUser_AccessBy_CompanyID")]
        public async Task<List<dynamic>> GetUser_AccessBy_CompanyID()
        {

            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<User_AccessBy_CompanyID_DTO>(s);
                Data.UserID = LoggedInUSerId;

                var Get_User_AccessBy_CompanyIDDetails = await Task.Run(() => user_AccessBy_CompanyID_Data.GetUser_AccessBy_CompanyIDDetails(Data));

                return Get_User_AccessBy_CompanyIDDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage("GetUser_AccessBy_CompanyID");
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

        [Authorize]
        [HttpPost]
        [Route("GetUserDataForCompany")]
        public async Task<List<dynamic>> GetUserDataForCompany()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<UserFiltermasterDTO>(s);
                UserFiltermasterDTO userFiltermasterDTO = new UserFiltermasterDTO();
                Data.Type = 8;
                Data.UserID = LoggedInUSerId;
                objdynamicobj = await Task.Run(() => userMasterData.GetUserfilterDetails(Data));





            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

            return objdynamicobj;

        }


        [Authorize]
        [HttpPost]
        [Route("WorkOrderPrintData")]
        public async Task<List<dynamic>> WorkOrderPrintData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            PrintPDFData printPDFData = new PrintPDFData();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<PrintPDFDTO>(s);
                Data.UserID = LoggedInUSerId;

                var GetPrintDetails = await Task.Run(() => printPDFData.WorkOrderPrintDataDetails(Data));
                return GetPrintDetails;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("GetRestoredWorkOrderMasterData")]
        public async Task<List<dynamic>> GetRestoredWorkOrder_Data()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<WorkOrderHistoryDTO>(s);
                WorkOrderHistoryDTO workOrderHistoryDTO = new WorkOrderHistoryDTO();
                PastWorkOrderHistoryData pastWorkOrderHistoryData = new PastWorkOrderHistoryData();
                workOrderHistoryDTO.workOrder_ID = Data.workOrder_ID;
                workOrderHistoryDTO.UserID = LoggedInUSerId;
                workOrderHistoryDTO.Type = Data.Type;


                var GetpastWoHistory = await Task.Run(() => pastWorkOrderHistoryData.Get_Restored_WorkOrderDetails(workOrderHistoryDTO));
                return GetpastWoHistory;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        #region ECD Notes functionality API

        [Authorize]
        [HttpPost]
        [Route("PostECDNoteData")]
        public async Task<List<dynamic>> PostECDNoteData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<ECD_Notes>(s);
                Data.Type = Data.Type == 0 ? 1 : Data.Type;
                Data.UserID = LoggedInUSerId;

                var getECDData = await Task.Run(() => ecdNoteData.AddUpdateDeleted_ECDNotes(Data));
                return getECDData;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [Authorize]
        [HttpPost]
        [Route("GetECDNotesList")]
        public async Task<List<dynamic>> GetECDNotesList()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<ECD_Notes_Filter>(s);
                var Getworkorder = await Task.Run(() => ecdNoteData.GetECDNotesList(Data));

                return Getworkorder;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        #endregion



        // UpdateTaskStatusDamageViolationHazard
        [Authorize]
        [HttpPost]
        [Route("UpdateTaskStatusDamageViolationHazard")]
        public async Task<List<dynamic>> UpdateTaskStatusDamageViolationHazard()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<Task_Status_Damage_Violation_Hazard>(s);
                Data.UserID = Convert.ToInt32(LoggedInUSerId);
                var GetCustNum = await Task.Run(() => Task_MasterDataObj.UpdateTaskStatusDamageViolationHazard(Data));
                return GetCustNum;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        [Authorize]
        [HttpPost]
        [Route("CreateUpdatePropertyTypeMaster")]
        public async Task<List<dynamic>> CreateUpdatePropertyTypeMaster()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<Property_Type_MasterDTO>(s);
                Data.UserID = LoggedInUSerId;
                var PropertyTypeMaster = await Task.Run(() => property_Type_MasterData.CreateUpdatePropertyTypeMasterDetails(Data));
                return PropertyTypeMaster;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("GetPropertyTypeMaster")]
        public async Task<List<dynamic>> GetPropertyTypeMaster()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<Property_Type_MasterDTO>(s);
                Data.UserID = LoggedInUSerId;
                var PropertyTypeMaster = await Task.Run(() => property_Type_MasterData.GetPropertyTypeMasterDetails(Data));
                return PropertyTypeMaster;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }



        [Authorize]
        [HttpPost]
        [Route("CreateUpdatePropertyAlertMaster")]
        public async Task<List<dynamic>> CreateUpdatePropertyAlertMaster()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<Property_Alert_MasterDTO>(s);
                Data.UserID = LoggedInUSerId;
                var PropertyTypeMaster = await Task.Run(() => Property_Alert_MasterData.CreateUpdatePropertyAlertMasterDetails(Data));
                return PropertyTypeMaster;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("GetPropertyAlertMaster")]
        public async Task<List<dynamic>> GetPropertyAlertMaster()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<Property_Alert_MasterDTO>(s);
                Data.UserID = LoggedInUSerId;
                var PropertyTypeMaster = await Task.Run(() => Property_Alert_MasterData.GetPropertyAlertMasterDetails(Data));
                return PropertyTypeMaster;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }



        [Authorize]
        [HttpPost]
        [Route("CreateUpdateOccupancyStatusMaster")]
        public async Task<List<dynamic>> CreateUpdateOccupancyStatusMaster()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<Occupancy_Status_MasterDTO>(s);
                Data.UserID = LoggedInUSerId;
                var PropertyTypeMaster = await Task.Run(() => Occupancy_Status_MasterData.CreateUpdateOccupancyStatusMasterDetails(Data));
                return PropertyTypeMaster;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("GetOccupancyStatusMaster")]
        public async Task<List<dynamic>> GetOccupancyStatusMaster()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<Occupancy_Status_MasterDTO>(s);
                Data.UserID = LoggedInUSerId;
                var PropertyTypeMaster = await Task.Run(() => Occupancy_Status_MasterData.GetOccupancyStatusMasterDetails(Data));
                return PropertyTypeMaster;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }




        [Authorize]
        [HttpPost]
        [Route("CreateUpdateLoanStatusMaster")]
        public async Task<List<dynamic>> CreateUpdateLoanStatusMaster()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<Loan_Status_MasterDTO>(s);
                Data.UserID = LoggedInUSerId;
                var PropertyTypeMaster = await Task.Run(() => Loan_Status_MasterData.CreateUpdateLoanStatusMasterDetails(Data));
                return PropertyTypeMaster;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("GetLoanStatusMaster")]
        public async Task<List<dynamic>> GetLoanStatusMaster()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<Loan_Status_MasterDTO>(s);
                Data.UserID = LoggedInUSerId;
                var PropertyTypeMaster = await Task.Run(() => Loan_Status_MasterData.GetLoanStatusMasterDetails(Data));
                return PropertyTypeMaster;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("AddUpdateFilterAdminCommon")]
        public async Task<List<dynamic>> AddUpdateFilterAdminCommon([FromBody] Filter_Admin_Common_Master_DTO model)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                model.UserId = LoggedInUSerId;
                var data = await Task.Run(() => filter_Admin_Common_Master_Data.AddUpdate_Filter_Admin_Common(model));
                return data;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }
        [Authorize]
        [HttpPost]
        [Route("GetProfessionalServiceMaster")]
        public async Task<List<dynamic>> GetProfessionalServiceMaster()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<ProfessionalServiceMasterDTO>(s);

                Data.UserID = LoggedInUSerId;

                var GetPcr = await Task.Run(() => professionalServiceMasterData.GetProfessionalServiceMasterDetails(Data));
                return GetPcr;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [Authorize]
        [HttpPost]
        [Route("PostProfessionalServiceMaster")]
        public async Task<List<dynamic>> PostProfessionalServiceMaster()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<ProfessionalServiceMasterDTO>(s);

                Data.UserID = LoggedInUSerId;

                var GetPcr = await Task.Run(() => professionalServiceMasterData.AddUpdate_ProfessionalServiceMaster(Data));
                return GetPcr;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("PostLotPricingFilterMaster")]
        public async Task<List<dynamic>> PostLotPricingFilter_Master()
        {

            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<LotPricingFilterMasterDTO>(s);
                Data.UserID = LoggedInUSerId;

                var PricingFilter = await Task.Run(() => _lotPricingFilter.AddLotPricingFilterMasterData(Data));

                return PricingFilter;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("GetLotPricingFilterMasterData")]
        public async Task<List<dynamic>> GetLotPricingFilter_MasterData()
        {

            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<LotPricingFilterMasterDTO>(s);
                Data.UserID = LoggedInUSerId;

                var _lotPricing = await Task.Run(() => _lotPricingFilter.GetLotPricingFilteryMasterData(Data));

                return _lotPricing;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("GetContactTypeMaster")]
        public async Task<List<dynamic>> GetContactTypeMaster()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<ContactTypeMasterDTO>(s);

                Data.UserId = LoggedInUSerId;

                var GetPcr = await Task.Run(() => contactTypeMasterData.GetContactTypeMasterDetails(Data));
                return GetPcr;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [Authorize]
        [HttpPost]
        [Route("PostContactTypeMaster")]
        public async Task<List<dynamic>> PostContactTypeMaster()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<ContactTypeMasterDTO>(s);

                Data.UserId = LoggedInUSerId;

                var GetPcr = await Task.Run(() => contactTypeMasterData.AddUpdate_ContactTypeMaster(Data));
                return GetPcr;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("PostPropertyLockReasonMaster")]
        public async Task<List<dynamic>> PostPropertyLockReasonMaster()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<PropertyLockReasonMasterDTO>(s);
                Data.UserID = LoggedInUSerId;
                var PropertyTypeMaster = await Task.Run(() => propertyLockReasonMasterData.CreateUpdatePropertyLockReason(Data));
                return PropertyTypeMaster;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("GetPropertyLockReasonMaster")]
        public async Task<List<dynamic>> GetPropertyLockReasonMaster()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<PropertyLockReasonMasterDTO>(s);
                Data.UserID = LoggedInUSerId;
                var PropertyTypeMaster = await Task.Run(() => propertyLockReasonMasterData.GetPropertyAlertMasterDetails(Data));
                return PropertyTypeMaster;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        [Authorize]
        [HttpPost]
        [Route("GetWorkoOrderTracker")]
        public async Task<List<dynamic>> GetWorkoOrderTracker()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<workOrderxDTO>(s);
                workOrder.Type = Data.Type;
                workOrder.UserID = LoggedInUSerId;
                var Getworkorder = await Task.Run(() => workOrderMasterData.GetWorkorderTrackerList(workOrder));
                return Getworkorder;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
    }
}


