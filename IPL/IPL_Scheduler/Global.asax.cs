﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using log4net;
using log4net.Config;
using Avigma.Repository.Lib;
using Avigma.Models;
using System.Threading.Tasks;
using System.Threading;

namespace IPL_Scheduler
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        protected void Application_Start()
        { 
            //log4net.Config.DOMConfigurator.Configure();
            log4net.Config.XmlConfigurator.Configure();
            log.Info("******************************************************");
            log.Info("IPL Application User Initialized");

            #region  Comment
          
            #endregion

            log.Debug("Scheduler Called First");
            string ppwusername = "IPL_start";
            Avigma.Repository.Scheduler.JobScheduler.Start(ppwusername);


            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected void Application_End()
        {
            log.Debug("IPL Application User End called");
            var client = new System.Net.WebClient();
            var url = "https://testiplscheduler.ipreservationlive.com/";
            client.DownloadString(url);
            log.Debug("******************************************************");
            log.Debug("IPL Application User End called");

        }
    }
}
