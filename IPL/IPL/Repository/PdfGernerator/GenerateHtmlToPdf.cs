﻿using Avigma.Repository.Lib;
using IPL.Models;
using iTextSharp.text;
using iTextSharp.text.pdf;
using NReco.PdfGenerator;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace IPL.Repository.PdfGernerator
{
    public class GenerateHtmlToPdf
    {
        Log log = new Log();
        //SoldInventoryDAL inventoryDAL = new SoldInventoryDAL();
        //NumberToWords toWords = new NumberToWords();
       // public string CreateWorkOrderPdf(WorkOrdersMaster workOrdersMaster, HttpContext context, string pdpath)
        public async Task<CustomReportPDF> CreateWorkOrderPdf(WorkOrdersMaster workOrdersMaster, HttpContext context, string pdpath)
        {
            CustomReportPDF custom = new CustomReportPDF();
            string pdfPath = string.Empty;
            HttpContext.Current = new HttpContext(new HttpRequest(null, "https://www.google.com", null), new HttpResponse(null));
            try
            {
                var applicationhtml = GenerateHtmlToPdf.RenderViewToString("Home", "WorkOrderView", workOrdersMaster, context);

                //pdfPath = CreatePDF(applicationhtml, pdpath);
                byte[] pdffile = GenerateRuntimePDF(applicationhtml.ToString());
                custom.Data = pdffile;
                custom.HttpStatusCode = System.Net.HttpStatusCode.OK;
                return custom;
            }
            catch (Exception ex)
            {
                string Exceptionmsg = ex.Message;
                if (ex.InnerException != null)
                {
                    Exceptionmsg += " | " + ex.InnerException.InnerException;
                }
                Exceptionmsg += " | " + ex.StackTrace;
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                //throw new Exception();
                return custom.Data;
            }
            //string AppoimentLetter = "";
            //string filebasepath = ConfigurationManager.AppSettings["DocumentPath"];
            //OTPGenerator oTPGenerator = new OTPGenerator();
            //int num = oTPGenerator.GenerateRandomNo();
            //AppoimentLetter = (filebasepath + "GenerateOrder_" + num + ".pdf");

            //return pdfPath;


        }



        public static string RenderViewToString(string controllerName, string viewName, object viewData, HttpContext context)
        {
            var writer = new StringWriter();
            try
            {


                var context1 = HttpContext.Current;
                var contextBase = new HttpContextWrapper(context);
                var routeData = new RouteData();
                routeData.Values.Add("controller", controllerName);

                var controllerContext = new ControllerContext(contextBase,
                                                              routeData,
                                                              new EmptyController());

                var razorViewEngine = new RazorViewEngine();
                var razorViewResult = razorViewEngine.FindView(controllerContext,
                                                               viewName,
                                                               "",
                                                               false);


                var viewContext = new ViewContext(controllerContext,
                                                  razorViewResult.View,
                                                  new ViewDataDictionary(viewData),
                                                  new TempDataDictionary(),
                                                  writer);
                razorViewResult.View.Render(viewContext, writer);
            }
            catch (Exception ex)
            {
                //log.logErrorMessage(ex.Message);
                //log.logErrorMessage(ex.StackTrace);
                throw new Exception(ex.Message);

            }
            return writer.ToString();
        }
        class EmptyController : ControllerBase
        {
            protected override void ExecuteCore() { }
        }

        public string CreatePDF(string html, string pdpath)
        {
            string localfpath = string.Empty;
            string databasePath = string.Empty;
            try
            {


                
                string dbpath = System.Configuration.ConfigurationManager.AppSettings["DocumentDBPath"].ToString();


                string localPath = System.Configuration.ConfigurationManager.AppSettings["DocumentPath"].ToString();

                string pdfPath = string.Empty;
                var filename = Guid.NewGuid();
                string ffilename = string.Empty;

                ffilename = "GenerateOrder" + filename + ".pdf";

                pdfPath = pdpath + ffilename;

                using (MemoryStream myMemoryStream = new MemoryStream())
                {
                    Document pdfDoc = new Document();
                    PdfWriter myPDFWriter = PdfWriter.GetInstance(pdfDoc, myMemoryStream);

                    pdfDoc.Open();
                    html = html.Replace("\r\n", "");
                    HtmlToPdfConverter nRecohtmltoPdfObj = new HtmlToPdfConverter();

                    var content = nRecohtmltoPdfObj.GeneratePdf(html);
                    using (FileStream fs = File.Create(pdfPath))
                    {
                        fs.Write(content, 0, (int)content.Length);
                    }
                }
                databasePath = dbpath + ffilename;


                localfpath = localPath + ffilename;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);

            }
            return databasePath;//localfpath;//; //pdfPath;
        }
        public static byte[] GenerateRuntimePDF(string html)
        {
            #region NReco.PdfGenerator
            HtmlToPdfConverter nRecohtmltoPdfObj = new HtmlToPdfConverter();
            return nRecohtmltoPdfObj.GeneratePdf(html);
            #endregion

        }

    }
}