﻿using Avigma.Repository.Lib;
using IPLApp.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPLApp.Repository.data
{
    public class MainInvoiceItemMasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public List<dynamic> AddMainInvoiceItemData(MainInvoiceItemMasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objrkdData = new List<dynamic>();
            string insertProcedure = "[CreateUpdateMainInviceItem_Master]";
            MainInvoiceItem mainInvoiceItem = new MainInvoiceItem();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Item_pkeyID", 1 + "#bigint#" + model.Item_pkeyID);
                input_parameters.Add("@Item_Name", 1 + "#varchar#" + model.Item_Name);
                input_parameters.Add("@Item_Bid", 1 + "#int#" + model.Item_Bid);
                input_parameters.Add("@Item_AlwaysShowClientWo", 1 + "#bit#" + model.Item_AlwaysShowClientWo);
                input_parameters.Add("@Item_RequiredClientWO", 1 + "#bit    #" + model.Item_RequiredClientWO);
                input_parameters.Add("@Item_LotSize", 1 + "#bigint#" + model.Item_LotSize);
                input_parameters.Add("@Item_Through", 1 + "#varchar#" + model.Item_Through);
                input_parameters.Add("@Item_AutoInvoiceClient", 1 + "#int#" + model.Item_AutoInvoiceClient);
                input_parameters.Add("@Item_Active", 1 + "#int#" + model.Item_Active);

                input_parameters.Add("@Item_IsActive", 1 + "#bit#" + model.Item_IsActive);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Item_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    mainInvoiceItem.Item_pkeyID = "0";
                    mainInvoiceItem.Status = "0";
                    mainInvoiceItem.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    mainInvoiceItem.Item_pkeyID = Convert.ToString(objData[0]);
                    mainInvoiceItem.Status = Convert.ToString(objData[1]);


                }
                objrkdData.Add(mainInvoiceItem);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objrkdData;



        }

        private DataSet GetMainInvoiceItemMaster(MainInvoiceItemMasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_MainInvoiceItem_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Item_pkeyID", 1 + "#bigint#" + model.Item_pkeyID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetMainInvoiceItemDetails(MainInvoiceItemMasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetMainInvoiceItemMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<MainInvoiceItemMasterDTO> MainInvoice =
                   (from item in myEnumerableFeaprd
                    select new MainInvoiceItemMasterDTO
                    {
                        Item_pkeyID = item.Field<Int64>("Item_pkeyID"),
                        Item_Name = item.Field<String>("Item_Name"),
                        Item_Bid = item.Field<int?>("Item_Bid"),
                        Item_AlwaysShowClientWo = item.Field<Boolean?>("Item_AlwaysShowClientWo"),
                        Item_RequiredClientWO = item.Field<Boolean?>("Item_RequiredClientWO"),
                        Item_LotSize = item.Field<Int64?>("Item_LotSize"),
                        Item_Through = item.Field<String>("Item_Through"),
                        Item_AutoInvoiceClient = item.Field<int?>("Item_AutoInvoiceClient"),
                        Item_Active = item.Field<int?>("Item_Active"),
                        Item_IsActive = item.Field<Boolean?>("Item_IsActive"),

                    }).ToList();

                objDynamic.Add(MainInvoice);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}