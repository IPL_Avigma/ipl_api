﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class ClientResultFoHData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        public List<dynamic> Add_Client_Photo_FoH_Master(ClientResultFohDTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_FoH_Photo_Master]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Client_Result_Photo_ID", 1 + "#bigint#" + model.Client_Result_Photo_ID);
                input_parameters.Add("@Client_Result_Photo_Wo_ID", 1 + "#bigint#" + model.Client_Result_Photo_Wo_ID);
                input_parameters.Add("@CRP_New_pkeyId", 1 + "#bigint#" + model.CRP_New_pkeyId);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Photo_ID_FoH_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;

        }
    }
}