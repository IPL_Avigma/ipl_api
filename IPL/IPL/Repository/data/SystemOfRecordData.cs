﻿using Avigma.Repository.Lib;
using IPLApp.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPLApp.Repository.data
{
    public class SystemOfRecordData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public List<dynamic> AddSystemRecordData(SystemOfRecordDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objrkdData = new List<dynamic>();
            string insertProcedure = "[CreateUpdateSystemRecord]";
            SystemOfRecord systemOfRecord = new SystemOfRecord();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Sys_Of_Rcd_PkeyID", 1 + "#bigint#" + model.Sys_Of_Rcd_PkeyID);
                input_parameters.Add("@Sys_Of_Rcd_Name", 1 + "#nvarchar#" + model.Sys_Of_Rcd_Name);
                input_parameters.Add("@Sys_Of_Rcd_IsActive", 1 + "#bit#" + model.Sys_Of_Rcd_IsActive);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Sys_Of_Rcd_PkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    systemOfRecord.Sys_Of_Rcd_PkeyID = "0";
                    systemOfRecord.Status = "0";
                    systemOfRecord.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    systemOfRecord.Sys_Of_Rcd_PkeyID = Convert.ToString(objData[0]);
                    systemOfRecord.Status = Convert.ToString(objData[1]);


                }
                objrkdData.Add(systemOfRecord);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objrkdData;



        }

        private DataSet GetSystemOfRecordMaster(SystemOfRecordDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_SystemOFRecords]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Sys_Of_Rcd_PkeyID", 1 + "#bigint#" + model.Sys_Of_Rcd_PkeyID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetSystemRecordMasterDetails(SystemOfRecordDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetSystemOfRecordMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<SystemOfRecordDTO> SystemRecord =
                   (from item in myEnumerableFeaprd
                    select new SystemOfRecordDTO
                    {
                        Sys_Of_Rcd_PkeyID = item.Field<Int64>("Sys_Of_Rcd_PkeyID"),
                        Sys_Of_Rcd_Name = item.Field<String>("Sys_Of_Rcd_Name"),
                        Sys_Of_Rcd_IsActive = item.Field<Boolean?>("Sys_Of_Rcd_IsActive"),

                    }).ToList();

                objDynamic.Add(SystemRecord);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}