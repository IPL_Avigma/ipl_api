﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class ScoreCard_DataVal_ChildData
    {

        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        //check
        public List<dynamic> AddScoreCard_DataVal_Child(ScoreCard_DataVal_ChildDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_ScoreCard_DataVal_Child]";
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Scdc_pkeyId", 1 + "#bigint#" + model.Scdc_pkeyId);
                input_parameters.Add("@Scdc_Scd_pkeyId", 1 + "#bigint#" + model.Scdc_Scd_pkeyId);
                input_parameters.Add("@Scdc_Sna_pkeyId", 1 + "#bigint#" + model.Scdc_Sna_pkeyId);
                input_parameters.Add("@Scdc_Status_Id", 1 + "#int#" + model.Scdc_Status_Id);
                input_parameters.Add("@Scdc_IsActive", 1 + "#bit#" + model.Scdc_IsActive);
                input_parameters.Add("@Scdc_IsDelete", 1 + "#bit#" + model.Scdc_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Scdc_pkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters); ;



            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }
            return objData;



        }

        private DataSet GetScoreCard_DataVal_Child(ScoreCard_DataVal_ChildDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_ScoreCard_DataVal_Child]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Scdc_pkeyId", 1 + "#bigint#" + model.Scdc_pkeyId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }



            return ds;
        }

        public List<dynamic> GetScoreCard_DataVal_ChildDetails(ScoreCard_DataVal_ChildDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetScoreCard_DataVal_Child(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<ScoreCard_DataVal_ChildDTO> ScoreCardChild =
                   (from item in myEnumerableFeaprd
                    select new ScoreCard_DataVal_ChildDTO
                    {
                        Scdc_pkeyId = item.Field<Int64>("Scdc_pkeyId"),
                        Scdc_Scd_pkeyId = item.Field<Int64?>("Scdc_Scd_pkeyId"),
                        Scdc_Sna_pkeyId = item.Field<Int64?>("Scdc_Sna_pkeyId"),
                        Scdc_Status_Id = item.Field<int?>("Scdc_Status_Id"),
                        Scdc_IsActive = item.Field<Boolean?>("Scdc_IsActive"),


                    }).ToList();

                objDynamic.Add(ScoreCardChild);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }

            return objDynamic;
        }

        private DataSet GetScoreCard_Name_Master(ScoreCard_Name_MasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_ScoreCard_Name_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Sna_pkeyId", 1 + "#bigint#" + model.Sna_pkeyId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }

            return ds;
        }

        public List<dynamic> GetScoreCard_Name_MasterDetails(ScoreCard_Name_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetScoreCard_Name_Master(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<ScoreCard_Name_MasterDTO> ScoreCardname =
                   (from item in myEnumerableFeaprd
                    select new ScoreCard_Name_MasterDTO
                    {
                        Sna_pkeyId = item.Field<Int64>("Sna_pkeyId"),
                        Sna_Name = item.Field<String>("Sna_Name"),
                        Sna_IsActive = item.Field<Boolean?>("Sna_IsActive"),


                    }).ToList();

                objDynamic.Add(ScoreCardname);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }

            return objDynamic;
        }


    }
}