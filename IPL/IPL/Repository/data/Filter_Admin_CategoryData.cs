﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class Filter_Admin_CategoryData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        public List<dynamic> AddUpdate_Filter_Admin_Category(Filter_Admin_Category_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_Filter_Admin_Category_Master]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Category_Filter_PkeyID", 1 + "#bigint#" + model.Category_Filter_PkeyID);
                input_parameters.Add("@Category_Filter_CategoryName", 1 + "#nvarchar#" + model.Category_Filter_CategoryName);
                input_parameters.Add("@Category_Filter_CategoryIsActive", 1 + "#bit#" + model.Category_Filter_CategoryIsActive);
                input_parameters.Add("@Category_Filter_IsActive", 1 + "#bit#" + model.Category_Filter_IsActive);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserId);
                input_parameters.Add("@Category_Filter_PkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        private DataSet Get_Filter_Admin_Category(Filter_Admin_Category_MasterDTO model)
        {
            DataSet ds = null;
            try
            {

                string selectProcedure = "[Get_Filter_Admin_Category_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Category_Filter_PkeyID", 1 + "#bigint#" + model.Category_Filter_PkeyID);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> Get_Filter_Admin_CategoryDetails(Filter_Admin_Category_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = Get_Filter_Admin_Category(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Filter_Admin_Category_MasterDTO> Forms_Master_Files =
                   (from item in myEnumerableFeaprd
                    select new Filter_Admin_Category_MasterDTO
                    {
                        Category_Filter_PkeyID = item.Field<Int64>("Category_Filter_PkeyID"),
                        Category_Filter_CategoryName = item.Field<String>("Category_Filter_CategoryName"),
                        Category_Filter_CategoryIsActive = item.Field<Boolean?>("Category_Filter_CategoryIsActive")

                    }).ToList();

                objDynamic.Add(Forms_Master_Files);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}