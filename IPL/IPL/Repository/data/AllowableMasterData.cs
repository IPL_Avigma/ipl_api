﻿using Avigma.Repository.Lib;
using IPL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class AllowableMasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        private List<dynamic> AddupdateAllowableMaster_Data(AllowableMasterDTO model)

        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_AllowableMaster]";
            AllowableDTO allowableDTO = new AllowableDTO();

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Allowable_PKeyId", 1 + "#bigint#" + model.Allowable_PKeyId);
                input_parameters.Add("@Allowable_Name", 1 + "#varchar#" + model.Allowable_Name);
                input_parameters.Add("@Allowable_StartDate", 1 + "#datetime#" + model.Allowable_StartDate);
                input_parameters.Add("@Allowable_EndDate", 1 + "#datetime#" + model.Allowable_EndDate);
                input_parameters.Add("@Allowable_OverallAllowables", 1 + "#bigint#" + model.Allowable_OverallAllowables);
                input_parameters.Add("@Allowable_CompanyId", 1 + "#bigint#" + model.Allowable_CompanyId);
                input_parameters.Add("@Allowable_Cust_ID", 1 + "#bigint#" + model.Allowable_Cust_ID);
                input_parameters.Add("@Allowable_IsActive ", 1 + "#bit#" + model.Allowable_IsActive);
                input_parameters.Add("@Allowable_IsDelete", 1 + "#bit#" + model.Allowable_IsDelete);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Allowable_PKeyId_Out ", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    allowableDTO.Allowable_PKeyId = "0";
                    allowableDTO.Status = "0";
                    allowableDTO.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    allowableDTO.Allowable_PKeyId = Convert.ToString(objData[0]);
                    allowableDTO.Status = Convert.ToString(objData[1]);


                }
                objcltData.Add(allowableDTO);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;



        }
        public List<dynamic> AddAllowableMaster_Data(AllowableMasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            try
            {
                objData = AddupdateAllowableMaster_Data(model);
                if (model.AllowablesArray != null && model.AllowablesArray.Count > 0)
                {
                    for (int i = 0; i < model.AllowablesArray.Count; i++)
                    {
                        Allowables_Child_Master_Data allowables_Child_Master_Data = new Allowables_Child_Master_Data();
                        Allowables_Child_Master_DTO allowables_Child_Master_DTO = new Allowables_Child_Master_DTO();
                        if (model.Type == 1)
                        {
                            if (objData.Count > 0 )
                            {
                                allowables_Child_Master_DTO.Allow_Child_Allowables_PkeyId = Convert.ToInt64(objData[0].Allowable_PKeyId);
                            }
                        }
                        else
                        {
                            allowables_Child_Master_DTO.Allow_Child_Allowables_PkeyId = model.Allowable_PKeyId;
                        }
                        allowables_Child_Master_DTO.Allow_Child_PkeyId = model.AllowablesArray[i].Allow_Child_PkeyId;
                        allowables_Child_Master_DTO.Allow_Child_Allowables_Cat_PkeyId = model.AllowablesArray[i].Allow_Child_Allowables_Cat_PkeyId;
                        allowables_Child_Master_DTO.Allow_Child_StartDate = model.AllowablesArray[i].Allow_Child_StartDate;
                        allowables_Child_Master_DTO.Allow_Child_EndDate = model.AllowablesArray[i].Allow_Child_EndDate;
                        allowables_Child_Master_DTO.Allow_Child_OverallAllowables = Convert.ToDecimal(model.AllowablesArray[i].Allow_Child_OverallAllowables);
                        allowables_Child_Master_DTO.Allow_Child_CompanyId = 0;
                        allowables_Child_Master_DTO.Allow_Child_UserId = model.UserID;
                        allowables_Child_Master_DTO.Allow_Child_IsActive = true;
                        allowables_Child_Master_DTO.Allow_Child_IsDelete = false;
                        allowables_Child_Master_DTO.UserID = model.UserID;
                        if (model.AllowablesArray[i].Allow_Child_PkeyId !=0)
                        {
                            allowables_Child_Master_DTO.Type = 2;
                        }
                        else
                        {
                            allowables_Child_Master_DTO.Type = 1;
                        }
                    

                        var objauto = allowables_Child_Master_Data.AddupdateAllowables_Child_Master_Data(allowables_Child_Master_DTO);
                        objData.Add(objauto);
                    }
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }



        private DataSet Get_AllowableMaster(AllowableMasterDTO model)
        {
            DataSet ds = null;
            try
            {

                string selectProcedure = "[Get_AllowableMaster]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Allowable_PKeyId", 1 + "#bigint#" + model.Allowable_PKeyId);
                input_parameters.Add("@WhereClause", 1 + "#varchar#" + model.WhereClause);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> Get_AllowableMasterDetails(AllowableMasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = Get_AllowableMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<AllowableMasterDTO> AllowableMaster =
                   (from item in myEnumerableFeaprd
                    select new AllowableMasterDTO
                    {
                        Allowable_PKeyId = item.Field<Int64>("Allowable_PKeyId"),
                        Allowable_Name = item.Field<String>("Allowable_Name"),
                        Allowable_StartDate = item.Field<DateTime?>("Allowable_StartDate"),
                        Allowable_EndDate = item.Field<DateTime?>("Allowable_EndDate"),
                        Allowable_OverallAllowables = item.Field< Int64?> ("Allowable_OverallAllowables"),
                        Allowable_CompanyId = item.Field<Int64?>("Allowable_CompanyId"),
                        Allowable_IsActive = item.Field<Boolean?>("Allowable_IsActive"),
                        User_FirstName = item.Field<String>("User_FirstName"),
                        Allowable_Cust_ID = item.Field<Int64?>("Allowable_Cust_ID"),
                        Allowable_CreatedBy = item.Field<String>("Allowable_CreatedBy"),
                        Allowable_ModifiedBy = item.Field<String>("Allowable_ModifiedBy"),
                        ViewUrl = null

                    }).ToList();

                objDynamic.Add(AllowableMaster);
                if (model.Type == 1)
                {
                    if (ds.Tables.Count > 1)
                    {
                        var myEnumerableFeaprd1 = ds.Tables[1].AsEnumerable();
                        List<Filter_Admin_Allowables_MasterDTO> Filterallowables =
                           (from item in myEnumerableFeaprd1
                            select new Filter_Admin_Allowables_MasterDTO
                            {
                                Filter_Admin_Allowable_PkeyId = item.Field<Int64>("Filter_Admin_Allowable_PkeyId"),
                                Filter_Admin_Allowable_Name = item.Field<String>("Filter_Admin_Allowable_Name"),
                                Filter_Admin_Allowable_StartDate = item.Field<DateTime?>("Filter_Admin_Allowable_StartDate"),
                                Filter_Admin_Allowable_EndDate = item.Field<DateTime?>("Filter_Admin_Allowable_EndDate"),
                                Filter_Admin_Allowable_OverallAllowables = item.Field<Int64?>("Filter_Admin_Allowable_OverallAllowables"),
                                Filter_Admin_Allowable_Isallowable = item.Field<Boolean?>("Filter_Admin_Allowable_Isallowable"),
                                 Filter_Admin_Allowable_CreatedBy = item.Field<String>("Filter_Admin_Allowable_CreatedBy"),
                                Filter_Admin_Allowable_ModifiedBy = item.Field<String>("Filter_Admin_Allowable_ModifiedBy"),
                            }).ToList();

                        objDynamic.Add(Filterallowables);
                    }
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        public List<dynamic> GetFilterAllowablesDetails(AllowableMasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();


            string wherecondition = string.Empty;
            try
            {

                var Data = JsonConvert.DeserializeObject<AllowableMasterDTO>(model.FilterData);
                if (!string.IsNullOrEmpty(Data.Allowable_Name))
                {
                    wherecondition = " And am.Allowable_Name LIKE '%" + Data.Allowable_Name + "%'";
                }


                if (Data.Allowable_IsActive == true)
                {
                    wherecondition = wherecondition + "  And am.Allowable_IsActive =  '1'";
                }
                if (Data.Allowable_IsActive == false)
                {
                    wherecondition = wherecondition + "  And am.Allowable_IsActive =  '0'";
                }


                AllowableMasterDTO allowableMasterDTO = new AllowableMasterDTO();

                allowableMasterDTO.WhereClause = wherecondition;
                allowableMasterDTO.Type = 3;
                allowableMasterDTO.UserID = model.UserID;
                DataSet ds = Get_AllowableMaster(allowableMasterDTO);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<AllowableMasterDTO> filterAllowableMaster =
                   (from item in myEnumerableFeaprd
                    select new AllowableMasterDTO
                    {
                        Allowable_PKeyId = item.Field<Int64>("Allowable_PKeyId"),
                        Allowable_Name = item.Field<String>("Allowable_Name"),
                        Allowable_StartDate = item.Field<DateTime?>("Allowable_StartDate"),
                        Allowable_EndDate = item.Field<DateTime?>("Allowable_EndDate"),
                        Allowable_OverallAllowables = item.Field<Int64?>("Allowable_OverallAllowables"),
                        Allowable_CompanyId = item.Field<Int64?>("Allowable_CompanyId"),
                        Allowable_IsActive = item.Field<Boolean?>("Allowable_IsActive"),
                        Allowable_CreatedBy = item.Field<String>("Allowable_CreatedBy"),
                        Allowable_ModifiedBy = item.Field<String>("Allowable_ModifiedBy"),

                    }).ToList();

                objDynamic.Add(filterAllowableMaster);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }

            return objDynamic;
        }

        public List<dynamic> AddUpdate_Filter_Admin_Allowables(Filter_Admin_Allowables_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_Filter_Admin_Allowables_Master]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Filter_Admin_Allowable_PkeyId", 1 + "#bigint#" + model.Filter_Admin_Allowable_PkeyId);
                input_parameters.Add("@Filter_Admin_Allowable_Name", 1 + "#nvarchar#" + model.Filter_Admin_Allowable_Name);
                input_parameters.Add("@Filter_Admin_Allowable_StartDate", 1 + "#datetime#" + model.Filter_Admin_Allowable_StartDate);
                input_parameters.Add("@Filter_Admin_Allowable_EndDate", 1 + "#datetime#" + model.Filter_Admin_Allowable_EndDate);   
                input_parameters.Add("@Filter_Admin_Allowable_OverallAllowables", 1 + "#bigint#" + model.Filter_Admin_Allowable_OverallAllowables);   
                input_parameters.Add("@Filter_Admin_Allowable_CompanyId", 1 + "#bigint#" + model.Filter_Admin_Allowable_CompanyId);   
                input_parameters.Add("@Filter_Admin_Allowable_UserId", 1 + "#bigint#" + model.Filter_Admin_Allowable_UserId);   
                input_parameters.Add("@Filter_Admin_Allowable_IsActive", 1 + "#bit#" + model.Filter_Admin_Allowable_IsActive);
                input_parameters.Add("@Filter_Admin_Allowable_IsDelete", 1 + "#bit#" + model.Filter_Admin_Allowable_IsDelete);
                input_parameters.Add("@Filter_Admin_Allowable_Isallowable", 1 + "#bit#" + model.Filter_Admin_Allowable_Isallowable);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserId);
                input_parameters.Add("@Filter_Admin_Allowable_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        public List<dynamic> Get_AllowablesSingleDetails(AllowableMasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {

                DataSet ds = Get_AllowableMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<AllowableMasterDTO> AllowableMaster =
                   (from item in myEnumerableFeaprd
                    select new AllowableMasterDTO
                    {
                        Allowable_PKeyId = item.Field<Int64>("Allowable_PKeyId"),
                        Allowable_Name = item.Field<String>("Allowable_Name"),
                        Allowable_StartDate = item.Field<DateTime?>("Allowable_StartDate"),
                        Allowable_EndDate = item.Field<DateTime?>("Allowable_EndDate"),
                        Allowable_IsActive = item.Field<Boolean?>("Allowable_IsActive"),
                        Allowable_Cust_ID = item.Field<Int64?>("Allowable_Cust_ID"),
                        Allowable_CreatedBy = item.Field<String>("Allowable_CreatedBy"),
                        Allowable_ModifiedBy = item.Field<String>("Allowable_ModifiedBy"),
                    }).ToList();

                objDynamic.Add(AllowableMaster);
                if (model.Type == 2)
                {
                    if (ds.Tables.Count > 1)
                    {
                        var myEnumerableFeaprd1 = ds.Tables[1].AsEnumerable();
                        List<Allowables_Child_Master_DTO> Allowables_Child_Master =
                           (from item in myEnumerableFeaprd1
                            select new Allowables_Child_Master_DTO
                            {
                                Allow_Child_PkeyId = item.Field<Int64>("Allow_Child_PkeyId"),
                                Allow_Child_Allowables_PkeyId = item.Field<Int64?>("Allow_Child_Allowables_PkeyId"),
                                Allow_Child_Allowables_Cat_PkeyId = item.Field<Int64?>("Allow_Child_Allowables_Cat_PkeyId"),
                                Allow_Child_StartDate = item.Field<DateTime?>("Allow_Child_StartDate"),
                                Allow_Child_EndDate = item.Field<DateTime?>("Allow_Child_EndDate"),
                                Allow_Child_OverallAllowables = item.Field<Decimal?>("Allow_Child_OverallAllowables"),
                                Allow_Child_CompanyId = item.Field<Int64?>("Allow_Child_CompanyId"),
                                Allow_Child_UserId = item.Field<Int64?>("Allow_Child_UserId"),
                                Allow_Child_IsActive = item.Field<Boolean?>("Allow_Child_IsActive"),

                            }).ToList();

                        objDynamic.Add(Allowables_Child_Master);
                    }
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}