﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class Support_Ticket_Setting_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Support_Setting_ReplyData support_Setting_ReplyData = new Support_Setting_ReplyData();
        Log log = new Log();

        private List<dynamic> AddUpdateSupportTicketData(Support_Ticket_Setting_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcustData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_Support_Ticket_Setting_Master]";
            Support_Ticket_Setting support_Ticket_Setting = new Support_Ticket_Setting();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Sup_Tickets_Pkey_ID", 1 + "#bigint#" + model.Sup_Tickets_Pkey_ID);
                input_parameters.Add("@Sup_Tickets_Phone", 1 + "#varchar#" + model.Sup_Tickets_Phone);
                input_parameters.Add("@Sup_Tickets_Email", 1 + "#varchar#" + model.Sup_Tickets_Email);
                input_parameters.Add("@Sup_Tickets_Subject", 1 + "#varchar#" + model.Sup_Tickets_Subject);
                input_parameters.Add("@Sup_Tickets_Message", 1 + "#varchar#" + model.Sup_Tickets_Message);
                input_parameters.Add("@Sup_Tickets_Ticket_Status", 1 + "#int#" + model.Sup_Tickets_Ticket_Status);
                input_parameters.Add("@Sup_Tickets_IsActive", 1 + "#bit#" + model.Sup_Tickets_IsActive);
                input_parameters.Add("@Sup_Tickets_IsDelete", 1 + "#bit#" + model.Sup_Tickets_IsDelete);
                input_parameters.Add("@Sup_Tickets_CompanyID", 1 + "#bigint#" + model.Sup_Tickets_CompanyID);
                input_parameters.Add("@Sup_Tickets_UserID", 1 + "#bigint#" + model.Sup_Tickets_UserID);
                input_parameters.Add("@Sup_Tickets_Relation_Id", 1 + "#bigint#" + model.Sup_Tickets_Relation_Id);
                input_parameters.Add("@Sup_Tickets_ID", 1 + "#bigint#" + model.Sup_Tickets_ID);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Sup_Tickets_Pkey_ID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    support_Ticket_Setting.Sup_Tickets_Pkey_ID = "0";
                    support_Ticket_Setting.Status = "0";
                    support_Ticket_Setting.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    support_Ticket_Setting.Sup_Tickets_Pkey_ID = Convert.ToString(objData[0]);
                    support_Ticket_Setting.Status = Convert.ToString(objData[1]);


                }
                objcustData.Add(support_Ticket_Setting);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcustData;



        }
        public List<dynamic> updateStatusDetails(Support_Ticket_Setting_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            objData = AddUpdateSupportTicketData(model);
            return objData;
        }
        public List<dynamic> AddupdateSupportTicketDetails(Support_Ticket_Setting_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                objData = AddUpdateSupportTicketData(model);
                if (objData[0].Status == "1")
                {
                    Support_Setting_ReplyDTO support_Setting_ReplyDTO = new Support_Setting_ReplyDTO();
                    support_Setting_ReplyDTO.Support_Rep_Support_Id = Convert.ToInt64(objData[0].Sup_Tickets_Pkey_ID);
                    support_Setting_ReplyDTO.Support_Rep_Reply = model.Sup_Tickets_Message;
                    support_Setting_ReplyDTO.Support_Rep_IsActie = true;
                    support_Setting_ReplyDTO.Support_Rep_IsDelete = false;
                    support_Setting_ReplyDTO.Support_Rep_IsComment = true;
                    support_Setting_ReplyDTO.Support_Rep_Status = 4; // Open Ticket
                    support_Setting_ReplyDTO.Type = 1;
                    var objMsgdata = support_Setting_ReplyData.AddupdateSupportTicketReply(support_Setting_ReplyDTO);
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        private DataSet Get_SupportTicket_Master(Support_Ticket_Setting_MasterDTO model)
        {
            DataSet ds = null;
            try

            {

                string selectProcedure = "[Get_Support_Ticket_Setting_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Sup_Tickets_Pkey_ID", 1 + "#bigint#" + model.Sup_Tickets_Pkey_ID);
                input_parameters.Add("@Sup_Tickets_CompanyID", 1 + "#bigint#" + model.Sup_Tickets_CompanyID);
                input_parameters.Add("@Sup_Tickets_UserID", 1 + "#bigint#" + model.Sup_Tickets_UserID);

                input_parameters.Add("@WhereClause", 1 + "#nvarchar#" + model.WhereClause);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@PageNumber", 1 + "#int#" + model.PageNumber);
                input_parameters.Add("@Rowcount", 1 + "#int#" + model.Rowcount);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }
        public List<dynamic> GetSupportTicketDetails(Support_Ticket_Setting_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = Get_SupportTicket_Master(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Support_Ticket_Setting_MasterDTO> SupportTicket_Master =
                   (from item in myEnumerableFeaprd
                    select new Support_Ticket_Setting_MasterDTO
                    {
                        Sup_Tickets_Pkey_ID = item.Field<Int64>("Sup_Tickets_Pkey_ID"),
                        Sup_Tickets_Phone = item.Field<String>("Sup_Tickets_Phone"),
                        Sup_Tickets_Email = item.Field<String>("Sup_Tickets_Email"),
                        Sup_Tickets_Subject = item.Field<String>("Sup_Tickets_Subject"),
                        Sup_Tickets_Message = item.Field<String>("Sup_Tickets_Message"),
                        Sup_Tickets_Ticket_Status = item.Field<int?>("Sup_Tickets_Ticket_Status"),
                        Sup_Tickets_IsActive = item.Field<Boolean?>("Sup_Tickets_IsActive"),
                        AddDate = item.Field<DateTime?>("AddDate"),
                        LastUpdate = item.Field<DateTime?>("LastUpdate"),
                        Sup_Tickets_Relation_Id = item.Field<Int64?>("Sup_Tickets_Relation_Id"),
                        Sup_Tickets_ID = item.Field<Int64?>("Sup_Tickets_ID"),
                        Sup_Tickets_CreatedBy = item.Field<String>("Sup_Tickets_CreatedBy"),
                        Sup_Tickets_ModifiedBy = item.Field<String>("Sup_Tickets_ModifiedBy"),
                    }).ToList();

                objDynamic.Add(SupportTicket_Master);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                log.logErrorMessage("GetSupportTicketDetails ----------------->" + model.Sup_Tickets_Pkey_ID);
            }

            return objDynamic;
        }

        public List<dynamic> GetSingleSupportTicketDetails(Support_Ticket_Setting_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = Get_SupportTicket_Master(model);
                if (ds.Tables.Count > 0)
                {
                    var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                    List<Support_Ticket_Setting_MasterDTO> SupportTicket_Master =
                       (from item in myEnumerableFeaprd
                        select new Support_Ticket_Setting_MasterDTO
                        {
                            Sup_Tickets_Pkey_ID = item.Field<Int64>("Sup_Tickets_Pkey_ID"),
                            Sup_Tickets_Phone = item.Field<String>("Sup_Tickets_Phone"),
                            Sup_Tickets_Email = item.Field<String>("Sup_Tickets_Email"),
                            Sup_Tickets_Subject = item.Field<String>("Sup_Tickets_Subject"),
                            Sup_Tickets_Message = item.Field<String>("Sup_Tickets_Message"),
                            Sup_Tickets_IsActive = item.Field<Boolean?>("Sup_Tickets_IsActive"),
                            AddDate = item.Field<DateTime?>("AddDate"),
                            LastUpdate = item.Field<DateTime?>("LastUpdate"),
                            Sup_Tickets_Relation_Id = item.Field<Int64?>("Sup_Tickets_Relation_Id"),
                            Sup_Tickets_ID = item.Field<Int64?>("Sup_Tickets_ID"),
                            User_FirstName = item.Field<String>("User_FirstName"),
                            User_LastName = item.Field<String>("User_LastName"),
                            Sup_Comment_Show = item.Field<Int32?>("Sup_Comment_Show") == 0 ? false : true,
                            Sup_Tickets_Ticket_Status = item.Field<int?>("Sup_Tickets_Ticket_Status"),
                            Sup_Tickets_CreatedBy = item.Field<String>("Sup_Tickets_CreatedBy"),
                            Sup_Tickets_ModifiedBy = item.Field<String>("Sup_Tickets_ModifiedBy"),
                        }).ToList();

                    objDynamic.Add(SupportTicket_Master);
                }
                if (ds.Tables.Count > 1)
                {
                    var myEnumerableFeaprd1 = ds.Tables[1].AsEnumerable();
                    List<Support_Setting_ReplyDTO> SupportTicket_reply =
                       (from item in myEnumerableFeaprd1
                        select new Support_Setting_ReplyDTO
                        {
                            Support_Rep_PkeyId = item.Field<Int64>("Support_Rep_PkeyId"),
                            Support_Rep_Support_Id = item.Field<Int64?>("Support_Rep_Support_Id"),
                            Support_Rep_Reply = item.Field<String>("Support_Rep_Reply"),
                            Support_Rep_Status = item.Field<int?>("Support_Rep_Status"),
                            Support_Rep_IsActie = item.Field<Boolean?>("Support_Rep_IsActie"),
                            Ipl_Ad_User_First_Name = item.Field<String>("Ipl_Ad_User_First_Name"),
                            Ipl_Ad_User_Last_Name = item.Field<String>("Ipl_Ad_User_Last_Name"),
                            Support_Rep_CreatedOn = item.Field<DateTime?>("Support_Rep_CreatedOn"),
                            Support_Rep_IsComment = item.Field<Boolean?>("Support_Rep_IsComment"),
                            Support_Rep_CommentId = item.Field<Int64?>("Support_Rep_CommentId"),

                        }).ToList();

                    objDynamic.Add(SupportTicket_reply);
                }
                if (ds.Tables.Count > 2)
                {
                    var myEnumerableFeaprd2 = ds.Tables[2].AsEnumerable();
                    List<Support_Ticket_Document_MasterDTO> SupportTicket_docs =
                       (from item in myEnumerableFeaprd2
                        select new Support_Ticket_Document_MasterDTO
                        {
                            Support_Docs_PkeyID = item.Field<Int64>("Support_Docs_PkeyID"),
                            Support_Docs_Ticket_ID = item.Field<Int64>("Support_Docs_Ticket_ID"),
                            Support_Docs_File_Path = item.Field<String>("Support_Docs_File_Path"),
                            Support_Docs_File_Size = item.Field<String>("Support_Docs_File_Size"),
                            Support_Docs_File_Name = item.Field<String>("Support_Docs_File_Name"),
                            Support_Docs_Project_Id = item.Field<String>("Support_Docs_Project_Id"),
                            Support_Docs_Object_Name = item.Field<String>("Support_Docs_Object_Name"),
                            Support_Docs_Folder_Name = item.Field<String>("Support_Docs_Folder_Name"),
                            Support_Docs_UploadedBy = item.Field<String>("Support_Docs_UploadedBy"),
                            Support_Docs_IsActive = item.Field<Boolean?>("Support_Docs_IsActive"),

                        }).ToList();

                    objDynamic.Add(SupportTicket_docs);
                }

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }



    }
}