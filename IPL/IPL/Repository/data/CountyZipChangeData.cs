﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class CountyZipChangeData
    {
        Log log = new Log();
        MyDataSourceFactory obj = new MyDataSourceFactory();
        private DataSet Get_CountyZip_Master(CountyZipChangeDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_ZipListCounty]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Zip_county_name", 1 + "#varchar#" + model.Zip_county_name);
                input_parameters.Add("@whereclause", 1 + "#varchar#" + model.whereclause);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@IPL_StateID", 1 + "#bigint#" + model.IPL_StateID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }
        //Get App Company Details 
        public List<dynamic> Get_CountyZip_Details(CountyZipChangeDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = Get_CountyZip_Master(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<CountyZipChangeDTO> countyzip =
                   (from item in myEnumerableFeaprd
                    select new CountyZipChangeDTO
                    {
                       // Zip_ID = item.Field<Int64>("Zip_ID"),
                        Zip_zip = item.Field<Int64?>("Zip_zip"),
                        Zip_state_id = item.Field<String>("Zip_state_id"),
                        Zip_county_name = item.Field<String>("Zip_county_name"),
                        Cont_Coverage_Area_IsActive = item.Field<Boolean?>("Cont_Coverage_Area_IsActive"),
                     

                    }).ToList();

                objDynamic.Add(countyzip);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}