﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using IPLApp.Models;
using Avigma.Repository.Scheduler;
using Newtonsoft.Json;

namespace IPLApp.Repository.data
{
    public class Access_Column_GroupData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        private List<dynamic> CreateUpdate_Access_Column_Group(Access_Column_GroupDTO Access_Column_GroupDTO)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            List<dynamic> objclntData = new List<dynamic>();
            try
            {
                string insertProcedure = "[CreateUpdate_Access_Column_Group]";
                AccessColumnImport accessColumnImport = new AccessColumnImport();
                Dictionary<string, string> inputParameter = new Dictionary<string, string>();
                inputParameter.Add("@ACG_PkeyID", 1 + "#bigint#" + Access_Column_GroupDTO.ACG_PkeyID);
                inputParameter.Add("@ACG_GroupID", 1 + "#bigint#" + Access_Column_GroupDTO.ACG_GroupID);
                inputParameter.Add("@ACG_ColumnID", 1 + "#bigint#" + Access_Column_GroupDTO.ACG_ColumnID);
                inputParameter.Add("@ACG_CompanyID", 1 + "#bigint#" + Access_Column_GroupDTO.ACG_CompanyID);
                inputParameter.Add("@ACG_IsActive", 1 + "#bit#" + Access_Column_GroupDTO.ACG_IsActive);
                inputParameter.Add("@ACG_IsDelete", 1 + "#bit#" + Access_Column_GroupDTO.ACG_IsDelete);

                inputParameter.Add("@UserID", 1 + "#bigint#" + Access_Column_GroupDTO.UserId);
                inputParameter.Add("@Type", 1 + "#int#" + Access_Column_GroupDTO.Type);
                inputParameter.Add("@ACG_Pkey_Out", 2 + "#bigint#" + null);
                inputParameter.Add("@ReturnValue", 2 + "#int#" + null);

                objDynamic = obj.SqlCRUD(insertProcedure, inputParameter);
                if (objDynamic[1] == 0)
                {
                    accessColumnImport.ACG_Pkey_ID = "0";
                    accessColumnImport.Status = "0";
                    accessColumnImport.ErrorMessage = "Error while Saviving in the Database";
                }
                else
                {
                    accessColumnImport.ACG_Pkey_ID = Convert.ToString(objDynamic[0]);
                    accessColumnImport.Status = Convert.ToString(objDynamic[1]);
                }
                objclntData.Add(accessColumnImport);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objclntData;

        }
        public List<dynamic> CreateUpdateAccessColumnGroup(Access_Column_GroupDTO Access_Column_GroupDTO)
        {
            List<dynamic> objclntData = new List<dynamic>();

            try
            {
                var Data = JsonConvert.DeserializeObject<List<AccessColumnGroup>>(Access_Column_GroupDTO.Access_Colum_str);
                for (int i = 0; i < Data.Count; i++)
                {
                    Access_Column_GroupDTO access_Column_GroupDTO = new Access_Column_GroupDTO();
                    access_Column_GroupDTO.ACG_PkeyID = Data[i].ACG_PKeyID;
                    access_Column_GroupDTO.ACG_GroupID = Data[i].ACG_GroupID;
                    access_Column_GroupDTO.ACG_ColumnID = Data[i].Wo_Column_PkeyId;
                    if (Data[i].ACG_PKeyID_sel == true)
                    {
                        access_Column_GroupDTO.ACG_IsActive = true;
                        access_Column_GroupDTO.ACG_IsDelete = false;
                    }
                    else
                    {
                        access_Column_GroupDTO.ACG_IsActive = false;
                        access_Column_GroupDTO.ACG_IsDelete = true;
                    }
                    if (access_Column_GroupDTO.ACG_PkeyID == 0)
                    {
                        access_Column_GroupDTO.Type = 1;
                       
                    }
                    else
                    {
                        access_Column_GroupDTO.Type = 2;
                      
                    }
                    access_Column_GroupDTO.UserId = Access_Column_GroupDTO.UserId;
                    objclntData = CreateUpdate_Access_Column_Group(access_Column_GroupDTO);
                }
                
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objclntData;

        }
        private DataSet GetaccessColumnGroupData(Access_Column_Group_Input model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Access_Column_Group]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@ACG_PKeyID", 1 + "#bigint#" + model.ACG_PKeyID);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserId);
                input_parameters.Add("@WhereClause", 1 + "#varchar#" + model.WhereClause);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@PageNumber", 1 + "#int#" + model.PageNumber);
                input_parameters.Add("@NoofRows", 1 + "#int#" + model.NoofRows);
                input_parameters.Add("@Orderby", 1 + "#nvarchar#" + model.Orderby);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }
        public List<dynamic> GetaccessColumnGroupDetails(Access_Column_Group_Input model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetaccessColumnGroupData(model);
                if (ds.Tables.Count > 0)
                {
                    var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                    List<Access_Column_GroupDTO> StudentDocumentData =
                       (from item in myEnumerableFeaprd
                        select new Access_Column_GroupDTO
                        {
                            ACG_PkeyID = item.Field<Int64>("ACG_PKeyID"),
                            ACG_GroupID = item.Field<Int64>("ACG_GroupID"),
                            ACG_ColumnID = item.Field<Int64>("ACG_ColumnID"),
                            ACG_CompanyID = item.Field<Int64>("ACG_CompanyID"),
                            ACG_UserID = item.Field<Int64>("ACG_UserID"),
                        }).ToList();
                    objDynamic.Add(StudentDocumentData);
                }

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

    }
}