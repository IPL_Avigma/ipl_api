﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class Filter_Admin_CustPhLblData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        public List<dynamic> AddUpdate_Filter_Admin_CustPhLbl(Filter_Admin_CustPhLbl_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_Filter_Admin_CustPhLbl_Master]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@CustPhLbl_Filter_PkeyID", 1 + "#bigint#" + model.CustPhLbl_Filter_PkeyID);
                input_parameters.Add("@CustPhLbl_Filter_CustPhLblName", 1 + "#nvarchar#" + model.CustPhLbl_Filter_CustPhLblName);
                input_parameters.Add("@CustPhLbl_Filter_CustPhLblIsActive", 1 + "#bit#" + model.CustPhLbl_Filter_CustPhLblIsActive);
                input_parameters.Add("@CustPhLbl_Filter_IsActive", 1 + "#bit#" + model.CustPhLbl_Filter_IsActive);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserId);
                input_parameters.Add("@CustPhLbl_Filter_PkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        private DataSet Get_Filter_Admin_CustPhLbl(Filter_Admin_CustPhLbl_MasterDTO model)
        {
            DataSet ds = null;
            try
            {

                string selectProcedure = "[Get_Filter_Admin_CustPhLbl_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@CustPhLbl_Filter_PkeyID", 1 + "#bigint#" + model.CustPhLbl_Filter_PkeyID);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> Get_Filter_Admin_CustPhLblDetails(Filter_Admin_CustPhLbl_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = Get_Filter_Admin_CustPhLbl(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Filter_Admin_CustPhLbl_MasterDTO> Forms_Master_Files =
                   (from item in myEnumerableFeaprd
                    select new Filter_Admin_CustPhLbl_MasterDTO
                    {
                        CustPhLbl_Filter_PkeyID = item.Field<Int64>("CustPhLbl_Filter_PkeyID"),
                        CustPhLbl_Filter_CustPhLblName = item.Field<String>("CustPhLbl_Filter_CustPhLblName"),
                        CustPhLbl_Filter_CustPhLblIsActive = item.Field<Boolean?>("CustPhLbl_Filter_CustPhLblIsActive")

                    }).ToList();

                objDynamic.Add(Forms_Master_Files);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}