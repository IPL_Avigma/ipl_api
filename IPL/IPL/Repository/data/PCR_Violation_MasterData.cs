﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class PCR_Violation_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public List<dynamic> AddPCR_Violation_Data(PCR_Violation_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objclntData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_PCR_Violation_Master]";
            PCR_Violation_Master pCR_Violation_Master = new PCR_Violation_Master();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@PCR_Violation_pkeyId", 1 + "#bigint#" + model.PCR_Violation_pkeyId);
                input_parameters.Add("@PCR_Violation_MasterID", 1 + "#bigint#" + model.PCR_Violation_MasterID);
                input_parameters.Add("@PCR_Violation_WO_ID", 1 + "#bigint#" + model.PCR_Violation_WO_ID);
                input_parameters.Add("@PCR_Violation_ValType", 1 + "#int#" + model.PCR_Violation_ValType);
                input_parameters.Add("@PCR_Violation_Any_Citation", 1 + "#varchar#" + model.PCR_Violation_Any_Citation);
                input_parameters.Add("@PCR_Violation_Describe_Citation", 1 + "#varchar#" + model.PCR_Violation_Describe_Citation);
                input_parameters.Add("@PCR_Violation_High_Vandalism_Area", 1 + "#varchar#" + model.PCR_Violation_High_Vandalism_Area);
                input_parameters.Add("@PCR_Violation_Describe_High_Vandalism_Reason", 1 + "#varchar#" + model.PCR_Violation_Describe_High_Vandalism_Reason);
                input_parameters.Add("@PCR_Violation_Any_Unusual_Circumstances", 1 + "#varchar#" + model.PCR_Violation_Any_Unusual_Circumstances);
                input_parameters.Add("@PCR_Violation_Attached_Proof_Path", 1 + "#varchar#" + model.PCR_Violation_Attached_Proof_Path);
                input_parameters.Add("@PCR_Violation_Attached_Proof_Size", 1 + "#int#" + model.PCR_Violation_Attached_Proof_Size);
                input_parameters.Add("@PCR_Violation_Describe", 1 + "#varchar#" + model.PCR_Violation_Describe);
                input_parameters.Add("@PCR_Violation_Attached_NoticesPosted_FilePath", 1 + "#varchar#" + model.PCR_Violation_Attached_NoticesPosted_FilePath);
                input_parameters.Add("@PCR_Violation_Attached_NoticesPosted_FileName", 1 + "#varchar#" + model.PCR_Violation_Attached_NoticesPosted_FileName);
                input_parameters.Add("@PCR_Violation_IsActive", 1 + "#varchar#" + model.PCR_Violation_IsActive);
                input_parameters.Add("@PCR_Violation_IsDelete", 1 + "#varchar#" + model.PCR_Violation_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@fwo_pkyeId", 1 + "#bigint#" + model.fwo_pkyeId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@PCR_Violation_pkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    pCR_Violation_Master.PCR_Violation_pkeyId = "0";
                    pCR_Violation_Master.Status = "0";
                    pCR_Violation_Master.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    pCR_Violation_Master.PCR_Violation_pkeyId = Convert.ToString(objData[0]);
                    pCR_Violation_Master.Status = Convert.ToString(objData[1]);


                }
                objclntData.Add(pCR_Violation_Master);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objclntData;



        }

        private DataSet GetPCRPCRViolationMaster(PCR_Violation_MasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_PCR_Violation_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@PCR_Violation_pkeyId", 1 + "#bigint#" + model.PCR_Violation_pkeyId);
                input_parameters.Add("@PCR_Violation_WO_ID", 1 + "#bigint#" + model.PCR_Violation_WO_ID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetPCRPCRViolationDetails(PCR_Violation_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetPCRPCRViolationMaster(model);

                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                }

                //var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                //List<PCR_Violation_MasterDTO> PCRViolation =
                //   (from item in myEnumerableFeaprd
                //    select new PCR_Violation_MasterDTO
                //    {
                //        PCR_Violation_pkeyId = item.Field<Int64>("PCR_Violation_pkeyId"),
                //        PCR_Violation_MasterID = item.Field<Int64>("PCR_Violation_MasterID"),
                //        PCR_Violation_WO_ID = item.Field<Int64>("PCR_Violation_WO_ID"),
                //        PCR_Violation_ValType = item.Field<int?>("PCR_Violation_ValType"),
                //        PCR_Violation_Any_Citation = item.Field<String>("PCR_Violation_Any_Citation"),
                //        PCR_Violation_Describe_Citation = item.Field<String>("PCR_Violation_Describe_Citation"),
                //        PCR_Violation_High_Vandalism_Area = item.Field<String>("PCR_Violation_High_Vandalism_Area"),
                //        PCR_Violation_Describe_High_Vandalism_Reason = item.Field<String>("PCR_Violation_Describe_High_Vandalism_Reason"),
                //        PCR_Violation_Any_Unusual_Circumstances = item.Field<String>("PCR_Violation_Any_Unusual_Circumstances"),
                //        PCR_Violation_Attached_Proof_Path = item.Field<String>("PCR_Violation_Attached_Proof_Path"),
                //        PCR_Violation_Attached_Proof_Size = item.Field<int?>("PCR_Violation_Attached_Proof_Size"),
                //        PCR_Violation_Describe = item.Field<String>("PCR_Violation_Describe"),
                //        PCR_Violation_Attached_NoticesPosted_FilePath = item.Field<String>("PCR_Violation_Attached_NoticesPosted_FilePath"),
                //        PCR_Violation_Attached_NoticesPosted_FileName = item.Field<String>("PCR_Violation_Attached_NoticesPosted_FileName"),
                //        PCR_Violation_IsActive = item.Field<Boolean?>("PCR_Violation_IsActive"),


                //    }).ToList();

                //objDynamic.Add(PCRViolation);

                //if (ds.Tables.Count > 1)
                //{
                //    var pcrHistory = ds.Tables[1].AsEnumerable();
                //    List<PCR_Violation_MasterDTO> violationHistory =
                //       (from item in pcrHistory
                //        select new PCR_Violation_MasterDTO
                //        {
                //            PCR_Violation_pkeyId = item.Field<Int64>("PCR_Violation_pkeyId"),
                //            PCR_Violation_MasterID = item.Field<Int64>("PCR_Violation_MasterID"),
                //            PCR_Violation_WO_ID = item.Field<Int64>("PCR_Violation_WO_ID"),
                //            PCR_Violation_ValType = item.Field<int?>("PCR_Violation_ValType"),
                //            PCR_Violation_Any_Citation = item.Field<String>("PCR_Violation_Any_Citation"),
                //            PCR_Violation_Describe_Citation = item.Field<String>("PCR_Violation_Describe_Citation"),
                //            PCR_Violation_High_Vandalism_Area = item.Field<String>("PCR_Violation_High_Vandalism_Area"),
                //            PCR_Violation_Describe_High_Vandalism_Reason = item.Field<String>("PCR_Violation_Describe_High_Vandalism_Reason"),
                //            PCR_Violation_Any_Unusual_Circumstances = item.Field<String>("PCR_Violation_Any_Unusual_Circumstances"),
                //            PCR_Violation_Attached_Proof_Path = item.Field<String>("PCR_Violation_Attached_Proof_Path"),
                //            PCR_Violation_Attached_Proof_Size = item.Field<int?>("PCR_Violation_Attached_Proof_Size"),
                //            PCR_Violation_Describe = item.Field<String>("PCR_Violation_Describe"),
                //            PCR_Violation_Attached_NoticesPosted_FilePath = item.Field<String>("PCR_Violation_Attached_NoticesPosted_FilePath"),
                //            PCR_Violation_Attached_NoticesPosted_FileName = item.Field<String>("PCR_Violation_Attached_NoticesPosted_FileName"),
                //            PCR_Violation_IsActive = item.Field<Boolean?>("PCR_Violation_IsActive"),


                //        }).ToList();

                //    objDynamic.Add(violationHistory);
                //}
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}