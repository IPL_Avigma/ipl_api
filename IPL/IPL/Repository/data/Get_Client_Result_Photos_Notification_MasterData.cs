﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
namespace IPL.Repository.data
{
    public class Get_Client_Result_Photos_Notification_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        public DataSet GetClientResultPhotosNotificationMaster(Client_Result_Photos_Notification_MasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Client_Result_Photos_Notification_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@PN_Pkey_Id", 1 + "#bigint#" + model.PN_Pkey_Id);
                input_parameters.Add("@PN_WoId", 1 + "#bigint#" + model.PN_WoId);
                input_parameters.Add("@PN_UserId", 1 + "#bigint#" + model.PN_UserId);
                input_parameters.Add("@WhereClause", 1 + "#nvarchar#" + model.WhereClause);
                input_parameters.Add("@PN_IplCompanyId", 1 + "#bigint#" + model.PN_IplCompanyId);

                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetClient_Result_Photos_Notification_MasterDetails(Client_Result_Photos_Notification_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetClientResultPhotosNotificationMaster(model);

                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                }

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}