﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class PCR_MCS_Maintenance_Vendor_Checklist_Data
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        public List<dynamic> PostMcsMaintenanceVendorChecklistFormMaster(PCR_MCS_Maintenance_Vendor_Checklist_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_PCR_MCS_Maintenance_Vendor_Checklist]";
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@MVC_PkeyID", 1 + "#bigint#" + model.MVC_PkeyID);
                input_parameters.Add("@MVC_WO_ID", 1 + "#bigint#" + model.MVC_WO_ID);
                input_parameters.Add("@MVC_Property_Info", 1 + "#nvarchar#" + model.MVC_Property_Info);
                input_parameters.Add("@MVC_Completion_Info", 1 + "#nvarchar#" + model.MVC_Completion_Info);
                input_parameters.Add("@MVC_Utilities", 1 + "#nvarchar#" + model.MVC_Utilities);
                input_parameters.Add("@MVC_Damage", 1 + "#nvarchar#" + model.MVC_Damage);
                input_parameters.Add("@MVC_Winterization_Info", 1 + "#nvarchar#" + model.MVC_Winterization_Info);
                input_parameters.Add("@MVC_Violation", 1 + "#nvarchar#" + model.MVC_Violation);
                input_parameters.Add("@MVC_Validation", 1 + "#nvarchar#" + model.MVC_Validation);
                input_parameters.Add("@MVC_Check_Ins", 1 + "#nvarchar#" + model.MVC_Check_Ins);
                input_parameters.Add("@MVC_Notes", 1 + "#nvarchar#" + model.MVC_Notes);
                input_parameters.Add("@MVC_IsActive", 1 + "#bit#" + model.MVC_IsActive);
                input_parameters.Add("@MVC_IsDelete", 1 + "#bit#" + model.MVC_IsDelete);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@fwo_pkyeId", 1 + "#bigint#" + model.fwo_pkyeId);
                input_parameters.Add("@MVC_PkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);
                objData = obj.SqlCRUD(insertProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        private DataSet GetMcsMaintenanceVendorChecklistFormMaster(PCR_MCS_Maintenance_Vendor_Checklist_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_PCR_MCS_Maintenance_Vendor_Checklist]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@MVC_PkeyID", 1 + "#bigint#" + model.MVC_PkeyID);
                input_parameters.Add("@MVC_WO_ID", 1 + "#bigint#" + model.MVC_WO_ID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetMcsMaintenanceVendorChecklistFormMasterDetail(PCR_MCS_Maintenance_Vendor_Checklist_DTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetMcsMaintenanceVendorChecklistFormMaster(model);

                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                }

                //var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                //List<PCR_MCS_Maintenance_Vendor_Checklist_DTO> pcrdata =
                //   (from item in myEnumerableFeaprd
                //    select new PCR_MCS_Maintenance_Vendor_Checklist_DTO
                //    {
                //        MVC_PkeyID = item.Field<Int64>("MVC_PkeyID"),
                //        MVC_WO_ID = item.Field<Int64>("MVC_WO_ID"),
                //        MVC_Company_ID = item.Field<Int64>("MVC_Company_ID"),
                //        MVC_Property_Info = item.Field<String>("MVC_Property_Info"),
                //        MVC_Completion_Info = item.Field<String>("MVC_Completion_Info"),
                //        MVC_Utilities = item.Field<String>("MVC_Utilities"),
                //        MVC_Damage = item.Field<String>("MVC_Damage"),
                //        MVC_Winterization_Info = item.Field<String>("MVC_Winterization_Info"),
                //        MVC_Violation = item.Field<String>("MVC_Violation"),
                //        MVC_Validation = item.Field<String>("MVC_Validation"),
                //        MVC_Check_Ins = item.Field<String>("MVC_Check_Ins"),
                //        MVC_Notes = item.Field<String>("MVC_Notes"),
                //        MVC_IsActive = item.Field<Boolean>("MVC_IsActive"),
                //    }).ToList();

                //objDynamic.Add(pcrdata);


                //if (ds.Tables.Count > 1)
                //{

                //    var pcrHistrory = ds.Tables[1].AsEnumerable();
                //    List<PCR_MCS_Maintenance_Vendor_Checklist_DTO> pcrhistorydata =
                //       (from item in pcrHistrory
                //        select new PCR_MCS_Maintenance_Vendor_Checklist_DTO
                //        {
                //            MVC_PkeyID = item.Field<Int64>("MVC_PkeyID"),
                //            MVC_WO_ID = item.Field<Int64>("MVC_WO_ID"),
                //            MVC_Company_ID = item.Field<Int64>("MVC_Company_ID"),
                //            MVC_Property_Info = item.Field<String>("MVC_Property_Info"),
                //            MVC_Completion_Info = item.Field<String>("MVC_Completion_Info"),
                //            MVC_Utilities = item.Field<String>("MVC_Utilities"),
                //            MVC_Damage = item.Field<String>("MVC_Damage"),
                //            MVC_Winterization_Info = item.Field<String>("MVC_Winterization_Info"),
                //            MVC_Violation = item.Field<String>("MVC_Violation"),
                //            MVC_Validation = item.Field<String>("MVC_Validation"),
                //            MVC_Check_Ins = item.Field<String>("MVC_Check_Ins"),
                //            MVC_Notes = item.Field<String>("MVC_Notes"),
                //            MVC_IsActive = item.Field<Boolean>("MVC_IsActive"),
                //        }).ToList();

                //    objDynamic.Add(pcrhistorydata);
                //}

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}