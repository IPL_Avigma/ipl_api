﻿using Avigma.Repository.Lib;
using Avigma.Models;
using IPLApp.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using AutoMapper;
using iTextSharp.text.pdf.qrcode;
using IPL.Models;
using IPLApp.Repository.data;

namespace IPL.Repository.data
{
    public class WorkOrderMaster_ImportAPI_Data
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public List<dynamic> APIWorkOrderItemDetails(string WorkOrderMaster_DTO, Int64 WI_Pkey_ID, string Filename, string FilePath,Int64 UserID)
        {
            WorkOrderMaster_API_Data workOrderMaster_API_Data = new WorkOrderMaster_API_Data();
            WorkOrderItemDetails_API_Data workOrderItemDetails_API_Data = new WorkOrderItemDetails_API_Data();

            List<dynamic> objDynamic = new List<dynamic>();

            try
            {

                JsonValidate jsonValidate = new JsonValidate();
                string Formatjson = FormattJsonData(WorkOrderMaster_DTO);
                bool val = jsonValidate.IsValidJson(Formatjson);
                if (val)
                {
                    var Data = JsonConvert.DeserializeObject<PPW_WorkOrderMaster_API_DTO>(Formatjson);
                    var config = new MapperConfiguration(cfg =>
                    {
                        cfg.CreateMap<PPW_WorkOrderMaster_API_DTO, WorkOrderMaster_API_DTO>();
                    });

                    IMapper mapper = config.CreateMapper();
                    var dest = mapper.Map<PPW_WorkOrderMaster_API_DTO, WorkOrderMaster_API_DTO>(Data);
                    if (string.IsNullOrEmpty(dest.Wo))
                    {
                        dest.Wo = dest.ppw;
                    }


                    GoogleLocation GoogleLocation = new GoogleLocation();
                    GoogleLocationDTO googleLocationDTO = new GoogleLocationDTO();

                    dest.Type = 1;
                    dest.ImportMasterPkey = WI_Pkey_ID;

                    WorkOrderMasterData workOrderMasterData = new WorkOrderMasterData();
                    workOrderxDTO workOrderxDTO = new workOrderxDTO();
                    workOrderxDTO.address1 = dest.address;
                    workOrderxDTO.Type = 2;
                    DataSet ds = workOrderMasterData.Get_WorkOrderLatLongByAddress(workOrderxDTO);

                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                dest.gpsLatitude = ds.Tables[0].Rows[i]["gpsLatitude"].ToString();
                                dest.gpsLongitude = ds.Tables[0].Rows[i]["gpsLongitude"].ToString();
                            }
                        }
                        else
                        {
                            ///////////////////////code added to exract GPS ////////////////////
                            googleLocationDTO.Address = dest.address +" "+ dest.city +" "+ dest.state + " "+ dest.zip;
                            googleLocationDTO = GoogleLocation.GetLatitudeLogitudeWithAPIKey(googleLocationDTO);
                            dest.gpsLatitude = googleLocationDTO.Latitude;
                            dest.gpsLongitude = googleLocationDTO.Longitude;
                        }
                    }
                    else
                    {
                        ///////////////////////code added to exract GPS ////////////////////
                        googleLocationDTO.Address = dest.address +" "+ dest.city  + " " + dest.state + " " + dest.zip;
                        googleLocationDTO = GoogleLocation.GetLatitudeLogitudeWithAPIKey(googleLocationDTO);
                        dest.gpsLatitude = googleLocationDTO.Latitude;
                        dest.gpsLongitude = googleLocationDTO.Longitude;
                    }


               
                    dest.Imp_Wo_File_Name = Filename;
                    dest.Imp_Wo_File_Path = FilePath;
                    dest.UserId = Convert.ToInt32(UserID);
                    objDynamic = workOrderMaster_API_Data.WorkOrderMasterDetails(dest);
                    if (objDynamic[1] == 1)
                    {


                        for (int i = 0; i < Data.work_order_item_details.Count; i++)
                        {
                            WorkOrderItemDetail workOrderItemDetail = new WorkOrderItemDetail();
                            workOrderItemDetail = Data.work_order_item_details[i];

                            var configItems = new MapperConfiguration(cfg =>
                            {
                                cfg.CreateMap<WorkOrderItemDetail, WorkOrderItemDetails_API_DTO>();
                            });

                            IMapper mapperItems = configItems.CreateMapper();
                            var destItems = mapperItems.Map<WorkOrderItemDetail, WorkOrderItemDetails_API_DTO>(workOrderItemDetail);
                            destItems.Type = 1;

                            if (!string.IsNullOrEmpty(destItems.Description) || !string.IsNullOrEmpty(destItems.Additional_Instructions))
                            {
                                destItems.WorkOrderMaster_API_Data_Pkey_Id = objDynamic[0];

                                workOrderItemDetails_API_Data.WorkOrderItemDetails(destItems);
                            }
                            else
                            {
                                log.logDebugMessage("------------------WorkOrder Item Null Record Found Start -------------" + objDynamic[0].ToString());
                                log.logDebugMessage(destItems.Description);
                                log.logDebugMessage(destItems.Additional_Instructions);
                                log.logDebugMessage("------------------WorkOrder Item Null Record Found End -------------" + objDynamic[0].ToString());
                            }



                        }
                    }
                }
                else
                {

                    log.logErrorMessage("------------------InValid Json -------------");
                    log.logErrorMessage(Filename);
                    log.logErrorMessage(WorkOrderMaster_DTO);
                }


            }
            catch (Exception ex)
            {
                log.logErrorMessage("------------------Json Error Start-------------");
                log.logErrorMessage(Filename);
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage("------------------Json Error  End-------------");

            }
            return objDynamic;
        }

        // get workorderQueue

        private string FormattJsonData(string JsonData)
        {
            string Formatjson = string.Empty;
            //string chk = "{\"category\":\"Belinda - 8167869413\",\"wo\":\"8214674\",\"address\":\"1219 NORTH CONCORD\\nSPRINGFIELD, MO 65802\",\"estimated_complete_date\":\"\",\"loan_info\":\"****4477\",\"office_locked\":\"no\",\"due_date\":\"January 30, 2021\",\"ready_for_office_Contractor\":\"no\",\"work_order_item_details\":[{\"Description\":\"LEVEL 2 PROPERTY CONDITION PHOTOS REQUIRED (50-100 pics)\",\"Qty\":\"0\",\"Price\":\"0.00\",\"Total\":\"0.00\",\"AdditionalInstructions\":\"All non-grass cut orders require level 2 at the very minimum.\\n\\nExterior photos - 360 view around house and all outbuildings.\\n\\nPhotos of the roof from the ground.\\n\\nDocument any damages or issues you encounter.\\n\\nPhotos should be large overall views clearly showing the layout of property and buildings.\\n\\nInterior Photos 2013 All ceilings, floors, room pics from door and opposite corner, appliances, inside toilet bowls, furnace, water heater, breaker box, etc%u2026\\n\\nDamages 2013 take pics of any damages that are new to you. Sometimes we will be given properties we have never been to. When in doubt take a pic. You can never take too many photos of a property.\\n\\nThe more stuff we find wrong, the more we can bid.\",\"Source\":\"Auto\"},{\"Description\":\"*IMPORTANT* DO NOT REMOVE UNAUTHORIZED ITEMS\",\"Qty\":\"1\",\"Price\":\"0.00\",\"Total\":\"0.00\",\"AdditionalInstructions\":\"REMOVING ANYTHING FROM A HOUSE WITHOUT BEING AUTHORIZED IS A BIG DEAL!!! ALL THE CONTENTS HAVE BEEN PRE-PHOTOGRAPHED. IF SOMETHING COMES UP MISSING THE CLIENT REQUIRES US TO GIVE THEM YOUR PERSONAL INFORMATION SO THEY CAN PROSECUTE. SO DON'T BE STUPID THERE ARE TOO MANY NOSEY NEIGHBORS, UPSET HOMEOWNERS, INSPECTORS AND OTHER CONTRACTORS JUST DYING TO CALL THE CLIENT AND TELL ON YOU. DON'T BE STUPID. IT'S NOT WORTH IT!!!\",\"Source\":\"Auto\"},{\"Description\":\"Sign In Sheet\",\"Qty\":\"1\",\"Price\":\"0.00\",\"Total\":\"0.00\",\"AdditionalInstructions\":\"always sign in and use the name of the client for company name E.G. Safeguard, MCS or Cyprexx, Etc..\",\"Source\":\"Auto\"},{\"Description\":\"*PPW APP Instructions and tips*\",\"Qty\":\"1\",\"Price\":\"0.00\",\"Total\":\"0.00\",\"AdditionalInstructions\":\"Click the following link to our guide and videos that that will teach you how to use different features in the app. https:\\/\\/drive.google.com\\/open?id=1xjdas6t9E98Zu8Xu7hzUnUR9qpRIXMCX\",\"Source\":\"Auto\"},{\"Description\":\"All Invoices subject to adjustment\",\"Qty\":\"0\",\"Price\":\"0.00\",\"Total\":\"0.00\",\"AdditionalInstructions\":\"All Invoice are subject to adjustment based upon work performed, Photo time stamps, and work quality.\",\"Source\":\"Auto\"},{\"Description\":\"10.)\",\"Qty\":\"0\",\"Price\":\"0.00\",\"Total\":\"0.00\",\"AdditionalInstructions\":\"PROVIDE PHOTOS TO SUPPORT WORK COMPLETED\\/MEASUREMENT PHOTOS\",\"Source\":\"Import\"},{\"Description\":\"11.)\",\"Qty\":\"0\",\"Price\":\"0.00\",\"Total\":\"0.00\",\"AdditionalInstructions\":\"PROVIDE DUMP RECEIPT\",\"Source\":\"Import\"},{\"Description\":\"12.)\",\"Qty\":\"0\",\"Price\":\"0.00\",\"Total\":\"0.00\",\"AdditionalInstructions\":\"\",\"Source\":\"Import\"},{\"Description\":\"INSTRUCTIONS FOR SERVICES TO BE PERFORMED\",\"Qty\":\"0\",\"Price\":\"0.00\",\"Total\":\"0.00\",\"AdditionalInstructions\":\"LOAN #: ****4477 POS REQUIRED: YES\\nSEND TO ASPEN: YES\\nHOA PHONE:\\n\\nPROPERTY INFO INITIAL SECURE DATE: 06\\/29\\/2019 LAST WINTERIZATION DATE:\\nLAST LOCK CHANGE DATE: 11\\/28\\/2019 LAST SNOW REMOVAL DATE:\\nFTV DATE: 11\\/19\\/2020 LOCKBOX: 4477 INITIAL GRASS CUT DATE: 04\\/16\\/2020\\nLAST INSPECTED DATE: 01\\/25\\/2021 KEYCODE: 67767\\/4477 LAST GRASS CUT DATE: 10\\/12\\/2020\\nLAST INSP OCCUPANCY: VACANT SECURITY ACCESS CODE: LAWN SIZE (SQ FT): 5,318\\nPROPERTY TYPE: SINGLE FAMILY\\nCOLOR:\\n\\nIF YOU ARE UNABLE TO COMPLETE THIS JOB AND RETURN IT TO US WITHIN THE SCHEDULED TIMEFRAME\\nPLEASE REJECT ORDER, ADVISE YOUR VM SO WE CAN RE-ASSIGN THIS JOB.\",\"Source\":\"Import\"},{\"Description\":\"KEY \\/ LOCK INSTRUCTIONS\",\"Qty\":\"0\",\"Price\":\"0.00\",\"Total\":\"0.00\",\"AdditionalInstructions\":\"**********WF01 - BID APPROVAL**********\\n. A PROPERTY VALIDATION PHOTO IS PROVIDED AT THE BOTTOM OF THIS WORK ORDER. THIS IS TO ENSURE\\nYOU ARE AT THE CORRECT LOCATION AND PROPERTY FOR ANY AND ALL WORK PERFORMED. YOU MUST\\nVALIDATE YOU ARE AT THE PROVIDED PROPERTY BEFORE ANY WORK IS STARTED. IF THERE ARE ANY\\nQUESTIONS REGARDING PROPERTY VALIDATION, PLEASE CALL MSI FROM SITE FOR DIRECTION AT 1-800-825-\\n4101 (OPTION 9 FOR REGION 9).********DO NOT ENTER PROPERTY UNLESS THERE IS A POST SECURE NOTICE\\nPOSTED UPON ARRIVAL AT PROPERTY. IF THERE IS NO POST SECURE NOTICE, CALL FROM SITE 1-800-825-4101\\n(OPTION 9 FOR REGION 9)**********\\n. PROPERTY MAY ALREADY BE SECURED: LOCKBOX _______4477__________\\/ KEY CODE\\n_______67767____________\\n\\n. IF UNABLE TO ENTER USING THE EXISTING KEY CODE, CALL FROM SITE 1-800-825-4101 (OPTION 9 FOR\\nREGION 9).\\n. IF UNABLE TO GAIN ACCESS DUE TO GATED COMMUNITY CALL FROM SITE 1-800-825-4101 (OPTION 9 FOR\\nREGION 9) (OBTAIN NAME\\/PHONE #\\/FAX # OF HOA, COA, ETC. AS NEEDED).\\n. IF REPORTED AS OCCUPIED, ATTEMPT DIRECT CONTACT AND REPORT OCCUPIED STATUS.\\n67767\\/4477\",\"Source\":\"Import\"},{\"Description\":\"GENERAL INSTRUCTIONS\",\"Qty\":\"0\",\"Price\":\"0.00\",\"Total\":\"0.00\",\"AdditionalInstructions\":\"********** WF01 - BID APPROVAL**********\\nRECEIVED BID APPROVAL FOR THE FOLLOWING: (PLEASE COMPLETE AND INVOICE WITHIN THE APPROVED\\nAMOUNT ONLY).\\n00-825-4101 - OPTION 9 FOR REGION 9 TO REPORT THE VIOLATION.\\n. BIG 6 DAMAGES: REPORT AND PROVIDE PHOTOS FOR ALL BIG 6 DAMAGES AND STRUCTURAL DAMAGE.\",\"Source\":\"Import\"},{\"Description\":\"WINTERIZATION INSTRUCTIONS\",\"Qty\":\"0\",\"Price\":\"0.00\",\"Total\":\"0.00\",\"AdditionalInstructions\":\"PLEASE SEE GENERAL INSTRUCTIONS FOR DIRECTION\",\"Source\":\"Import\"},{\"Description\":\"PHOTO INSTRUCTIONS\",\"Qty\":\"0\",\"Price\":\"0.00\",\"Total\":\"0.00\",\"AdditionalInstructions\":\"********** WF01 - BID APPROVAL**********SUBMIT CLEAR AND DETAILED PHOTOS WITH DATE STAMPS, AND LABEL\\nBY ROOM AND LOCATION**********\\n**BEST PRACTICES TO CONSIDER: PHOTOS OF WHITEBOARD OR PAPER WITH #1, #2 TO SUPPORT THE\\nNUMBER OF TRUCK LOADS, BAGS OF DEBRIS, NUMBER OF TREES, ETC.; PHOTOS OF A TAPE MEASURE\\nSHOWING THE SIZE OF TRUCK TRAILER, AREA OF MOLD REMEDIATED, HEIGHT OF GRASS, ETC**\\nPHOTOS SHOULD INCLUDE BUT ARE NOT LIMITED TO THE FOLLOWING:\\n. PROPERTY ADDRESS AND STREET SIGN (REQUIRED)\\n. PHOTO OF STREET SCENE\\n. SECURE POSTING (REQUIRED) - MUST BE CLEAR AND ABLE TO BE READ. (REQUIRED IN ORDER TO RECEIVE\\nPAYMENT)\\n. LOCK CHANGE: BEFORE, DURING, AND AFTER PHOTOS ARE REQUIRED OF THE LOCK CHANGE PROCESS (IF\\nAPPLICABLE).\\n. DAMAGES AND REPAIRS: PHOTOS MUST SUPPORT THE DESCRIPTION, LOCATION, MEASUREMENTS, AND\\nMATERIALS NEEDED AS LISTED IN BIDS (INCLUDING MOLD DAMAGES).\\n. MOLD: IF MOLD DAMAGES ARE REPORTED, CLEAR PHOTOS USING TAPE MEASURE, PAINTERS TAPE, OR\\nCHALK SHOWING DIMENSIONS ARE REQUIRED.\\n. ROOF AND CEILING: PHOTOS OF EXTERIOR ROOF AND INTERIOR CEILINGS OF EVERY ROOM REQUIRED\\nREGARDLESS OF CONDITION (MUST SHOW DAMAGES IF DAMAGES ARE REPORTED).\\n. EXTERIOR REQUIREMENTS: EACH SIDE OF PROPERTY FROM THE SAME ANGLE (FRONT, REAR, AND SIDES).\\nPHOTOS OF ALL EXTERIOR STRUCTURES REQUIRED. PHOTOS SHOULD ALSO BE TAKEN FROM THE HOUSE\\nOUT TOWARD THE PROPERTY LINE. IF THERE IS A PRIVACY FENCE, PHOTOS SHOULD BE TAKEN ON BOTH\\nSIDES OF FENCE IF POSSIBLE.\\n. INTERIOR REQUIREMENTS: PHOTOS (WIDE ANGLE) OF EVERY ROOM, INCLUDING: CLOSETS, CEILINGS AND\\nFLOORS, CORNERS, ATTICS, BASEMENTS, OPENED DRAWERS AND CABINETS, ALL APPLIANCES, ELECTRICAL\\nPANELS, STAIRWAYS, HALLWAYS AND FIREPLACES.\\n. PERSONAL PROPERTY: MUST HAVE CLEAR PHOTOS OF ALL INTERIOR AND EXTERIOR PERSONALS TO\\nSUPPORT CONDITION OF PROPERTY OR WORK COMPLETED. PROVIDE PHOTOS OF ALL ENTRANCES THAT\\nHAVE PERSONAL PROPERTY POSTINGS AND NOTICE OF RIGHT TO RECLAIM POSTINGS. MUST PROVIDE\\nPHOTOS TO SUPPORT THE NUMBER OF TRUCK LOADS, BAGS OF PERSONALS AND PHOTOS OF TAPE\\nMEASUREMENTS SHOWING THE SIZE OF TRUCK TRAILER ETC.\\n\\n. STORAGE OF PERSONAL PROPERTY: BEFORE, DURING AND AFTER PHOTOS SHOWING THE STORAGE UNIT\\nWITH THE PERSONAL PROPERTY INSIDE. AFTER PHOTOS ALSO MUST SUPPORT THE STORAGE UNIT WITH THE\\nPERSONAL PROPERTY INSIDE AND THE UNIT IS CLOSED AND LOCKED. MUST PROVIDE A COPY OF STORAGE\\nAGREEMENT\\/RECEIPT.\\n. DEBRIS: MUST HAVE CLEAR PHOTOS OF ALL INTERIOR AND EXTERIOR DEBRIS TO SUPPORT CONDITION OF\\nPROPERTY AND WORK COMPLETED. MUST PROVIDE PHOTOS TO SUPPORT THE NUMBER OF TRUCK LOADS,\\nBAGS OF PERSONALS AND PHOTOS OF TAPE MEASUREMENTS SHOWING THE SIZE OF TRUCK TRAILER ETC.\\n. MULTI UNIT: INCLUDE PHOTOS OF COMMON AREAS SUCH AS STAIRWELLS, HALLWAYS, LOBBIES, ETC. AND\\nLABEL PHOTOS WITH UNIT NUMBER.\\n. WORK COMPLETED: BEFORE, DURING, AND AFTER PHOTOS (WIDE ANGLE) ARE REQUIRED OF ALL WORK\\nCOMPLETED, INCLUDING TRASH AND DEBRIS REMOVAL (TRUCK EMPTY AND FULL, ROOMS FULL AND EMPTY,\\nETC.)\\n. BIDS: PHOTOS MUST SUPPORT THE DESCRIPTION, LOCATION, MEASUREMENTS, AND MATERIALS NEEDED AS\\nLISTED IN BIDS.\\n. APPLIANCES: BEFORE, DURING, AND AFTER PHOTOS REQUIRED OF ALL FIXTURES, INCLUDING ICE MAKERS,\\nDISH WASHERS, WATER HEATER, BLOWING LINES, TURNING OFF BREAKERS, SUMP PUMPS, GENERATOR\\nHOOKED TO COMPRESSOR, GAUGES OF PRESSURE TESTING (AT 35 PSI), ETC.\\n. ELECTRIC METER: MUST BE ABLE TO READ DETAILS ON METER.\\n. MOBILE HOMES: MUST PROVIDE CLEAR PHOTOS OF DATA PLATE INCLUDING VIN AND SERIAL NUMBERS.\\n.**********WINTERIZATION SECTION BELOW**********\\n. BEFORE, DURING, AND AFTER PHOTOS FROM THE SAME ANGLE, OF THE ENTIRE WINTERIZATION PROCESS\\nARE REQUIRED.\\n. FROZEN PROPERTY: PHOTOS MUST BE PROVIDED THAT CONFIRM THE PROPERTY IS FROZEN (TANK FROZEN,\\nLINES BURST AND FROSTED, ETC.)\\n. BREAKERS: PHOTOS OF BREAKERS TURNED TO OFF POSITION, UNLESS SUMP PUMP OR DEHUMIDIFIER IS\\nPRESENT.\\n. DRAINING: PHOTOS MUST SHOW THE WATER COMING OUT OF A HOSE ATTACHED TO THE HOT WATER\\nHEATER AND DRAINING TO THE EXTERIOR OF THE PROPERTY.\\n. PLUMBING: PHOTOS TO CONFIRM WATER IS BLOWN OUT OF PIPES AND THAT THE TOILET WATER HAS ALSO\\nBEEN DRAINED BOTH IN THE TANK AND IN THE BOWL.\\n. TOILETS: IF TOILETS ARE REPORTED DIRTY AND CANNOT COME CLEAN, ACTION SHOTS OF CLEANING ARE\\nREQUIRED.\\n. PRESSURE GAUGE: PHOTOS OF THE PRESSURE GAUGE ARE REQUIRED ON EVERY WINTERIZATION. THE\\nPRESSURE TEST IS REQUIRED TO HOLD 35 PSI FOR 20 MINUTES.\\n. NON-TOXIC ANTIFREEZE: PHOTOS OF EVERY TRAP HAVING NON-TOXIC ANTIFREEZE POURED IN THEM MUST\\nBE PROVIDED. THE TRAPS INCLUDE, BUT ARE NOT LIMITED TO: TOILET BOWLS AND TOILET TANKS,\\nDISHWASHER DRAINS, ALL SINKS, SHOWER\\/TUB DRAINS, WASHING MACHINE DRAIN, ANY EXTERIOR\\nAPPLIANCE ATTACHED TO INTERIOR PLUMBING (HOT TUBS), ETC.\\n. WINTERIZATION STICKERS: PHOTOS OF ALL STICKERS PLACED WITH WELLS FARGO CONTACT INFORMATION.\\n\\n***** IN CASES OF VANDALISM OR THEFT, REPORT TO THE POLICE AN ESTIMATE OF *****\\n***** DAMAGES AND PROVIDE THE CASE NUMBER AND OFFICER'S NAME TO MSI *****\\nPOINT OF SERVICE QUESTIONS 1.YES 2. NO 3. N\\/A\\n1. HAS A CODE VIOLATION OR OFFICIAL NOTICE BEEN POSTED AT THE PROPERTY 1.YES 2. NO 3. N\\/A\\n2. EXTERIOR DEBRIS, JUNK, TRASH OR DEAD VEGETATION FOUND AT THE PROPERTY\\n3. IS THE LAWN LANDSCAPED AND MAINTAINED 1.YES 2. NO 3. N\\/A\\n4. ANY OBSERVED ESCALATED EVENTS OR ISSUES AT THE PROPERTY\\n5. ANY UNSECURED OPENINGS,BROKEN WINDOWS OR DOORS AT THE PROPERTY 1.YES 2. NO 3. N\\/A\\n6. IS THERE ROOF DAMAGE THAT NEEDS REPAIR 1.YES 2. NO 3. N\\/A\\n7. IS THE PROPERTY VACANT\\n8. DOES THE PROPERTY HAVE INDICATIONS OF VANDALISM OR AN OBVIOUS CRIME SCENE 1.YES 2. NO 3. UNKNOWN\\n9. IS THERE ANY OBVIOUS HEALTH AND SAFETY ISSUE AT THE PROPERTY\\n10. DOES THE FOLLOWING DAMAGE EXIST AT THE PROPERTY THAT MEETS THE BIG SIX DEFINITION: FIRE, FLOOD, TORNADO, HURRICANE 1.YES 2. NO 3. N\\/A\\nBOILER(MULTI-UNIT), EARTHQUAKE 1.YES 2. NO 3. N\\/A\\n\\nASPEN ABC NUMBER: 1.YES 2. NO 3. N\\/A\\nPROPERTY AND PRESERVATION CHECKLIST\\n\\nPROPERTY INFORMATION\\n\\nPROPERTY ADDRESS:_________________________________UNIT #______CITY:______________ STATE: __ ZIP:_____\\nPROPERTY TYPE: SFR__ TOWN HOME__ MULTI-UNIT__CONDO__MOBILE HOME*__ MANF HOME__ # OF UNITS:__\\n*VIN#\\/SERIAL #:__________________ IS PROPERTY IN HIGH VANDAL AREA YES__ NO___\\n\\nSECURING Y N R WINTERIZATION YNR\\n\\nARE ALL EXTERIOR DOORS SECURED HAS PROPERTY BEEN WINTERIZED\\nKEY CODE IF YES, WHEN, BY WHOM, AND WHAT TYPE\\nIS GARAGE DOOR OPENER DIABLED IF NO, CALL FROM SITE FOR IMMEDIATE APPROVAL\\nIS OVERHEAD GARAGE DOOR SECURED IS WINTERIZATION INTACT\\nIS SLIDING GLASS DOOR SECURED ARE THE TOILETS CLEAN\\nARE ALL WINDOWS SECURED ARE WINTERIZATION STICKERS POSTED\\nWHAT IS UNSECURE IF POOL, HAS IT BEEN WINTERIZED IF NO, CALL MSI.\\nARE THERE ANY BOARDED WINDOWS IF YES, HOW MANY IS THERE A SUMP PUMP ONSITE IF YES, HOW MANY\\nIS THERE ANY BROKEN GLASS IS THE SUMP PUMP OPERATIONAL\\n\\nARE THERE ANY UNSECURE OPENINGS I.E. CRAWLSPACES, HOLES IN IF NO, REPLACE WITHIN ALLOWABLE OR CALL FROM SITE FOR IMMEDIATE\\nFASCIA, DRYER VENTS, OR ANYTHING THAT CAN LEAVE PROPERTY EXPOSED APPROVAL.\\nTO ELEMENTS IF YES, CALL FROM SITE FOR IMMEDIATE APPROVAL TO\\nSECURE. IS THERE WATER IN THE BASEMENT\\n\\nADVISE ON THE LOCATION IS PLUMBING IN WORKING CONDITION\\n\\nDESCRIBE- INCLUDE DIMENSIONS:\\n\\nARE THERE ANY SHEDS, OUTBUILDINGS, DETACHED GARAGES AND ARE ARE THERE ANY PLUMBING LINES THAT ARE BROKEN, SEVERED, OR NEED\\nTHEY SECURE CAPPING IF YES, WHERE\\nDAMAGES\\nPROVIDE KEY CODES\\n\\nIS THERE ANY DEBRIS IN THESE STRUCTURES ARE THERE ANY REPAIRS NECESSARY TO ADEQUATELY PROTECT AND\\nIS THERE A POOL, HOT TUB OR WATER FEATURE PRESERVE THE PROPERTY IF YES, DESCRIBE.\\nIF YES, ARE GATES SECURED BY PADLOCKS\\nABOVE OR IN GROUND POOL ARE THERE ANY VIOLATIONS POSTED\\n\\nIF YES, PROVIDE CLEAR LEGIBLE PHOTOS TO JUSTIFY\\n\\nIS THERE ANY STRUCTURAL DAMAGE\\n\\nIF ABOVE, WHAT IS THE CONDITION ARE THERE ANY CRACKS OR DAMAGES TO SHEETROCK OR EXTERIOR\\nIF ABOVE, IS THERE A DECK SURROUNDING CRACKS FROM FOUNDATION ISSUES\\n\\nARE THERE ANY SAFETY HAZARDS IF YES, DESCRIBE.\\n\\nIS THE POOL MAINTAINED ARE THERE ANY TRIP HAZARDS I.E LOOSE TILES, FLOOR VENTS. IF YES,\\nARE THERE ANY DAMAGES TO THE FENCE DESCRIBE.\\n\\nARE THERE ANY ACTIVE ROOF LEAKS IF YES,WHAT IS THE CAUSE\\n\\nIF YES, NOTE DAMAGES AND BID ONLY ADDRESSING PICKET OR PANELS IF YES, CHECK ROOF AND CEILINGS THEN BID.\\nIN NEED OF REPAIR. ARE THERE ANY DAMAGES TO THE ROOF EVEN IF THERE IS NO LEAK IF\\nYES, DESCRIBE.\\nIS PROPERTY FOR SALE IF YES, PROVIDE REALTOR NAME AND PHONE\\nNUMBER. WAS THE ROOF TARPED ON ARRIVAL\\nUTILITIES\\n\\nWHICH UTILITIES ARE ON: ELECTRIC GAS WATER HEAT WERE YOU ABLE TO PROACTIVELY FIX OR TARP ROOF TO PREVENT\\nWHAT IS POWERING THE HEAT SOURCE ADDITIONAL DAMAGE IF YES, DESCRIBE.\\nDOES IT APPEAR TO BE OPERABLE ARE THERE ANY DAMAGES TO THE SIDING, SOFFIT AND\\/OR FASCIA\\nIF HEAT LEFT ON, WHAT IS THERMOSTAT SET TO ARE THERE ANY DAMAGES TO THE SHEDS AND\\/OR OUTBUILDINGS\\nIS BREAKER BOX LEFT IN THE ON POSITION ARE THERE MISSING HANDRAILS OR MISSING STEPS TO A 3FT DROP\\nIF UTILITIES ARE OFF, HAVE ATTEMPTS BEEN MADE TO RESTORE IS THERE ANY EVIDENCE OF MOLD OR MILDEW\\nHAS A BID BEEN SUBMITTED TO ADDRESS MOLD AND ITS SOURCE\\nDAMAGES (CONT'D) PERSONAL PROPERTY\\/DEBRIS\\n\\nARE THERE MISSING HANDRAILS OR MISSING STEPS TO A 3FT DROP IS THERE ANY PERSONAL PROPERTY IF YES HOW MANY CUYDS\\nIS THERE ANY EVIDENCE OF MOLD OR MILDEW LIST ALL AND PROVIDE PHOTOS.\\n\\nHAS A BID BEEN SUBMITTED TO ADDRESS MOLD AND ITS SOURCE IS THERE ANY STRUCTURE CONCRETED OR OTHERWISE AFFIXED TO THE\\nPROPERTY OR ON THE GROUNDS OF THE PROPERTY IF YES, DESCRIB\\n\\nARE THERE ANY EXPOSED ELECTRICAL WIRES, OUTLETS, OR SWITCHES WERE ALL CABINETS, DRAWERS, APPLIANCES, MEDICINE CABINETS,\\nPROVIDE DETAILS AFTER RESOLVING ATTICS, CRAWL SPACE, SPACE UNDER PORCHES & DECKS CHECKED FOR\\nARE THERE ANY GAS LINES THAT NEED TO BE CAPPED DEBRIS\\nIF YES, THEN CALL FROM SITE FOR IMMEDIATE APPROVAL TO CAP.\\nARE ANY DAMAGES CAUSED BY: IS THERE INTERIOR DEBRIS IF YES HOW MANY CUYDS\\nBOILER EXPLOSION\\nEARTHQUAKE IS THERE EXTERIOR DEBRIS IF YES HOW MANY CUYDS\\nFIRE\\nARE THERE WINDOW COVERINGS\\n\\nARE THERE ANY HEALTH HAZARDS IF YES, DESCRIBE.\\n\\nIS THERE ANY INFESTATIONS\\n\\nARE THERE ANY VEHICLES ON SITE\\nAPPLIANCES\\n\\nFLOOD PLEASE STATE IF THE FOLLOWING APPLIANCES AREP RESENT AND .\\nHURRICANE .OPERATIONAL OR NON-OPERATIONAL OR M ISSING.\\nTHEFT STOVE\\nTORNADO\\nVANDALISM REFRIGERATOR\\nWATER\\nOTHER MICROWAVE\\nIF ANY ABOVE ANSWERED YES DESCRIBE.\\nYARD MAINTAINENCE RANGE HOOD\\n\\nDISHWASHER\\n\\nWASHING MACHINE\\n\\nDRYER\\nWATER HEATER\\n\\nIS THE GRASS MORE THAN 2\\\" HIGH HVAC EXTERIOR\\n\\nIF YES, CUT THE LAWN WITHIN ALLOWABLE OR CALL FROM SITE FOR HVAC INTERIOR\\nIMMEDIATE APPROVAL. FURNACE\\n\\nARE THERE ANY SHRUBS OR LIMBS TOUCHING THE HOUSE OR ROOF\\n\\nIF YES, TRIM WITHIN ALLOWABLE OR CALL FROM SITE FOR IMMEDIATE\\nAPPROVAL.\\n\\nIS THERE SNOW ON THE GROUND\\n\\nIF YES, DOES SIDEWALK AND DRIVEWAY NEED TO BE SHOVELED\\nCONDITION\\n\\nIS PROPERTY IN BROOM SWEPT CONDITION GOOD________ FAIR________ POOR________ DAMAGED_______\\nIS PROPERTY IN CONVEYANCE CONDITION GOOD________ FAIR________ POOR________ DAMAGED_______\\nHAS THE MSI SECURING SIGN BEEN POSTED\\nINTERIOR PROPERTY CONDITION\\nEXTERIOR PROPERTY CONDITION\\n\\nCOMMENTS:\\n\\nLEGEND: Y-YES N-NO R-RESOLVED.\\n- MUST HAVE PHOTOS OF ALL CATEGORIES.\\n- HAVE TO HAVE PHOTO TO JUSTIFY.\\nSALE DATE: LOAN \\/ INVESTOR TYPE: CONV \\/ OTHER\\n\\nCONDITION\\n\\nCONDITION: PNPORDER_CONDITION ESTIMATED DAMAGE AMOUNT: PNPORDER_ESTIMATEOFDA\\nARE THERE DAMAGES: PNPORDER_ISDAMAGED\\n\\nSECURITY DATE COMPLETED: YES NO COMMENTS\\n\\nWINDOWS BROKEN - HOW MANY\\n\\nROOF LEAKS\\n\\nVANDALISM (IF YES, DESCRIBE IN DETAIL)\\n\\nFIRE DAMAGE\\n\\nWATER DAMAGE\\n\\nSWIMMING POOL\\n\\nARE POOL & POOL AREA SECURE TO AREA SPECS\\n\\nFOR SALE SIGN\\n\\nIF YES, WAS THIS PREVIOUSLY REPORTED\\n\\nPHOTOS ATTACHED\\n\\nLOCKS KEYED TO: HOW MANY\\n\\nPADLOCK #: HOW MANY\\n\\nKEYS ENCLOSED\\n\\nWINTERIZATION DATE COMPLETED: YES NO COMMENTS\\n\\nUTILITIES OFF\\nWATER OFF \\/ DISCONNECTED\\nEVIDENCE OF BURST PIPES, JOINTS, FIXTURES\\nEVIDENCE OF FREEZE DAMAGE\\nALL FIXTURES DRAINES\\nALL PIPES BLOWN OUT\\nBOILER \\/ HEATING SYSTEM DRAINED\\nANTI-FREEZE INSTALED IN ALL TRAPS \\/ FIXTURES\\nTAG\\/STICKER W\\/DATE OF WINTERIZATION AFFIXED\\nSUMP PUMP ON SITE\\n\\nMOBILE HOME INFORMATION YES NO COMMENTS DATE COMPLETED:\\n\\nMOBILE HOME YEAR\\nMOBILE HOME MAKE\\nMOBILE HOME MODEL\\nMOBILE HOME LENGTH\\nMOBILE HOME WIDTH\\nMOBILE HOME SIZE\\nMOBILE HOME VIN NUMBER\\nMOBILE HOME HUD TAG NUMBER\\nMOBILE HOME SERIAL NUMBER\\nTONGUE, WHEELS, AXLES ATTACHED\\nMOBILE HOME ATTACHED TO FOUNDATION\\n\\nMOBILE HOME SKIRTED\\nEXTERIOR CONDITION OF PROPERTY:\\n\\nLIVING ROOM GOOD FAIR POOR MISSING COMMENTS\\nGOOD FAIR POOR MISSING COMMENTS\\nITEM GOOD FAIR POOR MISSING COMMENTS\\nWALL PANELS GOOD FAIR POOR MISSING COMMENTS\\nFLOOR COVERING\\nSUB FLOOR\\nCEILING\\nWINDOW GLASS\\n\\nDINING ROOM\\n\\nITEM\\nWALL PANELS\\nFLOOR COVERING\\nSUB FLOOR\\nCEILING\\nWINDOW GLASS\\n\\nBEDROOMS\\n\\nITEM\\nWALL PANELS\\nFLOOR COVERING\\nSUB FLOOR\\nCEILING\\nWINDOW GLASS\\n\\nBATHROOMS\\n\\nITEM\\nWALL PANELS\\nFLOOR COVERING\\nSUB FLOOR\\nCEILING\\nWINDOW GLASS\\nKITCHEN \\/ UTILITY ROOM\\n\\nITEM GOOD FAIR POOR MISSING COMMENTS\\n\\nSINK\\nCABINETS\\nWALL PANELS\\nFLOOR COVERING\\nSUB FLOORING\\nCEILING\\nWINDOW GLASS\\nDISHWASHER\\nCLOTHES WASHER\\nCLOTHES DRYER\\n\\nAPPLIANCES\\n\\nITEM GOOD FAIR POOR MISSING COMMENTS\\n\\nFURN \\/ H. PUMP\\nAIR CONDITIONER\\nWATER HEATER\\nRANGE\\nREFRIGERATOR\\n\\nNAME OF PREPARER:\\nDATE:\",\"Source\":\"Import\"},{\"Description\":\"1.)\",\"Qty\":\"1\",\"Price\":\"0.00\",\"Total\":\"0.00\",\"AdditionalInstructions\":\"BOARD 17 BROKEN WINDOWS. MEASUREMENTS ARE UNITED INCHES. WILL REQUIRE PLYWOOD, BOARDS,\\nBOLTS, AND BASIC TOOLS\\n52X34,52X34,41X33,32X21,22X33,33X22,21X33,52X34,52X34,52X34,52X34,52X34,52X34,52X34,52X34,52X34 1323UI\\nPER UI,\",\"Source\":\"Import - Modified\"},{\"Description\":\"2.)\",\"Qty\":\"1\",\"Price\":\"0.00\",\"Total\":\"0.00\",\"AdditionalInstructions\":\"INSTALL PADLOCK AND HASP ON THE OUTBUILDING\",\"Source\":\"Import - Modified\"},{\"Description\":\"3.)\",\"Qty\":\"1\",\"Price\":\"0.00\",\"Total\":\"0.00\",\"AdditionalInstructions\":\"REMOVE 1 CYD FECES FROM THE TOILET\",\"Source\":\"Import - Modified\"},{\"Description\":\"4.)\",\"Qty\":\"1\",\"Price\":\"0.00\",\"Total\":\"0.00\",\"AdditionalInstructions\":\"REMOVE 1 CYD FECES FROM THE BATHTUB\",\"Source\":\"Import - Modified\"},{\"Description\":\"5.)\",\"Qty\":\"1\",\"Price\":\"0.00\",\"Total\":\"0.00\",\"AdditionalInstructions\":\"CLEAN HEAVILY SOILED TOILET\\/ CLEAN WITH CLOROX OR SOMETHING EQUIVALENT. WILL REQUIRE\\nCLEANING TOOLS, 2 GALLONS OF BLEACH\",\"Source\":\"Import - Modified\"},{\"Description\":\"6.)\",\"Qty\":\"1\",\"Price\":\"0.00\",\"Total\":\"0.00\",\"AdditionalInstructions\":\"CLEAN AND SANITIZE 1 DIRTY SHOWER IN THE BATHROOM WITH BLEACH AND SCRUB CLEAN. WILL\\nREQUIRE CLEANING TOOLS, 2 GALLONS OF BLEACH\",\"Source\":\"Import - Modified\"},{\"Description\":\"7.)\",\"Qty\":\"1\",\"Price\":\"0.00\",\"Total\":\"0.00\",\"AdditionalInstructions\":\"REMOVE 1 CYD OF RAW FOOD FROM THE KTICHEN\",\"Source\":\"Import - Modified\"},{\"Description\":\"8.)\",\"Qty\":\"1\",\"Price\":\"0.00\",\"Total\":\"0.00\",\"AdditionalInstructions\":\"REMOVE 4 CYDS OF EXTERIOR DEBRIS. CONTENTS ARE: PLASTIC, CLOTHING, TRASH, MISC. PER CYD,\",\"Source\":\"Import - Modified\"},{\"Description\":\"9.)\",\"Qty\":\"1\",\"Price\":\"0.00\",\"Total\":\"0.00\",\"AdditionalInstructions\":\"TOTAL\",\"Source\":\"Import - Modified\"},{\"Description\":\"\",\"Qty\":\"\",\"Price\":\" \",\"Total\":\"0.00\",\"AdditionalInstructions\":\"\",\"Source\":\"\"}],\"complete_date\":\"\",\"comments\":\"1. BOARD 17 BROKEN WINDOWS. MEASUREMENTS ARE UNITED INCHES. WILL REQUIRE PLYWOOD, BOARDS, BOLTS, AND BASIC TOOLS 52X34,52X34,41X33,32X21,22X33,33X22,21X33,52X34,52X34,52X34,52X34,52X34,52X34,52X34,52X34,52X34 1323UI PER UI.\\n2. INSTALL PADLOCK AND HASP ON THE OUTBUILDING\\n3. REMOVE 1 CYD FECES FROM THE TOILET\\n4. REMOVE 1 CYD FECES FROM THE BATHTUB\\n5. CLEAN HEAVILY SOILED TOILET\\/ CLEAN WITH CLOROX OR SOMETHING EQUIVALENT. WILL REQUIRE CLEANING TOOLS, 2 GALLONS OF BLEACH.\\n6. CLEAN AND SANITIZE 1 DIRTY SHOWER IN THE BATHROOM WITH BLEACH AND SCRUB CLEAN. WILL REQUIRE CLEANING TOOLS, 2 GALLONS OF BLEACH.\\n7. REMOVE 1 CYD OF RAW FOOD FROM THE KTICHEN\\n8. REMOVE 4 CYDS OF EXTERIOR DEBRIS. CONTENTS ARE: PLASTIC, CLOTHING, TRASH, MISC.\\n9. PROVIDE PHOTOS TO SUPPORT WORK COMPLETED\\/MEASUREMENT PHOTOS\\n10. PROVIDE DUMP RECEIPT\\n \\n \\n\\nIf you have any questions please call or text Belinda at 8167869413\",\"client_Due_date\":\"January 15, 2021\",\"username\":\"Willadmin2\",\"follow_up_complete\":\"no\",\"broker_info\":\"\",\"lot_size\":\"5,318\",\"received_date\":\"January 8, 2021\",\"customer\":\"WF01\",\"client_company\":\"MSI\",\"freeze_property\":\"no\",\"ppw\":\"75592\",\"start_date\":\"\",\"contractor\":\"Sims, Kelly\",\"ready_for_office\":\"no\",\"missing_info\":\"No\",\"BATF\":\"No\",\"sub_contractor_follow_up\":\"\",\"work_type\":\"MSI - Post Sale Bid Approval\",\"cancel_date\":\"\",\"is_Inspection\":\"No\",\"assigned_admin\":\"Belinda Belcher\",\"lklg\":\"4477\\n67767\"}67767\"}";
            // string chk = "{\"category\":\"Belinda - 8167869413\",\"wo\":\"8214674\",\"address\":\"1219 NORTH CONCORD\\nSPRINGFIELD, MO 65802\",\"estimated_complete_date\":\"\",\"loan_info\":\"****4477\",\"office_locked\":\"no\",\"due_date\":\"January 30, 2021\",\"ready_for_office_Contractor\":\"no\",\"work_order_item_details\":[{\"Description\":\"LEVEL 2 PROPERTY CONDITION PHOTOS REQUIRED (50-100 pics)\",\"Qty\":\"0\",\"Price\":\"0.00\",\"Total\":\"0.00\",\"AdditionalInstructions\":\"All non-grass cut orders require level 2 at the very minimum.\\n\\nExterior photos - 360 view around house and all outbuildings.\\n\\nPhotos of the roof from the ground.\\n\\nDocument any damages or issues you encounter.\\n\\nPhotos should be large overall views clearly showing the layout of property and buildings.\\n\\nInterior Photos 2013 All ceilings, floors, room pics from door and opposite corner, appliances, inside toilet bowls, furnace, water heater, breaker box, etc%u2026\\n\\nDamages 2013 take pics of any damages that are new to you. Sometimes we will be given properties we have never been to. When in doubt take a pic. You can never take too many photos of a property.\\n\\nThe more stuff we find wrong, the more we can bid.\",\"Source\":\"Auto\"},{\"Description\":\"*IMPORTANT* DO NOT REMOVE UNAUTHORIZED ITEMS\",\"Qty\":\"1\",\"Price\":\"0.00\",\"Total\":\"0.00\",\"AdditionalInstructions\":\"REMOVING ANYTHING FROM A HOUSE WITHOUT BEING AUTHORIZED IS A BIG DEAL!!! ALL THE CONTENTS HAVE BEEN PRE-PHOTOGRAPHED. IF SOMETHING COMES UP MISSING THE CLIENT REQUIRES US TO GIVE THEM YOUR PERSONAL INFORMATION SO THEY CAN PROSECUTE. SO DON'T BE STUPID THERE ARE TOO MANY NOSEY NEIGHBORS, UPSET HOMEOWNERS, INSPECTORS AND OTHER CONTRACTORS JUST DYING TO CALL THE CLIENT AND TELL ON YOU. DON'T BE STUPID. IT'S NOT WORTH IT!!!\",\"Source\":\"Auto\"},{\"Description\":\"Sign In Sheet\",\"Qty\":\"1\",\"Price\":\"0.00\",\"Total\":\"0.00\",\"AdditionalInstructions\":\"always sign in and use the name of the client for company name E.G. Safeguard, MCS or Cyprexx, Etc..\",\"Source\":\"Auto\"},{\"Description\":\"*PPW APP Instructions and tips*\",\"Qty\":\"1\",\"Price\":\"0.00\",\"Total\":\"0.00\",\"AdditionalInstructions\":\"Click the following link to our guide and videos that that will teach you how to use different features in the app. https:\\/\\/drive.google.com\\/open?id=1xjdas6t9E98Zu8Xu7hzUnUR9qpRIXMCX\",\"Source\":\"Auto\"},{\"Description\":\"All Invoices subject to adjustment\",\"Qty\":\"0\",\"Price\":\"0.00\",\"Total\":\"0.00\",\"AdditionalInstructions\":\"All Invoice are subject to adjustment based upon work performed, Photo time stamps, and work quality.\",\"Source\":\"Auto\"},{\"Description\":\"10.)\",\"Qty\":\"0\",\"Price\":\"0.00\",\"Total\":\"0.00\",\"AdditionalInstructions\":\"PROVIDE PHOTOS TO SUPPORT WORK COMPLETED\\/MEASUREMENT PHOTOS\",\"Source\":\"Import\"},{\"Description\":\"11.)\",\"Qty\":\"0\",\"Price\":\"0.00\",\"Total\":\"0.00\",\"AdditionalInstructions\":\"PROVIDE DUMP RECEIPT\",\"Source\":\"Import\"},{\"Description\":\"12.)\",\"Qty\":\"0\",\"Price\":\"0.00\",\"Total\":\"0.00\",\"AdditionalInstructions\":\"\",\"Source\":\"Import\"},{\"Description\":\"INSTRUCTIONS FOR SERVICES TO BE PERFORMED\",\"Qty\":\"0\",\"Price\":\"0.00\",\"Total\":\"0.00\",\"AdditionalInstructions\":\"LOAN #: ****4477 POS REQUIRED: YES\\nSEND TO ASPEN: YES\\nHOA PHONE:\\n\\nPROPERTY INFO INITIAL SECURE DATE: 06\\/29\\/2019 LAST WINTERIZATION DATE:\\nLAST LOCK CHANGE DATE: 11\\/28\\/2019 LAST SNOW REMOVAL DATE:\\nFTV DATE: 11\\/19\\/2020 LOCKBOX: 4477 INITIAL GRASS CUT DATE: 04\\/16\\/2020\\nLAST INSPECTED DATE: 01\\/25\\/2021 KEYCODE: 67767\\/4477 LAST GRASS CUT DATE: 10\\/12\\/2020\\nLAST INSP OCCUPANCY: VACANT SECURITY ACCESS CODE: LAWN SIZE (SQ FT): 5,318\\nPROPERTY TYPE: SINGLE FAMILY\\nCOLOR:\\n\\nIF YOU ARE UNABLE TO COMPLETE THIS JOB AND RETURN IT TO US WITHIN THE SCHEDULED TIMEFRAME\\nPLEASE REJECT ORDER, ADVISE YOUR VM SO WE CAN RE-ASSIGN THIS JOB.\",\"Source\":\"Import\"},{\"Description\":\"KEY \\/ LOCK INSTRUCTIONS\",\"Qty\":\"0\",\"Price\":\"0.00\",\"Total\":\"0.00\",\"AdditionalInstructions\":\"**********WF01 - BID APPROVAL**********\\n. A PROPERTY VALIDATION PHOTO IS PROVIDED AT THE BOTTOM OF THIS WORK ORDER. THIS IS TO ENSURE\\nYOU ARE AT THE CORRECT LOCATION AND PROPERTY FOR ANY AND ALL WORK PERFORMED. YOU MUST\\nVALIDATE YOU ARE AT THE PROVIDED PROPERTY BEFORE ANY WORK IS STARTED. IF THERE ARE ANY\\nQUESTIONS REGARDING PROPERTY VALIDATION, PLEASE CALL MSI FROM SITE FOR DIRECTION AT 1-800-825-\\n4101 (OPTION 9 FOR REGION 9).********DO NOT ENTER PROPERTY UNLESS THERE IS A POST SECURE NOTICE\\nPOSTED UPON ARRIVAL AT PROPERTY. IF THERE IS NO POST SECURE NOTICE, CALL FROM SITE 1-800-825-4101\\n(OPTION 9 FOR REGION 9)**********\\n. PROPERTY MAY ALREADY BE SECURED: LOCKBOX _______4477__________\\/ KEY CODE\\n_______67767____________\\n\\n. IF UNABLE TO ENTER USING THE EXISTING KEY CODE, CALL FROM SITE 1-800-825-4101 (OPTION 9 FOR\\nREGION 9).\\n. IF UNABLE TO GAIN ACCESS DUE TO GATED COMMUNITY CALL FROM SITE 1-800-825-4101 (OPTION 9 FOR\\nREGION 9) (OBTAIN NAME\\/PHONE #\\/FAX # OF HOA, COA, ETC. AS NEEDED).\\n. IF REPORTED AS OCCUPIED, ATTEMPT DIRECT CONTACT AND REPORT OCCUPIED STATUS.\\n67767\\/4477\",\"Source\":\"Import\"},{\"Description\":\"GENERAL INSTRUCTIONS\",\"Qty\":\"0\",\"Price\":\"0.00\",\"Total\":\"0.00\",\"AdditionalInstructions\":\"********** WF01 - BID APPROVAL**********\\nRECEIVED BID APPROVAL FOR THE FOLLOWING: (PLEASE COMPLETE AND INVOICE WITHIN THE APPROVED\\nAMOUNT ONLY).\\n00-825-4101 - OPTION 9 FOR REGION 9 TO REPORT THE VIOLATION.\\n. BIG 6 DAMAGES: REPORT AND PROVIDE PHOTOS FOR ALL BIG 6 DAMAGES AND STRUCTURAL DAMAGE.\",\"Source\":\"Import\"},{\"Description\":\"WINTERIZATION INSTRUCTIONS\",\"Qty\":\"0\",\"Price\":\"0.00\",\"Total\":\"0.00\",\"AdditionalInstructions\":\"PLEASE SEE GENERAL INSTRUCTIONS FOR DIRECTION\",\"Source\":\"Import\"},{\"Description\":\"PHOTO INSTRUCTIONS\",\"Qty\":\"0\",\"Price\":\"0.00\",\"Total\":\"0.00\",\"AdditionalInstructions\":\"********** WF01 - BID APPROVAL**********SUBMIT CLEAR AND DETAILED PHOTOS WITH DATE STAMPS, AND LABEL\\nBY ROOM AND LOCATION**********\\n**BEST PRACTICES TO CONSIDER: PHOTOS OF WHITEBOARD OR PAPER WITH #1, #2 TO SUPPORT THE\\nNUMBER OF TRUCK LOADS, BAGS OF DEBRIS, NUMBER OF TREES, ETC.; PHOTOS OF A TAPE MEASURE\\nSHOWING THE SIZE OF TRUCK TRAILER, AREA OF MOLD REMEDIATED, HEIGHT OF GRASS, ETC**\\nPHOTOS SHOULD INCLUDE BUT ARE NOT LIMITED TO THE FOLLOWING:\\n. PROPERTY ADDRESS AND STREET SIGN (REQUIRED)\\n. PHOTO OF STREET SCENE\\n. SECURE POSTING (REQUIRED) - MUST BE CLEAR AND ABLE TO BE READ. (REQUIRED IN ORDER TO RECEIVE\\nPAYMENT)\\n. LOCK CHANGE: BEFORE, DURING, AND AFTER PHOTOS ARE REQUIRED OF THE LOCK CHANGE PROCESS (IF\\nAPPLICABLE).\\n. DAMAGES AND REPAIRS: PHOTOS MUST SUPPORT THE DESCRIPTION, LOCATION, MEASUREMENTS, AND\\nMATERIALS NEEDED AS LISTED IN BIDS (INCLUDING MOLD DAMAGES).\\n. MOLD: IF MOLD DAMAGES ARE REPORTED, CLEAR PHOTOS USING TAPE MEASURE, PAINTERS TAPE, OR\\nCHALK SHOWING DIMENSIONS ARE REQUIRED.\\n. ROOF AND CEILING: PHOTOS OF EXTERIOR ROOF AND INTERIOR CEILINGS OF EVERY ROOM REQUIRED\\nREGARDLESS OF CONDITION (MUST SHOW DAMAGES IF DAMAGES ARE REPORTED).\\n. EXTERIOR REQUIREMENTS: EACH SIDE OF PROPERTY FROM THE SAME ANGLE (FRONT, REAR, AND SIDES).\\nPHOTOS OF ALL EXTERIOR STRUCTURES REQUIRED. PHOTOS SHOULD ALSO BE TAKEN FROM THE HOUSE\\nOUT TOWARD THE PROPERTY LINE. IF THERE IS A PRIVACY FENCE, PHOTOS SHOULD BE TAKEN ON BOTH\\nSIDES OF FENCE IF POSSIBLE.\\n. INTERIOR REQUIREMENTS: PHOTOS (WIDE ANGLE) OF EVERY ROOM, INCLUDING: CLOSETS, CEILINGS AND\\nFLOORS, CORNERS, ATTICS, BASEMENTS, OPENED DRAWERS AND CABINETS, ALL APPLIANCES, ELECTRICAL\\nPANELS, STAIRWAYS, HALLWAYS AND FIREPLACES.\\n. PERSONAL PROPERTY: MUST HAVE CLEAR PHOTOS OF ALL INTERIOR AND EXTERIOR PERSONALS TO\\nSUPPORT CONDITION OF PROPERTY OR WORK COMPLETED. PROVIDE PHOTOS OF ALL ENTRANCES THAT\\nHAVE PERSONAL PROPERTY POSTINGS AND NOTICE OF RIGHT TO RECLAIM POSTINGS. MUST PROVIDE\\nPHOTOS TO SUPPORT THE NUMBER OF TRUCK LOADS, BAGS OF PERSONALS AND PHOTOS OF TAPE\\nMEASUREMENTS SHOWING THE SIZE OF TRUCK TRAILER ETC.\\n\\n. STORAGE OF PERSONAL PROPERTY: BEFORE, DURING AND AFTER PHOTOS SHOWING THE STORAGE UNIT\\nWITH THE PERSONAL PROPERTY INSIDE. AFTER PHOTOS ALSO MUST SUPPORT THE STORAGE UNIT WITH THE\\nPERSONAL PROPERTY INSIDE AND THE UNIT IS CLOSED AND LOCKED. MUST PROVIDE A COPY OF STORAGE\\nAGREEMENT\\/RECEIPT.\\n. DEBRIS: MUST HAVE CLEAR PHOTOS OF ALL INTERIOR AND EXTERIOR DEBRIS TO SUPPORT CONDITION OF\\nPROPERTY AND WORK COMPLETED. MUST PROVIDE PHOTOS TO SUPPORT THE NUMBER OF TRUCK LOADS,\\nBAGS OF PERSONALS AND PHOTOS OF TAPE MEASUREMENTS SHOWING THE SIZE OF TRUCK TRAILER ETC.\\n. MULTI UNIT: INCLUDE PHOTOS OF COMMON AREAS SUCH AS STAIRWELLS, HALLWAYS, LOBBIES, ETC. AND\\nLABEL PHOTOS WITH UNIT NUMBER.\\n. WORK COMPLETED: BEFORE, DURING, AND AFTER PHOTOS (WIDE ANGLE) ARE REQUIRED OF ALL WORK\\nCOMPLETED, INCLUDING TRASH AND DEBRIS REMOVAL (TRUCK EMPTY AND FULL, ROOMS FULL AND EMPTY,\\nETC.)\\n. BIDS: PHOTOS MUST SUPPORT THE DESCRIPTION, LOCATION, MEASUREMENTS, AND MATERIALS NEEDED AS\\nLISTED IN BIDS.\\n. APPLIANCES: BEFORE, DURING, AND AFTER PHOTOS REQUIRED OF ALL FIXTURES, INCLUDING ICE MAKERS,\\nDISH WASHERS, WATER HEATER, BLOWING LINES, TURNING OFF BREAKERS, SUMP PUMPS, GENERATOR\\nHOOKED TO COMPRESSOR, GAUGES OF PRESSURE TESTING (AT 35 PSI), ETC.\\n. ELECTRIC METER: MUST BE ABLE TO READ DETAILS ON METER.\\n. MOBILE HOMES: MUST PROVIDE CLEAR PHOTOS OF DATA PLATE INCLUDING VIN AND SERIAL NUMBERS.\\n.**********WINTERIZATION SECTION BELOW**********\\n. BEFORE, DURING, AND AFTER PHOTOS FROM THE SAME ANGLE, OF THE ENTIRE WINTERIZATION PROCESS\\nARE REQUIRED.\\n. FROZEN PROPERTY: PHOTOS MUST BE PROVIDED THAT CONFIRM THE PROPERTY IS FROZEN (TANK FROZEN,\\nLINES BURST AND FROSTED, ETC.)\\n. BREAKERS: PHOTOS OF BREAKERS TURNED TO OFF POSITION, UNLESS SUMP PUMP OR DEHUMIDIFIER IS\\nPRESENT.\\n. DRAINING: PHOTOS MUST SHOW THE WATER COMING OUT OF A HOSE ATTACHED TO THE HOT WATER\\nHEATER AND DRAINING TO THE EXTERIOR OF THE PROPERTY.\\n. PLUMBING: PHOTOS TO CONFIRM WATER IS BLOWN OUT OF PIPES AND THAT THE TOILET WATER HAS ALSO\\nBEEN DRAINED BOTH IN THE TANK AND IN THE BOWL.\\n. TOILETS: IF TOILETS ARE REPORTED DIRTY AND CANNOT COME CLEAN, ACTION SHOTS OF CLEANING ARE\\nREQUIRED.\\n. PRESSURE GAUGE: PHOTOS OF THE PRESSURE GAUGE ARE REQUIRED ON EVERY WINTERIZATION. THE\\nPRESSURE TEST IS REQUIRED TO HOLD 35 PSI FOR 20 MINUTES.\\n. NON-TOXIC ANTIFREEZE: PHOTOS OF EVERY TRAP HAVING NON-TOXIC ANTIFREEZE POURED IN THEM MUST\\nBE PROVIDED. THE TRAPS INCLUDE, BUT ARE NOT LIMITED TO: TOILET BOWLS AND TOILET TANKS,\\nDISHWASHER DRAINS, ALL SINKS, SHOWER\\/TUB DRAINS, WASHING MACHINE DRAIN, ANY EXTERIOR\\nAPPLIANCE ATTACHED TO INTERIOR PLUMBING (HOT TUBS), ETC.\\n. WINTERIZATION STICKERS: PHOTOS OF ALL STICKERS PLACED WITH WELLS FARGO CONTACT INFORMATION.\\n\\n***** IN CASES OF VANDALISM OR THEFT, REPORT TO THE POLICE AN ESTIMATE OF *****\\n***** DAMAGES AND PROVIDE THE CASE NUMBER AND OFFICER'S NAME TO MSI *****\\nPOINT OF SERVICE QUESTIONS 1.YES 2. NO 3. N\\/A\\n1. HAS A CODE VIOLATION OR OFFICIAL NOTICE BEEN POSTED AT THE PROPERTY 1.YES 2. NO 3. N\\/A\\n2. EXTERIOR DEBRIS, JUNK, TRASH OR DEAD VEGETATION FOUND AT THE PROPERTY\\n3. IS THE LAWN LANDSCAPED AND MAINTAINED 1.YES 2. NO 3. N\\/A\\n4. ANY OBSERVED ESCALATED EVENTS OR ISSUES AT THE PROPERTY\\n5. ANY UNSECURED OPENINGS,BROKEN WINDOWS OR DOORS AT THE PROPERTY 1.YES 2. NO 3. N\\/A\\n6. IS THERE ROOF DAMAGE THAT NEEDS REPAIR 1.YES 2. NO 3. N\\/A\\n7. IS THE PROPERTY VACANT\\n8. DOES THE PROPERTY HAVE INDICATIONS OF VANDALISM OR AN OBVIOUS CRIME SCENE 1.YES 2. NO 3. UNKNOWN\\n9. IS THERE ANY OBVIOUS HEALTH AND SAFETY ISSUE AT THE PROPERTY\\n10. DOES THE FOLLOWING DAMAGE EXIST AT THE PROPERTY THAT MEETS THE BIG SIX DEFINITION: FIRE, FLOOD, TORNADO, HURRICANE 1.YES 2. NO 3. N\\/A\\nBOILER(MULTI-UNIT), EARTHQUAKE 1.YES 2. NO 3. N\\/A\\n\\nASPEN ABC NUMBER: 1.YES 2. NO 3. N\\/A\\nPROPERTY AND PRESERVATION CHECKLIST\\n\\nPROPERTY INFORMATION\\n\\nPROPERTY ADDRESS:_________________________________UNIT #______CITY:______________ STATE: __ ZIP:_____\\nPROPERTY TYPE: SFR__ TOWN HOME__ MULTI-UNIT__CONDO__MOBILE HOME*__ MANF HOME__ # OF UNITS:__\\n*VIN#\\/SERIAL #:__________________ IS PROPERTY IN HIGH VANDAL AREA YES__ NO___\\n\\nSECURING Y N R WINTERIZATION YNR\\n\\nARE ALL EXTERIOR DOORS SECURED HAS PROPERTY BEEN WINTERIZED\\nKEY CODE IF YES, WHEN, BY WHOM, AND WHAT TYPE\\nIS GARAGE DOOR OPENER DIABLED IF NO, CALL FROM SITE FOR IMMEDIATE APPROVAL\\nIS OVERHEAD GARAGE DOOR SECURED IS WINTERIZATION INTACT\\nIS SLIDING GLASS DOOR SECURED ARE THE TOILETS CLEAN\\nARE ALL WINDOWS SECURED ARE WINTERIZATION STICKERS POSTED\\nWHAT IS UNSECURE IF POOL, HAS IT BEEN WINTERIZED IF NO, CALL MSI.\\nARE THERE ANY BOARDED WINDOWS IF YES, HOW MANY IS THERE A SUMP PUMP ONSITE IF YES, HOW MANY\\nIS THERE ANY BROKEN GLASS IS THE SUMP PUMP OPERATIONAL\\n\\nARE THERE ANY UNSECURE OPENINGS I.E. CRAWLSPACES, HOLES IN IF NO, REPLACE WITHIN ALLOWABLE OR CALL FROM SITE FOR IMMEDIATE\\nFASCIA, DRYER VENTS, OR ANYTHING THAT CAN LEAVE PROPERTY EXPOSED APPROVAL.\\nTO ELEMENTS IF YES, CALL FROM SITE FOR IMMEDIATE APPROVAL TO\\nSECURE. IS THERE WATER IN THE BASEMENT\\n\\nADVISE ON THE LOCATION IS PLUMBING IN WORKING CONDITION\\n\\nDESCRIBE- INCLUDE DIMENSIONS:\\n\\nARE THERE ANY SHEDS, OUTBUILDINGS, DETACHED GARAGES AND ARE ARE THERE ANY PLUMBING LINES THAT ARE BROKEN, SEVERED, OR NEED\\nTHEY SECURE CAPPING IF YES, WHERE\\nDAMAGES\\nPROVIDE KEY CODES\\n\\nIS THERE ANY DEBRIS IN THESE STRUCTURES ARE THERE ANY REPAIRS NECESSARY TO ADEQUATELY PROTECT AND\\nIS THERE A POOL, HOT TUB OR WATER FEATURE PRESERVE THE PROPERTY IF YES, DESCRIBE.\\nIF YES, ARE GATES SECURED BY PADLOCKS\\nABOVE OR IN GROUND POOL ARE THERE ANY VIOLATIONS POSTED\\n\\nIF YES, PROVIDE CLEAR LEGIBLE PHOTOS TO JUSTIFY\\n\\nIS THERE ANY STRUCTURAL DAMAGE\\n\\nIF ABOVE, WHAT IS THE CONDITION ARE THERE ANY CRACKS OR DAMAGES TO SHEETROCK OR EXTERIOR\\nIF ABOVE, IS THERE A DECK SURROUNDING CRACKS FROM FOUNDATION ISSUES\\n\\nARE THERE ANY SAFETY HAZARDS IF YES, DESCRIBE.\\n\\nIS THE POOL MAINTAINED ARE THERE ANY TRIP HAZARDS I.E LOOSE TILES, FLOOR VENTS. IF YES,\\nARE THERE ANY DAMAGES TO THE FENCE DESCRIBE.\\n\\nARE THERE ANY ACTIVE ROOF LEAKS IF YES,WHAT IS THE CAUSE\\n\\nIF YES, NOTE DAMAGES AND BID ONLY ADDRESSING PICKET OR PANELS IF YES, CHECK ROOF AND CEILINGS THEN BID.\\nIN NEED OF REPAIR. ARE THERE ANY DAMAGES TO THE ROOF EVEN IF THERE IS NO LEAK IF\\nYES, DESCRIBE.\\nIS PROPERTY FOR SALE IF YES, PROVIDE REALTOR NAME AND PHONE\\nNUMBER. WAS THE ROOF TARPED ON ARRIVAL\\nUTILITIES\\n\\nWHICH UTILITIES ARE ON: ELECTRIC GAS WATER HEAT WERE YOU ABLE TO PROACTIVELY FIX OR TARP ROOF TO PREVENT\\nWHAT IS POWERING THE HEAT SOURCE ADDITIONAL DAMAGE IF YES, DESCRIBE.\\nDOES IT APPEAR TO BE OPERABLE ARE THERE ANY DAMAGES TO THE SIDING, SOFFIT AND\\/OR FASCIA\\nIF HEAT LEFT ON, WHAT IS THERMOSTAT SET TO ARE THERE ANY DAMAGES TO THE SHEDS AND\\/OR OUTBUILDINGS\\nIS BREAKER BOX LEFT IN THE ON POSITION ARE THERE MISSING HANDRAILS OR MISSING STEPS TO A 3FT DROP\\nIF UTILITIES ARE OFF, HAVE ATTEMPTS BEEN MADE TO RESTORE IS THERE ANY EVIDENCE OF MOLD OR MILDEW\\nHAS A BID BEEN SUBMITTED TO ADDRESS MOLD AND ITS SOURCE\\nDAMAGES (CONT'D) PERSONAL PROPERTY\\/DEBRIS\\n\\nARE THERE MISSING HANDRAILS OR MISSING STEPS TO A 3FT DROP IS THERE ANY PERSONAL PROPERTY IF YES HOW MANY CUYDS\\nIS THERE ANY EVIDENCE OF MOLD OR MILDEW LIST ALL AND PROVIDE PHOTOS.\\n\\nHAS A BID BEEN SUBMITTED TO ADDRESS MOLD AND ITS SOURCE IS THERE ANY STRUCTURE CONCRETED OR OTHERWISE AFFIXED TO THE\\nPROPERTY OR ON THE GROUNDS OF THE PROPERTY IF YES, DESCRIB\\n\\nARE THERE ANY EXPOSED ELECTRICAL WIRES, OUTLETS, OR SWITCHES WERE ALL CABINETS, DRAWERS, APPLIANCES, MEDICINE CABINETS,\\nPROVIDE DETAILS AFTER RESOLVING ATTICS, CRAWL SPACE, SPACE UNDER PORCHES & DECKS CHECKED FOR\\nARE THERE ANY GAS LINES THAT NEED TO BE CAPPED DEBRIS\\nIF YES, THEN CALL FROM SITE FOR IMMEDIATE APPROVAL TO CAP.\\nARE ANY DAMAGES CAUSED BY: IS THERE INTERIOR DEBRIS IF YES HOW MANY CUYDS\\nBOILER EXPLOSION\\nEARTHQUAKE IS THERE EXTERIOR DEBRIS IF YES HOW MANY CUYDS\\nFIRE\\nARE THERE WINDOW COVERINGS\\n\\nARE THERE ANY HEALTH HAZARDS IF YES, DESCRIBE.\\n\\nIS THERE ANY INFESTATIONS\\n\\nARE THERE ANY VEHICLES ON SITE\\nAPPLIANCES\\n\\nFLOOD PLEASE STATE IF THE FOLLOWING APPLIANCES AREP RESENT AND .\\nHURRICANE .OPERATIONAL OR NON-OPERATIONAL OR M ISSING.\\nTHEFT STOVE\\nTORNADO\\nVANDALISM REFRIGERATOR\\nWATER\\nOTHER MICROWAVE\\nIF ANY ABOVE ANSWERED YES DESCRIBE.\\nYARD MAINTAINENCE RANGE HOOD\\n\\nDISHWASHER\\n\\nWASHING MACHINE\\n\\nDRYER\\nWATER HEATER\\n\\nIS THE GRASS MORE THAN 2\\\" HIGH HVAC EXTERIOR\\n\\nIF YES, CUT THE LAWN WITHIN ALLOWABLE OR CALL FROM SITE FOR HVAC INTERIOR\\nIMMEDIATE APPROVAL. FURNACE\\n\\nARE THERE ANY SHRUBS OR LIMBS TOUCHING THE HOUSE OR ROOF\\n\\nIF YES, TRIM WITHIN ALLOWABLE OR CALL FROM SITE FOR IMMEDIATE\\nAPPROVAL.\\n\\nIS THERE SNOW ON THE GROUND\\n\\nIF YES, DOES SIDEWALK AND DRIVEWAY NEED TO BE SHOVELED\\nCONDITION\\n\\nIS PROPERTY IN BROOM SWEPT CONDITION GOOD________ FAIR________ POOR________ DAMAGED_______\\nIS PROPERTY IN CONVEYANCE CONDITION GOOD________ FAIR________ POOR________ DAMAGED_______\\nHAS THE MSI SECURING SIGN BEEN POSTED\\nINTERIOR PROPERTY CONDITION\\nEXTERIOR PROPERTY CONDITION\\n\\nCOMMENTS:\\n\\nLEGEND: Y-YES N-NO R-RESOLVED.\\n- MUST HAVE PHOTOS OF ALL CATEGORIES.\\n- HAVE TO HAVE PHOTO TO JUSTIFY.\\nSALE DATE: LOAN \\/ INVESTOR TYPE: CONV \\/ OTHER\\n\\nCONDITION\\n\\nCONDITION: PNPORDER_CONDITION ESTIMATED DAMAGE AMOUNT: PNPORDER_ESTIMATEOFDA\\nARE THERE DAMAGES: PNPORDER_ISDAMAGED\\n\\nSECURITY DATE COMPLETED: YES NO COMMENTS\\n\\nWINDOWS BROKEN - HOW MANY\\n\\nROOF LEAKS\\n\\nVANDALISM (IF YES, DESCRIBE IN DETAIL)\\n\\nFIRE DAMAGE\\n\\nWATER DAMAGE\\n\\nSWIMMING POOL\\n\\nARE POOL & POOL AREA SECURE TO AREA SPECS\\n\\nFOR SALE SIGN\\n\\nIF YES, WAS THIS PREVIOUSLY REPORTED\\n\\nPHOTOS ATTACHED\\n\\nLOCKS KEYED TO: HOW MANY\\n\\nPADLOCK #: HOW MANY\\n\\nKEYS ENCLOSED\\n\\nWINTERIZATION DATE COMPLETED: YES NO COMMENTS\\n\\nUTILITIES OFF\\nWATER OFF \\/ DISCONNECTED\\nEVIDENCE OF BURST PIPES, JOINTS, FIXTURES\\nEVIDENCE OF FREEZE DAMAGE\\nALL FIXTURES DRAINES\\nALL PIPES BLOWN OUT\\nBOILER \\/ HEATING SYSTEM DRAINED\\nANTI-FREEZE INSTALED IN ALL TRAPS \\/ FIXTURES\\nTAG\\/STICKER W\\/DATE OF WINTERIZATION AFFIXED\\nSUMP PUMP ON SITE\\n\\nMOBILE HOME INFORMATION YES NO COMMENTS DATE COMPLETED:\\n\\nMOBILE HOME YEAR\\nMOBILE HOME MAKE\\nMOBILE HOME MODEL\\nMOBILE HOME LENGTH\\nMOBILE HOME WIDTH\\nMOBILE HOME SIZE\\nMOBILE HOME VIN NUMBER\\nMOBILE HOME HUD TAG NUMBER\\nMOBILE HOME SERIAL NUMBER\\nTONGUE, WHEELS, AXLES ATTACHED\\nMOBILE HOME ATTACHED TO FOUNDATION\\n\\nMOBILE HOME SKIRTED\\nEXTERIOR CONDITION OF PROPERTY:\\n\\nLIVING ROOM GOOD FAIR POOR MISSING COMMENTS\\nGOOD FAIR POOR MISSING COMMENTS\\nITEM GOOD FAIR POOR MISSING COMMENTS\\nWALL PANELS GOOD FAIR POOR MISSING COMMENTS\\nFLOOR COVERING\\nSUB FLOOR\\nCEILING\\nWINDOW GLASS\\n\\nDINING ROOM\\n\\nITEM\\nWALL PANELS\\nFLOOR COVERING\\nSUB FLOOR\\nCEILING\\nWINDOW GLASS\\n\\nBEDROOMS\\n\\nITEM\\nWALL PANELS\\nFLOOR COVERING\\nSUB FLOOR\\nCEILING\\nWINDOW GLASS\\n\\nBATHROOMS\\n\\nITEM\\nWALL PANELS\\nFLOOR COVERING\\nSUB FLOOR\\nCEILING\\nWINDOW GLASS\\nKITCHEN \\/ UTILITY ROOM\\n\\nITEM GOOD FAIR POOR MISSING COMMENTS\\n\\nSINK\\nCABINETS\\nWALL PANELS\\nFLOOR COVERING\\nSUB FLOORING\\nCEILING\\nWINDOW GLASS\\nDISHWASHER\\nCLOTHES WASHER\\nCLOTHES DRYER\\n\\nAPPLIANCES\\n\\nITEM GOOD FAIR POOR MISSING COMMENTS\\n\\nFURN \\/ H. PUMP\\nAIR CONDITIONER\\nWATER HEATER\\nRANGE\\nREFRIGERATOR\\n\\nNAME OF PREPARER:\\nDATE:\",\"Source\":\"Import\"},{\"Description\":\"1.)\",\"Qty\":\"1\",\"Price\":\"0.00\",\"Total\":\"0.00\",\"AdditionalInstructions\":\"BOARD 17 BROKEN WINDOWS. MEASUREMENTS ARE UNITED INCHES. WILL REQUIRE PLYWOOD, BOARDS,\\nBOLTS, AND BASIC TOOLS\\n52X34,52X34,41X33,32X21,22X33,33X22,21X33,52X34,52X34,52X34,52X34,52X34,52X34,52X34,52X34,52X34 1323UI\\nPER UI,\",\"Source\":\"Import - Modified\"},{\"Description\":\"2.)\",\"Qty\":\"1\",\"Price\":\"0.00\",\"Total\":\"0.00\",\"AdditionalInstructions\":\"INSTALL PADLOCK AND HASP ON THE OUTBUILDING\",\"Source\":\"Import - Modified\"},{\"Description\":\"3.)\",\"Qty\":\"1\",\"Price\":\"0.00\",\"Total\":\"0.00\",\"AdditionalInstructions\":\"REMOVE 1 CYD FECES FROM THE TOILET\",\"Source\":\"Import - Modified\"},{\"Description\":\"4.)\",\"Qty\":\"1\",\"Price\":\"0.00\",\"Total\":\"0.00\",\"AdditionalInstructions\":\"REMOVE 1 CYD FECES FROM THE BATHTUB\",\"Source\":\"Import - Modified\"},{\"Description\":\"5.)\",\"Qty\":\"1\",\"Price\":\"0.00\",\"Total\":\"0.00\",\"AdditionalInstructions\":\"CLEAN HEAVILY SOILED TOILET\\/ CLEAN WITH CLOROX OR SOMETHING EQUIVALENT. WILL REQUIRE\\nCLEANING TOOLS, 2 GALLONS OF BLEACH\",\"Source\":\"Import - Modified\"},{\"Description\":\"6.)\",\"Qty\":\"1\",\"Price\":\"0.00\",\"Total\":\"0.00\",\"AdditionalInstructions\":\"CLEAN AND SANITIZE 1 DIRTY SHOWER IN THE BATHROOM WITH BLEACH AND SCRUB CLEAN. WILL\\nREQUIRE CLEANING TOOLS, 2 GALLONS OF BLEACH\",\"Source\":\"Import - Modified\"},{\"Description\":\"7.)\",\"Qty\":\"1\",\"Price\":\"0.00\",\"Total\":\"0.00\",\"AdditionalInstructions\":\"REMOVE 1 CYD OF RAW FOOD FROM THE KTICHEN\",\"Source\":\"Import - Modified\"},{\"Description\":\"8.)\",\"Qty\":\"1\",\"Price\":\"0.00\",\"Total\":\"0.00\",\"AdditionalInstructions\":\"REMOVE 4 CYDS OF EXTERIOR DEBRIS. CONTENTS ARE: PLASTIC, CLOTHING, TRASH, MISC. PER CYD,\",\"Source\":\"Import - Modified\"},{\"Description\":\"9.)\",\"Qty\":\"1\",\"Price\":\"0.00\",\"Total\":\"0.00\",\"AdditionalInstructions\":\"TOTAL\",\"Source\":\"Import - Modified\"},{\"Description\":\"\",\"Qty\":\"\",\"Price\":\" \",\"Total\":\"0.00\",\"AdditionalInstructions\":\"\",\"Source\":\"\"}],\"complete_date\":\"\",\"comments\":\"1. BOARD 17 BROKEN WINDOWS. MEASUREMENTS ARE UNITED INCHES. WILL REQUIRE PLYWOOD, BOARDS, BOLTS, AND BASIC TOOLS 52X34,52X34,41X33,32X21,22X33,33X22,21X33,52X34,52X34,52X34,52X34,52X34,52X34,52X34,52X34,52X34 1323UI PER UI.\\n2. INSTALL PADLOCK AND HASP ON THE OUTBUILDING\\n3. REMOVE 1 CYD FECES FROM THE TOILET\\n4. REMOVE 1 CYD FECES FROM THE BATHTUB\\n5. CLEAN HEAVILY SOILED TOILET\\/ CLEAN WITH CLOROX OR SOMETHING EQUIVALENT. WILL REQUIRE CLEANING TOOLS, 2 GALLONS OF BLEACH.\\n6. CLEAN AND SANITIZE 1 DIRTY SHOWER IN THE BATHROOM WITH BLEACH AND SCRUB CLEAN. WILL REQUIRE CLEANING TOOLS, 2 GALLONS OF BLEACH.\\n7. REMOVE 1 CYD OF RAW FOOD FROM THE KTICHEN\\n8. REMOVE 4 CYDS OF EXTERIOR DEBRIS. CONTENTS ARE: PLASTIC, CLOTHING, TRASH, MISC.\\n9. PROVIDE PHOTOS TO SUPPORT WORK COMPLETED\\/MEASUREMENT PHOTOS\\n10. PROVIDE DUMP RECEIPT\\n \\n \\n\\nIf you have any questions please call or text Belinda at 8167869413\",\"client_Due_date\":\"January 15, 2021\",\"username\":\"Willadmin2\",\"follow_up_complete\":\"no\",\"broker_info\":\"\",\"lot_size\":\"5,318\",\"received_date\":\"January 8, 2021\",\"customer\":\"WF01\",\"client_company\":\"MSI\",\"freeze_property\":\"no\",\"ppw\":\"75592\",\"start_date\":\"\",\"contractor\":\"Sims, Kelly\",\"ready_for_office\":\"no\",\"missing_info\":\"No\",\"BATF\":\"No\",\"sub_contractor_follow_up\":\"\",\"work_type\":\"MSI - Post Sale Bid Approval\",\"cancel_date\":\"\",\"is_Inspection\":\"No\",\"assigned_admin\":\"Belinda Belcher\",\"lklg\":\"4477\\n67767\"}";
            try
            {
                var result = JsonData.Split().Where(x => x.EndsWith("}") && x.EndsWith("}")).ToList();
                var startTag = "}";
                int startIndex = result[0].IndexOf(startTag) + startTag.Length;
                int endIndex = result[0].IndexOf("}", startIndex);
                if (endIndex >= startIndex)
                {
                    var val = result[0].Substring(startIndex, endIndex - startIndex);
                    Formatjson = JsonData.Substring(0, JsonData.Length - (val.Length + 1));
                }
                else
                {
                    Formatjson = JsonData;
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return Formatjson;
        }

        public List<dynamic> GetWorkOrderQueueMasterDetails(WorkOrderMaster_API_DTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {

                WorkOrderMaster_API_DTO workOrderMaster_API_DTO;
                DataSet ds = null;
                string wherecondition = string.Empty;

                
                switch (model.Imrt_Import_From_ID)
                {

                    case 1:
                        {
                            if (!string.IsNullOrEmpty(model.Imrt_Wo_Number))
                            {
                                wherecondition = wherecondition != null ? wherecondition + "  And Wo = '" + model.Imrt_Wo_Number + "'" : "  And Wo = '" + model.Imrt_Wo_Number + "'";
                            }
                            if (!string.IsNullOrEmpty(model.Imrt_Wo_WTIDName))
                            {
                                wherecondition = wherecondition != null ? wherecondition + " And work_type =    '" + model.Imrt_Wo_WTIDName + "'" : wherecondition + " And work_type =    '" + model.Imrt_Wo_WTIDName + "'";
                            }
                            if (!string.IsNullOrEmpty(model.Imrt_Wo_CatIDName))
                            {
                                wherecondition = wherecondition != null ? wherecondition + " And Category =    '" + model.Imrt_Wo_CatIDName + "'" : " And Category =    '" + model.Imrt_Wo_CatIDName + "'";
                            }
                            model.WhereClause = wherecondition;
                            ds = GetImportWorkOrderQueueMaster(model);
                            break;
                        }
                    case 2:
                        {
                            if (!string.IsNullOrEmpty(model.Imrt_Wo_Number))
                            {
                                wherecondition = wherecondition != null ? wherecondition + "  And workOrderNumber = '" + model.Imrt_Wo_Number + "'" : "  And workOrderNumber = '" + model.Imrt_Wo_Number + "'";
                            }
                            //if (!string.IsNullOrEmpty(model.Imrt_Wo_WTIDName))
                            //{
                            //    wherecondition = wherecondition != null ? wherecondition + " And wim.WI_ImportFrom =    '" + model.Imrt_Wo_WTIDName + "'" : wherecondition + " And wim.WI_ImportFrom =    '" + model.Imrt_Wo_WTIDName + "'";
                            //}
                            //if (!string.IsNullOrEmpty(model.Imrt_Wo_CatIDName))
                            //{
                            //    wherecondition = wherecondition != null ? wherecondition + " And wim.WI_FriendlyName =    '" + model.Imrt_Wo_CatIDName + "'" : " And wim.WI_FriendlyName =    '" + model.Imrt_Wo_CatIDName + "'";
                            //}
                            model.WhereClause = wherecondition;
                            ds = GetWorkOrderPruvanMasterData(model);
                            break;
                        }
                    case 3:
                        {
                            if (!string.IsNullOrEmpty(model.Imrt_Wo_Number))
                            {
                                wherecondition = wherecondition != null ? wherecondition + "  And WMSI_Order = '" + model.Imrt_Wo_Number + "'" : "  And WMSI_Order = '" + model.Imrt_Wo_Number + "'";
                            }
                            if (!string.IsNullOrEmpty(model.Imrt_Wo_WTIDName))
                            {
                                wherecondition = wherecondition != null ? wherecondition + " And WMSI_OrderType =    '" + model.Imrt_Wo_WTIDName + "'" : wherecondition + " WMSI_OrderType =    '" + model.Imrt_Wo_WTIDName + "'";
                            }
                            //if (!string.IsNullOrEmpty(model.Imrt_Wo_CatIDName))
                            //{
                            //    wherecondition = wherecondition != null ? wherecondition + " And wim.WI_FriendlyName =    '" + model.Imrt_Wo_CatIDName + "'" : " And wim.WI_FriendlyName =    '" + model.Imrt_Wo_CatIDName + "'";
                            //}
                            model.WhereClause = wherecondition;
                            ds = GetMSIWorkOrderMasterData(model);
                            break;
                        }
                    case 4:
                        {
                            if (!string.IsNullOrEmpty(model.Imrt_Wo_Number))
                            {
                                wherecondition = wherecondition != null ? wherecondition + "  And WorkOrder_FiveBrother_Number = '" + model.Imrt_Wo_Number + "'" : "  And WorkOrder_FiveBrother_Number = '" + model.Imrt_Wo_Number + "'";
                            }
                            if (!string.IsNullOrEmpty(model.Imrt_Wo_WTIDName))
                            {
                                wherecondition = wherecondition != null ? wherecondition + " And WorkOrder_FiveBrother_OrderTypeCode =    '" + model.Imrt_Wo_WTIDName + "'" : wherecondition + " WorkOrder_FiveBrother_OrderTypeCode =    '" + model.Imrt_Wo_WTIDName + "'";
                            }
                            //if (!string.IsNullOrEmpty(model.Imrt_Wo_CatIDName))
                            //{
                            //    wherecondition = wherecondition != null ? wherecondition + " And wim.WI_FriendlyName =    '" + model.Imrt_Wo_CatIDName + "'" : " And wim.WI_FriendlyName =    '" + model.Imrt_Wo_CatIDName + "'";
                            //}
                            model.WhereClause = wherecondition;
                            ds = GetWorkOrderFiveBrotherMasterData(model);
                            break;
                        }
                    case 6:
                        {
                            if (!string.IsNullOrEmpty(model.Imrt_Wo_Number))
                            {
                                wherecondition = wherecondition != null ? wherecondition + "  And WMCS_wo_id = '" + model.Imrt_Wo_Number + "'" : "  And WMCS_wo_id = '" + model.Imrt_Wo_Number + "'";
                            }

                            model.WhereClause = wherecondition;
                            ds = GetMCSWorkOrderMasterData(model);
                            break;
                        }
                    case 7:
                        {
                            if (!string.IsNullOrEmpty(model.Imrt_Wo_Number))
                            {
                                wherecondition = wherecondition != null ? wherecondition + "  And WNFR_Order = '" + model.Imrt_Wo_Number + "'" : "  And WNFR_Order = '" + model.Imrt_Wo_Number + "'";
                            }
                       
                            model.WhereClause = wherecondition;
                            ds = GetNFRWorkOrderMasterData(model);
                            break;
                        }
                    case 10:
                        {
                            if (!string.IsNullOrEmpty(model.Imrt_Wo_Number))
                            {
                                wherecondition = wherecondition != null ? wherecondition + "  And crp.WCyprexx_wo_id = '" + model.Imrt_Wo_Number + "'" : "  And crp.WCyprexx_wo_id = '" + model.Imrt_Wo_Number + "'";
                            }

                            model.WhereClause = wherecondition;
                            ds = GetWorkOrderCyprexxMasterData(model);
                            break;
                        }
                    case 12:
                        {
                            if (!string.IsNullOrEmpty(model.Imrt_Wo_Number))
                            {
                                wherecondition = wherecondition != null ? wherecondition + "  And srp.WServiceLink_wo_id = '" + model.Imrt_Wo_Number + "'" : "  And srp.WServiceLink_wo_id = '" + model.Imrt_Wo_Number + "'";
                            }

                            model.WhereClause = wherecondition;
                            ds = GetWorkOrderServiceLinkMasterData(model);
                            break;
                        }
                    case 13:
                        {
                            if (!string.IsNullOrEmpty(model.Imrt_Wo_Number))
                            {
                                wherecondition = wherecondition != null ? wherecondition + "  And srp.WAltisource_wo_id = '" + model.Imrt_Wo_Number + "'" : "  And srp.WAltisource_wo_id = '" + model.Imrt_Wo_Number + "'";
                            }

                            model.WhereClause = wherecondition;
                            ds = GetWorkOrderAltisourceMasterData(model);
                            break;
                        }
                    case 19:
                        {
                            if (!string.IsNullOrEmpty(model.Imrt_Wo_Number))
                            {
                                wherecondition = wherecondition != null ? wherecondition + "  And WAG_wo_id = '" + model.Imrt_Wo_Number + "'" : "  And WAG_wo_id = '" + model.Imrt_Wo_Number + "'";
                            }

                            model.WhereClause = wherecondition;
                            ds = GetAGWorkOrderMasterData(model);
                            break;
                        }



                }
                if (ds != null)
                {
                    string Totalcount = string.Empty;

                    List<WorkOrderMaster_API_DTO> lstWorkOrderMaster_API_DTO = new List<WorkOrderMaster_API_DTO>();
                    #region old comment
                    //var myEnumerableFeapr = ds.Tables[0].AsEnumerable();
                    //List<WorkOrderMaster_API_DTO> WoImportQueue =
                    //   (from item in myEnumerableFeapr
                    //    select new WorkOrderMaster_API_DTO
                    //    {

                    //        Category = item.Field<String>("Category"),
                    //        Wo = item.Field<String>("Wo"),
                    //        address = item.Field<String>("address"),
                    //        estimated_complete_date = item.Field<String>("estimated_complete_date"),
                    //        loan_info = item.Field<String>("loan_info"),
                    //        office_locked = item.Field<String>("office_locked"),
                    //        due_date = item.Field<String>("due_date"),
                    //        ready_for_office_Contractor = item.Field<String>("ready_for_office_Contractor"),
                    //        client_Due_date = item.Field<String>("client_Due_date"),
                    //        complete_date = item.Field<String>("complete_date"),
                    //        comments = item.Field<String>("comments"),
                    //        username = item.Field<String>("username"),
                    //        follow_up_complete = item.Field<String>("follow_up_complete"),
                    //        broker_info = item.Field<String>("broker_info"),
                    //        lot_size = item.Field<String>("lot_size"),
                    //        received_date = item.Field<DateTime?>("received_date"),
                    //        customer = item.Field<String>("customer"),
                    //        client_company = item.Field<String>("client_company"),
                    //        freeze_property = item.Field<String>("freeze_property"),
                    //        ppw = item.Field<String>("ppw"),
                    //        start_date = item.Field<DateTime?>("start_date"),
                    //        contractor = item.Field<String>("contractor"),
                    //        ready_for_office = item.Field<String>("ready_for_office"),
                    //        missing_info = item.Field<String>("missing_info"),
                    //        BATF = item.Field<String>("BATF"),
                    //        sub_contractor_follow_up = item.Field<String>("sub_contractor_follow_up"),
                    //        work_type = item.Field<String>("work_type"),
                    //        cancel_date = item.Field<DateTime?>("cancel_date"),
                    //        is_Inspection = item.Field<String>("is_Inspection"),
                    //        assigned_admin = item.Field<String>("assigned_admin"),
                    //        lklg = item.Field<String>("lklg"),
                    //        IsActive = item.Field<Boolean?>("IsActive"),
                    //    }).ToList();

                    //objDynamic.Add(WoImportQueue);

                    //var myEnumerableFeaprd = ds.Tables[1].AsEnumerable();
                    //List<WorkOrderItemDetails_API_DTO> ImportQueue =
                    //   (from item in myEnumerableFeaprd
                    //    select new WorkOrderItemDetails_API_DTO
                    //    {


                    //        Description = item.Field<String>("Description"),
                    //        Qty = item.Field<String>("Qty"),
                    //        Price = item.Field<String>("Price"),
                    //        Total = item.Field<String>("Total"),
                    //        Additional_Instructions = item.Field<String>("Additional_Instructions"),

                    //        IsActive = item.Field<Boolean?>("IsActive"),

                    //    }).ToList();
                    #endregion
                    if (ds.Tables.Count > 0)
                    {

                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            workOrderMaster_API_DTO = new WorkOrderMaster_API_DTO();
                            workOrderMaster_API_DTO.Category = ds.Tables[0].Rows[i]["Category"].ToString();
                            if (!string.IsNullOrEmpty(ds.Tables[0].Rows[i]["Pkey_Id"].ToString()))
                            {
                                workOrderMaster_API_DTO.Pkey_Id = Convert.ToInt64(ds.Tables[0].Rows[i]["Pkey_Id"].ToString());
                            }

                            workOrderMaster_API_DTO.Wo = ds.Tables[0].Rows[i]["Wo"].ToString();
                            workOrderMaster_API_DTO.address = ds.Tables[0].Rows[i]["address"].ToString();
                            workOrderMaster_API_DTO.estimated_complete_date = ds.Tables[0].Rows[i]["estimated_complete_date"].ToString();
                            workOrderMaster_API_DTO.loan_info = ds.Tables[0].Rows[i]["loan_info"].ToString();
                            workOrderMaster_API_DTO.office_locked = ds.Tables[0].Rows[i]["office_locked"].ToString();
                            workOrderMaster_API_DTO.due_date = ds.Tables[0].Rows[i]["due_date"].ToString();
                            workOrderMaster_API_DTO.ready_for_office_Contractor = ds.Tables[0].Rows[i]["ready_for_office_Contractor"].ToString();
                            workOrderMaster_API_DTO.client_Due_date = ds.Tables[0].Rows[i]["client_Due_date"].ToString();
                            workOrderMaster_API_DTO.complete_date = ds.Tables[0].Rows[i]["complete_date"].ToString();
                            workOrderMaster_API_DTO.comments = ds.Tables[0].Rows[i]["comments"].ToString();
                            workOrderMaster_API_DTO.username = ds.Tables[0].Rows[i]["username"].ToString();
                            workOrderMaster_API_DTO.follow_up_complete = ds.Tables[0].Rows[i]["follow_up_complete"].ToString();
                            workOrderMaster_API_DTO.broker_info = ds.Tables[0].Rows[i]["broker_info"].ToString();
                            workOrderMaster_API_DTO.lot_size = ds.Tables[0].Rows[i]["lot_size"].ToString();
                            if (!string.IsNullOrEmpty(ds.Tables[0].Rows[i]["received_date"].ToString()))
                            {
                                workOrderMaster_API_DTO.received_date = Convert.ToDateTime(ds.Tables[0].Rows[i]["received_date"].ToString());
                            }

                            workOrderMaster_API_DTO.customer = ds.Tables[0].Rows[i]["customer"].ToString();
                            workOrderMaster_API_DTO.client_company = ds.Tables[0].Rows[i]["client_company"].ToString();
                            workOrderMaster_API_DTO.freeze_property = ds.Tables[0].Rows[i]["freeze_property"].ToString();
                            workOrderMaster_API_DTO.ppw = ds.Tables[0].Rows[i]["ppw"].ToString();
                            if (!string.IsNullOrEmpty(ds.Tables[0].Rows[i]["start_date"].ToString()))
                            {
                                workOrderMaster_API_DTO.start_date = Convert.ToDateTime(ds.Tables[0].Rows[i]["start_date"].ToString());
                            }

                            workOrderMaster_API_DTO.contractor = ds.Tables[0].Rows[i]["contractor"].ToString();
                            workOrderMaster_API_DTO.ready_for_office = ds.Tables[0].Rows[i]["ready_for_office"].ToString();
                            workOrderMaster_API_DTO.missing_info = ds.Tables[0].Rows[i]["missing_info"].ToString();
                            workOrderMaster_API_DTO.BATF = ds.Tables[0].Rows[i]["BATF"].ToString();
                            workOrderMaster_API_DTO.sub_contractor_follow_up = ds.Tables[0].Rows[i]["sub_contractor_follow_up"].ToString();
                            workOrderMaster_API_DTO.work_type = ds.Tables[0].Rows[i]["work_type"].ToString();
                            if (!string.IsNullOrEmpty(ds.Tables[0].Rows[i]["cancel_date"].ToString()))
                            {
                                workOrderMaster_API_DTO.cancel_date = Convert.ToDateTime(ds.Tables[0].Rows[i]["cancel_date"].ToString());
                            }

                            workOrderMaster_API_DTO.is_Inspection = ds.Tables[0].Rows[i]["is_Inspection"].ToString();
                            workOrderMaster_API_DTO.assigned_admin = ds.Tables[0].Rows[i]["assigned_admin"].ToString();
                            workOrderMaster_API_DTO.lklg = ds.Tables[0].Rows[i]["lklg"].ToString();
                            if (!string.IsNullOrEmpty(ds.Tables[0].Rows[i]["IsActive"].ToString()))
                            {
                                workOrderMaster_API_DTO.IsActive = Convert.ToBoolean(ds.Tables[0].Rows[i]["IsActive"].ToString());
                            }
                            if (model.Imrt_Import_From_ID == 2 || model.Imrt_Import_From_ID == 4)
                            {
                                workOrderMaster_API_DTO.Contractor_Id = ds.Tables[0].Rows[i]["Contractor_Id"] != null ? Convert.ToInt64(ds.Tables[0].Rows[i]["Contractor_Id"].ToString()) : 0;
                                workOrderMaster_API_DTO.Coordinator_Id = ds.Tables[0].Rows[i]["Coordinator_Id"] != null ? Convert.ToInt64(ds.Tables[0].Rows[i]["Coordinator_Id"].ToString()) : 0;
                            }
                            workOrderMaster_API_DTO.Imrt_Import_From_ID = model.Imrt_Import_From_ID;

                            List<WorkOrderItemDetail> lstWorkOrderItemDetail = new List<WorkOrderItemDetail>();

                            if (ds.Tables.Count > 1)
                            {
                                DataRow[] drApirow = ds.Tables[1].Select("WorkOrderMaster_API_Data_Pkey_Id = " + workOrderMaster_API_DTO.Pkey_Id + " ");
                                if (drApirow.Length != 0)
                                {
                                    foreach (var itemrow in drApirow)
                                    {
                                        WorkOrderItemDetail workOrderItemDetail = new WorkOrderItemDetail();

                                        workOrderItemDetail.Description = itemrow["Description"].ToString();
                                        workOrderItemDetail.Price = itemrow["Price"].ToString();
                                        workOrderItemDetail.Qty = itemrow["Qty"].ToString();
                                        workOrderItemDetail.Total = itemrow["Total"].ToString();
                                        workOrderItemDetail.AdditionalInstructions = itemrow["Additional_Instructions"].ToString();
                                        workOrderItemDetail.WorkOrderItem_PkeyId = Convert.ToInt64(itemrow["WorkOrderItem_PkeyId"].ToString());
                                        workOrderItemDetail.WorkOrderMaster_API_Data_Pkey_Id = Convert.ToInt64(itemrow["WorkOrderMaster_API_Data_Pkey_Id"].ToString());



                                        lstWorkOrderItemDetail.Add(workOrderItemDetail);
                                    }
                                }
                            }
                            if (ds.Tables.Count > 2)
                            {
                                Totalcount = ds.Tables[2].Rows[0]["count"].ToString();
                            }

                            workOrderMaster_API_DTO.WorkOrderItemDetail = lstWorkOrderItemDetail;
                            lstWorkOrderMaster_API_DTO.Add(workOrderMaster_API_DTO);



                        }

                        objDynamic.Add(lstWorkOrderMaster_API_DTO);
                        objDynamic.Add(Totalcount);
                    }
                }

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        // get workorderQueue

        private DataSet GetImportWorkOrderQueueMaster(WorkOrderMaster_API_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[GetWorkOrderMaster_API_Data]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Pkey_Id", 1 + "#bigint#" + model.Pkey_Id);
                input_parameters.Add("@WhereClause", 1 + "#varchar#" + model.WhereClause);
                input_parameters.Add("@PageNumber", 1 + "#int#" + model.PageNumber);
                input_parameters.Add("@NoofRows", 1 + "#int#" + model.NoofRows);
                input_parameters.Add("@Imrt_PkeyId", 1 + "#bigint#" + model.Imrt_PkeyId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        private DataSet GetImportIDFrom_PPW(WorkOrderMaster_API_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_WorkOrderMasterAPIData_PPW]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@ppw", 1 + "#bigint#" + model.ppw);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public WorkOrderMaster_API_DTO GetImportIDFromPPW(WorkOrderMaster_API_DTO model)
        {

            try
            {
                model.Type = 1;
                DataSet ds = GetImportIDFrom_PPW(model);

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    string strPkey_Id = ds.Tables[0].Rows[i]["Pkey_Id"].ToString();
                    string strImportMasterPkey = ds.Tables[0].Rows[i]["ImportMasterPkey"].ToString();
                    if (!string.IsNullOrEmpty(strPkey_Id))
                    {
                        model.Pkey_Id = Convert.ToInt64(strPkey_Id);
                    }
                    if (!string.IsNullOrEmpty(strImportMasterPkey))
                    {
                        model.ImportMasterPkey = Convert.ToInt64(strImportMasterPkey);
                    }
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return model;
        }

        public List<dynamic> GetImportWorkOrderQueueMasterDetails(WorkOrderMaster_API_DTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetImportWorkOrderQueueMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<WorkOrderMaster_API_DTO> WorkOrderImportQueue =
                   (from item in myEnumerableFeaprd
                    select new WorkOrderMaster_API_DTO
                    {
                        Pkey_Id = item.Field<Int64>("Pkey_Id"),
                        Category = item.Field<String>("Category"),
                        Wo = item.Field<String>("Wo"),
                        address = item.Field<String>("address"),
                        estimated_complete_date = item.Field<String>("estimated_complete_date"),
                        loan_info = item.Field<String>("loan_info"),
                        office_locked = item.Field<String>("office_locked"),
                        due_date = item.Field<String>("due_date"),
                        ready_for_office_Contractor = item.Field<String>("ready_for_office_Contractor"),
                        client_Due_date = item.Field<String>("client_Due_date"),
                        complete_date = item.Field<String>("complete_date"),
                        comments = item.Field<String>("comments"),
                        username = item.Field<String>("username"),
                        follow_up_complete = item.Field<String>("follow_up_complete"),
                        broker_info = item.Field<String>("broker_info"),
                        lot_size = item.Field<String>("lot_size"),
                        received_date = item.Field<DateTime?>("received_date"),
                        customer = item.Field<String>("customer"),
                        client_company = item.Field<String>("client_company"),
                        freeze_property = item.Field<String>("freeze_property"),
                        ppw = item.Field<String>("ppw"),
                        start_date = item.Field<DateTime?>("start_date"),
                        contractor = item.Field<String>("contractor"),
                        ready_for_office = item.Field<String>("ready_for_office"),
                        missing_info = item.Field<String>("missing_info"),
                        BATF = item.Field<String>("BATF"),
                        sub_contractor_follow_up = item.Field<String>("sub_contractor_follow_up"),
                        work_type = item.Field<String>("work_type"),
                        cancel_date = item.Field<DateTime?>("cancel_date"),
                        is_Inspection = item.Field<String>("is_Inspection"),
                        assigned_admin = item.Field<String>("assigned_admin"),
                        lklg = item.Field<String>("lklg"),
                        Mortgagor = item.Field<String>("Mortgagor"),
                        IsActive = item.Field<Boolean?>("IsActive"),


                    }).ToList();

                objDynamic.Add(WorkOrderImportQueue);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
        private DataSet GetWorkOrderPruvanMasterData(WorkOrderMaster_API_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[GetWorkOrderPruvanMaster_Data]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Pkey_Id", 1 + "#bigint#" + model.Pkey_Id);
                input_parameters.Add("@WhereClause", 1 + "#varchar#" + model.WhereClause);
                input_parameters.Add("@PageNumber", 1 + "#int#" + model.PageNumber);
                input_parameters.Add("@NoofRows", 1 + "#int#" + model.NoofRows);
                input_parameters.Add("@Imrt_PkeyId", 1 + "#bigint#" + model.Imrt_PkeyId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ds;
        }

        private DataSet GetWorkOrderFiveBrotherMasterData(WorkOrderMaster_API_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[GetWorkOrderFiveBrotherMaster_Data]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Pkey_Id", 1 + "#bigint#" + model.Pkey_Id);
                input_parameters.Add("@WhereClause", 1 + "#varchar#" + model.WhereClause);
                input_parameters.Add("@PageNumber", 1 + "#int#" + model.PageNumber);
                input_parameters.Add("@NoofRows", 1 + "#int#" + model.NoofRows);
                input_parameters.Add("@Imrt_PkeyId", 1 + "#bigint#" + model.Imrt_PkeyId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ds;
        }

        private DataSet GetMSIWorkOrderMasterData(WorkOrderMaster_API_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[GetWorkOrderMSIMaster_Data]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Pkey_Id", 1 + "#bigint#" + model.Pkey_Id);
                input_parameters.Add("@WhereClause", 1 + "#varchar#" + model.WhereClause);
                input_parameters.Add("@PageNumber", 1 + "#int#" + model.PageNumber);
                input_parameters.Add("@NoofRows", 1 + "#int#" + model.NoofRows);
                input_parameters.Add("@Imrt_PkeyId", 1 + "#bigint#" + model.Imrt_PkeyId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ds;
        }

        public List<dynamic> MSIWorkOrderItemDetails(string WorkOrderMaster_DTO, Int64 WI_Pkey_ID, string Filename, string FilePath,string PdfFilePath, string pdfname,Int64 UserID)
        {
            WorkOrder_MSI_MasterData workOrder_MSI_MasterData = new WorkOrder_MSI_MasterData();

            List<dynamic> objDynamic = new List<dynamic>();

            try
            {

                JsonValidate jsonValidate = new JsonValidate();
                string Formatjson = WorkOrderMaster_DTO;
                bool val = jsonValidate.IsValidJson(Formatjson);
                if (val)
                {
                    var Data = JsonConvert.DeserializeObject<WorkOrder_MSI_Item_JsonDTO>(Formatjson);
                    var Workorder = Data.work_order;
                    var Client = Data.client;
                    var PropertyInfo = Data.property_info;
                    string strpoint_Of_Service_Question = string.Empty;
                    if (Data.point_of_service_question != null)
                    {
                         strpoint_Of_Service_Question = JsonConvert.SerializeObject(Data.point_of_service_question);
                    }
                   
                    WorkOrder_MSI_MasterDTO model = new WorkOrder_MSI_MasterDTO();
                    model.WMSI_PkeyId = 0;
                    model.WMSI_Order = Workorder.order;
                    model.WMSI_AssignedDate = Workorder.assigned_date;
                    model.WMSI_DueDate = Workorder.due_date;
                    model.WMSI_Property_Id = Workorder.property_id;
                    model.WMSI_Priority = Workorder.priority;
                    model.WMSI_OrderType = Workorder.order_type;

                    model.WMSI_Vendor = Data.vendor;
                    model.WMSI_Mortgagor = Data.mortgagor;
                    model.WMSI_address = Data.address;
                    model.WMSI_City = Data.city;
                    model.WMSI_State = Data.state;
                    model.WMSI_zip = Data.zip;
                    model.WMSI_point_of_service_question = strpoint_Of_Service_Question;
                    model.WMSI_username = Data.username;
                    model.WMSI_id = Data.id;


                    model.WMSI_ClientCode = Client.client_code;
                    model.WMSI_Loan = Client.loan;
                    model.WMSI_HoaContact = Client.hoa_contact;
                    model.WMSI_PosRequired = Client.pos_required;
                    model.WMSI_sendtoAspen = Client.send_to_aspen;
                    model.WMSI_HoaPhone = Client.hoa_phone;
                    model.WMSI_loan_or_investor_type = Data.client.loan_or_investor_type;

                    model.WMSI_FtvDate = PropertyInfo.ftv_date;
                    model.WMSI_InitialSecureDate = PropertyInfo.initial_secure_date;
                    model.WMSI_LastWinterizationDate = PropertyInfo.last_winterization_date;
                    model.WMSI_LastInspectedDate = PropertyInfo.last_inspected_date;
                    model.WMSI_LastLockChangeDate = PropertyInfo.last_lock_change_date;
                    model.WMSI_LastSnowRemovalDate = PropertyInfo.last_snow_removal_date; 
                    model.WMSI_LastInspOccupancy = PropertyInfo.last_insp_occupancy; 
                    model.WMSI_Lockbox = PropertyInfo.lockbox; 
                    model.WMSI_InitialGrassCutDate = PropertyInfo.initial_grass_cut_date; 
                    model.WMSI_PropertyType = PropertyInfo.property_type; 
                    model.WMSI_Keycode = PropertyInfo.keycode; 
                    model.WMSI_LastGrassCutDate = PropertyInfo.last_grass_cut_date; 
                    model.WMSI_Color = PropertyInfo.color; 
                    model.WMSI_SecurityAccessCode = PropertyInfo.security_access_code; 
                    model.WMSI_LawnSizeSqFt = PropertyInfo.lawn_size_sq_ft; 

                    model.WMSI_IsActive = true; 
                    model.WMSI_IsDelete = false; 
                    model.WMSI_IsProcessed = false;

                   
                  
                    //var config = new MapperConfiguration(cfg =>
                    //{
                    //    cfg.CreateMap<WorkOrder_MSI_Item_JsonDTO, WorkOrder_MSI_MasterDTO>();
                    //});

                    //IMapper mapper = config.CreateMapper();
                    //var dest = mapper.Map<WorkOrder_MSI_Item_JsonDTO, WorkOrder_MSI_MasterDTO>(Data);
                    //if (string.IsNullOrEmpty(dest.WMSI_Order))
                    //{
                    //    dest.WMSI_Order = dest.ppw;
                    //}


                    GoogleLocation GoogleLocation = new GoogleLocation();
                    GoogleLocationDTO googleLocationDTO = new GoogleLocationDTO();

                    WorkOrderMasterData workOrderMasterData = new WorkOrderMasterData();
                    workOrderxDTO workOrderxDTO = new workOrderxDTO();
                    workOrderxDTO.address1 = model.WMSI_Mortgagor;
                    workOrderxDTO.Type = 2;
                    DataSet ds = workOrderMasterData.Get_WorkOrderLatLongByAddress(workOrderxDTO);

                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                model.WMSI_gpsLatitude = ds.Tables[0].Rows[i]["gpsLatitude"].ToString();
                                model.WMSI_gpsLongitude = ds.Tables[0].Rows[i]["gpsLongitude"].ToString();
                            }
                        }
                        else
                        { 
                            ///////////////////////code added to exract GPS ////////////////////
                            googleLocationDTO.Address = model.WMSI_Mortgagor;
                            googleLocationDTO = GoogleLocation.GetLatitudeLogitudeWithAPIKey(googleLocationDTO);
                            model.WMSI_gpsLatitude = googleLocationDTO.Latitude;
                            model.WMSI_gpsLongitude = googleLocationDTO.Longitude;
                        }
                    }
                    else
                    {
                        ///////////////////////code added to exract GPS ////////////////////
                        googleLocationDTO.Address = model.WMSI_Mortgagor;
                        googleLocationDTO = GoogleLocation.GetLatitudeLogitudeWithAPIKey(googleLocationDTO);
                        model.WMSI_gpsLatitude = googleLocationDTO.Latitude;
                        model.WMSI_gpsLongitude = googleLocationDTO.Longitude;
                    }

                    model.Type = 1;
                    model.WorkOrder_Import_FkeyId = WI_Pkey_ID;
                    model.WMSI_ImportMaster_Pkey = WI_Pkey_ID;

                   
                  
                    model.WMSI_Import_File_Name = Filename;
                    model.WMSI_Import_FilePath = FilePath;
                    model.WMSI_Import_Pdf_Name = pdfname;
                    model.WMSI_Import_Pdf_Path = PdfFilePath;
                    model.UserID = UserID;
                    objDynamic = workOrder_MSI_MasterData.AddWorkOrder_MSIData(model);
                    if (objDynamic[0].Status == "1")
                    {


                        for (int i = 0; i < Data.instructions.Count; i++)
                        {
                            InstructionsData instructionsData = new InstructionsData();
                            instructionsData = Data.instructions[i];

                            var configItems = new MapperConfiguration(cfg =>
                            {
                                cfg.CreateMap<InstructionsData, WorkOrder_MSI_Item_MasterDTO>();
                            });

                            IMapper mapperItems = configItems.CreateMapper();
                            var destItems = mapperItems.Map<InstructionsData, WorkOrder_MSI_Item_MasterDTO>(instructionsData);
                            destItems.Type = 1;


                            if (!string.IsNullOrEmpty(destItems.WMSINS_Description) || !string.IsNullOrEmpty(destItems.WMSINS_Additional_Instructions))
                            {
                                destItems.WMSINS_WMSI_FkeyId = Convert.ToInt64(objDynamic[0].WMSI_PkeyId);
                                destItems.WMSINS_Additional_Instructions = instructionsData.additional_details;
                                destItems.WMSINS_InstName = instructionsData.instruction_name;
                                destItems.UserID = UserID;
                                var ObjItemData = workOrder_MSI_MasterData.AddWorkOrder_MSIItemData(destItems);
                            }
                            else
                            {
                                log.logDebugMessage("------------------WorkOrder Item Null Record Found Start -------------" + objDynamic[0].ToString());
                                log.logDebugMessage(destItems.WMSINS_Description);
                                log.logDebugMessage(destItems.WMSINS_Additional_Instructions);
                                log.logDebugMessage("------------------WorkOrder Item Null Record Found End -------------" + objDynamic[0].ToString());
                            }
                        }
                    }
                }
                else
                {

                    log.logErrorMessage("------------------InValid Json -------------");
                    log.logErrorMessage(Filename);
                    log.logErrorMessage(WorkOrderMaster_DTO);
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage("------------------Json Error Start-------------");
                log.logErrorMessage(Filename);
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage("------------------Json Error  End-------------");

            }
            return objDynamic;
        }


        //nfr record insert
        private DataSet GetNFRWorkOrderMasterData(WorkOrderMaster_API_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[GetWorkOrderNFRMaster_Data]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@WNFR_PkeyID", 1 + "#bigint#" + model.Pkey_Id);
                input_parameters.Add("@WhereClause", 1 + "#varchar#" + model.WhereClause);
                input_parameters.Add("@PageNumber", 1 + "#int#" + model.PageNumber);
                input_parameters.Add("@NoofRows", 1 + "#int#" + model.NoofRows);
                input_parameters.Add("@Imrt_PkeyId", 1 + "#bigint#" + model.Imrt_PkeyId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ds;
        }
        public List<dynamic> NFRWorkOrderItemDetails(string WorkOrderMaster_DTO, Int64 WI_Pkey_ID, string Filename, string FilePath , string PdfFilePath , string pdfname,Int64 UserID)
        {
            WorkOrder_NFR_MasterData workOrder_NFR_MasterData = new WorkOrder_NFR_MasterData();

            List<dynamic> objDynamic = new List<dynamic>();

            try
            {

                JsonValidate jsonValidate = new JsonValidate();
                string Formatjson = WorkOrderMaster_DTO;
                bool val = jsonValidate.IsValidJson(Formatjson);
                if (val)
                {
                    var Data = JsonConvert.DeserializeObject<WorkOrder_NFR_Item_JsonDTO>(Formatjson);
                    //var Workorder = Data.work_order;
                    var Workorder = Data.work_order;
                    WorkOrder_NFR_MasterDTO model = new WorkOrder_NFR_MasterDTO();
                    model.WNFR_PkeyID = 0;
                    model.WNFR_Order = Workorder;
                    model.WNFR_To = Data.to;
                    model.WNFR_Date_Rec_Contact = Data.date_rep_contact;
                    model.WNFR_Due_Date = Data.date_due;
                    model.WNFR_MTG_Co = Data.mtg_co;
                    model.WNFR_Account = Data.account;
                    model.WNFR_LoanType = Data.loan_type;
                    model.WNFR_MTGR_Name = Data.mtgr_name;
                    model.WNFR_MTGR_Address = Data.mtgr_address;
                    model.WNFR_Requierment_Name = Data.requirement_name;
                    model.WNFR_Requierment_Details = Data.requirement_details;
                    model.WNFR_WorkType = Data.requirement_name;


                    model.WNFR_City = Data.city;
                    model.WNFR_State = Data.state;
                    model.WNFR_Zip = Data.zip;
                    model.WNFR_wo_id = Data.wo_id;
                    model.WNFR_username = Data.username;
                    model.WNFR_keycode = Data.keycode;
                    model.WNFR_lockbox = Data.lockbox;
                    model.WNFR_address = Data.address;
                    model.WNFR_NFR_ID = Data.wo_id;


                    model.WNFR_IsActive = true;
                    model.WNFR_IsDelete = false;
                    model.WNFR_IsProcessed = false;
                    model.Type = 1;
                    model.WNFR_ImportMaster_Pkey = WI_Pkey_ID;
                    GoogleLocation GoogleLocation = new GoogleLocation();
                    GoogleLocationDTO googleLocationDTO = new GoogleLocationDTO();
                    WorkOrderMasterData workOrderMasterData = new WorkOrderMasterData();
                    workOrderxDTO workOrderxDTO = new workOrderxDTO();
                    workOrderxDTO.address1 = model.WNFR_MTGR_Address;
                    workOrderxDTO.Type = 2;
                    DataSet ds=  workOrderMasterData.Get_WorkOrderLatLongByAddress(workOrderxDTO);

                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                model.WNFR_gpsLatitude = ds.Tables[0].Rows[i]["gpsLatitude"].ToString();
                                model.WNFR_gpsLongitude = ds.Tables[0].Rows[i]["gpsLongitude"].ToString();
                            }
                        }
                        else
                        {
                           
                            googleLocationDTO.Address = model.WNFR_MTGR_Address;
                            googleLocationDTO = GoogleLocation.GetLatitudeLogitudeWithAPIKey(googleLocationDTO);
                            model.WNFR_gpsLatitude = googleLocationDTO.Latitude;
                            model.WNFR_gpsLongitude = googleLocationDTO.Longitude;
                        }
                    }
                    else
                    {
                        
                        googleLocationDTO.Address = model.WNFR_MTGR_Address;
                        googleLocationDTO = GoogleLocation.GetLatitudeLogitudeWithAPIKey(googleLocationDTO);
                        model.WNFR_gpsLatitude = googleLocationDTO.Latitude;
                        model.WNFR_gpsLongitude = googleLocationDTO.Longitude;
                    }
                       
                    model.WNFR_Import_File_Name = Filename;
                    model.WNFR_Import_FilePath = FilePath;
                    model.WNFR_Import_Pdf_Name = pdfname;
                    model.WNFR_Import_Pdf_Path = PdfFilePath;
                    model.UserID = UserID;
                    objDynamic = workOrder_NFR_MasterData.AddWorkOrder_NFRData(model);
                    if (objDynamic[0].Status == "1")
                    {

                        //for new json
                        WorkOrder_NFR_Item_MasterDTO workOrder_NFR_Item_MasterDTO = new WorkOrder_NFR_Item_MasterDTO();
                        if (!string.IsNullOrEmpty(Data.requirement_name) || !string.IsNullOrEmpty(Data.requirement_details))
                        {
                            workOrder_NFR_Item_MasterDTO.WNFRINS_FkeyID = Convert.ToInt64(objDynamic[0].WNFR_PkeyID);
                            workOrder_NFR_Item_MasterDTO.WNFRINS_Ins_Name = Data.requirement_name;
                            workOrder_NFR_Item_MasterDTO.WNFRINS_Ins_Details = Data.requirement_details;
                            workOrder_NFR_Item_MasterDTO.Type = 1;
                            workOrder_NFR_Item_MasterDTO.UserID = UserID;
                            var ObjItemData = workOrder_NFR_MasterData.AddWorkOrder_NFRItemData(workOrder_NFR_Item_MasterDTO);
                        }
                        else
                        {
                            log.logDebugMessage("------------------WorkOrder Item Null Record Found Start -------------" + objDynamic[0].ToString());
                            log.logDebugMessage(workOrder_NFR_Item_MasterDTO.WNFRINS_Ins_Name);
                            log.logDebugMessage(workOrder_NFR_Item_MasterDTO.WNFRINS_Ins_Details);
                            log.logDebugMessage("------------------WorkOrder Item Null Record Found End -------------" + objDynamic[0].ToString());
                        }



                        if(Data.instructions != null)
                        {
                            for (int i = 0; i < Data.instructions.Count; i++)
                            {
                                NFRInstruction nFRInstruction = new NFRInstruction();
                                nFRInstruction = Data.instructions[i];
                                workOrder_NFR_Item_MasterDTO.Type = 1;


                                if (!string.IsNullOrEmpty(nFRInstruction.instruction_name) || !string.IsNullOrEmpty(nFRInstruction.instruction_details))
                                {
                                    workOrder_NFR_Item_MasterDTO.WNFRINS_FkeyID = Convert.ToInt64(objDynamic[0].WNFR_PkeyID);
                                    workOrder_NFR_Item_MasterDTO.WNFRINS_Ins_Name = nFRInstruction.instruction_name;
                                    workOrder_NFR_Item_MasterDTO.WNFRINS_Ins_Details = nFRInstruction.instruction_details;
                                    var ObjItemData = workOrder_NFR_MasterData.AddWorkOrder_NFRItemData(workOrder_NFR_Item_MasterDTO);
                                }
                                else
                                {
                                    log.logDebugMessage("------------------WorkOrder Item Instruction Null Record Found -------------" );
                                    
                                    
                                }
                            }
                        }
                        else
                        {
                            log.logDebugMessage("------------------WorkOrder Item Null Record Found Start -------------" + objDynamic[0].ToString());
                            log.logDebugMessage(workOrder_NFR_Item_MasterDTO.WNFRINS_Ins_Name);
                            log.logDebugMessage(workOrder_NFR_Item_MasterDTO.WNFRINS_Ins_Details);
                            log.logDebugMessage("------------------WorkOrder Item Null Record Found End -------------" + objDynamic[0].ToString());
                        }

                    }
                }
                else
                {

                    log.logErrorMessage("------------------InValid Json -------------");
                    log.logErrorMessage(Filename);
                    log.logErrorMessage(WorkOrderMaster_DTO);
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage("------------------Json Error Start-------------");
                log.logErrorMessage(Filename);
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage("------------------Json Error  End-------------");

            }
            return objDynamic;
        }


        //MCS record insert
        private DataSet GetMCSWorkOrderMasterData(WorkOrderMaster_API_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[GetWorkOrderMCSMaster_Data]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@WMCS_PkeyID", 1 + "#bigint#" + model.Pkey_Id);
                input_parameters.Add("@WhereClause", 1 + "#varchar#" + model.WhereClause);
                input_parameters.Add("@PageNumber", 1 + "#int#" + model.PageNumber);
                input_parameters.Add("@NoofRows", 1 + "#int#" + model.NoofRows);
                input_parameters.Add("@Imrt_PkeyId", 1 + "#bigint#" + model.Imrt_PkeyId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ds;
        }
        //MCS Details 

        public List<dynamic> MCSWorkOrderItemDetails(string WorkOrderMaster_DTO, Int64 WI_Pkey_ID, string Filename, string FilePath, string PdfFilePath, string pdfname, Int64 UserID)
        {
            WorkOrder_MCS_MasterData WorkOrder_MCS_MasterData = new WorkOrder_MCS_MasterData();

            List<dynamic> objDynamic = new List<dynamic>();

            try
            {

                JsonValidate jsonValidate = new JsonValidate();
                string Formatjson = WorkOrderMaster_DTO;
                bool val = jsonValidate.IsValidJson(Formatjson);
                if (val)
                {
                    var Data = JsonConvert.DeserializeObject<WorkOrder_MCSItem_MasterDTO>(Formatjson);
                    //var Workorder = Data.work_order;
                    var Workorder = Data.wo;
                    WorkOrder_MCS_MasterDTO model = new WorkOrder_MCS_MasterDTO();
                    model.WMCS_PkeyID = 0;
                    model.WMCS_wo_id = Workorder;
                    model.WMCS_address = Data.address;
                    model.WMCS_city = Data.city;
                    model.WMCS_state = Data.state;
                    model.WMCS_zip = Data.zip;
                    model.WMCS_loan_number = Data.loan_number;
                    model.WMCS_loan_type = Data.loan_type;
                    model.WMCS_due_date = Data.due_date;
                    model.WMCS_username = Data.username;
                    model.WMCS_lot_size = Data.lot_size;
                    model.WMCS_received_date = Data.received_date;
                    model.WMCS_customer = Data.customer;


                    model.WMCS_work_type = Data.work_type;
                    model.WMCS_lock_code = Data.lock_code;
                    model.WMCS_key_code = Data.key_code;
                    model.WMCS_mortgager = Data.mortgager;
                    model.WMCS_id = Data.wo_id;

                    model.WMCS_Comments = Data.comments;
                    model.WMCS_IsActive = true;
                    model.WMCS_IsDelete = false;
                    model.WMCS_IsProcessed = false;
                    model.Type = 1;
                    model.WMCS_ImportMaster_Pkey = WI_Pkey_ID;
                    GoogleLocation GoogleLocation = new GoogleLocation();
                    GoogleLocationDTO googleLocationDTO = new GoogleLocationDTO();
                    WorkOrderMasterData workOrderMasterData = new WorkOrderMasterData();
                    workOrderxDTO workOrderxDTO = new workOrderxDTO();
                    workOrderxDTO.address1 = model.WMCS_address + " " + model.WMCS_city + " " + model.WMCS_state + " " + model.WMCS_zip; ;
                    workOrderxDTO.Type = 2;
                    DataSet ds = workOrderMasterData.Get_WorkOrderLatLongByAddress(workOrderxDTO);

                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                model.WMCS_gpsLatitude = ds.Tables[0].Rows[i]["gpsLatitude"].ToString();
                                model.WMCS_gpsLongitude = ds.Tables[0].Rows[i]["gpsLongitude"].ToString();
                            }
                        }
                        else
                        {

                            googleLocationDTO.Address = model.WMCS_address + " " + model.WMCS_city + " " + model.WMCS_state + " " + model.WMCS_zip;
                            googleLocationDTO = GoogleLocation.GetLatitudeLogitudeWithAPIKey(googleLocationDTO);
                            model.WMCS_gpsLatitude = googleLocationDTO.Latitude;
                            model.WMCS_gpsLongitude = googleLocationDTO.Longitude;
                        }
                    }
                    else
                    {

                        googleLocationDTO.Address = model.WMCS_address + " " + model.WMCS_city + " " + model.WMCS_state + " " + model.WMCS_zip; ;
                        googleLocationDTO = GoogleLocation.GetLatitudeLogitudeWithAPIKey(googleLocationDTO);
                        model.WMCS_gpsLatitude = googleLocationDTO.Latitude;
                        model.WMCS_gpsLongitude = googleLocationDTO.Longitude;
                    }

                    model.WMCS_Import_File_Name = Filename;
                    model.WMCS_Import_FilePath = FilePath;
                    model.WMCS_Import_Pdf_Name = pdfname;
                    model.WMCS_Import_Pdf_Path = PdfFilePath;
                    model.UserID = UserID;
                    objDynamic = WorkOrder_MCS_MasterData.AddWorkOrderMCSData(model);
                    if (objDynamic[0].Status == "1")
                    {

                        //for new json

                        if (Data.work_order_item_details != null)
                        {
                            for (int i = 0; i < Data.work_order_item_details.Count; i++)
                            {
                                if (!string.IsNullOrEmpty(Data.work_order_item_details[i].instruction_name) || !string.IsNullOrEmpty(Data.work_order_item_details[i].description))
                                {

                                    WorkOrder_MCS_Item_MasterDTO WorkOrder_MCS_Item_MasterDTO = new WorkOrder_MCS_Item_MasterDTO();
                                    if (!string.IsNullOrWhiteSpace(objDynamic[0].WMCS_PkeyID))
                                    {
                                        WorkOrder_MCS_Item_MasterDTO.WMCSINS_FkeyID = Convert.ToInt64(objDynamic[0].WMCS_PkeyID);
                                        WorkOrder_MCS_Item_MasterDTO.WMCSINS_Ins_Name = Data.work_order_item_details[i].instruction_name;
                                        WorkOrder_MCS_Item_MasterDTO.WMCSINS_Ins_Details = Data.work_order_item_details[i].description;
                                        WorkOrder_MCS_Item_MasterDTO.WMCSINS_Additional_Details = Data.work_order_item_details[i].Additional_Instructions;
                                        WorkOrder_MCS_Item_MasterDTO.Type = 1;
                                        WorkOrder_MCS_Item_MasterDTO.UserID = UserID;
                                        WorkOrder_MCS_Item_MasterDTO.WMCSINS_IsActive = true;
                                        WorkOrder_MCS_Item_MasterDTO.WMCSINS_IsDelete = false;
                                        if (!string.IsNullOrWhiteSpace(Data.work_order_item_details[i].Qty))
                                        {
                                            WorkOrder_MCS_Item_MasterDTO.WMCSINS_Qty = Convert.ToInt64(Data.work_order_item_details[i].Qty);
                                        }
                                        if (!string.IsNullOrWhiteSpace(Data.work_order_item_details[i].Price))
                                        {
                                            WorkOrder_MCS_Item_MasterDTO.WMCSINS_Price = Convert.ToDecimal(Data.work_order_item_details[i].Price);
                                        }
                                        if (!string.IsNullOrWhiteSpace(Data.work_order_item_details[i].Total))
                                        {
                                            WorkOrder_MCS_Item_MasterDTO.WMCSINS_Total = Convert.ToDecimal(Data.work_order_item_details[i].Total);
                                        }

                                        WorkOrder_MCS_MasterData.AddWorkOrderMCSItemData(WorkOrder_MCS_Item_MasterDTO);
                                    }
                                    else
                                    {
                                        log.logDebugMessage("-----------------Error Occured while Adding Master-------------");
                                    }


                                }


                                else
                                {
                                    log.logDebugMessage("------------------WorkOrder Item Instruction Null Record Found -------------");


                                }
                            }
                        }
                        else
                        {
                            log.logDebugMessage("------------------WorkOrder Item Null Record Found Start -------------" + objDynamic[0].ToString());
                            log.logDebugMessage("------------------WorkOrder Item Null Record Found End -------------" + objDynamic[0].ToString());
                        }

                    }
                }
                else
                {

                    log.logErrorMessage("------------------InValid Json -------------");
                    log.logErrorMessage(Filename);
                    log.logErrorMessage(WorkOrderMaster_DTO);
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage("------------------Json Error Start-------------");
                log.logErrorMessage(Filename);
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage("------------------Json Error  End-------------");

            }
            return objDynamic;
        }

        //AG record insert
        private DataSet GetAGWorkOrderMasterData(WorkOrderMaster_API_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[GetWorkOrderAGMaster_Data]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@WAG_PkeyID", 1 + "#bigint#" + model.Pkey_Id);
                input_parameters.Add("@WhereClause", 1 + "#varchar#" + model.WhereClause);
                input_parameters.Add("@PageNumber", 1 + "#int#" + model.PageNumber);
                input_parameters.Add("@NoofRows", 1 + "#int#" + model.NoofRows);
                input_parameters.Add("@Imrt_PkeyId", 1 + "#bigint#" + model.Imrt_PkeyId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ds;
        }

        //AG Details

        public List<dynamic> AGWorkOrderItemDetails(string WorkOrderMaster_DTO, Int64 WI_Pkey_ID, string Filename, string FilePath, string PdfFilePath, string pdfname, Int64 UserID)
        {
            WorkOrder_AG_MasterData WorkOrder_AG_MasterData = new WorkOrder_AG_MasterData();

            List<dynamic> objDynamic = new List<dynamic>();

            try
            {

                JsonValidate jsonValidate = new JsonValidate();
                string Formatjson = WorkOrderMaster_DTO;
                bool val = jsonValidate.IsValidJson(Formatjson);
                if (val)
                {
                    var Data = JsonConvert.DeserializeObject<WorkOrder_AGItem_MasterDTO>(Formatjson);
                    //var Workorder = Data.work_order;
                    var Workorder = Data.wo;
                    WorkOrder_AG_MasterDTO model = new WorkOrder_AG_MasterDTO();
                    model.WAG_PkeyID = 0;
                    model.WAG_wo_id = Workorder;
                    model.WAG_address = Data.address;
                    model.WAG_city = Data.city;
                    model.WAG_state = Data.state;
                    model.WAG_zip = Data.zip;
                    model.WAG_loan_number = Data.loan_number;
                    model.WAG_loan_type = Data.loan_type;
                    model.WAG_due_date = Data.due_date;
                    model.WAG_username = Data.username;
                    model.WAG_lot_size = Data.lot_size;
                    model.WAG_received_date = Data.received_date;
                    model.WAG_customer = Data.customer;


                    model.WAG_work_type = Data.work_type;
                    model.WAG_lock_code = Data.lock_code;
                    model.WAG_key_code = Data.key_code;
                    model.WAG_mortgager = Data.mortgager;
                    model.WAG_id = Data.wo_id;

                    model.WAG_Comments = Data.comments;
                    model.WAG_IsActive = true;
                    model.WAG_IsDelete = false;
                    model.WAG_IsProcessed = false;
                    model.Type = 1;
                    model.WAG_ImportMaster_Pkey = WI_Pkey_ID;
                    GoogleLocation GoogleLocation = new GoogleLocation();
                    GoogleLocationDTO googleLocationDTO = new GoogleLocationDTO();
                    WorkOrderMasterData workOrderMasterData = new WorkOrderMasterData();
                    workOrderxDTO workOrderxDTO = new workOrderxDTO();
                    workOrderxDTO.address1 = model.WAG_address + " " + model.WAG_city + " " + model.WAG_state + " " + model.WAG_zip; ;
                    workOrderxDTO.Type = 2;
                    DataSet ds = workOrderMasterData.Get_WorkOrderLatLongByAddress(workOrderxDTO);

                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                model.WAG_gpsLatitude = ds.Tables[0].Rows[i]["gpsLatitude"].ToString();
                                model.WAG_gpsLongitude = ds.Tables[0].Rows[i]["gpsLongitude"].ToString();
                            }
                        }
                        else
                        {

                            googleLocationDTO.Address = model.WAG_address + " " + model.WAG_city + " " + model.WAG_state + " " + model.WAG_zip;
                            googleLocationDTO = GoogleLocation.GetLatitudeLogitudeWithAPIKey(googleLocationDTO);
                            model.WAG_gpsLatitude = googleLocationDTO.Latitude;
                            model.WAG_gpsLongitude = googleLocationDTO.Longitude;
                        }
                    }
                    else
                    {

                        googleLocationDTO.Address = model.WAG_address + " " + model.WAG_city + " " + model.WAG_state + " " + model.WAG_zip; ;
                        googleLocationDTO = GoogleLocation.GetLatitudeLogitudeWithAPIKey(googleLocationDTO);
                        model.WAG_gpsLatitude = googleLocationDTO.Latitude;
                        model.WAG_gpsLongitude = googleLocationDTO.Longitude;
                    }

                    model.WAG_Import_File_Name = Filename;
                    model.WAG_Import_FilePath = FilePath;
                    model.WAG_Import_Pdf_Name = pdfname;
                    model.WAG_Import_Pdf_Path = PdfFilePath;
                    model.UserID = UserID;
                    objDynamic = WorkOrder_AG_MasterData.AddWorkOrderAGData(model);
                    if (objDynamic[0].Status == "1")
                    {

                        //for new json

                        if (Data.work_order_item_details != null)
                        {
                            for (int i = 0; i < Data.work_order_item_details.Count; i++)
                            {
                                if (!string.IsNullOrEmpty(Data.work_order_item_details[i].Description) || !string.IsNullOrEmpty(Data.work_order_item_details[i].Description))
                                {

                                    WorkOrder_AG_Item_MasterDTO WorkOrder_AG_Item_MasterDTO = new WorkOrder_AG_Item_MasterDTO();
                                    if (!string.IsNullOrWhiteSpace(objDynamic[0].WAG_PkeyID))
                                    {
                                        WorkOrder_AG_Item_MasterDTO.WAGINS_FkeyID = Convert.ToInt64(objDynamic[0].WAG_PkeyID);
                                        WorkOrder_AG_Item_MasterDTO.WAGINS_Ins_Name = Data.work_order_item_details[i].Description;
                                        WorkOrder_AG_Item_MasterDTO.WAGINS_Ins_Details = Data.work_order_item_details[i].Description;
                                        WorkOrder_AG_Item_MasterDTO.WAGINS_Additional_Details = Data.work_order_item_details[i].Additional_Instructions;
                                        if (!string.IsNullOrWhiteSpace(Data.work_order_item_details[i].Qty))
                                        {
                                            WorkOrder_AG_Item_MasterDTO.WAGINS_Qty = Convert.ToInt64(Data.work_order_item_details[i].Qty);
                                        }
                                        if (!string.IsNullOrWhiteSpace(Data.work_order_item_details[i].Price))
                                        {
                                            WorkOrder_AG_Item_MasterDTO.WAGINS_Price = Convert.ToInt64(Data.work_order_item_details[i].Price);
                                        }
                                        if (!string.IsNullOrWhiteSpace(Data.work_order_item_details[i].Total))
                                        {
                                            WorkOrder_AG_Item_MasterDTO.WAGINS_Total = Convert.ToInt64(Data.work_order_item_details[i].Total);
                                        }


                                        WorkOrder_AG_Item_MasterDTO.Type = 1;
                                        WorkOrder_AG_Item_MasterDTO.UserID = UserID;
                                        WorkOrder_AG_Item_MasterDTO.WAGINS_IsActive = true;
                                        WorkOrder_AG_Item_MasterDTO.WAGINS_IsDelete = false;
                                        WorkOrder_AG_MasterData.AddWorkOrderAGItemData(WorkOrder_AG_Item_MasterDTO);
                                    }
                                    else
                                    {
                                        log.logDebugMessage("-----------------Error Occured while Adding Master-------------");
                                    }


                                }


                                else
                                {
                                    log.logDebugMessage("------------------WorkOrder Item Instruction Null Record Found -------------");


                                }
                            }
                        }
                        else
                        {
                            log.logDebugMessage("------------------WorkOrder Item Null Record Found Start -------------" + objDynamic[0].ToString());
                            log.logDebugMessage("------------------WorkOrder Item Null Record Found End -------------" + objDynamic[0].ToString());
                        }

                    }
                }
                else
                {

                    log.logErrorMessage("------------------InValid Json -------------");
                    log.logErrorMessage(Filename);
                    log.logErrorMessage(WorkOrderMaster_DTO);
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage("------------------Json Error Start-------------");
                log.logErrorMessage(Filename);
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage("------------------Json Error  End-------------");

            }
            return objDynamic;
        }

        //Altisource record insert
        private DataSet GetWorkOrderAltisourceMasterData(WorkOrderMaster_API_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[GetWorkOrderAltisourceMaster_Data]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@WAltisource_PkeyID", 1 + "#bigint#" + model.Pkey_Id);
                input_parameters.Add("@WhereClause", 1 + "#varchar#" + model.WhereClause);
                input_parameters.Add("@PageNumber", 1 + "#int#" + model.PageNumber);
                input_parameters.Add("@NoofRows", 1 + "#int#" + model.NoofRows);
                input_parameters.Add("@Imrt_PkeyId", 1 + "#bigint#" + model.Imrt_PkeyId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ds;
        }

        // Altisource Details

        public List<dynamic> AltisourceWorkOrderItemDetails(string WorkOrderMaster_DTO, Int64 WI_Pkey_ID, string Filename, string FilePath, string PdfFilePath, string pdfname, Int64 UserID)
        {
            WorkOrder_Altisource_MasterData WorkOrder_Altisource_MasterData = new WorkOrder_Altisource_MasterData();

            List<dynamic> objDynamic = new List<dynamic>();

            try
            {

                JsonValidate jsonValidate = new JsonValidate();
                string Formatjson = WorkOrderMaster_DTO;
                bool val = jsonValidate.IsValidJson(Formatjson);
                if (val)
                {
                    var Data = JsonConvert.DeserializeObject<WorkOrder_AltisourceItem_MasterDTO>(Formatjson);
                    //var Workorder = Data.work_order;
                    var Workorder = Data.wo;
                    WorkOrder_Altisource_MasterDTO model = new WorkOrder_Altisource_MasterDTO();
                    model.WAltisource_PkeyID = 0;
                    model.WAltisource_wo_id = Workorder;
                    model.WAltisource_category = Data.category;
                    model.WAltisource_address = Data.address;
                    model.WAltisource_city = Data.city;
                    model.WAltisource_state = Data.state;
                    model.WAltisource_zip = Data.zip;
                    model.WAltisource_estimated_complete_date = Data.estimated_complete_date;
                    model.WAltisource_loan_number = Data.loan_number;
                    model.WAltisource_loan_type = Data.loan_type;
                    model.WAltisource_due_date = Data.due_date;
                    model.WAltisource_client_Due_date = Data.client_Due_date;
                    model.WAltisource_username = Data.username;
                    model.WAltisource_lot_size = Data.lot_size;
                    model.WAltisource_received_date = Data.received_date;
                    model.WAltisource_customer = Data.customer;
                    model.WAltisource_client_company = Data.client_company;
                    model.WAltisource_ppw = Data.ppw;
                    model.WAltisource_start_date = Data.start_date;
                    model.WAltisource_contractor = Data.contractor;
                    model.WAltisource_missing_info = Data.missing_info;


                    model.WAltisource_work_type = Data.work_type;
                    model.WAltisource_assigned_admin = Data.assigned_admin;
                    model.WAltisource_lock_code = Data.lock_code;
                    model.WAltisource_key_code = Data.key_code;
                    model.WAltisource_wo_status = Data.wo_status;
                    model.WAltisource_mortgager = Data.mortgager;
                    model.WAltisource_ready_for_office_date = Data.ready_for_office_date;
                    model.WAltisource_id = Data.wo_id;

                    model.WAltisource_comments = Data.comments;
                    model.WAltisource_IsActive = true;
                    model.WAltisource_IsDelete = false;
                    model.WAltisource_IsProcessed = false;
                    model.Type = 1;
                    model.WAltisource_ImportMaster_Pkey = WI_Pkey_ID;
                    GoogleLocation GoogleLocation = new GoogleLocation();
                    GoogleLocationDTO googleLocationDTO = new GoogleLocationDTO();
                    WorkOrderMasterData workOrderMasterData = new WorkOrderMasterData();
                    workOrderxDTO workOrderxDTO = new workOrderxDTO();
                    workOrderxDTO.address1 = model.WAltisource_address + " " + model.WAltisource_city + " " + model.WAltisource_state + " " + model.WAltisource_zip; ;
                    workOrderxDTO.Type = 2;
                    DataSet ds = workOrderMasterData.Get_WorkOrderLatLongByAddress(workOrderxDTO);

                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                model.WAltisource_gpsLatitude = ds.Tables[0].Rows[i]["gpsLatitude"].ToString();
                                model.WAltisource_gpsLongitude = ds.Tables[0].Rows[i]["gpsLongitude"].ToString();
                            }
                        }
                        else
                        {

                            googleLocationDTO.Address = model.WAltisource_address + " " + model.WAltisource_city + " " + model.WAltisource_state + " " + model.WAltisource_zip;
                            googleLocationDTO = GoogleLocation.GetLatitudeLogitudeWithAPIKey(googleLocationDTO);
                            model.WAltisource_gpsLatitude = googleLocationDTO.Latitude;
                            model.WAltisource_gpsLongitude = googleLocationDTO.Longitude;
                        }
                    }
                    else
                    {

                        googleLocationDTO.Address = model.WAltisource_address + " " + model.WAltisource_city + " " + model.WAltisource_state + " " + model.WAltisource_zip; ;
                        googleLocationDTO = GoogleLocation.GetLatitudeLogitudeWithAPIKey(googleLocationDTO);
                        model.WAltisource_gpsLatitude = googleLocationDTO.Latitude;
                        model.WAltisource_gpsLongitude = googleLocationDTO.Longitude;
                    }

                    model.WAltisource_Import_File_Name = Filename;
                    model.WAltisource_Import_FilePath = FilePath;
                    model.WAltisource_Import_Pdf_Name = pdfname;
                    model.WAltisource_Import_Pdf_Path = PdfFilePath;
                    model.UserID = UserID;
                    objDynamic = WorkOrder_Altisource_MasterData.AddWorkOrderAltisourceData(model);
                    if (objDynamic[0].Status == "1")
                    {

                        //for new json

                        if (Data.work_order_item_details != null)
                        {
                            for (int i = 0; i < Data.work_order_item_details.Count; i++)
                            {
                                if (!string.IsNullOrEmpty(Data.work_order_item_details[i].Description) || !string.IsNullOrEmpty(Data.work_order_item_details[i].Description))
                                {

                                    WorkOrder_Altisource_Item_MasterDTO WorkOrder_Altisource_Item_MasterDTO = new WorkOrder_Altisource_Item_MasterDTO();
                                    if (!string.IsNullOrWhiteSpace(objDynamic[0].WAltisource_PkeyID))
                                    {
                                        WorkOrder_Altisource_Item_MasterDTO.WALTISOURCEINS_FkeyID = Convert.ToInt64(objDynamic[0].WAltisource_PkeyID);
                                        WorkOrder_Altisource_Item_MasterDTO.WALTISOURCEINS_Ins_Name = Data.work_order_item_details[i].Description;
                                        WorkOrder_Altisource_Item_MasterDTO.WALTISOURCEINS_Ins_Details = Data.work_order_item_details[i].Description;
                                        WorkOrder_Altisource_Item_MasterDTO.WALTISOURCEINS_Additional_Details = Data.work_order_item_details[i].Additional_Instructions;
                                        if (!string.IsNullOrWhiteSpace(Data.work_order_item_details[i].Qty))
                                        {
                                            WorkOrder_Altisource_Item_MasterDTO.WALTISOURCEINS_Qty = Convert.ToInt64(Data.work_order_item_details[i].Qty);
                                        }
                                        if (!string.IsNullOrWhiteSpace(Data.work_order_item_details[i].Price))
                                        {
                                            WorkOrder_Altisource_Item_MasterDTO.WALTISOURCEINS_Price = Convert.ToInt64(Data.work_order_item_details[i].Price);
                                        }
                                        if (!string.IsNullOrWhiteSpace(Data.work_order_item_details[i].Total))
                                        {
                                            WorkOrder_Altisource_Item_MasterDTO.WALTISOURCEINS_Total = Convert.ToInt64(Data.work_order_item_details[i].Total);
                                        }


                                        WorkOrder_Altisource_Item_MasterDTO.Type = 1;
                                        WorkOrder_Altisource_Item_MasterDTO.UserID = UserID;
                                        WorkOrder_Altisource_Item_MasterDTO.WALTISOURCEINS_IsActive = true;
                                        WorkOrder_Altisource_Item_MasterDTO.WALTISOURCEINS_IsDelete = false;
                                        WorkOrder_Altisource_MasterData.AddWorkOrderAltisourceItemData(WorkOrder_Altisource_Item_MasterDTO);
                                    }
                                    else
                                    {
                                        log.logDebugMessage("-----------------Error Occured while Adding Master-------------");
                                    }


                                }


                                else
                                {
                                    log.logDebugMessage("------------------WorkOrder Item Instruction Null Record Found -------------");


                                }
                            }
                        }
                        else
                        {
                            log.logDebugMessage("------------------WorkOrder Item Null Record Found Start -------------" + objDynamic[0].ToString());
                            log.logDebugMessage("------------------WorkOrder Item Null Record Found End -------------" + objDynamic[0].ToString());
                        }

                    }
                }
                else
                {

                    log.logErrorMessage("------------------InValid Json -------------");
                    log.logErrorMessage(Filename);
                    log.logErrorMessage(WorkOrderMaster_DTO);
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage("------------------Json Error Start-------------");
                log.logErrorMessage(Filename);
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage("------------------Json Error  End-------------");

            }
            return objDynamic;
        }
        private DataSet GetWorkOrderCyprexxMasterData(WorkOrderMaster_API_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[GetWorkOrderCyprexxMaster_Data]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@WCyprexx_PkeyID", 1 + "#bigint#" + model.Pkey_Id);
                input_parameters.Add("@Imrt_PkeyId", 1 + "#bigint#" + model.Imrt_PkeyId);
                input_parameters.Add("@WhereClause", 1 + "#varchar#" + model.WhereClause);
                input_parameters.Add("@PageNumber", 1 + "#int#" + model.PageNumber);
                input_parameters.Add("@NoofRows", 1 + "#int#" + model.NoofRows);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ds;
        }
        //Cyprexx record insert
        public List<dynamic> CyprexxkOrderItemDetails(string WorkOrderMaster_DTO, Int64 WI_Pkey_ID, string Filename, string FilePath, string PdfFilePath, string pdfname,Int64 UserID)
        {
            WorkOrder_Cyprexx_MasterData workOrder_Cyprexx_MasterData = new WorkOrder_Cyprexx_MasterData();

            List<dynamic> objDynamic = new List<dynamic>();

            try
            {

                JsonValidate jsonValidate = new JsonValidate();
                string Formatjson = WorkOrderMaster_DTO;
                bool val = jsonValidate.IsValidJson(Formatjson);
                if (val)
                {
                    var Data = JsonConvert.DeserializeObject<WorkOrder_CyprexxItem_MasterDTO>(Formatjson);
                    //var Workorder = Data.work_order;
                    var Workorder = Data.wo;
                    WorkOrder_Cyprexx_MasterDTO model = new WorkOrder_Cyprexx_MasterDTO();
                    model.WCyprexx_PkeyID = 0;
                    model.WCyprexx_wo_id = Workorder;
                    model.WCyprexx_address = Data.address;
                    model.WCyprexx_City = Data.city;
                    model.WCyprexx_State = Data.state;
                    model.WCyprexx_Zip = Data.zip;
                    model.WCyprexx_Due_Date = Data.due_date;
                    model.WCyprexx_Received_Date = Data.received_date;
                    model.WCyprexx_Customer = Data.customer;
                    model.WCyprexx_Loan_Type = Data.loan_type;
                    model.WCyprexx_Lock_Code = Data.lock_code;
                    model.WCyprexx_Key_Code = Data.key_code;
                    model.WCyprexx_Comments = Data.comments;
                    model.WCyprexx_IsActive = true;
                    model.WCyprexx_IsDelete = false;
                    model.WCyprexx_IsProcessed = false;
                    model.Type = 1;
                    model.WCyprexx_ImportMaster_Pkey = WI_Pkey_ID;
                    GoogleLocation GoogleLocation = new GoogleLocation();
                    GoogleLocationDTO googleLocationDTO = new GoogleLocationDTO();
                    WorkOrderMasterData workOrderMasterData = new WorkOrderMasterData();
                    workOrderxDTO workOrderxDTO = new workOrderxDTO();
                    workOrderxDTO.address1 = model.WCyprexx_address + " " + model.WCyprexx_City + " " + model.WCyprexx_State + " " + model.WCyprexx_Zip; ;
                    workOrderxDTO.Type = 2;
                    DataSet ds = workOrderMasterData.Get_WorkOrderLatLongByAddress(workOrderxDTO);

                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                model.WCyprexx_gpsLatitude = ds.Tables[0].Rows[i]["gpsLatitude"].ToString();
                                model.WCyprexx_gpsLongitude = ds.Tables[0].Rows[i]["gpsLongitude"].ToString();
                            }
                        }
                        else
                        {

                            googleLocationDTO.Address = model.WCyprexx_address + " " + model.WCyprexx_City + " " + model.WCyprexx_State + " " + model.WCyprexx_Zip;
                            googleLocationDTO = GoogleLocation.GetLatitudeLogitudeWithAPIKey(googleLocationDTO);
                            model.WCyprexx_gpsLatitude = googleLocationDTO.Latitude;
                            model.WCyprexx_gpsLongitude = googleLocationDTO.Longitude;
                        }
                    }
                    else
                    {

                        googleLocationDTO.Address = model.WCyprexx_address + " " + model.WCyprexx_City + " " + model.WCyprexx_State + " " + model.WCyprexx_Zip; ;
                        googleLocationDTO = GoogleLocation.GetLatitudeLogitudeWithAPIKey(googleLocationDTO);
                        model.WCyprexx_gpsLatitude = googleLocationDTO.Latitude;
                        model.WCyprexx_gpsLongitude = googleLocationDTO.Longitude;
                    }

                    model.WCyprexx_Import_File_Name = Filename;
                    model.WCyprexx_Import_FilePath = FilePath;
                    model.WCyprexx_Import_Pdf_Name = pdfname;
                    model.WCyprexx_Import_Pdf_Path = PdfFilePath;
                    model.UserID = UserID;
                    objDynamic = workOrder_Cyprexx_MasterData.AddWorkOrderCyprexxData(model);
                    if (objDynamic[0].Status == "1")
                    {

                        //for new json
                       
                        if (Data.work_order_item_details != null)
                        {
                            for (int i = 0; i < Data.work_order_item_details.Count; i++)
                            {
                                if (!string.IsNullOrEmpty(Data.work_order_item_details[i].instruction_name) || !string.IsNullOrEmpty(Data.work_order_item_details[i].description))
                                {
                                   
                                    WorkOrder_Cyprexx_Item_MasterDTO workOrder_Cyprexx_Item_MasterDTO = new WorkOrder_Cyprexx_Item_MasterDTO();
                                    if (!string.IsNullOrWhiteSpace(objDynamic[0].WCyprexx_PkeyID))
                                    {
                                        workOrder_Cyprexx_Item_MasterDTO.WCYPREXXINS_FkeyID = Convert.ToInt64(objDynamic[0].WCyprexx_PkeyID);
                                        workOrder_Cyprexx_Item_MasterDTO.WCYPREXXINS_Ins_Name = Data.work_order_item_details[i].instruction_name;
                                        workOrder_Cyprexx_Item_MasterDTO.WCYPREXXINS_Ins_Details = Data.work_order_item_details[i].description;
                                        workOrder_Cyprexx_Item_MasterDTO.WCYPREXXINS_Additional_Details = Data.work_order_item_details[i].additional_details;
                                        workOrder_Cyprexx_Item_MasterDTO.Type = 1;
                                        workOrder_Cyprexx_Item_MasterDTO.UserID = UserID;
                                        workOrder_Cyprexx_Item_MasterDTO.WCYPREXXINS_IsActive = true;
                                        workOrder_Cyprexx_Item_MasterDTO.WCYPREXXINS_IsDelete = false;
                                        workOrder_Cyprexx_MasterData.AddWorkOrderCyprexxItemData(workOrder_Cyprexx_Item_MasterDTO);
                                    }
                                    else
                                    {
                                        log.logDebugMessage("-----------------Error Occured while Adding Master-------------");
                                    }
                                    
                                    
                                }


                                else
                                {
                                    log.logDebugMessage("------------------WorkOrder Item Instruction Null Record Found -------------");


                                }
                            }
                        }
                        else
                        {
                            log.logDebugMessage("------------------WorkOrder Item Null Record Found Start -------------" + objDynamic[0].ToString());
                            log.logDebugMessage("------------------WorkOrder Item Null Record Found End -------------" + objDynamic[0].ToString());
                        }

                    }
                }
                else
                {

                    log.logErrorMessage("------------------InValid Json -------------");
                    log.logErrorMessage(Filename);
                    log.logErrorMessage(WorkOrderMaster_DTO);
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage("------------------Json Error Start-------------");
                log.logErrorMessage(Filename);
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage("------------------Json Error  End-------------");

            }
            return objDynamic;
        }



        //Service Link record insert


        private DataSet GetWorkOrderServiceLinkMasterData(WorkOrderMaster_API_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[GetWorkOrderServicelinkMaster_Data]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@WServiceLink_PkeyID", 1 + "#bigint#" + model.Pkey_Id);
                input_parameters.Add("@Imrt_PkeyId", 1 + "#bigint#" + model.Imrt_PkeyId);
                input_parameters.Add("@WhereClause", 1 + "#varchar#" + model.WhereClause);
                input_parameters.Add("@PageNumber", 1 + "#int#" + model.PageNumber);
                input_parameters.Add("@NoofRows", 1 + "#int#" + model.NoofRows);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ds;
        }

        public List<dynamic> ServiceLinkOrderItemDetails(string WorkOrderMaster_DTO, Int64 WI_Pkey_ID, string Filename, string FilePath, string PdfFilePath, string pdfname, Int64 UserID)
        {
            WorkOrder_ServiceLink_MasterData WorkOrder_ServiceLink_MasterData = new WorkOrder_ServiceLink_MasterData();

            List<dynamic> objDynamic = new List<dynamic>();

            try
            {

                JsonValidate jsonValidate = new JsonValidate();
                string Formatjson = WorkOrderMaster_DTO;
                bool val = jsonValidate.IsValidJson(Formatjson);
                if (val)
                {
                    var Data = JsonConvert.DeserializeObject<WorkOrder_ServiceLinkItem_MasterDTO>(Formatjson);
                    //var Workorder = Data.work_order;
                    var Workorder = Data.WO;
                    WorkOrder_ServiceLink_MasterDTO model = new WorkOrder_ServiceLink_MasterDTO();
                    model.WServiceLink_PkeyID = 0;
                    model.WServiceLink_wo_id = Workorder;
                    model.WServiceLink_Address = Data.Address;
                    model.WServiceLink_City = Data.City;
                    model.WServiceLink_State = Data.State;
                    model.WServiceLink_ZipCode = Data.ZipCode;
                    model.WServiceLink_DueDate = Data.DueDate;
                    model.WServiceLink_KeyCode = Data.KeyCode;
                    model.WServiceLink_LockBox = Data.LockBox;
                    model.WServiceLink_ClientID = Data.ClientID;
                    model.WServiceLink_Loan = Data.Loan;
                    model.WServiceLink_LoanType = Data.LoanType;
                    model.WServiceLink_Owner = Data.Owner;
                    model.WServiceLink_CoverageID = Data.CoverageID;
                    model.WServiceLink_Investor = Data.Investor;
                    //model.WServiceLink_services = Data.services;
                    model.WServiceLink_id = Data.id;
                    model.WServiceLink_username = Data.username;
                    model.WServiceLink_Comments = Data.comments;



                    model.WServiceLink_IsActive = true;
                    model.WServiceLink_IsDelete = false;
                   
                    model.Type = 1;
                    model.WServiceLink_ImportMaster_Pkey = WI_Pkey_ID;
                    GoogleLocation GoogleLocation = new GoogleLocation();
                    GoogleLocationDTO googleLocationDTO = new GoogleLocationDTO();
                    WorkOrderMasterData workOrderMasterData = new WorkOrderMasterData();
                    workOrderxDTO workOrderxDTO = new workOrderxDTO();
                    workOrderxDTO.address1 = model.WServiceLink_Address + " " + model.WServiceLink_City + " " + model.WServiceLink_State + " " + model.WServiceLink_ZipCode; ;
                    workOrderxDTO.Type = 2;
                    DataSet ds = workOrderMasterData.Get_WorkOrderLatLongByAddress(workOrderxDTO);

                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                            {
                                model.WServiceLink_gpsLatitude = ds.Tables[0].Rows[i]["gpsLatitude"].ToString();
                                model.WServiceLink_gpsLongitude = ds.Tables[0].Rows[i]["gpsLongitude"].ToString();
                            }
                        }
                        else
                        {

                            googleLocationDTO.Address = model.WServiceLink_Address + " " + model.WServiceLink_City + " " + model.WServiceLink_State + " " + model.WServiceLink_ZipCode;
                            googleLocationDTO = GoogleLocation.GetLatitudeLogitudeWithAPIKey(googleLocationDTO);
                            model.WServiceLink_gpsLatitude = googleLocationDTO.Latitude;
                            model.WServiceLink_gpsLongitude = googleLocationDTO.Longitude;
                        }
                    }
                    else
                    {

                        googleLocationDTO.Address = model.WServiceLink_Address + " " + model.WServiceLink_City + " " + model.WServiceLink_State + " " + model.WServiceLink_ZipCode; ;
                        googleLocationDTO = GoogleLocation.GetLatitudeLogitudeWithAPIKey(googleLocationDTO);
                        model.WServiceLink_gpsLatitude = googleLocationDTO.Latitude;
                        model.WServiceLink_gpsLongitude = googleLocationDTO.Longitude;
                    }

                    model.WServiceLink_Import_File_Name = Filename;
                    model.WServiceLink_Import_FilePath = FilePath;
                    model.WServiceLink_Import_Pdf_Name = pdfname;
                    model.WServiceLink_Import_Pdf_Path = PdfFilePath;
                    model.UserID = UserID;
                     objDynamic = WorkOrder_ServiceLink_MasterData.AddWorkOrderServiceLinkData(model);
                    if (objDynamic[0].Status == "1")
                    {

                        //for new json

                        if (Data.services != null)
                        {
                            for (int i = 0; i < Data.services.Count; i++)
                            {
                                if (!string.IsNullOrEmpty(Data.services[i].name) || !string.IsNullOrEmpty(Data.services[i].instructions))
                                {

                                    WorkOrder_ServiceLink_Item_MasterDTO WorkOrder_ServiceLink_Item_MasterDTO = new WorkOrder_ServiceLink_Item_MasterDTO();
                                    if (!string.IsNullOrWhiteSpace(objDynamic[0].WServiceLink_PkeyID))
                                    {
                                        WorkOrder_ServiceLink_Item_MasterDTO.WSERVICEINS_FkeyID = Convert.ToInt64(objDynamic[0].WServiceLink_PkeyID);
                                        WorkOrder_ServiceLink_Item_MasterDTO.WSERVICEINS_Ins_Name = Data.services[i].name;
                                        WorkOrder_ServiceLink_Item_MasterDTO.WSERVICEINS_Ins_Details = Data.services[i].instructions;
                                        WorkOrder_ServiceLink_Item_MasterDTO.WSERVICEINS_Additional_Details = string.Empty;
                                        WorkOrder_ServiceLink_Item_MasterDTO.Type = 1;
                                        WorkOrder_ServiceLink_Item_MasterDTO.UserID = UserID;
                                        WorkOrder_ServiceLink_Item_MasterDTO.WSERVICEINS_IsActive = true;
                                        WorkOrder_ServiceLink_Item_MasterDTO.WSERVICEINS_IsDelete = false;
                                        WorkOrder_ServiceLink_MasterData.AddWorkOrderServiceLinkItemData(WorkOrder_ServiceLink_Item_MasterDTO);
                                    }
                                    else
                                    {
                                        log.logDebugMessage("-----------------Error Occured while Adding Master-------------");
                                    }


                                }


                                else
                                {
                                    log.logDebugMessage("------------------WorkOrder Item Instruction Null Record Found -------------");


                                }
                            }
                        }
                        else
                        {
                            log.logDebugMessage("------------------WorkOrder Item Null Record Found Start -------------" + objDynamic[0].ToString());
                            log.logDebugMessage("------------------WorkOrder Item Null Record Found End -------------" + objDynamic[0].ToString());
                        }

                    }
                }
                else
                {

                    log.logErrorMessage("------------------InValid Json -------------");
                    log.logErrorMessage(Filename);
                    log.logErrorMessage(WorkOrderMaster_DTO);
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage("------------------Json Error Start-------------");
                log.logErrorMessage(Filename);
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage("------------------Json Error  End-------------");

            }
            return objDynamic;
        }
    }
}