﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class PCR_Securing_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public List<dynamic> AddPCR_Securing_Data(PCR_Securing_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objclntData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_PCR_Securing_Master]";
            PCR_Security_Master pCR_Security_Master = new PCR_Security_Master();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@PCR_Securing_pkeyId", 1 + "#bigint#" + model.PCR_Securing_pkeyId);
                input_parameters.Add("@PCR_Securing_MasterId", 1 + "#bigint#" + model.PCR_Securing_MasterId);
                input_parameters.Add("@PCR_Securing_WO_Id", 1 + "#bigint#" + model.PCR_Securing_WO_Id);
                input_parameters.Add("@PCR_Securing_ValType", 1 + "#int#" + model.PCR_Securing_ValType);
                input_parameters.Add("@PCR_Securing_On_Arrival", 1 + "#varchar#" + model.PCR_Securing_On_Arrival);
                input_parameters.Add("@PCR_Securing_On_Departure", 1 + "#varchar#" + model.PCR_Securing_On_Departure);
                input_parameters.Add("@PCR_Securing_Not_Secure_Reason_Missing_Doors", 1 + "#bit#" + model.PCR_Securing_Not_Secure_Reason_Missing_Doors);
                input_parameters.Add("@PCR_Securing_Not_Secure_Reason_Door_Open", 1 + "#bit#" + model.PCR_Securing_Not_Secure_Reason_Door_Open);
                input_parameters.Add("@PCR_Securing_Not_Secure_Reason_Missing_Locks", 1 + "#bit#" + model.PCR_Securing_Not_Secure_Reason_Missing_Locks);
                input_parameters.Add("@PCR_Securing_Not_Secure_Reason_Broken_Windows", 1 + "#bit#" + model.PCR_Securing_Not_Secure_Reason_Broken_Windows);
                input_parameters.Add("@PCR_Securing_Not_Secure_Reason_Missing_Window", 1 + "#bit#" + model.PCR_Securing_Not_Secure_Reason_Missing_Window);
                input_parameters.Add("@PCR_Securing_Not_Secure_Reason_Window_Open", 1 + "#bit#" + model.PCR_Securing_Not_Secure_Reason_Window_Open);
                input_parameters.Add("@PCR_Securing_Not_Secure_Reason_Broken_Door", 1 + "#bit#" + model.PCR_Securing_Not_Secure_Reason_Broken_Door);
                input_parameters.Add("@PCR_Securing_Not_Secure_Reason_Bids_Pending", 1 + "#bit#" + model.PCR_Securing_Not_Secure_Reason_Bids_Pending);
                input_parameters.Add("@PCR_Securing_Not_Secure_Reason_Damage_Locks", 1 + "#bit#" + model.PCR_Securing_Not_Secure_Reason_Damage_Locks);

                input_parameters.Add("@PCR_Securing_Depart_Not_Secure_Reason_Missing_Doors", 1 + "#bit#" + model.PCR_Securing_Depart_Not_Secure_Reason_Missing_Doors);
                input_parameters.Add("@PCR_Securing_Depart_Not_Secure_Reason_Door_Open", 1 + "#bit#" + model.PCR_Securing_Depart_Not_Secure_Reason_Door_Open);
                input_parameters.Add("@PCR_Securing_Depart_Not_Secure_Reason_Missing_Locks", 1 + "#bit#" + model.PCR_Securing_Depart_Not_Secure_Reason_Missing_Locks);
                input_parameters.Add("@PCR_Securing_Depart_Not_Secure_Reason_Broken_Windows", 1 + "#bit#" + model.PCR_Securing_Depart_Not_Secure_Reason_Broken_Windows);
                input_parameters.Add("@PCR_Securing_Depart_Not_Secure_Reason_Missing_Window", 1 + "#bit#" + model.PCR_Securing_Depart_Not_Secure_Reason_Missing_Window);
                input_parameters.Add("@PCR_Securing_Depart_Not_Secure_Reason_Broken_Door", 1 + "#bit#" + model.PCR_Securing_Depart_Not_Secure_Reason_Broken_Door);
                input_parameters.Add("@PCR_Securing_Depart_Not_Secure_Reason_Bids_Pending", 1 + "#bit#" + model.PCR_Securing_Depart_Not_Secure_Reason_Bids_Pending);
                input_parameters.Add("@PCR_Securing_Depart_Not_Secure_Reason_Damage_Locks", 1 + "#bit#" + model.PCR_Securing_Depart_Not_Secure_Reason_Damage_Locks);

                input_parameters.Add("@PCR_Securing_Boarded_Arrival", 1 + "#varchar#" + model.PCR_Securing_Boarded_Arrival);
                input_parameters.Add("@PCR_Securing_No_Of_First_Floor_Window", 1 + "#varchar#" + model.PCR_Securing_No_Of_First_Floor_Window);
                input_parameters.Add("@PCR_Securing_More_Boarding_Still_Required_OR_Not", 1 + "#varchar#" + model.PCR_Securing_More_Boarding_Still_Required_OR_Not);
                input_parameters.Add("@PCR_Securing_Prop_No_More_Boarding_Required", 1 + "#bit#" + model.PCR_Securing_Prop_No_More_Boarding_Required);
                input_parameters.Add("@PCR_Securing_IsActive", 1 + "#bit#" + model.PCR_Securing_IsActive);
                input_parameters.Add("@PCR_Securing_IsDelete", 1 + "#bit#" + model.PCR_Securing_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@fwo_pkyeId", 1 + "#bigint#" + model.fwo_pkyeId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@PCR_Securing_pkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    pCR_Security_Master.PCR_Securing_pkeyId = "0";
                    pCR_Security_Master.Status = "0";
                    pCR_Security_Master.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    pCR_Security_Master.PCR_Securing_pkeyId = Convert.ToString(objData[0]);
                    pCR_Security_Master.Status = Convert.ToString(objData[1]);


                }
                objclntData.Add(pCR_Security_Master);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objclntData;



        }

        private DataSet GetPCRSecuringMaster(PCR_Securing_MasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_PCR_Securing_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@PCR_Securing_pkeyId", 1 + "#bigint#" + model.PCR_Securing_pkeyId);
                input_parameters.Add("@PCR_Securing_WO_Id", 1 + "#bigint#" + model.PCR_Securing_WO_Id);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetPCRSecuringDetails(PCR_Securing_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetPCRSecuringMaster(model);

                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                }

                //var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                //List<PCR_Securing_MasterDTO> PCRSecuring =
                //   (from item in myEnumerableFeaprd
                //    select new PCR_Securing_MasterDTO
                //    {
                //        PCR_Securing_pkeyId = item.Field<Int64>("PCR_Securing_pkeyId"),
                //        PCR_Securing_MasterId = item.Field<Int64>("PCR_Securing_MasterId"),
                //        PCR_Securing_WO_Id = item.Field<Int64>("PCR_Securing_WO_Id"),
                //        PCR_Securing_ValType = item.Field<int?>("PCR_Securing_ValType"),
                //        PCR_Securing_On_Arrival = item.Field<string>("PCR_Securing_On_Arrival"),
                //        PCR_Securing_On_Departure = item.Field<string>("PCR_Securing_On_Departure"),
                //        PCR_Securing_Not_Secure_Reason_Missing_Doors = item.Field<Boolean?>("PCR_Securing_Not_Secure_Reason_Missing_Doors"),
                //        PCR_Securing_Not_Secure_Reason_Door_Open = item.Field<Boolean?>("PCR_Securing_Not_Secure_Reason_Door_Open"),
                //        PCR_Securing_Not_Secure_Reason_Missing_Locks = item.Field<Boolean?>("PCR_Securing_Not_Secure_Reason_Missing_Locks"),
                //        PCR_Securing_Not_Secure_Reason_Broken_Windows = item.Field<Boolean?>("PCR_Securing_Not_Secure_Reason_Broken_Windows"),
                //        PCR_Securing_Not_Secure_Reason_Missing_Window = item.Field<Boolean?>("PCR_Securing_Not_Secure_Reason_Missing_Window"),
                //        PCR_Securing_Not_Secure_Reason_Window_Open = item.Field<Boolean?>("PCR_Securing_Not_Secure_Reason_Window_Open"),
                //        PCR_Securing_Not_Secure_Reason_Broken_Door = item.Field<Boolean?>("PCR_Securing_Not_Secure_Reason_Broken_Door"),
                //        PCR_Securing_Not_Secure_Reason_Bids_Pending = item.Field<Boolean?>("PCR_Securing_Not_Secure_Reason_Bids_Pending"),
                //        PCR_Securing_Not_Secure_Reason_Damage_Locks = item.Field<Boolean?>("PCR_Securing_Not_Secure_Reason_Damage_Locks"),

                //        PCR_Securing_Depart_Not_Secure_Reason_Missing_Doors = item.Field<Boolean?>("PCR_Securing_Depart_Not_Secure_Reason_Missing_Doors"),
                //        PCR_Securing_Depart_Not_Secure_Reason_Door_Open = item.Field<Boolean?>("PCR_Securing_Depart_Not_Secure_Reason_Door_Open"),
                //        PCR_Securing_Depart_Not_Secure_Reason_Missing_Locks = item.Field<Boolean?>("PCR_Securing_Depart_Not_Secure_Reason_Missing_Locks"),
                //        PCR_Securing_Depart_Not_Secure_Reason_Broken_Windows = item.Field<Boolean?>("PCR_Securing_Depart_Not_Secure_Reason_Broken_Windows"),
                //        PCR_Securing_Depart_Not_Secure_Reason_Missing_Window = item.Field<Boolean?>("PCR_Securing_Depart_Not_Secure_Reason_Missing_Window"),
                //        PCR_Securing_Depart_Not_Secure_Reason_Broken_Door = item.Field<Boolean?>("PCR_Securing_Depart_Not_Secure_Reason_Broken_Door"),
                //        PCR_Securing_Depart_Not_Secure_Reason_Bids_Pending = item.Field<Boolean?>("PCR_Securing_Depart_Not_Secure_Reason_Bids_Pending"),
                //        PCR_Securing_Depart_Not_Secure_Reason_Damage_Locks = item.Field<Boolean?>("PCR_Securing_Depart_Not_Secure_Reason_Damage_Locks"),

                //        PCR_Securing_Boarded_Arrival = item.Field<string>("PCR_Securing_Boarded_Arrival"),
                //        PCR_Securing_No_Of_First_Floor_Window = item.Field<string>("PCR_Securing_No_Of_First_Floor_Window"),
                //        PCR_Securing_More_Boarding_Still_Required_OR_Not = item.Field<string>("PCR_Securing_More_Boarding_Still_Required_OR_Not"),
                //        PCR_Securing_IsActive = item.Field<Boolean?>("PCR_Securing_IsActive"),
                //        //PCR_Securing_IsDelete = item.Field<Boolean?>("PCR_Securing_IsDelete"),





                //    }).ToList();

                //objDynamic.Add(PCRSecuring);

                //if (ds.Tables.Count > 1)
                //{
                //    var pcrHistory = ds.Tables[1].AsEnumerable();
                //    List<PCR_Securing_MasterDTO> HistorySecuring =
                //       (from item in pcrHistory
                //        select new PCR_Securing_MasterDTO
                //        {
                //            PCR_Securing_pkeyId = item.Field<Int64>("PCR_Securing_pkeyId"),
                //            PCR_Securing_MasterId = item.Field<Int64>("PCR_Securing_MasterId"),
                //            PCR_Securing_WO_Id = item.Field<Int64>("PCR_Securing_WO_Id"),
                //            PCR_Securing_ValType = item.Field<int?>("PCR_Securing_ValType"),
                //            PCR_Securing_On_Arrival = item.Field<string>("PCR_Securing_On_Arrival"),
                //            PCR_Securing_On_Departure = item.Field<string>("PCR_Securing_On_Departure"),
                //            PCR_Securing_Not_Secure_Reason_Missing_Doors = item.Field<Boolean?>("PCR_Securing_Not_Secure_Reason_Missing_Doors"),
                //            PCR_Securing_Not_Secure_Reason_Door_Open = item.Field<Boolean?>("PCR_Securing_Not_Secure_Reason_Door_Open"),
                //            PCR_Securing_Not_Secure_Reason_Missing_Locks = item.Field<Boolean?>("PCR_Securing_Not_Secure_Reason_Missing_Locks"),
                //            PCR_Securing_Not_Secure_Reason_Broken_Windows = item.Field<Boolean?>("PCR_Securing_Not_Secure_Reason_Broken_Windows"),
                //            PCR_Securing_Not_Secure_Reason_Missing_Window = item.Field<Boolean?>("PCR_Securing_Not_Secure_Reason_Missing_Window"),
                //            PCR_Securing_Not_Secure_Reason_Window_Open = item.Field<Boolean?>("PCR_Securing_Not_Secure_Reason_Window_Open"),
                //            PCR_Securing_Not_Secure_Reason_Broken_Door = item.Field<Boolean?>("PCR_Securing_Not_Secure_Reason_Broken_Door"),
                //            PCR_Securing_Not_Secure_Reason_Bids_Pending = item.Field<Boolean?>("PCR_Securing_Not_Secure_Reason_Bids_Pending"),
                //            PCR_Securing_Not_Secure_Reason_Damage_Locks = item.Field<Boolean?>("PCR_Securing_Not_Secure_Reason_Damage_Locks"),

                //            PCR_Securing_Depart_Not_Secure_Reason_Missing_Doors = item.Field<Boolean?>("PCR_Securing_Depart_Not_Secure_Reason_Missing_Doors"),
                //            PCR_Securing_Depart_Not_Secure_Reason_Door_Open = item.Field<Boolean?>("PCR_Securing_Depart_Not_Secure_Reason_Door_Open"),
                //            PCR_Securing_Depart_Not_Secure_Reason_Missing_Locks = item.Field<Boolean?>("PCR_Securing_Depart_Not_Secure_Reason_Missing_Locks"),
                //            PCR_Securing_Depart_Not_Secure_Reason_Broken_Windows = item.Field<Boolean?>("PCR_Securing_Depart_Not_Secure_Reason_Broken_Windows"),
                //            PCR_Securing_Depart_Not_Secure_Reason_Missing_Window = item.Field<Boolean?>("PCR_Securing_Depart_Not_Secure_Reason_Missing_Window"),
                //            PCR_Securing_Depart_Not_Secure_Reason_Broken_Door = item.Field<Boolean?>("PCR_Securing_Depart_Not_Secure_Reason_Broken_Door"),
                //            PCR_Securing_Depart_Not_Secure_Reason_Bids_Pending = item.Field<Boolean?>("PCR_Securing_Depart_Not_Secure_Reason_Bids_Pending"),
                //            PCR_Securing_Depart_Not_Secure_Reason_Damage_Locks = item.Field<Boolean?>("PCR_Securing_Depart_Not_Secure_Reason_Damage_Locks"),

                //            PCR_Securing_Boarded_Arrival = item.Field<string>("PCR_Securing_Boarded_Arrival"),
                //            PCR_Securing_No_Of_First_Floor_Window = item.Field<string>("PCR_Securing_No_Of_First_Floor_Window"),
                //            PCR_Securing_More_Boarding_Still_Required_OR_Not = item.Field<string>("PCR_Securing_More_Boarding_Still_Required_OR_Not"),
                //            PCR_Securing_IsActive = item.Field<Boolean?>("PCR_Securing_IsActive"),
                //            //PCR_Securing_IsDelete = item.Field<Boolean?>("PCR_Securing_IsDelete"),





                //        }).ToList();

                //    objDynamic.Add(HistorySecuring);
                //}
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}