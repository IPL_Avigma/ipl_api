﻿using Avigma.Models;
using Avigma.Repository.Lib;
using IPL.Models;
using IPL.Repository.Lib;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace IPL.Repository.data
{
    public class WorkOrderCopyData
    {

        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();


        public List<dynamic> AddCopyWorkOrderData(WorkOrderCopyDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();

            string insertProcedure = "[CreateUpdateWorkOrder_Copy]";
            WorkOrderCopy workOrderCopy = new WorkOrderCopy();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@workOrder_ID", 1 + "#bigint#" + model.workOrder_ID);
                input_parameters.Add("@WorkOderInfo", 1 + "#int#" + model.WorkOderInfo);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@workOrder_ID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[0] == 0)
                {
                    workOrderCopy.workOrder_ID = "0";
                    workOrderCopy.Status = "0";
                    workOrderCopy.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    workOrderCopy.workOrder_ID = Convert.ToString(objData[0]);
                    workOrderCopy.Status = Convert.ToString(objData[1]);
                    EmailWorkOderDTO emailWorkOderDTO = new EmailWorkOderDTO();

                    switch (model.WorkOderInfo)
                    {
                        case 1:
                        case 2:
                            {
                                emailWorkOderDTO.Val_Type = 4;
                                emailWorkOderDTO.workOrder_ID = Convert.ToInt64(workOrderCopy.workOrder_ID);
                                emailWorkOderDTO.UserID = model.UserID;
                                emailWorkOderDTO.Type = 1;
                                GetEmailGobackWorkOderDetail(emailWorkOderDTO);
                                break;

                            }
                        case 3:
                            {
                                emailWorkOderDTO.Val_Type = 5;
                                emailWorkOderDTO.workOrder_ID = Convert.ToInt64(workOrderCopy.workOrder_ID);
                                emailWorkOderDTO.UserID = model.UserID;
                                emailWorkOderDTO.Type = 1;
                                GetEmailGobackWorkOderDetail(emailWorkOderDTO);
                                break;
                            }

                    }




                }
                objcltData.Add(workOrderCopy);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;



        }

        //for goback and infoneeded Email

        private DataSet GetEmailGoBackWorkOderData(EmailWorkOderDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[GetWorkOrder_EmailMaster]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@workOrder_ID", 1 + "#bigint#" + model.workOrder_ID);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Val_Type", 1 + "#int#" + model.Val_Type);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }
        public async Task<int> GetEmailGobackWorkOderDetail(EmailWorkOderDTO model)
        {
            DataSet dsdata = null;
            EmailManager emailManager = new EmailManager();
            //EmailDTO emailDTO = new EmailDTO();
            DyanmicEmailDTO emailDTO = new DyanmicEmailDTO();
            int ret = 0;
            string strSubject = string.Empty;
            string strBody = string.Empty;
            string strTaskComments = string.Empty;
            string strStatus = "0";
            try
            {
                dsdata = GetEmailGoBackWorkOderData(model);

                if (dsdata.Tables.Count > 2)
                {
                    for (int i = 0; i < dsdata.Tables[2].Rows.Count; i++)
                    {
                        strStatus = dsdata.Tables[2].Rows[i]["status"].ToString();
                    }
                }
                if (strStatus == "1")
                {
                    if (dsdata.Tables.Count > 1 && dsdata.Tables[1].Rows.Count > 0)
                    {
                        strSubject = dsdata.Tables[1].Rows[0]["Email_Temp_Subject"].ToString();
                        strBody = dsdata.Tables[1].Rows[0]["Email_Temp_HTML"].ToString();
                    }
                    if (dsdata.Tables.Count > 2 && dsdata.Tables[2].Rows.Count > 0)
                    {
                        model.ContractorName = dsdata.Tables[2].Rows[0]["ContractorName"].ToString();
                        model.ContractorEmail = dsdata.Tables[2].Rows[0]["ContractorEmail"].ToString();
                        model.Processor_Name = dsdata.Tables[2].Rows[0]["Processor_Name"].ToString();
                        model.ProcessorEmail = dsdata.Tables[2].Rows[0]["ProcessorEmail"].ToString();
                        model.Cordinator_Name = dsdata.Tables[2].Rows[0]["Cordinator_Name"].ToString();
                        model.CordinatorEmail = dsdata.Tables[2].Rows[0]["CordinatorEmail"].ToString();
                        model.User_FirstName = dsdata.Tables[2].Rows[0]["User_FirstName"].ToString();
                        model.Cordinator_Phone = dsdata.Tables[2].Rows[0]["Cordinator_Phone"].ToString();
                        model.Comments = dsdata.Tables[2].Rows[0]["Inst_Ch_Comand_Mobile"].ToString();
                        model.Client_Result_Photo_FilePath = dsdata.Tables[2].Rows[0]["Client_Result_Photo_FilePath"].ToString();

                        model.workOrderNumber = dsdata.Tables[2].Rows[0]["workOrderNumber"].ToString();
                        model.WT_WorkType = dsdata.Tables[2].Rows[0]["WT_WorkType"].ToString();
                        model.address1 = dsdata.Tables[2].Rows[0]["address1"].ToString();
                        model.city = dsdata.Tables[2].Rows[0]["city"].ToString();
                        model.state = dsdata.Tables[2].Rows[0]["state"].ToString();
                        model.zip = dsdata.Tables[2].Rows[0]["zip"].ToString();
                        model.IPLNO = dsdata.Tables[2].Rows[0]["IPLNO"].ToString();
                        model.dueDate = Convert.ToDateTime(dsdata.Tables[2].Rows[0]["dueDate"].ToString());
                        model.Client_Company_Name = dsdata.Tables[2].Rows[0]["Client_Company_Name"].ToString();
                        model.Comments = dsdata.Tables[2].Rows[0]["Comments"].ToString();

                    }

                    string strTask = string.Empty, strDocument = string.Empty;
                    string strTaskFile = string.Empty, strDocumentFile = string.Empty;

                    if (dsdata.Tables.Count > 2)
                    {
                        for (int i = 0; i < dsdata.Tables[3].Rows.Count; i++)
                        {
                            string strTaskdetails = string.Empty;
                            string strTaskName = dsdata.Tables[3].Rows[i]["Task_Name"].ToString();
                            strTaskComments = dsdata.Tables[3].Rows[i]["Inst_Comand_Mobile_details"].ToString();
                            string strTaskFilePath = dsdata.Tables[3].Rows[i]["TMF_Task_localPath"].ToString();
                            string TMF_Task_FileName = dsdata.Tables[3].Rows[i]["TMF_Task_FileName"].ToString();

                            if (!string.IsNullOrEmpty(strTaskFilePath))
                            {
                                strTaskFile = "<a href " + strTaskFilePath + " > " + TMF_Task_FileName + " </ a >";
                            }


                            strTaskdetails = strTaskName + "<br/>" + strTaskComments;

                            strTask = "<br/>" + strTask + strTaskdetails + "<br/>" + strTaskFile + "<br/>";

                        }
                    }
                    if (dsdata.Tables.Count > 3)
                    {
                        for (int i = 0; i < dsdata.Tables[4].Rows.Count; i++)
                        {
                            string Inst_Doc_File_Path = dsdata.Tables[4].Rows[i]["Inst_Doc_File_Path"].ToString();
                            string Inst_Doc_File_Name = dsdata.Tables[4].Rows[i]["Inst_Doc_File_Name"].ToString();
                            if (!string.IsNullOrEmpty(Inst_Doc_File_Path))
                            {
                                strDocumentFile = "<a href" + Inst_Doc_File_Path + " > " + Inst_Doc_File_Name + " </ a >";
                            }

                            strDocument = "<br/>" + strDocument + "<br/>" + strDocumentFile + "<br/>";

                        }
                    }
                    if (dsdata.Tables.Count > 0)
                    {
                        for (int i = 0; i < dsdata.Tables[0].Rows.Count; i++)
                        {
                            string StrKeyData = dsdata.Tables[0].Rows[i]["Keydata"].ToString();
                            string strCloumnData = dsdata.Tables[0].Rows[i]["Wo_Column_Name"].ToString();
                            string value = string.Empty;

                            if (StrKeyData == "Inst_Task_Name")
                            {
                                value = strTask;
                                strBody = strBody.Replace(strCloumnData, " " + strTask);
                            }
                            else if (StrKeyData == "Inst_Comand_Mobile_details")
                            {
                                value = strTaskComments;
                                strBody = strBody.Replace(strCloumnData, " " + value);
                            }
                            else if (StrKeyData == "dueDate")
                            {
                                if (model.dueDate != null)
                                {
                                    value = model.dueDate.Value.ToString("MM/dd/yyyy");
                                }
                                strBody = strBody.Replace(strCloumnData, " " + value);
                            }
                            else if (StrKeyData == "Document")
                            {
                                value = strDocument;
                                strBody = strBody.Replace(strCloumnData, " " + value);
                            }
                            else
                            {
                                if (dsdata.Tables.Count > 2 && dsdata.Tables[2].Rows.Count > 0)
                                {
                                    value = dsdata.Tables[2].Rows[0][StrKeyData].ToString();
                                    strBody = strBody.Replace(strCloumnData, " " + value);
                                }
                            }

                            strSubject = strSubject.Replace(strCloumnData, " " + value);
                        }
                    }

                    emailDTO.To = model.ContractorEmail;
                    emailDTO.Cc = model.CordinatorEmail;
                    emailDTO.Subject = strSubject;
                    emailDTO.Message = strBody;
                    ret = await DynamicEmailManager.SendDynamicEmail(emailDTO,6);

                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return ret;
        }

    }



}