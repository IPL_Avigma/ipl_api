﻿using Avigma.Models;
using Avigma.Repository.Lib;
using IPL.Models;
using IPLApp.Models;
using IPLApp.Repository.data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace IPL.Repository.data
{
    public class WorkOrder_Puran_MasterData
    {
        Log log = new Log();
        MyDataSourceFactory obj = new MyDataSourceFactory();
        private List<dynamic> Add_WorkorderData(PruvanWorkOrderDTO workOrder)
        {
            List<dynamic> objdata = new List<dynamic>();
            //List<dynamic> objreturndata = new List<dynamic>();
            //ReturnworkOrder returnworkOrder = new ReturnworkOrder();
            try
            {
                string insertProcedure = "[CreateUpdateWorkOrder_Puran_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@workOrder_ID", 1 + "#bigint#" + workOrder.workOrder_ID);
                input_parameters.Add("@workOrderNumber", 1 + "#varchar#" + workOrder.workOrderNumber);
                input_parameters.Add("@workOrderInfo", 1 + "#varchar#" + workOrder.workOrderInfo);
                input_parameters.Add("@address1", 1 + "#varchar#" + workOrder.address1);
                input_parameters.Add("@address2", 1 + "#varchar#" + workOrder.address2);
                input_parameters.Add("@city", 1 + "#varchar#" + workOrder.city);
                input_parameters.Add("@state", 1 + "#varchar#" + workOrder.state);
                input_parameters.Add("@zip", 1 + "#bigint#" + workOrder.zip);
                input_parameters.Add("@country", 1 + "#varchar#" + workOrder.country);
                input_parameters.Add("@options", 1 + "#varchar#" + workOrder.options);
                input_parameters.Add("@reference", 1 + "#varchar#" + workOrder.reference);
                input_parameters.Add("@description", 1 + "#varchar#" + workOrder.description);
                input_parameters.Add("@instructions", 1 + "#varchar#" + workOrder.instructions);
                input_parameters.Add("@status", 1 + "#varchar#" + workOrder.status);
                input_parameters.Add("@dueDate", 1 + "#datetime#" + workOrder.dueDate);
                input_parameters.Add("@startDate", 1 + "#datetime#" + workOrder.startDate);

                input_parameters.Add("@clientInstructions", 1 + "#varchar#" + workOrder.clientInstructions);
                input_parameters.Add("@clientStatus", 1 + "#varchar#" + workOrder.clientStatus);
                input_parameters.Add("@clientDueDate", 1 + "#datetime#" + workOrder.clientDueDate);
                input_parameters.Add("@gpsLatitude", 1 + "#varchar#" + workOrder.gpsLatitude);
                input_parameters.Add("@gpsLongitude", 1 + "#varchar#" + workOrder.gpsLongitude);
                input_parameters.Add("@attribute7", 1 + "#varchar#" + workOrder.attribute7);
                input_parameters.Add("@attribute8", 1 + "#varchar#" + workOrder.attribute8);
                input_parameters.Add("@attribute9", 1 + "#nvarchar#" + workOrder.attribute9);
                input_parameters.Add("@attribute10", 1 + "#nvarchar#" + workOrder.attribute10);
                input_parameters.Add("@attribute11", 1 + "#nvarchar#" + workOrder.attribute11);
                input_parameters.Add("@attribute12", 1 + "#nvarchar#" + workOrder.attribute12);
                input_parameters.Add("@attribute13", 1 + "#nvarchar#" + workOrder.attribute13);
                input_parameters.Add("@attribute14", 1 + "#nvarchar#" + workOrder.attribute14);
                input_parameters.Add("@attribute15", 1 + "#nvarchar#" + workOrder.attribute15);
                input_parameters.Add("@source_wo_provider", 1 + "#varchar#" + workOrder.source_wo_provider);
                input_parameters.Add("@source_wo_number", 1 + "#varchar#" + workOrder.source_wo_number);
                input_parameters.Add("@source_wo_id", 1 + "#bigint#" + workOrder.source_wo_id);
                input_parameters.Add("@controlConfig", 1 + "#bigint#" + workOrder.controlConfig);
                input_parameters.Add("@services_Id", 1 + "#bigint#" + workOrder.services_Id);
                input_parameters.Add("@IsActive", 1 + "#bit#" + workOrder.IsActive);
                input_parameters.Add("@Type", 1 + "#int#" + workOrder.Type);
                input_parameters.Add("@currUserId", 1 + "#bigint#" + workOrder.currUserId);
                input_parameters.Add("@WorkOrder_Import_FkeyId", 1 + "#bigint#" + workOrder.WorkOrder_Import_FkeyId);
                input_parameters.Add("@IsDelete", 1 + "#bit#" + workOrder.IsDelete);
                input_parameters.Add("@IsProcess", 1 + "#bit#" + workOrder.IsProcess);

                input_parameters.Add("@workOrder_IDOut", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objdata = obj.SqlCRUD(insertProcedure, input_parameters);

                //returnworkOrder.workOrder_ID = Convert.ToString(objdata[0]);
                //returnworkOrder.Status = Convert.ToString(objdata[1]);

                //objreturndata.Add(returnworkOrder);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return null;
            }

            return objdata;

        }

        public List<dynamic> AddWorkorderPuranData(PruvanWorkOrderDTO workOrder)
        {
            try
            {
                GoogleLocation GoogleLocation = new GoogleLocation();
                GoogleLocationDTO googleLocationDTO = new GoogleLocationDTO();
                List<dynamic> objdata = new List<dynamic>();
                switch (workOrder.Type)
                {

                    case 1:
                    case 2:
                    case 6:
                        {
                            WorkOrderMasterData workOrderMasterData = new WorkOrderMasterData();
                            workOrderxDTO workOrderxDTO = new workOrderxDTO();
                            workOrderxDTO.address1 = workOrder.address1;
                            workOrderxDTO.state = workOrder.state_name;
                            workOrderxDTO.zip = workOrder.zip;
                            workOrderxDTO.Type = 3;
                            DataSet ds = workOrderMasterData.Get_WorkOrderLatLongByAddress(workOrderxDTO);

                            if (ds.Tables.Count > 0)
                            {
                                if (ds.Tables[0].Rows.Count > 0)
                                {
                                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                                    {
                                        workOrder.gpsLatitude = ds.Tables[0].Rows[i]["gpsLatitude"].ToString();
                                        workOrder.gpsLongitude = ds.Tables[0].Rows[i]["gpsLongitude"].ToString();
                                    }
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(workOrder.address1) && !string.IsNullOrEmpty(workOrder.state) && workOrder.zip != 0)
                                    {
                                        string strAddress = workOrder.address1 + "  ," + workOrder.state_name + "  " + workOrder.zip.ToString();
                                        googleLocationDTO.Address = strAddress;
                                        googleLocationDTO = GoogleLocation.GetLatitudeLogitudeWithAPIKey(googleLocationDTO);

                                        workOrder.gpsLatitude = googleLocationDTO.Latitude;
                                        workOrder.gpsLongitude = googleLocationDTO.Longitude;
                                    }
                                }
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(workOrder.address1) && !string.IsNullOrEmpty(workOrder.state) && workOrder.zip != 0)
                                {
                                    string strAddress = workOrder.address1 + "  ," + workOrder.state_name + "  " + workOrder.zip.ToString();
                                    googleLocationDTO.Address = strAddress;
                                    googleLocationDTO = GoogleLocation.GetLatitudeLogitudeWithAPIKey(googleLocationDTO);

                                    workOrder.gpsLatitude = googleLocationDTO.Latitude;
                                    workOrder.gpsLongitude = googleLocationDTO.Longitude;
                                }
                            }
                              
                            break;
                        }
                }
                string strcomments = string.Empty;
                if (!string.IsNullOrEmpty(workOrder.Comments))
                {
                    strcomments = Regex.Replace(workOrder.Comments, "<.*?>", String.Empty);
                    workOrder.Inst_Ch_Comand_Mobile = strcomments;
                }

                objdata = Add_WorkorderData(workOrder);
                
                return objdata;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return null;
            }
        }
    }
}