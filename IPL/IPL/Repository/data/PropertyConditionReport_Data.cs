﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class PropertyConditionReport_Data
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public List<dynamic> CreatePropertyConditionReport_Data(PropertyConditionReport_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objclntData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_PropertyConditionReport_Master]";
            PCR_LoggerMessage pCR_Logger = new PCR_LoggerMessage();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@PCR_PkeyID", 1 + "#bigint#" + model.PCR_PkeyID);
                input_parameters.Add("@PCR_WO_ID", 1 + "#bigint#" + model.PCR_WO_ID);
                input_parameters.Add("@PCR_UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@PCR_General", 1 + "#nvarchar#" + model.PCR_General);

                input_parameters.Add("@PCR_Utilities", 1 + "#nvarchar#" + model.PCR_Utilities);
                input_parameters.Add("@PCR_Securing", 1 + "#nvarchar#" + model.PCR_Securing);
                input_parameters.Add("@PCR_Winterization", 1 + "#nvarchar#" + model.PCR_Winterization);
                input_parameters.Add("@PCR_Bording", 1 + "#nvarchar#" + model.PCR_Bording);
                input_parameters.Add("@PCR_Debris", 1 + "#nvarchar#" + model.PCR_Debris);
                input_parameters.Add("@PCR_Roof", 1 + "#nvarchar#" + model.PCR_Roof);
                input_parameters.Add("@PCR_Moisture", 1 + "#nvarchar#" + model.PCR_Moisture);
                input_parameters.Add("@PCR_Yard", 1 + "#nvarchar#" + model.PCR_Yard);
                input_parameters.Add("@PCR_Damages", 1 + "#nvarchar#" + model.PCR_Damages);
                input_parameters.Add("@PCR_Others", 1 + "#nvarchar#" + model.PCR_Others);
                input_parameters.Add("@PCR_PhotoCheckList", 1 + "#nvarchar#" + model.PCR_PhotoCheckList);
                input_parameters.Add("@PCR_Summary", 1 + "#nvarchar#" + model.PCR_Summary);

                input_parameters.Add("@PCR_IsActive", 1 + "#bit#" + model.PCR_IsActive);
                input_parameters.Add("@PCR_IsDelete", 1 + "#bit#" + model.PCR_IsDelete);
                //input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@fwo_pkyeId", 1 + "#bigint#" + model.fwo_pkyeId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@PCR_PkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    pCR_Logger.PCR_PkeyID = "0";
                    pCR_Logger.Status = "0";
                    pCR_Logger.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    pCR_Logger.PCR_PkeyID = Convert.ToString(objData[0]);
                    pCR_Logger.Status = Convert.ToString(objData[1]);


                }
                objclntData.Add(pCR_Logger);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objclntData;



        }

        private DataSet GetPropertyConditionReport_Data(PropertyConditionReport_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_PropertyConditionReport_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@PCR_PkeyID", 1 + "#bigint#" + model.PCR_PkeyID);
                input_parameters.Add("@PCR_WO_ID", 1 + "#bigint#" + model.PCR_WO_ID);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetPropertyConditionReportDetails(PropertyConditionReport_DTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetPropertyConditionReport_Data(model);

                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                }

                //var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                //List<PropertyConditionReport_DTO> pcrDTP =
                //   (from item in myEnumerableFeaprd
                //    select new PropertyConditionReport_DTO
                //    {
                //        PCR_PkeyID = item.Field<Int64>("PCR_PkeyID"),
                //        PCR_WO_ID = item.Field<Int64>("PCR_WO_ID"),
                //        PCR_Company_ID = item.Field<Int64>("PCR_Company_ID"),
                //        PCR_UserID = item.Field<Int64>("PCR_UserID"),
                //        PCR_General = item.Field<String>("PCR_General"),
                //        PCR_Utilities = item.Field<String>("PCR_Utilities"),
                //        PCR_Securing = item.Field<String>("PCR_Securing"),
                //        PCR_Winterization = item.Field<String>("PCR_Winterization"),
                //        PCR_Bording = item.Field<String>("PCR_Bording"),
                //        PCR_Debris = item.Field<String>("PCR_Debris"),
                //        PCR_Roof = item.Field<String>("PCR_Roof"),
                //        PCR_Moisture = item.Field<String>("PCR_Moisture"),
                //        PCR_Yard = item.Field<String>("PCR_Yard"),
                //        PCR_Damages = item.Field<String>("PCR_Damages"),
                //        PCR_Others = item.Field<String>("PCR_Others"),
                //        PCR_PhotoCheckList = item.Field<String>("PCR_PhotoCheckList"),
                //        PCR_Summary = item.Field<String>("PCR_Summary"),
                //        PCR_IsActive = item.Field<Boolean?>("PCR_IsActive"),
                //    }).ToList();

                //objDynamic.Add(pcrDTP);

                //if (ds.Tables.Count > 1)
                //{
                //    var pcrHistory = ds.Tables[1].AsEnumerable();
                //    List<PropertyConditionReport_DTO> pcrHistoryDTP =
                //       (from item in pcrHistory
                //        select new PropertyConditionReport_DTO
                //        {
                //            PCR_PkeyID = item.Field<Int64>("PCR_PkeyID"),
                //            PCR_WO_ID = item.Field<Int64>("PCR_WO_ID"),
                //            PCR_Company_ID = item.Field<Int64>("PCR_Company_ID"),
                //            PCR_UserID = item.Field<Int64>("PCR_UserID"),
                //            PCR_General = item.Field<String>("PCR_General"),
                //            PCR_Utilities = item.Field<String>("PCR_Utilities"),
                //            PCR_Securing = item.Field<String>("PCR_Securing"),
                //            PCR_Winterization = item.Field<String>("PCR_Winterization"),
                //            PCR_Bording = item.Field<String>("PCR_Bording"),
                //            PCR_Debris = item.Field<String>("PCR_Debris"),
                //            PCR_Roof = item.Field<String>("PCR_Roof"),
                //            PCR_Moisture = item.Field<String>("PCR_Moisture"),
                //            PCR_Yard = item.Field<String>("PCR_Yard"),
                //            PCR_Damages = item.Field<String>("PCR_Damages"),
                //            PCR_Others = item.Field<String>("PCR_Others"),
                //            PCR_PhotoCheckList = item.Field<String>("PCR_PhotoCheckList"),
                //            PCR_Summary = item.Field<String>("PCR_Summary"),
                //            PCR_IsActive = item.Field<Boolean?>("PCR_IsActive"),
                //        }).ToList();

                //    objDynamic.Add(pcrHistoryDTP);
                //}
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}