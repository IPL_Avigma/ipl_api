﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class User_Background_checkData
    { 
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        public List<dynamic> CreateUpdateUserBackgroundcheckData(User_Background_ProviderDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_User_Background_Provider_Master]";
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Background_check_PkeyID", 1 + "#bigint#" + model.Background_check_PkeyID);
                input_parameters.Add("@Background_check_Drop_ID", 1 + "#nvarchar#" + model.Background_check_Drop_ID);
                input_parameters.Add("@Background_check_ID", 1 + "#nvarchar#" + model.Background_check_ID);
                input_parameters.Add("@Background_check_File_Name", 1 + "#nvarchar#" + model.Background_check_File_Name);
                input_parameters.Add("@Background_check_File_Path", 1 + "#nvarchar#" + model.Background_check_File_Path);
                input_parameters.Add("@Background_check_File_Type", 1 + "#nvarchar#" + model.Background_check_File_Type);
                input_parameters.Add("@Background_check_User_ID", 1 + "#bigint#" + model.Background_check_User_ID);
                input_parameters.Add("@Background_check_IsActive", 1 + "#bit#" + model.Background_check_IsActive);
                input_parameters.Add("@Background_check_IsDelete", 1 + "#bit#" + model.Background_check_IsDelete);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Background_check_Pkey_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);
                objData = obj.SqlCRUD(insertProcedure, input_parameters);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

     
        public List<dynamic> AddUserBackgroundcheckDataDetails(User_Background_ProviderDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                objData = CreateUpdateUserBackgroundcheckData(model);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }
            return objData;
        }

        private DataSet GetUserBackgrMaster(User_Background_ProviderDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_UserBackgroundMaster]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Background_check_PkeyID", 1 + "#bigint#" + model.Background_check_PkeyID);
                input_parameters.Add("@Background_check_User_ID", 1 + "#bigint#" + model.Background_check_User_ID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetUserBackgroundDetails(User_Background_ProviderDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetUserBackgrMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<User_Background_ProviderDTO> UserDocDetails =
                   (from item in myEnumerableFeaprd
                    select new User_Background_ProviderDTO
                    {
                        Background_check_PkeyID = item.Field<Int64>("Background_check_PkeyID"),
                        Background_check_Drop_ID = item.Field<String>("Background_check_Drop_ID"),
                        Background_check_ID = item.Field<String>("Background_check_ID"),
                        Background_check_File_Name = item.Field<String>("Background_check_File_Name"),
                        Background_check_File_Path = item.Field<String>("Background_check_File_Path"),
                        Background_check_File_Type = item.Field<String>("Background_check_File_Type"),
                        Background_check_User_ID = item.Field<Int64>("Background_check_User_ID"),
                        Back_Chk_ProviderName = item.Field<String>("Back_Chk_ProviderName"),
                        Background_check_IsActive = item.Field<Boolean?>("Background_check_IsActive"),

                    }).ToList();

                objDynamic.Add(UserDocDetails);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}