﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class Filter_Admin_Client_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        public List<dynamic> AddUpdate_Filter_Admin_Client(Filter_Admin_Client_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_Filter_Admin_Client_Master]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Client_Filter_PkeyID", 1 + "#bigint#" + model.Client_Filter_PkeyID);
                input_parameters.Add("@Client_Filter_ClientName", 1 + "#nvarchar#" + model.Client_Filter_ClientName);
                input_parameters.Add("@Client_Filter_Address", 1 + "#nvarchar#" + model.Client_Filter_Address);
                input_parameters.Add("@Client_Filter_City", 1 + "#nvarchar#" + model.Client_Filter_City);
                input_parameters.Add("@Client_Filter_State", 1 + "#bigint#" + model.Client_Filter_State);
                input_parameters.Add("@Client_Filter_CLTIsActive", 1 + "#bit#" + model.Client_Filter_CLTIsActive);
                input_parameters.Add("@Client_Filter_IsActive", 1 + "#bit#" + model.Client_Filter_IsActive);
                input_parameters.Add("@Client_Filter_IsDelete", 1 + "#bit#" + model.Client_Filter_IsDelete);
                input_parameters.Add("@Client_Filter_CompanyID", 1 + "#bigint#" + model.Client_Filter_CompanyID);
                input_parameters.Add("@Client_Filter_UserID", 1 + "#bigint#" + model.Client_Filter_UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserId);
                input_parameters.Add("@Client_Filter_PkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        private DataSet Get_Filter_Admin_Client(Filter_Admin_Client_MasterDTO model)
        {
            DataSet ds = null;
            try
            {

                string selectProcedure = "[Get_Filter_Admin_Client_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Client_Filter_PkeyID", 1 + "#bigint#" + model.Client_Filter_PkeyID);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> Get_Filter_Admin_ClientDetails(Filter_Admin_Client_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = Get_Filter_Admin_Client(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Filter_Admin_Client_MasterDTO> Filterclient =
                   (from item in myEnumerableFeaprd
                    select new Filter_Admin_Client_MasterDTO
                    {
                        Client_Filter_PkeyID = item.Field<Int64>("Client_Filter_PkeyID"),
                        Client_Filter_ClientName = item.Field<String>("Client_Filter_ClientName"),
                        Client_Filter_Address = item.Field<String>("Client_Filter_Address"),
                        Client_Filter_City = item.Field<String>("Client_Filter_City"),
                        Client_Filter_State = item.Field<Int64?>("Client_Filter_State"),
                        Client_Filter_CLTIsActive = item.Field<Boolean?>("Client_Filter_CLTIsActive")

                    }).ToList();

                objDynamic.Add(Filterclient);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}