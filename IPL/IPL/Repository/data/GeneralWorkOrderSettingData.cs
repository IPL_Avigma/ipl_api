﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class GeneralWorkOrderSettingData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        //Add General WorkOrder Setting Details
        public List<dynamic> AddGeneralWorkOrderSettingData(GeneralWorkOrderSettingDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();
            string insertProcedure = "[CreateUpdateGeneralWorkOrderSetting]";
            GeneralWorkOrderSetting generalWorkOrderSetting = new GeneralWorkOrderSetting();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@GW_Sett_pkeyID", 1 + "#bigint#" + model.GW_Sett_pkeyID);
                input_parameters.Add("@GW_Sett_CompanyID", 1 + "#bigint#" + model.GW_Sett_CompanyID);
                input_parameters.Add("@GW_Sett_Field_Complete", 1 + "#bit#" + model.GW_Sett_Field_Complete);
                input_parameters.Add("@GW_Sett_Allow_Contractor", 1 + "#bit#" + model.GW_Sett_Allow_Contractor);
                input_parameters.Add("@GW_Sett_Assigned_Unread", 1 + "#bit#" + model.GW_Sett_Assigned_Unread);
                input_parameters.Add("@GW_Sett_Allow_Estimated", 1 + "#bit#" + model.GW_Sett_Allow_Estimated);
                input_parameters.Add("@GW_Sett_Require_Estimated", 1 + "#bit#" + model.GW_Sett_Require_Estimated);
                input_parameters.Add("@GW_Sett_Sent_Ass_Cooradinator", 1 + "#bit#" + model.GW_Sett_Sent_Ass_Cooradinator);
                input_parameters.Add("@GW_Sett_Sent_Ass_Processor", 1 + "#bit#" + model.GW_Sett_Sent_Ass_Processor);
                input_parameters.Add("@GW_Sett_Sent_Email_Multiple", 1 + "#bit#" + model.GW_Sett_Sent_Email_Multiple);
                input_parameters.Add("@GW_Sett_StaffName", 1 + "#bit#" + model.GW_Sett_StaffName);
                input_parameters.Add("@GW_Sett_IsDelete", 1 + "#bit#" + model.GW_Sett_IsDelete);
                input_parameters.Add("@GW_Sett_IsActive", 1 + "#bit#" + model.GW_Sett_IsActive);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@GW_Sett_UserID", 1 + "#bigint#" + model.GW_Sett_UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@GW_Sett_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    generalWorkOrderSetting.GW_Sett_pkeyID = "0";
                    generalWorkOrderSetting.Status = "0";
                    generalWorkOrderSetting.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    generalWorkOrderSetting.GW_Sett_pkeyID = Convert.ToString(objData[0]);
                    generalWorkOrderSetting.Status = Convert.ToString(objData[1]);


                }
                objcltData.Add(generalWorkOrderSetting);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;



        }

        private DataSet GetGeneralworkordersettingMaster(GeneralWorkOrderSettingDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_GeneralWorkOrderSetting]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@GW_Sett_pkeyID", 1 + "#bigint#" + model.GW_Sett_pkeyID);
                input_parameters.Add("@GW_Sett_UserID", 1 + "#bigint#" + model.GW_Sett_UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }
        //Get General work order setting Details 
        public List<dynamic> GetGeneralworkordersettingDetails(GeneralWorkOrderSettingDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetGeneralworkordersettingMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<GeneralWorkOrderSettingDTO> Genralworkordersetting =
                   (from item in myEnumerableFeaprd
                    select new GeneralWorkOrderSettingDTO
                    {
                        GW_Sett_pkeyID = item.Field<Int64>("GW_Sett_pkeyID"),
                        GW_Sett_CompanyID = item.Field<Int64?>("GW_Sett_CompanyID"),
                        GW_Sett_Field_Complete = item.Field<Boolean?>("GW_Sett_Field_Complete"),
                        GW_Sett_Allow_Contractor = item.Field<Boolean?>("GW_Sett_Allow_Contractor"),
                        GW_Sett_Assigned_Unread = item.Field<Boolean?>("GW_Sett_Assigned_Unread"),
                        GW_Sett_Allow_Estimated = item.Field<Boolean?>("GW_Sett_Allow_Estimated"),
                        GW_Sett_Require_Estimated = item.Field<Boolean?>("GW_Sett_Require_Estimated"),
                        GW_Sett_Sent_Ass_Cooradinator = item.Field<Boolean?>("GW_Sett_Sent_Ass_Cooradinator"),
                        GW_Sett_Sent_Ass_Processor = item.Field<Boolean?>("GW_Sett_Sent_Ass_Processor"),
                        GW_Sett_Sent_Email_Multiple = item.Field<Boolean?>("GW_Sett_Sent_Email_Multiple"),
                        GW_Sett_StaffName = item.Field<Boolean?>("GW_Sett_StaffName"),
                        GW_Sett_IsActive = item.Field<Boolean?>("GW_Sett_IsActive"),
                        GW_Sett_UserID = item.Field<Int64?>("GW_Sett_UserID"),


                    }).ToList();

                objDynamic.Add(Genralworkordersetting);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}