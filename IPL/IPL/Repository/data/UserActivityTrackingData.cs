﻿using Avigma.Repository.Lib;
using IPL.Models;
using IPLApp.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class UserActivityTrackingData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        public List<dynamic> AddUserActivityTrackingDetails(UserActivityTrackingDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();

            UserActivityTracking userActivityTracking = new UserActivityTracking();
            string insertProcedure = "[CreateUpdateUserActivityTrackingMaster]";
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@User_Track_PkeyId", 1 + "#bigint#" + model.User_Track_PkeyId);
                input_parameters.Add("@User_Track_UserID", 1 + "#bigint#" + model.User_Track_UserID);
                input_parameters.Add("@User_Track_Captured_On", 1 + "#datetime#" + model.User_Track_Captured_On);
                input_parameters.Add("@User_Track_Mouse_Captured_Count", 1 + "#int#" + model.User_Track_Mouse_Captured_Count);
                input_parameters.Add("@User_Track_Keyboard_Captured_Count", 1 + "#int#" + model.User_Track_Keyboard_Captured_Count);
                input_parameters.Add("@User_Track_Page_Url", 1 + "#nvarchar#" + model.User_Track_Page_Url);
                input_parameters.Add("@User_Track_Screen_Shot_Name", 1 + "#nvarchar#" + model.User_Track_Screen_Shot_Name);
                input_parameters.Add("@User_Track_Created_Date", 1 + "#datetime#" + model.User_Track_Created_Date);
                input_parameters.Add("@User_Track_Captured_From_Ip", 1 + "#nvarchar#" + model.User_Track_Captured_From_Ip);
                input_parameters.Add("@User_Track_Folder_Name", 1 + "#nvarchar#" + model.User_Track_Folder_Name);
                input_parameters.Add("@User_Track_File_Path", 1 + "#nvarchar#" + model.User_Track_File_Path);
                input_parameters.Add("@User_Track_File_Name", 1 + "#nvarchar#" + model.User_Track_File_Name);
                input_parameters.Add("@User_Track_Company_Id", 1 + "#bigint#" + model.User_Track_Company_Id);
                input_parameters.Add("@User_Track_IsActive", 1 + "#bit#" + model.User_Track_IsActive);
                input_parameters.Add("@User_Track_IsDelete", 1 + "#bit#" + model.User_Track_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@User_Track_Time_Frequency", 1 + "#int#" + model.User_Track_Time_Frequency);
                input_parameters.Add("@User_Track_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    userActivityTracking.User_Track_PkeyId = "0";
                    userActivityTracking.Status = "0";
                    userActivityTracking.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    userActivityTracking.User_Track_PkeyId = Convert.ToString(objData[0]);
                    userActivityTracking.Status = Convert.ToString(objData[1]);
                    userActivityTracking.ErrorMessage = "Sucess";

                }
                objcltData.Add(userActivityTracking);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;



        }

        private DataSet GetUserActivityMaster(UserActivityTrackingDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_UserActivityTrackingMaster]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@User_Track_PkeyId", 1 + "#bigint#" + model.User_Track_PkeyId);
                input_parameters.Add("@WhereClause", 1 + "#varchar#" + model.WhereClause);
                input_parameters.Add("@PageNumber", 1 + "#int#" + model.PageNumber);
                input_parameters.Add("@NoofRows", 1 + "#int#" + model.NoofRows);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
             
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetUserActivityTrackingDetails(UserActivityTrackingDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {

               
                DataSet ds = GetUserActivityMaster(model);
                string Totalcount = ds.Tables[1].Rows[0]["count"].ToString();

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<UserActivityTrackingDTO> UserAvtivity =
                   (from item in myEnumerableFeaprd
                    select new UserActivityTrackingDTO
                    {
                        User_Track_PkeyId = item.Field<Int64>("User_Track_PkeyId"),
                        User_Track_UserID = item.Field<Int64?>("User_Track_UserID"),
                        User_Track_Captured_On = item.Field<DateTime?>("User_Track_Captured_On"),
                        User_Track_Mouse_Captured_Count = item.Field<int?>("User_Track_Mouse_Captured_Count"),
                        User_Track_Keyboard_Captured_Count = item.Field<int?>("User_Track_Keyboard_Captured_Count"),
                        User_Track_Page_Url = item.Field<String>("User_Track_Page_Url"),
                        User_Track_Screen_Shot_Name = item.Field<String>("User_Track_Screen_Shot_Name"),
                        User_Track_Created_Date = item.Field<DateTime?>("User_Track_Created_Date"),
                        User_Track_Captured_From_Ip = item.Field<String>("User_Track_Captured_From_Ip"),
                        User_Track_Folder_Name = item.Field<String>("User_Track_Folder_Name"),
                        User_Track_File_Path = item.Field<String>("User_Track_File_Path"),
                        User_Track_File_Name = item.Field<String>("User_Track_File_Name"),
                        User_Track_Company_Id = item.Field<Int64?>("User_Track_Company_Id"),
                        User_Track_IsActive = item.Field<Boolean?>("User_Track_IsActive"),
                        User_Track_Time_Frequency = item.Field<int?>("User_Track_Time_Frequency"),


                    }).ToList();

                objDynamic.Add(UserAvtivity);
                objDynamic.Add(Totalcount);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        public List<dynamic> GetActivityTrackingFilterDetails(UserActivityTrackingFilterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();


            string wherecondition = string.Empty;
            try
            {



                //if (model.fromDate != null && model.toDate != null)
                //{
                //    DateTime toDate = Convert.ToDateTime(model.toDate).AddDays(1);
                //    wherecondition = " And track.User_Track_Created_Date >=   CONVERT(datetime,'" + model.fromDate + "')  " +
                //        "And track.User_Track_Created_Date <=   CONVERT(datetime,'" + toDate + "')";
                //}
                if (model.fromDate != null && model.toDate != null)
                {
                    
                    wherecondition = " And CAST(track.User_Track_Created_Date as date) >=   CONVERT(date,'" + model.fromDate.Value.ToString("yyyy-MM-dd") + "')  " +
                        "And Cast(track.User_Track_Created_Date as date) <=   CONVERT(date,'" + model.toDate.Value.ToString("yyyy-MM-dd") + "')";
                }

                if (model.User_Track_IsActive == true)
                {
                    wherecondition = wherecondition + "  And track.User_Track_IsActive =  1";
                }
                if (model.User_Track_IsActive == false)
                {
                    wherecondition = wherecondition + "  And track.User_Track_IsActive =  0";
                }
                if (model.User_Track_UserID != 0 && model.User_Track_UserID != null)
                {
                    wherecondition = wherecondition + "  And track.User_Track_UserID = "+ model.User_Track_UserID;
                }

                UserActivityTrackingDTO UserActivityTrackingDTO = new UserActivityTrackingDTO();

                UserActivityTrackingDTO.WhereClause = wherecondition;
                UserActivityTrackingDTO.Type = 3;
                UserActivityTrackingDTO.NoofRows = model.NoofRows;
                UserActivityTrackingDTO.PageNumber = model.PageNumber;

                DataSet ds = GetUserActivityMaster(UserActivityTrackingDTO);
                string Totalcount = ds.Tables[1].Rows[0]["count"].ToString();

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<UserActivityTrackingDTO> UserAvtivity =
                   (from item in myEnumerableFeaprd
                    select new UserActivityTrackingDTO
                    {
                        User_Track_PkeyId = item.Field<Int64>("User_Track_PkeyId"),
                        User_Track_UserID = item.Field<Int64?>("User_Track_UserID"),
                        User_Track_Captured_On = item.Field<DateTime?>("User_Track_Captured_On"),
                        User_Track_Mouse_Captured_Count = item.Field<int?>("User_Track_Mouse_Captured_Count"),
                        User_Track_Keyboard_Captured_Count = item.Field<int?>("User_Track_Keyboard_Captured_Count"),
                        User_Track_Page_Url = item.Field<String>("User_Track_Page_Url"),
                        User_Track_Screen_Shot_Name = item.Field<String>("User_Track_Screen_Shot_Name"),
                        User_Track_Created_Date = item.Field<DateTime?>("User_Track_Created_Date"),
                        User_Track_Captured_From_Ip = item.Field<String>("User_Track_Captured_From_Ip"),
                        User_Track_Folder_Name = item.Field<String>("User_Track_Folder_Name"),
                        User_Track_File_Path = item.Field<String>("User_Track_File_Path"),
                        User_Track_File_Name = item.Field<String>("User_Track_File_Name"),
                        User_Track_Company_Id = item.Field<Int64?>("User_Track_Company_Id"),
                        User_Track_IsActive = item.Field<Boolean?>("User_Track_IsActive"),
                        User_Track_Time_Frequency = item.Field<int?>("User_Track_Time_Frequency"),


                    }).ToList();

                objDynamic.Add(UserAvtivity);
                objDynamic.Add(Totalcount);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }

            return objDynamic;
        }


    }
}