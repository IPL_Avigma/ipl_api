﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace IPL.Repository.data
{
    public class TrainingVedioData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        public List<dynamic> AddVedioTrainingMaster(TrainingVedioDTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_Training_Vedio_Master]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Training_Vedio_pkeyID", 1 + "#bigint#" + model.Training_Vedio_pkeyID);
                input_parameters.Add("@Training_Vedio_Name", 1 + "#nvarchar#" + model.Training_Vedio_Name);
                input_parameters.Add("@Training_Vedio_Path", 1 + "#nvarchar#" + model.Training_Vedio_Path);
                input_parameters.Add("@Training_Vedio_Desc", 1 + "#nvarchar#" + model.Training_Vedio_Desc);
                input_parameters.Add("@Training_Vedio_IsActive", 1 + "#bit#" + model.Training_Vedio_IsActive);
                input_parameters.Add("@Training_Vedio_IsDelete", 1 + "#bit#" + model.Training_Vedio_IsDelete);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Training_Vedio_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;



        }
        private DataSet Get_TrainingVedoMaster(TrainingVedioDTO model)
        {
            DataSet ds = null;
            try
            {

                string selectProcedure = "[Get_Training_Vedio_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Training_Vedio_pkeyID", 1 + "#bigint#" + model.Training_Vedio_pkeyID);

                input_parameters.Add("@WhereClause", 1 + "#nvarchar#" + model.WhereClause);
                input_parameters.Add("@PageNumber", 1 + "#int#" + model.PageNumber);
                input_parameters.Add("@Rowcount", 1 + "#int#" + model.Rowcount);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }
        public List<dynamic> Get_Get_TrainingVedoMasterDetails(TrainingVedioDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {

                string wherecondition = string.Empty;
                if (model.Type == 4 && !string.IsNullOrEmpty(model.FilterData))
                {
                    var Data = JsonConvert.DeserializeObject<NewTrainingVedio_Filter>(model.FilterData);
                    if (!string.IsNullOrEmpty(Data.Training_Vedio_Name))
                    {
                        wherecondition = " And vdo.Training_Vedio_Name LIKE '%" + Data.Training_Vedio_Name + "%'";
                    }
                    if (!string.IsNullOrEmpty(Data.Training_Vedio_Path))
                    {
                        wherecondition = " And vdo.Training_Vedio_Path LIKE '%" + Data.Training_Vedio_Path + "%'";
                    }
                    if (!string.IsNullOrEmpty(Data.Training_Vedio_Desc))
                    {
                        wherecondition = " And vdo.Training_Vedio_Desc LIKE '%" + Data.Training_Vedio_Desc + "%'";
                    }

                    model.WhereClause = wherecondition;
                }

                DataSet ds = Get_TrainingVedoMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<TrainingVedioDTO> TrainingVedio =
                   (from item in myEnumerableFeaprd
                    select new TrainingVedioDTO
                    {
                        Training_Vedio_pkeyID = item.Field<Int64>("Training_Vedio_pkeyID"),
                        Training_Vedio_Name = item.Field<String>("Training_Vedio_Name"),
                        Training_Vedio_Path = item.Field<String>("Training_Vedio_Path"),
                        Training_Vedio_Desc = item.Field<String>("Training_Vedio_Desc"),
                        Training_Vedio_IsActive = item.Field<Boolean?>("Training_Vedio_IsActive"),


                    }).ToList();

                objDynamic.Add(TrainingVedio);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}