﻿using Avigma.Repository.Lib;
using IPL.Models;
using IPL.Models.FiveBrothers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace IPL.Repository.data
{
    public class WorkOrder_MSI_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        public List<dynamic> AddWorkOrder_MSIData(WorkOrder_MSI_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_WorkOrder_MSI_Master]";
            WorkOrder_MSI_Master workOrder_MSI_Master = new WorkOrder_MSI_Master();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@WMSI_PkeyId", 1 + "#bigint#" + model.WMSI_PkeyId);
                input_parameters.Add("@WMSI_Order", 1 + "#bigint#" + model.WMSI_Order);
                input_parameters.Add("@WMSI_Property_Id", 1 + "#bigint#" + model.WMSI_Property_Id);
                input_parameters.Add("@WMSI_OrderType", 1 + "#nvarchar#" + model.WMSI_OrderType);
                input_parameters.Add("@WMSI_AssignedDate", 1 + "#datetime#" + model.WMSI_AssignedDate);
                input_parameters.Add("@WMSI_DueDate", 1 + "#datetime#" + model.WMSI_DueDate);
                input_parameters.Add("@WMSI_Priority", 1 + "#nvarchar#" + model.WMSI_Priority);
                input_parameters.Add("@WMSI_Vendor", 1 + "#nvarchar#" + model.WMSI_Vendor);
                input_parameters.Add("@WMSI_Mortgagor", 1 + "#nvarchar#" + model.WMSI_Mortgagor);
                input_parameters.Add("@WMSI_ClientCode", 1 + "#nvarchar#" + model.WMSI_ClientCode);
                input_parameters.Add("@WMSI_Loan", 1 + "#nvarchar#" + model.WMSI_Loan);
                input_parameters.Add("@WMSI_PosRequired", 1 + "#nvarchar#" + model.WMSI_PosRequired);
                input_parameters.Add("@WMSI_LoanType", 1 + "#nvarchar#" + model.WMSI_LoanType);
                input_parameters.Add("@WMSI_sendtoAspen", 1 + "#nvarchar#" + model.WMSI_sendtoAspen);
                input_parameters.Add("@WMSI_HoaContact", 1 + "#nvarchar#" + model.WMSI_HoaContact);
                input_parameters.Add("@WMSI_HoaPhone", 1 + "#nvarchar#" + model.WMSI_HoaPhone);
                input_parameters.Add("@WMSI_FtvDate", 1 + "#datetime#" + model.WMSI_FtvDate);
                input_parameters.Add("@WMSI_InitialSecureDate", 1 + "#datetime#" + model.WMSI_InitialSecureDate);
                input_parameters.Add("@WMSI_LastWinterizationDate", 1 + "#datetime#" + model.WMSI_LastWinterizationDate);
                input_parameters.Add("@WMSI_LastInspectedDate", 1 + "#datetime#" + model.WMSI_LastInspectedDate);
                input_parameters.Add("@WMSI_LastLockChangeDate", 1 + "#datetime#" + model.WMSI_LastLockChangeDate);
                input_parameters.Add("@WMSI_LastSnowRemovalDate", 1 + "#datetime#" + model.WMSI_LastSnowRemovalDate);
                input_parameters.Add("@WMSI_LastInspOccupancy", 1 + "#nvarchar#" + model.WMSI_LastInspOccupancy);
                input_parameters.Add("@WMSI_Lockbox", 1 + "#nvarchar#" + model.WMSI_Lockbox);
                input_parameters.Add("@WMSI_InitialGrassCutDate", 1 + "#datetime#" + model.WMSI_InitialGrassCutDate);
                input_parameters.Add("@WMSI_PropertyType", 1 + "#nvarchar#" + model.WMSI_PropertyType);
                input_parameters.Add("@WMSI_Keycode", 1 + "#nvarchar#" + model.WMSI_Keycode);
                input_parameters.Add("@WMSI_LastGrassCutDate", 1 + "#datetime#" + model.WMSI_LastGrassCutDate);
                input_parameters.Add("@WMSI_Color", 1 + "#nvarchar#" + model.WMSI_Color);
                input_parameters.Add("@WMSI_SecurityAccessCode", 1 + "#nvarchar#" + model.WMSI_SecurityAccessCode);
                input_parameters.Add("@WMSI_LawnSizeSqFt", 1 + "#nvarchar#" + model.WMSI_LawnSizeSqFt);
                input_parameters.Add("@WMSI_IsActive", 1 + "#bit#" + model.WMSI_IsActive);
                input_parameters.Add("@WMSI_IsDelete", 1 + "#bit#" + model.WMSI_IsDelete);
                input_parameters.Add("@WMSI_IsProcessed", 1 + "#bit#" + model.WMSI_IsProcessed);
                input_parameters.Add("@WorkOrder_Import_FkeyId", 1 + "#bigint#" + model.WorkOrder_Import_FkeyId);
                input_parameters.Add("@WMSI_ImportMaster_Pkey", 1 + "#bigint#" + model.WMSI_ImportMaster_Pkey);
                input_parameters.Add("@WMSI_gpsLatitude", 1 + "#varchar#" + model.WMSI_gpsLatitude);
                input_parameters.Add("@WMSI_gpsLongitude", 1 + "#varchar#" + model.WMSI_gpsLongitude);
                input_parameters.Add("@WMSI_Import_File_Name", 1 + "#varchar#" + model.WMSI_Import_File_Name);
                input_parameters.Add("@WMSI_Import_FilePath", 1 + "#varchar#" + model.WMSI_Import_FilePath);
                input_parameters.Add("@WMSI_Import_Pdf_Name", 1 + "#varchar#" + model.WMSI_Import_Pdf_Name);
                input_parameters.Add("@WMSI_Import_Pdf_Path", 1 + "#varchar#" + model.WMSI_Import_Pdf_Path);
                input_parameters.Add("@WMSI_address", 1 + "#varchar#" + model.WMSI_address);
                input_parameters.Add("@WMSI_City", 1 + "#varchar#" + model.WMSI_City);
                input_parameters.Add("@WMSI_State", 1 + "#varchar#" + model.WMSI_State);
                input_parameters.Add("@WMSI_zip", 1 + "#varchar#" + model.WMSI_zip);
                input_parameters.Add("@WMSI_loan_or_investor_type", 1 + "#varchar#" + model.WMSI_loan_or_investor_type);
                input_parameters.Add("@WMSI_point_of_service_question", 1 + "#varchar#" + model.WMSI_point_of_service_question);
                input_parameters.Add("@WMSI_username", 1 + "#varchar#" + model.WMSI_username);
                input_parameters.Add("@WMSI_id", 1 + "#varchar#" + model.WMSI_id);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@WMSI_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    workOrder_MSI_Master.WMSI_PkeyId = "0";
                    workOrder_MSI_Master.Status = "0";
                    workOrder_MSI_Master.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    workOrder_MSI_Master.WMSI_PkeyId = Convert.ToString(objData[0]);
                    workOrder_MSI_Master.Status = Convert.ToString(objData[1]);


                }
                objcltData.Add(workOrder_MSI_Master);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;

        }

        public List<dynamic> AddWorkOrder_MSIItemData(WorkOrder_MSI_Item_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_WorkOrder_MSI_Item_Master]";
            WorkOrder_MSI_Item_Master workOrder_MSI_Item_Master = new WorkOrder_MSI_Item_Master();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@WMSINS_PkeyId", 1 + "#bigint#" + model.WMSINS_PkeyId);
                input_parameters.Add("@WMSINS_InstName", 1 + "#nvarchar#" + model.WMSINS_InstName);
                input_parameters.Add("@WMSINS_Description", 1 + "#nvarchar#" + model.WMSINS_Description);
                input_parameters.Add("@WMSINS_Qty", 1 + "#bigint#" + model.WMSINS_Qty);
                input_parameters.Add("@WMSINS_Price", 1 + "#decimal#" + model.WMSINS_Price);
                input_parameters.Add("@WMSINS_Total", 1 + "#decimal#" + model.WMSINS_Total);
                input_parameters.Add("@WMSINS_Additional_Instructions", 1 + "#nvarchar#" + model.WMSINS_Additional_Instructions);
                input_parameters.Add("@WMSINS_WMSI_FkeyId", 1 + "#bigint#" + model.WMSINS_WMSI_FkeyId);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@WMSINS_PkeyIdOut", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    workOrder_MSI_Item_Master.WMSINS_PkeyId = "0";
                    workOrder_MSI_Item_Master.Status = "0";
                    workOrder_MSI_Item_Master.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    workOrder_MSI_Item_Master.WMSINS_PkeyId = Convert.ToString(objData[0]);
                    workOrder_MSI_Item_Master.Status = Convert.ToString(objData[1]);

                }
                objcltData.Add(workOrder_MSI_Item_Master);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;



        }

        public List<dynamic> AddMSIOrders(FBAPI_RequestDTO model)
        {
            List<dynamic> objDataModel = new List<dynamic>();
            // model.ConPassword = securityHelper.GetMD5Hash(model.ConPassword);

            //string URL = model.ApiUrl.Trim() + "orders" + "?contractor=" + model.ConUserName + "&key=" + model.ConPassword;
            try
            {
                //byte[] credentials = UTF8Encoding.UTF8.GetBytes(model.AuthUserName + ":" + model.AuthPassword);
                //System.Net.Http.HttpClient client = new System.Net.Http.HttpClient();

                //client.BaseAddress = new System.Uri(URL);

                //client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(credentials));

                //client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                //HttpResponseMessage messge = client.GetAsync(URL).Result;
                //if (messge.IsSuccessStatusCode)
                //{
                //string result = messge.Content.ReadAsStringAsync().Result;    
               

                string result = "{\"work_order\":{\"order\":\"8204279\",\"property_id\":\"592738\",\"order_type\":\"PRESALE BID APPROVAL\",\"assigned_date\":\"10/30/2020\",\"due_date\":\"11/2/2020\",\"priority\":\"72 Hour Standard\"},\"instructions\":[{\"instruction_name\":\"key_or_lock_instructions\",\"description\":\"**********WF01 -  BID APPROVAL**********\",\"additional_details\":\"\u2022 A PROPERTY VALIDATION PHOTO IS PROVIDED AT THE BOTTOM OF THIS WORK ORDER. THIS IS TO ENSURE YOU ARE AT THE CORRECT LOCATION AND PROPERTY FOR ANY AND ALL WORK PERFORMED.  YOU MUST VALIDATE YOU ARE AT THE PROVIDED PROPERTY BEFORE ANY WORK IS STARTED. IF THERE ARE ANY QUESTIONS REGARDING PROPERTY VALIDATION, PLEASE CALL MSI FROM SITE FOR DIRECTION AT 1-800-825-4101 (OPTION 9 FOR REGION 9).********DO NOT ENTER PROPERTY UNLESS THERE IS A POST SECURE NOTICE POSTED UPON ARRIVAL AT PROPERTY. IF THERE IS NO POST SECURE NOTICE, CALL FROM SITE 1-800-825-4101 (OPTION 9 FOR REGION 9)**********   \u2022 PROPERTY MAY ALREADY BE SECURED: LOCKBOX _______0552__________/ KEY CODE ________67767___________                                                                                                                                                     \u2022 IF UNABLE TO ENTER USING THE EXISTING KEY CODE, CALL FROM SITE 1-800-825-4101 (OPTION 9 FOR REGION 9).         \u2022 IF UNABLE TO GAIN ACCESS DUE TO GATED COMMUNITY CALL FROM SITE 1-800-825-4101 (OPTION 9 FOR REGION 9) (OBTAIN NAME/PHONE #/FAX # OF HOA, COA, ETC. AS NEEDED).\u2022 IF REPORTED AS OCCUPIED, ATTEMPT DIRECT CONTACT AND REPORT OCCUPIED STATUS ACCESS CODE: A389/67767/0552\"},{\"instruction_name\":\"general_instructions\",\"description\":\"********** WF01 - BID APPROVAL**********\",\"additional_details\":\"RECEIVED BID APPROVAL FOR THE FOLLOWING: (PLEASE COMPLETE AND INVOICE WITHIN THE APPROVED AMOUNT ONLY).1.) BOARD BROKEN WINDOWS. MEASUREMENTS ARE 36X36, 26X17, 36X32, 75X34 (292 UNITED INCHES). WILL REQUIRE PLYWOOD, BOARDS, BOLTS, AND BASIC TOOLS. 2 MEN X 2 HOURS $262.802.) TOTAL $262.803.) PROVIDE PHOTOS TO SUPPORT WORK COMPLETED4.) BID FOR ANY OTHER DAMAGES AT PROPERTY5.)6.)7.)8.)9.)10.)*****PLEASE ADVISE OF ANY HEALTH/HAZARD CONCERNS (INCLUDING ZIKA/WEST NILE)*****\u2022 IF UNABLE TO COMPLETE WITHIN REQUIRED TIMEFRAMES THE REP IS RESPONSIBLE FOR NOTIFYING MSI WITHIN 48 HOURS.\u2022 EMERGENT CONDITIONS: CALL FROM SITE  1-800-825-4101 (OPTION 9 FOR REGION 9) TO REPORT EMERGENT CONDITIONS .  (BASEMENT FLOODING, ACTIVELY LEAKING ROOF WITH VISIBLE HOLES, OPENINGS TO PROPERTY OR OUT BUILDINGS, UNSECURED POOL ETC.)\u2022 VIOLATIONS: IF PROPERTY IS POSTED WITH ANY VIOLATIONS, CONDEMNATIONS, OR DEMOLITION ORDERS, CALL MSI (WF01 REQUIREMENT) AT 1-800-825-4101 - OPTION 9 FOR REGION 9 TO REPORT THE VIOLATION.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       \u2022 BIG 6 DAMAGES: REPORT AND PROVIDE PHOTOS FOR ALL BIG 6 DAMAGES AND STRUCTURAL DAMAGE.                                                                                                                                                                                                                                                                                                                                                                          \u2022 BID REQUIREMENTS: SUBMIT ALL BIDS TO ADDRESS DAMAGES WITH DETAILS INCLUDING BUT NOT LIMITED TO: DIMENSIONS, LOCATION, DESCRIPTION, MATERIALS TO BE USED, ETC.  BIDS MUST BE SUBMITTED FOR ALL ICC ISSUES ON EACH ORDER REGARDLESS IF THEY\u2019VE BEEN BID PREVIOUSLY. BIDS SHOULD INCLUDE REPAIRS TO PREVENT DAMAGE TO PROPERTY. ALL BIDS MUST BE ITEMIZED AND NOT COMBINED (INTERIOR/EXTERIOR, DEBRIS/PERSONALS, REPAIR/REPLACE, ETC.) OR THEY WILL BE REJECTED. ALL BID LINE ITEMS MUST BE SUPPORTED AND JUSTIFIABLE BY A GOVERNMENT COST ESTIMATOR.*****THE REP WILL BE HELD RESPONSIBLE FOR NOT BIDDING ALL CONVEYANCE ITEMS. IF ADDITIONAL ITEMS ARE NEEDED AND PHOTOS DON\u2019T JUSTIFY THE CONDITION CHANGE THEN REP WILL COMPLETE WORK AT THEIR EXPENSE.*****\"},{\"instruction_name\":\"winterization_instructions\",\"description\":\"\",\"additional_details\":\"PLEASE SEE GENERAL INSTRUCTIONS FOR DIRECTION\"},{\"instruction_name\":\"photo_instructions\",\"description\":\"********** WF01 - BID APPROVAL**********\",\"additional_details\":\"SUBMIT CLEAR AND DETAILED PHOTOS WITH DATE STAMPS, AND LABEL BY ROOM AND LOCATION************BEST PRACTICES TO CONSIDER: PHOTOS OF WHITEBOARD OR PAPER WITH #1, #2 TO SUPPORT THE NUMBER OF TRUCK LOADS, BAGS OF DEBRIS, NUMBER OF TREES, ETC.; PHOTOS OF A TAPE MEASURE SHOWING THE SIZE OF TRUCK TRAILER, AREA OF MOLD REMEDIATED, HEIGHT OF GRASS, ETC**           PHOTOS SHOULD INCLUDE BUT ARE NOT LIMITED TO THE FOLLOWING:\u2022 PROPERTY ADDRESS AND STREET SIGN (REQUIRED)\u2022 PHOTO OF STREET SCENE\u2022 SECURE POSTING (REQUIRED) \u2013 MUST BE CLEAR AND ABLE TO BE READ. (REQUIRED IN ORDER TO RECEIVE PAYMENT)\u2022 LOCK CHANGE: BEFORE, DURING, AND AFTER PHOTOS ARE REQUIRED OF THE LOCK CHANGE PROCESS (IF APPLICABLE).\u2022 DAMAGES AND REPAIRS: PHOTOS MUST SUPPORT THE DESCRIPTION, LOCATION, MEASUREMENTS, AND MATERIALS NEEDED AS LISTED IN BIDS (INCLUDING MOLD DAMAGES).\u2022 MOLD: IF MOLD DAMAGES ARE REPORTED, CLEAR PHOTOS USING TAPE MEASURE, PAINTERS TAPE, OR CHALK SHOWING DIMENSIONS ARE REQUIRED. \u2022 ROOF AND CEILING: PHOTOS OF EXTERIOR ROOF AND INTERIOR CEILINGS OF EVERY ROOM REQUIRED REGARDLESS OF CONDITION (MUST SHOW DAMAGES IF DAMAGES ARE REPORTED). \u2022 EXTERIOR REQUIREMENTS: EACH SIDE OF PROPERTY FROM THE SAME ANGLE (FRONT, REAR, AND SIDES). PHOTOS OF ALL EXTERIOR STRUCTURES REQUIRED. PHOTOS SHOULD ALSO BE TAKEN FROM THE HOUSE OUT TOWARD THE PROPERTY LINE.  IF THERE IS A PRIVACY FENCE, PHOTOS SHOULD BE TAKEN ON BOTH SIDES OF FENCE IF POSSIBLE.\u2022 INTERIOR REQUIREMENTS: PHOTOS (WIDE ANGLE) OF EVERY ROOM, INCLUDING: CLOSETS, CEILINGS AND FLOORS, CORNERS, ATTICS, BASEMENTS, OPENED DRAWERS AND CABINETS, ALL APPLIANCES, ELECTRICAL PANELS, STAIRWAYS, HALLWAYS AND FIREPLACES.  \u2022 PERSONAL PROPERTY: MUST HAVE CLEAR PHOTOS OF ALL INTERIOR AND EXTERIOR PERSONALS TO SUPPORT CONDITION OF PROPERTY OR WORK COMPLETED. PROVIDE PHOTOS OF ALL ENTRANCES THAT HAVE PERSONAL PROPERTY POSTINGS AND NOTICE OF RIGHT TO RECLAIM POSTINGS. MUST PROVIDE PHOTOS TO SUPPORT THE NUMBER OF TRUCK LOADS, BAGS OF PERSONALS AND PHOTOS OF TAPE MEASUREMENTS SHOWING THE SIZE OF TRUCK TRAILER ETC.\u2022 STORAGE OF PERSONAL PROPERTY: BEFORE, DURING AND AFTER PHOTOS SHOWING THE STORAGE UNIT WITH THE PERSONAL PROPERTY INSIDE. AFTER PHOTOS ALSO MUST SUPPORT THE STORAGE UNIT WITH THE PERSONAL PROPERTY INSIDE AND THE UNIT IS CLOSED AND LOCKED. MUST PROVIDE A COPY OF STORAGE AGREEMENT/RECEIPT.\u2022 DEBRIS: MUST HAVE CLEAR PHOTOS OF ALL INTERIOR AND EXTERIOR DEBRIS TO SUPPORT CONDITION OF PROPERTY AND WORK COMPLETED. MUST PROVIDE PHOTOS TO SUPPORT THE NUMBER OF TRUCK LOADS, BAGS OF PERSONALS AND PHOTOS OF TAPE MEASUREMENTS SHOWING THE SIZE OF TRUCK TRAILER ETC.\u2022 MULTI UNIT: INCLUDE PHOTOS OF COMMON AREAS SUCH AS STAIRWELLS, HALLWAYS, LOBBIES, ETC. AND LABEL PHOTOS WITH UNIT NUMBER.\u2022 WORK COMPLETED: BEFORE, DURING, AND AFTER PHOTOS (WIDE ANGLE) ARE REQUIRED OF ALL WORK COMPLETED, INCLUDING TRASH AND DEBRIS REMOVAL (TRUCK EMPTY AND FULL, ROOMS FULL AND EMPTY, ETC.)\u2022 BIDS: PHOTOS MUST SUPPORT THE DESCRIPTION, LOCATION, MEASUREMENTS, AND MATERIALS NEEDED AS LISTED IN BIDS.\u2022 APPLIANCES: BEFORE, DURING, AND AFTER PHOTOS REQUIRED OF ALL FIXTURES, INCLUDING ICE MAKERS, DISH WASHERS, WATER HEATER, BLOWING LINES, TURNING OFF BREAKERS, SUMP PUMPS, GENERATOR HOOKED TO COMPRESSOR, GAUGES OF PRESSURE TESTING (AT 35 PSI), ETC.\u2022 ELECTRIC METER: MUST BE ABLE TO READ DETAILS ON METER.\u2022 MOBILE HOMES: MUST PROVIDE CLEAR PHOTOS OF DATA PLATE INCLUDING VIN AND SERIAL NUMBERS. \u2022**********WINTERIZATION SECTION BELOW**********\u2022 BEFORE, DURING, AND AFTER PHOTOS FROM THE SAME ANGLE, OF THE ENTIRE WINTERIZATION PROCESS ARE REQUIRED.\u2022 FROZEN PROPERTY: PHOTOS MUST BE PROVIDED THAT CONFIRM THE PROPERTY IS FROZEN (TANK FROZEN, LINES BURST AND FROSTED, ETC.) \u2022 BREAKERS: PHOTOS OF BREAKERS TURNED TO OFF POSITION, UNLESS SUMP PUMP OR DEHUMIDIFIER IS PRESENT.\u2022 DRAINING: PHOTOS MUST SHOW THE WATER COMING OUT OF A HOSE ATTACHED TO THE HOT WATER HEATER AND DRAINING TO THE EXTERIOR OF THE PROPERTY.\u2022 PLUMBING: PHOTOS TO CONFIRM WATER IS BLOWN OUT OF PIPES AND THAT THE TOILET WATER HAS ALSO BEEN DRAINED BOTH IN THE TANK AND IN THE BOWL.\u2022 TOILETS: IF TOILETS ARE REPORTED DIRTY AND CANNOT COME CLEAN, ACTION SHOTS OF CLEANING ARE REQUIRED.\u2022 PRESSURE GAUGE: PHOTOS OF THE PRESSURE GAUGE ARE REQUIRED ON EVERY WINTERIZATION. THE PRESSURE TEST IS REQUIRED TO HOLD 35 PSI FOR 20 MINUTES.\u2022 NON-TOXIC ANTIFREEZE: PHOTOS OF EVERY TRAP HAVING NON-TOXIC ANTIFREEZE POURED IN THEM MUST BE PROVIDED. THE TRAPS INCLUDE, BUT ARE NOT LIMITED TO: TOILET BOWLS AND TOILET TANKS, DISHWASHER DRAINS, ALL SINKS, SHOWER/TUB DRAINS, WASHING MACHINE DRAIN, ANY EXTERIOR APPLIANCE ATTACHED TO INTERIOR PLUMBING (HOT TUBS), ETC.\u2022 WINTERIZATION STICKERS: PHOTOS OF ALL STICKERS PLACED WITH WELLS FARGO CONTACT INFORMATION.\"}],\"point_of_service_question\":[{\"question\":\"1. Has a code violation or official notice been posted at the property?\",\"answer\":\"1.YES 2. NO 3. N/A\"},{\"question\":\"2. Exterior debris, junk, trash or dead vegetation found at the property?\",\"answer\":\"1.YES 2. NO 3. N/A\"},{\"question\":\"3. Is the lawn landscaped and maintained?\",\"answer\":\"1.YES 2. NO 3. N/A\"},{\"question\":\"4. Any observed Escalated Events or Issues at the property?\",\"answer\":\"1.YES 2. NO 3. N/A\"},{\"question\":\"5. Any unsecured openings,broken windows or doors at the property?\",\"answer\":\"1.YES 2. NO 3. N/A\"},{\"question\":\"6. Is there roof damage that needs repair?\",\"answer\":\"1.YES 2. NO 3. N/A\"},{\"question\":\"7. Is the property vacant?\",\"answer\":\"1.YES 2. NO 3. UNKNOWN\"},{\"question\":\"8. Does the property have indications of vandalism or an obvious crime scene?\",\"answer\":\"1.YES 2. NO 3. N/A\"},{\"question\":\"9. Is there any obvious health and safety issue at the property?\",\"answer\":\"1.YES 2. NO 3. N/A\"},{\"question\":\"10. Does the following damage exist at the property that meets the Big Six definition: Fire, Flood, Tornado, Hurricane Boiler(Multi-Unit), Earthquake?\",\"answer\":\"1.YES 2. NO 3. N/A\"}],\"vendor\":\"MOMDLEES SUMMIT, MO 64064\",\"mortgagor\":\"PUGH, JOSEPH D409  CHESTNUT, MONROE CITY, MO 63456\",\"client\":{\"client_code\":\"WF01\",\"loan\":\"****0552\",\"pos_required\":\"Yes\",\"loan_or_investor_type\":\"VA\",\"send_to_aspen\":\"Yes\",\"hoa_contact\":\"\",\"hoa_phone\":\"\"},\"property_info\":{\"ftv_date\":\"09/22/2020\",\"initial_secure_date\":\"07/30/2020\",\"last_winterization_date\":\"\",\"last_inspected_date\":\"10/28/2020\",\"last_lock_change_date\":\"07/30/2020\",\"last_snow_removal_date\":\"\",\"last_insp_occupancy\":\"VACANT\",\"lockbox\":\"0552\",\"initial_grass_cut_date\":\"\",\"property_type\":\"SINGLE FAMILY\",\"keycode\":\"A389/67767/0552\",\"last_grass_cut_date\":\"10/27/2020\",\"color\":\"\",\"security_access_code\":\"\",\"lawn_size_sq_ft\":\"9,440\"}}";



                WorkOrder_MSI_Item_JsonDTO orders = JsonConvert.DeserializeObject<WorkOrder_MSI_Item_JsonDTO>(result);
                if (orders != null )
                {
                    string strpoint_Of_Service_Question = string.Empty;
                    if (orders.point_of_service_question != null)
                    {
                        strpoint_Of_Service_Question = JsonConvert.SerializeObject(orders.point_of_service_question);
                    }
                    WorkOrder_MSI_MasterDTO workOrder_MSI_MasterDTO = new WorkOrder_MSI_MasterDTO();
                    workOrder_MSI_MasterDTO.WMSI_Order = orders.work_order.order;
                    workOrder_MSI_MasterDTO.WMSI_Property_Id = orders.work_order.property_id;
                    workOrder_MSI_MasterDTO.WMSI_OrderType = orders.work_order.order_type;
                    workOrder_MSI_MasterDTO.WMSI_AssignedDate = orders.work_order.assigned_date;
                    workOrder_MSI_MasterDTO.WMSI_DueDate = orders.work_order.due_date;
                    workOrder_MSI_MasterDTO.WMSI_Priority = orders.work_order.priority;

                    workOrder_MSI_MasterDTO.WMSI_Vendor = orders.vendor;
                    workOrder_MSI_MasterDTO.WMSI_Mortgagor = orders.mortgagor;
                    workOrder_MSI_MasterDTO.WMSI_address = orders.address;
                    workOrder_MSI_MasterDTO.WMSI_City = orders.city;
                    workOrder_MSI_MasterDTO.WMSI_State = orders.state;
                    workOrder_MSI_MasterDTO.WMSI_zip = orders.zip;
                    workOrder_MSI_MasterDTO.WMSI_point_of_service_question = strpoint_Of_Service_Question;
                    workOrder_MSI_MasterDTO.WMSI_username = orders.username;
                    workOrder_MSI_MasterDTO.WMSI_id = orders.id;

                    workOrder_MSI_MasterDTO.WMSI_ClientCode = orders.client.client_code;
                    workOrder_MSI_MasterDTO.WMSI_Loan = orders.client.loan;
                    workOrder_MSI_MasterDTO.WMSI_PosRequired = orders.client.pos_required;
                    workOrder_MSI_MasterDTO.WMSI_LoanType = orders.client.loan_or_investor_type;
                    workOrder_MSI_MasterDTO.WMSI_sendtoAspen = orders.client.send_to_aspen;
                    workOrder_MSI_MasterDTO.WMSI_HoaContact = orders.client.hoa_contact;
                    workOrder_MSI_MasterDTO.WMSI_HoaPhone = orders.client.hoa_phone;
                    workOrder_MSI_MasterDTO.WMSI_loan_or_investor_type = orders.client.loan_or_investor_type;

                    workOrder_MSI_MasterDTO.WMSI_FtvDate = orders.property_info.ftv_date;
                    workOrder_MSI_MasterDTO.WMSI_InitialSecureDate = orders.property_info.initial_secure_date;
                    workOrder_MSI_MasterDTO.WMSI_LastWinterizationDate = orders.property_info.last_winterization_date;
                    workOrder_MSI_MasterDTO.WMSI_LastInspectedDate = orders.property_info.last_inspected_date;
                    workOrder_MSI_MasterDTO.WMSI_LastLockChangeDate = orders.property_info.last_lock_change_date;
                    workOrder_MSI_MasterDTO.WMSI_LastSnowRemovalDate = orders.property_info.last_snow_removal_date;
                    workOrder_MSI_MasterDTO.WMSI_LastInspOccupancy = orders.property_info.last_insp_occupancy;
                    workOrder_MSI_MasterDTO.WMSI_Lockbox = orders.property_info.lockbox;
                    workOrder_MSI_MasterDTO.WMSI_InitialGrassCutDate = orders.property_info.initial_grass_cut_date;
                    workOrder_MSI_MasterDTO.WMSI_PropertyType = orders.property_info.property_type;
                    workOrder_MSI_MasterDTO.WMSI_Keycode = orders.property_info.keycode;
                    workOrder_MSI_MasterDTO.WMSI_LastGrassCutDate = orders.property_info.last_grass_cut_date;
                    workOrder_MSI_MasterDTO.WMSI_Color = orders.property_info.color;
                    workOrder_MSI_MasterDTO.WMSI_SecurityAccessCode = orders.property_info.security_access_code;
                    workOrder_MSI_MasterDTO.WMSI_LawnSizeSqFt = orders.property_info.lawn_size_sq_ft;
                    workOrder_MSI_MasterDTO.UserID = model.UserID;
                    workOrder_MSI_MasterDTO.WorkOrder_Import_FkeyId = model.WI_Pkey_ID;
                    workOrder_MSI_MasterDTO.WMSI_IsActive = true;
                    workOrder_MSI_MasterDTO.WMSI_IsDelete = false;
                    workOrder_MSI_MasterDTO.WMSI_IsProcessed = false;
                    workOrder_MSI_MasterDTO.Type = 1;
                    var objData = AddWorkOrder_MSIData(workOrder_MSI_MasterDTO);
                    if (objData[0].WMSI_PkeyId != "0")
                    {
                        if (orders.instructions != null && orders.instructions != null && orders.instructions.Count > 0)
                        {
                            foreach (var ins in orders.instructions)
                            {
                                WorkOrder_MSI_Item_MasterDTO workOrder_MSI_Item_MasterDTO = new WorkOrder_MSI_Item_MasterDTO();
                                workOrder_MSI_Item_MasterDTO.WMSINS_InstName = ins.instruction_name;
                                workOrder_MSI_Item_MasterDTO.WMSINS_Description = ins.description;
                                workOrder_MSI_Item_MasterDTO.WMSINS_Additional_Instructions = ins.additional_details;
                                workOrder_MSI_Item_MasterDTO.WMSINS_WMSI_FkeyId = Convert.ToInt64(objData[0].WMSI_PkeyId);
                                workOrder_MSI_Item_MasterDTO.UserID = model.UserID;
                                workOrder_MSI_Item_MasterDTO.Type = 1;
                                var ObjItemData = AddWorkOrder_MSIItemData(workOrder_MSI_Item_MasterDTO);
                            }
                        }
                    }
                    objDataModel.Add(objData);
                }
                else
                {
                    log.logDebugMessage("<-------------------No MSI Open Work orders Found----------------->");
                }

                // }
                //string result1 = messge.Content.ReadAsStringAsync().Result;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objDataModel;
        }
        
    }
}