﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class Folder_GroupRole_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        public List<dynamic> AddFolder_GroupRole_Master(Folder_GroupRole_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_Folder_GroupRole_Master]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Fold_Role_Pkey_Id", 1 + "#bigint#" + model.Fold_Role_Pkey_Id);
                input_parameters.Add("@Fold_Role_Parent_Id", 1 + "#bigint#" + model.Fold_Role_Parent_Id);
                input_parameters.Add("@Fold_Role_GroupRole_Id", 1 + "#bigint#" + model.Fold_Role_GroupRole_Id);
                input_parameters.Add("@Fold_Role_IsActive", 1 + "#bit#" + model.Fold_Role_IsActive);
                input_parameters.Add("@Fold_Role_IsDelete", 1 + "#bit#" + model.Fold_Role_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Fold_Role_Pkey_Id_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;



        }
    }
}