﻿using Avigma.Repository.Lib;
using IPL.Models;
using IPLApp.Models;
using IPLApp.Repository.data;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace IPL.Repository.data
{
    public class ClientResultDataMaster
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        private List<dynamic> AddUpdateClientResultBidTaskPost(TaskBidMasterRootObject model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                TaskBidMasterData taskBidMasterData = new TaskBidMasterData();
                TaskBidMasterDTO taskBidMasterDTO = new TaskBidMasterDTO();
                if (model.TaskBidMasterDTO.Count > 0)
                {

                    for (int i = 0; i < model.TaskBidMasterDTO.Count; i++)
                    {
                        taskBidMasterDTO.Bid_Other_Task_Name = string.Empty;
                        taskBidMasterDTO.Task_Bid_pkeyID = model.TaskBidMasterDTO[i].Task_Bid_pkeyID;
                        taskBidMasterDTO.Task_Bid_TaskID = model.TaskBidMasterDTO[i].Task_Bid_TaskID;


                        taskBidMasterDTO.Task_Bid_WO_ID = model.Task_Bid_WO_ID;
                        taskBidMasterDTO.Task_Bid_Qty = model.TaskBidMasterDTO[i].Task_Bid_Qty;
                        taskBidMasterDTO.Task_Bid_Uom_ID = model.TaskBidMasterDTO[i].Task_Bid_Uom_ID;
                        taskBidMasterDTO.Task_Bid_Cont_Price = model.TaskBidMasterDTO[i].Task_Bid_Cont_Price;
                        taskBidMasterDTO.Task_Bid_Cont_Total = model.TaskBidMasterDTO[i].Task_Bid_Cont_Total;
                        taskBidMasterDTO.Task_Bid_Clnt_Price = model.TaskBidMasterDTO[i].Task_Bid_Clnt_Price;
                        taskBidMasterDTO.Task_Bid_Clnt_Total = model.TaskBidMasterDTO[i].Task_Bid_Clnt_Total;
                        taskBidMasterDTO.Task_Bid_Comments = model.TaskBidMasterDTO[i].Task_Bid_Comments;
                        taskBidMasterDTO.Task_Bid_Violation = model.TaskBidMasterDTO[i].Task_Bid_Violation;
                        taskBidMasterDTO.Task_Bid_damage = model.TaskBidMasterDTO[i].Task_Bid_damage;
                        //if (model.TaskBidMasterDTO[i].Task_Bid_Violation == false) {
                        //    taskBidMasterDTO.Task_Bid_damage = true;
                        //}

                        taskBidMasterDTO.Task_Bid_IsActive = model.TaskBidMasterDTO[i].Task_Bid_IsActive;
                        taskBidMasterDTO.Task_Bid_IsDelete = model.TaskBidMasterDTO[i].Task_Bid_IsDelete;
                        taskBidMasterDTO.UserID = model.UserID;
                        taskBidMasterDTO.Task_Bid_Hazards = model.TaskBidMasterDTO[i].Task_Bid_Hazards;
                        //taskBidMasterDTO.Task_Bid_DamageItem = model.TaskBidMasterDTO[i].Task_Bid_DamageItem;
                        //taskBidMasterDTO.Type = model.Type;

                        taskBidMasterDTO.Task_Ext_pkeyID = model.TaskBidMasterDTO[i].Task_Ext_pkeyID;
                        taskBidMasterDTO.Task_Ext_BidID = model.TaskBidMasterDTO[i].Task_Ext_BidID;
                        taskBidMasterDTO.Task_Ext_WO_ID = model.Task_Bid_WO_ID;
                        taskBidMasterDTO.Task_Ext_DamageCauseId = model.TaskBidMasterDTO[i].Task_Ext_DamageCauseId;
                        taskBidMasterDTO.Task_Ext_Location = model.TaskBidMasterDTO[i].Task_Ext_Location;
                        taskBidMasterDTO.Task_Ext_Length = model.TaskBidMasterDTO[i].Task_Ext_Length;
                        taskBidMasterDTO.Task_Ext_Width = model.TaskBidMasterDTO[i].Task_Ext_Width;
                        taskBidMasterDTO.Task_Ext_Height = model.TaskBidMasterDTO[i].Task_Ext_Height;
                        taskBidMasterDTO.Task_Ext_Men = model.TaskBidMasterDTO[i].Task_Ext_Men;
                        taskBidMasterDTO.Task_Ext_Hours = model.TaskBidMasterDTO[i].Task_Ext_Hours;
                        taskBidMasterDTO.Task_Ext_IsActive = model.TaskBidMasterDTO[i].Task_Ext_IsActive;

                        if (model.Type == 4)
                        {
                            taskBidMasterDTO.Type = 4;
                        }
                        else
                        {

                            if (model.TaskBidMasterDTO[i].Task_Bid_pkeyID != 0)
                            {
                                if (model.TaskBidMasterDTO[i].Task_Bid_WO_ID != model.Task_Bid_WO_ID)
                                {
                                    taskBidMasterDTO.Type = 1;
                                }
                                else
                                {
                                    if (model.TaskBidMasterDTO[i].Task_Bid_TaskID == -99)
                                    {
                                        taskBidMasterDTO.Task_Bid_TaskID = 0;
                                        taskBidMasterDTO.Bid_Other_Task_Name = model.TaskBidMasterDTO[i].Bid_Other_Task_Name;
                                        if (taskBidMasterDTO.Task_Bid_pkeyID != 0)
                                        {
                                            taskBidMasterDTO.Type = 2;
                                        }
                                        else
                                        {
                                            taskBidMasterDTO.Type = 1;
                                        }
                                       
                                    }
                                    else
                                    {
                                        taskBidMasterDTO.Type = 2;
                                    }

                                }

                            }
                            else
                            {
                                taskBidMasterDTO.Type = 1;
                                if (model.TaskBidMasterDTO[i].Task_Bid_TaskID == -99)
                                {
                                    taskBidMasterDTO.Task_Bid_TaskID = 0;
                                    taskBidMasterDTO.Bid_Other_Task_Name = model.TaskBidMasterDTO[i].Bid_Other_Task_Name;
                                    taskBidMasterDTO.Type = 1;
                                }
                            }
                        }

                        objData = taskBidMasterData.AddTaskBidData(taskBidMasterDTO);
                        if (objData[0].Status == "1")
                        {
                            taskBidMasterDTO.Task_Ext_BidID = taskBidMasterDTO.Type == 1 ? Convert.ToInt64(objData[0].Task_Bid_pkeyID) : taskBidMasterDTO.Task_Bid_pkeyID;
                            var objExtData = taskBidMasterData.AddTaskBidExtDetail(taskBidMasterDTO);

                        }
                    }


                    //Add New Access Log 12-12-2022
                    Access_log_MasterData access_Log_MasterData = new Access_log_MasterData();
                    NewAccessLogMaster newAccessLogMaster = new NewAccessLogMaster();
                    newAccessLogMaster.Access_WorkerOrderID = model.Task_Bid_WO_ID;
                    newAccessLogMaster.Access_Master_ID = 37;
                    newAccessLogMaster.Access_UserID = model.UserID;
                    newAccessLogMaster.Type = 1;
                    access_Log_MasterData.AddNewAccessLogMaster(newAccessLogMaster);
                }
                return objData;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return objData;
            }
        }

        public List<dynamic> ClientResultBidTaskPost(TaskBidMasterRootObject model)
        {
            List<dynamic> objData = new List<dynamic>();
            //List<dynamic> objAddData = new List<dynamic>();

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();

            try
            {


                if (!string.IsNullOrEmpty(model.str_Task_Bid_WO_ID))
                {
                    var Data = JsonConvert.DeserializeObject<List<TaskActionWorkOrder>>(model.str_Task_Bid_WO_ID);
                    for (int j = 0; j < Data.Count; j++)
                    {

                        model.Task_Bid_WO_ID = Data[j].Task_WO_ID;
                        objData = AddUpdateClientResultBidTaskPost(model);
                    }
                }
                else
                {
                    objData = AddUpdateClientResultBidTaskPost(model);
                }




            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objData;

        }


        // post clinet invoice data completion 
        private List<dynamic> AddUpdateClientResultInvoicePost(TaskInvoiceMasterRootObject model)
        {
            List<dynamic> objData = new List<dynamic>();
            // List<dynamic> objAddData = new List<dynamic>();

            Task_Invoice_MasterDTO task_Invoice_MasterDTO = new Task_Invoice_MasterDTO();
            Task_Invoice_MasterData task_Invoice_MasterData = new Task_Invoice_MasterData();

            try
            {
                if (model.Task_Invoice_MasterDTO.Count > 0)
                {

                    for (int i = 0; i < model.Task_Invoice_MasterDTO.Count; i++)
                    {
                        task_Invoice_MasterDTO.Com_Other_Task_Name = string.Empty;
                        task_Invoice_MasterDTO.Task_Inv_pkeyID = model.Task_Invoice_MasterDTO[i].Task_Inv_pkeyID;
                        task_Invoice_MasterDTO.Task_Inv_WO_ID = model.Task_Inv_WO_ID;
                        task_Invoice_MasterDTO.Task_Inv_TaskID = model.Task_Invoice_MasterDTO[i].Task_Inv_TaskID;
                        task_Invoice_MasterDTO.Task_Inv_Qty = model.Task_Invoice_MasterDTO[i].Task_Inv_Qty;
                        task_Invoice_MasterDTO.Task_Inv_Uom_ID = model.Task_Invoice_MasterDTO[i].Task_Inv_Uom_ID;
                        task_Invoice_MasterDTO.Task_Inv_Cont_Price = model.Task_Invoice_MasterDTO[i].Task_Inv_Cont_Price;
                        task_Invoice_MasterDTO.Task_Inv_Cont_Total = model.Task_Invoice_MasterDTO[i].Task_Inv_Cont_Total;
                        task_Invoice_MasterDTO.Task_Inv_Clnt_Price = model.Task_Invoice_MasterDTO[i].Task_Inv_Clnt_Price;
                        task_Invoice_MasterDTO.Task_Inv_Clnt_Total = model.Task_Invoice_MasterDTO[i].Task_Inv_Clnt_Total;
                        task_Invoice_MasterDTO.Task_Inv_Comments = model.Task_Invoice_MasterDTO[i].Task_Inv_Comments;
                        task_Invoice_MasterDTO.Task_Inv_IsActive = model.Task_Invoice_MasterDTO[i].Task_Inv_IsActive;
                        task_Invoice_MasterDTO.Task_Inv_IsDelete = model.Task_Invoice_MasterDTO[i].Task_Inv_IsDelete;
                        task_Invoice_MasterDTO.Task_Inv_Violation = model.Task_Invoice_MasterDTO[i].Task_Inv_Violation;
                        task_Invoice_MasterDTO.Task_Inv_damage = model.Task_Invoice_MasterDTO[i].Task_Inv_damage;
                        task_Invoice_MasterDTO.Task_Inv_Auto_Invoice = model.Task_Invoice_MasterDTO[i].Task_Inv_Auto_Invoice;
                        task_Invoice_MasterDTO.Task_Inv_Hazards = model.Task_Invoice_MasterDTO[i].Task_Inv_Hazards;
                        task_Invoice_MasterDTO.UserID = model.UserID;

                        task_Invoice_MasterDTO.Task_Ext_pkeyID = model.Task_Invoice_MasterDTO[i].Task_Ext_pkeyID;
                        task_Invoice_MasterDTO.Task_Ext_CompletionID = model.Task_Invoice_MasterDTO[i].Task_Ext_CompletionID;
                        task_Invoice_MasterDTO.Task_Ext_WO_ID = model.Task_Inv_WO_ID;
                        task_Invoice_MasterDTO.Task_Ext_DamageCauseId = model.Task_Invoice_MasterDTO[i].Task_Ext_DamageCauseId;
                        task_Invoice_MasterDTO.Task_Ext_Location = model.Task_Invoice_MasterDTO[i].Task_Ext_Location;
                        task_Invoice_MasterDTO.Task_Ext_Length = model.Task_Invoice_MasterDTO[i].Task_Ext_Length;
                        task_Invoice_MasterDTO.Task_Ext_Width = model.Task_Invoice_MasterDTO[i].Task_Ext_Width;
                        task_Invoice_MasterDTO.Task_Ext_Height = model.Task_Invoice_MasterDTO[i].Task_Ext_Height;
                        task_Invoice_MasterDTO.Task_Ext_Men = model.Task_Invoice_MasterDTO[i].Task_Ext_Men;
                        task_Invoice_MasterDTO.Task_Ext_Hours = model.Task_Invoice_MasterDTO[i].Task_Ext_Hours;
                        task_Invoice_MasterDTO.Task_Ext_IsActive = model.Task_Invoice_MasterDTO[i].Task_Ext_IsActive;

                        if (model.Type == 4)
                        {
                            task_Invoice_MasterDTO.Type = 4;
                        }
                        else
                        {
                            if (model.Task_Invoice_MasterDTO[i].Task_Inv_pkeyID != 0)
                            {
                                if (model.Task_Invoice_MasterDTO[i].Task_Inv_TaskID == -99)
                                {
                                    task_Invoice_MasterDTO.Task_Inv_TaskID = 0;
                                    task_Invoice_MasterDTO.Com_Other_Task_Name = model.Task_Invoice_MasterDTO[i].Com_Other_Task_Name;
                                    if (task_Invoice_MasterDTO.Task_Inv_pkeyID != 0)
                                    {
                                        task_Invoice_MasterDTO.Type = 2;
                                    }
                                    else
                                    {
                                        task_Invoice_MasterDTO.Type = 1;
                                    }
                                   
                                }
                                else
                                {
                                    task_Invoice_MasterDTO.Type = 2;
                                }

                            }
                            else
                            {
                                task_Invoice_MasterDTO.Type = 1;
                                if (model.Task_Invoice_MasterDTO[i].Task_Inv_TaskID == -99)
                                {
                                    task_Invoice_MasterDTO.Task_Inv_TaskID = 0;
                                    task_Invoice_MasterDTO.Com_Other_Task_Name = model.Task_Invoice_MasterDTO[i].Com_Other_Task_Name;
                                    task_Invoice_MasterDTO.Type = 1;
                                }
                            }

                        }

                        objData = task_Invoice_MasterData.AddTaskInvoiceData(task_Invoice_MasterDTO);
                        if (objData[0].Status == "1")
                        {
                            task_Invoice_MasterDTO.Task_Ext_CompletionID = task_Invoice_MasterDTO.Type == 1 ? Convert.ToInt64(objData[0].Task_Inv_pkeyID) : task_Invoice_MasterDTO.Task_Inv_pkeyID;
                            var objExtData = task_Invoice_MasterData.AddTaskCompletionExtDetail(task_Invoice_MasterDTO);

                        }
                    }


                    //Add New Access Log 12-12-2022
                    //Access_log_MasterData access_Log_MasterData = new Access_log_MasterData();
                    //NewAccessLogMaster newAccessLogMaster = new NewAccessLogMaster();
                    //newAccessLogMaster.Access_WorkerOrderID = model.Task_Inv_WO_ID;
                    //newAccessLogMaster.Access_Master_ID = 47;
                    //newAccessLogMaster.Access_UserID = model.UserID;
                    //newAccessLogMaster.Type = 1;
                    //access_Log_MasterData.AddNewAccessLogMaster(newAccessLogMaster);
                }

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objData;
        }


        public List<dynamic> ClientResultInvoicePost(TaskInvoiceMasterRootObject model)
        {
            List<dynamic> objData = new List<dynamic>();
            //List<dynamic> objAddData = new List<dynamic>();

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {

                if (!string.IsNullOrEmpty(model.str_Task_Bid_WO_ID))
                {
                    var Data = JsonConvert.DeserializeObject<List<TaskActionWorkOrder>>(model.str_Task_Bid_WO_ID);
                    for (int j = 0; j < Data.Count; j++)
                    {
                        model.Task_Inv_WO_ID = Data[j].Task_WO_ID;
                        
                        objData = AddUpdateClientResultInvoicePost(model);
                    }
                }
                else
                {
                    objData = AddUpdateClientResultInvoicePost(model);
                }




            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objData;



        }


        // post clinet Task Damage
        private List<dynamic> AddUpdateClientResultDamagePost(TaskDamageMasterRootObject model)
        {
            List<dynamic> objData = new List<dynamic>();
            //List<dynamic> objAddData = new List<dynamic>();



            TaskDamageMaster taskDamageMaster = new TaskDamageMaster();
            TaskDamageMasterData taskDamageMasterData = new TaskDamageMasterData();

            try
            {
                if (model.TaskDamageMaster.Count > 0)
                {

                    for (int i = 0; i < model.TaskDamageMaster.Count; i++)
                    {
                        taskDamageMaster.Task_Damage_WO_ID = model.Task_Inv_WO_ID;
                        taskDamageMaster.Task_Damage_pkeyID = model.TaskDamageMaster[i].Task_Damage_pkeyID;
                       
                        taskDamageMaster.Task_Damage_Task_ID = model.TaskDamageMaster[i].Task_Damage_Task_ID;
                        if (model.TaskDamageMaster[i].Task_Damage_Type != null)
                        {
                            taskDamageMaster.Task_Damage_ID = Convert.ToInt64(model.TaskDamageMaster[i].Task_Damage_Type);
                        }

                        taskDamageMaster.Task_Damage_ID = model.TaskDamageMaster[i].Task_Damage_ID;
                        taskDamageMaster.Task_Damage_Type = model.TaskDamageMaster[i].Task_Damage_Type;
                        taskDamageMaster.Task_Damage_Int = model.TaskDamageMaster[i].Task_Damage_Int;
                        taskDamageMaster.Task_Damage_Location = model.TaskDamageMaster[i].Task_Damage_Location;
                        taskDamageMaster.Task_Damage_Qty = model.TaskDamageMaster[i].Task_Damage_Qty;
                        taskDamageMaster.Task_Damage_Estimate = model.TaskDamageMaster[i].Task_Damage_Estimate;
                        taskDamageMaster.Task_Damage_Disc = model.TaskDamageMaster[i].Task_Damage_Disc;
                        taskDamageMaster.Task_Damage_IsActive = model.TaskDamageMaster[i].Task_Damage_IsActive;
                        taskDamageMaster.Task_Damage_IsDelete = model.TaskDamageMaster[i].Task_Damage_IsDelete;
                        taskDamageMaster.Task_Damage_Status = model.TaskDamageMaster[i].Task_Damage_Status;
                        taskDamageMaster.UserID = model.UserID;

                        if (model.Type == 4)
                        {

                            taskDamageMaster.Type = 4;
                        }
                        //else
                        //{

                        //    if (model.TaskDamageMaster[i].Task_Damage_pkeyID != 0)
                        //    {
                        //        taskDamageMaster.Type = 2;
                        //    }
                        //    else
                        //    {
                        //        taskDamageMaster.Type = 1;
                        //    }


                        //}

                        else
                        {

                            if (model.TaskDamageMaster[i].Task_Damage_pkeyID != 0)
                            {
                                //if (model.TaskDamageMaster[i].Task_Damage_WO_ID != model.Task_Damage_WO_ID)
                                //{
                                //    taskDamageMaster.Type = 1;
                                //}
                                //else
                                //{
                                    if (model.TaskDamageMaster[i].Task_Damage_ID == -99)
                                    {
                                        taskDamageMaster.Task_Damage_Task_ID = 0;
                                        taskDamageMaster.Task_Damage_Other_Name = model.TaskDamageMaster[i].Task_Damage_Other_Name;
                                        if (taskDamageMaster.Task_Damage_pkeyID != 0)
                                        {
                                            taskDamageMaster.Type = 2;
                                        }
                                        else
                                        {
                                            taskDamageMaster.Type = 1;
                                        }

                                    }
                                    else
                                    {
                                        taskDamageMaster.Type = 2;
                                    }

                                //}

                            }
                            else
                            {
                                taskDamageMaster.Type = 1;
                                if (model.TaskDamageMaster[i].Task_Damage_ID == -99)
                                {
                                    taskDamageMaster.Task_Damage_Task_ID = 0;
                                    taskDamageMaster.Task_Damage_Other_Name = model.TaskDamageMaster[i].Task_Damage_Other_Name;
                                    taskDamageMaster.Type = 1;
                                }
                            }
                        }


                        objData = taskDamageMasterData.AddTaskDamageMasterData(taskDamageMaster);
                    }


                    //Add New Access Log 12-12-2022
                    Access_log_MasterData access_Log_MasterData = new Access_log_MasterData();
                    NewAccessLogMaster newAccessLogMaster = new NewAccessLogMaster();
                    newAccessLogMaster.Access_WorkerOrderID = model.Task_Inv_WO_ID;
                    newAccessLogMaster.Access_Master_ID = 38;
                    newAccessLogMaster.Access_UserID = model.UserID;
                    newAccessLogMaster.Type = 1;
                    access_Log_MasterData.AddNewAccessLogMaster(newAccessLogMaster);
                }

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                log.logErrorMessage("AddUpdateClientResultDamagePost-------------->" + model.Task_Inv_WO_ID);
            }



            return objData;
        }
        public List<dynamic> ClientResultDamagePost(TaskDamageMasterRootObject model)
        {

            List<dynamic> objData = new List<dynamic>();
            //List<dynamic> objAddData = new List<dynamic>();

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {

                if (!string.IsNullOrEmpty(model.str_Task_Bid_WO_ID))
                {
                    var Data = JsonConvert.DeserializeObject<List<TaskActionWorkOrder>>(model.str_Task_Bid_WO_ID);
                    for (int j = 0; j < Data.Count; j++)
                    {
                        model.Task_Inv_WO_ID = Data[j].Task_WO_ID;
                        objData = AddUpdateClientResultDamagePost(model);
                    }
                }
                else
                {
                    objData = AddUpdateClientResultDamagePost(model);
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objData;


        }


        // post Client  invoice  contractor page 
        public List<dynamic> ClientResultInvoiceContractorPost(Invoice_ContractorDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objAddData = new List<dynamic>();

            Invoice_ContractorData invoice_ContractorData = new Invoice_ContractorData();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();


            try
            {

                Invoice_ContractorDTO invoice_ContractorDTO = new Invoice_ContractorDTO();

                invoice_ContractorDTO.Inv_Con_pkeyId = model.Inv_Con_pkeyId;
                invoice_ContractorDTO.Inv_Con_Invoice_Id = model.Inv_Con_Invoice_Id;
                invoice_ContractorDTO.Inv_Con_TaskId = model.Inv_Con_TaskId;
                invoice_ContractorDTO.Inv_Con_Wo_ID = model.Inv_Con_Wo_ID;
                invoice_ContractorDTO.Inv_Con_Uom_Id = model.Inv_Con_Uom_Id;
                invoice_ContractorDTO.Inv_Con_Sub_Total = model.Inv_Con_Sub_Total;

                invoice_ContractorDTO.Inv_Con_ContDiscount = model.Inv_Con_ContDiscount;
                invoice_ContractorDTO.Inv_Con_ContTotal = model.Inv_Con_ContTotal;
                invoice_ContractorDTO.Inv_Con_Short_Note = model.Inv_Con_Short_Note;
                invoice_ContractorDTO.Inv_Con_Inv_Followup = model.Inv_Con_Inv_Followup;
                invoice_ContractorDTO.Inv_Con_Inv_Comment = model.Inv_Con_Inv_Comment;
                invoice_ContractorDTO.Inv_Con_Ref_ID = model.Inv_Con_Ref_ID;
                invoice_ContractorDTO.Inv_Con_Followup_Com = model.Inv_Con_Followup_Com;
                invoice_ContractorDTO.Inv_Con_Invoce_Num = model.Inv_Con_Invoce_Num;
                invoice_ContractorDTO.Inv_Con_Inv_Date = model.Inv_Con_Inv_Date;
                invoice_ContractorDTO.Inv_Con_Inv_Hold_Date = model.Inv_Con_Inv_Hold_Date;
                invoice_ContractorDTO.Inv_Con_Status = model.Inv_Con_Status;
                invoice_ContractorDTO.Inv_Con_DiscountAmount = model.Inv_Con_DiscountAmount;
                invoice_ContractorDTO.Inv_Con_IsActive = model.Inv_Con_IsActive;
                invoice_ContractorDTO.Inv_Con_IsDelete = model.Inv_Con_IsDelete;

                invoice_ContractorDTO.Inv_Con_Auto_Invoice = model.Inv_Con_Auto_Invoice;
                invoice_ContractorDTO.Inv_Con_Inv_Approve_Date = model.Inv_Con_Inv_Approve_Date;
                invoice_ContractorDTO.Inv_Con_Inv_Approve = model.Inv_Con_Inv_Approve;
                invoice_ContractorDTO.UserID = model.UserID;

                if (model.Type == 4)
                {

                    Invoice_Contractor_ChildDTO invoice_Contractor_ChildDTO = new Invoice_Contractor_ChildDTO();
                    Invoice_Contractor_ChildData invoice_Contractor_ChildData = new Invoice_Contractor_ChildData();

                    for (int i = 0; i < model.Invoice_Contractor_ChildDTO.Count; i++)
                    {

                        invoice_Contractor_ChildDTO.Inv_Con_Ch_pkeyId = model.Invoice_Contractor_ChildDTO[i].Inv_Con_Ch_pkeyId;
                        invoice_Contractor_ChildDTO.UserID = model.UserID;
                        invoice_Contractor_ChildDTO.Type = model.Type;
                        invoice_Contractor_ChildDTO.Inv_Con_Ch_Other_Task_Name = model.Invoice_Contractor_ChildDTO[i].Inv_Con_Ch_Other_Task_Name;
                        var retunePkey = invoice_Contractor_ChildData.AddInvoiceContractorChildData(invoice_Contractor_ChildDTO);

                    }
                }
                else
                {
                    if (model.Inv_Con_pkeyId != 0)
                    {
                        invoice_ContractorDTO.Type = 2;
                    }
                    else
                    {
                        invoice_ContractorDTO.Type = model.Type;
                    }


                    objData = invoice_ContractorData.AddInvoiceContractorData(invoice_ContractorDTO);

                    objAddData.Add(objData);

                    if (objData[0] != 0 && model.Type !=5)
                    {
                         
                        if (model.Invoice_Contractor_ChildDTO.Count > 0)
                        {

                            Invoice_Contractor_ChildDTO invoice_Contractor_ChildDTO = new Invoice_Contractor_ChildDTO();
                            Invoice_Contractor_ChildData invoice_Contractor_ChildData = new Invoice_Contractor_ChildData();

                            for (int i = 0; i < model.Invoice_Contractor_ChildDTO.Count; i++)
                            {

                                invoice_Contractor_ChildDTO.Inv_Con_Ch_pkeyId = model.Invoice_Contractor_ChildDTO[i].Inv_Con_Ch_pkeyId;
                                if (model.Inv_Con_pkeyId != 0)
                                {
                                    invoice_Contractor_ChildDTO.Inv_Con_Ch_ContractorId = model.Inv_Con_pkeyId;
                                }
                                else
                                {
                                    invoice_Contractor_ChildDTO.Inv_Con_Ch_ContractorId = objData[0];
                                }

                                invoice_Contractor_ChildDTO.Inv_Con_Ch_InvoiceId = model.Invoice_Contractor_ChildDTO[i].Inv_Con_Ch_InvoiceId;
                                invoice_Contractor_ChildDTO.Inv_Con_Ch_TaskId = model.Invoice_Contractor_ChildDTO[i].Inv_Con_Ch_TaskId;
                                invoice_Contractor_ChildDTO.Inv_Con_Ch_Wo_Id = model.Inv_Con_Wo_ID;
                                invoice_Contractor_ChildDTO.Inv_Con_Ch_Uom_Id = model.Invoice_Contractor_ChildDTO[i].Inv_Con_Ch_Uom_Id;
                                invoice_Contractor_ChildDTO.Inv_Con_Ch_Qty = model.Invoice_Contractor_ChildDTO[i].Inv_Con_Ch_Qty;
                                invoice_Contractor_ChildDTO.Inv_Con_Ch_Price = model.Invoice_Contractor_ChildDTO[i].Inv_Con_Ch_Price;
                                invoice_Contractor_ChildDTO.Inv_Con_Ch_Total = model.Invoice_Contractor_ChildDTO[i].Inv_Con_Ch_Total;
                                invoice_Contractor_ChildDTO.Inv_Con_Ch_Adj_Price = model.Invoice_Contractor_ChildDTO[i].Inv_Con_Ch_Adj_Price;
                                invoice_Contractor_ChildDTO.Inv_Con_Ch_Adj_Total = model.Invoice_Contractor_ChildDTO[i].Inv_Con_Ch_Adj_Total;
                                invoice_Contractor_ChildDTO.Inv_Con_Ch_Comment = model.Invoice_Contractor_ChildDTO[i].Inv_Con_Ch_Comment;
                                invoice_Contractor_ChildDTO.Inv_Con_Ch_IsActive = model.Invoice_Contractor_ChildDTO[i].Inv_Con_Ch_IsActive;
                                invoice_Contractor_ChildDTO.Inv_Con_Ch_IsDelete = model.Invoice_Contractor_ChildDTO[i].Inv_Con_Ch_IsDelete;

                                invoice_Contractor_ChildDTO.Inv_Con_Ch_Auto_Invoice = model.Invoice_Contractor_ChildDTO[i].Inv_Con_Ch_Auto_Invoice;
                                invoice_Contractor_ChildDTO.Inv_Con_Ch_Client_ID = model.Invoice_Contractor_ChildDTO[i].Inv_Con_Ch_Client_ID;
                                invoice_Contractor_ChildDTO.Inv_Con_Ch_Flate_fee = model.Invoice_Contractor_ChildDTO[i].Inv_Con_Ch_Flate_fee;
                                invoice_Contractor_ChildDTO.Inv_Con_Ch_Discount = model.Invoice_Contractor_ChildDTO[i].Inv_Con_Ch_Discount;
                                invoice_Contractor_ChildDTO.Inv_Con_Ch_Other_Task_Name = model.Invoice_Contractor_ChildDTO[i].Inv_Con_Ch_Other_Task_Name;
                                invoice_Contractor_ChildDTO.UserID = model.UserID;


                                if (model.Invoice_Contractor_ChildDTO[i].Inv_Con_Ch_pkeyId != 0)
                                {
                                    invoice_Contractor_ChildDTO.Type = 2;
                                }
                                else
                                {
                                    invoice_Contractor_ChildDTO.Type = model.Type;
                                }

                                var retunePkey = invoice_Contractor_ChildData.AddInvoiceContractorChildData(invoice_Contractor_ChildDTO);
                            }
                        }
                    }
                }













            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return objAddData;



        }


        // post Client  invoice  Client page 
        public List<dynamic> ClientResultInvoiceClientPost(Invoice_ClientDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objAddData = new List<dynamic>();

            Invoice_ClientData invoice_ClientData = new Invoice_ClientData();
            try
            {

                Invoice_ClientDTO invoice_ClientDTO = new Invoice_ClientDTO();

                invoice_ClientDTO.Inv_Client_pkeyId = model.Inv_Client_pkeyId;
                invoice_ClientDTO.Inv_Client_Invoice_Id = model.Inv_Client_Invoice_Id;
                invoice_ClientDTO.Inv_Client_WO_Id = model.Inv_Client_WO_Id;
                invoice_ClientDTO.Inv_Client_Task_Id = model.Inv_Client_Task_Id;
                invoice_ClientDTO.Inv_Client_Uom_Id = model.Inv_Client_Uom_Id;
                invoice_ClientDTO.Inv_Client_Sub_Total = model.Inv_Client_Sub_Total;
                invoice_ClientDTO.Inv_Client_Client_Dis = model.Inv_Client_Client_Dis;
                invoice_ClientDTO.Inv_Client_Client_Total = model.Inv_Client_Client_Total;
                invoice_ClientDTO.Inv_Client_Short_Note = model.Inv_Client_Short_Note;
                invoice_ClientDTO.Inv_Client_Inv_Complete = model.Inv_Client_Inv_Complete;
                invoice_ClientDTO.Inv_Client_Credit_Memo = model.Inv_Client_Credit_Memo;
                invoice_ClientDTO.Inv_Client_Sent_Client = model.Inv_Client_Sent_Client;
                invoice_ClientDTO.Inv_Client_Comp_Date = model.Inv_Client_Comp_Date;
                invoice_ClientDTO.Inv_Client_Invoice_Number = model.Inv_Client_Invoice_Number;
                invoice_ClientDTO.Inv_Client_Inv_Date = model.Inv_Client_Inv_Date;
                invoice_ClientDTO.Inv_Client_Internal_Note = model.Inv_Client_Internal_Note;
                invoice_ClientDTO.Inv_Client_Status = model.Inv_Client_Status;
                invoice_ClientDTO.Inv_Client_Discout_Amount = model.Inv_Client_Discout_Amount;
                invoice_ClientDTO.Inv_Client_IsActive = model.Inv_Client_IsActive;
                invoice_ClientDTO.Inv_Client_IsDelete = model.Inv_Client_IsDelete;
                invoice_ClientDTO.Inv_Client_Followup = model.Inv_Client_Followup;
                invoice_ClientDTO.Inv_Client_Hold_Date = model.Inv_Client_Hold_Date;
                invoice_ClientDTO.Inv_Client_IsNoCharge = model.Inv_Client_IsNoCharge;
                invoice_ClientDTO.Inv_Client_NoChargeDate = model.Inv_Client_NoChargeDate;
                invoice_ClientDTO.UserID = model.UserID;
                if (model.Type == 4)
                {

                    Invoice_Client_ChildData invoice_Client_ChildData = new Invoice_Client_ChildData();
                    Invoice_Client_ChildDTO invoice_Client_ChildDTO = new Invoice_Client_ChildDTO();
                    for (int i = 0; i < model.Invoice_Client_ChildDTO.Count; i++)
                    {
                        invoice_Client_ChildDTO.Inv_Client_Ch_pkeyId = model.Invoice_Client_ChildDTO[i].Inv_Client_Ch_pkeyId;
                        invoice_Client_ChildDTO.UserID = model.UserID;
                        invoice_Client_ChildDTO.Type = 4;

                        var retunePkeyClient = invoice_Client_ChildData.AddInvoiceClientChildData(invoice_Client_ChildDTO);
                    }
                }
                else
                {

                    invoice_ClientDTO.Type = model.Inv_Client_pkeyId != 0 ? 2 : model.Type;

                    objData = invoice_ClientData.AddInvoiceClientData(invoice_ClientDTO);

                    objAddData.Add(objData);
                    if (objData.Count > 0 )
                    {
                        if (objData[0] != 0)
                        {
                            if (model.Invoice_Client_ChildDTO.Count > 0)
                            {
                                Invoice_Client_ChildData invoice_Client_ChildData = new Invoice_Client_ChildData();
                                Invoice_Client_ChildDTO invoice_Client_ChildDTO = new Invoice_Client_ChildDTO();

                                for (int i = 0; i < model.Invoice_Client_ChildDTO.Count; i++)
                                {
                                    invoice_Client_ChildDTO.Inv_Client_Ch_ClientID = model.Inv_Client_pkeyId != 0 ? model.Inv_Client_pkeyId : objData[0];
                                    invoice_Client_ChildDTO.Inv_Client_Ch_Wo_Id = model.Inv_Client_WO_Id;
                                    invoice_Client_ChildDTO.Inv_Client_Ch_pkeyId = model.Invoice_Client_ChildDTO[i].Inv_Client_Ch_pkeyId;
                                    invoice_Client_ChildDTO.Inv_Client_Ch_Task_Id = model.Invoice_Client_ChildDTO[i].Inv_Client_Ch_Task_Id;
                                    invoice_Client_ChildDTO.Inv_Client_Ch_Invoice_Id = model.Invoice_Client_ChildDTO[i].Inv_Client_Ch_Invoice_Id;
                                    invoice_Client_ChildDTO.Inv_Client_Ch_Uom_Id = model.Invoice_Client_ChildDTO[i].Inv_Client_Ch_Uom_Id;
                                    invoice_Client_ChildDTO.Inv_Client_Ch_Qty = model.Invoice_Client_ChildDTO[i].Inv_Client_Ch_Qty;
                                    invoice_Client_ChildDTO.Inv_Client_Ch_Price = model.Invoice_Client_ChildDTO[i].Inv_Client_Ch_Price;
                                    invoice_Client_ChildDTO.Inv_Client_Ch_Total = model.Invoice_Client_ChildDTO[i].Inv_Client_Ch_Total;
                                    invoice_Client_ChildDTO.Inv_Client_Ch_Adj_Price = model.Invoice_Client_ChildDTO[i].Inv_Client_Ch_Adj_Price;
                                    invoice_Client_ChildDTO.Inv_Client_Ch_Adj_Total = model.Invoice_Client_ChildDTO[i].Inv_Client_Ch_Adj_Total;
                                    invoice_Client_ChildDTO.Inv_Client_Ch_IsActive = model.Invoice_Client_ChildDTO[i].Inv_Client_Ch_IsActive;
                                    invoice_Client_ChildDTO.Inv_Client_Ch_IsDelete = model.Invoice_Client_ChildDTO[i].Inv_Client_Ch_IsDelete;
                                    invoice_Client_ChildDTO.Inv_Client_Ch_Auto_Invoice = model.Invoice_Client_ChildDTO[i].Inv_Client_Ch_Auto_Invoice;
                                    invoice_Client_ChildDTO.Inv_Client_Ch_Comment = model.Invoice_Client_ChildDTO[i].Inv_Client_Ch_Comment;
                                    invoice_Client_ChildDTO.Inv_Client_Ch_Flate_Fee = model.Invoice_Client_ChildDTO[i].Inv_Client_Ch_Flate_Fee;
                                    invoice_Client_ChildDTO.Inv_Client_Ch_Discount = model.Invoice_Client_ChildDTO[i].Inv_Client_Ch_Discount;
                                    invoice_Client_ChildDTO.Inv_Client_Ch_Other_Task_Name = model.Invoice_Client_ChildDTO[i].Inv_Client_Ch_Other_Task_Name;
                                    invoice_Client_ChildDTO.UserID = model.UserID;
                                    invoice_Client_ChildDTO.Type = model.Invoice_Client_ChildDTO[i].Inv_Client_Ch_pkeyId != 0 ? 2 : model.Type;

                                    var retunePkeyClient = invoice_Client_ChildData.AddInvoiceClientChildData(invoice_Client_ChildDTO);
                                }
                            }
                        }
                    }
                    
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objAddData;
        }


        //Violation Post
        public List<dynamic> ClientResultViolationPost(TaskViolationMasterRootObject model)
        {

            List<dynamic> objData = new List<dynamic>();
            //List<dynamic> objAddData = new List<dynamic>();

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {

                if (!string.IsNullOrEmpty(model.str_Task_Bid_WO_ID))
                {
                    var Data = JsonConvert.DeserializeObject<List<TaskActionWorkOrder>>(model.str_Task_Bid_WO_ID);
                    for (int j = 0; j < Data.Count; j++)
                    {
                        model.Task_Inv_WO_ID = Data[j].Task_WO_ID;
                        objData = AddUpdateClientResultViolationPost(model);
                    }
                }
                else
                {
                    objData = AddUpdateClientResultViolationPost(model);
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objData;


        }

        private List<dynamic> AddUpdateClientResultViolationPost(TaskViolationMasterRootObject model)
        {
            List<dynamic> objData = new List<dynamic>();
            //List<dynamic> objAddData = new List<dynamic>();



            TaskViolationMaster taskViolationMaster = new TaskViolationMaster();
            TaskViolationMasterData taskViolationMasterData = new TaskViolationMasterData();

            try
            {
                if (model.TaskViolationMaster.Count > 0)
                {

                    for (int i = 0; i < model.TaskViolationMaster.Count; i++)
                    {
                        taskViolationMaster.Task_Violation_WO_ID = model.Task_Inv_WO_ID;
                        taskViolationMaster.Task_Violation_pkeyID = model.TaskViolationMaster[i].Task_Violation_pkeyID;
                        taskViolationMaster.Task_Violation_Name = model.TaskViolationMaster[i].Task_Violation_Name;
                        taskViolationMaster.Task_Violation_Date = model.TaskViolationMaster[i].Task_Violation_Date;
                        taskViolationMaster.Task_Violation_Deadline = model.TaskViolationMaster[i].Task_Violation_Deadline;
                        taskViolationMaster.Task_Violation_Id = model.TaskViolationMaster[i].Task_Violation_Id;
                        taskViolationMaster.Task_Violation_Date_Discovered = model.TaskViolationMaster[i].Task_Violation_Date_Discovered;
                        taskViolationMaster.Task_Violation_Fine_Amount = model.TaskViolationMaster[i].Task_Violation_Fine_Amount;
                        taskViolationMaster.Task_Violation_Contact = model.TaskViolationMaster[i].Task_Violation_Contact;
                        taskViolationMaster.Task_Violation_Comment = model.TaskViolationMaster[i].Task_Violation_Comment;
                        //taskViolationMaster.Task_Violation_IsActive = model.TaskViolationMaster[i].Task_Violation_IsActive;
                        //taskViolationMaster.Task_Violation_IsDelete = model.TaskViolationMaster[i].Task_Violation_IsDelete;
                        taskViolationMaster.UserID = model.UserID;

                        if (model.Type == 4)
                        {
                            taskViolationMaster.Type = 4;
                        }
                        else
                        {

                            if (model.TaskViolationMaster[i].Task_Violation_pkeyID != 0)
                            {
                                taskViolationMaster.Type = 2;
                            }
                            else
                            {
                                taskViolationMaster.Type = 1;
                            }


                        }


                        objData = taskViolationMasterData.AddTaskViolationMasterData(taskViolationMaster);
                    }

                    //Add New Access Log 12-12-2022
                    Access_log_MasterData access_Log_MasterData = new Access_log_MasterData();
                    NewAccessLogMaster newAccessLogMaster = new NewAccessLogMaster();
                    newAccessLogMaster.Access_WorkerOrderID = model.Task_Inv_WO_ID;
                    newAccessLogMaster.Access_Master_ID = 39;
                    newAccessLogMaster.Access_UserID = model.UserID;
                    newAccessLogMaster.Type = 1;
                    access_Log_MasterData.AddNewAccessLogMaster(newAccessLogMaster);
                }

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                log.logErrorMessage("AddUpdateClientResultViolationPost-------------->" + model.Task_Inv_WO_ID);
            }



            return objData;
        }


        //Hazard Post
        public List<dynamic> ClientResultHazardPost(TaskHazardMasterRootObject model)
        {

            List<dynamic> objData = new List<dynamic>();
            //List<dynamic> objAddData = new List<dynamic>();

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {

                if (!string.IsNullOrEmpty(model.str_Task_Bid_WO_ID))
                {
                    var Data = JsonConvert.DeserializeObject<List<TaskActionWorkOrder>>(model.str_Task_Bid_WO_ID);
                    for (int j = 0; j < Data.Count; j++)
                    {
                        model.Task_Inv_WO_ID = Data[j].Task_WO_ID;
                        objData = AddUpdateClientResultHazardPost(model);
                    }
                }
                else
                {
                    objData = AddUpdateClientResultHazardPost(model);
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objData;


        }

        private List<dynamic> AddUpdateClientResultHazardPost(TaskHazardMasterRootObject model)
        {
            List<dynamic> objData = new List<dynamic>();
            //List<dynamic> objAddData = new List<dynamic>();



            TaskHazardMaster taskHazardMaster = new TaskHazardMaster();
            TaskHazardMasterData taskHazardMasterData = new TaskHazardMasterData();

            try
            {
                if (model.TaskHazardMaster.Count > 0)
                {

                    for (int i = 0; i < model.TaskHazardMaster.Count; i++)
                    {
                        taskHazardMaster.Task_Hazard_WO_ID = model.Task_Inv_WO_ID;
                        taskHazardMaster.Task_Hazard_pkeyID = model.TaskHazardMaster[i].Task_Hazard_pkeyID;
                        taskHazardMaster.Task_Hazard_Name = model.TaskHazardMaster[i].Task_Hazard_Name;
                        taskHazardMaster.Task_Hazard_Description = model.TaskHazardMaster[i].Task_Hazard_Description;
                        taskHazardMaster.Task_Hazard_Date_Discovered = model.TaskHazardMaster[i].Task_Hazard_Date_Discovered;
                        taskHazardMaster.Task_Hazard_Comment = model.TaskHazardMaster[i].Task_Hazard_Comment;
                        taskHazardMaster.UserID = model.UserID;

                        if (model.Type == 4)
                        {
                            taskHazardMaster.Type = 4;
                        }
                        else
                        {

                            if (model.TaskHazardMaster[i].Task_Hazard_pkeyID != 0)
                            {
                                taskHazardMaster.Type = 2;
                            }
                            else
                            {
                                taskHazardMaster.Type = 1;
                            }


                        }


                        objData = taskHazardMasterData.AddTaskHazardMasterData(taskHazardMaster);
                    }

                    //Access_log_MasterData access_Log_MasterData = new Access_log_MasterData();
                    //NewAccessLogMaster newAccessLogMaster = new NewAccessLogMaster();
                    //newAccessLogMaster.Access_WorkerOrderID = model.Task_Inv_WO_ID;
                    //newAccessLogMaster.Access_Master_ID = 49;
                    //newAccessLogMaster.Access_UserID = model.UserID;
                    //newAccessLogMaster.Type = 1;
                    //access_Log_MasterData.AddNewAccessLogMaster(newAccessLogMaster);
                }

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                log.logErrorMessage("AddUpdateClientResultHazardPost-------------->" + model.Task_Inv_WO_ID);
            }



            return objData;
        }

    }
}