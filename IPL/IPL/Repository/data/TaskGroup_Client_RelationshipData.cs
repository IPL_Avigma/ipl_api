﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class TaskGroup_Client_RelationshipData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        private List<dynamic> CreateUpdate_TaskGroupClientRelationship(TaskGroup_Client_RelationshipDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_TaskGroup_Client_Relationship]";
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@TGC_PkeyID", 1 + "#bigint#" + model.TGC_PkeyID);
                input_parameters.Add("@TGC_GroupID", 1 + "#bigint#" + model.TGC_GroupID);
                input_parameters.Add("@TGC_ClientID", 1 + "#bigint#" + model.TGC_ClientID);
                input_parameters.Add("@TGC_IsActive", 1 + "#bit#" + model.TGC_IsActive);
                input_parameters.Add("@TGC_IsDelete", 1 + "#bit#" + model.TGC_IsDelete);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@TGC_PkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);
                objData = obj.SqlCRUD(insertProcedure, input_parameters);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        private DataSet Get_TaskGroupClientRelationship(TaskGroup_Client_RelationshipDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_TaskGroup_Client_Relationship]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@TGC_PkeyID", 1 + "#bigint#" + model.TGC_PkeyID);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);

                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> CreateUpdate_TaskGroupClientRelationshipDetails(TaskGroup_Client_RelationshipDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                objData = CreateUpdate_TaskGroupClientRelationship(model);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }
            return objData;
        }

        public List<dynamic> Get_TaskGroupClientRelationshipDetails(TaskGroup_Client_RelationshipDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = Get_TaskGroupClientRelationship(model);

                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                }

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}