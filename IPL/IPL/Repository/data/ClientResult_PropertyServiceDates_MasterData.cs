﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class ClientResult_PropertyServiceDates_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        private DataSet GetClientResult_PropertyServiceDatesSettings(ClientResult_PropertyServiceDates_MasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_ClientResult_PropertyServiceDates_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@CRPSD_PkeyID", 1 + "#bigint#" + model.CRPSD_PkeyID);
                input_parameters.Add("@CRPSD_WO_ID", 1 + "#bigint#" + model.CRPSD_WO_ID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserID);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }
        public List<dynamic> GetClientResultPropertyServiceDatesData(ClientResult_PropertyServiceDates_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetClientResult_PropertyServiceDatesSettings(model);

                #region comment

                //        var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                //        List < ClientResult_PropertyServiceDates_MasterDTO > piData =
                //        (from item in myEnumerableFeaprd
                //                    select new ClientResult_PropertyServiceDates_MasterDTO
                //                    {
                //CRPSD_PkeyID = item.Field<Int64>("CRPSD_PkeyID"),
                //CRPSD_BoardingDate = item.Field<DateTime?>("CRPSD_BoardingDate"),
                //CRPSD_LastInspectedDate = item.Field<DateTime?>("CRPSD_LastInspectedDate"),
                //CRPSD_InspectionCycle = item.Field<DateTime?>("CRPSD_InspectionCycle"),
                //CRPSD_LastInteriorCleanDate = item.Field<DateTime?>("CRPSD_LastInteriorCleanDate"),
                //CRPSD_InitialInspectionComplete = item.Field<DateTime?>("CRPSD_InitialInspectionComplete"),
                //CRPSD_CleanOutComplete = item.Field<DateTime?>("CRPSD_CleanOutComplete"),
                //                    }).ToList();

                //        objDynamic.Add(piData);
                #endregion

                if (ds.Tables.Count > 0)
                {
                    for (int i = 0; i < ds.Tables.Count; i++)
                    {
                        objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                    }

                }

            }
            catch (Exception ex)            {

                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        public List<dynamic> CreateUpdate_ClientResult_PropertyServiceDates_Master(ClientResult_PropertyServiceDates_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objclntData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_ClientResult_PropertyServiceDates_Master]";
            ClientResult_PropertyServiceDates_Master clientResult_PropertyServiceDates_Master = new ClientResult_PropertyServiceDates_Master();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@CRPSD_IsActive", 1 + "#bit#" + model.CRPSD_IsActive);
                input_parameters.Add("@CRPSD_WO_ID", 1 + "#bigint#" + model.CRPSD_WO_ID);
                input_parameters.Add("@CRPSD_IsDelete", 1 + "#bit#" + model.CRPSD_IsDelete);
                input_parameters.Add("@CRPSD_PkeyID", 1 + "#bigint#" + model.CRPSD_PkeyID);

                input_parameters.Add("@CRPSD_BoardingDate", 1 + "#datetime#" + model.CRPSD_BoardingDate);
                input_parameters.Add("@CRPSD_LastInspectedDate", 1 + "#datetime#" + model.CRPSD_LastInspectedDate);
                input_parameters.Add("@CRPSD_InspectionCycle", 1 + "#datetime#" + model.CRPSD_InspectionCycle);
                input_parameters.Add("@CRPSD_LastInteriorCleanDate", 1 + "#datetime#" + model.CRPSD_LastInteriorCleanDate);
                input_parameters.Add("@CRPSD_InitialInspectionComplete", 1 + "#datetime#" + model.CRPSD_InitialInspectionComplete);
                input_parameters.Add("@CRPSD_CleanOutComplete", 1 + "#datetime#" + model.CRPSD_CleanOutComplete);


                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#bigint#" + model.Type);
                input_parameters.Add("@CRPSD_PkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    clientResult_PropertyServiceDates_Master.CRPSD_PkeyID = "0";
                    clientResult_PropertyServiceDates_Master.Status = "0";
                    clientResult_PropertyServiceDates_Master.ErrorMessage = "Error while Saviving in the Database";
                }
                else
                {
                    clientResult_PropertyServiceDates_Master.CRPSD_PkeyID = Convert.ToString(objData[0]);
                    clientResult_PropertyServiceDates_Master.Status = Convert.ToString(objData[1]);
                }
                objclntData.Add(clientResult_PropertyServiceDates_Master);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objclntData;
        }

        public List<dynamic> AddClientResultPropertyServiceDatesData(ClientResult_PropertyServiceDates_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                objData = CreateUpdate_ClientResult_PropertyServiceDates_Master(model);

            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }
    }
}