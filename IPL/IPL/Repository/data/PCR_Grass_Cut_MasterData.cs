﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class PCR_Grass_Cut_MasterData
    {

        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public List<dynamic> AddPCR_Grass_Cut_Masterdata(PCR_Grass_Cut_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objclntData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_PCR_Grass_Cut_Master]";
            PCR_Grass_Cut pCR_Grass_Cut = new PCR_Grass_Cut();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Grass_Cut_PkeyID", 1 + "#bigint#" + model.Grass_Cut_PkeyID);
                input_parameters.Add("@Grass_Cut_WO_ID", 1 + "#bigint#" + model.Grass_Cut_WO_ID);

                input_parameters.Add("@Grass_Cut_Completed", 1 + "#varchar#" + model.Grass_Cut_Completed);
                input_parameters.Add("@Grass_Cut_LotSize", 1 + "#varchar#" + model.Grass_Cut_LotSize);
                input_parameters.Add("@Grass_Cut_Unable_To_Cut", 1 + "#varchar#" + model.Grass_Cut_Unable_To_Cut);
                input_parameters.Add("@Grass_Cut_Other", 1 + "#varchar#" + model.Grass_Cut_Other);
                input_parameters.Add("@Grass_Cut_Completion_Date", 1 + "#datetime#" + model.Grass_Cut_Completion_Date);
                input_parameters.Add("@Grass_Cut_EarliestDate", 1 + "#datetime#" + model.Grass_Cut_EarliestDate);
                input_parameters.Add("@Grass_Cut_LatestDate", 1 + "#datetime#" + model.Grass_Cut_LatestDate);
                input_parameters.Add("@Grass_Cut_ForSale", 1 + "#varchar#" + model.Grass_Cut_ForSale);
                input_parameters.Add("@Grass_Cut_ForRent", 1 + "#varchar#" + model.Grass_Cut_ForRent);
                input_parameters.Add("@Grass_Cut_Realtor_Phone", 1 + "#varchar#" + model.Grass_Cut_Realtor_Phone);
                input_parameters.Add("@Grass_Cut_Realtor_Name", 1 + "#varchar#" + model.Grass_Cut_Realtor_Name);
                input_parameters.Add("@Grass_Cut_New_Damage_Found", 1 + "#varchar#" + model.Grass_Cut_New_Damage_Found);
                input_parameters.Add("@Grass_Cut_Damage_Fire", 1 + "#varchar#" + model.Grass_Cut_Damage_Fire);
                input_parameters.Add("@Grass_Cut_Damage_Neglect", 1 + "#varchar#" + model.Grass_Cut_Damage_Neglect);
                input_parameters.Add("@Grass_Cut_Damage_Vandal", 1 + "#varchar#" + model.Grass_Cut_Damage_Vandal);
                input_parameters.Add("@Grass_Cut_Damage_Freeze", 1 + "#varchar#" + model.Grass_Cut_Damage_Freeze);
                input_parameters.Add("@Grass_Cut_Damage_Storm", 1 + "#varchar#" + model.Grass_Cut_Damage_Storm);
                input_parameters.Add("@Grass_Cut_Damage_Flood", 1 + "#varchar#" + model.Grass_Cut_Damage_Flood);
                input_parameters.Add("@Grass_Cut_Roof_Leak", 1 + "#varchar#" + model.Grass_Cut_Roof_Leak);
                input_parameters.Add("@Grass_Cut_Explain_New_Damage", 1 + "#varchar#" + model.Grass_Cut_Explain_New_Damage);
                input_parameters.Add("@Grass_Cut_Occupancy", 1 + "#varchar#" + model.Grass_Cut_Occupancy);
                input_parameters.Add("@Grass_Cut_Property_Secure", 1 + "#varchar#" + model.Grass_Cut_Property_Secure);
                input_parameters.Add("@Grass_Cut_Pool_Present", 1 + "#varchar#" + model.Grass_Cut_Pool_Present);
                input_parameters.Add("@Grass_Cut_Pool_Secured", 1 + "#varchar#" + model.Grass_Cut_Pool_Secured);
                input_parameters.Add("@Grass_Cut_Violation_Posted", 1 + "#varchar#" + model.Grass_Cut_Violation_Posted);
                input_parameters.Add("@Grass_Cut_Opening_Not_Boarded", 1 + "#varchar#" + model.Grass_Cut_Opening_Not_Boarded);
                input_parameters.Add("@Grass_Cut_Opening_Boarded", 1 + "#varchar#" + model.Grass_Cut_Opening_Boarded);
                input_parameters.Add("@Grass_Cut_Debris_Present", 1 + "#varchar#" + model.Grass_Cut_Debris_Present);
                input_parameters.Add("@Grass_Cut_Trees_Touching_House", 1 + "#varchar#" + model.Grass_Cut_Trees_Touching_House);
                input_parameters.Add("@Grass_Cut_Vines_Touching_House", 1 + "#varchar#" + model.Grass_Cut_Vines_Touching_House);
                input_parameters.Add("@Grass_Cut_Explain_violations", 1 + "#varchar#" + model.Grass_Cut_Explain_violations);

                input_parameters.Add("@Grass_Cut_IsActive", 1 + "#bit#" + model.Grass_Cut_IsActive);
                input_parameters.Add("@Grass_Cut_IsDelete", 1 + "#bit#" + model.Grass_Cut_IsDelete);

                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@fwo_pkyeId", 1 + "#bigint#" + model.fwo_pkyeId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Grass_Cut_PkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    pCR_Grass_Cut.Grass_Cut_PkeyID = "0";
                    pCR_Grass_Cut.Status = "0";
                    pCR_Grass_Cut.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    pCR_Grass_Cut.Grass_Cut_PkeyID = Convert.ToString(objData[0]);
                    pCR_Grass_Cut.Status = Convert.ToString(objData[1]);


                }
                objclntData.Add(pCR_Grass_Cut);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objclntData;



        }

        private DataSet GetPCR_Grass_Cut_Master(PCR_Grass_Cut_MasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_PCR_Grass_Cut_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Grass_Cut_PkeyID", 1 + "#bigint#" + model.Grass_Cut_PkeyID);
                input_parameters.Add("@Grass_Cut_WO_ID", 1 + "#bigint#" + model.Grass_Cut_WO_ID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetPCR_Grass_Cut_MasterDetails(PCR_Grass_Cut_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetPCR_Grass_Cut_Master(model);

                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                }

                //var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                //List<PCR_Grass_Cut_MasterDTO> PCR_Grass_Cut =
                //   (from item in myEnumerableFeaprd
                //    select new PCR_Grass_Cut_MasterDTO
                //    {

                //        Grass_Cut_PkeyID = item.Field<Int64>("Grass_Cut_PkeyID"),
                //        Grass_Cut_WO_ID = item.Field<Int64>("Grass_Cut_WO_ID"),
                //        Grass_Cut_Completed = item.Field<String>("Grass_Cut_Completed"),
                //        Grass_Cut_LotSize = item.Field<String>("Grass_Cut_LotSize"),
                //        Grass_Cut_Unable_To_Cut = item.Field<String>("Grass_Cut_Unable_To_Cut"),
                //        Grass_Cut_Other = item.Field<String>("Grass_Cut_Other"),
                //        Grass_Cut_Completion_Date = item.Field<DateTime?>("Grass_Cut_Completion_Date"),
                //        Grass_Cut_EarliestDate = item.Field<DateTime?>("Grass_Cut_EarliestDate"),
                //        Grass_Cut_LatestDate = item.Field<DateTime?>("Grass_Cut_LatestDate"),
                //        Grass_Cut_ForSale = item.Field<String>("Grass_Cut_ForSale"),
                //        Grass_Cut_ForRent = item.Field<String>("Grass_Cut_ForRent"),
                //        Grass_Cut_Realtor_Phone = item.Field<String>("Grass_Cut_Realtor_Phone"),
                //        Grass_Cut_Realtor_Name = item.Field<String>("Grass_Cut_Realtor_Name"),
                //        Grass_Cut_New_Damage_Found = item.Field<String>("Grass_Cut_New_Damage_Found"),
                //        Grass_Cut_Damage_Fire = item.Field<String>("Grass_Cut_Damage_Fire"),
                //        Grass_Cut_Damage_Neglect = item.Field<String>("Grass_Cut_Damage_Neglect"),
                //        Grass_Cut_Damage_Vandal = item.Field<String>("Grass_Cut_Damage_Vandal"),
                //        Grass_Cut_Damage_Freeze = item.Field<String>("Grass_Cut_Damage_Freeze"),
                //        Grass_Cut_Damage_Storm = item.Field<String>("Grass_Cut_Damage_Storm"),
                //        Grass_Cut_Damage_Flood = item.Field<String>("Grass_Cut_Damage_Flood"),
                //        Grass_Cut_Roof_Leak = item.Field<String>("Grass_Cut_Roof_Leak"),
                //        Grass_Cut_Explain_New_Damage = item.Field<String>("Grass_Cut_Explain_New_Damage"),
                //        Grass_Cut_Occupancy = item.Field<String>("Grass_Cut_Occupancy"),
                //        Grass_Cut_Property_Secure = item.Field<String>("Grass_Cut_Property_Secure"),
                //        Grass_Cut_Pool_Present = item.Field<String>("Grass_Cut_Pool_Present"),
                //        Grass_Cut_Pool_Secured = item.Field<String>("Grass_Cut_Pool_Secured"),
                //        Grass_Cut_Violation_Posted = item.Field<String>("Grass_Cut_Violation_Posted"),
                //        Grass_Cut_Opening_Not_Boarded = item.Field<String>("Grass_Cut_Opening_Not_Boarded"),
                //        Grass_Cut_Opening_Boarded = item.Field<String>("Grass_Cut_Opening_Boarded"),
                //        Grass_Cut_Debris_Present = item.Field<String>("Grass_Cut_Debris_Present"),
                //        Grass_Cut_Trees_Touching_House = item.Field<String>("Grass_Cut_Trees_Touching_House"),
                //        Grass_Cut_Vines_Touching_House = item.Field<String>("Grass_Cut_Vines_Touching_House"),
                //        Grass_Cut_Explain_violations = item.Field<String>("Grass_Cut_Explain_violations"),
                //        Grass_Cut_IsActive = item.Field<Boolean?>("Grass_Cut_IsActive"),

                //    }).ToList();

                //objDynamic.Add(PCR_Grass_Cut);

                //if (ds.Tables.Count > 1)
                //{
                //    var historyDataset = ds.Tables[1].AsEnumerable();
                //    List<PCR_Grass_Cut_MasterDTO> PCR_Grass_Cut_history =
                //       (from item in historyDataset
                //        select new PCR_Grass_Cut_MasterDTO
                //        {

                //            Grass_Cut_PkeyID = item.Field<Int64>("Grass_Cut_PkeyID"),
                //            Grass_Cut_WO_ID = item.Field<Int64>("Grass_Cut_WO_ID"),
                //            Grass_Cut_Completed = item.Field<String>("Grass_Cut_Completed"),
                //            Grass_Cut_LotSize = item.Field<String>("Grass_Cut_LotSize"),
                //            Grass_Cut_Unable_To_Cut = item.Field<String>("Grass_Cut_Unable_To_Cut"),
                //            Grass_Cut_Other = item.Field<String>("Grass_Cut_Other"),
                //            Grass_Cut_Completion_Date = item.Field<DateTime?>("Grass_Cut_Completion_Date"),
                //            Grass_Cut_EarliestDate = item.Field<DateTime?>("Grass_Cut_EarliestDate"),
                //            Grass_Cut_LatestDate = item.Field<DateTime?>("Grass_Cut_LatestDate"),
                //            Grass_Cut_ForSale = item.Field<String>("Grass_Cut_ForSale"),
                //            Grass_Cut_ForRent = item.Field<String>("Grass_Cut_ForRent"),
                //            Grass_Cut_Realtor_Phone = item.Field<String>("Grass_Cut_Realtor_Phone"),
                //            Grass_Cut_Realtor_Name = item.Field<String>("Grass_Cut_Realtor_Name"),
                //            Grass_Cut_New_Damage_Found = item.Field<String>("Grass_Cut_New_Damage_Found"),
                //            Grass_Cut_Damage_Fire = item.Field<String>("Grass_Cut_Damage_Fire"),
                //            Grass_Cut_Damage_Neglect = item.Field<String>("Grass_Cut_Damage_Neglect"),
                //            Grass_Cut_Damage_Vandal = item.Field<String>("Grass_Cut_Damage_Vandal"),
                //            Grass_Cut_Damage_Freeze = item.Field<String>("Grass_Cut_Damage_Freeze"),
                //            Grass_Cut_Damage_Storm = item.Field<String>("Grass_Cut_Damage_Storm"),
                //            Grass_Cut_Damage_Flood = item.Field<String>("Grass_Cut_Damage_Flood"),
                //            Grass_Cut_Roof_Leak = item.Field<String>("Grass_Cut_Roof_Leak"),
                //            Grass_Cut_Explain_New_Damage = item.Field<String>("Grass_Cut_Explain_New_Damage"),
                //            Grass_Cut_Occupancy = item.Field<String>("Grass_Cut_Occupancy"),
                //            Grass_Cut_Property_Secure = item.Field<String>("Grass_Cut_Property_Secure"),
                //            Grass_Cut_Pool_Present = item.Field<String>("Grass_Cut_Pool_Present"),
                //            Grass_Cut_Pool_Secured = item.Field<String>("Grass_Cut_Pool_Secured"),
                //            Grass_Cut_Violation_Posted = item.Field<String>("Grass_Cut_Violation_Posted"),
                //            Grass_Cut_Opening_Not_Boarded = item.Field<String>("Grass_Cut_Opening_Not_Boarded"),
                //            Grass_Cut_Opening_Boarded = item.Field<String>("Grass_Cut_Opening_Boarded"),
                //            Grass_Cut_Debris_Present = item.Field<String>("Grass_Cut_Debris_Present"),
                //            Grass_Cut_Trees_Touching_House = item.Field<String>("Grass_Cut_Trees_Touching_House"),
                //            Grass_Cut_Vines_Touching_House = item.Field<String>("Grass_Cut_Vines_Touching_House"),
                //            Grass_Cut_Explain_violations = item.Field<String>("Grass_Cut_Explain_violations"),
                //            Grass_Cut_IsActive = item.Field<Boolean?>("Grass_Cut_IsActive"),

                //        }).ToList();

                //    objDynamic.Add(PCR_Grass_Cut_history);
                //}
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }

}