﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
  
    public class ImportWorkOrderData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        private List<dynamic> Delete_WorkOrderAPI_Data(ImportWorkOrderDTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[Delete_WorkOrderAPI_Data]";


            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@ImportFrom", 1 + "#bigint#" + model.ImportFrom);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Companyid", 1 + "#bigint#" + model.Companyid);
                input_parameters.Add("@WI_Pkey_ID", 1 + "#bigint#" + model.WI_Pkey_ID);
                input_parameters.Add("@Pkey_ID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;



        }



        public List<dynamic> DeleteWorkOrderAPIData(ImportWorkOrderDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                objData = Delete_WorkOrderAPI_Data(model);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }
        }
}