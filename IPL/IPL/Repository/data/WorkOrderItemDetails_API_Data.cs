﻿using Avigma.Repository.Lib;
using IPLApp.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class WorkOrderItemDetails_API_Data
    {
        WorkOrderItemDetails_API_DTO WorkOrderItemDetails_DTO = new WorkOrderItemDetails_API_DTO();
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public List<dynamic> WorkOrderItemDetails(WorkOrderItemDetails_API_DTO WorkOrderItemDetails_DTO)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                string insertProcedure = "[CreateUpdateWorkOrderItemMaster_API_Data]";
                Dictionary<string, string> inputParameter = new Dictionary<string, string>();
                inputParameter.Add("@WorkOrderItem_PkeyId", 1 + "#bigint#" + WorkOrderItemDetails_DTO.WorkOrderItem_PkeyId);
                inputParameter.Add("@Description", 1 + "#varchar#" + WorkOrderItemDetails_DTO.Description);
                inputParameter.Add("@Qty", 1 + "#varchar#" + WorkOrderItemDetails_DTO.Qty);
                inputParameter.Add("@Price", 1 + "#varchar#" + WorkOrderItemDetails_DTO.Price);
                inputParameter.Add("@Total", 1 + "#varchar#" + WorkOrderItemDetails_DTO.Total);
                inputParameter.Add("@Additional_Instructions", 1 + "#varchar#" + WorkOrderItemDetails_DTO.Additional_Instructions);
                inputParameter.Add("@WorkOrderMaster_API_Data_Pkey_Id", 1 + "#bigint#" + WorkOrderItemDetails_DTO.WorkOrderMaster_API_Data_Pkey_Id);
                inputParameter.Add("@Action", 1 + "#varchar#" + WorkOrderItemDetails_DTO.Action);
                inputParameter.Add("@UserID", 1 + "#int#" + WorkOrderItemDetails_DTO.UserID);
                inputParameter.Add("@Type", 1 + "#int#" + WorkOrderItemDetails_DTO.Type);
                inputParameter.Add("@WorkOrderItem_PkeyIdOut", 2 + "#bigint#" + null);
                inputParameter.Add("@ReturnValue", 2 + "#int#" + null);

                objDynamic = obj.SqlCRUD(insertProcedure, inputParameter);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objDynamic;

        }

        private DataSet GetWorkOrderItemData(WorkOrderItemDetails_API_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[GetWorkOrderItem_Master_API_Data]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@WorkOrderItem_PkeyId", 1 + "#bigint#" + model.WorkOrderItem_PkeyId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }
        public List<dynamic> GetWorkOrderItemDetails(WorkOrderItemDetails_API_DTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetWorkOrderItemData(model);
                if (ds.Tables.Count > 0)
                {
                    var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                    List<WorkOrderItemDetails_API_DTO> StudentDocumentData =
                       (from item in myEnumerableFeaprd
                        select new WorkOrderItemDetails_API_DTO
                        {
                            WorkOrderItem_PkeyId = item.Field<Int64>("WorkOrderItem_PkeyId"),
                            Description = item.Field<string>("Description"),
                            Qty = item.Field<string>("Qty"),
                            Price = item.Field<string>("Price"),
                            Total = item.Field<string>("Total"),
                            Additional_Instructions = item.Field<string>("Additional_Instructions"),
                            WorkOrderMaster_API_Data_Pkey_Id = item.Field<Int64>("WorkOrderMaster_API_Data_Pkey_Id"),
                            IsActive = item.Field<Boolean?>("IsActive"),




                        }).ToList();
                    objDynamic.Add(StudentDocumentData);
                }

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}