﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace IPL.Repository.data
{
    public class ImportFromMasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        SecurityHelper securityHelper = new SecurityHelper();
        Log log = new Log();


        private List<dynamic> AddUpdateImportFromMasterData(ImportFromMasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_ImportFromMaster]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Import_Form_PkeyId", 1 + "#bigint#" + model.Import_Form_PkeyId);
                input_parameters.Add("@Import_Form_Name", 1 + "#varchar#" + model.Import_Form_Name);
                input_parameters.Add("@Import_Form_IsActive", 1 + "#bit#" + model.Import_Form_IsActive);
                input_parameters.Add("@Import_Form_IsDelete", 1 + "#bit#" + model.Import_Form_IsDelete);
                input_parameters.Add("@Import_Form_URL_Name", 1 + "#varchar#" + model.Import_Form_URL_Name);
                input_parameters.Add("@Import_Form_PCRImportForm", 1 + "#bit#" + model.Import_Form_PCRImportForm);
                input_parameters.Add("@Import_Form_IsFixed", 1 + "#bit#" + model.Import_Form_IsFixed);
                input_parameters.Add("@IsImportFrom", 1 + "#bit#" + model.IsImportFrom);
                input_parameters.Add("@Import_BucketName", 1 + "#varchar#" + model.Import_BucketName);
                input_parameters.Add("@Import_BucketFolderName", 1 + "#varchar#" + model.Import_BucketFolderName);
                input_parameters.Add("@Import_DestBucketFolderName", 1 + "#varchar#" + model.Import_DestBucketFolderName);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Import_Form_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);
                objData = obj.SqlCRUD(insertProcedure, input_parameters);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;



        }
        public List<dynamic> AddImportFromMasterData(ImportFromMasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                objData = AddUpdateImportFromMasterData(model);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        private DataSet Get_ImportFromMaster(ImportFromMasterDTO model)
        {
            DataSet ds = null;
            try
            {

                string selectProcedure = "[Get_ImportFromMaster]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Import_Form_PkeyId", 1 + "#bigint#" + model.Import_Form_PkeyId);

                input_parameters.Add("@WhereClause", 1 + "#nvarchar#" + model.WhereClause);
                input_parameters.Add("@PageNumber", 1 + "#int#" + model.PageNumber);
                input_parameters.Add("@Rowcount", 1 + "#int#" + model.Rowcount);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);

                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ds;
        }

        public List<dynamic> Get_ImportFromMasterDetails(ImportFromMasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {

                string wherecondition = string.Empty;
                if (model.Type == 3 && !string.IsNullOrEmpty(model.FilterData))
                {
                    var Data = JsonConvert.DeserializeObject<ImportFromMaster_Filter>(model.FilterData);
                    if (!string.IsNullOrEmpty(Data.Import_Form_Name))
                    {
                        wherecondition = " And im.Import_Form_Name LIKE '%" + Data.Import_Form_Name + "%'";
                    }
                    if (!string.IsNullOrEmpty(Data.Import_BucketName))
                    {
                        wherecondition = " And im.Import_BucketName LIKE '%" + Data.Import_BucketName + "%'";
                    }

                    model.WhereClause = wherecondition;
                }

                DataSet ds = Get_ImportFromMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<ImportFromMasterDTO> importfrom =
                   (from item in myEnumerableFeaprd
                    select new ImportFromMasterDTO
                    {
                        Import_Form_PkeyId = item.Field<Int64>("Import_Form_PkeyId"),
                        Import_Form_Name = item.Field<String>("Import_Form_Name"),
                        Import_Form_URL_Name = item.Field<String>("Import_Form_URL_Name"),
                        Import_Form_PCRImportForm = item.Field<Boolean?>("Import_Form_PCRImportForm"),
                        Import_Form_IsFixed = item.Field<Boolean?>("Import_Form_IsFixed"),
                        IsImportFrom = item.Field<Boolean?>("IsImportFrom"),
                        Import_BucketName = item.Field<String>("Import_BucketName"),
                        Import_BucketFolderName = item.Field<String>("Import_BucketFolderName"),
                        Import_DestBucketFolderName = item.Field<String>("Import_DestBucketFolderName"),
                        Import_Form_IsActive = item.Field<Boolean?>("Import_Form_IsActive"),
                    }).ToList();

                objDynamic.Add(importfrom);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}