﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class PCR_MCS_Grass_Cut_Form_Master_Data
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        public List<dynamic> PostMCSGrassCutFormMaster(PCR_MCS_Grass_Cut_Form_Master_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_PCR_MCS_Grass_Cut_Form_Master]";
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@MG_PkeyID", 1 + "#bigint#" + model.MG_PkeyID);
                input_parameters.Add("@MG_WO_ID", 1 + "#bigint#" + model.MG_WO_ID);
                input_parameters.Add("@MG_Property_Info", 1 + "#nvarchar#" + model.MG_Property_Info);
                input_parameters.Add("@MG_Completion_Info", 1 + "#nvarchar#" + model.MG_Completion_Info);
                input_parameters.Add("@MG_Access_Issue", 1 + "#nvarchar#" + model.MG_Access_Issue);
                input_parameters.Add("@MG_Validation", 1 + "#nvarchar#" + model.MG_Validation);
                input_parameters.Add("@MG_Check_Ins", 1 + "#nvarchar#" + model.MG_Check_Ins);
                input_parameters.Add("@MG_Notes", 1 + "#nvarchar#" + model.MG_Notes);
                input_parameters.Add("@MG_Expected_Completion_Date", 1 + "#nvarchar#" + model.MG_Expected_Completion_Date);
                input_parameters.Add("@MG_IsActive", 1 + "#bit#" + model.MG_IsActive);
                input_parameters.Add("@MG_IsDelete", 1 + "#bit#" + model.MG_IsDelete);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@fwo_pkyeId", 1 + "#bigint#" + model.fwo_pkyeId);
                input_parameters.Add("@MG_PkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);
                objData = obj.SqlCRUD(insertProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        private DataSet GetMCSGrassCutFormMaster(PCR_MCS_Grass_Cut_Form_Master_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_PCR_MCS_Grass_Cut_Form_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@MG_PkeyID", 1 + "#bigint#" + model.MG_PkeyID);
                input_parameters.Add("@MG_WO_ID", 1 + "#bigint#" + model.MG_WO_ID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetMCSGrassCutFormMasterDetails(PCR_MCS_Grass_Cut_Form_Master_DTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetMCSGrassCutFormMaster(model);

                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                }

                //var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                //List<PCR_MCS_Grass_Cut_Form_Master_DTO> pcrdata =
                //   (from item in myEnumerableFeaprd
                //    select new PCR_MCS_Grass_Cut_Form_Master_DTO
                //    {
                //        MG_PkeyID = item.Field<Int64>("MG_PkeyID"),
                //        MG_WO_ID = item.Field<Int64>("MG_WO_ID"),
                //        MG_Company_ID = item.Field<Int64>("MG_Company_ID"),
                //        MG_Property_Info = item.Field<String>("MG_Property_Info"),
                //        MG_Completion_Info = item.Field<String>("MG_Completion_Info"),
                //        MG_Access_Issue = item.Field<String>("MG_Access_Issue"),
                //        MG_Validation = item.Field<String>("MG_Validation"),
                //        MG_Check_Ins = item.Field<String>("MG_Check_Ins"),
                //        MG_Notes = item.Field<String>("MG_Notes"),
                //        MG_Expected_Completion_Date = item.Field<String>("MG_Expected_Completion_Date"),
                //        MG_IsActive = item.Field<Boolean>("MG_IsActive"),
                //    }).ToList();

                //objDynamic.Add(pcrdata);

                //if (ds.Tables.Count > 0)
                //{
                //    var pcrHistory = ds.Tables[1].AsEnumerable();
                //    List<PCR_MCS_Grass_Cut_Form_Master_DTO> pcrdata_history =
                //       (from item in pcrHistory
                //        select new PCR_MCS_Grass_Cut_Form_Master_DTO
                //        {
                //            MG_PkeyID = item.Field<Int64>("MG_PkeyID"),
                //            MG_WO_ID = item.Field<Int64>("MG_WO_ID"),
                //            MG_Company_ID = item.Field<Int64>("MG_Company_ID"),
                //            MG_Property_Info = item.Field<String>("MG_Property_Info"),
                //            MG_Completion_Info = item.Field<String>("MG_Completion_Info"),
                //            MG_Access_Issue = item.Field<String>("MG_Access_Issue"),
                //            MG_Validation = item.Field<String>("MG_Validation"),
                //            MG_Check_Ins = item.Field<String>("MG_Check_Ins"),
                //            MG_Notes = item.Field<String>("MG_Notes"),
                //            MG_Expected_Completion_Date = item.Field<String>("MG_Expected_Completion_Date"),
                //            MG_IsActive = item.Field<Boolean>("MG_IsActive"),
                //        }).ToList();

                //    objDynamic.Add(pcrdata_history);
                //}
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}