﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class WT_Company_TBLData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public List<dynamic> AddWTCompanyData(WT_Company_TBLDTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_WT_Company_TBL]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@WC_pkeyID", 1 + "#bigint#" + model.WC_pkeyID);
                input_parameters.Add("@WC_Task_pkeyID", 1 + "#bigint#" + model.WC_Task_pkeyID);
                input_parameters.Add("@WC_Task_sett_ID", 1 + "#bigint#" + model.WC_Task_sett_ID);
                input_parameters.Add("@WC_IsActive", 1 + "#bit#" + model.WC_IsActive);
                input_parameters.Add("@WC_Isdelete", 1 + "#bit#" + model.WC_Isdelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@WC_WTTaskSett_Id", 1 + "#bigint#" + model.WC_WTTaskSett_Id);
                input_parameters.Add("@WC_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }
        public List<dynamic> AddWTCustomerData(WT_Customer_TBLDTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_WT_Customer_TBL]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@WCust_pkeyID", 1 + "#bigint#" + model.WCust_pkeyID);
                input_parameters.Add("@WCust_Task_pkeyID", 1 + "#bigint#" + model.WCust_Task_pkeyID);
                input_parameters.Add("@WCust_Task_sett_ID", 1 + "#bigint#" + model.WCust_Task_sett_ID);
                input_parameters.Add("@WCust_IsActive", 1 + "#bit#" + model.WCust_IsActive);
                input_parameters.Add("@WCust_Isdelete", 1 + "#bit#" + model.WCust_Isdelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@WCust_WTTaskSett_Id", 1 + "#bigint#" + model.WCust_WTTaskSett_Id);
                input_parameters.Add("@WCust_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }
        public List<dynamic> AddWTLoanData(WT_Loan_TBLDTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_WT_Loan_TBL]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@WL_pkeyID", 1 + "#bigint#" + model.WL_pkeyID);
                input_parameters.Add("@WL_Task_pkeyID", 1 + "#bigint#" + model.WL_Task_pkeyID);
                input_parameters.Add("@WL_Task_sett_ID", 1 + "#bigint#" + model.WL_Task_sett_ID);
                input_parameters.Add("@WL_IsActive", 1 + "#bit#" + model.WL_IsActive);
                input_parameters.Add("@WL_Isdelete", 1 + "#bit#" + model.WL_Isdelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@WL_WTTaskSett_Id", 1 + "#bigint#" + model.WL_WTTaskSett_Id);
                input_parameters.Add("@WL_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }
        public List<dynamic> AddWTStateData(WT_State_TBLDTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_WT_State_TBL]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@WS_pkeyID", 1 + "#bigint#" + model.WS_pkeyID);
                input_parameters.Add("@WS_Task_pkeyID", 1 + "#bigint#" + model.WS_Task_pkeyID);
                input_parameters.Add("@WS_Task_sett_ID", 1 + "#bigint#" + model.WS_Task_sett_ID);
                input_parameters.Add("@WS_IsActive", 1 + "#bit#" + model.WS_IsActive);
                input_parameters.Add("@WS_Isdelete", 1 + "#bit#" + model.WS_Isdelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@WS_WTTaskSett_Id", 1 + "#bigint#" + model.WS_WTTaskSett_Id);
                input_parameters.Add("@WS_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }
        public List<dynamic> AddWTWorkTypeData(WT_WorkType_TBLDTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_WT_WorkType_TBL]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Wwork_pkeyID", 1 + "#bigint#" + model.Wwork_pkeyID);
                input_parameters.Add("@Wwork_Task_pkeyID", 1 + "#bigint#" + model.Wwork_Task_pkeyID);
                input_parameters.Add("@Wwork_Task_sett_ID", 1 + "#bigint#" + model.Wwork_Task_sett_ID);
                input_parameters.Add("@Wwork_IsActive", 1 + "#bit#" + model.Wwork_IsActive);
                input_parameters.Add("@Wwork_Isdelete", 1 + "#bit#" + model.Wwork_Isdelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Wwork_WTTaskSett_Id", 1 + "#bigint#" + model.Wwork_WTTaskSett_Id);
                input_parameters.Add("@Wwork_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        public List<dynamic> AddWTWorkTypeGroupData(WT_WorkType_Group_TBLDTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_WT_WorkType_Group_TBL]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Wwork_Group_pkeyID", 1 + "#bigint#" + model.Wwork_Group_pkeyID);
                input_parameters.Add("@Wwork_Group_Task_pkeyID", 1 + "#bigint#" + model.Wwork_Group_Task_pkeyID);
                input_parameters.Add("@Wwork_Group_Task_sett_ID", 1 + "#bigint#" + model.Wwork_Group_Task_sett_ID);
                input_parameters.Add("@Wwork_Group_IsActive", 1 + "#bit#" + model.Wwork_Group_IsActive);
                input_parameters.Add("@Wwork_Group_Isdelete", 1 + "#bit#" + model.Wwork_Group_Isdelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Wwork_Group_WTTaskSett_Id", 1 + "#bigint#" + model.Wwork_Group_WTTaskSett_Id);
                input_parameters.Add("@Wwork_Group_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }
    }
}