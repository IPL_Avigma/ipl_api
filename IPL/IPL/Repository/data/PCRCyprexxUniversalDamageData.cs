﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class PCRCyprexxUniversalDamageData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        public List<dynamic> AddPCRCyprexxUniversalDamageData(PCRCyprexxUniversalDamage_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objclntData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_PCR_Cyprexx_Universal_Damage]";
            //PCRCyprexxUniversalDamage_DTO pCR_CYPREXX_FORMS = new PCRCyprexxUniversalDamage_DTO();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@PCR_CU_PkeyID", 1 + "#bigint#" + model.PCR_CU_PkeyID);
                input_parameters.Add("@PCR_CU_General_Info", 1 + "#nvarchar#" + model.PCR_CU_General_Info);
                input_parameters.Add("@PCR_CU_Interior_Access_Information", 1 + "#nvarchar#" + model.PCR_CU_Interior_Access_Information);
                input_parameters.Add("@PCR_CU_Upload_Photos", 1 + "#nvarchar#" + model.PCR_CU_Upload_Photos);

                input_parameters.Add("@PCR_CU_IsActive", 1 + "#bit#" + model.PCR_CU_IsActive);
                input_parameters.Add("@PCR_CU_IsDelete", 1 + "#bit#" + model.PCR_CU_IsDelete);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@PCR_CU_Pkey_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        private DataSet GetPCRCyprexxUniversalDamage_Master(PCRCyprexxUniversalDamage_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_PCR_Cyprexx_Universal_Damage]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@PCR_CU_PkeyID", 1 + "#bigint#" + model.PCR_CU_PkeyID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                //input_parameters.Add("@ReturnValue", 2 + "#int#" + null);
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetPCRCyprexxUniversalDamageMasterDetails(PCRCyprexxUniversalDamage_DTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetPCRCyprexxUniversalDamage_Master(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<PCRCyprexxUniversalDamage_DTO> pCRCyprexxUniversalDamage_DTOs =
                   (from item in myEnumerableFeaprd
                    select new PCRCyprexxUniversalDamage_DTO
                    {
                        PCR_CU_PkeyID = item.Field<Int64>("PCR_CU_PkeyID"),

                        PCR_CU_General_Info = item.Field<String>("PCR_CU_General_Info"),
                        PCR_CU_Interior_Access_Information = item.Field<String>("PCR_CU_Interior_Access_Information"),
                        PCR_CU_Upload_Photos = item.Field<String>("PCR_CU_Upload_Photos"),
                       
                        PCR_CU_IsActive = item.Field<Boolean>("PCR_CU_IsActive"),
                        PCR_CU_IsDelete = item.Field<Boolean>("PCR_CU_IsDelete"),
                    }).ToList();

                objDynamic.Add(pCRCyprexxUniversalDamage_DTOs);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}