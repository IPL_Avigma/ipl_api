﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class ClientResult_PropertyLoanSettings_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        private DataSet GetClientResult_PropertyLoanSettings(ClientResult_PropertyLoanSettings_MasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_ClientResult_PropertyLoanSettings_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@CRPLS_PkeyID", 1 + "#bigint#" + model.CRPLS_PkeyID);
                input_parameters.Add("@CRPLS_WO_ID", 1 + "#bigint#" + model.CRPLS_WO_ID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserID);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }
        public List<dynamic> GetClientResultPropertyLoanSettingsData(ClientResult_PropertyLoanSettings_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetClientResult_PropertyLoanSettings(model);

                #region comment
                //        var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                //        List < ClientResult_PropertyLoanSettings_MasterDTO > piData =
                //        (from item in myEnumerableFeaprd
                //                    select new ClientResult_PropertyLoanSettings_MasterDTO
                //                    {
                //            CRPLS_PkeyID = item.Field<Int64>("CRPLS_PkeyID"),
                //CRPLS_BorrowerEmail = item.Field<string>("CRPLS_BorrowerEmail"),
                //CRPLS_BorrowerName = item.Field<string>("CRPLS_BorrowerName"),
                //CRPLS_BorrowerPhone = item.Field<string>("CRPLS_BorrowerPhone"),
                //CRPLS_LoanNumber = item.Field<string>("CRPLS_LoanNumber"),
                //CRPLS_LoanStatus = item.Field<Int64?>("CRPLS_LoanStatus"),
                //CRPLS_LoanType = item.Field<Int64?>("CRPLS_LoanType"),
                //CRPLS_PropertyMortgagee = item.Field<string>("CRPLS_PropertyMortgagee"),
                //CRPLS_UnpaidPrincipalBalance = item.Field<string>("CRPLS_UnpaidPrincipalBalance"),
                //                    }).ToList();

                //        objDynamic.Add(piData);
                #endregion

                {
                    for (int i = 0; i < ds.Tables.Count; i++)
                    {
                        objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                    }

                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        public List<dynamic> CreateUpdate_ClientResult_PropertyLoanSettings_Master(ClientResult_PropertyLoanSettings_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objclntData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_ClientResult_PropertyLoanSettings_Master]";
            ClientResult_PropertyLoanSettings_Master clientResult_PropertyLoanSettings_Master = new ClientResult_PropertyLoanSettings_Master();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@CRPLS_IsActive", 1 + "#bit#" + model.CRPLS_IsActive);
                input_parameters.Add("@CRPLS_WO_ID", 1 + "#bigint#" + model.CRPLS_WO_ID);
                input_parameters.Add("@CRPLS_IsDelete", 1 + "#bit#" + model.CRPLS_IsDelete);
                input_parameters.Add("@CRPLS_PkeyID", 1 + "#bigint#" + model.CRPLS_PkeyID);

                input_parameters.Add("@CRPLS_BorrowerEmail", 1 + "#nvarchar#" + model.CRPLS_BorrowerEmail);
                input_parameters.Add("@CRPLS_BorrowerName", 1 + "#nvarchar#" + model.CRPLS_BorrowerName);
                input_parameters.Add("@CRPLS_BorrowerPhone", 1 + "#nvarchar#" + model.CRPLS_BorrowerPhone);
                input_parameters.Add("@CRPLS_UnpaidPrincipalBalance", 1 + "#nvarchar#" + model.CRPLS_UnpaidPrincipalBalance);
                input_parameters.Add("@CRPLS_LoanNumber", 1 + "#nvarchar#" + model.CRPLS_LoanNumber);
                input_parameters.Add("@CRPLS_LoanStatus", 1 + "#nvarchar#" + model.CRPLS_LoanStatus);
                input_parameters.Add("@CRPLS_PropertyMortgagee", 1 + "#nvarchar#" + model.CRPLS_PropertyMortgagee);
                input_parameters.Add("@CRPLS_LoanType", 1 + "#nvarchar#" + model.CRPLS_LoanType);

                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#bigint#" + model.Type);
                input_parameters.Add("@CRPLS_PkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    clientResult_PropertyLoanSettings_Master.CRPLS_PkeyID = "0";
                    clientResult_PropertyLoanSettings_Master.Status = "0";
                    clientResult_PropertyLoanSettings_Master.ErrorMessage = "Error while Saviving in the Database";
                }
                else
                {
                    clientResult_PropertyLoanSettings_Master.CRPLS_PkeyID = Convert.ToString(objData[0]);
                    clientResult_PropertyLoanSettings_Master.Status = Convert.ToString(objData[1]);
                }
                objclntData.Add(clientResult_PropertyLoanSettings_Master);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objclntData;
        }

        public List<dynamic> AddClientResult_PropertyLoanSettingsData(ClientResult_PropertyLoanSettings_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                objData = CreateUpdate_ClientResult_PropertyLoanSettings_Master(model);

            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }
    }
}