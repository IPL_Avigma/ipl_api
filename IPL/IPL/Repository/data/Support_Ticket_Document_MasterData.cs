﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class Support_Ticket_Document_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        private List<dynamic> AddUpdateSupportTicket_DocumentData(Support_Ticket_Document_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcustData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_Support_Ticket_Document_Master]";
            Support_Ticket_Setting_Document support_Ticket_Setting_Document = new Support_Ticket_Setting_Document();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Support_Docs_PkeyID", 1 + "#bigint#" + model.Support_Docs_PkeyID);
                input_parameters.Add("@Support_Docs_Ticket_ID", 1 + "#bigint#" + model.Support_Docs_Ticket_ID);
                input_parameters.Add("@Support_Docs_File_Path", 1 + "#varchar#" + model.Support_Docs_File_Path);
                input_parameters.Add("@Support_Docs_File_Size", 1 + "#varchar#" + model.Support_Docs_File_Size);
                input_parameters.Add("@Support_Docs_File_Name", 1 + "#varchar#" + model.Support_Docs_File_Name);
                input_parameters.Add("@Support_Docs_Bucket_Name", 1 + "#varchar#" + model.Support_Docs_Bucket_Name);
                input_parameters.Add("@Support_Docs_Project_Id", 1 + "#varchar#" + model.Support_Docs_Project_Id);
                input_parameters.Add("@Support_Docs_Object_Name", 1 + "#varchar#" + model.Support_Docs_Object_Name);
                input_parameters.Add("@Support_Docs_Folder_Name", 1 + "#varchar#" + model.Support_Docs_Folder_Name);
                input_parameters.Add("@Support_Docs_UploadedBy", 1 + "#varchar#" + model.Support_Docs_UploadedBy);
                input_parameters.Add("@Support_Docs_IsActive", 1 + "#bit#" + model.Support_Docs_IsActive);
                input_parameters.Add("@Support_Docs_IsDelete", 1 + "#bit#" + model.Support_Docs_IsDelete);
                input_parameters.Add("@Support_Docs_Company_ID", 1 + "#bigint#" + model.Support_Docs_Company_ID);
                input_parameters.Add("@Support_Docs_User_ID", 1 + "#bigint#" + model.Support_Docs_User_ID);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Support_Docs_PkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    support_Ticket_Setting_Document.Support_Docs_PkeyID = "0";
                    support_Ticket_Setting_Document.Status = "0";
                    support_Ticket_Setting_Document.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    support_Ticket_Setting_Document.Support_Docs_PkeyID = Convert.ToString(objData[0]);
                    support_Ticket_Setting_Document.Status = Convert.ToString(objData[1]);


                }
                objcustData.Add(support_Ticket_Setting_Document);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                log.logDebugMessage(ex.StackTrace);
                log.logDebugMessage(ex.Message);
            }
            return objcustData;



        }
        public List<dynamic> AddupdateSupportTicket_DocumentDetails(Support_Ticket_Document_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                objData = AddUpdateSupportTicket_DocumentData(model);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        private DataSet Get_SupportTicket_Document_Master(Support_Ticket_Document_MasterDTO model)
        {
            DataSet ds = null;
            try

            {

                string selectProcedure = "[Get_Support_Ticket_Document_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Support_Docs_PkeyID", 1 + "#bigint#" + model.Support_Docs_PkeyID);
                input_parameters.Add("@Support_Docs_Company_ID", 1 + "#bigint#" + model.Support_Docs_Company_ID);
                input_parameters.Add("@Support_Docs_User_ID", 1 + "#bigint#" + model.Support_Docs_User_ID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }
        public List<dynamic> GetSupportTicket_DocumentDetails(Support_Ticket_Document_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = Get_SupportTicket_Document_Master(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Support_Ticket_Document_MasterDTO> SupportTicketdocs_Master =
                   (from item in myEnumerableFeaprd
                    select new Support_Ticket_Document_MasterDTO
                    {
                        Support_Docs_PkeyID = item.Field<Int64>("Support_Docs_PkeyID"),
                        Support_Docs_Ticket_ID = item.Field<Int64>("Support_Docs_Ticket_ID"),
                        Support_Docs_File_Path = item.Field<String>("Support_Docs_File_Path"),
                        Support_Docs_File_Size = item.Field<String>("Support_Docs_File_Size"),
                        Support_Docs_File_Name = item.Field<String>("Support_Docs_File_Name"),
                        Support_Docs_Project_Id = item.Field<String>("Support_Docs_Project_Id"),
                        Support_Docs_Object_Name = item.Field<String>("Support_Docs_Object_Name"),
                        Support_Docs_Folder_Name = item.Field<String>("Support_Docs_Folder_Name"),
                        Support_Docs_UploadedBy = item.Field<String>("Support_Docs_UploadedBy"),
                        Support_Docs_IsActive = item.Field<Boolean?>("Support_Docs_IsActive"),

                    }).ToList();

                objDynamic.Add(SupportTicketdocs_Master);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

    }
}