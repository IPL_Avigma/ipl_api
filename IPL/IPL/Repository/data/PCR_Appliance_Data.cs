﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class PCR_Appliance_Data
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public List<dynamic> AddPCR_Appliance_Data(PCR_Appliance_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objclntData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_PCR_Appliance_Master]";
            PCRApplianceDetails pCRApplianceDetails = new PCRApplianceDetails();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@PCR_Appliance_pkeyId", 1 + "#bigint#" + model.PCR_Appliance_pkeyId);
                input_parameters.Add("@PCR_Appliance_MasterId", 1 + "#bigint#" + model.PCR_Appliance_MasterId);
                input_parameters.Add("@PCR_Appliance_WO_Id", 1 + "#bigint#" + model.PCR_Appliance_WO_Id);
                input_parameters.Add("@PCR_Appliance_ValType", 1 + "#int#" + model.PCR_Appliance_ValType);
                input_parameters.Add("@PCR_Appliance_Refrigerator", 1 + "#varchar#" + model.PCR_Appliance_Refrigerator);
                input_parameters.Add("@PCR_Appliance_Stove", 1 + "#varchar#" + model.PCR_Appliance_Stove);
                input_parameters.Add("@PCR_Appliance_Stove_Wall_Oven", 1 + "#varchar#" + model.PCR_Appliance_Stove_Wall_Oven);
                input_parameters.Add("@PCR_Appliance_Dishwasher", 1 + "#varchar#" + model.PCR_Appliance_Dishwasher);
                input_parameters.Add("@PCR_Appliance_Build_In_Microwave", 1 + "#varchar#" + model.PCR_Appliance_Build_In_Microwave);
                input_parameters.Add("@PCR_Appliance_Dryer", 1 + "#varchar#" + model.PCR_Appliance_Dryer);
                input_parameters.Add("@PCR_Appliance_Washer", 1 + "#varchar#" + model.PCR_Appliance_Washer);
                input_parameters.Add("@PCR_Appliance_Air_Conditioner", 1 + "#varchar#" + model.PCR_Appliance_Air_Conditioner);
                input_parameters.Add("@PCR_Appliance_Hot_Water_Heater", 1 + "#varchar#" + model.PCR_Appliance_Hot_Water_Heater);
                input_parameters.Add("@PCR_Appliance_Dehumidifier", 1 + "#varchar#" + model.PCR_Appliance_Dehumidifier);
                input_parameters.Add("@PCR_Appliance_Furnace", 1 + "#varchar#" + model.PCR_Appliance_Furnace);
                input_parameters.Add("@PCR_Appliance_Water_Softener", 1 + "#varchar#" + model.PCR_Appliance_Water_Softener);
                input_parameters.Add("@PCR_Appliance_Boiler", 1 + "#varchar#" + model.PCR_Appliance_Boiler);


                input_parameters.Add("@PCR_Appliance_IsActive", 1 + "#bit#" + model.PCR_Appliance_IsActive);
                input_parameters.Add("@PCR_Appliance_IsDelete", 1 + "#bit#" + model.PCR_Appliance_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@fwo_pkyeId", 1 + "#bigint#" + model.fwo_pkyeId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@PCR_Appliance_pkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    pCRApplianceDetails.PCR_Appliance_pkeyId = "0";
                    pCRApplianceDetails.Status = "0";
                    pCRApplianceDetails.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    pCRApplianceDetails.PCR_Appliance_pkeyId = Convert.ToString(objData[0]);
                    pCRApplianceDetails.Status = Convert.ToString(objData[1]);


                }
                objclntData.Add(pCRApplianceDetails);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objclntData;



        }

        private DataSet GetPCRApplianceMaster(PCR_Appliance_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_PCR_Appliance_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@PCR_Appliance_pkeyId", 1 + "#bigint#" + model.PCR_Appliance_pkeyId);
                input_parameters.Add("@PCR_Appliance_WO_Id", 1 + "#bigint#" + model.PCR_Appliance_WO_Id);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetPCRApplianceDetails(PCR_Appliance_DTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetPCRApplianceMaster(model);
                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                }


                //var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                //List<PCR_Appliance_DTO> PCRAppiance =
                //   (from item in myEnumerableFeaprd
                //    select new PCR_Appliance_DTO
                //    {
                //        PCR_Appliance_pkeyId = item.Field<Int64>("PCR_Appliance_pkeyId"),
                //        PCR_Appliance_MasterId = item.Field<Int64>("PCR_Appliance_MasterId"),
                //        PCR_Appliance_WO_Id = item.Field<Int64>("PCR_Appliance_WO_Id"),
                //        PCR_Appliance_ValType = item.Field<int?>("PCR_Appliance_ValType"),

                //        PCR_Appliance_Refrigerator = item.Field<string>("PCR_Appliance_Refrigerator"),
                //        PCR_Appliance_Stove = item.Field<string>("PCR_Appliance_Stove"),
                //        PCR_Appliance_Stove_Wall_Oven = item.Field<string>("PCR_Appliance_Stove_Wall_Oven"),
                //        PCR_Appliance_Dishwasher = item.Field<string>("PCR_Appliance_Dishwasher"),
                //        PCR_Appliance_Build_In_Microwave = item.Field<string>("PCR_Appliance_Build_In_Microwave"),
                //        PCR_Appliance_Dryer = item.Field<string>("PCR_Appliance_Dryer"),
                //        PCR_Appliance_Washer = item.Field<string>("PCR_Appliance_Washer"),
                //        PCR_Appliance_Air_Conditioner = item.Field<string>("PCR_Appliance_Air_Conditioner"),
                //        PCR_Appliance_Hot_Water_Heater = item.Field<string>("PCR_Appliance_Hot_Water_Heater"),
                //        PCR_Appliance_Dehumidifier = item.Field<string>("PCR_Appliance_Dehumidifier"),
                //        PCR_Appliance_Furnace = item.Field<string>("PCR_Appliance_Furnace"),
                //        PCR_Appliance_Water_Softener = item.Field<string>("PCR_Appliance_Water_Softener"),
                //        PCR_Appliance_Boiler = item.Field<string>("PCR_Appliance_Boiler"),


                //        PCR_Appliance_IsActive = item.Field<Boolean?>("PCR_Appliance_IsActive"),

                //    }).ToList();

                //objDynamic.Add(PCRAppiance);

                //if (ds.Tables.Count > 1)
                //{
                //    var pcrHistory = ds.Tables[1].AsEnumerable();
                //    List<PCR_Appliance_DTO> AppianceHistory =
                //       (from item in pcrHistory
                //        select new PCR_Appliance_DTO
                //        {
                //            PCR_Appliance_pkeyId = item.Field<Int64>("PCR_Appliance_pkeyId"),
                //            PCR_Appliance_MasterId = item.Field<Int64>("PCR_Appliance_MasterId"),
                //            PCR_Appliance_WO_Id = item.Field<Int64>("PCR_Appliance_WO_Id"),
                //            PCR_Appliance_ValType = item.Field<int?>("PCR_Appliance_ValType"),

                //            PCR_Appliance_Refrigerator = item.Field<string>("PCR_Appliance_Refrigerator"),
                //            PCR_Appliance_Stove = item.Field<string>("PCR_Appliance_Stove"),
                //            PCR_Appliance_Stove_Wall_Oven = item.Field<string>("PCR_Appliance_Stove_Wall_Oven"),
                //            PCR_Appliance_Dishwasher = item.Field<string>("PCR_Appliance_Dishwasher"),
                //            PCR_Appliance_Build_In_Microwave = item.Field<string>("PCR_Appliance_Build_In_Microwave"),
                //            PCR_Appliance_Dryer = item.Field<string>("PCR_Appliance_Dryer"),
                //            PCR_Appliance_Washer = item.Field<string>("PCR_Appliance_Washer"),
                //            PCR_Appliance_Air_Conditioner = item.Field<string>("PCR_Appliance_Air_Conditioner"),
                //            PCR_Appliance_Hot_Water_Heater = item.Field<string>("PCR_Appliance_Hot_Water_Heater"),
                //            PCR_Appliance_Dehumidifier = item.Field<string>("PCR_Appliance_Dehumidifier"),
                //            PCR_Appliance_Furnace = item.Field<string>("PCR_Appliance_Furnace"),
                //            PCR_Appliance_Water_Softener = item.Field<string>("PCR_Appliance_Water_Softener"),
                //            PCR_Appliance_Boiler = item.Field<string>("PCR_Appliance_Boiler"),


                //            PCR_Appliance_IsActive = item.Field<Boolean?>("PCR_Appliance_IsActive"),

                //        }).ToList();

                //    objDynamic.Add(AppianceHistory);
                //}
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}