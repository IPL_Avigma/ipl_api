﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class IPL_Custom_DropDown_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        private DataSet GetCustomDropDownMaster(CustomDRDMaster model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Ipl_Custom_DropDown_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@CompanyID", 1 + "#bigint#" + model.CompanyID);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetCustomDRDDetails(CustomDRDMaster model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                CustomDRDMaster customDRDMaster = new CustomDRDMaster();
              

                DataSet ds = GetCustomDropDownMaster(model);
                customDRDMaster.IPL_Custom_DropDown_MasterDTO = GetFirstCustomDRD(ds.Tables[0]);
                customDRDMaster.IPL_Custom_DropDown_SecondDTO = GetSecondCustomDRD(ds.Tables[1]);
                customDRDMaster.IPL_Custom_DropDown_ThirdDTO = GetThirdCustomDRD(ds.Tables[2]);
                customDRDMaster.IPL_Custom_DropDown_FourthDTO = GetFourthCustomDRD(ds.Tables[3]);

                objDynamic.Add(customDRDMaster);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }


        private List<IPL_Custom_DropDown_MasterDTO> GetFirstCustomDRD(DataTable dataTable)
        {
            List<IPL_Custom_DropDown_MasterDTO> FirstMaster_Data = new List<IPL_Custom_DropDown_MasterDTO>();
            try
            {
                var myEnumerableFeaprd = dataTable.AsEnumerable();
                FirstMaster_Data =

                    (from item in myEnumerableFeaprd
                     select new IPL_Custom_DropDown_MasterDTO
                     {
                         Ipl_PkeyId = item.Field<Int64>("Ipl_PkeyId"),
                         Ipl_Name = item.Field<String>("Ipl_Name"),

                     }).ToList();

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return FirstMaster_Data;
        }
        private List<IPL_Custom_DropDown_SecondDTO> GetSecondCustomDRD(DataTable dataTable)
        {
            List<IPL_Custom_DropDown_SecondDTO> SecondMaster_Data = new List<IPL_Custom_DropDown_SecondDTO>();
            try
            {
                var myEnumerableFeaprd = dataTable.AsEnumerable();
                SecondMaster_Data =

                    (from item in myEnumerableFeaprd
                     select new IPL_Custom_DropDown_SecondDTO
                     {
                         Ipl_Sec_PkeyId = item.Field<Int64>("Ipl_Sec_PkeyId"),
                         Ipl_Sec_Name = item.Field<String>("Ipl_Sec_Name"),

                     }).ToList();

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return SecondMaster_Data;
        }
        private List<IPL_Custom_DropDown_ThirdDTO> GetThirdCustomDRD(DataTable dataTable)
        {
            List<IPL_Custom_DropDown_ThirdDTO> ThirdMaster_Data = new List<IPL_Custom_DropDown_ThirdDTO>();
            try
            {
                var myEnumerableFeaprd = dataTable.AsEnumerable();
                 ThirdMaster_Data =

                    (from item in myEnumerableFeaprd
                     select new IPL_Custom_DropDown_ThirdDTO
                     {
                         Ipl_Third_PkeyId = item.Field<Int64>("Ipl_Third_PkeyId"),
                         Ipl_Third_Name = item.Field<String>("Ipl_Third_Name"),

                     }).ToList();

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ThirdMaster_Data;


        }
        private List<IPL_Custom_DropDown_FourthDTO> GetFourthCustomDRD(DataTable dataTable)
        {
            List<IPL_Custom_DropDown_FourthDTO> FourthMaster_Data = new List<IPL_Custom_DropDown_FourthDTO>();
            try
            {
                var myEnumerableFeaprd = dataTable.AsEnumerable();
                 FourthMaster_Data =

                    (from item in myEnumerableFeaprd
                     select new IPL_Custom_DropDown_FourthDTO
                     {
                         Ipl_Fourth_PkeyId = item.Field<Int64>("Ipl_Fourth_PkeyId"),
                         Ipl_Fourth_Name = item.Field<String>("Ipl_Fourth_Name"),

                     }).ToList();

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return FourthMaster_Data;


        }
    }
}