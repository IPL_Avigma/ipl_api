﻿using Avigma.Repository.Lib;
using IPL.Models;
using IPLApp.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class StatusMasterDRD
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        private DataSet GetStatusMaster(StatusDetailsDTO model)
        {
            DataSet ds = null;
            try
            {
                 string selectProcedure = "[Get_Status_Master]";
                //string selectProcedure = "[Get_Status_Master_v001]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();
                input_parameters.Add("@Status_ID", 1 + "#bigint#" + model.Status_ID);
                input_parameters.Add("@whereclause", 1 + "#varchar#" + model.whereclause);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        private bool SetStatusValue(Int64 StatusVal,int RoleID)
        {
            bool val = false;
            switch (RoleID)
            {
                case 1:
                case 3:
                case 4:
                    {
                        if (StatusVal == 1 || StatusVal == 2 || StatusVal == 3 || StatusVal == 4 || StatusVal == 5 || StatusVal == 6)
                        {
                            val = true;
                        }
                        break;
                    }
                case 2: // For Contractor  Only Assigend / Read
                    {
                        if (StatusVal == 2 || StatusVal == 3 )
                        {
                            val = true;
                        }
                        break;

                    }

            }
            
            return val;
        }

        //Get drop down
        public List<dynamic> GetStatusMasterDetails(StatusDetailsDTO model)
        {
            List<StatusMasterDto> statusMasterDto = new List<StatusMasterDto>();
            if (!string.IsNullOrEmpty(model.whereclause))
            {
                statusMasterDto = JsonConvert.DeserializeObject<List<StatusMasterDto>>(model.whereclause);
            }
           

            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                int RoleID = 0;
                DataSet ds = GetStatusMaster(model);

                if (ds.Tables.Count>1)
                {
                    string strRoleID = ds.Tables[1].Rows[0]["RoleID"].ToString();
                    if (!string.IsNullOrEmpty(strRoleID))
                    {
                        RoleID = Convert.ToInt32(strRoleID);
                    }
                   
                }

                var myEnumerableInfso = ds.Tables[0].AsEnumerable();
                List<StatusMasterDto> StatusDetails =
                   (from item in myEnumerableInfso
                    select new StatusMasterDto
                    {
                        Status_ID = item.Field<Int64>("Status_ID"),
                        Status_Name = item.Field<String>("Status_Name"),
                        mydata = SetStatusValue(item.Field<Int64>("Status_ID"), RoleID),
                       

                    }).ToList();

                objDynamic.Add(StatusDetails);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

            }

            return objDynamic;
        }

     
    }
}