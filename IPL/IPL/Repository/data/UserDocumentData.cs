﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class UserDocumentData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();


        public List<dynamic> AddUserDocumentData(UserDocumentDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();

            string insertProcedure = "[CreateUpdateUserDocumentMaster]";
            UserDocument userDocument = new UserDocument();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@User_Doc_pkeyID", 1 + "#bigint#" + model.User_Doc_pkeyID);
                input_parameters.Add("@User_Doc_DocPath", 1 + "#varchar#" + model.User_Doc_DocPath);
                input_parameters.Add("@User_Doc_FileName", 1 + "#varchar#" + model.User_Doc_FileName);
                input_parameters.Add("@User_Doc_ValType", 1 + "#varchar#" + model.User_Doc_ValType);
                input_parameters.Add("@User_Doc_Exp_Date", 1 + "#datetime#" + model.User_Doc_Exp_Date);
                input_parameters.Add("@User_Doc_RecievedDate", 1 + "#datetime#" + model.User_Doc_RecievedDate);
                input_parameters.Add("@User_Doc_NotificationDate", 1 + "#datetime#" + model.User_Doc_NotificationDate);
                input_parameters.Add("@User_Doc_AlertUser", 1 + "#bit#" + model.User_Doc_AlertUser);
                input_parameters.Add("@User_Doc_UserID", 1 + "#bigint#" + model.User_Doc_UserID);
                input_parameters.Add("@User_Doc_IsActive", 1 + "#bit#" + model.User_Doc_IsActive);
                input_parameters.Add("@User_Doc_IsDelete", 1 + "#bit#" + model.User_Doc_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@User_Doc_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters); ;
                if (objData[1] == 0)
                {
                    userDocument.User_Doc_pkeyID = "0";
                    userDocument.Status = "0";
                    userDocument.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    userDocument.User_Doc_pkeyID = Convert.ToString(objData[0]);
                    userDocument.Status = Convert.ToString(objData[1]);


                }
                objcltData.Add(userDocument);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;



        }

        public List<dynamic> AddUserDocumentDataDetails(UserDocumentDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                objData = AddUserDocumentData(model);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }
            return objData;
        }

        private DataSet GetUserDocMaster(UserDocumentDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_UserDocumentMaster]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@User_Doc_pkeyID", 1 + "#bigint#" + model.User_Doc_pkeyID);
                input_parameters.Add("@User_Doc_UserID", 1 + "#bigint#" + model.User_Doc_UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetUserDocumentDetails(UserDocumentDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetUserDocMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<UserDocumentDTO>UserDocDetails =
                   (from item in myEnumerableFeaprd
                    select new UserDocumentDTO
                    {
                        User_Doc_pkeyID = item.Field<Int64>("User_Doc_pkeyID"),
                        User_Doc_DocPath = item.Field<String>("User_Doc_DocPath"),
                        User_Doc_FileName = item.Field<String>("User_Doc_FileName"),
                        User_Doc_ValType = item.Field<String>("User_Doc_ValType"),
                        User_Doc_Exp_Date = item.Field<DateTime?>("User_Doc_Exp_Date"),
                        User_Doc_RecievedDate = item.Field<DateTime?>("User_Doc_RecievedDate"),
                        User_Doc_NotificationDate = item.Field<DateTime?>("User_Doc_NotificationDate"),
                        User_Doc_AlertUser = item.Field<Boolean?>("User_Doc_AlertUser"),
                        User_Doc_UserID = item.Field<Int64?>("User_Doc_UserID"),
                        User_Doc_IsActive = item.Field<Boolean?>("User_Doc_IsActive"),

                    }).ToList();

                objDynamic.Add(UserDocDetails);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}