﻿using Avigma.Repository.Lib;
using IPL.Models;
using IPLApp.Models;
using IPLApp.Repository.data;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class ApplianceMasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public List<dynamic> AddApplianceMasterDetails(ApplianceMasterDTO model)
        {


            string insertProcedure = "[CreateUpdate_Appliance]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Appl_pkeyId", 1 + "#bigint#" + model.Appl_pkeyId);
                input_parameters.Add("@Appl_Wo_Id", 1 + "#bigint#" + model.Appl_Wo_Id);
                input_parameters.Add("@Appl_App_Id", 1 + "#bigint#" + model.Appl_App_Id);
                input_parameters.Add("@Appl_Apliance_Name", 1 + "#varchar#" + model.Appl_Apliance_Name);
                input_parameters.Add("@Appl_Comment", 1 + "#varchar#" + model.Appl_Comment);
                input_parameters.Add("@Appl_Status_Id", 1 + "#int#" + model.Appl_Status_Id);
                input_parameters.Add("@Appl_IsActive", 1 + "#bit#" + model.Appl_IsActive);
                input_parameters.Add("@Appl_IsDelete", 1 + "#bit#" + model.Appl_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Appl_pkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return obj.SqlCRUD(insertProcedure, input_parameters);



        }

        private int? GetStatusValue(string strStatus)
        {
            int? val = 0;
            try
            {
                switch (strStatus)
                {
                    case "Yes":
                        {
                            val = 1;
                            break;
                        }
                    case "No":
                        {
                            val = 2;
                            break;
                        }
                    case "Missing":
                        {
                            val = 3;
                            break;
                        }
                    case "Damage":
                        {
                            val = 4;
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return val;
        }

        private List<dynamic> AddUpdateApplianceMasterdata(RootObject_App model)
        {
            List<dynamic> objcltData = new List<dynamic>();

            for (int i = 0; i < model.ApplianceMasterDTO.Count; i++)
            {
                ApplianceMasterDTO applianceMasterDTO = new ApplianceMasterDTO();

                applianceMasterDTO.Appl_pkeyId = model.ApplianceMasterDTO[i].Appl_pkeyId;
                applianceMasterDTO.Appl_App_Id = model.ApplianceMasterDTO[i].App_pkeyId;
                applianceMasterDTO.Appl_Apliance_Name = model.ApplianceMasterDTO[i].App_Apliance_Name;
                applianceMasterDTO.Appl_Wo_Id = model.Appl_Wo_Id;
                applianceMasterDTO.Appl_Status_Id = GetStatusValue(model.ApplianceMasterDTO[i].str_Appl_Status_Id);
                applianceMasterDTO.Appl_Comment = model.ApplianceMasterDTO[i].Appl_Comment;
                applianceMasterDTO.Appl_IsActive = true;
                applianceMasterDTO.UserID = model.UserID;
                if (model.ApplianceMasterDTO[i].Appl_pkeyId != 0)
                {
                    applianceMasterDTO.Type = 2;
                }
                else
                {
                    applianceMasterDTO.Type = model.Type;
                }


                var returnval = AddApplianceMasterDetails(applianceMasterDTO);
            }

            Access_log_MasterData access_Log_MasterData = new Access_log_MasterData();
            NewAccessLogMaster newAccessLogMaster = new NewAccessLogMaster();
            newAccessLogMaster.Access_WorkerOrderID = model.Appl_Wo_Id;
            newAccessLogMaster.Access_Master_ID = 40;
            newAccessLogMaster.Access_UserID = model.UserID;
            newAccessLogMaster.Type = 1;
            access_Log_MasterData.AddNewAccessLogMaster(newAccessLogMaster);


            return objcltData;
        }


        public List<dynamic> AddApplianceMasterdata(RootObject_App model)
        {
            List<dynamic> objData = new List<dynamic>();
            //List<dynamic> objAddData = new List<dynamic>();

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {

                if (!string.IsNullOrEmpty(model.str_Task_Bid_WO_ID))
                {
                    var Data = JsonConvert.DeserializeObject<List<TaskActionWorkOrder>>(model.str_Task_Bid_WO_ID);
                    for (int j = 0; j < Data.Count; j++)
                    {
                        model.Appl_Wo_Id = Data[j].Task_WO_ID;
                        objData = AddUpdateApplianceMasterdata(model);
                    }
                }
                else
                {
                    objData = AddUpdateApplianceMasterdata(model);
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objData;
        }


        private DataSet GetApplianceMaster(ApplianceMasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Appliance_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Appl_pkeyId", 1 + "#bigint#" + model.Appl_pkeyId);
                input_parameters.Add("@Appl_Wo_Id", 1 + "#bigint#" + model.Appl_Wo_Id);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetApplianceMasterDetails(ApplianceMasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetApplianceMaster(model);

                if (ds.Tables.Count > 0 )
                {
                    var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                    List<ApplianceMasterDTO> ApplianceMaster =
                       (from item in myEnumerableFeaprd
                        select new ApplianceMasterDTO
                        {
                            Appl_pkeyId = item.Field<Int64>("Appl_pkeyId"),
                            Appl_Wo_Id = item.Field<Int64>("Appl_Wo_Id"),
                            App_pkeyId = item.Field<Int64>("Appl_App_Id"),
                            App_Apliance_Name = item.Field<String>("Appl_Apliance_Name"),
                            Appl_Comment = item.Field<String>("Appl_Comment"),
                        str_Appl_Status_Id = item.Field<String>("str_Appl_Status_Id"),
                        Appl_IsActive = item.Field<Boolean?>("Appl_IsActive"),



                        }).ToList();

                    objDynamic.Add(ApplianceMaster);
                }
         
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }


      


    }
}