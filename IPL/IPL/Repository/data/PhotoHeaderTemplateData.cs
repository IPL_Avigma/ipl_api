﻿using Avigma.Repository.Lib;
using IPL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class PhotoHeaderTemplateData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
      
        public List<dynamic> AddPhotoheaderData(PhotoHeaderTemplateDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_PhotoHeaderTemplateMaster]";
            PhotoHeaderTemplate photoHeaderTemplate = new PhotoHeaderTemplate();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Photo_head_PkeyId", 1 + "#bigint#" + model.Photo_head_PkeyId);
                input_parameters.Add("@Photo_head_HeaderTemp", 1 + "#nvarchar#" + model.Photo_head_HeaderTemp);
                input_parameters.Add("@Photo_head_Client_Company", 1 + "#bigint#" + model.Photo_head_Client_Company);
                input_parameters.Add("@Photo_head_FileName_Temp", 1 + "#nvarchar#" + model.Photo_head_FileName_Temp);
                input_parameters.Add("@Photo_head_Pdf_Temp", 1 + "#bit#" + model.Photo_head_Pdf_Temp);
                input_parameters.Add("@Photo_head_IsActive", 1 + "#bit#" + model.Photo_head_IsActive);
                input_parameters.Add("@Photo_head_IsDelete", 1 + "#bit#" + model.Photo_head_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Photo_head_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);


                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    photoHeaderTemplate.Photo_head_PkeyId = "0";
                    photoHeaderTemplate.Status = "0";
                    photoHeaderTemplate.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    photoHeaderTemplate.Photo_head_PkeyId = Convert.ToString(objData[0]);
                    photoHeaderTemplate.Status = Convert.ToString(objData[1]);


                }
                objcltData.Add(photoHeaderTemplate);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;



        }
       
        private DataSet GetPhotoHeaderTempMaster(PhotoHeaderTemplateDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_PhotoHeaderTemplateMaster]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Photo_head_PkeyId", 1 + "#bigint#" + model.Photo_head_PkeyId);
                input_parameters.Add("@WhereClause", 1 + "#varchar#" + model.WhereClause);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ds;
        }

        public List<dynamic> GetPhotoHeaderTempDetails(PhotoHeaderTemplateDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {

                DataSet ds = GetPhotoHeaderTempMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<PhotoHeaderTemplateDTO> PhotoHeaderDetails =
                   (from item in myEnumerableFeaprd
                    select new PhotoHeaderTemplateDTO
                    {
                        Photo_head_PkeyId = item.Field<Int64>("Photo_head_PkeyId"),
                        Photo_head_HeaderTemp = item.Field<String>("Photo_head_HeaderTemp"),
                        Photo_head_Client_Company = item.Field<Int64?>("Photo_head_Client_Company"),
                        Photo_head_FileName_Temp = item.Field<String>("Photo_head_FileName_Temp"),
                        Photo_head_Pdf_Temp = item.Field<Boolean?>("Photo_head_Pdf_Temp"),
                        Photo_head_IsActive = item.Field<Boolean?>("Photo_head_IsActive"),
                        Client_Company_Name = item.Field<String>("Client_Company_Name"),
                        Photo_head_CreatedBy= item.Field<String>("Photo_head_CreatedBy"),
                        Photo_head_ModifiedBy = item.Field<String>("Photo_head_ModifiedBy"),
                        ViewUrl = null

                    }).ToList();

                objDynamic.Add(PhotoHeaderDetails);
                if (model.Type == 1)
                {
                    if (ds.Tables.Count > 1)
                    {
                        var myEnumerabledamage = ds.Tables[1].AsEnumerable();
                        List<Filter_Admin_PhotoHeader_DTO> FilterPhotoheader =
                           (from item in myEnumerabledamage
                            select new Filter_Admin_PhotoHeader_DTO
                            {
                                Photo_Head_Filter_PkeyId = item.Field<Int64>("Photo_Head_Filter_PkeyId"),
                                Photo_Head_Filter_Header_Temp = item.Field<String>("Photo_Head_Filter_Header_Temp"),
                                Photo_Head_Filter_Client_Company = item.Field<Int64?>("Photo_Head_Filter_Client_Company"),
                                Photo_Head_Filter_File_Name_temp = item.Field<String>("Photo_Head_Filter_File_Name_temp"),
                                Photo_Head_Filter_pdf_temp = item.Field<Boolean?>("Photo_Head_Filter_pdf_temp"),
                                Photo_Head_Filter_FilterActive = item.Field<Boolean?>("Photo_Head_Filter_FilterActive")

                            }).ToList();

                        objDynamic.Add(FilterPhotoheader);
                    }
                }


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        public List<dynamic> GetPhotoHeaderFilterDetails(PhotoHeaderTemplateDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();


            string wherecondition = string.Empty;
            try
            {

                var Data = JsonConvert.DeserializeObject<PhotoHeaderTemplateDTO>(model.FilterData);
               
                if (!string.IsNullOrEmpty(Data.Photo_head_HeaderTemp))
                {
                   
                    wherecondition = wherecondition + " And Photo_head_HeaderTemp LIKE '%" + Data.Photo_head_HeaderTemp + "%'";

                }

                if (Data.Photo_head_IsActive == true)
                {
                    wherecondition = wherecondition + "  And Photo_head_IsActive =  '1'";
                }
                if (Data.Photo_head_IsActive == false)
                {
                    wherecondition = wherecondition + "  And Photo_head_IsActive =  '0'";
                }


                PhotoHeaderTemplateDTO photoHeaderTemplateDTO = new PhotoHeaderTemplateDTO();

                photoHeaderTemplateDTO.WhereClause = wherecondition;
                photoHeaderTemplateDTO.Type = 3;
                photoHeaderTemplateDTO.UserID = model.UserID;

                DataSet ds = GetPhotoHeaderTempMaster(photoHeaderTemplateDTO);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<PhotoHeaderTemplateDTO> PhotoHeaderfDetails =
                   (from item in myEnumerableFeaprd
                    select new PhotoHeaderTemplateDTO
                    {
                        Photo_head_PkeyId = item.Field<Int64>("Photo_head_PkeyId"),
                        Photo_head_HeaderTemp = item.Field<String>("Photo_head_HeaderTemp"),
                        Photo_head_Client_Company = item.Field<Int64?>("Photo_head_Client_Company"),
                        Photo_head_FileName_Temp = item.Field<String>("Photo_head_FileName_Temp"),
                        Photo_head_Pdf_Temp = item.Field<Boolean?>("Photo_head_Pdf_Temp"),
                        Photo_head_IsActive = item.Field<Boolean?>("Photo_head_IsActive"),
                        Client_Company_Name = item.Field<String>("Client_Company_Name"),
                        Photo_head_CreatedBy = item.Field<String>("Photo_head_CreatedBy"),
                        Photo_head_ModifiedBy = item.Field<String>("Photo_head_ModifiedBy"),
                        ViewUrl = null
                        
                    }).ToList();

                objDynamic.Add(PhotoHeaderfDetails);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }

            return objDynamic;
        }

    }
}