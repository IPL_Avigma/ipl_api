﻿using Avigma.Repository.Lib;
using IPLApp.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class PastWorkOrderHistoryData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        private DataSet Get_Past_WorkOrder_HistoryData(WorkOrderHistoryDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_WorkOrder_Past_HistoryData]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@workOrder_ID", 1 + "#bigint#" + model.workOrder_ID);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@PageNumber", 1 + "#int#" + model.PageNumber);
                input_parameters.Add("@NoofRows", 1 + "#int#" + model.NoofRows);
                input_parameters.Add("@Skip", 1 + "#int#" + model.Skip);
                input_parameters.Add("@FilterData", 1 + "#varchar#" + model.FilterWhereClause);


                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }
        public string GenerateWhereClauseforFilter(string FilterData)
        {
            string whereclause = string.Empty;
            try
            {
                var Data = JsonConvert.DeserializeObject<List<FilterDataDTO>>(FilterData);
                for (int i = 0; i < Data.Count; i++)
                {
                    string Field = string.Empty, Value = string.Empty;
                    switch (Data[i].field)
                    {
                        case "dueDate":  
                            {

                                Field = "cast( wo." + Data[i].field + " as date)";
                                Value = " = cast('" + Convert.ToDateTime(Data[i].value) + "' as date)";
                                if (string.IsNullOrEmpty(whereclause))
                                {
                                    whereclause = Field + Value;
                                }
                                else
                                {
                                    whereclause = whereclause + " and " + Field + Value;
                                }
                                break;
                            }
                        case "status":
                            {
                                Field = "sm.Status_Name";
                                Value = " like '%" + Data[i].value + "%'";
                                if (string.IsNullOrEmpty(whereclause))
                                {
                                    whereclause = Field + Value;
                                }
                                else
                                {
                                    whereclause = whereclause + " and " + Field + Value;
                                }
                                break;
                            }
                        case "WT_WorkType":
                            {
                                Field = "wtm.WT_WorkType";
                                Value = " like '%" + Data[i].value + "%'";
                                if (string.IsNullOrEmpty(whereclause))
                                {
                                    whereclause = Field + Value;
                                }
                                else
                                {
                                    whereclause = whereclause + " and " + Field + Value;
                                }
                                break;
                            }
                        case "CORNT_User_FirstName":
                            {
                                Field = "CORNT.User_FirstName like '%" + Data[i].value + "%' or " + "CORNT.User_LastName like '%" + Data[i].value + "%'";
                                if (string.IsNullOrEmpty(whereclause))
                                {
                                    whereclause = Field ;
                                }
                                else
                                {
                                    whereclause = whereclause + " and " + Field;
                                }
                                break;
                            }
                        case "FullAddress":
                            {
                                Field = "wo.finaladdress";
                                Value = " like '%" + Data[i].value + "%'";
                                if (string.IsNullOrEmpty(whereclause))
                                {
                                    whereclause = Field + Value;
                                }
                                else
                                {
                                    whereclause = whereclause + " and " + Field + Value;
                                }
                                break;
                            }

                        default:
                            {
                                Field = "wo." + Data[i].field;
                                //Field = "wo.WT_WorkType";
                                Value = " like '%" + Data[i].value + "%'";
                                if (string.IsNullOrEmpty(whereclause))
                                {
                                    whereclause = Field + Value;
                                }
                                else
                                {
                                    whereclause = whereclause + " and " + Field + Value;
                                }
                                break;
                            }
                    }

                    
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return whereclause;
        }


        public List<dynamic> Get_Past_WorkOrderHistoryDetails(WorkOrderHistoryDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            WorkOrderHistoryData workOrderHistoryData = new WorkOrderHistoryData();

            try
            {
               
                if (!string.IsNullOrWhiteSpace(model.FilterData))
                {
                    string where = GenerateWhereClauseforFilter(model.FilterData);

                    model.FilterWhereClause = " and "+where;

                }

                DataSet ds = Get_Past_WorkOrder_HistoryData(model);

                log.logDebugMessage(ds.Tables.Count.ToString());

                //past workorder history
                if (ds.Tables.Count > 0)
                {
                    log.logDebugMessage("0");
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        log.logDebugMessage("R4");

                        var MYEnumerablefeaprd = ds.Tables[0].AsEnumerable();
                        List<WorkOrderHistoryPastDTO> WorkOrderHistorypastDetails =
                           (from item in MYEnumerablefeaprd
                            select new WorkOrderHistoryPastDTO
                            {
                                workOrder_ID = item.Field<Int64>("workOrder_ID"),
                                workOrderNumber = item.Field<String>("workOrderNumber"),
                                workOrderInfo = item.Field<String>("workOrderInfo"),
                                address1 = item.Field<String>("address1"),
                                address2 = item.Field<String>("address2"),
                                city = item.Field<String>("city"),
                                state = item.Field<String>("state"),
                                zip = item.Field<Int64?>("zip"),
                                country = item.Field<String>("country"),
                                status = item.Field<String>("status"),
                                dueDate = item.Field<DateTime?>("dueDate"),
                                strdueDate = item.Field<String>("strdueDate"),
                                startDate = item.Field<DateTime?>("startDate"),
                                clientStatus = item.Field<String>("clientStatus"),
                                clientDueDate = item.Field<DateTime?>("clientDueDate"),
                                gpsLatitude = item.Field<String>("gpsLatitude"),
                                gpsLongitude = item.Field<String>("gpsLongitude"),
                                FullAddress = item.Field<String>("FullAddress"),
                                Lock_Location = item.Field<String>("Lock_Location"),
                                Key_Code = item.Field<String>("Key_Code"),
                                Gate_Code = item.Field<String>("Gate_Code"),
                                Loan_Number = item.Field<String>("Loan_Number"),

                                Office_Approved = item.Field<String>("Office_Approved"),
                                Sent_to_Client = item.Field<String>("Sent_to_Client"),
                                IPLNO = item.Field<String>("IPLNO"),
                                CORNT_User_FirstName = item.Field<String>("CORNT_User_FirstName"),
                                //IsActive = item.Field<Boolean?>("IsActive"),
                                WT_WorkType = item.Field<String>("WT_WorkType"),
                                CountPhotos = item.Field<int>("CountPhotos"),
                            }).ToList();

                        objDynamic.Add(WorkOrderHistorypastDetails);
                        objDynamic.Add(workOrderHistoryData.Get_WorkOrderHistoryDetails(model));

                        if (ds.Tables.Count > 1)
                        {
                            for (int i = 1; i < ds.Tables.Count; i++)
                            {
                                //result=ds.Tables[i];

                                objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));

                            }
                        }

                    }
                }
            }


            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);

            }

            return objDynamic;
        }
        private DataSet Get_Restored_WorkOrder_Data(WorkOrderHistoryDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_WorkOrder_Deleted_Edit_Data]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@workOrder_ID", 1 + "#bigint#" + model.workOrder_ID);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);


                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }
        public List<dynamic> Get_Restored_WorkOrderDetails(WorkOrderHistoryDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            WorkOrderHistoryData workOrderHistoryData = new WorkOrderHistoryData();

            try
            {


                DataSet ds = Get_Restored_WorkOrder_Data(model);

                log.logDebugMessage(ds.Tables.Count.ToString());

                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                }

                //past workorder history
                //if (ds.Tables.Count > 0)
                //{
                //    log.logDebugMessage("0");
                //    if (ds.Tables[0].Rows.Count > 0)
                //    {
                //        log.logDebugMessage("R4");

                //        var MYEnumerablefeaprd = ds.Tables[0].AsEnumerable();
                //        List<WorkOrderHistoryPastDTO> WorkOrderHistorypastDetails =
                //           (from item in MYEnumerablefeaprd
                //            select new WorkOrderHistoryPastDTO
                //            {
                //                workOrder_ID = item.Field<Int64>("workOrder_ID"),
                //                workOrderNumber = item.Field<String>("workOrderNumber"),
                //                workOrderInfo = item.Field<String>("workOrderInfo"),
                //                address1 = item.Field<String>("address1"),
                //                address2 = item.Field<String>("address2"),
                //                city = item.Field<String>("city"),
                //                state = item.Field<String>("state"),
                //                zip = item.Field<Int64?>("zip"),
                //                country = item.Field<String>("country"),
                //                status = item.Field<String>("status"),
                //                dueDate = item.Field<DateTime?>("dueDate"),
                //                strdueDate = item.Field<String>("strdueDate"),
                //                startDate = item.Field<DateTime?>("startDate"),
                //                clientStatus = item.Field<String>("clientStatus"),
                //                clientDueDate = item.Field<DateTime?>("clientDueDate"),
                //                gpsLatitude = item.Field<String>("gpsLatitude"),
                //                gpsLongitude = item.Field<String>("gpsLongitude"),
                //                FullAddress = item.Field<String>("FullAddress"),
                //                Lock_Location = item.Field<String>("Lock_Location"),
                //                Key_Code = item.Field<String>("Key_Code"),
                //                Gate_Code = item.Field<String>("Gate_Code"),
                //                Loan_Number = item.Field<String>("Loan_Number"),

                //                Office_Approved = item.Field<String>("Office_Approved"),
                //                Sent_to_Client = item.Field<String>("Sent_to_Client"),
                //                IPLNO = item.Field<String>("IPLNO"),
                //                CORNT_User_FirstName = item.Field<String>("CORNT_User_FirstName"),
                //                //IsActive = item.Field<Boolean?>("IsActive"),
                //                WT_WorkType = item.Field<String>("WT_WorkType"),
                //                IsEdit = item.Field<Boolean?>("IsEdit"),
                //                IsActive = item.Field<Boolean?>("IsActive"),
                //                IsDelete = item.Field<Boolean?>("IsDelete"),
                //                CreatedBy = item.Field<String>("CreatedBy"),
                //                ModifiedBy = item.Field<String>("ModifiedBy"),
                //                CompanyName = item.Field<String>("CompanyName"),
                //            }).ToList();

                //        objDynamic.Add(WorkOrderHistorypastDetails);
                //        objDynamic.Add(workOrderHistoryData.Get_WorkOrderHistoryDetails(model));
                //    }
                //}
            }


            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);

            }

            return objDynamic;
        }

    }
}