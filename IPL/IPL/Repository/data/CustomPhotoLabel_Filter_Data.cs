﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class CustomPhotoLabel_Filter_Data
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        public List<dynamic> AddUpdate_CustomPhotoLabel_Filter_Master(CustomPhotoLabel_FilterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();

            string insertProcedure = "[CreateUpdate_CustomPhotoLabel_Filter_Master]";
            
            try
            {
                input_parameters.Add("@CustomPhotoLabel_Filter_Master_PkeyId", 1 + "#bigint#" + model.CustomPhotoLabel_Filter_Master_PkeyId);
                input_parameters.Add("@CustomPhotoLabel_Filter_Client", 1 + "#nvarchar#" + model.CustomPhotoLabel_Filter_Client);
                input_parameters.Add("@CustomPhotoLabel_Filter_Customer", 1 + "#nvarchar#" + model.CustomPhotoLabel_Filter_Customer);
                input_parameters.Add("@CustomPhotoLabel_Filter_LoanType", 1 + "#nvarchar#" + model.CustomPhotoLabel_Filter_LoanType);
                input_parameters.Add("@CustomPhotoLabel_Filter_WorkType", 1 + "#nvarchar#" + model.CustomPhotoLabel_Filter_WorkType);
                input_parameters.Add("@CustomPhotoLabel_Filter_WorkTypeGroup", 1 + "#nvarchar#" + model.CustomPhotoLabel_Filter_WorkTypeGroup);
                input_parameters.Add("@CustomPhotoLabel_Filter_State", 1 + "#nvarchar#" + model.CustomPhotoLabel_Filter_State);
                input_parameters.Add("@CustomPhotoLabel_Filter_County", 1 + "#nvarchar#" + model.CustomPhotoLabel_Filter_County);
                input_parameters.Add("@CustomPhotoLabel_Filter_IsActive", 1 + "#bit#" + model.CustomPhotoLabel_Filter_IsActive);
                input_parameters.Add("@CustomPhotoLabel_Filter_IsDelete", 1 + "#bit#" + model.CustomPhotoLabel_Filter_IsDelete);
                input_parameters.Add("@Custom_PhotoLabel_Master_FkeyId", 1 + "#bigint#" + model.Custom_PhotoLabel_Master_FkeyId);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@CustomPhotoLabel_Filter_Master_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        public List<dynamic> AddUpdate_CustomPhotoLabel_Filter_Client(CustomPhotoLabel_Filter_ClientDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();

            string insertProcedure = "[CreateUpdate_CustomPhotoLabel_Filter_Client]";
            try
            {
                input_parameters.Add("@CustomPhotoLabel_Filter_Client_PkeyId", 1 + "#bigint#" + model.CustomPhotoLabel_Filter_Client_PkeyId);
                input_parameters.Add("@CustomPhotoLabel_Filter_Client_CId", 1 + "#bigint#" + model.CustomPhotoLabel_Filter_Client_CId);
                input_parameters.Add("@Custom_PhotoLabel_Master_FkeyId", 1 + "#bigint#" + model.Custom_PhotoLabel_Master_FkeyId);
                input_parameters.Add("@CustomPhotoLabel_Filter_Master_FkeyId", 1 + "#bigint#" + model.CustomPhotoLabel_Filter_Master_FkeyId);
                input_parameters.Add("@CustomPhotoLabel_Filter_Client_IsActive", 1 + "#bit#" + model.CustomPhotoLabel_Filter_Client_IsActive);
                input_parameters.Add("@CustomPhotoLabel_Filter_Client_IsDelete", 1 + "#bit#" + model.CustomPhotoLabel_Filter_Client_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@CustomPhotoLabel_Filter_Client_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        public List<dynamic> AddUpdate_CustomPhotoLabel_Filter_County(CustomPhotoLabel_Filter_CountyDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();

            string insertProcedure = "[CreateUpdate_CustomPhotoLabel_Filter_County]";
            try
            {
                input_parameters.Add("@CustomPhotoLabel_Filter_County_PkeyId", 1 + "#bigint#" + model.CustomPhotoLabel_Filter_County_PkeyId);
                input_parameters.Add("@CustomPhotoLabel_Filter_County_CnId", 1 + "#bigint#" + model.CustomPhotoLabel_Filter_County_CnId);
                input_parameters.Add("@Custom_PhotoLabel_Master_FkeyId", 1 + "#bigint#" + model.Custom_PhotoLabel_Master_FkeyId);
                input_parameters.Add("@CustomPhotoLabel_Filter_Master_FkeyId", 1 + "#bigint#" + model.CustomPhotoLabel_Filter_Master_FkeyId);
                input_parameters.Add("@CustomPhotoLabel_Filter_County_IsActive", 1 + "#bit#" + model.CustomPhotoLabel_Filter_County_IsActive);
                input_parameters.Add("@CustomPhotoLabel_Filter_County_IsDelete", 1 + "#bit#" + model.CustomPhotoLabel_Filter_County_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@CustomPhotoLabel_Filter_County_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        public List<dynamic> AddUpdate_CustomPhotoLabel_Filter_Customer(CustomPhotoLabel_Filter_CustomerDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();

            string insertProcedure = "[CreateUpdate_CustomPhotoLabel_Filter_Customer]";
            try
            {
                input_parameters.Add("@CustomPhotoLabel_Filter_Customer_PkeyId", 1 + "#bigint#" + model.CustomPhotoLabel_Filter_Customer_PkeyId);
                input_parameters.Add("@CustomPhotoLabel_Filter_Customer_CustId", 1 + "#bigint#" + model.CustomPhotoLabel_Filter_Customer_CustId);
                input_parameters.Add("@Custom_PhotoLabel_Master_FkeyId", 1 + "#bigint#" + model.Custom_PhotoLabel_Master_FkeyId);
                input_parameters.Add("@CustomPhotoLabel_Filter_Master_FkeyId", 1 + "#bigint#" + model.CustomPhotoLabel_Filter_Master_FkeyId);
                input_parameters.Add("@CustomPhotoLabel_Filter_Customer_IsActive", 1 + "#bit#" + model.CustomPhotoLabel_Filter_Customer_IsActive);
                input_parameters.Add("@CustomPhotoLabel_Filter_Customer_IsDelete", 1 + "#bit#" + model.CustomPhotoLabel_Filter_Customer_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@CustomPhotoLabel_Filter_Customer_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        public List<dynamic> AddUpdate_CustomPhotoLabel_Filter_LoanType(CustomPhotoLabel_Filter_LoanTypeDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();

            string insertProcedure = "[CreateUpdate_CustomPhotoLabel_Filter_LoanType]";
            try
            {
                input_parameters.Add("@CustomPhotoLabel_Filter_LoanType_PkeyId", 1 + "#bigint#" + model.CustomPhotoLabel_Filter_LoanType_PkeyId);
                input_parameters.Add("@CustomPhotoLabel_Filter_LoanType_LId", 1 + "#bigint#" + model.CustomPhotoLabel_Filter_LoanType_LId);
                input_parameters.Add("@Custom_PhotoLabel_Master_FkeyId", 1 + "#bigint#" + model.Custom_PhotoLabel_Master_FkeyId);
                input_parameters.Add("@CustomPhotoLabel_Filter_Master_FkeyId", 1 + "#bigint#" + model.CustomPhotoLabel_Filter_Master_FkeyId);
                input_parameters.Add("@CustomPhotoLabel_Filter_LoanType_IsActive", 1 + "#bit#" + model.CustomPhotoLabel_Filter_LoanType_IsActive);
                input_parameters.Add("@CustomPhotoLabel_Filter_LoanType_IsDelete", 1 + "#bit#" + model.CustomPhotoLabel_Filter_LoanType_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@CustomPhotoLabel_Filter_LoanType_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        public List<dynamic> AddUpdate_CustomPhotoLabel_Filter_State(CustomPhotoLabel_Filter_StateDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();

            string insertProcedure = "[CreateUpdate_CustomPhotoLabel_Filter_State]";
            try
            {
                input_parameters.Add("@CustomPhotoLabel_Filter_State_PkeyId", 1 + "#bigint#" + model.CustomPhotoLabel_Filter_State_PkeyId);
                input_parameters.Add("@CustomPhotoLabel_Filter_State_SId", 1 + "#bigint#" + model.CustomPhotoLabel_Filter_State_SId);
                input_parameters.Add("@Custom_PhotoLabel_Master_FkeyId", 1 + "#bigint#" + model.Custom_PhotoLabel_Master_FkeyId);
                input_parameters.Add("@CustomPhotoLabel_Filter_Master_FkeyId", 1 + "#bigint#" + model.CustomPhotoLabel_Filter_Master_FkeyId);
                input_parameters.Add("@CustomPhotoLabel_Filter_State_IsActive", 1 + "#bit#" + model.CustomPhotoLabel_Filter_State_IsActive);
                input_parameters.Add("@CustomPhotoLabel_Filter_State_IsDelete", 1 + "#bit#" + model.CustomPhotoLabel_Filter_State_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@CustomPhotoLabel_Filter_State_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        public List<dynamic> AddUpdate_CustomPhotoLabel_Filter_WorkType(CustomPhotoLabel_Filter_WorkTypeDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();

            string insertProcedure = "[CreateUpdate_CustomPhotoLabel_Filter_WorkType]";
            try
            {
                input_parameters.Add("@CustomPhotoLabel_Filter_WorkType_PkeyId", 1 + "#bigint#" + model.CustomPhotoLabel_Filter_WorkType_PkeyId);
                input_parameters.Add("@CustomPhotoLabel_Filter_WorkType_WId", 1 + "#bigint#" + model.CustomPhotoLabel_Filter_WorkType_WId);
                input_parameters.Add("@Custom_PhotoLabel_Master_FkeyId", 1 + "#bigint#" + model.Custom_PhotoLabel_Master_FkeyId);
                input_parameters.Add("@CustomPhotoLabel_Filter_Master_FkeyId", 1 + "#bigint#" + model.CustomPhotoLabel_Filter_Master_FkeyId);
                input_parameters.Add("@CustomPhotoLabel_Filter_WorkType_IsActive", 1 + "#bit#" + model.CustomPhotoLabel_Filter_WorkType_IsActive);
                input_parameters.Add("@CustomPhotoLabel_Filter_WorkType_IsDelete", 1 + "#bit#" + model.CustomPhotoLabel_Filter_WorkType_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@CustomPhotoLabel_Filter_WorkType_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        public List<dynamic> AddUpdate_CustomPhotoLabel_Filter_WorkTypeGroup(CustomPhotoLabel_Filter_WorkTypeGroupDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();

            string insertProcedure = "[CreateUpdate_CustomPhotoLabel_Filter_WorkTypeGroup]";
            try
            {
                input_parameters.Add("@CustomPhotoLabel_Filter_WorkTypeGroup_PkeyId", 1 + "#bigint#" + model.CustomPhotoLabel_Filter_WorkTypeGroup_PkeyId);
                input_parameters.Add("@CustomPhotoLabel_Filter_WorkTypeGroup_WGId", 1 + "#bigint#" + model.CustomPhotoLabel_Filter_WorkTypeGroup_WGId);
                input_parameters.Add("@Custom_PhotoLabel_Master_FkeyId", 1 + "#bigint#" + model.Custom_PhotoLabel_Master_FkeyId);
                input_parameters.Add("@CustomPhotoLabel_Filter_Master_FkeyId", 1 + "#bigint#" + model.CustomPhotoLabel_Filter_Master_FkeyId);
                input_parameters.Add("@CustomPhotoLabel_Filter_WorkTypeGroup_IsActive", 1 + "#bit#" + model.CustomPhotoLabel_Filter_WorkTypeGroup_IsActive);
                input_parameters.Add("@CustomPhotoLabel_Filter_WorkTypeGroup_IsDelete", 1 + "#bit#" + model.CustomPhotoLabel_Filter_WorkTypeGroup_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@CustomPhotoLabel_Filter_WorkTypeGroup_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }
    }
}