﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class ECD_Notes_Data
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public List<dynamic> AddUpdateDeleted_ECDNotes(ECD_Notes model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_ECDNotesMaster]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();


            try
            {
                input_parameters.Add("@ECD_Note_pkeyID", 1 + "#bigint#" + model.ECD_Note_pkeyID);
                input_parameters.Add("@ECD_Note_WorkOrderId", 1 + "#bigint#" + model.ECD_Note_WorkOrderId);
                input_parameters.Add("@ECD_Note_Comment", 1 + "#nvarchar#" + model.ECD_Note_Comment);
                input_parameters.Add("@ECD_Note_Date", 1 + "#datetime#" + model.ECD_Note_Date);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@ECD_Note_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objData;

        }

        public List<dynamic> GetECDNotesList(ECD_Notes_Filter model)
        {
            List<dynamic> objdata = new List<dynamic>();
            try
            {
                DataSet ds = ECDNotesList(model);

                var myEnumerable = ds.Tables[0].AsEnumerable();
                List<ECD_Notes_DTO> WorkOrderData =
                    (from item in myEnumerable
                     select new ECD_Notes_DTO
                     {
                         ECD_Note_pkeyID = item.Field<Int64>("ECD_Note_pkeyID"),
                         ECD_Note_WorkOrderId = item.Field<Int64>("ECD_Note_WorkOrderId"),
                         ECD_Note_CompanyID = item.Field<Int64>("ECD_Note_CompanyID"),
                         ECD_Note_Comment = item.Field<String>("ECD_Note_Comment"),
                         ECD_Note_Date = item.Field<DateTime>("ECD_Note_Date"),
                         ECD_Note_CreatedOn = item.Field<DateTime>("ECD_Note_CreatedOn"),
                         ECD_Note_CreatedBy = item.Field<Int64>("ECD_Note_CreatedBy"),
                         CreatedByUser = item.Field<String>("CreatedByUser"),
                     }).ToList();
                objdata.Add(WorkOrderData);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage("---------Type----->" + model.Type + "<----------------");
                log.logErrorMessage("---------UserID----->" + model.ECD_Note_pkeyID + "<----------------");
                log.logErrorMessage("GetECDNotesList......." + model.ECD_Note_WorkOrderId + " < ----------------");

                log.logErrorMessage(ex.Message);

                return objdata;
            }
            return objdata;
        }
        
        private DataSet ECDNotesList(ECD_Notes_Filter model)
        {
            DataSet ds = new DataSet();
            try
            {
                string insertProcedure = "[Get_ECD_Notes]";
                //string insertProcedure = "[GetWorkOrderMaster_v001]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@ECD_Note_pkeyID", 1 + "#bigint#" + model.ECD_Note_pkeyID);
                input_parameters.Add("@ECD_Note_WorkOrderId", 1 + "#bigint#" + model.ECD_Note_WorkOrderId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(insertProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ds;
        }
    }
}