﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class ClientResult_PropertySettings_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        private DataSet GetClientResult_PropertySettings(ClientResult_PropertySettings_MasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_ClientResult_PropertySettings_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@CRPS_PkeyID", 1 + "#bigint#" + model.CRPS_PkeyID);
                input_parameters.Add("@CRPS_WO_ID", 1 + "#bigint#" + model.PS_WO_ID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserID);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }
        public List<dynamic> GetClientResultPropertySettingsData(ClientResult_PropertySettings_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetClientResult_PropertySettings(model);

                #region comment

                //        var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                //        List < ClientResult_PropertySettings_MasterDTO > piData =
                //        (from item in myEnumerableFeaprd
                //                    select new ClientResult_PropertySettings_MasterDTO
                //                    {
                // CRPS_PkeyID = item.Field<long>("CRPS_PkeyID"),
                //CRPS_Client = item.Field<long?>("CRPS_Client"),
                //CRPS_Customer = item.Field<long?>("CRPS_Customer"),
                //CRPS_Property_Id = item.Field<string>("CRPS_Property_Id"),
                //CRPS_HOA_Name = item.Field<string>("CRPS_HOA_Name"),
                //CRPS_HOA_Identifier = item.Field<string>("CRPS_HOA_Identifier"),
                //CRPS_HOA_PhoneNo = item.Field<string>("CRPS_HOA_PhoneNo"),
                //CRPS_Tax_Parcel_Number = item.Field<string>("CRPS_Tax_Parcel_Number"),
                //CRPS_Vacant_Land_Identifier = item.Field<string>("CRPS_Vacant_Land_Identifier"),
                //CRPS_Recurring_Grass_Cuts = item.Field<Boolean?>("CRPS_Recurring_Grass_Cuts"),
                //CRPS_Pool_On_Site = item.Field<Boolean?>("CRPS_Pool_On_Site"),
                //CRPS_Sump_Pump_On_Site = item.Field<Boolean?>("CRPS_Sump_Pump_On_Site"),
                //CRPS_Sump_Pump_Operational = item.Field<Boolean?>("CRPS_Sump_Pump_Operational"),
                //CRPS_AssetID = item.Field<string>("CRPS_AssetID"),
                //CRPS_Inspection_Required = item.Field<Boolean?>("CRPS_Inspection_Required"),
                //CRPS_VPR_Required = item.Field<Boolean?>("CRPS_VPR_Required"),
                //CRPS_Property_Type = item.Field<Int64?>("CRPS_Property_Type"),
                //CRPS_Environmental_Flag = item.Field<Boolean?>("CRPS_Environmental_Flag"),
                //                    }).ToList();

                //        objDynamic.Add(piData);
                #endregion

                if (ds.Tables.Count > 0)
                {
                    for (int i = 0; i < ds.Tables.Count; i++)
                    {
                        objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                    }

                }

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        public List<dynamic> CreateUpdate_ClientResult_PropertySettings_Master(ClientResult_PropertySettings_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objclntData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_ClientResult_PropertySettings_Master]";
            ClientResult_PropertySetting_Master clientResult_PropertySettings_Master = new ClientResult_PropertySetting_Master();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@CRPS_IsActive", 1 + "#bit#" + model.PS_IsActive);
                input_parameters.Add("@CRPS_IsDelete", 1 + "#bit#" + model.CRPS_IsDelete);

                input_parameters.Add("@CRPS_PkeyID", 1 + "#bigint#" + model.CRPS_PkeyID);
                input_parameters.Add("@CRPS_Property_Type", 1 + "#bigint#" + model.CRPS_Property_Type);
                input_parameters.Add("@CRPS_WO_ID", 1 + "#bigint#" + model.PS_WO_ID);
                input_parameters.Add("@CRPS_Client", 1 + "#bigint#" + model.CRPS_Client);
                input_parameters.Add("@CRPS_Environmental_Flag", 1 + "#bit#" + model.CRPS_Environmental_Flag);

                input_parameters.Add("@CRPS_Customer", 1 + "#bigint#" + model.CRPS_Customer);
                input_parameters.Add("@CRPS_HOA_Name", 1 + "#nvarchar#" + model.CRPS_HOA_Name);
                input_parameters.Add("@CRPS_AssetID", 1 + "#nvarchar#" + model.CRPS_AssetID);
                input_parameters.Add("@CRPS_HOA_Identifier", 1 + "#nvarchar#" + model.CRPS_HOA_Identifier);
                input_parameters.Add("@CRPS_HOA_PhoneNo", 1 + "#nvarchar#" + model.CRPS_HOA_PhoneNo);
                input_parameters.Add("@CRPS_Tax_Parcel_Number", 1 + "#nvarchar#" + model.CRPS_Tax_Parcel_Number);
                input_parameters.Add("@CRPS_Vacant_Land_Identifier", 1 + "#nvarchar#" + model.CRPS_Vacant_Land_Identifier);
                input_parameters.Add("@CRPS_Property_Id", 1 + "#nvarchar#" + model.CRPS_Property_Id);
                input_parameters.Add("@CRPS_Inspection_Required", 1 + "#bit#" + model.CRPS_Inspection_Required);
                input_parameters.Add("@CRPS_VPR_Required", 1 + "#bit#" + model.CRPS_VPR_Required);
                input_parameters.Add("@CRPS_Recurring_Grass_Cuts", 1 + "#bit#" + model.CRPS_Recurring_Grass_Cuts);
                input_parameters.Add("@CRPS_Pool_On_Site", 1 + "#bit#" + model.CRPS_Pool_On_Site);
                input_parameters.Add("@CRPS_Sump_Pump_On_Site", 1 + "#bit#" + model.CRPS_Sump_Pump_On_Site);
                input_parameters.Add("@CRPS_Sump_Pump_Operational", 1 + "#bit#" + model.CRPS_Sump_Pump_Operational);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#bigint#" + model.Type);
                input_parameters.Add("@CRPS_PkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    clientResult_PropertySettings_Master.CRPS_PkeyID = "0";
                    clientResult_PropertySettings_Master.Status = "0";
                    clientResult_PropertySettings_Master.ErrorMessage = "Error while Saviving in the Database";
                }
                else
                {
                    clientResult_PropertySettings_Master.CRPS_PkeyID = Convert.ToString(objData[0]);
                    clientResult_PropertySettings_Master.Status = Convert.ToString(objData[1]);
                }
                objclntData.Add(clientResult_PropertySettings_Master);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objclntData;
        }


        public List<dynamic> AddClientResult_PropertySettingsData(ClientResult_PropertySettings_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                objData = CreateUpdate_ClientResult_PropertySettings_Master(model);

            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }
    }
}