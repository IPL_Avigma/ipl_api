﻿using Avigma.Repository.Lib;
using IPL.Models;
using IPLApp.Models;
using IPLApp.Repository.data;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace IPL.Repository.data
{
    public class DashBoardData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();


        private DataSet GetDashBoardClientDetails(DashBordDataDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[GetAuthDashBoardClientDetails]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Company", 1 + "#bigint#" + model.Company);
                input_parameters.Add("@whereClause", 1 + "#nvarchar#" + model.whereClause);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ds;
        }



        public List<dynamic> GetDashBoardClientDetailsData(DashBordDataDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                string wherecondition = string.Empty;

                DataSet ds = GetDashBoardClientDetails(model);
                if (ds != null && ds.Tables.Count > 0)
                {
                    for (int i = 0; i < ds.Tables.Count; i++)
                    {
                        objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                    }

                
                    var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                    List<DashBordDataDTO> ReportDetails =
                       (from item in myEnumerableFeaprd
                        select new DashBordDataDTO
                        {
                                Client_InvoiceTotal = item.Field<Decimal?>("Client_InvoiceTotal"),
                                

                                Client_Pay_Amount = item.Field<Decimal?>("Client_Pay_Amount"),

                                //Con_InvoiceTotal = item.Field<Decimal?>("Con_InvoiceTotal"),
                               

                            }).ToList();


                    objDynamic.Add(ReportDetails);
                    var totalAmount = ReportDetails.Sum(r => r.Client_InvoiceTotal);
                    var paymentAmount = ReportDetails.Sum(r => r.Client_Pay_Amount);
                    Decimal? BalanceAmount = 0;
                    objDynamic.Add(totalAmount);
                    objDynamic.Add(paymentAmount);
                    if (totalAmount != null && totalAmount != 0 && ReportDetails.Count != 0)
                    {
                        var avgAmount = totalAmount / ReportDetails.Count;
                        objDynamic.Add(avgAmount);
                        if (paymentAmount != null && paymentAmount != 0 && ReportDetails.Count != 0)
                        {
                            BalanceAmount = totalAmount - paymentAmount;
                        }
                        else
                        {
                            BalanceAmount = totalAmount;
                        }
                        objDynamic.Add(BalanceAmount);
                    }
                    else
                    {
                        var avgAmount = 0;
                        objDynamic.Add(avgAmount);
                        objDynamic.Add(BalanceAmount);
                    }


                }

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

            }

            return objDynamic;
        }
    }
}