﻿using Avigma.Repository.Lib;
using IPL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class IPL_Company_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        public List<dynamic> Add_IPL_Company_Data(IPL_Company_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_IPL_Company_Master]";
            IPL_Company_Master iPL_Company_Master = new IPL_Company_Master();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@IPL_Company_PkeyId", 1 + "#bigint#" + model.IPL_Company_PkeyId);
                input_parameters.Add("@IPL_Company_Name", 1 + "#nvarchar#" + model.IPL_Company_Name);
                input_parameters.Add("@IPL_Company_Address", 1 + "#nvarchar#" + model.IPL_Company_Address);
                input_parameters.Add("@IPL_Company_Mobile", 1 + "#nvarchar#" + model.IPL_Company_Mobile);
                input_parameters.Add("@IPL_Company_City", 1 + "#nvarchar#" + model.IPL_Company_City);
                input_parameters.Add("@IPL_Company_State", 1 + "#nvarchar#" + model.IPL_Company_State);
                input_parameters.Add("@IPL_Company_PinCode", 1 + "#bigint#" + model.IPL_Company_PinCode);
                input_parameters.Add("@IPL_Company_Subscribe_Amount", 1 + "#decimal#" + model.IPL_Company_Subscribe_Amount);
                input_parameters.Add("@IPL_Company_Subscribe_date", 1 + "#datetime#" + model.IPL_Company_Subscribe_date);
                input_parameters.Add("@IPL_Company_Subscribe_Type", 1 + "#varchar#" + model.IPL_Company_Subscribe_Type);
                input_parameters.Add("@IPL_Company_Subscribe_Valid", 1 + "#varchar#" + model.IPL_Company_Subscribe_Valid);
                input_parameters.Add("@IPL_Company_Subscribe_Users", 1 + "#varchar#" + model.IPL_Company_Subscribe_Users);
                input_parameters.Add("@IPL_Company_Subscribe_User_Count", 1 + "#int#" + model.IPL_Company_Subscribe_User_Count);
                input_parameters.Add("@IPL_Company_IsActive", 1 + "#bit#" + model.IPL_Company_IsActive);
                input_parameters.Add("@IPL_Company_IsDelete", 1 + "#bit#" + model.IPL_Company_IsDelete);
                input_parameters.Add("@IPL_Company_County", 1 + "#varchar#" + model.IPL_Company_County);
                input_parameters.Add("@IPL_Contact_Name", 1 + "#varchar#" + model.IPL_Contact_Name);
                input_parameters.Add("@IPL_Company_Email", 1 + "#varchar#" + model.IPL_Company_Email);
                input_parameters.Add("@IPL_Company_Contact_Email", 1 + "#varchar#" + model.IPL_Company_Contact_Email);
                input_parameters.Add("@IPL_Company_Contact_Mobile", 1 + "#varchar#" + model.IPL_Company_Contact_Mobile);
                input_parameters.Add("@IPL_Company_Phone", 1 + "#varchar#" + model.IPL_Company_Phone);
                input_parameters.Add("@IPL_Company_Company_Link", 1 + "#varchar#" + model.IPL_Company_Company_Link);
                input_parameters.Add("@IPL_Company_ID", 1 + "#varchar#" + model.IPL_Company_ID);


                input_parameters.Add("@User_FirstName", 1 + "#nvarchar#" + model.User_FirstName);
                input_parameters.Add("@User_LastName", 1 + "#nvarchar#" + model.User_LastName);
                input_parameters.Add("@User_LoginName", 1 + "#nvarchar#" + model.User_LoginName);
                input_parameters.Add("@User_Password", 1 + "#nvarchar#" + model.User_Password);


                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@IPL_Company_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    iPL_Company_Master.IPL_Company_PkeyId = "0";
                    iPL_Company_Master.Status = "0";
                    iPL_Company_Master.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    iPL_Company_Master.IPL_Company_PkeyId = Convert.ToString(objData[0]);
                    iPL_Company_Master.Status = Convert.ToString(objData[1]);


                }
                objcltData.Add(iPL_Company_Master);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;



        }

        private DataSet Get_IPL_Company_Master(IPL_Company_MasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_IPL_Company_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@IPL_Company_PkeyId", 1 + "#bigint#" + model.IPL_Company_PkeyId);

                input_parameters.Add("@WhereClause", 1 + "#nvarchar#" + model.WhereClause);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@PageNumber", 1 + "#int#" + model.PageNumber);
                input_parameters.Add("@Rowcount", 1 + "#int#" + model.Rowcount);

                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }
        //Get App Company Details 
        public List<dynamic> Get_IPL_Company_Master_Details(IPL_Company_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                string wherecondition = string.Empty;
                if (model.Type == 3 && !string.IsNullOrEmpty(model.FilterData))
                {
                    var Data = JsonConvert.DeserializeObject<NewIPLCompanyMaster_Filter>(model.FilterData);
                    if (!string.IsNullOrEmpty(Data.IPL_Company_Name))
                    {
                        wherecondition = " And sub.IPL_Company_Name LIKE '%" + Data.IPL_Company_Name + "%'";
                    }
                    if (!string.IsNullOrEmpty(Data.IPL_Company_Address))
                    {
                        wherecondition = " And sub.IPL_Company_Address LIKE '%" + Data.IPL_Company_Address + "%'";
                    }
                    if (!string.IsNullOrEmpty(Data.IPL_Company_State))
                    {
                        wherecondition = " And sub.IPL_Company_State LIKE '%" + Data.IPL_Company_State + "%'";
                    }

                    model.WhereClause = wherecondition;
                }

                DataSet ds = Get_IPL_Company_Master(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<IPL_Company_MasterDTO> iplcompany =
                   (from item in myEnumerableFeaprd
                    select new IPL_Company_MasterDTO
                    {
                        IPL_Company_PkeyId = item.Field<Int64>("IPL_Company_PkeyId"),
                        IPL_Company_Name = item.Field<String>("IPL_Company_Name"),
                        IPL_Company_Address = item.Field<String>("IPL_Company_Address"),
                        IPL_Company_Mobile = item.Field<String>("IPL_Company_Mobile"),
                        IPL_Company_City = item.Field<String>("IPL_Company_City"),
                        IPL_Company_State = item.Field<String>("IPL_Company_State"),
                        IPL_Company_PinCode = item.Field<Int64?>("IPL_Company_PinCode"),
                        IPL_Company_Subscribe_Amount = item.Field<Decimal?>("IPL_Company_Subscribe_Amount"),
                        IPL_Company_Subscribe_date = item.Field<DateTime?>("IPL_Company_Subscribe_date"),
                        IPL_Company_Subscribe_Type = item.Field<String>("IPL_Company_Subscribe_Type"),
                        IPL_Company_Subscribe_Valid = item.Field<String>("IPL_Company_Subscribe_Valid"),
                        IPL_Company_Subscribe_Users = item.Field<String>("IPL_Company_Subscribe_Users"),
                        IPL_Company_Subscribe_User_Count = item.Field<int?>("IPL_Company_Subscribe_User_Count"),
                        IPL_Company_IsActive = item.Field<Boolean?>("IPL_Company_IsActive"),
                        IPL_Company_County = item.Field<String>("IPL_Company_County"),
                        IPL_Contact_Name = item.Field<String>("IPL_Contact_Name"),
                        IPL_Company_Contact_Email = item.Field<String>("IPL_Company_Contact_Email"),
                        IPL_Company_Email = item.Field<String>("IPL_Company_Email"),
                        IPL_Company_Contact_Mobile = item.Field<String>("IPL_Company_Contact_Mobile"),
                        IPL_Company_Phone = item.Field<String>("IPL_Company_Phone"),
                        IPL_Company_Company_Link = item.Field<String>("IPL_Company_Company_Link"),
                        IPL_Company_ID = item.Field<String>("IPL_Company_ID"),


                    }).ToList();

                objDynamic.Add(iplcompany);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }


    }
}