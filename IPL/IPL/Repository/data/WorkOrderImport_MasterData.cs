﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using IPLApp.Models;
using Avigma.Repository.Scheduler;
using Newtonsoft.Json;

namespace IPLApp.Repository.data
{
    public class WorkOrderImport_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public List<dynamic> WorkOrderImportMasterDetails(WorkOrderImport_MasterData_DTO WorkOrderImport_MasterData_DTO)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            List<dynamic> objclntData = new List<dynamic>();
            try
            {
                string insertProcedure = "[CreateUpdateworkorderimportmasterdata]";
                WorkOrderImport workOrderImport = new WorkOrderImport();
                Dictionary<string, string> inputParameter = new Dictionary<string, string>();
                inputParameter.Add("@WI_Pkey_ID", 1 + "#bigint#" + WorkOrderImport_MasterData_DTO.WI_Pkey_ID);
                inputParameter.Add("@WI_ImportFrom", 1 + "#bigint#" + WorkOrderImport_MasterData_DTO.WI_ImportFrom);
                inputParameter.Add("@WI_SetClientCompany", 1 + "#bigint#" + WorkOrderImport_MasterData_DTO.WI_SetClientCompany);
                inputParameter.Add("@WI_LoginName", 1 + "#varchar#" + WorkOrderImport_MasterData_DTO.WI_LoginName);
                inputParameter.Add("@WI_Password", 1 + "#varchar#" + WorkOrderImport_MasterData_DTO.WI_Password);
                inputParameter.Add("@WI_AlertEmail", 1 + "#varchar#" + WorkOrderImport_MasterData_DTO.WI_AlertEmail);
                inputParameter.Add("@WI_FriendlyName", 1 + "#varchar#" + WorkOrderImport_MasterData_DTO.WI_FriendlyName);
                inputParameter.Add("@WI_SkipComments", 1 + "#bit#" + WorkOrderImport_MasterData_DTO.WI_SkipComments);
                inputParameter.Add("@WI_SkipLineItems", 1 + "#bit#" + WorkOrderImport_MasterData_DTO.WI_SkipLineItems);
                inputParameter.Add("@WI_SetCategory", 1 + "#bigint#" + WorkOrderImport_MasterData_DTO.WI_SetCategory);
                inputParameter.Add("@WI_StateFilter", 1 + "#bigint#" + WorkOrderImport_MasterData_DTO.WI_StateFilter);
                inputParameter.Add("@WI_Discount_Import", 1 + "#decimal#" + WorkOrderImport_MasterData_DTO.WI_Discount_Import);
                inputParameter.Add("@WI_ImageDownload", 1 + "#bit#" + WorkOrderImport_MasterData_DTO.WI_ImageDownload);
                inputParameter.Add("@WI_Processor", 1 + "#bigint#" + WorkOrderImport_MasterData_DTO.WI_Processor);
                inputParameter.Add("@WI_Coordinator", 1 + "#bigint#" + WorkOrderImport_MasterData_DTO.WI_Coordinator);
                inputParameter.Add("@WI_IsActive", 1 + "#bit#" + WorkOrderImport_MasterData_DTO.WI_IsActive);
                inputParameter.Add("@WI_IsDeleted", 1 + "#bit#" + WorkOrderImport_MasterData_DTO.WI_IsDeleted);
                inputParameter.Add("@WI_FB_LoginName", 1 + "#varchar#" + WorkOrderImport_MasterData_DTO.WI_FB_LoginName);
                inputParameter.Add("@WI_FB_Password", 1 + "#varchar#" + WorkOrderImport_MasterData_DTO.WI_FB_Password);
                inputParameter.Add("@WI_Res_Code", 1 + "#varchar#" + WorkOrderImport_MasterData_DTO.WI_Res_Code);
                inputParameter.Add("@WI_Changed_Order_Alert", 1 + "#varchar#" + WorkOrderImport_MasterData_DTO.WI_Changed_Order_Alert);
                inputParameter.Add("@WI_Cancelled_Order_Alert", 1 + "#varchar#" + WorkOrderImport_MasterData_DTO.WI_Cancelled_Order_Alert);


                inputParameter.Add("@UserId", 1 + "#bigint#" + WorkOrderImport_MasterData_DTO.UserId);
                inputParameter.Add("@Type", 1 + "#int#" + WorkOrderImport_MasterData_DTO.Type);
                inputParameter.Add("@WI_Pkey_IDOut", 2 + "#bigint#" + null);
                inputParameter.Add("@ReturnValue", 2 + "#int#" + null);

                objDynamic = obj.SqlCRUD(insertProcedure, inputParameter);
                if (objDynamic[1] == 0)
                {
                    workOrderImport.WI_Pkey_ID = "0";
                    workOrderImport.Status = "0";
                    workOrderImport.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    workOrderImport.WI_Pkey_ID = Convert.ToString(objDynamic[0]);
                    workOrderImport.Status = Convert.ToString(objDynamic[1]);

                    // GetWorkOrders getWorkOrders = new GetWorkOrders();
                    LoginDTO loginDTO = new LoginDTO();

                    string ppwusername = WorkOrderImport_MasterData_DTO.WI_LoginName;
                    //  loginDTO.password = WorkOrderImport_MasterData_DTO.WI_Password;

                    //   getWorkOrders.GetPPWWorkOdersDetails(loginDTO);


                    //CheckJob checkJob = new CheckJob();
                    //List<dynamic> objjobData = new List<dynamic>();
                    //objjobData =  checkJob.getPrintJobs();
                    Avigma.Repository.Scheduler.JobScheduler.Start(ppwusername);

                }
                objclntData.Add(workOrderImport);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objclntData;

        }
        private DataSet GetWorkOrderImportData(WorkOrderImport_MasterData_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Getworkorderimportmasterdata]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@WI_Pkey_ID", 1 + "#bigint#" + model.WI_Pkey_ID);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserId);
                input_parameters.Add("@WhereClause", 1 + "#varchar#" + model.WhereClause);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }
        public List<dynamic> GetWorkOrderImportDetails(WorkOrderImport_MasterData_DTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetWorkOrderImportData(model);
                if (ds.Tables.Count > 0)
                {
                    var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                    List<WorkOrderImport_MasterData_DTO> StudentDocumentData =
                       (from item in myEnumerableFeaprd
                        select new WorkOrderImport_MasterData_DTO
                        {
                            WI_Pkey_ID = item.Field<Int64>("WI_Pkey_ID"),
                            WI_ImportFrom = item.Field<Int64?>("WI_ImportFrom"),
                            WI_SetClientCompany = item.Field<Int64?>("WI_SetClientCompany"),
                            WI_LoginName = item.Field<string>("WI_LoginName"),

                            WI_Password = item.Field<string>("WI_Password"),
                            WI_AlertEmail = item.Field<string>("WI_AlertEmail"),
                            WI_FriendlyName = item.Field<string>("WI_FriendlyName"),
                            WI_SkipCommentsDec = item.Field<string>("WI_SkipCommentsDec"),
                            WI_SkipComments = item.Field<Boolean?>("WI_SkipComments"),
                            WI_SkipLineItemsDec = item.Field<string>("WI_SkipLineItemsDec"),

                            WI_SkipLineItems = item.Field<Boolean?>("WI_SkipLineItems"),
                            WI_SetCategory = item.Field<Int64?>("WI_SetCategory"),
                            WI_StateFilter = item.Field<Int64?>("WI_StateFilter"),
                            WI_Discount_Import = item.Field<decimal?>("WI_Discount_Import"),
                            WI_IsActive = item.Field<Boolean?>("WI_IsActive"),
                            Client_Company_Name = item.Field<string>("Client_Company_Name"),
                            Main_Cat_Name = item.Field<string>("Main_Cat_Name"),
                            Import_Form_Name = item.Field<string>("Import_Form_Name"),
                            Import_Form_URL_Name = item.Field<string>("Import_Form_URL_Name"),
                            WI_ImageDownload = item.Field<Boolean?>("WI_ImageDownload"),
                            WI_Processor = item.Field<Int64?>("WI_Processor"),
                            WI_Coordinator = item.Field<Int64?>("WI_Coordinator"),
                            WI_FB_LoginName = item.Field<string>("WI_FB_LoginName"),
                            WI_FB_Password = item.Field<string>("WI_FB_Password"),
                            Import_BucketName = item.Field<string>("Import_BucketName"),
                            Import_BucketFolderName = item.Field<string>("Import_BucketFolderName"),
                            Import_DestBucketFolderName = item.Field<string>("Import_DestBucketFolderName"),
                            WI_Res_Code = item.Field<string>("WI_Res_Code"),
                            WI_Changed_Order_Alert = item.Field<string>("WI_Changed_Order_Alert"),
                            WI_Cancelled_Order_Alert = item.Field<string>("WI_Cancelled_Order_Alert"),
                            WI_Createdby = item.Field<string>("WI_Createdby"),
                            WI_Modifiedby = item.Field<string>("WI_Modifiedby"),
                            ViewUrl = null


                        }).ToList();
                    objDynamic.Add(StudentDocumentData);
                }

                if (model.Type == 1)
                {
                    if (ds.Tables.Count > 1)
                    {
                        var myEnumerableimp = ds.Tables[1].AsEnumerable();
                        List<Filter_Admin_Import_MasterDTO> imp_List =
                           (from item in myEnumerableimp
                            select new Filter_Admin_Import_MasterDTO
                            {
                                Import_Filter_PkeyID = item.Field<Int64>("Import_Filter_PkeyID"),
                                Import_Filter_ImpFromID = item.Field<String>("Import_Filter_ImpFromID"),
                                Import_Filter_ImpName = item.Field<String>("Import_Filter_ImpName"),
                                Import_Filter_ClientName = item.Field<String>("Import_Filter_ClientName"),
                                Import_Filter_LoginName = item.Field<String>("Import_Filter_LoginName"),
                                Import_Filter_ImpIsActive = item.Field<Boolean?>("Import_Filter_ImpIsActive"),
                                Import_Filter_CreatedBy = item.Field<String>("Import_Filter_CreatedBy"),
                                Import_Filter_ModifiedBy = item.Field<String>("Import_Filter_ModifiedBy"),
                            }).ToList();

                        objDynamic.Add(imp_List);
                    }
                }

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }


        public List<dynamic> GetWorkOrderImportDetailsForScheduler(WorkOrderImport_MasterData_DTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetWorkOrderImportData(model);
                if (ds.Tables.Count > 0)
                {
                    var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                    List<WorkOrderImport_MasterData_DTO> WorkOrderImportMasterData =
                       (from item in myEnumerableFeaprd
                        select new WorkOrderImport_MasterData_DTO
                        {
                            WI_Pkey_ID = item.Field<Int64>("WI_Pkey_ID"),
                            WI_ImportFrom = item.Field<Int64?>("WI_ImportFrom"),
                            WI_LoginName = item.Field<string>("WI_LoginName"),
                            WI_Password = item.Field<string>("WI_Password"),
                            WI_ImageDownload = item.Field<Boolean?>("WI_ImageDownload"),
                            WI_FB_LoginName = item.Field<string>("WI_FB_LoginName"),
                            WI_FB_Password = item.Field<string>("WI_FB_Password"),
                            Import_Form_URL_Name = item.Field<string>("Import_Form_URL_Name"),
                            WI_Res_Code = item.Field<String>("WI_Res_Code"),
                            Import_BucketName = item.Field<string>("Import_BucketName"),
                            Import_BucketFolderName = item.Field<string>("Import_BucketFolderName"),
                            Import_DestBucketFolderName = item.Field<string>("Import_DestBucketFolderName"),


                        }).ToList();
                    objDynamic.Add(WorkOrderImportMasterData);
                }

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }


        public List<dynamic> GetDamageFilterDetails(WorkOrderImport_MasterData_DTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();


            string wherecondition = string.Empty;
            try
            {

                var Data = JsonConvert.DeserializeObject<WorkOrderImport_MasterData_DTO>(model.FilterData);
                if (!string.IsNullOrEmpty(Data.Import_Form_Name) && Data.Import_Form_Name != "Select")
                {
                    // wherecondition = " And Import_Form_Name =    '" + Data.Import_Form_Name + "'";
                    wherecondition = wherecondition + " And Import_Form_Name LIKE '%" + Data.Import_Form_Name + "%'";
                }
                if (!string.IsNullOrEmpty(Data.WI_LoginName))
                {
                    //wherecondition = " And WI_LoginName =    '" + Data.WI_LoginName + "'";
                    wherecondition = wherecondition + " And WI_LoginName LIKE '%" + Data.WI_LoginName + "%'";
                }
                if (!string.IsNullOrEmpty(Data.Client_Company_Name))
                {
                    //  wherecondition = " And Client_Company_Name =    '" + Data.Client_Company_Name + "'";
                    wherecondition = wherecondition + " And Client_Company_Name LIKE '%" + Data.Client_Company_Name + "%'";
                }
                if (!string.IsNullOrEmpty(Data.WI_FriendlyName))
                {
                    //wherecondition = " And WI_FriendlyName =    '" + Data.WI_FriendlyName + "'";
                    wherecondition = wherecondition + " And WI_FriendlyName LIKE '%" + Data.WI_FriendlyName + "%'";
                }

                if (Data.WI_IsActive == true)
                {
                    wherecondition = wherecondition + "  And WI_IsActive =  '1'";
                }
                if (Data.WI_IsActive == false)
                {
                    wherecondition = wherecondition + "  And WI_IsActive =  '0'";
                }


                WorkOrderImport_MasterData_DTO workOrderImport_MasterData_DTO = new WorkOrderImport_MasterData_DTO();

                workOrderImport_MasterData_DTO.WhereClause = wherecondition;
                workOrderImport_MasterData_DTO.Type = 4;
                workOrderImport_MasterData_DTO.UserId = model.UserId;

                DataSet ds = GetWorkOrderImportData(workOrderImport_MasterData_DTO);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<WorkOrderImport_MasterData_DTO> autofilter =
                   (from item in myEnumerableFeaprd
                    select new WorkOrderImport_MasterData_DTO
                    {
                        WI_Pkey_ID = item.Field<Int64>("WI_Pkey_ID"),
                        WI_ImportFrom = item.Field<Int64?>("WI_ImportFrom"),
                        WI_SetClientCompany = item.Field<Int64?>("WI_SetClientCompany"),
                        WI_LoginName = item.Field<string>("WI_LoginName"),

                        WI_Password = item.Field<string>("WI_Password"),
                        WI_AlertEmail = item.Field<string>("WI_AlertEmail"),
                        WI_FriendlyName = item.Field<string>("WI_FriendlyName"),
                        WI_SkipCommentsDec = item.Field<string>("WI_SkipCommentsDec"),
                        WI_SkipComments = item.Field<Boolean?>("WI_SkipComments"),
                        WI_SkipLineItemsDec = item.Field<string>("WI_SkipLineItemsDec"),

                        WI_SkipLineItems = item.Field<Boolean?>("WI_SkipLineItems"),
                        WI_SetCategory = item.Field<Int64?>("WI_SetCategory"),
                        WI_StateFilter = item.Field<Int64?>("WI_StateFilter"),
                        WI_Discount_Import = item.Field<decimal?>("WI_Discount_Import"),
                        WI_IsActive = item.Field<Boolean?>("WI_IsActive"),
                        Client_Company_Name = item.Field<string>("Client_Company_Name"),
                        Main_Cat_Name = item.Field<string>("Main_Cat_Name"),
                        Import_Form_Name = item.Field<string>("Import_Form_Name"),
                        Import_Form_URL_Name = item.Field<string>("Import_Form_URL_Name"),
                        WI_ImageDownload = item.Field<Boolean?>("WI_ImageDownload"),
                        WI_Processor = item.Field<Int64?>("WI_Processor"),
                        WI_Coordinator = item.Field<Int64?>("WI_Coordinator"),
                        WI_FB_LoginName = item.Field<string>("WI_FB_LoginName"),
                        WI_FB_Password = item.Field<string>("WI_FB_Password"),
                        WI_Res_Code = item.Field<string>("WI_Res_Code"),
                        WI_Changed_Order_Alert = item.Field<string>("WI_Changed_Order_Alert"),
                        WI_Cancelled_Order_Alert = item.Field<string>("WI_Cancelled_Order_Alert"),

                        WI_Createdby = item.Field<string>("WI_Createdby"),
                        WI_Modifiedby = item.Field<string>("WI_Modifiedby"),

                    }).ToList();

                objDynamic.Add(autofilter);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }

            return objDynamic;
        }
    }
}