﻿using Avigma.Repository.Lib;
using Firebase.Database;
using IPL.Models;
using IPLApp.Repository.data;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace IPL.Repository.data
{
    public class ClientResult_Photos_Download_Notification_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        WorkOrderMasterData workOrderMasterData = new WorkOrderMasterData();

        public List<dynamic> AddDownloadPhotosNotificationMaster(ClientResult_Photos_Download_Notification_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_Client_Result_Photos_Notification_Master]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@PN_Pkey_Id", 1 + "#bigint#" + model.PN_Pkey_Id);
                input_parameters.Add("@PN_IsRead", 1 + "#bit#" + model.PN_IsRead);
                input_parameters.Add("@PN_IsDelete", 1 + "#bit#" + model.PN_IsDelete);
                input_parameters.Add("@PN_Title", 1 + "#varchar#" + model.PN_Title);
                input_parameters.Add("@PN_Message", 1 + "#varchar#" + model.PN_Message);
                input_parameters.Add("@PN_DownloadLink", 1 + "#varchar#" + model.PN_DownloadLink);

                input_parameters.Add("@PN_WoId", 1 + "#bigint#" + model.PN_WoId);

                input_parameters.Add("@PN_IsActive", 1 + "#bit#" + model.PN_IsActive);
                input_parameters.Add("@PN_ExpireOn", 1 + "#datetime#" + DateTime.Now.AddHours(2));

                input_parameters.Add("@PN_DN_Type", 1 + "#int#" + model.PN_DN_Type);

                input_parameters.Add("@UserID", 1 + "#bigint#" + model.PN_UserId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                input_parameters.Add("@PN_Pkey_Id_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        public DataSet GetDownloadPhotosNotificationMaster(ClientResult_Photos_Download_Notification_MasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_ClientResult_Photo_Download_Notification_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@UserID", 1 + "#bigint#" + model.PN_UserId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ds;
        }


        public List<dynamic> GetDownloadPhotosNotificationDetails(ClientResult_Photos_Download_Notification_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetDownloadPhotosNotificationMaster(model);


                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<ClientResult_Photos_Download_Notification_MasterDTO> FileDetails =
                   (from item in myEnumerableFeaprd
                    select new ClientResult_Photos_Download_Notification_MasterDTO
                    {
                        PN_Pkey_Id = item.Field<Int64>("PN_Pkey_Id"),
                        PN_UserId = item.Field<Int64?>("PN_UserId"),
                        PN_Title = item.Field<String>("PN_Title"),
                        PN_Message = item.Field<String>("PN_Message"),
                        PN_IsRead = item.Field<Boolean?>("PN_IsRead"),
                        PN_IsActive = item.Field<Boolean?>("PN_IsActive"),
                        PN_DownloadLink = item.Field<String>("PN_DownloadLink"),
                        PN_ExpireOn = item.Field<DateTime>("PN_ExpireOn"),
                        PN_WoId = item.Field<Int64>("PN_WoId"),
                    }).ToList();

                objDynamic.Add(FileDetails);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        public async Task<FirebaseObject<string>> AddFirbase_DownloadNotification(ClientResult_Photos_Download_Notification_MasterDTO firebaseNotificationDTO)
        {
            try
            {
                if (firebaseNotificationDTO != null)
                {
                    UserDetailDTO userDetailDTO = new UserDetailDTO();
                    userDetailDTO.UserID = Convert.ToInt64(firebaseNotificationDTO.PN_UserId);
                    userDetailDTO.Type = 1;
                    var userDetail = workOrderMasterData.GetMessageUserDetail(userDetailDTO);
                    if (userDetail != null && userDetail.LoggedUserDetail != null && userDetail.LoggedUserDetail.Count > 0)
                    {
                        string firebaseUrl = System.Configuration.ConfigurationManager.AppSettings["FirebaseUrl"];
                        ClientResult_Photos_Download_FirebaseNotification_DTO clientResult_Photos_Download_FirebaseNotification_DTO = new ClientResult_Photos_Download_FirebaseNotification_DTO();
                        clientResult_Photos_Download_FirebaseNotification_DTO.PN_Title = firebaseNotificationDTO.PN_Title;
                        clientResult_Photos_Download_FirebaseNotification_DTO.PN_DownloadLink = firebaseNotificationDTO.PN_DownloadLink;
                        clientResult_Photos_Download_FirebaseNotification_DTO.PN_CreatedBy = firebaseNotificationDTO.PN_CreatedBy;
                        clientResult_Photos_Download_FirebaseNotification_DTO.PN_CreatedOn = DateTime.Now;
                        clientResult_Photos_Download_FirebaseNotification_DTO.PN_ExpireOn = DateTime.Now.AddHours(2);

                        string msg = JsonConvert.SerializeObject(clientResult_Photos_Download_FirebaseNotification_DTO);
                        var firebaseClient = new FirebaseClient(firebaseUrl);

                        var result = await firebaseClient
                            .Child("DownloadNotification/" + userDetail.LoggedUserDetail[0].IPL_Company_ID + "/" + userDetail.LoggedUserDetail[0].User_LoginName + "/")
                            .PostAsync(msg);

                        return result;
                    }
                    else
                    {
                        return null;
                    }
                }
                return null;
            }
            catch (Exception ex)
            {

                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return null;
            }


        }
    }
}