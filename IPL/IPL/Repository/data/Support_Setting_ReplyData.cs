﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class Support_Setting_ReplyData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        private List<dynamic> AddUpdateSupportTicketReplyData(Support_Setting_ReplyDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcustData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_Support_Setting_Reply_Master]";
            Support_Setting_Reply support_Setting_Reply = new Support_Setting_Reply();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Support_Rep_PkeyId", 1 + "#bigint#" + model.Support_Rep_PkeyId);
                input_parameters.Add("@Support_Rep_Support_Id", 1 + "#bigint#" + model.Support_Rep_Support_Id);
                input_parameters.Add("@Support_Rep_Reply", 1 + "#varchar#" + model.Support_Rep_Reply);
                input_parameters.Add("@Support_Rep_Status", 1 + "#int#" + model.Support_Rep_Status);
                input_parameters.Add("@Support_Rep_IsActie", 1 + "#bit#" + model.Support_Rep_IsActie);
                input_parameters.Add("@Support_Rep_IsDelete", 1 + "#bit#" + model.Support_Rep_IsDelete);
                input_parameters.Add("@Support_Rep_IsComment", 1 + "#bit#" + model.Support_Rep_IsComment);
                input_parameters.Add("@Support_Rep_CommentId", 1 + "#bigint#" + model.Support_Rep_CommentId);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Support_Rep_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    support_Setting_Reply.Support_Rep_PkeyId = "0";
                    support_Setting_Reply.Status = "0";
                    support_Setting_Reply.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    support_Setting_Reply.Support_Rep_PkeyId = Convert.ToString(objData[0]);
                    support_Setting_Reply.Status = Convert.ToString(objData[1]);


                }
                objcustData.Add(support_Setting_Reply);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcustData;



        }
        public List<dynamic> AddupdateSupportTicketReply(Support_Setting_ReplyDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                if (model.Support_Rep_Status != 0)
                {
                    Support_Ticket_Setting_MasterData support_Ticket_Setting_MasterData = new Support_Ticket_Setting_MasterData();
                    Support_Ticket_Setting_MasterDTO support_Ticket_Setting_MasterDTO = new Support_Ticket_Setting_MasterDTO();
                    support_Ticket_Setting_MasterDTO.Sup_Tickets_Pkey_ID = Convert.ToInt64(model.Support_Rep_Support_Id);
                    support_Ticket_Setting_MasterDTO.Sup_Tickets_Ticket_Status = model.Support_Rep_Status;
                    support_Ticket_Setting_MasterDTO.Type = 5;
                    objData = support_Ticket_Setting_MasterData.updateStatusDetails(support_Ticket_Setting_MasterDTO);

                }
                objData = AddUpdateSupportTicketReplyData(model);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        private DataSet Get_SupportTicket_Reply_Master(Support_Setting_ReplyDTO model)
        {
            DataSet ds = null;
            try

            {

                string selectProcedure = "[Get_Support_Setting_Reply_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Support_Rep_PkeyId", 1 + "#bigint#" + model.Support_Rep_PkeyId);
                input_parameters.Add("@Support_Rep_Support_Id", 1 + "#bigint#" + model.Support_Rep_Support_Id);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }
        public List<dynamic> GetSupportTicketDetails(Support_Setting_ReplyDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = Get_SupportTicket_Reply_Master(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Support_Setting_ReplyDTO> SupportTicket_Reply =
                   (from item in myEnumerableFeaprd
                    select new Support_Setting_ReplyDTO
                    {
                        Support_Rep_PkeyId = item.Field<Int64>("Support_Rep_PkeyId"),
                        Support_Rep_Support_Id = item.Field<Int64?>("Support_Rep_Support_Id"),
                        Support_Rep_Reply = item.Field<String>("Support_Rep_Reply"),
                        Support_Rep_Status = item.Field<int?>("Support_Rep_Status"),
                        Ipl_Ad_User_First_Name = item.Field<String>("Ipl_Ad_User_First_Name"),
                        Ipl_Ad_User_Last_Name = item.Field<String>("Ipl_Ad_User_Last_Name"),
                        Support_Rep_IsActie = item.Field<Boolean?>("Support_Rep_IsActie"),
                        Support_Rep_CreatedOn = item.Field<DateTime?>("Support_Rep_CreatedOn"),


                    }).ToList();

                objDynamic.Add(SupportTicket_Reply);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}