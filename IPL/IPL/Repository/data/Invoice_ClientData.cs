﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class Invoice_ClientData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public List<dynamic> AddInvoiceClientData(Invoice_ClientDTO model)
        {

            string insertProcedure = "[CreateUpdateInvoice_Client]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Inv_Client_pkeyId", 1 + "#bigint#" + model.Inv_Client_pkeyId);
                input_parameters.Add("@Inv_Client_Invoice_Id", 1 + "#bigint#" + model.Inv_Client_Invoice_Id);
                input_parameters.Add("@Inv_Client_WO_Id", 1 + "#bigint#" + model.Inv_Client_WO_Id);
                input_parameters.Add("@Inv_Client_Task_Id", 1 + "#bigint#" + model.Inv_Client_Task_Id);
                input_parameters.Add("@Inv_Client_Uom_Id", 1 + "#bigint#" + model.Inv_Client_Uom_Id);
                input_parameters.Add("@Inv_Client_Sub_Total", 1 + "#decimal#" + model.Inv_Client_Sub_Total);
                input_parameters.Add("@Inv_Client_Client_Dis", 1 + "#int#" + model.Inv_Client_Client_Dis);
                input_parameters.Add("@Inv_Client_Client_Total", 1 + "#decimal#" + model.Inv_Client_Client_Total);
                input_parameters.Add("@Inv_Client_Short_Note", 1 + "#varchar#" + model.Inv_Client_Short_Note);
                input_parameters.Add("@Inv_Client_Inv_Complete", 1 + "#bit#" + model.Inv_Client_Inv_Complete);
                input_parameters.Add("@Inv_Client_Credit_Memo", 1 + "#bit#" + model.Inv_Client_Credit_Memo);
                input_parameters.Add("@Inv_Client_Sent_Client", 1 + "#datetime#" + model.Inv_Client_Sent_Client);
                input_parameters.Add("@Inv_Client_Comp_Date", 1 + "#datetime#" + model.Inv_Client_Comp_Date);
                input_parameters.Add("@Inv_Client_Invoice_Number", 1 + "#varchar#" + model.Inv_Client_Invoice_Number);
                input_parameters.Add("@Inv_Client_Inv_Date", 1 + "#datetime#" + model.Inv_Client_Inv_Date);

                input_parameters.Add("@Inv_Client_Internal_Note", 1 + "#varchar#" + model.Inv_Client_Internal_Note);
                input_parameters.Add("@Inv_Client_Status", 1 + "#int#" + model.Inv_Client_Status);
                input_parameters.Add("@Inv_Client_Auto_Invoice", 1 + "#bit#" + model.Inv_Client_Auto_Invoice);
                input_parameters.Add("@Inv_Client_Discout_Amount", 1 + "#decimal#" + model.Inv_Client_Discout_Amount);
                input_parameters.Add("@Inv_Client_IsActive", 1 + "#bit#" + model.Inv_Client_IsActive);
                input_parameters.Add("@Inv_Client_IsDelete", 1 + "#bit#" + model.Inv_Client_IsDelete);
                input_parameters.Add("@Inv_Client_Followup", 1 + "#bit#" + model.Inv_Client_Followup);
                input_parameters.Add("@Inv_Client_Hold_Date", 1 + "#datetime#" + model.Inv_Client_Hold_Date);

                input_parameters.Add("@Inv_Client_IsNoCharge", 1 + "#bit#" + model.Inv_Client_IsNoCharge);
                input_parameters.Add("@Inv_Client_NoChargeDate", 1 + "#datetime#" + model.Inv_Client_NoChargeDate);

                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Inv_Client_pkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return obj.SqlCRUD(insertProcedure, input_parameters);



        }
        private DataSet GetInvoiceClientMaster(Invoice_ClientDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Invoice_Client]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Inv_Client_pkeyId", 1 + "#bigint#" + model.Inv_Client_pkeyId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetInvoiceClientDetails(Invoice_ClientDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetInvoiceClientMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Invoice_ClientDTO> InvoiceClient =
                   (from item in myEnumerableFeaprd
                    select new Invoice_ClientDTO
                    {
                        Inv_Client_pkeyId = item.Field<Int64>("Inv_Client_pkeyId"),
                        Inv_Client_Invoice_Id = item.Field<Int64?>("Inv_Client_Invoice_Id"),
                        Inv_Client_WO_Id = item.Field<Int64?>("Inv_Client_WO_Id"),
                        Inv_Client_Task_Id = item.Field<Int64?>("Inv_Client_Task_Id"),
                        Inv_Client_Uom_Id = item.Field<Int64?>("Inv_Client_Uom_Id"),
                        Inv_Client_Sub_Total = item.Field<Decimal?>("Inv_Client_Sub_Total"),
                        Inv_Client_Client_Dis = item.Field<int?>("Inv_Client_Client_Dis"),
                        Inv_Client_Client_Total = item.Field<Decimal?>("Inv_Client_Client_Total"),
                        Inv_Client_Short_Note = item.Field<String>("Inv_Client_Short_Note"),
                        Inv_Client_Inv_Complete = item.Field<Boolean?>("Inv_Client_Inv_Complete"),
                        Inv_Client_Credit_Memo = item.Field<Boolean?>("Inv_Client_Credit_Memo"),
                        Inv_Client_Sent_Client = item.Field<DateTime?>("Inv_Client_Sent_Client"),
                        Inv_Client_Comp_Date = item.Field<DateTime?>("Inv_Client_Comp_Date"),
                        Inv_Client_IsActive = item.Field<Boolean?>("Inv_Client_IsActive"),
                        Inv_Client_Status = item.Field<int?>("Inv_Client_Status"),
                        Inv_Client_Invoice_Number = item.Field<String>("Inv_Client_Invoice_Number"),
                        Inv_Client_Inv_Date = item.Field<DateTime>("Inv_Client_Inv_Date"),
                        Inv_Client_Internal_Note = item.Field<String>("Inv_Client_Internal_Note"),
                        Inv_Client_Followup = item.Field<Boolean?>("Inv_Client_Followup"),
                        Inv_Client_Hold_Date = item.Field<DateTime?>("Inv_Client_Hold_Date"),

                    }).ToList();

                objDynamic.Add(InvoiceClient);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }


        public List<dynamic> AddClientPayData(Client_Pay_DTO_List model)
        {
            List<dynamic> objAddData = new List<dynamic>();
            List<dynamic> objAddDataRet = new List<dynamic>();

           
            //Client_Pay_DTO_List client_Pay_DTO_List_Error = new Client_Pay_DTO_List();
            //Client_Pay_DTO_List client_Pay_DTO_List_Sucess = new Client_Pay_DTO_List();
            List<Client_Pay_DTO> Client_Pay_DTOs_sucess = new List<Client_Pay_DTO>();
            List<Client_Pay_DTO> Client_Pay_DTOs_error = new List<Client_Pay_DTO>();
            List<Client_Pay_DTO> Client_Pay_DTOs_nc = new List<Client_Pay_DTO>();

            for (int i = 0; i < model.Client_Pay_DTOs.Count; i++)
            {
                Client_Pay_DTO client_Pay_DTO = new Client_Pay_DTO();
                client_Pay_DTO.ICP_PkeyID = model.Client_Pay_DTOs[i].ICP_PkeyID;
                //client_Pay_DTO.Client_Pay_CheckNumber = model.Client_Pay_DTOs[i].Client_Pay_CheckNumber;
                //client_Pay_DTO.Client_Pay_Payment_Date = model.Client_Pay_DTOs[i].Client_Pay_Payment_Date;

                client_Pay_DTO.Client_Pay_CheckNumber = model.Client_Pay_DTOs[i].CheckNumber;
                client_Pay_DTO.Client_Pay_Payment_Date = model.Client_Pay_DTOs[i].CheckDate;
                client_Pay_DTO.Client_Pay_Invoice_Id = model.Client_Pay_DTOs[i].Client_Pay_Invoice_Id;
                client_Pay_DTO.Client_Pay_Amount = model.Client_Pay_DTOs[i].Client_Pay_Amount;
                client_Pay_DTO.Client_Pay_Comment = model.Client_Pay_DTOs[i].Client_Pay_Comment;

                client_Pay_DTO.Inv_Client_Inv_Date = model.Client_Pay_DTOs[i].Inv_Client_Inv_Date;
                client_Pay_DTO.IPLNO = model.Client_Pay_DTOs[i].IPLNO;
                client_Pay_DTO.workOrderNumber = model.Client_Pay_DTOs[i].workOrderNumber;
                client_Pay_DTO.Initial_Comments = model.Client_Pay_DTOs[i].Initial_Comments;
                client_Pay_DTO.Write_Off = model.Client_Pay_DTOs[i].Write_Off;

                //client_Pay_DTO.CheckNumber = model.Client_Pay_DTOs[i].CheckNumber;
                //client_Pay_DTO.CheckDate = model.Client_Pay_DTOs[i].CheckDate;

                client_Pay_DTO.ClientId = model.Client_Pay_DTOs[i].ClientId;

              



                client_Pay_DTO.ICP_IsActive = model.Client_Pay_DTOs[i].ICP_IsActive;
                client_Pay_DTO.ICP_IsDelete = model.Client_Pay_DTOs[i].ICP_IsDelete;

                client_Pay_DTO.UserID = model.UserID;
                client_Pay_DTO.Type = model.Type;

                objAddData = CreateUpdateClientPayData(client_Pay_DTO);
                var Returnval = objAddData[3];

                switch(Returnval)
                {
                    case 1:
                        {
                            client_Pay_DTO.workOrder_ID = objAddData[1];
                            client_Pay_DTO.Inv_Client_pkeyId = objAddData[2];
                            client_Pay_DTO.ICP_PkeyID = objAddData[0];
                            Client_Pay_DTOs_sucess.Add(client_Pay_DTO);
                            break;
                        }
                    case 2:
                        {
                            client_Pay_DTO.workOrder_ID = objAddData[1];
                            client_Pay_DTO.Inv_Client_pkeyId = objAddData[2];
                            client_Pay_DTO.ICP_PkeyID = objAddData[0];
                            Client_Pay_DTOs_error.Add(client_Pay_DTO);
                            break;
                        }
                    case 3:
                        {
                            client_Pay_DTO.workOrder_ID = objAddData[1];
                            client_Pay_DTO.Inv_Client_pkeyId = objAddData[2];
                            client_Pay_DTO.ICP_PkeyID = objAddData[0];
                            Client_Pay_DTOs_nc.Add(client_Pay_DTO);
                            break;
                        }
                }

                
                


            }
            objAddDataRet.Add(Client_Pay_DTOs_sucess);
            objAddDataRet.Add(Client_Pay_DTOs_error);
            objAddDataRet.Add(Client_Pay_DTOs_nc);
            return objAddDataRet; 
        }
        private List<dynamic> CreateUpdateClientPayData(Client_Pay_DTO model)
        {
            string insertProcedure = "[CreateUpdate_Client_Pay]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@ICP_PkeyID", 1 + "#bigint#" + model.ICP_PkeyID);
                input_parameters.Add("@Client_Pay_CheckNumber", 1 + "#varchar#" + model.Client_Pay_CheckNumber);
                input_parameters.Add("@Client_Pay_Payment_Date", 1 + "#datetime#" + model.Client_Pay_Payment_Date);
                input_parameters.Add("@Client_Pay_Invoice_Id", 1 + "#bigint#" + model.Client_Pay_Invoice_Id);
                input_parameters.Add("@Client_Pay_Amount", 1 + "#decimal#" + model.Client_Pay_Amount);
                input_parameters.Add("@Client_Pay_Comment", 1 + "#varchar#" + model.Client_Pay_Comment);


                input_parameters.Add("@Inv_Client_Inv_Date", 1 + "#datetime#" + model.Inv_Client_Inv_Date);
                input_parameters.Add("@IPLNO", 1 + "#nvarchar#" + model.IPLNO);
                input_parameters.Add("@workOrderNumber", 1 + "#nvarchar#" + model.workOrderNumber);
                input_parameters.Add("@Initial_Comments", 1 + "#nvarchar#" + model.Initial_Comments);
                input_parameters.Add("@Write_Off", 1 + "#bit#" + model.Write_Off);

                //input_parameters.Add("@CheckNumber", 1 + "#nvarchar#" + model.CheckNumber);
                //input_parameters.Add("@CheckDate", 1 + "#nvarchar#" + model.CheckDate);

                input_parameters.Add("@ClientId", 1 + "#nvarchar#" + model.ClientId);
               
               


                input_parameters.Add("@ICP_IsActive", 1 + "#bit#" + model.ICP_IsActive);
                input_parameters.Add("@ICP_IsDelete", 1 + "#bit#" + model.ICP_IsDelete);


                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@ICP_PkeyID_Out", 2 + "#bigint#" + null);

                input_parameters.Add("@workOrder_ID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@Inv_Client_pkeyId_Out", 2 + "#bigint#" + null);

                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                return obj.SqlCRUD(insertProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return null;
            }
            

        }

    }
}