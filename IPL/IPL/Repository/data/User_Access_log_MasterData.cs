﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class User_Access_log_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public List<dynamic> AddUser_Access_logdata(User_Access_log_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objclntData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_User_Access_log_Master]";
            User_Access_log user_Access_log = new User_Access_log();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@User_Acc_Log_PkeyId", 1 + "#bigint#" + model.User_Acc_Log_PkeyId);
                input_parameters.Add("@User_Acc_Log_IP_Address", 1 + "#varchar#" + model.User_Acc_Log_IP_Address);
                input_parameters.Add("@User_Acc_Log_Logged_In", 1 + "#varchar#" + model.User_Acc_Log_Logged_In);
                input_parameters.Add("@User_Acc_Log_Logged_Out", 1 + "#varchar#" + model.User_Acc_Log_Logged_Out);
                input_parameters.Add("@User_Acc_Log_MacD", 1 + "#varchar#" + model.User_Acc_Log_MacD);
                input_parameters.Add("@User_Acc_Log_IsActive", 1 + "#bit#" + model.User_Acc_Log_IsActive);
                input_parameters.Add("@User_Acc_Log_IsDelete", 1 + "#bit#" + model.User_Acc_Log_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@User_Acc_Log_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    user_Access_log.User_Acc_Log_PkeyId = "0";
                    user_Access_log.Status = "0";
                    user_Access_log.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    user_Access_log.User_Acc_Log_PkeyId = Convert.ToString(objData[0]);
                    user_Access_log.Status = Convert.ToString(objData[1]);


                }
                objclntData.Add(user_Access_log);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objclntData;



        }

        private DataSet GetUser_Access_logMaster(User_Access_log_MasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_User_Access_log_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@User_Acc_Log_PkeyId", 1 + "#bigint#" + model.User_Acc_Log_PkeyId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetUser_Access_logDetails(User_Access_log_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetUser_Access_logMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<User_Access_log_MasterDTO> User_Access =
                   (from item in myEnumerableFeaprd
                    select new User_Access_log_MasterDTO
                    {

                        User_Acc_Log_PkeyId = item.Field<Int64>("User_Acc_Log_PkeyId"),
                        User_Acc_Log_IP_Address = item.Field<String>("User_Acc_Log_IP_Address"),
                        User_Acc_Log_Logged_In = item.Field<String>("User_Acc_Log_Logged_In"),
                        User_Acc_Log_Logged_Out = item.Field<String>("User_Acc_Log_Logged_Out"),
                        User_Acc_Log_MacD = item.Field<String>("User_Acc_Log_MacD"),
                        User_Acc_Log_IsActive = item.Field<Boolean?>("User_Acc_Log_IsActive"),

                    }).ToList();

                objDynamic.Add(User_Access);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}