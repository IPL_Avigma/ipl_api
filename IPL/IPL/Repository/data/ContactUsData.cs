﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace IPL.Repository.data
{
    public class ContactUsData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        public List<dynamic> AddContactDetailsMaster(ContactUsDTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_Contact_Us_Master]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Con_Us_PkeyId", 1 + "#bigint#" + model.Con_Us_PkeyId);
                input_parameters.Add("@Con_Us_FName", 1 + "#nvarchar#" + model.Con_Us_FName);
                input_parameters.Add("@Con_Us_Email", 1 + "#nvarchar#" + model.Con_Us_Email);
                input_parameters.Add("@Con_Us_Contact_Number", 1 + "#nvarchar#" + model.Con_Us_Contact_Number);
                input_parameters.Add("@Con_Us_Message", 1 + "#nvarchar#" + model.Con_Us_Message);
                input_parameters.Add("@Con_Us_Subject", 1 + "#nvarchar#" + model.Con_Us_Subject);
                input_parameters.Add("@Con_Us_ValType", 1 + "#int#" + model.Con_Us_ValType);
                input_parameters.Add("@Con_Us_IsActive", 1 + "#bit#" + model.Con_Us_IsActive);
                input_parameters.Add("@Con_Us_IsDelete", 1 + "#bit#" + model.Con_Us_IsDelete);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Con_Us_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;



        }

        private DataSet Get_ContactData_Master(ContactUsDTO model)
        {
            DataSet ds = null;
            try
            {

                string selectProcedure = "[Get_Contact_Us_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Con_Us_PkeyId", 1 + "#bigint#" + model.Con_Us_PkeyId);

                input_parameters.Add("@WhereClause", 1 + "#nvarchar#" + model.WhereClause);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@PageNumber", 1 + "#int#" + model.PageNumber);
                input_parameters.Add("@Rowcount", 1 + "#int#" + model.Rowcount);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> Get_Contact_Details_Master(ContactUsDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();

            try
            {


                string wherecondition = string.Empty;
                if (model.Type == 3 && !string.IsNullOrEmpty(model.FilterData))
                {
                    var Data = JsonConvert.DeserializeObject<NewContactUs_Filter>(model.FilterData);
                    if (!string.IsNullOrEmpty(Data.Con_Us_FName))
                    {
                        wherecondition = " And cu.Con_Us_FName LIKE '%" + Data.Con_Us_FName + "%'";
                    }
                    if (!string.IsNullOrEmpty(Data.Con_Us_Subject))
                    {
                        wherecondition = " And cu.Con_Us_Subject LIKE '%" + Data.Con_Us_Subject + "%'";
                    }
                    if (!string.IsNullOrEmpty(Data.Con_Us_Email))
                    {
                        wherecondition = " And cu.Con_Us_Email LIKE '%" + Data.Con_Us_Email + "%'";
                    }

                    model.WhereClause = wherecondition;
                }
                DataSet ds = Get_ContactData_Master(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<ContactUsDTO> contact_Master =
                   (from item in myEnumerableFeaprd
                    select new ContactUsDTO
                    {
                        Con_Us_PkeyId = item.Field<Int64>("Con_Us_PkeyId"),
                        Con_Us_FName = item.Field<String>("Con_Us_FName"),
                        Con_Us_Email = item.Field<String>("Con_Us_Email"),
                        Con_Us_Contact_Number = item.Field<String>("Con_Us_Contact_Number"),
                        Con_Us_Message = item.Field<String>("Con_Us_Message"),
                        Con_Us_Subject = item.Field<String>("Con_Us_Subject"),
                        Con_Us_ValType = item.Field<int?>("Con_Us_ValType"),
                        Con_Us_IsActive = item.Field<Boolean?>("Con_Us_IsActive"),

                    }).ToList();

                objDynamic.Add(contact_Master);
               

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}