﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class PCR_FiveBrotherData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public List<dynamic> AddPCR_FiveBrother_Data(PCR_FiveBrotherDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objclntData = new List<dynamic>();
            //string insertProcedure = "[CreateUpdate_PCR_Fivebrothers_Master]";
            string insertProcedure = "[CreateUpdate_PCR_Fivebrothers_Master_Json]";
            PCR_Fivebrother_Master pCR_Fivebrother_Master = new PCR_Fivebrother_Master();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@PCR_FiveBro_id", 1 + "#bigint#" + model.PCR_FiveBro_id);
                input_parameters.Add("@PCR_FiveBro_Json", 1 + "#varchar#" + model.PCR_FiveBro_Json);
                input_parameters.Add("@PCR_FiveBro_Valtype", 1 + "#int#" + model.PCR_FiveBro_Valtype);
                input_parameters.Add("@PCR_FiveBro_WO_ID", 1 + "#bigint#" + model.PCR_FiveBro_WO_ID);

                //input_parameters.Add("@PCR_FiveBro_Propertyinfo", 1 + "#varchar#" + model.PCR_FiveBro_Propertyinfo);
                //input_parameters.Add("@PCR_FiveBro_Violations", 1 + "#varchar#" + model.PCR_FiveBro_Violations);
                //input_parameters.Add("@PCR_FiveBro_Securing", 1 + "#varchar#" + model.PCR_FiveBro_Securing);
                //input_parameters.Add("@PCR_FiveBro_Winterization", 1 + "#varchar#" + model.PCR_FiveBro_Winterization);
                //input_parameters.Add("@PCR_FiveBro_Yard", 1 + "#varchar#" + model.PCR_FiveBro_Yard);
                //input_parameters.Add("@PCR_FiveBro_Debris_Hazards", 1 + "#varchar#" + model.PCR_FiveBro_Debris_Hazards);
                //input_parameters.Add("@PCR_FiveBro_Roof", 1 + "#varchar#" + model.PCR_FiveBro_Roof);
                //input_parameters.Add("@PCR_FiveBro_Pool", 1 + "#varchar#" + model.PCR_FiveBro_Pool);
                //input_parameters.Add("@PCR_FiveBro_Utilities", 1 + "#varchar#" + model.PCR_FiveBro_Utilities);
                //input_parameters.Add("@PCR_FiveBro_Appliances", 1 + "#varchar#" + model.PCR_FiveBro_Appliances);
                //input_parameters.Add("@PCR_FiveBro_Damages", 1 + "#varchar#" + model.PCR_FiveBro_Damages);
                //input_parameters.Add("@PCR_FiveBro_Conveyance", 1 + "#varchar#" + model.PCR_FiveBro_Conveyance);
                //input_parameters.Add("@PCR_FiveBro_Integration_Type", 1 + "#bigint#" + model.PCR_FiveBro_Integration_Type);
                //input_parameters.Add("@PCR_FiveBro_IsIntegration", 1 + "#bit#" + model.PCR_FiveBro_IsIntegration);

                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@PCR_FiveBro_IsActive", 1 + "#bit#" + model.PCR_FiveBro_IsActive);
                //input_parameters.Add("@PCR_FiveBro_IsDelete", 1 + "#bit#" + model.PCR_FiveBro_IsDelete);
                input_parameters.Add("@PCR_FiveBro_id_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    pCR_Fivebrother_Master.PCR_FiveBro_id = "0";
                    pCR_Fivebrother_Master.Status = "0";
                    pCR_Fivebrother_Master.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    pCR_Fivebrother_Master.PCR_FiveBro_id = Convert.ToString(objData[0]);
                    pCR_Fivebrother_Master.Status = Convert.ToString(objData[1]);


                }
                objclntData.Add(pCR_Fivebrother_Master);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objclntData;



        }

        private DataSet GetPCRfivebrotherMaster(PCR_FiveBrotherDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_PCR_Fivebrothers_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@PCR_FiveBro_id", 1 + "#bigint#" + model.PCR_FiveBro_id);
                input_parameters.Add("@PCR_FiveBro_WO_ID", 1 + "#bigint#" + model.PCR_FiveBro_WO_ID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetPCRfivebrotherDetails(PCR_FiveBrotherDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetPCRfivebrotherMaster(model);


                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                }

                //var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                //List<PCR_FiveBrotherDTO> PCRFivebrothers =
                //   (from item in myEnumerableFeaprd
                //    select new PCR_FiveBrotherDTO
                //    {
                //        PCR_FiveBro_id = item.Field<Int64>("PCR_FiveBro_id"),
                //        PCR_FiveBro_WO_ID = item.Field<Int64>("PCR_FiveBro_WO_ID"),
                //        PCR_FiveBro_Propertyinfo = item.Field<String>("PCR_FiveBro_Propertyinfo"),
                //        PCR_FiveBro_Violations = item.Field<String>("PCR_FiveBro_Violations"),
                //        PCR_FiveBro_Securing = item.Field<String>("PCR_FiveBro_Securing"),
                //        PCR_FiveBro_Winterization = item.Field<String>("PCR_FiveBro_Winterization"),
                //        PCR_FiveBro_Yard = item.Field<String>("PCR_FiveBro_Yard"),
                //        PCR_FiveBro_Debris_Hazards = item.Field<String>("PCR_FiveBro_Debris_Hazards"),
                //        PCR_FiveBro_Roof = item.Field<String>("PCR_FiveBro_Roof"),
                //        PCR_FiveBro_Pool = item.Field<String>("PCR_FiveBro_Pool"),
                //        PCR_FiveBro_Utilities = item.Field<String>("PCR_FiveBro_Utilities"),
                //        PCR_FiveBro_Appliances = item.Field<String>("PCR_FiveBro_Appliances"),
                //        PCR_FiveBro_Damages = item.Field<String>("PCR_FiveBro_Damages"),
                //        PCR_FiveBro_Conveyance = item.Field<String>("PCR_FiveBro_Conveyance"),
                //        PCR_FiveBro_Integration_Type = item.Field<Int64?>("PCR_FiveBro_Integration_Type"),
                //        PCR_FiveBro_IsIntegration = item.Field<Boolean?>("PCR_FiveBro_IsIntegration"),
                //        PCR_FiveBro_IsActive = item.Field<Boolean?>("PCR_FiveBro_IsActive"),




                //    }).ToList();

                //objDynamic.Add(PCRFivebrothers);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}