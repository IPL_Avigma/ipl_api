﻿using Avigma.Repository.Lib;
using IPL.Models;
using IPLApp.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
 
namespace IPL.Repository.data
{
    public class InstructionDocumentData
    {

        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
      

        private List<dynamic> AddUpdateInstructionDocumentData(Instruction_DocumentDTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_Instruction_Document]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Inst_Doc_PkeyID", 1 + "#bigint#" + model.Inst_Doc_PkeyID); // Own primary key
                input_parameters.Add("@Inst_Doc_Inst_Ch_ID", 1 + "#bigint#" + model.Inst_Doc_Inst_Ch_ID);  // Instrction child id
                input_parameters.Add("@Inst_Doc_Wo_ID", 1 + "#bigint#" + model.Inst_Doc_Wo_ID);
                input_parameters.Add("@Inst_Doc_File_Path", 1 + "#varchar#" + model.Inst_Doc_File_Path);
                input_parameters.Add("@Inst_Doc_File_Size", 1 + "#varchar#" + model.Inst_Doc_File_Size);
                input_parameters.Add("@Inst_Doc_File_Name", 1 + "#varchar#" + model.Inst_Doc_File_Name);
                input_parameters.Add("@Inst_Doc_BucketName", 1 + "#varchar#" + model.Inst_Doc_BucketName);
                input_parameters.Add("@Inst_Doc_ProjectID", 1 + "#varchar#" + model.Inst_Doc_ProjectID);
                input_parameters.Add("@Inst_Doc_Object_Name", 1 + "#varchar#" + model.Inst_Doc_Object_Name);
                input_parameters.Add("@Inst_Doc_Folder_Name", 1 + "#varchar#" + model.Inst_Doc_Folder_Name);
                input_parameters.Add("@Inst_Doc_UploadedBy", 1 + "#varchar#" + model.Inst_Doc_UploadedBy);
                input_parameters.Add("@Inst_Doc_IsActive", 1 + "#bit#" + model.Inst_Doc_IsActive);
                input_parameters.Add("@Inst_Doc_IsDelete", 1 + "#bit#" + model.Inst_Doc_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Folder_File_Master_FKId", 1 + "#bigint#" + model.Folder_File_Master_FKId); // Added by Dipali
                input_parameters.Add("@Inst_Doc_PkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;



        }
        private DataSet Get_Instruction_Document_Master(Client_Result_PhotoDTO model)
        {
            DataSet ds = null;
            try

            {
              
                string selectProcedure = "[Get_Instruction_Document_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Inst_Doc_PkeyID", 1 + "#bigint#" + model.Client_Result_Photo_ID);
                input_parameters.Add("@Inst_Doc_Inst_Ch_ID", 1 + "#bigint#" + model.Client_Result_Photo_Ch_ID);
                input_parameters.Add("@Inst_Doc_Wo_ID", 1 + "#bigint#" + model.Client_Result_Photo_Wo_ID);

                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        //Add multiple Instruction document
        public List<dynamic> AddUpdateInstructionDocsData(Client_Result_PhotoDTO client_Result_PhotoDTO)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                log.logDebugMessage("Method Called AddUpdateInstructionDocsData");
                var WorkOrderData = JsonConvert.DeserializeObject<List<WorkOrderIDItems>>(client_Result_PhotoDTO.WorkOrder_ID_Data);
                for (int i = 0; i < WorkOrderData.Count; i++)
                {
                    Instruction_DocumentDTO instruction_DocumentDTO = new Instruction_DocumentDTO();

                    instruction_DocumentDTO.Inst_Doc_PkeyID = client_Result_PhotoDTO.Client_Result_Photo_ID;  // own primary key
                    instruction_DocumentDTO.Inst_Doc_Inst_Ch_ID = client_Result_PhotoDTO.Client_Result_Photo_Ch_ID;  //instruction child id
                    instruction_DocumentDTO.Inst_Doc_Wo_ID = WorkOrderData[i].WorkOrderID;
                    instruction_DocumentDTO.Inst_Doc_File_Path = client_Result_PhotoDTO.Client_Result_Photo_FilePath;
                    instruction_DocumentDTO.Inst_Doc_File_Size = client_Result_PhotoDTO.Client_Result_Photo_FileSize;
                    instruction_DocumentDTO.Inst_Doc_File_Name = client_Result_PhotoDTO.Client_Result_Photo_FileName;
                    instruction_DocumentDTO.Inst_Doc_BucketName = client_Result_PhotoDTO.Client_Result_Photo_BucketName;

                    instruction_DocumentDTO.Inst_Doc_ProjectID = client_Result_PhotoDTO.Client_Result_Photo_ProjectID;
                    instruction_DocumentDTO.Inst_Doc_Object_Name = client_Result_PhotoDTO.Client_Result_Photo_objectName;

                    instruction_DocumentDTO.Inst_Doc_Folder_Name = client_Result_PhotoDTO.Client_Result_Photo_FolderName;
                    instruction_DocumentDTO.Inst_Doc_IsActive = true;
                    instruction_DocumentDTO.Inst_Doc_IsDelete = false;
                    instruction_DocumentDTO.Inst_Doc_UploadedBy = Convert.ToString(client_Result_PhotoDTO.UserID);

                    instruction_DocumentDTO.UserID = client_Result_PhotoDTO.UserID;
                    instruction_DocumentDTO.Type = 1;


                    objData = AddUpdateInstructionDocumentData(instruction_DocumentDTO);

                }

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);



            }
            return objData;
        }

        public List<dynamic> AddInstructionDocumentData(Client_Result_PhotoDTO client_Result_PhotoDTO)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                Instruction_DocumentDTO instruction_DocumentDTO = new Instruction_DocumentDTO();

                instruction_DocumentDTO.Inst_Doc_PkeyID = client_Result_PhotoDTO.Client_Result_Photo_ID;  // own primary key
                instruction_DocumentDTO.Inst_Doc_Inst_Ch_ID = client_Result_PhotoDTO.Client_Result_Photo_Ch_ID;  //instruction child id
                instruction_DocumentDTO.Inst_Doc_Wo_ID = client_Result_PhotoDTO.Client_Result_Photo_Wo_ID;
                instruction_DocumentDTO.Inst_Doc_File_Path = client_Result_PhotoDTO.Client_Result_Photo_FilePath;
                instruction_DocumentDTO.Inst_Doc_File_Size = client_Result_PhotoDTO.Client_Result_Photo_FileSize;
                instruction_DocumentDTO.Inst_Doc_File_Name = client_Result_PhotoDTO.Client_Result_Photo_FileName;
                instruction_DocumentDTO.Inst_Doc_BucketName = client_Result_PhotoDTO.Client_Result_Photo_BucketName;

                instruction_DocumentDTO.Inst_Doc_ProjectID = client_Result_PhotoDTO.Client_Result_Photo_ProjectID;
                instruction_DocumentDTO.Inst_Doc_Object_Name = client_Result_PhotoDTO.Client_Result_Photo_objectName;

                instruction_DocumentDTO.Inst_Doc_Folder_Name = client_Result_PhotoDTO.Client_Result_Photo_FolderName;
                instruction_DocumentDTO.Inst_Doc_IsActive = true;
                instruction_DocumentDTO.Inst_Doc_IsDelete = false;
                instruction_DocumentDTO.Inst_Doc_UploadedBy = Convert.ToString(client_Result_PhotoDTO.UserID);

                instruction_DocumentDTO.UserID = client_Result_PhotoDTO.UserID;
                instruction_DocumentDTO.Type = 1;

                objData = AddUpdateInstructionDocumentData(instruction_DocumentDTO);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        public List<dynamic> Get_Instruction_Document_MasterDetails(Client_Result_PhotoDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = Get_Instruction_Document_Master(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Client_Result_PhotoDTO> InstructionDocument =
                   (from item in myEnumerableFeaprd
                    select new Client_Result_PhotoDTO
                    {
                        Client_Result_Photo_ID = item.Field<Int64>("Inst_Doc_PkeyID"),
                        Client_Result_Photo_Ch_ID = item.Field<Int64>("Inst_Doc_Inst_Ch_ID"),
                        Client_Result_Photo_Wo_ID = item.Field<Int64?>("Inst_Doc_Wo_ID"),
                        Client_Result_Photo_FilePath = item.Field<String>("Inst_Doc_File_Path"),
                        Client_Result_Photo_FileSize = item.Field<String>("Inst_Doc_File_Size"),
                        Client_Result_Photo_FileName = item.Field<String>("Inst_Doc_File_Name"),
                        Client_Result_Photo_BucketName = item.Field<String>("Inst_Doc_BucketName"),
                        Client_Result_Photo_ProjectID = item.Field<String>("Inst_Doc_ProjectID"),
                        Client_Result_Photo_objectName = item.Field<String>("Inst_Doc_Object_Name"),
                        Client_Result_Photo_FolderName = item.Field<String>("Inst_Doc_Folder_Name"),
                        //  UserID = item.Field<String>("Inst_Doc_UploadedBy"),
                        Client_Result_Photo_IsActive = item.Field<Boolean?>("Inst_Doc_IsActive"),


                        


                    }).ToList();

                objDynamic.Add(InstructionDocument);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
        public List<dynamic> DeleteInstructionFile(Client_Result_PhotoDTO client_Result_PhotoDTO)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                Instruction_DocumentDTO instruction_DocumentDTO = new Instruction_DocumentDTO();

                instruction_DocumentDTO.Inst_Doc_PkeyID = client_Result_PhotoDTO.Client_Result_Photo_ID;  // own primary key
                instruction_DocumentDTO.Inst_Doc_Inst_Ch_ID = client_Result_PhotoDTO.Client_Result_Photo_Ch_ID;  //instruction child id
                instruction_DocumentDTO.Inst_Doc_Wo_ID = client_Result_PhotoDTO.Client_Result_Photo_Wo_ID;               

                instruction_DocumentDTO.UserID = client_Result_PhotoDTO.UserID;
                instruction_DocumentDTO.Type = client_Result_PhotoDTO.Type;

                objData = AddUpdateInstructionDocumentData(instruction_DocumentDTO);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }
    }
}























