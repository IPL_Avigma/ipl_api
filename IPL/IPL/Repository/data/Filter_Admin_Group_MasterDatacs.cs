﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class Filter_Admin_Group_MasterDatacs
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        public List<dynamic> AddUpdate_Filter_Admin_Group(Filter_Admin_Group_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_Filter_Admin_Group_Master]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Group_Filter_PkeyID", 1 + "#bigint#" + model.Group_Filter_PkeyID);
                input_parameters.Add("@Group_Filter_Group_Name", 1 + "#nvarchar#" + model.Group_Filter_Group_Name);
                input_parameters.Add("@Group_Filter_GRPIsActive", 1 + "#bit#" + model.Group_Filter_GRPIsActive);
                input_parameters.Add("@Group_Filter_IsActive", 1 + "#bit#" + model.Group_Filter_IsActive);
                input_parameters.Add("@Group_Filter_IsDelete", 1 + "#bit#" + model.Group_Filter_IsDelete);
                input_parameters.Add("@Group_Filter_CompanyId", 1 + "#bigint#" + model.Group_Filter_CompanyId);
                input_parameters.Add("@Group_Filter_UserId", 1 + "#bigint#" + model.Group_Filter_UserId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserId);
                input_parameters.Add("@Group_Filter_PkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        private DataSet Get_Filter_Admin_Group(Filter_Admin_Group_MasterDTO model)
        {
            DataSet ds = null;
            try
            {

                string selectProcedure = "[Get_Filter_Admin_Group_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Group_Filter_PkeyID", 1 + "#bigint#" + model.Group_Filter_PkeyID);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> Get_Filter_Admin_GroupDetails(Filter_Admin_Group_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = Get_Filter_Admin_Group(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Filter_Admin_Group_MasterDTO> FilterGroup =
                   (from item in myEnumerableFeaprd
                    select new Filter_Admin_Group_MasterDTO
                    {
                        Group_Filter_PkeyID = item.Field<Int64>("Group_Filter_PkeyID"),
                        Group_Filter_Group_Name = item.Field<String>("Group_Filter_Group_Name"),
                        Group_Filter_GRPIsActive = item.Field<Boolean?>("Group_Filter_GRPIsActive")

                    }).ToList();

                objDynamic.Add(FilterGroup);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}