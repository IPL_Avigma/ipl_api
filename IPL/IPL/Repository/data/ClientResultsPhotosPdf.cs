﻿using Avigma.Repository.Lib;
using IPL.Models;
using NReco.PdfGenerator;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace IPL.Repository.data
{
    public class ClientResultsPhotosPdf
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

     
        public static byte[] GenerateRuntimePDF(string html)
        {
            #region NReco.PdfGenerator
            HtmlToPdfConverter nRecohtmltoPdfObj = new HtmlToPdfConverter();
            return nRecohtmltoPdfObj.GeneratePdf(html);
            #endregion

        }
        public async Task<CustomReportPDF> GeneratePDFClientResultsPhotos(GetClientResultPhoto_DTO model)
        {
            CustomReportPDF custom = new CustomReportPDF();
            Client_Result_PhotoData client_Result_PhotoData = new Client_Result_PhotoData();


            try
            {
                StringBuilder html = new StringBuilder();
                StringBuilder Photosgellary = new StringBuilder();
                string Photosdata = "";
                var PhotoList = client_Result_PhotoData.GetClientResultPhotopdfMaster(model);

                if (PhotoList.Tables.Count > 0)
                {

                    switch (model.valtype)
                    {
                        case 1:
                            {
                                Photosdata += "<div class='container' style = 'display: flex;flex - wrap: wrap;'> ";
                                if (PhotoList != null)
                                {
                                    //foreach ( DataRow item in PhotoList.Tables[0].Rows)
                                    //{
                                    //    var filePath = item[3];
                                    //    Photosdata += "<div style = 'width: 100%;'> <img src =" + filePath + " style = 'width: 40%; display: flex;flex - wrap: wrap; margin: 40px;/> </div> </div>";

                                    //}
                                    for (int i = 0; i < PhotoList.Tables[0].Rows.Count; i++)
                                    {
                                           var filePath = PhotoList.Tables[0].Rows[i]["Client_Result_Photo_FilePath"].ToString();
                                      

                                        if (!string.IsNullOrWhiteSpace(Photosdata))
                                        {
                                            Photosdata = Photosdata + "<div style = 'width: 100%;'> <img src =" + filePath + " style = 'width: 40%; display: flex;flex - wrap: wrap; margin: 40px;/></div> </div>";

                                        }
                                        else
                                        {
                                            Photosdata =  "<div style = 'width: 100%;'> <img src =" + filePath + " style = 'width: 40%; display: flex;flex - wrap: wrap; margin: 40px;/> </div> </div>";
                                        }
                                       
                                        
                                    }


                                }
                                Photosdata += "<img src  = ''/>";
                                Photosdata += "</div>";

                                break;
                            }
                        case 2:
                            {
                                Photosdata += "<div class='container' style = 'display: flex;flex - wrap: wrap;'> ";
                                if (PhotoList != null)
                                {
                                    for (int i = 0; i < PhotoList.Tables[0].Rows.Count; i++)
                                    {
                                        var filePath = PhotoList.Tables[0].Rows[i]["Client_Result_Photo_FilePath"].ToString();


                                        if (!string.IsNullOrWhiteSpace(Photosdata))
                                        {
                                            Photosdata += "<div style = 'width: 100%;'> <img src =" + filePath + " style = 'width: 30%; display: flex;flex - wrap: wrap; margin: 10px;/> </div> </div>";

                                        }
                                        else
                                        {
                                            Photosdata += "<div style = 'width: 100%;'> <img src =" + filePath + " style = 'width: 30%; display: flex;flex - wrap: wrap; margin: 10px;/> </div> </div>";
                                        }


                                    }

                                    //foreach (DataRow item in PhotoList.Tables[0].Rows)
                                    //{
                                    //    var filePath = item[3];
                                    //    Photosdata += "<div style = 'width: 100%;'> <img src =" + filePath + " style = 'width: 30%; display: flex;flex - wrap: wrap; margin: 10px;/> </div> </div>";

                                    //}
                                }
                                Photosdata += "<img src  = ''/>";
                                Photosdata += "</div>";
                                break;
                            }

                        case 3:
                            {
                                Photosdata += "<div class='container' style = 'display: flex;flex - wrap: wrap;'> ";
                                if (PhotoList != null)
                                {
                                    for (int i = 0; i < PhotoList.Tables[0].Rows.Count; i++)
                                    {
                                        var filePath = PhotoList.Tables[0].Rows[i]["Client_Result_Photo_FilePath"].ToString();


                                        if (!string.IsNullOrWhiteSpace(Photosdata))
                                        {
                                            Photosdata += "<div style = 'width: 100%;'> <img src =" + filePath + " style = 'width: 22%; display: flex;flex - wrap: wrap; margin: 10px;/> </div>";

                                        }
                                        else
                                        {
                                            Photosdata += "<div style = 'width: 100%;'> <img src =" + filePath + " style = 'width: 22%; display: flex;flex - wrap: wrap; margin: 10px;/> </div>";
                                        }


                                    }
                                    //foreach (DataRow item in PhotoList.Tables[0].Rows)
                                    //{
                                    //    var filePath = item[3];
                                    //    Photosdata += "<div style = 'width: 100%;'> <img src =" + filePath + " style = 'width: 22%; display: flex;flex - wrap: wrap; margin: 10px;/> </div>";

                                    //}
                                }
                                Photosdata += "<img src  = ''/>";
                                Photosdata += "</div>";
                                break;
                            }
                    }
                }

               



                Photosgellary.Append(Photosdata);



                html.Append(Photosgellary.ToString());
                byte[] pdffile = GenerateRuntimePDF(html.ToString());
                custom.Data = pdffile;
                custom.HttpStatusCode = System.Net.HttpStatusCode.OK;
                return custom;
            }
            catch (Exception ex)
            {
                custom.Message = ex.Message;
                custom.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                return custom;
            }
        }



 



    }
}