﻿using Avigma.Repository.Lib;
using IPL.Models;
using IPL.Models.FiveBrothers;
using IPL.Repository.data;
using IPLApp.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Net;

namespace IPLApp.Repository.data
{
    public class GetWorkOrders
    {
        WorkOrderMasterData workOrderMasterData = new WorkOrderMasterData();
        ServiceMasterData serviceMasterData = new ServiceMasterData();
        Survey_Question_MasterData survey_Question_MasterData = new Survey_Question_MasterData();
        Survey_CHQuestion_MasterData survey_CHQuestion_MasterData = new Survey_CHQuestion_MasterData();
        WorkOrder_Puran_MasterData workOrder_Puran_MasterData = new WorkOrder_Puran_MasterData();
        ImportWorkOrderData ImportWorkOrderData = new ImportWorkOrderData();
        ImportWorkOrderDTO ImportWorkOrderDTO = new ImportWorkOrderDTO();
        WorkOrder_FiveBrother_MasterData workOrder_FiveBrother_MasterData = new WorkOrder_FiveBrother_MasterData();
        WorkOrder_MSI_MasterData workOrder_MSI_MasterData = new WorkOrder_MSI_MasterData();
        WorkOrder_NFR_MasterData workOrder_NFR_MasterData = new WorkOrder_NFR_MasterData();
        Log log = new Log();

        //public List<dynamic> GetPPWWorkOdersDetails(LoginDTO Model)
        //{
        //    List<dynamic> objdynamicobj = new List<dynamic>();
        //    try
        //    {
        //        log.logDebugMessage("-------------Scrapper Called Start ----------------");
        //        log.logDebugMessage("---------URL ----------------" + Model.URL);



        //        //string strURL = System.Configuration.ConfigurationManager.AppSettings["PPW"];
        //       string strURL = Model.URL;
        //        string Schedulertime = System.Configuration.ConfigurationManager.AppSettings["AddSchedulertime"];
        //       var client = new RestClient(strURL);
        //        var request = new RestRequest(Method.POST);
        //        client.Timeout = -1;

        //        log.logDebugMessage("---------username ----------------" + Model.username);
        //        log.logDebugMessage("---------password ----------------" + Model.password);
        //        log.logDebugMessage("---------image_download ----------------" + Model.image_download.ToString().ToLower());


        //        //request.AlwaysMultipartFormData = true;
        //        //request.AddParameter("ppw_username", Model.username);
        //        //request.AddParameter("ppw_password", Model.password);
        //        //request.AddParameter("image_download", Model.image_download.ToString().ToLower());

        //        request.AlwaysMultipartFormData = true;
        //        request.AddParameter("username", Model.username);
        //        request.AddParameter("password", Model.password);
        //     //   request.AddParameter("image_download", Model.image_download.ToString().ToLower());


        //        IRestResponse response = client.Execute(request);
        //        DateTime dateTime = System.DateTime.Now.AddMinutes(Convert.ToInt32(Schedulertime) + 5);
        //        //DateTime dateTime = System.DateTime.Now.AddMinutes(Convert.ToInt32(Schedulertime) + 0);
        //        log.logDebugMessage("PPW Fetch Data------------------>"+dateTime.ToString());

        //        Avigma.Repository.Scheduler.JobSchedulerStart.Start(dateTime, Model);

        //        log.logDebugMessage("-------------Scrapper Called End ----------------");
        //        return objdynamicobj;
        //    }
        //    catch (Exception ex)
        //    {
        //        log.logErrorMessage(ex.StackTrace);
        //        log.logErrorMessage(ex.Message);
        //        return objdynamicobj;

        //    }
        //}

        public List<dynamic> GetPPWWorkOdersDetails(LoginDTO Model)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                log.logDebugMessage("-------------Scrapper Called Start ----------------");
                log.logDebugMessage("---------URL ----------------" + Model.URL);
                ScrapperResponseDTO scrapperResponseDTO = new ScrapperResponseDTO();


                //string strURL = System.Configuration.ConfigurationManager.AppSettings["PPW"];
                string strURL = Model.URL;
                string Schedulertime = System.Configuration.ConfigurationManager.AppSettings["AddSchedulertime"];
                var client = new RestClient(strURL);
                client.Timeout = -1;

                log.logDebugMessage("---------username ----------------" + Model.username);
                log.logDebugMessage("---------password ----------------" + Model.password);




                var request = new RestRequest("?username=" + Model.username + "&password=" + Model.password + "&id=" + Model.WI_Pkey_ID +  "", Method.GET);


                IRestResponse response = client.Execute(request);
                scrapperResponseDTO.Responseval = response.Content;

                DateTime dateTime = System.DateTime.Now.AddMinutes(Convert.ToInt32(Schedulertime) + 5);
                //DateTime dateTime = System.DateTime.Now.AddMinutes(Convert.ToInt32(Schedulertime) + 0);
                log.logDebugMessage("PPW Fetch Data------------------>" + dateTime.ToString());
                log.logDebugMessage("?username=" + Model.username + "&password=" + Model.password + "&id=" + Model.WI_Pkey_ID + "");
                log.logDebugMessage("Auto Scrapper" + response.StatusCode);
                log.logDebugMessage("Auto Scrapper Response" + scrapperResponseDTO.Responseval);
                Model.Scrapper_rep_code = scrapperResponseDTO.Responseval;
                Avigma.Repository.Scheduler.JobSchedulerStart.Start(dateTime, Model);

                log.logDebugMessage("-------------Scrapper Called End ----------------");
                return objdynamicobj;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return objdynamicobj;

            }
        }
        //public List<dynamic> GetPPWWorkOdersDetailsByUser(LoginDTO Model)
        //{
        //    List<dynamic> objdynamicobj = new List<dynamic>();
        //    try
        //    {
        //        log.logDebugMessage("-------------Manual Scrapper Called Start ----------------");
        //        log.logDebugMessage("---------Manual URL ----------------" + Model.URL);

        //        //// Delete Unprocess Data
        //        //ImportWorkOrderDTO.ImportFrom = 1;
        //        //ImportWorkOrderDTO.Type = 1;
        //        //ImportWorkOrderDTO.UserID = 0;
        //        //ImportWorkOrderData.DeleteWorkOrderAPIData(ImportWorkOrderDTO);

        //        //string strURL = System.Configuration.ConfigurationManager.AppSettings["PPW"];
        //        string strURL = Model.URL;
        //        var client = new RestClient(strURL.Trim());
        //        var request = new RestRequest(Method.POST);
        //        client.Timeout = -1;

        //        log.logDebugMessage("--------- Manual username ----------------" + Model.username);
        //        log.logDebugMessage("---------Manual password ----------------" + Model.password);
        //        log.logDebugMessage("--------- Manual image_download ----------------" + Model.image_download.ToString().ToLower());


        //        request.AlwaysMultipartFormData = true;
        //        //request.AddParameter("ppw_username", Model.username);
        //        //request.AddParameter("ppw_password", Model.password);
        //        //request.AddParameter("image_download", Model.image_download.ToString().ToLower());

        //        request.AddParameter("username", Model.username);
        //        request.AddParameter("password", Model.password);
        //        //request.AddParameter("image_download", Model.image_download.ToString().ToLower());

        //        IRestResponse response = client.Execute(request);


        //        log.logDebugMessage("-------------Manual Scrapper Called End ----------------");





        //        return objdynamicobj;
        //    }
        //    catch (Exception ex)
        //    {
        //        log.logErrorMessage(ex.StackTrace);
        //        log.logErrorMessage(ex.Message);
        //        return objdynamicobj;

        //    }
        //}

        public List<dynamic> GetPPWWorkOdersDetailsByUser(LoginDTO Model)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                log.logDebugMessage("-------------Manual Scrapper Called Start ----------------");
                log.logDebugMessage("---------Manual URL ----------------" + Model.URL);
                ScrapperResponseDTO scrapperResponseDTO = new ScrapperResponseDTO();

                string strURL = Model.URL;
                var client = new RestClient(strURL.Trim());
                
                client.Timeout = -1;

                log.logDebugMessage("--------- Manual username ----------------" + Model.username);
                log.logDebugMessage("---------Manual password ----------------" + Model.password);
                


                var request = new RestRequest("?username="+Model.username+"&password="+Model.password + "&id=" + Model.WI_Pkey_ID + "", Method.GET);

                IRestResponse response = client.Execute(request);
                scrapperResponseDTO.Responseval = response.Content;
                Model.Scrapper_rep_code = scrapperResponseDTO.Responseval;
                log.logDebugMessage("?username=" + Model.username + "&password=" + Model.password + "&id=" + Model.WI_Pkey_ID + "");
                log.logDebugMessage("Manual Scrapper"+response.StatusCode);
                log.logDebugMessage("Auto Scrapper Response" + scrapperResponseDTO.Responseval);
                log.logDebugMessage("-------------Manual Scrapper Responseval ----------------" + scrapperResponseDTO.Responseval);
                log.logDebugMessage("-------------Manual Scrapper Called End ----------------");


                objdynamicobj.Add(scrapperResponseDTO);


                return objdynamicobj;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return objdynamicobj;

            }
        }

        public List<dynamic> GetMsiWorkOdersDetails(LoginDTO Model)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                log.logDebugMessage("-------------MSI Scrapper Called Start ----------------");
                log.logDebugMessage("---------MSI URL ----------------" + Model.URL);

                ScrapperResponseDTO scrapperResponseDTO = new ScrapperResponseDTO();

                string Schedulertime = System.Configuration.ConfigurationManager.AppSettings["AddSchedulertime"];
             
                string strURL = Model.URL;
                var client = new RestClient(strURL.Trim());
             
                client.Timeout = -1;

                log.logDebugMessage("--------- MSI username ----------------" + Model.username);
                log.logDebugMessage("--------- MSI rep_code ----------------" + Model.rep_code);
                log.logDebugMessage("---------MSI password ----------------" + Model.password);
                //log.logDebugMessage("--------- Manual image_download ----------------" + Model.image_download.ToString().ToLower());

                var request = new RestRequest("?username=" + Model.username + "&password=" + Model.password + "&rep_code=" + Model.rep_code + "&id=" + Model.WI_Pkey_ID + "", Method.GET);
                IRestResponse response = client.Execute(request);
                scrapperResponseDTO.Responseval = response.Content;
                log.logDebugMessage("-------------MSI Scrapper Called StatusCode ----------------" + response.StatusCode);
                log.logDebugMessage("-------------MSI Scrapper Called Responseval ----------------" + scrapperResponseDTO.Responseval);
                Model.Scrapper_rep_code = scrapperResponseDTO.Responseval;
                if (!string.IsNullOrWhiteSpace(scrapperResponseDTO.Responseval) && scrapperResponseDTO.Responseval == "1")
                {
                    DateTime dateTime = System.DateTime.Now.AddMinutes(Convert.ToInt32(Schedulertime) + 5);

                    log.logDebugMessage("MSI Fetch Data------------------>" + dateTime.ToString());

                    Avigma.Repository.Scheduler.JobSchedulerStart.Start(dateTime, Model);
                }
                log.logDebugMessage("-------------MSI Scrapper Called End ----------------");




                objdynamicobj.Add(scrapperResponseDTO);

                return objdynamicobj;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return objdynamicobj;

            }
        }

        //msi details by user 
        public List<dynamic> GetMsiWorkOdersDetailsByUser(LoginDTO Model)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                log.logDebugMessage("-------------MSI Manual Scrapper Called Start ----------------");
                log.logDebugMessage("---------MSI Manual URL ----------------" + Model.URL);

                ScrapperResponseDTO scrapperResponseDTO = new ScrapperResponseDTO();

                //// Delete Unprocess Data
                //ImportWorkOrderDTO.ImportFrom = 1;
                //ImportWorkOrderDTO.Type = 1;
                //ImportWorkOrderDTO.UserID = 0;
                //ImportWorkOrderData.DeleteWorkOrderAPIData(ImportWorkOrderDTO);

                //string strURL = System.Configuration.ConfigurationManager.AppSettings["PPW"];
                string strURL = Model.URL;
                var client = new RestClient(strURL.Trim());
               
                client.Timeout = -1;

                log.logDebugMessage("---------MSI Manual username ----------------" + Model.username);
                log.logDebugMessage("---------MSI Manual rep_code ----------------" + Model.rep_code);
                log.logDebugMessage("---------MSI Manual password ----------------" + Model.password);
                //log.logDebugMessage("--------- Manual image_download ----------------" + Model.image_download.ToString().ToLower());


                var request = new RestRequest("?username=" + Model.username + "&password=" + Model.password + "&rep_code=" + Model.rep_code + "&id=" + Model.WI_Pkey_ID + "", Method.GET);


                IRestResponse response = client.Execute(request);
                scrapperResponseDTO.Responseval = response.Content;
                log.logDebugMessage("-------------MSI Scrapper Called StatusCode ----------------" + response.StatusCode);
                log.logDebugMessage("-------------MSI Scrapper Called Responseval ----------------" + scrapperResponseDTO.Responseval);

                log.logDebugMessage("-------------Manual Scrapper Called End ----------------");




                objdynamicobj.Add(scrapperResponseDTO);
                return objdynamicobj;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return objdynamicobj;

            }
        }



        public List<dynamic> GetCyprexxWorkOdersDetails(LoginDTO Model)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                log.logDebugMessage("-------------Cyprexx Scrapper Called Start ----------------");
                log.logDebugMessage("---------Cyprexx URL ----------------" + Model.URL);

                ScrapperResponseDTO scrapperResponseDTO = new ScrapperResponseDTO();

                string Schedulertime = System.Configuration.ConfigurationManager.AppSettings["AddSchedulertime"];

                string strURL = Model.URL;
                var client = new RestClient(strURL.Trim());
              
                client.Timeout = -1;

                log.logDebugMessage("--------- Cyprexx username ----------------" + Model.username);
                log.logDebugMessage("---------Cyprexx password ----------------" + Model.password);
                //log.logDebugMessage("--------- Manual image_download ----------------" + Model.image_download.ToString().ToLower());


                var request = new RestRequest("?username=" + Model.username + "&password=" + Model.password + "&id = " + Model.WI_Pkey_ID + "", Method.GET);


                IRestResponse response = client.Execute(request);
                scrapperResponseDTO.Responseval = response.Content;

                log.logDebugMessage("-------------Cyprexx Scrapper Called StatusCode ----------------" + response.StatusCode);
                log.logDebugMessage("-------------Cyprexx Scrapper Called Responseval ----------------" + scrapperResponseDTO.Responseval);
                Model.Scrapper_rep_code = scrapperResponseDTO.Responseval;
                if (!string.IsNullOrWhiteSpace(scrapperResponseDTO.Responseval) && scrapperResponseDTO.Responseval == "1")
                {
                    DateTime dateTime = System.DateTime.Now.AddMinutes(Convert.ToInt32(Schedulertime) + 5);

                    log.logDebugMessage("Cyprexx Fetch Data------------------>" + dateTime.ToString());

                    Avigma.Repository.Scheduler.JobSchedulerStart.Start(dateTime, Model);
                }
                log.logDebugMessage("-------------Cyprexx Scrapper Called End ----------------");





                objdynamicobj.Add(scrapperResponseDTO);
                return objdynamicobj;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return objdynamicobj;

            }
        }

        //Cyprexx details by user 
        public List<dynamic> GetCyprexxWorkOdersDetailsByUser(LoginDTO Model)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                log.logDebugMessage("-------------Cyprexx Manual Scrapper Called Start ----------------");
                log.logDebugMessage("---------Cyprexx Manual URL ----------------" + Model.URL);

                ScrapperResponseDTO scrapperResponseDTO = new ScrapperResponseDTO();

                string strURL = Model.URL;
                var client = new RestClient(strURL.Trim());
             
                client.Timeout = -1;

                log.logDebugMessage("---------Cyprexx Manual username ----------------" + Model.username);
                log.logDebugMessage("---------Cyprexx Manual password ----------------" + Model.password);

                var request = new RestRequest("?username=" + Model.username + "&password=" + Model.password + "&id = " + Model.WI_Pkey_ID + "", Method.GET);

                IRestResponse response = client.Execute(request);
                scrapperResponseDTO.Responseval = response.Content;
                log.logDebugMessage("-------------Cyprexx Scrapper Called StatusCode ----------------" + response.StatusCode);
                log.logDebugMessage("-------------Cyprexx Scrapper Called Responseval ----------------" + scrapperResponseDTO.Responseval);

                log.logDebugMessage("-------------Manual Scrapper Called End ----------------");




                objdynamicobj.Add(scrapperResponseDTO);
                return objdynamicobj;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return objdynamicobj;

            }
        }


        //ServiceLink details by user

        public List<dynamic> GetServiceLinkWorkOdersDetails(LoginDTO Model)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                log.logDebugMessage("-------------ServiceLink Scrapper Called Start ----------------");
                log.logDebugMessage("---------ServiceLink URL ----------------" + Model.URL);

                ScrapperResponseDTO scrapperResponseDTO = new ScrapperResponseDTO();

                string Schedulertime = System.Configuration.ConfigurationManager.AppSettings["AddSchedulertime"];

                string strURL = Model.URL;
                var client = new RestClient(strURL.Trim());

                client.Timeout = -1;

                log.logDebugMessage("--------- ServiceLink username ----------------" + Model.username);
                log.logDebugMessage("---------ServiceLink password ----------------" + Model.password);
                //log.logDebugMessage("--------- Manual image_download ----------------" + Model.image_download.ToString().ToLower());


                var request = new RestRequest("?username=" + Model.username + "&password=" + Model.password + "&id = " + Model.WI_Pkey_ID + "", Method.GET);


                IRestResponse response = client.Execute(request);
                scrapperResponseDTO.Responseval = response.Content;

                log.logDebugMessage("-------------ServiceLink Scrapper Called StatusCode ----------------" + response.StatusCode);
                log.logDebugMessage("-------------ServiceLink Scrapper Called Responseval ----------------" + scrapperResponseDTO.Responseval);
                
                Model.Scrapper_rep_code = scrapperResponseDTO.Responseval;
                if (!string.IsNullOrWhiteSpace(scrapperResponseDTO.Responseval) && scrapperResponseDTO.Responseval == "1")
                {
                    DateTime dateTime = System.DateTime.Now.AddMinutes(Convert.ToInt32(Schedulertime) + 5);

                    log.logDebugMessage("ServiceLink Fetch Data------------------>" + dateTime.ToString());

                    Avigma.Repository.Scheduler.JobSchedulerStart.Start(dateTime, Model);
                }

                log.logDebugMessage("-------------ServiceLink Scrapper Called End ----------------");





                objdynamicobj.Add(scrapperResponseDTO);
                return objdynamicobj;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return objdynamicobj;

            }
        }

        public List<dynamic> GetServiceLinkWorkOdersDetailsByUser(LoginDTO Model)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                log.logDebugMessage("-------------ServiceLink Manual Scrapper Called Start ----------------");
                log.logDebugMessage("---------ServiceLink Manual URL ----------------" + Model.URL);

                ScrapperResponseDTO scrapperResponseDTO = new ScrapperResponseDTO();

                string strURL = Model.URL;
                var client = new RestClient(strURL.Trim());

                client.Timeout = -1;

                log.logDebugMessage("---------ServiceLink Manual username ----------------" + Model.username);
                log.logDebugMessage("---------ServiceLink Manual password ----------------" + Model.password);

                var request = new RestRequest("?username=" + Model.username + "&password=" + Model.password + "&id = " + Model.WI_Pkey_ID + "", Method.GET);

                IRestResponse response = client.Execute(request);
                scrapperResponseDTO.Responseval = response.Content;
                log.logDebugMessage("-------------ServiceLink Scrapper Called StatusCode ----------------" + response.StatusCode);
                log.logDebugMessage("-------------ServiceLink Scrapper Called Responseval ----------------" + scrapperResponseDTO.Responseval);

                log.logDebugMessage("-------------Manual Scrapper Called End ----------------");




                objdynamicobj.Add(scrapperResponseDTO);
                return objdynamicobj;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return objdynamicobj;

            }
        }



        public List<dynamic> GetWorkOdersDetails(LoginDTO model , int Flag)
        {
            model.token = model.password;
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                log.logDebugMessage("Purvan API Called ");

                // Delete Unprocess Data
                ImportWorkOrderDTO.ImportFrom = 2;
                ImportWorkOrderDTO.Type = Flag;
                ImportWorkOrderDTO.UserID = 0;
                ImportWorkOrderDTO.WI_Pkey_ID = model.WI_Pkey_ID;
                ImportWorkOrderData.DeleteWorkOrderAPIData(ImportWorkOrderDTO);

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                var client = new RestClient("https://www.direct.pruvan.com/ps2/getWorkOrders.html");
                //var client = new RestClient(Model.URL);
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Connection", "keep-alive");
                request.AddHeader("content-length", "99");
                request.AddHeader("accept-encoding", "gzip, deflate");
                request.AddHeader("Host", "www.direct.pruvan.com");
                request.AddHeader("Postman-Token", "d41379fa-fe1a-4a8e-b980-f06b5a2b6c4f,fe0023ba-c60a-44a7-8e1a-c64285d08e85");
                request.AddHeader("Cache-Control", "no-cache");
                request.AddHeader("Accept", "*/*");
                request.AddHeader("User-Agent", "PostmanRuntime/7.15.0");
                request.AddHeader("User-Agent", "PostmanRuntime/7.15.0");
                request.AddHeader("Content-Type", "application/x-www-form-urlencoded");//Willwebhooks  //Webhooks2019
                request.AddParameter("undefined", "payload=%7B%0A%22username%22%3A%20%22" + model.username + "%22%2C%0A%22token%22%3A%20%22" + model.token + "%22%0A%7D", ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);

                string val = Convert.ToString(response.Content);

                var Data = JsonConvert.DeserializeObject<RootObject>(val);
                // here update query
                log.logDebugMessage("Purvan API Called count --------------->" + Data.error);
                if (Data.error != "invalid username or password")
                {
                    log.logDebugMessage("Purvan API Called count --------------->"+ Data.workOrders.Count);

                    for (int i = 0; i < Data.workOrders.Count; i++)
                    {
                        PruvanWorkOrderDTO workOrderxDTO = new PruvanWorkOrderDTO();
                        workOrderxDTO.address1 = Data.workOrders[i].address1;
                        workOrderxDTO.address2 = Data.workOrders[i].address2;
                        workOrderxDTO.attribute7 = Data.workOrders[i].attribute7;
                        workOrderxDTO.attribute8 = Data.workOrders[i].attribute8;
                        workOrderxDTO.attribute9 = Data.workOrders[i].attribute9;
                        workOrderxDTO.attribute10 = Data.workOrders[i].attribute10;
                        workOrderxDTO.attribute11 = Data.workOrders[i].attribute11;
                        workOrderxDTO.attribute12 = Data.workOrders[i].attribute12;
                        workOrderxDTO.attribute13 = Data.workOrders[i].attribute13;
                        workOrderxDTO.attribute14 = Data.workOrders[i].attribute14;
                        workOrderxDTO.attribute15 = Data.workOrders[i].attribute15;
                        workOrderxDTO.city = Data.workOrders[i].city;
                        workOrderxDTO.clientInstructions = Data.workOrders[i].clientInstructions;
                        workOrderxDTO.clientStatus = Data.workOrders[i].clientStatus;
                        //workOrderxDTO.controlConfig = Data.workOrders[1].controlConfig;
                        workOrderxDTO.country = Data.workOrders[i].country;
                        workOrderxDTO.description = Data.workOrders[i].description;
                        workOrderxDTO.reference = Data.workOrders[i].reference;
                        workOrderxDTO.source_wo_id = Convert.ToInt64(Data.workOrders[i].source_wo_id);
                        workOrderxDTO.source_wo_number = Data.workOrders[i].source_wo_number;
                        workOrderxDTO.source_wo_provider = Data.workOrders[i].source_wo_provider;
                        workOrderxDTO.state = Data.workOrders[i].state;
                        workOrderxDTO.status = Data.workOrders[i].status;
                        workOrderxDTO.workOrderInfo = Data.workOrders[i].workOrderInfo;
                        workOrderxDTO.workOrderNumber = Data.workOrders[i].workOrderNumber;
                        if (!string.IsNullOrEmpty(Data.workOrders[i].zip))
                        {
                            int index = Data.workOrders[i].zip.IndexOf("-");
                            if (index > 0)
                                Data.workOrders[i].zip = Data.workOrders[i].zip.Substring(0, index);
                        }
                        workOrderxDTO.zip = Convert.ToInt64(Data.workOrders[i].zip);

                        workOrderxDTO.dueDate = new DateTime(1970, 1, 1, 0, 0, 0, 0).AddSeconds(Data.workOrders[i].dueDate.GetValueOrDefault()).ToLocalTime();
                        workOrderxDTO.Type = 1;
                        workOrderxDTO.IsActive = true;
                        workOrderxDTO.IsDelete = false;
                        workOrderxDTO.IsProcess = false;
                        workOrderxDTO.WorkOrder_Import_FkeyId = model.WI_Pkey_ID;
                        var WorkOrdePkey = workOrder_Puran_MasterData.AddWorkorderPuranData(workOrderxDTO);
                        //var WorkOrdePkey = workOrderMasterData.AddWorkorderData(workOrderxDTO);

                        if (WorkOrdePkey[0] != 0 && WorkOrdePkey[1] == 1 && Data.workOrders[i].services != null && Data.workOrders[i].services.Count > 0)
                        {
                            ServiceDTO serviceDTO = new ServiceDTO();

                            for (int k = 0; k < Data.workOrders[i].services.Count; k++)
                            {

                                serviceDTO.workOrder_ID = Convert.ToInt64(WorkOrdePkey[0]);
                                serviceDTO.attribute1 = Data.workOrders[i].services[k].attribute1;
                                serviceDTO.attribute2 = Data.workOrders[i].services[k].attribute2;
                                serviceDTO.attribute3 = Data.workOrders[i].services[k].attribute3;
                                serviceDTO.attribute4 = Data.workOrders[i].services[k].attribute4;
                                serviceDTO.attribute5 = Data.workOrders[i].services[k].attribute5;
                                serviceDTO.attribute6 = Data.workOrders[i].services[k].attribute6;
                                serviceDTO.attribute7 = Data.workOrders[i].services[k].attribute7;
                                serviceDTO.attribute8 = Data.workOrders[i].services[k].attribute8;
                                serviceDTO.attribute9 = Data.workOrders[i].services[k].attribute9;
                                serviceDTO.attribute10 = Data.workOrders[i].services[k].attribute10;
                                serviceDTO.attribute11 = Data.workOrders[i].services[k].attribute11;
                                serviceDTO.attribute12 = Data.workOrders[i].services[k].attribute12;
                                serviceDTO.attribute13 = Data.workOrders[i].services[k].attribute13;
                                serviceDTO.attribute14 = Data.workOrders[i].services[k].attribute14;
                                serviceDTO.attribute15 = Data.workOrders[i].services[k].attribute15;
                                serviceDTO.instructions = Data.workOrders[i].services[k].instructions;
                                serviceDTO.IsActive = true;
                                if (!string.IsNullOrEmpty(Data.workOrders[i].services[k].source_service_id))
                                {
                                    serviceDTO.serviceID = Convert.ToInt64(Data.workOrders[i].services[k].source_service_id);
                                }
                                serviceDTO.source_service = Data.workOrders[i].services[k].source_service;
                                serviceDTO.survey = Data.workOrders[i].services[k].survey;
                                if (!string.IsNullOrEmpty(Data.workOrders[i].services[k].survey))
                                {
                                    serviceDTO.survey = Data.workOrders[i].services[k].survey;
                                }
                                serviceDTO.Type = 1;
                                serviceDTO.serviceName = Data.workOrders[i].services[k].serviceName;
                                serviceDTO.IsDelete = false;
                                serviceDTO.IsProcess = false;

                                var ServicePkey = serviceMasterData.AddServiceData(serviceDTO);

                                if (!string.IsNullOrEmpty(Data.workOrders[i].services[k].surveyDynamic) && !string.IsNullOrEmpty(ServicePkey[0].serviceID))
                                {
                                    //SurveyDynamicDTO surveyDynamic = new SurveyDynamicDTO();// parent table 

                                    Survey_Question_MasterDTO surveyDynamic = new Survey_Question_MasterDTO();

                                    var Datax = JsonConvert.DeserializeObject<ServicesRootUI>(Data.workOrders[i].services[k].surveyDynamic);

                                    for (int j = 0; j < Datax.questions.Count; j++)
                                    {
                                        surveyDynamic.que_serviceID = Convert.ToInt64(ServicePkey[0].serviceID);
                                        surveyDynamic.que_anonymous = Datax.questions[j].anonymous;
                                        // surveyDynamic.answerEnables = Convert.ToString(Datax.questions[j].answerEnables);
                                        //surveyDynamic.answerRequiresPics = Convert.ToString(Datax.questions[j].answerRequiresPics);
                                        //surveyDynamic.answerType = Datax.questions[j].answerType;
                                        //surveyDynamic.choices = Convert.ToString(JsonConvert.SerializeObject(Datax.questions[j].choices));
                                        //surveyDynamic.commentEnabled = Datax.questions[j].commentEnabled;
                                        surveyDynamic.que_enabledByDefault = Datax.questions[j].enabledByDefault;
                                        surveyDynamic.que_enableTotal = Datax.questions[j].enableTotal;
                                        surveyDynamic.que_expand = Datax.questions[j].expand;
                                        //surveyDynamic.hint = Datax.questions[j].hint;
                                        surveyDynamic.que_id = Datax.questions[j].id;
                                        //surveyDynamic.mode = Datax.questions[j].mode;
                                        //surveyDynamic.modeOptions = Convert.ToString(Datax.questions[j].modeOptions);
                                        surveyDynamic.que_name = Datax.questions[j].name;
                                        //surveyDynamic.options = Convert.ToString(JsonConvert.SerializeObject(Datax.questions[j].options));// comment this 
                                        surveyDynamic.que_prompt = Datax.questions[j].prompt;
                                        //surveyDynamic.question = Datax.questions[j].question;
                                        //surveyDynamic.questions = Convert.ToString(Datax.questions[j].questions);
                                        surveyDynamic.que_repeat = Convert.ToString(Datax.questions[j].repeat);
                                        //surveyDynamic.required = Convert.ToString(Datax.questions[j].required);

                                        surveyDynamic.que_uuid = Datax.questions[j].uuid;
                                        surveyDynamic.que_type = Datax.questions[j].type;
                                        surveyDynamic.Type = 1;
                                        surveyDynamic.que_IsActive = true;

                                        // peky set
                                        var parentquestionPkey = survey_Question_MasterData.AddIPL_Survey_Question(surveyDynamic);


                                        if (Datax.questions[j].questions != null)
                                        {
                                            if (parentquestionPkey[0].Status == "1")
                                            {
                                                //ChildquestionDTO childquestionDTO = new ChildquestionDTO();
                                                Survey_CHQuestion_MasterDTO childquestionDTO = new Survey_CHQuestion_MasterDTO();
                                                for (int c = 0; c < Datax.questions[j].questions.Count; c++)
                                                {
                                                    childquestionDTO.ParentSur_que_pkeyId = parentquestionPkey[0].que_pkey_Id;
                                                    //childquestionDTO.Sur_que_answerRequiresPics = 
                                                    //Sur_que_enabledByDefault

                                                    //childquestionDTO.hint = Datax.questions[j].questions[c].hint;
                                                    childquestionDTO.Sur_que_answerEnables = Convert.ToString(Datax.questions[j].questions[c].answerEnables);
                                                    childquestionDTO.Sur_que_answerType = Datax.questions[j].questions[c].answerType;
                                                    childquestionDTO.Sur_que_choices = Convert.ToString(JsonConvert.SerializeObject(Datax.questions[j].questions[c].choices));
                                                    childquestionDTO.Sur_que_commentEnabled = Datax.questions[j].questions[c].commentEnabled;
                                                    childquestionDTO.Sur_que_id = Datax.questions[j].questions[c].id;
                                                    childquestionDTO.Sur_que_mode = Datax.questions[j].questions[c].mode;
                                                    childquestionDTO.Sur_que_modeOptions = Convert.ToString(Datax.questions[j].questions[c].modeOptions);
                                                    childquestionDTO.Sur_que_name = Datax.questions[j].questions[c].name;
                                                    childquestionDTO.Sur_que_options = Convert.ToString(JsonConvert.SerializeObject(Datax.questions[j].questions[c].options));
                                                    childquestionDTO.Sur_que_question = Datax.questions[j].questions[c].question;
                                                    childquestionDTO.Sur_que_required = Datax.questions[j].questions[c].required;
                                                    childquestionDTO.Sur_que_text = Datax.questions[j].questions[c].text;
                                                    childquestionDTO.Sur_que_uuid = Datax.questions[j].questions[c].uuid;
                                                    childquestionDTO.Sur_que_IsActive = true;
                                                    childquestionDTO.Type = 1;

                                                    var returnKey = survey_CHQuestion_MasterData.AddIPL_ChildMeta_Data(childquestionDTO);


                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        objdynamicobj.Add(workOrderxDTO);

                        if (Flag == 2)
                        {
                            Import_Queue_TransData import_Queue_TransData = new Import_Queue_TransData();
                            Import_Queue_Trans_DTO import_Queue_Trans_DTO = new Import_Queue_Trans_DTO();
                            import_Queue_Trans_DTO.Imrt_Wo_Import_ID = model.WI_Pkey_ID;
                            //import_Queue_Trans_DTO.Imrt_Wo_ID = strworkOrder;
                            import_Queue_Trans_DTO.Type = 1;
                            import_Queue_Trans_DTO.Imrt_Import_From_ID = 2;
                            import_Queue_TransData.UpdateAutoImportQueueTransaction(import_Queue_Trans_DTO);
                            log.logDebugMessage("---------------Purvan API Called End----------------");
                        }



                    }
                }

                return objdynamicobj;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return objdynamicobj;

            }

        }

        public List<dynamic> GetFBWorkOdersDetails(LoginDTO model, int Flag)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            ScrapperResponseDTO scrapperResponseDTO = new ScrapperResponseDTO();
            try
            {
                log.logDebugMessage("FB API Called  ");
                ImportWorkOrderDTO.ImportFrom = 4;
                ImportWorkOrderDTO.Type = Flag;
                ImportWorkOrderDTO.UserID = 0;
                ImportWorkOrderDTO.WI_Pkey_ID = model.WI_Pkey_ID;
                ImportWorkOrderData.DeleteWorkOrderAPIData(ImportWorkOrderDTO);


                FBAPI_RequestDTO fBAPI_RequestDTO = new FBAPI_RequestDTO();
                fBAPI_RequestDTO.AuthUserName = model.FBUsername;
                fBAPI_RequestDTO.AuthPassword = model.FBPassword;
                fBAPI_RequestDTO.ConUserName = model.username;
                fBAPI_RequestDTO.ConPassword = model.password;
                fBAPI_RequestDTO.UserID = model.UserID;
                fBAPI_RequestDTO.WI_Pkey_ID = model.WI_Pkey_ID;
                fBAPI_RequestDTO.ApiUrl = model.URL;



                var obiData = workOrder_FiveBrother_MasterData.AddOrders(fBAPI_RequestDTO);

                if (Flag == 2)
                {
                    
                    Import_Queue_TransData import_Queue_TransData = new Import_Queue_TransData();
                    Import_Queue_Trans_DTO import_Queue_Trans_DTO = new Import_Queue_Trans_DTO();
                    if (obiData.Count == 1)
                    {
                        import_Queue_Trans_DTO.Imrt_Status_Msg = ((ScrapperResponseDTO)obiData[0]).Responseval;
                    }
                    import_Queue_Trans_DTO.Imrt_Wo_Import_ID = model.WI_Pkey_ID;
                    //import_Queue_Trans_DTO.Imrt_Wo_ID = strworkOrder;
                    import_Queue_Trans_DTO.Type = 1;
                    import_Queue_Trans_DTO.Imrt_Import_From_ID = 4;
                    import_Queue_TransData.UpdateAutoImportQueueTransaction(import_Queue_Trans_DTO);
                    log.logDebugMessage("---------------FB API  Called End----------------");
                }

                return obiData;
            }
            catch (Exception ex)
            {
                scrapperResponseDTO.Responseval = "3";
                objdynamicobj.Add(scrapperResponseDTO);
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return objdynamicobj;

            }

        }
        #region comment

        //public List<dynamic> GetMSIWorkOdersDetails(LoginDTO model, int Flag)
        //{
        //    List<dynamic> objdynamicobj = new List<dynamic>();
        //    try
        //    {
        //        log.logDebugMessage("MSI API Called  ");
        //        ImportWorkOrderDTO.ImportFrom = 3;
        //        ImportWorkOrderDTO.Type = Flag;
        //        ImportWorkOrderDTO.UserID = 0;
        //        ImportWorkOrderDTO.WI_Pkey_ID = model.WI_Pkey_ID;
        //        ImportWorkOrderData.DeleteWorkOrderAPIData(ImportWorkOrderDTO);


        //        FBAPI_RequestDTO fBAPI_RequestDTO = new FBAPI_RequestDTO();
        //        fBAPI_RequestDTO.AuthUserName = model.FBUsername;
        //        fBAPI_RequestDTO.AuthPassword = model.FBPassword;
        //        fBAPI_RequestDTO.ConUserName = model.username;
        //        fBAPI_RequestDTO.ConPassword = model.password;
        //        fBAPI_RequestDTO.UserID = model.UserID;
        //        fBAPI_RequestDTO.WI_Pkey_ID = model.WI_Pkey_ID;
        //        fBAPI_RequestDTO.ApiUrl = model.URL;


        //        var obiData = workOrder_MSI_MasterData.AddMSIOrders(fBAPI_RequestDTO);

        //        if (Flag == 2)
        //        {
        //            Import_Queue_TransData import_Queue_TransData = new Import_Queue_TransData();
        //            Import_Queue_Trans_DTO import_Queue_Trans_DTO = new Import_Queue_Trans_DTO();
        //            import_Queue_Trans_DTO.Imrt_Wo_Import_ID = model.WI_Pkey_ID;
        //            //import_Queue_Trans_DTO.Imrt_Wo_ID = strworkOrder;
        //            import_Queue_Trans_DTO.Type = 1;
        //            import_Queue_Trans_DTO.Imrt_Import_From_ID = 7;
        //            import_Queue_TransData.UpdateAutoImportQueueTransaction(import_Queue_Trans_DTO);
        //            log.logDebugMessage("---------------NFR API Called End----------------");
        //        }

        //        return obiData;
        //    }
        //    catch (Exception ex)
        //    {
        //        log.logErrorMessage(ex.StackTrace);
        //        log.logErrorMessage(ex.Message);
        //        return objdynamicobj;

        //    }

        //}

        ///////////////////////Added by Mohit 
        ///
        //public List<dynamic> GetNFRWorkOdersDetails(LoginDTO model, int Flag)
        //{
        //    List<dynamic> objdynamicobj = new List<dynamic>();
        //    try
        //    {
        //        log.logDebugMessage("NFR API Called  ");
        //        ImportWorkOrderDTO.ImportFrom = 7;
        //        ImportWorkOrderDTO.Type = Flag;
        //        ImportWorkOrderDTO.UserID = 0;
        //        ImportWorkOrderDTO.WI_Pkey_ID = model.WI_Pkey_ID;
        //        ImportWorkOrderData.DeleteWorkOrderAPIData(ImportWorkOrderDTO);


        //        FBAPI_RequestDTO fBAPI_RequestDTO = new FBAPI_RequestDTO();
        //        fBAPI_RequestDTO.AuthUserName = model.FBUsername;
        //        fBAPI_RequestDTO.AuthPassword = model.FBPassword;
        //        fBAPI_RequestDTO.ConUserName = model.username;
        //        fBAPI_RequestDTO.ConPassword = model.password;
        //        fBAPI_RequestDTO.UserID = model.UserID;
        //        fBAPI_RequestDTO.WI_Pkey_ID = model.WI_Pkey_ID;
        //        fBAPI_RequestDTO.ApiUrl = model.URL;


        //        var obiData = workOrder_NFR_MasterData.AddNFROrders(fBAPI_RequestDTO);

        //        if (Flag == 2)
        //        {
        //            Import_Queue_TransData import_Queue_TransData = new Import_Queue_TransData();
        //            Import_Queue_Trans_DTO import_Queue_Trans_DTO = new Import_Queue_Trans_DTO();
        //            import_Queue_Trans_DTO.Imrt_Wo_Import_ID = model.WI_Pkey_ID;
        //            //import_Queue_Trans_DTO.Imrt_Wo_ID = strworkOrder;
        //            import_Queue_Trans_DTO.Type = 1;
        //            import_Queue_Trans_DTO.Imrt_Import_From_ID = 7;
        //            import_Queue_TransData.UpdateAutoImportQueueTransaction(import_Queue_Trans_DTO);
        //            log.logDebugMessage("---------------NFR API Called End----------------");
        //        }

        //        return obiData;
        //    }
        //    catch (Exception ex)
        //    {
        //        log.logErrorMessage(ex.StackTrace);
        //        log.logErrorMessage(ex.Message);
        //        return objdynamicobj;

        //    }

        //}
        #endregion

        public List<dynamic> GetNFRWorkOdersDetails(LoginDTO Model)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                log.logDebugMessage("-------------NFR Scrapper Called Start ----------------");
                log.logDebugMessage("---------NFR URL ----------------" + Model.URL);

                ScrapperResponseDTO scrapperResponseDTO = new ScrapperResponseDTO();
                string Schedulertime = System.Configuration.ConfigurationManager.AppSettings["AddSchedulertime"];

                string strURL = Model.URL;
                var client = new RestClient(strURL.Trim());
               
                client.Timeout = -1;

                log.logDebugMessage("--------- NFR username ----------------" + Model.username);
                log.logDebugMessage("--------- NFR nfr_id ----------------" + Model.rep_code);
                log.logDebugMessage("---------NFR password ----------------" + Model.password);
                //log.logDebugMessage("--------- Manual image_download ----------------" + Model.image_download.ToString().ToLower());


                var request = new RestRequest("?username=" + Model.username + "&password=" + Model.password + "&nfr_id=" + Model.rep_code + "&id=" + Model.WI_Pkey_ID + "", Method.GET);




                IRestResponse response = client.Execute(request);
                scrapperResponseDTO.Responseval = response.Content;

                log.logDebugMessage("-------------NFR Scrapper Called StatusCode ----------------" + response.StatusCode);
                log.logDebugMessage("-------------NFR Scrapper Called Responseval ----------------" + scrapperResponseDTO.Responseval);
                Model.Scrapper_rep_code = scrapperResponseDTO.Responseval;
                if (!string.IsNullOrWhiteSpace(scrapperResponseDTO.Responseval) && scrapperResponseDTO.Responseval == "1")
                {
                    DateTime dateTime = System.DateTime.Now.AddMinutes(Convert.ToInt32(Schedulertime) + 5);

                    log.logDebugMessage("NFR Fetch Data------------------>" + dateTime.ToString());

                    Avigma.Repository.Scheduler.JobSchedulerStart.Start(dateTime, Model);
                }
                log.logDebugMessage("-------------NFR Scrapper Called End ----------------");



                objdynamicobj.Add(scrapperResponseDTO);


                return objdynamicobj;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return objdynamicobj;

            }
        }

        //msi details by user 
        public List<dynamic> GetNFRWorkOdersDetailsByUser(LoginDTO Model)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                log.logDebugMessage("-------------NFR Manual Scrapper Called Start ----------------");
                log.logDebugMessage("---------NFR Manual URL ----------------" + Model.URL);

                ScrapperResponseDTO scrapperResponseDTO = new ScrapperResponseDTO();

                string strURL = Model.URL;                                                                                  
                var client = new RestClient(strURL.Trim());
                
                client.Timeout = -1;

                log.logDebugMessage("---------NFR Manual username ----------------" + Model.username);
                log.logDebugMessage("---------NFR Manual password ----------------" + Model.password);
                log.logDebugMessage("---------NFR Manual rep_code ----------------" + Model.rep_code);


                var request = new RestRequest("?username=" + Model.username + "&password=" + Model.password + "&nfr_id=" + Model.rep_code + "&id=" + Model.WI_Pkey_ID + "", Method.GET);



                IRestResponse response = client.Execute(request);
                scrapperResponseDTO.Responseval = response.Content;


                log.logDebugMessage("-------------Manual Scrapper Called End ----------------");
                log.logDebugMessage("-------------NFR Scrapper Called StatusCode ----------------" + response.StatusCode);
                log.logDebugMessage("-------------NFR Scrapper Called Responseval ----------------" + scrapperResponseDTO.Responseval);



                objdynamicobj.Add(scrapperResponseDTO);

                return objdynamicobj;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return objdynamicobj;

            }
        }

        //MCS DETAILS BY USER
        public List<dynamic> GetMCSWorkOdersDetails(LoginDTO Model)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                log.logDebugMessage("-------------MCS Scrapper Called Start ----------------");
                log.logDebugMessage("---------MCS URL ----------------" + Model.URL);

                ScrapperResponseDTO scrapperResponseDTO = new ScrapperResponseDTO();
                string Schedulertime = System.Configuration.ConfigurationManager.AppSettings["AddSchedulertime"];

                string strURL = Model.URL;
                var client = new RestClient(strURL.Trim());

                client.Timeout = -1;

                log.logDebugMessage("--------- MCS username ----------------" + Model.username);
                log.logDebugMessage("--------- MCS nfr_id ----------------" + Model.rep_code);
                log.logDebugMessage("---------MCS password ----------------" + Model.password);
                //log.logDebugMessage("--------- Manual image_download ----------------" + Model.image_download.ToString().ToLower());


                var request = new RestRequest("?username=" + Model.username + "&password=" + Model.password + "&nfr_id=" + Model.rep_code + "&id=" + Model.WI_Pkey_ID + "", Method.GET);




                IRestResponse response = client.Execute(request);
                scrapperResponseDTO.Responseval = response.Content;

                log.logDebugMessage("-------------MCS Scrapper Called StatusCode ----------------" + response.StatusCode);
                log.logDebugMessage("-------------MCS Scrapper Called Responseval ----------------" + scrapperResponseDTO.Responseval);
                Model.Scrapper_rep_code = scrapperResponseDTO.Responseval;
                if (!string.IsNullOrWhiteSpace(scrapperResponseDTO.Responseval) && scrapperResponseDTO.Responseval == "1")
                {
                    DateTime dateTime = System.DateTime.Now.AddMinutes(Convert.ToInt32(Schedulertime) + 5);

                    log.logDebugMessage("MCS Fetch Data------------------>" + dateTime.ToString());

                    Avigma.Repository.Scheduler.JobSchedulerStart.Start(dateTime, Model);

                }
                log.logDebugMessage("-------------MCS Scrapper Called End ----------------");


                objdynamicobj.Add(scrapperResponseDTO);


                return objdynamicobj;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return objdynamicobj;

            }
        }

        public List<dynamic> GetMCSWorkOdersDetailsByUser(LoginDTO Model)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                log.logDebugMessage("-------------MCS Manual Scrapper Called Start ----------------");
                log.logDebugMessage("---------MCS Manual URL ----------------" + Model.URL);
                
                ScrapperResponseDTO scrapperResponseDTO = new ScrapperResponseDTO();
               
                string strURL = Model.URL;
                var client = new RestClient(strURL.Trim());

                client.Timeout = -1;

                log.logDebugMessage("---------MCS Manual username ----------------" + Model.username);
                log.logDebugMessage("---------MCS Manual password ----------------" + Model.password);
                log.logDebugMessage("---------MCS Manual rep_code ----------------" + Model.rep_code);


                var request = new RestRequest("?username=" + Model.username + "&password=" + Model.password + "&nfr_id=" + Model.rep_code + "&id=" + Model.WI_Pkey_ID + "", Method.GET);



                IRestResponse response = client.Execute(request);
                scrapperResponseDTO.Responseval = response.Content;

                log.logDebugMessage("-------------Manual Scrapper Called End ----------------");
                log.logDebugMessage("-------------MCS Scrapper Called StatusCode ----------------" + response.StatusCode);
                log.logDebugMessage("-------------MCS Scrapper Called Responseval ----------------" + scrapperResponseDTO.Responseval);

                objdynamicobj.Add(scrapperResponseDTO);


                return objdynamicobj;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return objdynamicobj;

            }
        }


        //AG DETAILS BY USER

        public List<dynamic> GetAGWorkOdersDetails(LoginDTO Model)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                log.logDebugMessage("-------------AG Scrapper Called Start ----------------");
                log.logDebugMessage("---------AG URL ----------------" + Model.URL);

                ScrapperResponseDTO scrapperResponseDTO = new ScrapperResponseDTO();
                string Schedulertime = System.Configuration.ConfigurationManager.AppSettings["AddSchedulertime"];

                string strURL = Model.URL;
                var client = new RestClient(strURL.Trim());

                client.Timeout = -1;

                log.logDebugMessage("--------- AG username ----------------" + Model.username);
                log.logDebugMessage("--------- AG nfr_id ----------------" + Model.rep_code);
                log.logDebugMessage("---------AG password ----------------" + Model.password);
                //log.logDebugMessage("--------- Manual image_download ----------------" + Model.image_download.ToString().ToLower());


                var request = new RestRequest("?username=" + Model.username + "&password=" + Model.password + "&nfr_id=" + Model.rep_code + "&id=" + Model.WI_Pkey_ID + "", Method.GET);




                IRestResponse response = client.Execute(request);
                scrapperResponseDTO.Responseval = response.Content;

                log.logDebugMessage("-------------AG Scrapper Called StatusCode ----------------" + response.StatusCode);
                log.logDebugMessage("-------------AG Scrapper Called Responseval ----------------" + scrapperResponseDTO.Responseval);
                Model.Scrapper_rep_code = scrapperResponseDTO.Responseval;
                if (!string.IsNullOrWhiteSpace(scrapperResponseDTO.Responseval) && scrapperResponseDTO.Responseval == "1")
                {
                    DateTime dateTime = System.DateTime.Now.AddMinutes(Convert.ToInt32(Schedulertime) + 5);

                    log.logDebugMessage("AG Fetch Data------------------>" + dateTime.ToString());

                    Avigma.Repository.Scheduler.JobSchedulerStart.Start(dateTime, Model);

                }
                log.logDebugMessage("-------------AG Scrapper Called End ----------------");


                objdynamicobj.Add(scrapperResponseDTO);


                return objdynamicobj;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return objdynamicobj;

            }
        }

        public List<dynamic> GetAGWorkOdersDetailsByUser(LoginDTO Model)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                log.logDebugMessage("-------------AG Manual Scrapper Called Start ----------------");
                log.logDebugMessage("---------AG Manual URL ----------------" + Model.URL);

                ScrapperResponseDTO scrapperResponseDTO = new ScrapperResponseDTO();

                string strURL = Model.URL;
                var client = new RestClient(strURL.Trim());

                client.Timeout = -1;

                log.logDebugMessage("---------AG Manual username ----------------" + Model.username);
                log.logDebugMessage("---------AG Manual password ----------------" + Model.password);
                log.logDebugMessage("---------AG Manual rep_code ----------------" + Model.rep_code);


                var request = new RestRequest("?username=" + Model.username + "&password=" + Model.password + "&nfr_id=" + Model.rep_code + "&id=" + Model.WI_Pkey_ID + "", Method.GET);



                IRestResponse response = client.Execute(request);
                scrapperResponseDTO.Responseval = response.Content;

                log.logDebugMessage("-------------Manual Scrapper Called End ----------------");
                log.logDebugMessage("-------------AG Scrapper Called StatusCode ----------------" + response.StatusCode);
                log.logDebugMessage("-------------AG Scrapper Called Responseval ----------------" + scrapperResponseDTO.Responseval);

                objdynamicobj.Add(scrapperResponseDTO);


                return objdynamicobj;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return objdynamicobj;

            }
        }

        // Altisource Details By User

        public List<dynamic> GetAltisourceWorkOdersDetails(LoginDTO Model)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                log.logDebugMessage("-------------Altisource Scrapper Called Start ----------------");
                log.logDebugMessage("---------Altisource URL ----------------" + Model.URL);

                ScrapperResponseDTO scrapperResponseDTO = new ScrapperResponseDTO();

                string Schedulertime = System.Configuration.ConfigurationManager.AppSettings["AddSchedulertime"];

                string strURL = Model.URL;
                var client = new RestClient(strURL.Trim());

                client.Timeout = -1;

                log.logDebugMessage("--------- Altisource username ----------------" + Model.username);
                log.logDebugMessage("---------Altisource password ----------------" + Model.password);
                //log.logDebugMessage("--------- Manual image_download ----------------" + Model.image_download.ToString().ToLower());


                var request = new RestRequest("?username=" + Model.username + "&password=" + Model.password + "&id = " + Model.WI_Pkey_ID + "", Method.GET);


                IRestResponse response = client.Execute(request);
                scrapperResponseDTO.Responseval = response.Content;

                log.logDebugMessage("-------------Altisource Scrapper Called StatusCode ----------------" + response.StatusCode);
                log.logDebugMessage("-------------Altisource Scrapper Called Responseval ----------------" + scrapperResponseDTO.Responseval);
                Model.Scrapper_rep_code = scrapperResponseDTO.Responseval;
                if (!string.IsNullOrWhiteSpace(scrapperResponseDTO.Responseval) && scrapperResponseDTO.Responseval == "1")
                {
                    DateTime dateTime = System.DateTime.Now.AddMinutes(Convert.ToInt32(Schedulertime) + 5);

                    log.logDebugMessage("Altisource Fetch Data------------------>" + dateTime.ToString());

                    Avigma.Repository.Scheduler.JobSchedulerStart.Start(dateTime, Model);
                }
                log.logDebugMessage("-------------Altisource Scrapper Called End ----------------");





                objdynamicobj.Add(scrapperResponseDTO);
                return objdynamicobj;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return objdynamicobj;

            }
        }

        //Cyprexx details by user 
        public List<dynamic> GetAltisourceWorkOdersDetailsByUser(LoginDTO Model)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                log.logDebugMessage("-------------Altisource Manual Scrapper Called Start ----------------");
                log.logDebugMessage("---------Altisource Manual URL ----------------" + Model.URL);

                ScrapperResponseDTO scrapperResponseDTO = new ScrapperResponseDTO();

                string strURL = Model.URL;
                var client = new RestClient(strURL.Trim());

                client.Timeout = -1;

                log.logDebugMessage("---------Altisource  Manual username ----------------" + Model.username);
                log.logDebugMessage("---------Altisource  Manual password ----------------" + Model.password);

                var request = new RestRequest("?username=" + Model.username + "&password=" + Model.password + "&id = " + Model.WI_Pkey_ID + "", Method.GET);

                IRestResponse response = client.Execute(request);
                scrapperResponseDTO.Responseval = response.Content;
                log.logDebugMessage("-------------Altisource  Scrapper Called StatusCode ----------------" + response.StatusCode);
                log.logDebugMessage("-------------Altisource  Scrapper Called Responseval ----------------" + scrapperResponseDTO.Responseval);

                log.logDebugMessage("-------------Manual Scrapper Called End ----------------");




                objdynamicobj.Add(scrapperResponseDTO);
                return objdynamicobj;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return objdynamicobj;

            }
        }
    }
}