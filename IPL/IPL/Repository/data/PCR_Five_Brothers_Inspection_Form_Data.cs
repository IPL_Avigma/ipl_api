﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class PCR_Five_Brothers_Inspection_Form_Data
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        public List<dynamic> PostPCRInspectionForm(PCR_Five_Brothers_Inspection_Form_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_PCR_Five_Brothers_Inspection_Form_Master]";
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@INS_PkeyID", 1 + "#bigint#" + model.INS_PkeyID);
                input_parameters.Add("@INS_WO_ID", 1 + "#bigint#" + model.INS_WO_ID);
                input_parameters.Add("@INS_Inspection", 1 + "#nvarchar#" + model.INS_Inspection);
                input_parameters.Add("@INS_IsActive", 1 + "#bit#" + model.INS_IsActive);
                input_parameters.Add("@INS_IsDelete", 1 + "#bit#" + model.INS_IsDelete);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@fwo_pkyeId", 1 + "#bigint#" + model.fwo_pkyeId);
                input_parameters.Add("@INS_PkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);
                objData = obj.SqlCRUD(insertProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        private DataSet GetPCRInspectionForm(PCR_Five_Brothers_Inspection_Form_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_PCR_Five_Brothers_Inspection_Form_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@INS_PkeyID", 1 + "#bigint#" + model.INS_PkeyID);
                input_parameters.Add("@INS_WO_ID", 1 + "#bigint#" + model.INS_WO_ID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetPCRInspectionFormDetails(PCR_Five_Brothers_Inspection_Form_DTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetPCRInspectionForm(model);

                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                }

                //var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                //List<PCR_Five_Brothers_Inspection_Form_DTO> pcrdata =
                //   (from item in myEnumerableFeaprd
                //    select new PCR_Five_Brothers_Inspection_Form_DTO
                //    {
                //        INS_PkeyID = item.Field<Int64>("INS_PkeyID"),
                //        INS_WO_ID = item.Field<Int64>("INS_WO_ID"),
                //        INS_Company_ID = item.Field<Int64>("INS_Company_ID"),
                //        INS_Inspection = item.Field<String>("INS_Inspection"),
                //        INS_IsActive = item.Field<Boolean>("INS_IsActive"),
                //    }).ToList();

                //objDynamic.Add(pcrdata);

                //if (ds.Tables.Count > 1)
                //{

                //    var pcrHistory = ds.Tables[1].AsEnumerable();
                //    List<PCR_Five_Brothers_Inspection_Form_DTO> pcrdata_history =
                //       (from item in pcrHistory
                //        select new PCR_Five_Brothers_Inspection_Form_DTO
                //        {
                //            INS_PkeyID = item.Field<Int64>("INS_PkeyID"),
                //            INS_WO_ID = item.Field<Int64>("INS_WO_ID"),
                //            INS_Company_ID = item.Field<Int64>("INS_Company_ID"),
                //            INS_Inspection = item.Field<String>("INS_Inspection"),
                //            INS_IsActive = item.Field<Boolean>("INS_IsActive"),
                //        }).ToList();

                //    objDynamic.Add(pcrdata_history);
                //}
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}