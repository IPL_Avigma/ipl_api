﻿using Avigma.Repository.Lib;
using IPLApp.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPLApp.Repository.data
{
    public class InvoiceItemChildData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        //Add Invoice Details For Dropdown 
        public List<dynamic> AddInvoiceItemChildData(InvoiceItemChildDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objrkdData = new List<dynamic>();
            string insertProcedure = "[CreateUpdateInvoiceItemChildMaster]";
            InvoiceItemChild invoiceItemChild = new InvoiceItemChild();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Inv_Chd_pkeyID", 1 + "#bigint#" + model.Inv_Chd_pkeyID);
                input_parameters.Add("@Inv_Chd_ClientID", 1 + "#bigint#" + model.Inv_Chd_ClientID);
                input_parameters.Add("@Inv_Chd_StateID", 1 + "#bigint#" + model.Inv_Chd_StateID);
                input_parameters.Add("@Inv_Chd_CustomerID", 1 + "#bigint#" + model.Inv_Chd_CustomerID);
                input_parameters.Add("@Inv_Chd_LoanTypeID", 1 + "#bigint#" + model.Inv_Chd_LoanTypeID);
                input_parameters.Add("@Inv_Chd_ContractorUnitPrice", 1 + "#decimal#" + model.Inv_Chd_ContractorUnitPrice);
                input_parameters.Add("@Inv_Chd_ClientUnitPrice", 1 + "#decimal#" + model.Inv_Chd_ClientUnitPrice);
                input_parameters.Add("@Inv_Chd_InvoiceID", 1 + "#bigint#" + model.Inv_Chd_InvoiceID);
          
                input_parameters.Add("@Inv_Chd_IsActive", 1 + "#bit#" + model.Inv_Chd_IsActive);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Inv_Chd_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    invoiceItemChild.Inv_Chd_pkeyID = "0";
                    invoiceItemChild.Status = "0";
                    invoiceItemChild.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    invoiceItemChild.Inv_Chd_pkeyID = Convert.ToString(objData[0]);
                    invoiceItemChild.Status = Convert.ToString(objData[1]);


                }
                objrkdData.Add(invoiceItemChild);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objrkdData;



        }
        //Get Invoice Item Data For Child Item
        private DataSet GetInvoiceItemChildMaster(InvoiceItemChildDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_InvoiceItemChild_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Inv_Chd_pkeyID", 1 + "#bigint#" + model.Inv_Chd_pkeyID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetInvoiceItemChildDetails(InvoiceItemChildDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetInvoiceItemChildMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<InvoiceItemChildDTO> InvoiceItemChild =
                   (from item in myEnumerableFeaprd
                    select new InvoiceItemChildDTO
                    {
                        Inv_Chd_pkeyID = item.Field<Int64>("Inv_Chd_pkeyID"),
                        Inv_Chd_ClientID = item.Field<Int64?>("Inv_Chd_ClientID"),
                        Inv_Chd_StateID = item.Field<Int64?>("Inv_Chd_StateID"),
                        Inv_Chd_CustomerID = item.Field<Int64?>("Inv_Chd_CustomerID"),
                        Inv_Chd_LoanTypeID = item.Field<Int64?>("Inv_Chd_LoanTypeID"),
                        Inv_Chd_ContractorUnitPrice = item.Field<Decimal?>("Inv_Chd_ContractorUnitPrice"),
                        Inv_Chd_ClientUnitPrice = item.Field<Decimal?>("Inv_Chd_ClientUnitPrice"),
                        Inv_Chd_InvoiceID = item.Field<Int64?>("Inv_Chd_InvoiceID"),
                        Inv_Chd_IsActive = item.Field<Boolean?>("Inv_Chd_IsActive"),

                    }).ToList();

                objDynamic.Add(InvoiceItemChild);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}