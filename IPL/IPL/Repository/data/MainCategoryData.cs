﻿using Avigma.Repository.Lib;
using IPL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class MainCategoryData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public List<dynamic> AddMainCategoryData(MainCategoryDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objAddData = new List<dynamic>();
            string insertProcedure = "[CreateUpdateMainCategory]";
            MainCategory mainCategory = new MainCategory();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Main_Cat_pkeyID", 1 + "#bigint#" + model.Main_Cat_pkeyID);
                input_parameters.Add("@Main_Cat_Name", 1 + "#nvarchar#" + model.Main_Cat_Name);
                input_parameters.Add("@Main_Cat_Back_Color", 1 + "#nvarchar#" + model.Main_Cat_Back_Color);
                input_parameters.Add("@Main_Cat_Con_Icon", 1 + "#nvarchar#" + model.Main_Cat_Con_Icon);
                input_parameters.Add("@Main_Cat_Active", 1 + "#bit#" + model.Main_Cat_Active);
                input_parameters.Add("@Main_Cat_IsActive", 1 + "#bit#" + model.Main_Cat_IsActive);
                input_parameters.Add("@Main_Cat_IsDelete", 1 + "#bit#" + model.Main_Cat_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Main_Cat_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    mainCategory.Main_Cat_pkeyID = "0";
                    mainCategory.Status = "0";
                    mainCategory.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    mainCategory.Main_Cat_pkeyID = Convert.ToString(objData[0]);
                    mainCategory.Status = Convert.ToString(objData[1]);


                }
                objAddData.Add(mainCategory);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objAddData;



        }

        private DataSet GetMainCategoryMaster(MainCategoryDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_MainCategory]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Main_Cat_pkeyID", 1 + "#bigint#" + model.Main_Cat_pkeyID);
                input_parameters.Add("@WhereClause", 1 + "#varchar#" + model.WhereClause);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetMainCategoryDetails(MainCategoryDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetMainCategoryMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<MainCategoryDTO> MainCatDetails =
                   (from item in myEnumerableFeaprd
                    select new MainCategoryDTO
                    {
                        Main_Cat_pkeyID = item.Field<Int64>("Main_Cat_pkeyID"),
                        Main_Cat_Name = item.Field<String>("Main_Cat_Name"),
                        Main_Cat_Back_Color = item.Field<String>("Main_Cat_Back_Color"),
                        Main_Cat_Con_Icon = item.Field<String>("Main_Cat_Con_Icon"),
                        Main_Cat_Active = item.Field<Boolean?>("Main_Cat_Active"),
                        Main_Cat_IsActive = item.Field<Boolean?>("Main_Cat_IsActive"),
                        Main_Cat_CreatedBy = item.Field<String>("Main_Cat_CreatedBy"),
                        Main_Cat_ModifiedBy = item.Field<String>("Main_Cat_ModifiedBy"),
                        Main_Cat_IsDeleteAllow = item.Field<Boolean?>("Main_Cat_IsDeleteAllow"),
                    }).ToList();

                objDynamic.Add(MainCatDetails);

                if (model.Type == 1)
                {
                    if (ds.Tables.Count > 1)
                    {
                        var myEnumerableInsFilter = ds.Tables[1].AsEnumerable();
                        List<Filter_Admin_Category_MasterDTO> insFilterList =
                           (from item in myEnumerableInsFilter
                            select new Filter_Admin_Category_MasterDTO
                            {
                                Category_Filter_PkeyID = item.Field<Int64>("Category_Filter_PkeyID"),
                                Category_Filter_CategoryName = item.Field<String>("Category_Filter_CategoryName"),
                                Category_Filter_CategoryIsActive = item.Field<Boolean?>("Category_Filter_CategoryIsActive"),
                                Category_Filter_CreatedBy = item.Field<String>("Category_Filter_CreatedBy"),
                                Category_Filter_ModifiedBy = item.Field<String>("Category_Filter_ModifiedBy"),
                            }).ToList();

                        objDynamic.Add(insFilterList);
                    }
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        public List<dynamic> GetCategoryFilterDetails(MainCategoryDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();


            string wherecondition = string.Empty;
            try
            {

                var Data = JsonConvert.DeserializeObject<MainCategoryDTO>(model.FilterData);
                if (!string.IsNullOrEmpty(Data.Main_Cat_Name))
                {
                    //  wherecondition = " And Main_Cat_Name =    '" + Data.Main_Cat_Name + "'";
                    wherecondition = " And Main_Cat_Name LIKE '%" + Data.Main_Cat_Name + "%'";
                }


                if (Data.Main_Cat_IsActive == true)
                {
                    wherecondition = wherecondition + "  And Main_Cat_IsActive =  '1'";
                }
                if (Data.Main_Cat_IsActive == false)
                {
                    wherecondition = wherecondition + "  And Main_Cat_IsActive =  '0'";
                }


                MainCategoryDTO mainCategoryDTO = new MainCategoryDTO();

                mainCategoryDTO.WhereClause = wherecondition;
                mainCategoryDTO.Type = 3;
                mainCategoryDTO.UserID = model.UserID;
                DataSet ds = GetMainCategoryMaster(mainCategoryDTO);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<MainCategoryDTO> Categoryfilter =
                   (from item in myEnumerableFeaprd
                    select new MainCategoryDTO
                    {

                        Main_Cat_pkeyID = item.Field<Int64>("Main_Cat_pkeyID"),
                        Main_Cat_Name = item.Field<String>("Main_Cat_Name"),
                        Main_Cat_Back_Color = item.Field<String>("Main_Cat_Back_Color"),
                        Main_Cat_Con_Icon = item.Field<String>("Main_Cat_Con_Icon"),
                        Main_Cat_Active = item.Field<Boolean?>("Main_Cat_Active"),
                        Main_Cat_IsActive = item.Field<Boolean?>("Main_Cat_IsActive"),
                        Main_Cat_CreatedBy = item.Field<String>("Main_Cat_CreatedBy"),
                        Main_Cat_ModifiedBy = item.Field<String>("Main_Cat_ModifiedBy"),
                    }).ToList();

                objDynamic.Add(Categoryfilter);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }

            return objDynamic;
        }

    }
}