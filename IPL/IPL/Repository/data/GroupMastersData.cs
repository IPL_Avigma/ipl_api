﻿using Avigma.Repository.Lib;
using IPL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using IPLApp.Models;
namespace IPLApp.Repository.data
{
    public class GroupMastersData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public List<dynamic> AddGroupData(GroupMastersDTO model)
        {

            string insertProcedure = "[CreateUpdateGroupMaster]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Grp_pkeyID", 1 + "#bigint#" + model.Grp_pkeyID);
                input_parameters.Add("@Grp_Name", 1 + "#nvarchar#" + model.Grp_Name);
                input_parameters.Add("@Grp_IsActive", 1 + "#bit#" + model.Grp_IsActive);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Grp_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return obj.SqlCRUD(insertProcedure, input_parameters);



        }


        public List<dynamic> AddGroupMenuData(Group_Masters_MenuDTO model)
        {

            string insertProcedure = "[CreateUpdateGroupMaster]";
            List<dynamic> objdynamic = new List<dynamic>();

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                MenuGroupRelationData menuGroupRelationData = new MenuGroupRelationData();
                input_parameters.Add("@Grp_pkeyID", 1 + "#bigint#" + model.Grp_pkeyID);
                input_parameters.Add("@Grp_Name", 1 + "#nvarchar#" + model.Grp_Name);
                input_parameters.Add("@GroupRoleId", 1 + "#bigint#" + model.GroupRoleId);
                input_parameters.Add("@Grp_IsActive", 1 + "#bit#" + model.Grp_IsActive);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Grp_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);
                objdynamic = obj.SqlCRUD(insertProcedure, input_parameters);
                Int64 groupID = 0;
                switch (model.Type)
                {
                    case 1:
                        {
                            if (objdynamic[0] != null)
                            {
                                groupID = objdynamic[0];
                            }
                           
                            break;
                        }
                    case 2:
                        {
                            groupID = model.Grp_pkeyID;
                            break;
                        }

                }
                if (model != null && model.MenuArray != null)
                {
                    for (int i = 0; i < model.MenuArray.Count; i++)
                    {
                        MenuGroupRelationDTO menuGroupRelationDTO = new MenuGroupRelationDTO();
                        menuGroupRelationDTO.Mgr_Group_Id = groupID;
                        menuGroupRelationDTO.Mgr_Menu_Id = model.MenuArray[i].MenuId;
                        menuGroupRelationDTO.Type = 1;
                        menuGroupRelationDTO.Mgr_IsActive = model.MenuArray[i].IsAssignedMenu;
                        menuGroupRelationData.AddMenuGroupData(menuGroupRelationDTO);

                    }
                }

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objdynamic;



        }


        private DataSet GetGroupMaster(GroupMastersDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[GetGroupMaster]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Grp_pkeyID", 1 + "#bigint#" + model.Grp_pkeyID);
                if (model.Type == 1 || model.Type == 4)
                {
                    input_parameters.Add("@WhereClause", 1 + "#nvarchar#" + null);
                    input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                    input_parameters.Add("@MenuID", 1 + "#bigint#" + 0);
                    input_parameters.Add("@FilterData", 1 + "#nvarchar#" + null);
                }
                else {
                    if (model.SearchMaster != null)
                    {
                        input_parameters.Add("@WhereClause", 1 + "#nvarchar#" + model.SearchMaster.WhereClause);
                        input_parameters.Add("@UserID", 1 + "#bigint#" + model.SearchMaster.UserID);
                        input_parameters.Add("@MenuID", 1 + "#bigint#" + model.SearchMaster.MenuID);
                        input_parameters.Add("@FilterData", 1 + "#nvarchar#" + model.SearchMaster.FilterData);
                    }
                    else
                    {
                        input_parameters.Add("@WhereClause", 1 + "#nvarchar#" + null);
                        input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                        input_parameters.Add("@MenuID", 1 + "#bigint#" + 0);
                        input_parameters.Add("@FilterData", 1 + "#nvarchar#" + null);
                        model.Type = 1;
                    }
                 
                }
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetGroupDetails(GroupMastersDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            string wherecondition = string.Empty;
            DataSet ds = null;
            try
            {

                switch (model.Type)
                {
                    case 1:
                        {
                            ds = GetGroupMaster(model);

                            break;
                        }
                    case 2:
                        {
                            ds = GetGroupMaster(model);
                            break;
                        }

                    case 3:
                        {
                            if (!string.IsNullOrWhiteSpace(model.FilterData))
                            {
                                var Data = JsonConvert.DeserializeObject<GroupMastersDTO>(model.FilterData);
                                if (!string.IsNullOrEmpty(Data.Grp_Name))
                                {
                                    wherecondition = " And gp.[Grp_Name] LIKE '%" + Data.Grp_Name + "%'";
                                }
                                if (Data.GroupRoleId != 0 && Data.GroupRoleId != null)
                                {
                                    wherecondition = wherecondition + "  And gp.GroupRoleId LIKE '%" + Data.GroupRoleId + "%'";
                                }

                                if (Data.Grp_IsActive == true)
                                {
                                    wherecondition = wherecondition + "  And Grp_IsActive =  '1'";
                                }
                                if (Data.Grp_IsActive == false)
                                {
                                    wherecondition = wherecondition + "  And Grp_IsActive =  '0'";
                                }
                            }
                        

                            GroupMastersDTO groupMastersDTO = new GroupMastersDTO();
                            SearchMasterDTO searchMasterDTO = new SearchMasterDTO();
                            groupMastersDTO.Type = 3;
                            searchMasterDTO.MenuID = model.SearchMaster.MenuID;
                            searchMasterDTO.UserID = model.SearchMaster.UserID;
                            model.SearchMaster.WhereClause = wherecondition;

                            ds = GetGroupMaster(model);

                            break;
                        }
                    case 4:
                        {
                            ds = GetGroupMaster(model);
                            
                            break;
                        }
                }
                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                }

                #region comment
                //if (model.Type == 4)
                //{
                //    objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[0]));
                //}
                //else
                //{   
                //    if (ds.Tables.Count > 0)
                //    {
                //        var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                //        List<GroupMastersDTO> GroupDetails =
                //           (from item in myEnumerableFeaprd
                //            select new GroupMastersDTO
                //            {
                //                Grp_pkeyID = item.Field<Int64>("Grp_pkeyID"),
                //                Grp_Name = item.Field<String>("Grp_Name"),
                //                Group_DR_Name = item.Field<String>("Group_DR_Name"),
                //                GroupRoleId = item.Field<Int64?>("GroupRoleId"),
                //                Grp_IsActive = item.Field<Boolean?>("Grp_IsActive"),
                //                Grp_IsDeleteAllow = item.Field<Boolean?>("Grp_IsDeleteAllow"),
                //                Grp_CreatedBy = item.Field<String>("Grp_CreatedBy"),
                //                Grp_ModifiedBy = item.Field<String>("Grp_ModifiedBy"),
                //                ViewUrl = null


                //            }).ToList();

                //        objDynamic.Add(GroupDetails);
                //    }

                //    if (ds.Tables.Count > 1)
                //    {
                //        var myEnumerableFeaprd = ds.Tables[1].AsEnumerable();
                //        List<ContractorCategorydrd> concategory =
                //           (from item in myEnumerableFeaprd
                //            select new ContractorCategorydrd
                //            {
                //                Con_Cat_PkeyID = item.Field<Int64>("Con_Cat_PkeyID"),
                //                Con_Cat_Name = item.Field<String>("Con_Cat_Name"),

                //            }).ToList();

                //        objDynamic.Add(concategory);
                //    }

                //    if (model.Type == 1)
                //    {
                //        if (ds.Tables.Count > 1)
                //        {
                //            var myEnumerableFeaprd = ds.Tables[3].AsEnumerable();
                //            List<Filter_Admin_Group_MasterDTO> FilterGroup =
                //               (from item in myEnumerableFeaprd
                //                select new Filter_Admin_Group_MasterDTO
                //                {
                //                    Group_Filter_PkeyID = item.Field<Int64>("Group_Filter_PkeyID"),
                //                    Group_Filter_Group_Name = item.Field<String>("Group_Filter_Group_Name"),
                //                    Group_Filter_GRPIsActive = item.Field<Boolean?>("Group_Filter_GRPIsActive")

                //                }).ToList();

                //            objDynamic.Add(FilterGroup);

                //        }
                //        if (ds.Tables.Count > 3)
                //        {
                //            var myEnumerableCheckin = ds.Tables[4].AsEnumerable();
                //            List<CheckinProvider> CheckinDetails =
                //               (from item in myEnumerableCheckin
                //                select new CheckinProvider
                //                {
                //                    Back_Chk_ProviderID = item.Field<Int64>("Back_Chk_ProviderID"),
                //                    Back_Chk_ProviderName = item.Field<String>("Back_Chk_ProviderName"),

                //                }).ToList();

                //            objDynamic.Add(CheckinDetails);

                //        }
                //    }
                //}
                #endregion
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }


        private DataSet GetGrouproleMaster(GroupRoleDRDMaster model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_DropDown_GroupRole]";
                model.Type = 2;
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetGrouproleMasterDetails(GroupRoleDRDMaster model )
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetGrouproleMaster(model);

                var myEnumerablerole = ds.Tables[0].AsEnumerable();
                List<GroupRoleDRDMaster> grouprole =
                   (from item in myEnumerablerole
                    select new GroupRoleDRDMaster
                    {
                        Group_DR_PkeyID = item.Field<Int64>("Group_DR_PkeyID"),
                        Group_DR_Name = item.Field<String>("Group_DR_Name"),
                        checkitem = false

                    }).ToList();

                objDynamic.Add(grouprole);

                var myEnumerable = ds.Tables[1].AsEnumerable();
                List<folderDrdDTO> folderdrd =
                   (from item in myEnumerable
                    select new folderDrdDTO
                    {
                        Fold_Pkey_Id = item.Field<Int64>("Fold_Pkey_Id"),
                        Fold_Name = item.Field<String>("Fold_Name"),

                    }).ToList();

                objDynamic.Add(folderdrd);

                GroupMastersDTO groupMastersDTO = new GroupMastersDTO();
                groupMastersDTO.Grp_pkeyID = 0;
                groupMastersDTO.Type = 1;


                objDynamic.Add(GetGroupDetails(groupMastersDTO));

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }




    }
}