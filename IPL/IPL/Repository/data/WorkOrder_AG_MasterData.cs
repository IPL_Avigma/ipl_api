﻿using Avigma.Repository.Lib;
using IPL.Models;
using IPL.Models.FiveBrothers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class WorkOrder_AG_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        private List<dynamic> AddWorkOrder_AGData(WorkOrder_AG_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_WorkOrder_AG_Master]";
            WorkOrder_AG_Master WorkOrder_AG_Master = new WorkOrder_AG_Master();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@WAG_PkeyID", 1 + "#bigint#" + model.WAG_PkeyID);
                input_parameters.Add("@WAG_wo_id", 1 + "#nvarchar#" + model.WAG_wo_id);

                input_parameters.Add("@WAG_address", 1 + "#nvarchar#" + model.WAG_address);
                input_parameters.Add("@WAG_city", 1 + "#nvarchar#" + model.WAG_city);
                input_parameters.Add("@WAG_state", 1 + "#nvarchar#" + model.WAG_state);
                input_parameters.Add("@WAG_zip", 1 + "#nvarchar#" + model.WAG_zip);
                input_parameters.Add("@WAG_loan_number", 1 + "#nvarchar#" + model.WAG_loan_number);
                input_parameters.Add("@WAG_loan_type", 1 + "#nvarchar#" + model.WAG_loan_type);
                input_parameters.Add("@WAG_due_date", 1 + "#datetime#" + model.WAG_due_date);
                input_parameters.Add("@WAG_username", 1 + "#nvarchar#" + model.WAG_username);
                input_parameters.Add("@WAG_lot_size", 1 + "#nvarchar#" + model.WAG_lot_size);
                input_parameters.Add("@WAG_received_date", 1 + "#datetime#" + model.WAG_received_date);
                input_parameters.Add("@WAG_customer", 1 + "#nvarchar#" + model.WAG_customer);
                input_parameters.Add("@WAG_work_type", 1 + "#nvarchar#" + model.WAG_work_type);
                input_parameters.Add("@WAG_lock_code", 1 + "#nvarchar#" + model.WAG_lock_code);
                input_parameters.Add("@WAG_key_code", 1 + "#nvarchar#" + model.WAG_key_code);
                input_parameters.Add("@WAG_mortgager", 1 + "#nvarchar#" + model.WAG_mortgager);
                input_parameters.Add("@WAG_id", 1 + "#nvarchar#" + model.WAG_id);



                input_parameters.Add("@WAG_Comments", 1 + "#nvarchar#" + model.WAG_Comments);
                input_parameters.Add("@WAG_gpsLatitude", 1 + "#varchar#" + model.WAG_gpsLatitude);
                input_parameters.Add("@WAG_gpsLongitude", 1 + "#varchar#" + model.WAG_gpsLongitude);
                input_parameters.Add("@WAG_ImportMaster_Pkey", 1 + "#bigint#" + model.WAG_ImportMaster_Pkey);
                input_parameters.Add("@WAG_Import_File_Name", 1 + "#nvarchar#" + model.WAG_Import_File_Name);
                input_parameters.Add("@WAG_Import_FilePath", 1 + "#varchar#" + model.WAG_Import_FilePath);
                input_parameters.Add("@WAG_Import_Pdf_Name", 1 + "#varchar#" + model.WAG_Import_Pdf_Name);
                input_parameters.Add("@WAG_Import_Pdf_Path", 1 + "#varchar#" + model.WAG_Import_Pdf_Path);








                input_parameters.Add("@WAG_IsActive", 1 + "#bit#" + model.WAG_IsActive);
                input_parameters.Add("@WAG_IsDelete", 1 + "#bit#" + model.WAG_IsDelete);

                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@WAG_Pkey_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    WorkOrder_AG_Master.WAG_PkeyID = "0";
                    WorkOrder_AG_Master.Status = "0";
                    WorkOrder_AG_Master.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    WorkOrder_AG_Master.WAG_PkeyID = Convert.ToString(objData[0]);
                    WorkOrder_AG_Master.Status = Convert.ToString(objData[1]);


                }
                objcltData.Add(WorkOrder_AG_Master);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;

        }

        private List<dynamic> AddWorkOrder_AGItemData(WorkOrder_AG_Item_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_WorkOrder_AG_Item_Master]";
            WorkOrder_AG_Item_Master WorkOrder_AG_Item_Master = new WorkOrder_AG_Item_Master();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@WAGINS_PkeyId", 1 + "#bigint#" + model.WAGINS_PkeyId);
                input_parameters.Add("@WAGINS_Ins_Name", 1 + "#varchar#" + model.WAGINS_Ins_Name);
                input_parameters.Add("@WAGINS_Ins_Details", 1 + "#varchar#" + model.WAGINS_Ins_Details);
                input_parameters.Add("@WAGINS_Qty", 1 + "#bigint#" + model.WAGINS_Qty);
                input_parameters.Add("@WAGINS_Price", 1 + "#decimal#" + model.WAGINS_Price);
                input_parameters.Add("@WAGINS_Total", 1 + "#decimal#" + model.WAGINS_Total);
                input_parameters.Add("@WAGINS_FkeyID", 1 + "#bigint#" + model.WAGINS_FkeyID);

                input_parameters.Add("@WAGINS_IsActive", 1 + "#bit#" + model.WAGINS_IsActive);
                input_parameters.Add("@WAGINS_IsDelete", 1 + "#bit#" + model.WAGINS_IsDelete);
                input_parameters.Add("@WAGINS_Additional_Details", 1 + "#nvarchar#" + model.WAGINS_Additional_Details);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@WAGINS_Pkey_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    WorkOrder_AG_Item_Master.WAGINS_PkeyId = "0";
                    WorkOrder_AG_Item_Master.Status = "0";
                    WorkOrder_AG_Item_Master.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    WorkOrder_AG_Item_Master.WAGINS_PkeyId = Convert.ToString(objData[0]);
                    WorkOrder_AG_Item_Master.Status = Convert.ToString(objData[1]);

                }
                objcltData.Add(WorkOrder_AG_Item_Master);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;



        }

        public List<dynamic> AddWorkOrderAGData(WorkOrder_AG_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                objData = AddWorkOrder_AGData(model);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }


        public List<dynamic> AddWorkOrderAGItemData(WorkOrder_AG_Item_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                objData = AddWorkOrder_AGItemData(model);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }


    }
}