﻿using Avigma.Repository.Lib;
using IPL.Models;
using System.Data;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Web;

namespace IPL.Repository.data
{
    public class TaskDamageMasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();


        public List<dynamic> AddTaskDamageMasterData(TaskDamageMaster model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();

            string insertProcedure = "[CreateUpdateTask_Damage_Master]";
            TaskDamage taskDamage = new TaskDamage();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Task_Damage_pkeyID", 1 + "#bigint#" + model.Task_Damage_pkeyID);
                input_parameters.Add("@Task_Damage_WO_ID", 1 + "#bigint#" + model.Task_Damage_WO_ID);
                input_parameters.Add("@Task_Damage_Task_ID", 1 + "#bigint#" + model.Task_Damage_Task_ID);
                input_parameters.Add("@Task_Damage_ID", 1 + "#bigint#" + model.Task_Damage_ID);
                input_parameters.Add("@Task_Damage_Type", 1 + "#varchar#" + model.Task_Damage_Type);
                input_parameters.Add("@Task_Damage_Int", 1 + "#varchar#" + model.Task_Damage_Int);
                input_parameters.Add("@Task_Damage_Location", 1 + "#varchar#" + model.Task_Damage_Location);
                input_parameters.Add("@Task_Damage_Qty", 1 + "#varchar#" + model.Task_Damage_Qty);
                input_parameters.Add("@Task_Damage_Estimate", 1 + "#varchar#" + model.Task_Damage_Estimate);
                input_parameters.Add("@Task_Damage_Disc", 1 + "#varchar#" + model.Task_Damage_Disc);
                input_parameters.Add("@Task_Damage_IsActive", 1 + "#bit#" + model.Task_Damage_IsActive);
                input_parameters.Add("@Task_Damage_IsDelete", 1 + "#bit#" + model.Task_Damage_IsDelete);
                input_parameters.Add("@Task_Damage_Status", 1 + "#int#" + model.Task_Damage_Status);
                input_parameters.Add("@Task_Damage_Other_Name", 1 + "#varchar#" + model.Task_Damage_Other_Name);

                
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Task_Damage_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters); ;
                if (objData[1] == 0)
                {
                    taskDamage.Task_Damage_pkeyID = "0";
                    taskDamage.Status = "0";
                    taskDamage.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    taskDamage.Task_Damage_pkeyID = Convert.ToString(objData[0]);
                    taskDamage.Status = Convert.ToString(objData[1]);


                }
                objcltData.Add(taskDamage);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;



        }

        private DataSet GetTaskDamageMaster(TaskDamageMaster model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Task_Damage_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Task_Damage_pkeyID", 1 + "#bigint#" + model.Task_Damage_pkeyID);
                input_parameters.Add("@Task_Damage_WO_ID", 1 + "#bigint#" + model.Task_Damage_WO_ID);
                input_parameters.Add("@Task_Damage_Status", 1 + "#int#" + model.Task_Damage_Status);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetTaskDamageMasterDetails(TaskDamageMaster model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetTaskDamageMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<TaskDamageMaster> taskDamageMasterDetails =
                   (from item in myEnumerableFeaprd
                    select new TaskDamageMaster
                    {
                        Task_Damage_pkeyID = item.Field<Int64>("Task_Damage_pkeyID"),
                        Task_Damage_WO_ID = item.Field<Int64?>("Task_Damage_WO_ID"),
                        Task_Damage_Task_ID = item.Field<Int64?>("Task_Damage_Task_ID"),
                        Task_Damage_ID = item.Field<Int64?>("Task_Damage_ID"),
                        Task_Damage_Type = item.Field<String>("Task_Damage_Type"),
                        Task_Damage_Int = item.Field<String>("Task_Damage_Int"),
                        Task_Damage_Location = item.Field<String>("Task_Damage_Location"),
                        Task_Damage_Qty = item.Field<String>("Task_Damage_Qty"),
                        Task_Damage_Estimate = item.Field<String>("Task_Damage_Estimate"),
                        Task_Damage_Disc = item.Field<String>("Task_Damage_Disc"),
                        Task_Damage_IsActive = item.Field<Boolean?>("Task_Damage_IsActive"),
                        Task_Damage_Status = item.Field<int?>("Task_Damage_Status"),
                        Damage_Disc = item.Field<String>("Damage_Disc"),
                        Task_Damage_PreTextHide = false, // for text hide n show sathi pretext

                    }).ToList();

                objDynamic.Add(taskDamageMasterDetails);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}