﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class Allowables_Child_Master_Data
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        public List<dynamic> AddupdateAllowables_Child_Master_Data(Allowables_Child_Master_DTO model)

        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_Allowables_Child_Master]";


            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Allow_Child_PkeyId", 1 + "#bigint#" + model.Allow_Child_PkeyId);
                input_parameters.Add("@Allow_Child_Allowables_PkeyId", 1 + "#bigint#" + model.Allow_Child_Allowables_PkeyId);
                input_parameters.Add("@Allow_Child_Allowables_Cat_PkeyId", 1 + "#bigint#" + model.Allow_Child_Allowables_Cat_PkeyId);
                input_parameters.Add("@Allow_Child_StartDate", 1 + "#datetime#" + model.Allow_Child_StartDate);
                input_parameters.Add("@Allow_Child_EndDate ", 1 + "#datetime#" + model.Allow_Child_EndDate);
                input_parameters.Add("@Allow_Child_OverallAllowables ", 1 + "#decimal#" + model.Allow_Child_OverallAllowables);
                input_parameters.Add("@Allow_Child_CompanyId  ", 1 + "#bigint#" + model.Allow_Child_CompanyId);
                input_parameters.Add("@Allow_Child_UserId  ", 1 + "#bigint#" + model.Allow_Child_UserId);
                input_parameters.Add("@Allow_Child_IsActive ", 1 + "#bit#" + model.Allow_Child_IsActive);
                input_parameters.Add("@Allow_Child_IsDelete", 1 + "#bit#" + model.Allow_Child_IsDelete);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Allow_Child_PkeyId_Out ", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;



        }
 



        private DataSet Get_Allowables_Child_Master(Allowables_Child_Master_DTO model)
        {
            DataSet ds = null;
            try
            {

                string selectProcedure = "[Get_Allowables_Child_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Allow_Child_PkeyId", 1 + "#bigint#" + model.Allow_Child_PkeyId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> Get_Allowables_Child_MasterDetails(Allowables_Child_Master_DTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = Get_Allowables_Child_Master(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Allowables_Child_Master_DTO> Allowables_Child_Master =
                   (from item in myEnumerableFeaprd
                    select new Allowables_Child_Master_DTO
                    {
                        Allow_Child_PkeyId = item.Field<Int64>("Allow_Child_PkeyId"),
                        Allow_Child_Allowables_PkeyId = item.Field<Int64?>("Allow_Child_Allowables_PkeyId"),
                        Allow_Child_Allowables_Cat_PkeyId = item.Field<Int64?>("Allow_Child_Allowables_Cat_PkeyId"),
                        Allow_Child_StartDate = item.Field<DateTime?>("Allow_Child_StartDate"),
                        Allow_Child_EndDate = item.Field<DateTime?>("Allow_Child_EndDate"),
                        Allow_Child_OverallAllowables = item.Field<Decimal?>("Allow_Child_OverallAllowables"),
                        Allow_Child_CompanyId = item.Field<Int64?>("Allow_Child_CompanyId"),
                        Allow_Child_UserId = item.Field<Int64?>("Allow_Child_UserId"),
                        Allow_Child_IsActive = item.Field<Boolean?>("Allow_Child_IsActive"),

                    }).ToList();

                objDynamic.Add(Allowables_Child_Master);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}