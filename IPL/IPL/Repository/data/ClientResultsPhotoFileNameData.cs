﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class ClientResultsPhotoFileNameData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        private DataSet Get_Client_Result_Photo_Name(ClientResultsPhotoFileNameDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_ClientResultPhotosFiles]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Client_Result_Photo_Wo_ID", 1 + "#bigint#" + model.Client_Result_Photo_Wo_ID);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return ds;
        }
      
        public List<dynamic> Get_Client_Result_Photo_Name_Details(ClientResultsPhotoFileNameDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = Get_Client_Result_Photo_Name(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<ClientResultsPhotoFileNameDTO> photofilename =
                   (from item in myEnumerableFeaprd
                    select new ClientResultsPhotoFileNameDTO
                    {
                        Client_Result_Photo_FileName = item.Field<String>("Client_Result_Photo_FileName"),

                    }).ToList();

          
                objDynamic.Add(photofilename);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}