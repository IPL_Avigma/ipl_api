﻿using Avigma.Repository.Lib;
using IPL.Models.FiveBrothers;
using IPLApp.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPLApp.Repository.data
{
    public class WorkTypeCategoryData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public List<dynamic> AddWorkTypeCatData(WorkTypeCategoryDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcatData = new List<dynamic>();
            string insertProcedure = "[CreateUpdateWorkTypeCat]";
            WorkTypeCategory workTypeCategory = new WorkTypeCategory();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Work_Type_Cat_pkeyID", 1 + "#bigint#" + model.Work_Type_Cat_pkeyID);
                input_parameters.Add("@Work_Type_Name", 1 + "#nvarchar#" + model.Work_Type_Name);
                input_parameters.Add("@Work_Type_Client_pkeyID", 1 + "#bigint#" + model.Work_Type_Client_pkeyID);
                input_parameters.Add("@Work_Type_Template_pkeyID", 1 + "#bigint#" + model.Work_Type_Template_pkeyID);
                input_parameters.Add("@Work_Type_IsActive", 1 + "#bit#" + model.Work_Type_IsActive);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Work_Type_Cat_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    workTypeCategory.Work_Type_Cat_pkeyID = "0";
                    workTypeCategory.Status = "0";
                    workTypeCategory.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    workTypeCategory.Work_Type_Cat_pkeyID = Convert.ToString(objData[0]);
                    workTypeCategory.Status = Convert.ToString(objData[1]);


                }
                objcatData.Add(workTypeCategory);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcatData;



        }

        private DataSet GetworkcatMaster(WorkTypeCategoryDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_WorkType_Cat]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Work_Type_Cat_pkeyID", 1 + "#bigint#" + model.Work_Type_Cat_pkeyID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetWorkTypeCategoryDetails(WorkTypeCategoryDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetworkcatMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<WorkTypeCategoryDTO> WorkCategoryDetails =
                   (from item in myEnumerableFeaprd
                    select new WorkTypeCategoryDTO
                    {
                        Work_Type_Cat_pkeyID = item.Field<Int64>("Work_Type_Cat_pkeyID"),
                        Work_Type_Name = item.Field<String>("Work_Type_Name"),
                        Work_Type_Client_pkeyID = item.Field<Int64>("Work_Type_Client_pkeyID"),
                        Work_Type_Template_pkeyID = item.Field<Int64>("Work_Type_Template_pkeyID"),
                        Work_Type_IsActive = item.Field<Boolean>("Work_Type_IsActive"),
                        Client_Company_Name = item.Field<String>("Client_Company_Name"),
                        WTT_Template_Name = item.Field<String>("WTT_Template_Name"),
                        Work_Type_IsDeleteAllow = item.Field<Boolean>("Work_Type_IsDeleteAllow"),
                        Work_Type_CreatedBy = item.Field<String>("Work_Type_CreatedBy"),
                        Work_Type_ModifiedBy = item.Field<String>("Work_Type_ModifiedBy"),
                    }).ToList();

                objDynamic.Add(WorkCategoryDetails);

                if (ds.Tables.Count > 1 && model.Type == 1)
                {
                    var myEnumerableaprd = ds.Tables[1].AsEnumerable();
                    List<Import_Client_OrderTypeCode_MasterDTO> WtList =
                       (from item in myEnumerableaprd
                        select new Import_Client_OrderTypeCode_MasterDTO
                        {
                            Import_Client_OrderTypeCodes_Code = item.Field<String>("Import_Client_OrderTypeCodes_Code"),
                            Import_Client_OrderTypeCodes_Description = item.Field<String>("Import_Client_OrderTypeCodes_Description"),
                            Import_Client_OrderTypeCodes_CreatedBy = item.Field<String>("Import_Client_OrderTypeCodes_CreatedBy"),
                            Import_Client_OrderTypeCodes_ModifiedBy = item.Field<String>("Import_Client_OrderTypeCodes_ModifiedBy"),


                        }).ToList();

                    objDynamic.Add(WtList);
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}