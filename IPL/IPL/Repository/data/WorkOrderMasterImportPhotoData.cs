﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class WorkOrderMasterImportPhotoData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        private List<dynamic> AddWorkOrderMasterImportPhotoData(WorkOrderMasterImportPhotoDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_WorkOrderMasterImportPhoto]";
            WoImportPhotoDTO woImportPhotoDTO = new WoImportPhotoDTO();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@WAPI_PkeyID", 1 + "#bigint#" + model.WAPI_PkeyID);
                input_parameters.Add("@WAPI_importpkey", 1 + "#bigint#" + model.WAPI_importpkey);
                input_parameters.Add("@WAPI_WI_PkeyID", 1 + "#bigint#" + model.WAPI_WI_PkeyID);
                input_parameters.Add("@WAPI_Taskname", 1 + "#varchar#" + model.WAPI_Taskname);
                input_parameters.Add("@WAPI_Filename", 1 + "#varchar#" + model.WAPI_Filename);
                input_parameters.Add("@WAPI_Filepath", 1 + "#varchar#" + model.WAPI_Filepath);
                input_parameters.Add("@WAPI_Labelid", 1 + "#int#" + model.WAPI_Labelid);
                input_parameters.Add("@WAPI_IsActive", 1 + "#bit#" + model.WAPI_IsActive);
                input_parameters.Add("@WAPI_IsDelete", 1 + "#bit#" + model.WAPI_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@WAPI_PkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);


                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    woImportPhotoDTO.WAPI_PkeyID = "0";
                    woImportPhotoDTO.Status = "0";
                    woImportPhotoDTO.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    woImportPhotoDTO.WAPI_PkeyID = Convert.ToString(objData[0]);
                    woImportPhotoDTO.Status = Convert.ToString(objData[1]);


                }
                objcltData.Add(woImportPhotoDTO);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }
            return objcltData;



        }
        public List<dynamic> AddUpdateWorkOrderMasterImportPhotoData(UPload_Client_Result_PhotoDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                WorkOrderMasterImportPhotoDTO workOrderMasterImportPhotoDTO = new WorkOrderMasterImportPhotoDTO();
                workOrderMasterImportPhotoDTO.WAPI_importpkey = model.Client_Result_Photo_Wo_ID;
                workOrderMasterImportPhotoDTO.WAPI_WI_PkeyID = model.Client_Result_Photo_Ch_ID;
                workOrderMasterImportPhotoDTO.WAPI_Taskname = model.Client_Result_File_Desc;
                workOrderMasterImportPhotoDTO.WAPI_Filename = model.Client_Result_Photo_FileName;
                workOrderMasterImportPhotoDTO.WAPI_Filepath = model.Client_Result_Photo_FilePath;
                workOrderMasterImportPhotoDTO.WAPI_Labelid = Convert.ToInt32(model.Inst_Doc_Inst_Ch_ID);
                workOrderMasterImportPhotoDTO.Type = 1;
                workOrderMasterImportPhotoDTO.WAPI_IsActive = true;
                workOrderMasterImportPhotoDTO.WAPI_IsDelete = false;
                objData = AddWorkOrderMasterImportPhotoData(workOrderMasterImportPhotoDTO);




            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }
            return objData;
        }


            private DataSet GetWorkOrderMasterImportPhotoMaster(WorkOrderMasterImportPhotoDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[GetWorkOrderMasterImportPhoto]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@WAPI_PkeyID", 1 + "#bigint#" + model.WAPI_PkeyID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }

            return ds;
        }

        public List<dynamic> GetWorkOrderMasterImportPhotoDetails(WorkOrderMasterImportPhotoDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetWorkOrderMasterImportPhotoMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<WorkOrderMasterImportPhotoDTO> wophotodetails =
                   (from item in myEnumerableFeaprd
                    select new WorkOrderMasterImportPhotoDTO
                    {
                        WAPI_PkeyID = item.Field<Int64>("WAPI_PkeyID"),
                        WAPI_importpkey = item.Field<Int64?>("WAPI_importpkey"),
                        WAPI_WI_PkeyID = item.Field<Int64?>("WAPI_WI_PkeyID"),
                        WAPI_Taskname = item.Field<String>("WAPI_Taskname"),
                        WAPI_Filename = item.Field<String>("WAPI_Filename"),
                        WAPI_Filepath = item.Field<String>("WAPI_Filepath"),
                        WAPI_Labelid = item.Field<int?>("WAPI_Labelid"),
                        WAPI_IsActive = item.Field<Boolean?>("WAPI_IsActive"),

                    }).ToList();

                objDynamic.Add(wophotodetails);





            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }

            return objDynamic;
        }
    }
}