﻿using Avigma.Repository.Lib;
using IPL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class Folder_Parent_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        private List<dynamic> AddFolder_Parent_Master(Folder_Parent_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
           
            string insertProcedure = "[CreateUpdate_Folder_Parent_Master]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Fold_Pkey_Id", 1 + "#bigint#" + model.Fold_Pkey_Id);
                input_parameters.Add("@Fold_Name", 1 + "#varchar#" + model.Fold_Name);
                input_parameters.Add("@Fold_IsActive", 1 + "#bit#" + model.Fold_IsActive);
                input_parameters.Add("@Fold_IsDelete", 1 + "#bit#" + model.Fold_IsDelete);
                input_parameters.Add("@Fold_Desc", 1 + "#varchar#" + model.Fold_Desc);
                input_parameters.Add("@Fold_Parent_Id", 1 + "#bigint#" + model.Fold_Parent_Id);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Fold_Pkey_Id_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);

              

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;



        }
        private DataSet GetFolderDetails(Folder_Parent_MasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Folder_Parent_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Fold_Pkey_Id", 1 + "#bigint#" + model.Fold_Pkey_Id);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetFolderMasterDetails(Folder_Parent_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {

                DataSet ds = GetFolderDetails(model);

                if (ds.Tables.Count > 0)
                {
                    var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                    List<Folder_Parent_MasterDTO> FolderParentDetails =
                       (from item in myEnumerableFeaprd
                        select new Folder_Parent_MasterDTO
                        {
                            Fold_Pkey_Id = item.Field<Int64>("Fold_Pkey_Id"),
                            Fold_Name = item.Field<String>("Fold_Name"),
                            Fold_IsActive = item.Field<Boolean?>("Fold_IsActive"),
                            Fold_Desc = item.Field<String>("Fold_Desc"),
                            Fold_Parent_Id = item.Field<Int64?>("Fold_Parent_Id"),


                        }).ToList();

                    objDynamic.Add(FolderParentDetails);
                }

                if (ds.Tables.Count > 1)
                {
                    var myEnumerableFeaprdass = ds.Tables[1].AsEnumerable();
                    List<Folder_Auto_Assine_Master_DTO> assdata =
                       (from item in myEnumerableFeaprdass
                        select new Folder_Auto_Assine_Master_DTO
                        {
                            Fold_Auto_Assine_PkeyId = item.Field<Int64>("Fold_Auto_Assine_PkeyId"),
                            Fold_Parent_Id = item.Field<Int64?>("Fold_Parent_Id"),
                            Fold_Auto_Assine_Client = item.Field<String>("Fold_Auto_Assine_Client"),
                            Fold_Auto_Assine_Customer = item.Field<String>("Fold_Auto_Assine_Customer"),
                            Fold_Auto_Assine_LoneType = item.Field<String>("Fold_Auto_Assine_LoneType"),
                            Fold_Auto_Assine_WorkType = item.Field<String>("Fold_Auto_Assine_WorkType"),
                            Fold_Auto_Assine_WorkType_Group = item.Field<String>("Fold_Auto_Assine_WorkType_Group"),
                            Fold_Auto_Assine_State = item.Field<String>("Fold_Auto_Assine_State"),
                            Fold_Auto_Assine_County = item.Field<String>("Fold_Auto_Assine_County"),
                            Fold_Auto_Assine_Zip = item.Field<Int64?>("Fold_Auto_Assine_Zip"),
                            Fold_Auto_Assine_IsActive = item.Field<Boolean?>("Fold_Auto_Assine_IsActive"),


                        }).ToList();

                    objDynamic.Add(assdata);
                }


                if (ds.Tables.Count > 2)
                {
                    var myEnumerableFeaprdper = ds.Tables[2].AsEnumerable();
                    List<Folder_GroupRole_MasterDTO> Permisda =
                       (from item in myEnumerableFeaprdper
                        select new Folder_GroupRole_MasterDTO
                        {
                            Fold_Role_Pkey_Id = item.Field<Int64>("Fold_Role_Pkey_Id"),
                            Fold_Role_Parent_Id = item.Field<Int64?>("Fold_Role_Parent_Id"),
                            Fold_Role_GroupRole_Id = item.Field<Int64?>("Fold_Role_GroupRole_Id"),
                            Fold_Role_IsActive = item.Field<Boolean?>("Fold_Role_IsActive"),


                        }).ToList();

                    objDynamic.Add(Permisda);
                }

               

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        //filter all details document 

        public List<dynamic> AddUpdateFolderFilterDetails(Folder_Parent_MasterDTO model)
        {
            
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objAddData = new List<dynamic>();


            objData =AddFolder_Parent_Master(model);


            try
            {
                if (model != null && model.PermisionArray != null && model.PermisionArray.Count > 0)
                {                   

                    for (int s = 0; s < model.PermisionArray.Count; s++)
                    {
                        Folder_GroupRole_MasterData folder_GroupRole_MasterData = new Folder_GroupRole_MasterData();
                        Folder_GroupRole_MasterDTO folder_GroupRole_MasterDTO = new Folder_GroupRole_MasterDTO();

                        folder_GroupRole_MasterDTO.Fold_Role_Pkey_Id = model.PermisionArray[s].Grp_pkeyID;
                      //  folder_GroupRole_MasterDTO.Fold_Role_GroupRole_Id = model.PermisionArray[s].GroupRoleId;
                        folder_GroupRole_MasterDTO.Fold_Role_GroupRole_Id = model.PermisionArray[s].Group_DR_PkeyID;
					    folder_GroupRole_MasterDTO.Fold_Role_Parent_Id = objData[0];
                        folder_GroupRole_MasterDTO.Fold_Role_IsActive = model.PermisionArray[s].checkitem;
                        folder_GroupRole_MasterDTO.Fold_Role_IsDelete = false;
                        folder_GroupRole_MasterDTO.UserID = model.UserID;
                        folder_GroupRole_MasterDTO.Type = 1;

                        var grouproledata = folder_GroupRole_MasterData.AddFolder_GroupRole_Master(folder_GroupRole_MasterDTO);

                    }

                }

                objAddData.Add(objData);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return objAddData;



        }

    }
}