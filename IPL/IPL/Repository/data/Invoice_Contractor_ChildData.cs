﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class Invoice_Contractor_ChildData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public List<dynamic> AddInvoiceContractorChildData(Invoice_Contractor_ChildDTO model)
        {

            string insertProcedure = "[CreateUpdateInvoice_Contractor_Child]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Inv_Con_Ch_pkeyId", 1 + "#bigint#" + model.Inv_Con_Ch_pkeyId);
                input_parameters.Add("@Inv_Con_Ch_ContractorId", 1 + "#bigint#" + model.Inv_Con_Ch_ContractorId);
                input_parameters.Add("@Inv_Con_Ch_InvoiceId", 1 + "#bigint#" + model.Inv_Con_Ch_InvoiceId);
                input_parameters.Add("@Inv_Con_Ch_TaskId", 1 + "#bigint#" + model.Inv_Con_Ch_TaskId);
                input_parameters.Add("@Inv_Con_Ch_Wo_Id", 1 + "#bigint#" + model.Inv_Con_Ch_Wo_Id);
                input_parameters.Add("@Inv_Con_Ch_Uom_Id", 1 + "#bigint#" + model.Inv_Con_Ch_Uom_Id);
                input_parameters.Add("@Inv_Con_Ch_Qty", 1 + "#varchar#" + model.Inv_Con_Ch_Qty);
                input_parameters.Add("@Inv_Con_Ch_Price", 1 + "#decimal#" + model.Inv_Con_Ch_Price);
                input_parameters.Add("@Inv_Con_Ch_Total", 1 + "#decimal#" + model.Inv_Con_Ch_Total);
                input_parameters.Add("@Inv_Con_Ch_Adj_Price", 1 + "#decimal#" + model.Inv_Con_Ch_Adj_Price);
                input_parameters.Add("@Inv_Con_Ch_Adj_Total", 1 + "#decimal#" + model.Inv_Con_Ch_Adj_Total);
                input_parameters.Add("@Inv_Con_Ch_Comment", 1 + "#varchar#" + model.Inv_Con_Ch_Comment);
                input_parameters.Add("@Inv_Con_Ch_IsActive", 1 + "#bit#" + model.Inv_Con_Ch_IsActive);
                input_parameters.Add("@Inv_Con_Ch_IsDelete", 1 + "#bit#" + model.Inv_Con_Ch_IsDelete);
                input_parameters.Add("@Inv_Con_Ch_Auto_Invoice", 1 + "#bit#" + model.Inv_Con_Ch_Auto_Invoice);
                input_parameters.Add("@Inv_Con_Ch_Client_ID", 1 + "#bigint#" + model.Inv_Con_Ch_Client_ID);
                input_parameters.Add("@Inv_Con_Ch_Flate_fee", 1 + "#bit#" + model.Inv_Con_Ch_Flate_fee);
                input_parameters.Add("@Inv_Con_Ch_Discount", 1 + "#decimal#" + model.Inv_Con_Ch_Discount);
                input_parameters.Add("@Inv_Con_Ch_Other_Task_Name", 1 + "#varchar#" + model.Inv_Con_Ch_Other_Task_Name);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Inv_Con_Ch_pkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return obj.SqlCRUD(insertProcedure, input_parameters);



        }
        private DataSet GetInvoiceContractorchildMaster(Invoice_Contractor_ChildDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Invoice_Contractor_Child]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Inv_Con_Ch_pkeyId", 1 + "#bigint#" + model.Inv_Con_Ch_pkeyId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetInvoiceContractorChildDetails(Invoice_Contractor_ChildDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetInvoiceContractorchildMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Invoice_Contractor_ChildDTO> InvoiceContractorChild =
                   (from item in myEnumerableFeaprd
                    select new Invoice_Contractor_ChildDTO
                    {
                        Inv_Con_Ch_pkeyId = item.Field<Int64>("Inv_Con_Ch_pkeyId"),
                        Inv_Con_Ch_ContractorId = item.Field<Int64?>("Inv_Con_Ch_ContractorId"),
                        Inv_Con_Ch_InvoiceId = item.Field<Int64?>("Inv_Con_Ch_InvoiceId"),
                        Inv_Con_Ch_TaskId = item.Field<Int64?>("Inv_Con_Ch_TaskId"),
                        Inv_Con_Ch_Wo_Id = item.Field<Int64?>("Inv_Con_Ch_Wo_Id"),
                        Inv_Con_Ch_Uom_Id = item.Field<Int64?>("Inv_Con_Ch_Uom_Id"),
                        Inv_Con_Ch_Qty = item.Field<String>("Inv_Con_Ch_Qty"),
                        Inv_Con_Ch_Price = item.Field<Decimal?>("Inv_Con_Ch_Price"),
                        Inv_Con_Ch_Total = item.Field<Decimal?>("Inv_Con_Ch_Total"),
                        Inv_Con_Ch_Adj_Price = item.Field<Decimal?>("Inv_Con_Ch_Adj_Price"),
                        Inv_Con_Ch_Adj_Total = item.Field<Decimal?>("Inv_Con_Ch_Adj_Total"),
                        Inv_Con_Ch_Comment = item.Field<String>("Inv_Con_Ch_Comment"),
                        Inv_Con_Ch_IsActive = item.Field<Boolean?>("Inv_Con_Ch_IsActive"),
                       


                    }).ToList();

                objDynamic.Add(InvoiceContractorChild);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}