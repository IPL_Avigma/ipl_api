﻿using AdminDemo.Repositotry.Data;
using Avigma.Repository.Lib;
using IPL.Models;
using IPLApp.Models;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class Question_PDFField_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        FormData formData = new FormData();
        Log log = new Log();
        Forms_Master_FilesData forms_Master_FilesData = new Forms_Master_FilesData();

        public List<dynamic> CreateUpdate_QuestionPDFFieldMaster(Question_PDFField_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_Question_PDFField_Master]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@PF_pkeyId", 1 + "#bigint#" + model.PF_pkeyId);
                input_parameters.Add("@PF_FileId", 1 + "#bigint#" + model.PF_FileId);
                input_parameters.Add("@PF_Name", 1 + "#nvarchar#" + model.PF_Name);
                input_parameters.Add("@PF_IsActive", 1 + "#bit#" + model.PF_IsActive);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserId);
                input_parameters.Add("@PF_pkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;



        }

        private DataSet Get_QuestionPDFFieldMaster(Question_PDFField_MasterDTO model)
        {
            DataSet ds = null;
            try
            {

                string selectProcedure = "[Get_Question_PDFField_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@PF_pkeyId", 1 + "#bigint#" + model.PF_pkeyId);
                input_parameters.Add("@PF_FileId", 1 + "#bigint#" + model.PF_FileId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> Get_Get_QuestionPDFFieldDetails(Question_PDFField_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = Get_QuestionPDFFieldMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Question_PDFField_MasterDTO> Forms_Master_Files =
                   (from item in myEnumerableFeaprd
                    select new Question_PDFField_MasterDTO
                    {
                        PF_pkeyId = item.Field<Int64>("PF_pkeyId"),
                        PF_Name = item.Field<String>("PF_Name"),
                        PF_FileId = item.Field<Int64?>("PF_FileId")

                    }).ToList();

                objDynamic.Add(Forms_Master_Files);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
        public List<dynamic> AddUpdateQuePDFField(Forms_Master_FilesDTO fileModel)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                fileModel.Type = 2;
                var filedata = forms_Master_FilesData.Get_Forms_Master_FilesDetails(fileModel);
                if (filedata[0].Count > 0 && filedata[0][0] != null)
                {
                    string filePath = filedata[0][0].FMFI_File_Path;
                    if (!string.IsNullOrEmpty(filePath))
                    {
                        PdfReader pdfReader = new PdfReader(filePath);
                        if (pdfReader != null && pdfReader.AcroFields != null && pdfReader.AcroFields.Fields != null && pdfReader.AcroFields.Fields.Count > 0)
                        {
                            var fields = pdfReader.AcroFields.Fields;
                            foreach (var key in fields.Keys)
                            {
                                Question_PDFField_MasterDTO model = new Question_PDFField_MasterDTO();
                                model.PF_Name = key;
                                model.PF_FileId = fileModel.FMFI_Pkey;
                                model.PF_IsActive = true;
                                model.Type = 1;
                                model.UserId = fileModel.UserId;
                                objData.Add(CreateUpdate_QuestionPDFFieldMaster(model));
                            }
                        }                        
                    }
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

            }
            return objData;
        }
    }
}