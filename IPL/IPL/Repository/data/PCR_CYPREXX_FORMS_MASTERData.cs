﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class PCR_CYPREXX_FORMS_MASTERData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        public List<dynamic> AddPCRCYPREXXFORMSMasterData(PCR_CYPREXX_FORMS_MASTER_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objclntData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_PCR_CYPREXX_FORMS_MASTER]";
            PCR_CYPREXX_FORMS_MASTER_DTO pCR_CYPREXX_FORMS = new PCR_CYPREXX_FORMS_MASTER_DTO();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@PCR_PCFM_pkeyId", 1 + "#bigint#" + model.PCR_PCFM_pkeyId);
                input_parameters.Add("@PCR_PCFM_WO_Id", 1 + "#bigint#" + model.PCR_PCFM_WO_Id);
                input_parameters.Add("@PCR_PCFM_CompanyID", 1 + "#bigint#" + model.PCR_PCFM_CompanyID);
                input_parameters.Add("@PCR_PCFM_General_Information", 1 + "#nvarchar#" + model.PCR_PCFM_General_Information);
                input_parameters.Add("@PCR_PCFM_Property_Accessibility", 1 + "#nvarchar#" + model.PCR_PCFM_Property_Accessibility);
                input_parameters.Add("@PCR_PCFM_Property_Type", 1 + "#nvarchar#" + model.PCR_PCFM_Property_Type);
                input_parameters.Add("@PCR_PCFM_Utilities", 1 + "#nvarchar#" + model.PCR_PCFM_Utilities);
                input_parameters.Add("@PCR_PCFM_Occupancy_Information", 1 + "#nvarchar#" + model.PCR_PCFM_Occupancy_Information);
                input_parameters.Add("@PCR_PCFM_Securing__Lock_Changes", 1 + "#nvarchar#" + model.PCR_PCFM_Securing__Lock_Changes);
                input_parameters.Add("@PCR_PCFM_Grage_shed_outbuilding_securing", 1 + "#nvarchar#" + model.PCR_PCFM_Grage_shed_outbuilding_securing);
                input_parameters.Add("@PCR_PCFM_Window_securing", 1 + "#nvarchar#" + model.PCR_PCFM_Window_securing);
                input_parameters.Add("@PCR_PCFM_Pool", 1 + "#nvarchar#" + model.PCR_PCFM_Pool);
                input_parameters.Add("@PCR_PCFM_Debris_Hazzards", 1 + "#nvarchar#" + model.PCR_PCFM_Debris_Hazzards);
                input_parameters.Add("@PCR_PCFM_Yard", 1 + "#nvarchar#" + model.PCR_PCFM_Yard);
                input_parameters.Add("@PCR_PCFM_Hazard_Abatement", 1 + "#nvarchar#" + model.PCR_PCFM_Hazard_Abatement);
                input_parameters.Add("@PCR_PCFM_Winterization", 1 + "#nvarchar#" + model.PCR_PCFM_Winterization);
                input_parameters.Add("@PCR_PCFM_Damages", 1 + "#nvarchar#" + model.PCR_PCFM_Damages);
                input_parameters.Add("@PCR_PCFM_Signage", 1 + "#nvarchar#" + model.PCR_PCFM_Signage);
                input_parameters.Add("@PCR_PCFM_Canveyance", 1 + "#nvarchar#" + model.PCR_PCFM_Canveyance);
                input_parameters.Add("@PCR_PCFM_General_Comment", 1 + "#nvarchar#" + model.PCR_PCFM_General_Comment);
                input_parameters.Add("@PCR_PCFM_Vendor_Signature", 1 + "#nvarchar#" + model.PCR_PCFM_Vendor_Signature);
                input_parameters.Add("@PCR_PCFM_IsActive", 1 + "#bit#" + model.PCR_PCFM_IsActive);
                input_parameters.Add("@PCR_PCFM_IsDelete", 1 + "#bit#" + model.PCR_PCFM_IsDelete);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@fwo_pkyeId", 1 + "#bigint#" + model.fwo_pkyeId);
                input_parameters.Add("@PCR_PCFM_pkey_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        private DataSet GetPCRCYPREXXFORMS_Master(PCR_CYPREXX_FORMS_MASTER_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_PCR_CYPREXX_FORMS_MASTER]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@PCR_PCFM_pkeyId", 1 + "#bigint#" + model.PCR_PCFM_pkeyId);
                input_parameters.Add("@PCR_PCFM_WO_Id", 1 + "#bigint#" + model.PCR_PCFM_WO_Id);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                //input_parameters.Add("@ReturnValue", 2 + "#int#" + null);
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetPCRCYPREXXFORMSMasterDetails(PCR_CYPREXX_FORMS_MASTER_DTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetPCRCYPREXXFORMS_Master(model);

                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                }

                //var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                //List<PCR_CYPREXX_FORMS_MASTER_DTO> _pcr_CYPREXX_FORMS_MASTER_DTO =
                //   (from item in myEnumerableFeaprd
                //    select new PCR_CYPREXX_FORMS_MASTER_DTO
                //    {
                //        PCR_PCFM_pkeyId = item.Field<Int64>("PCR_PCFM_pkeyId"),
                //        PCR_PCFM_WO_Id = item.Field<Int64>("PCR_PCFM_WO_Id"),
                //        PCR_PCFM_CompanyID = item.Field<Int64>("PCR_PCFM_CompanyID"),
                //        PCR_PCFM_General_Information = item.Field<String>("PCR_PCFM_General_Information"),
                //        PCR_PCFM_Property_Accessibility = item.Field<String>("PCR_PCFM_Property_Accessibility"),
                //        PCR_PCFM_Property_Type = item.Field<String>("PCR_PCFM_Property_Type"),
                //        PCR_PCFM_Utilities = item.Field<String>("PCR_PCFM_Utilities"),
                //        PCR_PCFM_Occupancy_Information = item.Field<String>("PCR_PCFM_Occupancy_Information"),
                //        PCR_PCFM_Securing__Lock_Changes = item.Field<String>("PCR_PCFM_Securing__Lock_Changes"),
                //        PCR_PCFM_Grage_shed_outbuilding_securing = item.Field<String>("PCR_PCFM_Grage_shed_outbuilding_securing"),
                //        PCR_PCFM_Window_securing = item.Field<String>("PCR_PCFM_Window_securing"),
                //        PCR_PCFM_Pool = item.Field<String>("PCR_PCFM_Pool"),
                //        PCR_PCFM_Debris_Hazzards = item.Field<String>("PCR_PCFM_Debris_Hazzards"),
                //        PCR_PCFM_Yard = item.Field<String>("PCR_PCFM_Yard"),
                //        PCR_PCFM_Hazard_Abatement = item.Field<String>("PCR_PCFM_Hazard_Abatement"),
                //        PCR_PCFM_Winterization = item.Field<String>("PCR_PCFM_Winterization"),
                //        PCR_PCFM_Damages = item.Field<String>("PCR_PCFM_Damages"),
                //        PCR_PCFM_Signage = item.Field<String>("PCR_PCFM_Signage"),
                //        PCR_PCFM_Canveyance = item.Field<String>("PCR_PCFM_Canveyance"),
                //        PCR_PCFM_General_Comment = item.Field<String>("PCR_PCFM_General_Comment"),
                //        PCR_PCFM_Vendor_Signature = item.Field<String>("PCR_PCFM_Vendor_Signature"),
                //        PCR_PCFM_IsActive = item.Field<Boolean>("PCR_PCFM_IsActive"),
                //        PCR_PCFM_IsDelete = item.Field<Boolean>("PCR_PCFM_IsDelete"),
                //    }).ToList();

                //objDynamic.Add(_pcr_CYPREXX_FORMS_MASTER_DTO);

                //if (ds.Tables.Count > 1)
                //{

                //    var pcrHistory = ds.Tables[1].AsEnumerable();
                //    List<PCR_CYPREXX_FORMS_MASTER_DTO> _pcrHistory =
                //       (from item in pcrHistory
                //        select new PCR_CYPREXX_FORMS_MASTER_DTO
                //        {
                //            PCR_PCFM_pkeyId = item.Field<Int64>("PCR_PCFM_pkeyId"),
                //            PCR_PCFM_WO_Id = item.Field<Int64>("PCR_PCFM_WO_Id"),
                //            PCR_PCFM_CompanyID = item.Field<Int64>("PCR_PCFM_CompanyID"),
                //            PCR_PCFM_General_Information = item.Field<String>("PCR_PCFM_General_Information"),
                //            PCR_PCFM_Property_Accessibility = item.Field<String>("PCR_PCFM_Property_Accessibility"),
                //            PCR_PCFM_Property_Type = item.Field<String>("PCR_PCFM_Property_Type"),
                //            PCR_PCFM_Utilities = item.Field<String>("PCR_PCFM_Utilities"),
                //            PCR_PCFM_Occupancy_Information = item.Field<String>("PCR_PCFM_Occupancy_Information"),
                //            PCR_PCFM_Securing__Lock_Changes = item.Field<String>("PCR_PCFM_Securing__Lock_Changes"),
                //            PCR_PCFM_Grage_shed_outbuilding_securing = item.Field<String>("PCR_PCFM_Grage_shed_outbuilding_securing"),
                //            PCR_PCFM_Window_securing = item.Field<String>("PCR_PCFM_Window_securing"),
                //            PCR_PCFM_Pool = item.Field<String>("PCR_PCFM_Pool"),
                //            PCR_PCFM_Debris_Hazzards = item.Field<String>("PCR_PCFM_Debris_Hazzards"),
                //            PCR_PCFM_Yard = item.Field<String>("PCR_PCFM_Yard"),
                //            PCR_PCFM_Hazard_Abatement = item.Field<String>("PCR_PCFM_Hazard_Abatement"),
                //            PCR_PCFM_Winterization = item.Field<String>("PCR_PCFM_Winterization"),
                //            PCR_PCFM_Damages = item.Field<String>("PCR_PCFM_Damages"),
                //            PCR_PCFM_Signage = item.Field<String>("PCR_PCFM_Signage"),
                //            PCR_PCFM_Canveyance = item.Field<String>("PCR_PCFM_Canveyance"),
                //            PCR_PCFM_General_Comment = item.Field<String>("PCR_PCFM_General_Comment"),
                //            PCR_PCFM_Vendor_Signature = item.Field<String>("PCR_PCFM_Vendor_Signature"),
                //            PCR_PCFM_IsActive = item.Field<Boolean>("PCR_PCFM_IsActive"),
                //            PCR_PCFM_IsDelete = item.Field<Boolean>("PCR_PCFM_IsDelete"),
                //        }).ToList();

                //    objDynamic.Add(_pcrHistory);
                //}
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}