﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class Filter_Admin_PhotoHeaderData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        public List<dynamic> AddUpdate_Filter_Admin_PhotoHeader(Filter_Admin_PhotoHeader_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_Filter_Admin_PhotoHeader_Master]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Photo_Head_Filter_PkeyId", 1 + "#bigint#" + model.Photo_Head_Filter_PkeyId);
                input_parameters.Add("@Photo_Head_Filter_Header_Temp", 1 + "#nvarchar#" + model.Photo_Head_Filter_Header_Temp);
                input_parameters.Add("@Photo_Head_Filter_Client_Company", 1 + "#bigint#" + model.Photo_Head_Filter_Client_Company);
                input_parameters.Add("@Photo_Head_Filter_File_Name_temp", 1 + "#nvarchar#" + model.Photo_Head_Filter_File_Name_temp);
                input_parameters.Add("@Photo_Head_Filter_pdf_temp", 1 + "#bit#" + model.Photo_Head_Filter_pdf_temp);
                input_parameters.Add("@Photo_Head_Filter_IsActive", 1 + "#bit#" + model.Photo_Head_Filter_IsActive);
                input_parameters.Add("@Photo_Head_Filter_FilterActive", 1 + "#bit#" + model.Photo_Head_Filter_FilterActive);
                input_parameters.Add("@Photo_Head_Filter_IsDelete", 1 + "#bit#" + model.Photo_Head_Filter_IsDelete);
                input_parameters.Add("@Photo_Head_Filter_UserId", 1 + "#bigint#" + model.UserId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserId);
                input_parameters.Add("@Photo_Head_Filter_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }
    }
}