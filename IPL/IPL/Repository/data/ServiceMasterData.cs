﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Avigma.Repository.Lib;
using System.Data;
using IPLApp.Models;

namespace IPLApp.Repository
{
    public class ServiceMasterData
    {
        Log log = new Log();
        MyDataSourceFactory obj = new MyDataSourceFactory();
        public List<dynamic> AddServiceData(ServiceDTO serviceDTO)
        {
            List<dynamic> objdata = new List<dynamic>();
            List<dynamic> objreturndata = new List<dynamic>();
            ServiceReturn serviceReturn = new ServiceReturn();
            try
            {
                string insertProcedure = "[CreateUpdateServiceMaster]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@serviceID", 1 + "#bigint#" + serviceDTO.serviceID);
                input_parameters.Add("@serviceName", 1 + "#varchar#" + serviceDTO.serviceName);
                input_parameters.Add("@survey", 1 + "#varchar#" + serviceDTO.survey);
                input_parameters.Add("@instructions", 1 + "#varchar#" + serviceDTO.instructions);
                input_parameters.Add("@attribute1", 1 + "#nvarchar#" +serviceDTO.attribute1);
                input_parameters.Add("@attribute2", 1 + "#nvarchar#" + serviceDTO.attribute2);
                input_parameters.Add("@attribute3", 1 + "#nvarchar#" + serviceDTO.attribute3);
                input_parameters.Add("@attribute4", 1 + "#nvarchar#" + serviceDTO.attribute4);
                input_parameters.Add("@attribute5", 1 + "#nvarchar#" + serviceDTO.attribute5);
                input_parameters.Add("@attribute6", 1 + "#nvarchar#" + serviceDTO.attribute6);
                input_parameters.Add("@attribute7", 1 + "#nvarchar#" + serviceDTO.attribute7);
                input_parameters.Add("@attribute8", 1 + "#nvarchar#" + serviceDTO.attribute8);
                input_parameters.Add("@attribute9", 1 + "#nvarchar#" + serviceDTO.attribute9);
                input_parameters.Add("@attribute10", 1 + "#nvarchar#" + serviceDTO.attribute10);
                input_parameters.Add("@attribute11", 1 + "#nvarchar#" + serviceDTO.attribute11);
                input_parameters.Add("@attribute12", 1 + "#nvarchar#" + serviceDTO.attribute12);
                input_parameters.Add("@attribute13", 1 + "#nvarchar#" + serviceDTO.attribute13);
                input_parameters.Add("@attribute14", 1 + "#nvarchar#" + serviceDTO.attribute14);
                input_parameters.Add("@attribute15", 1 + "#nvarchar#" + serviceDTO.attribute15);
                input_parameters.Add("@source_service", 1 + "#varchar#" + serviceDTO.source_service);
                input_parameters.Add("@source_service_id", 1 + "#bigint#" + serviceDTO.source_service_id);
                input_parameters.Add("@surveyDynamic", 1 + "#bigint#" + serviceDTO.surveyDynamic);
                input_parameters.Add("@workOrder_ID", 1 + "#bigint#" +serviceDTO.workOrder_ID);
                input_parameters.Add("@IsActive", 1 + "#bit#" + serviceDTO.IsActive);
                input_parameters.Add("@Type", 1 + "#int#" + serviceDTO.Type);
                input_parameters.Add("@currUserId", 1 + "#bigint#" + serviceDTO.currUserId);
                input_parameters.Add("@IsDelete", 1 + "#bit#" + serviceDTO.IsDelete);
                input_parameters.Add("@IsProcess", 1 + "#bit#" + serviceDTO.IsProcess);
                input_parameters.Add("@serviceIDOut", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

               objdata= obj.SqlCRUD(insertProcedure, input_parameters);
               serviceReturn.serviceID =Convert.ToString(objdata[0]);
                serviceReturn.Status = Convert.ToString(objdata[1]);

                objreturndata.Add(serviceReturn);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return null;
            }

            return objreturndata;

        }

        public List<dynamic> GetServiceData(ServiceDTO service)
        {
            List<dynamic> objdata = new List<dynamic>();
            try
            {
                string insertProcedure = "[GetServiceMaster]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@serviceID", 1 + "#bigint#" + service.serviceID);
                input_parameters.Add("@Type", 1 + "#int#" + service.Type);

                DataSet ds = obj.SelectSql(insertProcedure, input_parameters);

                var myEnumerable = ds.Tables[0].AsEnumerable();
                List<ViewService> servicesData =
                    (from item in myEnumerable
                     select new ViewService
                     {
                         serviceID = item.Field<Int64>("serviceID"),
                         serviceName = item.Field<String>("serviceName"),
                         survey = item.Field<String>("survey"),
                         instructions = item.Field<String>("instructions"),
                         attribute1 = item.Field<Int64?>("attribute1"),
                         attribute2 = item.Field<Int64?>("attribute2"),
                         attribute3 = item.Field<Int64?>("attribute3"),
                         attribute4 = item.Field<Int64?>("attribute4"),
                         attribute5 = item.Field<Int64?>("attribute5"),
                         attribute6 = item.Field<Int64?>("attribute6"),
                         attribute7 = item.Field<Int64?>("attribute7"),
                         attribute8 = item.Field<Int64?>("attribute8"),
                         attribute9 = item.Field<Int64?>("attribute9"),
                         attribute10 = item.Field<Int64?>("attribute10"),
                         attribute11 = item.Field<Int64?>("attribute11"),
                         attribute12 = item.Field<Int64?>("attribute12"),
                         attribute13 = item.Field<Int64?>("attribute13"),
                         attribute14 = item.Field<Int64?>("attribute14"),
                         attribute15 = item.Field<Int64?>("attribute15"),
                         source_service = item.Field<String>("source_service"),
                         source_service_id=item.Field<Int64?>("source_service_id"),
                         surveyDynamic=item.Field<Int64?>("surveyDynamic"),
                         workOrder_ID=item.Field<Int64?>("workOrder_ID")


                     }).ToList();
                objdata.Add(servicesData);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return objdata;
            }
            return objdata;
        }
    }
}