﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Avigma.Repository.Lib;
using IPLApp.Models;
using Avigma.Models;
using Newtonsoft.Json;
using System.Text.RegularExpressions;
using IPL.Repository.data;
using IPL.Models;
using Avigma.Repository.Lib.FireBase;
using System.Net.Http;
using Microsoft.Ajax.Utilities;
using Firebase.Database;
using System.Threading.Tasks;
using Firebase.Database.Query;
using System.Security.Cryptography;
using System.Text;
using IPL.Repository.Lib;
using System.Data.SqlClient;

namespace IPLApp.Repository.data
{
    public class WorkOrderMasterData
    {
        Log log = new Log();
        MyDataSourceFactory obj = new MyDataSourceFactory();
        WorkOrderRecurringMasterData workOrderRecurringMasterData = new WorkOrderRecurringMasterData();
        private List<dynamic> Add_WorkorderData(workOrderxDTO workOrder)
        {
            List<dynamic> objdata = new List<dynamic>();
            List<dynamic> objreturndata = new List<dynamic>();
            ReturnworkOrder returnworkOrder = new ReturnworkOrder();
            try
            {
                string insertProcedure = "[CreateUpdateWorkOrderMaster]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@workOrder_ID", 1 + "#bigint#" + workOrder.workOrder_ID);
                input_parameters.Add("@workOrderNumber", 1 + "#varchar#" + workOrder.workOrderNumber);
                input_parameters.Add("@workOrderInfo", 1 + "#varchar#" + workOrder.workOrderInfo);
                input_parameters.Add("@address1", 1 + "#varchar#" + workOrder.address1);
                input_parameters.Add("@address2", 1 + "#varchar#" + workOrder.address2);
                input_parameters.Add("@city", 1 + "#varchar#" + workOrder.city);
                input_parameters.Add("@state", 1 + "#varchar#" + workOrder.state);
                input_parameters.Add("@zip", 1 + "#bigint#" + workOrder.zip);
                input_parameters.Add("@country", 1 + "#varchar#" + workOrder.country);
                input_parameters.Add("@status", 1 + "#varchar#" + workOrder.status);
                input_parameters.Add("@dueDate", 1 + "#datetime#" + workOrder.dueDate);
                input_parameters.Add("@startDate", 1 + "#datetime#" + workOrder.startDate);

                input_parameters.Add("@clientStatus", 1 + "#varchar#" + workOrder.clientStatus);
                input_parameters.Add("@clientDueDate", 1 + "#datetime#" + workOrder.clientDueDate);
                input_parameters.Add("@gpsLatitude", 1 + "#varchar#" + workOrder.gpsLatitude);
                input_parameters.Add("@gpsLongitude", 1 + "#varchar#" + workOrder.gpsLongitude);
                input_parameters.Add("@IsActive", 1 + "#bit#" + workOrder.IsActive);
                input_parameters.Add("@Type", 1 + "#int#" + workOrder.Type);
                input_parameters.Add("@currUserId", 1 + "#bigint#" + workOrder.currUserId);
                input_parameters.Add("@WorkType", 1 + "#bigint#" + workOrder.WorkType);
                input_parameters.Add("@Company", 1 + "#varchar#" + workOrder.Company);
                input_parameters.Add("@Com_Name", 1 + "#varchar#" + workOrder.Com_Name);
                input_parameters.Add("@Com_Phone", 1 + "#varchar#" + workOrder.Com_Phone);
                input_parameters.Add("@Com_Email", 1 + "#varchar#" + workOrder.Com_Email);
                input_parameters.Add("@Contractor", 1 + "#bigint#" + workOrder.Contractor);
                input_parameters.Add("@Processor", 1 + "#bigint#" + workOrder.Processor);
                input_parameters.Add("@Received_Date", 1 + "#datetime#" + workOrder.Received_Date);
                input_parameters.Add("@Complete_Date", 1 + "#datetime#" + workOrder.Complete_Date);
                input_parameters.Add("@Cancel_Date", 1 + "#datetime#" + workOrder.Cancel_Date);
                input_parameters.Add("@IPLNO", 1 + "#varchar#" + workOrder.IPLNO);

                input_parameters.Add("@Category", 1 + "#bigint#" + workOrder.Category);
                input_parameters.Add("@Loan_Info", 1 + "#varchar#" + workOrder.Loan_Info);
                input_parameters.Add("@Customer_Number", 1 + "#bigint#" + workOrder.Customer_Number);
                input_parameters.Add("@Cordinator", 1 + "#bigint#" + workOrder.Cordinator);
                input_parameters.Add("@BATF", 1 + "#bit#" + workOrder.BATF);
                input_parameters.Add("@ISInspection", 1 + "#bit#" + workOrder.ISInspection);
                input_parameters.Add("@Lotsize", 1 + "#varchar#" + workOrder.Lotsize);
                input_parameters.Add("@Rush", 1 + "#bigint#" + workOrder.Rush);
                input_parameters.Add("@Lock_Code", 1 + "#varchar#" + workOrder.Lock_Code);
                input_parameters.Add("@Broker_Info", 1 + "#varchar#" + workOrder.Broker_Info);
                input_parameters.Add("@Comments", 1 + "#varchar#" + workOrder.Comments);
                input_parameters.Add("@Lock_Location", 1 + "#varchar#" + workOrder.Lock_Location);
                input_parameters.Add("@Key_Code", 1 + "#varchar#" + workOrder.Key_Code);
                input_parameters.Add("@Gate_Code", 1 + "#varchar#" + workOrder.Gate_Code);
                input_parameters.Add("@Loan_Number", 1 + "#varchar#" + workOrder.Loan_Number);

                input_parameters.Add("@Office_Approved", 1 + "#varchar#" + workOrder.Office_Approved);
                input_parameters.Add("@Sent_to_Client", 1 + "#varchar#" + workOrder.Sent_to_Client);
                input_parameters.Add("@workOrderNumber_Org", 1 + "#varchar#" + workOrder.@workOrderNumber_Org);
                input_parameters.Add("@IsDelete", 1 + "#bit#" + workOrder.IsDelete);

                input_parameters.Add("@DateCreated", 1 + "#datetime#" + workOrder.DateCreated);
                input_parameters.Add("@EstimatedDate", 1 + "#datetime#" + workOrder.EstimatedDate);
                input_parameters.Add("@Mortgagor", 1 + "#varchar#" + workOrder.Mortgagor);
                input_parameters.Add("@Inst_Ch_Comand_Mobile", 1 + "#varchar#" + workOrder.Inst_Ch_Comand_Mobile);

                input_parameters.Add("@Recurring", 1 + "#bit#" + workOrder.Recurring);
                input_parameters.Add("@Recurs_CutOffDate", 1 + "#datetime#" + workOrder.Recurs_CutOffDate);
                input_parameters.Add("@Recurs_Period", 1 + "#bigint#" + workOrder.Recurs_Period);
                input_parameters.Add("@Recurs_Limit", 1 + "#bigint#" + workOrder.Recurs_Limit);
                input_parameters.Add("@Recurs_Day", 1 + "#int#" + workOrder.Recurs_Day);
                input_parameters.Add("@Background_Provider", 1 + "#bigint#" + workOrder.Background_Provider);
                input_parameters.Add("@IsEdit", 1 + "#bit#" + workOrder.IsEdit);

                input_parameters.Add("@workOrder_IDOut", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objdata = obj.SqlCRUD(insertProcedure, input_parameters);

                returnworkOrder.workOrder_ID = Convert.ToString(objdata[0]);
                returnworkOrder.Status = Convert.ToString(objdata[1]);

                objreturndata.Add(returnworkOrder);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return null;
            }

            return objreturndata;

        }
        private static readonly Regex sWhitespace = new Regex(@"\s+");
        public string ReplaceWhitespace(string input, string replacement)
        {
            try
            {
                return sWhitespace.Replace(input, replacement);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return null;
            }

        }

        public DataSet Get_WorkOrderLatLongByAddress(workOrderxDTO workOrder)
        {
            DataSet ds = new DataSet();
            try
            {
                string insertProcedure = "[Get_WorkOrderLatLongByAddress]";

                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@address1", 1 + "#nvarchar#" + ReplaceWhitespace(workOrder.address1,""));
                input_parameters.Add("@address2", 1 + "#nvarchar#" + workOrder.address2);
                input_parameters.Add("@city", 1 + "#varchar#" + workOrder.city);
                input_parameters.Add("@state", 1 + "#varchar#" + workOrder.state);
                input_parameters.Add("@zip", 1 + "#bigint#" + workOrder.zip);
                input_parameters.Add("@country", 1 + "#varchar#" + workOrder.country);
                input_parameters.Add("@Type", 1 + "#int#" + workOrder.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + workOrder.UserID);


                ds = obj.SelectSql(insertProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ds;
        }

        public DataSet GetWorkOrderLatLongByAddress(workOrderxDTO workOrder)
        {
            DataSet ds = new DataSet();
            try
            {
                workOrderxDTO workOrderxObj = new workOrderxDTO();
                workOrderxObj.address1 = workOrder.address1;
                workOrderxObj.address2 = workOrder.address2;
                workOrderxObj.city = workOrder.city;
                workOrderxObj.state = workOrder.state;
                workOrderxObj.country = workOrder.country;
                workOrderxObj.Type = 1;
                workOrderxObj.UserID = workOrder.UserID;

                ds = Get_WorkOrderLatLongByAddress(workOrderxObj);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ds;
        }

        public async Task<List<dynamic>> AddWorkorderDataAsync(workOrderxDTO workOrder)
        {
            try
            {
                GoogleLocation GoogleLocation = new GoogleLocation();
                GoogleLocationDTO googleLocationDTO = new GoogleLocationDTO();
                List<dynamic> objdata = new List<dynamic>();
                switch (workOrder.Type)
                {

                    case 1:
                    case 2:
                    case 6:
                        {
                            DataSet ds = GetWorkOrderLatLongByAddress(workOrder);
                            if (ds.Tables.Count > 0)
                            {
                                if (ds.Tables[0].Rows.Count > 0)
                                {
                                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                                    {
                                        workOrder.gpsLatitude = ds.Tables[0].Rows[i]["gpsLatitude"].ToString();
                                        workOrder.gpsLongitude = ds.Tables[0].Rows[i]["gpsLongitude"].ToString();
                                    }
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(workOrder.address1) && !string.IsNullOrEmpty(workOrder.state) && workOrder.zip != 0)
                                    {
                                        string strAddress = workOrder.address1 + "  ," + workOrder.state_name + "  " + workOrder.zip.ToString();
                                        googleLocationDTO.Address = strAddress;
                                        googleLocationDTO = GoogleLocation.GetLatitudeLogitudeWithAPIKey(googleLocationDTO);

                                        workOrder.gpsLatitude = googleLocationDTO.Latitude;
                                        workOrder.gpsLongitude = googleLocationDTO.Longitude;
                                    }
                                }
                            }

                            else
                            {
                                if (!string.IsNullOrEmpty(workOrder.address1) && !string.IsNullOrEmpty(workOrder.state) && workOrder.zip != 0)
                                {
                                    string strAddress = workOrder.address1 + "  ," + workOrder.state_name + "  " + workOrder.zip.ToString();
                                    googleLocationDTO.Address = strAddress;
                                    googleLocationDTO = GoogleLocation.GetLatitudeLogitudeWithAPIKey(googleLocationDTO);

                                    workOrder.gpsLatitude = googleLocationDTO.Latitude;
                                    workOrder.gpsLongitude = googleLocationDTO.Longitude;
                                }
                            }

                            if (workOrder.Type == 2)
                            {
                                var Old_workorderData = GetWorkorderForNotification(workOrder.workOrder_ID, workOrder.UserID);
                                // Sending Notification to the old contractor when workorder is UnAssigned
                                if (workOrder.Contractor != Old_workorderData.Contractor)
                                {
                                    string IPLNO = Old_workorderData.IPLNO;
                                    string Address = Old_workorderData.address1 + " " + Old_workorderData.city + " " + Old_workorderData.state + " " + Old_workorderData.zip;
                                    NotificationGetData notificationGetData = new NotificationGetData();
                                    var notificationResult = notificationGetData.SendNotification(Old_workorderData.User_Token_val, "Work order with IPL number #" + IPLNO + "  " + Address + " has been UnAssigned .", "Work Order UnAssigned", workOrder.workOrder_ID.ToString(), 1, IPLNO);
                                }
                            }
                           

                            break;
                        }
                }
                string strcomments = string.Empty;
                if (!string.IsNullOrEmpty(workOrder.Comments))
                {
                    strcomments = Regex.Replace(workOrder.Comments, "<.*?>", String.Empty);
                    workOrder.Inst_Ch_Comand_Mobile = strcomments;
                }
               

               

                workOrderxDTO old_workorderData = new workOrderxDTO();
                if (workOrder.workOrder_ID > 0)
                {
                    old_workorderData = GetWorkorderForNotification(workOrder.workOrder_ID, workOrder.UserID);

                    if (workOrder.status == "2" && (string.IsNullOrEmpty(workOrder.Contractor) || workOrder.Contractor == "0"))
                    {
                        workOrder.status = "1";
                    }
                }

                objdata = Add_WorkorderData(workOrder);
                if (workOrder.workOrder_ID ==0)
                {
                    if (objdata.Count > 0)
                    {
                        workOrder.workOrder_ID = Convert.ToInt64(((ReturnworkOrder)objdata[0]).workOrder_ID);
                    }
                }
                
                
                UserDetailDTO userDetailDTO = new UserDetailDTO();
                userDetailDTO.UserID = workOrder.currUserId;
                userDetailDTO.ContractorID = Convert.ToInt64(workOrder.Contractor);
                userDetailDTO.CordinatorID = workOrder.Cordinator.GetValueOrDefault(0);
                userDetailDTO.ProcessorID = workOrder.Processor.GetValueOrDefault(0);
                userDetailDTO.WoID = workOrder.workOrder_ID;
                userDetailDTO.Type = 1;
                var userDetail = GetMessageUserDetail(userDetailDTO);

                if (workOrder.Recurring == true && workOrder.Recurs_ReceivedDateArray != null && workOrder.Recurs_ReceivedDateArray.Count > 0)
                {
                    for (int i = 0; i < workOrder.Recurs_ReceivedDateArray.Count; i++)
                    {
                        if (workOrder.Recurs_ReceivedDateArray[i] != null)
                        {
                            WorkOrderRecurringMasterDTO recModel = new WorkOrderRecurringMasterDTO();
                            recModel.Wo_Rec_WoId = workOrder.workOrder_ID == 0 ? Convert.ToInt64(objdata[0].workOrder_ID) : workOrder.workOrder_ID;
                            recModel.Wo_Rec_ReceiveDate = workOrder.Recurs_ReceivedDateArray[i];
                            recModel.Wo_Rec_DueDate = workOrder.Recurs_DueDateArray[i];
                            recModel.Wo_Rec_IsProcess = false;
                            recModel.UserId = workOrder.currUserId;
                            recModel.Type = 1;
                            var recObjData = workOrderRecurringMasterData.AddWorkOrderRecurringMaster(recModel);
                        }
                    }
                }


                if (workOrder.Type == 4)
                {
                    var isMsgDelete = DeleteFirbaseMessage(workOrder.IPLNO);
                 //   NotificationGetData notificationGetData = new NotificationGetData();
                 //   var msgResult = notificationGetData.SendNotification(userDetail.ContractorDetail[0].UserToken, "Work order with IPL number " + workOrder.IPLNO + " has been Deleted.", "IPL WorkOrder ", workOrder.workOrder_ID.ToString(), 10);
                }
                WorkOrderUpdateStatusDTO workOrderUpdateStatusDTO = new WorkOrderUpdateStatusDTO();
                workOrderUpdateStatusDTO.UserId = workOrder.currUserId;
                workOrderUpdateStatusDTO.workOrder_ID = workOrder.workOrder_ID == 0 ? Convert.ToInt64(objdata[0].workOrder_ID) : workOrder.workOrder_ID;
                workOrderUpdateStatusDTO.IPLNO = workOrder.IPLNO;
                workOrderUpdateStatusDTO.address1 = userDetail.address1 + " " + userDetail.city + " " + userDetail.state + " " + userDetail.zip;

                if (workOrder.Type == 1|| workOrder.Type == 6) //Add message 
                {
                    if (workOrder.currUserId > 0 && userDetail != null && userDetail.LoggedUserDetail != null && userDetail.LoggedUserDetail.Count > 0)
                    {
                        FirebaseMessageDTO msg = new FirebaseMessageDTO();
                        msg.avatar = "";
                        msg.from = userDetail.LoggedUserDetail[0].User_LoginName.ToLower();
                        //msg.message = "Work order  with IPL number #" + workOrder.IPLNO +" "+ workOrderUpdateStatusDTO.address1+" Created ";
                        msg.message = "Work order  with IPL number " + workOrder.IPLNO + " " + workOrderUpdateStatusDTO.address1 + " Created ";
                        msg.name = userDetail.LoggedUserDetail[0].UserName.ToLower();
                        msg.status = "unread";
                        msg.threadid = "0_internal";
                        msg.threadtype = "internal";
                        // msg.time = DateTime.Now;
                        msg.time = new Dictionary<string, string> { { ".sv", "timestamp" } };// Assign the Firebase ServerValue
                        var msgResult = await AddFirbaseMessage(msg, workOrder.IPLNO, userDetail.LoggedUserDetail[0].IPL_Company_ID, workOrderUpdateStatusDTO.address1);
                    }

                   
                   

                    if (Convert.ToInt64(workOrder.Contractor) > 0 && userDetail != null && userDetail.ContractorDetail != null && userDetail.ContractorDetail.Count > 0)
                    {
                        if (!string.IsNullOrWhiteSpace(userDetail.ContractorDetail[0].UserToken))
                        {
                            NotificationGetData notificationGetData = new NotificationGetData();
                            var msgResult = notificationGetData.SendNotification(userDetail.ContractorDetail[0].UserToken, "Work order with IPL number #" + workOrder.IPLNO + " " + workOrderUpdateStatusDTO.address1 + "   has been Assigned.", "IPL WorkOrder ", workOrderUpdateStatusDTO.workOrder_ID.ToString(),3, userDetail.IPLNo);
                        }

                        userDetail.ContractorDetail[0].User_LoginName = userDetail.ContractorDetail[0].User_LoginName.Replace(@".", "");
                        FirebaseUserGroupDTO userGroup = new FirebaseUserGroupDTO();
                        userGroup.id = userDetail.ContractorDetail[0].User_LoginName.ToLower();
                        userGroup.avatar = "http://gravatar.com/avatar/" + GenerateMD5(userGroup.id) + "?d=identicon";
                        await AddFirebaseUserGroupIPLNO(userGroup, workOrder.IPLNO , userDetail.ContractorDetail[0].IPL_Company_ID, userDetail.ContractorDetail[0].User_LoginName.ToLower(), workOrderUpdateStatusDTO.address1);


                        //workOrderUpdateStatusDTO.WN_UserId = Convert.ToInt64(workOrder.Contractor);
                        //var nObj = SendCreatedWebNotification(workOrderUpdateStatusDTO);
                    }
                    if (workOrder.Cordinator > 0 && userDetail != null && userDetail.CordinatorDetail != null && userDetail.CordinatorDetail.Count > 0)
                    {
                        userDetail.CordinatorDetail[0].User_LoginName = userDetail.CordinatorDetail[0].User_LoginName.Replace(@".", "");
                        FirebaseUserGroupDTO userGroup = new FirebaseUserGroupDTO();
                        userGroup.id = userDetail.CordinatorDetail[0].User_LoginName;
                        userGroup.avatar = "http://gravatar.com/avatar/" + GenerateMD5(userGroup.id) + "?d=identicon";
                        await AddFirebaseUserGroupIPLNO(userGroup, workOrder.IPLNO , userDetail.CordinatorDetail[0].IPL_Company_ID, userDetail.CordinatorDetail[0].User_LoginName.ToLower(), workOrderUpdateStatusDTO.address1);

                        // workOrderUpdateStatusDTO.WN_UserId = workOrder.Cordinator;
                        //var nObj = SendCreatedWebNotification(workOrderUpdateStatusDTO);
                    }
                    if (workOrder.Processor > 0 && userDetail != null && userDetail.ProcessorDetail != null && userDetail.ProcessorDetail.Count > 0)
                    {
                        userDetail.ProcessorDetail[0].User_LoginName = userDetail.ProcessorDetail[0].User_LoginName.Replace(@".", "");
                        FirebaseUserGroupDTO userGroup = new FirebaseUserGroupDTO();
                        userGroup.id = userDetail.ProcessorDetail[0].User_LoginName;
                        userGroup.avatar = "http://gravatar.com/avatar/" + GenerateMD5(userGroup.id) + "?d=identicon";
                        await AddFirebaseUserGroupIPLNO(userGroup, workOrder.IPLNO, userDetail.ProcessorDetail[0].IPL_Company_ID, userDetail.ProcessorDetail[0].User_LoginName.ToLower(), workOrderUpdateStatusDTO.address1);

                        //workOrderUpdateStatusDTO.WN_UserId = workOrder.Processor;
                        //var nObj = SendCreatedWebNotification(workOrderUpdateStatusDTO);
                    }

                    if (userDetail != null && userDetail.AdminUserDetail != null && userDetail.AdminUserDetail.Count > 0)
                    {
                        foreach (var adminUser in userDetail.AdminUserDetail)
                        {
                            // workOrderUpdateStatusDTO.WN_UserId = adminUser.User_pkeyID;
                            //var nObj = SendCreatedWebNotification(workOrderUpdateStatusDTO);

                            adminUser.User_LoginName = adminUser.User_LoginName.Replace(@".", "");
                            FirebaseUserGroupDTO userGroup = new FirebaseUserGroupDTO();
                            userGroup.id = adminUser.User_LoginName;
                            userGroup.avatar = "http://gravatar.com/avatar/" + GenerateMD5(userGroup.id) + "?d=identicon";
                            await AddFirebaseUserGroupIPLNO(userGroup, workOrder.IPLNO , adminUser.IPL_Company_ID, adminUser.User_LoginName.ToLower(), workOrderUpdateStatusDTO.address1);
                        }
                    }


                }

                if (workOrder.Type == 2) //Add message 
                {
                    if (userDetail != null && userDetail.Old_ContractorID > 0 && Convert.ToInt64(workOrder.Contractor) != userDetail.Old_ContractorID && userDetail.Old_ContractorDetail != null && userDetail.Old_ContractorDetail.Count > 0)
                    {
                        userDetail.Old_ContractorDetail[0].User_LoginName = userDetail.Old_ContractorDetail[0].User_LoginName.Replace(@".", "");
                        await DeleteFirebaseUserGroupIPLNO(workOrder.IPLNO , userDetail.Old_ContractorDetail[0].IPL_Company_ID, userDetail.Old_ContractorDetail[0].User_LoginName.ToLower());
                    }
                    if (userDetail != null && userDetail.Old_CordinatorID > 0 && workOrder.Cordinator != userDetail.Old_CordinatorID && userDetail.Old_CordinatorDetail != null && userDetail.Old_CordinatorDetail.Count > 0)
                    {
                        userDetail.Old_CordinatorDetail[0].User_LoginName = userDetail.Old_CordinatorDetail[0].User_LoginName.Replace(@".", "");
                        await DeleteFirebaseUserGroupIPLNO(workOrder.IPLNO , userDetail.Old_CordinatorDetail[0].IPL_Company_ID, userDetail.Old_CordinatorDetail[0].User_LoginName.ToLower());
                    }
                    if (userDetail != null && userDetail.Old_ProcessorID > 0 && workOrder.Processor != userDetail.Old_ProcessorID && userDetail.Old_ProcessorDetail != null && userDetail.Old_ProcessorDetail.Count > 0)
                    {
                        userDetail.Old_ProcessorDetail[0].User_LoginName = userDetail.Old_ProcessorDetail[0].User_LoginName.Replace(@".", "");
                        await DeleteFirebaseUserGroupIPLNO(workOrder.IPLNO , userDetail.Old_ProcessorDetail[0].IPL_Company_ID, userDetail.Old_ProcessorDetail[0].User_LoginName.ToLower());
                    }

                    if (Convert.ToInt64(workOrder.Contractor) > 0 && userDetail != null && userDetail.ContractorDetail != null && userDetail.ContractorDetail.Count > 0)
                    {
                        // Notification code when Workorder is Edited
                        if (!string.IsNullOrWhiteSpace(userDetail.ContractorDetail[0].UserToken))
                        {
                            NotificationGetData notificationGetData = new NotificationGetData();
                            var msgResult = notificationGetData.SendNotification(userDetail.ContractorDetail[0].UserToken, "Work order with IPL number #" + workOrder.IPLNO +" " + workOrderUpdateStatusDTO.address1 + " has been Modified.", "IPL WorkOrder ", workOrder.workOrder_ID.ToString(),2, userDetail.IPLNo);
                        }

                        userDetail.ContractorDetail[0].User_LoginName = userDetail.ContractorDetail[0].User_LoginName.Replace(@".", "");
                        FirebaseUserGroupDTO userGroup = new FirebaseUserGroupDTO();
                        userGroup.id = userDetail.ContractorDetail[0].User_LoginName.ToLower();
                        userGroup.avatar = "http://gravatar.com/avatar/" + GenerateMD5(userGroup.id) + "?d=identicon";
                        await AddFirebaseUserGroupIPLNO(userGroup, workOrder.IPLNO , userDetail.ContractorDetail[0].IPL_Company_ID, userDetail.ContractorDetail[0].User_LoginName.ToLower(), workOrderUpdateStatusDTO.address1);
                    }
                    if (workOrder.Cordinator > 0 && userDetail != null && userDetail.CordinatorDetail != null && userDetail.CordinatorDetail.Count > 0)
                    {
                        userDetail.CordinatorDetail[0].User_LoginName = userDetail.CordinatorDetail[0].User_LoginName.Replace(@".", "");
                        FirebaseUserGroupDTO userGroup = new FirebaseUserGroupDTO();
                        userGroup.id = userDetail.CordinatorDetail[0].User_LoginName;
                        userGroup.avatar = "http://gravatar.com/avatar/" + GenerateMD5(userGroup.id) + "?d=identicon";
                        await AddFirebaseUserGroupIPLNO(userGroup, workOrder.IPLNO , userDetail.CordinatorDetail[0].IPL_Company_ID, userDetail.CordinatorDetail[0].User_LoginName.ToLower(), workOrderUpdateStatusDTO.address1);
                    }
                    if (workOrder.Processor > 0 && userDetail != null && userDetail.ProcessorDetail != null && userDetail.ProcessorDetail.Count > 0)
                    {
                        userDetail.ProcessorDetail[0].User_LoginName = userDetail.ProcessorDetail[0].User_LoginName.Replace(@".", "");
                        FirebaseUserGroupDTO userGroup = new FirebaseUserGroupDTO();
                        userGroup.id = userDetail.ProcessorDetail[0].User_LoginName;
                        userGroup.avatar = "http://gravatar.com/avatar/" + GenerateMD5(userGroup.id) + "?d=identicon";
                        await AddFirebaseUserGroupIPLNO(userGroup, workOrder.IPLNO, userDetail.ProcessorDetail[0].IPL_Company_ID, userDetail.ProcessorDetail[0].User_LoginName.ToLower(), workOrderUpdateStatusDTO.address1);
                    }


                    this.WorkOrderFirebaseNotification(old_workorderData);
                }

                InstctionMasterDTO model = new InstctionMasterDTO();

                List<dynamic> objdataval = new List<dynamic>();
                EmailTemplateData emailTemplateData = new EmailTemplateData();
                EmailWorkOderDTO emailWorkOderDTO = new EmailWorkOderDTO();
                if (workOrder.workOrder_ID == 0)
                {
                    emailWorkOderDTO.workOrder_ID = Convert.ToInt64(objdata[0].workOrder_ID);
                    model.Instr_WO_Id = Convert.ToInt64(objdata[0].workOrder_ID);
                }
                else
                {
                    emailWorkOderDTO.workOrder_ID = workOrder.workOrder_ID;
                    model.Instr_WO_Id = workOrder.workOrder_ID;
                }

                emailWorkOderDTO.UserID = workOrder.currUserId;
                emailWorkOderDTO.Type = 1;
                emailWorkOderDTO.Val_Type = 1;
                if (workOrder.Type == 1) //If Workorder created
                {
                    await emailTemplateData.GetEmailWorkOderDetail(emailWorkOderDTO);
                }
                else if (workOrder.Type == 2)
                {
                    if (old_workorderData.Contractor != workOrder.Contractor) //If Contractor changed
                    {
                        await emailTemplateData.GetEmailWorkOderDetail(emailWorkOderDTO);
                    }
                }


                if (workOrder.Type == 1 || workOrder.Type == 2 || workOrder.Type == 6) // Added by Dipali
                {
                    var isFileAdded = AddWorkOrderFiles(model, workOrder);
                }

                return objdata;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return null;
            }
        }



        public DataSet PvtGetWorkOrderData(workOrderxDTO work)
        {
            DataSet ds = new DataSet();
            try
            {
                string insertProcedure = "[GetWorkOrderMaster]";
                //string insertProcedure = "[GetWorkOrderMaster_v001]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@workOrder_ID", 1 + "#bigint#" + work.workOrder_ID);
                input_parameters.Add("@UserID", 1 + "#bigint#" + work.UserID);
                input_parameters.Add("@whereClause", 1 + "#nvarchar#" + work.whereClause);
                input_parameters.Add("@PageNumber", 1 + "#int#" + work.PageNumber);
                input_parameters.Add("@NoofRows", 1 + "#int#" + work.NoofRows);
                input_parameters.Add("@Skip", 1 + "#int#" + work.Skip);
                input_parameters.Add("@Type", 1 + "#int#" + work.Type);

                ds = obj.SelectSql(insertProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ds;
        }

        public string GenerateWhereClauseforFilter(string FilterData)
        {
            string whereclause = string.Empty;
            try
            {
                var Data = JsonConvert.DeserializeObject<List<FilterDataDTO>>(FilterData);
                for (int i = 0; i < Data.Count; i++)
                {
                    string Field = string.Empty, Value = string.Empty;
                    switch (Data[i].field)
                    {
                        case "dueDate":
                        case "clientDueDate":
                        case "Cancel_Date":
                        case "DateCreated":
                        case "Field_complete_date":
                        case "SentToClient_date":
                        case "startDate":
                        case "Complete_Date":
                        case "LastCutDate":
                        case "NextCutDate":
                        case "EstimatedDate":

                            {
                                
                                Field = "cast( wo."+Data[i].field + " as date)";
                                Value = " = cast('" + Convert.ToDateTime(Data[i].value) + "' as date)";
                                break;
                            }
                        default:
                            {
                                Field = "wo." + Data[i].field;
                                Value = " like '%" + Data[i].value + "%'"; 
                                break;
                            }
                    }

                    if (string.IsNullOrEmpty(whereclause))
                    {
                        whereclause =  Field + Value;
                    }
                    else
                    {
                        whereclause = whereclause +" and " +  Field + Value;
                    }
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return whereclause;
        } 

        public List<dynamic> GetWorkOrderFilterData(StatusDetailsDTO model)
        {
            List<dynamic> objdata = new List<dynamic>();
            try
            {
                workOrderxDTO workOrderxDTO = new workOrderxDTO();
                
                if (!string.IsNullOrWhiteSpace(model.whereclause))
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
                {
                    var Data = JsonConvert.DeserializeObject<List<StatusMasterDto>>(model.whereclause);
                    String whereCondition = string.Empty, strFinalwhere = String.Empty;
                    String strparams = string.Empty;
                    for (int i = 0; i < Data.Count; i++)
                    {

                        if (Data[i].mydata == true)
                        {
                            if (!string.IsNullOrEmpty(strparams))
                            {
                                if (Data[i].Status_ID == 2)
                                {
                                    strparams = strparams + " , 3 , " + Data[i].Status_ID;
                                }
                                else
                                {
                                    strparams = strparams + " ,  " + Data[i].Status_ID;
                                }

                            }
                            else
                            {
                                if (Data[i].Status_ID == 2)
                                {
                                    strparams = " 3," + Data[i].Status_ID.ToString();
                                }
                                else
                                {
                                    strparams = Data[i].Status_ID.ToString();
                                }

                            }

                        }

                    }
                    if (!string.IsNullOrEmpty(strparams))
                    {
                        strFinalwhere = " and wo.[status] in  ( " + strparams + " )";
                    }
                    if (!string.IsNullOrWhiteSpace(model.FilterData))
                    {
                        string where = GenerateWhereClauseforFilter(model.FilterData);
                        if (!string.IsNullOrEmpty(where))
                        {
                            if (!string.IsNullOrEmpty(strFinalwhere))
                            {
                                strFinalwhere = strFinalwhere + " and " + where;
                            }
                            else
                            {
                                strFinalwhere = " and " + where;
                            }
                        }
                        
                       
                    }


                    if (!string.IsNullOrEmpty(strFinalwhere))
                    {
                        whereCondition = strFinalwhere;
                        workOrderxDTO.whereClause = whereCondition;
                        workOrderxDTO.Type = 5;
                        workOrderxDTO.UserID = model.UserID;
                        workOrderxDTO.Skip = model.Skip;
                        workOrderxDTO.PageNumber = model.PageNumber;
                        workOrderxDTO.NoofRows = model.NoofRows;
                        objdata = GetWorkOrderData(workOrderxDTO);
                    }

                }


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return objdata;
            }
            return objdata;
        }


        public List<dynamic> GetWorkOrderData(workOrderxDTO work)
        {
            List<dynamic> objdata = new List<dynamic>();
            try
            {
                ////string insertProcedure = "[GetWorkOrderMaster]";
                //string insertProcedure = "[GetWorkOrderMaster_v001]";
                //Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                //input_parameters.Add("@workOrder_ID", 1 + "#bigint#" + work.workOrder_ID);
                //input_parameters.Add("@UserID", 1 + "#bigint#" + work.UserID);
                //input_parameters.Add("@whereClause", 1 + "#nvarchar#" + work.whereClause);
                //input_parameters.Add("@Type", 1 + "#int#" + work.Type);

                DataSet ds = PvtGetWorkOrderData(work);

                var myEnumerable = ds.Tables[0].AsEnumerable();
                List<ViewworkOrder> WorkOrderData =
                    (from item in myEnumerable
                     select new ViewworkOrder
                     {
                         workOrder_ID = item.Field<Int64>("workOrder_ID"),
                         workOrderNumber = item.Field<String>("workOrderNumber"),
                         Status_Name = item.Field<String>("Status_Name"),
                         dueDate = item.Field<DateTime?>("dueDate"),
                         Received_Date = item.Field<DateTime?>("Received_Date"),
                         clientDueDate = item.Field<DateTime?>("clientDueDate"),
                         EstimatedDate = item.Field<DateTime?>("EstimatedDate"),
                         Com_Name = item.Field<String>("Com_Name"),
                         Complete_Date = item.Field<DateTime?>("Complete_Date"),
                         address1 = item.Field<String>("address1"),
                         city = item.Field<String>("city"),
                         Cancel_Date = item.Field<DateTime?>("Cancel_Date"),
                         DateCreated = item.Field<DateTime?>("DateCreated"),
                         startDate = item.Field<DateTime?>("startDate"),
                         Loan_Number = item.Field<String>("Loan_Number"),
                         Loan_Info = item.Field<String>("Loan_Info"),
                         country = item.Field<String>("country"),
                         Lotsize = item.Field<String>("Lotsize"),
                         Lock_Code = item.Field<String>("Lock_Code"),
                         assigned_admin = item.Field<Int64?>("assigned_admin"),
                         state = item.Field<String>("state"),
                         zip = item.Field<Int64?>("zip"),
                         IPLNO = item.Field<String>("IPLNO"),


                         gpsLatitude = item.Field<String>("gpsLatitude"),
                         gpsLongitude = item.Field<String>("gpsLongitude"),
                         ContractorName = item.Field<String>("ContractorName"),
                         photocount = item.Field<int>("photocount"),
                         addresscount = item.Field<int>("addresscount"),
                         WT_WorkType = item.Field<String>("WT_WorkType"),
                         Main_Cat_Name = item.Field<String>("Main_Cat_Name"),
                         Field_complete_date = item.Field<DateTime?>("Field_complete_date"),
                         Cust_Num_Number = item.Field<String>("Cust_Num_Number"),
                         CordinatorName = item.Field<String>("CordinatorName"),
                         ProcessorName = item.Field<String>("ProcessorName"),
                         Days_Infield = item.Field<int?>("Days_Infield"),
                         Inv_Client_Invoice_Number = item.Field<String>("Inv_Client_Invoice_Number"),
                         Inv_Client_Sub_Total = item.Field<Decimal?>("Inv_Client_Sub_Total"),
                         Inv_Client_Inv_Date = item.Field<DateTime?>("Inv_Client_Inv_Date"),
                         Inv_Client_Status = item.Field<string>("Inv_Client_Status"),
                         Inv_Client_pkeyId = item.Field<Int64?>("Inv_Client_pkeyId"),
                         Client_Balance = item.Field<Decimal?>("Client_Balance"),
                         Client_Payment = item.Field<Decimal?>("Client_Payment"),

                         Inv_Con_Invoce_Num = item.Field<String>("Inv_Con_Invoce_Num"),
                         Inv_Con_Sub_Total = item.Field<Decimal?>("Inv_Con_Sub_Total"),
                         Inv_Con_Inv_Date = item.Field<DateTime?>("Inv_Con_Inv_Date"),
                         Inv_Con_Status = item.Field<string>("Inv_Con_Status"),
                         Inv_Con_pkeyId = item.Field<Int64?>("Inv_Con_pkeyId"),
                         Contractor_Balance = item.Field<Decimal?>("Contractor_Balance"),
                         Contractor_Payment = item.Field<Decimal?>("Contractor_Payment"),

                         Mortgagor = item.Field<String>("Mortgagor"),
                         Loan_Type = item.Field<String>("Loan_Type"),
                         Main_Cat_Back_Color = item.Field<String>("Main_Cat_Back_Color"),
                         status = item.Field<String>("status"),
                         Client_Company_Name = item.Field<String>("Client_Company_Name"),
                         IsEdit = item.Field<bool?>("IsEdit"),
                         ecdnotecount = item.Field<int>("ecdnotecount"),
                         CountyName = item.Field<string>("CountyName"),
                         ImportID = item.Field<string>("ImportID"),
                         ImportName = item.Field<string>("ImportName"),
                         Import_ID = item.Field<string>("ImportID"),
                         Occupancy_Status = item.Field<string>("Occupancy_Status"),
                         Property_Status = item.Field<string>("Property_Status"),
                         Loan_Status = item.Field<string>("Loan_Status"),

                         LastCutDate = item.Field<DateTime?>("LastCutDate"),
                         NextCutDate = item.Field<DateTime?>("NextCutDate"),

                         // import_from_id = item.Field<Int64?>("import_from_id"),

                         ContractorLoginName = item.Field<string>("ContractorLoginName"),
                         ContractorEmail = item.Field<string>("ContractorEmail"),
                         ContractorCellNumber = item.Field<string>("ContractorCellNumber"),
                         ContractorAddress = item.Field<string>("ContractorAddress"),


                         CordinatorLoginName = item.Field<string>("CordinatorLoginName"),
                         CordinatorEmail = item.Field<string>("CordinatorEmail"),
                         CordinatorCellNumber = item.Field<string>("CordinatorCellNumber"),
                         CordinatorAddress = item.Field<string>("CordinatorAddress"),


                         ProcessorLoginName = item.Field<string>("ProcessorLoginName"),
                         ProcessorEmail = item.Field<string>("ProcessorEmail"),
                         ProcessorCellNumber = item.Field<string>("ProcessorCellNumber"),
                         ProcessorAddress = item.Field<string>("ProcessorAddress"),

                         ContractorBackColor = item.Field<string>("ContractorBackColor"),
                         SentToClient_date =  item.Field<DateTime?>("SentToClient_date"),
                         IsAlert = item.Field<Boolean?>("IsAlert"),
                         IsLock = item.Field<Boolean?>("IsLock"),
                         PA_Name = item.Field<string>("PA_Name"),
                         Lock_Name = item.Field<string>("Lock_Name"),



                         ViewUrl = null
                         #region comment
                         //workOrderInfo=item.Field<String>("workOrderInfo"),
                         //address1=item.Field<String>("address1"),
                         //address2=item.Field<String>("address2"),
                         //city=item.Field<String>("city"),
                         //state=item.Field<String>("state"),
                         //zip=item.Field<Int64?>("zip"),
                         //country=item.Field<String>("country"),
                         //options=item.Field<String>("options"),
                         //reference=item.Field<String>("reference"),
                         //description=item.Field<String>("description"),
                         //instructions=item.Field<String>("instructions"),
                         //status=item.Field<String>("status"),
                         //dueDate=item.Field<DateTime?>("dueDate"),
                         //strdueDate = item.Field<String>("strdueDate"),
                         //startDate =item.Field<DateTime?>("startDate"),
                         //clientInstructions=item.Field<String>("clientInstructions"),
                         //clientStatus=item.Field<String>("clientStatus"),
                         //clientDueDate=item.Field<DateTime?>("clientDueDate"),
                         //gpsLatitude=item.Field<String>("gpsLatitude"),
                         //gpsLongitude=item.Field<String>("gpsLongitude"),
                         //attribute7 = item.Field<String>("attribute7"),
                         //attribute8 = item.Field<String>("attribute8"),
                         //attribute9 = item.Field<Int64?>("attribute9"),
                         //attribute10 = item.Field<Int64?>("attribute10"),
                         //attribute11 = item.Field<Int64?>("attribute11"),
                         //attribute12 = item.Field<Int64?>("attribute12"),
                         //attribute13 = item.Field<Int64?>("attribute13"),
                         //attribute14 = item.Field<Int64?>("attribute14"),
                         //attribute15 = item.Field<Int64?>("attribute15"),
                         //source_wo_provider=item.Field<String>("source_wo_provider"),
                         //source_wo_number=item.Field<String>("source_wo_number"),
                         //source_wo_id=item.Field<Int64?>("source_wo_id"),
                         //controlConfig=item.Field<Int64?>("controlConfig"),
                         //services_Id=item.Field<Int64?>("services_Id"),
                         //Lock_Location = item.Field<String>("Lock_Location"),
                         //Key_Code = item.Field<String>("Key_Code"),
                         //Gate_Code = item.Field<String>("Gate_Code"),
                         //Loan_Number = item.Field<String>("Loan_Number"),
                         //Search_Address = item.Field<String>("Search_Address"),
                         //IPLNO = item.Field<String>("IPLNO"),
                         //Office_Approved = item.Field<String>("Office_Approved"),
                         //DateCreated = item.Field<DateTime?>("DateCreated"),
                         //EstimatedDate = item.Field<DateTime?>("EstimatedDate"),
                         #endregion


                     }).ToList();
                objdata.Add(WorkOrderData);

                if (work.Type == 1 || work.Type == 5)
                {
                    if (ds.Tables.Count >1)
                    {
                        for (int i = 1; i < ds.Tables.Count; i++)
                        {
                            //result=ds.Tables[i];

                            objdata.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                           
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage("---------Type----->" + work.Type + "<----------------");
                log.logErrorMessage("---------UserID----->" + work.UserID + "<----------------");
                log.logErrorMessage("GetWorkOrderData......." + work.workOrder_ID + " < ----------------");

                log.logErrorMessage(ex.Message);

                return objdata;
            }
            return objdata;
        }

        // get client result
        public List<dynamic> GetWorkOrderDataClientResult(workOrderxDTO work)
        {
            List<dynamic> objdata = new List<dynamic>();
            try
            {
                string insertProcedure = "[GetWorkOrder_ClientResult]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@workOrder_ID", 1 + "#bigint#" + work.workOrder_ID);
                input_parameters.Add("@Type", 1 + "#int#" + work.Type);
                input_parameters.Add("@UserID", 1 + "#int#" + work.UserID);

                DataSet ds = obj.SelectSql(insertProcedure, input_parameters);

                var myEnumerable = ds.Tables[0].AsEnumerable();

                objdata.Add(obj.AsDynamicEnumerable(ds.Tables[0]));

                //List<workOrderxDTO> WorkOrderData =
                //    (from item in myEnumerable
                //     select new workOrderxDTO
                //     {
                //         workOrder_ID = item.Field<Int64>("workOrder_ID"),
                //         workOrderNumber = item.Field<String>("workOrderNumber"),
                //         workOrderInfo = item.Field<String>("workOrderInfo"),
                //         address1 = item.Field<String>("address1"),
                //         address2 = item.Field<String>("address2"),
                //         city = item.Field<String>("city"),
                //         state = item.Field<String>("state"),
                //         zip = item.Field<Int64?>("zip"),
                //         country = item.Field<String>("country"),

                //         //options = item.Field<String>("options"),
                //         //reference = item.Field<String>("reference"),
                //         // description = item.Field<String>("description"),
                //         //instructions = item.Field<String>("instructions"),
                //         status = item.Field<String>("status"),
                //         dueDate = item.Field<DateTime?>("dueDate"),
                //         startDate = item.Field<DateTime?>("startDate"),
                //         //clientInstructions = item.Field<String>("clientInstructions"),

                //         //  options = item.Field<String>("options"),
                //         //  reference = item.Field<String>("reference"),
                //         //  description = item.Field<String>("description"),
                //         //  instructions = item.Field<String>("instructions"),

                //         // clientInstructions = item.Field<String>("clientInstructions"),

                //         clientStatus = item.Field<String>("clientStatus"),
                //         clientDueDate = item.Field<DateTime?>("clientDueDate"),
                //         gpsLatitude = item.Field<String>("gpsLatitude"),
                //         gpsLongitude = item.Field<String>("gpsLongitude"),

                //         //attribute7 = item.Field<String>("attribute7"),
                //         //attribute8 = item.Field<String>("attribute8"),
                //         //attribute9 = item.Field<Int64?>("attribute9"),
                //         //attribute10 = item.Field<Int64?>("attribute10"),
                //         //attribute11 = item.Field<Int64?>("attribute11"),
                //         //attribute12 = item.Field<Int64?>("attribute12"),
                //         //attribute13 = item.Field<Int64?>("attribute13"),
                //         //attribute14 = item.Field<Int64?>("attribute14"),
                //         //attribute15 = item.Field<Int64?>("attribute15"),
                //         //source_wo_provider = item.Field<String>("source_wo_provider"),
                //         //source_wo_number = item.Field<String>("source_wo_number"),
                //         //source_wo_id = item.Field<Int64?>("source_wo_id"),
                //         //controlConfig = item.Field<Int64?>("controlConfig"),
                //         //services_Id = item.Field<Int64?>("services_Id"),



                //         WorkType = item.Field<Int64?>("WorkType"),
                //         Company = item.Field<String>("Company"),
                //         Com_Name = item.Field<String>("Com_Name"),
                //         Com_Phone = item.Field<String>("Com_Phone"),
                //         Com_Email = item.Field<String>("Com_Email"),
                //         Contractor = item.Field<String>("Contractor"),
                //         Received_Date = item.Field<DateTime?>("Received_Date"),
                //         Complete_Date = item.Field<DateTime?>("Complete_Date"),
                //         Cancel_Date = item.Field<DateTime?>("Cancel_Date"),
                //         IPLNO = item.Field<String>("IPLNO"),
                //         Mortgagor = item.Field<String>("Mortgagor"),
                //         Category = item.Field<Int64?>("Category"),
                //         Loan_Info = item.Field<String>("Loan_Info"),

                //         Loan_Type = item.Field<String>("Loan_Type"),

                //         Customer_Number = item.Field<Int64?>("Customer_Number"),
                //         Cordinator = item.Field<Int64?>("Cordinator"),
                //         BATF = item.Field<bool?>("BATF"),
                //         ISInspection = item.Field<bool?>("ISInspection"),
                //         Lotsize = item.Field<String>("Lotsize"),
                //         Rush = item.Field<Int64?>("Rush"),
                //         Lock_Code = item.Field<String>("Lock_Code"),
                //         Broker_Info = item.Field<String>("Broker_Info"),
                //         Comments = item.Field<String>("Comments"),

                //         SM_Name = item.Field<String>("SM_Name"),
                //         WT_WorkType = item.Field<String>("WT_WorkType"),
                //         Client_Company_Name = item.Field<String>("Client_Company_Name"),
                //         Cont_Name = item.Field<String>("Cont_Name"),
                //         Work_Type_Name = item.Field<String>("Work_Type_Name"),
                //         Cust_Num_Number = item.Field<String>("Cust_Num_Number"),
                //         rus_Name = item.Field<String>("rus_Name"),

                //         Cordinator_Name = item.Field<String>("Cordinator_Name"),
                //         Key_Code = item.Field<String>("Key_Code"),
                //         Loan_Number = item.Field<String>("Loan_Number"),
                //         Lock_Location = item.Field<String>("Lock_Location"),
                //         Gate_Code = item.Field<String>("Gate_Code"),
                //         ClientMetaData = item.Field<String>("ClientMetaData"),
                //         Client_Result_Photo_FilePath = item.Field<String>("Client_Result_Photo_FilePath"),
                //         Client_Result_Photo_FileName = item.Field<String>("Client_Result_Photo_FileName"),
                //         fulladdress = item.Field<String>("fulladdress"),
                //         Processor_Name = item.Field<String>("Processor_Name"),
                //         Processor = item.Field<Int64?>("Processor"),
                //         SentToClient_date = item.Field<DateTime?>("SentToClient_date"),
                //         OfficeApproved_date = item.Field<DateTime?>("OfficeApproved_date"),
                //         Field_complete_date = item.Field<DateTime?>("Field_complete_date"),
                //         Status_Name = item.Field<String>("Status_Name"),
                //         CORNT_User_LoginName = item.Field<String>("CORNT_User_LoginName"),
                //         CORNT_User_FirstName = item.Field<String>("CORNT_User_FirstName"),
                //         import_from = item.Field<Int64?>("import_from"),
                //         assigned_date = item.Field<DateTime?>("assigned_date"),
                //         DateCreated = item.Field<DateTime?>("DateCreated"),
                //         Work_Type_Cat_pkeyID = item.Field<Int64?>("Work_Type_Cat_pkeyID"),
                //         EstimatedDate = item.Field<DateTime?>("EstimatedDate"),

                //         Recurring = item.Field<Boolean?>("Recurring"),
                //         Recurs_Day = item.Field<Int32?>("Recurs_Day"),
                //         Recurs_Period = item.Field<Int64?>("Recurs_Period"),
                //         Recurs_Limit = item.Field<Int64?>("Recurs_Limit"),
                //         Recurs_CutOffDate = item.Field<DateTime?>("Recurs_CutOffDate"),
                //         Background_Provider = item.Field<Int64?>("Background_Provider"),
                //         Back_Chk_ProviderName = item.Field<String>("Back_Chk_ProviderName"),
                //         IsEdit = item.Field<bool?>("IsEdit"),


                //     }).ToList();
                //objdata.Add(WorkOrderData);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return objdata;
            }
            return objdata;
        }
        public List<dynamic> GetWorkorderNotification(GetWorkorderNotificationDetailsDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                
                DataSet ds = GetWorkorderNotificationDetails(model);
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    string User_Token_val = ds.Tables[0].Rows[i]["User_Token_val"].ToString();
                    string iplno = ds.Tables[0].Rows[i]["iplno"].ToString();
                    string Address = ds.Tables[0].Rows[i]["address1"].ToString() + " " + ds.Tables[0].Rows[i]["city"].ToString() + " " + ds.Tables[0].Rows[i]["state"].ToString() + " " + ds.Tables[0].Rows[i]["zip"].ToString();
                    switch (model.Action)
                    {
                        case 12:
                            {
                                model.message = "Work order with IPL number #" + iplno +" "+ Address + " has been Cancel.";
                                model.msgtitle = "IPL WorkOrder";
                                model.from = 2;
                                break;
                            }
                        case 2:
                            {
                                model.message = "Work order with IPL number #" + iplno +" "+ Address+ " has been Assigned.";
                                model.msgtitle = "IPL WorkOrder";
                                model.from = 3;
                                break;
                            }
                        case 6:
                            {
                                model.message = "Work order with IPL number #" + iplno +" "+ Address+ " Due Date has been changed.";
                                model.msgtitle = "IPL WorkOrder";
                                model.from = 4;
                                break;
                            }
                        case 7:
                            {
                                model.message = "Work order with IPL number #" + iplno +" "+ Address+ " Start Date has been changed.";
                                model.msgtitle = "IPL WorkOrder";
                                model.from = 4;
                                break;
                            }
                    }
                    if (!string.IsNullOrWhiteSpace(User_Token_val) && User_Token_val != "null")
                    {
                        NotificationGetData notificationGetData = new NotificationGetData();
                        var response = notificationGetData.SendNotification(User_Token_val, model.message, model.msgtitle, model.workOrder_ID.ToString(), model.from,iplno);
                        objDynamic.Add(response);
                    }
                    else
                    {
                        log.logDebugMessage("Notication from Action ------>UserToken is Null for "+ model.workOrder_ID.ToString());
                    }
                   
                }

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        //workorder Actions 
        private async Task<List<dynamic>> UpdateWorkorderAction(workoderaction model)
        {
            List<dynamic> objdata = new List<dynamic>();
            ReturnworkOrder returnworkOrder = new ReturnworkOrder();
            EmailWorkOderDTO emailWorkOderDTO = new EmailWorkOderDTO();
            try
            {
                string insertProcedure = "[Update_WorkOrderActions]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@whereClause", 1 + "#varchar#" + model.whereClause);
                input_parameters.Add("@Assigned_Param", 1 + "#varchar#" + model.Assigned_Param);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);


                switch (model.Type)
                {
                    case 1:
                        {
                            for (int i = 0; i < model.workOrderIDItems.Count; i++)
                            {
                                GetWorkorderNotificationDetailsDTO getWorkorderNotificationDetailsDTO = new GetWorkorderNotificationDetailsDTO();

                                getWorkorderNotificationDetailsDTO.workOrder_ID = model.workOrderIDItems[i].WorkOrderID;
                                getWorkorderNotificationDetailsDTO.UserID = model.UserId;
                                getWorkorderNotificationDetailsDTO.Type = 1;
                                getWorkorderNotificationDetailsDTO.Action = 12;
                                var recObjDatas = GetWorkorderNotification(getWorkorderNotificationDetailsDTO);
                            }
                                break;
                        }
                }

                objdata = obj.SqlCRUD(insertProcedure, input_parameters);

                switch (model.Type)
                {
                    case 1:
                        {


                            EmailTemplateData emailTemplateData = new EmailTemplateData();

                            for (int i = 0; i < model.workOrderIDItems.Count; i++)
                            {
                                emailWorkOderDTO.workOrder_ID = model.workOrderIDItems[i].WorkOrderID;
                                emailWorkOderDTO.UserID = model.UserId;
                                emailWorkOderDTO.Type = 1;
                                emailWorkOderDTO.Val_Type = 1;
                                await emailTemplateData.GetEmailWorkOderDetail(emailWorkOderDTO);

                                GetWorkorderNotificationDetailsDTO getWorkorderNotificationDetailsDTO = new GetWorkorderNotificationDetailsDTO();

                                getWorkorderNotificationDetailsDTO.workOrder_ID = model.workOrderIDItems[i].WorkOrderID;
                                getWorkorderNotificationDetailsDTO.UserID = model.UserId;
                                getWorkorderNotificationDetailsDTO.Type = 1;
                                getWorkorderNotificationDetailsDTO.Action = 2;
                                var recObjDatas = GetWorkorderNotification(getWorkorderNotificationDetailsDTO);


                            }
                            break;
                        }
                    case 12:
                        {    emailWorkOderDTO.Val_Type = 2;
                            for (int i = 0; i < model.workOrderIDItems.Count; i++)
                            {
                                emailWorkOderDTO.workOrder_ID = model.workOrderIDItems[i].WorkOrderID;
                                emailWorkOderDTO.UserID = model.UserId;
                                emailWorkOderDTO.Type = 1;
                                await GetEmailCancelWorkOderDetail(emailWorkOderDTO);

                                GetWorkorderNotificationDetailsDTO getWorkorderNotificationDetailsDTO = new GetWorkorderNotificationDetailsDTO();

                                getWorkorderNotificationDetailsDTO.workOrder_ID = model.workOrderIDItems[i].WorkOrderID;
                                getWorkorderNotificationDetailsDTO.UserID = model.UserId;
                                getWorkorderNotificationDetailsDTO.Type = 1;
                                getWorkorderNotificationDetailsDTO.Action = 12;
                                var recObjDatas = GetWorkorderNotification(getWorkorderNotificationDetailsDTO);
                            }


                            break;
                        }
                    case 6:
                        {
                            for (int i = 0; i < model.workOrderIDItems.Count; i++)
                            {
                                GetWorkorderNotificationDetailsDTO getWorkorderNotificationDetailsDTO = new GetWorkorderNotificationDetailsDTO();

                                getWorkorderNotificationDetailsDTO.workOrder_ID = model.workOrderIDItems[i].WorkOrderID;
                                getWorkorderNotificationDetailsDTO.UserID = model.UserId;
                                getWorkorderNotificationDetailsDTO.Type = 1;
                                getWorkorderNotificationDetailsDTO.Action = 6;
                                var recObjDatas = GetWorkorderNotification(getWorkorderNotificationDetailsDTO);
                            }
                                break;
                        }
                    case 7:
                        {
                            for (int i = 0; i < model.workOrderIDItems.Count; i++)
                            {
                                GetWorkorderNotificationDetailsDTO getWorkorderNotificationDetailsDTO = new GetWorkorderNotificationDetailsDTO();

                                getWorkorderNotificationDetailsDTO.workOrder_ID = model.workOrderIDItems[i].WorkOrderID;
                                getWorkorderNotificationDetailsDTO.UserID = model.UserId;
                                getWorkorderNotificationDetailsDTO.Type = 1;
                                getWorkorderNotificationDetailsDTO.Action = 7;
                                var recObjDatas = GetWorkorderNotification(getWorkorderNotificationDetailsDTO);
                            }
                            break;
                        }
                }

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage("----------------Data Start------------");
                log.logErrorMessage(model.whereClause);
                log.logErrorMessage(model.Type);
                log.logErrorMessage("----------------Data End------------");
                log.logErrorMessage(ex.Message);
                return null;
            }

            return objdata;

        }




        //cancel email work order
        private DataSet GetEmailCancelWorkOderData(EmailWorkOderDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[GetWorkOrder_EmailMaster]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@workOrder_ID", 1 + "#bigint#" + model.workOrder_ID);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Val_Type", 1 + "#int#" + model.Val_Type);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }
        public async Task<int> GetEmailCancelWorkOderDetail(EmailWorkOderDTO model)
        {
            DataSet dsdata = null;
            EmailManager emailManager = new EmailManager();
            //EmailDTO emailDTO = new EmailDTO();
            DyanmicEmailDTO emailDTO = new DyanmicEmailDTO();
            int ret = 0;
            string strSubject = string.Empty;
            string strBody = string.Empty;
            string strTaskComments = string.Empty;
            string strStatus = "0";
            try
            {
                dsdata = GetEmailCancelWorkOderData(model);

                if (dsdata.Tables.Count > 2)
                {
                    for (int i = 0; i < dsdata.Tables[2].Rows.Count; i++)
                    {
                        strStatus = dsdata.Tables[2].Rows[i]["status"].ToString();
                    }
                }

                if (strStatus == "10")
                {
                    if (dsdata.Tables.Count > 1 && dsdata.Tables[1].Rows.Count > 0)
                    {
                        strSubject = dsdata.Tables[1].Rows[0]["Email_Temp_Subject"].ToString();
                        strBody = dsdata.Tables[1].Rows[0]["Email_Temp_HTML"].ToString();
                    }
                    if (dsdata.Tables.Count > 2 && dsdata.Tables[2].Rows.Count > 0)
                    {

                        model.ContractorName = dsdata.Tables[2].Rows[0]["ContractorName"].ToString();
                        model.ContractorEmail = dsdata.Tables[2].Rows[0]["ContractorEmail"].ToString();
                        model.Processor_Name = dsdata.Tables[2].Rows[0]["Processor_Name"].ToString();
                        model.ProcessorEmail = dsdata.Tables[2].Rows[0]["ProcessorEmail"].ToString();
                        model.Cordinator_Name = dsdata.Tables[2].Rows[0]["Cordinator_Name"].ToString();
                        model.CordinatorEmail = dsdata.Tables[2].Rows[0]["CordinatorEmail"].ToString();
                        model.User_FirstName = dsdata.Tables[2].Rows[0]["User_FirstName"].ToString();
                        model.Cordinator_Phone = dsdata.Tables[2].Rows[0]["Cordinator_Phone"].ToString();
                        model.Comments = dsdata.Tables[2].Rows[0]["Inst_Ch_Comand_Mobile"].ToString();
                        model.Client_Result_Photo_FilePath = dsdata.Tables[2].Rows[0]["Client_Result_Photo_FilePath"].ToString();

                        model.workOrderNumber = dsdata.Tables[2].Rows[0]["workOrderNumber"].ToString();
                        model.WT_WorkType = dsdata.Tables[2].Rows[0]["WT_WorkType"].ToString();
                        model.address1 = dsdata.Tables[2].Rows[0]["address1"].ToString();
                        model.city = dsdata.Tables[2].Rows[0]["city"].ToString();
                        model.state = dsdata.Tables[2].Rows[0]["state"].ToString();
                        model.zip = dsdata.Tables[2].Rows[0]["zip"].ToString();
                        model.IPLNO = dsdata.Tables[2].Rows[0]["IPLNO"].ToString();
                        if (!string.IsNullOrEmpty(dsdata.Tables[2].Rows[0]["dueDate"].ToString()))
                        {
                            model.dueDate = Convert.ToDateTime(dsdata.Tables[2].Rows[0]["dueDate"].ToString());
                        }

                        model.Client_Company_Name = dsdata.Tables[2].Rows[0]["Client_Company_Name"].ToString();
                        model.Comments = dsdata.Tables[2].Rows[0]["Comments"].ToString();
                    }

                    string strTask = string.Empty, strDocument = string.Empty;
                    string strTaskFile = string.Empty, strDocumentFile = string.Empty;

                    if (dsdata.Tables.Count > 2)
                    {
                        for (int i = 0; i < dsdata.Tables[3].Rows.Count; i++)
                        {
                            string strTaskdetails = string.Empty;
                            string strTaskName = dsdata.Tables[3].Rows[i]["Task_Name"].ToString();
                            strTaskComments = dsdata.Tables[3].Rows[i]["Inst_Comand_Mobile_details"].ToString();
                            string strTaskFilePath = dsdata.Tables[3].Rows[i]["TMF_Task_localPath"].ToString();
                            string TMF_Task_FileName = dsdata.Tables[3].Rows[i]["TMF_Task_FileName"].ToString();

                            if (!string.IsNullOrEmpty(strTaskFilePath))
                            {
                                strTaskFile = "<a href " + strTaskFilePath + " > " + TMF_Task_FileName + " </ a >";
                            }


                            strTaskdetails = strTaskName + "<br/>" + strTaskComments;

                            strTask = "<br/>" + strTask + strTaskdetails + "<br/>" + strTaskFile + "<br/>";

                        }
                    }

                    if (dsdata.Tables.Count > 3)
                    {
                        for (int i = 0; i < dsdata.Tables[4].Rows.Count; i++)
                        {
                            string Inst_Doc_File_Path = dsdata.Tables[4].Rows[i]["Inst_Doc_File_Path"].ToString();
                            string Inst_Doc_File_Name = dsdata.Tables[4].Rows[i]["Inst_Doc_File_Name"].ToString();
                            if (!string.IsNullOrEmpty(Inst_Doc_File_Path))
                            {
                                strDocumentFile = "<a href" + Inst_Doc_File_Path + " > " + Inst_Doc_File_Name + " </ a >";
                            }

                            strDocument = "<br/>" + strDocument + "<br/>" + strDocumentFile + "<br/>";

                        }
                    }

                    if (dsdata.Tables.Count > 0)
                    {
                        for (int i = 0; i < dsdata.Tables[0].Rows.Count; i++)
                        {
                            string StrKeyData = dsdata.Tables[0].Rows[i]["Keydata"].ToString();
                            string strCloumnData = dsdata.Tables[0].Rows[i]["Wo_Column_Name"].ToString();
                            string value = string.Empty;
                            //string value = dsdata.Tables[2].Rows[0][StrKeyData].ToString();
                            //strSubject = strSubject.Replace(strCloumnData, " " + value);

                            if (StrKeyData == "Inst_Task_Name")
                            {
                                value = strTask;
                                strBody = strBody.Replace(strCloumnData, " " + strTask);
                            }
                            else if (StrKeyData == "Inst_Comand_Mobile_details")
                            {
                                value = strTaskComments;
                                strBody = strBody.Replace(strCloumnData, " " + value);
                            }
                            else if (StrKeyData == "dueDate")
                            {
                                if (model.dueDate != null)
                                {
                                    value = model.dueDate.Value.ToString("MM/dd/yyyy");
                                }

                                strBody = strBody.Replace(strCloumnData, " " + value);
                            }
                            else if (StrKeyData == "Document")
                            {
                                value = strDocument;
                                strBody = strBody.Replace(strCloumnData, " " + value);
                            }
                            else
                            {
                                if (dsdata.Tables.Count > 2 && dsdata.Tables[2].Rows.Count > 0)
                                {
                                    value = dsdata.Tables[2].Rows[0][StrKeyData].ToString();
                                    strBody = strBody.Replace(strCloumnData, " " + value);
                                }
                            }

                            strSubject = strSubject.Replace(strCloumnData, " " + value);
                        }

                        //  strBody = strBody + "<br/>" + "Documents" + "<br/>" + strDocument;
                    }

                    emailDTO.To = model.ContractorEmail;
                    emailDTO.Cc = model.CordinatorEmail;
                    emailDTO.Subject = strSubject;
                    emailDTO.Message = strBody;
                    ret = await DynamicEmailManager.SendDynamicEmail(emailDTO,7);

                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return ret;
        }


        public async Task<int> SendWorkOrderEmail(EmailWorkOderDTO model)
        {
            DataSet dsdata = null;
            EmailManager emailManager = new EmailManager();
            //EmailDTO emailDTO = new EmailDTO();
            DyanmicEmailDTO emailDTO = new DyanmicEmailDTO();
            int ret = 0;
            string strSubject = string.Empty;
            string strBody = string.Empty;
            string strTaskComments = string.Empty;
            string strStatus = "0";
            try
            {
                dsdata = GetEmailCancelWorkOderData(model);
                if (dsdata.Tables.Count > 2)
                {
                    for (int i = 0; i < dsdata.Tables[2].Rows.Count; i++)
                    {
                        strStatus = dsdata.Tables[2].Rows[i]["status"].ToString();
                    }
                }


                if (dsdata.Tables.Count > 1 && dsdata.Tables[1].Rows.Count > 0)
                {
                    strSubject = dsdata.Tables[1].Rows[0]["Email_Temp_Subject"].ToString();
                    strBody = dsdata.Tables[1].Rows[0]["Email_Temp_HTML"].ToString();
                }
                if (dsdata.Tables.Count > 2 && dsdata.Tables[2].Rows.Count > 0)
                {

                    model.ContractorName = dsdata.Tables[2].Rows[0]["ContractorName"].ToString();
                    model.ContractorEmail = dsdata.Tables[2].Rows[0]["ContractorEmail"].ToString();
                    model.Processor_Name = dsdata.Tables[2].Rows[0]["Processor_Name"].ToString();
                    model.ProcessorEmail = dsdata.Tables[2].Rows[0]["ProcessorEmail"].ToString();
                    model.Cordinator_Name = dsdata.Tables[2].Rows[0]["Cordinator_Name"].ToString();
                    model.CordinatorEmail = dsdata.Tables[2].Rows[0]["CordinatorEmail"].ToString();
                    model.User_FirstName = dsdata.Tables[2].Rows[0]["User_FirstName"].ToString();
                    model.Cordinator_Phone = dsdata.Tables[2].Rows[0]["Cordinator_Phone"].ToString();
                    model.Comments = dsdata.Tables[2].Rows[0]["Inst_Ch_Comand_Mobile"].ToString();
                    model.Client_Result_Photo_FilePath = dsdata.Tables[2].Rows[0]["Client_Result_Photo_FilePath"].ToString();

                    model.workOrderNumber = dsdata.Tables[2].Rows[0]["workOrderNumber"].ToString();
                    model.WT_WorkType = dsdata.Tables[2].Rows[0]["WT_WorkType"].ToString();
                    model.address1 = dsdata.Tables[2].Rows[0]["address1"].ToString();
                    model.city = dsdata.Tables[2].Rows[0]["city"].ToString();
                    model.state = dsdata.Tables[2].Rows[0]["state"].ToString();
                    model.zip = dsdata.Tables[2].Rows[0]["zip"].ToString();
                    model.IPLNO = dsdata.Tables[2].Rows[0]["IPLNO"].ToString();
                    if (!string.IsNullOrEmpty(dsdata.Tables[2].Rows[0]["dueDate"].ToString()))
                    {
                        model.dueDate = Convert.ToDateTime(dsdata.Tables[2].Rows[0]["dueDate"].ToString());
                    }

                    model.Client_Company_Name = dsdata.Tables[2].Rows[0]["Client_Company_Name"].ToString();
                    model.Comments = dsdata.Tables[2].Rows[0]["Comments"].ToString();
                }

                string strTask = string.Empty, strDocument = string.Empty;
                string strTaskFile = string.Empty, strDocumentFile = string.Empty;


                if (dsdata.Tables.Count > 2)
                {
                    for (int i = 0; i < dsdata.Tables[3].Rows.Count; i++)
                    {
                        string strTaskdetails = string.Empty;
                        string strTaskName = dsdata.Tables[3].Rows[i]["Task_Name"].ToString();
                        strTaskComments = dsdata.Tables[3].Rows[i]["Inst_Comand_Mobile_details"].ToString();
                        string strTaskFilePath = dsdata.Tables[3].Rows[i]["TMF_Task_localPath"].ToString();
                        string TMF_Task_FileName = dsdata.Tables[3].Rows[i]["TMF_Task_FileName"].ToString();

                        if (!string.IsNullOrEmpty(strTaskFilePath))
                        {
                            strTaskFile = "<a href " + strTaskFilePath + " > " + TMF_Task_FileName + " </ a >";
                        }


                        strTaskdetails = strTaskName + "<br/>" + strTaskComments;

                        strTask = "<br/>" + strTask + strTaskdetails + "<br/>" + strTaskFile + "<br/>";

                    }
                }

                if (dsdata.Tables.Count > 3)
                {
                    for (int i = 0; i < dsdata.Tables[4].Rows.Count; i++)
                    {
                        string Inst_Doc_File_Path = dsdata.Tables[4].Rows[i]["Inst_Doc_File_Path"].ToString();
                        string Inst_Doc_File_Name = dsdata.Tables[4].Rows[i]["Inst_Doc_File_Name"].ToString();
                        if (!string.IsNullOrEmpty(Inst_Doc_File_Path))
                        {
                            strDocumentFile = "<a href" + Inst_Doc_File_Path + " > " + Inst_Doc_File_Name + " </ a >";
                        }

                        strDocument = "<br/>" + strDocument + "<br/>" + strDocumentFile + "<br/>";

                    }
                }

                if (dsdata.Tables.Count > 0)
                {
                    for (int i = 0; i < dsdata.Tables[0].Rows.Count; i++)
                    {
                        string StrKeyData = dsdata.Tables[0].Rows[i]["Keydata"].ToString();
                        string strCloumnData = dsdata.Tables[0].Rows[i]["Wo_Column_Name"].ToString();
                        string value = string.Empty;
                        //string value = dsdata.Tables[2].Rows[0][StrKeyData].ToString();
                        //strSubject = strSubject.Replace(strCloumnData, " " + value);

                        if (StrKeyData == "Inst_Task_Name")
                        {
                            value = strTask;
                            strBody = strBody.Replace(strCloumnData, " " + strTask);
                        }
                        else if (StrKeyData == "Inst_Comand_Mobile_details")
                        {
                            value = strTaskComments;
                            strBody = strBody.Replace(strCloumnData, " " + value);
                        }
                        else if (StrKeyData == "dueDate")
                        {
                            if (model.dueDate != null)
                            {
                                value = model.dueDate.Value.ToString("MM/dd/yyyy");

                            }
                            strBody = strBody.Replace(strCloumnData, " " + value);
                        }
                        else if (StrKeyData == "Document")
                        {
                            value = strDocument;
                            strBody = strBody.Replace(strCloumnData, " " + value);
                        }
                        else
                        {
                            if (dsdata.Tables.Count > 2 && dsdata.Tables[2].Rows.Count > 0)
                            {
                                value = dsdata.Tables[2].Rows[0][StrKeyData].ToString();
                                strBody = strBody.Replace(strCloumnData, " " + value);
                            }
                        }

                        strSubject = strSubject.Replace(strCloumnData, " " + value);
                    }

                }

                if (!string.IsNullOrWhiteSpace(strSubject) && !string.IsNullOrWhiteSpace(strBody))
                {
                    emailDTO.To = model.ProcessorEmail;
                    emailDTO.Cc = model.CordinatorEmail;
                    emailDTO.Subject = strSubject;
                    emailDTO.Message = strBody;
                    ret = await DynamicEmailManager.SendDynamicEmail(emailDTO, 8);
                }
                else
                {
                    log.logDebugMessage("Email Check for WorkorderID ---->" + model.workOrder_ID);
                }
             


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return ret;
        }
        public async Task<List<dynamic>> MultiActions_WorkOrder(WorkoderActionItems model)
        {
            try
            {
                List<dynamic> objdata = new List<dynamic>();
                workoderaction workoderaction = new workoderaction();
                var Data = JsonConvert.DeserializeObject<List<WorkOrderIDItems>>(model.Arr_WorkOrderID);
                string strwhere = string.Empty;
                for (int i = 0; i < Data.Count; i++)
                {
                    if (!string.IsNullOrEmpty(strwhere))
                    {
                        strwhere = strwhere + "," + Data[i].WorkOrderID.ToString();
                    }
                    else
                    {
                        strwhere = Data[i].WorkOrderID.ToString();
                    }
                }

                workoderaction.whereClause = strwhere;
                workoderaction.Type = model.Type;
                workoderaction.UserId = model.UserId;
                workoderaction.Assigned_Param = model.WorkOrder_Action;
                workoderaction.workOrderIDItems = Data;

                objdata = await UpdateWorkorderAction(workoderaction);
                return objdata;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return null;
            }
        }

        private bool AddWorkOrderFiles(InstctionMasterDTO model, workOrderxDTO workOrder)
        {
            try
            {
                Instruction_DocumentData instruction_DocumentData = new Instruction_DocumentData();
                InstructionMasterData instructionMasterData = new InstructionMasterData();
                model.Type = 2;
                DataSet ds = instructionMasterData.GetInstructionMaster(model);
                if (ds.Tables.Count > 0 && model.Instr_WO_Id > 0)
                {
                    var myEnumerableFeaprdx = ds.Tables[1].AsEnumerable();
                    List<Instruction_Master_ChildDTO> InstructionChildDetails =
                       (from item in myEnumerableFeaprdx
                        select new Instruction_Master_ChildDTO
                        {
                            Inst_Ch_pkeyId = item.Field<Int64>("Inst_Ch_pkeyId"),
                            Inst_Ch_Wo_Id = item.Field<Int64?>("Inst_Ch_Wo_Id"),
                            Inst_Ch_Text = item.Field<String>("Inst_Ch_Text"),
                            Inst_Ch_IsActive = item.Field<Boolean?>("Inst_Ch_IsActive"),
                        }).ToList();

                    var fileList = GetFolderFileMaster(workOrder);
                    foreach (var file in fileList)
                    {
                        Instruction_DocumentDTO instruction_DocumentDTO = new Instruction_DocumentDTO();
                        instruction_DocumentDTO.Inst_Doc_Inst_Ch_ID = InstructionChildDetails[0].Inst_Ch_pkeyId;
                        instruction_DocumentDTO.Inst_Doc_Wo_ID = InstructionChildDetails[0].Inst_Ch_Wo_Id;
                        instruction_DocumentDTO.Inst_Doc_File_Path = file.Fold_File_Local_Path;
                        instruction_DocumentDTO.Inst_Doc_File_Name = file.Fold_File_Name;
                        instruction_DocumentDTO.Inst_Doc_Object_Name = file.Fold_File_Object_Name;
                        instruction_DocumentDTO.Inst_Doc_Folder_Name = file.Fold_File_Folder_Name;
                        instruction_DocumentDTO.Inst_Doc_UploadedBy = workOrder.currUserId.ToString();
                        instruction_DocumentDTO.Inst_Doc_IsActive = true;
                        instruction_DocumentDTO.Inst_Doc_IsDelete = false;
                        instruction_DocumentDTO.UserID = workOrder.currUserId;
                        instruction_DocumentDTO.Type = 1;
                        instruction_DocumentDTO.Folder_File_Master_FKId = file.Fold_File_Pkey_Id;
                        var fileObj = instruction_DocumentData.UploadInstructionDocumentData(instruction_DocumentDTO);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return false;
            }
        }

        private List<Folder_File_MasterDTO> GetFolderFileMaster(workOrderxDTO workOrder)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_WorkOrder_Files]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@client_Id", 1 + "#bigint#" + Convert.ToInt64(workOrder.Company));
                input_parameters.Add("@customer_Id", 1 + "#bigint#" + workOrder.Customer_Number);
                input_parameters.Add("@loanType_Id", 1 + "#bigint#" + Convert.ToInt64(workOrder.Loan_Info));
                input_parameters.Add("@state_Id", 1 + "#bigint#" + Convert.ToInt64(workOrder.state));
                input_parameters.Add("@workType_Id", 1 + "#bigint#" + Convert.ToInt64(workOrder.WorkType));
                input_parameters.Add("@workOrder_Id", 1 + "#bigint#" + Convert.ToInt64(workOrder.workOrder_ID));
                input_parameters.Add("@UserID", 1 + "#bigint#" + workOrder.currUserId); //Added By dipali
                input_parameters.Add("@Type", 1 + "#int#" + workOrder.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
                if (ds.Tables.Count > 0)
                {
                    var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                    List<Folder_File_MasterDTO> FileDetails =
                       (from item in myEnumerableFeaprd
                        select new Folder_File_MasterDTO
                        {
                            Fold_File_Pkey_Id = item.Field<Int64>("Fold_File_Pkey_Id"),
                            Fold_File_Name = item.Field<String>("Fold_File_Name"),
                            Fold_File_Local_Path = item.Field<String>("Fold_File_Local_Path"),
                            Fold_File_Object_Name = item.Field<String>("Fold_File_Object_Name"),
                            Fold_File_Folder_Name = item.Field<String>("Fold_File_Folder_Name"),
                        }).ToList();

                    return FileDetails;
                }
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return null;
        }

        public UserDetailListDTO GetMessageUserDetail(UserDetailDTO model)
        {
            DataSet ds = null;
            UserDetailListDTO userDetailListDTO = new UserDetailListDTO();
            try
            {
                string selectProcedure = "[Get_Message_User]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@ContractorID", 1 + "#bigint#" + model.ContractorID);
                input_parameters.Add("@CordinatorID", 1 + "#bigint#" + model.CordinatorID);
                input_parameters.Add("@ProcessorID", 1 + "#bigint#" + model.ProcessorID);
                input_parameters.Add("@WorkOrderID", 1 + "#bigint#" + model.WoID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);

                if (ds.Tables.Count > 0)
                {
                    var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();

                    List<UserDetailDTO> userList =
                       (from item in myEnumerableFeaprd
                        select new UserDetailDTO
                        {
                            User_LoginName = item.Field<String>("User_LoginName"),
                            UserName = item.Field<String>("UserName"),
                            IPL_Company_ID = item.Field<String>("IPL_Company_ID"),
                            GroupRoleId = item.Field<Int64>("GroupRoleId"),
                        }).ToList();

                    userDetailListDTO.LoggedUserDetail = userList;
                }
                if (ds.Tables.Count > 1)
                {
                    var myEnumerableFeaprd = ds.Tables[1].AsEnumerable();

                    List<UserDetailDTO> userList =
                       (from item in myEnumerableFeaprd
                        select new UserDetailDTO
                        {
                            User_LoginName = item.Field<String>("User_LoginName"),
                            UserName = item.Field<String>("UserName"),
                            IPL_Company_ID = item.Field<String>("IPL_Company_ID"),
                            UserToken = item.Field<String>("UserToken"),
                        }).ToList();

                    userDetailListDTO.ContractorDetail = userList;
                }
                if (ds.Tables.Count > 2)
                {
                    var myEnumerableFeaprd = ds.Tables[2].AsEnumerable();

                    List<UserDetailDTO> userList =
                       (from item in myEnumerableFeaprd
                        select new UserDetailDTO
                        {
                            User_LoginName = item.Field<String>("User_LoginName"),
                            UserName = item.Field<String>("UserName"),
                            IPL_Company_ID = item.Field<String>("IPL_Company_ID"),
                        }).ToList();

                    userDetailListDTO.CordinatorDetail = userList;
                }
                if (ds.Tables.Count > 3)
                {
                    var myEnumerableFeaprd = ds.Tables[3].AsEnumerable();

                    List<UserDetailDTO> userList =
                       (from item in myEnumerableFeaprd
                        select new UserDetailDTO
                        {
                            User_LoginName = item.Field<String>("User_LoginName"),
                            UserName = item.Field<String>("UserName"),
                            IPL_Company_ID = item.Field<String>("IPL_Company_ID"),
                        }).ToList();

                    userDetailListDTO.ProcessorDetail = userList;
                }
                if (ds.Tables.Count > 4 && ds.Tables[4].Rows.Count > 0)
                {
                    userDetailListDTO.Old_ContractorID = Convert.ToInt64(ds.Tables[4].Rows[0]["Contractor"]);
                    userDetailListDTO.Old_CordinatorID = Convert.ToInt64(ds.Tables[4].Rows[0]["Cordinator"]);
                    userDetailListDTO.Old_ProcessorID = Convert.ToInt64(ds.Tables[4].Rows[0]["Processor"]);
                    userDetailListDTO.IPLNo = ds.Tables[4].Rows[0]["IPLNO"].ToString();
                    userDetailListDTO.status = Convert.ToInt32(ds.Tables[4].Rows[0]["status"]);
                    userDetailListDTO.address1 = ds.Tables[4].Rows[0]["address1"].ToString();
                    userDetailListDTO.city = ds.Tables[4].Rows[0]["city"].ToString();
                    userDetailListDTO.state = ds.Tables[4].Rows[0]["state"].ToString();
                    userDetailListDTO.zip = Convert.ToInt64(ds.Tables[4].Rows[0]["zip"]);
                }
                if (ds.Tables.Count > 5)
                {
                    var myEnumerableFeaprd = ds.Tables[5].AsEnumerable();

                    List<UserDetailDTO> userList =
                       (from item in myEnumerableFeaprd
                        select new UserDetailDTO
                        {
                            User_LoginName = item.Field<String>("User_LoginName"),
                            UserName = item.Field<String>("UserName"),
                            IPL_Company_ID = item.Field<String>("IPL_Company_ID"),
                        }).ToList();

                    userDetailListDTO.Old_ContractorDetail = userList;
                }
                if (ds.Tables.Count > 6)
                {
                    var myEnumerableFeaprd = ds.Tables[6].AsEnumerable();

                    List<UserDetailDTO> userList =
                       (from item in myEnumerableFeaprd
                        select new UserDetailDTO
                        {
                            User_LoginName = item.Field<String>("User_LoginName"),
                            UserName = item.Field<String>("UserName"),
                            IPL_Company_ID = item.Field<String>("IPL_Company_ID"),
                        }).ToList();

                    userDetailListDTO.Old_CordinatorDetail = userList;
                }
                if (ds.Tables.Count > 7)
                {
                    var myEnumerableFeaprd = ds.Tables[7].AsEnumerable();

                    List<UserDetailDTO> userList =
                       (from item in myEnumerableFeaprd
                        select new UserDetailDTO
                        {
                            User_LoginName = item.Field<String>("User_LoginName"),
                            UserName = item.Field<String>("UserName"),
                            IPL_Company_ID = item.Field<String>("IPL_Company_ID"),
                        }).ToList();

                    userDetailListDTO.Old_ProcessorDetail = userList;
                }
                if (ds.Tables.Count > 8)
                {
                    var myEnumerableFeaprd = ds.Tables[8].AsEnumerable();

                    List<UserDetailDTO> userList =
                       (from item in myEnumerableFeaprd
                        select new UserDetailDTO
                        {
                            User_pkeyID = item.Field<Int64>("User_pkeyID"),
                            User_LoginName = item.Field<String>("User_LoginName"),
                            UserName = item.Field<String>("UserName"),
                            IPL_Company_ID = item.Field<String>("IPL_Company_ID"),
                        }).ToList();

                    userDetailListDTO.AdminUserDetail = userList;
                }

                return userDetailListDTO;
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return null;
        }

        public bool DeleteFirbaseMessage(string IPLNO)
        {
            if (!string.IsNullOrEmpty(IPLNO) && !string.IsNullOrWhiteSpace(IPLNO))
            {
                string firebaseMsgUrl = System.Configuration.ConfigurationManager.AppSettings["FirebaseMsgUrl"];

                string deleteNodeUrl = firebaseMsgUrl + IPLNO + ".json";
                System.Net.Http.HttpClient client = new System.Net.Http.HttpClient();
                HttpResponseMessage messge = client.DeleteAsync(deleteNodeUrl).Result;
                return messge.IsSuccessStatusCode;
            }
            return false;
        }

        public async Task<FirebaseObject<string>> AddFirbaseMessage(FirebaseMessageDTO firebaseMessageDTO, string iplNo, string iplCompany,string Address)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(iplCompany) && !string.IsNullOrWhiteSpace(iplNo) && firebaseMessageDTO != null)
                {
                    string firebaseUrl = System.Configuration.ConfigurationManager.AppSettings["FirebaseUrl"];

                    
                        string msg = JsonConvert.SerializeObject(firebaseMessageDTO);
                        var firebaseClient = new FirebaseClient(firebaseUrl);
                        string Fulldetails = iplNo + " " + Address;
                        var result = await firebaseClient
                            .Child("messages/" + iplCompany + "/" + iplNo)
                            .PostAsync(msg);
                    if (!string.IsNullOrWhiteSpace(Address))
                    {
                        FirebaseFullDetailsDTO firebaseFullDetailsDTO = new FirebaseFullDetailsDTO();
                        firebaseFullDetailsDTO.Details = Fulldetails;

                        var resultval = await firebaseClient
                           .Child("messages/" + iplCompany + "/" + iplNo+ "/" +"Details")
                           .PostAsync(JsonConvert.SerializeObject(firebaseFullDetailsDTO));
                        
                    }
                    return result;


                }
                return null;
            }
            catch (Exception ex)
            {

                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return null;
            }


        }

        public  string RemoveSpecialCharacters(string input)
        {
            // Regular expression to keep only letters, digits, and spaces
            return Regex.Replace(input, @"[^a-zA-Z0-9\s]", "");
        }

        public async Task AddFirebaseUserGroupIPLNO(FirebaseUserGroupDTO firebaseUserGroupDTO, string iplNo, string iplCompany, string loginName, string Address,FirebaseUserIPLDTO firebaseUserIPLDTO = null)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(iplCompany) && !string.IsNullOrWhiteSpace(iplNo) && !string.IsNullOrWhiteSpace(loginName) && firebaseUserGroupDTO != null)
                {
                    string firebaseUrl = System.Configuration.ConfigurationManager.AppSettings["FirebaseUrl"];

                    string jsonStr = JsonConvert.SerializeObject(firebaseUserGroupDTO);
                    var firebaseClient = new FirebaseClient(firebaseUrl);
                    string FullDetails = iplNo + " " + Address;
                    var objData = await firebaseClient
                     .Child("users")
                     .Child(iplCompany)
                     .Child(loginName)
                     .OnceAsync<dynamic>();

                    if (objData.Count == 0)
                    {
                        await firebaseClient
                         .Child("users")
                         .Child(iplCompany)
                         .Child(loginName)
                         .PutAsync(jsonStr);
                    }
                    if (firebaseUserIPLDTO == null)
                    {
                        firebaseUserIPLDTO = new FirebaseUserIPLDTO { IPLNO = iplNo, lastUpdate = "", Details = FullDetails };
                    }

                    string iplJsonStr = JsonConvert.SerializeObject(firebaseUserIPLDTO);

                    await firebaseClient
                    .Child("users")
                    .Child(iplCompany)
                    .Child(loginName)
                    .Child("groups")
                    .Child((iplNo))
                    .PutAsync(iplJsonStr);

                }
            }
            catch (Exception ex)
            {

                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

        }

        public async Task DeleteFirebaseUserGroupIPLNO(string iplNo, string iplCompany, string loginName)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(iplCompany) && !string.IsNullOrWhiteSpace(iplNo) && !string.IsNullOrWhiteSpace(loginName))
                {
                    string firebaseUrl = System.Configuration.ConfigurationManager.AppSettings["FirebaseUrl"];

                    var firebaseClient = new FirebaseClient(firebaseUrl);

                    await firebaseClient
                    .Child("users")
                    .Child(iplCompany)
                    .Child(loginName)
                    .Child("groups")
                    .Child(iplNo)
                    .DeleteAsync();

                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

        }
        public List<dynamic> RecuredWorkOrderData(List<ActionRecurringModel> workRecList)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                if (workRecList != null && workRecList.Count > 0)
                {
                    foreach (var work in workRecList)
                    {
                        workOrderxDTO workOrder = new workOrderxDTO();
                        workOrder.workOrder_ID = work.WorkOrderID;
                        workOrder.Recurring = work.Recurring;
                        workOrder.Recurs_Day = work.Recurs_Day;
                        workOrder.Recurs_Period = work.Recurs_Period;
                        workOrder.Recurs_Limit = work.Recurs_Limit;
                        workOrder.Recurs_CutOffDate = work.Recurs_CutOffDate;
                        workOrder.UserID = work.UserID;
                        workOrder.Type = 8;

                        var objdata = Add_WorkorderData(workOrder);

                        if (work.Recurs_ReceivedDateArray != null && work.Recurs_ReceivedDateArray.Count > 0 && work.Recurs_ReceivedDateArray.Count == work.Recurs_DueDateArray.Count)
                        {
                            for (int i = 0; i < work.Recurs_ReceivedDateArray.Count; i++)
                            {
                                if (work.Recurs_ReceivedDateArray[i] != null)
                                {
                                    WorkOrderRecurringMasterDTO recModel = new WorkOrderRecurringMasterDTO();
                                    recModel.Wo_Rec_WoId = work.WorkOrderID;
                                    recModel.Wo_Rec_ReceiveDate = work.Recurs_ReceivedDateArray[i];
                                    recModel.Wo_Rec_DueDate = work.Recurs_DueDateArray[i];
                                    recModel.Wo_Rec_IsProcess = false;
                                    recModel.UserId = work.UserID;
                                    recModel.Type = 1;
                                    var recObjData = workOrderRecurringMasterData.AddWorkOrderRecurringMaster(recModel);
                                    objDynamic.Add(recObjData);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
        public static double GetTimestamp(DateTime value)
        {
            TimeSpan timeSpan = value - new DateTime(1970, 1, 1, 0, 0, 0);
            return (double)timeSpan.TotalSeconds;
        }

        public string GenerateMD5(string yourString)
        {
            try
            {
                return string.Join("", MD5.Create().ComputeHash(Encoding.ASCII.GetBytes(yourString)).Select(s => s.ToString("x2")));
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return null;
            }

        }

        public List<dynamic> SendCreatedWebNotification(WorkOrderUpdateStatusDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();

            try
            {
                Workorder_Notification_MasterDTO workorder_Notification_MasterDTO = new Workorder_Notification_MasterDTO();

                workorder_Notification_MasterDTO.WN_WoId = model.workOrder_ID;
                workorder_Notification_MasterDTO.WN_UserId = model.WN_UserId;
                workorder_Notification_MasterDTO.WN_Title = "Work Order Created";
                workorder_Notification_MasterDTO.WN_Message = "Work order with IPL number " + model.IPLNO + " has been created.";
                workorder_Notification_MasterDTO.WN_IsActive = true;
                workorder_Notification_MasterDTO.WN_IsRead = false;
                workorder_Notification_MasterDTO.WN_IsDelete = false;
                workorder_Notification_MasterDTO.UserID = model.UserId;
                workorder_Notification_MasterDTO.Type = 1;

                Workorder_Notification_MasterData workorder_Notification_MasterData = new Workorder_Notification_MasterData();
                var NoObj = workorder_Notification_MasterData.AddWorkorderNotificationMaster(workorder_Notification_MasterDTO);
                objDynamic.Add(NoObj);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        public workOrderxDTO GetWorkorderForNotification(Int64 workOrder_ID, Int64 UserID)
        {
            workOrderxDTO workorderDetails = new workOrderxDTO();
            try
            {
                workOrderxDTO workOrderxObj = new workOrderxDTO();
                workOrderxObj.workOrder_ID = workOrder_ID;
                workOrderxObj.UserID = UserID;
                workOrderxObj.Type = 2;
                List<dynamic> old_workorder = GetWorkOrderDataClientResult(workOrderxObj);
                var json = JsonConvert.SerializeObject(old_workorder[0]);
                if (!string.IsNullOrEmpty(json))
                {
                    workorderDetails = JsonConvert.DeserializeObject<List<workOrderxDTO>>(json)[0];
                    workorderDetails.UserID = UserID;
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return workorderDetails;
        }
        public List<dynamic> WorkOrderFirebaseNotification(workOrderxDTO oldWorkOrder)
        {
            List<dynamic> objDynamic = new List<dynamic>();

            try
            {
                var newWorkorder = GetWorkorderForNotification(oldWorkOrder.workOrder_ID, oldWorkOrder.UserID);

                //get user details
                UserDetailDTO userDetailDTO = new UserDetailDTO();
                userDetailDTO.UserID = oldWorkOrder.UserID;
                userDetailDTO.ContractorID = Convert.ToInt64(newWorkorder.Contractor);
                userDetailDTO.CordinatorID = newWorkorder.Cordinator.GetValueOrDefault(0);
                userDetailDTO.ProcessorID = newWorkorder.Processor.GetValueOrDefault(0);
                userDetailDTO.WoID = newWorkorder.workOrder_ID;
                userDetailDTO.Type = 1;
                var userDetail = GetMessageUserDetail(userDetailDTO);
                userDetail.address1 = userDetail.address1 + " " + userDetail.city + " " + userDetail.state + " " + userDetail.zip;


                //Workorder change of Contractor
                if (oldWorkOrder.Contractor  != newWorkorder.Contractor)
                {
                     if (!string.IsNullOrWhiteSpace(newWorkorder.Contractor) && Convert.ToInt64(newWorkorder.Contractor) > 0 && userDetail != null && userDetail.ContractorDetail != null && userDetail.ContractorDetail.Count > 0)
                        {
                            if (!string.IsNullOrWhiteSpace(userDetail.ContractorDetail[0].UserToken))
                            {
                                NotificationGetData notificationGetData = new NotificationGetData();
                                var msgResult = notificationGetData.SendNotification(userDetail.ContractorDetail[0].UserToken, "Work order with IPL number #" + newWorkorder.IPLNO + " " + userDetail.address1 + " has been Assigned.", "IPL WorkOrder ", newWorkorder.workOrder_ID.ToString(), 4, newWorkorder.IPLNO);
                            }
                        }
                  
                }

                //Due Date change Notification
                if (oldWorkOrder.dueDate != null && newWorkorder.dueDate != null)
                {
                    if (oldWorkOrder.dueDate.Value.Date != newWorkorder.dueDate.Value.Date)
                    {
                        if (!string.IsNullOrWhiteSpace(newWorkorder.Contractor) && Convert.ToInt64(newWorkorder.Contractor) > 0 && userDetail != null && userDetail.ContractorDetail != null && userDetail.ContractorDetail.Count > 0)
                        {
                            if (!string.IsNullOrWhiteSpace(userDetail.ContractorDetail[0].UserToken))
                            {
                                NotificationGetData notificationGetData = new NotificationGetData();
                                var msgResult = notificationGetData.SendNotification(userDetail.ContractorDetail[0].UserToken, "Work order with IPL number #" + newWorkorder.IPLNO + " "+ userDetail.address1 + " due date has been changed.", "IPL WorkOrder ", newWorkorder.workOrder_ID.ToString(),4, newWorkorder.IPLNO);
                            }
                        }
                    }
                }


                if (newWorkorder.status == "2" || newWorkorder.status == "3")
                {
                    // Order Reopening notification
                    if (oldWorkOrder.status == "5" || oldWorkOrder.status == "7" || oldWorkOrder.status == "10")
                    {
                        if (!string.IsNullOrWhiteSpace(newWorkorder.Contractor) && Convert.ToInt64(newWorkorder.Contractor) > 0 && userDetail != null && userDetail.ContractorDetail != null && userDetail.ContractorDetail.Count > 0)
                        {
                            if (!string.IsNullOrWhiteSpace(userDetail.ContractorDetail[0].UserToken))
                            {
                                NotificationGetData notificationGetData = new NotificationGetData();
                                var msgResult = notificationGetData.SendNotification(userDetail.ContractorDetail[0].UserToken, "Work order with IPL number #" + newWorkorder.IPLNO + " "+ userDetail.address1 + " has been Reopened.", "IPL WorkOrder ", newWorkorder.workOrder_ID.ToString(),5, newWorkorder.IPLNO);
                            }
                        }
                    }

                    // Task changed Notification
                    if (oldWorkOrder.TaskCount != newWorkorder.TaskCount || oldWorkOrder.TaskCount > newWorkorder.TaskCount)
                    {

                        if (!string.IsNullOrWhiteSpace(newWorkorder.Contractor) && Convert.ToInt64(newWorkorder.Contractor) > 0 && userDetail != null && userDetail.ContractorDetail != null && userDetail.ContractorDetail.Count > 0)
                        {
                            if (!string.IsNullOrWhiteSpace(userDetail.ContractorDetail[0].UserToken))
                            {
                                NotificationGetData notificationGetData = new NotificationGetData();
                                var msgResult = notificationGetData.SendNotification(userDetail.ContractorDetail[0].UserToken, "Work order task with IPL number #" + newWorkorder.IPLNO + " "+ userDetail.address1 + " has been Modified.", "IPL WorkOrder ", newWorkorder.workOrder_ID.ToString(),6, newWorkorder.IPLNO);
                            }
                        }
                    }

                    // Instruction changed Notification
                    if (oldWorkOrder.InstructionCount != newWorkorder.InstructionCount || oldWorkOrder.InstructionCount > newWorkorder.InstructionCount)
                    {
                        if (!string.IsNullOrWhiteSpace(newWorkorder.Contractor) && Convert.ToInt64(newWorkorder.Contractor) > 0 && userDetail != null && userDetail.ContractorDetail != null && userDetail.ContractorDetail.Count > 0)
                        {
                            if (!string.IsNullOrWhiteSpace(userDetail.ContractorDetail[0].UserToken))
                            {
                                NotificationGetData notificationGetData = new NotificationGetData();
                                var msgResult = notificationGetData.SendNotification(userDetail.ContractorDetail[0].UserToken, "Work order instruction with IPL number #" + newWorkorder.IPLNO + " "+ userDetail.address1 + " has been Modified.", "IPL WorkOrder ", newWorkorder.workOrder_ID.ToString(),7, newWorkorder.IPLNO);
                            }
                        }
                    }

                    // Comment changed  Notification
                    if (!string.IsNullOrWhiteSpace(oldWorkOrder.Comments) && !string.IsNullOrWhiteSpace(newWorkorder.Comments) && !oldWorkOrder.Comments.ToLower().Equals(newWorkorder.Comments.ToLower()))
                    {
                        if (Convert.ToInt64(newWorkorder.Contractor) > 0 && userDetail != null && userDetail.ContractorDetail != null && userDetail.ContractorDetail.Count > 0)
                        {
                            if (!string.IsNullOrWhiteSpace(userDetail.ContractorDetail[0].UserToken))
                            {
                                NotificationGetData notificationGetData = new NotificationGetData();
                                var msgResult = notificationGetData.SendNotification(userDetail.ContractorDetail[0].UserToken, "Work order with IPL number #" + newWorkorder.IPLNO +" "+userDetail.address1+ " comment has been changed.", "IPL WorkOrder ", newWorkorder.workOrder_ID.ToString(),8, newWorkorder.IPLNO);
                            }
                        }
                    }

                   
                }

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }



        private DataSet GetWorkorderTracker(workOrderxDTO work)
        {
            DataSet ds = new DataSet();
            try
            {
                string insertProcedure = "[GetWorkoOrderTracker]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@UserID", 1 + "#bigint#" + work.UserID);
                input_parameters.Add("@whereClause", 1 + "#nvarchar#" + work.whereClause);
                input_parameters.Add("@Type", 1 + "#int#" + work.Type);

                ds = obj.SelectSql(insertProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ds;
        }
        public List<dynamic> GetWorkorderTrackerList(workOrderxDTO work)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetWorkorderTracker(work);
                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        public DataSet GetdeleteWorkmasterData(GetdeleteWorkmasterDataDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_deleteWorkmasterData]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ds;
        }

        public List<dynamic> GetdeleteWorkmasterDataDetails(GetdeleteWorkmasterDataDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetdeleteWorkmasterData(model);
                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }


        private DataSet GetWorkorderNotificationDetails(GetWorkorderNotificationDetailsDTO model)
        {
            DataSet ds = new DataSet();
            try
            {
                string insertProcedure = "[GetWorkorderNotificationDetails]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();
                input_parameters.Add("@User_pkeyID", 1 + "#bigint#" + model.User_pkeyID);
                input_parameters.Add("@workOrder_ID", 1 + "#bigint#" + model.workOrder_ID);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                ds = obj.SelectSql(insertProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ds;
        }

    

    }
}