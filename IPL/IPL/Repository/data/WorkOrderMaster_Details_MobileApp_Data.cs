﻿using Avigma.Repository.Lib;
using IPL.Models;
using IPLApp.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class WorkOrderMaster_Details_MobileApp_Data
    {

        Log log = new Log();
        MyDataSourceFactory obj = new MyDataSourceFactory();
        private DataSet GetworkorderMasterDetails(workOrderxDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_WorkOrderMaster_Details_MobileApp]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@workOrder_ID", 1 + "#bigint#" + model.workOrder_ID);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }


        private DataSet GetWorkOrderMasterCountMobileApp(CountWorkOrder model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_WorkOrderMaster_Count_MobileApp]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@workOrder_ID", 1 + "#bigint#" + model.workOrder_ID);
                input_parameters.Add("@User_Token_val", 1 + "#bigint#" + model.User_Token_val);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }
        public List<dynamic> GetWorkOrderCountForMobile(CountWorkOrder model)
        {
            List<dynamic> objdata = new List<dynamic>();
            CountWorkOrder countWorkOrder = new CountWorkOrder();
            try
            {
                DataSet ds = GetWorkOrderMasterCountMobileApp(model);
                
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    if (!string.IsNullOrEmpty(ds.Tables[0].Rows[i]["ViewAll"].ToString()))
                    {
                        countWorkOrder.ViewAll = Convert.ToInt32(ds.Tables[0].Rows[i]["ViewAll"].ToString());
                    }
                    
                }
                for (int j = 0; j < ds.Tables[1].Rows.Count; j++)
                {
                    if (!string.IsNullOrEmpty(ds.Tables[1].Rows[j]["ViewGoBack"].ToString()))
                    {
                        countWorkOrder.ViewGoBack = Convert.ToInt32(ds.Tables[1].Rows[j]["ViewGoBack"].ToString());
                    }
                    
                }
                for (int k = 0; k < ds.Tables[2].Rows.Count; k++)
                {
                    if (!string.IsNullOrEmpty(ds.Tables[2].Rows[k]["ViewInfo"].ToString()))
                    {
                        countWorkOrder.ViewInfo = Convert.ToInt32(ds.Tables[2].Rows[k]["ViewInfo"].ToString());
                    }
                    
                }
                for (int l = 0;l < ds.Tables[3].Rows.Count;l++)
                {
                    if (!string.IsNullOrEmpty(ds.Tables[3].Rows[l]["ViewDueDate"].ToString()))
                    {
                        countWorkOrder.ViewDueDate = Convert.ToInt32(ds.Tables[3].Rows[l]["ViewDueDate"].ToString());
                    }
                }
                for (int m = 0; m < ds.Tables[4].Rows.Count; m++)
                {
                    if (!string.IsNullOrEmpty(ds.Tables[4].Rows[m]["ConScore"].ToString()))
                    {
                        countWorkOrder.ConScore = Convert.ToDecimal(ds.Tables[4].Rows[m]["ConScore"].ToString());
                    }
                }

                for (int p = 0; p < ds.Tables[5].Rows.Count; p++)
                {
                    countWorkOrder.IPL_Company_ID = ds.Tables[5].Rows[p]["IPL_Company_ID"].ToString();
                    if (!string.IsNullOrWhiteSpace(ds.Tables[5].Rows[p]["GroupRoleId"].ToString()))
                    {
                        countWorkOrder.GroupRoleId = Convert.ToInt64(ds.Tables[5].Rows[p]["GroupRoleId"].ToString());
                       
                    }
                    countWorkOrder.Score_Details = ds.Tables[5].Rows[p]["Score_Details"].ToString();
                }
                
                objdata.Add(countWorkOrder);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                
            }
            return objdata;
        }


            public List<dynamic> GetWorkOrderDataDetailsForMobile(workOrderxDTO model)
        {
            List<dynamic> objdata = new List<dynamic>();
            try
            {

                HtmlToText htmlToText = new HtmlToText();
              


                switch (model.Type)
                {
                    case 1:
                        {
                            DataSet ds = GetworkorderMasterDetails(model);
                            //log.logDebugMessage("Mobile Table Count" + ds.Tables.Count.ToString());
                            var myEnumerable = ds.Tables[0].AsEnumerable();
                            if (myEnumerable == null)
                            {
                                log.logDebugMessage("No Records founds case 1");
                            }
                            List<workOrderAppDTO> WorkOrderDataMobile =
                                (from item in myEnumerable
                                 select new workOrderAppDTO
                                 {
                                     workOrder_ID = item.Field<Int64>("workOrder_ID"),
                                     workOrderNumber = item.Field<String>("workOrderNumber"),
                                     workOrderInfo = item.Field<String>("workOrderInfo"),
                                     address1 = item.Field<String>("address1"),
                                    
                                     city = item.Field<String>("city"),
                                     state = item.Field<String>("state"),
                                     zip = item.Field<Int64?>("zip"),
                                     country = item.Field<String>("country"),
                                     
                                     status = item.Field<String>("status"),
                                  
                                   //  clientInstructions = item.Field<String>("clientInstructions"),
                                     clientStatus = item.Field<String>("clientStatus"),
                                   
                                     gpsLatitude = item.Field<String>("gpsLatitude"),
                                     gpsLongitude = item.Field<String>("gpsLongitude"),
                             


                                     WorkType = item.Field<Int64?>("WorkType"),
                                     Company = item.Field<String>("Company"),
                                     Com_Name = item.Field<String>("Com_Name"),
                                     Com_Phone = item.Field<String>("Com_Phone"),
                                     Com_Email = item.Field<String>("Com_Email"),
                                    // Contractor = item.Field<String>("Contractor"),
                                  
                                     IPLNO = item.Field<String>("IPLNO"),
                                     Category = item.Field<Int64?>("Category"),
                                     Loan_Info = item.Field<String>("Loan_Info"),
                                     Customer_Number = item.Field<Int64?>("Customer_Number"),
                                     //Cordinator = item.Field<Int64?>("Cordinator"),
                                     BATF = item.Field<bool?>("BATF"),
                                     ISInspection = item.Field<bool?>("ISInspection"),
                                     Lotsize = item.Field<String>("Lotsize"),
                                     Rush = item.Field<Int64?>("Rush"),
                                     Lock_Code = item.Field<String>("Lock_Code"),
                                     Broker_Info = item.Field<String>("Broker_Info"),
                                     Comments = item.Field<String>("Comments"),

                                     SM_Name = item.Field<String>("SM_Name"),
                                     WT_WorkType = item.Field<String>("WT_WorkType"),

                                     Loan_Number = item.Field<String>("Loan_Number"),
                                     Client_Company_Name = item.Field<String>("Client_Company_Name"),
                                     Loan_Type = item.Field<String>("Loan_Type"),

                                     //Cont_Name = item.Field<String>("Cont_Name"),
                                     CORNT_User_Email = item.Field<String>("CORNT_User_Email"),
                                     Work_Type_Name = item.Field<String>("Work_Type_Name"),
                                     Cust_Num_Number = item.Field<String>("Cust_Num_Number"),
                                     rus_Name = item.Field<String>("rus_Name"),

                                     Cordinator_Name = item.Field<String>("Cordinator_Name"),
                                     CORNT_CellNumber = item.Field<String>("CORNT_CellNumber"),

                                     Key_Code = item.Field<String>("Key_Code"),
                                   
                                     Lock_Location = item.Field<String>("Lock_Location"),
                                     Gate_Code = item.Field<String>("Gate_Code"),
                                     ClientMetaData = item.Field<String>("ClientMetaData"),
                                     Client_Result_Photo_FilePath = item.Field<String>("Client_Result_Photo_FilePath"),
                                     Client_Result_Photo_FileName = item.Field<String>("Client_Result_Photo_FileName"),

                                     CORNT_User_LoginName =  item.Field<String>("CORNT_User_LoginName"),
                                     CORNT_User_FirstName = item.Field<String>("CORNT_User_FirstName"),
                                     Workstatustype = 1,

                                     assigned_date = item.Field<String>("assigned_date"),
                                     Received_Date = item.Field<String>("Received_Date"),
                                     Complete_Date = item.Field<String>("Complete_Date"),
                                     Cancel_Date = item.Field<String>("Cancel_Date"),
                                     EstimatedDate = item.Field<String>("EstimatedDate"),
                                     clientDueDate = item.Field<String>("clientDueDate"),
                                     dueDate = item.Field<String>("dueDate"),
                                     startDate = item.Field<String>("startDate"),
                                     photostatus = item.Field<Boolean?>("photostatus"),
                                     Wo_Start = item.Field<int?>("Wo_Start"),

                                 }).ToList();
                            objdata.Add(WorkOrderDataMobile);

                            var myEnumerableFeaprdx = ds.Tables[1].AsEnumerable();
                            List<Instruction_Master_ChildDTO> InstructionChildDetails =
                               (from item in myEnumerableFeaprdx
                                select new Instruction_Master_ChildDTO
                                {
                                    Inst_Ch_pkeyId = item.Field<Int64>("Inst_Ch_pkeyId"),
                                    Inst_Ch_Wo_Id = item.Field<Int64?>("Inst_Ch_Wo_Id"),
                                    Inst_Ch_Text = item.Field<String>("Inst_Ch_Text"),
                                    //Inst_Ch_Comand_Mobile = item.Field<String>("Inst_Ch_Comand_Mobile"),
                                    Inst_Ch_Comand_Mobile = htmlToText.ConvertHtml(item.Field<String>("Inst_Ch_Text")),
                                    Inst_Ch_IsActive = item.Field<Boolean?>("Inst_Ch_IsActive"),

                                }).ToList();

                            objdata.Add(InstructionChildDetails);
                            var myEnumerableFeaprd = ds.Tables[2].AsEnumerable();
                            List<InstctionMasterDTO> InstructionDetails =
                               (from item in myEnumerableFeaprd
                                select new InstctionMasterDTO
                                {
                                    Instr_pkeyId = item.Field<Int64>("Instr_pkeyId"),
                                    Instr_Task_Id = item.Field<Int64?>("Instr_Task_Id"),
                                    Inst_Task_Name = item.Field<String>("Inst_Task_Name"),
                                    Instr_WO_Id = item.Field<Int64?>("Instr_WO_Id"),
                                    Instr_Task_Name = item.Field<String>("Instr_Task_Name"),
                                    Task_Name = item.Field<String>("Task_Name"),
                                    Instr_Qty = item.Field<int?>("Instr_Qty"),
                                    Instr_Contractor_Price = item.Field<Decimal?>("Instr_Contractor_Price"),
                                    Instr_Client_Price = item.Field<Decimal?>("Instr_Client_Price"),
                                    Instr_Contractor_Total = item.Field<Decimal?>("Instr_Contractor_Total"),
                                    Instr_Client_Total = item.Field<Decimal?>("Instr_Client_Total"),
                                    Instr_Action = item.Field<int?>("Instr_Action"),
                                    Instr_IsActive = item.Field<bool?>("Instr_IsActive"),
                                    Instr_IsDelete = item.Field<bool?>("Instr_IsDelete"),
                                    Instr_Details_Data = item.Field<string>("Instr_Details_Data"),
                                    Inst_Comand_Mobile_details = item.Field<string>("Inst_Comand_Mobile_details"),
                                    Instr_ValType = item.Field<int?>("Instr_ValType"),

                                    Instr_Qty_Text = item.Field<int?>("Instr_Qty_Text"),
                                    Instr_Price_Text = item.Field<decimal?>("Instr_Price_Text"),
                                    Instr_Total_Text = item.Field<decimal?>("Instr_Total_Text"),
                                    Instr_Ch_pkeyId = item.Field<Int64?>("Instr_Ch_pkeyId"),


                                    TMF_Task_localPath = item.Field<string>("TMF_Task_localPath"),
                                    TMF_Task_FileName = item.Field<string>("TMF_Task_FileName"),
                                    Instr_Task_Comment = item.Field<string>("Instr_Task_Comment"),
                                    Inst_Task_Type_pkeyId = 1   // bid Completion

                        }).ToList();

                            objdata.Add(InstructionDetails);

                            var myEnumerableFeaprd1 = ds.Tables[3].AsEnumerable();
                            List<InstctionMasterDTO> InstructionDetailsmobile =
                               (from item in myEnumerableFeaprd1
                                select new InstctionMasterDTO
                                {
                                    Instr_pkeyId = item.Field<Int64>("Instr_pkeyId"),
                                    Instr_Task_Id = item.Field<Int64?>("Instr_Task_Id"),
                                    Inst_Task_Name = item.Field<String>("Inst_Task_Name"),
                                    Instr_WO_Id = item.Field<Int64?>("Instr_WO_Id"),
                                    Instr_Task_Name = item.Field<String>("Instr_Task_Name"),
                                    Task_Name = item.Field<String>("Task_Name"),
                                    Instr_Qty = item.Field<int?>("Instr_Qty"),
                                    Instr_Contractor_Price = item.Field<Decimal?>("Instr_Contractor_Price"),
                                    Instr_Client_Price = 0,
                                    Instr_Contractor_Total = item.Field<Decimal?>("Instr_Contractor_Total"),
                                    Instr_Client_Total = 0,
                                    Instr_Action = item.Field<int?>("Instr_Action"),
                                    Instr_IsActive = item.Field<bool?>("Instr_IsActive"),
                                    Instr_IsDelete = item.Field<bool?>("Instr_IsDelete"),
                                    Instr_Details_Data = item.Field<string>("Instr_Details_Data"),
                                    Inst_Comand_Mobile_details = item.Field<string>("Inst_Comand_Mobile_details"),
                                    Instr_ValType = item.Field<int?>("Instr_ValType"),

                                    Instr_Qty_Text = item.Field<int?>("Instr_Qty_Text"),
                                    Instr_Price_Text = item.Field<decimal?>("Instr_Price_Text"),
                                    Instr_Total_Text = item.Field<decimal?>("Instr_Total_Text"),
                                    Instr_Ch_pkeyId = item.Field<Int64?>("Instr_Ch_pkeyId"),

                                    TMF_Task_localPath = item.Field<string>("TMF_Task_localPath"),
                                    TMF_Task_FileName = item.Field<string>("TMF_Task_FileName"),
                                    Instr_Task_Comment = item.Field<string>("Instr_Task_Comment"),
                                    Inst_Task_Type_pkeyId = 3  // Inspection

                                }).ToList();

                            objdata.Add(InstructionDetailsmobile);

                            if (ds.Tables.Count > 3)
                            {
                                objdata.Add(obj.AsDynamicEnumerable(ds.Tables[4]));
                            }
                           
                            break;
                        }

                    case 2:
                    case 3:
                    case 4:
                    case 5:
                        {
                            DataSet ds = GetworkorderMasterDetails(model);
                            //log.logDebugMessage("Mobile Table Count" + ds.Tables.Count.ToString());
                            var myEnumerable = ds.Tables[0].AsEnumerable();
                            if (myEnumerable == null)
                            {
                                log.logDebugMessage("No Records founds 2");
                            }
                            List<workOrderListAppDTO> WorkOrderDataMobile =
                                (from item in myEnumerable
                                 select new workOrderListAppDTO
                                 {
                                     workOrder_ID = item.Field<Int64>("workOrder_ID"),
                                     workOrderNumber = item.Field<String>("workOrderNumber"),
                                   
                                     address1 = item.Field<String>("address1"),
                              
                                     city = item.Field<String>("city"),
                                     state = item.Field<String>("state"),
                                     zip = item.Field<Int64?>("zip"),
                                    
                                    
                                     status = item.Field<String>("status"),
                                 
                                    
                                     gpsLatitude = item.Field<String>("gpsLatitude"),
                                     gpsLongitude = item.Field<String>("gpsLongitude"),
                                   


                                     WorkType = item.Field<Int64?>("WorkType"),
                                    
                                    
                                   
                                     IPLNO = item.Field<String>("IPLNO"),
                                     Category = item.Field<Int64?>("Category"),
                                     Loan_Info = item.Field<String>("Loan_Info"),
                                   
                                     BATF = item.Field<bool?>("BATF"),
                                      

                                     SM_Name = item.Field<String>("SM_Name"),
                                     WT_WorkType = item.Field<String>("WT_WorkType"),
                                     

                                     Key_Code = item.Field<String>("Key_Code"),
                                 
                                     Lock_Location = item.Field<String>("Lock_Location"),
                                     
                                     ////ClientMetaData = item.Field<String>("ClientMetaData"),
                                     Client_Result_Photo_FilePath = item.Field<String>("Client_Result_Photo_FilePath"),
                                     Client_Result_Photo_FileName = item.Field<String>("Client_Result_Photo_FileName"),
                                     Cordinator_Name = item.Field<String>("Cordinator_Name"),
                                     CORNT_CellNumber = item.Field<String>("CORNT_CellNumber"),
                                     CORNT_User_LoginName = item.Field<String>("CORNT_User_LoginName"),
                                     CORNT_User_FirstName = item.Field<String>("CORNT_User_FirstName"),
                                     Workstatustype = 1,
                                  
                                     Lock_Code = item.Field<String>("Lock_Code"),

                                     startDate = item.Field<String>("startDate"),
                                     dueDate = item.Field<String>("dueDate"),
                                     assigned_date = item.Field<String>("assigned_date"),
                                     Received_Date = item.Field<String>("Received_Date"),
                                     EstimatedDate = item.Field<String>("EstimatedDate"),
                                     photostatus = item.Field<Boolean?>("photostatus"),
                                     rus_Name =   item.Field<String>("rus_Name"),
                                     Wo_Start = item.Field<int?>("Wo_Start"),
                                     CORNT_User_Email = item.Field<String>("CORNT_User_Email"),


                                 }).ToList();
                            objdata.Add(WorkOrderDataMobile);
                            break;
                        }

                    case 6:
                        {
                            model.Type = 2;
                               DataSet ds = GetworkorderMasterDetails(model);
                              // log.logDebugMessage("Mobile Table Count" + ds.Tables.Count.ToString());
                            var myEnumerable = ds.Tables[0].AsEnumerable();
                            if (myEnumerable == null)
                            {
                                log.logDebugMessage("No Records founds 6");
                            }
                            List<workOrderListMapDTO> WorkOrderDataMobile =
                                (from item in myEnumerable
                                 select new workOrderListMapDTO
                                 {
                                     workOrder_ID = item.Field<Int64>("workOrder_ID"),
                               

                                     address1 = item.Field<String>("address1"),

                                     city = item.Field<String>("city"),
                                     state = item.Field<String>("state"),
                                     zip = item.Field<Int64?>("zip"),

                                     photostatus = item.Field<Boolean?>("photostatus"),
                                     status = item.Field<String>("status"),
                                     dueDate = item.Field<String>("dueDate"),

                                     gpsLatitude = item.Field<String>("gpsLatitude"),
                                     gpsLongitude = item.Field<String>("gpsLongitude"),

                                     IPLNO = item.Field<String>("IPLNO"),
                                     CORNT_User_Email = item.Field<String>("CORNT_User_Email"),


                                 }).ToList();
                            objdata.Add(WorkOrderDataMobile);
                            break;
                        }

                    case 7:
                        {
                            TaskBidForPhotoData taskBidForPhotoData = new TaskBidForPhotoData();
                            DataSet ds = GetworkorderMasterDetails(model);
                           // log.logDebugMessage("Mobile Table Count" + ds.Tables.Count.ToString());
                            var myEnumerable = ds.Tables[0].AsEnumerable();
                            if (myEnumerable == null)
                            {
                                log.logDebugMessage("No Records founds 7");
                            }
                            List<workOrderListAppOffilneDTO> WorkOrderDataMobile =
                                (from item in myEnumerable
                                 select new workOrderListAppOffilneDTO
                                 {
                                     workOrder_ID = item.Field<Int64>("workOrder_ID"),
                                     workOrderNumber = item.Field<String>("workOrderNumber"),

                                     address1 = item.Field<String>("address1"),

                                     city = item.Field<String>("city"),
                                     state = item.Field<String>("state"),
                                     zip = item.Field<Int64?>("zip"),
                                     status = item.Field<String>("status"),
                                   

                                     gpsLatitude = item.Field<String>("gpsLatitude"),
                                     gpsLongitude = item.Field<String>("gpsLongitude"),
                                 

                                     WorkType = item.Field<Int64?>("WorkType"),

                                   

                                     IPLNO = item.Field<String>("IPLNO"),
                                     Category = item.Field<Int64?>("Category"),
                                     Loan_Info = item.Field<String>("Loan_Info"),

                                     BATF = item.Field<bool?>("BATF"),


                                     SM_Name = item.Field<String>("SM_Name"),
                                     WT_WorkType = item.Field<String>("WT_WorkType"),
                                     Key_Code = item.Field<String>("Key_Code"),
                                     Lock_Location = item.Field<String>("Lock_Location"),

                                     ////ClientMetaData = item.Field<String>("ClientMetaData"),
                                     Client_Result_Photo_FilePath = item.Field<String>("Client_Result_Photo_FilePath"),
                                     Client_Result_Photo_FileName = item.Field<String>("Client_Result_Photo_FileName"),
                                     Cordinator_Name = item.Field<String>("Cordinator_Name"),
                                     CORNT_CellNumber = item.Field<String>("CORNT_CellNumber"),
                                     GB_work  = item.Field<Boolean?>("GB_work"),
                                     Info_work = item.Field<Boolean?>("Info_work"),

                                     CORNT_User_LoginName = item.Field<String>("CORNT_User_LoginName"),
                                     CORNT_User_FirstName = item.Field<String>("CORNT_User_FirstName"),
                                     Workstatustype = 1,
                                     Lock_Code = item.Field<String>("Lock_Code"),

                                     assigned_date = item.Field<String>("assigned_date"),
                                     startDate = item.Field<String>("startDate"),
                                     EstimatedDate = item.Field<String>("EstimatedDate"),
                                     dueDate = item.Field<String>("dueDate"),
                                     Received_Date = item.Field<String>("Received_Date"),
                                     photostatus = item.Field<Boolean?>("photostatus"),
                                     Additional_Details = GetAdditionalDetailsData(item.Field<Int64>("workOrder_ID"),model.UserID),
                                     rus_Name = item.Field<String>("rus_Name"),
                                     Wo_Start = item.Field<int?>("Wo_Start"),
                                     CORNT_User_Email = item.Field<String>("CORNT_User_Email"),
                                     TaskInvoiceBid_MobApps = taskBidForPhotoData.GetInvoiceBidfortaskMobAppDetailsForOffline(item.Field<Int64>("workOrder_ID"), model.UserID,2,0)
                                    

                                 }).ToList();
                            objdata.Add(WorkOrderDataMobile);
                            break;
                        }
                    case 8:
                        {
                            model.Type = 1;
                            DataSet ds = GetworkorderMasterDetails(model);
                            //log.logDebugMessage("Mobile Table Count" + ds.Tables.Count.ToString());
                            var myEnumerable = ds.Tables[0].AsEnumerable();
                            if (myEnumerable == null)
                            {
                                log.logDebugMessage("No Records founds case 1");
                            }
                            List<workOrderAppDTO> WorkOrderDataMobile =
                                (from item in myEnumerable
                                 select new workOrderAppDTO
                                 {
                                     workOrder_ID = item.Field<Int64>("workOrder_ID"),
                                     workOrderNumber = item.Field<String>("workOrderNumber"),
                                     workOrderInfo = item.Field<String>("workOrderInfo"),
                                     address1 = item.Field<String>("address1"),

                                     city = item.Field<String>("city"),
                                     state = item.Field<String>("state"),
                                     zip = item.Field<Int64?>("zip"),
                                     country = item.Field<String>("country"),

                                     status = item.Field<String>("status"),
                                  
                                     //  clientInstructions = item.Field<String>("clientInstructions"),
                                     clientStatus = item.Field<String>("clientStatus"),
                                    
                                     gpsLatitude = item.Field<String>("gpsLatitude"),
                                     gpsLongitude = item.Field<String>("gpsLongitude"),
                              
                                     WorkType = item.Field<Int64?>("WorkType"),
                                     Company = item.Field<String>("Company"),
                                     Com_Name = item.Field<String>("Com_Name"),
                                     Com_Phone = item.Field<String>("Com_Phone"),
                                     Com_Email = item.Field<String>("Com_Email"),
                                     // Contractor = item.Field<String>("Contractor"),
                                   
                                     IPLNO = item.Field<String>("IPLNO"),
                                     Category = item.Field<Int64?>("Category"),
                                     Loan_Info = item.Field<String>("Loan_Info"),
                                     Customer_Number = item.Field<Int64?>("Customer_Number"),
                                     //Cordinator = item.Field<Int64?>("Cordinator"),
                                     BATF = item.Field<bool?>("BATF"),
                                     ISInspection = item.Field<bool?>("ISInspection"),
                                     Lotsize = item.Field<String>("Lotsize"),
                                     Rush = item.Field<Int64?>("Rush"),
                                     Lock_Code = item.Field<String>("Lock_Code"),
                                     Broker_Info = item.Field<String>("Broker_Info"),
                                     Comments = item.Field<String>("Comments"),

                                     SM_Name = item.Field<String>("SM_Name"),
                                     WT_WorkType = item.Field<String>("WT_WorkType"),
                                     Client_Company_Name = item.Field<String>("Client_Company_Name"),

                                     //Cont_Name = item.Field<String>("Cont_Name"),
                                     CORNT_User_Email = item.Field<String>("CORNT_User_Email"),
                                     Work_Type_Name = item.Field<String>("Work_Type_Name"),
                                     Cust_Num_Number = item.Field<String>("Cust_Num_Number"),
                                     rus_Name = item.Field<String>("rus_Name"),

                                     Cordinator_Name = item.Field<String>("Cordinator_Name"),
                                     CORNT_CellNumber = item.Field<String>("CORNT_CellNumber"),

                                     Key_Code = item.Field<String>("Key_Code"),
                                     Loan_Number = item.Field<String>("Loan_Number"),
                                     Lock_Location = item.Field<String>("Lock_Location"),
                                     Gate_Code = item.Field<String>("Gate_Code"),
                                     ClientMetaData = item.Field<String>("ClientMetaData"),
                                     Client_Result_Photo_FilePath = item.Field<String>("Client_Result_Photo_FilePath"),
                                     Client_Result_Photo_FileName = item.Field<String>("Client_Result_Photo_FileName"),

                                     CORNT_User_LoginName = item.Field<String>("CORNT_User_LoginName"),
                                     CORNT_User_FirstName = item.Field<String>("CORNT_User_FirstName"),
                                     Workstatustype = 1,

                                      assigned_date = item.Field<String>("assigned_date"),
                                     dueDate = item.Field<String>("dueDate"),
                                     startDate = item.Field<String>("startDate"),
                                     EstimatedDate = item.Field<String>("EstimatedDate"),
                                     clientDueDate = item.Field<String>("clientDueDate"),
                                     Received_Date = item.Field<String>("Received_Date"),
                                     Complete_Date = item.Field<String>("Complete_Date"),
                                     Cancel_Date = item.Field<String>("Cancel_Date"),
                                     photostatus = item.Field<Boolean?>("photostatus"),
                                     Wo_Start = item.Field<int?>("Wo_Start"),

                                 }).ToList();
                            objdata.Add(WorkOrderDataMobile);

                            break;
                        }
                }

              

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                log.logErrorMessage("WorkOrderID------------->" + model.workOrder_ID);
                log.logErrorMessage("UserID------------->" + model.UserID);
                log.logErrorMessage("Type------------->" + model.Type);
                return objdata;
            }
            return objdata;
        }


        public Additional_Details GetAdditionalDetailsData(Int64 WorkOrderID,Int64 UserID)
        {
            Additional_Details additional_Details = new Additional_Details();
            HtmlToText htmlToText = new HtmlToText();

            try
            {
                workOrderxDTO model = new workOrderxDTO();
                model.Type = 1;
                model.workOrder_ID = WorkOrderID;
                model.UserID = UserID;
                DataSet ds = GetworkorderMasterDetails(model);
               // log.logDebugMessage("Mobile Table Count" + ds.Tables.Count.ToString());
                var myEnumerable = ds.Tables[0].AsEnumerable();
                if (myEnumerable == null)
                {
                    log.logDebugMessage("No Records founds case 1");
                }

                var myEnumerableFeaprdx = ds.Tables[1].AsEnumerable();
                List<Instruction_Master_ChildDTO> InstructionChildDetails =
                   (from item in myEnumerableFeaprdx
                    select new Instruction_Master_ChildDTO
                    {
                        Inst_Ch_pkeyId = item.Field<Int64>("Inst_Ch_pkeyId"),
                        Inst_Ch_Wo_Id = item.Field<Int64?>("Inst_Ch_Wo_Id"),
                        Inst_Ch_Text = item.Field<String>("Inst_Ch_Text"),
                                    //Inst_Ch_Comand_Mobile = item.Field<String>("Inst_Ch_Comand_Mobile"),
                                    Inst_Ch_Comand_Mobile = htmlToText.ConvertHtml(item.Field<String>("Inst_Ch_Text")),
                        Inst_Ch_IsActive = item.Field<Boolean?>("Inst_Ch_IsActive"),

                    }).ToList();

                additional_Details.Instruction_Master_ChildDTOs = InstructionChildDetails;

                var myEnumerableFeaprd = ds.Tables[2].AsEnumerable();
                List<InstctionMasterDTO> InstructionDetails =
                   (from item in myEnumerableFeaprd
                    select new InstctionMasterDTO
                    {
                        Instr_pkeyId = item.Field<Int64>("Instr_pkeyId"),
                        Instr_Task_Id = item.Field<Int64?>("Instr_Task_Id"),
                        Inst_Task_Name = item.Field<String>("Inst_Task_Name"),
                        Instr_WO_Id = item.Field<Int64?>("Instr_WO_Id"),
                        Instr_Task_Name = item.Field<String>("Instr_Task_Name"),
                        Task_Name = item.Field<String>("Task_Name"),
                        Instr_Qty = item.Field<int?>("Instr_Qty"),
                        Instr_Contractor_Price = item.Field<Decimal?>("Instr_Contractor_Price"),
                        Instr_Client_Price = item.Field<Decimal?>("Instr_Client_Price"),
                        Instr_Contractor_Total = item.Field<Decimal?>("Instr_Contractor_Total"),
                        Instr_Client_Total = item.Field<Decimal?>("Instr_Client_Total"),
                        Instr_Action = item.Field<int?>("Instr_Action"),
                        Instr_IsActive = item.Field<bool?>("Instr_IsActive"),
                        Instr_IsDelete = item.Field<bool?>("Instr_IsDelete"),
                        Instr_Details_Data = item.Field<string>("Instr_Details_Data"),
                        Inst_Comand_Mobile_details = item.Field<string>("Inst_Comand_Mobile_details"),
                        Instr_ValType = item.Field<int?>("Instr_ValType"),

                        Instr_Qty_Text = item.Field<int?>("Instr_Qty_Text"),
                        Instr_Price_Text = item.Field<decimal?>("Instr_Price_Text"),
                        Instr_Total_Text = item.Field<decimal?>("Instr_Total_Text"),
                        Instr_Ch_pkeyId = item.Field<Int64?>("Instr_Ch_pkeyId"),


                        TMF_Task_localPath = item.Field<string>("TMF_Task_localPath"),
                        TMF_Task_FileName = item.Field<string>("TMF_Task_FileName"),
                        Inst_Task_Type_pkeyId = 1   // bid Completion

                                }).ToList();


                additional_Details.InstructionMasterDTOs_Bid = InstructionDetails;
               var myEnumerableFeaprd1 = ds.Tables[3].AsEnumerable();
                List<InstctionMasterDTO> InstructionDetailsmobile =
                   (from item in myEnumerableFeaprd1
                    select new InstctionMasterDTO
                    {
                        Instr_pkeyId = item.Field<Int64>("Instr_pkeyId"),
                        Instr_Task_Id = item.Field<Int64?>("Instr_Task_Id"),
                        Inst_Task_Name = item.Field<String>("Inst_Task_Name"),
                        Instr_WO_Id = item.Field<Int64?>("Instr_WO_Id"),
                        Instr_Task_Name = item.Field<String>("Instr_Task_Name"),
                        Task_Name = item.Field<String>("Task_Name"),
                        Instr_Qty = item.Field<int?>("Instr_Qty"),
                        Instr_Contractor_Price = item.Field<Decimal?>("Instr_Contractor_Price"),
                        Instr_Client_Price = 0,
                        Instr_Contractor_Total = item.Field<Decimal?>("Instr_Contractor_Total"),
                        Instr_Client_Total = 0,
                        Instr_Action = item.Field<int?>("Instr_Action"),
                        Instr_IsActive = item.Field<bool?>("Instr_IsActive"),
                        Instr_IsDelete = item.Field<bool?>("Instr_IsDelete"),
                        Instr_Details_Data = item.Field<string>("Instr_Details_Data"),
                        Inst_Comand_Mobile_details = item.Field<string>("Inst_Comand_Mobile_details"),
                        Instr_ValType = item.Field<int?>("Instr_ValType"),

                        Instr_Qty_Text = item.Field<int?>("Instr_Qty_Text"),
                        Instr_Price_Text = item.Field<decimal?>("Instr_Price_Text"),
                        Instr_Total_Text = item.Field<decimal?>("Instr_Total_Text"),
                        Instr_Ch_pkeyId = item.Field<Int64?>("Instr_Ch_pkeyId"),

                        TMF_Task_localPath = item.Field<string>("TMF_Task_localPath"),
                        TMF_Task_FileName = item.Field<string>("TMF_Task_FileName"),
                        Inst_Task_Type_pkeyId = 3  // Inspection

                                }).ToList();

                additional_Details.InstructionMasterDTOs_Inspection = InstructionDetailsmobile;


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return additional_Details;
        }
    }
}