﻿using Avigma.Repository.Lib;
using IPLApp.Models.data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPLApp.Repository.data
{
    public class IPLCityData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public List<dynamic> AddCityData(IPLCityDTO model)
        {

            string insertProcedure = "[CreateUpdateCity]";
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@IPL_CityID", 1 + "#bigint#" + model.IPL_CityID);
                input_parameters.Add("@IPL_CityName", 1 + "#varchar#" + model.IPL_CityName);
                input_parameters.Add("@IPL_StateID", 1 + "#bigint#" + model.IPL_StateID);
                input_parameters.Add("@IPL_IsActive", 1 + "#bit#" + model.IPL_IsActive);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@IPL_CityID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return obj.SqlCRUD(insertProcedure, input_parameters); ;



        }

        private DataSet GetCityMaster(IPLCityDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_City_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@IPL_CityID", 1 + "#bigint#" + model.IPL_CityID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetCityDetails(IPLCityDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetCityMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<IPLCityDTO>CityDetails =
                   (from item in myEnumerableFeaprd
                    select new IPLCityDTO
                    {
                        IPL_CityID = item.Field<Int64>("IPL_CityID"),
                        IPL_CityName = item.Field<String>("IPL_CityName"),
                        IPL_StateID = item.Field<Int64?>("IPL_StateID"),
                        IPL_IsActive = item.Field<Boolean?>("IPL_IsActive"),

                    }).ToList();

                objDynamic.Add(CityDetails);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}