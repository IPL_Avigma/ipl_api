﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class Filter_Admin_ImportData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        public List<dynamic> AddUpdate_Filter_Admin_Import(Filter_Admin_Import_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_Filter_Admin_Import_Master]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Import_Filter_PkeyID", 1 + "#bigint#" + model.Import_Filter_PkeyID);
                input_parameters.Add("@Import_Filter_ImpFromID", 1 + "#nvarchar#" + model.Import_Filter_ImpFromID);
                input_parameters.Add("@Import_Filter_ImpName", 1 + "#nvarchar#" + model.Import_Filter_ImpName);
                input_parameters.Add("@Import_Filter_ClientName", 1 + "#nvarchar#" + model.Import_Filter_ClientName);
                input_parameters.Add("@Import_Filter_LoginName", 1 + "#nvarchar#" + model.Import_Filter_LoginName);
                input_parameters.Add("@Import_Filter_ImpIsActive", 1 + "#bit#" + model.Import_Filter_ImpIsActive);
                input_parameters.Add("@Ins_Filter_IsActive", 1 + "#bit#" + model.Import_Filter_IsActive);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserId);
                input_parameters.Add("@Import_Filter_PkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        private DataSet Get_Filter_Admin_Import(Filter_Admin_Import_MasterDTO model)
        {
            DataSet ds = null;
            try
            {

                string selectProcedure = "[Get_Filter_Admin_Import_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Import_Filter_PkeyID", 1 + "#bigint#" + model.Import_Filter_PkeyID);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> Get_Filter_Admin_ImportDetails(Filter_Admin_Import_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = Get_Filter_Admin_Import(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Filter_Admin_Import_MasterDTO> Forms_Master_Files =
                   (from item in myEnumerableFeaprd
                    select new Filter_Admin_Import_MasterDTO
                    {
                        Import_Filter_PkeyID = item.Field<Int64>("Import_Filter_PkeyID"),
                        Import_Filter_ImpFromID = item.Field<String>("Import_Filter_ImpFromID"),
                        Import_Filter_ImpName = item.Field<String>("Import_Filter_ImpName"),
                        Import_Filter_ClientName = item.Field<String>("Import_Filter_ClientName"),
                        Import_Filter_LoginName = item.Field<String>("Import_Filter_LoginName"),
                        Import_Filter_ImpIsActive = item.Field<Boolean?>("Import_Filter_ImpIsActive")

                    }).ToList();

                objDynamic.Add(Forms_Master_Files);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}