﻿using Avigma.Repository.Lib;
using IPL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class Client_Invoice_PatymentData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public List<dynamic> AddClientPaymentData(Client_Invoice_PatymentDTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_Client_Invoice_Patyment]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Client_Pay_PkeyId", 1 + "#bigint#" + model.Client_Pay_PkeyId);
                input_parameters.Add("@Client_Pay_Invoice_Id", 1 + "#bigint#" + model.Client_Pay_Invoice_Id);
                input_parameters.Add("@Client_Pay_Wo_Id", 1 + "#bigint#" + model.Client_Pay_Wo_Id);
                input_parameters.Add("@Client_Pay_Payment_Date", 1 + "#datetime#" + model.Client_Pay_Payment_Date);
                input_parameters.Add("@Client_Pay_Amount", 1 + "#decimal#" + model.Client_Pay_Amount);
                input_parameters.Add("@Client_Pay_CheckNumber", 1 + "#varchar#" + model.Client_Pay_CheckNumber);
                input_parameters.Add("@Client_Pay_Comment", 1 + "#varchar#" + model.Client_Pay_Comment);
                input_parameters.Add("@Client_Pay_EnteredBy", 1 + "#varchar#" + model.Client_Pay_EnteredBy);
                input_parameters.Add("@Client_Pay_Balance_Due", 1 + "#decimal#" + model.Client_Pay_Balance_Due);
                input_parameters.Add("@Client_Pay_IsActive", 1 + "#bit#" + model.Client_Pay_IsActive);
                input_parameters.Add("@Client_Pay_IsDelete", 1 + "#bit#" + model.Client_Pay_IsDelete);

                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Client_Pay_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;



        }

        private DataSet GetClientPaymentMaster(Client_Invoice_PatymentDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Client_Invoice_Patyment]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Client_Pay_PkeyId", 1 + "#bigint#" + model.Client_Pay_PkeyId);
                input_parameters.Add("@Client_Pay_Wo_Id", 1 + "#bigint#" + model.Client_Pay_Wo_Id);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetClientPaymentDetails(Client_Invoice_PatymentDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                model.Client_Pay_PkeyId = model.Pay_PkeyId;
                model.Client_Pay_Wo_Id = model.Pay_Wo_Id;

                DataSet ds = GetClientPaymentMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Client_Invoice_PatymentDTO> ClientPayment =
                   (from item in myEnumerableFeaprd
                    select new Client_Invoice_PatymentDTO
                    {
                        Client_Pay_PkeyId = item.Field<Int64>("Client_Pay_PkeyId"),
                        Client_Pay_Invoice_Id = item.Field<Int64?>("Client_Pay_Invoice_Id"),
                        Client_Pay_Wo_Id = item.Field<Int64?>("Client_Pay_Wo_Id"),
                        Client_Pay_Payment_Date = item.Field<DateTime?>("Client_Pay_Payment_Date"),
                        Client_Pay_Amount = item.Field<Decimal?>("Client_Pay_Amount"),
                        Client_Pay_CheckNumber = item.Field<String>("Client_Pay_CheckNumber"),
                        Client_Pay_Comment = item.Field<String>("Client_Pay_Comment"),
                        Client_Pay_EnteredBy = item.Field<String>("Client_Pay_EnteredBy"),
                        Client_Pay_Balance_Due = item.Field<Decimal?>("Client_Pay_Balance_Due"),
                        Client_Pay_IsActive = item.Field<Boolean?>("Client_Pay_IsActive"),


                    }).ToList();

                objDynamic.Add(ClientPayment);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
        // Added By Dipali
        public List<dynamic> AddClientPaymentDataList(List<Client_Invoice_PatymentDTO> modelList)
        {
            List<dynamic> objDataModel = new List<dynamic>();
            foreach (var model in modelList)
            {
                try
                {
                    var objData = AddClientPaymentData(model);
                    objDataModel.Add(objData);
                }
                catch (Exception ex)
                {
                    log.logErrorMessage(ex.StackTrace);
                    log.logErrorMessage(ex.Message);
                }                
            }
            return objDataModel;
        }



        private DataSet GetClientMultiPaymentMaster(Client_Invoice_MultiPatymentDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_MultipleClientInvoice_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();
                input_parameters.Add("@whereclause", 1 + "#varchar#" + model.whereclause);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ds;
        }

        public List<dynamic> GetClientMultiPaymentDetails(Client_Invoice_MultiPatymentDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                string wherecondition = string.Empty;
                string wo = string.Empty;
                var data = JsonConvert.DeserializeObject<List<FilterDataDto>>(model.FilterData);

                for (int i = 0; i < data.Count; i++)
                {
                    wo = wo == null ?  data[i].WorkOrderID.ToString() : wo + "," + data[i].WorkOrderID.ToString();
                   
                }
                if (wo != null)
                {
                    string str = wo.Remove(0, 1);
                    wherecondition = str + ")";
                }
                Client_Invoice_MultiPatymentDTO client_Invoice_MultiPatymentDTO = new Client_Invoice_MultiPatymentDTO();

                client_Invoice_MultiPatymentDTO.whereclause = wherecondition;
                client_Invoice_MultiPatymentDTO.Type = 1;
                DataSet ds = GetClientMultiPaymentMaster(client_Invoice_MultiPatymentDTO);

                var myEnumerableFeaprdm = ds.Tables[0].AsEnumerable();
                List<Client_Invoice_MultiPatymentDTO> multiClientPayment =
                   (from item in myEnumerableFeaprdm
                    select new Client_Invoice_MultiPatymentDTO
                    {
                        Inv_Client_pkeyId = item.Field<Int64>("Inv_Client_pkeyId"),
                        Inv_Client_Invoice_Id = item.Field<Int64?>("Inv_Client_Invoice_Id"),
                        Inv_Client_WO_Id = item.Field<Int64?>("Inv_Client_WO_Id"),
                        Inv_Client_Sub_Total = item.Field<Decimal?>("Inv_Client_Sub_Total"),
                        Inv_Client_Invoice_Number = item.Field<String>("Inv_Client_Invoice_Number"),
                        IPLNO = item.Field<String>("IPLNO"),
                        Client_Amount = 0,
                        Client_Comment = "",

                    }).ToList();

                objDynamic.Add(multiClientPayment);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

    }
}