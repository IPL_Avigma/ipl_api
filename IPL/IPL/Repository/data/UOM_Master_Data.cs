﻿using Avigma.Repository.Lib;
using IPL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class UOM_Master_Data
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();


        public List<dynamic> AddUOMMasterData(UOM_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();

            string insertProcedure = "[CreateUpdateUOM_Master]";
            UOM_Master uOM_Master = new UOM_Master();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@UOM_pkeyId", 1 + "#bigint#" + model.UOM_pkeyId);
                input_parameters.Add("@UOM_Name", 1 + "#varchar#" + model.UOM_Name);
                input_parameters.Add("@UOM_IsActive", 1 + "#bit#" + model.UOM_IsActive);
                input_parameters.Add("@UOM_IsDelete", 1 + "#bit#" + model.UOM_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UOM_pkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters); 
                if (objData[1] == 0)
                {
                    uOM_Master.UOM_pkeyId = "0";
                    uOM_Master.Status = "0";
                    uOM_Master.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    uOM_Master.UOM_pkeyId = Convert.ToString(objData[0]);
                    uOM_Master.Status = Convert.ToString(objData[1]);


                }
                objcltData.Add(uOM_Master);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;



        }

        private DataSet GetUOM_Master(UOM_MasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_UOMMaster]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@UOM_pkeyId", 1 + "#bigint#" + model.UOM_pkeyId);
                input_parameters.Add("@WhereClause", 1 + "#varchar#" + model.WhereClause);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetUOM_MasterDetails(UOM_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetUOM_Master(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<UOM_MasterDTO> UOM_MasterDetails =
                   (from item in myEnumerableFeaprd
                    select new UOM_MasterDTO
                    {
                        UOM_pkeyId = item.Field<Int64>("UOM_pkeyId"),
                        UOM_Name = item.Field<String>("UOM_Name"),
                        UOM_IsActive = item.Field<Boolean?>("UOM_IsActive"),
                        UOM_CreatedBy = item.Field<String>("UOM_CreatedBy"),
                        UOM_ModifiedBy = item.Field<String>("UOM_ModifiedBy"),

                    }).ToList();

                objDynamic.Add(UOM_MasterDetails);

                if (model.Type == 1)
                {
                    if (ds.Tables.Count > 1)
                    {
                        var myEnumerableInsFilter = ds.Tables[1].AsEnumerable();
                        List<Filter_Admin_UOM_MasterDTO> insFilterList =
                           (from item in myEnumerableInsFilter
                            select new Filter_Admin_UOM_MasterDTO
                            {
                                UOM_Filter_PkeyID = item.Field<Int64>("UOM_Filter_PkeyID"),
                                UOM_Filter_UOMName = item.Field<String>("UOM_Filter_UOMName"),
                                UOM_Filter_UOMIsActive = item.Field<Boolean?>("UOM_Filter_UOMIsActive"),
                                UOM_Filter_CreatedBy = item.Field<String>("UOM_Filter_CreatedBy"),
                                UOM_Filter_ModifiedBy = item.Field<String>("UOM_Filter_ModifiedBy"),
                            }).ToList();

                        objDynamic.Add(insFilterList);
                    }
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        public List<dynamic> GetUomFilterDetails(UOM_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();


            string wherecondition = string.Empty;
            try
            {

                var Data = JsonConvert.DeserializeObject<UOM_MasterDTO>(model.FilterData);
                if (!string.IsNullOrEmpty(Data.UOM_Name))
                {
                    // wherecondition = " And UOM_Name =    '" + Data.UOM_Name + "'";
                    wherecondition = " And UOM_Name LIKE '%" + Data.UOM_Name + "%'";
                }


                if (Data.UOM_IsActive == true)
                {
                    wherecondition = wherecondition + "  And UOM_IsActive =  '1'";
                }
                if (Data.UOM_IsActive == false)
                {
                    wherecondition = wherecondition + "  And UOM_IsActive =  0";
                }


                UOM_MasterDTO uOM_MasterDTO = new UOM_MasterDTO();

                uOM_MasterDTO.WhereClause = wherecondition;
                uOM_MasterDTO.Type = 3;
                uOM_MasterDTO.UserID = model.UserID;

                DataSet ds = GetUOM_Master(uOM_MasterDTO);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<UOM_MasterDTO> uomfilter =
                   (from item in myEnumerableFeaprd
                    select new UOM_MasterDTO
                    {

                        UOM_pkeyId = item.Field<Int64>("UOM_pkeyId"),
                        UOM_Name = item.Field<String>("UOM_Name"),
                        UOM_IsActive = item.Field<Boolean?>("UOM_IsActive"),
                        UOM_CreatedBy = item.Field<String>("UOM_CreatedBy"),
                        UOM_ModifiedBy = item.Field<String>("UOM_ModifiedBy"),
                    }).ToList();

                objDynamic.Add(uomfilter);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }

            return objDynamic;
        }

    }
}