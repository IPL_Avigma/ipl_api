﻿using Avigma.Repository.Lib;
using IPL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Avigma.Models;
using IPLApp.Repository.data;
using IPLApp.Models;
namespace IPL.Repository.data
{
    public class Import_WorkOrder_Excel_MasterData
    {
        Log log = new Log();
        MyDataSourceFactory obj = new MyDataSourceFactory();
        private List<dynamic> Add_Import_Workorder_Excel_Data(Import_WorkOrder_Excel_MasterDTO model)
        {
            List<dynamic> objdata = new List<dynamic>();
         
            try
            {
                string insertProcedure = "[CreateUpdate_Import_WorkOrder_Excel]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@IMP_PkeyId", 1 + "#bigint#" + model.IMP_PkeyId);
                input_parameters.Add("@workOrderNumber", 1 + "#varchar#" + model.workOrderNumber.Trim());
                input_parameters.Add("@WorkType", 1 + "#varchar#" + model.WorkType.Trim());
                input_parameters.Add("@address1", 1 + "#varchar#" + model.address1.Trim());
                input_parameters.Add("@city", 1 + "#varchar#" + model.city.Trim());
                input_parameters.Add("@state", 1 + "#varchar#" + model.state.Trim());
                input_parameters.Add("@zip", 1 + "#varchar#" + model.zip.Trim());
                input_parameters.Add("@Company", 1 + "#varchar#" + model.Company.Trim());
                input_parameters.Add("@Contractor", 1 + "#varchar#" + model.Contractor.Trim());
                input_parameters.Add("@Cordinator", 1 + "#varchar#" + model.Cordinator.Trim());
                input_parameters.Add("@Processor", 1 + "#varchar#" + model.Processor.Trim());
                input_parameters.Add("@Category", 1 + "#varchar#" + model.Category.Trim());
                input_parameters.Add("@Loan_Info", 1 + "#varchar#" + model.Loan_Info.Trim());
                input_parameters.Add("@Loan_Number", 1 + "#varchar#" + model.Loan_Number.Trim());
                input_parameters.Add("@Customer_Number", 1 + "#varchar#" + model.Customer_Number.Trim());
                input_parameters.Add("@BATF", 1 + "#varchar#" + model.BATF.Trim());
                input_parameters.Add("@ISInspection", 1 + "#varchar#" + model.ISInspection.Trim());
                input_parameters.Add("@Lotsize", 1 + "#varchar#" + model.Lotsize.Trim());
                input_parameters.Add("@Rush", 1 + "#varchar#" + model.Rush.Trim());
                input_parameters.Add("@Lock_Code", 1 + "#varchar#" + model.Lock_Code.Trim());
                input_parameters.Add("@Lock_Location", 1 + "#varchar#" + model.Lock_Location.Trim());
                input_parameters.Add("@Key_Code", 1 + "#varchar#" + model.Key_Code.Trim());
                input_parameters.Add("@Gate_Code", 1 + "#varchar#" + model.Gate_Code.Trim());
                input_parameters.Add("@Broker_Info", 1 + "#varchar#" + model.Broker_Info.Trim());
                input_parameters.Add("@Received_Date", 1 + "#varchar#" + model.Received_Date.Trim());
                input_parameters.Add("@startDate", 1 + "#varchar#" + model.startDate.Trim());
                input_parameters.Add("@dueDate", 1 + "#varchar#" + model.dueDate.Trim());
                input_parameters.Add("@clientDueDate", 1 + "#varchar#" + model.clientDueDate.Trim());
                input_parameters.Add("@Comments", 1 + "#varchar#" + model.Comments.Trim());
                input_parameters.Add("@import_type", 1 + "#int#" + model.import_type);
                input_parameters.Add("@IMP_File_Path", 1 + "#varchar#" + model.IMP_File_Path.Trim());
                input_parameters.Add("@IMP_File_Name", 1 + "#varchar#" + model.IMP_File_Name.Trim());
                input_parameters.Add("@IMP_IsActive", 1 + "#bit#" + model.IMP_IsActive);
                input_parameters.Add("@IMP_IsDelete", 1 + "#bit#" + model.IMP_IsDelete);

                input_parameters.Add("@gpsLatitude", 1 + "#varchar#" + model.gpsLatitude);
                input_parameters.Add("@gpsLongitude", 1 + "#varchar#" + model.gpsLongitude);

                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                input_parameters.Add("@IMP_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objdata = obj.SqlCRUD(insertProcedure, input_parameters);

               
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return null;
            }

            return objdata;

        }

        public List<dynamic> AddWorkOrderExcelDetails(ExcelDataDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
          
            try
            {

               
                var Data = JsonConvert.DeserializeObject<List<Import_WorkOrder_Excel_MasterDTO>>(model.ExcelData);
                for (int i = 0; i < Data.Count; i++)
                {
                    GoogleLocation googleLocation = new GoogleLocation();
                    GoogleLocationDTO googleLocationDTO = new GoogleLocationDTO();

                    WorkOrderMasterData workOrderMasterData = new WorkOrderMasterData();
                    workOrderxDTO workOrderxDTO = new workOrderxDTO();
                    workOrderxDTO.address1 = Data[i].address1;
                    workOrderxDTO.state = Data[i].state;
                    if (!string.IsNullOrWhiteSpace(Data[i].zip))
                    {
                        workOrderxDTO.zip = Convert.ToInt32(Data[i].zip);
                    }
                   
                    workOrderxDTO.Type = 3;
                    DataSet ds = workOrderMasterData.Get_WorkOrderLatLongByAddress(workOrderxDTO);

                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            for (int j = 0; j < ds.Tables[0].Rows.Count; j++)
                            {
                                Data[i].gpsLatitude = ds.Tables[0].Rows[j]["gpsLatitude"].ToString();
                                Data[i].gpsLongitude = ds.Tables[0].Rows[j]["gpsLongitude"].ToString();
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(Data[i].address1) && !string.IsNullOrEmpty(Data[i].state) && !string.IsNullOrEmpty(Data[i].zip))
                            {
                                string strAddress = Data[i].address1 + "  ," + Data[i].state + "  " + Data[i].zip;

                                googleLocationDTO.Address = strAddress;
                                googleLocationDTO = googleLocation.GetLatitudeLogitudeWithAPIKey(googleLocationDTO);

                                Data[i].gpsLatitude = googleLocationDTO.Latitude;
                                Data[i].gpsLongitude = googleLocationDTO.Longitude;

                            }
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(Data[i].address1) && !string.IsNullOrEmpty(Data[i].state) && !string.IsNullOrEmpty(Data[i].zip))
                        {
                            string strAddress = Data[i].address1 + "  ," + Data[i].state + "  " + Data[i].zip;

                            googleLocationDTO.Address = strAddress;
                            googleLocationDTO = googleLocation.GetLatitudeLogitudeWithAPIKey(googleLocationDTO);

                            Data[i].gpsLatitude = googleLocationDTO.Latitude;
                            Data[i].gpsLongitude = googleLocationDTO.Longitude;

                        }
                    }


                     
                    Data[i].UserID = model.UserID;
                    Data[i].Type = 1;
                    Data[i].IMP_IsActive = true;
                    Data[i].IMP_IsDelete = false;

                    objData = Add_Import_Workorder_Excel_Data(Data[i]);

                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }
            return objData;

        }





        private DataSet GetImport_Workorder_Excel_Data(Import_WorkOrder_Excel_MasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Import_WorkOrder_Excel]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@IMP_PkeyId", 1 + "#bigint#" + model.IMP_PkeyId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetImportWororderExcelDetails(Import_WorkOrder_Excel_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetImport_Workorder_Excel_Data(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Import_WorkOrder_Excel_MasterDTO> exceldetails =
                   (from item in myEnumerableFeaprd
                    select new Import_WorkOrder_Excel_MasterDTO
                    {
                        IMP_PkeyId = item.Field<Int64>("IMP_PkeyId"),
                        workOrderNumber = item.Field<String>("workOrderNumber"),
                        WorkType = item.Field<String>("WorkType"),
                        address1 = item.Field<String>("address1"),
                        city = item.Field<String>("city"),
                        state = item.Field<String>("state"),
                        zip = item.Field<String>("zip"),
                        Company = item.Field<String>("Company"),
                        Contractor = item.Field<String>("Contractor"),
                        Cordinator = item.Field<String>("Cordinator"),
                        Processor = item.Field<String>("Processor"),
                        Category = item.Field<String>("Category"),
                        Loan_Info = item.Field<String>("Loan_Info"),
                        Loan_Number = item.Field<String>("Loan_Number"),
                        Customer_Number = item.Field<String>("Customer_Number"),
                        BATF = item.Field<String>("BATF"),
                        ISInspection = item.Field<String>("ISInspection"),
                        Lotsize = item.Field<String>("Lotsize"),
                        Rush = item.Field<String>("Rush"),
                        Lock_Code = item.Field<String>("Lock_Code"),
                        Lock_Location = item.Field<String>("Lock_Location"),
                        Key_Code = item.Field<String>("Key_Code"),
                        Gate_Code = item.Field<String>("Gate_Code"),
                        Broker_Info = item.Field<String>("Broker_Info"),
                        Received_Date = item.Field<String>("Received_Date"),
                        startDate = item.Field<String>("startDate"),
                        dueDate = item.Field<String>("dueDate"),
                        clientDueDate = item.Field<String>("clientDueDate"),
                        Comments = item.Field<String>("Comments"),
                        import_type = item.Field<int?>("import_type"),
                        IMP_File_Path = item.Field<String>("IMP_File_Path"),
                        IMP_File_Name = item.Field<String>("IMP_File_Name"),
                        IMP_IsActive = item.Field<Boolean?>("IMP_IsActive"),

                    }).ToList();

                objDynamic.Add(exceldetails);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}