﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class WorkOrder_Message_DocumentData
    {
        Log log = new Log();
        MyDataSourceFactory obj = new MyDataSourceFactory();
        private List<dynamic> AddWorkOrderMessageDocumentData(WorkOrder_Message_DocumentDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objAddData = new List<dynamic>();
            string insertProcedure = "[CreateUpdateWorkOrder_Msg_Document]";
            WorkOrder_Message_Document workOrder_Message_Document = new WorkOrder_Message_Document();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Wo_Msg_Doc_PkeyId", 1 + "#bigint#" + model.Wo_Msg_Doc_PkeyId);
                input_parameters.Add("@Wo_Msg_Doc_Wo_ID", 1 + "#bigint#" + model.Wo_Msg_Doc_Wo_ID);
                input_parameters.Add("@Wo_Msg_Doc_File_Path", 1 + "#nvarchar#" + model.Wo_Msg_Doc_File_Path);
                input_parameters.Add("@Wo_Msg_Doc_File_Size", 1 + "#nvarchar#" + model.Wo_Msg_Doc_File_Size);
                input_parameters.Add("@Wo_Msg_Doc_File_Name", 1 + "#nvarchar#" + model.Wo_Msg_Doc_File_Name);
                input_parameters.Add("@Wo_Msg_Doc_BucketName", 1 + "#nvarchar#" + model.Wo_Msg_Doc_BucketName);
                input_parameters.Add("@Wo_Msg_Doc_ProjectID", 1 + "#nvarchar#" + model.Wo_Msg_Doc_ProjectID);
                input_parameters.Add("@Wo_Msg_Doc_Object_Name", 1 + "#nvarchar#" + model.Wo_Msg_Doc_Object_Name);
                input_parameters.Add("@Wo_Msg_Doc_Folder_Name", 1 + "#nvarchar#" + model.Wo_Msg_Doc_Folder_Name);
                input_parameters.Add("@Wo_Msg_Doc_UploadedBy", 1 + "#nvarchar#" + model.Wo_Msg_Doc_UploadedBy);
                input_parameters.Add("@Wo_Msg_Doc_Processor_ID", 1 + "#bigint#" + model.Wo_Msg_Doc_Processor_ID);
                input_parameters.Add("@Wo_Msg_Doc_Contractor_ID", 1 + "#bigint#" + model.Wo_Msg_Doc_Contractor_ID);
                input_parameters.Add("@Wo_Msg_Doc_Cordinator_ID", 1 + "#bigint#" + model.Wo_Msg_Doc_Cordinator_ID);
                input_parameters.Add("@Wo_Msg_Doc_Client_ID", 1 + "#bigint#" + model.Wo_Msg_Doc_Client_ID);
                input_parameters.Add("@Wo_Msg_Doc_IPLNO", 1 + "#nvarchar#" + model.Wo_Msg_Doc_IPLNO);
                input_parameters.Add("@Wo_Folder_File_Master_FKId", 1 + "#bigint#" + model.Wo_Folder_File_Master_FKId);
                input_parameters.Add("@Wo_Msg_Doc_Ch_ID", 1 + "#bigint#" + model.Wo_Msg_Doc_Ch_ID);
                input_parameters.Add("@Wo_Msg_Doc_Company_Id", 1 + "#bigint#" + model.Wo_Msg_Doc_Company_Id);

                input_parameters.Add("@Wo_Msg_Doc_IsActive", 1 + "#bit#" + model.Wo_Msg_Doc_IsActive);
                input_parameters.Add("@Wo_Msg_Doc_IsDelete", 1 + "#bit#" + model.Wo_Msg_Doc_IsDelete);
                input_parameters.Add("@Wo_Msg_Doc_Type", 1 + "#int#" + model.Wo_Msg_Doc_Type);

                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Wo_Msg_Doc_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    workOrder_Message_Document.Wo_Msg_Doc_PkeyId = "0";
                    workOrder_Message_Document.Status = "0";
                    workOrder_Message_Document.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    workOrder_Message_Document.Wo_Msg_Doc_PkeyId = Convert.ToString(objData[0]);
                    workOrder_Message_Document.Status = Convert.ToString(objData[1]);


                }
                objAddData.Add(workOrder_Message_Document);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objAddData;



        }

        // call workorder Message documents

        public List<dynamic> AddUpdateWorkOrderMessageDocsData(Client_Result_PhotoDTO client_Result_PhotoDTO)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
               
                WorkOrder_Message_DocumentDTO workOrder_Message_DocumentDTO = new WorkOrder_Message_DocumentDTO();

                workOrder_Message_DocumentDTO.Wo_Msg_Doc_PkeyId = client_Result_PhotoDTO.Client_Result_Photo_ID;
                workOrder_Message_DocumentDTO.Wo_Msg_Doc_Ch_ID = client_Result_PhotoDTO.Client_Result_Photo_Ch_ID;
                workOrder_Message_DocumentDTO.Wo_Msg_Doc_Wo_ID = Convert.ToInt64(client_Result_PhotoDTO.Client_Result_Photo_Wo_ID);
                workOrder_Message_DocumentDTO.Wo_Msg_Doc_File_Path = client_Result_PhotoDTO.Client_Result_Photo_FilePath;
                workOrder_Message_DocumentDTO.Wo_Msg_Doc_File_Size = client_Result_PhotoDTO.Client_Result_Photo_FileSize;
                workOrder_Message_DocumentDTO.Wo_Msg_Doc_File_Name = client_Result_PhotoDTO.Client_Result_Photo_FileName;
                workOrder_Message_DocumentDTO.Wo_Msg_Doc_BucketName = client_Result_PhotoDTO.Client_Result_Photo_BucketName;
                workOrder_Message_DocumentDTO.Wo_Msg_Doc_ProjectID = client_Result_PhotoDTO.Client_Result_Photo_ProjectID;
                workOrder_Message_DocumentDTO.Wo_Msg_Doc_Object_Name = client_Result_PhotoDTO.Client_Result_Photo_objectName;
                workOrder_Message_DocumentDTO.Wo_Msg_Doc_Folder_Name = client_Result_PhotoDTO.Client_Result_Photo_FolderName;
                workOrder_Message_DocumentDTO.Wo_Msg_Doc_UploadedBy = Convert.ToString(client_Result_PhotoDTO.UserID);
                workOrder_Message_DocumentDTO.Wo_Msg_Doc_Processor_ID = client_Result_PhotoDTO.Wo_Msg_Doc_Processor_ID;
                workOrder_Message_DocumentDTO.Wo_Msg_Doc_Contractor_ID = client_Result_PhotoDTO.Wo_Msg_Doc_Contractor_ID;
                workOrder_Message_DocumentDTO.Wo_Msg_Doc_Cordinator_ID = client_Result_PhotoDTO.Wo_Msg_Doc_Cordinator_ID;
                workOrder_Message_DocumentDTO.Wo_Msg_Doc_Client_ID = client_Result_PhotoDTO.Wo_Msg_Doc_Client_ID;
                workOrder_Message_DocumentDTO.Wo_Msg_Doc_IPLNO = client_Result_PhotoDTO.Wo_Msg_Doc_IPLNO;
                workOrder_Message_DocumentDTO.Wo_Folder_File_Master_FKId = client_Result_PhotoDTO.Folder_File_Master_FKId;
                workOrder_Message_DocumentDTO.Wo_Msg_Doc_Company_Id = client_Result_PhotoDTO.Wo_Office_Doc_Company_Id;
                workOrder_Message_DocumentDTO.Wo_Msg_Doc_Type = client_Result_PhotoDTO.ContentType;
                workOrder_Message_DocumentDTO.Wo_Msg_Doc_IsActive = true;
                workOrder_Message_DocumentDTO.Wo_Msg_Doc_IsDelete = false;


                workOrder_Message_DocumentDTO.UserID = client_Result_PhotoDTO.UserID;
                workOrder_Message_DocumentDTO.Type = 1;


                objData = AddWorkOrderMessageDocumentData(workOrder_Message_DocumentDTO);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }



        private DataSet GetWorkOrderMessageDocumentMaster(WorkOrder_Message_DocumentDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_WorkOrder_Msg_Document]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Wo_Msg_Doc_PkeyId", 1 + "#bigint#" + model.Wo_Msg_Doc_PkeyId);
                input_parameters.Add("@Wo_Msg_Doc_Wo_ID", 1 + "#bigint#" + model.Wo_Msg_Doc_Wo_ID);
                input_parameters.Add("@Wo_Msg_Doc_Company_Id", 1 + "#bigint#" + model.Wo_Msg_Doc_Company_Id);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetWorkOrderMessageDocumentDetails(WorkOrder_Message_DocumentDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetWorkOrderMessageDocumentMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<WorkOrder_Message_DocumentDTO> WorkOrderMessageDoc =
                   (from item in myEnumerableFeaprd
                    select new WorkOrder_Message_DocumentDTO
                    {
                        Wo_Msg_Doc_PkeyId = item.Field<Int64>("Wo_Msg_Doc_PkeyId"),
                        Wo_Msg_Doc_Wo_ID = item.Field<Int64>("Wo_Msg_Doc_Wo_ID"),
                        Wo_Msg_Doc_File_Path = item.Field<String>("Wo_Msg_Doc_File_Path"),
                        Wo_Msg_Doc_File_Size = item.Field<String>("Wo_Msg_Doc_File_Size"),
                        Wo_Msg_Doc_File_Name = item.Field<String>("Wo_Msg_Doc_File_Name"),
                        Wo_Msg_Doc_BucketName = item.Field<String>("Wo_Msg_Doc_BucketName"),
                        Wo_Msg_Doc_ProjectID = item.Field<String>("Wo_Msg_Doc_ProjectID"),
                        Wo_Msg_Doc_Object_Name = item.Field<String>("Wo_Msg_Doc_Object_Name"),
                        Wo_Msg_Doc_Folder_Name = item.Field<String>("Wo_Msg_Doc_Folder_Name"),
                        Wo_Msg_Doc_UploadedBy = item.Field<String>("Wo_Msg_Doc_UploadedBy"),
                        Wo_Msg_Doc_Processor_ID = item.Field<Int64?>("Wo_Msg_Doc_Processor_ID"),
                        Wo_Msg_Doc_Contractor_ID = item.Field<Int64?>("Wo_Msg_Doc_Contractor_ID"),
                        Wo_Msg_Doc_Cordinator_ID = item.Field<Int64?>("Wo_Msg_Doc_Cordinator_ID"),
                        Wo_Msg_Doc_Client_ID = item.Field<Int64?>("Wo_Msg_Doc_Client_ID"),
                        Wo_Msg_Doc_IPLNO = item.Field<String>("Wo_Msg_Doc_IPLNO"),
                        Wo_Msg_Doc_Ch_ID = item.Field<Int64?>("Wo_Msg_Doc_Ch_ID"),
                        Wo_Msg_Doc_Company_Id = item.Field<Int64?>("Wo_Msg_Doc_Company_Id"),
                        Wo_Folder_File_Master_FKId = item.Field<Int64?>("Wo_Folder_File_Master_FKId"),
                        Wo_Msg_Doc_IsActive = item.Field<Boolean?>("Wo_Msg_Doc_IsActive"),
                        Wo_Msg_Doc_Type = item.Field<int?>("Wo_Msg_Doc_Company_Id"),

                    }).ToList();

                objDynamic.Add(WorkOrderMessageDoc);

                
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
        public List<dynamic> DeleteMessageDocument(WorkOrder_Message_DocumentDTO workOrder_Message_DocumentDTO)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                workOrder_Message_DocumentDTO.Wo_Msg_Doc_IsActive = false;
                workOrder_Message_DocumentDTO.Wo_Msg_Doc_IsDelete = true;

                objData = AddWorkOrderMessageDocumentData(workOrder_Message_DocumentDTO);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }
    }
}