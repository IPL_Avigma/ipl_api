﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class Folder_Client_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        public List<dynamic> AddFolder_Client_Master(Folder_Client_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_Folder_Client_Master]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Fold_Client_PkeyId", 1 + "#bigint#" + model.Fold_Client_PkeyId);
                input_parameters.Add("@Fold_Client_Id", 1 + "#bigint#" + model.Fold_Client_Id);
                input_parameters.Add("@Fold_Client_Folder_Id", 1 + "#bigint#" + model.Fold_Client_Folder_Id);
                input_parameters.Add("@Fold_Client_File_Id", 1 + "#bigint#" + model.Fold_Client_File_Id);
                input_parameters.Add("@Fold_Client_Auto_Assine_Id", 1 + "#bigint#" + model.Fold_Client_Auto_Assine_Id);

                input_parameters.Add("@Fold_Client_IsActive", 1 + "#bit#" + model.Fold_Client_IsActive);
                input_parameters.Add("@Fold_Client_IsDelete", 1 + "#bit#" + model.Fold_Client_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Fold_Client_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);
                input_parameters.Add("@Folder_File_Master_FK_Id", 1 + "#bigint#" + model.Folder_File_Master_FK_Id);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;



        }
    }
}