﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class HeaderMapData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        private DataSet GetOfficeMapMaster(HeaderMapDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_OfficeResultsMapDetails]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@workOrder_ID", 1 + "#bigint#" + model.workOrder_ID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetOfficeMapDetails(HeaderMapDTO model)
        {
               List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                  DataSet ds = GetOfficeMapMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<HeaderMapDTO> mapdata =
                   (from item in myEnumerableFeaprd
                    select new HeaderMapDTO
                    {
                        workOrder_ID = item.Field<Int64>("workOrder_ID"),
                        address1 = item.Field<String>("address1"),
                        gpsLatitude = item.Field<String>("gpsLatitude"),
                        gpsLongitude = item.Field<String>("gpsLongitude"),

                    }).ToList();

                objDynamic.Add(mapdata);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}