﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class Mass_File_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        private List<dynamic> AddupdateMassAttachedData(Mass_File_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_Mass_File_Master]";
            Mass_File_Master mass_File_Master = new Mass_File_Master();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Mass_File_PkeyId", 1 + "#bigint#" + model.Mass_File_PkeyId);
                input_parameters.Add("@Mass_File_Email_ID", 1 + "#bigint#" + model.Mass_File_Email_ID);
                input_parameters.Add("@Mass_File_FileName", 1 + "#nvarchar#" + model.Mass_File_FileName);
                input_parameters.Add("@Mass_File_FilePth", 1 + "#nvarchar#" + model.Mass_File_FilePth);
                input_parameters.Add("@Mass_File_IsActive", 1 + "#bit#" + model.Mass_File_IsActive);
                input_parameters.Add("@Mass_File_IsDelete", 1 + "#bit#" + model.Mass_File_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Mass_File_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    mass_File_Master.Mass_File_PkeyId = "0";
                    mass_File_Master.Status = "0";
                    mass_File_Master.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    mass_File_Master.Mass_File_PkeyId = Convert.ToString(objData[0]);
                    mass_File_Master.Status = Convert.ToString(objData[1]);
                }
                objcltData.Add(mass_File_Master);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;

        }

        public List<dynamic> AddMassAttachedData(Mass_File_MasterDTO model)
        {
            List<dynamic> objcltData = new List<dynamic>();
            try
            {
                objcltData = AddupdateMassAttachedData(model);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;

        }

        public List<dynamic> AddMassAttachedData(UPload_Client_Result_PhotoDTO model)
        {
            List<dynamic> objcltData = new List<dynamic>();
            Mass_File_MasterDTO mass_File_MasterDTO = new Mass_File_MasterDTO();
            try
            {
                mass_File_MasterDTO.Mass_File_PkeyId = Convert.ToInt64(model.Client_Result_Photo_Wo_ID);
                mass_File_MasterDTO.Mass_File_Email_ID = model.Client_Result_Photo_Ch_ID;
                mass_File_MasterDTO.Mass_File_FileName = model.Client_Result_Photo_FileName;
                mass_File_MasterDTO.Mass_File_FilePth = model.Client_Result_Photo_FilePath;
                mass_File_MasterDTO.Mass_File_IsActive = true;
                mass_File_MasterDTO.Mass_File_IsDelete = false;
                mass_File_MasterDTO.Type = model.Type;
                objcltData = AddupdateMassAttachedData(mass_File_MasterDTO);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;

        }

        private DataSet GetMassAttachedData(Mass_File_MasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Mass_File_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Mass_File_PkeyId", 1 + "#bigint#" + model.Mass_File_PkeyId);
                input_parameters.Add("@Mass_File_Email_ID", 1 + "#bigint#" + model.Mass_File_Email_ID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return ds;
        }

        public List<dynamic> GetMassAttachedDetaills(Mass_File_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetMassAttachedData(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Mass_File_MasterDTO> massemailfile =
                   (from item in myEnumerableFeaprd
                    select new Mass_File_MasterDTO
                    {
                        Mass_File_PkeyId = item.Field<Int64>("Mass_File_PkeyId"),
                        Mass_File_Email_ID = item.Field<Int64>("Mass_File_Email_ID"),
                        Mass_File_FileName = item.Field<String>("Mass_File_FileName"),
                        Mass_File_FilePth = item.Field<String>("Mass_File_FilePth"),
                        Mass_File_IsActive = item.Field<Boolean?>("Mass_File_IsActive"),
                    

                    }).ToList();

                objDynamic.Add(massemailfile);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}