﻿using Avigma.Repository.Lib;
using IPL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class ScoreCard_Data
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
    
        //check
        private List<dynamic> AddScoreCard_Data(ScoreCard_DataDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_ScoreCard_Data]";
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Scd_pkeyId", 1 + "#bigint#" + model.Scd_pkeyId);
                input_parameters.Add("@Scd_Wo_Id", 1 + "#bigint#" + model.Scd_Wo_Id);
                input_parameters.Add("@Scd_Con_ID", 1 + "#bigint#" + model.Scd_Con_ID);
                input_parameters.Add("@Scd_Comment", 1 + "#varchar#" + model.Scd_Comment);
                input_parameters.Add("@Scd_Status_Id", 1 + "#int#" + model.Scd_Status_Id);
                input_parameters.Add("@Scd_IsActive", 1 + "#bit#" + model.Scd_IsActive);
                input_parameters.Add("@Scd_IsDelete", 1 + "#bit#" + model.Scd_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Scd_pkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters); ;
              


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }
            return objData;



        }

        private List<dynamic> CreateUpdate_ContractorScoreByWorkOrder(ScoreCard_DataDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_ContractorScoreByWorkOrder]";
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                
                input_parameters.Add("@WorkOrderID", 1 + "#bigint#" + model.Scd_Wo_Id);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Con_Us_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters); ;



            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }
            return objData;



        }


        private int? GetStatusValue(string strStatus)
        {
            int? val = 0;
            try
            {
                switch (strStatus)
                {
                    case "A":
                        {
                            val = 1;
                            break;
                        }
                    case "B":
                        {
                            val = 2;
                            break;
                        }
                    case "C":
                        {
                            val = 3;
                            break;
                        }
                    case "D":
                        {
                            val = 4;
                            break;
                        }
                    case "E":
                        {
                            val = 5;
                            break;
                        }
                    case "F":
                        {
                            val = 6;
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return val;
        }

        public List<dynamic> AddScoreCardDetails(ScoreCard_DataDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                if (model.Scd_pkeyId == 0)
                {
                    model.Type = 1;
                }
                else
                {
                    model.Type = 2;
                }

                ScoreCard_DataVal_ChildData scoreCard_DataVal_ChildData = new ScoreCard_DataVal_ChildData();
                objData = AddScoreCard_Data(model);
                if (!string.IsNullOrWhiteSpace(model.ScoreCard_data))
                {
                    var Data = JsonConvert.DeserializeObject<List<ScoreCard_DTO>>(model.ScoreCard_data);
                    for (int i = 0; i < Data.Count; i++)
                    {
                        ScoreCard_DataVal_ChildDTO scoreCard_DataVal_ChildDTO = new ScoreCard_DataVal_ChildDTO();
                        scoreCard_DataVal_ChildDTO.Scdc_Status_Id = GetStatusValue(Data[i].str_Scdc_Status_Id);
                        scoreCard_DataVal_ChildDTO.Scdc_Sna_pkeyId = Data[i].Sna_pkeyId;
                        if (model.Scd_pkeyId == 0)
                        {
                            scoreCard_DataVal_ChildDTO.Scdc_Scd_pkeyId = objData[0];
                        }
                        else
                        {
                            scoreCard_DataVal_ChildDTO.Scdc_Scd_pkeyId = model.Scd_pkeyId;
                        }
                        scoreCard_DataVal_ChildDTO.Type = 1;
                        scoreCard_DataVal_ChildDTO.Scdc_IsActive = true;
                        scoreCard_DataVal_ChildDTO.Scdc_IsDelete = false;
                        scoreCard_DataVal_ChildDTO.UserID = model.UserID;

                        scoreCard_DataVal_ChildData.AddScoreCard_DataVal_Child(scoreCard_DataVal_ChildDTO);


                    }
                    CreateUpdate_ContractorScoreByWorkOrder(model);

                }
             
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }
            return objData;

        }

        private DataSet GetScoreCardData(ScoreCard_DataDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_ScoreCard_Data]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Scd_pkeyId", 1 + "#bigint#" + model.Scd_pkeyId);
                input_parameters.Add("@Scd_Wo_Id", 1 + "#bigint#" + model.Scd_Wo_Id);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }



            return ds;
        }

        public List<dynamic> GetScoreCardDetails(ScoreCard_DataDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetScoreCardData(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<ScoreCard_DataDTO> ScoreCarddetails =
                   (from item in myEnumerableFeaprd
                    select new ScoreCard_DataDTO
                    {
                        Scd_pkeyId = item.Field<Int64>("Scd_pkeyId"),
                        Scd_Wo_Id = item.Field<Int64?>("Scd_Wo_Id"),
                        Scd_Con_ID = item.Field<Int64?>("Scd_Con_ID"),
                        Scd_Comment = item.Field<String>("Scd_Comment"),
                        Scd_Status_Id = item.Field<int?>("Scd_Status_Id"),
                        Scd_IsActive = item.Field<Boolean?>("Scd_IsActive"),
                       

                    }).ToList();

                objDynamic.Add(ScoreCarddetails);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }

            return objDynamic;
        }
    }
}