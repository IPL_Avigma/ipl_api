﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class ContractorMapData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        private DataSet GetContractorMapData(ContractorMapDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_UserAddress_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@IPL_PkeyID", 1 + "#bigint#" + model.IPL_PkeyID);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@IPL_UserID", 1 + "#bigint#" + model.IPL_UserID);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetContractorMapDetails(ContractorMapDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {

                DataSet ds = GetContractorMapData(model);
              
                    var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                    List<ContractorMapDTO> ContractorMapDetails =
                       (from item in myEnumerableFeaprd
                        select new ContractorMapDTO
                        {
                            IPL_PkeyID = item.Field<Int64>("IPL_PkeyID"),
                            IPL_Primary_Zip_Code = item.Field<Int64?>("IPL_Primary_Zip_Code"),
                            IPL_Primary_Latitude = item.Field<String>("IPL_Primary_Latitude"),
                            IPL_Primary_Longitude = item.Field<String>("IPL_Primary_Longitude"),
                            IPL_UserID = item.Field<Int64?>("IPL_UserID"),
                            IPL_Address = item.Field<String>("IPL_Address"),
                            IPL_City = item.Field<String>("IPL_City"),
                            IPL_State = item.Field<String>("IPL_State"),
                            //Con_Cat_Name = item.Field<String>("Con_Cat_Name"),
                            Con_Cat_Back_Color = item.Field<String>("Con_Cat_Back_Color"),
                            Con_Cat_Icon = item.Field<String>("Con_Cat_Icon"),
                            IPL_IsActive = item.Field<Boolean?>("IPL_IsActive"),
                            FirstName = item.Field<String>("FirstName"),
                            User_Email = item.Field<String>("User_Email"),
                            User_CellNumber = item.Field<String>("User_CellNumber"),
                            User_Con_Cat_Id = item.Field<Int64?>("User_Con_Cat_Id"),

                        }).ToList();

                    objDynamic.Add(ContractorMapDetails);
            

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}