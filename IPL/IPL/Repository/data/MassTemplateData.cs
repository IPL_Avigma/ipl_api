﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class MassTemplateData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public List<dynamic> AddMassTemplate(MassTemplateDTO model)
        {

            string insertProcedure = "[CreateUpdate_Mass_Template_Master]";
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Mass_Template_PkeyId", 1 + "#bigint#" + model.Mass_Template_PkeyId);
                input_parameters.Add("@Mass_Template_Subject", 1 + "#varchar#" + model.Mass_Template_Subject);
                input_parameters.Add("@Mass_Template_Contant", 1 + "#varchar#" + model.Mass_Template_Contant);
                input_parameters.Add("@Mass_Template_IsActive", 1 + "#bit#" + model.Mass_Template_IsActive);
                input_parameters.Add("@Mass_Template_IsDelete", 1 + "#bit#" + model.Mass_Template_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Mass_Template_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return obj.SqlCRUD(insertProcedure, input_parameters); ;



        }

        private DataSet GetMassTemplate(MassTemplateDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Mass_Template_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Mass_Template_PkeyId", 1 + "#bigint#" + model.Mass_Template_PkeyId);
                input_parameters.Add("@WhereClause", 1 + "#varchar#" + model.WhereClause);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetMassTemplateDetails(MassTemplateDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetMassTemplate(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<MassTemplateDTO> masstemplate =
                   (from item in myEnumerableFeaprd
                    select new MassTemplateDTO
                    {
                        Mass_Template_PkeyId = item.Field<Int64>("Mass_Template_PkeyId"),
                        Mass_Template_Subject = item.Field<String>("Mass_Template_Subject"),
                        Mass_Template_Contant = item.Field<String>("Mass_Template_Contant"),
                        Mass_Template_IsActive = item.Field<Boolean?>("Mass_Template_IsActive"),

                    }).ToList();

                objDynamic.Add(masstemplate);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}