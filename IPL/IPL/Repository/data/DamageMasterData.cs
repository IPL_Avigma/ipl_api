﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Avigma.Repository.Lib;
using IPL.Models;
using Newtonsoft.Json;

namespace IPL.Repository.data
{
    public class DamageMasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();


        public List<dynamic> AddDamageMasterData(DamageMasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();

            string insertProcedure = "[CreateUpdateDamage_Master]";
            DamageMaster damageMaster = new DamageMaster();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Damage_pkeyID", 1 + "#bigint#" + model.Damage_pkeyID);
                input_parameters.Add("@Damage_Type", 1 + "#varchar#" + model.Damage_Type);
                input_parameters.Add("@Damage_Int", 1 + "#int#" + model.Damage_Int);
                input_parameters.Add("@Damage_Location", 1 + "#varchar#" + model.Damage_Location);
                input_parameters.Add("@Damage_Qty", 1 + "#varchar#" + model.Damage_Qty);
                input_parameters.Add("@Damage_Estimate", 1 + "#decimal#" + model.Damage_Estimate);
                input_parameters.Add("@Damage_Disc", 1 + "#varchar#" + model.Damage_Disc);

                input_parameters.Add("@Damage_Sys_Type", 1 + "#int#" + model.Damage_Sys_Type);

                input_parameters.Add("@Damage_IsActive", 1 + "#bit#" + model.Damage_IsActive);
                input_parameters.Add("@Damage_IsDelete", 1 + "#bit#" + model.Damage_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Damage_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters); 
                if (objData[1] == 0)
                {
                    damageMaster.Damage_pkeyID = "0";
                    damageMaster.Status = "0";
                    damageMaster.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    damageMaster.Damage_pkeyID = Convert.ToString(objData[0]);
                    damageMaster.Status = Convert.ToString(objData[1]);


                }
                objcltData.Add(damageMaster);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;



        }

        private DataSet GetDamageMaster(DamageMasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_DamageMaster]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Damage_pkeyID", 1 + "#bigint#" + model.Damage_pkeyID);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@WhereClause", 1 + "#varchar#" + model.WhereClause);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetDamageMasterDetails(DamageMasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetDamageMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<DamageMasterDTO> DamageMasterDetails =
                   (from item in myEnumerableFeaprd
                    select new DamageMasterDTO
                    {
                        Damage_pkeyID = item.Field<Int64>("Damage_pkeyID"),
                        Damage_Type = item.Field<String>("Damage_Type"),
                        Damage_Int = item.Field<int?>("Damage_Int"),
                        Damage_Location = item.Field<String>("Damage_Location"),
                        Damage_Qty = item.Field<String>("Damage_Qty"),
                        Damage_Estimate = item.Field<Decimal?>("Damage_Estimate"),
                        Damage_Disc = item.Field<String>("Damage_Disc"),
                        Damage_IsActive = item.Field<Boolean?>("Damage_IsActive"),
                        Damage_IntName = item.Field<String>("Damage_IntName"),
                        Damage_CreatedBy = item.Field<String>("Damage_CreatedBy"),
                        Damage_ModifiedBy = item.Field<String>("Damage_ModifiedBy"),
                        Damage_IsDeleteAllow = item.Field<Boolean?>("Damage_IsDeleteAllow"),
                        ViewUrl = null
                    }).ToList();

                objDynamic.Add(DamageMasterDetails);

                if (model.Type == 1)
                {
                    if (ds.Tables.Count > 1)
                    {
                        var myEnumerabledamage = ds.Tables[1].AsEnumerable();
                        List<Filter_Admin_Damage_MasterDTO> Forms_Master_Files =
                           (from item in myEnumerabledamage
                            select new Filter_Admin_Damage_MasterDTO
                            {
                                Damage_Filter_PkeyID = item.Field<Int64>("Damage_Filter_PkeyID"),
                                Damage_Filter_DamageName = item.Field<String>("Damage_Filter_DamageName"),
                                Damage_Filter_DamageIntExt = item.Field<String>("Damage_Filter_DamageIntExt"),
                                Damage_Filter_DamageLocation = item.Field<String>("Damage_Filter_DamageLocation"),
                                Damage_Filter_DamageDesc = item.Field<String>("Damage_Filter_DamageDesc"),
                                Damage_Filter_CreatedBy = item.Field<String>("Damage_Filter_CreatedBy"),
                                Damage_Filter_ModifiedBy = item.Field<String>("Damage_Filter_ModifiedBy"),
                                Damage_Filter_DamageIsActive = item.Field<Boolean?>("Damage_Filter_DamageIsActive")

                            }).ToList();

                        objDynamic.Add(Forms_Master_Files);
                    }
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }



        public List<dynamic> GetDamageFilterDetails(DamageMasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();


            string wherecondition = string.Empty;
            try
            {

                var Data = JsonConvert.DeserializeObject<DamageMasterDTO>(model.FilterData);
                if (!string.IsNullOrEmpty(Data.Damage_Type))
                {
                    //wherecondition = " And Damage_Type =    '" + Data.Damage_Type + "'";
                    wherecondition = wherecondition + " And Damage_Type LIKE '%" + Data.Damage_Type + "%'";
                }
                if (Data.Damage_Int != 0)
                {
                
                        //wherecondition = " And Damage_Int =    '" + Data.Damage_Int + "'";
                        wherecondition = wherecondition + " And Damage_Int LIKE '%" + Data.Damage_Int + "%'";

                    
                }

                if (!string.IsNullOrEmpty(Data.Damage_Location))
                {
                    // wherecondition = " And Damage_Location =    '" + Data.Damage_Location + "'";
                    wherecondition = wherecondition + " And Damage_Location LIKE '%" + Data.Damage_Location + "%'";

                }
                if (!string.IsNullOrEmpty(Data.Damage_Disc))
                {
                    //wherecondition = " And Damage_Disc =    '" + Data.Damage_Disc + "'";
                    wherecondition = wherecondition + " And Damage_Disc LIKE '%" + Data.Damage_Disc + "%'";

                }

                if (Data.Damage_IsActive == true)
                {
                    wherecondition = wherecondition + "  And Damage_IsActive =  '1'";
                }
                if (Data.Damage_IsActive == false)
                {
                    wherecondition = wherecondition + "  And Damage_IsActive =  '0'";
                }


                DamageMasterDTO damageMasterDTO = new DamageMasterDTO();

                damageMasterDTO.WhereClause = wherecondition;
                damageMasterDTO.Type = 3;
                damageMasterDTO.UserID = model.UserID;

                DataSet ds = GetDamageMaster(damageMasterDTO);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<DamageMasterDTO> damagefilter =
                   (from item in myEnumerableFeaprd
                    select new DamageMasterDTO
                    {
                        Damage_pkeyID = item.Field<Int64>("Damage_pkeyID"),
                        Damage_Type = item.Field<String>("Damage_Type"),
                        Damage_Int = item.Field<int?>("Damage_Int"),
                        Damage_Location = item.Field<String>("Damage_Location"),
                        Damage_Qty = item.Field<String>("Damage_Qty"),
                        Damage_Estimate = item.Field<Decimal?>("Damage_Estimate"),
                        Damage_Disc = item.Field<String>("Damage_Disc"),
                        Damage_IsActive = item.Field<Boolean?>("Damage_IsActive"),
                        Damage_IntName = item.Field<String>("Damage_IntName"),
                        Damage_CreatedBy = item.Field<String>("Damage_CreatedBy"),
                        Damage_ModifiedBy = item.Field<String>("Damage_ModifiedBy"),
                        ViewUrl = null

                    }).ToList();

                objDynamic.Add(damagefilter);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }

            return objDynamic;
        }

    }
}