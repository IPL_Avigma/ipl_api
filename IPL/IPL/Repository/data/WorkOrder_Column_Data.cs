﻿using Avigma.Repository.Lib;
using IPL.Models;
using IPLApp.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using IPLApp.Repository.data;

namespace IPL.Repository.data
{
    public class WorkOrder_Column_Data
    {
        Log log = new Log();
        MyDataSourceFactory obj = new MyDataSourceFactory();

        //start

        private List<dynamic> AddUPdateWorkorderColumnJsonData(WorkOrder_Column_Json_DTO model)
        {
            List<dynamic> objdata = new List<dynamic>();


            try
            {
                string insertProcedure = "[CreateUpdate_WorkOrderColumnjson]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@WC_PeyID", 1 + "#bigint#" + model.WC_PeyID);
                input_parameters.Add("@WC_UserId", 1 + "#bigint#" + model.WC_UserId);
                input_parameters.Add("@WC_Right_Column_Json", 1 + "#varchar#" + model.WC_Show_Column_Jsonarr);
                input_parameters.Add("@WC_Left_Column_Json", 1 + "#varchar#" + model.WC_Hide_Column_Jsonarr);
                input_parameters.Add("@WC_Query", 1 + "#varchar#" + model.WC_Query);
                input_parameters.Add("@WC_FilterQuery", 1 + "#varchar#" + model.WC_FilterQuery);
                input_parameters.Add("@WC_IsActive", 1 + "#bit#" + model.WC_IsActive);
                input_parameters.Add("@WC_IsDelete", 1 + "#bit#" + model.WC_IsDelete);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.WC_UserId);
                input_parameters.Add("@Wc_Grid_ShortID", 1 + "#bigint#" + model.Wc_Grid_ShortID);
                input_parameters.Add("@WC_Sort_Data", 1 + "#varchar#" + model.WC_Sort_Data);
                input_parameters.Add("@WC_PeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objdata = obj.SqlCRUD(insertProcedure, input_parameters);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return null;
            }

            return objdata;

        }

        public List<dynamic> AddWorkorderColumnJsonData(WorkOrder_Column_Json_DTO model)
        {
            List<dynamic> objdata = new List<dynamic>();


            try
            {
                List<WorkOrder_Column_DTO> lstrightworkOrder_Column_DTO = new List<WorkOrder_Column_DTO>();
                if (!string.IsNullOrEmpty(model.WC_Show_Column_Jsonarr))
                {
                    lstrightworkOrder_Column_DTO = JsonConvert.DeserializeObject<List<WorkOrder_Column_DTO>>(model.WC_Show_Column_Jsonarr);

                }
                else
                {
                    log.logErrorMessage("------Json Data is Null (model.WC_Show_Column_Jsonarr)------------");
                }

                objdata = AddUPdateWorkorderColumnJsonData(model);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return null;
            }

            return objdata;

        }

        //save filters
        public List<dynamic> SaveFiltersData(WorkOrder_Column_Json_DTO model)
        {
            List<dynamic> objdata = new List<dynamic>();


            try
            {

                WorkOrder_Column_Json_DTO workOrder_Column_Json_DTO = new WorkOrder_Column_Json_DTO();

                if (model.Type == 5)
                {
                    workOrder_Column_Json_DTO.Type = 5;
                }
                else if (model.Type == 7)
                {
                    workOrder_Column_Json_DTO.Type = 7;
                    workOrder_Column_Json_DTO.Wc_Grid_ShortID = model.Wc_Grid_ShortID;
                    workOrder_Column_Json_DTO.UserID = model.UserID;
                    workOrder_Column_Json_DTO.WC_UserId = Convert.ToInt64(model.UserID);
                    workOrder_Column_Json_DTO.WC_Sort_Data = model.WC_Sort_Data;

                }
                else
                {
                    workOrder_Column_Json_DTO.Type = 6;
                }
                workOrder_Column_Json_DTO.UserID = model.UserID;
                workOrder_Column_Json_DTO.WC_UserId = Convert.ToInt64(model.UserID); ;
                workOrder_Column_Json_DTO.WC_Query = model.WhereClause;
                workOrder_Column_Json_DTO.WC_FilterQuery = AddUpdateSaveFilter(model.WhereClause);
                objdata = AddUPdateWorkorderColumnJsonData(workOrder_Column_Json_DTO);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return null;
            }

            return objdata;

        }

        public List<dynamic> SaveFiltersDataNew(WorkOrderFilterDTO model)
        {
            List<dynamic> objdata = new List<dynamic>();

            WorkOrderFilterData workOrderFilterData = new WorkOrderFilterData();
            try
            {
                model.WF_Query = AddUpdateSaveFilter(model.WhereClause);
                objdata = workOrderFilterData.AddUpdateWorkOrderFilterQueryMaster(model);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return null;
            }

            return objdata;

        }
        //end

        private string GetWorkOrderViewQuery(List<WorkOrder_Column_DTO> workOrder_Column_DTO)
        {
            String strQuery = string.Empty, strFrom = string.Empty;

            try
            {
                strQuery = "Select  wo.[workOrder_ID] as 'workOrder_ID' ,wo.[IPLNO] as 'IPLNO' ";

                strFrom = "FROM [dbo].[Status_Master] sm ,[dbo].[WorkOrderMaster]wo  " +
                    "left join [dbo].[IPLState]ST on  ST.IPL_StateID = wo.state " +
                    "where sm.Status_ID = wo.[status] and wo.IsActive = 1 and wo.IsDelete = 0  order by CreatedOn desc";

                if (workOrder_Column_DTO.Count > 1)
                {
                    strQuery = strQuery + " , ";
                    foreach (var item in workOrder_Column_DTO)
                    {
                        strQuery = strQuery + item.Wo_Column_parameter + "  ,";
                    }
                    strQuery = strQuery.Trim().Remove(strQuery.Length - 1);
                    strQuery = strQuery + " " + strFrom;
                }
                else if (workOrder_Column_DTO.Count > 0)
                {
                    strQuery = strQuery + " , ";
                    foreach (var item in workOrder_Column_DTO)
                    {
                        strQuery = strQuery + item.Wo_Column_parameter;
                    }
                    strQuery = strQuery + " " + strFrom;

                }
                else if (workOrder_Column_DTO.Count == 0)
                {
                    strQuery = strQuery + " " + strFrom;
                }
            }
            catch (Exception ex)
            {

                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return strQuery;
        }

        private List<dynamic> UpdateAutoworkorderValues(ActionUpdateWorkOderDTO model)
        {
            List<dynamic> objdata = new List<dynamic>();


            try
            {
                string insertProcedure = "[UpdateAutoworkorderValues]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@workOrder_ID", 1 + "#bigint#" + model.WorkOrder_ID);
                input_parameters.Add("@Sql_Val", 1 + "#nvarchar#" + model.Sql_Val);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);



                objdata = obj.SqlCRUD(insertProcedure, input_parameters);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return null;
            }

            return objdata;

        }

        public List<dynamic> PostWorkOrderActionData(ActionResultsWorkOderDTO model)
        {
            List<dynamic> objdata = new List<dynamic>();
            string strwhere = string.Empty;
            try
            {
                ActionUpdateWorkOderDTO actionUpdateWorkOderDTO = new ActionUpdateWorkOderDTO();
                if (model.Assign_Contractor != 0 || model.Assign_Contractor > 0)
                {
                    strwhere = "  Contractor = " + model.Assign_Contractor;
                }
                else if (model.Assign_Coordinator != 0 || model.Assign_Coordinator > 0)
                {
                    strwhere = "  Cordinator = " + model.Assign_Coordinator;
                }
                else if (model.Assign_Processor != 0 || model.Assign_Processor > 0)
                {
                    strwhere = " Processor = " + model.Assign_Processor;
                }
                else if (model.Client_Company != 0 || model.Client_Company > 0)
                {
                    strwhere = " Company = " + model.Client_Company;
                }
                else if (model.Work_Type != 0 || model.Work_Type > 0)
                {
                    strwhere = " WorkType = " + model.Work_Type;
                }
                else if (!string.IsNullOrEmpty(model.Due_Date))
                {
                    strwhere = "  dueDate =  '" + Convert.ToDateTime(model.Due_Date) + "' ";
                }
                else if (!string.IsNullOrEmpty(model.Start_Date))
                {
                    strwhere = "  startDate = '" + Convert.ToDateTime(model.Start_Date) + "' ";
                }
                else if (!string.IsNullOrEmpty(model.Client_Due_Date))
                {
                    strwhere = "  clientDueDate = '" + Convert.ToDateTime(model.Client_Due_Date) + "' ";
                }
                else if (!string.IsNullOrEmpty(model.Comments))
                {
                    strwhere = " Comments = '" + model.Comments + "' ";
                }
                else if (!string.IsNullOrEmpty(model.Estimated_Date))
                {
                    strwhere = "  EstimatedDate = '" + Convert.ToDateTime(model.Estimated_Date) + "' ";
                }
                else if (model.Category != 0 || model.Category > 0)
                {
                    strwhere = "  Category = " + model.Category;
                }
                actionUpdateWorkOderDTO.Type = model.Type;
                actionUpdateWorkOderDTO.UserId = model.UserId;
                actionUpdateWorkOderDTO.Sql_Val = strwhere;
                var Data = JsonConvert.DeserializeObject<List<workOrderxDTO>>(model.WorkActionArray);
                for (int i = 0; i < Data.Count; i++)
                {
                    actionUpdateWorkOderDTO.WorkOrder_ID = Data[i].workOrder_ID;
                    UpdateAutoworkorderValues(actionUpdateWorkOderDTO);
                }

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return objdata;
            }
            return objdata;
        }

        // privat method for column data 

        private DataSet GetColumnActionData(WorkOrder_Column_DTO work)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Column_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Type", 1 + "#int#" + work.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + work.UserID);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return ds;
        }

        public List<dynamic> GetWorkOrderData(WorkOrder_Column_DTO work)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetColumnActionData(work);

                var myEnumerable = ds.Tables[0].AsEnumerable();
                List<WorkOrder_Column_DTO> WorkOrdercolumn =
                    (from item in myEnumerable
                     select new WorkOrder_Column_DTO
                     {
                         // Wo_Column_PkeyId = item.Field<Int64>("Wo_Column_PkeyId"),
                         Wo_Column_Name = item.Field<String>("Wo_Column_Name"),
                         //Wo_Column_parameter = item.Field<String>("Wo_Column_parameter"),
                         Keydata = item.Field<String>("Keydata"),



                     }).ToList();
                objDynamic.Add(WorkOrdercolumn);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }



        // column json data get 

        private DataSet GetColumnJsonDetailsData(WorkOrder_Column_Json_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[GetCountUserCloumnJson]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@User_pkeyID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);


                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return ds;
        }

        public List<dynamic> GetColumnJsonDetails(WorkOrder_Column_Json_DTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetColumnJsonDetailsData(model);
                int val = 1;
                List<WorkOrder_Column_DTO> lstrightworkOrder_Column_DTO = new List<WorkOrder_Column_DTO>();
                List<WorkOrder_Column_DTO> lstrleftworkOrder_Column_DTO = new List<WorkOrder_Column_DTO>();
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        if (!string.IsNullOrEmpty(ds.Tables[0].Rows[0]["val"].ToString()))
                        {
                            val = Convert.ToInt32(ds.Tables[0].Rows[0]["val"].ToString());
                        }


                    }

                }

                switch (val)
                {

                    case 1:
                        {
                            WorkOrder_Column_Json_DTO workOrder_Column_Json_DTO = new WorkOrder_Column_Json_DTO();


                            if (ds.Tables.Count > 0)
                            {
                                if (ds.Tables[0].Rows.Count > 0)
                                {
                                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                                    {
                                        workOrder_Column_Json_DTO.WC_Right_Column_Json = ds.Tables[0].Rows[i]["WC_Right_Column_Json"].ToString();
                                        workOrder_Column_Json_DTO.WC_Left_Column_Json = ds.Tables[0].Rows[i]["WC_Left_Column_Json"].ToString();



                                    }
                                    lstrleftworkOrder_Column_DTO = JsonConvert.DeserializeObject<List<WorkOrder_Column_DTO>>(workOrder_Column_Json_DTO.WC_Right_Column_Json);
                                    lstrightworkOrder_Column_DTO = JsonConvert.DeserializeObject<List<WorkOrder_Column_DTO>>(workOrder_Column_Json_DTO.WC_Left_Column_Json);
                                    workOrder_Column_Json_DTO.Wc_Grid_ShortID = Convert.ToInt32(ds.Tables[0].Rows[0]["Wc_Grid_ShortID"].ToString());

                                }

                            }

                            objDynamic.Add(lstrleftworkOrder_Column_DTO);
                            objDynamic.Add(lstrightworkOrder_Column_DTO);
                            objDynamic.Add(GetWorkOrderSaveFilter(model));
                            objDynamic.Add(workOrder_Column_Json_DTO);


                            break;

                        }

                    case 2:
                        {
                            var myEnumerableFeaprdc = ds.Tables[0].AsEnumerable();
                            lstrleftworkOrder_Column_DTO =
                               (from item in myEnumerableFeaprdc
                                select new WorkOrder_Column_DTO
                                {

                                    Wo_Column_Name = item.Field<String>("Wo_Column_Name"),
                                    // Wo_Column_parameter = item.Field<String>("Wo_Column_parameter"),
                                    Keydata = item.Field<String>("Keydata"),
                                    // Wc_Grid_ShortID = item.Field<Int64?>("Wc_Grid_ShortID"),


                                }).ToList();
                            objDynamic.Add(lstrleftworkOrder_Column_DTO);
                            objDynamic.Add(lstrightworkOrder_Column_DTO);
                            objDynamic.Add(GetWorkOrderSaveFilter(model));


                            break;
                        }

                }







            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        // get filter data
        public List<dynamic> GetWorkOrderSaveFilter(WorkOrder_Column_Json_DTO work)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {

                work.Type = 1;
                DataSet ds = GetColumnJsonDetailsData(work);

                var myEnumerable = ds.Tables[0].AsEnumerable();
                List<WorkOrder_Column_Json_DTO> WorkOrderAction =
                    (from item in myEnumerable
                     select new WorkOrder_Column_Json_DTO
                     {
                         WC_Query = item.Field<String>("WC_Query"),
                         WC_FilterQuery = item.Field<String>("WC_FilterQuery"),
                         Wc_Grid_ShortID = item.Field<Int64?>("Wc_Grid_ShortID"),

                     }).ToList();
                objDynamic.Add(WorkOrderAction);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        //action workorder data

        public List<dynamic> GetWorkOrderActionData(WorkOrder_Column_DTO work)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {

                work.Type = 2;
                DataSet ds = GetColumnActionData(work);

                var myEnumerable = ds.Tables[0].AsEnumerable();
                List<WorkOrderActionsDTO> WorkOrderAction =
                    (from item in myEnumerable
                     select new WorkOrderActionsDTO
                     {
                         Wo_Column_PkeyId = item.Field<Int64>("Wo_Column_PkeyId"),
                         Wo_Column_Name = item.Field<String>("Wo_Column_Name"),
                         Wo_Column_IsActive = item.Field<Boolean?>("Wo_Column_IsActive"),
                         Auto_Assine = item.Field<Boolean?>("Auto_Assine"),
                         Type = item.Field<int>("Auto_Assine_Type"),
                         Wo_Column_Parent_Id = item.Field<Int64?>("Wo_Column_Parent_Id"),
                         Wo_Sort_order = item.Field<int?>("Wo_Sort_order"),
                     }).ToList();

                var WorkOrderParentAction = WorkOrderAction.Where(w => w.Wo_Column_Parent_Id == 0).ToList().OrderBy(w => w.Wo_Column_Name);
                foreach (var workOrderActions in WorkOrderParentAction)
                {
                    WorkOrderActionDisplayDTO workOrderActionDisplayDTO = new WorkOrderActionDisplayDTO();
                    workOrderActionDisplayDTO.Wo_Column_PkeyId = workOrderActions.Wo_Column_PkeyId;
                    workOrderActionDisplayDTO.Wo_Column_Name = workOrderActions.Wo_Column_Name;
                    workOrderActionDisplayDTO.Wo_Column_IsActive = workOrderActions.Wo_Column_IsActive;
                    workOrderActionDisplayDTO.Auto_Assine = workOrderActions.Auto_Assine;
                    workOrderActionDisplayDTO.Type = workOrderActions.Type;
                    workOrderActionDisplayDTO.Wo_Column_Parent_Id = workOrderActions.Wo_Column_Parent_Id;
                    workOrderActionDisplayDTO.ChildActionList = WorkOrderAction.Where(w => w.Wo_Column_Parent_Id == workOrderActions.Wo_Column_PkeyId).ToList().OrderBy(w => w.Wo_Sort_order).ToList();
                    objDynamic.Add(workOrderActionDisplayDTO);
                }

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }




        public List<dynamic> GetCommonMethodDetails(WorkOrderActionsDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                //for actions
                WorkOrder_Column_DTO workOrder_Column_DTO = new WorkOrder_Column_DTO();
                workOrder_Column_DTO.UserID = model.UserID;
                objDynamic.Add(GetWorkOrderActionData(workOrder_Column_DTO));

                // For Status
                StatusMasterDRD statusMasterDRD = new StatusMasterDRD();
                StatusDetailsDTO statusDetailsDTO = new StatusDetailsDTO();
                statusDetailsDTO.Type = 1;
                statusDetailsDTO.UserID = model.UserID;
                objDynamic.Add(statusMasterDRD.GetStatusMasterDetails(statusDetailsDTO));

                // for Columns
                WorkOrder_Column_Json_DTO workOrder_Column_Json_DTO = new WorkOrder_Column_Json_DTO();
                workOrder_Column_Json_DTO.Type = 1;
                workOrder_Column_Json_DTO.UserID = model.UserID;
                objDynamic.Add(GetColumnJsonDetails(workOrder_Column_Json_DTO));

                //for dropdown
                DropDownData dropDownData = new DropDownData();
                WorkOrderFilterData workOrderFilterData = new WorkOrderFilterData();
                DropDownMasterDTO dropDownMasterDTO = new DropDownMasterDTO();
                dropDownMasterDTO.UserID = model.UserID;
                dropDownMasterDTO.Type = 3;
                dropDownMasterDTO.PageID = 4;
                objDynamic.Add(dropDownData.GetDropdownWorkOrdernew(dropDownMasterDTO)); // Uncomment it when implemented new from Angular
                // for Filter

                dropDownMasterDTO.Type = 1;
                objDynamic.Add(workOrderFilterData.GetWorkOrderFilterQueryMaster(dropDownMasterDTO));
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }


        // Email Template data
        public List<dynamic> GetEmailMetaData(WorkOrder_Column_DTO work)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {

                work.Type = 4;
                DataSet ds = GetColumnActionData(work);

                var myEnumerable = ds.Tables[0].AsEnumerable();
                List<WorkOrder_Column_DTO> EmailMeta =
                    (from item in myEnumerable
                     select new WorkOrder_Column_DTO
                     {
                         Wo_Column_PkeyId = item.Field<Int64>("Wo_Column_PkeyId"),
                         Wo_Column_Name = item.Field<String>("Wo_Column_Name"),
                         Keydata = item.Field<String>("Keydata"),


                     }).ToList();
                objDynamic.Add(EmailMeta);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

            }

            return objDynamic;
        }

        //Add Update save Filter Data
        private String AddUpdateSaveFilter(string WhereClause)
        {
            List<dynamic> objDynamic = new List<dynamic>();


            string wherecondition = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(WhereClause))
                {
                    var Data = JsonConvert.DeserializeObject<List<SaveFilterDTO>>(WhereClause);

                    for (int i = 0; i < Data.Count; i++)
                    {
                        wherecondition = string.Empty;
                        if (!string.IsNullOrEmpty(Data[i].workOrderNumber) && Data[i].workOrderNumber != "0")
                        {
                            wherecondition = wherecondition + " And wo.workOrderNumber =    '" + Data[i].workOrderNumber + "'";
                        }
                        if (!string.IsNullOrEmpty(Data[i].status) && Data[i].status != "0")
                        {
                            wherecondition = wherecondition + " And wo.status =    '" + Data[i].status + "'";
                        }
                        if (!string.IsNullOrEmpty(Data[i].dueDate))
                        {
                            wherecondition = wherecondition + " And wo.dueDate =    '" + Data[i].dueDate + "'";
                        }
                        if (!string.IsNullOrEmpty(Data[i].city) && Data[i].city != "0")
                        {
                            wherecondition = wherecondition + " And wo.city =    '" + Data[i].city + "'";
                        }
                        if (!string.IsNullOrEmpty(Data[i].state) && Data[i].state != "0")
                        {
                            wherecondition = wherecondition + " And wo.state =    '" + Data[i].state + "'";
                        }
                        if (Data[i].zip != null && Data[i].zip != 0)
                        {
                            wherecondition = wherecondition + " And wo.zip =    " + Data[i].zip + "";
                        }
                        if (!string.IsNullOrEmpty(Data[i].IPLNO) && Data[i].IPLNO != "0")
                        {
                            wherecondition = wherecondition + " And wo.IPLNO =    '" + Data[i].IPLNO + "'";
                        }
                        if (!string.IsNullOrEmpty(Data[i].Lotsize) && Data[i].Lotsize != "0")
                        {
                            wherecondition = wherecondition + " And wo.Lotsize =    '" + Data[i].Lotsize + "'";
                        }
                        if (!string.IsNullOrEmpty(Data[i].Lock_Code) && Data[i].Lock_Code != "0")
                        {
                            wherecondition = wherecondition + " And wo.Lock_Code =    '" + Data[i].Lock_Code + "'";
                        }
                        if (!string.IsNullOrEmpty(Data[i].Loan_Number) && Data[i].Loan_Number != "0")
                        {
                            wherecondition = wherecondition + " And wo.Loan_Number =    '" + Data[i].Loan_Number + "'";
                        }
                        if (!string.IsNullOrEmpty(Data[i].Loan_Info) && Data[i].Loan_Info != "0")
                        {
                            wherecondition = wherecondition + " And wo.Loan_Info =    '" + Data[i].Loan_Info + "'";
                        }
                        if (!string.IsNullOrEmpty(Data[i].address1) && Data[i].address1 != "0")
                        {
                            wherecondition = wherecondition + " And dbo.fn_CleanAndTrim(wo.address1) =    dbo.fn_CleanAndTrim('" + Data[i].address1 + "')";
                        }
                        if (!string.IsNullOrEmpty(Data[i].Client) && Data[i].Client != "0")
                        {
                            wherecondition = wherecondition + " And  wo.Company =    '" + Data[i].Client + "'";
                        }

                        if (Data[i].WorkType != 0 && Data[i].WorkType != null)
                        {
                            wherecondition = wherecondition + " And  wo.WorkType =    '" + Data[i].WorkType + "'";
                        }

                        if (Data[i].Cordinator != 0 && Data[i].Cordinator != null)
                        {
                            wherecondition = wherecondition + " And  wo.Cordinator =    '" + Data[i].Cordinator + "'";
                        }

                        if (Data[i].Processor != 0 && Data[i].Processor != null)
                        {
                            wherecondition = wherecondition + " And  wo.Processor =    '" + Data[i].Processor + "'";
                        }

                        if (Data[i].Contractor != 0 && Data[i].Contractor != null)
                        {
                            wherecondition = wherecondition + " And  wo.Contractor =    '" + Data[i].Contractor + "'";
                        }
                        if (!string.IsNullOrEmpty(Data[i].Category) && Data[i].Category != "0")
                        {
                            wherecondition = wherecondition + " And  wo.Category =    '" + Data[i].Category + "'";
                        }

                        if (Data[i].Customer_Number != 0 && Data[i].Customer_Number != null)
                        {
                            wherecondition = wherecondition + " And  wo.Customer_Number =    '" + Data[i].Customer_Number + "'";
                        }
                        if (!string.IsNullOrEmpty(Data[i].Work_Type_Group) && Data[i].Work_Type_Group != "0")
                        {
                            wherecondition = wherecondition + " And  wo.Work_Type_Group =    '" + Data[i].Work_Type_Group + "'";
                        }
                        if (!string.IsNullOrEmpty(Data[i].County) && Data[i].County != "0")
                        {
                            wherecondition = wherecondition + " And  wo.County =    '" + Data[i].County + "'";
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                log.logErrorMessage("AddUpdateSaveFilter............ " + WhereClause);
            }

            return wherecondition;
        }

        public List<dynamic> GetReportWorkOrderActionData(WorkOrder_Column_DTO work)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {

                work.Type = 5;
                DataSet ds = GetColumnActionData(work);

                var myEnumerable = ds.Tables[0].AsEnumerable();
                List<WorkOrder_Column_DTO> WorkOrderAction =
                    (from item in myEnumerable
                     select new WorkOrder_Column_DTO
                     {
                         Wo_Column_PkeyId = item.Field<Int64>("Wo_Column_PkeyId"),
                         Wo_Column_Name = item.Field<String>("Wo_Column_Name"),
                         Keydata = item.Field<String>("Keydata"),

                     }).ToList();

                objDynamic.Add(WorkOrderAction);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

    }
}