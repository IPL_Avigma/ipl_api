﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class Folder_Auto_Assine_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        public List<dynamic> AddFolderAutoAssineMasterjson(Folder_Auto_Assine_Master_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_Folder_Auto_Assine_Master]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Fold_Auto_Assine_PkeyId", 1 + "#bigint#" + model.Fold_Auto_Assine_PkeyId);
                input_parameters.Add("@Fold_Auto_Assine_Client", 1 + "#varchar#" + model.Fold_Auto_Assine_Client);
                input_parameters.Add("@Fold_Auto_Assine_Customer", 1 + "#varchar#" + model.Fold_Auto_Assine_Customer);
                input_parameters.Add("@Fold_Auto_Assine_LoneType", 1 + "#varchar#" + model.Fold_Auto_Assine_LoneType);
                input_parameters.Add("@Fold_Auto_Assine_WorkType", 1 + "#varchar#" + model.Fold_Auto_Assine_WorkType);
                input_parameters.Add("@Fold_Auto_Assine_WorkType_Group", 1 + "#varchar#" + model.Fold_Auto_Assine_WorkType_Group);
                input_parameters.Add("@Fold_Auto_Assine_State", 1 + "#varchar#" + model.Fold_Auto_Assine_State);
                input_parameters.Add("@Fold_Auto_Assine_County", 1 + "#varchar#" + model.Fold_Auto_Assine_County);
                input_parameters.Add("@Fold_Auto_Assine_Zip", 1 + "#bigint#" + model.Fold_Auto_Assine_Zip);
                input_parameters.Add("@Fold_Parent_Id", 1 + "#bigint#" + model.Fold_Parent_Id);
                input_parameters.Add("@Fold_Auto_Assine_IsActive", 1 + "#bit#" + model.Fold_Auto_Assine_IsActive);
                input_parameters.Add("@Fold_Auto_Assine_IsDelete", 1 + "#bit#" + model.Fold_Auto_Assine_IsDelete);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Fold_Auto_Assine_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);
                input_parameters.Add("@Folder_File_Master_FK_Id", 1 + "#bigint#" + model.Folder_File_Master_FK_Id);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;



        }
    }
}