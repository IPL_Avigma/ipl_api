﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class Filter_Admin_TaskData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        public List<dynamic> AddUpdate_Filter_Admin_Task(Filter_Admin_Task_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_Filter_Admin_Task_Master]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Task_Filter_PkeyID", 1 + "#bigint#" + model.Task_Filter_PkeyID);
                input_parameters.Add("@Task_Filter_TaskName", 1 + "#nvarchar#" + model.Task_Filter_TaskName);
                input_parameters.Add("@Task_Filter_TaskType", 1 + "#int#" + model.Task_Filter_TaskType);
                input_parameters.Add("@Task_Filter_TaskPhName", 1 + "#nvarchar#" + model.Task_Filter_TaskPhName);
                input_parameters.Add("@Task_Filter_TaskIsActive", 1 + "#bit#" + model.Task_Filter_TaskIsActive);
                input_parameters.Add("@Task_Filter_IsActive", 1 + "#bit#" + model.Task_Filter_IsActive);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserId);
                input_parameters.Add("@Task_Filter_PkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        private DataSet Get_Filter_Admin_Task(Filter_Admin_Task_MasterDTO model)
        {
            DataSet ds = null;
            try
            {

                string selectProcedure = "[Get_Filter_Admin_Task_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Task_Filter_PkeyID", 1 + "#bigint#" + model.Task_Filter_PkeyID);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> Get_Filter_Admin_TaskDetails(Filter_Admin_Task_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = Get_Filter_Admin_Task(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Filter_Admin_Task_MasterDTO> Forms_Master_Files =
                   (from item in myEnumerableFeaprd
                    select new Filter_Admin_Task_MasterDTO
                    {
                        Task_Filter_PkeyID = item.Field<Int64>("Task_Filter_PkeyID"),
                        Task_Filter_TaskName = item.Field<String>("Task_Filter_TaskName"),
                        Task_Filter_TaskType = item.Field<Int32>("Task_Filter_TaskType"),
                        Task_Filter_TaskPhName = item.Field<String>("Task_Filter_TaskPhName"),
                        Task_Filter_TaskIsActive = item.Field<Boolean?>("Task_Filter_TaskIsActive")

                    }).ToList();

                objDynamic.Add(Forms_Master_Files);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}