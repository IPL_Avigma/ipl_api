﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class PCR_Cyprexx_Grass_Checklist_Data
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        public List<dynamic> PostCyprexxGrassCheckData(PCR_Cyprexx_Grass_Checklist_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_PCR_Cyprexx_Grass_Checklist]";
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@CG_PkeyID", 1 + "#bigint#" + model.CG_PkeyID);
                input_parameters.Add("@CG_WO_Id", 1 + "#bigint#" + model.CG_WO_Id);
                input_parameters.Add("@CG_Property_Maintenance", 1 + "#nvarchar#" + model.CG_Property_Maintenance);
                input_parameters.Add("@CG_General_Property_Info", 1 + "#nvarchar#" + model.CG_General_Property_Info);
                input_parameters.Add("@CG_Pool_Information", 1 + "#nvarchar#" + model.CG_Pool_Information);
                input_parameters.Add("@CG_Utilities", 1 + "#nvarchar#" + model.CG_Utilities);
                input_parameters.Add("@CG_Recommended_Services", 1 + "#nvarchar#" + model.CG_Recommended_Services);
                input_parameters.Add("@CG_General_Comments", 1 + "#nvarchar#" + model.CG_General_Comments);
                input_parameters.Add("@CG_Order_Completion", 1 + "#nvarchar#" + model.CG_Order_Completion);
                input_parameters.Add("@CG_IsActive", 1 + "#bit#" + model.CG_IsActive);
                input_parameters.Add("@CG_IsDelete", 1 + "#bit#" + model.CG_IsDelete);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@fwo_pkyeId", 1 + "#bigint#" + model.fwo_pkyeId);
                input_parameters.Add("@CG_PkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);
                objData = obj.SqlCRUD(insertProcedure, input_parameters);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        private DataSet GetCyprexxGrassCheckListData(PCR_Cyprexx_Grass_Checklist_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_PCR_Cyprexx_Grass_Checklist]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@CG_PkeyID", 1 + "#bigint#" + model.CG_PkeyID);
                input_parameters.Add("@CG_WO_Id", 1 + "#bigint#" + model.CG_WO_Id);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetCyprexxGrassCheckListDetails(PCR_Cyprexx_Grass_Checklist_DTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetCyprexxGrassCheckListData(model);


                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                }

                //var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                //List<PCR_Cyprexx_Grass_Checklist_DTO> cyprexxGrassdata =
                //   (from item in myEnumerableFeaprd
                //    select new PCR_Cyprexx_Grass_Checklist_DTO
                //    {
                //        CG_PkeyID = item.Field<Int64>("CG_PkeyID"),
                //        CG_WO_Id = item.Field<Int64>("CG_WO_Id"),
                //        CG_CompanyID = item.Field<Int64>("CG_CompanyID"),
                //        CG_Property_Maintenance = item.Field<String>("CG_Property_Maintenance"),
                //        CG_General_Property_Info = item.Field<String>("CG_General_Property_Info"),
                //        CG_General_Comments = item.Field<String>("CG_General_Comments"),
                //        CG_Utilities = item.Field<String>("CG_Utilities"),
                //        CG_Order_Completion = item.Field<String>("CG_Order_Completion"),
                //        CG_Pool_Information = item.Field<String>("CG_Pool_Information"),
                //        CG_Recommended_Services = item.Field<String>("CG_Recommended_Services"),
                //        CG_IsActive = item.Field<Boolean>("CG_IsActive"),
                //        CG_IsDelete = item.Field<Boolean>("CG_IsDelete"),
                //    }).ToList();

                //objDynamic.Add(cyprexxGrassdata);

                //if (ds.Tables.Count > 1)
                //{
                //    var pcrHistory = ds.Tables[1].AsEnumerable();
                //    List<PCR_Cyprexx_Grass_Checklist_DTO> cyprexxGrassdata_history =
                //       (from item in pcrHistory
                //        select new PCR_Cyprexx_Grass_Checklist_DTO
                //        {
                //            CG_PkeyID = item.Field<Int64>("CG_PkeyID"),
                //            CG_WO_Id = item.Field<Int64>("CG_WO_Id"),
                //            CG_CompanyID = item.Field<Int64>("CG_CompanyID"),
                //            CG_Property_Maintenance = item.Field<String>("CG_Property_Maintenance"),
                //            CG_General_Property_Info = item.Field<String>("CG_General_Property_Info"),
                //            CG_General_Comments = item.Field<String>("CG_General_Comments"),
                //            CG_Utilities = item.Field<String>("CG_Utilities"),
                //            CG_Order_Completion = item.Field<String>("CG_Order_Completion"),
                //            CG_Pool_Information = item.Field<String>("CG_Pool_Information"),
                //            CG_Recommended_Services = item.Field<String>("CG_Recommended_Services"),
                //            CG_IsActive = item.Field<Boolean>("CG_IsActive"),
                //            CG_IsDelete = item.Field<Boolean>("CG_IsDelete"),
                //        }).ToList();

                //    objDynamic.Add(cyprexxGrassdata_history);

                //}
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}