﻿using Avigma.Repository.Lib;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using IPLApp.Models;
using IPL.Repository.data;
using IPL.Models;
namespace IPL.Repository.data
{
    public class Filter_Admin_Form_Master_Data
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        SecurityHelper securityHelper = new SecurityHelper();

        private List<dynamic> CreateUpdate_Filter_Admin_Form_Master(Filter_Admin_Form_Master_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_Filter_Admin_Form_Master]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Form_Filter_PkeyId", 1 + "#bigint#" + model.Form_Filter_PkeyId);
                input_parameters.Add("@Form_Filter_FormName", 1 + "#varchar#" + model.Form_Filter_FormName);
                input_parameters.Add("@Form_Filter_FormIsActive", 1 + "#bit#" + model.Form_Filter_FormIsActive);
                input_parameters.Add("@Form_Filter_FormIsRequired", 1 + "#bit#" + model.Form_Filter_FormIsRequired);
                input_parameters.Add("@Form_Filter_FormIsFieldResult", 1 + "#bit#" + model.Form_Filter_FormIsFieldResult);
                input_parameters.Add("@Form_Filter_FormIsOfficeResult", 1 + "#bit#" + model.Form_Filter_FormIsOfficeResult);
                input_parameters.Add("@Form_Filter_FormIsPublished", 1 + "#bit#" + model.Form_Filter_FormIsPublished);
                input_parameters.Add("@Form_Filter_FormAddedby", 1 + "#bigint#" + model.Form_Filter_FormAddedby);
                input_parameters.Add("@Form_Filter_FormVersion", 1 + "#int#" + model.Form_Filter_FormVersion);
                input_parameters.Add("@Form_Filter_Where", 1 + "#nvarchar#" + model.Form_Filter_Where);
                input_parameters.Add("@Form_Filter_IsActive", 1 + "#bit#" + model.Form_Filter_IsActive);
                input_parameters.Add("@Form_Filter_IsDelete", 1 + "#bit#" + model.Form_Filter_IsDelete);
                input_parameters.Add("@Form_Filter_CompanyID", 1 + "#bigint#" + model.Form_Filter_CompanyID);
                input_parameters.Add("@Form_Filter_UserID", 1 + "#bigint#" + model.Form_Filter_UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Form_Filter_Pkey_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);
                objData = obj.SqlCRUD(insertProcedure, input_parameters);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;



        }

        private DataSet Get_Filter_Admin_Form_Master(Filter_Admin_Form_Master_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Filter_Admin_Form_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Form_Filter_PkeyId", 1 + "#bigint#" + model.Form_Filter_PkeyId);

                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }



            return ds;
        }
        public string CreateWhereClause(Filter_Admin_Form_Master_DTO model)
        {
            string strWhere = string.Empty;
            try
            {
                if (!string.IsNullOrWhiteSpace(model.Form_Filter_FormName))
                {
                    strWhere = "  AND dbo.fn_CleanAndTrim(formmst.FormName) Like  dbo.fn_CleanAndTrim('%" + model.Form_Filter_FormName + "%')";
                }
                if (model.Form_Filter_FormIsRequired == true)
                {
                    strWhere = strWhere + "  And formmst.IsRequired = 1";
                }
                if (model.Form_Filter_FormIsPublished == true)
                {
                    strWhere = strWhere + "  And formmst.Form_IsPublished = 1";
                }

                if (model.Form_Filter_FormIsOfficeResult == true)
                {
                    strWhere = strWhere + "  And formmst.OfficeResults = 1";
                }

                if (model.Form_Filter_FormIsFieldResult == true)
                {
                    strWhere = strWhere + "  And formmst.FieldResults = 1";
                }

                if (model.Form_Filter_FormIsActive == true)
                {
                    strWhere = strWhere + "  And formmst.Form_IsVisible = 1";
                }

                if (model.Form_Filter_FormVersion != null)
                {
                    strWhere = strWhere + "  And formmst.Form_Version_No = " + model.Form_Filter_FormVersion;
                }
                if (model.Form_Filter_FormAddedby != null)
                {
                    strWhere = strWhere + "  And formmst.Form_CreatedBy = " + model.Form_Filter_FormAddedby;
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return strWhere;

        }

        public List<dynamic> CreateUpdate_Filter_Admin_Form_Master_DataDetails(Filter_Admin_Form_Master_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                if(model.Type ==1)
                {
                    model.Form_Filter_IsActive = true;
                    model.Form_Filter_IsDelete = false;
                    model.Form_Filter_Where = CreateWhereClause(model);
                }
               
                objData = CreateUpdate_Filter_Admin_Form_Master(model);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }
            return objData;
        }


        public List<dynamic> Get_Filter_Admin_Form_MasterDetails(Filter_Admin_Form_Master_DTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {

                DataSet ds = Get_Filter_Admin_Form_Master(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Filter_Admin_Form_Master_DTO> Get_details =
                   (from item in myEnumerableFeaprd
                    select new Filter_Admin_Form_Master_DTO
                    {
                        Form_Filter_PkeyId = item.Field<Int64>("Form_Filter_PkeyId"),
                        Form_Filter_FormName = item.Field<String>("Form_Filter_FormName"),
                        Form_Filter_FormIsActive = item.Field<Boolean?>("Form_Filter_FormIsActive"),
                        Form_Filter_FormIsRequired = item.Field<Boolean?>("Form_Filter_FormIsRequired"),
                        Form_Filter_FormIsFieldResult = item.Field<Boolean?>("Form_Filter_FormIsFieldResult"),
                        Form_Filter_FormIsOfficeResult = item.Field<Boolean?>("Form_Filter_FormIsOfficeResult"),
                        Form_Filter_FormIsPublished = item.Field<Boolean?>("Form_Filter_FormIsPublished"),
                        Form_Filter_FormAddedby = item.Field<Int64?>("Form_Filter_FormAddedby"),
                        Form_Filter_IsActive = item.Field<Boolean?>("Form_Filter_IsActive"),
                        Form_Filter_CompanyID = item.Field<Int64?>("Form_Filter_CompanyID"),
                        Form_Filter_UserID = item.Field<Int64?>("Form_Filter_UserID"),

                    }).ToList();

                objDynamic.Add(Get_details);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }

            return objDynamic;





        }
    }
}