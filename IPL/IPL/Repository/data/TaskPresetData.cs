﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class TaskPresetData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public List<dynamic> AddTaskPresetChildData(TaskPresetDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
           
            string insertProcedure = "[CreateUpdateTaskPresetMaster]";
           
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();


            try
            {
                input_parameters.Add("@Task_Preset_pkeyId", 1 + "#bigint#" + model.Task_Preset_pkeyId);
                input_parameters.Add("@Task_Preset_ID", 1 + "#bigint#" + model.Task_Preset_ID);
                input_parameters.Add("@Task_Preset_Text", 1 + "#varchar#" + model.Task_Preset_Text);
                input_parameters.Add("@Task_Preset_IsActive", 1 + "#bit#" + model.Task_Preset_IsActive);
                input_parameters.Add("@Task_Preset_IsDelete", 1 + "#bit#" + model.Task_Preset_IsDelete);
                input_parameters.Add("@Task_Preset_IsDefault", 1 + "#bit#" + model.Task_Preset_IsDefault);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Task_Preset_pkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
               
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objData;

        }

        private DataSet GetTaskPresetChildMaster(TaskPresetDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_TaskPresetMaster]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Task_Preset_pkeyId", 1 + "#bigint#" + model.Task_Preset_pkeyId);
                input_parameters.Add("@Task_Preset_ID", 1 + "#bigint#" + model.Task_Preset_ID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetTaskPresetChildDetails(TaskPresetDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetTaskPresetChildMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<TaskPresetDTO> TaskpresetDetails =
                   (from item in myEnumerableFeaprd
                    select new TaskPresetDTO
                    {
                        Task_Preset_pkeyId = item.Field<Int64>("Task_Preset_pkeyId"),
                        Task_Preset_ID = item.Field<Int64>("Task_Preset_ID"),
                        Task_Preset_Text = item.Field<String>("Task_Preset_Text"),
                        Task_Preset_IsActive = item.Field<Boolean?>("Task_Preset_IsActive"),
                        Task_Preset_IsDefault=item.Field<Boolean?>("Task_Preset_IsDefault"),


                    }).ToList();

                objDynamic.Add(TaskpresetDetails);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        public List<TaskPresetDTO> GetTaskPresetChildDetails(Int64 Task_Preset_ID)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                TaskPresetDTO taskPresetDTO = new TaskPresetDTO();

                taskPresetDTO.Task_Preset_ID = Task_Preset_ID;
                taskPresetDTO.Type = 2;
                taskPresetDTO.Task_Preset_pkeyId = 0;

                DataSet ds = GetTaskPresetChildMaster(taskPresetDTO);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<TaskPresetDTO> TaskpresetDetails =
                   (from item in myEnumerableFeaprd
                    select new TaskPresetDTO
                    {
                        Task_Preset_pkeyId = item.Field<Int64>("Task_Preset_pkeyId"),
                        Task_Preset_ID = item.Field<Int64>("Task_Preset_ID"),
                        Task_Preset_Text = item.Field<String>("Task_Preset_Text"),
                        Task_Preset_IsActive = item.Field<Boolean?>("Task_Preset_IsActive"),
                        Task_Preset_IsDefault = item.Field<Boolean?>("Task_Preset_IsDefault"),

                    }).ToList();

               return TaskpresetDetails;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return null;
        }
    }
}