﻿using Avigma.Repository.Lib;
using IPL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace IPL.Repository.data
{
    public class TaskGroupMaster
    {

        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        //Add app Company Details
        public List<dynamic> AddTaskGroup(TaskGroupMasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_TaskGroup]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Task_Group_pkeyID", 1 + "#bigint#" + model.Task_Group_pkeyID);
                input_parameters.Add("@Task_Group_Name", 1 + "#nvarchar#" + model.Task_Group_Name);
                input_parameters.Add("@Task_Group_Client_data", 1 + "#nvarchar#" + model.Task_Group_Client_data);
                input_parameters.Add("@Task_Group_IsActive", 1 + "#bit#" + model.Task_Group_IsActive);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Task_Group_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);

                objcltData.Add(objData);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;



        }

        public List<dynamic> AddTaskGroupData(TaskGroupMasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();
            TaskGroupMasterDTO taskGroupMasterDTO = new TaskGroupMasterDTO();
            TaskGroup_Client_RelationshipData taskGroup_Client_RelationshipData = new TaskGroup_Client_RelationshipData();
            try
            {
                if (model.Task_GroupNameArr != null)
                {
                    for (int i = 0; i < model.Task_GroupNameArr.Count; i++)
                    {
                        taskGroupMasterDTO.Task_Group_pkeyID = model.Task_GroupNameArr[i].Task_Group_pkeyID;
                        taskGroupMasterDTO.Task_Group_Name = model.Task_GroupNameArr[i].Task_Group_Name;
                        taskGroupMasterDTO.Task_Group_IsActive = model.Task_Group_IsActive;
                        taskGroupMasterDTO.UserID = model.UserID;

                        if (model.Task_GroupNameArr[i].Task_Group_pkeyID == 0)
                        {
                            taskGroupMasterDTO.Type = 1;
                        }
                        else
                        {
                            taskGroupMasterDTO.Type = 2;

                        }

                        var xxx = AddTaskGroup(taskGroupMasterDTO);

                        if (!string.IsNullOrWhiteSpace(model.Task_GroupNameArr[i].Task_Group_Client_data))
                        {
                            var Data = JsonConvert.DeserializeObject<List<Task_GroupClient>>(model.Task_GroupNameArr[i].Task_Group_Client_data);
                            for (int k = 0; k < Data.Count; k++)
                            {
                                TaskGroup_Client_RelationshipDTO taskGroup_Client_RelationshipDTO = new TaskGroup_Client_RelationshipDTO();
                                taskGroup_Client_RelationshipDTO.TGC_ClientID = Data[k].Client_pkeyID;
                                taskGroup_Client_RelationshipDTO.TGC_GroupID = xxx[0][0];
                                taskGroup_Client_RelationshipDTO.Type = 1;
                                taskGroup_Client_RelationshipDTO.UserID = model.UserID;
                                taskGroup_Client_RelationshipData.CreateUpdate_TaskGroupClientRelationshipDetails(taskGroup_Client_RelationshipDTO);
                            }

                        }

                    }
                }
                else
                {
                    taskGroupMasterDTO.Task_Group_pkeyID = model.Task_Group_pkeyID;
                    taskGroupMasterDTO.Task_Group_Name = model.Task_Group_Name;
                    taskGroupMasterDTO.Task_Group_Client_data = model.Task_Group_Client_data;
                    taskGroupMasterDTO.Task_Group_IsActive = model.Task_Group_IsActive;
                    taskGroupMasterDTO.UserID = model.UserID;

                    if (model.Task_Group_pkeyID == 0)
                    {
                        taskGroupMasterDTO.Type = 1;
                    }
                    else
                    {
                        taskGroupMasterDTO.Type = model.Type;

                    }

                    var xxx = AddTaskGroup(taskGroupMasterDTO);

                    if (!string.IsNullOrWhiteSpace(model.Task_Group_Client_data))
                    {
                        var Data = JsonConvert.DeserializeObject<List<Task_GroupClient>>(model.Task_Group_Client_data);
                        for (int k = 0; k < Data.Count; k++)
                        {
                            TaskGroup_Client_RelationshipDTO taskGroup_Client_RelationshipDTO = new TaskGroup_Client_RelationshipDTO();
                            taskGroup_Client_RelationshipDTO.TGC_ClientID = Data[k].Client_pkeyID;
                            taskGroup_Client_RelationshipDTO.TGC_GroupID = xxx[0][0];
                            taskGroup_Client_RelationshipDTO.Type = 1;
                            taskGroup_Client_RelationshipDTO.UserID = model.UserID;
                            taskGroup_Client_RelationshipDTO.TGC_IsActive = true;
                            taskGroup_Client_RelationshipData.CreateUpdate_TaskGroupClientRelationshipDetails(taskGroup_Client_RelationshipDTO);
                        }

                    }

                }



            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objcltData;
        }

        private DataSet GetTaskGroupMaster(TaskGroupMasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_TaskGroup]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Task_Group_pkeyID", 1 + "#bigint#" + model.Task_Group_pkeyID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }
        //Get Task group list 
        public List<dynamic> GetAppCompanyMasterDetails(TaskGroupMasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetTaskGroupMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<TaskGroupMasterDTO> AppClientDetails =
                   (from item in myEnumerableFeaprd
                    select new TaskGroupMasterDTO
                    {
                        Task_Group_pkeyID = item.Field<Int64>("Task_Group_pkeyID"),
                        Task_Group_Name = item.Field<String>("Task_Group_Name"),
                        Task_Group_IsActive = item.Field<Boolean?>("Task_Group_IsActive"),
                        //Client_Company_Name = item.Field<String>("Client_Company_Name"),
                        Task_Group_Client_data = item.Field<String>("Task_Group_Client_data"),
                        Task_Group_IsDeleteAllow = item.Field<bool>("Task_Group_IsDeleteAllow"),
                        Task_Group_CreatedBy = item.Field<String>("Task_Group_CreatedBy"),
                        Task_Group_ModifiedBy = item.Field<String>("Task_Group_ModifiedBy"),
                    }).ToList();

                objDynamic.Add(AppClientDetails);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }








    }
}