﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class MSI_GrassPcr_Forms_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        public List<dynamic> AddMsiGrassPCRCFORMSMasterData(MSI_GrassPcr_Forms_Master_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objclntData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_MSI_GrassPCR_FORMS_MASTER]";
            //MSI_GrassPcr_Forms_Master_DTO mSI_GrassPcr_Forms_Master = new MSI_GrassPcr_Forms_Master_DTO();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@MSI_Grass_PkeyId", 1 + "#bigint#" + model.MSI_Grass_PkeyId);
                input_parameters.Add("@MSI_Grass_WO_Id", 1 + "#bigint#" + model.MSI_Grass_WO_Id);
                input_parameters.Add("@MSI_Grass_CompanyID", 1 + "#bigint#" + model.MSI_Grass_CompanyID);
                input_parameters.Add("@MSI_Grass_SubjectProperty", 1 + "#nvarchar#" + model.MSI_Grass_SubjectProperty);
                input_parameters.Add("@MSI_Grass_ConditionReport", 1 + "#nvarchar#" + model.MSI_Grass_ConditionReport);
                input_parameters.Add("@MSI_Grass_BidItems", 1 + "#nvarchar#" + model.MSI_Grass_BidItems);
                input_parameters.Add("@MSI_Grass_PhotoManager", 1 + "#nvarchar#" + model.MSI_Grass_PhotoManager);
                input_parameters.Add("@MSI_Grass_Comments", 1 + "#nvarchar#" + model.MSI_Grass_Comments);
                input_parameters.Add("@MSI_Grass_FinalReviews", 1 + "#nvarchar#" + model.MSI_Grass_FinalReviews);
                input_parameters.Add("@MSI_Grass_IsActive", 1 + "#bit#" + model.MSI_Grass_IsActive);
                input_parameters.Add("@MSI_Grass_IsDelete", 1 + "#bit#" + model.MSI_Grass_IsDelete);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@fwo_pkyeId", 1 + "#bigint#" + model.fwo_pkyeId);
                input_parameters.Add("@MSI_Grass_Pkey_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        private DataSet GetMsiGrassPcrFORMS_Master(MSI_GrassPcr_Forms_Master_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_MSI_GrassPCR_FORMS_MASTER]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@MSI_Grass_PkeyId", 1 + "#bigint#" + model.MSI_Grass_PkeyId);
                input_parameters.Add("@MSI_Grass_WO_Id", 1 + "#bigint#" + model.MSI_Grass_WO_Id);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetMsiGrassPcrFORMSMasterDetails(MSI_GrassPcr_Forms_Master_DTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetMsiGrassPcrFORMS_Master(model);

                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                }

                //var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                //List<MSI_GrassPcr_Forms_Master_DTO> _GrassPcr_Forms_Master_DTOs =
                //   (from item in myEnumerableFeaprd
                //    select new MSI_GrassPcr_Forms_Master_DTO
                //    {
                //        MSI_Grass_PkeyId = item.Field<Int64>("MSI_Grass_PkeyId"),
                //        MSI_Grass_WO_Id = item.Field<Int64>("MSI_Grass_WO_Id"),
                //        MSI_Grass_CompanyID = item.Field<Int64>("MSI_Grass_CompanyID"),
                //        MSI_Grass_SubjectProperty = item.Field<String>("MSI_Grass_SubjectProperty"),
                //        MSI_Grass_ConditionReport = item.Field<String>("MSI_Grass_ConditionReport"),
                //        MSI_Grass_BidItems = item.Field<String>("MSI_Grass_BidItems"),
                //        MSI_Grass_PhotoManager = item.Field<String>("MSI_Grass_PhotoManager"),
                //        MSI_Grass_Comments = item.Field<String>("MSI_Grass_Comments"),
                //        MSI_Grass_FinalReviews = item.Field<String>("MSI_Grass_FinalReviews"),

                //        MSI_Grass_IsActive = item.Field<Boolean>("MSI_Grass_IsActive"),
                //        MSI_Grass_IsDelete = item.Field<Boolean>("MSI_Grass_IsDelete"),
                //    }).ToList();

                //objDynamic.Add(_GrassPcr_Forms_Master_DTOs);

                //if (ds.Tables.Count > 1)
                //{
                //    var pcrHistory = ds.Tables[1].AsEnumerable();
                //    List<MSI_GrassPcr_Forms_Master_DTO> _pcrHistory =
                //       (from item in pcrHistory
                //        select new MSI_GrassPcr_Forms_Master_DTO
                //        {
                //            MSI_Grass_PkeyId = item.Field<Int64>("MSI_Grass_PkeyId"),
                //            MSI_Grass_WO_Id = item.Field<Int64>("MSI_Grass_WO_Id"),
                //            MSI_Grass_CompanyID = item.Field<Int64>("MSI_Grass_CompanyID"),
                //            MSI_Grass_SubjectProperty = item.Field<String>("MSI_Grass_SubjectProperty"),
                //            MSI_Grass_ConditionReport = item.Field<String>("MSI_Grass_ConditionReport"),
                //            MSI_Grass_BidItems = item.Field<String>("MSI_Grass_BidItems"),
                //            MSI_Grass_PhotoManager = item.Field<String>("MSI_Grass_PhotoManager"),
                //            MSI_Grass_Comments = item.Field<String>("MSI_Grass_Comments"),
                //            MSI_Grass_FinalReviews = item.Field<String>("MSI_Grass_FinalReviews"),
                //            MSI_Grass_IsActive = item.Field<Boolean>("MSI_Grass_IsActive"),
                //            MSI_Grass_IsDelete = item.Field<Boolean>("MSI_Grass_IsDelete"),
                //        }).ToList();

                //    objDynamic.Add(_pcrHistory);
                //}
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}