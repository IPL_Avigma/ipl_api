﻿using Avigma.Repository.Lib;
using IPL.Models;
using iTextSharp.text;
using iTextSharp.text.pdf;
using NReco.PdfGenerator;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace IPL.Repository.data
{
    public class ContractorClientPrintData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        private DataSet GetContractorPrintData(ContractorInvoicePrintDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Contractor_Invoice_Print]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Inv_Con_Wo_ID", 1 + "#bigint#" + model.Inv_Con_Wo_ID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);

                ds = obj.SelectSql(selectProcedure, input_parameters);

            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }
        public static byte[] GenerateRuntimePDF(string html)
        {
            #region NReco.PdfGenerator
            HtmlToPdfConverter nRecohtmltoPdfObj = new HtmlToPdfConverter();
            return nRecohtmltoPdfObj.GeneratePdf(html);
            #endregion

        }
        public async Task<CustomReportPDF> GeneratePDFContractorInvoicePrint(ContractorInvoicePrintDTO model)
        {
            CustomReportPDF custom = new CustomReportPDF();
            try
            {
                StringBuilder html = new StringBuilder();
                StringBuilder table = new StringBuilder();
                string theader = "";
                string tfooter = "";
                int index = 0;
                string tbody = "<tbody>";
                decimal tdata = 0;
                var reportList = pdfGetContractorInvoicePrintDetail(model);

                theader += "<table width = '100%' border='1' cellpadding='1' cellspacing='0' bgcolor='#efefef'>";
                table.Append(theader);

                //tbody += "<tr> <td colspan='2' bgcolor = '#CCCCCC' style='text-align:center;height:50px;'><span class='MsoNormal1' style='text-align:center; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px'><b><u> </u></b></span>  <div align='center;' style='font - family: Verdana, Arial, Helvetica, sans - serif; font - size: 10px; '><span class='MsoNormal1' style='text - align:center; font - size: 35px'><b><u>INVOICE</u></b></span></div> </td></tr>";
                if (reportList[2] != null)
                {
                    var wodata = reportList[2];
                    foreach (var wodetails in wodata)
                    {
                        tbody += "<tr><td bgcolor = '#CCCCCC' style = 'text-align:left; border: none; width:75px;' > <img src =" + wodetails.YR_Company_App_logo + " style = 'width:50px; height:50px; margin-top:1px; margin-left:1px; border: none;' /> </td> <td colspan='1' bgcolor = '#CCCCCC' style='text-align:left;height:50px;  border: none;'><span class='MsoNormal1' style='text-align:center; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px'><b><u> </u></b></span>  <div align='center;' style='font - family: Verdana, Arial, Helvetica, sans - serif; font - size: 10px; '>    <span class='MsoNormal1' style='text - align:center; font - size: 35px'><b><u>INVOICE</u></b></span></div> </td></tr>";

                        tbody += "<tr  height='118px'>   <td bgcolor='#FFFFFF' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px; width: 50%;'>  <strong >" + wodetails.Cont_Name + "</strong> <br />" + wodetails.ContractorAddress + "<br/>" + wodetails.ConState + "<br/>" + wodetails.ConZip + " </td> <td bgcolor='#FFFFFF' valign = 'top' colspan = '2' style = 'font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px;' > <table width = '100%' border = '0' cellpadding = '4'> <tr><td width = '33%' style = 'text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px; font-weight: bold;' height = '22' bgcolor = '#CCCCCC' > Invoice Number </td>  <td width = '33%' style = 'text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px; font-weight: bold;'  height = '22' bgcolor = '#CCCCCC'> Invoice Date </td> </tr>";

                    }
                }
                if (reportList[0] != null)
                {
                    var invoicedata = reportList[0];
                    foreach (var item in invoicedata)
                    {
                        string Inv_Con_Inv_Date = string.Empty;
                        if (item.Inv_Con_Inv_Date != null)
                        {
                            Inv_Con_Inv_Date = item.Inv_Con_Inv_Date.ToString("dd/MM/yyyy");
                        }
                        tbody += "<tr > <td style = 'text-align: center;'> <strong>" + item.Inv_Con_Invoce_Num +

                            "</strong> </td > <td style = 'text-align: center;'><strong>"
                            + Inv_Con_Inv_Date + "</strong></td> </tr> </table></td> </tr>";
                        tdata = item.Inv_Con_Sub_Total;
                    }
                }
                if (reportList[2] != null)
                {
                    var wodata = reportList[2];
                    foreach (var wodetails in wodata)
                    {
                        string completedate = string.Empty;
                        if (wodetails.Complete_Date != null)
                        {
                            completedate = wodetails.Complete_Date.ToString("dd/MM/yyyy");
                        }
                        tbody += "<tr><td colspan='2' height='26'><table width='100%' border='0'><tr height = '40px'> " +
                            "<td   bgcolor='#CCCCCC' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 16px; font-weight: bold;width: 50%;'>" +
                            "Bill To </td><td  bgcolor='#CCCCCC' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 16px; font-weight: bold;'>" +
                            "Work Order Info</td> </tr> <tr> <td  bgcolor='#FFFFFF' style = 'font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px;'>" +
                            wodetails.IPL_Company_Name + ",<br>" + wodetails.CompanyAddress + "<br>" + wodetails.IPL_StateName + "<br>" + wodetails.IPL_Company_PinCode + "<br>" +
                            wodetails.IPL_Company_Mobile + "</td> <td  bgcolor='#FFFFFF' style = 'font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px;'> <table width ='150' border='0'></td><tr> <td><strong>WO # </strong> :" +
                            wodetails.workOrderNumber + "<br> <strong>Loan # </strong> :" +
                            wodetails.Loan_Info + "<br>" + wodetails.address1 + "," +
                            wodetails.city + "," + wodetails.SM_Name + "," + wodetails.zip + "," + "<br> <strong>Complete Date</strong> :" +
                            completedate + " <br> </td></tr> </table> </td> </tr></table></td> </tr>";
                    }
                }
                if (reportList[1] != null)
                {
                    var chinvoicedata = reportList[1];
                    tbody += "<tr><td colspan='2' ><table width='100%' height ='900px' border='0'> <tr height ='30px'><td width='10%' bgcolor='#CCCCCC' style='font-size: 12px'><div align='center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'>&nbsp;Sr No </span></b></span></div><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'></span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align='center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'>&nbsp;Description </span></b></span></div><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align='center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;' > &nbsp; Qty </span ></b ></span ></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Price</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td>  <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Disc</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Total</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> </tr> ";


                    foreach (var childdetails in chinvoicedata)
                    {
                        var sr = 1 + index++;
                        //tdata = tdata + childdetails.Inv_Con_Ch_Total;
                        tbody += "<tr height ='30px'> <td style = 'text-align: center;'width='10%'>" + sr + "</td >" +
                            "<td style = 'text-align: center;'>" + childdetails.Task_Name + "<br/><span style = 'font-size: 12px'>" + childdetails.Inv_Con_Ch_Comment + "</span></td >" +
                            "<td style = 'text-align: center;'>" + childdetails.Inv_Con_Ch_Qty + "</td>" +
                            "<td style = 'text-align: center;'>" + Math.Round(childdetails.Inv_Con_Ch_Price, 2) + "</td>" +
                            "<td style = 'text-align: center;'>" + Math.Round(childdetails.Inv_Con_Ch_Discount, 2) + "%" + "</td>" +
                            "<td style = 'text-align: center;'>" + Math.Round(childdetails.Inv_Con_Ch_Adj_Total, 2) + "</td>" +
                            "</tr>";

                    }
                    tbody += "<tr><td></td></tr>";

                    tbody += "<tr height ='50px'> <td colspan='5'  bgcolor='#CCCCCC' style='text-align: right; font-size: 18px; font-weight: bold; border: 1;'>Total</td><td  bgcolor='#CCCCCC' style='text-align: center; border: 1;'>" + "<strong>$" + Math.Round(tdata, 2) + "</strong></td></tr>";

                    tbody += "</table</td></tr>";
                }




                tfooter += "</ table > ";
                tbody += "</tbody>";
                table.Append(tbody);



                html.Append(table.ToString());
                byte[] pdffile = GenerateRuntimePDF(html.ToString());
                custom.Data = pdffile;
                custom.HttpStatusCode = System.Net.HttpStatusCode.OK;
                return custom;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                custom.Message = ex.Message;
                custom.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                return custom;
            }
        }

        public List<dynamic> pdfGetContractorInvoicePrintDetail(ContractorInvoicePrintDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();

            try
            {
                DataSet ds = GetContractorPrintData(model);

                if (ds != null && ds.Tables.Count > 0)
                {
                    var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                    List<ContractorInvoicePrintDTO> contractor =
                       (from item in myEnumerableFeaprd
                        select new ContractorInvoicePrintDTO
                        {
                            Inv_Con_pkeyId = item.Field<Int64>("Inv_Con_pkeyId"),
                            Inv_Con_Wo_ID = item.Field<Int64>("Inv_Con_Wo_ID"),
                            Inv_Con_Invoce_Num = item.Field<String>("Inv_Con_Invoce_Num"),
                            Inv_Con_Inv_Date = item.Field<DateTime?>("Inv_Con_Inv_Date"),
                            Inv_Con_Sub_Total = item.Field<Decimal?>("Inv_Con_Sub_Total"),

                        }).ToList();
                    objDynamic.Add(contractor);
                }
                if (ds != null && ds.Tables.Count > 1)
                {
                    var myEnumerableFeaprd = ds.Tables[1].AsEnumerable();
                    List<ContractorInvoiceChildPrintDTO> contractorch =
                       (from item in myEnumerableFeaprd
                        select new ContractorInvoiceChildPrintDTO
                        {
                            Inv_Con_Ch_pkeyId = item.Field<Int64>("Inv_Con_Ch_pkeyId"),
                            Inv_Con_Ch_Qty = item.Field<String>("Inv_Con_Ch_Qty"),
                            Inv_Con_Ch_Price = item.Field<Decimal?>("Inv_Con_Ch_Price"),
                            Inv_Con_Ch_Total = item.Field<Decimal?>("Inv_Con_Ch_Total"),
                            Inv_Con_Ch_Adj_Price = item.Field<Decimal?>("Inv_Con_Ch_Adj_Price"),
                            Inv_Con_Ch_Adj_Total = item.Field<Decimal?>("Inv_Con_Ch_Adj_Total"),
                            Inv_Con_Ch_Comment = item.Field<String>("Inv_Con_Ch_Comment"),
                            Inv_Con_Ch_Discount = item.Field<Decimal?>("Inv_Con_Ch_Discount"),
                            Task_Name = item.Field<String>("Task_Name"),

                        }).ToList();
                    objDynamic.Add(contractorch);
                }
                if (ds != null && ds.Tables.Count > 2)
                {
                    var myEnumerableFeaprd = ds.Tables[2].AsEnumerable();
                    List<ContractorInvoiceWoDetailPrintDTO> contractorwo =
                       (from item in myEnumerableFeaprd
                        select new ContractorInvoiceWoDetailPrintDTO
                        {
                            workOrderNumber = item.Field<String>("workOrderNumber"),
                            workOrderInfo = item.Field<String>("workOrderInfo"),
                            address1 = item.Field<String>("address1"),
                            IPLNO = item.Field<String>("IPLNO"),
                            city = item.Field<String>("city"),
                            SM_Name = item.Field<String>("SM_Name"),
                            zip = item.Field<Int64?>("zip"),
                            country = item.Field<String>("country"),
                            Complete_Date = item.Field<DateTime?>("Complete_Date"),
                            Loan_Number = item.Field<String>("Loan_Number"),
                            Loan_Info = item.Field<String>("Loan_Info"),
                            fulladdress = item.Field<String>("fulladdress"),
                            WT_WorkType = item.Field<String>("WT_WorkType"),
                            Client_Company_Name = item.Field<String>("Client_Company_Name"),
                            Cont_Name = item.Field<String>("Cont_Name"),
                            Cordinator_Name = item.Field<String>("Cordinator_Name"),
                            Work_Type_Name = item.Field<String>("Work_Type_Name"),
                            Cust_Num_Number = item.Field<String>("Cust_Num_Number"),
                            ClientMetaData = item.Field<String>("ClientMetaData"),
                            ClientName = item.Field<String>("ClientName"),
                            ClientAddress = item.Field<String>("ClientAddress"),
                            ContractorAddress = item.Field<String>("ContractorAddress"),
                            IPL_Company_Name = item.Field<String>("IPL_Company_Name"),
                            CompanyAddress = item.Field<String>("CompanyAddress"),
                            IPL_Company_Mobile = item.Field<String>("IPL_Company_Mobile"),
                            ClientZip = item.Field<String>("ClientZip"),
                            ClientState = item.Field<String>("ClientState"),
                            ConZip = item.Field<String>("ConZip"),
                            ConState = item.Field<String>("ConState"),
                            IPL_StateName = item.Field<String>("IPL_StateName"),
                            IPL_Company_PinCode = item.Field<String>("IPL_Company_PinCode"),
                            YR_Company_App_logo = item.Field<String>("YR_Company_App_logo"),

                        }).ToList();
                    objDynamic.Add(contractorwo);
                }

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objDynamic;
        }

        //////////////////////
        ///



        //public string ContractorInvoiceprint(dynamic ItemList)
        //{
        //    string pdfPath = string.Empty;
        //   // string AmtInWords = toWords.words(Convert.ToDouble(ItemList.Value), false);
        //    //ItemList.AmountInWords = AmtInWords;
        //    DataTableResponse datatable = new DataTableResponse();

        //    try
        //    {

        //        var applicationhtml = RenderViewToString("InvoicePrint", "Contractor", ItemList);
        //        pdfPath = CreatePDF(applicationhtml);

        //    }
        //    catch (Exception ex)
        //    {
        //        log.logErrorMessage(ex.StackTrace);
        //        log.logErrorMessage(ex.Message);
        //    }
        //    string AppoimentLetter = "";
        //    string filebasepath = ConfigurationManager.AppSettings["BasePath"];
        //    AppoimentLetter = (filebasepath + "UploadDocuments/consignorInvoice_" + 1 + ".pdf");


        //    return pdfPath;

        //}


        //public static string RenderViewToString(string controllerName, string viewName, object viewData)
        //{

        //    var context = HttpContext.Current;
        //    var contextBase = new HttpContextWrapper(context);
        //    var routeData = new RouteData();
        //    routeData.Values.Add("controller", controllerName);

        //    var controllerContext = new ControllerContext(contextBase,
        //                                                  routeData,
        //                                                  new EmptyController());

        //    var razorViewEngine = new RazorViewEngine();
        //    var razorViewResult = razorViewEngine.FindView(controllerContext,
        //                                                   viewName,
        //                                                   "",
        //                                                   false);

        //    var writer = new StringWriter();
        //    var viewContext = new ViewContext(controllerContext,
        //                                      razorViewResult.View,
        //                                      new ViewDataDictionary(viewData),
        //                                      new TempDataDictionary(),
        //                                      writer);
        //    razorViewResult.View.Render(viewContext, writer);

        //    return writer.ToString();
        //}
        //class EmptyController : ControllerBase
        //{
        //    protected override void ExecuteCore() { }
        //}

        //public string CreatePDF(string html )
        //{
        //    string localfpath = string.Empty;
        //    try
        //    {


        //        string databasePath = string.Empty;
        //        // string dbpath = System.Configuration.ConfigurationManager.AppSettings["BasePathForUploadDocuments"].ToString();


        //        string localPath = System.Configuration.ConfigurationManager.AppSettings["UploadDocumentspath"];


        //        string pdfPath = string.Empty;
        //        var filename = Guid.NewGuid();
        //        string ffilename = string.Empty;

        //            ffilename = "ContractorInvoice" + filename + ".pdf";



        //        pdfPath = HttpContext.Current.Server.MapPath(@"../Document/UploadFiles/" + ffilename);




        //        using (MemoryStream myMemoryStream = new MemoryStream())
        //        {
        //            Document pdfDoc = new Document();
        //            PdfWriter myPDFWriter = PdfWriter.GetInstance(pdfDoc, myMemoryStream);

        //            pdfDoc.Open();
        //            html = html.Replace("\r\n", "");

        //            HtmlToPdfConverter nRecohtmltoPdfObj = new HtmlToPdfConverter();



        //            var content = nRecohtmltoPdfObj.GeneratePdf(html);
        //            custom.Data = pdffile;
        //            custom.HttpStatusCode = System.Net.HttpStatusCode.OK;
        //            return custom;

        //            //using (FileStream fs = File.Create(pdfPath))
        //            //{
        //            //    fs.Write(content, 0, (int)content.Length);
        //            //}
        //        }
        //        databasePath = localPath + ffilename;





        //    }
        //    catch (Exception ex)
        //    {
        //        log.logErrorMessage(ex.Message);
        //        log.logErrorMessage(ex.StackTrace);

        //    }
        //    return localfpath;//databasePath; //pdfPath;
        //}


    }
}