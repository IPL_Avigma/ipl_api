﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;
using Avigma.Repository.Lib;
using IPL.Models;
using IPL.Repository.data;
using IPLApp.Models;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;

namespace AdminDemo.Repositotry.Data
{
    public class Forms_Master_FilesData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        FormData formData = new FormData();
        Log log = new Log();

        public List<dynamic> AddCreateUpdate_Forms_Master_Files(Forms_Master_FilesDTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_Forms_Master_Files]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@FMFI_Pkey", 1 + "#bigint#" + model.FMFI_Pkey);
                input_parameters.Add("@FMFI_FormId", 1 + "#bigint#" + model.FMFI_FormId);
                input_parameters.Add("@FMFI_File_FolderName", 1 + "#nvarchar#" + model.FMFI_File_FolderName);
                input_parameters.Add("@FMFI_File_Name", 1 + "#nvarchar#" + model.FMFI_File_Name);
                input_parameters.Add("@FMFI_File_Path", 1 + "#nvarchar#" + model.FMFI_File_Path);
                input_parameters.Add("@FMFI_File_BucketName", 1 + "#nvarchar#" + model.FMFI_File_BucketName);
                input_parameters.Add("@FMFI_File_ProjectID", 1 + "#nvarchar#" + model.FMFI_File_ProjectID);
                input_parameters.Add("@FMFI_File_Size", 1 + "#nvarchar#" + model.FMFI_File_Size);
                input_parameters.Add("@FMFI_UploadBy", 1 + "#bigint#" + model.FMFI_UploadBy);
                input_parameters.Add("@FMFI_IsActive", 1 + "#bit#" + model.FMFI_IsActive);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserId);
                input_parameters.Add("@FMFI_Pkey_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;



        }

        private DataSet Get_Forms_Master_Files(Forms_Master_FilesDTO model)
        {
            DataSet ds = null;
            try
            {

                string selectProcedure = "[Get_Forms_Master_Files]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@FMFI_Pkey", 1 + "#bigint#" + model.FMFI_Pkey);
                input_parameters.Add("@FMFI_FormId", 1 + "#bigint#" + model.FMFI_FormId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> Get_Forms_Master_FilesDetails(Forms_Master_FilesDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = Get_Forms_Master_Files(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Forms_Master_FilesDTO> Forms_Master_Files =
                   (from item in myEnumerableFeaprd
                    select new Forms_Master_FilesDTO
                    {
                        FMFI_Pkey = item.Field<Int64>("FMFI_Pkey"),
                        FMFI_FormId = item.Field<Int64?>("FMFI_FormId"),
                        FMFI_File_FolderName = item.Field<String>("FMFI_File_FolderName"),
                        FMFI_File_Name = item.Field<String>("FMFI_File_Name"),
                        FMFI_File_Path = item.Field<String>("FMFI_File_Path"),
                        FMFI_File_BucketName = item.Field<String>("FMFI_File_BucketName"),
                        FMFI_File_ProjectID = item.Field<String>("FMFI_File_ProjectID"),
                        FMFI_File_Size = item.Field<String>("FMFI_File_Size"),
                        FMFI_IsActive = item.Field<Boolean?>("FMFI_IsActive")

                    }).ToList();

                objDynamic.Add(Forms_Master_Files);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        public List<dynamic> AddUpdateFormDocument(Client_Result_PhotoDTO client_Result_PhotoDTO)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                Forms_Master_FilesDTO forms_Master_FilesDTO = new Forms_Master_FilesDTO();
                forms_Master_FilesDTO.FMFI_Pkey = client_Result_PhotoDTO.Client_Result_Photo_ID;
                forms_Master_FilesDTO.FMFI_FormId = client_Result_PhotoDTO.Client_Result_Photo_Ch_ID;
                forms_Master_FilesDTO.FMFI_File_Name = client_Result_PhotoDTO.Client_Result_Photo_FileName;
                forms_Master_FilesDTO.FMFI_File_Path = client_Result_PhotoDTO.Client_Result_Photo_FilePath;
                forms_Master_FilesDTO.FMFI_File_BucketName = client_Result_PhotoDTO.Client_Result_Photo_BucketName;
                forms_Master_FilesDTO.FMFI_File_FolderName = client_Result_PhotoDTO.IPLNO;
                forms_Master_FilesDTO.FMFI_IsActive = client_Result_PhotoDTO.Client_Result_Photo_IsActive;
                forms_Master_FilesDTO.FMFI_IsDelete = client_Result_PhotoDTO.Client_Result_Photo_IsDelete;
                forms_Master_FilesDTO.Type = client_Result_PhotoDTO.Type;
                forms_Master_FilesDTO.UserId = client_Result_PhotoDTO.UserID;
                objData = AddCreateUpdate_Forms_Master_Files(forms_Master_FilesDTO);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

            }
            return objData;
        }

        public async Task<List<dynamic>> ImportPCRFormDetail(ImportPCRFormDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<PCRImportFormDTO> pCRImportFormDTOObj = new List<PCRImportFormDTO>();
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                if (!string.IsNullOrWhiteSpace(model.Imtr_File))
                {
                    var isJsonValid = IsValidJson(model.Imtr_File);
                    if (isJsonValid)
                    {
                        dynamic item = serializer.Deserialize<object>(model.Imtr_File);

                        foreach (dynamic itemData in item)
                        {
                            PCRImportFormDTO pCRImportFormDTO = new PCRImportFormDTO();
                            foreach (dynamic formDataValue in itemData.Value)
                            {
                                if (formDataValue.Key == "options_pcr_forms")
                                {
                                    pCRImportFormDTO.options_pcr_forms = GetOptPCRForm(formDataValue.Value);
                                }
                                if (formDataValue.Key == "options_pcr_forms_questions")
                                {
                                    if (pCRImportFormDTO.options_pcr_forms_questions == null)
                                        pCRImportFormDTO.options_pcr_forms_questions = new List<OptionsPcrFormsQuestions>();

                                    foreach (var optitem in formDataValue.Value)
                                    {
                                        pCRImportFormDTO.options_pcr_forms_questions.Add(GetOptPCRFormQue(optitem.Value));
                                    }
                                }
                                if (formDataValue.Key == "options_pcr_forms_questions_values")
                                {
                                    if (pCRImportFormDTO.options_pcr_forms_questions_values == null)
                                        pCRImportFormDTO.options_pcr_forms_questions_values = new List<OptionsPcrFormsQuestionsValues>();

                                    foreach (var optitem in formDataValue.Value)
                                    {
                                        pCRImportFormDTO.options_pcr_forms_questions_values.Add(GetOptPCRFormQueVal(optitem.Value));
                                    }
                                }
                                if (formDataValue.Key == "options_pcr_forms_rules")
                                {
                                    if (pCRImportFormDTO.options_pcr_forms_rules == null)
                                        pCRImportFormDTO.options_pcr_forms_rules = new List<OptionsPcrFormsRules>();

                                    foreach (var optitem in formDataValue.Value)
                                    {
                                        pCRImportFormDTO.options_pcr_forms_rules.Add(GetOptPCRFormRule(optitem.Value));
                                    }
                                }
                                if (formDataValue.Key == "options_pcr_forms_rules_checks")
                                {
                                    if (pCRImportFormDTO.options_pcr_forms_rules_checks == null)
                                        pCRImportFormDTO.options_pcr_forms_rules_checks = new List<OptionsPcrFormsRulesChecks>();

                                    foreach (var optitem in formDataValue.Value)
                                    {
                                        pCRImportFormDTO.options_pcr_forms_rules_checks.Add(GetOptPCRFormRuleCheck(optitem.Value));
                                    }
                                }
                                if (formDataValue.Key == "options_pcr_forms_photo_rules")
                                {
                                    if (pCRImportFormDTO.options_pcr_forms_photo_rules == null)
                                        pCRImportFormDTO.options_pcr_forms_photo_rules = new List<OptionsPcrFormsPhotoRules>();

                                    foreach (var optitem in formDataValue.Value)
                                    {
                                        pCRImportFormDTO.options_pcr_forms_photo_rules.Add(GetOptPCRFormPhotoRule(optitem.Value));
                                    }
                                }
                            }
                            pCRImportFormDTOObj.Add(pCRImportFormDTO);
                        }
                        var formDetails = await Task.Run(() => ImportPCRFormToDB(pCRImportFormDTOObj, model.UserID));

                        objData.Add(pCRImportFormDTOObj);
                    }
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        public async Task<List<dynamic>> ImportPCRFormToDB(List<PCRImportFormDTO> pCRImportFormDTOObj, Int64 userId)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {


                foreach (var item in pCRImportFormDTOObj)
                {
                    FormsDTO form = new FormsDTO();
                    form.FormName = item.options_pcr_forms.pcr_form_name;
                    form.IsRequired = item.options_pcr_forms.default_required != null ? Convert.ToBoolean(Convert.ToInt32(item.options_pcr_forms.default_required)) : false;
                    form.Form_IsActive = true;
                    form.Form_IsDelete = false;
                    form.UserId = userId;
                    form.FormNumber_Id = item.options_pcr_forms.pcr_form_id.ToString();
                    form.Type = 1;

                    var formDetails = await Task.Run(() => formData.Add_Form(form));
                    long formId = formDetails[0];

                    objData.Add(formDetails);

                    if (item.options_pcr_forms_questions != null)
                    {
                        foreach (var que in item.options_pcr_forms_questions)
                        {
                            Forms_Question_MasterDTO queObj = new Forms_Question_MasterDTO();
                            queObj.Type = 1;
                            queObj.Question = que.question_name;
                            queObj.Instructions = que.question_desc;
                            queObj.Que_Show = true;
                            queObj.Que_Required = true;
                            queObj.Que_IsActive = true;
                            queObj.Que_ClonePrevAnswer = 2; // Allow previous answer to be displayed
                            queObj.FormId = formId;
                            queObj.UserId = userId;

                            switch (que.question_type)
                            {
                                case "title":
                                    {
                                        queObj.QuestionTypeId = 6;
                                        break;
                                    }
                                case "radio":
                                    {
                                        queObj.QuestionTypeId = 4;
                                        break;
                                    }
                                case "dropdown":
                                    {
                                        queObj.QuestionTypeId = 3;
                                        break;
                                    }
                                case "textarea":
                                    {
                                        queObj.QuestionTypeId = 2;
                                        break;
                                    }
                                case "photos":
                                    {
                                        queObj.QuestionTypeId = 9;
                                        break;
                                    }
                                case "checkbox":
                                    {
                                        queObj.QuestionTypeId = 1;
                                        break;
                                    }
                                default:
                                    {
                                        queObj.QuestionTypeId = 5; // textBox
                                        break;
                                    }
                            }
                            var queDetails = await Task.Run(() => formData.Add_Question(queObj));
                            long questionId = queDetails[0];

                            if (item.options_pcr_forms_questions_values != null)
                            {
                                var QueOptList = item.options_pcr_forms_questions_values.Where(i => i.pcr_question_id == que.pcr_question_id).ToList();
                                if (QueOptList.Count > 0)
                                {
                                    List<Forms_Que_Options> forms_Que_Options = new List<Forms_Que_Options>();
                                    List<Forms_ActionRules_MasterDTO> forms_actionRules = new List<Forms_ActionRules_MasterDTO>();

                                    foreach (var queOptObj in QueOptList)
                                    {
                                        Forms_Que_Options queopt = new Forms_Que_Options();
                                        queopt.Type = 1;
                                        queopt.Option_QuestionId = questionId;
                                        queopt.OptionName = queOptObj.question_value;
                                        queopt.Option_IsActive = true;
                                        queopt.UserId = userId;
                                        queopt.ActionRuleList = item.options_pcr_forms_rules != null ? item.options_pcr_forms_rules.Where(i => Convert.ToInt64(i.rule_value) == queOptObj.question_value_id).ToList() : null;
                                        forms_Que_Options.Add(queopt);
                                    }
                                    var OptionsDetails = await Task.Run(() => formData.Add_Options(forms_Que_Options));

                                    for (int i = 0; i < forms_Que_Options.Count; i++)
                                    {
                                        var data = OptionsDetails[i];
                                        if (forms_Que_Options[i].ActionRuleList != null)
                                        {
                                            forms_Que_Options[i].ActionRuleList.ForEach(c => c.rule_value = data[0].ToString());
                                        }

                                    }
                                    if (forms_Que_Options != null)
                                    {
                                        foreach (var rules in forms_Que_Options)
                                        {
                                            if(rules.ActionRuleList!= null)
                                            {
                                                foreach (var pcrRule in rules.ActionRuleList)
                                                {
                                                    Forms_ActionRules_MasterDTO pcrFormRule = new Forms_ActionRules_MasterDTO();
                                                    pcrFormRule.ActionRule_Operator = pcrRule.rule_operator == "=" ? "Equals" : pcrRule.rule_operator;
                                                    pcrFormRule.ActionRule_Value = pcrRule.rule_value;
                                                    pcrFormRule.Type = 1;
                                                    pcrFormRule.ActionRule_IsActive = true;
                                                    pcrFormRule.UserId = userId;
                                                    pcrFormRule.QuestionId = questionId;

                                                    List<Forms_ActionRules> forms_ActionRules = new List<Forms_ActionRules>();
                                                    var actionChildList = item.options_pcr_forms_rules_checks != null ? item.options_pcr_forms_rules_checks.Where(i => i.pcr_form_rule_id == pcrRule.pcr_form_rule_id && i.pcr_question_id == que.pcr_question_id).ToList() : null;
                                                    if (actionChildList != null)
                                                    {
                                                        foreach (var chactionRule in actionChildList)
                                                        {
                                                            Forms_ActionRules forms_ActionRule = new Forms_ActionRules();
                                                            forms_ActionRule.Type = 1;
                                                            forms_ActionRule.Action_IsActive = true;
                                                            forms_ActionRule.UserId = userId;
                                                            forms_ActionRule.ActionQuestionId = questionId;
                                                            forms_ActionRule.QuestionId = questionId;
                                                            forms_ActionRule.ActionValue = chactionRule.check_operator == "show" ? "Show" : chactionRule.check_operator;
                                                            pcrFormRule.ActionRules.Add(forms_ActionRule);
                                                        }
                                                        forms_actionRules.Add(pcrFormRule);
                                                    }
                                                }
                                            }
                                            
                                        }
                                        var ActionRulesDetails = await Task.Run(() => formData.Add_Forms_ActionRules(forms_actionRules));
                                    }
                                }
                            }


                            if (item.options_pcr_forms_photo_rules != null)
                            {
                                var pcrPhotoRuleList = item.options_pcr_forms_photo_rules.Where(i => i.pcr_question_id == que.pcr_question_id).ToList();
                                if (pcrPhotoRuleList.Count > 0)
                                {
                                    List<Forms_PhotoRulesDTO> forms_PhotoRulesList = new List<Forms_PhotoRulesDTO>();

                                    foreach (var queOptObj in pcrPhotoRuleList)
                                    {
                                        Forms_PhotoRulesDTO photoRule = new Forms_PhotoRulesDTO();
                                        photoRule.Type = 1;
                                        photoRule.QuestionId = questionId;
                                        photoRule.PhotoRule_Min = Convert.ToInt64(queOptObj.min_photo);
                                        photoRule.PhotoRule_Max = Convert.ToInt64(queOptObj.max_photo);
                                        photoRule.PhotoRule_Operator = queOptObj.rule_operator;
                                        photoRule.PhotoRule_Value = photoRule.PhotoRule_Operator == "default" ? 0 : Convert.ToInt64(queOptObj.rule_value);
                                        photoRule.PhotoRule_IsActive = true;
                                        photoRule.UserId = userId;

                                        forms_PhotoRulesList.Add(photoRule);
                                    }
                                    var PhotoRulesDetails = await Task.Run(() => formData.Add_Forms_PhotoRules(forms_PhotoRulesList));
                                }
                            }



                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                throw;
            }
            return objData;
        }
        public OptionsPcrForms GetOptPCRForm(dynamic objList)
        {
            OptionsPcrForms optionsPcrForms = new OptionsPcrForms();
            foreach (var chitem in objList)
            {
                if (chitem.Key == "pcr_form_id")
                    optionsPcrForms.pcr_form_id = Convert.ToInt64(chitem.Value);
                if (chitem.Key == "org_id")
                    optionsPcrForms.org_id = chitem.Value;
                if (chitem.Key == "pcr_form_name")
                    optionsPcrForms.pcr_form_name = chitem.Value;
                if (chitem.Key == "default_required")
                    optionsPcrForms.default_required = chitem.Value;
                if (chitem.Key == "form_photos_required")
                    optionsPcrForms.form_photos_required = chitem.Value;
                if (chitem.Key == "transfer_prev_values")
                    optionsPcrForms.transfer_prev_values = chitem.Value;
                if (chitem.Key == "client_new_wo")
                    optionsPcrForms.client_new_wo = chitem.Value;
                if (chitem.Key == "global_form")
                    optionsPcrForms.global_form = chitem.Value;
                if (chitem.Key == "fillable_pdf")
                    optionsPcrForms.fillable_pdf = chitem.Value;
                if (chitem.Key == "pcr_form_active")
                    optionsPcrForms.pcr_form_active = chitem.Value;
                if (chitem.Key == "pruvan_ver")
                    optionsPcrForms.pruvan_ver = chitem.Value;
                if (chitem.Key == "thirdparty_id")
                    optionsPcrForms.thirdparty_id = chitem.Value;
                if (chitem.Key == "form_deleted")
                    optionsPcrForms.form_deleted = chitem.Value;
                if (chitem.Key == "form_share_name")
                    optionsPcrForms.form_share_name = chitem.Value;
                if (chitem.Key == "pruvan_master_id")
                    optionsPcrForms.pruvan_master_id = chitem.Value;
                if (chitem.Key == "insert_by")
                    optionsPcrForms.insert_by = chitem.Value;
                if (chitem.Key == "insert_date")
                    optionsPcrForms.insert_date = chitem.Value;
                if (chitem.Key == "publish_changes")
                    optionsPcrForms.publish_changes = chitem.Value;
                if (chitem.Key == "inspectorade_id")
                    optionsPcrForms.inspectorade_id = chitem.Value;
                if (chitem.Key == "cyprexx_id")
                    optionsPcrForms.cyprexx_id = chitem.Value;
                if (chitem.Key == "pruvan_id")
                    optionsPcrForms.pruvan_id = chitem.Value;
            }
            return optionsPcrForms;
        }

        public OptionsPcrFormsQuestions GetOptPCRFormQue(dynamic objList)
        {
            OptionsPcrFormsQuestions optionsPcrFormsQuestions = new OptionsPcrFormsQuestions();
            foreach (var chitem in objList)
            {
                if (chitem.Key == "pcr_question_id")
                    optionsPcrFormsQuestions.pcr_question_id = Convert.ToInt64(chitem.Value);
                if (chitem.Key == "pcr_form_id")
                    optionsPcrFormsQuestions.pcr_form_id = Convert.ToInt64(chitem.Value);
                if (chitem.Key == "question_num")
                    optionsPcrFormsQuestions.question_num = chitem.Value;
                if (chitem.Key == "question_name")
                    optionsPcrFormsQuestions.question_name = chitem.Value;
                if (chitem.Key == "question_desc")
                    optionsPcrFormsQuestions.question_desc = chitem.Value;
                if (chitem.Key == "question_type")
                    optionsPcrFormsQuestions.question_type = chitem.Value;
                if (chitem.Key == "question_data_type")
                    optionsPcrFormsQuestions.question_data_type = chitem.Value;
                if (chitem.Key == "question_display_active")
                    optionsPcrFormsQuestions.question_display_active = chitem.Value;
                if (chitem.Key == "question_required")
                    optionsPcrFormsQuestions.question_required = chitem.Value;
                if (chitem.Key == "question_na_option")
                    optionsPcrFormsQuestions.question_na_option = chitem.Value;
                if (chitem.Key == "question_flag_photos")
                    optionsPcrFormsQuestions.question_flag_photos = chitem.Value;
                if (chitem.Key == "question_photos_required")
                    optionsPcrFormsQuestions.question_photos_required = chitem.Value;
                if (chitem.Key == "question_active")
                    optionsPcrFormsQuestions.question_active = chitem.Value;
                if (chitem.Key == "question_deleted")
                    optionsPcrFormsQuestions.question_deleted = chitem.Value;
                if (chitem.Key == "question_form_length")
                    optionsPcrFormsQuestions.question_form_length = chitem.Value;
                if (chitem.Key == "question_order")
                    optionsPcrFormsQuestions.question_order = chitem.Value;
                if (chitem.Key == "pdf_field_id")
                    optionsPcrFormsQuestions.pdf_field_id = chitem.Value;
                if (chitem.Key == "thirdpartyfield_id")
                    optionsPcrFormsQuestions.thirdpartyfield_id = chitem.Value;
                if (chitem.Key == "question_share_name")
                    optionsPcrFormsQuestions.question_share_name = chitem.Value;
                if (chitem.Key == "question_transfer_prev_values")
                    optionsPcrFormsQuestions.question_transfer_prev_values = chitem.Value;
                if (chitem.Key == "question_show_prev_values")
                    optionsPcrFormsQuestions.question_show_prev_values = chitem.Value;
                if (chitem.Key == "question_auto_transfer_prev_values")
                    optionsPcrFormsQuestions.question_auto_transfer_prev_values = chitem.Value;
                if (chitem.Key == "question_field_rules_cnt")
                    optionsPcrFormsQuestions.question_field_rules_cnt = chitem.Value;
                if (chitem.Key == "question_action_rules_cnt")
                    optionsPcrFormsQuestions.question_action_rules_cnt = chitem.Value;
                if (chitem.Key == "question_photo_rules_cnt")
                    optionsPcrFormsQuestions.question_photo_rules_cnt = chitem.Value;
                if (chitem.Key == "insert_by")
                    optionsPcrFormsQuestions.insert_by = chitem.Value;
                if (chitem.Key == "insert_date")
                    optionsPcrFormsQuestions.insert_date = chitem.Value;
            }
            return optionsPcrFormsQuestions;
        }

        public OptionsPcrFormsQuestionsValues GetOptPCRFormQueVal(dynamic objList)
        {
            OptionsPcrFormsQuestionsValues optionsPcrFormsQuestionsValues = new OptionsPcrFormsQuestionsValues();
            foreach (var chitem in objList)
            {
                if (chitem.Key == "question_value_id")
                    optionsPcrFormsQuestionsValues.question_value_id = Convert.ToInt64(chitem.Value);
                if (chitem.Key == "pcr_question_id")
                    optionsPcrFormsQuestionsValues.pcr_question_id = Convert.ToInt64(chitem.Value);
                if (chitem.Key == "question_value")
                    optionsPcrFormsQuestionsValues.question_value = chitem.Value;
                if (chitem.Key == "pdf_field_id")
                    optionsPcrFormsQuestionsValues.pdf_field_id = chitem.Value;
                if (chitem.Key == "question_value_active")
                    optionsPcrFormsQuestionsValues.question_value_active = chitem.Value;
                if (chitem.Key == "thirdparty_value_id")
                    optionsPcrFormsQuestionsValues.thirdparty_value_id = chitem.Value;
                if (chitem.Key == "question_share_value")
                    optionsPcrFormsQuestionsValues.question_share_value = chitem.Value;
            }
            return optionsPcrFormsQuestionsValues;
        }

        public OptionsPcrFormsRules GetOptPCRFormRule(dynamic objList)
        {
            OptionsPcrFormsRules optionsPcrFormsRules = new OptionsPcrFormsRules();
            foreach (var chitem in objList)
            {
                if (chitem.Key == "pcr_form_rule_id")
                    optionsPcrFormsRules.pcr_form_rule_id = Convert.ToInt64(chitem.Value);
                if (chitem.Key == "pcr_form_id")
                    optionsPcrFormsRules.pcr_form_id = Convert.ToInt64(chitem.Value);
                if (chitem.Key == "pcr_question_id")
                    optionsPcrFormsRules.pcr_question_id = Convert.ToInt64(chitem.Value);
                if (chitem.Key == "rule_type")
                    optionsPcrFormsRules.rule_type = chitem.Value;
                if (chitem.Key == "rule_operator")
                    optionsPcrFormsRules.rule_operator = chitem.Value;
                if (chitem.Key == "rule_value")
                    optionsPcrFormsRules.rule_value = chitem.Value;
                if (chitem.Key == "insert_by")
                    optionsPcrFormsRules.insert_by = chitem.Value;
                if (chitem.Key == "insert_date")
                    optionsPcrFormsRules.insert_date = chitem.Value;
            }
            return optionsPcrFormsRules;
        }
        public OptionsPcrFormsRulesChecks GetOptPCRFormRuleCheck(dynamic objList)
        {
            OptionsPcrFormsRulesChecks optionsPcrFormsRulesChecks = new OptionsPcrFormsRulesChecks();
            foreach (var chitem in objList)
            {
                if (chitem.Key == "pcr_rule_check_id")
                    optionsPcrFormsRulesChecks.pcr_rule_check_id = Convert.ToInt64(chitem.Value);
                if (chitem.Key == "pcr_form_rule_id")
                    optionsPcrFormsRulesChecks.pcr_form_rule_id = Convert.ToInt64(chitem.Value);
                if (chitem.Key == "pcr_question_id")
                    optionsPcrFormsRulesChecks.pcr_question_id = Convert.ToInt64(chitem.Value);
                if (chitem.Key == "check_operator")
                    optionsPcrFormsRulesChecks.check_operator = chitem.Value;
                if (chitem.Key == "check_value")
                    optionsPcrFormsRulesChecks.check_value = chitem.Value;
                if (chitem.Key == "insert_by")
                    optionsPcrFormsRulesChecks.insert_by = chitem.Value;
                if (chitem.Key == "insert_date")
                    optionsPcrFormsRulesChecks.insert_date = chitem.Value;
            }
            return optionsPcrFormsRulesChecks;
        }

        public OptionsPcrFormsPhotoRules GetOptPCRFormPhotoRule(dynamic objList)
        {
            OptionsPcrFormsPhotoRules optionsPcrFormsPhotoRules = new OptionsPcrFormsPhotoRules();
            foreach (var chitem in objList)
            {
                if (chitem.Key == "pcr_photo_rule_id")
                    optionsPcrFormsPhotoRules.pcr_photo_rule_id = Convert.ToInt64(chitem.Value);
                if (chitem.Key == "pcr_question_id")
                    optionsPcrFormsPhotoRules.pcr_question_id = Convert.ToInt64(chitem.Value);
                if (chitem.Key == "rule_operator")
                    optionsPcrFormsPhotoRules.rule_operator = chitem.Value;
                if (chitem.Key == "rule_value")
                    optionsPcrFormsPhotoRules.rule_value = chitem.Value;
                if (chitem.Key == "take_photo")
                    optionsPcrFormsPhotoRules.take_photo = chitem.Value;
                if (chitem.Key == "require_photo")
                    optionsPcrFormsPhotoRules.require_photo = chitem.Value;
                if (chitem.Key == "min_photo")
                    optionsPcrFormsPhotoRules.min_photo = chitem.Value;
                if (chitem.Key == "max_photo")
                    optionsPcrFormsPhotoRules.max_photo = chitem.Value;
                if (chitem.Key == "insert_by")
                    optionsPcrFormsPhotoRules.insert_by = chitem.Value;
                if (chitem.Key == "insert_date")
                    optionsPcrFormsPhotoRules.insert_date = chitem.Value;
            }
            return optionsPcrFormsPhotoRules;
        }

        private static bool IsValidJson(string strInput)
        {
            if (string.IsNullOrWhiteSpace(strInput))
                return false;
            Log log = new Log();
            strInput = strInput.Trim();

            if ((strInput.StartsWith("{") && strInput.EndsWith("}")) || //For object
                (strInput.StartsWith("[") && strInput.EndsWith("]"))) //For array
            {
                try
                {
                    var obj = JToken.Parse(strInput);
                    return true;
                }
                catch (JsonReaderException jex)
                {
                    //Exception in parsing json
                    log.logErrorMessage(jex.Message);
                    return false;
                }
                catch (Exception ex) //some other exception
                {
                    log.logErrorMessage(ex.ToString());
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
    }
}