﻿using Avigma.Repository.Lib;
using IPL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class RushesMasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public List<dynamic> AddRushesData(RushesMasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();

            string insertProcedure = "[CreateUpdateRushesMaster]";
            RushesMaster rushesMaster = new RushesMaster();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@rus_pkeyID", 1 + "#bigint#" + model.rus_pkeyID);
                input_parameters.Add("@rus_Name", 1 + "#varchar#" + model.rus_Name);
                input_parameters.Add("@rus_Active", 1 + "#bit#" + model.rus_Active);
                input_parameters.Add("@rus_IsActive", 1 + "#bit#" + model.rus_IsActive);
                input_parameters.Add("@rus_IsDelete", 1 + "#bit#" + model.rus_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@rus_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters); ;
                if (objData[1] == 0)
                {
                    rushesMaster.rus_pkeyID = "0";
                    rushesMaster.Status = "0";
                    rushesMaster.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    rushesMaster.rus_pkeyID = Convert.ToString(objData[0]);
                    rushesMaster.Status = Convert.ToString(objData[1]);


                }
                objcltData.Add(rushesMaster);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;



        }

        private DataSet GetRushesMaster(RushesMasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Rushes_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@rus_pkeyID", 1 + "#bigint#" + model.rus_pkeyID);
                input_parameters.Add("@WhereClause", 1 + "#varchar#" + model.WhereClause);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetRushesDetails(RushesMasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetRushesMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<RushesMasterDTO> RushesDetails =
                   (from item in myEnumerableFeaprd
                    select new RushesMasterDTO
                    {
                        rus_pkeyID = item.Field<Int64>("rus_pkeyID"),
                        rus_Name = item.Field<String>("rus_Name"),
                        rus_Active = item.Field<Boolean?>("rus_Active"),
                        rus_IsActive = item.Field<Boolean?>("rus_IsActive"),
                        rus_IsDelete = item.Field<Boolean?>("rus_IsDelete"),
                        rus_CreatedBy = item.Field<String>("rus_CreatedBy"),
                        rus_ModifiedBy = item.Field<String>("rus_ModifiedBy"),
                    }).ToList();

                objDynamic.Add(RushesDetails);

                if (model.Type == 1)
                {
                    if (ds.Tables.Count > 1)
                    {
                        var myEnumerableInsFilter = ds.Tables[1].AsEnumerable();
                        List<Filter_Admin_Rush_MasterDTO> insFilterList =
                           (from item in myEnumerableInsFilter
                            select new Filter_Admin_Rush_MasterDTO
                            {
                                Rush_Filter_PkeyID = item.Field<Int64>("Rush_Filter_PkeyID"),
                                Rush_Filter_RushName = item.Field<String>("Rush_Filter_RushName"),
                                Rush_Filter_RushIsActive = item.Field<Boolean?>("Rush_Filter_RushIsActive"),
                                Rush_Filter_CreatedBy = item.Field<String>("Rush_Filter_CreatedBy"),
                                Rush_Filter_ModifiedBy = item.Field<String>("Rush_Filter_ModifiedBy"),
                            }).ToList();

                        objDynamic.Add(insFilterList);
                    }
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        public List<dynamic> GetRushFilterDetails(RushesMasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();


            string wherecondition = string.Empty;
            try
            {

                var Data = JsonConvert.DeserializeObject<RushesMasterDTO>(model.FilterData);
                if (!string.IsNullOrEmpty(Data.rus_Name))
                {
                    wherecondition = " And rus_Name LIKE '%" + Data.rus_Name + "%'";
                }


                if (Data.rus_IsActive == true)
                {
                    wherecondition = wherecondition + "  And rus_IsActive =  '1'";
                }
                if (Data.rus_IsActive == false)
                {
                    wherecondition = wherecondition + "  And rus_IsActive =  '0'";
                }


                RushesMasterDTO rushesMasterDTO = new RushesMasterDTO();

                rushesMasterDTO.WhereClause = wherecondition;
                rushesMasterDTO.Type = 3;
                rushesMasterDTO.UserID = model.UserID;
                DataSet ds = GetRushesMaster(rushesMasterDTO);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<RushesMasterDTO> rushfilter =
                   (from item in myEnumerableFeaprd
                    select new RushesMasterDTO
                    {
                        rus_pkeyID = item.Field<Int64>("rus_pkeyID"),
                        rus_Name = item.Field<String>("rus_Name"),
                        rus_Active = item.Field<Boolean?>("rus_Active"),
                        rus_IsActive = item.Field<Boolean?>("rus_IsActive"),
                        rus_IsDelete = item.Field<Boolean?>("rus_IsDelete"),
                        rus_CreatedBy = item.Field<String>("rus_CreatedBy"),
                        rus_ModifiedBy = item.Field<String>("rus_ModifiedBy"),
                    }).ToList();

                objDynamic.Add(rushfilter);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }

            return objDynamic;
        }
    }
}