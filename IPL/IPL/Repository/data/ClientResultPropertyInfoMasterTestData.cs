﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class ClientResultPropertyInfoMasterTestData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        private List<dynamic> CreateUpdateClientResultPropertyInfoMasterTest(ClientResultPropertyInfoMasterTestDTO model)


        {
            List<dynamic> objData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_ClientResult_PropertyInfo_Master_Test]";
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@CRPIM_PkeyID", 1 + "#bigint#" + model.CRPIM_PkeyID);
                input_parameters.Add("@CRPIM_WO_ID", 1 + "#bigint#" + model.CRPIM_WO_ID);
                input_parameters.Add("@CRPIM_AddressID", 1 + "#bigint#" + model.CRPIM_AddressID);
                input_parameters.Add("@CRPIM_Lock_Box_Code", 1 + "#nvarchar#" + model.CRPIM_Lock_Box_Code);
                input_parameters.Add("@CRPIM_LotSize", 1 + "#nvarchar#" + model.CRPIM_LotSize);
                input_parameters.Add("@CRPIM_GPS_Latitude", 1 + "#nvarchar#" + model.CRPIM_GPS_Latitude);
                input_parameters.Add("@CRPIM_GPS_Longitude", 1 + "#nvarchar#" + model.CRPIM_GPS_Longitude);
                input_parameters.Add("@CRPIM_IsActive", 1 + "#bit#" + model.CRPIM_IsActive);
                input_parameters.Add("@CRPIM_IsDelete", 1 + "#bit#" + model.CRPIM_IsDelete);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@CRPIM_Pkey_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);
                objData = obj.SqlCRUD(insertProcedure, input_parameters);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        private DataSet GetClientResultPropertyInfoMasterTest(ClientResultPropertyInfoMasterTestDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_ClientResult_PropertyInfo_Master_Test]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@CRPIM_PkeyID", 1 + "#bigint#" + model.CRPIM_PkeyID);
                input_parameters.Add("@CRPIM_WO_ID", 1 + "#bigint#" + model.CRPIM_WO_ID);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);

                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }
            catch (Exception ex)

            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic>PostClientResultPropertyInfoMasterTestDetails(ClientResultPropertyInfoMasterTestDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                objData = CreateUpdateClientResultPropertyInfoMasterTest(model);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }
            return objData;
        }

        public List<dynamic> GetClientResultPropertyInfoMasterTestDetails(ClientResultPropertyInfoMasterTestDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetClientResultPropertyInfoMasterTest(model);

                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                }

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}