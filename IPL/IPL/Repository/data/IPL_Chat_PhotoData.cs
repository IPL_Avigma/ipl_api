﻿using Avigma.Repository.Lib;
using Firebase.Database;
using Firebase.Database.Query;
using FirebaseAdmin;
using FirebaseAdmin.Messaging;
using Google.Apis.Auth.OAuth2;
using IPL.Models;
using IPLApp.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Linq;

namespace IPL.Repository.data
{
    public class IPL_Chat_PhotoData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        //public DataSet GetIPLChatFileDetails(IPL_Chat_PhotoDTO model)
        //{
        //    DataSet ds = null;
        //    try
        //    {
        //        string selectProcedure = "[Get_WorkOrder_Msg_Document]";
        //        Dictionary<string, string> input_parameters = new Dictionary<string, string>();
        //        input_parameters.Add("@Wo_Msg_Doc_PkeyId", 1 + "#bigint#" + model.Wo_Msg_Doc_PkeyId);
        //        input_parameters.Add("@Wo_Msg_Doc_Wo_ID", 1 + "#bigint#" + model.Wo_Msg_Doc_Wo_ID);
        //        input_parameters.Add("@Wo_Msg_Doc_Company_Id", 1 + "#bigint#" + model.Wo_Msg_Doc_Company_Id);
        //        input_parameters.Add("@Type", 1 + "#int#" + model.Type);

        //        ds = obj.SelectSql(selectProcedure, input_parameters);
        //    }

        //    catch (Exception ex)
        //    {
        //        log.logErrorMessage(ex.StackTrace);
        //        log.logErrorMessage(ex.Message);
        //    }

        //    return ds;
        //}
        private DataSet GetIPLChatFile(WorkOrder_Message_DocumentDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_WorkOrder_Msg_Document]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();
                input_parameters.Add("@Wo_Msg_Doc_PkeyId", 1 + "#bigint#" + model.Wo_Msg_Doc_PkeyId);
                input_parameters.Add("@Wo_Msg_Doc_Wo_ID", 1 + "#bigint#" + model.Wo_Msg_Doc_Wo_ID);
                input_parameters.Add("@Wo_Msg_Doc_Company_Id", 1 + "#bigint#" + model.Wo_Msg_Doc_Company_Id);
                input_parameters.Add("@Wo_Msg_Doc_Type", 1 + "#int#" + model.Wo_Msg_Doc_Type);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
            }



            return ds;
        }

        public List<dynamic> GetIPLChatFileDetails(WorkOrder_Message_DocumentDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetIPLChatFile(model);
                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<WorkOrder_Message_DocumentDTO> WorkOrder_Message_DocumentDetails =
                   (from item in myEnumerableFeaprd
                    select new WorkOrder_Message_DocumentDTO
                    {
                        Wo_Msg_Doc_File_Path = item.Field<String>("Wo_Msg_Doc_File_Path"),
                    }).ToList();

                objDynamic.Add(WorkOrder_Message_DocumentDetails);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
            }

            return objDynamic;
        }

        //public DataSet GetIPLChatImageDetails(IPL_Chat_PhotoDTO model)
        //{
        //    DataSet ds = null;
        //    List<dynamic> objAddData = new List<dynamic>();
        //    Client_Result_PhotoDTO CRP = new Client_Result_PhotoDTO();
        //    try
        //    {

        //        string selectProcedure = "[Get_ClientPhotoMaster]";
        //        Dictionary<string, string> input_parameters = new Dictionary<string, string>();
        //        input_parameters.Add("@Client_Result_Photo_ID", 1 + "#bigint#" + model.Wo_Msg_Doc_PkeyId);
        //        input_parameters.Add("@Client_Result_Photo_Wo_ID", 1 + "#bigint#" + model.Wo_Msg_Doc_Wo_ID);
        //        input_parameters.Add("@Type", 1 + "#int#" + model.Type);
        //        ds = obj.SelectSql(selectProcedure, input_parameters);


        //    }
        //    catch (Exception ex)
        //    {
        //        log.logErrorMessage(ex.StackTrace);
        //        log.logErrorMessage(ex.Message);
        //    }

        //    return ds;
        //}
        private DataSet GetIPLChatImage(IPL_Chat_PhotoDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_ClientPhotoMaster]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();
                input_parameters.Add("@Client_Result_Photo_ID", 1 + "#bigint#" + model.Wo_Msg_Doc_PkeyId);
                input_parameters.Add("@Client_Result_Photo_Wo_ID", 1 + "#bigint#" + model.Wo_Msg_Doc_Wo_ID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
            }



            return ds;
        }

        public List<dynamic> GetIPLChatImageDetails(IPL_Chat_PhotoDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetIPLChatImage(model);
                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Client_Result_PhotoDTO> ClientResultPhotoDetails =
                   (from item in myEnumerableFeaprd
                    select new Client_Result_PhotoDTO
                    {
                        Client_Result_Photo_FilePath = item.Field<String>("Client_Result_Photo_FilePath"),
                    }).ToList();

                objDynamic.Add(ClientResultPhotoDetails);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
            }

            return objDynamic;
        }
        private DataSet PvtGetWorkOrderData(workOrderxDTO work)
        {
            DataSet ds = new DataSet();
            try
            {
                string insertProcedure = "[GetMessageWorkOrderMaster]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();
                input_parameters.Add("@workOrder_ID", 1 + "#bigint#" + work.workOrder_ID);
                input_parameters.Add("@UserID", 1 + "#bigint#" + work.UserID);
                input_parameters.Add("@whereClause", 1 + "#nvarchar#" + work.whereClause);
                input_parameters.Add("@Type", 1 + "#int#" + work.Type);
                ds = obj.SelectSql(insertProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ds;
        }


        public List<dynamic> GetWorkOrderData(workOrderxDTO work, int skip, int take)
        {
            List<dynamic> objdata = new List<dynamic>();
            try
            {
                DataSet ds = PvtGetWorkOrderData(work);
                var myEnumerable = ds.Tables[0].AsEnumerable();
                var dmyEnumerable = myEnumerable.Skip(skip).Take(take);
                List<ViewMessageWorkOrder> WorkOrderData =
                    (from item in dmyEnumerable
                     select new ViewMessageWorkOrder
                     {
                         workOrder_ID = item.Field<Int64>("workOrder_ID"),
                         workOrderNumber = item.Field<String>("workOrderNumber"),
                         Status_Name = item.Field<String>("Status_Name"),
                         dueDate = item.Field<DateTime?>("dueDate"),
                         Com_Name = item.Field<String>("Com_Name"),
                         address1 = item.Field<String>("address1"),
                         city = item.Field<String>("city"),
                         assigned_admin = item.Field<Int64?>("assigned_admin"),
                         state = item.Field<String>("state"),
                         zip = item.Field<Int64?>("zip"),
                         IPLNO = item.Field<String>("IPLNO"),
                         ContractorName = item.Field<String>("ContractorName"),
                         WT_WorkType = item.Field<String>("WT_WorkType"),
                         Main_Cat_Name = item.Field<String>("Main_Cat_Name"),
                         Cust_Num_Number = item.Field<String>("Cust_Num_Number"),
                         CordinatorName = item.Field<String>("CordinatorName"),
                         ProcessorName = item.Field<String>("ProcessorName"),
                         status = item.Field<String>("status"),
                         Client_Company_Name = item.Field<String>("Client_Company_Name"),
                         Cordinator = item.Field<Int64?>("Cordinator"),
                         Processor = item.Field<Int64?>("Processor"),
                         Contractor = item.Field<Int64?>("Contractor"),
                         Company = item.Field<String>("Company"),
                         Customer_Number = item.Field<Int64?>("Customer_Number"),
                         Category = item.Field<Int64?>("Category"),
                         WorkType = item.Field<Int64?>("WorkType"),
                         ContractorLoginName = item.Field<String>("ContractorLoginName"),
                         CordinatorLoginName = item.Field<String>("CordinatorLoginName"),
                         ProcessorLoginName = item.Field<String>("ProcessorLoginName"),
                         FullAddress =  item.Field<String>("IPLNO") + " " + item.Field<String>("address1") + " " + item.Field<String>("city") + " " + item.Field<String>("state") + " " + item.Field<Int64?>("zip"),


                     }).ToList();
                objdata.Add(WorkOrderData);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return objdata;
            }
            return objdata;
        }



        public List<dynamic> GetWorkOrderDataOnSearch(workOrderxDTO work, string SearchStr)
        {
            List<dynamic> objdata = new List<dynamic>();
            try
            {
                EmailTemplateData emailTemplateData = new EmailTemplateData();
                EmailWorkOderDTO emailWorkOderDTO = new EmailWorkOderDTO();
                Email_Message_DTO email_Message_DTO = new Email_Message_DTO();
                emailWorkOderDTO.Type = 2;
                emailWorkOderDTO.UserID = work.UserID;
                emailWorkOderDTO.workOrder_ID = work.workOrder_ID;

                email_Message_DTO = emailTemplateData.GetEmailMessageDetail(emailWorkOderDTO);
                if (work.workOrder_ID != 0)
                {
                    work.whereClause = " and wo.[workOrder_ID] =  " + work.workOrder_ID;
                }
                if (!string.IsNullOrEmpty(work.IPLNO))
                {
                    work.whereClause = " and wo.[IPLNO]  LIKE '%" + work.IPLNO + "%'";
                }
                DataSet ds = PvtGetWorkOrderData(work);
                var myEnumerable = ds.Tables[0].AsEnumerable();
                List<ViewMessageWorkOrder> WorkOrderData =
                    (from item in myEnumerable
                     select new ViewMessageWorkOrder
                     {
                         workOrder_ID = item.Field<Int64>("workOrder_ID"),
                         workOrderNumber = item.Field<String>("workOrderNumber"),
                         Status_Name = item.Field<String>("Status_Name"),
                         dueDate = item.Field<DateTime?>("dueDate"),
                         Com_Name = item.Field<String>("Com_Name"),
                         address1 = item.Field<String>("address1"),
                         city = item.Field<String>("city"),
                         assigned_admin = item.Field<Int64?>("assigned_admin"),
                         state = item.Field<String>("state"),
                         zip = item.Field<Int64?>("zip"),
                         IPLNO = item.Field<String>("IPLNO") == null ? "" : item.Field<String>("IPLNO"),
                         ContractorName = item.Field<String>("ContractorName"),
                         WT_WorkType = item.Field<String>("WT_WorkType"),
                         Main_Cat_Name = item.Field<String>("Main_Cat_Name"),
                         Cust_Num_Number = item.Field<String>("Cust_Num_Number"),
                         CordinatorName = item.Field<String>("CordinatorName"),
                         ProcessorName = item.Field<String>("ProcessorName"),
                         status = item.Field<String>("status"),
                         Client_Company_Name = item.Field<String>("Client_Company_Name"),
                         Cordinator = item.Field<Int64?>("Cordinator"),
                         Processor = item.Field<Int64?>("Processor"),
                         Contractor = item.Field<Int64?>("Contractor"),
                         Company = item.Field<String>("Company"),
                         Customer_Number = item.Field<Int64?>("Customer_Number"),
                         Category = item.Field<Int64?>("Category"),
                         WorkType = item.Field<Int64?>("WorkType"),
                         ContractorLoginName = item.Field<String>("ContractorLoginName"),
                         CordinatorLoginName = item.Field<String>("CordinatorLoginName"),
                         ProcessorLoginName = item.Field<String>("ProcessorLoginName"),
                         Contractor_ImagePath = item.Field<String>("Contractor_ImagePath"),
                         Contractor_Email = item.Field<String>("Contractor_Email"),
                         Contractor_LoginName = item.Field<String>("Contractor_LoginName"),
                         Cordinator_ImagePath = item.Field<String>("Cordinator_ImagePath"),
                         Cordinator_Email = item.Field<String>("Cordinator_Email"),
                         Cordinator_LoginName = item.Field<String>("Cordinator_LoginName"),
                         Processor_ImagePath = item.Field<String>("Processor_ImagePath"),
                         Processor_Email = item.Field<String>("Processor_Email"),
                         Processor_LoginName = item.Field<String>("Processor_LoginName"),
                         Body = email_Message_DTO.Body,
                         Subject = email_Message_DTO.Subject,
                         FullAddress =   item.Field<String>("IPLNO") + " " + item.Field<String>("address1") + " " + item.Field<String>("city") + " " + item.Field<String>("state") + " " + item.Field<Int64?>("zip"),
                     }).ToList();

                //if (!string.IsNullOrWhiteSpace(SearchStr))
                //{
                //    WorkOrderData = WorkOrderData.Where(a => !string.IsNullOrWhiteSpace(a.IPLNO) && a.IPLNO.Contains(SearchStr)).ToList();
                //}
                
                objdata.Add(WorkOrderData);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return objdata;
            }
            return objdata;
        }

        public List<dynamic> GetWorkOrderData(workOrderxDTO work)
        {
            List<dynamic> objdata = new List<dynamic>();
            try
            {
                DataSet ds = PvtGetWorkOrderData(work);
                var myEnumerable = ds.Tables[0].AsEnumerable();

                List<ViewMessageWorkOrder> WorkOrderData =
                    (from item in myEnumerable
                     select new ViewMessageWorkOrder
                     {
                         workOrder_ID = item.Field<Int64>("workOrder_ID"),
                         workOrderNumber = item.Field<String>("workOrderNumber"),
                         Status_Name = item.Field<String>("Status_Name"),
                         dueDate = item.Field<DateTime?>("dueDate"),
                         Com_Name = item.Field<String>("Com_Name"),
                         address1 = item.Field<String>("address1"),
                         city = item.Field<String>("city"),
                         assigned_admin = item.Field<Int64?>("assigned_admin"),
                         state = item.Field<String>("state"),
                         zip = item.Field<Int64?>("zip"),
                         IPLNO = item.Field<String>("IPLNO"),
                         ContractorName = item.Field<String>("ContractorName"),
                         WT_WorkType = item.Field<String>("WT_WorkType"),
                         Main_Cat_Name = item.Field<String>("Main_Cat_Name"),
                         Cust_Num_Number = item.Field<String>("Cust_Num_Number"),
                         CordinatorName = item.Field<String>("CordinatorName"),
                         ProcessorName = item.Field<String>("ProcessorName"),
                         status = item.Field<String>("status"),
                         Client_Company_Name = item.Field<String>("Client_Company_Name"),
                         Cordinator = item.Field<Int64?>("Cordinator"),
                         Processor = item.Field<Int64?>("Processor"),
                         Contractor = item.Field<Int64?>("Contractor"),
                         Company = item.Field<String>("Company"),
                         Customer_Number = item.Field<Int64?>("Customer_Number"),
                         Category = item.Field<Int64?>("Category"),
                         WorkType = item.Field<Int64?>("WorkType"),
                         ContractorLoginName = item.Field<String>("ContractorLoginName"),
                         CordinatorLoginName = item.Field<String>("CordinatorLoginName"),
                         ProcessorLoginName = item.Field<String>("ProcessorLoginName"),
                         FullAddress =   item.Field<String>("IPLNO") + " " + item.Field<String>("address1") + " " + item.Field<String>("city") + " " + item.Field<String>("state") + " " + item.Field<Int64?>("zip"),


                     }).ToList();
                objdata.Add(WorkOrderData);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return objdata;
            }
            return objdata;
        }

        private DataSet GetMessageWorkOrderList(workOrderxDTO work)
        {
            DataSet ds = new DataSet();
            try
            {
                string insertProcedure = "[GetMessageWorkOrderListMaster]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();
                input_parameters.Add("@workOrder_ID", 1 + "#bigint#" + work.workOrder_ID);
                input_parameters.Add("@UserID", 1 + "#bigint#" + work.UserID);
                input_parameters.Add("@whereClause", 1 + "#nvarchar#" + work.whereClause);
                input_parameters.Add("@IPLNO", 1 + "#nvarchar#" + work.IPLNO);
                input_parameters.Add("@Type", 1 + "#int#" + work.Type);
                ds = obj.SelectSql(insertProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ds;
        }

        public async Task<List<dynamic>> GetMessageWorkOrderListData(workOrderxDTO work)
        {
            List<dynamic> objdata = new List<dynamic>();
            var iplCompany = "";
            var loginName = "";
            List<ViewMessageWorkOrder> WorkOrderData = new List<ViewMessageWorkOrder>();
            try
            {
                DataSet ds = GetMessageWorkOrderList(work);

                if (ds.Tables.Count > 0)
                {
                    var myEnumerable = ds.Tables[0].AsEnumerable();

                     WorkOrderData = (from item in myEnumerable
                                       select new ViewMessageWorkOrder
                                       {
                                           workOrder_ID = item.Field<Int64>("workOrder_ID"),
                                           workOrderNumber = item.Field<String>("workOrderNumber"),
                                           IPLNO = item.Field<String>("IPLNO"),
                                           FullAddress =  item.Field<String>("IPLNO") +" "+ item.Field<String>("address1")+" "+ item.Field<String>("city") + " " + item.Field<String>("state") + " " + item.Field<Int64?>("zip"),
                                           address1 = item.Field<String>("address1"),
                                           city = item.Field<String>("city"),
                                           state = item.Field<String>("state"),
                                           zip = item.Field<Int64?>("zip")
                                       }).ToList();

                   
                }
                if (ds.Tables.Count > 1)
                {
                    iplCompany = ds.Tables[1].Rows[0][0].ToString();
                }
                if (ds.Tables.Count > 2)
                {
                    loginName = ds.Tables[2].Rows[0][0].ToString();
                }

                List<ViewMessageWorkOrder> WorkOrderDataList = new List<ViewMessageWorkOrder>();

                var firebaseWoMessageList = await GetFirebaseWoMessage(iplCompany, loginName);
                if (firebaseWoMessageList != null)
                {
                    foreach (var firebaseWoMessage in firebaseWoMessageList)
                    {
                        var woMsgData = WorkOrderData.FirstOrDefault(w => w.IPLNO == firebaseWoMessage.IPLNO && w.IPLNO != null);
                        if (woMsgData != null)
                        {
                            woMsgData.lastUpdate = firebaseWoMessage.lastUpdate;
                            woMsgData.lastUpdateDate = firebaseWoMessage.lastUpdateDate;

                            WorkOrderDataList.Add(woMsgData);
                        }
                    }
                }
                WorkOrderDataList.OrderByDescending(o => o.lastUpdateDate).ToList();
                objdata.Add(WorkOrderDataList);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return objdata;
            }
            return objdata;
        }

        //Filter message Data

        public List<dynamic> GetMessageWorkOrderListFilterData(workOrderxDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {

                //Notification coden to  be added here
                model.Type = 1;
                DataSet ds = GetMessageWorkOrderList(model);

                #region Comment
                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<workOrderxDTO> FilterData =
                   (from item in myEnumerableFeaprd
                    select new workOrderxDTO
                    {
                        workOrder_ID = item.Field<Int64>("workOrder_ID"),
                        workOrderNumber = item.Field<String>("workOrderNumber"),
                        IPLNO = item.Field<String>("IPLNO"),

                    }).ToList();
                #endregion


                objDynamic.Add(FilterData);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        public async Task<List<FirebaseUserIPLListDTO>> GetFirebaseWoMessage(string iplCompany, string loginName)
        {
            try
            {
                //var result = GetZestimateAsync();

                string firebaseUrl = System.Configuration.ConfigurationManager.AppSettings["FirebaseUrl"];

                var firebaseClient = new FirebaseClient(firebaseUrl);

                var objComData = await firebaseClient
                     .Child("users")
                     .Child(iplCompany)
                     .OnceAsync<dynamic>();               

                if (objComData.Count > 0)
                {
                    var objUserData = objComData.FirstOrDefault(w => w.Key == loginName.Replace(".com", "com") && w.Key != null);

                    if (objUserData != null)
                    {
                        var objData = await firebaseClient
                                .Child("users")
                                .Child(iplCompany)
                                .Child(loginName.Replace(".com", "com"))
                                .Child("groups")
                                .OnceAsync<FirebaseUserIPLDTO>();

                        var locationList = new List<FirebaseUserIPLListDTO>();

                        if (objData != null && objData.Count > 0)
                        {
                            foreach (var location in objData)
                            {
                                FirebaseUserIPLListDTO liveUserLocationDTO = new FirebaseUserIPLListDTO();
                                liveUserLocationDTO.IPLNO = location.Object.IPLNO;
                                if (!string.IsNullOrWhiteSpace(location.Object.lastUpdate))
                                {
                                    liveUserLocationDTO.lastUpdate = location.Object.lastUpdate;

                                    liveUserLocationDTO.lastUpdateDate = TimeStampToDateTime(Convert.ToDouble(location.Object.lastUpdate));
                                }
                                locationList.Add(liveUserLocationDTO);
                            }
                        }
                        locationList = locationList.OrderByDescending(l => l.lastUpdateDate).ToList();

                        return locationList;
                    }                    
                }                
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return null;
        }

        public DateTime TimeStampToDateTime(double timeStamp)
        {
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddMilliseconds(timeStamp).ToLocalTime();
            return dtDateTime;
        }

        public double DateTimeToTimeStamp(DateTime datetime)
        {
            TimeSpan timeSpan = datetime - new DateTime(1970, 1, 1, 0, 0, 0);
            return (double)timeSpan.TotalSeconds;
        }
        public async Task GetZestimateAsync()
        {
            try
            {
                var client = new HttpClient();
                var request = new HttpRequestMessage
                {
                    Method = HttpMethod.Post,
                    RequestUri = new Uri("https://zillowdimashirokovv1.p.rapidapi.com/GetSearchResults.htm"),
                    Headers =
                    {
                        { "x-rapidapi-key", "b43c1d3582mshe91cac8d13457bap144018jsnf85b47ca21cf" },
                        { "x-rapidapi-host", "ZillowdimashirokovV1.p.rapidapi.com" },
                    },
                    Content = new FormUrlEncodedContent(new Dictionary<string, string>
                    {
                        { "citystatezip", "Seattle, WA" },
                        { "zws-id", "X1-ZWz1idcl01pe6j_76gmj" },
                        { "rentzestimate", "[true,false]" },
                        { "address", "2114 Bigelow Ave" },
                    }),
                };
                using (var response = await client.SendAsync(request))
                {
                    response.EnsureSuccessStatusCode();
                    var body = await response.Content.ReadAsStringAsync();
                    log.logInfoMessage(body);
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
           
        }

    }
    public class ZestimateResult
    {
        public int Low { get; set; }
        public int High { get; set; }

        public int RentLow { get; set; }
        public int RentHigh { get; set; }
    }
    internal static class Constants
    {
        public const string zwsId = "X1-ZWz1idcl01pe6j_76gmj"; //api key
        public const string baseUrl = "http://www.zillow.com/webservice/";
        public const string userAgent = "Chanel/0.1";

        //endpoints
        public const string getSearchResults = "GetSearchResults.htm";
        public const string getZestimate = "GetZestimate.htm";
    }
}