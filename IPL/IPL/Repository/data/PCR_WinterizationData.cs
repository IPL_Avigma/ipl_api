﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class PCR_WinterizationData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public List<dynamic> AddPCR_Winterization_Data(PCR_WinterizationDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objclntData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_PCR_Winterization_Master]";
            PCR_Winterization_Master pCR_Winterization_Master = new PCR_Winterization_Master();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {

                input_parameters.Add("@PCR_Winterization_pkeyId", 1 + "#bigint#" + model.PCR_Winterization_pkeyId);
                input_parameters.Add("@PCR_Winterization_MasterId", 1 + "#bigint#" + model.PCR_Winterization_MasterId);
                input_parameters.Add("@PCR_Winterization_WO_Id", 1 + "#bigint#" + model.PCR_Winterization_WO_Id);
                input_parameters.Add("@PCR_Winterization_ValType", 1 + "#int#" + model.PCR_Winterization_ValType);
                input_parameters.Add("@PCR_Winterization_Upon_Arrival", 1 + "#varchar#" + model.PCR_Winterization_Upon_Arrival);
                input_parameters.Add("@PCR_Winterization_Compleate_This_Order_Yes", 1 + "#varchar#" + model.PCR_Winterization_Compleate_This_Order_Yes);
                input_parameters.Add("@PCR_Winterization_Compleate_This_Order_No", 1 + "#varchar#" + model.PCR_Winterization_Compleate_This_Order_No);
                input_parameters.Add("@PCR_Winterization_Compleate_This_Order_Partial", 1 + "#varchar#" + model.PCR_Winterization_Compleate_This_Order_Partial);
                input_parameters.Add("@PCR_Winterization_Upon_Arrival_Never_Winterized", 1 + "#varchar#" + model.PCR_Winterization_Upon_Arrival_Never_Winterized);
                input_parameters.Add("@PCR_Winterization_Upon_Arrival_Breached", 1 + "#varchar#" + model.PCR_Winterization_Upon_Arrival_Breached);
                input_parameters.Add("@PCR_Winterization_Reason_Wint_NotCompleted_Allowable", 1 + "#bit#" + model.PCR_Winterization_Reason_Wint_NotCompleted_Allowable);
                input_parameters.Add("@PCR_Winterization_Reason_Wint_NotCompleted_Upon_Arrival", 1 + "#bit#" + model.PCR_Winterization_Reason_Wint_NotCompleted_Upon_Arrival);
                input_parameters.Add("@PCR_Winterization_Reason_Wint_NotCompleted_Out_Season", 1 + "#bit#" + model.PCR_Winterization_Reason_Wint_NotCompleted_Out_Season);
                input_parameters.Add("@PCR_Winterization_Reason_Wint_NotCompleted_TernedOff", 1 + "#bit#" + model.PCR_Winterization_Reason_Wint_NotCompleted_TernedOff);
                input_parameters.Add("@PCR_Winterization_Reason_Wint_NotCompleted_Prop_Damaged", 1 + "#bit#" + model.PCR_Winterization_Reason_Wint_NotCompleted_Prop_Damaged);
                input_parameters.Add("@PCR_Winterization_Reason_Wint_NotCompleted_Plumbing_Damage", 1 + "#bit#" + model.PCR_Winterization_Reason_Wint_NotCompleted_Plumbing_Damage);
                input_parameters.Add("@PCR_Winterization_Reason_Wint_NotCompleted_Plumbing_IsMissing", 1 + "#bit#" + model.PCR_Winterization_Reason_Wint_NotCompleted_Plumbing_IsMissing);
                input_parameters.Add("@PCR_Winterization_Reason_Wint_NotCompleted_AllReady_Winterized", 1 + "#bit#" + model.PCR_Winterization_Reason_Wint_NotCompleted_AllReady_Winterized);
                input_parameters.Add("@PCR_Winterization_Reason_Wint_NotCompleted_Common_Water_Line", 1 + "#bit#" + model.PCR_Winterization_Reason_Wint_NotCompleted_Common_Water_Line);
                input_parameters.Add("@PCR_Winterization_Reason_Wint_NotCompleted_Maintaining_Utilities", 1 + "#bit#" + model.PCR_Winterization_Reason_Wint_NotCompleted_Maintaining_Utilities);
                input_parameters.Add("@PCR_Winterization_Reason_Wint_NotCompleted_Other", 1 + "#bit#" + model.PCR_Winterization_Reason_Wint_NotCompleted_Other);
                input_parameters.Add("@PCR_Winterization_Heating_System", 1 + "#varchar#" + model.PCR_Winterization_Heating_System);
                input_parameters.Add("@PCR_Winterization_Heating_System_Well", 1 + "#varchar#" + model.PCR_Winterization_Heating_System_Well);
                input_parameters.Add("@PCR_Winterization_Radiant_Heat_System", 1 + "#varchar#" + model.PCR_Winterization_Radiant_Heat_System);
                input_parameters.Add("@PCR_Winterization_Radiant_Heat_System_Well", 1 + "#varchar#" + model.PCR_Winterization_Radiant_Heat_System_Well);
                input_parameters.Add("@PCR_Winterization_Steam_Heat_System", 1 + "#varchar#" + model.PCR_Winterization_Steam_Heat_System);
                input_parameters.Add("@PCR_Winterization_Steam_Heat_System_Well", 1 + "#varchar#" + model.PCR_Winterization_Steam_Heat_System_Well);
                input_parameters.Add("@PCR_Winterization_Posted_Signs", 1 + "#varchar#" + model.PCR_Winterization_Posted_Signs);
                input_parameters.Add("@PCR_Winterization_Common_Water_Line", 1 + "#varchar#" + model.PCR_Winterization_Common_Water_Line);
                input_parameters.Add("@PCR_Winterization_AntiFreeze_Toilet", 1 + "#varchar#" + model.PCR_Winterization_AntiFreeze_Toilet);
                input_parameters.Add("@PCR_Winterization_Water_Heater_Drained", 1 + "#varchar#" + model.PCR_Winterization_Water_Heater_Drained);
                input_parameters.Add("@PCR_Winterization_Water_Off_At_Curb", 1 + "#varchar#" + model.PCR_Winterization_Water_Off_At_Curb);
                input_parameters.Add("@PCR_Winterization_Blown_All_Lines", 1 + "#varchar#" + model.PCR_Winterization_Blown_All_Lines);
                input_parameters.Add("@PCR_Winterization_System_Held_Pressure", 1 + "#varchar#" + model.PCR_Winterization_System_Held_Pressure);
                input_parameters.Add("@PCR_Winterization_Disconnected_Water_Meter_Yes", 1 + "#varchar#" + model.PCR_Winterization_Disconnected_Water_Meter_Yes);
                input_parameters.Add("@PCR_Winterization_Disconnected_Water_Meter_No_Shut_Valve", 1 + "#varchar#" + model.PCR_Winterization_Disconnected_Water_Meter_No_Shut_Valve);
                input_parameters.Add("@PCR_Winterization_Disconnected_Water_Meter_No_Common_Water_Line", 1 + "#varchar#" + model.PCR_Winterization_Disconnected_Water_Meter_No_Common_Water_Line);
                input_parameters.Add("@PCR_Winterization_Disconnected_Water_Meter_No_Unable_To_Locate", 1 + "#varchar#" + model.PCR_Winterization_Disconnected_Water_Meter_No_Unable_To_Locate);
                input_parameters.Add("@PCR_Winterization_Disconnected_Water_Meter_No_Prohibited_Ordinance", 1 + "#varchar#" + model.PCR_Winterization_Disconnected_Water_Meter_No_Prohibited_Ordinance);
                input_parameters.Add("@PCR_Winterization_Disconnected_Water_Meter_No_Others", 1 + "#varchar#" + model.PCR_Winterization_Disconnected_Water_Meter_No_Others);
                input_parameters.Add("@PCR_Winterization_Radiant_Heat_Boiler_Drained", 1 + "#varchar#" + model.PCR_Winterization_Radiant_Heat_Boiler_Drained);
                input_parameters.Add("@PCR_Winterization_Radiant_Heat_Zone_Valves_Opened", 1 + "#varchar#" + model.PCR_Winterization_Radiant_Heat_Zone_Valves_Opened);
                input_parameters.Add("@PCR_Winterization_Radiant_Heat_AntiFreeze_Boiler", 1 + "#varchar#" + model.PCR_Winterization_Radiant_Heat_AntiFreeze_Boiler);
                input_parameters.Add("@PCR_Winterization_If_Well_System_Breaker_Off", 1 + "#varchar#" + model.PCR_Winterization_If_Well_System_Breaker_Off);
                input_parameters.Add("@PCR_Winterization_If_Well_System_Pressure_Tank_Drained", 1 + "#varchar#" + model.PCR_Winterization_If_Well_System_Pressure_Tank_Drained);
                input_parameters.Add("@PCR_Winterization_If_Well_System_Supply_Line_Disconnect", 1 + "#varchar#" + model.PCR_Winterization_If_Well_System_Supply_Line_Disconnect);
                input_parameters.Add("@PCR_Winterization_Interior_Main_Valve_Shut_Off", 1 + "#varchar#" + model.PCR_Winterization_Interior_Main_Valve_Shut_Off);
                input_parameters.Add("@PCR_Winterization_Interior_Main_Valve_Reason", 1 + "#varchar#" + model.PCR_Winterization_Interior_Main_Valve_Reason);
                input_parameters.Add("@PCR_Winterization_Interior_Main_Valve_Fire_Suppression_System", 1 + "#varchar#" + model.PCR_Winterization_Interior_Main_Valve_Fire_Suppression_System);
                input_parameters.Add("@PCR_Winterization_To_Bid", 1 + "#varchar#" + model.PCR_Winterization_To_Bid);
                input_parameters.Add("@PCR_Winterization_To_Bit_Text", 1 + "#varchar#" + model.PCR_Winterization_To_Bit_Text);
                input_parameters.Add("@PCR_Winterization_Winterize", 1 + "#varchar#" + model.PCR_Winterization_Winterize);
                input_parameters.Add("@PCR_Winterization_Thaw", 1 + "#varchar#" + model.PCR_Winterization_Thaw);
                input_parameters.Add("@PCR_Winterization_Description", 1 + "#varchar#" + model.PCR_Winterization_Description);
                input_parameters.Add("@PCR_Winterization_System_Type", 1 + "#int#" + model.PCR_Winterization_System_Type);
                input_parameters.Add("@PCR_Winterization_Reason", 1 + "#varchar#" + model.PCR_Winterization_Reason);
                input_parameters.Add("@PCR_Winterization_Amount", 1 + "#decimal#" + model.PCR_Winterization_Amount);
                input_parameters.Add("@PCR_Winterization_Winterize_Men", 1 + "#varchar#" + model.PCR_Winterization_Winterize_Men);
                input_parameters.Add("@PCR_Winterization_Winterize_Hrs", 1 + "#varchar#" + model.PCR_Winterization_Winterize_Hrs);
                input_parameters.Add("@PCR_Winterization_IsActive", 1 + "#bit#" + model.PCR_Winterization_IsActive);
                input_parameters.Add("@PCR_Winterization_IsDelete", 1 + "#bit#" + model.PCR_Winterization_IsDelete);
                input_parameters.Add("@PCR_Winterization_Reason_Wint_NotCompleted_Other_Text", 1 + "#varchar#" + model.PCR_Winterization_Reason_Wint_NotCompleted_Other_Text);
                input_parameters.Add("@PCR_Winterization_Disconnected_Water_Meter_Other_Text", 1 + "#varchar#" + model.PCR_Winterization_Disconnected_Water_Meter_Other_Text);
                input_parameters.Add("@PCR_Winterization_TextArea_Comment", 1 + "#varchar#" + model.PCR_Winterization_TextArea_Comment);
                input_parameters.Add("@fwo_pkyeId", 1 + "#bigint#" + model.fwo_pkyeId);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@PCR_Winterization_pkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    pCR_Winterization_Master.PCR_Winterization_pkeyId = "0";
                    pCR_Winterization_Master.Status = "0";
                    pCR_Winterization_Master.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    pCR_Winterization_Master.PCR_Winterization_pkeyId = Convert.ToString(objData[0]);
                    pCR_Winterization_Master.Status = Convert.ToString(objData[1]);


                }
                objclntData.Add(pCR_Winterization_Master);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objclntData;



        }

        private DataSet GetPCRPCRPCRWinterizationMaster(PCR_WinterizationDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_PCR_Winterization_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@PCR_Winterization_pkeyId", 1 + "#bigint#" + model.PCR_Winterization_pkeyId);
                input_parameters.Add("@PCR_Winterization_WO_Id", 1 + "#bigint#" + model.PCR_Winterization_WO_Id);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetPCRPCRPCRWinterizationDetails(PCR_WinterizationDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetPCRPCRPCRWinterizationMaster(model);

                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                }


                //var PCRWinterization = DatasetToModel(ds.Tables[0]);
                //objDynamic.Add(PCRWinterization);

                //if (ds.Tables.Count > 1)
                //{
                //    var history = DatasetToModel(ds.Tables[1]);
                //    objDynamic.Add(history);
                //}
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
        private List<PCR_WinterizationDTO> DatasetToModel(DataTable dataTable)
        {
            var myEnumerableFeaprd = dataTable.AsEnumerable();
            List<PCR_WinterizationDTO> PCRWinterization =
               (from item in myEnumerableFeaprd
                select new PCR_WinterizationDTO
                {

                    PCR_Winterization_pkeyId = item.Field<Int64>("PCR_Winterization_pkeyId"),
                    PCR_Winterization_MasterId = item.Field<Int64?>("PCR_Winterization_MasterId"),
                    PCR_Winterization_WO_Id = item.Field<Int64?>("PCR_Winterization_WO_Id"),
                    PCR_Winterization_ValType = item.Field<int?>("PCR_Winterization_ValType"),
                    PCR_Winterization_Upon_Arrival = item.Field<String>("PCR_Winterization_Upon_Arrival"),
                    PCR_Winterization_Compleate_This_Order_Yes = item.Field<String>("PCR_Winterization_Compleate_This_Order_Yes"),
                    PCR_Winterization_Compleate_This_Order_No = item.Field<String>("PCR_Winterization_Compleate_This_Order_No"),
                    PCR_Winterization_Compleate_This_Order_Partial = item.Field<String>("PCR_Winterization_Compleate_This_Order_Partial"),
                    PCR_Winterization_Upon_Arrival_Never_Winterized = item.Field<String>("PCR_Winterization_Upon_Arrival_Never_Winterized"),


                    PCR_Winterization_Upon_Arrival_Breached = item.Field<String>("PCR_Winterization_Upon_Arrival_Breached"),
                    PCR_Winterization_Reason_Wint_NotCompleted_Allowable = item.Field<Boolean?>("PCR_Winterization_Reason_Wint_NotCompleted_Allowable"),
                    PCR_Winterization_Reason_Wint_NotCompleted_Upon_Arrival = item.Field<Boolean?>("PCR_Winterization_Reason_Wint_NotCompleted_Upon_Arrival"),
                    PCR_Winterization_Reason_Wint_NotCompleted_Out_Season = item.Field<Boolean?>("PCR_Winterization_Reason_Wint_NotCompleted_Out_Season"),
                    PCR_Winterization_Reason_Wint_NotCompleted_TernedOff = item.Field<Boolean?>("PCR_Winterization_Reason_Wint_NotCompleted_TernedOff"),
                    PCR_Winterization_Reason_Wint_NotCompleted_Prop_Damaged = item.Field<Boolean?>("PCR_Winterization_Reason_Wint_NotCompleted_Prop_Damaged"),
                    PCR_Winterization_Reason_Wint_NotCompleted_Plumbing_Damage = item.Field<Boolean?>("PCR_Winterization_Reason_Wint_NotCompleted_Plumbing_Damage"),
                    PCR_Winterization_Reason_Wint_NotCompleted_Plumbing_IsMissing = item.Field<Boolean?>("PCR_Winterization_Reason_Wint_NotCompleted_Plumbing_IsMissing"),
                    PCR_Winterization_Reason_Wint_NotCompleted_AllReady_Winterized = item.Field<Boolean?>("PCR_Winterization_Reason_Wint_NotCompleted_AllReady_Winterized"),
                    PCR_Winterization_Reason_Wint_NotCompleted_Common_Water_Line = item.Field<Boolean?>("PCR_Winterization_Reason_Wint_NotCompleted_Common_Water_Line"),
                    PCR_Winterization_Reason_Wint_NotCompleted_Maintaining_Utilities = item.Field<Boolean?>("PCR_Winterization_Reason_Wint_NotCompleted_Maintaining_Utilities"),
                    PCR_Winterization_Reason_Wint_NotCompleted_Other = item.Field<Boolean?>("PCR_Winterization_Reason_Wint_NotCompleted_Other"),
                    PCR_Winterization_Heating_System = item.Field<String>("PCR_Winterization_Heating_System"),


                    PCR_Winterization_Heating_System_Well = item.Field<String>("PCR_Winterization_Heating_System_Well"),
                    PCR_Winterization_Radiant_Heat_System = item.Field<String>("PCR_Winterization_Radiant_Heat_System"),
                    PCR_Winterization_Radiant_Heat_System_Well = item.Field<String>("PCR_Winterization_Radiant_Heat_System_Well"),
                    PCR_Winterization_Steam_Heat_System = item.Field<String>("PCR_Winterization_Steam_Heat_System"),
                    PCR_Winterization_Steam_Heat_System_Well = item.Field<String>("PCR_Winterization_Steam_Heat_System_Well"),
                    PCR_Winterization_Posted_Signs = item.Field<String>("PCR_Winterization_Posted_Signs"),
                    PCR_Winterization_Common_Water_Line = item.Field<String>("PCR_Winterization_Common_Water_Line"),
                    PCR_Winterization_AntiFreeze_Toilet = item.Field<String>("PCR_Winterization_AntiFreeze_Toilet"),
                    PCR_Winterization_Water_Heater_Drained = item.Field<String>("PCR_Winterization_Water_Heater_Drained"),
                    PCR_Winterization_Water_Off_At_Curb = item.Field<String>("PCR_Winterization_Water_Off_At_Curb"),
                    PCR_Winterization_Blown_All_Lines = item.Field<String>("PCR_Winterization_Blown_All_Lines"),
                    PCR_Winterization_System_Held_Pressure = item.Field<String>("PCR_Winterization_System_Held_Pressure"),
                    PCR_Winterization_Disconnected_Water_Meter_Yes = item.Field<String>("PCR_Winterization_Disconnected_Water_Meter_Yes"),
                    PCR_Winterization_Disconnected_Water_Meter_No_Shut_Valve = item.Field<String>("PCR_Winterization_Disconnected_Water_Meter_No_Shut_Valve"),
                    PCR_Winterization_Disconnected_Water_Meter_No_Common_Water_Line = item.Field<String>("PCR_Winterization_Disconnected_Water_Meter_No_Common_Water_Line"),
                    PCR_Winterization_Disconnected_Water_Meter_No_Unable_To_Locate = item.Field<String>("PCR_Winterization_Disconnected_Water_Meter_No_Unable_To_Locate"),
                    PCR_Winterization_Disconnected_Water_Meter_No_Prohibited_Ordinance = item.Field<String>("PCR_Winterization_Disconnected_Water_Meter_No_Prohibited_Ordinance"),
                    PCR_Winterization_Disconnected_Water_Meter_No_Others = item.Field<String>("PCR_Winterization_Disconnected_Water_Meter_No_Others"),


                    PCR_Winterization_Radiant_Heat_Boiler_Drained = item.Field<String>("PCR_Winterization_Radiant_Heat_Boiler_Drained"),
                    PCR_Winterization_Radiant_Heat_Zone_Valves_Opened = item.Field<String>("PCR_Winterization_Radiant_Heat_Zone_Valves_Opened"),
                    PCR_Winterization_Radiant_Heat_AntiFreeze_Boiler = item.Field<String>("PCR_Winterization_Radiant_Heat_AntiFreeze_Boiler"),
                    PCR_Winterization_If_Well_System_Breaker_Off = item.Field<String>("PCR_Winterization_If_Well_System_Breaker_Off"),
                    PCR_Winterization_If_Well_System_Pressure_Tank_Drained = item.Field<String>("PCR_Winterization_If_Well_System_Pressure_Tank_Drained"),
                    PCR_Winterization_If_Well_System_Supply_Line_Disconnect = item.Field<String>("PCR_Winterization_If_Well_System_Supply_Line_Disconnect"),
                    PCR_Winterization_Interior_Main_Valve_Shut_Off = item.Field<String>("PCR_Winterization_Interior_Main_Valve_Shut_Off"),
                    PCR_Winterization_Interior_Main_Valve_Reason = item.Field<String>("PCR_Winterization_Interior_Main_Valve_Reason"),
                    PCR_Winterization_Interior_Main_Valve_Fire_Suppression_System = item.Field<String>("PCR_Winterization_Interior_Main_Valve_Fire_Suppression_System"),
                    PCR_Winterization_To_Bid = item.Field<String>("PCR_Winterization_To_Bid"),
                    PCR_Winterization_To_Bit_Text = item.Field<String>("PCR_Winterization_To_Bit_Text"),


                    PCR_Winterization_Winterize = item.Field<String>("PCR_Winterization_Winterize"),
                    PCR_Winterization_Thaw = item.Field<String>("PCR_Winterization_Thaw"),
                    PCR_Winterization_Description = item.Field<String>("PCR_Winterization_Description"),

                    //PCR_Winterization_System_Type = item.Field<int?>("PCR_Winterization_System_Type"),
                    PCR_Winterization_Reason = item.Field<String>("PCR_Winterization_Reason"),
                    PCR_Winterization_Amount = item.Field<Decimal?>("PCR_Winterization_Amount"),

                    PCR_Winterization_Winterize_Men = item.Field<String>("PCR_Winterization_Winterize_Men"),
                    PCR_Winterization_Winterize_Hrs = item.Field<String>("PCR_Winterization_Winterize_Hrs"),
                    PCR_Winterization_IsActive = item.Field<Boolean?>("PCR_Winterization_IsActive"),
                    PCR_Winterization_Reason_Wint_NotCompleted_Other_Text = item.Field<String>("PCR_Winterization_Reason_Wint_NotCompleted_Other_Text"),
                    PCR_Winterization_Disconnected_Water_Meter_Other_Text = item.Field<String>("PCR_Winterization_Disconnected_Water_Meter_Other_Text"),
                    PCR_Winterization_TextArea_Comment = item.Field<String>("PCR_Winterization_TextArea_Comment"),




                }).ToList();

            return PCRWinterization;
        }
    }
}