﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
namespace IPL.Repository.data
{
    public class User_AccessBy_CompanyID_Data
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        private DataSet GetUser_AccessBy_CompanyID(User_AccessBy_CompanyID_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_User_AccessBy_CompanyID]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@User_IPL_Company_PkeyId", 1 + "#bigint#" + model.User_IPL_Company_PkeyId);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetUser_AccessBy_CompanyIDDetails(User_AccessBy_CompanyID_DTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetUser_AccessBy_CompanyID(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<User_AccessBy_CompanyID_DTO> Get_Details =
                   (from item in myEnumerableFeaprd
                    select new User_AccessBy_CompanyID_DTO
                    {
                        
                        User_Acc_Log_PkeyId = item.Field<Int64>("User_Acc_Log_PkeyId"),
                        User_Acc_Log_IP_Address = item.Field<String>("User_Acc_Log_IP_Address"),
                        User_Acc_Log_Logged_In = item.Field<String>("User_Acc_Log_Logged_In"),
                        User_Acc_Log_Logged_Out = item.Field<String>("User_Acc_Log_Logged_Out"),
                        User_Acc_Log_MacD = item.Field<String>("User_Acc_Log_MacD"),
                        User_Acc_Log_UserName = item.Field<String>("User_Acc_Log_UserName"),
                        Name = item.Field<String>("Name"),
                        

                    }).ToList();

                objDynamic.Add(Get_Details);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}
