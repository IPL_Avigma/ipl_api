﻿using Avigma.Repository.Lib;
using IPL.Models;
using IPLApp.Models;
using IPLApp.Repository.data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using AutoMapper;

namespace IPL.Repository.data
{
    public class UserLoginPasswordForget
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        EmailData emailData = new EmailData();

        private DataSet GetUserEmail(AuthLoginDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[GetUser_Email_id]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@User_LoginName", 1 + "#varchar#" + model.User_LoginName);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
            }
            return ds;
        }

        public List<dynamic> GetUserEamil(AuthLoginDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetUserEmail(model);

                var myEnumerablefor = ds.Tables[0].AsEnumerable();
                List<AuthLoginDTO> usersDetails =
                   (from item in myEnumerablefor
                    select new AuthLoginDTO
                    {
                        User_pkeyID = item.Field<Int64>("User_pkeyID"),
                        User_Email = item.Field<String>("User_Email"),

                    }).ToList();

               
                UserMasterData userMasterData = new UserMasterData();
           

                var config = new MapperConfiguration(cfg => {
                    cfg.CreateMap<AuthLoginDTO, UserMasterDTO>();
                });
                IMapper mapper = config.CreateMapper();
                foreach (var item in usersDetails)
                {
                    item.User_LoginName = model.User_LoginName;
                    var dest = mapper.Map<AuthLoginDTO, UserMasterDTO>(item);
                    dest.User_Source = model.User_Source;
                    userMasterData.GeneratePasswordLink(dest, 2);
                }
                
                #region comment
                //if (usersDetails.Count > 0)
                //{

                //    //authLoginDTO.User_Email = usersDetails[0].User_Email;
                //    //authLoginDTO.User_Password = CreatePassword(8);
                //    //authLoginDTO.User_LoginName = model.User_LoginName;


                //    //var returnEmailMsg = emailData.SendRequestPassword(authLoginDTO);

                //    //if (returnEmailMsg[0] != 0) {

                //    //    userMasterDTO.User_pkeyID = usersDetails[0].User_pkeyID;
                //    //    userMasterDTO.User_Password = authLoginDTO.User_Password;
                //    //    userMasterDTO.Type = 1;

                //    //    var retuneval = PasswordUserMasterdetails(userMasterDTO);
                //    //}

                //  
                //}
                #endregion
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
            }
            return objDynamic;
        }




        public string CreatePassword(int length)
        {
            const string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            StringBuilder res = new StringBuilder();
            Random rnd = new Random();
            while (0 < length--)
            {
                res.Append(valid[rnd.Next(valid.Length)]);
            }
            return res.ToString();
        }



        // password set dBase
        public List<dynamic> PasswordUserMasterdetails(UserMasterDTO model)
        {
            SecurityHelper securityHelper = new SecurityHelper();
            
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objuserData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_User_Master_Password]";
            UserMaster userMaster = new UserMaster();
            if(!string.IsNullOrWhiteSpace(model.User_Token_val))
            { 
            string decriptedUid = securityHelper.Decrypt(model.User_Token_val, false);
            model.User_pkeyID = Convert.ToInt64(decriptedUid);
            }
            if(model.UserID == null)
            {
                model.UserID = model.User_pkeyID;
            }
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@User_pkeyID", 1 + "#bigint#" + model.User_pkeyID);
                input_parameters.Add("@User_Password", 1 + "#nvarchar#" + model.User_Password);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@User_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);

                objuserData.Add(objData);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                objuserData.Add(ex);
            }
            return objuserData;



        }



        //check user name
        private DataSet Checkcheck(AuthLoginDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_UserLoginCheck]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@User_LoginName", 1 + "#varchar#" + model.User_LoginName);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
            }
            return ds;
        }
        public List<dynamic> CheckUsername(AuthLoginDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = Checkcheck(model);

                var myEnumerableforx = ds.Tables[0].AsEnumerable();
                List<AuthLoginDTO> userCkek =
                   (from item in myEnumerableforx
                    select new AuthLoginDTO
                    {
                        UserCount = item.Field<int?>("UserCount"),
                    }).ToList();

                objDynamic.Add(userCkek);

                
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
            }
            return objDynamic;
        }



        private DataSet GetUserPassword(AuthLoginDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_UserLoginpassword]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@User_pkeyID", 1 + "#bigint#" + model.User_pkeyID);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
            }
            return ds;
        }


        public List<dynamic> GetPassword(AuthLoginDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetUserPassword(model);

                var myEnumerableforxx = ds.Tables[0].AsEnumerable();
                List<AuthLoginDTO> pass =
                   (from item in myEnumerableforxx
                    select new AuthLoginDTO
                    {
                        User_Password = item.Field<string>("User_Password"),
                    }).ToList();

                objDynamic.Add(pass);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
            }
            return objDynamic;
        }


        // change password  password set dBase page se
        public List<dynamic> PasswordChangeUser(AuthLoginDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objuserData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_User_Master_CHANGEPassword]";
            UserMaster userMaster = new UserMaster();
            if (model.UserID == null)
            {
                model.UserID = Convert.ToInt32(model.User_pkeyID);
            }
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@User_pkeyID", 1 + "#bigint#" + model.User_pkeyID);
                input_parameters.Add("@User_Password", 1 + "#nvarchar#" + model.User_Password);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@User_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);

                objuserData.Add(objData);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                objuserData.Add(ex);
            }
            return objuserData;



        }
    }
}