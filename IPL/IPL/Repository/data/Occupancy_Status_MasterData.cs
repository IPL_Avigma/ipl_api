﻿using Avigma.Repository.Lib;
using IPLApp.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class Occupancy_Status_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        private List<dynamic> CreateUpdateOccupancyStatusMaster(Occupancy_Status_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();
            Occupancy_Status_Master occupancy_Status_Master = new Occupancy_Status_Master();
            string insertProcedure = "[CreateUpdate_Occupancy_Status_Master]";
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@OS_PkeyID", 1 + "#bigint#" + model.OS_PkeyID);
                input_parameters.Add("@OS_Name", 1 + "#varchar#" + model.OS_Name);
                input_parameters.Add("@OS_IsActive", 1 + "#bit#" + model.OS_IsActive);
                input_parameters.Add("@OS_IsDelete", 1 + "#bit#" + model.OS_IsDelete);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@OS_Pkey_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters); ;
                if (objData[1] == 0)
                {
                    occupancy_Status_Master.OS_PkeyID = "0";
                    occupancy_Status_Master.Status = "0";
                    occupancy_Status_Master.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    occupancy_Status_Master.OS_PkeyID = Convert.ToString(objData[0]);
                    occupancy_Status_Master.Status = Convert.ToString(objData[1]);


                }
                objcltData.Add(occupancy_Status_Master);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;



        }


        private DataSet GetOccupancyStatusMaster(Occupancy_Status_MasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Occupancy_Status_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@OS_PkeyID", 1 + "#bigint#" + model.OS_PkeyID);
                input_parameters.Add("@WhereClause", 1 + "#varchar#" + model.WhereClause);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> CreateUpdateOccupancyStatusMasterDetails(Occupancy_Status_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                objData = CreateUpdateOccupancyStatusMaster(model);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }
            return objData;
        }

        public List<dynamic> GetOccupancyStatusMasterDetails(Occupancy_Status_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {

                string wherecondition = string.Empty;
                if (model.Type == 3 && !string.IsNullOrEmpty(model.FilterData))
                {
                    var Data = JsonConvert.DeserializeObject<Occupancy_Status_MasterDTO>(model.FilterData);
                    if (!string.IsNullOrEmpty(Data.OS_Name))
                    {
                        wherecondition = " And os.[OS_Name] LIKE '%" + Data.OS_Name + "%'";
                        //wherecondition = " And Task_Name LIKE '%" + Data.Task_Name + "%'";
                    }

                    if (Data.OS_IsActive == true)
                    {
                        wherecondition = wherecondition + "  And os.[OS_IsActive] =  1 ";
                    }
                    if (Data.OS_IsActive == false)
                    {
                        wherecondition = wherecondition + "  And os.[OS_IsActive] =  0";
                    }
                    model.WhereClause = wherecondition;
                }

                DataSet ds = GetOccupancyStatusMaster(model);

                if (ds.Tables.Count > 0)
                {
                    for (int i = 0; i < ds.Tables.Count; i++)
                    {
                        objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                    }

                }
                //objDynamic.Add(Get_details);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }

            return objDynamic;
        }
    }
}