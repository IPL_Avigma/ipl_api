﻿using Avigma.Repository.Lib;
using IPL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class CustomerNumberData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        //check
        public List<dynamic> AddCustomerNumberData(CustomerNumberDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();

            string insertProcedure = "[CreateUpdateCustomerNumber]";
            CustomerNumberDetails customerNumber = new CustomerNumberDetails();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Cust_Num_pkeyId", 1 + "#bigint#" + model.Cust_Num_pkeyId);
                input_parameters.Add("@Cust_Num_Number", 1 + "#varchar#" + model.Cust_Num_Number);
                input_parameters.Add("@Cust_Num_Active", 1 + "#bit#" + model.Cust_Num_Active);
                input_parameters.Add("@Cust_Num_IsDelete", 1 + "#bit#" + model.Cust_Num_IsDelete);
                input_parameters.Add("@Cust_Num_IsActive", 1 + "#bit#" + model.Cust_Num_IsActive);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Cust_Num_pkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters); ;
                if (objData[1] == 0)
                {
                    customerNumber.Cust_Num_pkeyId = "0";
                    customerNumber.Status = "0";
                    customerNumber.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    customerNumber.Cust_Num_pkeyId = Convert.ToString(objData[0]);
                    customerNumber.Status = Convert.ToString(objData[1]);


                }
                objcltData.Add(customerNumber);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }
            return objcltData;



        }

        private DataSet GetCustomerNumberMaster(CustomerNumberDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_CustomerNumberMaster]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Cust_Num_pkeyId", 1 + "#bigint#" + model.Cust_Num_pkeyId);
                input_parameters.Add("@WhereClause", 1 + "#varchar#" + model.WhereClause);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }



            return ds;
        }

        public List<dynamic> GetCustomerNumberDetails(CustomerNumberDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetCustomerNumberMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<CustomerNumberDTO> CustomerNumberDetails =
                   (from item in myEnumerableFeaprd
                    select new CustomerNumberDTO
                    {
                        Cust_Num_pkeyId = item.Field<Int64>("Cust_Num_pkeyId"),
                        Cust_Num_Number = item.Field<String>("Cust_Num_Number"),
                        Cust_Num_Active = item.Field<Boolean?>("Cust_Num_Active"),
                        Cust_Num_IsActive = item.Field<Boolean?>("Cust_Num_IsActive"),
                        Cust_Num_CreatedBy = item.Field<String>("Cust_Num_CreatedBy"),
                        Cust_Num_ModifiedBy = item.Field<String>("Cust_Num_ModifiedBy"),
                        Cust_Num_IsDeleteAllow = item.Field <Boolean?>("Cust_Num_IsDeleteAllow")
                    }).ToList();

                objDynamic.Add(CustomerNumberDetails);
                if (model.Type == 1)
                {
                    if (ds.Tables.Count > 1)
                    {
                        var myEnumerableFeaprd1 = ds.Tables[1].AsEnumerable();
                        List<Filter_Admin_Customer_MasterDTO> Filtercustomer =
                           (from item in myEnumerableFeaprd1
                            select new Filter_Admin_Customer_MasterDTO
                            {
                                Cust_Filter_PkeyId = item.Field<Int64>("Cust_Filter_PkeyId"),
                                Cust_Filter_Name = item.Field<String>("Cust_Filter_Name"),
                                Cust_Filter_IsCustActive = item.Field<Boolean?>("Cust_Filter_IsCustActive"),
                                Cust_Filter_CreatedBy = item.Field<String>("Cust_Filter_CreatedBy"),
                                Cust_Filter_ModifiedBy = item.Field<String>("Cust_Filter_ModifiedBy"),
                            }).ToList();

                        objDynamic.Add(Filtercustomer);
                    }
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }

            return objDynamic;
        }
        public List<dynamic> GetCustomerNumberFilterDetails(CustomerNumberDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();


            string wherecondition = string.Empty;
            try
            {

                var Data = JsonConvert.DeserializeObject<CustomerNumberDTO>(model.FilterData);
                if (!string.IsNullOrEmpty(Data.Cust_Num_Number))
                {
                    wherecondition = " And Cust_Num_Number LIKE '%" + Data.Cust_Num_Number + "%'";
                }

                if (Data.Cust_Num_IsActive == true)
                {
                    wherecondition = wherecondition + "  And Cust_Num_IsActive =  1";
                }
                if (Data.Cust_Num_IsActive == false)
                {
                    wherecondition = wherecondition + "  And Cust_Num_IsActive =  0";
                }

                CustomerNumberDTO customerNumberDTO = new CustomerNumberDTO();

                customerNumberDTO.WhereClause = wherecondition;
                customerNumberDTO.Type = 3;
                customerNumberDTO.UserID = model.UserID;

                DataSet ds = GetCustomerNumberMaster(customerNumberDTO);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<CustomerNumberDTO> CustomerNumberfilter =
                   (from item in myEnumerableFeaprd
                    select new CustomerNumberDTO
                    {
                        Cust_Num_pkeyId = item.Field<Int64>("Cust_Num_pkeyId"),
                        Cust_Num_Number = item.Field<String>("Cust_Num_Number"),
                        Cust_Num_Active = item.Field<Boolean?>("Cust_Num_Active"),
                        Cust_Num_IsActive = item.Field<Boolean?>("Cust_Num_IsActive"),
                        Cust_Num_CreatedBy = item.Field<String>("Cust_Num_CreatedBy"),
                        Cust_Num_ModifiedBy = item.Field<String>("Cust_Num_ModifiedBy"),
                    }).ToList();

                objDynamic.Add(CustomerNumberfilter);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }

            return objDynamic;
        }

    }


}