﻿using Avigma.Repository.Lib;
using IPLApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPLApp.Repository.data
{
    public class UserFilterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        //Add Filter Details
        public List<dynamic> AddfilterData(UserFilterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();
            string insertProcedure = "[CreateUpdateUserFilterData]";
            UserFilter userFilter = new UserFilter();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@usr_filter_pkeyID", 1 + "#bigint#" + model.usr_filter_pkeyID);
                input_parameters.Add("@usr_filter_WhereData", 1 + "#nvarchar#" + model.usr_filter_WhereData);
                input_parameters.Add("@usr_filter_FilterData", 1 + "#nvarchar#" + model.usr_filter_FilterData);
                input_parameters.Add("@usr_filter_MenuId", 1 + "#bigint#" + model.usr_filter_MenuId);
                input_parameters.Add("@usr_filter_UserID", 1 + "#bigint#" + model.usr_filter_UserID);
                input_parameters.Add("@usr_filter_IsActive", 1 + "#bit#" + model.usr_filter_IsActive);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@usr_filter_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    userFilter.usr_filter_pkeyID = "0";
                    userFilter.Status = "0";
                    userFilter.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    userFilter.usr_filter_pkeyID = Convert.ToString(objData[0]);
                    userFilter.Status = Convert.ToString(objData[1]);


                }
                objcltData.Add(userFilter);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;



        }
    }
}