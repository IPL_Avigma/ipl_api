﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class PCRDamageMasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public List<dynamic> AddPCR_Damage_Data(PCRDamageMasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objclntData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_PCR_Damage_Master]";
            PcrDamageDto pcrDamageDto = new PcrDamageDto();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@PCR_Damage_pkeyId", 1 + "#bigint#" + model.PCR_Damage_pkeyId);
                input_parameters.Add("@PCR_Damage_MasterId", 1 + "#bigint#" + model.PCR_Damage_MasterId);
                input_parameters.Add("@PCR_Damage_WO_Id", 1 + "#bigint#" + model.PCR_Damage_WO_Id);
                input_parameters.Add("@PCR_Damage_ValType", 1 + "#int#" + model.PCR_Damage_ValType);
                input_parameters.Add("@PCR_Damage_Fire_Smoke_Damage_Yes", 1 + "#varchar#" + model.PCR_Damage_Fire_Smoke_Damage_Yes);
                input_parameters.Add("@PCR_Damage_Mortgagor_Neglect_Yes", 1 + "#varchar#" + model.PCR_Damage_Mortgagor_Neglect_Yes);
                input_parameters.Add("@PCR_Damage_Vandalism_Yes", 1 + "#varchar#" + model.PCR_Damage_Vandalism_Yes);
                input_parameters.Add("@PCR_Damage_Freeze_Damage_Yes", 1 + "#varchar#" + model.PCR_Damage_Freeze_Damage_Yes);
                input_parameters.Add("@PCR_Damage_Storm_Damage_Yes", 1 + "#varchar#" + model.PCR_Damage_Storm_Damage_Yes);
                input_parameters.Add("@PCR_Damage_Flood_Damage_Yes", 1 + "#varchar#" + model.PCR_Damage_Flood_Damage_Yes);
                input_parameters.Add("@PCR_Damage_Water_Damage_Yes", 1 + "#varchar#" + model.PCR_Damage_Water_Damage_Yes);
                input_parameters.Add("@PCR_Damage_Wear_And_Tear_Yes", 1 + "#varchar#" + model.PCR_Damage_Wear_And_Tear_Yes);
                input_parameters.Add("@PCR_Damage_Unfinished_Renovation_Yes", 1 + "#varchar#" + model.PCR_Damage_Unfinished_Renovation_Yes);
                input_parameters.Add("@PCR_Damage_Structural_Damage_Yes", 1 + "#varchar#" + model.PCR_Damage_Structural_Damage_Yes);
                input_parameters.Add("@PCR_Damage_Excessive_Humidty_Yes", 1 + "#varchar#" + model.PCR_Damage_Excessive_Humidty_Yes);
                input_parameters.Add("@PCR_Urgent_Damages_Roof_Leak_Yes", 1 + "#varchar#" + model.PCR_Urgent_Damages_Roof_Leak_Yes);
                input_parameters.Add("@PCR_Urgent_Damages_Roof_Traped_Yes", 1 + "#varchar#" + model.PCR_Urgent_Damages_Roof_Traped_Yes);
                input_parameters.Add("@PCR_Urgent_Damages_Mold_Damage_Yes", 1 + "#varchar#" + model.PCR_Urgent_Damages_Mold_Damage_Yes);
                input_parameters.Add("@PCR_Urgent_Damages_SeePage_Yes", 1 + "#varchar#" + model.PCR_Urgent_Damages_SeePage_Yes);
                input_parameters.Add("@PCR_Urgent_Damages_Flooded_Basement_Yes", 1 + "#varchar#" + model.PCR_Urgent_Damages_Flooded_Basement_Yes);
                input_parameters.Add("@PCR_Urgent_Damages_Foundation_Cracks_Yes", 1 + "#varchar#" + model.PCR_Urgent_Damages_Foundation_Cracks_Yes);
                input_parameters.Add("@PCR_Urgent_Damages_Wet_Carpet_Yes", 1 + "#varchar#" + model.PCR_Urgent_Damages_Wet_Carpet_Yes);
                input_parameters.Add("@PCR_Urgent_Damages_Water_Stains_Yes", 1 + "#varchar#" + model.PCR_Urgent_Damages_Water_Stains_Yes);
                input_parameters.Add("@PCR_Urgent_Damages_Floors_Safety_Yes", 1 + "#varchar#" + model.PCR_Urgent_Damages_Floors_Safety_Yes);
                input_parameters.Add("@PCR_Urgent_Damages_Other_Causing_Damage_Yes", 1 + "#varchar#" + model.PCR_Urgent_Damages_Other_Causing_Damage_Yes);
                input_parameters.Add("@PCR_Urgent_Damages_Other_Safety_Issue_Yes", 1 + "#varchar#" + model.PCR_Urgent_Damages_Other_Safety_Issue_Yes);
                input_parameters.Add("@PCR_System_Damages_HVAC_System_Damage_Yes", 1 + "#varchar#" + model.PCR_System_Damages_HVAC_System_Damage_Yes);
                input_parameters.Add("@PCR_System_Damages_Electric_Damage_Yes", 1 + "#varchar#" + model.PCR_System_Damages_Electric_Damage_Yes);
                input_parameters.Add("@PCR_System_Damages_Plumbing_Damage_Yes", 1 + "#varchar#" + model.PCR_System_Damages_Plumbing_Damage_Yes);
                input_parameters.Add("@PCR_System_Damages_Uncapped_Wire_Yes", 1 + "#varchar#" + model.PCR_System_Damages_Uncapped_Wire_Yes);
                input_parameters.Add("@PCR_Damages_FEMA_Damages_Yes", 1 + "#varchar#" + model.PCR_Damages_FEMA_Damages_Yes);
                input_parameters.Add("@PCR_Damages_FEMA_Neighborhood_Level_Light", 1 + "#varchar#" + model.PCR_Damages_FEMA_Neighborhood_Level_Light);
                input_parameters.Add("@PCR_Damages_FEMA_Trailer_Present", 1 + "#varchar#" + model.PCR_Damages_FEMA_Trailer_Present);
                input_parameters.Add("@PCR_Damages_FEMA_Property_Level_Light_Moderate", 1 + "#varchar#" + model.PCR_Damages_FEMA_Property_Level_Light_Moderate);
                input_parameters.Add("@PCR_Damages_Property_Habitable", 1 + "#varchar#" + model.PCR_Damages_Property_Habitable);
                input_parameters.Add("@PCR_Damages_Property_Habitable_FEMA_Damage_Cause_By_Wind", 1 + "#bit#" + model.PCR_Damages_Property_Habitable_FEMA_Damage_Cause_By_Wind);
                input_parameters.Add("@PCR_Damages_Property_Habitable_FEMA_Damage_Cause_By_Water", 1 + "#bit#" + model.PCR_Damages_Property_Habitable_FEMA_Damage_Cause_By_Water);
                input_parameters.Add("@PCR_Damages_Property_Habitable_FEMA_Damage_Cause_By_Fire", 1 + "#bit#" + model.PCR_Damages_Property_Habitable_FEMA_Damage_Cause_By_Fire);
                input_parameters.Add("@PCR_Damages_Property_Habitable_FEMA_Damage_Cause_By_Flood", 1 + "#bit#" + model.PCR_Damages_Property_Habitable_FEMA_Damage_Cause_By_Flood);
                input_parameters.Add("@PCR_Damages_FEMA_Damage_Estimate", 1 + "#varchar#" + model.PCR_Damages_FEMA_Damage_Estimate);
                input_parameters.Add("@PCR_Damages_Damage", 1 + "#int#" + model.PCR_Damages_Damage);
                input_parameters.Add("@PCR_Damages_Status", 1 + "#int#" + model.PCR_Damages_Status);
                input_parameters.Add("@PCR_Damages_Cause", 1 + "#int#" + model.PCR_Damages_Cause);
                input_parameters.Add("@PCR_Damages_Int_Ext", 1 + "#int#" + model.PCR_Damages_Int_Ext);
                input_parameters.Add("@PCR_Damages_Building", 1 + "#int#" + model.PCR_Damages_Building);
                input_parameters.Add("@PCR_Damages_Room", 1 + "#int#" + model.PCR_Damages_Room);
                input_parameters.Add("@PCR_Damages_Description", 1 + "#varchar#" + model.PCR_Damages_Description);
                input_parameters.Add("@PCR_Damages_Qty", 1 + "#varchar#" + model.PCR_Damages_Qty);
                input_parameters.Add("@PCR_Damages_Estimate", 1 + "#varchar#" + model.PCR_Damages_Estimate);
                input_parameters.Add("@PCR_Damages_IsActive", 1 + "#bit#" + model.PCR_Damages_IsActive);
                input_parameters.Add("@PCR_Damages_IsDelete", 1 + "#bit#" + model.PCR_Damages_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@fwo_pkyeId", 1 + "#bigint#" + model.fwo_pkyeId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@PCR_Damage_pkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    pcrDamageDto.PCR_Damage_pkeyId = "0";
                    pcrDamageDto.Status = "0";
                    pcrDamageDto.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    pcrDamageDto.PCR_Damage_pkeyId = Convert.ToString(objData[0]);
                    pcrDamageDto.Status = Convert.ToString(objData[1]);


                }
                objclntData.Add(pcrDamageDto);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objclntData;



        }

        private DataSet GetPCRDamageMaster(PCRDamageMasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_PCR_Damage_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@PCR_Damage_pkeyId", 1 + "#bigint#" + model.PCR_Damage_pkeyId);
                input_parameters.Add("@PCR_Damage_WO_Id", 1 + "#bigint#" + model.PCR_Damage_WO_Id);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetPCRDamageDetails(PCRDamageMasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetPCRDamageMaster(model);

                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                }

                //var PCRDamge = DatatableToModel(ds.Tables[0]);
                //objDynamic.Add(PCRDamge);

                //if (ds.Tables.Count > 1)
                //{
                //    var History = DatatableToModel(ds.Tables[1]);
                //    objDynamic.Add(History);
                //}
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
        private List<PCRDamageMasterDTO> DatatableToModel(DataTable dataTable)
        {
            var myEnumerableFeaprd = dataTable.AsEnumerable();
            List<PCRDamageMasterDTO> PCRDamge =
               (from item in myEnumerableFeaprd
                select new PCRDamageMasterDTO
                {
                    PCR_Damage_pkeyId = item.Field<Int64>("PCR_Damage_pkeyId"),
                    PCR_Damage_MasterId = item.Field<Int64>("PCR_Damage_MasterId"),
                    PCR_Damage_WO_Id = item.Field<Int64>("PCR_Damage_WO_Id"),
                    PCR_Damage_ValType = item.Field<int?>("PCR_Damage_ValType"),
                    PCR_Damage_Fire_Smoke_Damage_Yes = item.Field<String>("PCR_Damage_Fire_Smoke_Damage_Yes"),
                    PCR_Damage_Mortgagor_Neglect_Yes = item.Field<String>("PCR_Damage_Mortgagor_Neglect_Yes"),
                    PCR_Damage_Vandalism_Yes = item.Field<String>("PCR_Damage_Vandalism_Yes"),
                    PCR_Damage_Freeze_Damage_Yes = item.Field<String>("PCR_Damage_Freeze_Damage_Yes"),
                    PCR_Damage_Storm_Damage_Yes = item.Field<String>("PCR_Damage_Storm_Damage_Yes"),
                    PCR_Damage_Flood_Damage_Yes = item.Field<String>("PCR_Damage_Flood_Damage_Yes"),
                    PCR_Damage_Water_Damage_Yes = item.Field<String>("PCR_Damage_Water_Damage_Yes"),
                    PCR_Damage_Wear_And_Tear_Yes = item.Field<String>("PCR_Damage_Wear_And_Tear_Yes"),
                    PCR_Damage_Unfinished_Renovation_Yes = item.Field<String>("PCR_Damage_Unfinished_Renovation_Yes"),
                    PCR_Damage_Structural_Damage_Yes = item.Field<String>("PCR_Damage_Structural_Damage_Yes"),
                    PCR_Damage_Excessive_Humidty_Yes = item.Field<String>("PCR_Damage_Excessive_Humidty_Yes"),
                    PCR_Urgent_Damages_Roof_Leak_Yes = item.Field<String>("PCR_Urgent_Damages_Roof_Leak_Yes"),
                    PCR_Urgent_Damages_Roof_Traped_Yes = item.Field<String>("PCR_Urgent_Damages_Roof_Traped_Yes"),
                    PCR_Urgent_Damages_Mold_Damage_Yes = item.Field<String>("PCR_Urgent_Damages_Mold_Damage_Yes"),
                    PCR_Urgent_Damages_SeePage_Yes = item.Field<String>("PCR_Urgent_Damages_SeePage_Yes"),
                    PCR_Urgent_Damages_Flooded_Basement_Yes = item.Field<String>("PCR_Urgent_Damages_Flooded_Basement_Yes"),
                    PCR_Urgent_Damages_Foundation_Cracks_Yes = item.Field<String>("PCR_Urgent_Damages_Foundation_Cracks_Yes"),
                    PCR_Urgent_Damages_Wet_Carpet_Yes = item.Field<String>("PCR_Urgent_Damages_Wet_Carpet_Yes"),
                    PCR_Urgent_Damages_Water_Stains_Yes = item.Field<String>("PCR_Urgent_Damages_Water_Stains_Yes"),
                    PCR_Urgent_Damages_Floors_Safety_Yes = item.Field<String>("PCR_Urgent_Damages_Floors_Safety_Yes"),
                    PCR_Urgent_Damages_Other_Causing_Damage_Yes = item.Field<String>("PCR_Urgent_Damages_Other_Causing_Damage_Yes"),
                    PCR_Urgent_Damages_Other_Safety_Issue_Yes = item.Field<String>("PCR_Urgent_Damages_Other_Safety_Issue_Yes"),
                    PCR_System_Damages_HVAC_System_Damage_Yes = item.Field<String>("PCR_System_Damages_HVAC_System_Damage_Yes"),
                    PCR_System_Damages_Electric_Damage_Yes = item.Field<String>("PCR_System_Damages_Electric_Damage_Yes"),
                    PCR_System_Damages_Plumbing_Damage_Yes = item.Field<String>("PCR_System_Damages_Plumbing_Damage_Yes"),
                    PCR_System_Damages_Uncapped_Wire_Yes = item.Field<String>("PCR_System_Damages_Uncapped_Wire_Yes"),
                    PCR_Damages_FEMA_Damages_Yes = item.Field<String>("PCR_Damages_FEMA_Damages_Yes"),
                    PCR_Damages_FEMA_Neighborhood_Level_Light = item.Field<String>("PCR_Damages_FEMA_Neighborhood_Level_Light"),
                    PCR_Damages_FEMA_Trailer_Present = item.Field<String>("PCR_Damages_FEMA_Trailer_Present"),
                    PCR_Damages_FEMA_Property_Level_Light_Moderate = item.Field<String>("PCR_Damages_FEMA_Property_Level_Light_Moderate"),
                    PCR_Damages_Property_Habitable = item.Field<String>("PCR_Damages_Property_Habitable"),
                    PCR_Damages_Property_Habitable_FEMA_Damage_Cause_By_Wind = item.Field<Boolean?>("PCR_Damages_Property_Habitable_FEMA_Damage_Cause_By_Wind"),
                    PCR_Damages_Property_Habitable_FEMA_Damage_Cause_By_Water = item.Field<Boolean?>("PCR_Damages_Property_Habitable_FEMA_Damage_Cause_By_Water"),
                    PCR_Damages_Property_Habitable_FEMA_Damage_Cause_By_Fire = item.Field<Boolean?>("PCR_Damages_Property_Habitable_FEMA_Damage_Cause_By_Fire"),
                    PCR_Damages_Property_Habitable_FEMA_Damage_Cause_By_Flood = item.Field<Boolean?>("PCR_Damages_Property_Habitable_FEMA_Damage_Cause_By_Flood"),
                    PCR_Damages_FEMA_Damage_Estimate = item.Field<String>("PCR_Damages_FEMA_Damage_Estimate"),
                    PCR_Damages_Damage = item.Field<int?>("PCR_Damages_Damage"),
                    PCR_Damages_Status = item.Field<int?>("PCR_Damages_Status"),
                    PCR_Damages_Cause = item.Field<int?>("PCR_Damages_Cause"),
                    PCR_Damages_Int_Ext = item.Field<int?>("PCR_Damages_Int_Ext"),
                    PCR_Damages_Building = item.Field<int?>("PCR_Damages_Building"),
                    PCR_Damages_Room = item.Field<int?>("PCR_Damages_Room"),
                    PCR_Damages_Description = item.Field<String>("PCR_Damages_Description"),
                    PCR_Damages_Qty = item.Field<String>("PCR_Damages_Qty"),
                    PCR_Damages_Estimate = item.Field<String>("PCR_Damages_Estimate"),
                    PCR_Damages_IsActive = item.Field<Boolean?>("PCR_Damages_IsActive"),



                }).ToList();

            return PCRDamge;
        }
    }
}