﻿using Avigma.Repository.Lib;
using IPL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class Folder_File_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        public List<dynamic> AddFolder_File_Master(Folder_File_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            log.logInfoMessage("file processing  ----- Start" + model);
           

            string insertProcedure = "[CreateUpdate_Folder_File_Master]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Fold_File_Pkey_Id", 1 + "#bigint#" + model.Fold_File_Pkey_Id);
                input_parameters.Add("@Fold_File_ParentId", 1 + "#bigint#" + model.Fold_File_ParentId);
                input_parameters.Add("@Fold_File_Role_Folder_Id", 1 + "#bigint#" + model.Fold_File_Role_Folder_Id);
                input_parameters.Add("@Fold_File_Name", 1 + "#varchar#" + model.Fold_File_Name);
                input_parameters.Add("@Fold_File_Local_Path", 1 + "#varchar#" + model.Fold_File_Local_Path);
                input_parameters.Add("@Fold_File_Bucket_Name", 1 + "#varchar#" + model.Fold_File_Bucket_Name);
               
                input_parameters.Add("@Fold_File_ProjectId", 1 + "#varchar#" + model.Fold_File_ProjectId);
                input_parameters.Add("@Fold_File_Object_Name", 1 + "#varchar#" + model.Fold_File_Object_Name);
                input_parameters.Add("@Fold_File_Folder_Name", 1 + "#varchar#" + model.Fold_File_Folder_Name);
                input_parameters.Add("@Fold_File_Desc", 1 + "#nvarchar#" + model.Fold_File_Desc);
                input_parameters.Add("@Fold_File_IsActive", 1 + "#bit#" + model.Fold_File_IsActive);
                input_parameters.Add("@Fold_File_IsDelete", 1 + "#bit#" + model.Fold_File_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Fold_Is_AutoAssign", 1 + "#bit#" + model.Fold_Is_AutoAssign);

                input_parameters.Add("@Fold_WorkType_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;



        }
        public List<dynamic> AddFormsDocFolder_File_Master(Client_Result_PhotoDTO client_Result_PhotoDTO)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                log.logDebugMessage("File Details===" + client_Result_PhotoDTO.UserID);
                Folder_File_MasterDTO folder_File_MasterDTO = new Folder_File_MasterDTO();
                folder_File_MasterDTO.Fold_File_Pkey_Id = client_Result_PhotoDTO.Client_Result_Photo_ID;
                folder_File_MasterDTO.Fold_File_ParentId = client_Result_PhotoDTO.Client_Result_Photo_Ch_ID;
                folder_File_MasterDTO.Fold_File_Name = client_Result_PhotoDTO.Client_Result_Photo_FileName;
                folder_File_MasterDTO.Fold_File_Local_Path = client_Result_PhotoDTO.Client_Result_Photo_FilePath;
                folder_File_MasterDTO.Fold_File_Bucket_Name = client_Result_PhotoDTO.Client_Result_Photo_BucketName;
                folder_File_MasterDTO.Fold_File_ProjectId = client_Result_PhotoDTO.IPLNO;
                folder_File_MasterDTO.Fold_File_Object_Name = client_Result_PhotoDTO.Client_Result_Photo_objectName;
                folder_File_MasterDTO.Fold_File_Folder_Name = client_Result_PhotoDTO.IPLNO;
                folder_File_MasterDTO.Fold_File_Desc = client_Result_PhotoDTO.Client_Result_File_Desc;
                folder_File_MasterDTO.Fold_File_IsActive = client_Result_PhotoDTO.Client_Result_Photo_IsActive;
                folder_File_MasterDTO.Fold_File_IsDelete = client_Result_PhotoDTO.Client_Result_Photo_IsDelete;
                folder_File_MasterDTO.Type = client_Result_PhotoDTO.Type;
                folder_File_MasterDTO.UserID = client_Result_PhotoDTO.UserID;
                folder_File_MasterDTO.Fold_Is_AutoAssign = client_Result_PhotoDTO.Fold_Is_AutoAssign;
                objData = AddFolder_File_Master(folder_File_MasterDTO);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

            }
            return objData;
        }


        private DataSet GetFolderFileMaster(Folder_File_MasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Folder_File_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Fold_File_Pkey_Id", 1 + "#bigint#" + model.Fold_File_Pkey_Id);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetFolderFileMasterDetails(Folder_File_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetFolderFileMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Folder_File_MasterDTO> FileDetails =
                   (from item in myEnumerableFeaprd
                    select new Folder_File_MasterDTO
                    {
                        Fold_File_Pkey_Id = item.Field<Int64>("Fold_File_Pkey_Id"),
                        Fold_File_ParentId = item.Field<Int64?>("Fold_File_ParentId"),
                        Fold_File_Role_Folder_Id = item.Field<Int64?>("Fold_File_Role_Folder_Id"),
                        Fold_File_Name = item.Field<String>("Fold_File_Name"),
                        Fold_File_Local_Path = item.Field<String>("Fold_File_Local_Path"),
                        Fold_File_Bucket_Name = item.Field<String>("Fold_File_Bucket_Name"),
                        Fold_File_Object_Name = item.Field<String>("Fold_File_Object_Name"),
                        Fold_File_Folder_Name = item.Field<String>("Fold_File_Folder_Name"),
                        Fold_File_IsActive = item.Field<Boolean?>("Fold_File_IsActive"),
                        Fold_File_Desc = item.Field<String>("Fold_File_Desc"),
                        Fold_Is_AutoAssign = item.Field<Boolean?>("Fold_Is_AutoAssign"),
                    }).ToList();

                objDynamic.Add(FileDetails);

                if (ds.Tables.Count > 0)
                {
                    var myEnumerableFeaprdass = ds.Tables[1].AsEnumerable();
                    List<Folder_Auto_Assine_Master_DTO> assdata =
                       (from item in myEnumerableFeaprdass
                        select new Folder_Auto_Assine_Master_DTO
                        {
                            Fold_Auto_Assine_PkeyId = item.Field<Int64>("Fold_Auto_Assine_PkeyId"),
                            Fold_Parent_Id = item.Field<Int64?>("Fold_Parent_Id"),
                            Fold_Auto_Assine_Client = item.Field<String>("Fold_Auto_Assine_Client"),
                            Fold_Auto_Assine_Customer = item.Field<String>("Fold_Auto_Assine_Customer"),
                            Fold_Auto_Assine_LoneType = item.Field<String>("Fold_Auto_Assine_LoneType"),
                            Fold_Auto_Assine_WorkType = item.Field<String>("Fold_Auto_Assine_WorkType"),
                            Fold_Auto_Assine_WorkType_Group = item.Field<String>("Fold_Auto_Assine_WorkType_Group"),
                            Fold_Auto_Assine_State = item.Field<String>("Fold_Auto_Assine_State"),
                            Fold_Auto_Assine_County = item.Field<String>("Fold_Auto_Assine_County"),
                            Fold_Auto_Assine_Zip = item.Field<Int64?>("Fold_Auto_Assine_Zip"),
                            Fold_Auto_Assine_IsActive = item.Field<Boolean?>("Fold_Auto_Assine_IsActive"),
                            Folder_File_Master_FK_Id = item.Field<Int64?>("Folder_File_Master_FK_Id"),

                        }).ToList();

                    objDynamic.Add(assdata);
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
        public List<dynamic> AddUpdateFileMasterDetails(Folder_File_MasterDTO model)
        {

            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objAddData = new List<dynamic>();

            objData = AddFolder_File_Master(model);
            if (model.Type == 2) // Added By Dipali
            {
                Delete_File_AutoAssignReference(model);

                var objAutoAssign = AddUpdate_File_AutoAssignReference(objData[0], model);
            }
            objAddData.Add(objData);
            return objAddData;
        }
        public void Delete_File_AutoAssignReference(Folder_File_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[Delete_File_AutoAssignReference]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Fold_File_Pkey_Id", 1 + "#bigint#" + model.Fold_File_Pkey_Id);                

                objData = obj.SqlCRUD(insertProcedure, input_parameters);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
        }
        public List<dynamic> AddUpdate_File_AutoAssignReference(Int64? folder_File_Master_FK_Id, Folder_File_MasterDTO model)
        {
            List<dynamic> objAddData = new List<dynamic>();
            if (model.Fold_Is_AutoAssign == true)
            {
                try
                {
                    if (model != null && model.AutoAssinArray != null && model.AutoAssinArray.Count > 0)
                    {
                        for (int i = 0; i < model.AutoAssinArray.Count; i++)
                        {
                            Folder_Auto_Assine_MasterData folder_Auto_Assine_MasterData = new Folder_Auto_Assine_MasterData();
                            Folder_Auto_Assine_Master_DTO folder_Auto_Assine_Master_DTO = new Folder_Auto_Assine_Master_DTO();

                            if (model.Type == 1)
                            {
                                if (folder_File_Master_FK_Id != null)
                                {
                                    folder_Auto_Assine_Master_DTO.Folder_File_Master_FK_Id = folder_File_Master_FK_Id;
                                }
                            }
                            else
                            {
                                folder_Auto_Assine_Master_DTO.Folder_File_Master_FK_Id = model.Fold_File_Pkey_Id;
                            }
                            folder_Auto_Assine_Master_DTO.Fold_Auto_Assine_PkeyId = model.AutoAssinArray[i].Fold_Auto_Assine_PkeyId;
                            folder_Auto_Assine_Master_DTO.Fold_Auto_Assine_Client = JsonConvert.SerializeObject(model.AutoAssinArray[i].Task_sett_Company);
                            folder_Auto_Assine_Master_DTO.Fold_Auto_Assine_County = JsonConvert.SerializeObject(model.AutoAssinArray[i].Task_sett_Country);
                            folder_Auto_Assine_Master_DTO.Fold_Auto_Assine_Customer = JsonConvert.SerializeObject(model.AutoAssinArray[i].Task_sett_Customer);
                            folder_Auto_Assine_Master_DTO.Fold_Auto_Assine_LoneType = JsonConvert.SerializeObject(model.AutoAssinArray[i].Task_sett_Lone);
                            folder_Auto_Assine_Master_DTO.Fold_Auto_Assine_State = JsonConvert.SerializeObject(model.AutoAssinArray[i].Task_sett_State);
                            folder_Auto_Assine_Master_DTO.Fold_Auto_Assine_WorkType = JsonConvert.SerializeObject(model.AutoAssinArray[i].WTTaskWorkType);
                            folder_Auto_Assine_Master_DTO.Fold_Auto_Assine_WorkType_Group = JsonConvert.SerializeObject(model.AutoAssinArray[i].Task_Work_TypeGroup);
                            folder_Auto_Assine_Master_DTO.Fold_Auto_Assine_Zip = Convert.ToInt64(model.AutoAssinArray[i].Task_sett_Zip);
                            folder_Auto_Assine_Master_DTO.Fold_Auto_Assine_IsActive = model.AutoAssinArray[i].Task_sett_IsActive;
                            folder_Auto_Assine_Master_DTO.Fold_Auto_Assine_IsDelete = model.AutoAssinArray[i].Task_sett_IsDelete;
                            folder_Auto_Assine_Master_DTO.UserID = model.UserID;
                            if (model.AutoAssinArray[i].Fold_Auto_Assine_PkeyId != 0)
                            {
                                folder_Auto_Assine_Master_DTO.Type = 2;
                                folder_Auto_Assine_Master_DTO.Fold_Auto_Assine_IsActive = true;
                                folder_Auto_Assine_Master_DTO.Fold_Auto_Assine_IsDelete = false;
                            }
                            else
                            {
                                folder_Auto_Assine_Master_DTO.Type = 1;
                            }
                            var objautoasn = folder_Auto_Assine_MasterData.AddFolderAutoAssineMasterjson(folder_Auto_Assine_Master_DTO);


                            if (model.AutoAssinArray[i].Task_sett_Company != null)
                            {
                                for (int j = 0; j < model.AutoAssinArray[i].Task_sett_Company.Count; j++)
                                {
                                    Folder_Client_MasterData folder_Client_MasterData = new Folder_Client_MasterData();
                                    Folder_Client_MasterDTO folder_Client_MasterDTO = new Folder_Client_MasterDTO();
                                    folder_Client_MasterDTO.Fold_Client_PkeyId = 0;
                                    folder_Client_MasterDTO.Fold_Client_Id = model.AutoAssinArray[i].Task_sett_Company[j].Client_pkeyID;
                                    folder_Client_MasterDTO.Fold_Client_Auto_Assine_Id = objautoasn[0];
                                    folder_Client_MasterDTO.Folder_File_Master_FK_Id = folder_File_Master_FK_Id;
                                    folder_Client_MasterDTO.Fold_Client_IsActive = true;
                                    folder_Client_MasterDTO.Fold_Client_IsDelete = false;
                                    folder_Client_MasterDTO.UserID = model.UserID;
                                    folder_Client_MasterDTO.Type = 1;

                                    var objclient = folder_Client_MasterData.AddFolder_Client_Master(folder_Client_MasterDTO);
                                }
                            }

                            if (model.AutoAssinArray[i].Task_sett_Country != null)
                            {
                                for (int k = 0; k < model.AutoAssinArray[i].Task_sett_Country.Count; k++)
                                {
                                    Folder_County_MasterData folder_County_MasterData = new Folder_County_MasterData();
                                    Folder_County_MasterDTO folder_County_MasterDTO = new Folder_County_MasterDTO();

                                    folder_County_MasterDTO.Fold_County_Auto_Assine_Id = objautoasn[0];
                                    folder_County_MasterDTO.Folder_File_Master_FK_Id = folder_File_Master_FK_Id;
                                    folder_County_MasterDTO.Fold_County_Id = model.AutoAssinArray[i].Task_sett_Country[k].ID;
                                    folder_County_MasterDTO.Fold_County_PkeyId = 0;
                                    folder_County_MasterDTO.Fold_County_IsActive = true;
                                    folder_County_MasterDTO.Fold_County_IsDelete = false;
                                    folder_County_MasterDTO.UserID = model.UserID;
                                    folder_County_MasterDTO.Type = 1;

                                    var countydata = folder_County_MasterData.AddFolder_County_Master(folder_County_MasterDTO);
                                }

                            }
                            if (model.AutoAssinArray[i].Task_sett_Customer != null)
                            {
                                for (int m = 0; m < model.AutoAssinArray[i].Task_sett_Customer.Count; m++)
                                {
                                    Folder_Customer_MasterData folder_Customer_MasterData = new Folder_Customer_MasterData();
                                    Folder_Customer_MasterDTO folder_Customer_MasterDTO = new Folder_Customer_MasterDTO();

                                    folder_Customer_MasterDTO.Fold_Customer_Auto_Assine_Id = objautoasn[0];
                                    folder_Customer_MasterDTO.Folder_File_Master_FK_Id = folder_File_Master_FK_Id;
                                    folder_Customer_MasterDTO.Fold_Customer_Id = model.AutoAssinArray[i].Task_sett_Customer[m].Cust_Num_pkeyId;
                                    folder_Customer_MasterDTO.Fold_Customer_PkeyId = 0;
                                    folder_Customer_MasterDTO.Fold_Customer_IsActive = true;
                                    folder_Customer_MasterDTO.Fold_Customer_IsDelete = false;
                                    folder_Customer_MasterDTO.UserID = model.UserID;
                                    folder_Customer_MasterDTO.Type = 1;

                                    var customerdata = folder_Customer_MasterData.AddFolder_Customer_Master(folder_Customer_MasterDTO);

                                }
                            }
                            if (model.AutoAssinArray[i].Task_sett_Lone != null)
                            {
                                for (int n = 0; n < model.AutoAssinArray[i].Task_sett_Lone.Count; n++)
                                {
                                    Folder_LoneType_MasterData folder_LoneType_MasterData = new Folder_LoneType_MasterData();
                                    Folder_LoneType_MasterDTO folder_LoneType_MasterDTO = new Folder_LoneType_MasterDTO();

                                    folder_LoneType_MasterDTO.Fold_LoneType_Auto_Assine_Id = objautoasn[0];
                                    folder_LoneType_MasterDTO.Folder_File_Master_FK_Id = folder_File_Master_FK_Id;
                                    folder_LoneType_MasterDTO.Fold_LoneType_Id = model.AutoAssinArray[i].Task_sett_Lone[n].Loan_pkeyId;
                                    folder_LoneType_MasterDTO.Fold_LoneType_PkeyId = 0;
                                    folder_LoneType_MasterDTO.Fold_LoneType_IsActive = true;
                                    folder_LoneType_MasterDTO.Fold_LoneType_IsDelete = false;
                                    folder_LoneType_MasterDTO.UserID = model.UserID;
                                    folder_LoneType_MasterDTO.Type = 1;

                                    var lonedata = folder_LoneType_MasterData.AddFolder_LoneType_Master(folder_LoneType_MasterDTO);

                                }
                            }
                            if (model.AutoAssinArray[i].Task_sett_State != null)
                            {
                                for (int p = 0; p < model.AutoAssinArray[i].Task_sett_State.Count; p++)
                                {
                                    Folder_State_MasterData folder_State_MasterData = new Folder_State_MasterData();
                                    Folder_State_MasterDTO folder_State_MasterDTO = new Folder_State_MasterDTO();

                                    folder_State_MasterDTO.Fold_State_Auto_Assine_Id = objautoasn[0];
                                    folder_State_MasterDTO.Folder_File_Master_FK_Id = folder_File_Master_FK_Id;
                                    folder_State_MasterDTO.Fold_State_Id = model.AutoAssinArray[i].Task_sett_State[p].IPL_StateID;
                                    folder_State_MasterDTO.Fold_State_PkeyId = 0;
                                    folder_State_MasterDTO.Fold_State_IsActive = true;
                                    folder_State_MasterDTO.Fold_State_IsDelete = false;
                                    folder_State_MasterDTO.UserID = model.UserID;
                                    folder_State_MasterDTO.Type = 1;

                                    var statedata = folder_State_MasterData.AddFolder_State_Master(folder_State_MasterDTO);
                                }
                            }



                            if (model.AutoAssinArray[i].WTTaskWorkType != null)
                            {
                                for (int q = 0; q < model.AutoAssinArray[i].WTTaskWorkType.Count; q++)
                                {
                                    Folder_WorkType_MasterData folder_WorkType_MasterData = new Folder_WorkType_MasterData();
                                    Folder_WorkType_MasterDTO folder_WorkType_MasterDTO = new Folder_WorkType_MasterDTO();

                                    folder_WorkType_MasterDTO.Fold_WorkType_Auto_Assine_Id = objautoasn[0];
                                    folder_WorkType_MasterDTO.Folder_File_Master_FK_Id = folder_File_Master_FK_Id;
                                    folder_WorkType_MasterDTO.Fold_WorkType_Id = model.AutoAssinArray[i].WTTaskWorkType[q].WT_pkeyID;
                                    folder_WorkType_MasterDTO.Fold_WorkType_PkeyId = 0;
                                    folder_WorkType_MasterDTO.Fold_WorkType_IsActive = true;
                                    folder_WorkType_MasterDTO.Fold_WorkType_IsDelete = false;
                                    folder_WorkType_MasterDTO.UserID = model.UserID;
                                    folder_WorkType_MasterDTO.Type = 1;

                                    var worktypedata = folder_WorkType_MasterData.AddFolder_WorkType_Master(folder_WorkType_MasterDTO);
                                }
                            }
                            if (model.AutoAssinArray[i].Task_Work_TypeGroup != null)
                            {
                                for (int r = 0; r < model.AutoAssinArray[i].Task_Work_TypeGroup.Count; r++)
                                {
                                    Folder_WorkType_Group_MasterData folder_WorkType_Group_MasterData = new Folder_WorkType_Group_MasterData();
                                    Folder_WorkType_Group_MasterDTO folder_WorkType_Group_MasterDTO = new Folder_WorkType_Group_MasterDTO();

                                    folder_WorkType_Group_MasterDTO.Fold_WorkType_Group_Auto_Assine_Id = objautoasn[0];
                                    folder_WorkType_Group_MasterDTO.Folder_File_Master_FK_Id = folder_File_Master_FK_Id;
                                    folder_WorkType_Group_MasterDTO.Fold_WorkType_Group_Id = model.AutoAssinArray[i].Task_Work_TypeGroup[r].Work_Type_Cat_pkeyID;
                                    folder_WorkType_Group_MasterDTO.Fold_WorkType_Group_PkeyId = 0;
                                    folder_WorkType_Group_MasterDTO.Fold_WorkType_Group_IsActive = true;
                                    folder_WorkType_Group_MasterDTO.Fold_WorkType_Group_IsDelete = false;
                                    folder_WorkType_Group_MasterDTO.UserID = model.UserID;
                                    folder_WorkType_Group_MasterDTO.Type = 1;

                                    var workgroup = folder_WorkType_Group_MasterData.AddFolder_WorkType_Group_Master(folder_WorkType_Group_MasterDTO);
                                }
                            }
                        }
                    }
                    objAddData.Add(folder_File_Master_FK_Id);
                }
                catch (Exception ex)
                {
                    log.logErrorMessage(ex.StackTrace);
                    log.logErrorMessage(ex.Message);
                }
            }
            
            return objAddData;
        }
    }
}