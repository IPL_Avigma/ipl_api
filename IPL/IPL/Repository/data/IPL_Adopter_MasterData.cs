﻿using Avigma.Repository.Lib;
using IPL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class IPL_Adopter_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        private List<dynamic> AddUpdateIPL_Adopter_Master(IPL_Adopter_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcustData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_IPL_Adopter_Master]";
            IPL_Adopter_Master iPL_Adopter_Master = new IPL_Adopter_Master();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@IPL_Adopter_PkeyId", 1 + "#bigint#" + model.IPL_Adopter_PkeyId);
                input_parameters.Add("@IPL_Adopter_Name", 1 + "#varchar#" + model.IPL_Adopter_Name);
                input_parameters.Add("@IPL_Adopter_Company_Name", 1 + "#varchar#" + model.IPL_Adopter_Company_Name);
                input_parameters.Add("@IPL_Adopter_Email", 1 + "#varchar#" + model.IPL_Adopter_Email);
                input_parameters.Add("@IPL_Adopter_Phone", 1 + "#varchar#" + model.IPL_Adopter_Phone);
                input_parameters.Add("@IPL_Adopter_Preservation_Ind", 1 + "#varchar#" + model.IPL_Adopter_Preservation_Ind);
                input_parameters.Add("@IPL_Adopter_Mobile_App", 1 + "#varchar#" + model.IPL_Adopter_Mobile_App);
                input_parameters.Add("@IPL_Adopter_Process_WO", 1 + "#varchar#" + model.IPL_Adopter_Process_WO);
                input_parameters.Add("@IPL_Adopter_IsActive", 1 + "#bit#" + model.IPL_Adopter_IsActive);
                input_parameters.Add("@IPL_Adopter_IsDelete", 1 + "#bit#" + model.IPL_Adopter_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@IPL_Adopter_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    iPL_Adopter_Master.IPL_Adopter_PkeyId = "0";
                    iPL_Adopter_Master.Status = "0";
                    iPL_Adopter_Master.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    iPL_Adopter_Master.IPL_Adopter_PkeyId = Convert.ToString(objData[0]);
                    iPL_Adopter_Master.Status = Convert.ToString(objData[1]);


                }
                objcustData.Add(iPL_Adopter_Master);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcustData;



        }

        // refrence add
        private List<dynamic> AddUpdateAdop_Ref__Master(IPL_Adop_Ref_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_IPL_Adop_Ref_Master]";
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@IPL_Adop_Ref_pkeyId", 1 + "#bigint#" + model.IPL_Adop_Ref_pkeyId);
                input_parameters.Add("@IPL_Adop_Ref_Adop_Id", 1 + "#bigint#" + model.IPL_Adop_Ref_Adop_Id);
                input_parameters.Add("@IPL_Adop_Ref_Chk_Id", 1 + "#bigint#" + model.IPL_Adop_Ref_Chk_Id);
                input_parameters.Add("@IPL_Adop_Ref_flag", 1 + "#bit#" + model.IPL_Adop_Ref_flag);
                input_parameters.Add("@IPL_Adop_Ref_IsActive", 1 + "#bit#" + model.IPL_Adop_Ref_IsActive);
                input_parameters.Add("@IPL_Adop_Ref_IsDelete", 1 + "#bit#" + model.IPL_Adop_Ref_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@IPL_Adop_Ref_pkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
              
               

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;



        }
        public List<dynamic> AddupdateIPL_AdopterDetails(IPL_Adopter_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                IPL_Adop_Ref_MasterDTO iPL_Adop_Ref_MasterDTO = new IPL_Adop_Ref_MasterDTO();
                objData = AddUpdateIPL_Adopter_Master(model);

                var formdata = model.Refrencearray;//JsonConvert.DeserializeObject<List<IPL_Adop_Ref_MasterDTO>>(model.Refrencearray);
                if (formdata != null)
                {
                    for (int i = 0; i < formdata.Count; i++)
                    {
                        iPL_Adop_Ref_MasterDTO.IPL_Adop_Ref_pkeyId = 0;
                        iPL_Adop_Ref_MasterDTO.IPL_Adop_Ref_Adop_Id = Convert.ToInt64(objData[0].IPL_Adopter_PkeyId);
                        iPL_Adop_Ref_MasterDTO.IPL_Adop_Ref_Chk_Id = formdata[i].IPL_Adop_Chk_PkeyID;
                        iPL_Adop_Ref_MasterDTO.IPL_Adop_Ref_flag = formdata[i].IPL_Adop_Chk_IsChecked;
                        iPL_Adop_Ref_MasterDTO.IPL_Adop_Ref_IsActive = true;
                        iPL_Adop_Ref_MasterDTO.IPL_Adop_Ref_IsDelete = false;
                        iPL_Adop_Ref_MasterDTO.UserID = model.UserID;
                        iPL_Adop_Ref_MasterDTO.Type = 1;

                        objDynamic = AddUpdateAdop_Ref__Master(iPL_Adop_Ref_MasterDTO);
                    }
                }
               
                }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }
        public List<dynamic> DeleteIPL_AdopterDetails(IPL_Adopter_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            objData = AddUpdateIPL_Adopter_Master(model);
            return objData;
        }

            private DataSet Get_IPL_Adopter_Master(IPL_Adopter_MasterDTO model)
        {
            DataSet ds = null;
            try

            {

                string selectProcedure = "[Get_IPL_Adopter_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@IPL_Adopter_PkeyId", 1 + "#bigint#" + model.IPL_Adopter_PkeyId);

                input_parameters.Add("@WhereClause", 1 + "#nvarchar#" + model.WhereClause);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@PageNumber", 1 + "#int#" + model.PageNumber);
                input_parameters.Add("@Rowcount", 1 + "#int#" + model.Rowcount);

                input_parameters.Add("@Type", 1 + "#int#" + model.Type);



                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }
        public List<dynamic> GetIPL_AdopterDetails(IPL_Adopter_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                string wherecondition = string.Empty;
                if (model.Type == 3 && !string.IsNullOrEmpty(model.FilterData))
                {
                    var Data = JsonConvert.DeserializeObject<NewIPLAdopterMaster_Filter>(model.FilterData);
                    if (!string.IsNullOrEmpty(Data.IPL_Adopter_Company_Name))
                    {
                        wherecondition = " And ado.IPL_Adopter_Company_Name LIKE '%" + Data.IPL_Adopter_Company_Name + "%'";
                    }
                    if (!string.IsNullOrEmpty(Data.IPL_Adopter_Email))
                    {
                        wherecondition = " And ado.IPL_Adopter_Email LIKE '%" + Data.IPL_Adopter_Email + "%'";
                    }
                    if (!string.IsNullOrEmpty(Data.IPL_Adopter_Name))
                    {
                        wherecondition = " And ado.IPL_Adopter_Name LIKE '%" + Data.IPL_Adopter_Name + "%'";
                    }

                    model.WhereClause = wherecondition;
                }

                DataSet ds = Get_IPL_Adopter_Master(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<IPL_Adopter_MasterDTO> IPL_Adopter =
                   (from item in myEnumerableFeaprd
                    select new IPL_Adopter_MasterDTO
                    {
                        IPL_Adopter_PkeyId = item.Field<Int64>("IPL_Adopter_PkeyId"),
                        IPL_Adopter_Name = item.Field<String>("IPL_Adopter_Name"),
                        IPL_Adopter_Company_Name = item.Field<String>("IPL_Adopter_Company_Name"),
                        IPL_Adopter_Email = item.Field<String>("IPL_Adopter_Email"),
                        IPL_Adopter_Phone = item.Field<String>("IPL_Adopter_Phone"),
                        IPL_Adopter_Preservation_Ind = item.Field<String>("IPL_Adopter_Preservation_Ind"),
                        IPL_Adopter_Mobile_App = item.Field<String>("IPL_Adopter_Mobile_App"),
                        IPL_Adopter_Process_WO = item.Field<String>("IPL_Adopter_Process_WO"),
                        IPL_Adopter_IsActive = item.Field<Boolean?>("IPL_Adopter_IsActive"),
                        
                     

                    }).ToList();

                objDynamic.Add(IPL_Adopter);
                if (ds.Tables.Count > 1)
                {
                    var myEnumerableFeaprd1 = ds.Tables[1].AsEnumerable();
                    List<IPL_Adop_Ref_MasterDTO> refarr =
                       (from item in myEnumerableFeaprd1
                        select new IPL_Adop_Ref_MasterDTO
                        {
                            IPL_Adop_Ref_pkeyId = item.Field<Int64>("IPL_Adop_Ref_pkeyId"),
                            IPL_Adop_Ref_Adop_Id = item.Field<Int64?>("IPL_Adop_Ref_Adop_Id"),
                            IPL_Adop_Ref_Chk_Id = item.Field<Int64?>("IPL_Adop_Ref_Chk_Id"),
                            IPL_Adop_Ref_flag = item.Field<Boolean?>("IPL_Adop_Ref_flag"),
                            IPL_Adop_Ref_IsActive = item.Field<Boolean?>("IPL_Adop_Ref_IsActive"),


                        }).ToList();

                    objDynamic.Add(refarr);
                }
                }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
        private DataSet Get_IPL_Adop_Chk(IPL_Adop_Chk_MasterDTO model)
        {
            DataSet ds = null;
            try

            {

                string selectProcedure = "[Get_IPL_Adop_Chk_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }
        public List<dynamic> GetIPL_Adop_Chk_Master(IPL_Adop_Chk_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = Get_IPL_Adop_Chk(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<IPL_Adop_Chk_MasterDTO> IPL_Adop_Chk =
                   (from item in myEnumerableFeaprd
                    select new IPL_Adop_Chk_MasterDTO
                    {
                        IPL_Adop_Chk_PkeyID = item.Field<Int64>("IPL_Adop_Chk_PkeyID"),
                        IPL_Adop_Chk_Name = item.Field<String>("IPL_Adop_Chk_Name"),
                        IPL_Adop_Chk_IsChecked = false,

                    }).ToList();

                objDynamic.Add(IPL_Adop_Chk);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

    }
}