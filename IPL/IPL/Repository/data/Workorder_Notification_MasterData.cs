﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class Workorder_Notification_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        public List<dynamic> AddWorkorderNotificationMaster(Workorder_Notification_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_Workorder_Notification_Master]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@WN_Pkey_Id", 1 + "#bigint#" + model.WN_Pkey_Id);
                input_parameters.Add("@WN_UserId", 1 + "#bigint#" + model.WN_UserId);
                input_parameters.Add("@WN_WoId", 1 + "#bigint#" + model.WN_WoId);
                input_parameters.Add("@WN_Title", 1 + "#varchar#" + model.WN_Title);
                input_parameters.Add("@WN_Message", 1 + "#varchar#" + model.WN_Message);
                input_parameters.Add("@WN_IsRead", 1 + "#bit#" + model.WN_IsRead);               
                input_parameters.Add("@WN_IsActive", 1 + "#bit#" + model.WN_IsActive);
                input_parameters.Add("@WN_IsDelete", 1 + "#bit#" + model.WN_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                input_parameters.Add("@WN_Pkey_Id_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;

        }

        private DataSet GetWorkorderNotificationMaster(Workorder_Notification_MasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Workorder_Notification_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@WN_Pkey_Id", 1 + "#bigint#" + model.WN_Pkey_Id);
                input_parameters.Add("@WN_UserId", 1 + "#bigint#" + model.WN_UserId);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetWorkorderNotificationDetails(Workorder_Notification_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetWorkorderNotificationMaster(model);


                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Workorder_Notification_MasterDTO> FileDetails =
                   (from item in myEnumerableFeaprd
                    select new Workorder_Notification_MasterDTO
                    {
                        WN_Pkey_Id = item.Field<Int64>("WN_Pkey_Id"),
                        WN_UserId = item.Field<Int64?>("WN_UserId"),
                        WN_Title = item.Field<String>("WN_Title"),
                        WN_Message = item.Field<String>("WN_Message"),
                        WN_IsRead = item.Field<Boolean?>("WN_IsRead"),
                        WN_IsActive = item.Field<Boolean?>("WN_IsActive"),
                        WN_WoId = item.Field<Int64?>("WN_WoId"),
                    }).ToList();

                objDynamic.Add(FileDetails);
                
                                
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}