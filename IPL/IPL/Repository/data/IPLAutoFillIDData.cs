﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class IPLAutoFillIDData
    {
        Log log = new Log();
        MyDataSourceFactory obj = new MyDataSourceFactory();
        public List<dynamic> AddIPLAutoGenrateNo(IPLAutoGenrateIDDTO model)
        {
            try
            {
                String insertProcedure = "[CreateAutoIPLId]";

                Dictionary<string, string> input_parameters = new Dictionary<string, string>();
               
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);


                return obj.SqlCRUD(insertProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage("");
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return null;
            }

        }

        public List<dynamic> AddIPLAutoGenrateSupportTicket(IPLAutoGenrateIDDTO model)
        {
            try
            {
                String insertProcedure = "[CreateIPLAutoGenrateSupportTicket]";

                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);


                return obj.SqlCRUD(insertProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage("");
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return null;
            }

        }
    }
}