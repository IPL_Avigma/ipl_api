﻿using Avigma.Repository.Lib;
using IPL.Models;
using Avigma.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.IO;
using IPL.Repository.Lib;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace IPL.Repository.data
{
    public class Mass_Email_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        private List<dynamic> AddMassEmailData(Mass_Email_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_Mass_Email_Master]";
            Mass_Email_Master mass_Email_Master = new Mass_Email_Master();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Mass_Email_PkeyId", 1 + "#bigint#" + model.Mass_Email_PkeyId);
                input_parameters.Add("@Mass_Email_Group_ID", 1 + "#bigint#" + model.Mass_Email_Group_ID);
                input_parameters.Add("@Mass_Email_From", 1 + "#nvarchar#" + model.Mass_Email_From);
                input_parameters.Add("@Mass_Email_Subject", 1 + "#nvarchar#" + model.Mass_Email_Subject);
                input_parameters.Add("@Mass_Email_Message", 1 + "#nvarchar#" + model.Mass_Email_Message);
                input_parameters.Add("@Mass_Email_Company_Id", 1 + "#bigint#" + model.Mass_Email_Company_Id);
                input_parameters.Add("@Mass_Email_User_Id", 1 + "#bigint#" + model.Mass_Email_User_Id);
                input_parameters.Add("@Mass_Email_Schedule_Time", 1 + "#datetime#" + model.Mass_Email_Schedule_Time);
                input_parameters.Add("@Mass_Email_IsActive", 1 + "#bit#" + model.Mass_Email_IsActive);
                input_parameters.Add("@Mass_Email_IsDelete", 1 + "#bit#" + model.Mass_Email_IsDelete);
                input_parameters.Add("@Mass_Email_Sched_Flag", 1 + "#bit#" + model.Mass_Email_Sched_Flag);
                input_parameters.Add("@Mass_Email_FirstName", 1 + "#nvarchar#" + model.Mass_Email_FirstName);
                input_parameters.Add("Mass_Email_Message_text", 1 + "#nvarchar#" + model.Mass_Email_Message_text);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Mass_Email_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);

                if (objData[1] == 0)
                {
                    mass_Email_Master.Mass_Email_PkeyId = "0";
                    mass_Email_Master.Status = "0";
                    mass_Email_Master.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    mass_Email_Master.Mass_Email_PkeyId = Convert.ToString(objData[0]);
                    mass_Email_Master.Status = Convert.ToString(objData[1]);
                }
                objcltData.Add(mass_Email_Master);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;

        }

        private List<dynamic> AddMassEmailDocuments(Mass_File_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_Mass_File_Master]";
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Mass_File_PkeyId", 1 + "#bigint#" + model.Mass_File_PkeyId);
                input_parameters.Add("@Mass_File_Email_ID", 1 + "#bigint#" + model.Mass_File_Email_ID);
                input_parameters.Add("@Mass_File_FileName", 1 + "#nvarchar#" + model.Mass_File_FileName);
                input_parameters.Add("@Mass_File_FilePth", 1 + "#nvarchar#" + model.Mass_File_FilePth);
                input_parameters.Add("@Mass_File_IsActive", 1 + "#bit#" + model.Mass_File_IsActive);
                input_parameters.Add("@Mass_File_IsDelete", 1 + "#bit#" + model.Mass_File_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Mass_File_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;

        }

        //public List<dynamic> EmailMassDetails(Mass_Email_MasterDTO model)
        //{

        //    List<dynamic> objData = new List<dynamic>();
        //    List<dynamic> objAddData = new List<dynamic>();


        //    try
        //    {
        //        var datetime = DateTime.Today;

        //        if (datetime.Date == model.Mass_Email_Schedule_Time.Value.Date)
        //        {
        //            model.Mass_Email_Sched_Flag = true;

        //        }
        //        objData = AddMassEmailData(model);

        //        if (!string.IsNullOrEmpty(objData[0].Mass_Email_PkeyId))
        //        {
        //            Mass_File_MasterData mass_File_MasterData = new Mass_File_MasterData();
        //            Mass_File_MasterDTO mass_File_MasterDTO = new Mass_File_MasterDTO();
        //            for (int i = 0; i < model.filenamearr.Count; i++)
        //            {

        //                FileUploadDownload fileUploadDownload = new FileUploadDownload();
        //                mass_File_MasterDTO.Mass_File_PkeyId = 0;
        //                mass_File_MasterDTO.Mass_File_Email_ID = Convert.ToInt64(objData[0].Mass_Email_PkeyId);
        //                mass_File_MasterDTO.Mass_File_FileName = model.filenamearr[i];
        //                mass_File_MasterDTO.Mass_File_FilePth = model.Attachmentarr[i];
        //                mass_File_MasterDTO.Mass_File_Ext = model.Extension[i];
        //                mass_File_MasterDTO.Mass_File_IsActive = model.Mass_Email_IsActive;
        //                mass_File_MasterDTO.Mass_File_IsDelete = model.Mass_Email_IsDelete;
        //                mass_File_MasterDTO.Type = 1;
        //                fileUploadDownload.UploadEmailDocument(mass_File_MasterDTO);

        //            }
        //            if (model.Mass_Email_Sched_Flag == true)
        //            {
        //                if (model.Mass_Email_Group_ID != 0)
        //                {
        //                    User_Group_masterDTO user_Group_MasterDTO = new User_Group_masterDTO();
        //                    user_Group_MasterDTO.User_pkeyID = Convert.ToInt64(model.Mass_Email_User_Id);
        //                    user_Group_MasterDTO.User_Group = Convert.ToInt32(model.Mass_Email_Group_ID);
        //                    user_Group_MasterDTO.Type = 1;

        //                    var groupdata = User_Group_ChangeDetaills(user_Group_MasterDTO);
        //                    string strto = "";
        //                    for (int i = 0; i < groupdata[0].Count; i++)
        //                    {
        //                        if (!string.IsNullOrEmpty(strto))
        //                        {
        //                            strto = strto + "," + groupdata[0][i].User_Email;
        //                        }
        //                        else
        //                        {
        //                            strto = groupdata[0][i].User_Email;
        //                        }


        //                    }

        //                    if (!string.IsNullOrEmpty(strto))
        //                    {
        //                        EmailManager emailManager = new EmailManager();
        //                        EmailDTO emailDTO = new EmailDTO();
        //                        emailDTO.To = strto;
        //                        emailDTO.Subject = model.Mass_Email_Subject;
        //                        emailDTO.MainBody = model.Mass_Email_Message;
        //                        emailDTO.Attachmentarr = model.Attachmentarr;
        //                        emailDTO.filenamearr = model.filenamearr;
        //                        var emaildata = emailManager.email_send(emailDTO);
        //                    }
        //                    // Delete the File once Email has been sent


        //                }
        //            }
        //            else
        //            {
        //                for (int j = 0; j < model.Attachmentarr.Count; j++)
        //                {

        //                    File.Delete(model.Attachmentarr[j]);
        //                }
        //            }



        //        }




        //    }
        //    catch (Exception ex)
        //    {
        //        log.logErrorMessage(ex.StackTrace);
        //        log.logErrorMessage(ex.Message);
        //    }
        //    return objAddData;
        //}

        public async Task<List<dynamic>> EmailMassDetails(Mass_Email_MasterDTO model)
        {

            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objAddData = new List<dynamic>();


            try
            {
                var datetime = DateTime.Today;

                if (model.Mass_Email_Schedule_Time != null)
                {
                    if (datetime.Date == model.Mass_Email_Schedule_Time.Value.Date)
                    {
                        model.Mass_Email_Sched_Flag = true;
                    }

                }
                string strcomments = string.Empty;
                if (!string.IsNullOrEmpty(model.Mass_Email_Message))
                {
                    strcomments = Regex.Replace(model.Mass_Email_Message, "<.*?>", String.Empty);
                }
                model.Mass_Email_Message_text = strcomments;



                objData = AddMassEmailData(model);

                if (!string.IsNullOrEmpty(objData[0].Mass_Email_PkeyId))
                {
                    Mass_File_MasterDTO mass_File_MasterDTO = new Mass_File_MasterDTO();
                  
                    mass_File_MasterDTO.Mass_File_PkeyId = model.Mass_File_PkeyId;
                    mass_File_MasterDTO.Mass_File_Email_ID = Convert.ToInt64(objData[0].Mass_Email_PkeyId);
                    mass_File_MasterDTO.Mass_File_FileName = model.filename;
                    mass_File_MasterDTO.Mass_File_FilePth = model.filepath;
                    mass_File_MasterDTO.Mass_File_IsActive = model.Mass_Email_IsActive;
                    mass_File_MasterDTO.Mass_File_IsDelete = model.Mass_Email_IsDelete;
                    mass_File_MasterDTO.UserID = model.UserID;
                    mass_File_MasterDTO.Type = model.Type;
                    AddMassEmailDocuments(mass_File_MasterDTO);

                    if (model.Mass_Email_Sched_Flag == true)
                    {
                        if (model.Mass_Email_Group_ID != 0)
                        {
                            User_Group_masterDTO user_Group_MasterDTO = new User_Group_masterDTO();
                            user_Group_MasterDTO.User_pkeyID = Convert.ToInt64(model.Mass_Email_User_Id);
                            user_Group_MasterDTO.User_Group = Convert.ToInt32(model.Mass_Email_Group_ID);
                            user_Group_MasterDTO.Type = 1;

                            var groupdata = User_Group_ChangeDetaills(user_Group_MasterDTO);
                            string strto = "";
                            for (int i = 0; i < groupdata[0].Count; i++)
                            {
                                if (!string.IsNullOrEmpty(strto))
                                {
                                    strto = strto + "," + groupdata[0][i].User_Email;
                                }
                                else
                                {
                                    strto = groupdata[0][i].User_Email;
                                }
                            }

                            if (!string.IsNullOrEmpty(strto))
                            {
                                List<object> dynamicsFiles = new List<object>();
                                dynamicsFiles.Add(model.filepath);

                                DyanmicEmailDTO dyanmicEmailDTO = new DyanmicEmailDTO();
                                //dyanmicEmailDTO.To = strto;
                                dyanmicEmailDTO.Bcc = strto;
                                dyanmicEmailDTO.Subject = model.Mass_Email_Subject;
                                dyanmicEmailDTO.Message = model.Mass_Email_Message;
                                dyanmicEmailDTO.FileLinkes = dynamicsFiles;
                                await DynamicEmailManager.SendDynamicEmail(dyanmicEmailDTO,5);

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objAddData;
        }

        private DataSet GetMassEmailData(Mass_Email_MasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Mass_Email_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Mass_Email_PkeyId", 1 + "#bigint#" + model.Mass_Email_PkeyId);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@WhereClause", 1 + "#varchar#" + model.WhereClause);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return ds;
        }


        public List<dynamic> GetMassEmailDataForMobile(Mass_Email_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {

                DataSet ds = GetMassEmailData(model);
                if (ds.Tables.Count > 0)
                {
                    for (int i = 0; i < ds.Tables.Count; i++)
                    {
                        objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                    }

                }

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objDynamic;
        }

        public List<dynamic> GetMassEmailDetaills(Mass_Email_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetMassEmailData(model);
                if (ds.Tables.Count > 0)
                {
                    var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                    List<Mass_Email_MasterDTO> massemail =
                       (from item in myEnumerableFeaprd
                        select new Mass_Email_MasterDTO
                        {
                            Mass_Email_PkeyId = item.Field<Int64>("Mass_Email_PkeyId"),
                            Mass_Email_Group_ID = item.Field<Int64?>("Mass_Email_Group_ID"),
                            Mass_Email_From = item.Field<String>("Mass_Email_From"),
                            Mass_Email_Subject = item.Field<String>("Mass_Email_Subject"),
                            Mass_Email_Message = item.Field<String>("Mass_Email_Message"),
                            Mass_Email_Company_Id = item.Field<Int64?>("Mass_Email_Company_Id"),
                            Mass_Email_User_Id = item.Field<Int64?>("Mass_Email_User_Id"),
                            Mass_Email_Schedule_Time = item.Field<DateTime?>("Mass_Email_Schedule_Time"),
                            Mass_Email_IsActive = item.Field<Boolean?>("Mass_Email_IsActive"),
                            Mass_Email_Sched_Flag = item.Field<Boolean?>("Mass_Email_Sched_Flag"),
                            Mass_Email_FirstName = item.Field<String>("Mass_Email_FirstName"),
                            Mass_Email_Message_text = item.Field<String>("Mass_Email_Message_text"),
                            filename = item.Field<String>("filename"),
                            filepath = item.Field<String>("filepath"),
                            Mass_File_PkeyId = item.Field<Int64?>("Mass_File_PkeyId"),

                        }).OrderByDescending(x => x.Mass_Email_Schedule_Time).ToList();

                    objDynamic.Add(massemail);
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage("----------Type-------->" + model.Type + "<---------------");
                log.logErrorMessage("----------UserID-------->" + model.UserID + "<---------------");
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        private DataSet User_Group_ChangeData(User_Group_masterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_User_Group_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@User_pkeyID", 1 + "#bigint#" + model.User_pkeyID);
                input_parameters.Add("@User_Group", 1 + "#int#" + model.User_Group);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return ds;
        }

        public List<dynamic> User_Group_ChangeDetaills(User_Group_masterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = User_Group_ChangeData(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<User_Group_masterDTO> userEmail =
                   (from item in myEnumerableFeaprd
                    select new User_Group_masterDTO
                    {
                        User_pkeyID = item.Field<Int64>("User_pkeyID"),
                        User_Group = item.Field<int>("User_Group"),
                        User_FirstName = item.Field<String>("User_FirstName"),
                        User_Email = item.Field<String>("User_Email"),

                    }).ToList();

                objDynamic.Add(userEmail);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}