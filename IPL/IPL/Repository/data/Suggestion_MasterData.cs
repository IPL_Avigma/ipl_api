﻿using Avigma.Repository.Lib;
using IPLApp.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPLApp.Repository.data
{
    public class Suggestion_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        private List<dynamic> CreateUpdateSuggestion_Master_Data(Suggestion_Master_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_Suggestion_Master]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Sug_PkeyID", 1 + "#bigint#" + model.Sug_PkeyID);
                input_parameters.Add("@Sug_Tittle", 1 + "#varchar#" + model.Sug_Tittle);
                input_parameters.Add("@Sug_Description", 1 + "#nvarchar#" + model.Sug_Description);
                input_parameters.Add("@Sug_UserID", 1 + "#bigint#" + model.Sug_UserID);
                
                input_parameters.Add("@Sug_IsActive", 1 + "#bit#" + model.Sug_IsActive);
                input_parameters.Add("@Sug_IsDelete", 1 + "#bit#" + model.Sug_IsDelete);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                
                input_parameters.Add("@Sug_PkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);
                objData = obj.SqlCRUD(insertProcedure, input_parameters);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        public List<dynamic> AddUpdateUserSuggestion_Master_Data(Suggestion_Master_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                objData = CreateUpdateSuggestion_Master_Data(model);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        private DataSet Get_Suggestion_Master(Suggestion_Master_DTO model)
        {
            DataSet ds = null;
            try
            {

                string selectProcedure = "[GetSuggestion_MasterDetails]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Sug_PkeyID", 1 + "#bigint#" + model.Sug_PkeyID);
                input_parameters.Add("@WhereClause", 1 + "#varchar#" + model.WhereClause);
                input_parameters.Add("@PageNumber", 1 + "#int#" + model.PageNumber);
                input_parameters.Add("@NoofRows", 1 + "#int#" + model.NoofRows);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }
        public List<dynamic> Get_Suggestion_MasterDetails(Suggestion_Master_DTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = Get_Suggestion_Master(model);
                string Totalcount = ds.Tables[1].Rows[0]["count"].ToString();
                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Suggestion_Master_DTO> SuggestionMaster =
                   (from item in myEnumerableFeaprd
                    select new Suggestion_Master_DTO
                    {
                        Sug_PkeyID = item.Field<Int64>("Sug_PkeyID"),
                        Sug_Tittle = item.Field<String>("Sug_Tittle"),
                        Sug_Description = item.Field<String>("Sug_Description"),
                        votecount = item.Field<int?>("votecount"),
                        
                    }).ToList();

                objDynamic.Add(SuggestionMaster);
                objDynamic.Add(Totalcount);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
        
    }
}