﻿using Avigma.Repository.Lib;
using IPL.Models;
using IPLApp.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
using IPLApp.Repository.data;

namespace IPL.Repository.data
{
    public class InstructionMasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Instruction_Master_ChildData instruction_Master_ChildData = new Instruction_Master_ChildData();
        Log log = new Log();

        public List<dynamic> AddInstructionData(InstctionMasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_InstructionMaster]";
            InstctionMaster instctionMaster = new InstctionMaster();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Instr_pkeyId", 1 + "#bigint#" + model.Instr_pkeyId);
                input_parameters.Add("@Instr_Task_Id", 1 + "#bigint#" + model.Instr_Task_Id);
                input_parameters.Add("@Instr_Task_pkeyId", 1 + "#bigint#" + model.Instr_Task_pkeyId);
                input_parameters.Add("@Instr_WO_Id", 1 + "#bigint#" + model.Instr_WO_Id);
                input_parameters.Add("@Instr_Task_Name", 1 + "#nvarchar#" + model.Instr_Task_Name);
                input_parameters.Add("@Instr_Qty", 1 + "#int#" + model.Instr_Qty);
                input_parameters.Add("@Instr_Contractor_Price", 1 + "#decimal#" + model.Instr_Contractor_Price);
                input_parameters.Add("@Instr_Client_Price", 1 + "#decimal#" + model.Instr_Client_Price);
                input_parameters.Add("@Instr_Contractor_Total", 1 + "#decimal#" + model.Instr_Contractor_Total);
                input_parameters.Add("@Instr_Client_Total", 1 + "#decimal#" + model.Instr_Client_Total);
                input_parameters.Add("@Instr_Action", 1 + "#int#" + model.Instr_Action);
                input_parameters.Add("@Instr_IsActive", 1 + "#bit#" + model.Instr_IsActive);
                input_parameters.Add("@Instr_IsDelete", 1 + "#bit#" + model.Instr_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);

                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@InputType", 1 + "#int#" + model.InputType);

                input_parameters.Add("@Instr_Details_Data", 1 + "#varchar#" + model.Instr_Details_Data);
                input_parameters.Add("@Instr_ValType", 1 + "#int#" + model.Instr_ValType);
                input_parameters.Add("@Instr_Qty_Text", 1 + "#int#" + model.Instr_Qty_Text);
                input_parameters.Add("@Instr_Price_Text", 1 + "#decimal#" + model.Instr_Price_Text);
                input_parameters.Add("@Instr_Total_Text", 1 + "#decimal#" + model.Instr_Total_Text);
                input_parameters.Add("@Instr_Ch_pkeyId", 1 + "#bigint#" + model.Instr_Ch_pkeyId);

                input_parameters.Add("@Inst_Comand_Mobile_details", 1 + "#varchar#" + model.Inst_Comand_Mobile_details);
                input_parameters.Add("@Instr_Task_Comment", 1 + "#varchar#" + model.Instr_Task_Comment);
                input_parameters.Add("Instr_Other_Task_Name", 1 + "#varchar#" + model.Instr_Other_Task_Name);

                input_parameters.Add("@Task_Violation", 1 + "#bit#" + model.Task_Violation);
                input_parameters.Add("@Task_Hazards", 1 + "#bit#" + model.Task_Hazards);
                input_parameters.Add("@Task_damage", 1 + "#bit#" + model.Task_damage);

                input_parameters.Add("@Instr_pkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    instctionMaster.Instr_pkeyId = "0";
                    instctionMaster.Status = "0";
                    instctionMaster.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    instctionMaster.Instr_pkeyId = Convert.ToString(objData[0]);
                    instctionMaster.Status = Convert.ToString(objData[1]);


                }
                objcltData.Add(instctionMaster);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;



        }

        public List<dynamic> AddInstructionMobileData(InstctionMasterDTO instctionMasterDTO)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();
            try
            {
                instctionMasterDTO.InputType = 1;
                if (instctionMasterDTO.Instr_pkeyId == 0)
                {
                    instctionMasterDTO.Type = 1;
                }
                else
                {
                    instctionMasterDTO.Type = 2;
                }

                if (instctionMasterDTO.Instr_Ch_pkeyId == 0)
                {
                    Instruction_Master_ChildDTO instruction_Master_ChildDTO = new Instruction_Master_ChildDTO();
                    instruction_Master_ChildDTO.Inst_Ch_Wo_Id = instctionMasterDTO.Instr_WO_Id;
                    instruction_Master_ChildDTO.Inst_Ch_IsActive = true;
                    instruction_Master_ChildDTO.Inst_Ch_Delete = false;
                    instruction_Master_ChildDTO.Type = 1;//model.Instruction_Master_ChildDTO.Type;


                    objcltData = instruction_Master_ChildData.AddInstructionChildData(instruction_Master_ChildDTO);
                    if (objcltData[0].Status == "1")
                    {
                        if (!string.IsNullOrEmpty(objcltData[0].Inst_Ch_pkeyId))
                        {
                            instctionMasterDTO.Instr_Ch_pkeyId = Convert.ToInt64(objcltData[0].Inst_Ch_pkeyId);
                        }

                    }

                }

                instctionMasterDTO.Instr_IsActive = true;
                instctionMasterDTO.Instr_IsDelete = false;

                objData = AddInstructionData(instctionMasterDTO);
            }
            catch (Exception ex)
            {

                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objData;

        }
        public List<dynamic> AddMutipleTask(Task_Instruction model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                if (!string.IsNullOrEmpty(model.WorkOrder_ID_Data))
                {


                    var WorkOrderData = JsonConvert.DeserializeObject<List<WorkOrderIDItems>>(model.WorkOrder_ID_Data);
                    for (int i = 0; i < WorkOrderData.Count; i++)
                    {
                        Instruction_Master_ChildDTO instruction_Master_ChildDTO = new Instruction_Master_ChildDTO();
                        instruction_Master_ChildDTO.Inst_Ch_Wo_Id = WorkOrderData[i].WorkOrderID;
                        instruction_Master_ChildDTO.Type = 6;
                        instruction_Master_ChildDTO.UserID = model.UserID;
                        var returnPkeyId = instruction_Master_ChildData.AddInstructionChildData(instruction_Master_ChildDTO);
                        instruction_Master_ChildDTO.Inst_Ch_pkeyId = Convert.ToInt64(returnPkeyId[0].Inst_Ch_pkeyId);
                        if (!string.IsNullOrEmpty(model.Task_Instruction_Data))
                        {


                            var TaskData = JsonConvert.DeserializeObject<List<InstctionMasterDTO>>(model.Task_Instruction_Data);
                            for (int j = 0; j < TaskData.Count; j++)
                            {
                                string strcomments = string.Empty;
                                TaskData[j].Instr_WO_Id = WorkOrderData[i].WorkOrderID;
                                TaskData[j].Instr_Task_Name = TaskData[j].Instr_Task_Id.ToString();
                                if (!string.IsNullOrEmpty(TaskData[j].Instr_Details_Data))
                                {
                                    strcomments = Regex.Replace(TaskData[j].Instr_Details_Data, "<.*?>", String.Empty);
                                }

                                TaskData[j].Inst_Comand_Mobile_details = strcomments;
                                TaskData[j].Instr_Ch_pkeyId = instruction_Master_ChildDTO.Inst_Ch_pkeyId;
                                TaskData[j].UserID = model.UserID;
                                if (TaskData[j].Instr_pkeyId == 0)
                                {
                                    TaskData[j].Type = 1;
                                }
                                else
                                {
                                    TaskData[j].Type = 2;
                                }
                                objData = AddInstructionData(TaskData[j]);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                log.logErrorMessage("AddMutipleTask ----------------->" + model.WorkOrder_ID_Data);
            }
            return objData;
        }

        public List<dynamic> AddInstructionDetails(InstctionMasterDTORootObject model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();

            string Instr_Comand_Mobile = string.Empty;
            Instruction_Master_ChildDTO instruction_Master_ChildDTO = new Instruction_Master_ChildDTO();
            //get old workorder details
            WorkOrderMasterData workOrderMasterData = new WorkOrderMasterData();
            var Old_workorderData = workOrderMasterData.GetWorkorderForNotification(model.Instr_WO_Id, Convert.ToInt64(model.UserID));

            model.Instruction_Master_ChildDTO.UserID = model.UserID;

            instruction_Master_ChildDTO.Inst_Ch_pkeyId = model.Instruction_Master_ChildDTO.Inst_Ch_pkeyId;
            instruction_Master_ChildDTO.Inst_Ch_Wo_Id = model.Instruction_Master_ChildDTO.Inst_Ch_Wo_Id;
            instruction_Master_ChildDTO.Inst_Ch_Text = model.Instruction_Master_ChildDTO.Inst_Ch_Text;

            if (!string.IsNullOrEmpty(instruction_Master_ChildDTO.Inst_Ch_Text))
            {
                Instr_Comand_Mobile = Regex.Replace(instruction_Master_ChildDTO.Inst_Ch_Text, "<.*?>", String.Empty);
            }

            instruction_Master_ChildDTO.Instr_Comand_Mobile = Instr_Comand_Mobile;

            instruction_Master_ChildDTO.Inst_Ch_Delete = model.Instruction_Master_ChildDTO.Inst_Ch_Delete;
            instruction_Master_ChildDTO.Inst_Ch_IsActive = model.Instruction_Master_ChildDTO.Inst_Ch_IsActive;
            instruction_Master_ChildDTO.UserID = model.Instruction_Master_ChildDTO.UserID;
            instruction_Master_ChildDTO.Type = 1;//model.Instruction_Master_ChildDTO.Type;

            if (model.Instruction_Master_ChildDTO.Inst_Ch_pkeyId != 0)
            {

                instruction_Master_ChildDTO.Type = 2;
            }

            var returnPkeyId = instruction_Master_ChildData.AddInstructionChildData(instruction_Master_ChildDTO);

            objcltData.Add(returnPkeyId);

            model.Instr_Ch_pkeyId = Convert.ToInt64(returnPkeyId[0].Inst_Ch_pkeyId);


            try
            {

                for (int i = 0; i < model.InstctionMasterDTO.Count; i++)
                {

                    InstctionMasterDTO instctionMasterDTO = new InstctionMasterDTO();
                    string strcomments = string.Empty;
                    if (!string.IsNullOrEmpty(model.InstctionMasterDTO[i].Instr_Details_Data))
                    {
                        strcomments = Regex.Replace(model.InstctionMasterDTO[i].Instr_Details_Data, "<.*?>", String.Empty);
                    }

                    instctionMasterDTO.Instr_pkeyId = model.InstctionMasterDTO[i].Instr_pkeyId;

                    instctionMasterDTO.Instr_Task_pkeyId = model.InstctionMasterDTO[i].Instr_Task_pkeyId;
                    instctionMasterDTO.Instr_WO_Id = model.Instr_WO_Id;
                    instctionMasterDTO.Instr_Task_Name = model.InstctionMasterDTO[i].Instr_Task_Name;

                    instctionMasterDTO.Instr_Task_Id = model.InstctionMasterDTO[i].Instr_Task_Id;

                    if (model.InstctionMasterDTO[i].Instr_Task_Name == "other")
                    {
                        model.InstctionMasterDTO[i].Instr_Task_Name = "0";
                        instctionMasterDTO.Instr_Other_Task_Name = model.InstctionMasterDTO[i].Instr_Other_Task_Name;
                        instctionMasterDTO.Type = 1;
                    }
                    else if (model.InstctionMasterDTO[i].Instr_Task_pkeyId == -99)
                    {
                        InstctionTaskMaster instctionTaskMaster = new InstctionTaskMaster();
                        instctionTaskMaster.Type = 1;
                        instctionTaskMaster.Inst_Task_Name = model.InstctionMasterDTO[i].Instr_Other_Task_Name;
                        instctionTaskMaster.Inst_Task_Type_pkeyId = 3;
                        instctionTaskMaster.Inst_Task_Desc = strcomments;
                        instctionTaskMaster.Inst_Task_IsActive = true;
                        instctionTaskMaster.Inst_Task_IsDelete = false;
                        instctionTaskMaster.UserID = model.UserID;
                        objData = AddAutoInstructionTaskData(instctionTaskMaster);
                        model.InstctionMasterDTO[i].Instr_Task_Name = "0";
                        instctionMasterDTO.Instr_Task_pkeyId = objData[0];
                        instctionMasterDTO.Instr_Other_Task_Name = model.InstctionMasterDTO[i].Instr_Other_Task_Name;
                        instctionMasterDTO.Type = 1;
                    }
                    else
                    {
                        instctionMasterDTO.Type = model.Type;
                    }


                    if (!string.IsNullOrEmpty(model.InstctionMasterDTO[i].Instr_Task_Name))
                    {
                        instctionMasterDTO.Instr_Task_Id = Convert.ToInt64(model.InstctionMasterDTO[i].Instr_Task_Name);
                    }

                    instctionMasterDTO.Instr_Qty = model.InstctionMasterDTO[i].Instr_Qty;
                    instctionMasterDTO.Instr_Contractor_Price = model.InstctionMasterDTO[i].Instr_Contractor_Price;
                    instctionMasterDTO.Instr_Client_Price = model.InstctionMasterDTO[i].Instr_Client_Price;
                    instctionMasterDTO.Instr_Contractor_Total = model.InstctionMasterDTO[i].Instr_Contractor_Total;
                    instctionMasterDTO.Instr_Client_Total = model.InstctionMasterDTO[i].Instr_Client_Total;
                    instctionMasterDTO.Instr_Action = model.InstctionMasterDTO[i].Instr_Action;
                    instctionMasterDTO.Instr_IsActive = model.InstctionMasterDTO[i].Instr_IsActive;
                    instctionMasterDTO.Instr_IsDelete = model.InstctionMasterDTO[i].Instr_IsDelete;
                    //instctionMasterDTO.UserID = model.InstctionMasterDTO[i].UserID;

                    instctionMasterDTO.UserID = model.UserID;


                    instctionMasterDTO.Instr_Task_Comment = model.InstctionMasterDTO[i].Instr_Task_Comment;

                    instctionMasterDTO.Instr_Details_Data = model.InstctionMasterDTO[i].Instr_Details_Data;


                    instctionMasterDTO.Inst_Comand_Mobile_details = strcomments;
                    instctionMasterDTO.Instr_ValType = model.InstctionMasterDTO[i].Instr_ValType;

                    instctionMasterDTO.Instr_Qty_Text = model.InstctionMasterDTO[i].Instr_Qty_Text;
                    instctionMasterDTO.Instr_Price_Text = model.InstctionMasterDTO[i].Instr_Price_Text;
                    instctionMasterDTO.Instr_Total_Text = model.InstctionMasterDTO[i].Instr_Total_Text;
                    instctionMasterDTO.Instr_Ch_pkeyId = model.Instr_Ch_pkeyId;

                    if (model.InstctionMasterDTO[i].Instr_pkeyId == 0)
                    {
                        instctionMasterDTO.Type = 1;
                    }
                    else
                    {
                        instctionMasterDTO.Type = 2;
                    }


                    if (instctionMasterDTO.Instr_Task_pkeyId != 0 || instctionMasterDTO.Instr_Task_Id != 0)
                    {
                        var returnval = AddInstructionData(instctionMasterDTO);
                    }

                }
                // Added for Notification 
                workOrderMasterData.WorkOrderFirebaseNotification(Old_workorderData);
                //Add New Access Log 12-12-2022
                #region Task Log
                Access_log_MasterData access_Log_MasterData = new Access_log_MasterData();
                NewAccessLogMaster newAccessLogMaster = new NewAccessLogMaster();
                newAccessLogMaster.Access_WorkerOrderID = model.Instr_WO_Id;
                newAccessLogMaster.Access_Master_ID = 35;
                newAccessLogMaster.Access_UserID = Convert.ToInt64(model.UserID);
                newAccessLogMaster.Type = 1;
                access_Log_MasterData.AddNewAccessLogMaster(newAccessLogMaster);
                #endregion

                #region Intr log
                NewAccessLogMaster newAccessLogMaster_inst = new NewAccessLogMaster();
                newAccessLogMaster_inst.Access_WorkerOrderID = model.Instr_WO_Id;
                newAccessLogMaster_inst.Access_Master_ID = 36;
                newAccessLogMaster_inst.Access_UserID = Convert.ToInt64(model.UserID);
                newAccessLogMaster_inst.Type = 1;
                access_Log_MasterData.AddNewAccessLogMaster(newAccessLogMaster_inst);
                #endregion

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;



        }


        public DataSet GetInstructionMaster(InstctionMasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Instruction_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Instr_pkeyId", 1 + "#bigint#" + model.Instr_pkeyId);
                input_parameters.Add("@Instr_WO_Id", 1 + "#bigint#" + model.Instr_WO_Id);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetInstructionMasterDetails(InstctionMasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetInstructionMaster(model);
                if (ds.Tables.Count > 0)
                {
                    var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                    List<InstctionMasterDTO> InstructionDetails =
                       (from item in myEnumerableFeaprd
                        select new InstctionMasterDTO
                        {
                            Instr_pkeyId = item.Field<Int64>("Instr_pkeyId"),
                            Instr_Task_Id = item.Field<Int64?>("Instr_Task_Id"),
                            Instr_Task_pkeyId = item.Field<Int64?>("Instr_Task_pkeyId"),
                            Instr_WO_Id = item.Field<Int64?>("Instr_WO_Id"),
                            Instr_Task_Name = item.Field<String>("Instr_Task_Name"),
                            Instr_Qty = item.Field<int?>("Instr_Qty"),
                            Instr_Contractor_Price = item.Field<Decimal?>("Instr_Contractor_Price"),
                            Instr_Client_Price = item.Field<Decimal?>("Instr_Client_Price"),
                            Instr_Contractor_Total = item.Field<Decimal?>("Instr_Contractor_Total"),
                            Instr_Client_Total = item.Field<Decimal?>("Instr_Client_Total"),
                            Instr_Action = item.Field<int?>("Instr_Action"),
                            Instr_IsActive = item.Field<bool?>("Instr_IsActive"),
                            Instr_IsDelete = item.Field<bool?>("Instr_IsDelete"),
                            Instr_Details_Data = item.Field<string>("Instr_Details_Data"),
                            Instr_ValType = item.Field<int?>("Instr_ValType"),

                            Instr_Qty_Text = item.Field<int?>("Instr_Qty_Text"),
                            Instr_Price_Text = item.Field<decimal?>("Instr_Price_Text"),
                            Instr_Total_Text = item.Field<decimal?>("Instr_Total_Text"),
                            Instr_Ch_pkeyId = item.Field<Int64?>("Instr_Ch_pkeyId"),
                            Inst_Comand_Mobile_details = item.Field<String>("Inst_Comand_Mobile_details"),
                            TMF_Task_localPath = item.Field<String>("TMF_Task_localPath"),
                            TMF_Task_FileName = item.Field<String>("TMF_Task_FileName"),
                            Instr_Task_Comment = item.Field<String>("Instr_Task_Comment"),




                        }).ToList();

                    objDynamic.Add(InstructionDetails);
                }

                if (ds.Tables.Count > 1)
                {
                    var myEnumerableFeaprdx = ds.Tables[1].AsEnumerable();
                    List<Instruction_Master_ChildDTO> InstructionChildDetails =
                       (from item in myEnumerableFeaprdx
                        select new Instruction_Master_ChildDTO
                        {
                            Inst_Ch_pkeyId = item.Field<Int64>("Inst_Ch_pkeyId"),
                            Inst_Ch_Wo_Id = item.Field<Int64?>("Inst_Ch_Wo_Id"),
                            Inst_Ch_Text = item.Field<String>("Inst_Ch_Text"),
                            Inst_Ch_IsActive = item.Field<Boolean?>("Inst_Ch_IsActive"),

                        }).ToList();

                    objDynamic.Add(InstructionChildDetails);
                }

                if (ds.Tables.Count > 2)
                {
                    var myEnumerableFeaprdx1 = ds.Tables[2].AsEnumerable();
                    List<Instruction_DocumentDTO> instructiondocument =
                       (from item in myEnumerableFeaprdx1
                        select new Instruction_DocumentDTO
                        {
                            Inst_Doc_PkeyID = item.Field<Int64>("Inst_Doc_PkeyID"),
                            Inst_Doc_Inst_Ch_ID = item.Field<Int64?>("Inst_Doc_Inst_Ch_ID"),
                            Inst_Doc_Wo_ID = item.Field<Int64?>("Inst_Doc_Wo_ID"),
                            Inst_Doc_File_Path = item.Field<String>("Inst_Doc_File_Path"),
                            Inst_Doc_File_Size = item.Field<String>("Inst_Doc_File_Size"),
                            Inst_Doc_File_Name = item.Field<String>("Inst_Doc_File_Name"),
                            Inst_Doc_BucketName = item.Field<String>("Inst_Doc_BucketName"),
                            Inst_Doc_ProjectID = item.Field<String>("Inst_Doc_ProjectID"),
                            Inst_Doc_Object_Name = item.Field<String>("Inst_Doc_Object_Name"),
                            Inst_Doc_Folder_Name = item.Field<String>("Inst_Doc_Folder_Name"),
                            Inst_Doc_UploadedBy = item.Field<String>("Inst_Doc_UploadedBy"),
                            Inst_Doc_IsActive = item.Field<Boolean?>("Inst_Doc_IsActive"),

                        }).ToList();

                    objDynamic.Add(instructiondocument);
                }




            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
        //Dropdown For Name Insruction
        private DataSet GetInstructiondrdMaster(InstctionMasterDropdownName model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Instruction_dropdown]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Inst_Task_pkeyId", 1 + "#bigint#" + model.Inst_Task_pkeyId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@WorkOrderID", 1 + "#bigint#" + model.WorkOrderID);
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetInstructionDRDDetails(InstctionMasterDropdownName model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetInstructiondrdMaster(model);

                if (ds.Tables.Count > 0 && ds.Tables[0] != null)
                {
                    var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                    List<InstctionMasterDropdownName> InstructionDrdDetails =
                       (from item in myEnumerableFeaprd
                        select new InstctionMasterDropdownName
                        {
                            Task_pkeyID = Convert.ToString(item.Field<Int64>("Task_pkeyID")),
                            Task_Name = item.Field<String>("Task_Name"),
                            Task_Type = item.Field<int?>("Task_Type"),
                            Task_Photo_Label_Name = item.Field<String>("Task_Photo_Label_Name"),

                            Task_Contractor_UnitPrice = item.Field<decimal?>("Task_Contractor_UnitPrice"),
                            Task_Client_UnitPrice = item.Field<decimal?>("Task_Client_UnitPrice"),
                            Task_Disable_Default = item.Field<Boolean?>("Task_Disable_Default"),
                            TaskPreset = item.Field<String>("Task_Preset") != null ? JsonConvert.DeserializeObject<List<TaskPresetDrd>>(item.Field<String>("Task_Preset")) : null,

                        }).ToList();

                    objDynamic.Add(InstructionDrdDetails);
                    TaskFilterPriceFlag taskFilterPriceFlag = new TaskFilterPriceFlag();

                    if (ds.Tables.Count > 1)
                    {
                        taskFilterPriceFlag.Flag = 1;
                        objDynamic.Add(taskFilterPriceFlag);
                        var myEnumerabletask = ds.Tables[1].AsEnumerable();
                        List<InstctionMasterDropdownName> FilterInstructionDrdDetails =
                           (from item in myEnumerabletask
                            select new InstctionMasterDropdownName
                            {
                                Task_pkeyID = Convert.ToString(item.Field<Int64>("Task_pkeyID")),
                                Task_Name = item.Field<String>("Task_Name"),
                                Task_Type = item.Field<int?>("Task_Type"),
                                Task_Disable_Default = item.Field<Boolean?>("Task_Disable_Default"),
                                Task_Photo_Label_Name = item.Field<String>("Task_Photo_Label_Name"),
                                Task_Contractor_UnitPrice = item.Field<decimal?>("Task_Contractor_UnitPrice"),
                                Task_Client_UnitPrice = item.Field<decimal?>("Task_Client_UnitPrice"),


                            }).ToList();

                        objDynamic.Add(FilterInstructionDrdDetails);
                    }
                    else
                    {
                        taskFilterPriceFlag.Flag = 0;
                        objDynamic.Add(taskFilterPriceFlag);
                    }
                }



            }
            catch (Exception ex)
            {
                log.logErrorMessage("---------------GetInstructionDRDDetails-----------");
                log.logErrorMessage("WorkOrderID------->" + model.WorkOrderID);
                log.logErrorMessage("Type------->" + model.Type);
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }


        public List<dynamic> GetInstructionForMobile(InstctionMasterDropdownName model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetInstructiondrdMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<InstctionMasterDropdownName> InstructionDrdDetails =
                   (from item in myEnumerableFeaprd
                    select new InstctionMasterDropdownName
                    {
                        Task_pkeyID = Convert.ToString(item.Field<Int64>("Task_pkeyID")),
                        Int_Task_pkeyID = item.Field<Int64>("Task_pkeyID"),
                        Task_Name = item.Field<String>("Task_Name"),
                        Task_Type = item.Field<int?>("Task_Type"),
                        Task_Contractor_UnitPrice = item.Field<decimal?>("Task_Contractor_UnitPrice"),
                        Task_Client_UnitPrice = item.Field<decimal?>("Task_Client_UnitPrice"),
                        Task_Disable_Default = item.Field<Boolean?>("Task_Disable_Default"),
                        Task_Photo_Label_Name = item.Field<String>("Task_Photo_Label_Name"),
                        WorkOrderID = model.WorkOrderID

                    }).ToList();

                objDynamic.Add(InstructionDrdDetails);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                if (model.WorkOrderID == null)
                {
                    model.WorkOrderID = 0;
                }
                log.logErrorMessage("WorkOrderID------------->" + model.WorkOrderID);
                if (model.UserID == null)
                {
                    model.UserID = 0;
                }
                log.logErrorMessage("UserID------------->" + model.UserID);
                log.logErrorMessage("Type------------->" + model.Type);
            }

            return objDynamic;
        }

        //Dropdown For Task Insruction
        private DataSet GetInstructionTaskMaster(InstctionTaskMaster model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Instruction_Task_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Inst_Task_pkeyId", 1 + "#bigint#" + model.Inst_Task_pkeyId);
                input_parameters.Add("@Inst_Task_Type_pkeyId", 1 + "#bigint#" + model.Inst_Task_Type_pkeyId);
                input_parameters.Add("@whereClause", 1 + "#nvarchar#" + model.whereClause);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@WorkOrderID", 1 + "#bigint#" + model.WorkOrderID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetInstructionTaskDetails(InstctionTaskMaster model)
        {
            List<dynamic> objDynamic = new List<dynamic>();

            try
            {
                DataSet ds = GetInstructionTaskMaster(model);
                if (ds.Tables.Count > 0)
                {
                    var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                    List<InstctionTaskMaster> InstructionTaskDetails =
                       (from item in myEnumerableFeaprd
                        select new InstctionTaskMaster
                        {
                            Inst_Task_pkeyId = item.Field<Int64>("Inst_Task_pkeyId"),
                            Inst_Task_Type_pkeyId = item.Field<Int64?>("Inst_Task_Type_pkeyId"),
                            Inst_Task_Name = item.Field<String>("Inst_Task_Name"),
                            Inst_Task_Desc = item.Field<String>("Inst_Task_Desc"),
                            Inst_Task_IsActive = item.Field<Boolean?>("Inst_Task_IsActive"),

                        }).ToList();

                    objDynamic.Add(InstructionTaskDetails);
                }
                if (ds.Tables.Count > 1)
                {
                    var myEnumerableInstList = ds.Tables[1].AsEnumerable();
                    List<InstctionTaskMaster> instructionTaskList =
                       (from item in myEnumerableInstList
                        select new InstctionTaskMaster
                        {
                            Inst_Task_pkeyId = item.Field<Int64>("Inst_Task_pkeyId"),
                            Inst_Task_Type_pkeyId = item.Field<Int64?>("Inst_Task_Type_pkeyId"),
                            Inst_Task_Name = item.Field<String>("Inst_Task_Name"),
                            Inst_Task_Desc = item.Field<String>("Inst_Task_Desc"),
                            Inst_Task_IsActive = item.Field<Boolean?>("Inst_Task_IsActive"),

                        }).ToList();

                    objDynamic.Add(instructionTaskList);
                }






            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
        //Dropdown For Task Type Insruction
        private DataSet GetInstructionTaskTypeMaster(InstctionTaskTypeMaster model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Instruction_Type_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Ins_Type_pkeyId", 1 + "#bigint#" + model.Ins_Type_pkeyId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetInstructionTaskTypeDetails(InstctionTaskTypeMaster model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetInstructionTaskTypeMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<InstctionTaskTypeMaster> InstructionTaskTypeDetails =
                   (from item in myEnumerableFeaprd
                    select new InstctionTaskTypeMaster
                    {
                        Ins_Type_pkeyId = item.Field<Int64>("Ins_Type_pkeyId"),
                        Ins_Type_Name = item.Field<String>("Ins_Type_Name"),


                    }).ToList();

                objDynamic.Add(InstructionTaskTypeDetails);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        //for mobile instructon save

        public List<dynamic> AddMobileInstruction(InstctionMasterDTO instctionMasterDTO)
        {
            List<dynamic> objData = new List<dynamic>();

            try
            {
                objData = AddInstructionData(instctionMasterDTO);



            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;



        }

        // add auto instruction task Master details
        public List<dynamic> AddAutoInstructionTaskData(InstctionTaskMaster model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_InstructionTaskMaster]";
            InstctionMaster instctionMaster = new InstctionMaster();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Inst_Task_pkeyId", 1 + "#bigint#" + model.Inst_Task_pkeyId);
                input_parameters.Add("@Inst_Task_Type_pkeyId", 1 + "#bigint#" + model.Inst_Task_Type_pkeyId);
                input_parameters.Add("@Inst_Task_Name", 1 + "#varchar#" + model.Inst_Task_Name);
                input_parameters.Add("@Inst_Task_Desc", 1 + "#varchar#" + model.Inst_Task_Desc);
                input_parameters.Add("@Inst_Task_IsAutoAssign", 1 + "#bit#" + model.Inst_Task_IsAutoAssign);

                input_parameters.Add("@Inst_Task_IsActive", 1 + "#bit#" + model.Inst_Task_IsActive);
                input_parameters.Add("@Inst_Task_IsDelete", 1 + "#bit#" + model.Inst_Task_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                input_parameters.Add("@Inst_Task_pkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;

        }




        public List<dynamic> AddUpdateInstructionTaskDetails(InstctionTaskMaster model)
        {

            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objAddData = new List<dynamic>();

            objData = AddAutoInstructionTaskData(model);
            try
            {
                if (model.AutoAssinArray != null && model.AutoAssinArray.Count > 0 && objData.Count > 0 && objData[1] != 0)
                {
                    for (int i = 0; i < model.AutoAssinArray.Count; i++)
                    {
                        Instruction_Client_Data instruction_Client_Data = new Instruction_Client_Data();
                        Instruction_Json_DTO instruction_Json_DTO = new Instruction_Json_DTO();

                        if (model.Type == 1)
                        {
                            if (objData.Count > 0 && objData[1] != 0)
                            {
                                instruction_Json_DTO.Instruction_Json_Inst_Id = objData[0];
                            }
                        }
                        else
                        {
                            instruction_Json_DTO.Instruction_Json_Inst_Id = model.Inst_Task_pkeyId;
                        }
                        //  instruction_Json_DTO.Instruction_Json_PkeyId = model.Instruction_Json_PkeyId.GetValueOrDefault(0);

                        instruction_Json_DTO.Instruction_Json_PkeyId = model.Instruction_Json_PkeyId;

                        instruction_Json_DTO.Instruction_Json_Client = JsonConvert.SerializeObject(model.AutoAssinArray[i].Task_sett_Company);
                        instruction_Json_DTO.Instruction_Json_WorkType = JsonConvert.SerializeObject(model.AutoAssinArray[i].WTTaskWorkType);
                        instruction_Json_DTO.Instruction_Json_LoanType = JsonConvert.SerializeObject(model.AutoAssinArray[i].Task_sett_Lone);
                        instruction_Json_DTO.Instruction_Json_Customer = JsonConvert.SerializeObject(model.AutoAssinArray[i].Task_sett_Customer);
                        instruction_Json_DTO.Instruction_Json_Work_Group = JsonConvert.SerializeObject(model.AutoAssinArray[i].Task_Work_TypeGroup);
                        instruction_Json_DTO.UserID = model.UserID;
                        instruction_Json_DTO.Instruction_Json_IsActive = true;
                        instruction_Json_DTO.Instruction_Json_IsDelete = false;
                        instruction_Json_DTO.Type = model.Instruction_Json_PkeyId != 0 ? 2 : 1;

                        var objautoasn = instruction_Client_Data.Addinstruction_Json_Master(instruction_Json_DTO);

                        if (model.AutoAssinArray[i].Task_sett_Company != null)
                        {
                            for (int j = 0; j < model.AutoAssinArray[i].Task_sett_Company.Count; j++)
                            {
                                Instruction_ClientDTO instruction_ClentDTO = new Instruction_ClientDTO();

                                instruction_ClentDTO.Intruction_Client_PkeyId = 0;
                                instruction_ClentDTO.Intruction_Client_CilentId = model.AutoAssinArray[i].Task_sett_Company[j].Client_pkeyID;
                                instruction_ClentDTO.Intruction_Client_InstJsonId = objautoasn[0];
                                instruction_ClentDTO.Intruction_Client_instId = objData[0];
                                instruction_ClentDTO.Intruction_Client_IsActive = true;
                                instruction_ClentDTO.Intruction_Client_IsDelete = false;
                                instruction_ClentDTO.UserID = model.UserID;
                                instruction_ClentDTO.Type = 1;

                                var objclient = instruction_Client_Data.Addinstruction_Client_Master(instruction_ClentDTO);
                            }
                        }

                        if (model.AutoAssinArray[i].Task_sett_Customer != null)
                        {
                            for (int m = 0; m < model.AutoAssinArray[i].Task_sett_Customer.Count; m++)
                            {
                                Instruction_Customer_DTO instruction_Customer_DTO = new Instruction_Customer_DTO();

                                instruction_Customer_DTO.Instruction_Cust_InstJson_Id = objautoasn[0];
                                instruction_Customer_DTO.Instruction_Cust_InstId = objData[0];
                                instruction_Customer_DTO.Instruction_Cust_CustId = model.AutoAssinArray[i].Task_sett_Customer[m].Cust_Num_pkeyId;
                                instruction_Customer_DTO.Instruction_Cust_PkeyId = 0;
                                instruction_Customer_DTO.Instruction_Cust_IsActive = true;
                                instruction_Customer_DTO.Instruction_Cust_IsDelete = false;
                                instruction_Customer_DTO.UserID = model.UserID;
                                instruction_Customer_DTO.Type = 1;

                                var customerdata = instruction_Client_Data.Addinstruction_Customer_Master(instruction_Customer_DTO);
                            }
                        }
                        if (model.AutoAssinArray[i].Task_sett_Lone != null)
                        {
                            for (int n = 0; n < model.AutoAssinArray[i].Task_sett_Lone.Count; n++)
                            {
                                Instruction_LoanType_DTO instruction_LoanType_DTO = new Instruction_LoanType_DTO();

                                instruction_LoanType_DTO.Instruction_Loan_Inst_JsonId = objautoasn[0];
                                instruction_LoanType_DTO.Instruction_Loan_InstId = objData[0];
                                instruction_LoanType_DTO.Instruction_Loan_LoanId = model.AutoAssinArray[i].Task_sett_Lone[n].Loan_pkeyId;
                                instruction_LoanType_DTO.Instruction_Loan_PkeyId = 0;
                                instruction_LoanType_DTO.Instruction_Loan_IsActive = true;
                                instruction_LoanType_DTO.Instruction_Loan_IsDelete = false;
                                instruction_LoanType_DTO.UserID = model.UserID;
                                instruction_LoanType_DTO.Type = 1;

                                var lonedata = instruction_Client_Data.Addinstruction_LoanType_Master(instruction_LoanType_DTO);
                            }
                        }
                        if (model.AutoAssinArray[i].WTTaskWorkType != null)
                        {
                            for (int q = 0; q < model.AutoAssinArray[i].WTTaskWorkType.Count; q++)
                            {
                                Instruction_WorkType_DTO instruction_WorkType_DTO = new Instruction_WorkType_DTO();

                                instruction_WorkType_DTO.Instruction_workType_Inst_Json_Id = objautoasn[0];
                                instruction_WorkType_DTO.Instruction_workType_InstId = objData[0];
                                instruction_WorkType_DTO.Instruction_workType_WT_Id = model.AutoAssinArray[i].WTTaskWorkType[q].WT_pkeyID;
                                instruction_WorkType_DTO.Instruction_workType_PkeyId = 0;
                                instruction_WorkType_DTO.Instruction_workType_IsActive = true;
                                instruction_WorkType_DTO.Instruction_workType_IsDelete = false;
                                instruction_WorkType_DTO.UserID = model.UserID;
                                instruction_WorkType_DTO.Type = 1;

                                var worktypedata = instruction_Client_Data.Addinstruction_WorkType_Master(instruction_WorkType_DTO);
                            }
                        }
                        if (model.AutoAssinArray[i].Task_Work_TypeGroup != null)
                        {
                            for (int r = 0; r < model.AutoAssinArray[i].Task_Work_TypeGroup.Count; r++)
                            {
                                Instruction_WT_Group_DTO instruction_WT_Group_DTO = new Instruction_WT_Group_DTO();

                                instruction_WT_Group_DTO.Instruction_Wt_Group_InstJsonId = objautoasn[0];
                                instruction_WT_Group_DTO.Instruction_Wt_Group_Inst_Id = objData[0];
                                instruction_WT_Group_DTO.Instruction_Wt_Group_Wtg_Id = model.AutoAssinArray[i].Task_Work_TypeGroup[r].Work_Type_Cat_pkeyID;
                                instruction_WT_Group_DTO.Instruction_Wt_Group_PkeyId = 0;
                                instruction_WT_Group_DTO.Instruction_Wt_Group_IsActive = true;
                                instruction_WT_Group_DTO.Instruction_Wt_Group_IsDelete = false;
                                instruction_WT_Group_DTO.UserID = model.UserID;
                                instruction_WT_Group_DTO.Type = 1;

                                var workgroup = instruction_Client_Data.Addinstruction_WorkType_Group_Master(instruction_WT_Group_DTO);
                            }
                        }
                    }
                }
                objAddData.Add(objData);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objAddData;

        }






        //get Auto instruction task master
        public List<dynamic> GetAutoInstructionTaskDetails(InstctionTaskMaster model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            string wherecondition = string.Empty;
            InstctionTaskMaster instctionTaskMaster = new InstctionTaskMaster();
            try
            {
                switch (model.Type)
                {
                    case 4:
                        {
                            if (!string.IsNullOrEmpty(model.FilterData))
                            {
                                var Data = JsonConvert.DeserializeObject<InstctionTaskMaster>(model.FilterData);
                                if (!string.IsNullOrEmpty(Data.Inst_Task_Name))
                                {
                                    wherecondition = " And Inst_Task_Name LIKE '%" + Data.Inst_Task_Name + "%'";
                                    //wherecondition = " And Task_Name LIKE '%" + Data.Task_Name + "%'";
                                }
                                if (!string.IsNullOrEmpty(Data.Inst_Task_Desc))
                                {
                                    wherecondition = wherecondition + " And Inst_Task_Desc LIKE '%" + Data.Inst_Task_Desc + "%'";
                                }

                                if (Data.Inst_Task_IsActive == true)
                                {
                                    wherecondition = wherecondition + "  And Inst_Task_IsActive =  1 ";
                                }
                                if (Data.Inst_Task_IsActive == false)
                                {
                                    wherecondition = wherecondition + "  And Inst_Task_IsActive =  0";
                                }




                                instctionTaskMaster.whereClause = wherecondition;
                                instctionTaskMaster.Type = 4;
                                instctionTaskMaster.UserID = model.UserID;
                            }
                            break;
                        }

                    case 5:
                        {
                            instctionTaskMaster.Type = 5;
                            instctionTaskMaster.UserID = model.UserID;
                            break;
                        }

                    case 2:
                        {
                            instctionTaskMaster.Type = 2;
                            instctionTaskMaster.Inst_Task_pkeyId = model.Inst_Task_pkeyId;
                            break;
                        }

                }
                DataSet ds = GetInstructionTaskMaster(instctionTaskMaster);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<InstctionTaskMaster> InstructionTaskDetails =
                   (from item in myEnumerableFeaprd
                    select new InstctionTaskMaster
                    {
                        Inst_Task_pkeyId = item.Field<Int64>("Inst_Task_pkeyId"),
                        Inst_Task_Type_pkeyId = item.Field<Int64?>("Inst_Task_Type_pkeyId"),
                        Inst_Task_Name = item.Field<String>("Inst_Task_Name"),
                        Inst_Task_Desc = item.Field<String>("Inst_Task_Desc"),
                        Inst_Task_IsActive = item.Field<Boolean?>("Inst_Task_IsActive"),
                        Inst_Task_CreatedBy = item.Field<String>("Inst_Task_CreatedBy"),
                        Inst_Task_ModifiedBy = item.Field<String>("Inst_Task_ModifiedBy"),
                        Inst_Task_IsAutoAssign = item.Field<Boolean>("Inst_Task_IsAutoAssign"),
                        ViewUrl = null
                    }).ToList();

                objDynamic.Add(InstructionTaskDetails);

                if (model.Type == 2)
                {
                    if (ds.Tables.Count > 1)
                    {
                        var myEnumerableFeaprds = ds.Tables[1].AsEnumerable();
                        List<Instruction_Json_DTO> instjson =
                           (from item in myEnumerableFeaprds
                            select new Instruction_Json_DTO
                            {
                                Instruction_Json_PkeyId = item.Field<Int64>("Instruction_Json_PkeyId"),
                                Instruction_Json_Inst_Id = item.Field<Int64?>("Instruction_Json_Inst_Id"),
                                Instruction_Json_Client = item.Field<String>("Instruction_Json_Client"),
                                Instruction_Json_WorkType = item.Field<String>("Instruction_Json_WorkType"),
                                Instruction_Json_LoanType = item.Field<String>("Instruction_Json_LoanType"),
                                Instruction_Json_Customer = item.Field<String>("Instruction_Json_Customer"),
                                Instruction_Json_Work_Group = item.Field<String>("Instruction_Json_Work_Group"),
                                Instruction_Json_CreatedBy = item.Field<String>("Instruction_Json_CreatedBy"),
                                Instruction_Json_ModifiedBy = item.Field<String>("Instruction_Json_ModifiedBy"),
                            }).ToList();

                        objDynamic.Add(instjson);
                    }
                }
                if (model.Type == 5)
                {
                    if (ds.Tables.Count > 1)
                    {
                        var myEnumerableInsFilter = ds.Tables[1].AsEnumerable();
                        List<Filter_Admin_Instruction_MasterDTO> insFilterList =
                           (from item in myEnumerableInsFilter
                            select new Filter_Admin_Instruction_MasterDTO
                            {
                                Ins_Filter_PkeyID = item.Field<Int64>("Ins_Filter_PkeyID"),
                                Ins_Filter_InsName = item.Field<String>("Ins_Filter_InsName"),
                                Ins_Filter_InsDesc = item.Field<String>("Ins_Filter_InsDesc"),
                                Ins_Filter_InsIsActive = item.Field<Boolean?>("Ins_Filter_InsIsActive"),
                                Ins_Filter_CreatedBy = item.Field<String>("Ins_Filter_CreatedBy"),
                                Ins_Filter_ModifiedBy = item.Field<String>("Ins_Filter_ModifiedBy"),
                            }).ToList();

                        objDynamic.Add(insFilterList);
                    }
                }

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }


        //print intructions 
        private DataSet GetprintInstructionMaster(InstctionMasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Instruction_Master_Actions]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Instr_WO_Id", 1 + "#bigint#" + model.Instr_WO_Id);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }
        public List<dynamic> PrintMultipleInstruction(WorkoderActionItems model)
        {
            try
            {
                List<dynamic> objdata = new List<dynamic>();
                workoderaction workoderaction = new workoderaction();
                var Data = JsonConvert.DeserializeObject<List<WorkOrderIDItems>>(model.Arr_WorkOrderID);
                string strwhere = string.Empty;
                for (int i = 0; i < Data.Count; i++)
                {
                    InstctionMasterDTO instctionMasterDTO = new InstctionMasterDTO();
                    instctionMasterDTO.Instr_WO_Id = Data[i].WorkOrderID;
                    instctionMasterDTO.Type = 1;
                    objdata.Add(GetPRintInstructionMasterDetails(instctionMasterDTO));
                }


                return objdata;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return null;
            }



        }

        public List<dynamic> GetPRintInstructionMasterDetails(InstctionMasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetprintInstructionMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<InstctionMasterDTO> InstructionDetails =
                   (from item in myEnumerableFeaprd
                    select new InstctionMasterDTO
                    {
                        Instr_pkeyId = item.Field<Int64>("Instr_pkeyId"),
                        Instr_Task_Id = item.Field<Int64?>("Instr_Task_Id"),
                        Instr_Task_pkeyId = item.Field<Int64?>("Instr_Task_pkeyId"),
                        Instr_WO_Id = item.Field<Int64?>("Instr_WO_Id"),
                        Instr_Task_Name = item.Field<String>("Instr_Task_Name"),
                        Instr_Qty = item.Field<int?>("Instr_Qty"),
                        Instr_Contractor_Price = item.Field<Decimal?>("Instr_Contractor_Price"),
                        Instr_Client_Price = item.Field<Decimal?>("Instr_Client_Price"),
                        Instr_Contractor_Total = item.Field<Decimal?>("Instr_Contractor_Total"),
                        Instr_Client_Total = item.Field<Decimal?>("Instr_Client_Total"),
                        Instr_Action = item.Field<int?>("Instr_Action"),
                        Instr_Details_Data = item.Field<string>("Instr_Details_Data"),
                        Instr_ValType = item.Field<int?>("Instr_ValType"),

                        Instr_Qty_Text = item.Field<int?>("Instr_Qty_Text"),
                        Instr_Price_Text = item.Field<decimal?>("Instr_Price_Text"),
                        Instr_Total_Text = item.Field<decimal?>("Instr_Total_Text"),
                        Instr_Ch_pkeyId = item.Field<Int64?>("Instr_Ch_pkeyId"),
                        Inst_Comand_Mobile_details = item.Field<String>("Inst_Comand_Mobile_details"),
                        TMF_Task_localPath = item.Field<String>("TMF_Task_localPath"),
                        TMF_Task_FileName = item.Field<String>("TMF_Task_FileName"),
                        Instr_Task_Comment = item.Field<String>("Instr_Task_Comment"),
                        Task_Name = item.Field<String>("Task_Name"),




                    }).ToList();

                objDynamic.Add(InstructionDetails);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

    }

}