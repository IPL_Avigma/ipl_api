﻿using Avigma.Repository.Lib;
using IPL.Models;
using IPL.Models.FiveBrothers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class WorkOrder_MCS_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        private List<dynamic> AddWorkOrder_MCSData(WorkOrder_MCS_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_WorkOrder_MCS_Master]";
            WorkOrder_MCS_Master WorkOrder_MCS_Master = new WorkOrder_MCS_Master();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@WMCS_PkeyID", 1 + "#bigint#" + model.WMCS_PkeyID);
                input_parameters.Add("@WMCS_wo_id", 1 + "#nvarchar#" + model.WMCS_wo_id);

                input_parameters.Add("@WMCS_address", 1 + "#nvarchar#" + model.WMCS_address);
                input_parameters.Add("@WMCS_city", 1 + "#nvarchar#" + model.WMCS_city);
                input_parameters.Add("@WMCS_state", 1 + "#nvarchar#" + model.WMCS_state);
                input_parameters.Add("@WMCS_zip", 1 + "#nvarchar#" + model.WMCS_zip);
                input_parameters.Add("@WMCS_loan_number", 1 + "#nvarchar#" + model.WMCS_loan_number);
                input_parameters.Add("@WMCS_loan_type", 1 + "#nvarchar#" + model.WMCS_loan_type);
                input_parameters.Add("@WMCS_due_date", 1 + "#datetime#" + model.WMCS_due_date);
                input_parameters.Add("@WMCS_username", 1 + "#nvarchar#" + model.WMCS_username);
                input_parameters.Add("@WMCS_lot_size", 1 + "#nvarchar#" + model.WMCS_lot_size);
                input_parameters.Add("@WMCS_received_date", 1 + "#datetime#" + model.WMCS_received_date);
                input_parameters.Add("@WMCS_customer", 1 + "#nvarchar#" + model.WMCS_customer);
                input_parameters.Add("@WMCS_work_type", 1 + "#nvarchar#" + model.WMCS_work_type);
                input_parameters.Add("@WMCS_lock_code", 1 + "#nvarchar#" + model.WMCS_lock_code);
                input_parameters.Add("@WMCS_key_code", 1 + "#nvarchar#" + model.WMCS_key_code);
                input_parameters.Add("@WMCS_mortgager", 1 + "#nvarchar#" + model.WMCS_mortgager);
                input_parameters.Add("@WMCS_id", 1 + "#nvarchar#" + model.WMCS_id);



                input_parameters.Add("@WMCS_Comments", 1 + "#nvarchar#" + model.WMCS_Comments);
                input_parameters.Add("@WMCS_gpsLatitude", 1 + "#varchar#" + model.WMCS_gpsLatitude);
                input_parameters.Add("@WMCS_gpsLongitude", 1 + "#varchar#" + model.WMCS_gpsLongitude);
                input_parameters.Add("@WMCS_ImportMaster_Pkey", 1 + "#bigint#" + model.WMCS_ImportMaster_Pkey);
                input_parameters.Add("@WMCS_Import_File_Name", 1 + "#nvarchar#" + model.WMCS_Import_File_Name);
                input_parameters.Add("@WMCS_Import_FilePath", 1 + "#varchar#" + model.WMCS_Import_FilePath);
                input_parameters.Add("@WMCS_Import_Pdf_Name", 1 + "#varchar#" + model.WMCS_Import_Pdf_Name);
                input_parameters.Add("@WMCS_Import_Pdf_Path", 1 + "#varchar#" + model.WMCS_Import_Pdf_Path);








                input_parameters.Add("@WMCS_IsActive", 1 + "#bit#" + model.WMCS_IsActive);
                input_parameters.Add("@WMCS_IsDelete", 1 + "#bit#" + model.WMCS_IsDelete);

                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@WMCS_Pkey_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    WorkOrder_MCS_Master.WMCS_PkeyID = "0";
                    WorkOrder_MCS_Master.Status = "0";
                    WorkOrder_MCS_Master.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    WorkOrder_MCS_Master.WMCS_PkeyID = Convert.ToString(objData[0]);
                    WorkOrder_MCS_Master.Status = Convert.ToString(objData[1]);


                }
                objcltData.Add(WorkOrder_MCS_Master);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;

        }

        private List<dynamic> AddWorkOrder_MCSItemData(WorkOrder_MCS_Item_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_WorkOrder_MCS_Item_Master]";
            WorkOrder_MCS_Item_Master WorkOrder_MCS_Item_Master = new WorkOrder_MCS_Item_Master();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@WMCSINS_PkeyId", 1 + "#bigint#" + model.WMCSINS_PkeyId);
                input_parameters.Add("@WMCSINS_Ins_Name", 1 + "#varchar#" + model.WMCSINS_Ins_Name);
                input_parameters.Add("@WMCSINS_Ins_Details", 1 + "#varchar#" + model.WMCSINS_Ins_Details);
                input_parameters.Add("@WMCSINS_Qty", 1 + "#bigint#" + model.WMCSINS_Qty);
                input_parameters.Add("@WMCSINS_Price", 1 + "#decimal#" + model.WMCSINS_Price);
                input_parameters.Add("@WMCSINS_Total", 1 + "#decimal#" + model.WMCSINS_Total);
                input_parameters.Add("@WMCSINS_FkeyID", 1 + "#bigint#" + model.WMCSINS_FkeyID);

                input_parameters.Add("@WMCSINS_IsActive", 1 + "#bit#" + model.WMCSINS_IsActive);
                input_parameters.Add("@WMCSINS_IsDelete", 1 + "#bit#" + model.WMCSINS_IsDelete);
                input_parameters.Add("@WMCSINS_Additional_Details", 1 + "#nvarchar#" + model.WMCSINS_Additional_Details);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@WMCSINS_Pkey_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    WorkOrder_MCS_Item_Master.WMCSINS_PkeyId = "0";
                    WorkOrder_MCS_Item_Master.Status = "0";
                    WorkOrder_MCS_Item_Master.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    WorkOrder_MCS_Item_Master.WMCSINS_PkeyId = Convert.ToString(objData[0]);
                    WorkOrder_MCS_Item_Master.Status = Convert.ToString(objData[1]);

                }
                objcltData.Add(WorkOrder_MCS_Item_Master);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;



        }

        public List<dynamic> AddWorkOrderMCSData(WorkOrder_MCS_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                objData = AddWorkOrder_MCSData(model);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        public List<dynamic> AddWorkOrderMCSItemData(WorkOrder_MCS_Item_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                objData = AddWorkOrder_MCSItemData(model);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }
    }
}