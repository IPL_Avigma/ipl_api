﻿using Avigma.Repository.Lib;
using IPLApp.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPLApp.Repository.data
{
    public class Survey_Question_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public List<dynamic> AddIPL_Survey_Question(Survey_Question_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objqueData = new List<dynamic>();
            string insertProcedure = "[CreateUpdateSurvey_Question_Master]";
            Survey_Question_Master survey_Question_Master = new Survey_Question_Master();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@que_pkey_Id", 1 + "#bigint#" + model.que_pkey_Id);
                input_parameters.Add("@que_serviceID", 1 + "#bigint#" + model.que_serviceID);
                input_parameters.Add("@que_type", 1 + "#nvarchar#" + model.que_type);
                input_parameters.Add("@que_repeat", 1 + "#nvarchar#" + model.que_repeat);
                input_parameters.Add("@que_expand", 1 + "#bit#" + model.que_expand);
                input_parameters.Add("@que_name", 1 + "#nvarchar#" + model.que_name);
                input_parameters.Add("@que_id", 1 + "#nvarchar#" + model.que_id);
                input_parameters.Add("@que_prompt", 1 + "#nvarchar#" + model.que_prompt);
                input_parameters.Add("@que_enableTotal", 1 + "#bit#" + model.que_enableTotal);
                input_parameters.Add("@que_anonymous", 1 + "#bit#" + model.que_anonymous);
                input_parameters.Add("@que_enabledByDefault", 1 + "#bit#" + model.que_enabledByDefault);
                input_parameters.Add("@que_uuid", 1 + "#nvarchar#" + model.que_uuid);
                input_parameters.Add("@que_IsActive", 1 + "#bit#" + model.que_IsActive);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@que_pkey_Id_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    survey_Question_Master.que_pkey_Id = 0;
                    survey_Question_Master.Status = "0";
                    survey_Question_Master.ErrorMessage = "Error while Saving in the Database";

                }
                else
                {
                    survey_Question_Master.que_pkey_Id = Convert.ToInt64(objData[0]);
                    survey_Question_Master.Status = Convert.ToString(objData[1]);


                }
                objqueData.Add(survey_Question_Master);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objqueData;



        }

        private DataSet GetSurvey_Question(Survey_Question_MasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[GetSurvey_Question_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@que_pkey_Id", 1 + "#bigint#" + model.que_pkey_Id);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetSurvey_QuestionDetails(Survey_Question_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetSurvey_Question(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Survey_Question_MasterDTO> QuestionDetails =
                   (from item in myEnumerableFeaprd
                    select new Survey_Question_MasterDTO
                    {
                        que_pkey_Id = item.Field<Int64>("que_pkey_Id"),
                        que_type = item.Field<String>("que_type"),
                        que_repeat = item.Field<String>("que_repeat"),
                        que_expand = item.Field<Boolean?>("que_expand"),
                        que_name = item.Field<String>("que_name"),
                        que_id = item.Field<String>("que_id"),
                        que_prompt = item.Field<String>("que_prompt"),
                        que_enableTotal = item.Field<Boolean?>("que_enableTotal"),
                        que_anonymous = item.Field<Boolean?>("que_anonymous"),
                        que_enabledByDefault = item.Field<Boolean?>("que_enabledByDefault"),
                        que_uuid = item.Field<String>("que_uuid"),
                        que_serviceID = item.Field<Int64?>("que_serviceID"),

                    }).ToList();

                objDynamic.Add(QuestionDetails);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}