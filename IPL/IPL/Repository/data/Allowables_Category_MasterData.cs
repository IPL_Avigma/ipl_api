﻿using Avigma.Repository.Lib;
using IPL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class Allowables_Category_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();


        public List<dynamic> AddAllowablesCategoryData(Allowables_Category_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_Allowables_Category_Master]";
            Allowables_Category allowables_Category = new Allowables_Category();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Allowables_Cat_PkeyId", 1 + "#bigint#" + model.Allowables_Cat_PkeyId);
                input_parameters.Add("@Allowables_Cat_Name", 1 + "#varchar#" + model.Allowables_Cat_Name);
                input_parameters.Add("@Allowables_Cat_IsActive", 1 + "#bit#" + model.Allowables_Cat_IsActive);
                input_parameters.Add("@Allowables_Cat_IsDelete", 1 + "#bit#" + model.Allowables_Cat_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Allowables_Cat_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters); ;
                if (objData[1] == 0)
                {
                    allowables_Category.Allowables_Cat_PkeyId = "0";
                    allowables_Category.Status = "0";
                    allowables_Category.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    allowables_Category.Allowables_Cat_PkeyId = Convert.ToString(objData[0]);
                    allowables_Category.Status = Convert.ToString(objData[1]);


                }
                objcltData.Add(allowables_Category);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;



        }

        private DataSet GetAllowablesCategoryMaster(Allowables_Category_MasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Allowables_Category_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Allowables_Cat_PkeyId", 1 + "#bigint#" + model.Allowables_Cat_PkeyId);
                input_parameters.Add("@WhereClause", 1 + "#varchar#" + model.WhereClause);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
            }



            return ds;
        }

        public List<dynamic> GetAllowablesCategoryDetails(Allowables_Category_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetAllowablesCategoryMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Allowables_Category_MasterDTO> Allowables_Category  =
                   (from item in myEnumerableFeaprd
                    select new Allowables_Category_MasterDTO
                    {
                        Allowables_Cat_PkeyId = item.Field<Int64>("Allowables_Cat_PkeyId"),
                        Allowables_Cat_Name = item.Field<String>("Allowables_Cat_Name"),
                        Allowables_Cat_IsActive = item.Field<Boolean?>("Allowables_Cat_IsActive"),
                        Allowables_Cat_CreatedBy = item.Field<String>("Allowables_Cat_CreatedBy"),
                        Allowables_Cat_ModifiedBy = item.Field<String>("Allowables_Cat_ModifiedBy"),

                    }).ToList();

                objDynamic.Add(Allowables_Category);
                if (model.Type == 1)
                {
                    if (ds.Tables.Count > 1)
                    {
                        var myEnumerableFeaprd1 = ds.Tables[1].AsEnumerable();
                        List<Filter_Admin_Allowables_Category_MasterDTO> Filterallowcat =
                           (from item in myEnumerableFeaprd1
                            select new Filter_Admin_Allowables_Category_MasterDTO
                            {
                                Allowables_Cat_Filter_PkeyId = item.Field<Int64>("Allowables_Cat_Filter_PkeyId"),
                                Allowables_Cat_Filter_Name = item.Field<String>("Allowables_Cat_Filter_Name"),
                                Allowables_Cat_Filter_IsCateActive = item.Field<Boolean?>("Allowables_Cat_Filter_IsCateActive"),
                                Allowables_Cat_Filter_CreatedBy = item.Field<String>("Allowables_Cat_Filter_CreatedBy"),
                                Allowables_Cat_Filter_ModifiedBy = item.Field<String>("Allowables_Cat_Filter_ModifiedBy"),
                            }).ToList();

                        objDynamic.Add(Filterallowcat);
                    }
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
            }

            return objDynamic;
        }

        public List<dynamic> GetAllowablesFilterDetails(Allowables_Category_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();


            string wherecondition = string.Empty;
            try
            {

                var Data = JsonConvert.DeserializeObject<Allowables_Category_MasterDTO>(model.FilterData);
                if (!string.IsNullOrEmpty(Data.Allowables_Cat_Name))
                {
                    wherecondition = " And allow.Allowables_Cat_Name LIKE '%" + Data.Allowables_Cat_Name + "%'";
                }


                if (Data.Allowables_Cat_IsActive == true)
                {
                    wherecondition = wherecondition + "  And allow.Allowables_Cat_IsActive =  '1'";
                }
                if (Data.Allowables_Cat_IsActive == false)
                {
                    wherecondition = wherecondition + "  And allow.Allowables_Cat_IsActive =  '0'";
                }


                Allowables_Category_MasterDTO allowables_Category_MasterDTO = new Allowables_Category_MasterDTO();

                allowables_Category_MasterDTO.WhereClause = wherecondition;
                allowables_Category_MasterDTO.Type = 3;
                allowables_Category_MasterDTO.UserID = model.UserID;
                DataSet ds = GetAllowablesCategoryMaster(allowables_Category_MasterDTO);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Allowables_Category_MasterDTO> allowablefilter =
                   (from item in myEnumerableFeaprd
                    select new Allowables_Category_MasterDTO
                    {

                        Allowables_Cat_PkeyId = item.Field<Int64>("Allowables_Cat_PkeyId"),
                        Allowables_Cat_Name = item.Field<String>("Allowables_Cat_Name"),
                        Allowables_Cat_IsActive = item.Field<Boolean?>("Allowables_Cat_IsActive"),
                        Allowables_Cat_CreatedBy = item.Field<String>("Allowables_Cat_CreatedBy"),
                        Allowables_Cat_ModifiedBy = item.Field<String>("Allowables_Cat_ModifiedBy"),
                    }).ToList();

                objDynamic.Add(allowablefilter);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }

            return objDynamic;
        }

        public List<dynamic> AddUpdate_Filter_Admin_AllowablesCategory(Filter_Admin_Allowables_Category_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_Filter_Admin_Allowables_Category_Master]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Allowables_Cat_Filter_PkeyId", 1 + "#bigint#" + model.Allowables_Cat_Filter_PkeyId);
                input_parameters.Add("@Allowables_Cat_Filter_Name", 1 + "#nvarchar#" + model.Allowables_Cat_Filter_Name);
                input_parameters.Add("@Allowables_Cat_Filter_IsActive", 1 + "#bit#" + model.Allowables_Cat_Filter_IsActive);
                input_parameters.Add("@Allowables_Cat_Filter_IsCateActive", 1 + "#bit#" + model.Allowables_Cat_Filter_IsCateActive);
                input_parameters.Add("@Allowables_Cat_Filter_IsDelete", 1 + "#bit#" + model.Allowables_Cat_Filter_IsDelete);
                input_parameters.Add("@Allowables_Cat_Filter_CompanyId", 1 + "#bigint#" + model.Allowables_Cat_Filter_CompanyId);
                input_parameters.Add("@Allowables_Cat_Filter_UserId", 1 + "#bigint#" + model.Allowables_Cat_Filter_UserId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserId);
                input_parameters.Add("@Allowables_Cat_Filter_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        private DataSet GetAllowablesCategory(Allowables_Category_MasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Allowables_Category_DRD]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
            }



            return ds;
        }

        public List<dynamic> GetAllowablesCategoryDRD(Allowables_Category_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetAllowablesCategory(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Allowables_Category_MasterDTO> Allowables_Category =
                   (from item in myEnumerableFeaprd
                    select new Allowables_Category_MasterDTO
                    {
                        Allowables_Cat_PkeyId = item.Field<Int64>("Allowables_Cat_PkeyId"),
                        Allowables_Cat_Name = item.Field<String>("Allowables_Cat_Name"),

                    }).ToList();

                objDynamic.Add(Allowables_Category);

                var myEnumerableCustnumber = ds.Tables[1].AsEnumerable();
                List<CustomerNumberDTO> CustomerNumber =
                   (from item in myEnumerableCustnumber
                    select new CustomerNumberDTO
                    {
                        Cust_Num_pkeyId = item.Field<Int64>("Cust_Num_pkeyId"),
                        Cust_Num_Number = item.Field<String>("Cust_Num_Number"),

                    }).ToList();

                objDynamic.Add(CustomerNumber);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
            }

            return objDynamic;
        }


    }
}