﻿using Avigma.Repository.Lib;
using IPL.Models;
using IPLApp.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPLApp.Repository.data
{
    public class LoanTypeMasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public List<dynamic> AddLoanTypeMasyerData(LoanTypeMasterDTO model)
        {

            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objloanData = new List<dynamic>();
            string insertProcedure = "[CreateUpdateLoanType_Master]";
            LoanTypeMaster loanTypeMaster = new LoanTypeMaster();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Loan_pkeyId", 1 + "#bigint#" + model.Loan_pkeyId);
                input_parameters.Add("@Loan_Type", 1 + "#nvarchar#" + model.Loan_Type);
                input_parameters.Add("@Loan_IsActive", 1 + "#bit#" + model.Loan_IsActive);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Loan_pkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);

                if (objData[1] == 0)
                {
                    loanTypeMaster.Loan_pkeyId = "0";
                    loanTypeMaster.Status = "0";
                    loanTypeMaster.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    loanTypeMaster.Loan_pkeyId = Convert.ToString(objData[0]);
                    loanTypeMaster.Status = Convert.ToString(objData[1]);


                }
                objloanData.Add(loanTypeMaster);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objloanData;



        }

        private DataSet GetLoanTypeMaster(LoanTypeMasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_LoanType_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Loan_pkeyId", 1 + "#bigint#" + model.Loan_pkeyId);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@WhereClause", 1 + "#varchar#" + model.WhereClause);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetLoanTypeMasterDetails(LoanTypeMasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetLoanTypeMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<LoanTypeMasterDTO> LoanDetails =
                   (from item in myEnumerableFeaprd
                    select new LoanTypeMasterDTO
                    {
                        Loan_pkeyId = item.Field<Int64>("Loan_pkeyId"),
                        Loan_Type = item.Field<String>("Loan_Type"),
                        Loan_CreatedBy = item.Field<String>("Loan_CreatedBy"),
                        Loan_ModifiedBy = item.Field<String>("Loan_ModifiedBy"),
                        Loan_IsActive = item.Field<Boolean?>("Loan_IsActive"),
                        Loan_IsDeleteAllow = item.Field<Boolean?>("Loan_IsDeleteAllow"),

                    }).ToList();

                objDynamic.Add(LoanDetails);

                if(model.Type == 1)
                {
                    if (ds.Tables.Count > 1)
                    {
                        var myEnumerableInsFilter = ds.Tables[1].AsEnumerable();
                        List<Filter_Admin_Loan_MasterDTO> insFilterList =
                           (from item in myEnumerableInsFilter
                            select new Filter_Admin_Loan_MasterDTO
                            {
                                Loan_Filter_PkeyID = item.Field<Int64>("Loan_Filter_PkeyID"),
                                Loan_Filter_LoanName = item.Field<String>("Loan_Filter_LoanName"),
                                Loan_Filter_CreatedBy = item.Field<String>("Loan_Filter_CreatedBy"),
                                Loan_Filter_ModifiedBy = item.Field<String>("Loan_Filter_ModifiedBy"),
                                Loan_Filter_LoanIsActive = item.Field<Boolean?>("Loan_Filter_LoanIsActive")

                            }).ToList();

                        objDynamic.Add(insFilterList);
                    }
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        public List<dynamic> GetLoanFilterDetails(LoanTypeMasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();


            string wherecondition = string.Empty;
            try
            {

                var Data = JsonConvert.DeserializeObject<LoanTypeMasterDTO>(model.FilterData);
                if (!string.IsNullOrEmpty(Data.Loan_Type))
                {
                    //  wherecondition = " And Loan_Type =    '" + Data.Loan_Type + "'";
                    wherecondition = " And Loan_Type LIKE '%" + Data.Loan_Type + "%'";
                }


                if (Data.Loan_IsActive == true)
                {
                    wherecondition = wherecondition + "  And Loan_IsActive =  '1'";
                }
                if (Data.Loan_IsActive == false)
                {
                    wherecondition = wherecondition + "  And Loan_IsActive =  0";
                }


                LoanTypeMasterDTO loanTypeMasterDTO = new LoanTypeMasterDTO();

                loanTypeMasterDTO.WhereClause = wherecondition;
                loanTypeMasterDTO.Type = 3;
                loanTypeMasterDTO.UserID = model.UserID;

                DataSet ds = GetLoanTypeMaster(loanTypeMasterDTO);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<LoanTypeMasterDTO> Loanfilter =
                   (from item in myEnumerableFeaprd
                    select new LoanTypeMasterDTO
                    {
                        Loan_pkeyId = item.Field<Int64>("Loan_pkeyId"),
                        Loan_Type = item.Field<String>("Loan_Type"),
                        Loan_IsActive = item.Field<Boolean?>("Loan_IsActive"),
                        Loan_CreatedBy = item.Field<String>("Loan_CreatedBy"),
                        Loan_ModifiedBy = item.Field<String>("Loan_ModifiedBy"),
                    }).ToList();

                objDynamic.Add(Loanfilter);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }

            return objDynamic;
        }
    }
}