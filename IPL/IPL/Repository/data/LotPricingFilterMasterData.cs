﻿using Avigma.Repository.Lib;
using IPL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository
{
    public class LotPricingFilterMasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();


        public List<dynamic> AddLotPricingFilterMasterData(LotPricingFilterMasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();

            ResonseMessages resonseMessages = new ResonseMessages();
            string insertProcedure = "[CreateUpdate_Lot_Pricing_Filter_Master]";
           
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Lot_Pricing_PkeyID", 1 + "#bigint#" + model.Lot_Pricing_PkeyID);
                input_parameters.Add("@Lot_Pricing_CompanyID", 1 + "#bigint#" + model.Lot_Pricing_CompanyID);
                input_parameters.Add("@Lot_Pricing_Name", 1 + "#varchar#" + model.Lot_Pricing_Name);
                input_parameters.Add("@Lot_Pricing_IsDeleteAllow", 1 + "#bit#" + model.Lot_Pricing_IsDeleteAllow);
                input_parameters.Add("@Lot_Pricing_IsActive", 1 + "#bit#" + model.Lot_Pricing_IsActive);
                input_parameters.Add("@Lot_Pricing_IsDelete", 1 + "#bit#" + model.Lot_Pricing_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Lot_Pricing_Pkey_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters); ;
                if (objData[1] == 0)
                {
                    resonseMessages.ID = "0";
                    resonseMessages.Status = "0";
                    resonseMessages.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    resonseMessages.ID = Convert.ToString(objData[0]);
                    resonseMessages.Status = Convert.ToString(objData[1]);


                }
                objcltData.Add(resonseMessages);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;



        }

        private DataSet GetLotPricingFilteryMaster(LotPricingFilterMasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Lot_Pricing_Filter_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Lot_Pricing_PkeyID", 1 + "#bigint#" + model.Lot_Pricing_PkeyID);
                input_parameters.Add("@whereClause", 1 + "#nvarchar#" + model.whereClause);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
            }



            return ds;
        }

        public List<dynamic> GetLotPricingFilteryMasterData(LotPricingFilterMasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                var Data = JsonConvert.DeserializeObject<LotPricingFilterMasterDTO>(model.FilterData);
                model.whereClause = Data.Lot_Pricing_Name;
                DataSet ds = GetLotPricingFilteryMaster(model);
                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<LotPricingFilterMasterDTO> lotpricing =
                   (from item in myEnumerableFeaprd
                    select new LotPricingFilterMasterDTO
                    {
                        Lot_Pricing_PkeyID = item.Field<Int64>("Lot_Pricing_PkeyID"),
                        Lot_Pricing_CompanyID = item.Field<Int64>("Lot_Pricing_CompanyID"),
                        Lot_Pricing_Name = item.Field<String>("Lot_Pricing_Name"),
                        Lot_Pricing_IsDeleteAllow = item.Field<Boolean?>("Lot_Pricing_IsDeleteAllow"),
                        Lot_Pricing_IsActive = item.Field<Boolean?>("Lot_Pricing_IsActive"),
                        Lot_Pricing_CreatedBy = item.Field<String>("Lot_Pricing_CreatedBy"),
                        Lot_Pricing_ModifiedBy = item.Field<String>("Lot_Pricing_ModifiedBy"),
                    }).ToList();

                objDynamic.Add(lotpricing);
               
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
            }

            return objDynamic;
        } 
    }
}