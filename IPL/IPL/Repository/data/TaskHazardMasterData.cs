﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class TaskHazardMasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();


        public List<dynamic> AddTaskHazardMasterData(TaskHazardMaster model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();

            string insertProcedure = "[CreateUpdateTask_Hazard_Master]";
            TaskHazard taskHazard = new TaskHazard();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Task_Hazard_pkeyID", 1 + "#bigint#" + model.Task_Hazard_pkeyID);
                input_parameters.Add("@Task_Hazard_WO_ID", 1 + "#bigint#" + model.Task_Hazard_WO_ID);
                input_parameters.Add("@Task_Hazard_Name", 1 + "#varchar#" + model.Task_Hazard_Name);
                input_parameters.Add("@Task_Hazard_Description", 1 + "#varchar#" + model.Task_Hazard_Description);
                input_parameters.Add("@Task_Hazard_Date_Discovered", 1 + "#datetime#" + model.Task_Hazard_Date_Discovered);
                input_parameters.Add("@Task_Hazard_Comment", 1 + "#varchar#" + model.Task_Hazard_Comment);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Task_Hazard_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters); ;
                if (objData[1] == 0)
                {
                    taskHazard.Task_Hazard_pkeyID = "0";
                    taskHazard.Status = "0";
                    taskHazard.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    taskHazard.Task_Hazard_pkeyID = Convert.ToString(objData[0]);
                    taskHazard.Status = Convert.ToString(objData[1]);


                }
                objcltData.Add(taskHazard);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;



        }

        public List<dynamic> GetTaskHazardMasterDetails(TaskHazardMaster model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {

                DataSet ds = GetTaskHazardMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<TaskHazardMaster> taskDamageMasterDetails =
                   (from item in myEnumerableFeaprd
                    select new TaskHazardMaster
                    {
                        Task_Hazard_pkeyID = item.Field<Int64>("Task_Hazard_pkeyID"),
                        Task_Hazard_WO_ID = item.Field<Int64?>("Task_Hazard_WO_ID"),
                        Task_Hazard_Name = item.Field<String>("Task_Hazard_Name"),
                        Task_Hazard_Description = item.Field<String>("Task_Hazard_Description"),
                        Task_Hazard_Date_Discovered = item.Field<DateTime>("Task_Hazard_Date_Discovered"),
                        Task_Hazard_Comment = item.Field<String>("Task_Hazard_Comment"),
                        Task_Hazard_IsActive = item.Field<Boolean>("Task_Hazard_IsActive"),

                    }).ToList();

                objDynamic.Add(taskDamageMasterDetails);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        private DataSet GetTaskHazardMaster(TaskHazardMaster model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Task_Hazard_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Task_Hazard_pkeyID", 1 + "#bigint#" + model.Task_Hazard_pkeyID);
                input_parameters.Add("@Task_Hazard_WO_ID", 1 + "#bigint#" + model.Task_Hazard_WO_ID);
                input_parameters.Add("@Task_Hazard_Status", 1 + "#int#" + model.Task_Hazard_Status);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }
    }
}