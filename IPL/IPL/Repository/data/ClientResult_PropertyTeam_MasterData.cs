﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class ClientResult_PropertyTeam_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        private DataSet GetClientResult_PropertyTeam(ClientResult_PropertyTeam_MasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_ClientResult_PropertyTeam_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@CRPT_PkeyID", 1 + "#bigint#" + model.CRPT_PkeyID);
                input_parameters.Add("@CRPT_WO_ID", 1 + "#bigint#" + model.CRPT_WO_ID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserID);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }
        public List<dynamic> GetClientResultPropertyTeamData(ClientResult_PropertyTeam_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetClientResult_PropertyTeam(model);

                #region comment

                // var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                //        List < ClientResult_PropertyTeam_MasterDTO > piData =
                //        (from item in myEnumerableFeaprd
                //                    select new ClientResult_PropertyTeam_MasterDTO
                //                    {
                //            CRPT_PkeyID = item.Field<long>("CRPT_PkeyID"),
                //CRPT_InspectionVendor = item.Field<Boolean?>("CRPT_InspectionVendor"),
                //CRPT_PMVendor = item.Field<string>("CRPT_PMVendor"),
                //CRPT_PrimaryVendor = item.Field<string>("CRPT_PrimaryVendor"),
                //CRPT_GeneralContractor = item.Field<string>("CRPT_GeneralContractor"),
                //CRPT_SalesSpecialist = item.Field<string>("CRPT_SalesSpecialist"),
                //CRPT_Investor = item.Field<string>("CRPT_Investor"),
                //CRPT_InvestorCaseNumber = item.Field<string>("CRPT_InvestorCaseNumber"),
                //CRPT_ServicerFamily = item.Field<string>("CRPT_ServicerFamily"),
                //CRPT_ServicerLoan = item.Field<string>("CRPT_ServicerLoan"), 
                //                    }).ToList();



                //objDynamic.Add(piData);
                #endregion

                if (ds.Tables.Count > 0)
                {
                    for (int i = 0; i < ds.Tables.Count; i++)
                    {
                        objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                    }

                }

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        private List<dynamic> CreateUpdate_ClientResult_PropertyTeam_Master(ClientResult_PropertyTeam_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objclntData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_ClientResult_PropertyTeam_Master]";
            ClientResult_PropertyTeam_Master clientResult_PropertyTeam_Master = new ClientResult_PropertyTeam_Master();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@CRPT_IsActive", 1 + "#bit#" + model.CRPT_IsActive);
                input_parameters.Add("@CRPT_WO_ID", 1 + "#bigint#" + model.CRPT_WO_ID);
                input_parameters.Add("@CRPT_IsDelete", 1 + "#bit#" + model.CRPT_IsDelete);
                input_parameters.Add("@CRPT_PkeyID", 1 + "#bigint#" + model.CRPT_PkeyID);
                input_parameters.Add("@CRPT_InspectionVendor", 1 + "#bit#" + model.CRPT_InspectionVendor);
                input_parameters.Add("@CRPT_PMVendor", 1 + "#nvarchar#" + model.CRPT_PMVendor);
                input_parameters.Add("@CRPT_PrimaryVendor", 1 + "#nvarchar#" + model.CRPT_PrimaryVendor);
                input_parameters.Add("@CRPT_GeneralContractor", 1 + "#nvarchar#" + model.CRPT_GeneralContractor);
                input_parameters.Add("@CRPT_SalesSpecialist", 1 + "#nvarchar#" + model.CRPT_SalesSpecialist);
                input_parameters.Add("@CRPT_Investor", 1 + "#nvarchar#" + model.CRPT_Investor);
                input_parameters.Add("@CRPT_InvestorCaseNumber", 1 + "#nvarchar#" + model.CRPT_InvestorCaseNumber);
                input_parameters.Add("@CRPT_ServicerFamily", 1 + "#nvarchar#" + model.CRPT_ServicerFamily);
                input_parameters.Add("@CRPT_ServicerLoan", 1 + "#nvarchar#" + model.CRPT_ServicerLoan);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#bigint#" + model.Type);
                input_parameters.Add("@CRPT_PkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    clientResult_PropertyTeam_Master.CRPT_PkeyID = "0";
                    clientResult_PropertyTeam_Master.Status = "0";
                    clientResult_PropertyTeam_Master.ErrorMessage = "Error while Saviving in the Database";
                }
                else
                {
                    clientResult_PropertyTeam_Master.CRPT_PkeyID = Convert.ToString(objData[0]);
                    clientResult_PropertyTeam_Master.Status = Convert.ToString(objData[1]);
                }
                objclntData.Add(clientResult_PropertyTeam_Master);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objclntData;
        }

        public List<dynamic> AddClientResult_PropertyTeamData(ClientResult_PropertyTeam_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                objData = CreateUpdate_ClientResult_PropertyTeam_Master(model);

            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }
    }
}