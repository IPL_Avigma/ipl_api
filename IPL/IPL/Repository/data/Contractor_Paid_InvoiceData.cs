﻿using Avigma.Repository.Lib;
using IPL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class Contractor_Paid_InvoiceData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        private DataSet GetContractorInvoicePaidMaster(Contractor_Paid_InvoiceDTO model)
        {            
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Contractor_Paid_Invoice]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();
                input_parameters.Add("@WhereClause", 1 + "#varchar#" + model.whereClause);                
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }
            return ds;
        }

        public List<Contractor_Paid_InvoiceDTO> GetClientMasterDetails(string checkNumber,string whereClause)
        {
            List<Contractor_Paid_InvoiceDTO> contractorinvoicepaid = new List<Contractor_Paid_InvoiceDTO>();
            try
            {
                Contractor_Paid_InvoiceDTO contractor_Paid_InvoiceDTO = new Contractor_Paid_InvoiceDTO();
               // contractor_Paid_InvoiceDTO.Inv_Con_Inv_Date = ContractorInvoiceDate;
                contractor_Paid_InvoiceDTO.Type = 1;
                contractor_Paid_InvoiceDTO.whereClause = whereClause;

                contractor_Paid_InvoiceDTO.whereClause = string.IsNullOrEmpty(contractor_Paid_InvoiceDTO.whereClause) ? " and cp.Con_Pay_CheckNumber = '" + checkNumber + "'" : contractor_Paid_InvoiceDTO.whereClause + " and cp.Con_Pay_CheckNumber = '" + checkNumber + "'";

                DataSet ds = GetContractorInvoicePaidMaster(contractor_Paid_InvoiceDTO);

                var myEnumerableFeaprdc = ds.Tables[0].AsEnumerable();
                contractorinvoicepaid =
                   (from item in myEnumerableFeaprdc
                    select new Contractor_Paid_InvoiceDTO
                    {
                        Inv_Con_pkeyId = item.Field<Int64>("Inv_Con_pkeyId"),
                        Inv_Con_Invoice_Id = item.Field<Int64?>("Inv_Con_Invoice_Id"),
                        Inv_Con_TaskId = item.Field<Int64?>("Inv_Con_TaskId"),
                        Inv_Con_Wo_ID = item.Field<Int64?>("Inv_Con_Wo_ID"),
                        Inv_Con_Sub_Total = item.Field<Decimal?>("Inv_Con_Sub_Total"),
                        Inv_Con_ContTotal = item.Field<Decimal?>("Inv_Con_ContTotal"),
                        Con_Pay_Amount = item.Field<Decimal?>("Con_Pay_Amount"),
                        Inv_Con_Invoce_Num = item.Field<String>("Inv_Con_Invoce_Num"),
                        Inv_Con_Inv_Date = item.Field<DateTime?>("Inv_Con_Inv_Date"),
                        Inv_Con_Status = item.Field<int?>("Inv_Con_Status"),
                        dueDate = item.Field<DateTime?>("dueDate"),
                        IPLNO = item.Field<String>("IPLNO"),
                        workOrderNumber = item.Field<String>("workOrderNumber"),
                        address1 = item.Field<String>("address1"),
                        city = item.Field<String>("city"),
                        SM_Name = item.Field<String>("SM_Name"),
                        zip = item.Field<Int64?>("zip"),
                        ContractorName = item.Field<String>("ContractorName"),
                        WT_WorkType = item.Field<String>("WT_WorkType"),
                        Inv_Con_Inv_Comment = item.Field<String>("Inv_Con_Inv_Comment"),
                        Contractor_Paid_Invoice_ChildDTO = GetContractorInvoice_Paid_Child_Details(item.Field<Int64>("Inv_Con_pkeyId"))

                    }).ToList();

                
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                
            }

            return contractorinvoicepaid;
        }

        //get contractor date 
        private DataSet GetContractorInvoice_Date(Contractor_Paid_Invoice_DateDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Contractor_Paid_Invoice_Date]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();
                input_parameters.Add("@WhereClause", 1 + "#varchar#" + model.WhereClause);
                input_parameters.Add("@PageNumber", 1 + "#int#" + model.PageNumber);
                input_parameters.Add("@NoofRows", 1 + "#int#" + model.NoofRows);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#int#" + model.UserID);
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }
            return ds;
        }

        public List<dynamic> GetContractorInvoiceDetailForMobileUser(Contractor_Paid_Invoice_DateDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                List<Contractorarr> contractorarrs = new List<Contractorarr>();
                Contractorarr contractorarr = new Contractorarr();
                contractorarr.User_pkeyID = model.UserID;
                contractorarrs.Add(contractorarr);
                model.Contractorarr = contractorarrs;
                model.FromDatePaidInvoice = System.DateTime.Now.AddYears(-1);
                model.ToDateDatePaidInvoice = System.DateTime.Now;

                objDynamic = GetContractorInvoice_Details(model);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }
            return objDynamic;
        }


            public List<dynamic> GetContractorInvoice_Details(Contractor_Paid_Invoice_DateDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            string wherecondition = string.Empty;
            string contractor = null;
           
            try
            {
                if (!string.IsNullOrEmpty(model.WhereClause))
                {
                    var Data = JsonConvert.DeserializeObject<Contractor_Paid_Invoice_DateDTO>(model.WhereClause);
                    if (Data.FromDatePaidInvoice != null && Data.ToDateDatePaidInvoice != null)
                    {
                        //wherecondition = " And CAST(dat.Inv_Con_Inv_Date as date) >=   CONVERT(date,'" + Data.FromDatePaidInvoice.Value.ToString("yyyy-MM-dd") + "')  And CAST(dat.Inv_Con_Inv_Date as date) <=   CONVERT(date,'" + Data.ToDateDatePaidInvoice.Value.ToString("yyyy-MM-dd") + "')";
                        wherecondition = " And CAST(cp.Con_Pay_Payment_Date as date) >=   CONVERT(date,'" + Data.FromDatePaidInvoice.Value.ToString("yyyy-MM-dd") + "')  And CAST(cp.Con_Pay_Payment_Date as date) <=   CONVERT(date,'" + Data.ToDateDatePaidInvoice.Value.ToString("yyyy-MM-dd") + "')";
                    }
                }
                else
                {
                    if (model.FromDatePaidInvoice != null && model.ToDateDatePaidInvoice != null)
                    {
                        //wherecondition = " And CAST(dat.Inv_Con_Inv_Date as date) >=   CONVERT(date,'" + Data.FromDatePaidInvoice.Value.ToString("yyyy-MM-dd") + "')  And CAST(dat.Inv_Con_Inv_Date as date) <=   CONVERT(date,'" + Data.ToDateDatePaidInvoice.Value.ToString("yyyy-MM-dd") + "')";
                        wherecondition = " And CAST(cp.Con_Pay_Payment_Date as date) >=   CONVERT(date,'" + model.FromDatePaidInvoice.Value.ToString("yyyy-MM-dd") + "')  And CAST(cp.Con_Pay_Payment_Date as date) <=   CONVERT(date,'" + model.ToDateDatePaidInvoice.Value.ToString("yyyy-MM-dd") + "')";
                    }
                }
               
                if (model != null && model.Contractorarr != null && model.Contractorarr.Count > 0)
                {
                    for (int i = 0; i < model.Contractorarr.Count; i++)
                    {
                        contractor = contractor == null ? model.Contractorarr[i].User_pkeyID.ToString() : contractor + "," + model.Contractorarr[i].User_pkeyID.ToString();
                    }
                }
                if (!string.IsNullOrEmpty(contractor))
                {
                    wherecondition = wherecondition != null ? wherecondition + " And wo.Contractor IN(" + contractor + ")" : " And wo.Contractor IN(" + contractor + ")";
                }

                Contractor_Paid_Invoice_DateDTO contractor_Paid_Invoice_DateDTO = new Contractor_Paid_Invoice_DateDTO();
                contractor_Paid_Invoice_DateDTO.Type = 1;
                contractor_Paid_Invoice_DateDTO.PageNumber = model.PageNumber;
                contractor_Paid_Invoice_DateDTO.NoofRows = model.NoofRows;
                contractor_Paid_Invoice_DateDTO.WhereClause = wherecondition;
                contractor_Paid_Invoice_DateDTO.UserID = model.UserID;
                DataSet ds = GetContractorInvoice_Date(contractor_Paid_Invoice_DateDTO);
                //string Totalcount = ds.Tables[1].Rows[0]["count"].ToString();
                var conwherecondition = string.Empty;
                if (!string.IsNullOrEmpty(contractor))
                {
                    conwherecondition = " And wo.Contractor IN(" + contractor + ")";
                }


                var myEnumerableFeaprdm = ds.Tables[0].AsEnumerable();
                List<Contractor_Paid_Invoice_DateDTO> contractorinvoiceDate =
                   (from item in myEnumerableFeaprdm
                    select new Contractor_Paid_Invoice_DateDTO
                    {
                        Inv_Con_Sub_Total = item.Field<Decimal?>("Inv_Con_Sub_Total"),
                        Con_Pay_CheckNumber = item.Field<String>("Con_Pay_CheckNumber"),
                        Con_Pay_Amount = item.Field<Decimal?>("Con_Pay_Amount"),
                        Con_Pay_Payment_Date = item.Field<DateTime?>("Con_Pay_Payment_Date"),
                        Con_Pay_Comment = item.Field<string>("Con_Pay_Comment"),
                        ContractorName = item.Field<string>("ContractorName"),
                        Contractor_Paid_InvoiceDTO = GetClientMasterDetails(item.Field<String>("Con_Pay_CheckNumber"), conwherecondition)


                    }).ToList();

                objDynamic.Add(contractorinvoiceDate);
                //objDynamic.Add(Totalcount);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }

            return objDynamic;
        }

        //get contractor child 
        private DataSet GetContractorInvoice_Paid_Child(Contractor_Paid_Invoice_ChildDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Contractor_Paid_Invoice_Child]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();
                input_parameters.Add("@Inv_Con_pkeyId", 1 + "#bigint#" + model.Inv_Con_pkeyId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }
            return ds;
        }

        public List<Contractor_Paid_Invoice_ChildDTO> GetContractorInvoice_Paid_Child_Details(Int64 Inv_Con_pkeyId)
        {
            List<Contractor_Paid_Invoice_ChildDTO> contractorinvoicechild = new List<Contractor_Paid_Invoice_ChildDTO>();
            try
            {
                Contractor_Paid_Invoice_ChildDTO model = new Contractor_Paid_Invoice_ChildDTO();
                model.Inv_Con_pkeyId = Inv_Con_pkeyId;
                model.Type = 1;
                DataSet ds = GetContractorInvoice_Paid_Child(model);

                var myEnumerableFeaprdd = ds.Tables[0].AsEnumerable();
                contractorinvoicechild =
                   (from item in myEnumerableFeaprdd
                    select new Contractor_Paid_Invoice_ChildDTO
                    {
                        Inv_Con_Ch_pkeyId = item.Field<Int64>("Inv_Con_Ch_pkeyId"),
                        Inv_Con_Ch_Qty = item.Field<String>("Inv_Con_Ch_Qty"),
                        Inv_Con_Ch_Price = item.Field<Decimal?>("Inv_Con_Ch_Price"),
                        Inv_Con_Ch_Total = item.Field<Decimal?>("Inv_Con_Ch_Total"),
                        Inv_Con_Ch_Discount = item.Field<Decimal?>("Inv_Con_Ch_Discount"),
                        Task_Name = item.Field<String>("Task_Name"),
                        Inv_Con_Ch_Comment = item.Field<String>("Inv_Con_Ch_Comment"),
                    }).ToList();

                
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }

            return contractorinvoicechild;
        }

    }
}