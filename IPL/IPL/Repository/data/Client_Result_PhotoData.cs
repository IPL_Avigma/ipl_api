﻿using Avigma.Repository.Lib;
using IPL.Models;
using IPLApp.Models;
using IPLApp.Repository.data;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class Client_Result_PhotoData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public List<dynamic> AddClientResultPhotoData(Client_Result_PhotoDTO model)
        {

            string insertProcedure = "[CreateUpdateClientPhoto]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Client_Result_Photo_ID", 1 + "#bigint#" + model.Client_Result_Photo_ID);
                input_parameters.Add("@Client_Result_Photo_Wo_ID", 1 + "#bigint#" + model.Client_Result_Photo_Wo_ID);
                input_parameters.Add("@Client_Result_Photo_FileName", 1 + "#varchar#" + model.Client_Result_Photo_FileName);
                input_parameters.Add("@Client_Result_Photo_FilePath", 1 + "#varchar#" + model.Client_Result_Photo_FilePath);
                input_parameters.Add("@Client_Result_Photo_FileType", 1 + "#varchar#" + model.Client_Result_Photo_FileType);
                input_parameters.Add("@Client_Result_Photo_FileSize", 1 + "#varchar#" + model.Client_Result_Photo_FileSize);
                input_parameters.Add("@Client_Result_Photo_TaskId", 1 + "#bigint#" + model.Client_Result_Photo_TaskId);
                input_parameters.Add("@Client_Result_Photo_Invoice_Id", 1 + "#bigint#" + model.Client_Result_Photo_Invoice_Id);
                input_parameters.Add("@Client_Result_Photo_Bid_Id", 1 + "#bigint#" + model.Client_Result_Photo_Bid_Id);
                input_parameters.Add("@Client_Result_Photo_TaskLable_Name", 1 + "#varchar#" + model.Client_Result_Photo_TaskLable_Name);
                input_parameters.Add("@Client_Result_Photo_Lable_Status", 1 + "#int#" + model.Client_Result_Photo_Lable_Status);
                input_parameters.Add("@Client_Result_Photo_BucketName", 1 + "#varchar#" + model.Client_Result_Photo_BucketName);
                input_parameters.Add("@Client_Result_Photo_ProjectID", 1 + "#varchar#" + model.Client_Result_Photo_ProjectID);
                input_parameters.Add("@Client_Result_Photo_objectName", 1 + "#varchar#" + model.Client_Result_Photo_objectName);
                input_parameters.Add("@Client_Result_Photo_localPath", 1 + "#varchar#" + model.Client_Result_Photo_localPath);
                input_parameters.Add("@Client_Result_Photo_folder", 1 + "#varchar#" + model.Client_Result_Photo_folder);
                input_parameters.Add("@Client_Result_Photo_FolderName", 1 + "#varchar#" + model.Client_Result_Photo_FolderName);
                input_parameters.Add("@Client_Result_Photo_StatusType", 1 + "#int#" + model.Client_Result_Photo_StatusType);
                input_parameters.Add("@Client_Result_Photo_IsActive", 1 + "#bit#" + model.Client_Result_Photo_IsActive);
                input_parameters.Add("@Client_Result_Photo_IsDelete", 1 + "#bit#" + model.Client_Result_Photo_IsDelete);
                input_parameters.Add("@Client_Result_Photo_UploadBy", 1 + "#varchar#" + model.Client_Result_Photo_UploadBy);
                input_parameters.Add("@Client_Result_Photo_UploadTimestamp", 1 + "#datetime#" + model.Client_Result_Photo_UploadTimestamp);
                input_parameters.Add("@Client_Result_Photo_DateTimeOriginal", 1 + "#datetime#" + model.Client_Result_Photo_DateTimeOriginal);
                input_parameters.Add("@Client_Result_Photo_GPSLatitude", 1 + "#varchar#" + model.Client_Result_Photo_GPSLatitude);
                input_parameters.Add("@Client_Result_Photo_GPSLongitude", 1 + "#varchar#" + model.Client_Result_Photo_GPSLongitude);
                input_parameters.Add("@Client_Result_Photo_Make", 1 + "#varchar#" + model.Client_Result_Photo_Make);
                input_parameters.Add("@Client_Result_Photo_Model", 1 + "#varchar#" + model.Client_Result_Photo_Model);
                input_parameters.Add("@Client_Result_Photo_UploadFrom", 1 + "#varchar#" + model.Client_Result_Photo_UploadFrom);
                input_parameters.Add("@Client_Result_Photo_FlaggedTo", 1 + "#varchar#" + model.Client_Result_Photo_FlaggedTo);
                input_parameters.Add("@Client_Result_Photo_Caption", 1 + "#varchar#" + model.Client_Result_Photo_Caption);
                input_parameters.Add("@Client_Result_Photo_Type", 1 + "#int#" + model.Client_Result_Photo_Type);
                input_parameters.Add("@Client_Result_Photo_GPSAltitude", 1 + "#varchar#" + model.Client_Result_Photo_GPSAltitude);
                input_parameters.Add("@Client_Result_Photo_MeteringMode", 1 + "#int#" + model.Client_Result_Photo_MeteringMode);
                input_parameters.Add("@Client_Result_Photo_Seq", 1 + "#int#" + model.Client_Result_Photo_Seq);
                input_parameters.Add("@Client_Result_PhotoLabel_Name", 1 + "#varchar#" + model.Client_Result_PhotoLabel_Name);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Client_Result_Photo_Task_Bid_pkeyID", 1 + "#bigint#" + model.Client_Result_Photo_Task_Bid_pkeyID);
                //input_parameters.Add("@Client_Result_Photo_Damage_ID", 1 + "#bigint#" + model.Client_Result_Photo_Damage_ID);
                input_parameters.Add("@CRP_New_pkeyId", 1 + "#bigint#" + model.CRP_New_pkeyId);
                input_parameters.Add("@Client_Result_Photo_ID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

            }
            return obj.SqlCRUD(insertProcedure, input_parameters);



        }

        private GetClientResultPhoto_DTO GetClientResultData(DataRow dr)
        {
            GetClientResultPhoto_DTO getClientResultPhoto_DTO = new GetClientResultPhoto_DTO();

            if (!string.IsNullOrEmpty(dr["Client_Result_Photo_ID"].ToString()))
            {
                getClientResultPhoto_DTO.Client_Result_Photo_ID = Convert.ToInt64(dr["Client_Result_Photo_ID"].ToString());
            }
            if (!string.IsNullOrEmpty(dr["Client_Result_Photo_Wo_ID"].ToString()))
            {
                getClientResultPhoto_DTO.Client_Result_Photo_Wo_ID = Convert.ToInt64(dr["Client_Result_Photo_Wo_ID"].ToString());
            }

            getClientResultPhoto_DTO.Client_Result_Photo_FileName = dr["Client_Result_Photo_FileName"].ToString();
            getClientResultPhoto_DTO.Client_Result_Photo_FilePath = dr["Client_Result_Photo_FilePath"].ToString();
            getClientResultPhoto_DTO.Client_Result_Photo_FileType = dr["Client_Result_Photo_FileType"].ToString();
            getClientResultPhoto_DTO.Client_Result_Photo_FileSize = dr["Client_Result_Photo_FileSize"].ToString();
            if (!string.IsNullOrEmpty(dr["Client_Result_Photo_TaskId"].ToString()))
            {
                getClientResultPhoto_DTO.Client_Result_Photo_TaskId = Convert.ToInt64(dr["Client_Result_Photo_TaskId"].ToString());
            }

            if (!string.IsNullOrEmpty(dr["Client_Result_Photo_Invoice_Id"].ToString()))
            {
                getClientResultPhoto_DTO.Client_Result_Photo_Invoice_Id = Convert.ToInt64(dr["Client_Result_Photo_Invoice_Id"].ToString());
            }
            if (!string.IsNullOrEmpty(dr["Client_Result_Photo_Bid_Id"].ToString()))
            {
                getClientResultPhoto_DTO.Client_Result_Photo_Bid_Id = Convert.ToInt64(dr["Client_Result_Photo_Bid_Id"].ToString());
            }
            if (!string.IsNullOrEmpty(dr["Client_Result_Photo_Damage_ID"].ToString()))
            {
                getClientResultPhoto_DTO.Client_Result_Photo_Damage_ID = Convert.ToInt64(dr["Client_Result_Photo_Damage_ID"].ToString());
            }

            if (!string.IsNullOrEmpty(dr["Client_Result_Photo_WorkOrderPhotoLabel_ID"].ToString()))
            {
                getClientResultPhoto_DTO.Client_Result_Photo_WorkOrderPhotoLabel_ID = Convert.ToInt64(dr["Client_Result_Photo_WorkOrderPhotoLabel_ID"].ToString());
            }

            if (!string.IsNullOrEmpty(dr["Client_Result_Photo_StatusType"].ToString()))
            {
                getClientResultPhoto_DTO.Client_Result_Photo_StatusType = Convert.ToInt32(dr["Client_Result_Photo_StatusType"].ToString());
            }
            if (!string.IsNullOrEmpty(dr["CRP_New_pkeyId"].ToString()))
            {
                getClientResultPhoto_DTO.CRP_New_pkeyId = Convert.ToInt64(dr["CRP_New_pkeyId"].ToString());
            }

            getClientResultPhoto_DTO.Client_Result_Photo_TaskLable_Name = dr["Client_Result_Photo_TaskLable_Name"].ToString();
            getClientResultPhoto_DTO.Client_Result_Photo_BucketName = dr["Client_Result_Photo_BucketName"].ToString();
            getClientResultPhoto_DTO.Client_Result_Photo_ProjectID = dr["Client_Result_Photo_ProjectID"].ToString();
            getClientResultPhoto_DTO.Client_Result_Photo_objectName = dr["Client_Result_Photo_objectName"].ToString();
            getClientResultPhoto_DTO.Client_Result_Photo_localPath = dr["Client_Result_Photo_localPath"].ToString();
            getClientResultPhoto_DTO.Client_Result_Photo_folder = dr["Client_Result_Photo_folder"].ToString();
            getClientResultPhoto_DTO.Client_Result_Photo_FolderName = dr["Client_Result_Photo_FolderName"].ToString();

            getClientResultPhoto_DTO.Client_Result_Photo_UploadBy = dr["Client_Result_Photo_UploadBy"].ToString();
            getClientResultPhoto_DTO.Client_Result_Photo_Make = dr["Client_Result_Photo_Make"].ToString();
            getClientResultPhoto_DTO.Client_Result_Photo_Model = dr["Client_Result_Photo_Model"].ToString();
            getClientResultPhoto_DTO.Client_Result_Photo_UploadFrom = dr["Client_Result_Photo_UploadFrom"].ToString();

            getClientResultPhoto_DTO.Client_Result_Photo_GPSLatitude = dr["Client_Result_Photo_GPSLatitude"].ToString();
            getClientResultPhoto_DTO.Client_Result_Photo_GPSLongitude = dr["Client_Result_Photo_GPSLongitude"].ToString();

            if (!string.IsNullOrEmpty(dr["Client_Result_Photo_CreatedOn"].ToString()))
            {
                getClientResultPhoto_DTO.Client_Result_Photo_CreatedOn = Convert.ToDateTime(dr["Client_Result_Photo_CreatedOn"].ToString());
            }

            if (!string.IsNullOrEmpty(dr["Client_Result_Photo_DateTimeOriginal"].ToString()))
            {
                getClientResultPhoto_DTO.Client_Result_Photo_DateTimeOriginal = Convert.ToDateTime(dr["Client_Result_Photo_DateTimeOriginal"].ToString());
            }
            if (!string.IsNullOrEmpty(dr["Client_Result_Photo_UploadTimestamp"].ToString()))
            {
                getClientResultPhoto_DTO.Client_Result_Photo_UploadTimestamp = Convert.ToDateTime(dr["Client_Result_Photo_UploadTimestamp"].ToString());
            }
            if (!string.IsNullOrEmpty(dr["Client_Result_Photo_Seq"].ToString()))
            {
                getClientResultPhoto_DTO.Client_Result_Photo_Seq = Convert.ToInt32(dr["Client_Result_Photo_Seq"].ToString());
            }


            return getClientResultPhoto_DTO;
        }

        private DataSet GetClientResultPhotoMaster(GetClientResultPhoto_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_ClientPhotoMaster]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Client_Result_Photo_ID", 1 + "#bigint#" + model.Client_Result_Photo_ID);
                input_parameters.Add("@Client_Result_Photo_Wo_ID", 1 + "#bigint#" + model.Client_Result_Photo_Wo_ID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }


        public DataSet GetClientResultPhotopdfMaster(GetClientResultPhoto_DTO model)
        {
            DataSet ds = null;
            try
            {
                ds = GetClientResultPhotoMaster(model);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return ds;
        }


        private DataSet GetClientPhotoMasterByTaskData(ClientResultPhotoRef model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_ClientPhotoMasterByTaskData]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();


                input_parameters.Add("@Client_Result_Photo_Wo_ID", 1 + "#bigint#" + model.Client_Result_Photo_Wo_ID);
                input_parameters.Add("@CRP_New_Status_Type", 1 + "#bigint#" + model.CRP_New_Status_Type);
                input_parameters.Add("@CRP_New_Task_Bid_pkeyID", 1 + "#bigint#" + model.CRP_New_Task_Bid_pkeyID);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }


        private DataSet Get_WorkOrder_PastData(WorkOrder_HistoryData_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_WorkOrder_PastData]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();


                input_parameters.Add("@workOrder_ID", 1 + "#bigint#" + model.WorkOrder_ID);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> Get_WorkOrderPastData(WorkOrder_HistoryData_DTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = Get_WorkOrder_PastData(model);
                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<WorkOrder_HistoryData_DTO> WorkOrder_HistoryData_DTO =
                  (from item in myEnumerableFeaprd
                   select new WorkOrder_HistoryData_DTO
                   {

                       WorkOrder_ID = item.Field<Int64>("WorkOrder_ID"),
                       WorkOrder_Data = item.Field<String>("WorkOrder_Data"),
                       IPLNO = item.Field<String>("IPLNO"),
                       CreatedOn = item.Field<String>("CreatedOn"),
                       workOrderNumber = item.Field<String>("workOrderNumber"),


                   }).ToList();
                objDynamic.Add(WorkOrder_HistoryData_DTO);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objDynamic;
        }


        public List<dynamic> Get_ClientPhotoMasterByTaskData(ClientResultPhotoRef model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {

                // This will refresh the Workorder if it find any Error in link
                WorkOrderPicsDTO workOrderPicsDTO = new WorkOrderPicsDTO();
                WorkOrderPicsData workOrderPicsData = new WorkOrderPicsData();
                workOrderPicsDTO.Type = 3;
                workOrderPicsDTO.WorkOrder_ID = Convert.ToInt64(model.Client_Result_Photo_Wo_ID);
                workOrderPicsData.UpdateByWorkorderID(workOrderPicsDTO);


                DataSet ds = GetClientPhotoMasterByTaskData(model);

                List<GetClientResultPhoto_DTO> lstClientResultPhotoDTObid;
                List<TaskPhotoButtonDetails> lsttaskPhotoButtonDetails;
                GetClientResultPhoto_DTO getClientResultPhoto_DTO;
                TaskPhotoDetails taskPhotoDetails = new TaskPhotoDetails(); ;
                TaskPhotoButtonDetails taskPhotoButtonDetails;

                if (ds.Tables.Count > 0)
                {
                    switch (model.CRP_New_Status_Type)
                    {
                        case 1:
                        case 2:
                        case 3:
                        case 12:
                        case 13:
                            {
                                taskPhotoButtonDetails = new TaskPhotoButtonDetails();
                                DataRow[] drtaskrowB = ds.Tables[0].Select("Client_Result_Photo_StatusType = '1' ");

                                int countB = drtaskrowB.Length;
                                taskPhotoButtonDetails.Task_Photo_Button_Name = "Before(" + countB + ")";
                                taskPhotoButtonDetails.Task_Photo_Button_Id = 1;
                                getClientResultPhoto_DTO = new GetClientResultPhoto_DTO();
                                lstClientResultPhotoDTObid = new List<GetClientResultPhoto_DTO>();
                                lsttaskPhotoButtonDetails = new List<TaskPhotoButtonDetails>();

                                foreach (var itemrow in drtaskrowB)
                                {
                                    getClientResultPhoto_DTO = new GetClientResultPhoto_DTO();
                                    getClientResultPhoto_DTO = GetClientResultData(itemrow);

                                    lstClientResultPhotoDTObid.Add(getClientResultPhoto_DTO);

                                }
                                taskPhotoButtonDetails.Items = lstClientResultPhotoDTObid;
                                lsttaskPhotoButtonDetails.Add(taskPhotoButtonDetails);


                                taskPhotoButtonDetails = new TaskPhotoButtonDetails();
                                DataRow[] drtaskrowD = ds.Tables[0].Select("Client_Result_Photo_StatusType = '2' ");
                                int countD = drtaskrowD.Length;
                                taskPhotoButtonDetails.Task_Photo_Button_Name = "During(" + countD + ")";
                                taskPhotoButtonDetails.Task_Photo_Button_Id = 2;


                                getClientResultPhoto_DTO = new GetClientResultPhoto_DTO();
                                lstClientResultPhotoDTObid = new List<GetClientResultPhoto_DTO>();


                                foreach (var itemrow in drtaskrowD)
                                {
                                    getClientResultPhoto_DTO = new GetClientResultPhoto_DTO();
                                    getClientResultPhoto_DTO = GetClientResultData(itemrow);

                                    lstClientResultPhotoDTObid.Add(getClientResultPhoto_DTO);

                                }
                                taskPhotoButtonDetails.Items = lstClientResultPhotoDTObid;
                                lsttaskPhotoButtonDetails.Add(taskPhotoButtonDetails);


                                taskPhotoButtonDetails = new TaskPhotoButtonDetails();
                                DataRow[] drtaskrowA = ds.Tables[0].Select("Client_Result_Photo_StatusType = '3' ");

                                int countA = drtaskrowA.Length;
                                taskPhotoButtonDetails.Task_Photo_Button_Name = "After(" + countA + ")";
                                taskPhotoButtonDetails.Task_Photo_Button_Id = 3;
                                getClientResultPhoto_DTO = new GetClientResultPhoto_DTO();
                                lstClientResultPhotoDTObid = new List<GetClientResultPhoto_DTO>();


                                foreach (var itemrow in drtaskrowA)
                                {
                                    getClientResultPhoto_DTO = new GetClientResultPhoto_DTO();
                                    getClientResultPhoto_DTO = GetClientResultData(itemrow);

                                    lstClientResultPhotoDTObid.Add(getClientResultPhoto_DTO);

                                }
                                taskPhotoButtonDetails.Items = lstClientResultPhotoDTObid;
                                lsttaskPhotoButtonDetails.Add(taskPhotoButtonDetails);


                                // Code Added for Load Measurement // 

                                taskPhotoButtonDetails = new TaskPhotoButtonDetails();
                                DataRow[] drtaskrowM = ds.Tables[0].Select("Client_Result_Photo_StatusType = '12' ");

                                int countm = drtaskrowM.Length;
                                taskPhotoButtonDetails.Task_Photo_Button_Name = "CompleteMeasurement(" + countm + ")";
                                taskPhotoButtonDetails.Task_Photo_Button_Id = 12;
                                getClientResultPhoto_DTO = new GetClientResultPhoto_DTO();
                                lstClientResultPhotoDTObid = new List<GetClientResultPhoto_DTO>();


                                foreach (var itemrow in drtaskrowM)
                                {
                                    getClientResultPhoto_DTO = new GetClientResultPhoto_DTO();
                                    getClientResultPhoto_DTO = GetClientResultData(itemrow);

                                    lstClientResultPhotoDTObid.Add(getClientResultPhoto_DTO);

                                }
                                taskPhotoButtonDetails.Items = lstClientResultPhotoDTObid;
                                lsttaskPhotoButtonDetails.Add(taskPhotoButtonDetails);


                                taskPhotoButtonDetails = new TaskPhotoButtonDetails();
                                DataRow[] drtaskrowL = ds.Tables[0].Select("Client_Result_Photo_StatusType = '13' ");

                                int countL = drtaskrowL.Length;
                                taskPhotoButtonDetails.Task_Photo_Button_Name = "CompleteLoad(" + countL + ")";
                                taskPhotoButtonDetails.Task_Photo_Button_Id = 13;
                                getClientResultPhoto_DTO = new GetClientResultPhoto_DTO();
                                lstClientResultPhotoDTObid = new List<GetClientResultPhoto_DTO>();


                                foreach (var itemrow in drtaskrowL)
                                {
                                    getClientResultPhoto_DTO = new GetClientResultPhoto_DTO();
                                    getClientResultPhoto_DTO = GetClientResultData(itemrow);

                                    lstClientResultPhotoDTObid.Add(getClientResultPhoto_DTO);

                                }
                                taskPhotoButtonDetails.Items = lstClientResultPhotoDTObid;
                                lsttaskPhotoButtonDetails.Add(taskPhotoButtonDetails);


                                objDynamic.Add(lsttaskPhotoButtonDetails);

                                break;
                            }

                        case 4:
                        case 11:
                            {
                                taskPhotoButtonDetails = new TaskPhotoButtonDetails();
                                DataRow[] drtaskrow = ds.Tables[0].Select("Client_Result_Photo_StatusType = '4' ");

                                int count = drtaskrow.Length;
                                taskPhotoButtonDetails.Task_Photo_Button_Name = "Bid(" + count + ")";
                                taskPhotoButtonDetails.Task_Photo_Button_Id = 4;
                                getClientResultPhoto_DTO = new GetClientResultPhoto_DTO();
                                lstClientResultPhotoDTObid = new List<GetClientResultPhoto_DTO>();
                                lsttaskPhotoButtonDetails = new List<TaskPhotoButtonDetails>();

                                foreach (var itemrow in drtaskrow)
                                {
                                    getClientResultPhoto_DTO = new GetClientResultPhoto_DTO();
                                    getClientResultPhoto_DTO = GetClientResultData(itemrow);

                                    lstClientResultPhotoDTObid.Add(getClientResultPhoto_DTO);

                                }
                                taskPhotoButtonDetails.Items = lstClientResultPhotoDTObid;
                                lsttaskPhotoButtonDetails.Add(taskPhotoButtonDetails);



                                taskPhotoButtonDetails = new TaskPhotoButtonDetails();
                                DataRow[] drtaskrowM = ds.Tables[0].Select("Client_Result_Photo_StatusType = '11' ");

                                int countM = drtaskrowM.Length;
                                taskPhotoButtonDetails.Task_Photo_Button_Name = "BidMeasurement(" + countM + ")";
                                taskPhotoButtonDetails.Task_Photo_Button_Id = 11;
                                getClientResultPhoto_DTO = new GetClientResultPhoto_DTO();
                                lstClientResultPhotoDTObid = new List<GetClientResultPhoto_DTO>();
                                lsttaskPhotoButtonDetails = new List<TaskPhotoButtonDetails>();

                                foreach (var itemrow in drtaskrowM)
                                {
                                    getClientResultPhoto_DTO = new GetClientResultPhoto_DTO();
                                    getClientResultPhoto_DTO = GetClientResultData(itemrow);

                                    lstClientResultPhotoDTObid.Add(getClientResultPhoto_DTO);

                                }
                                taskPhotoButtonDetails.Items = lstClientResultPhotoDTObid;
                                lsttaskPhotoButtonDetails.Add(taskPhotoButtonDetails);


                                objDynamic.Add(lsttaskPhotoButtonDetails);
                                break;
                            }
                        case 5:
                            {
                                taskPhotoButtonDetails = new TaskPhotoButtonDetails();
                                DataRow[] drtaskrow = ds.Tables[0].Select("Client_Result_Photo_StatusType = '5' ");

                                int count = drtaskrow.Length;
                                taskPhotoButtonDetails.Task_Photo_Button_Name = "Damage(" + count + ")";
                                taskPhotoButtonDetails.Task_Photo_Button_Id = 5;
                                getClientResultPhoto_DTO = new GetClientResultPhoto_DTO();
                                lstClientResultPhotoDTObid = new List<GetClientResultPhoto_DTO>();
                                lsttaskPhotoButtonDetails = new List<TaskPhotoButtonDetails>();

                                foreach (var itemrow in drtaskrow)
                                {
                                    getClientResultPhoto_DTO = new GetClientResultPhoto_DTO();
                                    getClientResultPhoto_DTO = GetClientResultData(itemrow);

                                    lstClientResultPhotoDTObid.Add(getClientResultPhoto_DTO);

                                }
                                taskPhotoButtonDetails.Items = lstClientResultPhotoDTObid;
                                lsttaskPhotoButtonDetails.Add(taskPhotoButtonDetails);


                                objDynamic.Add(lsttaskPhotoButtonDetails);
                                break;
                            }
                        case 6:
                            {
                                taskPhotoButtonDetails = new TaskPhotoButtonDetails();
                                DataRow[] drtaskrow = ds.Tables[0].Select("Client_Result_Photo_StatusType = '6' ");

                                int count = drtaskrow.Length;
                                taskPhotoButtonDetails.Task_Photo_Button_Name = "Label(" + count + ")";
                                taskPhotoButtonDetails.Task_Photo_Button_Id = 6;
                                getClientResultPhoto_DTO = new GetClientResultPhoto_DTO();
                                lstClientResultPhotoDTObid = new List<GetClientResultPhoto_DTO>();
                                lsttaskPhotoButtonDetails = new List<TaskPhotoButtonDetails>();

                                foreach (var itemrow in drtaskrow)
                                {
                                    getClientResultPhoto_DTO = new GetClientResultPhoto_DTO();
                                    getClientResultPhoto_DTO = GetClientResultData(itemrow);

                                    lstClientResultPhotoDTObid.Add(getClientResultPhoto_DTO);

                                }
                                taskPhotoButtonDetails.Items = lstClientResultPhotoDTObid;
                                lsttaskPhotoButtonDetails.Add(taskPhotoButtonDetails);


                                objDynamic.Add(lsttaskPhotoButtonDetails);
                                break;
                            }
                        case 7:
                            {
                                taskPhotoButtonDetails = new TaskPhotoButtonDetails();
                                DataRow[] drtaskrow = ds.Tables[0].Select("Client_Result_Photo_StatusType = '7' ");

                                int count = drtaskrow.Length;
                                taskPhotoButtonDetails.Task_Photo_Button_Name = "Inspection(" + count + ")";
                                taskPhotoButtonDetails.Task_Photo_Button_Id = 7;
                                getClientResultPhoto_DTO = new GetClientResultPhoto_DTO();
                                lstClientResultPhotoDTObid = new List<GetClientResultPhoto_DTO>();
                                lsttaskPhotoButtonDetails = new List<TaskPhotoButtonDetails>();

                                foreach (var itemrow in drtaskrow)
                                {
                                    getClientResultPhoto_DTO = new GetClientResultPhoto_DTO();
                                    getClientResultPhoto_DTO = GetClientResultData(itemrow);

                                    lstClientResultPhotoDTObid.Add(getClientResultPhoto_DTO);

                                }
                                taskPhotoButtonDetails.Items = lstClientResultPhotoDTObid;
                                lsttaskPhotoButtonDetails.Add(taskPhotoButtonDetails);


                                objDynamic.Add(lsttaskPhotoButtonDetails);
                                break;
                            }
                        case 8:
                            {
                                taskPhotoButtonDetails = new TaskPhotoButtonDetails();
                                DataRow[] drtaskrow = ds.Tables[0].Select("Client_Result_Photo_StatusType = '8' ");

                                int count = drtaskrow.Length;
                                taskPhotoButtonDetails.Task_Photo_Button_Name = "Violation(" + count + ")";
                                taskPhotoButtonDetails.Task_Photo_Button_Id = 8;
                                getClientResultPhoto_DTO = new GetClientResultPhoto_DTO();
                                lstClientResultPhotoDTObid = new List<GetClientResultPhoto_DTO>();
                                lsttaskPhotoButtonDetails = new List<TaskPhotoButtonDetails>();

                                foreach (var itemrow in drtaskrow)
                                {
                                    getClientResultPhoto_DTO = new GetClientResultPhoto_DTO();
                                    getClientResultPhoto_DTO = GetClientResultData(itemrow);

                                    lstClientResultPhotoDTObid.Add(getClientResultPhoto_DTO);

                                }
                                taskPhotoButtonDetails.Items = lstClientResultPhotoDTObid;
                                lsttaskPhotoButtonDetails.Add(taskPhotoButtonDetails);


                                objDynamic.Add(lsttaskPhotoButtonDetails);
                                break;
                            }
                        case 9:
                            {
                                taskPhotoButtonDetails = new TaskPhotoButtonDetails();
                                DataRow[] drtaskrow = ds.Tables[0].Select("Client_Result_Photo_StatusType = '9' ");

                                int count = drtaskrow.Length;
                                taskPhotoButtonDetails.Task_Photo_Button_Name = "Hazard(" + count + ")";
                                taskPhotoButtonDetails.Task_Photo_Button_Id = 9;
                                getClientResultPhoto_DTO = new GetClientResultPhoto_DTO();
                                lstClientResultPhotoDTObid = new List<GetClientResultPhoto_DTO>();
                                lsttaskPhotoButtonDetails = new List<TaskPhotoButtonDetails>();

                                foreach (var itemrow in drtaskrow)
                                {
                                    getClientResultPhoto_DTO = new GetClientResultPhoto_DTO();
                                    getClientResultPhoto_DTO = GetClientResultData(itemrow);

                                    lstClientResultPhotoDTObid.Add(getClientResultPhoto_DTO);

                                }
                                taskPhotoButtonDetails.Items = lstClientResultPhotoDTObid;
                                lsttaskPhotoButtonDetails.Add(taskPhotoButtonDetails);


                                objDynamic.Add(lsttaskPhotoButtonDetails);
                                break;
                            }
                        case 10:
                            {
                                taskPhotoButtonDetails = new TaskPhotoButtonDetails();
                                DataRow[] drtaskrow = ds.Tables[0].Select("Client_Result_Photo_StatusType = '10' ");

                                int count = drtaskrow.Length;
                                taskPhotoButtonDetails.Task_Photo_Button_Name = "PCRPhotoLabel(" + count + ")";
                                taskPhotoButtonDetails.Task_Photo_Button_Id = 10;
                                getClientResultPhoto_DTO = new GetClientResultPhoto_DTO();
                                lstClientResultPhotoDTObid = new List<GetClientResultPhoto_DTO>();
                                lsttaskPhotoButtonDetails = new List<TaskPhotoButtonDetails>();

                                foreach (var itemrow in drtaskrow)
                                {
                                    getClientResultPhoto_DTO = new GetClientResultPhoto_DTO();
                                    getClientResultPhoto_DTO = GetClientResultData(itemrow);

                                    lstClientResultPhotoDTObid.Add(getClientResultPhoto_DTO);

                                }
                                taskPhotoButtonDetails.Items = lstClientResultPhotoDTObid;
                                lsttaskPhotoButtonDetails.Add(taskPhotoButtonDetails);


                                objDynamic.Add(lsttaskPhotoButtonDetails);
                                break;
                            }
                    }

                }



                #region Comment
                //var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                //List<Client_Result_PhotoDTO> ClientResultPhoto =
                //   (from item in myEnumerableFeaprd
                //    select new Client_Result_PhotoDTO
                //    {
                //        Client_Result_Photo_ID = item.Field<Int64>("Client_Result_Photo_ID"),
                //        Client_Result_Photo_Wo_ID = item.Field<Int64?>("Client_Result_Photo_Wo_ID"),
                //        Client_Result_Photo_FileName = item.Field<String>("Client_Result_Photo_FileName"),
                //        Client_Result_Photo_FilePath = item.Field<String>("Client_Result_Photo_FilePath"),
                //        Client_Result_Photo_FileType = item.Field<String>("Client_Result_Photo_FileType"),
                //        Client_Result_Photo_FileSize = item.Field<String>("Client_Result_Photo_FileSize"),
                //        Client_Result_Photo_TaskId = item.Field<Int64?>("Client_Result_Photo_TaskId"),
                //        Client_Result_Photo_Invoice_Id = item.Field<Int64?>("Client_Result_Photo_Invoice_Id"),
                //        Client_Result_Photo_Bid_Id = item.Field<Int64?>("Client_Result_Photo_Bid_Id"),
                //        Client_Result_Photo_Damage_ID = item.Field<Int64?>("Client_Result_Photo_Damage_ID"),
                //        Client_Result_Photo_WorkOrderPhotoLabel_ID = item.Field<Int64?>("Client_Result_Photo_WorkOrderPhotoLabel_ID"),
                //        Client_Result_Photo_StatusType = item.Field<int?>("Client_Result_Photo_StatusType"),
                //        Client_Result_Photo_TaskLable_Name = item.Field<String>("Client_Result_Photo_TaskLable_Name"),
                //        Client_Result_Photo_IsActive = item.Field<Boolean?>("Client_Result_Photo_IsActive"),
                //        Client_Result_Photo_localPath = item.Field<String>("Client_Result_Photo_localPath"),
                //        CRP_New_pkeyId = item.Field<Int64?>("CRP_New_pkeyId"),
                //        Client_Result_Photo_BucketName = item.Field<String>("Client_Result_Photo_BucketName"),
                //        Client_Result_Photo_ProjectID = item.Field<String>("Client_Result_Photo_ProjectID"),
                //        Client_Result_Photo_objectName = item.Field<String>("Client_Result_Photo_objectName"),
                //        Client_Result_Photo_folder = item.Field<String>("Client_Result_Photo_folder"),
                //        Client_Result_Photo_FolderName = item.Field<String>("Client_Result_Photo_FolderName"),
                //        Client_Result_Photo_GPSLatitude = item.Field<String>("Client_Result_Photo_GPSLatitude"),
                //        Client_Result_Photo_GPSLongitude = item.Field<String>("Client_Result_Photo_GPSLongitude"),
                //        Client_Result_Photo_UploadTimestamp = item.Field<DateTime?>("Client_Result_Photo_UploadTimestamp"),
                //        Client_Result_Photo_DateTimeOriginal = item.Field<DateTime?>("Client_Result_Photo_DateTimeOriginal"),
                //       // Client_Result_Photo_UploadTimestamp = item.Field<DateTime?>("Client_Result_Photo_DateTimeOriginal"),
                //        Client_Result_Photo_UploadBy = item.Field<String>("Client_Result_Photo_UploadBy"),
                //        Client_Result_Photo_Make = item.Field<String>("Client_Result_Photo_Make"),
                //        Client_Result_Photo_Model = item.Field<String>("Client_Result_Photo_Model"),
                //        Client_Result_Photo_UploadFrom = item.Field<String>("Client_Result_Photo_UploadFrom"),
                //        Client_Result_Photo_CreatedOn = item.Field<DateTime?>("Client_Result_Photo_CreatedOn"),
                //        Client_Result_Photo_Seq = item.Field<int?>("Client_Result_Photo_Seq"),

                //    }).ToList();

                //objDynamic.Add(ClientResultPhoto);

                #endregion

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;

        }

        public List<dynamic> GetClientResultPhotoDetails(GetClientResultPhoto_DTO model,int val)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                // This will refresh the Workorder if it find any Error in link
                if (val == 1)
                {
                    WorkOrderPicsDTO workOrderPicsDTO = new WorkOrderPicsDTO();
                    WorkOrderPicsData workOrderPicsData = new WorkOrderPicsData();
                    workOrderPicsDTO.Type = 3;
                    workOrderPicsDTO.WorkOrder_ID = Convert.ToInt64(model.Client_Result_Photo_Wo_ID);
                    workOrderPicsData.UpdateByWorkorderID(workOrderPicsDTO);
                }
               

                DataSet ds = GetClientResultPhotoMaster(model);

                // All 
                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<GetClientResultPhoto_DTO> ClientResultPhoto =
                   (from item in myEnumerableFeaprd
                    select new GetClientResultPhoto_DTO
                    {
                        Client_Result_Photo_ID = item.Field<Int64>("Client_Result_Photo_ID"),
                        Client_Result_Photo_Wo_ID = item.Field<Int64?>("Client_Result_Photo_Wo_ID"),
                        Client_Result_Photo_FileName = item.Field<String>("Client_Result_Photo_FileName"),
                        Client_Result_Photo_FilePath = item.Field<String>("Client_Result_Photo_FilePath"),
                        Client_Result_Photo_FileType = item.Field<String>("Client_Result_Photo_FileType"),
                        Client_Result_Photo_FileSize = item.Field<String>("Client_Result_Photo_FileSize"),
                        Client_Result_Photo_TaskId = item.Field<int?>("Client_Result_Photo_TaskId"),
                        Client_Result_Photo_Invoice_Id = item.Field<int?>("Client_Result_Photo_Invoice_Id"),
                        Client_Result_Photo_Bid_Id = item.Field<int?>("Client_Result_Photo_Bid_Id"),
                        Client_Result_Photo_Damage_ID = item.Field<int?>("Client_Result_Photo_Damage_ID"),
                        Client_Result_Photo_WorkOrderPhotoLabel_ID = item.Field<int?>("Client_Result_Photo_WorkOrderPhotoLabel_ID"),
                        Client_Result_Photo_StatusType = item.Field<int?>("Client_Result_Photo_StatusType"),
                        Client_Result_Photo_TaskLable_Name = item.Field<String>("Client_Result_Photo_TaskLable_Name"),
                        Client_Result_Photo_IsActive = item.Field<Boolean?>("Client_Result_Photo_IsActive"),
                        Client_Result_Photo_localPath = item.Field<String>("Client_Result_Photo_localPath"),
                        CRP_New_pkeyId = item.Field<int?>("CRP_New_pkeyId"),
                        Client_Result_Photo_BucketName = item.Field<String>("Client_Result_Photo_BucketName"),
                        Client_Result_Photo_ProjectID = item.Field<String>("Client_Result_Photo_ProjectID"),
                        Client_Result_Photo_objectName = item.Field<String>("Client_Result_Photo_objectName"),
                        Client_Result_Photo_folder = item.Field<String>("Client_Result_Photo_folder"),
                        Client_Result_Photo_FolderName = item.Field<String>("Client_Result_Photo_FolderName"),
                        Client_Result_Photo_GPSLatitude = item.Field<String>("Client_Result_Photo_GPSLatitude"),
                        Client_Result_Photo_GPSLongitude = item.Field<String>("Client_Result_Photo_GPSLongitude"),
                        Client_Result_Photo_UploadTimestamp = item.Field<DateTime?>("Client_Result_Photo_UploadTimestamp"),
                        Client_Result_Photo_DateTimeOriginal = item.Field<DateTime?>("Client_Result_Photo_DateTimeOriginal"),
                        //Client_Result_Photo_UploadTimestamp = item.Field<DateTime?>("Client_Result_Photo_DateTimeOriginal"),
                        Client_Result_Photo_UploadBy = item.Field<String>("Client_Result_Photo_UploadBy"),
                        Client_Result_Photo_Make = item.Field<String>("Client_Result_Photo_Make"),
                        Client_Result_Photo_Model = item.Field<String>("Client_Result_Photo_Model"),
                        Client_Result_Photo_UploadFrom = item.Field<String>("Client_Result_Photo_UploadFrom"),
                        Client_Result_Photo_CreatedOn = item.Field<DateTime?>("Client_Result_Photo_CreatedOn"),
                        Client_Result_Photo_Seq = item.Field<int?>("Client_Result_Photo_Seq"),

                    }).ToList();

                objDynamic.Add(ClientResultPhoto);


                // UnFlag
                var myEnumerableUnlabelprd = ds.Tables[1].AsEnumerable();
                List<GetClientResultPhoto_DTO> ClientResulUnlabeltPhoto =
                   (from item in myEnumerableUnlabelprd
                    select new GetClientResultPhoto_DTO
                    {
                        Client_Result_Photo_ID = item.Field<Int64>("Client_Result_Photo_ID"),
                        Client_Result_Photo_Wo_ID = item.Field<Int64?>("Client_Result_Photo_Wo_ID"),
                        Client_Result_Photo_FileName = item.Field<String>("Client_Result_Photo_FileName"),
                        Client_Result_Photo_FilePath = item.Field<String>("Client_Result_Photo_FilePath"),
                        Client_Result_Photo_FileType = item.Field<String>("Client_Result_Photo_FileType"),
                        Client_Result_Photo_FileSize = item.Field<String>("Client_Result_Photo_FileSize"),
                        Client_Result_Photo_TaskId = item.Field<int?>("Client_Result_Photo_TaskId"),
                        Client_Result_Photo_Invoice_Id = item.Field<int?>("Client_Result_Photo_Invoice_Id"),
                        Client_Result_Photo_Bid_Id = item.Field<int?>("Client_Result_Photo_Bid_Id"),
                        Client_Result_Photo_Damage_ID = item.Field<int?>("Client_Result_Photo_Damage_ID"),
                        Client_Result_Photo_WorkOrderPhotoLabel_ID = item.Field<int?>("Client_Result_Photo_WorkOrderPhotoLabel_ID"),
                        Client_Result_Photo_StatusType = item.Field<int?>("Client_Result_Photo_StatusType"),
                        Client_Result_Photo_TaskLable_Name = item.Field<String>("Client_Result_Photo_TaskLable_Name"),
                        Client_Result_Photo_IsActive = item.Field<Boolean?>("Client_Result_Photo_IsActive"),
                        Client_Result_Photo_localPath = item.Field<String>("Client_Result_Photo_localPath"),
                        CRP_New_pkeyId = item.Field<int?>("CRP_New_pkeyId"),
                        Client_Result_Photo_BucketName = item.Field<String>("Client_Result_Photo_BucketName"),
                        Client_Result_Photo_ProjectID = item.Field<String>("Client_Result_Photo_ProjectID"),
                        Client_Result_Photo_objectName = item.Field<String>("Client_Result_Photo_objectName"),
                        Client_Result_Photo_folder = item.Field<String>("Client_Result_Photo_folder"),
                        Client_Result_Photo_FolderName = item.Field<String>("Client_Result_Photo_FolderName"),
                        Client_Result_Photo_GPSLatitude = item.Field<String>("Client_Result_Photo_GPSLatitude"),
                        Client_Result_Photo_GPSLongitude = item.Field<String>("Client_Result_Photo_GPSLongitude"),
                        Client_Result_Photo_UploadTimestamp = item.Field<DateTime?>("Client_Result_Photo_UploadTimestamp"),
                        Client_Result_Photo_DateTimeOriginal = item.Field<DateTime?>("Client_Result_Photo_DateTimeOriginal"),
                        //Client_Result_Photo_UploadTimestamp = item.Field<DateTime?>("Client_Result_Photo_DateTimeOriginal"),
                        Client_Result_Photo_UploadBy = item.Field<String>("Client_Result_Photo_UploadBy"),
                        Client_Result_Photo_Make = item.Field<String>("Client_Result_Photo_Make"),
                        Client_Result_Photo_Model = item.Field<String>("Client_Result_Photo_Model"),
                        Client_Result_Photo_UploadFrom = item.Field<String>("Client_Result_Photo_UploadFrom"),
                        Client_Result_Photo_CreatedOn = item.Field<DateTime?>("Client_Result_Photo_CreatedOn"),
                        Client_Result_Photo_Seq = item.Field<int?>("Client_Result_Photo_Seq"),
                    }).ToList();

                objDynamic.Add(ClientResulUnlabeltPhoto);


                List<GetClientResultPhoto_DTO> lstClientResultPhotoDTObid;
                List<TaskPhotoButtonDetails> lsttaskPhotoButtonDetails;
                List<GetClientResultPhoto_DTO> lstClientResultPhotoDTOMeasurement;
                GetClientResultPhoto_DTO getClientResultPhoto_DTO;
                TaskPhotoDetails taskPhotoDetails;
                TaskPhotoButtonDetails taskPhotoButtonDetails;
                int taskcount = ds.Tables[3].Rows.Count;
                if (taskcount != 0)
                {
                    List<dynamic> FLagobjDynamic = new List<dynamic>();
                    DataRow[] drtaskrow = ds.Tables[3].Select("Orderby = 'A' ");

                    if (drtaskrow.Length != 0)
                    {
                        foreach (var item in drtaskrow)
                        {
                            taskPhotoDetails = new TaskPhotoDetails();
                            taskPhotoButtonDetails = new TaskPhotoButtonDetails();
                            TaskPhotoButtonDetails taskPhotoMeasureMentButtonDetails = new TaskPhotoButtonDetails();
                            taskPhotoButtonDetails.Task_Photo_Button_Name = "Bid";
                            taskPhotoButtonDetails.Task_Photo_Button_Id = 4;

                            taskPhotoMeasureMentButtonDetails.Task_Photo_Button_Name = "Measurement";
                            taskPhotoMeasureMentButtonDetails.Task_Photo_Button_Id = 11;

                            taskPhotoDetails.Task_Bid_pkeyID = Convert.ToInt64(item["Task_Bid_pkeyID"].ToString());
                            taskPhotoDetails.Task_Photo_Label_Name = item["Task_Photo_Label_Name"].ToString();
                            getClientResultPhoto_DTO = new GetClientResultPhoto_DTO();
                            lstClientResultPhotoDTObid = new List<GetClientResultPhoto_DTO>();
                            lstClientResultPhotoDTOMeasurement = new List<GetClientResultPhoto_DTO>();
                         
                            lsttaskPhotoButtonDetails = new List<TaskPhotoButtonDetails>();
                            DataRow[] drBidrow = ds.Tables[2].Select("Client_Result_Photo_Bid_Id = " + taskPhotoDetails.Task_Bid_pkeyID + " ");
                            if (drBidrow.Length != 0)
                            {


                                foreach (var itemrow in drBidrow)
                                {
                                    getClientResultPhoto_DTO = new GetClientResultPhoto_DTO();
                                    getClientResultPhoto_DTO = GetClientResultData(itemrow);
                                    switch (getClientResultPhoto_DTO.Client_Result_Photo_StatusType)
                                    {

                                        case 4:
                                            {
                                                lstClientResultPhotoDTObid.Add(getClientResultPhoto_DTO);
                                                break;
                                            }
                                        case 11:
                                            {
                                                lstClientResultPhotoDTOMeasurement.Add(getClientResultPhoto_DTO);
                                                break;
                                            }
                                    }
                                    

                                }
                                if (lstClientResultPhotoDTObid.Count != 0)
                                {
                                    taskPhotoButtonDetails.Items = lstClientResultPhotoDTObid;
                                    lsttaskPhotoButtonDetails.Add(taskPhotoButtonDetails);
                                }
                                if (lstClientResultPhotoDTOMeasurement.Count != 0)
                                {
                                    taskPhotoMeasureMentButtonDetails.Items = lstClientResultPhotoDTOMeasurement;
                                    lsttaskPhotoButtonDetails.Add(taskPhotoMeasureMentButtonDetails);
                                }

                                taskPhotoDetails.Items = lsttaskPhotoButtonDetails;
                                FLagobjDynamic.Add(taskPhotoDetails);
                            }
                        }
                    }

                    DataRow[] drtaskInvoicerow = ds.Tables[3].Select("Orderby = 'B' ");
                    if (drtaskInvoicerow.Length != 0)
                    {
                        foreach (var itemInvoice in drtaskInvoicerow)
                        {
                            taskPhotoDetails = new TaskPhotoDetails();
                            lsttaskPhotoButtonDetails = new List<TaskPhotoButtonDetails>();


                            taskPhotoButtonDetails = new TaskPhotoButtonDetails();
                            TaskPhotoButtonDetails taskPhotoAfterButtonDetails = new TaskPhotoButtonDetails();
                            TaskPhotoButtonDetails taskPhotoDuringButtonDetails = new TaskPhotoButtonDetails();
                            TaskPhotoButtonDetails taskPhotoMeasureMentButtonDetails = new TaskPhotoButtonDetails();
                            TaskPhotoButtonDetails taskPhotoLoadButtonDetails = new TaskPhotoButtonDetails();

                            taskPhotoButtonDetails.Task_Photo_Button_Name = "Before";
                            taskPhotoButtonDetails.Task_Photo_Button_Id = 1;
                            taskPhotoDuringButtonDetails.Task_Photo_Button_Name = "During";
                            taskPhotoDuringButtonDetails.Task_Photo_Button_Id = 2;
                            taskPhotoAfterButtonDetails.Task_Photo_Button_Name = "After";
                            taskPhotoAfterButtonDetails.Task_Photo_Button_Id = 3;

                            taskPhotoMeasureMentButtonDetails.Task_Photo_Button_Name = "MeasureMent";
                            taskPhotoMeasureMentButtonDetails.Task_Photo_Button_Id = 12;
                            taskPhotoLoadButtonDetails.Task_Photo_Button_Name = "Load";
                            taskPhotoLoadButtonDetails.Task_Photo_Button_Id = 13;

                            lstClientResultPhotoDTObid = new List<GetClientResultPhoto_DTO>();
                            List<GetClientResultPhoto_DTO> lstClientAfterResultPhotoDTObid = new List<GetClientResultPhoto_DTO>();
                            List<GetClientResultPhoto_DTO> lstClientDuringResultPhotoDTObid = new List<GetClientResultPhoto_DTO>();
                            List<GetClientResultPhoto_DTO> lstClientMeasureMentResultPhotoDTO = new List<GetClientResultPhoto_DTO>();
                            List<GetClientResultPhoto_DTO> lstClientLoadResultPhotoDTO = new List<GetClientResultPhoto_DTO>();

                            taskPhotoDetails.Task_Bid_pkeyID = Convert.ToInt64(itemInvoice["Task_Bid_pkeyID"].ToString());
                            taskPhotoDetails.Task_Photo_Label_Name = itemInvoice["Task_Photo_Label_Name"].ToString();
                            DataRow[] drInvoiceBeforerow = ds.Tables[2].Select("Client_Result_Photo_Invoice_Id = " + taskPhotoDetails.Task_Bid_pkeyID + "");

                            if (drInvoiceBeforerow.Length != 0)
                            {


                                getClientResultPhoto_DTO = new GetClientResultPhoto_DTO();

                                foreach (var itemrow in drInvoiceBeforerow)
                                {


                                    getClientResultPhoto_DTO = new GetClientResultPhoto_DTO();
                                    getClientResultPhoto_DTO = GetClientResultData(itemrow);
                                    //if (getClientResultPhoto_DTO.Client_Result_Photo_StatusType == 1)
                                    //{

                                    //    lstClientResultPhotoDTObid.Add(getClientResultPhoto_DTO);
                                    //}
                                    //else if (getClientResultPhoto_DTO.Client_Result_Photo_StatusType == 2)
                                    //{

                                    //    lstClientDuringResultPhotoDTObid.Add(getClientResultPhoto_DTO);
                                    //}
                                    //else if (getClientResultPhoto_DTO.Client_Result_Photo_StatusType == 3)
                                    //{

                                    //    lstClientAfterResultPhotoDTObid.Add(getClientResultPhoto_DTO);
                                    //}

                                    switch (getClientResultPhoto_DTO.Client_Result_Photo_StatusType)
                                    {
                                        case 1:
                                            {
                                                lstClientResultPhotoDTObid.Add(getClientResultPhoto_DTO);
                                                break;
                                            }
                                        case 2:
                                            {
                                                lstClientDuringResultPhotoDTObid.Add(getClientResultPhoto_DTO);
                                                break;
                                            }
                                        case 3:
                                            {
                                                lstClientAfterResultPhotoDTObid.Add(getClientResultPhoto_DTO);
                                                break;
                                            }
                                        case 12:
                                            {
                                                lstClientMeasureMentResultPhotoDTO.Add(getClientResultPhoto_DTO);
                                                break;
                                            }
                                        case 13:
                                            {
                                                lstClientLoadResultPhotoDTO.Add(getClientResultPhoto_DTO);
                                                break;
                                            }
                                    }

                                  



                                }

                                if (lstClientResultPhotoDTObid.Count != 0)
                                {
                                    taskPhotoButtonDetails.Items = lstClientResultPhotoDTObid;
                                    lsttaskPhotoButtonDetails.Add(taskPhotoButtonDetails);
                                }

                                if (lstClientDuringResultPhotoDTObid.Count != 0)
                                {
                                    taskPhotoDuringButtonDetails.Items = lstClientDuringResultPhotoDTObid;
                                    lsttaskPhotoButtonDetails.Add(taskPhotoDuringButtonDetails);
                                }

                                if (lstClientAfterResultPhotoDTObid.Count != 0)
                                {
                                    taskPhotoAfterButtonDetails.Items = lstClientAfterResultPhotoDTObid;
                                    lsttaskPhotoButtonDetails.Add(taskPhotoAfterButtonDetails);
                                }
                                if (lstClientMeasureMentResultPhotoDTO.Count != 0)
                                {
                                    taskPhotoMeasureMentButtonDetails.Items = lstClientMeasureMentResultPhotoDTO;
                                    lsttaskPhotoButtonDetails.Add(taskPhotoMeasureMentButtonDetails);
                                }
                                if (lstClientLoadResultPhotoDTO.Count != 0)
                                {
                                    taskPhotoLoadButtonDetails.Items = lstClientLoadResultPhotoDTO;
                                    lsttaskPhotoButtonDetails.Add(taskPhotoLoadButtonDetails);
                                }
                                taskPhotoDetails.Items = lsttaskPhotoButtonDetails;

                                FLagobjDynamic.Add(taskPhotoDetails);


                            }


                        }
                    }

                    DataRow[] drtaskDamagerow = ds.Tables[3].Select("Orderby = 'C' ");

                    if (drtaskDamagerow.Length != 0)
                    {
                        foreach (var item in drtaskDamagerow)
                        {
                            taskPhotoDetails = new TaskPhotoDetails();
                            taskPhotoButtonDetails = new TaskPhotoButtonDetails();
                            taskPhotoButtonDetails.Task_Photo_Button_Name = "Damage";
                            taskPhotoButtonDetails.Task_Photo_Button_Id = 5;

                            taskPhotoDetails.Task_Bid_pkeyID = Convert.ToInt64(item["Task_Bid_pkeyID"].ToString());
                            taskPhotoDetails.Task_Photo_Label_Name = item["Task_Photo_Label_Name"].ToString();
                            getClientResultPhoto_DTO = new GetClientResultPhoto_DTO();
                            lstClientResultPhotoDTObid = new List<GetClientResultPhoto_DTO>();
                            lsttaskPhotoButtonDetails = new List<TaskPhotoButtonDetails>();
                            DataRow[] drBidrow = ds.Tables[2].Select("Client_Result_Photo_Damage_ID = " + taskPhotoDetails.Task_Bid_pkeyID + " ");
                            if (drBidrow.Length != 0)
                            {


                                foreach (var itemrow in drBidrow)
                                {
                                    getClientResultPhoto_DTO = new GetClientResultPhoto_DTO();
                                    getClientResultPhoto_DTO = GetClientResultData(itemrow);

                                    lstClientResultPhotoDTObid.Add(getClientResultPhoto_DTO);

                                }
                                taskPhotoButtonDetails.Items = lstClientResultPhotoDTObid;
                                lsttaskPhotoButtonDetails.Add(taskPhotoButtonDetails);
                                taskPhotoDetails.Items = lsttaskPhotoButtonDetails;

                                FLagobjDynamic.Add(taskPhotoDetails);
                            }
                        }
                    }

                    //For Inspection


                    DataRow[] drtaskInspectionrow = ds.Tables[3].Select("Orderby = 'D' ");

                    if (drtaskInspectionrow.Length != 0)
                    {
                        foreach (var item in drtaskInspectionrow)
                        {
                            taskPhotoDetails = new TaskPhotoDetails();
                            taskPhotoButtonDetails = new TaskPhotoButtonDetails();
                            taskPhotoButtonDetails.Task_Photo_Button_Name = "Inspection";
                            taskPhotoButtonDetails.Task_Photo_Button_Id = 7;

                            taskPhotoDetails.Task_Bid_pkeyID = Convert.ToInt64(item["Task_Bid_pkeyID"].ToString());
                            taskPhotoDetails.Task_Photo_Label_Name = item["Task_Photo_Label_Name"].ToString();
                            getClientResultPhoto_DTO = new GetClientResultPhoto_DTO();
                            lstClientResultPhotoDTObid = new List<GetClientResultPhoto_DTO>();
                            lsttaskPhotoButtonDetails = new List<TaskPhotoButtonDetails>();
                            DataRow[] drBidrow = ds.Tables[2].Select("Client_Result_Photo_Inspection_Id = " + taskPhotoDetails.Task_Bid_pkeyID + " ");
                            if (drBidrow.Length != 0)
                            {


                                foreach (var itemrow in drBidrow)
                                {
                                    getClientResultPhoto_DTO = new GetClientResultPhoto_DTO();
                                    getClientResultPhoto_DTO = GetClientResultData(itemrow);

                                    lstClientResultPhotoDTObid.Add(getClientResultPhoto_DTO);

                                }
                                taskPhotoButtonDetails.Items = lstClientResultPhotoDTObid;
                                lsttaskPhotoButtonDetails.Add(taskPhotoButtonDetails);
                                taskPhotoDetails.Items = lsttaskPhotoButtonDetails;

                                FLagobjDynamic.Add(taskPhotoDetails);
                            }
                        }
                    }


                    //Hazard
                    DataRow[] drtaskHazardrow = ds.Tables[3].Select("Orderby = 'H' ");

                    if (drtaskHazardrow.Length != 0)
                    {
                        foreach (var item in drtaskHazardrow)
                        {
                            taskPhotoDetails = new TaskPhotoDetails();
                            taskPhotoButtonDetails = new TaskPhotoButtonDetails();
                            taskPhotoButtonDetails.Task_Photo_Button_Name = "Hazard";
                            taskPhotoButtonDetails.Task_Photo_Button_Id = 9;

                            taskPhotoDetails.Task_Bid_pkeyID = Convert.ToInt64(item["Task_Bid_pkeyID"].ToString());
                            taskPhotoDetails.Task_Photo_Label_Name = item["Task_Photo_Label_Name"].ToString();
                            getClientResultPhoto_DTO = new GetClientResultPhoto_DTO();
                            lstClientResultPhotoDTObid = new List<GetClientResultPhoto_DTO>();
                            lsttaskPhotoButtonDetails = new List<TaskPhotoButtonDetails>();
                            DataRow[] drBidrow = ds.Tables[2].Select("Client_Result_Photo_Hazard_Id = " + taskPhotoDetails.Task_Bid_pkeyID + " ");
                            if (drBidrow.Length != 0)
                            {


                                foreach (var itemrow in drBidrow)
                                {
                                    getClientResultPhoto_DTO = new GetClientResultPhoto_DTO();
                                    getClientResultPhoto_DTO = GetClientResultData(itemrow);

                                    lstClientResultPhotoDTObid.Add(getClientResultPhoto_DTO);

                                }
                                taskPhotoButtonDetails.Items = lstClientResultPhotoDTObid;
                                lsttaskPhotoButtonDetails.Add(taskPhotoButtonDetails);
                                taskPhotoDetails.Items = lsttaskPhotoButtonDetails;

                                FLagobjDynamic.Add(taskPhotoDetails);
                            }
                        }
                    }


                    //Violation
                    DataRow[] drtaskViolationrow = ds.Tables[3].Select("Orderby = 'V' ");

                    if (drtaskViolationrow.Length != 0)
                    {
                        foreach (var item in drtaskViolationrow)
                        {
                            taskPhotoDetails = new TaskPhotoDetails();
                            taskPhotoButtonDetails = new TaskPhotoButtonDetails();
                            taskPhotoButtonDetails.Task_Photo_Button_Name = "Violation";
                            taskPhotoButtonDetails.Task_Photo_Button_Id = 8;

                            taskPhotoDetails.Task_Bid_pkeyID = Convert.ToInt64(item["Task_Bid_pkeyID"].ToString());
                            taskPhotoDetails.Task_Photo_Label_Name = item["Task_Photo_Label_Name"].ToString();
                            getClientResultPhoto_DTO = new GetClientResultPhoto_DTO();
                            lstClientResultPhotoDTObid = new List<GetClientResultPhoto_DTO>();
                            lsttaskPhotoButtonDetails = new List<TaskPhotoButtonDetails>();
                            DataRow[] drBidrow = ds.Tables[2].Select("Client_Result_Photo_Violation_Id = " + taskPhotoDetails.Task_Bid_pkeyID + " ");
                            if (drBidrow.Length != 0)
                            {


                                foreach (var itemrow in drBidrow)
                                {
                                    getClientResultPhoto_DTO = new GetClientResultPhoto_DTO();
                                    getClientResultPhoto_DTO = GetClientResultData(itemrow);

                                    lstClientResultPhotoDTObid.Add(getClientResultPhoto_DTO);

                                }
                                taskPhotoButtonDetails.Items = lstClientResultPhotoDTObid;
                                lsttaskPhotoButtonDetails.Add(taskPhotoButtonDetails);
                                taskPhotoDetails.Items = lsttaskPhotoButtonDetails;

                                FLagobjDynamic.Add(taskPhotoDetails);
                            }
                        }
                    }


                    //PCRPhotoLabel
                    DataRow[] drPCRPhotoLabel = ds.Tables[3].Select("Orderby = 'F' ");

                    if (drPCRPhotoLabel.Length != 0)
                    {
                        foreach (var item in drPCRPhotoLabel)
                        {
                            taskPhotoDetails = new TaskPhotoDetails();
                            taskPhotoButtonDetails = new TaskPhotoButtonDetails();
                            taskPhotoButtonDetails.Task_Photo_Button_Name = "Labels";
                            taskPhotoButtonDetails.Task_Photo_Button_Id = 10;

                            taskPhotoDetails.Task_Bid_pkeyID = Convert.ToInt64(item["Task_Bid_pkeyID"].ToString());
                            taskPhotoDetails.Task_Photo_Label_Name = item["Task_Photo_Label_Name"].ToString();
                            getClientResultPhoto_DTO = new GetClientResultPhoto_DTO();
                            lstClientResultPhotoDTObid = new List<GetClientResultPhoto_DTO>();
                            lsttaskPhotoButtonDetails = new List<TaskPhotoButtonDetails>();
                            DataRow[] drBidrow = ds.Tables[2].Select("Client_Result_Photo_PCRPhotoLabel_ID = " + taskPhotoDetails.Task_Bid_pkeyID + " ");
                            if (drBidrow.Length != 0)
                            {


                                foreach (var itemrow in drBidrow)
                                {
                                    getClientResultPhoto_DTO = new GetClientResultPhoto_DTO();
                                    getClientResultPhoto_DTO = GetClientResultData(itemrow);

                                    lstClientResultPhotoDTObid.Add(getClientResultPhoto_DTO);

                                }
                                taskPhotoButtonDetails.Items = lstClientResultPhotoDTObid;
                                lsttaskPhotoButtonDetails.Add(taskPhotoButtonDetails);
                                taskPhotoDetails.Items = lsttaskPhotoButtonDetails;

                                FLagobjDynamic.Add(taskPhotoDetails);
                            }
                        }
                    }

                    // For Label

                    DataRow[] drtaskCustomrow = ds.Tables[3].Select("Orderby = 'E' ");

                    if (drtaskCustomrow.Length != 0)
                    {
                        foreach (var item in drtaskCustomrow)
                        {
                            taskPhotoDetails = new TaskPhotoDetails();
                            taskPhotoButtonDetails = new TaskPhotoButtonDetails();
                            taskPhotoButtonDetails.Task_Photo_Button_Name = "Label";
                            taskPhotoButtonDetails.Task_Photo_Button_Id = 6;

                            taskPhotoDetails.Task_Bid_pkeyID = Convert.ToInt64(item["Task_Bid_pkeyID"].ToString());
                            taskPhotoDetails.Task_Photo_Label_Name = item["Task_Photo_Label_Name"].ToString();
                            getClientResultPhoto_DTO = new GetClientResultPhoto_DTO();
                            lstClientResultPhotoDTObid = new List<GetClientResultPhoto_DTO>();
                            lsttaskPhotoButtonDetails = new List<TaskPhotoButtonDetails>();
                            DataRow[] drBidrow = ds.Tables[2].Select("Client_Result_Photo_WorkOrderPhotoLabel_ID = " + taskPhotoDetails.Task_Bid_pkeyID + " ");
                            if (drBidrow.Length != 0)
                            {


                                foreach (var itemrow in drBidrow)
                                {
                                    getClientResultPhoto_DTO = new GetClientResultPhoto_DTO();
                                    getClientResultPhoto_DTO = GetClientResultData(itemrow);

                                    lstClientResultPhotoDTObid.Add(getClientResultPhoto_DTO);

                                }
                                taskPhotoButtonDetails.Items = lstClientResultPhotoDTObid;
                                lsttaskPhotoButtonDetails.Add(taskPhotoButtonDetails);
                                taskPhotoDetails.Items = lsttaskPhotoButtonDetails;

                                FLagobjDynamic.Add(taskPhotoDetails);
                            }
                        }
                    }



                    objDynamic.Add(FLagobjDynamic);

                }
                else
                {
                    objDynamic.Add(null);
                }


                // Documents 
                if (ds.Tables.Count > 3)
                {
                    var myEnumerableDocument = ds.Tables[4].AsEnumerable();
                    List<GetClientResultPhoto_DTO> ClientResultPhotoDocument =
                       (from item in myEnumerableDocument
                        select new GetClientResultPhoto_DTO
                        {
                            Client_Result_Photo_ID = item.Field<Int64>("Client_Result_Photo_ID"),
                            Client_Result_Photo_Wo_ID = item.Field<Int64?>("Client_Result_Photo_Wo_ID"),
                            Client_Result_Photo_FileName = item.Field<String>("Client_Result_Photo_FileName"),
                            Client_Result_Photo_FilePath = item.Field<String>("Client_Result_Photo_FilePath"),
                            Client_Result_Photo_FileType = item.Field<String>("Client_Result_Photo_FileType"),
                            Client_Result_Photo_FileSize = item.Field<String>("Client_Result_Photo_FileSize"),
                            Client_Result_Photo_TaskId = item.Field<int?>("Client_Result_Photo_TaskId"),
                            Client_Result_Photo_Invoice_Id = item.Field<int?>("Client_Result_Photo_Invoice_Id"),
                            Client_Result_Photo_Bid_Id = item.Field<int?>("Client_Result_Photo_Bid_Id"),
                            Client_Result_Photo_Damage_ID = item.Field<int?>("Client_Result_Photo_Damage_ID"),
                            Client_Result_Photo_WorkOrderPhotoLabel_ID = item.Field<int?>("Client_Result_Photo_WorkOrderPhotoLabel_ID"),
                            Client_Result_Photo_StatusType = item.Field<int?>("Client_Result_Photo_StatusType"),
                            Client_Result_Photo_TaskLable_Name = item.Field<String>("Client_Result_Photo_TaskLable_Name"),
                            Client_Result_Photo_IsActive = item.Field<Boolean?>("Client_Result_Photo_IsActive"),
                            Client_Result_Photo_localPath = item.Field<String>("Client_Result_Photo_localPath"),
                            CRP_New_pkeyId = item.Field<int?>("CRP_New_pkeyId"),
                            Client_Result_Photo_BucketName = item.Field<String>("Client_Result_Photo_BucketName"),
                            Client_Result_Photo_ProjectID = item.Field<String>("Client_Result_Photo_ProjectID"),
                            Client_Result_Photo_objectName = item.Field<String>("Client_Result_Photo_objectName"),
                            Client_Result_Photo_folder = item.Field<String>("Client_Result_Photo_folder"),
                            Client_Result_Photo_FolderName = item.Field<String>("Client_Result_Photo_FolderName"),
                            Client_Result_Photo_Seq = item.Field<int?>("Client_Result_Photo_Seq"),

                        }).ToList();

                    objDynamic.Add(ClientResultPhotoDocument);
                }
                //deleted photos
                if (ds.Tables.Count > 4)
                {
                    var myEnumerableDocument = ds.Tables[5].AsEnumerable();
                    List<GetClientResultPhoto_DTO> ClientResultPhotoDocument =
                       (from item in myEnumerableDocument
                        select new GetClientResultPhoto_DTO
                        {
                            Client_Result_Photo_ID = item.Field<Int64>("Client_Result_Photo_ID"),
                            Client_Result_Photo_Wo_ID = item.Field<Int64?>("Client_Result_Photo_Wo_ID"),
                            Client_Result_Photo_FileName = item.Field<String>("Client_Result_Photo_FileName"),
                            Client_Result_Photo_FilePath = item.Field<String>("Client_Result_Photo_FilePath"),
                            Client_Result_Photo_FileType = item.Field<String>("Client_Result_Photo_FileType"),
                            Client_Result_Photo_FileSize = item.Field<String>("Client_Result_Photo_FileSize"),
                            Client_Result_Photo_TaskId = item.Field<int?>("Client_Result_Photo_TaskId"),
                            Client_Result_Photo_Invoice_Id = item.Field<int?>("Client_Result_Photo_Invoice_Id"),
                            Client_Result_Photo_Bid_Id = item.Field<int?>("Client_Result_Photo_Bid_Id"),
                            Client_Result_Photo_Damage_ID = item.Field<int?>("Client_Result_Photo_Damage_ID"),
                            Client_Result_Photo_WorkOrderPhotoLabel_ID = item.Field<int?>("Client_Result_Photo_WorkOrderPhotoLabel_ID"),
                            Client_Result_Photo_StatusType = item.Field<int?>("Client_Result_Photo_StatusType"),
                            Client_Result_Photo_TaskLable_Name = item.Field<String>("Client_Result_Photo_TaskLable_Name"),
                            Client_Result_Photo_IsActive = item.Field<Boolean?>("Client_Result_Photo_IsActive"),
                            Client_Result_Photo_localPath = item.Field<String>("Client_Result_Photo_localPath"),
                            CRP_New_pkeyId = item.Field<int?>("CRP_New_pkeyId"),
                            Client_Result_Photo_BucketName = item.Field<String>("Client_Result_Photo_BucketName"),
                            Client_Result_Photo_ProjectID = item.Field<String>("Client_Result_Photo_ProjectID"),
                            Client_Result_Photo_objectName = item.Field<String>("Client_Result_Photo_objectName"),
                            Client_Result_Photo_folder = item.Field<String>("Client_Result_Photo_folder"),
                            Client_Result_Photo_FolderName = item.Field<String>("Client_Result_Photo_FolderName"),
                            Client_Result_Photo_Seq = item.Field<int?>("Client_Result_Photo_Seq"),

                        }).ToList();

                    objDynamic.Add(ClientResultPhotoDocument);
                }




            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }




        //get single image for work order
        private DataSet GetSingaleImageforInstruction(Client_Result_PhotoDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Singleimageinstruction]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Client_Result_Photo_StatusType", 1 + "#int#" + model.Client_Result_Photo_StatusType);
                input_parameters.Add("@Client_Result_Photo_Wo_ID", 1 + "#bigint#" + model.Client_Result_Photo_Wo_ID);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetSingleImageInstructionDetails(Client_Result_PhotoDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetSingaleImageforInstruction(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Client_Result_PhotoDTO> ImageDetails =
                   (from item in myEnumerableFeaprd
                    select new Client_Result_PhotoDTO
                    {
                        Client_Result_Photo_ID = item.Field<Int64>("Client_Result_Photo_ID"),
                        Client_Result_Photo_Wo_ID = item.Field<Int64?>("Client_Result_Photo_Wo_ID"),
                        Client_Result_Photo_FileName = item.Field<String>("Client_Result_Photo_FileName"),
                        Client_Result_Photo_FilePath = item.Field<String>("Client_Result_Photo_FilePath"),
                        Client_Result_Photo_IsActive = item.Field<Boolean>("Client_Result_Photo_IsActive"),
                        Client_Result_Photo_FileType = item.Field<String>("Client_Result_Photo_FileType"),
                        Client_Result_Photo_FileSize = item.Field<String>("Client_Result_Photo_FileSize"),
                        Client_Result_Photo_TaskLable_Name = item.Field<String>("Client_Result_Photo_TaskLable_Name"),
                        Client_Result_Photo_BucketName = item.Field<String>("Client_Result_Photo_BucketName"),
                        Client_Result_Photo_ProjectID = item.Field<String>("Client_Result_Photo_ProjectID"),
                        Client_Result_Photo_objectName = item.Field<String>("Client_Result_Photo_objectName"),
                        Client_Result_Photo_localPath = item.Field<String>("Client_Result_Photo_localPath"),
                        Client_Result_Photo_folder = item.Field<String>("Client_Result_Photo_folder"),
                        Client_Result_Photo_FolderName = item.Field<String>("Client_Result_Photo_FolderName"),
                        Client_Result_Photo_StatusType = item.Field<int?>("Client_Result_Photo_StatusType"),

                    }).ToList();

                objDynamic.Add(ImageDetails);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }


        //delete photo record according task
        public List<dynamic> DeleteClientResultPhotoData(ClientResultPhotoRef model)
        {

            string insertProcedure = "[CreateUpdate_Client_Result_Photos_New_Ref]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@CRP_New_pkeyId", 1 + "#bigint#" + model.CRP_New_pkeyId);
                input_parameters.Add("@CRP_New_Photo_ID", 1 + "#bigint#" + model.CRP_New_Photo_ID);
                input_parameters.Add("@CRP_New_Bid_Id", 1 + "#bigint#" + model.CRP_New_Bid_Id);
                input_parameters.Add("@CRP_New_Inv_Id", 1 + "#bigint#" + model.CRP_New_Inv_Id);
                input_parameters.Add("@CRP_New_Damage_Id", 1 + "#bigint#" + model.CRP_New_Damage_Id);
                input_parameters.Add("@CRP_New_Status_Type", 1 + "#bigint#" + model.CRP_New_Status_Type);
                input_parameters.Add("@CRP_New_Task_Bid_pkeyID", 1 + "#bigint#" + model.CRP_New_Task_Bid_pkeyID);
                input_parameters.Add("@CRP_New_IsActive", 1 + "#bit#" + model.CRP_New_IsActive);
                input_parameters.Add("@CRP_New_Task_ID", 1 + "#bigint#" + model.CRP_New_Task_ID);
                input_parameters.Add("@CRP_WorkOrderPhotoLabel_ID", 1 + "#bigint#" + model.CRP_WorkOrderPhotoLabel_ID);
                input_parameters.Add("@CRP_Inspection_Id", 1 + "#bigint#" + model.CRP_Inspection_Id);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@CRP_New_pkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return obj.SqlCRUD(insertProcedure, input_parameters);



        }


        private List<dynamic> AddUpdateMoveCopy_ClientPhoto_Data(MoveCopy_ClientPhoto_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_MoveCopy_ClientPhoto]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Client_Result_Photo_Wo_ID", 1 + "#bigint#" + model.Client_Result_Photo_Wo_ID);
                input_parameters.Add("@Client_Result_Photo_IPLNo", 1 + "#varchar#" + model.Client_Result_Photo_IPLNo);
                input_parameters.Add("@Client_Result_Photo_ID", 1 + "#bigint#" + model.Client_Result_Photo_ID);
                input_parameters.Add("@CRP_New_pkeyId", 1 + "#bigint#" + model.CRP_New_pkeyId);
                input_parameters.Add("@Client_Result_Photo_StatusType", 1 + "#int#" + model.Client_Result_Photo_StatusType);
                input_parameters.Add("@Client_Result_Photo_CM_Type", 1 + "#int#" + model.Client_Result_Photo_CM_Type);

                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Client_Result_Photo_TaskId", 1 + "#bigint#" + model.Client_Result_Photo_TaskId);
                input_parameters.Add("@Client_Result_Photo_Task_Bid_pkeyID", 1 + "#bigint#" + model.Client_Result_Photo_Task_Bid_pkeyID);

                input_parameters.Add("@CRP_New_pkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);
                objData = obj.SqlCRUD(insertProcedure, input_parameters);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;



        }
        public List<dynamic> AddMoveCopy_ClientPhoto_Data(WorkOrderMoveCopyPhotoDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                if (!string.IsNullOrWhiteSpace(model.WorkOrderLabelMoveCopyPhoto))
                {
                    var Data = JsonConvert.DeserializeObject<List<WorkOrderLabelMoveCopyPhotoDTO>>(model.WorkOrderLabelMoveCopyPhoto);
                    foreach (var item in Data)
                    {
                        MoveCopy_ClientPhoto_DTO moveCopy_ClientPhoto_DTO = new MoveCopy_ClientPhoto_DTO();

                        moveCopy_ClientPhoto_DTO.Client_Result_Photo_CM_Type = model.Client_Result_Photo_CM_Type;
                        moveCopy_ClientPhoto_DTO.Client_Result_Photo_ID = item.Client_Result_Photo_ID;
                        moveCopy_ClientPhoto_DTO.Client_Result_Photo_IPLNo = model.Client_Result_Photo_IPLNo;
                        moveCopy_ClientPhoto_DTO.Client_Result_Photo_StatusType = model.Client_Result_Photo_StatusType;
                        moveCopy_ClientPhoto_DTO.Client_Result_Photo_Wo_ID = model.Client_Result_Photo_Wo_ID;
                        moveCopy_ClientPhoto_DTO.Client_Result_Photo_TaskId = model.Client_Result_Photo_TaskId;
                        moveCopy_ClientPhoto_DTO.Client_Result_Photo_Task_Bid_pkeyID = model.Client_Result_Photo_Task_Bid_pkeyID;
                        moveCopy_ClientPhoto_DTO.CRP_New_pkeyId = item.CRP_New_pkeyId;
                        moveCopy_ClientPhoto_DTO.UserID = model.UserID;
                        moveCopy_ClientPhoto_DTO.Type = model.Type;
                        objData = AddUpdateMoveCopy_ClientPhoto_Data(moveCopy_ClientPhoto_DTO);
                    }

                    //Add New Access Log 12-12-2022
                    Access_log_MasterData access_Log_MasterData = new Access_log_MasterData();
                    NewAccessLogMaster newAccessLogMaster = new NewAccessLogMaster();
                    newAccessLogMaster.Access_WorkerOrderID = model.Client_Result_Photo_Wo_ID;
                    newAccessLogMaster.Access_Master_ID = 15;
                    newAccessLogMaster.Access_UserID = model.UserID;
                    newAccessLogMaster.Type = 1;
                    access_Log_MasterData.AddNewAccessLogMaster(newAccessLogMaster);
                }



            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        public List<dynamic> GetClientResult_PhotoHistoryData(Client_Result_PhotoDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetClientResult_PhotoHistory(model);

                if (ds.Tables.Count > 1)
                {
                    var myEnumerableFeaprd = ds.Tables[1].AsEnumerable();
                    List<Client_Result_Photo_HistoryDTO> piData =
                       (from item in myEnumerableFeaprd
                        select new Client_Result_Photo_HistoryDTO
                        {

                            Client_Result_Photo_Wo_ID = item.Field<Int64>("workOrder_ID"),
                            Client_Result_Photo_IPLNo = item.Field<String>("IPLNO"),
                            IsTabHidden = true
                        }).ToList();

                    objDynamic.Add(piData);
                }

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        private DataSet GetClientResult_PhotoHistory(Client_Result_PhotoDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Photo_History]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Client_Result_Photo_Wo_ID", 1 + "#bigint#" + model.Client_Result_Photo_Wo_ID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserID);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> UpdateClientResultPhoto_Data(Client_Result_PhotoDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            Client_Result_PhotoDTO client_Result_PhotoDTO = new Client_Result_PhotoDTO();
            try
            {

                if (model.Dataitems != null && model.Dataitems.Count > 0)
                {
                    for (int i = 0; i < model.Dataitems.Count; i++)
                    {

                        client_Result_PhotoDTO.Client_Result_Photo_ID = model.Dataitems[i].Client_Result_Photo_ID;
                        // client_Result_PhotoDTO.Client_Result_Photo_DateTimeOriginal = model.Client_Result_Photo_DateTimeOriginal;
                        client_Result_PhotoDTO.Client_Result_Photo_UploadTimestamp = model.Client_Result_Photo_UploadTimestamp;
                        client_Result_PhotoDTO.Type = model.Type;

                        var objauto = AddClientResultPhotoData(client_Result_PhotoDTO);
                        objData.Add(objauto);
                    }
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }


        //Client result Photo history details data
        private DataSet GetClientResultPhotoMasterHistory(GetClientResultPhoto_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_ClientPhotoMaster_History]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Client_Result_Photo_ID", 1 + "#bigint#" + model.Client_Result_Photo_ID);
                input_parameters.Add("@Client_Result_Photo_Wo_ID", 1 + "#bigint#" + model.Client_Result_Photo_Wo_ID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetClientResultPhotosHistoryDetails(GetClientResultPhoto_DTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetClientResultPhotoMasterHistory(model);


                List<GetClientResultPhoto_DTO> lstClientResultPhotoDTObid;
                List<TaskPhotoButtonDetails> lsttaskPhotoButtonDetails;
                GetClientResultPhoto_DTO getClientResultPhoto_DTO;
                TaskPhotoDetails taskPhotoDetails;
                TaskPhotoButtonDetails taskPhotoButtonDetails;
                int taskcount = ds.Tables[1].Rows.Count;
                if (taskcount != 0)
                {
                    List<dynamic> FLagobjDynamic = new List<dynamic>();
                    DataRow[] drtaskrow = ds.Tables[1].Select("Orderby = 'A' ");

                    if (drtaskrow.Length != 0)
                    {
                        foreach (var item in drtaskrow)
                        {
                            taskPhotoDetails = new TaskPhotoDetails();
                            taskPhotoButtonDetails = new TaskPhotoButtonDetails();
                            taskPhotoButtonDetails.Task_Photo_Button_Name = "Bid";
                            taskPhotoButtonDetails.Task_Photo_Button_Id = 4;

                            taskPhotoDetails.Task_Bid_pkeyID = Convert.ToInt64(item["Task_Bid_pkeyID"].ToString());
                            taskPhotoDetails.Task_Photo_Label_Name = item["Task_Photo_Label_Name"].ToString();
                            getClientResultPhoto_DTO = new GetClientResultPhoto_DTO();
                            lstClientResultPhotoDTObid = new List<GetClientResultPhoto_DTO>();
                            lsttaskPhotoButtonDetails = new List<TaskPhotoButtonDetails>();
                            DataRow[] drBidrow = ds.Tables[0].Select("Client_Result_Photo_Bid_Id = " + taskPhotoDetails.Task_Bid_pkeyID + " ");
                            if (drBidrow.Length != 0)
                            {


                                foreach (var itemrow in drBidrow)
                                {
                                    getClientResultPhoto_DTO = new GetClientResultPhoto_DTO();
                                    getClientResultPhoto_DTO = GetClientResultData(itemrow);

                                    lstClientResultPhotoDTObid.Add(getClientResultPhoto_DTO);

                                }
                                taskPhotoButtonDetails.Items = lstClientResultPhotoDTObid;
                                lsttaskPhotoButtonDetails.Add(taskPhotoButtonDetails);
                                taskPhotoDetails.Items = lsttaskPhotoButtonDetails;

                                FLagobjDynamic.Add(taskPhotoDetails);
                            }
                        }
                    }


                    DataRow[] drtaskDamagerow = ds.Tables[1].Select("Orderby = 'C' ");

                    if (drtaskDamagerow.Length != 0)
                    {
                        foreach (var item in drtaskDamagerow)
                        {
                            taskPhotoDetails = new TaskPhotoDetails();
                            taskPhotoButtonDetails = new TaskPhotoButtonDetails();
                            taskPhotoButtonDetails.Task_Photo_Button_Name = "Damage";
                            taskPhotoButtonDetails.Task_Photo_Button_Id = 5;

                            taskPhotoDetails.Task_Bid_pkeyID = Convert.ToInt64(item["Task_Bid_pkeyID"].ToString());
                            taskPhotoDetails.Task_Photo_Label_Name = item["Task_Photo_Label_Name"].ToString();
                            getClientResultPhoto_DTO = new GetClientResultPhoto_DTO();
                            lstClientResultPhotoDTObid = new List<GetClientResultPhoto_DTO>();
                            lsttaskPhotoButtonDetails = new List<TaskPhotoButtonDetails>();
                            DataRow[] drBidrow = ds.Tables[0].Select("Client_Result_Photo_Damage_ID = " + taskPhotoDetails.Task_Bid_pkeyID + " ");
                            if (drBidrow.Length != 0)
                            {


                                foreach (var itemrow in drBidrow)
                                {
                                    getClientResultPhoto_DTO = new GetClientResultPhoto_DTO();
                                    getClientResultPhoto_DTO = GetClientResultData(itemrow);

                                    lstClientResultPhotoDTObid.Add(getClientResultPhoto_DTO);

                                }
                                taskPhotoButtonDetails.Items = lstClientResultPhotoDTObid;
                                lsttaskPhotoButtonDetails.Add(taskPhotoButtonDetails);
                                taskPhotoDetails.Items = lsttaskPhotoButtonDetails;

                                FLagobjDynamic.Add(taskPhotoDetails);
                            }
                        }
                    }

                    // For Label

                    DataRow[] drtaskCustomrow = ds.Tables[1].Select("Orderby = 'E' ");

                    if (drtaskCustomrow.Length != 0)
                    {
                        foreach (var item in drtaskCustomrow)
                        {
                            taskPhotoDetails = new TaskPhotoDetails();
                            taskPhotoButtonDetails = new TaskPhotoButtonDetails();
                            taskPhotoButtonDetails.Task_Photo_Button_Name = "Label";
                            taskPhotoButtonDetails.Task_Photo_Button_Id = 6;

                            taskPhotoDetails.Task_Bid_pkeyID = Convert.ToInt64(item["Task_Bid_pkeyID"].ToString());
                            taskPhotoDetails.Task_Photo_Label_Name = item["Task_Photo_Label_Name"].ToString();
                            getClientResultPhoto_DTO = new GetClientResultPhoto_DTO();
                            lstClientResultPhotoDTObid = new List<GetClientResultPhoto_DTO>();
                            lsttaskPhotoButtonDetails = new List<TaskPhotoButtonDetails>();
                            DataRow[] drBidrow = ds.Tables[0].Select("Client_Result_Photo_WorkOrderPhotoLabel_ID = " + taskPhotoDetails.Task_Bid_pkeyID + " ");
                            if (drBidrow.Length != 0)
                            {

                                foreach (var itemrow in drBidrow)
                                {
                                    getClientResultPhoto_DTO = new GetClientResultPhoto_DTO();
                                    getClientResultPhoto_DTO = GetClientResultData(itemrow);

                                    lstClientResultPhotoDTObid.Add(getClientResultPhoto_DTO);

                                }
                                taskPhotoButtonDetails.Items = lstClientResultPhotoDTObid;
                                lsttaskPhotoButtonDetails.Add(taskPhotoButtonDetails);
                                taskPhotoDetails.Items = lsttaskPhotoButtonDetails;

                                FLagobjDynamic.Add(taskPhotoDetails);
                            }
                        }
                    }



                    objDynamic.Add(FLagobjDynamic);

                }
                else
                {
                    objDynamic.Add(null);
                }

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        public List<dynamic> GetClientResultPhotosHistoryMobileApp(Client_Result_PhotoDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetClientResultPhotosHistoryMobile(model);


                List<GetClientResultPhoto_DTO> lstClientResultPhotoDTObid;
                List<TaskPhotoButtonDetails> lsttaskPhotoButtonDetails;
                GetClientResultPhoto_DTO getClientResultPhoto_DTO;
                TaskPhotoDetails taskPhotoDetails;
                TaskPhotoButtonDetails taskPhotoButtonDetails;

                Client_Result_Photo_HistoryMobileDTO dataList = new Client_Result_Photo_HistoryMobileDTO();
                int tablecount = ds.Tables.Count;
                if (tablecount > 2)
                {
                    int taskcount = ds.Tables[2].Rows.Count;
                    if (taskcount != 0)
                    {
                        DataRow[] drtaskrow = ds.Tables[2].Select("Orderby = 'A' ");

                        if (drtaskrow.Length != 0)
                        {
                            foreach (var item in drtaskrow)
                            {
                                taskPhotoDetails = new TaskPhotoDetails();
                                taskPhotoButtonDetails = new TaskPhotoButtonDetails();
                                taskPhotoButtonDetails.Task_Photo_Button_Name = "Bid";
                                taskPhotoButtonDetails.Task_Photo_Button_Id = 4;

                                taskPhotoDetails.Task_Bid_pkeyID = Convert.ToInt64(item["Task_Bid_pkeyID"].ToString());
                                taskPhotoDetails.Task_Photo_Label_Name = item["Task_Photo_Label_Name"].ToString();
                                getClientResultPhoto_DTO = new GetClientResultPhoto_DTO();
                                lstClientResultPhotoDTObid = new List<GetClientResultPhoto_DTO>();
                                lsttaskPhotoButtonDetails = new List<TaskPhotoButtonDetails>();
                                DataRow[] drBidrow = ds.Tables[1].Select("Client_Result_Photo_Bid_Id = " + taskPhotoDetails.Task_Bid_pkeyID + " ");
                                if (drBidrow.Length != 0)
                                {


                                    foreach (var itemrow in drBidrow)
                                    {
                                        getClientResultPhoto_DTO = new GetClientResultPhoto_DTO();
                                        getClientResultPhoto_DTO = GetClientResultData(itemrow);

                                        lstClientResultPhotoDTObid.Add(getClientResultPhoto_DTO);

                                    }
                                    taskPhotoButtonDetails.Items = lstClientResultPhotoDTObid;
                                    lsttaskPhotoButtonDetails.Add(taskPhotoButtonDetails);
                                    taskPhotoDetails.Items = lsttaskPhotoButtonDetails;

                                    if (dataList.Bid == null)
                                    {
                                        dataList.Bid = new List<TaskPhotoDetails>();
                                    }
                                    dataList.Bid.Add(taskPhotoDetails);
                                }
                            }
                        }


                        DataRow[] drtaskDamagerow = ds.Tables[2].Select("Orderby = 'C' ");

                        if (drtaskDamagerow.Length != 0)
                        {
                            foreach (var item in drtaskDamagerow)
                            {
                                taskPhotoDetails = new TaskPhotoDetails();
                                taskPhotoButtonDetails = new TaskPhotoButtonDetails();
                                taskPhotoButtonDetails.Task_Photo_Button_Name = "Damage";
                                taskPhotoButtonDetails.Task_Photo_Button_Id = 5;

                                taskPhotoDetails.Task_Bid_pkeyID = Convert.ToInt64(item["Task_Bid_pkeyID"].ToString());
                                taskPhotoDetails.Task_Photo_Label_Name = item["Task_Photo_Label_Name"].ToString();
                                getClientResultPhoto_DTO = new GetClientResultPhoto_DTO();
                                lstClientResultPhotoDTObid = new List<GetClientResultPhoto_DTO>();
                                lsttaskPhotoButtonDetails = new List<TaskPhotoButtonDetails>();
                                DataRow[] drBidrow = ds.Tables[1].Select("Client_Result_Photo_Damage_ID = " + taskPhotoDetails.Task_Bid_pkeyID + " ");
                                if (drBidrow.Length != 0)
                                {


                                    foreach (var itemrow in drBidrow)
                                    {
                                        getClientResultPhoto_DTO = new GetClientResultPhoto_DTO();
                                        getClientResultPhoto_DTO = GetClientResultData(itemrow);

                                        lstClientResultPhotoDTObid.Add(getClientResultPhoto_DTO);

                                    }
                                    taskPhotoButtonDetails.Items = lstClientResultPhotoDTObid;
                                    lsttaskPhotoButtonDetails.Add(taskPhotoButtonDetails);
                                    taskPhotoDetails.Items = lsttaskPhotoButtonDetails;
                                    if (dataList.Damage == null)
                                    {
                                        dataList.Damage = new List<TaskPhotoDetails>();
                                    }
                                    dataList.Damage.Add(taskPhotoDetails);
                                }
                            }
                        }


                        objDynamic.Add(dataList);

                    }
                    else
                    {
                        objDynamic.Add(null);
                    }
                }

                else
                {
                    objDynamic.Add(null);
                }

                if (ds.Tables.Count > 3)
                {
                    objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[3]));
                }


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        //Client result Photo history details data for Mobile App
        private DataSet GetClientResultPhotosHistoryMobile(Client_Result_PhotoDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_ClientPhotoMaster_History_MobileApp]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Client_Result_Photo_Wo_ID", 1 + "#bigint#" + model.Client_Result_Photo_Wo_ID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserID);


                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        private DataSet GetClientResultPhotoInfo(GetClientResultPhoto_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_ClientPhotoInfo]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Client_Result_Photo_ID", 1 + "#bigint#" + model.Client_Result_Photo_ID);
                input_parameters.Add("@Client_Result_Photo_Wo_ID", 1 + "#bigint#" + model.Client_Result_Photo_Wo_ID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetClientResultPhotoInfoData(GetClientResultPhoto_DTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetClientResultPhotoInfo(model);

                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            objDynamic.Add(ds.Tables[0].Rows[0][1]);
                        }


                    }
                    if (ds.Tables.Count > 1)
                    {
                        if (ds.Tables[1].Rows.Count > 0)
                        {
                            objDynamic.Add(ds.Tables[1].Rows[0][1]);
                        }


                    }
                    if (ds.Tables.Count > 2 && ds.Tables.Count > 3)
                    {

                        var myEnumerableFeaprd = ds.Tables[2].AsEnumerable();
                        List<Client_Result_Photo_Info> ClientResultPhoto =
                           (from item in myEnumerableFeaprd
                            select new Client_Result_Photo_Info
                            {
                                Client_Result_Photo_ID = item.Field<Int64>("Client_Result_Photo_ID"),
                                Client_Result_Photo_UploadTimestamp = item.Field<DateTime?>("Client_Result_Photo_UploadTimestamp"),
                                Client_Result_Photo_DateTimeOriginal = item.Field<DateTime?>("Client_Result_Photo_DateTimeOriginal"),
                                Max_Distance = item.Field<decimal?>("Max_Distance"),

                            }).ToList();

                        var myEnumerableFeapd = ds.Tables[3].AsEnumerable();
                        List<Client_Result_Photo_Info> ClientResultPhotoDate =
                           (from item in myEnumerableFeapd
                            select new Client_Result_Photo_Info
                            {
                                Client_Result_Photo_DateTimeOriginal = item.Field<DateTime?>("Client_Result_Photo_DateTimeOriginal"),

                            }).ToList();

                        if (ClientResultPhoto != null && ClientResultPhoto.Count > 0 && ClientResultPhotoDate != null && ClientResultPhotoDate.Count > 0)
                        {
                            foreach (var item in ClientResultPhotoDate)
                            {
                                var dateList = ClientResultPhoto.Where(c => c.Client_Result_Photo_DateTimeOriginal.Value.Date == item.Client_Result_Photo_DateTimeOriginal.Value.Date).ToList();
                                if (dateList == null || dateList.Count == 0 || dateList.Count == 1)
                                {
                                    item.Working_Hour = "00 : 00";
                                }
                                else
                                {
                                    TimeSpan diff = dateList.Last().Client_Result_Photo_DateTimeOriginal.Value - dateList.First().Client_Result_Photo_DateTimeOriginal.Value;
                                    item.Working_Hour = diff.Hours < 10 ? "0" + diff.Hours.ToString() + " : " : diff.Hours.ToString() + " : ";
                                    item.Working_Hour = diff.Minutes < 10 ? item.Working_Hour + "0" + diff.Minutes.ToString() : item.Working_Hour + diff.Minutes.ToString();
                                }
                                item.Max_Distance = dateList.Sum(x => x.Max_Distance);
                            }
                        }

                        objDynamic.Add(ClientResultPhotoDate);

                    }
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        public List<dynamic> DownLoadTextImage()
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                //define a string of text to use as the Copyright message
                string imagePath = "https://storage.googleapis.com/rare-lambda-245821.appspot.com/201540/201539_1.jpg?GoogleAccessId=firebase-adminsdk-rwdun%40rare-lambda-245821.iam.gserviceaccount.com&Expires=2554416000&Signature=aC6nRwd0KJmG3j2QYoNj7%2BDA1eI0BlIiNVVhPm670GBIi2FU%2BNYLIS42ZocwuX3C3CebSR6l4D7Cp5toLI6botU9DdwCIzRdZXHme5jLyp8e2EaQBc2f6pAalzKsvo5NDOVM153G2LZrEZlCM3zJmpdxtWa6T4LjqL7W2UR13foo7lmgcQmnEv9dI55OJ5923NT3%2BBxDrybAE%2BfdtG2%2FDOt8knP79FEfy%2BXSs8WOYaA4b5KlB8W1ls7FjP14tB8inDuGTF9l6PAhVkBQCqDiSAV%2B1ZPd3Dnh7SedEWj%2BSOCJU2iVI3yKJmZuCMyuxte0%2FHQssYV2ORHOS0iN9p6z6A%3D%3D";

                //create a image object containing the photograph to watermark
                System.Net.HttpWebRequest webRequest = (System.Net.HttpWebRequest)System.Net.HttpWebRequest.Create(imagePath);
                webRequest.AllowWriteStreamBuffering = true;
                webRequest.Timeout = 30000;

                System.Net.WebResponse webResponse = webRequest.GetResponse();

                System.IO.Stream stream = webResponse.GetResponseStream();

                Image imgPhoto = System.Drawing.Image.FromStream(stream);

                webResponse.Close();

                int phWidth = imgPhoto.Width;
                int phHeight = imgPhoto.Height;

                Graphics graphicsImage = Graphics.FromImage(imgPhoto);

                StringFormat stringformat = new StringFormat();
                stringformat.Alignment = StringAlignment.Far;
                stringformat.LineAlignment = StringAlignment.Far;

                StringFormat stringformat2 = new StringFormat();
                stringformat2.Alignment = StringAlignment.Center;
                stringformat2.LineAlignment = StringAlignment.Center;

                Color StringColor = System.Drawing.ColorTranslator.FromHtml("#933eea");//direct color adding     
                string Str_TextOnImage = DateTime.Now.ToShortDateString();//Your Text On Image   

                graphicsImage.DrawString(Str_TextOnImage, new Font("arial", 25,
                FontStyle.Regular), new SolidBrush(StringColor), new Point(200, 170),
                stringformat);

                imgPhoto.Save(@"D:\Projects\IPL\ipl_api\IPL\IPL\Document\ClientPhotos\IplTextImage.jpg", ImageFormat.Jpeg);
                imgPhoto.Dispose();


            }
            catch (Exception ex)
            {

                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        //client results photo temp data
        private List<dynamic> AddClientResultPhotoTempData(Client_Result_Photo_Temp model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_Client_Results_Photo_Temp]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Client_Res_Photo_PkeyID", 1 + "#bigint#" + model.Client_Res_Photo_PkeyID);
                input_parameters.Add("@Client_Res_Photo_ID", 1 + "#bigint#" + model.Client_Res_Photo_ID);
                input_parameters.Add("@IPLNO", 1 + "#varchar#" + model.IPLNO);
                input_parameters.Add("@Client_Result_Photo_Wo_ID", 1 + "#bigint#" + model.Client_Result_Photo_Wo_ID);
                input_parameters.Add("@Client_Result_Photo_FileName", 1 + "#varchar#" + model.Client_Result_Photo_FileName);
                input_parameters.Add("@Client_Result_Photo_FilePath", 1 + "#varchar#" + model.Client_Result_Photo_FilePath);
                input_parameters.Add("@Client_Result_Photo_Type", 1 + "#int#" + model.Client_Result_Photo_Type);
                input_parameters.Add("@Client_Result_Photo_Ch_ID", 1 + "#bigint#" + model.Client_Result_Photo_Ch_ID);
                input_parameters.Add("@ReqType", 1 + "#int#" + model.ReqType);
                input_parameters.Add("@ContentType", 1 + "#int#" + model.ContentType);
                input_parameters.Add("@Client_Result_Photo_GPSLongitude", 1 + "#varchar#" + model.Client_Result_Photo_GPSLongitude);
                input_parameters.Add("@Client_Result_Photo_GPSLatitude", 1 + "#varchar#" + model.Client_Result_Photo_GPSLatitude);
                input_parameters.Add("@Client_Result_Photo_DateTimeOriginal", 1 + "#datetime#" + model.Client_Result_Photo_DateTimeOriginal_Dt);
                input_parameters.Add("@Client_Result_Photo_StatusType", 1 + "#int#" + model.Client_Result_Photo_StatusType);
                input_parameters.Add("@Client_Result_Photo_Task_Bid_pkeyID", 1 + "#bigint#" + model.Client_Result_Photo_Task_Bid_pkeyID);
                input_parameters.Add("@Client_Result_Photo_GPSAltitude", 1 + "#varchar#" + model.Client_Result_Photo_GPSAltitude);
                input_parameters.Add("@Client_Result_Photo_Model", 1 + "#varchar#" + model.Client_Result_Photo_Model);
                input_parameters.Add("@Client_Result_Photo_Make", 1 + "#varchar#" + model.Client_Result_Photo_Make);
                input_parameters.Add("@Client_Result_Photo_MeteringMode", 1 + "#int#" + model.Client_Result_Photo_MeteringMode);
                input_parameters.Add("@Client_Result_Photo_Seq", 1 + "#int#" + model.Client_Result_Photo_Seq);
                input_parameters.Add("@Orginal_Type", 1 + "#int#" + model.Orginal_Type);
                input_parameters.Add("@Client_Result_Photo_IsActive", 1 + "#bit#" + model.Client_Result_Photo_IsActive);
                input_parameters.Add("@Client_Result_Photo_IsDelete", 1 + "#bit#" + model.Client_Result_Photo_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Client_Res_Photo_PkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);
                objData = obj.SqlCRUD(insertProcedure, input_parameters);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

            }
            return objData;



        }

        public List<dynamic> AddUpdatePhotoTempData(Client_Result_Photo_Temp model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {

                DateTime.TryParse(model.Client_Result_Photo_DateTimeOriginal, out DateTime dt);
                if (dt.ToString() == "01-01-0001 00:00:00")
                {
                    dt = System.DateTime.Now;
                }

                model.Client_Result_Photo_MeteringMode = model.MeteringMode;
                model.Orginal_Type = model.Type;
                model.Type = 1;
                model.Client_Result_Photo_DateTimeOriginal_Dt = dt;
                objData = AddClientResultPhotoTempData(model);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }
        public List<dynamic> GetPhotosListStatusTypeWise(Client_Result_Photo_Data model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<StatusWIsePhoto_DTO> photosList = new List<StatusWIsePhoto_DTO>();
            ClientSettingDTO client_Photo_Settings_DTO = new ClientSettingDTO();
            try
            {
                string selectProcedure = "[GetPhotosListStatusTypeWise]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();
                input_parameters.Add("@WorkOrder_ID", 1 + "#bigint#" + model.Client_Result_Photo_Wo_ID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                DataSet ds = obj.SelectSql(selectProcedure, input_parameters);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                photosList =
                   (from item in myEnumerableFeaprd
                    select new StatusWIsePhoto_DTO
                    {
                        Client_Result_Photo_ID = item.Field<Int64>("Client_Result_Photo_ID"),
                        Client_Result_Photo_FileName = item.Field<string>("Client_Result_Photo_FileName"),
                        Client_Result_Photo_FilePath = item.Field<string>("Client_Result_Photo_FilePath"),
                        Client_Result_Photo_StatusType = item.Field<int>("Client_Result_Photo_StatusType"),
                        LableName = item.Field<string>("LableName"),
                        TaskName = item.Field<string>("TaskName"),

                    }).ToList();

                if (ds.Tables[1].Rows.Count > 0)
                {
                    var client_photo_setting = ds.Tables[1].AsEnumerable();
                    client_Photo_Settings_DTO =

                        (from item in client_photo_setting
                         select new ClientSettingDTO
                         {
                             Client_pkeyID = item.Field<Int64>("Client_pkeyID"),
                             Client_Photo_Resize_height = item.Field<Int64>("Client_Photo_Resize_height"),
                             Client_Photo_Resize_width = item.Field<Int64>("Client_Photo_Resize_width"),
                             Client_DateTimeOverlay = item.Field<DateOverlapingEnum>("Client_DateTimeOverlay"),

                         }).FirstOrDefault();
                }
                objData.Add(photosList);
                objData.Add(client_Photo_Settings_DTO);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return objData;
        }
        public List<dynamic> DeletedClientResultPhotos(ClientResultPhotoDTO model)
        {

            string insertProcedure = "[DeletedClientPhoto]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Client_Result_Photo_ID_Comm", 1 + "#varchar#" + model.Client_Result_Photo_ID_Comma);
                input_parameters.Add("@Client_Result_Photo_Wo_ID", 1 + "#bigint#" + model.Client_Result_Photo_Wo_ID);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

            }
            return obj.SqlCRUD(insertProcedure, input_parameters);



        }

    }
}