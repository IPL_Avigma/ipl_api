﻿using Avigma.Repository.Lib;
using IPLApp.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class Property_Type_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        private List<dynamic> CreateUpdatePropertyTypeMaster(Property_Type_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();
            Property_Type_Master property_Type_Master = new Property_Type_Master();
            string insertProcedure = "[CreateUpdate_Property_Type_Master]";
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@PT_PkeyID", 1 + "#bigint#" + model.PT_PkeyID);
                input_parameters.Add("@PT_Name", 1 + "#varchar#" + model.PT_Name);
                input_parameters.Add("@PT_IsActive", 1 + "#bit#" + model.PT_IsActive);
                input_parameters.Add("@PT_IsDelete", 1 + "#bit#" + model.PT_IsDelete);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@PT_Pkey_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters); ;
                if (objData[1] == 0)
                {
                    property_Type_Master.PT_PkeyID = "0";
                    property_Type_Master.Status = "0";
                    property_Type_Master.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    property_Type_Master.PT_PkeyID = Convert.ToString(objData[0]);
                    property_Type_Master.Status = Convert.ToString(objData[1]);


                }
                objcltData.Add(property_Type_Master);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;



        }


        private DataSet GetPropertyTypeMaster(Property_Type_MasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Property_Type_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@PT_PkeyID", 1 + "#bigint#" + model.PT_PkeyID);
                input_parameters.Add("@WhereClause", 1 + "#varchar#" + model.WhereClause);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> CreateUpdatePropertyTypeMasterDetails(Property_Type_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                objData = CreateUpdatePropertyTypeMaster(model);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }
            return objData;
        }

        public List<dynamic> GetPropertyTypeMasterDetails(Property_Type_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                string wherecondition = string.Empty;
                if (model.Type == 3 && !string.IsNullOrEmpty(model.FilterData))
                {
                    var Data = JsonConvert.DeserializeObject<Property_Type_MasterDTO>(model.FilterData);
                    if (!string.IsNullOrEmpty(Data.PT_Name))
                    {
                        wherecondition = " And pt.PT_Name LIKE '%" + Data.PT_Name + "%'";
                    }
                    if (Data.PT_IsActive == true)
                    {
                        wherecondition = wherecondition + "  And pt.PT_IsActive =  1 ";
                    }
                    if (Data.PT_IsActive == false)
                    {
                        wherecondition = wherecondition + "  And pt.PT_IsActive =  0";
                    }
                    model.WhereClause = wherecondition;
                }

                DataSet ds = GetPropertyTypeMaster(model);

                if (ds.Tables.Count > 0)
                {
                    for (int i = 0; i < ds.Tables.Count; i++)
                    {
                        objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                    }

                }
                //objDynamic.Add(Get_details);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }

            return objDynamic;
        }


    }
}