﻿using Avigma.Repository.Lib;
using IPLApp.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPLApp.Repository.data
{
    public class ClientMasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public List<dynamic> AddClientMasyerData(ClientMasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objclntData = new List<dynamic>();
            string insertProcedure = "[CreateUpdateClient_Master]";
            ClientMaster clientMaster = new ClientMaster();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                //input_parameters.Add("@client_pkyeId", 1 + "#bigint#" + model.client_pkyeId);
                //input_parameters.Add("@client_Name", 1 + "#nvarchar#" + model.client_Name);
                //input_parameters.Add("@client_IsActive", 1 + "#bit#" + model.client_IsActive);
                //input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@client_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    clientMaster.client_pkyeId = "0";
                    clientMaster.Status = "0";
                    clientMaster.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    clientMaster.client_pkyeId = Convert.ToString(objData[0]);
                    clientMaster.Status = Convert.ToString(objData[1]);


                }
                objclntData.Add(clientMaster);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objclntData;



        }

        private DataSet GetClientMaster(ClientMasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Client_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@client_pkeyID", 1 + "#bigint#" + model.client_pkyeId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetClientMasterDetails(ClientMasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetClientMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<ClientMasterDTO> ClientMasterDetails =
                   (from item in myEnumerableFeaprd
                    select new ClientMasterDTO
                    {
                        Clnt_Con_List_ClientComID = item.Field<Int64>("Clnt_Con_List_ClientComID"),
                        Clnt_Con_List_Name = item.Field<String>("Clnt_Con_List_Name"),
                        Clnt_Con_List_Email = item.Field<String>("Clnt_Con_List_Email"),
                        Clnt_Con_List_Phone = item.Field<String>("Clnt_Con_List_Phone"),
                        Clnt_Con_List_TypeName = item.Field<String>("Clnt_Con_List_TypeName"),

                    }).ToList();

                objDynamic.Add(ClientMasterDetails);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}