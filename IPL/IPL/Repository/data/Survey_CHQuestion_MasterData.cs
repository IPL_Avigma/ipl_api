﻿using Avigma.Repository.Lib;
using IPLApp.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPLApp.Repository.data
{
    public class Survey_CHQuestion_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public List<dynamic> AddIPL_ChildMeta_Data(Survey_CHQuestion_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objchqueData = new List<dynamic>();
            string insertProcedure = "[CreateUpdateSurvey_CHQuestion_Master]";
            Survey_CHQuestion_Master survey_CHQuestion_Master = new Survey_CHQuestion_Master();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Sur_que_pkeyId", 1 + "#bigint#" + model.Sur_que_pkeyId);
                input_parameters.Add("@Sur_que_name", 1 + "#nvarchar#" + model.Sur_que_name);
                input_parameters.Add("@Sur_que_id", 1 + "#nvarchar#" + model.Sur_que_id);
                input_parameters.Add("@Sur_que_uuid", 1 + "#nvarchar#" + model.Sur_que_uuid);
                input_parameters.Add("@Sur_que_answerType", 1 + "#nvarchar#" + model.Sur_que_answerType);
                input_parameters.Add("@Sur_que_text", 1 + "#nvarchar#" + model.Sur_que_text);
                input_parameters.Add("@Sur_que_question", 1 + "#nvarchar#" + model.Sur_que_question);
                input_parameters.Add("@Sur_que_required", 1 + "#bit#" + model.Sur_que_required);
                input_parameters.Add("@Sur_que_answerRequiresPics", 1 + "#nvarchar#" + model.Sur_que_answerRequiresPics);
                input_parameters.Add("@Sur_que_choices", 1 + "#nvarchar#" + model.Sur_que_choices);
                input_parameters.Add("@Sur_que_options", 1 + "#nvarchar#" + model.Sur_que_options);
                input_parameters.Add("@Sur_que_enabledByDefault", 1 + "#bit#" + model.Sur_que_enabledByDefault);
                input_parameters.Add("@Sur_que_commentEnabled", 1 + "#bit#" + model.Sur_que_commentEnabled);
                input_parameters.Add("@Sur_que_modeOptions", 1 + "#nvarchar#" + model.Sur_que_modeOptions);
                input_parameters.Add("@Sur_que_mode", 1 + "#nvarchar#" + model.Sur_que_mode);
                input_parameters.Add("@Sur_que_answerEnables", 1 + "#nvarchar#" + model.Sur_que_answerEnables);
                input_parameters.Add("@Sur_que_IsActive", 1 + "#bit#" + model.Sur_que_IsActive);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Sur_que_pkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    survey_CHQuestion_Master.Sur_que_pkeyId = 0;
                    survey_CHQuestion_Master.Status = "0";
                    survey_CHQuestion_Master.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    survey_CHQuestion_Master.Sur_que_pkeyId = Convert.ToInt64(objData[0]);
                    survey_CHQuestion_Master.Status = Convert.ToString(objData[1]);


                }
                objchqueData.Add(survey_CHQuestion_Master);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objchqueData;



        }

        private DataSet GetSurvey_CHQuestion(Survey_CHQuestion_MasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[GetSurvey_Question_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Sur_que_pkeyId", 1 + "#bigint#" + model.Sur_que_pkeyId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetSurvey_CHQuestionDetails(Survey_CHQuestion_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetSurvey_CHQuestion(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Survey_CHQuestion_MasterDTO> Survey_CHQuestion =
                   (from item in myEnumerableFeaprd
                    select new Survey_CHQuestion_MasterDTO
                    {
                        Sur_que_pkeyId = item.Field<Int64>("Sur_que_pkeyId"),
                        Sur_que_name = item.Field<String>("Sur_que_name"),
                        Sur_que_id = item.Field<String>("Sur_que_id"),
                        Sur_que_uuid = item.Field<String>("Sur_que_uuid"),
                        Sur_que_answerType = item.Field<String>("Sur_que_answerType"),
                        Sur_que_text = item.Field<String>("Sur_que_text"),
                        Sur_que_question = item.Field<String>("Sur_que_question"),
                        Sur_que_required = item.Field<Boolean?>("Sur_que_required"),
                        Sur_que_answerRequiresPics = item.Field<String>("Sur_que_answerRequiresPics"),
                        Sur_que_choices = item.Field<String>("Sur_que_choices"),
                        Sur_que_options = item.Field<String>("Sur_que_options"),
                        Sur_que_enabledByDefault = item.Field<Boolean?>("Sur_que_enabledByDefault"),
                        Sur_que_commentEnabled = item.Field<Boolean?>("Sur_que_commentEnabled"),
                        Sur_que_modeOptions = item.Field<String>("Sur_que_modeOptions"),
                        Sur_que_mode = item.Field<String>("Sur_que_mode"),
                        Sur_que_answerEnables = item.Field<String>("Sur_que_answerEnables"),

                    }).ToList();

                objDynamic.Add(Survey_CHQuestion);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}