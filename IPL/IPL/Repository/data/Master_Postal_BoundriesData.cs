﻿using Avigma.Repository.Lib;
using IPL.Models;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;

namespace IPL.Repository.data
{
    public class Master_Postal_BoundriesData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        private DataSet GetPostalBoundriesMaster(List<string> postalCodes)
        {
            if (!postalCodes.Any())
                return new DataSet();

            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_ZipCode_Geometry]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();
                input_parameters.Add("@ZipCodes", 1 + "#nvarchar#" + String.Join(",", postalCodes));
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ds;
        }

        public List<Feature> GetPostalBoundriesDetails(List<string> postalCodes)
        {
            if (!postalCodes.Any())
                return new List<Feature>();

            postalCodes = postalCodes.Distinct().ToList();
            var response = new RapidapiPostalBoundryResponse();
            try
            {
                List<Get_ZipCode_GeometryDTO> postalBoundaries = new List<Get_ZipCode_GeometryDTO>();
                DataSet ds = GetPostalBoundriesMaster(postalCodes);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Get_ZipCode_GeometryDTO> postalCodeGeometry =
                   (from item in myEnumerableFeaprd
                    select new Get_ZipCode_GeometryDTO
                    {
                        PostalBoundry_pkeyId = item.Field<Int64>("PostalBoundry_pkeyId"),
                        ZipCode = item.Field<String>("ZipCode"),
                        GeometryData = item.Field<String>("GeometryData"),

                    }).ToList();

                if (postalCodeGeometry == null || !postalCodeGeometry.Any())
                {
                    response = GetRapidApiPostalCodeBoundaries(postalCodes);
                }
                else if (postalCodeGeometry.Count() != postalCodes.Count())
                {
                    var missingPostalCode = postalCodes.Where(x => !postalCodeGeometry.Select(i => i.ZipCode).Contains(x)).ToList();
                    var missingResponse = GetRapidApiPostalCodeBoundaries(missingPostalCode);
                    if (missingResponse != null && missingResponse.features != null && missingResponse.features.Any())
                    {
                        GetPostalBoundriesDetails(postalCodes); //do the recurrsive call to get all the response from the database.
                    }
                }

                if (postalCodeGeometry != null && postalCodeGeometry.Any())
                {
                    var results = String.Join(",", postalCodeGeometry.Select(x => x.GeometryData));
                    results = "[" + results + "]";
                    var features = JsonConvert.DeserializeObject<List<Feature>>(results);
                    response.features = new List<Feature>();
                    response.features.AddRange(features);
                }

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return response.features;
        }

        //public RapidapiPostalBoundryResponse GetRapidApiPostalCodeBoundaries(List<string> postalCodes)
        //{
        //    var repidApiResonse = new RapidapiPostalBoundryResponse();
        //    try
        //    {
        //        // var client = new RestClient("https://vanitysoft-boundaries-io-v1.p.rapidapi.com/rest/v1/public/boundary/zipcode?zipcode=22066%2C20003%2C20019%2C20015%2C20854");
        //        //var client = new RestClient("https://vanitysoft-boundaries-io-v1.p.rapidapi.com/rest/v1/public/boundary/zipcode?zipcode=" + String.Join("%2C", postalCodes));
        //        //var request = new RestRequest(Method.GET);
        //        //request.AddHeader("X-RapidAPI-Key", "d888d90331mshced65919e546fd1p18373fjsn4800c4da63dc");
        //        //request.AddHeader("X-RapidAPI-Host", "vanitysoft-boundaries-io-v1.p.rapidapi.com");


        //        string RapidURL = ConfigurationManager.AppSettings["RapidURL"];
        //        string RapidAPIKey = ConfigurationManager.AppSettings["RapidAPIKey"];
        //        string RapidAPIHost = ConfigurationManager.AppSettings["RapidAPIHost"];

        //        var client = new RestClient(RapidURL+"?zipcode=" + String.Join("%2C", postalCodes));
        //        var request = new RestRequest(Method.GET);

        //        request.AddHeader("X-RapidAPI-Key", RapidAPIKey);
        //        request.AddHeader("X-RapidAPI-Host", RapidAPIHost);



        //        ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
        //        IRestResponse response = client.Execute(request);

        //        string val = Convert.ToString(response.Content);
        //        repidApiResonse = JsonConvert.DeserializeObject<RapidapiPostalBoundryResponse>(val);

        //        foreach (var feature in repidApiResonse.features)
        //        {
        //            AddUpdatePostal_Boundaries_Data(feature);
        //        }
        //        return repidApiResonse;
        //    }
        //    catch (Exception ex)
        //    {
        //        log.logErrorMessage(ex.StackTrace);
        //        log.logErrorMessage(ex.Message);
        //        return repidApiResonse;

        //    }

        //}


        public RapidapiPostalBoundryResponse GetRapidApiPostalCodeBoundaries(List<string> postalCodes)
        {
            var repidApiResonse = new RapidapiPostalBoundryResponse();
            try
            {
                // var client = new RestClient("https://vanitysoft-boundaries-io-v1.p.rapidapi.com/rest/v1/public/boundary/zipcode?zipcode=22066%2C20003%2C20019%2C20015%2C20854");
                //var client = new RestClient("https://vanitysoft-boundaries-io-v1.p.rapidapi.com/rest/v1/public/boundary/zipcode?zipcode=" + String.Join("%2C", postalCodes));
                //var request = new RestRequest(Method.GET);
                //request.AddHeader("X-RapidAPI-Key", "d888d90331mshced65919e546fd1p18373fjsn4800c4da63dc");
                //request.AddHeader("X-RapidAPI-Host", "vanitysoft-boundaries-io-v1.p.rapidapi.com");


                string RapidURL = ConfigurationManager.AppSettings["RapidURL"];
                string RapidAPIKey = ConfigurationManager.AppSettings["RapidAPIKey"];
                string RapidAPIHost = ConfigurationManager.AppSettings["RapidAPIHost"];

                var client = new RestClient(RapidURL + "?zipcode=" + String.Join("%2C", postalCodes));
                var request = new RestRequest(Method.GET);

                request.AddHeader("X-RapidAPI-Key", RapidAPIKey);
                request.AddHeader("X-RapidAPI-Host", RapidAPIHost);



                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                IRestResponse response = client.Execute(request);

                string val = Convert.ToString(response.Content);
                repidApiResonse = JsonConvert.DeserializeObject<RapidapiPostalBoundryResponse>(val);

                //foreach (var feature in repidApiResonse.features)
                //{
                //    AddUpdatePostal_Boundaries_Data(feature);
                //}
                foreach (var feature in repidApiResonse.features)
                {
                    if (feature.geometry.coordinates is List<List<List<double>>>)
                    {
                        // Handle three nested arrays
                        var coordinates = feature.geometry.coordinates as List<List<List<double>>>;
                        // Do something with the coordinates...
                    }
                    else if (feature.geometry.coordinates is List<List<double>>)
                    {
                        // Handle two nested arrays
                        var coordinates = feature.geometry.coordinates as List<List<double>>;
                        // Do something with the coordinates...
                    }
                    else
                    {
                        // Handle other cases as needed
                    }

                    // Rest of your code...
                    AddUpdatePostal_Boundaries_Data(feature);
                }

                return repidApiResonse;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return repidApiResonse;

            }

        }

        private List<dynamic> AddUpdatePostal_Boundaries_Data(Feature model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_Master_PostalCode_Boundaries]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {

                input_parameters.Add("@ZipCode", 1 + "#varchar#" + model.properties.zipCode);
                input_parameters.Add("@GeometryData", 1 + "#nvarchar#" + JsonConvert.SerializeObject(model));
                //input_parameters.Add("@PostalBoundry_pkeyId_Out", 2 + "#bigint#" + null)   ;
                //input_parameters.Add("@ReturnValue", 2 + "#int#" + null);
                objData = obj.SqlCRUD(insertProcedure, input_parameters);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

            }
            return objData;
        }
    }
}