﻿using Avigma.Repository.Lib;
using IPLApp.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPLApp.Repository.data
{
    public class UserClientRelationData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        //Add User Client Data 
        public List<dynamic> AddUserClientData(UserClientRelationDTO model)
        {
            List<dynamic> objuserData = new List<dynamic>();

            string insertProcedure = "[CreateUpdateUserClientRelation]";
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@UCM_pkeyID", 1 + "#bigint#" + model.UCM_pkeyID);
                input_parameters.Add("@UCM_UserID", 1 + "#bigint#" + model.UCM_UserID);
                input_parameters.Add("@UCM_ClientID", 1 + "#bigint#" + model.UCM_ClientID);
                input_parameters.Add("@UCM_IsActive", 1 + "#bit#" + model.UCM_IsActive);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UCM_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objuserData = obj.SqlCRUD(insertProcedure, input_parameters);
                return objuserData;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objuserData;





        }
        //get User Client Details
        private DataSet GetUserClientMaster(UserClientRelationDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_UserClientRelation]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@UCM_pkeyID", 1 + "#bigint#" + model.UCM_pkeyID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetUserClientDetails(UserClientRelationDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetUserClientMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<UserClientRelationDTO> UserClientDetails =
                   (from item in myEnumerableFeaprd
                    select new UserClientRelationDTO
                    {
                        UCM_pkeyID = item.Field<Int64>("UCM_pkeyID"),
                        UCM_UserID = item.Field<Int64?>("UCM_UserID"),
                        UCM_ClientID = item.Field<Int64?>("UCM_ClientID"),
                        UCM_IsActive = item.Field<Boolean?>("UCM_IsActive"),

                    }).ToList();

                objDynamic.Add(UserClientDetails);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}