﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class PCR_Conveyance_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public List<dynamic> AddPCR_Conveyance_Data(PCR_Conveyance_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objclntData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_PCR_Conveyance_Master]";
            PCR_Conveyance pCR_Conveyance = new PCR_Conveyance();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@PCR_Conveyance_pkeyID", 1 + "#bigint#" + model.PCR_Conveyance_pkeyID);
                input_parameters.Add("@PCR_Conveyance_MasterID", 1 + "#bigint#" + model.PCR_Conveyance_MasterID);
                input_parameters.Add("@PCR_Conveyance_Wo_ID", 1 + "#bigint#" + model.PCR_Conveyance_Wo_ID);
                input_parameters.Add("@PCR_Conveyance_ValType", 1 + "#int#" + model.PCR_Conveyance_ValType);

                input_parameters.Add("@PCR_Conveyance_Work_Order_Instruction", 1 + "#varchar#" + model.PCR_Conveyance_Work_Order_Instruction);
                input_parameters.Add("@PCR_Conveyance_Secured_Per_Guidelines", 1 + "#varchar#" + model.PCR_Conveyance_Secured_Per_Guidelines);
                input_parameters.Add("@PCR_Conveyance_Additional_Damage", 1 + "#varchar#" + model.PCR_Conveyance_Additional_Damage);
                input_parameters.Add("@PCR_Conveyance_Bid_On_This_Visit", 1 + "#varchar#" + model.PCR_Conveyance_Bid_On_This_Visit);
                input_parameters.Add("@PCR_Conveyance_Need_Maintenance", 1 + "#varchar#" + model.PCR_Conveyance_Need_Maintenance);

                input_parameters.Add("@PCR_Conveyance_Broom_Swept_Condition", 1 + "#varchar#" + model.PCR_Conveyance_Broom_Swept_Condition);
                input_parameters.Add("@PCR_Conveyance_HUD_Guidelines", 1 + "#varchar#" + model.PCR_Conveyance_HUD_Guidelines);
                input_parameters.Add("@PCR_Conveyance_Accidental_Entry", 1 + "#varchar#" + model.PCR_Conveyance_Accidental_Entry);
                input_parameters.Add("@PCR_Conveyance_Features_Are_Secure", 1 + "#varchar#" + model.PCR_Conveyance_Features_Are_Secure);
                input_parameters.Add("@PCR_Conveyance_In_Place_Operational", 1 + "#varchar#" + model.PCR_Conveyance_In_Place_Operational);
                input_parameters.Add("@PCR_Conveyance_Property_Of_Animals", 1 + "#varchar#" + model.PCR_Conveyance_Property_Of_Animals);

                input_parameters.Add("@PCR_Conveyance_Intact_Secure", 1 + "#varchar#" + model.PCR_Conveyance_Intact_Secure);
                input_parameters.Add("@PCR_Conveyance_Water_Instruction", 1 + "#varchar#" + model.PCR_Conveyance_Water_Instruction);
                input_parameters.Add("@PCR_Conveyance_Free_Of_Water", 1 + "#varchar#" + model.PCR_Conveyance_Free_Of_Water);
                input_parameters.Add("@PCR_Conveyance_Moisture_has_Eliminated", 1 + "#varchar#" + model.PCR_Conveyance_Moisture_has_Eliminated);
                input_parameters.Add("@PCR_Conveyance_Orderdinance", 1 + "#varchar#" + model.PCR_Conveyance_Orderdinance);

                input_parameters.Add("@PCR_Conveyance_Uneven", 1 + "#varchar#" + model.PCR_Conveyance_Uneven);
                input_parameters.Add("@PCR_Conveyance_Conveyance_Condition", 1 + "#varchar#" + model.PCR_Conveyance_Conveyance_Condition);
                input_parameters.Add("@PCR_Conveyance_Damage", 1 + "#bit#" + model.PCR_Conveyance_Damage);
                input_parameters.Add("@PCR_Conveyance_Debris", 1 + "#bit#" + model.PCR_Conveyance_Debris);
                input_parameters.Add("@PCR_Conveyance_Repairs", 1 + "#bit#" + model.PCR_Conveyance_Repairs);

                input_parameters.Add("@PCR_Conveyance_Hazards", 1 + "#bit#" + model.PCR_Conveyance_Hazards);
                input_parameters.Add("@PCR_Conveyance_Other", 1 + "#bit#" + model.PCR_Conveyance_Other);
                input_parameters.Add("@PCR_Conveyance_Describe", 1 + "#varchar#" + model.PCR_Conveyance_Describe);
                input_parameters.Add("@PCR_Conveyance_Note", 1 + "#varchar#" + model.PCR_Conveyance_Note);

                input_parameters.Add("@PCR_Conveyance_Work_Order_Instruction_Reason", 1 + "#varchar#" + model.PCR_Conveyance_Work_Order_Instruction_Reason);
                input_parameters.Add("@PCR_Conveyance_Secured_Per_Guidelines_Reason", 1 + "#varchar#" + model.PCR_Conveyance_Secured_Per_Guidelines_Reason);
                input_parameters.Add("@PCR_Conveyance_HUD_Guidelines_Reasponce", 1 + "#varchar#" + model.PCR_Conveyance_HUD_Guidelines_Reasponce);
                input_parameters.Add("@PCR_Conveyance_IsActive", 1 + "#bit#" + model.PCR_Conveyance_IsActive);
                input_parameters.Add("@PCR_Conveyance_IsDelete", 1 + "#bit#" + model.PCR_Conveyance_IsDelete);

                input_parameters.Add("@PCR_Conveyance_Shrubs_or_tree", 1 + "#varchar#" + model.PCR_Conveyance_Shrubs_or_tree);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@fwo_pkyeId", 1 + "#bigint#" + model.fwo_pkyeId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@PCR_Conveyance_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    pCR_Conveyance.PCR_Conveyance_pkeyID = "0";
                    pCR_Conveyance.Status = "0";
                    pCR_Conveyance.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    pCR_Conveyance.PCR_Conveyance_pkeyID = Convert.ToString(objData[0]);
                    pCR_Conveyance.Status = Convert.ToString(objData[1]);


                }
                objclntData.Add(pCR_Conveyance);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objclntData;



        }

        private DataSet GetPCRConveyanceMaster(PCR_Conveyance_MasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_PCR_Conveyance_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@PCR_Conveyance_pkeyID", 1 + "#bigint#" + model.PCR_Conveyance_pkeyID);
                input_parameters.Add("@PCR_Conveyance_Wo_ID", 1 + "#bigint#" + model.PCR_Conveyance_Wo_ID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetPCRConveyanceDetails(PCR_Conveyance_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetPCRConveyanceMaster(model);

                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                }

                //var PCRConveyance = DatatableToModel(ds.Tables[0]);
                //objDynamic.Add(PCRConveyance);

                //if (ds.Tables.Count > 1)
                //{
                //    var History = DatatableToModel(ds.Tables[1]);
                //    objDynamic.Add(History);
                //}
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
        private List<PCR_Conveyance_MasterDTO> DatatableToModel(DataTable dataTable)
        {
            var myEnumerableFeaprd = dataTable.AsEnumerable();
            List<PCR_Conveyance_MasterDTO> PCRConveyance =
               (from item in myEnumerableFeaprd
                select new PCR_Conveyance_MasterDTO
                {

                    PCR_Conveyance_pkeyID = item.Field<Int64>("PCR_Conveyance_pkeyID"),
                    PCR_Conveyance_MasterID = item.Field<Int64>("PCR_Conveyance_MasterID"),
                    PCR_Conveyance_Wo_ID = item.Field<Int64>("PCR_Conveyance_Wo_ID"),
                    PCR_Conveyance_ValType = item.Field<int?>("PCR_Conveyance_ValType"),
                    PCR_Conveyance_Work_Order_Instruction = item.Field<String>("PCR_Conveyance_Work_Order_Instruction"),
                    PCR_Conveyance_Secured_Per_Guidelines = item.Field<String>("PCR_Conveyance_Secured_Per_Guidelines"),
                    PCR_Conveyance_Additional_Damage = item.Field<String>("PCR_Conveyance_Additional_Damage"),
                    PCR_Conveyance_Bid_On_This_Visit = item.Field<String>("PCR_Conveyance_Bid_On_This_Visit"),
                    PCR_Conveyance_Need_Maintenance = item.Field<String>("PCR_Conveyance_Need_Maintenance"),
                    PCR_Conveyance_Broom_Swept_Condition = item.Field<String>("PCR_Conveyance_Broom_Swept_Condition"),
                    PCR_Conveyance_HUD_Guidelines = item.Field<String>("PCR_Conveyance_HUD_Guidelines"),
                    PCR_Conveyance_Accidental_Entry = item.Field<String>("PCR_Conveyance_Accidental_Entry"),
                    PCR_Conveyance_Features_Are_Secure = item.Field<String>("PCR_Conveyance_Features_Are_Secure"),
                    PCR_Conveyance_In_Place_Operational = item.Field<String>("PCR_Conveyance_In_Place_Operational"),
                    PCR_Conveyance_Property_Of_Animals = item.Field<String>("PCR_Conveyance_Property_Of_Animals"),
                    PCR_Conveyance_Intact_Secure = item.Field<String>("PCR_Conveyance_Intact_Secure"),
                    PCR_Conveyance_Water_Instruction = item.Field<String>("PCR_Conveyance_Water_Instruction"),
                    PCR_Conveyance_Free_Of_Water = item.Field<String>("PCR_Conveyance_Free_Of_Water"),
                    PCR_Conveyance_Moisture_has_Eliminated = item.Field<String>("PCR_Conveyance_Moisture_has_Eliminated"),
                    PCR_Conveyance_Orderdinance = item.Field<String>("PCR_Conveyance_Orderdinance"),
                    PCR_Conveyance_Uneven = item.Field<String>("PCR_Conveyance_Uneven"),
                    PCR_Conveyance_Conveyance_Condition = item.Field<String>("PCR_Conveyance_Conveyance_Condition"),
                    PCR_Conveyance_Damage = item.Field<Boolean?>("PCR_Conveyance_Damage"),
                    PCR_Conveyance_Debris = item.Field<Boolean?>("PCR_Conveyance_Debris"),
                    PCR_Conveyance_Repairs = item.Field<Boolean?>("PCR_Conveyance_Repairs"),
                    PCR_Conveyance_Hazards = item.Field<Boolean?>("PCR_Conveyance_Hazards"),
                    PCR_Conveyance_Other = item.Field<Boolean?>("PCR_Conveyance_Other"),
                    PCR_Conveyance_Describe = item.Field<String>("PCR_Conveyance_Describe"),
                    PCR_Conveyance_Note = item.Field<String>("PCR_Conveyance_Note"),
                    PCR_Conveyance_Work_Order_Instruction_Reason = item.Field<String>("PCR_Conveyance_Work_Order_Instruction_Reason"),
                    PCR_Conveyance_Secured_Per_Guidelines_Reason = item.Field<String>("PCR_Conveyance_Secured_Per_Guidelines_Reason"),
                    PCR_Conveyance_HUD_Guidelines_Reasponce = item.Field<String>("PCR_Conveyance_HUD_Guidelines_Reasponce"),
                    PCR_Conveyance_IsActive = item.Field<Boolean?>("PCR_Conveyance_IsActive"),
                    PCR_Conveyance_Shrubs_or_tree = item.Field<String>("PCR_Conveyance_Shrubs_or_tree"),






                }).ToList();

            return PCRConveyance;
        }
    }
}