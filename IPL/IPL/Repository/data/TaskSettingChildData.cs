﻿using Avigma.Repository.Lib;
using IPL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class TaskSettingChildData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public List<dynamic> AddTaskSettingChildData(TaskSettingChildDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objAddData = new List<dynamic>();
            string insertProcedure = "[CreateUpdateTaskSettingMaster]";
            TaskSettingChildMaster taskSettingChildMaster = new TaskSettingChildMaster();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();


            try
            {
                input_parameters.Add("@Task_sett_pkeyID", 1 + "#bigint#" + model.Task_sett_pkeyID);
                input_parameters.Add("@Task_sett_ID", 1 + "#bigint#" + model.Task_sett_ID);
                input_parameters.Add("@Task_sett_Company", 1 + "#varchar#" + model.Task_sett_Company);
                input_parameters.Add("@Task_sett_State", 1 + "#varchar#" + model.Task_sett_State);
                input_parameters.Add("@Task_sett_Country", 1 + "#varchar#" + model.Task_sett_Country);
                input_parameters.Add("@Task_sett_Zip", 1 + "#varchar#" + model.Task_sett_Zip);
                input_parameters.Add("@Task_sett_Contractor", 1 + "#varchar#" + model.Task_sett_Contractor);
                input_parameters.Add("@Task_sett_Customer", 1 + "#varchar#" + model.Task_sett_Customer);
                input_parameters.Add("@Task_sett_Lone", 1 + "#varchar#" + model.Task_sett_Lone);
                input_parameters.Add("@Task_sett_Con_Unit_Price", 1 + "#decimal#" + model.Task_sett_Con_Unit_Price);
                input_parameters.Add("@Task_sett_CLI_Unit_Price", 1 + "#decimal#" + model.Task_sett_CLI_Unit_Price);
                input_parameters.Add("@Task_sett_Flat_Free", 1 + "#bit#" + model.Task_sett_Flat_Free);
                input_parameters.Add("@Task_sett_Price_Edit", 1 + "#bit#" + model.Task_sett_Price_Edit);
                input_parameters.Add("@Task_Work_TypeGroup", 1 + "#varchar#" + model.Task_Work_TypeGroup);
                input_parameters.Add("@Task_sett_IsActive", 1 + "#bit#" + model.Task_sett_IsActive);
                input_parameters.Add("@Task_sett_IsDelete", 1 + "#bit#" + model.Task_sett_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Task_sett_Disable_Default", 1 + "#bit#" + model.Task_sett_Disable_Default);
                input_parameters.Add("@Task_sett_WorkType", 1 + "#varchar#" + model.Task_sett_WorkType);
                input_parameters.Add("@Task_sett_LotPrice", 1 + "#varchar#" + model.Task_sett_LotPrice);
                input_parameters.Add("@Task_sett_LOT_Min", 1 + "#decimal#" + model.Task_sett_LOT_Min);
                input_parameters.Add("@Task_sett_LOT_Max", 1 + "#decimal#" + model.Task_sett_LOT_Max);
                input_parameters.Add("@Task_sett_MapTaskFkeyId", 1 + "#bigint#" + model.Task_sett_MapTaskFkeyId);
                input_parameters.Add("@Task_sett_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[0] == 0)
                {
                    taskSettingChildMaster.Task_sett_pkeyID = "0";
                    taskSettingChildMaster.Status = "0";
                    taskSettingChildMaster.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    taskSettingChildMaster.Task_sett_pkeyID = Convert.ToString(objData[0]);
                    taskSettingChildMaster.Status = Convert.ToString(objData[1]);


                }



                if (model.TaskPresetDTO != null)
                {
                    TaskPresetDTO taskPresetDTO = new TaskPresetDTO();
                    TaskPresetData taskPresetData = new TaskPresetData();

                    for (int i = 0; i < model.TaskPresetDTO.Count; i++)
                    {
                        taskPresetDTO.Task_Preset_pkeyId = model.TaskPresetDTO[i].Task_Preset_pkeyId;
                        taskPresetDTO.Task_Preset_ID = model.Task_sett_pkeyID;
                        taskPresetDTO.Task_Preset_Text = model.TaskPresetDTO[i].Task_Preset_Text;
                        taskPresetDTO.Task_Preset_IsActive = model.TaskPresetDTO[i].Task_Preset_IsActive;
                        taskPresetDTO.Task_Preset_IsDelete = model.TaskPresetDTO[i].Task_Preset_IsDelete;
                        if (model.TaskPresetDTO[i].Task_Preset_pkeyId != 0) {
                            taskPresetDTO.Type = 2;
                        } else {
                            taskPresetDTO.Type = model.Type;
                        }
                        
                       

                        taskPresetData.AddTaskPresetChildData(taskPresetDTO);

                    }

                }

                objAddData.Add(taskSettingChildMaster);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return objAddData;



        }

        private DataSet GetTaskSettingChildMaster(TaskSettingChildDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Task_Setting_Child]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Task_sett_pkeyID", 1 + "#bigint#" + model.Task_sett_pkeyID);
                input_parameters.Add("@Task_sett_ID", 1 + "#bigint#" + model.Task_sett_ID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetTaskSettingChildDetails(TaskSettingChildDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetTaskSettingChildMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<TaskSettingChildDTO> TaskChildDetails =
                   (from item in myEnumerableFeaprd
                    select new TaskSettingChildDTO
                    {
                        Task_sett_pkeyID = item.Field<Int64>("Task_sett_pkeyID"),
                        Task_sett_ID = item.Field<Int64>("Task_sett_ID"),
                        Task_sett_Company = item.Field<String>("Task_sett_Company"),
                        Task_sett_State = item.Field<String>("Task_sett_State"),
                        Task_sett_Country = item.Field<String>("Task_sett_Country"),
                        Task_sett_Zip = item.Field<String>("Task_sett_Zip"),
                        Task_sett_Contractor = item.Field<String>("Task_sett_Contractor"),
                        Task_sett_Customer = item.Field<String>("Task_sett_Customer"),
                        Task_sett_Lone = item.Field<String>("Task_sett_Lone"),
                        Task_sett_Con_Unit_Price = item.Field<Decimal?>("Task_sett_Con_Unit_Price"),
                        Task_sett_CLI_Unit_Price = item.Field<Decimal?>("Task_sett_CLI_Unit_Price"),
                        Task_sett_Flat_Free = item.Field<Boolean?>("Task_sett_Flat_Free"),
                        Task_sett_Price_Edit = item.Field<Boolean?>("Task_sett_Price_Edit"),
                        Task_sett_IsActive = item.Field<Boolean?>("Task_sett_IsActive"),
                        Task_sett_Disable_Default = item.Field<Boolean?>("Task_sett_Disable_Default"),
                        Task_sett_WorkType = item.Field<String>("Task_sett_WorkType"),
                        Task_sett_LOT_Min = item.Field<Decimal?>("Task_sett_LOT_Min"),
                        Task_sett_LOT_Max = item.Field<Decimal?>("Task_sett_LOT_Max"),

                    }).ToList();

                objDynamic.Add(TaskChildDetails);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}