﻿using Avigma.Repository.Lib;
using IPLApp.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPLApp.Repository.data
{
    public class Suggestion_VoteData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        private List<dynamic> CreateUpdateSuggestion_Vote_Data(Suggestion_Vote_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_Suggestion_Vote]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Sug_Vote_PkeyID", 1 + "#bigint#" + model.Sug_Vote_PkeyID);
                input_parameters.Add("@Sug_Vote_UserID", 1 + "#bigint#" + model.Sug_Vote_UserID);
                input_parameters.Add("@Sug_Vote_Sug_PkeyID", 1 + "#bigint#" + model.Sug_Vote_Sug_PkeyID);
                input_parameters.Add("@Sug_Vote_Val", 1 + "#bit#" + model.Sug_Vote_Val);

                input_parameters.Add("@Sug_Vote_IsActive", 1 + "#bit#" + model.Sug_Vote_IsActive);
                input_parameters.Add("@Sug_Vote_IsDelete", 1 + "#bit#" + model.Sug_Vote_IsDelete);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Sug_Vote_PkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);
                objData = obj.SqlCRUD(insertProcedure, input_parameters);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        public List<dynamic> AddUpdateSuggestion_Vote_Data(Suggestion_Vote_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                objData = CreateUpdateSuggestion_Vote_Data(model);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        private DataSet Get_Suggestion_VoteDetails(Suggestion_Vote_DTO model)
        {
            DataSet ds = null;
            try
            {

                string selectProcedure = "[GetSuggestion_VoteDetails]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Sug_Vote_PkeyID", 1 + "#bigint#" + model.Sug_Vote_PkeyID);

                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }
        public List<dynamic> GetSuggestion_VoteDetails(Suggestion_Vote_DTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {

                model.Type = 1;
                DataSet ds = Get_Suggestion_VoteDetails(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Suggestion_Vote_DTO> SuggestionVote =
                   (from item in myEnumerableFeaprd
                    select new Suggestion_Vote_DTO
                    {
                        Sug_Vote_PkeyID = item.Field<Int64>("Sug_Vote_PkeyID"),
                        Sug_Vote_UserID = item.Field<Int64>("Sug_Vote_UserID"),
                        Sug_Vote_Sug_PkeyID = item.Field<Int64>("Sug_Vote_Sug_PkeyID"),
                        Sug_Vote_Val = item.Field<Boolean>("Sug_Vote_Val"),
                        Sug_Vote_IsActive = item.Field<Boolean>("Sug_Vote_IsActive"),
                        Sug_Vote_IsDelete = item.Field<Boolean>("Sug_Vote_IsDelete"),

                    }).ToList();

                objDynamic.Add(SuggestionVote);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }


    }
}