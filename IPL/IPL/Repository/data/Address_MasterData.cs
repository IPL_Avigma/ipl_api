﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class Address_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        private List<dynamic> CreateUpdateAddressMaster(Address_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_Address_Master]";
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@ADD_PkeyId", 1 + "#bigint#" + model.ADD_PkeyId);
                input_parameters.Add("@ADD_address", 1 + "#nvarchar#" + model.ADD_address);
                input_parameters.Add("@ADD_city", 1 + "#nvarchar#" + model.ADD_city);
                input_parameters.Add("@ADD_state", 1 + "#int#" + model.ADD_state);
                input_parameters.Add("@ADD_zip", 1 + "#int#" + model.ADD_zip);
                input_parameters.Add("@ADD_county", 1 + "#nvarchar#" + model.ADD_county);
                input_parameters.Add("@ADD_gpsLatitude", 1 + "#nvarchar#" + model.ADD_gpsLatitude);
                input_parameters.Add("@ADD_gpsLongitude", 1 + "#nvarchar#" + model.ADD_gpsLongitude);
                input_parameters.Add("@ADD_FinalAddress", 1 + "#nvarchar#" + model.ADD_FinalAddress);
                input_parameters.Add("@ADD_IsActive", 1 + "#bit#" + model.ADD_IsActive);
                input_parameters.Add("@ADD_IsDelete", 1 + "#bit#" + model.ADD_IsDelete);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@ADD_Pkey_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);
                objData = obj.SqlCRUD(insertProcedure, input_parameters);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        private DataSet GetAddressMaster(Address_MasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Address_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@ADD_PkeyId", 1 + "#bigint#" + model.ADD_PkeyId);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);

                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> PostAddressMasterDetails(Address_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                objData = CreateUpdateAddressMaster(model);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }
            return objData;
        }

        public List<dynamic> GetAddressMasterDetails(Address_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetAddressMaster(model);

                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                }

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

    }
}