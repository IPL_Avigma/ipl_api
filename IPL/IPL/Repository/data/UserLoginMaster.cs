﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Web;


namespace IPL.Repository.data
{
    public class UserLoginMaster
    {

        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        private DataSet GetLoginMaster(AuthLoginDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[GetAuthUserLogin]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@User_UserName", 1 + "#varchar#" + model.User_LoginName);
                input_parameters.Add("@User_pkeyID", 1 + "#bigint#" + model.User_pkeyID);
                input_parameters.Add("@User_Password", 1 + "#varchar#" + model.User_Password);
                input_parameters.Add("@User_Acc_Log_Device_Name", 1 + "#varchar#" + model.User_Acc_Log_Device_Name);
                input_parameters.Add("@MacId", 1 + "#varchar#" + model.MacId);
                input_parameters.Add("@IP", 1 + "#varchar#" + model.IP);
                input_parameters.Add("@User_Token_val", 1 + "#varchar#" + model.User_Token_val);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }


            return ds;
        }
        public List<dynamic> GetAuthenticateLoginDetails(AuthLoginDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetLoginMaster(model);
                if (ds.Tables.Count > 0)
                {
                    var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                    List<AuthLoginDTO> usersDetails =
                       (from item in myEnumerableFeaprd
                        select new AuthLoginDTO
                        {
                            User_pkeyID = item.Field<Int64>("User_pkeyID"),

                            User_FirstName = item.Field<String>("User_FirstName"),
                            User_LastName = item.Field<String>("User_LastName"),
                            // User_LoginName = item.Field<String>("User_LoginName"),
                            //User_Email = item.Field<String>("User_Email"),
                            User_Tracking = item.Field<Boolean?>("User_Tracking"),
                            User_Tracking_Time = item.Field<int?>("User_Tracking_Time"),

                            User_IsPasswordChange = item.Field<Boolean?>("User_IsPasswordChange"),
                            GroupRoleId = item.Field<Int64>("GroupRoleId"),
                            IPL_Company_ID = item.Field<String>("IPL_Company_ID"),
                            User_ImagePath = item.Field<String>("User_ImagePath"),
                        }).ToList();

                    objDynamic.Add(usersDetails);
                }
                else
                {
                    objDynamic.Add("No UserRecord Found");
                    log.logDebugMessage("No UserRecord Found For" + model.User_LoginName);
                }

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

            }

            return objDynamic;
        }
        public List<dynamic> GetLoginDetails(AuthLoginDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetLoginMaster(model);
                if (ds.Tables.Count > 0)
                {
                    var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                    List<AuthLoginDTO> usersDetails =
                       (from item in myEnumerableFeaprd
                        select new AuthLoginDTO
                        {
                            User_pkeyID = item.Field<Int64>("User_pkeyID"),
                            User_FirstName = item.Field<String>("User_FirstName"),
                            User_LastName = item.Field<String>("User_LastName"),
                            User_Password = item.Field<String>("User_Password"),
                            User_Tracking = item.Field<Boolean?>("User_Tracking"),
                            User_Tracking_Time = item.Field<int?>("User_Tracking_Time"),

                            User_IsPasswordChange = item.Field<Boolean?>("User_IsPasswordChange"),
                            GroupRoleId = item.Field<Int64>("GroupRoleId"),
                            User_CellNumber = item.Field<String>("User_CellNumber"),
                            IPL_Company_ID = item.Field<String>("IPL_Company_ID"),
                            User_ImagePath = item.Field<String>("User_ImagePath"),
                            User_LoginName = item.Field<String>("User_LoginName"),

                        }).ToList();

                    objDynamic.Add(usersDetails);
                }



                switch (model.Type)
                {
                    case 2:
                        {
                            if (ds.Tables.Count > 1)
                            {
                                var myEnumerableStatusCount = ds.Tables[1].AsEnumerable();
                                List<StatusCountChart> StatusCountChart =
                                (from item in myEnumerableStatusCount
                                 select new StatusCountChart
                                 {
                                     Client_Company_Name = item.Field<String>("Client_Company_Name"),
                                     Unassigned_count = item.Field<int>("Unassigned_count"),
                                     Assigned_count = item.Field<int>("Assigned_count"),
                                     Field_Complete_count = item.Field<int>("Field_Complete_count"),
                                     Office_Approved_count = item.Field<int>("Office_Approved_count"),
                                     Total_count = item.Field<int>("Total_count")
                                 }).ToList();

                                objDynamic.Add(StatusCountChart);



                                var myEnumerableWorkOrderCount = ds.Tables[2].AsEnumerable();
                                List<WorkOrderCountChart> WorkOrderCountChart =
                                (from item in myEnumerableWorkOrderCount
                                 select new WorkOrderCountChart
                                 {
                                     countval = item.Field<int>("countval"),
                                     Client_Company_Name = item.Field<String>("Client_Company_Name")


                                 }).ToList();

                                objDynamic.Add(WorkOrderCountChart);


                                var myEnumerableWorkOrderDueDate = ds.Tables[3].AsEnumerable();
                                List<WorkOrderCountChart> WorkOrderCountDueDate =
                                (from item in myEnumerableWorkOrderDueDate
                                 select new WorkOrderCountChart
                                 {
                                     countval = item.Field<int>("countval"),
                                     Client_Company_Name = item.Field<String>("Client_Company_Name")


                                 }).ToList();

                                objDynamic.Add(WorkOrderCountDueDate);


                                var myEnumerableWorkOrderpast = ds.Tables[4].AsEnumerable();
                                List<Pastworkordercount> WorkOrderCountpast =
                                (from item in myEnumerableWorkOrderpast
                                 select new Pastworkordercount
                                 {
                                     Status_ID = item.Field<Int64>("Status_ID"),
                                     Status_Name = item.Field<String>("Status_Name"),
                                     PastStatusCount = item.Field<int>("PastStatusCount"),
                                     FutureStatusCount = item.Field<int>("FutureStatusCount"),
                                     TotalCount = item.Field<int>("TotalCount"),


                                 }).ToList();

                                objDynamic.Add(WorkOrderCountpast);


                                var myEnumerableWorkOrderinv = ds.Tables[5].AsEnumerable();
                                List<InvoiceDeshboardDto> woinvoice =
                                (from item in myEnumerableWorkOrderinv
                                 select new InvoiceDeshboardDto
                                 {
                                     Client_Company_Name = item.Field<String>("Client_Company_Name"),
                                     Client_pkeyID = item.Field<Int64>("Client_pkeyID"),
                                     Firstval = Convert.ToInt64(item.Field<Decimal>("Firstval")),
                                     Secondval = Convert.ToInt64(item.Field<Decimal>("Secondval")),
                                     Thirdval = Convert.ToInt64(item.Field<Decimal>("Thirdval")),
                                     Fourthval = Convert.ToInt64(item.Field<Decimal>("Fourthval")),


                                 }).ToList();

                                objDynamic.Add(woinvoice);


                                objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[6]));
                            }
                            break;
                        }
                    case 3:
                        {
                            if (ds.Tables.Count > 1)
                            {
                                var myEnumerableStatusCount = ds.Tables[1].AsEnumerable();
                                List<StatusCountChart> StatusCountChart =
                                (from item in myEnumerableStatusCount
                                 select new StatusCountChart
                                 {
                                     Client_Company_Name = item.Field<String>("Client_Company_Name"),
                                     Unassigned_count = item.Field<int>("Unassigned_count"),
                                     Assigned_count = item.Field<int>("Assigned_count"),
                                     Field_Complete_count = item.Field<int>("Field_Complete_count"),
                                     Office_Approved_count = item.Field<int>("Office_Approved_count"),
                                     Total_count = item.Field<int>("Total_count")
                                 }).ToList();

                                objDynamic.Add(StatusCountChart);



                                var myEnumerableWorkOrderCount = ds.Tables[2].AsEnumerable();
                                List<WorkOrderCountChart> WorkOrderCountChart =
                                (from item in myEnumerableWorkOrderCount
                                 select new WorkOrderCountChart
                                 {
                                     countval = item.Field<int>("countval"),
                                     Client_Company_Name = item.Field<String>("Client_Company_Name")


                                 }).ToList();

                                objDynamic.Add(WorkOrderCountChart);


                                var myEnumerableWorkOrderDueDate = ds.Tables[3].AsEnumerable();
                                List<WorkOrderCountChart> WorkOrderCountDueDate =
                                (from item in myEnumerableWorkOrderDueDate
                                 select new WorkOrderCountChart
                                 {
                                     countval = item.Field<int>("countval"),
                                     Client_Company_Name = item.Field<String>("Client_Company_Name")


                                 }).ToList();

                                objDynamic.Add(WorkOrderCountDueDate);


                                var myEnumerableWorkOrderpast = ds.Tables[4].AsEnumerable();
                                List<Pastworkordercount> WorkOrderCountpast =
                                (from item in myEnumerableWorkOrderpast
                                 select new Pastworkordercount
                                 {
                                     Status_ID = item.Field<Int64>("Status_ID"),
                                     Status_Name = item.Field<String>("Status_Name"),
                                     PastStatusCount = item.Field<int>("PastStatusCount"),
                                     FutureStatusCount = item.Field<int>("FutureStatusCount"),
                                     TotalCount = item.Field<int>("TotalCount"),


                                 }).ToList();

                                objDynamic.Add(WorkOrderCountpast);


                                var myEnumerableWorkOrderinv = ds.Tables[5].AsEnumerable();
                                List<InvoiceDeshboardDto> woinvoice =
                                (from item in myEnumerableWorkOrderinv
                                 select new InvoiceDeshboardDto
                                 {
                                     Client_Company_Name = item.Field<String>("Client_Company_Name"),
                                     Firstval = Convert.ToInt64(item.Field<Decimal>("Firstval")),
                                     Secondval = Convert.ToInt64(item.Field<Decimal>("Secondval")),
                                     Thirdval = Convert.ToInt64(item.Field<Decimal>("Thirdval")),
                                     Fourthval = Convert.ToInt64(item.Field<Decimal>("Fourthval")),


                                 }).ToList();

                                objDynamic.Add(woinvoice);


                                objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[6]));
                            }
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

            }

            return objDynamic;
        }

        public List<dynamic> AddUserAccessLog_Logout(UserAccessLog model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();
            UserAccessLogmaster userAccessLogmaster = new UserAccessLogmaster();

            string insertProcedure = "[CreateUpdate_UserAccesslogMaster_Logout]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@User_Acc_Log_UserName_Out", 2 + "#nvarchar#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    userAccessLogmaster.User_Acc_Log_UserName = "0";
                    userAccessLogmaster.Status = "0";
                    userAccessLogmaster.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    userAccessLogmaster.User_Acc_Log_UserName = Convert.ToString(objData[0]);
                    userAccessLogmaster.Status = Convert.ToString(objData[1]);
                }

                objcltData.Add(userAccessLogmaster);

                //if (!string.IsNullOrEmpty(userAccessLogmaster.User_Acc_Log_UserName) && !string.IsNullOrWhiteSpace(userAccessLogmaster.User_Acc_Log_UserName))
                //{
                //    var isUserLocationDelete = DeleteFirbaseUserLocation(userAccessLogmaster.User_Acc_Log_UserName);
                //}
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;



        }

        public List<dynamic> AddUserAccessLog_Logout_Mobile(UserAccessLog model)
        {
            try
            {
                List<dynamic> objData = new List<dynamic>();
                List<dynamic> objcltData = new List<dynamic>();
                UserAccessLogmaster userAccessLogmaster = new UserAccessLogmaster();

                string insertProcedure = "[CreateUpdate_UserAccesslogMaster_Logout]";

                Dictionary<string, string> input_parameters = new Dictionary<string, string>();
                try
                {
                    input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                    input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                    input_parameters.Add("@User_Acc_Log_UserName_Out", 2 + "#nvarchar#" + null);
                    input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                    objData = obj.SqlCRUD(insertProcedure, input_parameters);
                    if (objData[1] == 0)
                    {
                        userAccessLogmaster.User_Acc_Log_UserName = "0";
                        userAccessLogmaster.Status = "0";
                        userAccessLogmaster.ErrorMessage = "Error while Saviving in the Database";

                    }
                    else
                    {
                        userAccessLogmaster.User_Acc_Log_UserName = Convert.ToString(objData[0]);
                        userAccessLogmaster.Status = Convert.ToString(objData[1]);
                    }

                    objcltData.Add(userAccessLogmaster);

                    if (!string.IsNullOrEmpty(userAccessLogmaster.User_Acc_Log_UserName) && !string.IsNullOrWhiteSpace(userAccessLogmaster.User_Acc_Log_UserName))
                    {
                        var isUserLocationDelete = DeleteFirbaseUserLocation(userAccessLogmaster.User_Acc_Log_UserName);
                    }
                }
                catch (Exception ex)
                {
                    log.logErrorMessage(ex.StackTrace);
                    log.logErrorMessage(ex.Message);
                }
                return objcltData;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

            }

            return null;
        }

        public bool DeleteFirbaseUserLocation(string locationUserName)
        {
            if (!string.IsNullOrEmpty(locationUserName) && !string.IsNullOrWhiteSpace(locationUserName))
            {
                string firebaseLocationUrl = System.Configuration.ConfigurationManager.AppSettings["FirebaseLocationUrl"];

                string deleteNodeUrl = firebaseLocationUrl + locationUserName + ".json";
                System.Net.Http.HttpClient client = new System.Net.Http.HttpClient();
                HttpResponseMessage messge = client.DeleteAsync(deleteNodeUrl).Result;
                return messge.IsSuccessStatusCode;
            }
            return false;
        }
    }
}