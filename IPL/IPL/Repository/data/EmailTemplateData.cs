﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Avigma.Models;
using System.Text.RegularExpressions;
using IPL.Repository.Lib;
using System.Threading.Tasks;

namespace IPL.Repository.data
{
    public class EmailTemplateData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        public List<dynamic> AddEmailTempData(EmailTemplateDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_EmailTemplateMaster]";
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Email_Temp_PkeyId", 1 + "#bigint#" + model.Email_Temp_PkeyId);
                input_parameters.Add("@Email_Temp_HTML", 1 + "#nvarchar#" + model.Email_Temp_HTML);
                input_parameters.Add("@Email_Temp_Subject", 1 + "#nvarchar#" + model.Email_Temp_Subject);
                input_parameters.Add("@Email_Temp_IsActive", 1 + "#bit#" + model.Email_Temp_IsActive);
                input_parameters.Add("@Email_Temp_Delete", 1 + "#bit#" + model.Email_Temp_Delete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Val_Type", 1 + "#int#" + model.Val_Type);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Email_Temp_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;



        }

        private DataSet GetEmailTempMaster(EmailTemplateDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[GetEmailTemplateMaster]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Email_Temp_PkeyId", 1 + "#bigint#" + model.Email_Temp_PkeyId);
                input_parameters.Add("@Val_Type", 1 + "#int#" + model.Val_Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }
        //Get App Company Details 
        public List<dynamic> GetEmailTempMasterDetails(EmailTemplateDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetEmailTempMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<EmailTemplateDTO> EmailTempDetails =
                   (from item in myEnumerableFeaprd
                    select new EmailTemplateDTO
                    {
                        Email_Temp_PkeyId = item.Field<Int64>("Email_Temp_PkeyId"),
                        Email_Temp_HTML = item.Field<String>("Email_Temp_HTML"),
                        Email_Temp_Subject = item.Field<String>("Email_Temp_Subject"),
                        Email_Temp_IsActive = item.Field<Boolean?>("Email_Temp_IsActive"),
                        Val_Type = item.Field<int>("Val_Type"),

                    }).ToList();

                objDynamic.Add(EmailTempDetails);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        private DataSet GetEmailheadingData()
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_EmailHeading_MasterDrd]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();


                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }
        public List<dynamic> GetEmailheadingDetail()
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetEmailheadingData();

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<EmailHeadingDTO> EmailHeading =
                   (from item in myEnumerableFeaprd
                    select new EmailHeadingDTO
                    {
                        Eml_PkeyId = item.Field<Int64>("Eml_PkeyId"),
                        Eml_Name = item.Field<String>("Eml_Name"),

                    }).ToList();

                objDynamic.Add(EmailHeading);

                WorkOrder_Column_Data workOrder_Column_Data = new WorkOrder_Column_Data();
                WorkOrder_Column_DTO workOrder_Column_DTO = new WorkOrder_Column_DTO();
                workOrder_Column_DTO.UserID = 0;
                workOrder_Column_DTO.Type = 4;
                objDynamic.Add(workOrder_Column_Data.GetEmailMetaData(workOrder_Column_DTO));

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        //email workOrder details
        public DataSet GetEmailWorkOderData(EmailWorkOderDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[GetWorkOrder_EmailMaster]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@workOrder_ID", 1 + "#bigint#" + model.workOrder_ID);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Val_Type", 1 + "#int#" + model.Val_Type);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }
        public async Task<int> GetEmailWorkOderDetail(EmailWorkOderDTO model)
        {
            DataSet dsdata = null;
            EmailManager emailManager = new EmailManager();
            //EmailDTO emailDTO = new EmailDTO();
            DyanmicEmailDTO emailDTO = new DyanmicEmailDTO();
            int ret = 0;
            string strSubject = string.Empty;
            string strBody = string.Empty;
            string strTaskComments = string.Empty;
            string strStatus = "0";
            try
            {
                dsdata = GetEmailWorkOderData(model);

                for (int i = 0; i < dsdata.Tables[2].Rows.Count; i++)
                {
                    strStatus = dsdata.Tables[2].Rows[i]["status"].ToString();
                }
                if (strStatus == "1" || strStatus == "2")
                {
                    if (dsdata.Tables.Count > 1 && dsdata.Tables[1].Rows.Count > 0)
                    {
                        strSubject = dsdata.Tables[1].Rows[0]["Email_Temp_Subject"].ToString();
                        strBody = dsdata.Tables[1].Rows[0]["Email_Temp_HTML"].ToString();
                    }
                    if (dsdata.Tables.Count > 2 && dsdata.Tables[2].Rows.Count > 0)
                    {
                        model.ContractorName = dsdata.Tables[2].Rows[0]["ContractorName"].ToString();
                        model.ContractorEmail = dsdata.Tables[2].Rows[0]["ContractorEmail"].ToString();
                        model.Processor_Name = dsdata.Tables[2].Rows[0]["Processor_Name"].ToString();
                        model.ProcessorEmail = dsdata.Tables[2].Rows[0]["ProcessorEmail"].ToString();
                        model.Cordinator_Name = dsdata.Tables[2].Rows[0]["Cordinator_Name"].ToString();
                        model.CordinatorEmail = dsdata.Tables[2].Rows[0]["CordinatorEmail"].ToString();
                        model.User_FirstName = dsdata.Tables[2].Rows[0]["User_FirstName"].ToString();
                        model.Cordinator_Phone = dsdata.Tables[2].Rows[0]["Cordinator_Phone"].ToString();
                        model.Comments = dsdata.Tables[2].Rows[0]["Inst_Ch_Comand_Mobile"].ToString();
                        model.Client_Result_Photo_FilePath = dsdata.Tables[2].Rows[0]["Client_Result_Photo_FilePath"].ToString();

                        model.workOrderNumber = dsdata.Tables[2].Rows[0]["workOrderNumber"].ToString();
                        model.WT_WorkType = dsdata.Tables[2].Rows[0]["WT_WorkType"].ToString();
                        model.address1 = dsdata.Tables[2].Rows[0]["address1"].ToString();
                        model.city = dsdata.Tables[2].Rows[0]["city"].ToString();
                        model.state = dsdata.Tables[2].Rows[0]["state"].ToString();
                        model.zip = dsdata.Tables[2].Rows[0]["zip"].ToString();
                        model.IPLNO = dsdata.Tables[2].Rows[0]["IPLNO"].ToString();
                        if (!string.IsNullOrEmpty(dsdata.Tables[2].Rows[0]["dueDate"].ToString()))
                        {
                            model.dueDate = Convert.ToDateTime(dsdata.Tables[2].Rows[0]["dueDate"].ToString());
                        }

                        model.Client_Company_Name = dsdata.Tables[2].Rows[0]["Client_Company_Name"].ToString();
                        //model.Comments = dsdata.Tables[2].Rows[0]["Comments"].ToString();
                    }

                    string strTask = string.Empty, strDocument = string.Empty;
                    string strTaskFile = string.Empty, strDocumentFile = string.Empty;


                    if (dsdata.Tables.Count > 2)
                    {
                        for (int i = 0; i < dsdata.Tables[3].Rows.Count; i++)
                        {
                            string strTaskdetails = string.Empty;
                            string strTaskName = dsdata.Tables[3].Rows[i]["Task_Name"].ToString();
                            strTaskComments = dsdata.Tables[3].Rows[i]["Inst_Comand_Mobile_details"].ToString();
                            string strTaskFilePath = dsdata.Tables[3].Rows[i]["TMF_Task_localPath"].ToString();
                            string TMF_Task_FileName = dsdata.Tables[3].Rows[i]["TMF_Task_FileName"].ToString();

                            if (!string.IsNullOrEmpty(strTaskFilePath))
                            {
                                strTaskFile = "<a href=" + strTaskFilePath + "> " + TMF_Task_FileName + " </a>";
                            }


                            strTaskdetails = strTaskName + "<br/>" + strTaskComments;

                            strTask = "<br/>" + strTask + strTaskdetails + "<br/>" + strTaskFile + "<br/>";

                        }
                    }

                    if (dsdata.Tables.Count > 3)
                    {
                        for (int i = 0; i < dsdata.Tables[4].Rows.Count; i++)
                        {
                            string Inst_Doc_File_Path = dsdata.Tables[4].Rows[i]["Inst_Doc_File_Path"].ToString();
                            string Inst_Doc_File_Name = dsdata.Tables[4].Rows[i]["Inst_Doc_File_Name"].ToString();
                            if (!string.IsNullOrEmpty(Inst_Doc_File_Path))
                            {
                                strDocumentFile = "<a href=" + Inst_Doc_File_Path + " > " + Inst_Doc_File_Name + " </a>";
                            }

                            strDocument = "<br/>" + strDocument + "<br/>" + strDocumentFile + "<br/>";

                        }
                    }

                    if (dsdata.Tables.Count > 0)
                    {
                        for (int i = 0; i < dsdata.Tables[0].Rows.Count; i++)
                        {
                            string StrKeyData = dsdata.Tables[0].Rows[i]["Keydata"].ToString();
                            string strCloumnData = dsdata.Tables[0].Rows[i]["Wo_Column_Name"].ToString();
                            string value = string.Empty;


                            if (StrKeyData == "Inst_Task_Name")
                            {
                                value = strTask;
                                strBody = strBody.Replace(strCloumnData, " " + strTask);
                            }
                            else if (StrKeyData == "Inst_Comand_Mobile_details")
                            {
                                value = strTaskComments;
                                strBody = strBody.Replace(strCloumnData, " " + value);
                            }
                            else if (StrKeyData == "dueDate")
                            {
                                if (model.dueDate != null)
                                {
                                    value = model.dueDate.Value.ToString("MM/dd/yyyy");
                                }
                                strBody = strBody.Replace(strCloumnData, " " + value);
                            }
                            else if (StrKeyData == "Document")
                            {
                                value = strDocument;
                                strBody = strBody.Replace(strCloumnData, " " + value);
                            }

                            else
                            {
                                if (dsdata.Tables.Count > 2 && dsdata.Tables[2].Rows.Count > 0)
                                {
                                    value = dsdata.Tables[2].Rows[0][StrKeyData].ToString();
                                    strBody = strBody.Replace(strCloumnData, " " + value);
                                }
                            }

                            strSubject = strSubject.Replace(strCloumnData, " " + value);
                            #region Comment
                            //switch (StrKeyData)
                            //{

                            //    case "Inst_Task_Name":
                            //        {
                            //            strBody = strBody.Replace(strCloumnData, " " + strTask);
                            //            break;
                            //        }

                            //    case "Inst_Task_Name":
                            //        {
                            //            strBody = strBody.Replace(strCloumnData, " " + strTask);
                            //            break;
                            //        }

                            //}
                            //strBody = strBody.Replace(strCloumnData, " " + value);
                            #endregion
                        }
                        //strBody = strBody + "<br/>" + "Documents" + "<br/>" + strDocument;

                    }

                    if (!string.IsNullOrEmpty(model.ContractorEmail))
                    {
                        emailDTO.To = model.ContractorEmail;
                        emailDTO.Cc = model.CordinatorEmail;
                        emailDTO.Subject = strSubject;
                        emailDTO.Message = strBody;

                        ret = await DynamicEmailManager.SendDynamicEmail(emailDTO,3);
                    }
                    else
                    {
                        log.logInfoMessage("Contractor email is empty or null");
                    }

                    //ret =  emailManager.SendEmail(emailDTO);

                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return ret;
        }




        public Email_Message_DTO GetEmailMessageDetail(EmailWorkOderDTO model)
        {
            DataSet dsdata = null;
            Email_Message_DTO email_Message_DTO = new Email_Message_DTO();

            string strSubject = string.Empty;
            string strBody = string.Empty;
            string strTaskComments = string.Empty;
            try
            {

                dsdata = GetEmailWorkOderData(model);



                if (dsdata.Tables.Count > 1 && dsdata.Tables[1].Rows.Count > 0)
                {
                    strSubject = dsdata.Tables[1].Rows[0]["Email_Temp_Subject"].ToString();
                    strBody = dsdata.Tables[1].Rows[0]["Email_Temp_HTML"].ToString();
                }
                if (dsdata.Tables.Count > 2 && dsdata.Tables[2].Rows.Count > 0)
                {
                    model.ContractorName = dsdata.Tables[2].Rows[0]["ContractorName"].ToString();
                    model.ContractorEmail = dsdata.Tables[2].Rows[0]["ContractorEmail"].ToString();
                    model.Processor_Name = dsdata.Tables[2].Rows[0]["Processor_Name"].ToString();
                    model.ProcessorEmail = dsdata.Tables[2].Rows[0]["ProcessorEmail"].ToString();
                    model.Cordinator_Name = dsdata.Tables[2].Rows[0]["Cordinator_Name"].ToString();
                    model.CordinatorEmail = dsdata.Tables[2].Rows[0]["CordinatorEmail"].ToString();
                    model.User_FirstName = dsdata.Tables[2].Rows[0]["User_FirstName"].ToString();
                    model.Cordinator_Phone = dsdata.Tables[2].Rows[0]["Cordinator_Phone"].ToString();
                    model.Comments = dsdata.Tables[2].Rows[0]["Inst_Ch_Comand_Mobile"].ToString();
                    model.Client_Result_Photo_FilePath = dsdata.Tables[2].Rows[0]["Client_Result_Photo_FilePath"].ToString();

                    model.workOrderNumber = dsdata.Tables[2].Rows[0]["workOrderNumber"].ToString();
                    model.WT_WorkType = dsdata.Tables[2].Rows[0]["WT_WorkType"].ToString();
                    model.address1 = dsdata.Tables[2].Rows[0]["address1"].ToString();
                    model.city = dsdata.Tables[2].Rows[0]["city"].ToString();
                    model.state = dsdata.Tables[2].Rows[0]["state"].ToString();
                    model.zip = dsdata.Tables[2].Rows[0]["zip"].ToString();
                    model.IPLNO = dsdata.Tables[2].Rows[0]["IPLNO"].ToString();
                    if (!string.IsNullOrEmpty(dsdata.Tables[2].Rows[0]["dueDate"].ToString()))
                    {
                        model.dueDate = Convert.ToDateTime(dsdata.Tables[2].Rows[0]["dueDate"].ToString());
                    }

                    model.Client_Company_Name = dsdata.Tables[2].Rows[0]["Client_Company_Name"].ToString();
                    model.Comments = dsdata.Tables[2].Rows[0]["Comments"].ToString();
                }

                string strTask = string.Empty, strDocument = string.Empty;
                string strTaskFile = string.Empty, strDocumentFile = string.Empty;



                if (dsdata.Tables.Count > 0)
                {
                    for (int i = 0; i < dsdata.Tables[0].Rows.Count; i++)
                    {
                        string StrKeyData = dsdata.Tables[0].Rows[i]["Keydata"].ToString();
                        string strCloumnData = dsdata.Tables[0].Rows[i]["Wo_Column_Name"].ToString();
                        string value = string.Empty;


                        if (StrKeyData == "Inst_Task_Name")
                        {
                            value = strTask;
                            strBody = strBody.Replace(strCloumnData, " " + strTask);
                        }
                        else if (StrKeyData == "Inst_Comand_Mobile_details")
                        {
                            value = strTaskComments;
                            strBody = strBody.Replace(strCloumnData, " " + value);
                        }
                        else if (StrKeyData == "dueDate")
                        {
                            if (model.dueDate != null)
                            {
                                value = model.dueDate.Value.ToString("MM/dd/yyyy");

                            }
                            strBody = strBody.Replace(strCloumnData, " " + value);
                        }
                        else if (StrKeyData == "Document")
                        {
                            value = strDocument;
                            strBody = strBody.Replace(strCloumnData, " " + value);
                        }
                        else
                        {
                            if (dsdata.Tables.Count > 2 && dsdata.Tables[2].Rows.Count > 0)
                            {
                                value = dsdata.Tables[2].Rows[0][StrKeyData].ToString();
                                strBody = strBody.Replace(strCloumnData, " " + value);
                            }
                        }

                        strSubject = strSubject.Replace(strCloumnData, " " + value);


                    }
                    strBody = strBody + "<br/>" + "New  Message " + "<br/>";
                    email_Message_DTO.Subject = strSubject;
                    email_Message_DTO.Body = strBody;

                }




            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return email_Message_DTO;
        }

    }
}