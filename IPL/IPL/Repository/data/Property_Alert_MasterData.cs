﻿using Avigma.Repository.Lib;
using IPLApp.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class Property_Alert_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        private List<dynamic> CreateUpdatePropertyAlertMaster(Property_Alert_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();
            Property_Alert_Master property_Alert_Master = new Property_Alert_Master();
            string insertProcedure = "[CreateUpdate_Property_Alert_Master]";
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@PA_PkeyID", 1 + "#bigint#" + model.PA_PkeyID);
                input_parameters.Add("@PA_Name", 1 + "#varchar#" + model.PA_Name);
                input_parameters.Add("@PA_IsActive", 1 + "#bit#" + model.PA_IsActive);
                input_parameters.Add("@PA_IsDelete", 1 + "#bit#" + model.PA_IsDelete);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@PA_Pkey_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters); ;
                if (objData[1] == 0)
                {
                    property_Alert_Master.PA_PkeyID = "0";
                    property_Alert_Master.Status = "0";
                    property_Alert_Master.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    property_Alert_Master.PA_PkeyID = Convert.ToString(objData[0]);
                    property_Alert_Master.Status = Convert.ToString(objData[1]);


                }
                objcltData.Add(property_Alert_Master);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;



        }


        private DataSet GetPropertyAlertMaster(Property_Alert_MasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Property_Alert_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@PA_PkeyID", 1 + "#bigint#" + model.PA_PkeyID);
                input_parameters.Add("@WhereClause", 1 + "#varchar#" + model.WhereClause);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> CreateUpdatePropertyAlertMasterDetails(Property_Alert_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                objData = CreateUpdatePropertyAlertMaster(model);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }
            return objData;
        }

        public List<dynamic> GetPropertyAlertMasterDetails(Property_Alert_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                string wherecondition = string.Empty;
                if (model.Type == 3 && !string.IsNullOrEmpty(model.FilterData))
                {
                    var Data = JsonConvert.DeserializeObject<Property_Alert_MasterDTO>(model.FilterData);
                    if (!string.IsNullOrEmpty(Data.PA_Name))
                    {
                        wherecondition = " And pa.PA_Name LIKE '%" + Data.PA_Name + "%'";
                    }
                    if (Data.PA_IsActive == true)
                    {
                        wherecondition = wherecondition + "  And pa.PA_IsActive =  1 ";
                    }
                    if (Data.PA_IsActive == false)
                    {
                        wherecondition = wherecondition + "  And pa.PA_IsActive =  0";
                    }
                    model.WhereClause = wherecondition;
                }

                DataSet ds = GetPropertyAlertMaster(model);

                if (ds.Tables.Count > 0)
                {
                    for (int i = 0; i < ds.Tables.Count; i++)
                    {
                        objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                    }

                }
                //objDynamic.Add(Get_details);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }

            return objDynamic;
        }
    }
}