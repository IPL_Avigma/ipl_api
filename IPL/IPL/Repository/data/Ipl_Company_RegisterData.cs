﻿using Avigma.Repository.Lib;
using Firebase.Database;
using Firebase.Database.Query;
using IPL.Models;
using IPLApp.Models;
using IPLApp.Repository.data;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace IPL.Repository.data
{
    public class Ipl_Company_RegisterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        UserMasterData userMasterData = new UserMasterData();
        public async Task<List<dynamic>> Register_IPL_Company_Data(RegisterCompanyDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();
            string insertProcedure = "[CreateRegister_IPL_Company_Master]";
            IPL_Company_Master iPL_Company_Master = new IPL_Company_Master();
            UserMaster userMaster = new UserMaster();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@IPL_Company_PkeyId", 1 + "#bigint#" + model.IPL_Company_PkeyId);
                input_parameters.Add("@IPL_Company_Name", 1 + "#nvarchar#" + model.IPL_Company_Name);
                input_parameters.Add("@IPL_Company_Address", 1 + "#nvarchar#" + model.IPL_Company_Address);
                input_parameters.Add("@IPL_Company_Mobile", 1 + "#nvarchar#" + model.IPL_Company_Mobile);
                input_parameters.Add("@IPL_Company_City", 1 + "#nvarchar#" + model.IPL_Company_City);
                input_parameters.Add("@IPL_Company_State", 1 + "#nvarchar#" + model.IPL_Company_State);
                input_parameters.Add("@IPL_Company_PinCode", 1 + "#bigint#" + model.IPL_Company_PinCode);
                input_parameters.Add("@IPL_Company_IsActive", 1 + "#bit#" + model.IPL_Company_IsActive);
                input_parameters.Add("@IPL_Company_IsDelete", 1 + "#bit#" + model.IPL_Company_IsDelete);
                input_parameters.Add("@IPL_Company_County", 1 + "#varchar#" + model.IPL_Company_County);
                input_parameters.Add("@IPL_Contact_Name", 1 + "#varchar#" + model.IPL_Contact_Name);
                input_parameters.Add("@IPL_Company_Email", 1 + "#varchar#" + model.IPL_Company_Email);
                input_parameters.Add("@IPL_Company_Company_Link", 1 + "#varchar#" + model.IPL_Company_Company_Link);
                input_parameters.Add("@IPL_Company_ID", 1 + "#varchar#" + model.IPL_Company_ID);
                input_parameters.Add("@IPL_Company_Phone", 1 + "#varchar#" + model.IPL_Company_Phone);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                input_parameters.Add("@User_pkeyID", 1 + "#bigint#" + model.User_pkeyID);
                input_parameters.Add("@User_FirstName", 1 + "#nvarchar#" + model.User_FirstName);
                input_parameters.Add("@User_LastName", 1 + "#nvarchar#" + model.User_LastName);
                input_parameters.Add("@User_CompanyName", 1 + "#nvarchar#" + model.User_CompanyName);
                input_parameters.Add("@User_Address", 1 + "#nvarchar#" + model.User_Address);
                input_parameters.Add("@User_City", 1 + "#nvarchar#" + model.User_City);
                input_parameters.Add("@User_State", 1 + "#int#" + model.User_State);
                input_parameters.Add("@User_Zip", 1 + "#bigint#" + model.User_Zip);
                input_parameters.Add("@User_CellNumber", 1 + "#nvarchar#" + model.User_CellNumber);
                input_parameters.Add("@User_LoginName", 1 + "#nvarchar#" + model.User_LoginName);
                input_parameters.Add("@User_Password", 1 + "#nvarchar#" + model.User_Password);
                input_parameters.Add("@User_Email", 1 + "#nvarchar#" + model.User_Email);
                input_parameters.Add("@User_IsActive", 1 + "#bit#" + model.User_IsActive);
                input_parameters.Add("@User_IsDelete", 1 + "#bit#" + model.User_IsDelete);
               

                input_parameters.Add("@IPL_Company_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@IPL_User_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    userMaster.User_pkeyID = "0";
                    userMaster.Status = "0";
                    userMaster.ErrorMessage = "Error while Saving in the Database";

                }
             
                else
                {
                        userMaster.User_pkeyID = Convert.ToString(objData[1]);
                        model.User_pkeyID = objData[1];

                }
                if (model.User_pkeyID > 0)
                {
                    LiveUserLocationData liveUserLocationData = new LiveUserLocationData();
                    WorkOrderMasterData workOrderMasterData = new WorkOrderMasterData();
                    FirebaseUserDTO firebaseUserDTO = new FirebaseUserDTO { UserID = model.User_pkeyID, Type = 1 };

                    var userDetail = liveUserLocationData.GetFirebaseUser(firebaseUserDTO);
                    if (userDetail != null && userDetail.LoggedUserDetail != null && userDetail.LoggedUserDetail.Count > 0)
                    {
                        userDetail.LoggedUserDetail[0].User_LoginName = userDetail.LoggedUserDetail[0].User_LoginName.Replace(@".", "");
                        FirebaseUserGroupDTO userGroup = new FirebaseUserGroupDTO();
                        userGroup.id = userDetail.LoggedUserDetail[0].User_LoginName;
                        userGroup.avatar = "http://gravatar.com/avatar/" + workOrderMasterData.GenerateMD5(userGroup.id) + "?d=identicon";
                        await AddFirebaseIPLCompanyUser(userGroup, userDetail.LoggedUserDetail[0].IPL_Company_ID, userDetail.LoggedUserDetail[0].User_LoginName);
                    }
                }

                UserMasterDTO userMasterDTO = new UserMasterDTO();
                userMasterDTO.User_pkeyID = model.User_pkeyID;
                userMasterDTO.User_Email = model.User_Email;
                userMasterDTO.User_LoginName = model.User_LoginName;
                userMasterDTO.User_Password = model.User_Password;
                userMasterDTO.User_FirstName = model.User_FirstName;
                userMasterDTO.User_LastName = model.User_LastName;
                userMasterDTO.User_Source = 3;
                var emaildata = userMasterData.GeneratePasswordLink(userMasterDTO, 1);

                objcltData.Add(userMaster);
                
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;



        }

        private DataSet GeRegisterStateMaster(UserStateDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_DropDown_register]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();
                input_parameters.Add("@IPL_StateID", 1 + "#bigint#" + model.IPL_StateID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }
        public List<dynamic> GetregisterStateDetails(UserStateDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GeRegisterStateMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<contractorMapDRD> stateDrdDetails =
                   (from item in myEnumerableFeaprd
                    select new contractorMapDRD
                    {
                        IPL_StateID = item.Field<Int64>("IPL_StateID"),
                        IPL_StateName = item.Field<String>("IPL_StateName"),


                    }).ToList();

                objDynamic.Add(stateDrdDetails);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        public List<dynamic> GetUserregisterCountyDetails(UserStateDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                model.Type = 2;

                DataSet ds = GeRegisterStateMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<UserStateCountDTO> stateDrdDetails =
                   (from item in myEnumerableFeaprd
                    select new UserStateCountDTO
                    {
                        ID = item.Field<Int64>("ID"),
                        COUNTY = item.Field<String>("COUNTY"),


                    }).ToList();

                objDynamic.Add(stateDrdDetails);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        public async Task AddFirebaseIPLCompanyUser(FirebaseUserGroupDTO firebaseUserGroupDTO, string iplCompany, string loginName)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(iplCompany) &&  !string.IsNullOrWhiteSpace(loginName) && firebaseUserGroupDTO != null)
                {
                    string firebaseUrl = System.Configuration.ConfigurationManager.AppSettings["FirebaseUrl"];

                    string jsonStr = JsonConvert.SerializeObject(firebaseUserGroupDTO);
                    var firebaseClient = new FirebaseClient(firebaseUrl);

                    var objData = await firebaseClient
                     .Child("users")
                     .Child(iplCompany)
                     .Child(loginName)
                     .OnceAsync<dynamic>();

                    if (objData.Count == 0)
                    {
                        await firebaseClient
                         .Child("users")
                         .Child(iplCompany)
                         .Child(loginName)
                         .PutAsync(jsonStr);
                    }
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
    }
}