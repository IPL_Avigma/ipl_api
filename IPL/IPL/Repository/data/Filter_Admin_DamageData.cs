﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class Filter_Admin_DamageData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        public List<dynamic> AddUpdate_Filter_Admin_Damage(Filter_Admin_Damage_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_Filter_Admin_Damage_Master]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Damage_Filter_PkeyID", 1 + "#bigint#" + model.Damage_Filter_PkeyID);
                input_parameters.Add("@Damage_Filter_DamageName", 1 + "#nvarchar#" + model.Damage_Filter_DamageName);
                input_parameters.Add("@Damage_Filter_DamageIntExt", 1 + "#nvarchar#" + model.Damage_Filter_DamageIntExt);
                input_parameters.Add("@Damage_Filter_DamageLocation", 1 + "#nvarchar#" + model.Damage_Filter_DamageLocation);
                input_parameters.Add("@Damage_Filter_DamageDesc", 1 + "#nvarchar#" + model.Damage_Filter_DamageDesc);
                input_parameters.Add("@Damage_Filter_DamageIsActive", 1 + "#bit#" + model.Damage_Filter_DamageIsActive);
                input_parameters.Add("@Damage_Filter_IsActive", 1 + "#bit#" + model.Damage_Filter_IsActive);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserId);
                input_parameters.Add("@Damage_Filter_PkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        private DataSet Get_Filter_Admin_Damage(Filter_Admin_Damage_MasterDTO model)
        {
            DataSet ds = null;
            try
            {

                string selectProcedure = "[Get_Filter_Admin_Damage_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Damage_Filter_PkeyID", 1 + "#bigint#" + model.Damage_Filter_PkeyID);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> Get_Filter_Admin_DamageDetails(Filter_Admin_Damage_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = Get_Filter_Admin_Damage(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Filter_Admin_Damage_MasterDTO> Forms_Master_Files =
                   (from item in myEnumerableFeaprd
                    select new Filter_Admin_Damage_MasterDTO
                    {
                        Damage_Filter_PkeyID = item.Field<Int64>("Damage_Filter_PkeyID"),
                        Damage_Filter_DamageName = item.Field<String>("Damage_Filter_DamageName"),
                        Damage_Filter_DamageIntExt = item.Field<String>("Damage_Filter_DamageIntExt"),
                        Damage_Filter_DamageLocation = item.Field<String>("Damage_Filter_DamageLocation"),
                        Damage_Filter_DamageDesc = item.Field<String>("Damage_Filter_DamageDesc"),
                        Damage_Filter_DamageIsActive = item.Field<Boolean?>("Damage_Filter_DamageIsActive")

                    }).ToList();

                objDynamic.Add(Forms_Master_Files);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}