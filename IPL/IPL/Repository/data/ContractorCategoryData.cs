﻿using Avigma.Repository.Lib;
using IPL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class ContractorCategoryData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public List<dynamic> AddContractorCategoryData(ContractorCategoryDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objAddData = new List<dynamic>();
            string insertProcedure = "[CreateUpdateContractorCategoryMaster]";
            ContractorCategory contractorCategory = new ContractorCategory();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Con_Cat_PkeyID", 1 + "#bigint#" + model.Con_Cat_PkeyID);
                input_parameters.Add("@Con_Cat_Name", 1 + "#nvarchar#" + model.Con_Cat_Name);
                input_parameters.Add("@Con_Cat_Back_Color", 1 + "#nvarchar#" + model.Con_Cat_Back_Color);
                input_parameters.Add("@Con_Cat_Icon", 1 + "#nvarchar#" + model.Con_Cat_Icon);
                input_parameters.Add("@Con_Cat_IsActive", 1 + "#bit#" + model.Con_Cat_IsActive);
                input_parameters.Add("@Con_Cat_IsDelete", 1 + "#bit#" + model.Con_Cat_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Con_Cat_PkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    contractorCategory.Con_Cat_PkeyID = "0";
                    contractorCategory.Status = "0";
                    contractorCategory.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    contractorCategory.Con_Cat_PkeyID = Convert.ToString(objData[0]);
                    contractorCategory.Status = Convert.ToString(objData[1]);


                }
                objAddData.Add(contractorCategory);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objAddData;



        }

        private DataSet GetContractorategoryMaster(ContractorCategoryDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_ContractorCategoryMaster]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Con_Cat_PkeyID", 1 + "#bigint#" + model.Con_Cat_PkeyID);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@WhereClause", 1 + "#varchar#" + model.WhereClause);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetContractorCategoryDetails(ContractorCategoryDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetContractorategoryMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<ContractorCategoryDTO> ContractorCatDetails =
                   (from item in myEnumerableFeaprd
                    select new ContractorCategoryDTO
                    {
                        Con_Cat_PkeyID = item.Field<Int64>("Con_Cat_PkeyID"),
                        Con_Cat_Name = item.Field<String>("Con_Cat_Name"),
                        Con_Cat_Back_Color = item.Field<String>("Con_Cat_Back_Color"),
                        Con_Cat_Icon = item.Field<String>("Con_Cat_Icon"),
                        Con_Cat_IsActive = item.Field<Boolean?>("Con_Cat_IsActive"),
                        Con_Cat_IsDeleteAllow = item.Field<Boolean?>("Con_Cat_IsDeleteAllow"),
                        Con_Cat_CreatedBy = item.Field<String>("Con_Cat_CreatedBy"),
                        Con_Cat_ModifiedBy = item.Field<String>("Con_Cat_ModifiedBy"),
                    }).ToList();

                objDynamic.Add(ContractorCatDetails);
                if (model.Type == 1)
                {
                    if (ds.Tables.Count > 1)
                    {
                        var myEnumerableFeaprd1 = ds.Tables[1].AsEnumerable();
                        List<Filter_Admin_Contractor_Cat_MasterDTO> FilterGroup =
                           (from item in myEnumerableFeaprd1
                            select new Filter_Admin_Contractor_Cat_MasterDTO
                            {
                                ConCat_Filter_PkeyId = item.Field<Int64>("ConCat_Filter_PkeyId"),
                                ConCat_Filter_Name = item.Field<String>("ConCat_Filter_Name"),
                                ConCat_Filter_CatIsActive = item.Field<Boolean?>("ConCat_Filter_CatIsActive"),
                                ConCat_Filter_CreatedBy = item.Field<String>("ConCat_Filter_CreatedBy"),
                                ConCat_Filter_ModifiedBy = item.Field<String>("ConCat_Filter_ModifiedBy"),
                            }).ToList();

                        objDynamic.Add(FilterGroup);
                    }
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        public List<dynamic> GetContractorCategoryFilterDetails(ContractorCategoryDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();


            string wherecondition = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(model.FilterData))
                {
                    var Data = JsonConvert.DeserializeObject<ContractorCategoryDTO>(model.FilterData);
                    if (!string.IsNullOrEmpty(Data.Con_Cat_Name))
                    {
                        wherecondition = " And Con_Cat_Name LIKE '%" + Data.Con_Cat_Name + "%'";
                    }


                    if (Data.Con_Cat_IsActive == true)
                    {
                        wherecondition = wherecondition + "  And Con_Cat_IsActive =  '1'";
                    }
                    if (Data.Con_Cat_IsActive == false)
                    {
                        wherecondition = wherecondition + "  And Con_Cat_IsActive =  '0'";
                    }
                }
                


                ContractorCategoryDTO contractorCategoryDTO = new ContractorCategoryDTO();

                contractorCategoryDTO.WhereClause = wherecondition;
                contractorCategoryDTO.Type = 3;
                contractorCategoryDTO.UserID = model.UserID;
                DataSet ds = GetContractorategoryMaster(contractorCategoryDTO);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<ContractorCategoryDTO> ContractorCatfilter =
                   (from item in myEnumerableFeaprd
                    select new ContractorCategoryDTO
                    {
                        Con_Cat_PkeyID = item.Field<Int64>("Con_Cat_PkeyID"),
                        Con_Cat_Name = item.Field<String>("Con_Cat_Name"),
                        Con_Cat_Back_Color = item.Field<String>("Con_Cat_Back_Color"),
                        Con_Cat_Icon = item.Field<String>("Con_Cat_Icon"),
                        Con_Cat_IsActive = item.Field<Boolean?>("Con_Cat_IsActive"),
                        Con_Cat_CreatedBy = item.Field<String>("Con_Cat_CreatedBy"),
                        Con_Cat_ModifiedBy = item.Field<String>("Con_Cat_ModifiedBy"),

                    }).ToList();

                objDynamic.Add(ContractorCatfilter);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }

            return objDynamic;
        }
    }
}