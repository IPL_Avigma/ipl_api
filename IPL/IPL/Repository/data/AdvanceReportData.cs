﻿using Avigma.Repository.Lib;
using IPL.Models;
using IPLApp.Models;
using Newtonsoft.Json;
using NReco.PdfGenerator;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace IPL.Repository.data
{
    public class AdvanceReportData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        WorkOrder_Column_Data workOrder_Column_Data = new WorkOrder_Column_Data();
        StatusMasterDRD statusMasterDRD = new StatusMasterDRD();
        Report_WO_Filter_MasterData report_WO_Filter_MasterData = new Report_WO_Filter_MasterData();
        private DataSet GetReportTypeMaster(ReportType_MasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_ReportType_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@ReportType_PkeyId", 1 + "#bigint#" + model.ReportType_PkeyId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ds;
        }
        public List<dynamic> GetReportTypeMasterDaata(ReportType_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetReportTypeMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<ReportType_MasterDTO> reportType_MasterDTO =
                   (from item in myEnumerableFeaprd
                    select new ReportType_MasterDTO
                    {
                        ReportType_PkeyId = item.Field<Int64>("ReportType_PkeyId"),
                        ReportType_Name = item.Field<String>("ReportType_Name"),
                        ReportType_GroupById = item.Field<Int64?>("ReportType_GroupById"),
                        ReportType_IsActive = item.Field<Boolean?>("ReportType_IsActive"),

                    }).ToList();
                objDynamic.Add(reportType_MasterDTO);

                // For Column
                WorkOrder_Column_DTO workOrder_Column_DTO = new WorkOrder_Column_DTO();
                workOrder_Column_DTO.UserID = model.UserID.GetValueOrDefault(0);
                objDynamic.Add(workOrder_Column_Data.GetReportWorkOrderActionData(workOrder_Column_DTO));

                // For Status
                StatusDetailsDTO statusDetailsDTO = new StatusDetailsDTO();
                statusDetailsDTO.Status_ID = 0;
                statusDetailsDTO.Type = 2;
                statusDetailsDTO.UserID = model.UserID.GetValueOrDefault(0);
                objDynamic.Add(statusMasterDRD.GetStatusMasterDetails(statusDetailsDTO));

                // For WO Filter
                Report_WO_Filter_MasterDTO report_WO_Filter_MasterDTO = new Report_WO_Filter_MasterDTO();
                report_WO_Filter_MasterDTO.UserID = model.UserID.GetValueOrDefault(0);
                report_WO_Filter_MasterDTO.Type = 1;
                objDynamic.Add(report_WO_Filter_MasterData.GetReportWOFilterMasterData(report_WO_Filter_MasterDTO));
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objDynamic;
        }
        public List<dynamic> GetAdvanceReportDetails(AdvanceReportMasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            string wherecondition = string.Empty;
            var Data = JsonConvert.DeserializeObject<AdvanceReportMasterDTO>(model.whereClause);

            try
            {
                if (model.ReportTypeId == 1)
                {
                    // For WO Filter
                    Report_WO_Filter_ChildDTO report_WO_Filter_ChildDTO = new Report_WO_Filter_ChildDTO();
                    report_WO_Filter_ChildDTO.UserID = model.UserId.GetValueOrDefault(0);
                    report_WO_Filter_ChildDTO.Type = 3;
                    report_WO_Filter_ChildDTO.Report_WO_Filter_Ch_Master_FKeyId = model.WoFilterId;
                    var filterObjList = report_WO_Filter_MasterData.GetReportWOFilterChildList(report_WO_Filter_ChildDTO);
                    foreach (var filterObj in filterObjList)
                    {
                        if(filterObj.Report_WO_Filter_Ch_FeildId == 3 && filterObj.Report_WO_Filter_Ch_FeildDateValue.HasValue) //Due Date 
                        {
                            wherecondition = wherecondition != null ? wherecondition + " And CAST(work.dueDate as date) " + filterObj.Report_WO_Filter_Ch_FeildOperator + " CONVERT(date,'" + filterObj.Report_WO_Filter_Ch_FeildDateValue.Value.ToString("yyyy-MM-dd") + "')" : " And CAST(work.dueDate as date) " + filterObj.Report_WO_Filter_Ch_FeildOperator + " CONVERT(date,'" + filterObj.Report_WO_Filter_Ch_FeildDateValue.Value.ToString("yyyy-MM-dd") + "')";
                        }
                        else if(filterObj.Report_WO_Filter_Ch_FeildId == 5 && filterObj.Report_WO_Filter_Ch_FeildDateValue.HasValue) //Estimated Complete Date
                        {
                            wherecondition = wherecondition != null ? wherecondition + " And CAST(work.EstimatedDate as date) " + filterObj.Report_WO_Filter_Ch_FeildOperator + " CONVERT(date,'" + filterObj.Report_WO_Filter_Ch_FeildDateValue.Value.ToString("yyyy-MM-dd") + "')" : " And CAST(work.EstimatedDate as date) " + filterObj.Report_WO_Filter_Ch_FeildOperator + " CONVERT(date,'" + filterObj.Report_WO_Filter_Ch_FeildDateValue.Value.ToString("yyyy-MM-dd") + "')";
                        }
                        else if (filterObj.Report_WO_Filter_Ch_FeildId == 7 && filterObj.Report_WO_Filter_Ch_FeildDateValue.HasValue) //Client Due Date
                        {
                            wherecondition = wherecondition != null ? wherecondition + " And CAST(work.clientDueDate as date) " + filterObj.Report_WO_Filter_Ch_FeildOperator + " CONVERT(date,'" + filterObj.Report_WO_Filter_Ch_FeildDateValue.Value.ToString("yyyy-MM-dd") + "')" : " And CAST(work.clientDueDate as date) " + filterObj.Report_WO_Filter_Ch_FeildOperator + " CONVERT(date,'" + filterObj.Report_WO_Filter_Ch_FeildDateValue.Value.ToString("yyyy-MM-dd") + "')";
                        }
                        else if(filterObj.Report_WO_Filter_Ch_FeildId == 10 && filterObj.Report_WO_Filter_Ch_FeildDateValue.HasValue) //Sent to Client Date
                        {
                            wherecondition = wherecondition != null ? wherecondition + " And CAST(work.SentToClient_date as date) " + filterObj.Report_WO_Filter_Ch_FeildOperator + " CONVERT(date,'" + filterObj.Report_WO_Filter_Ch_FeildDateValue.Value.ToString("yyyy-MM-dd") + "')" : " And CAST(work.SentToClient_date as date) " + filterObj.Report_WO_Filter_Ch_FeildOperator + " CONVERT(date,'" + filterObj.Report_WO_Filter_Ch_FeildDateValue.Value.ToString("yyyy-MM-dd") + "')";
                        }
                        else if (filterObj.Report_WO_Filter_Ch_FeildId == 13 && filterObj.Report_WO_Filter_Ch_FeildDateValue.HasValue) //Cancel Date
                        {
                            wherecondition = wherecondition != null ? wherecondition + " And CAST(work.Cancel_Date as date) " + filterObj.Report_WO_Filter_Ch_FeildOperator + " CONVERT(date,'" + filterObj.Report_WO_Filter_Ch_FeildDateValue.Value.ToString("yyyy-MM-dd") + "')" : " And CAST(work.Cancel_Date as date) " + filterObj.Report_WO_Filter_Ch_FeildOperator + " CONVERT(date,'" + filterObj.Report_WO_Filter_Ch_FeildDateValue.Value.ToString("yyyy-MM-dd") + "')";
                        }
                        else if(filterObj.Report_WO_Filter_Ch_FeildId == 18 && filterObj.Report_WO_Filter_Ch_FeildDateValue.HasValue) //Created Date
                        {
                            wherecondition = wherecondition != null ? wherecondition + " And CAST(work.DateCreated as date) " + filterObj.Report_WO_Filter_Ch_FeildOperator + " CONVERT(date,'" + filterObj.Report_WO_Filter_Ch_FeildDateValue.Value.ToString("yyyy-MM-dd") + "')" : " And CAST(work.DateCreated as date) " + filterObj.Report_WO_Filter_Ch_FeildOperator + " CONVERT(date,'" + filterObj.Report_WO_Filter_Ch_FeildDateValue.Value.ToString("yyyy-MM-dd") + "')";
                        }
                        else if(filterObj.Report_WO_Filter_Ch_FeildId == 2 && !string.IsNullOrWhiteSpace(filterObj.Report_WO_Filter_Ch_FeildValue)) // status
                        {
                            if (filterObj.Report_WO_Filter_Ch_FeildOperator == "Contains" || filterObj.Report_WO_Filter_Ch_FeildOperator == "Does not contain" || filterObj.Report_WO_Filter_Ch_FeildOperator == "Starts with" || filterObj.Report_WO_Filter_Ch_FeildOperator == "Ends with")
                            {


                                switch (filterObj.Report_WO_Filter_Ch_FeildOperator)
                                {
                                    case "Contains":
                                        {
                                            wherecondition = wherecondition != null ? wherecondition + " And sts.Status_Name Like % '" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'" : " And sts.Status_Name Like % '" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'";
                                            break;
                                        }
                                    case "Does not contain":
                                        {
                                            wherecondition = wherecondition != null ? wherecondition + " And sts.Status_Name Not Like % '" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'" : " And sts.Status_Name Not Like % '" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'";
                                            break;
                                        }
                                    case "Starts with":
                                        {
                                            wherecondition = wherecondition != null ? wherecondition + " And sts.Status_Name Like % '" + filterObj.Report_WO_Filter_Ch_FeildValue + "'" : " And sts.Status_Name Like % '" + filterObj.Report_WO_Filter_Ch_FeildValue + "'";
                                            break;
                                        }
                                    case "Ends with":
                                        {
                                            wherecondition = wherecondition != null ? wherecondition + " And sts.Status_Name Like '" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'" : " And sts.Status_Name Like '" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'";
                                            break;
                                        }
                                }
                            }
                            else
                            {
                                wherecondition = wherecondition != null ? wherecondition + " And sts.Status_Name" + filterObj.Report_WO_Filter_Ch_FeildOperator + "'" + filterObj.Report_WO_Filter_Ch_FeildValue + "'" : " And sts.Status_Name" + filterObj.Report_WO_Filter_Ch_FeildOperator + "'" + filterObj.Report_WO_Filter_Ch_FeildValue + "'";
                            }
                        }
                        else if (filterObj.Report_WO_Filter_Ch_FeildId == 8 && !string.IsNullOrWhiteSpace(filterObj.Report_WO_Filter_Ch_FeildValue)) // Client
                        {
                            if (filterObj.Report_WO_Filter_Ch_FeildOperator == "Contains" || filterObj.Report_WO_Filter_Ch_FeildOperator == "Does not contain" || filterObj.Report_WO_Filter_Ch_FeildOperator == "Starts with" || filterObj.Report_WO_Filter_Ch_FeildOperator == "Ends with")
                            {


                                switch (filterObj.Report_WO_Filter_Ch_FeildOperator)
                                {
                                    case "Contains":
                                        {
                                            wherecondition = wherecondition != null ? wherecondition + " And ccm.Client_Company_Name Like % '" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'" : " And ccm.Client_Company_Name Like % '" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'";
                                            break;
                                        }
                                    case "Does not contain":
                                        {
                                            wherecondition = wherecondition != null ? wherecondition + " And ccm.Client_Company_Name Not Like % '" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'" : " And ccm.Client_Company_Name  Not Like % '" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'";
                                            break;
                                        }
                                    case "Starts with":
                                        {
                                            wherecondition = wherecondition != null ? wherecondition + " And ccm.Client_Company_Name Like % '" + filterObj.Report_WO_Filter_Ch_FeildValue + "'" : " And ccm.Client_Company_Name Like % '" + filterObj.Report_WO_Filter_Ch_FeildValue + "'";
                                            break;
                                        }
                                    case "Ends with":
                                        {
                                            wherecondition = wherecondition != null ? wherecondition + " And ccm.Client_Company_Name Like '" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'" : " And ccm.Client_Company_Name Like '" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'";
                                            break;
                                        }
                                }
                            }
                            else
                            {
                                wherecondition = wherecondition != null ? wherecondition + " And ccm.Client_Company_Name" + filterObj.Report_WO_Filter_Ch_FeildOperator + "'" + filterObj.Report_WO_Filter_Ch_FeildValue + "'" : " And ccm.Client_Company_Name" + filterObj.Report_WO_Filter_Ch_FeildOperator + "'" + filterObj.Report_WO_Filter_Ch_FeildValue + "'";

                            }
                        }
                        else if(filterObj.Report_WO_Filter_Ch_FeildId == 29 && !string.IsNullOrWhiteSpace(filterObj.Report_WO_Filter_Ch_FeildValue)) // State
                        {
                            if (filterObj.Report_WO_Filter_Ch_FeildOperator == "Contains" || filterObj.Report_WO_Filter_Ch_FeildOperator == "Does not contain" || filterObj.Report_WO_Filter_Ch_FeildOperator == "Starts with" || filterObj.Report_WO_Filter_Ch_FeildOperator == "Ends with")
                            {


                                switch (filterObj.Report_WO_Filter_Ch_FeildOperator)
                                {
                                    case "Contains":
                                        {
                                            wherecondition = wherecondition != null ? wherecondition + " And st.IPL_StateName Like  '%" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'" : " And st.IPL_StateName Like  '%" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'";
                                            break;
                                        }
                                    case "Does not contain":
                                        {
                                            wherecondition = wherecondition != null ? wherecondition + " And st.IPL_StateName  Not Like  '%" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'" : " And st.IPL_StateName  Not Like  '%" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'";
                                            break;
                                        }
                                    case "Starts with":
                                        {

                                            wherecondition = wherecondition != null ? wherecondition + " And st.IPL_StateName Like  '%" + filterObj.Report_WO_Filter_Ch_FeildValue + "'" : " And st.IPL_StateName Like  '%" + filterObj.Report_WO_Filter_Ch_FeildValue + "'";
                                            break;
                                        }
                                    case "Ends with":
                                        {
                                            wherecondition = wherecondition != null ? wherecondition + " And st.IPL_StateName Like '" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'" : " And st.IPL_StateName Like '" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'";

                                            break;
                                        }
                                }
                            }
                            else
                            {
                                wherecondition = wherecondition != null ? wherecondition + " And st.IPL_StateName" + filterObj.Report_WO_Filter_Ch_FeildOperator + "'" + filterObj.Report_WO_Filter_Ch_FeildValue + "'" : " And st.IPL_StateName" + filterObj.Report_WO_Filter_Ch_FeildOperator + "'" + filterObj.Report_WO_Filter_Ch_FeildValue + "'";

                            }
                        }
                        else if(filterObj.Report_WO_Filter_Ch_FeildId == 126 && !string.IsNullOrWhiteSpace(filterObj.Report_WO_Filter_Ch_FeildValue)) // work type
                        {
                            if (filterObj.Report_WO_Filter_Ch_FeildOperator == "Contains" || filterObj.Report_WO_Filter_Ch_FeildOperator == "Does not contain" || filterObj.Report_WO_Filter_Ch_FeildOperator == "Starts with" || filterObj.Report_WO_Filter_Ch_FeildOperator == "Ends with")
                            {


                                switch (filterObj.Report_WO_Filter_Ch_FeildOperator)
                                {
                                    case "Contains":
                                        {
                                            wherecondition = wherecondition != null ? wherecondition + " And wt.WT_WorkType Like % '" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'" : " And wt.WT_WorkType Like %'" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'";

                                            break;
                                        }
                                    case "Does not contain":
                                        {
                                            wherecondition = wherecondition != null ? wherecondition + " And wt.WT_WorkType Not Like % '" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'" : " And wt.WT_WorkType Not Like %'" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'";
                                            break;
                                        }
                                    case "Starts with":
                                        {
                                            wherecondition = wherecondition != null ? wherecondition + " And wt.WT_WorkType Like % '" + filterObj.Report_WO_Filter_Ch_FeildValue + "'" : " And wt.WT_WorkType Like % '" + filterObj.Report_WO_Filter_Ch_FeildValue + "'";
                                            break;
                                        }
                                    case "Ends with":
                                        {
                                            wherecondition = wherecondition != null ? wherecondition + " And wt.WT_WorkType Like  '" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'" : " And wt.WT_WorkType Like '" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'";

                                            break;
                                        }
                                }
                            }
                            else
                            {
                                wherecondition = wherecondition != null ? wherecondition + " And wt.WT_WorkType" + filterObj.Report_WO_Filter_Ch_FeildOperator + "'" + filterObj.Report_WO_Filter_Ch_FeildValue + "'" : " And wt.WT_WorkType" + filterObj.Report_WO_Filter_Ch_FeildOperator + "'" + filterObj.Report_WO_Filter_Ch_FeildValue + "'";


                            }
                        }
                        else if(filterObj.Report_WO_Filter_Ch_FeildId == 127 && !string.IsNullOrWhiteSpace(filterObj.Report_WO_Filter_Ch_FeildValue)) // Contractor
                        {
                            if (filterObj.Report_WO_Filter_Ch_FeildOperator == "Contains" || filterObj.Report_WO_Filter_Ch_FeildOperator == "Does not contain" || filterObj.Report_WO_Filter_Ch_FeildOperator == "Starts with" || filterObj.Report_WO_Filter_Ch_FeildOperator == "Ends with")
                            {


                                switch (filterObj.Report_WO_Filter_Ch_FeildOperator)
                                {
                                    case "Contains":
                                        {
                                            wherecondition = wherecondition != null ? wherecondition + " And isnull(umc.User_FirstName,'')+''+isnull(umc.User_LastName,'')  Like %'" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'" : " And isnull(umc.User_FirstName,'')+''+isnull(umc.User_LastName,'') Like %'" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'";

                                            break;
                                        }
                                    case "Does not contain":
                                        {
                                            wherecondition = wherecondition != null ? wherecondition + " And isnull(umc.User_FirstName,'')+''+isnull(umc.User_LastName,'') Not Like %'" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'" : " And isnull(umc.User_FirstName,'')+''+isnull(umc.User_LastName,'') Not Like %'" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'";

                                            break;
                                        }
                                    case "Starts with":
                                        {
                                            wherecondition = wherecondition != null ? wherecondition + " And isnull(umc.User_FirstName,'')+''+isnull(umc.User_LastName,'') Like %'" + filterObj.Report_WO_Filter_Ch_FeildValue + "'" : " And isnull(umc.User_FirstName,'')+''+isnull(umc.User_LastName,'') Like %'" + filterObj.Report_WO_Filter_Ch_FeildValue + "'";

                                            break;
                                        }
                                    case "Ends with":
                                        {
                                            wherecondition = wherecondition != null ? wherecondition + " And isnull(umc.User_FirstName,'')+''+isnull(umc.User_LastName,'') Like'" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'" : " And isnull(umc.User_FirstName,'')+''+isnull(umc.User_LastName,'') Like '" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'";

                                            break;
                                        }
                                }
                            }
                            else
                            {
                                wherecondition = wherecondition != null ? wherecondition + " And isnull(umc.User_FirstName,'')+''+isnull(umc.User_LastName,'')" + filterObj.Report_WO_Filter_Ch_FeildOperator + "'" + filterObj.Report_WO_Filter_Ch_FeildValue + "'" : " And isnull(umc.User_FirstName,'')+''+isnull(umc.User_LastName,'')" + filterObj.Report_WO_Filter_Ch_FeildOperator + "'" + filterObj.Report_WO_Filter_Ch_FeildValue + "'";


                            }
                        }
                        else if(filterObj.Report_WO_Filter_Ch_FeildId == 128 && !string.IsNullOrWhiteSpace(filterObj.Report_WO_Filter_Ch_FeildValue)) // Cordinator
                        {
                            if (filterObj.Report_WO_Filter_Ch_FeildOperator == "Contains" || filterObj.Report_WO_Filter_Ch_FeildOperator == "Does not contain" || filterObj.Report_WO_Filter_Ch_FeildOperator == "Starts with" || filterObj.Report_WO_Filter_Ch_FeildOperator == "Ends with")
                            {


                                switch (filterObj.Report_WO_Filter_Ch_FeildOperator)
                                {
                                    case "Contains":
                                        {
                                            wherecondition = wherecondition != null ? wherecondition + " And isnull(cor.User_FirstName,'')+''+isnull(cor.User_LastName,'')  Like %'" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'" : " And isnull(cor.User_FirstName,'')+''+isnull(cor.User_LastName,'') Like %'" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'";
                                            break;
                                        }
                                    case "Does not contain":
                                        {
                                            wherecondition = wherecondition != null ? wherecondition + " And isnull(cor.User_FirstName,'')+''+isnull(cor.User_LastName,'') Not Like %'" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'" : " And isnull(cor.User_FirstName,'')+''+isnull(cor.User_LastName,'') Not Like %'" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'";
                                            break;
                                        }
                                    case "Starts with":
                                        {
                                            wherecondition = wherecondition != null ? wherecondition + " And isnull(cor.User_FirstName,'')+''+isnull(cor.User_LastName,'') Like %'" + filterObj.Report_WO_Filter_Ch_FeildValue + "'" : " And isnull(cor.User_FirstName,'')+''+isnull(cor.User_LastName,'') Like %'" + filterObj.Report_WO_Filter_Ch_FeildValue + "'";
                                            break;
                                        }
                                    case "Ends with":
                                        {

                                            wherecondition = wherecondition != null ? wherecondition + " And isnull(cor.User_FirstName,'')+''+isnull(cor.User_LastName,'') Like'" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'" : " And isnull(cor.User_FirstName,'')+''+isnull(cor.User_LastName,'') Like '" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'";
                                            break;
                                        }
                                }
                            }
                            else
                            {
                                wherecondition = wherecondition != null ? wherecondition + " And isnull(cor.User_FirstName,'')+''+isnull(cor.User_LastName,'')" + filterObj.Report_WO_Filter_Ch_FeildOperator + "'" + filterObj.Report_WO_Filter_Ch_FeildValue + "'" : " And isnull(cor.User_FirstName,'')+''+isnull(cor.User_LastName,'')" + filterObj.Report_WO_Filter_Ch_FeildOperator + "'" + filterObj.Report_WO_Filter_Ch_FeildValue + "'";

                            }
                        }
                        else if(filterObj.Report_WO_Filter_Ch_FeildId == 129 && !string.IsNullOrWhiteSpace(filterObj.Report_WO_Filter_Ch_FeildValue)) // Processor
                        {
                            if (filterObj.Report_WO_Filter_Ch_FeildOperator == "Contains" || filterObj.Report_WO_Filter_Ch_FeildOperator == "Does not contain" || filterObj.Report_WO_Filter_Ch_FeildOperator == "Starts with" || filterObj.Report_WO_Filter_Ch_FeildOperator == "Ends with")
                            {


                                switch (filterObj.Report_WO_Filter_Ch_FeildOperator)
                                {
                                    case "Contains":
                                        {
                                            wherecondition = wherecondition != null ? wherecondition + " And isnull(pro.User_FirstName,'')+''+isnull(pro.User_LastName,'') Like %'" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'" : " And isnull(pro.User_FirstName,'')+''+isnull(pro.User_LastName,'') Like %'" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'";
                                            break;
                                        }
                                    case "Does not contain":
                                        {
                                            wherecondition = wherecondition != null ? wherecondition + " And isnull(pro.User_FirstName,'')+''+isnull(pro.User_LastName,'')  Not Like %'" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'" : " And isnull(pro.User_FirstName,'')+''+isnull(pro.User_LastName,'') Not  Like %'" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'";
                                            break;
                                        }
                                    case "Starts with":
                                        {
                                            wherecondition = wherecondition != null ? wherecondition + " And isnull(pro.User_FirstName,'')+''+isnull(pro.User_LastName,'') Like %'" + filterObj.Report_WO_Filter_Ch_FeildValue + "'" : " And isnull(pro.User_FirstName,'')+''+isnull(pro.User_LastName,'') Like %'" + filterObj.Report_WO_Filter_Ch_FeildValue + "'";
                                            break;
                                        }
                                    case "Ends with":
                                        {

                                            wherecondition = wherecondition != null ? wherecondition + " And isnull(pro.User_FirstName,'')+''+isnull(pro.User_LastName,'') Like '" + filterObj.Report_WO_Filter_Ch_FeildValue + "'%" : " And isnull(pro.User_FirstName,'')+''+isnull(pro.User_LastName,'') Like '" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'";
                                            break;
                                        }
                                }
                            }
                            else
                            {
                                wherecondition = wherecondition != null ? wherecondition + " And isnull(pro.User_FirstName,'')+''+isnull(pro.User_LastName,'')" + filterObj.Report_WO_Filter_Ch_FeildOperator + "'" + filterObj.Report_WO_Filter_Ch_FeildValue + "'" : " And isnull(pro.User_FirstName,'')+''+isnull(pro.User_LastName,'')" + filterObj.Report_WO_Filter_Ch_FeildOperator + "'" + filterObj.Report_WO_Filter_Ch_FeildValue + "'";
                            }
                        }
                        else if(filterObj.Report_WO_Filter_Ch_FeildId == 160 && !string.IsNullOrWhiteSpace(filterObj.Report_WO_Filter_Ch_FeildValue)) //Task
                        {
                            if (filterObj.Report_WO_Filter_Ch_FeildOperator == "Contains" || filterObj.Report_WO_Filter_Ch_FeildOperator == "Does not contain" || filterObj.Report_WO_Filter_Ch_FeildOperator == "Starts with" || filterObj.Report_WO_Filter_Ch_FeildOperator == "Ends with")
                            {


                                switch (filterObj.Report_WO_Filter_Ch_FeildOperator)
                                {
                                    case "Contains":
                                        {
                                            wherecondition = wherecondition != null ? wherecondition + " And task_master.Task_Name Like '%" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'" : " And task_master.Task_Name Like  '%" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'";
                                            break;
                                        }
                                    case "Does not contain":
                                        {
                                            wherecondition = wherecondition != null ? wherecondition + " And task_master.Task_Name Not Like '%" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'" : " And task_master.Task_Name Not Like  '%" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'";
                                            break;
                                        }
                                    case "Starts with":
                                        {
                                            wherecondition = wherecondition != null ? wherecondition + " And task_master.Task_Name Like '%" + filterObj.Report_WO_Filter_Ch_FeildValue + "'" : " And task_master.Task_Name Like  '%" + filterObj.Report_WO_Filter_Ch_FeildValue + "'";
                                            break;
                                        }
                                    case "Ends with":
                                        {
                                            wherecondition = wherecondition != null ? wherecondition + " And task_master.Task_Name Like '" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'" : " And task_master.Task_Name Like  '" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'";
                                            break;
                                        }
                                }
                            }
                            else
                            {
                                wherecondition = wherecondition != null ? wherecondition + " And task_master.Task_Name" + filterObj.Report_WO_Filter_Ch_FeildOperator + "'" + filterObj.Report_WO_Filter_Ch_FeildValue + "'" : " And task_master.Task_Name" + filterObj.Report_WO_Filter_Ch_FeildOperator + "'" + filterObj.Report_WO_Filter_Ch_FeildValue + "'";
                            }
                        }
                        else
                        {
                                wherecondition = wherecondition != null ? wherecondition + " And work." + filterObj.Report_WO_Filter_Ch_FeildName + filterObj.Report_WO_Filter_Ch_FeildOperator + "'" + filterObj.Report_WO_Filter_Ch_FeildValue + "'" : " And work." + filterObj.Report_WO_Filter_Ch_FeildName + filterObj.Report_WO_Filter_Ch_FeildOperator + "'" + filterObj.Report_WO_Filter_Ch_FeildValue + "'";                    
                        }                        
                    }
                    AdvanceReportMasterDTO reportMasterDTO = new AdvanceReportMasterDTO();

                    reportMasterDTO.Type = 2;
                    reportMasterDTO.whereClause = wherecondition;
                    reportMasterDTO.UserId = model.UserId;
                    DataSet ds = GetFilterReportData(reportMasterDTO);

                    if (ds != null && ds.Tables.Count > 0)
                    {
                        var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                        List<AdvanceFilterReportMasterDTO> ReportDetails =
                           (from item in myEnumerableFeaprd
                            select new AdvanceFilterReportMasterDTO
                            {
                                workOrder_ID = item.Field<Int64>("workOrder_ID"),
                                IPLNO = item.Field<String>("IPLNO"),
                                workOrderNumber = item.Field<String>("workOrderNumber"),
                                Contractor = item.Field<Int64?>("Contractor"),
                                address1 = item.Field<String>("address1"),
                                city = item.Field<String>("city"),
                                state = item.Field<Int64?>("state"),
                                zip = item.Field<Int64?>("zip"),
                                WorkType = item.Field<Int64?>("WorkType"),
                                dueDate = item.Field<DateTime?>("dueDate"),
                                Field_complete_date = item.Field<DateTime?>("Field_complete_date"),
                                SentToClient_date = item.Field<DateTime?>("SentToClient_date"),
                                OfficeApproved_date = item.Field<DateTime?>("OfficeApproved_date"),
                                EstimatedDate = item.Field<DateTime?>("EstimatedDate"),
                                clientDueDate = item.Field<DateTime?>("clientDueDate"),
                                Cancel_Date = item.Field<DateTime?>("Cancel_Date"),
                                DateCreated = item.Field<DateTime?>("DateCreated"),
                                Loan_Number = item.Field<String>("Loan_Number"),
                                Lotsize = item.Field<String>("Lotsize"),
                                Lock_Code = item.Field<String>("Lock_Code"),
                                status = item.Field<Int32?>("status"),

                                ContractorName = item.Field<String>("ContractorName"),
                                ProcessorName = item.Field<String>("ProcessorName"),
                                CordinatorName = item.Field<String>("CordinatorName"),


                                Client_Company_Name = item.Field<String>("Client_Company_Name"),
                                IPL_StateName = item.Field<String>("IPL_StateName"),
                                WT_WorkType = item.Field<String>("WT_WorkType"),
                                CategoryName = item.Field<String>("CategoryName"),
                                Status_Name = item.Field<String>("Status_Name"),

                            }).ToList();


                        objDynamic.Add(ReportDetails);
                    }
                }
         
             
                ///////////////
                ///
                if (model.ReportTypeId == 9)
                {

                    foreach (var filterObj in model.FilterData)
                    {
                        if (filterObj.Report_WO_Filter_Ch_FeildId == 3 ) //Due Date 
                        {
                            wherecondition = wherecondition != null ? wherecondition + " And CAST(work.dueDate as date) " + filterObj.Report_WO_Filter_Ch_FeildOperator + " CONVERT(date,'" + filterObj.Report_WO_Filter_Ch_FeildDateValue.Value.ToString("yyyy-MM-dd") + "')" : " And CAST(work.dueDate as date) " + filterObj.Report_WO_Filter_Ch_FeildOperator + " CONVERT(date,'" + filterObj.Report_WO_Filter_Ch_FeildDateValue.Value.ToString("yyyy-MM-dd") + "')";
                           
                        }
                        else if (filterObj.Report_WO_Filter_Ch_FeildId == 5 && filterObj.Report_WO_Filter_Ch_FeildDateValue.HasValue) //Estimated Complete Date
                        {
                            wherecondition = wherecondition != null ? wherecondition + " And CAST(work.EstimatedDate as date) " + filterObj.Report_WO_Filter_Ch_FeildOperator + " CONVERT(date,'" + filterObj.Report_WO_Filter_Ch_FeildDateValue.Value.ToString("yyyy-MM-dd") + "')" : " And CAST(work.EstimatedDate as date) " + filterObj.Report_WO_Filter_Ch_FeildOperator + " CONVERT(date,'" + filterObj.Report_WO_Filter_Ch_FeildDateValue.Value.ToString("yyyy-MM-dd") + "')";
                        }
                        else if (filterObj.Report_WO_Filter_Ch_FeildId == 7 && filterObj.Report_WO_Filter_Ch_FeildDateValue.HasValue) //Client Due Date
                        {
                            wherecondition = wherecondition != null ? wherecondition + " And CAST(work.clientDueDate as date) " + filterObj.Report_WO_Filter_Ch_FeildOperator + " CONVERT(date,'" + filterObj.Report_WO_Filter_Ch_FeildDateValue.Value.ToString("yyyy-MM-dd") + "')" : " And CAST(work.clientDueDate as date) " + filterObj.Report_WO_Filter_Ch_FeildOperator + " CONVERT(date,'" + filterObj.Report_WO_Filter_Ch_FeildDateValue.Value.ToString("yyyy-MM-dd") + "')";
                        }
                        else if (filterObj.Report_WO_Filter_Ch_FeildId == 10 && filterObj.Report_WO_Filter_Ch_FeildDateValue.HasValue) //Sent to Client Date
                        {
                            wherecondition = wherecondition != null ? wherecondition + " And CAST(work.SentToClient_date as date) " + filterObj.Report_WO_Filter_Ch_FeildOperator + " CONVERT(date,'" + filterObj.Report_WO_Filter_Ch_FeildDateValue.Value.ToString("yyyy-MM-dd") + "')" : " And CAST(work.SentToClient_date as date) " + filterObj.Report_WO_Filter_Ch_FeildOperator + " CONVERT(date,'" + filterObj.Report_WO_Filter_Ch_FeildDateValue.Value.ToString("yyyy-MM-dd") + "')";
                        }
                        else if (filterObj.Report_WO_Filter_Ch_FeildId == 13 && filterObj.Report_WO_Filter_Ch_FeildDateValue.HasValue) //Cancel Date
                        {
                            wherecondition = wherecondition != null ? wherecondition + " And CAST(work.Cancel_Date as date) " + filterObj.Report_WO_Filter_Ch_FeildOperator + " CONVERT(date,'" + filterObj.Report_WO_Filter_Ch_FeildDateValue.Value.ToString("yyyy-MM-dd") + "')" : " And CAST(work.Cancel_Date as date) " + filterObj.Report_WO_Filter_Ch_FeildOperator + " CONVERT(date,'" + filterObj.Report_WO_Filter_Ch_FeildDateValue.Value.ToString("yyyy-MM-dd") + "')";
                        }
                        else if (filterObj.Report_WO_Filter_Ch_FeildId == 18 && filterObj.Report_WO_Filter_Ch_FeildDateValue.HasValue) //Created Date
                        {
                            wherecondition = wherecondition != null ? wherecondition + " And CAST(work.DateCreated as date) " + filterObj.Report_WO_Filter_Ch_FeildOperator + " CONVERT(date,'" + filterObj.Report_WO_Filter_Ch_FeildDateValue.Value.ToString("yyyy-MM-dd") + "')" : " And CAST(work.DateCreated as date) " + filterObj.Report_WO_Filter_Ch_FeildOperator + " CONVERT(date,'" + filterObj.Report_WO_Filter_Ch_FeildDateValue.Value.ToString("yyyy-MM-dd") + "')";
                        }
                        else if (filterObj.Report_WO_Filter_Ch_FeildId == 2 && !string.IsNullOrWhiteSpace(filterObj.Report_WO_Filter_Ch_FeildValue))// status
                        {
                            if (filterObj.Report_WO_Filter_Ch_FeildOperator == "Contains" || filterObj.Report_WO_Filter_Ch_FeildOperator == "Does not contain" || filterObj.Report_WO_Filter_Ch_FeildOperator == "Starts with" || filterObj.Report_WO_Filter_Ch_FeildOperator == "Ends with")
                            {


                                switch (filterObj.Report_WO_Filter_Ch_FeildOperator)
                                {
                                    case "Contains":
                                        {
                                            wherecondition = wherecondition != null ? wherecondition + " And sts.Status_Name Like  '%" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'" : " And sts.Status_Name Like  '%" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'";
                                            break;
                                        }
                                    case "Does not contain":
                                        {
                                            wherecondition = wherecondition != null ? wherecondition + " And sts.Status_Name Not Like  '%" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'" : " And sts.Status_Name Not Like  '%" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'";
                                            break;
                                        }
                                    case "Starts with":
                                        {
                                            wherecondition = wherecondition != null ? wherecondition + " And sts.Status_Name Like  '%" + filterObj.Report_WO_Filter_Ch_FeildValue + "'" : " And sts.Status_Name Like  '%" + filterObj.Report_WO_Filter_Ch_FeildValue + "'";
                                            break;
                                        }
                                    case "Ends with":
                                        {
                                            wherecondition = wherecondition != null ? wherecondition + " And sts.Status_Name Like '" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'" : " And sts.Status_Name Like '" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'";
                                             break;
                                        }
                                }
                            }
                            else
                            {
                                wherecondition = wherecondition != null ? wherecondition + " And sts.Status_Name" + filterObj.Report_WO_Filter_Ch_FeildOperator + "'" + filterObj.Report_WO_Filter_Ch_FeildValue + "'" : " And sts.Status_Name" + filterObj.Report_WO_Filter_Ch_FeildOperator + "'" + filterObj.Report_WO_Filter_Ch_FeildValue + "'";
                             }
                           
                        }
                        else if (filterObj.Report_WO_Filter_Ch_FeildId == 8 && !string.IsNullOrWhiteSpace(filterObj.Report_WO_Filter_Ch_FeildValue)) // Client
                        {
                            if (filterObj.Report_WO_Filter_Ch_FeildOperator == "Contains" || filterObj.Report_WO_Filter_Ch_FeildOperator == "Does not contain" || filterObj.Report_WO_Filter_Ch_FeildOperator == "Starts with" || filterObj.Report_WO_Filter_Ch_FeildOperator == "Ends with")
                            {


                                switch (filterObj.Report_WO_Filter_Ch_FeildOperator)
                                {
                                    case "Contains":
                                        {
                                            wherecondition = wherecondition != null ? wherecondition + " And ccm.Client_Company_Name Like % '" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'" : " And ccm.Client_Company_Name Like % '" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'";
                                             break;
                                        }
                                    case "Does not contain":
                                        {
                                            wherecondition = wherecondition != null ? wherecondition + " And ccm.Client_Company_Name Not Like % '" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'" : " And ccm.Client_Company_Name  Not Like % '" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'";
                                            break;
                                        }
                                    case "Starts with":
                                        {
                                            wherecondition = wherecondition != null ? wherecondition + " And ccm.Client_Company_Name Like % '" + filterObj.Report_WO_Filter_Ch_FeildValue + "'" : " And ccm.Client_Company_Name Like % '" + filterObj.Report_WO_Filter_Ch_FeildValue + "'";
                                             break;
                                        }
                                    case "Ends with":
                                        {
                                            wherecondition = wherecondition != null ? wherecondition + " And ccm.Client_Company_Name Like '" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'" : " And ccm.Client_Company_Name Like '" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'";
                                            break;
                                        }
                                }
                            }
                            else
                            {
                                wherecondition = wherecondition != null ? wherecondition + " And ccm.Client_Company_Name" + filterObj.Report_WO_Filter_Ch_FeildOperator + "'" + filterObj.Report_WO_Filter_Ch_FeildValue + "'" : " And ccm.Client_Company_Name" + filterObj.Report_WO_Filter_Ch_FeildOperator + "'" + filterObj.Report_WO_Filter_Ch_FeildValue + "'";

                            }
                            
                        }
                        else if (filterObj.Report_WO_Filter_Ch_FeildId == 29 && !string.IsNullOrWhiteSpace(filterObj.Report_WO_Filter_Ch_FeildValue)) // State
                        {
                            if (filterObj.Report_WO_Filter_Ch_FeildOperator == "Contains" || filterObj.Report_WO_Filter_Ch_FeildOperator == "Does not contain" || filterObj.Report_WO_Filter_Ch_FeildOperator == "Starts with" || filterObj.Report_WO_Filter_Ch_FeildOperator == "Ends with")
                            {


                                switch (filterObj.Report_WO_Filter_Ch_FeildOperator)
                                {
                                    case "Contains":
                                        {
                                            wherecondition = wherecondition != null ? wherecondition + " And st.IPL_StateName Like '%" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'" : " And st.IPL_StateName Like  '%" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'";
                                             break;
                                        }
                                    case "Does not contain":
                                        {
                                            wherecondition = wherecondition != null ? wherecondition + " And st.IPL_StateName  Not Like  '%" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'" : " And st.IPL_StateName  Not Like  '%" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'";
                                           break;
                                        }
                                    case "Starts with":
                                        {
                                           
                                            wherecondition = wherecondition != null ? wherecondition + " And st.IPL_StateName Like '%" + filterObj.Report_WO_Filter_Ch_FeildValue + "'" : " And st.IPL_StateName Like  '%" + filterObj.Report_WO_Filter_Ch_FeildValue + "'";
                                            break;
                                        }
                                    case "Ends with":
                                        {
                                            wherecondition = wherecondition != null ? wherecondition + " And st.IPL_StateName Like '" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'" : " And st.IPL_StateName Like '" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'";

                                            break;
                                        }
                                }
                            }
                            else
                            {
                                wherecondition = wherecondition != null ? wherecondition + " And st.IPL_StateName" + filterObj.Report_WO_Filter_Ch_FeildOperator + "'" + filterObj.Report_WO_Filter_Ch_FeildValue + "'" : " And st.IPL_StateName" + filterObj.Report_WO_Filter_Ch_FeildOperator + "'" + filterObj.Report_WO_Filter_Ch_FeildValue + "'";
                             
                            }
                           
                        }
                        else if (filterObj.Report_WO_Filter_Ch_FeildId == 126 && !string.IsNullOrWhiteSpace(filterObj.Report_WO_Filter_Ch_FeildValue)) // work type
                        {
                            if (filterObj.Report_WO_Filter_Ch_FeildOperator == "Contains" || filterObj.Report_WO_Filter_Ch_FeildOperator == "Does not contain" || filterObj.Report_WO_Filter_Ch_FeildOperator == "Starts with" || filterObj.Report_WO_Filter_Ch_FeildOperator == "Ends with")
                            {


                                switch (filterObj.Report_WO_Filter_Ch_FeildOperator)
                                {
                                    case "Contains":
                                        {
                                            wherecondition = wherecondition != null ? wherecondition + " And wt.WT_WorkType Like  '%" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'" : " And wt.WT_WorkType Like '%" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'";

                                            break;
                                        }
                                    case "Does not contain":
                                        {
                                            wherecondition = wherecondition != null ? wherecondition + " And wt.WT_WorkType Not Like  '%" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'" : " And wt.WT_WorkType Not Like '%" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'";
                                            break;
                                        }
                                    case "Starts with":
                                        {
                                            wherecondition = wherecondition != null ? wherecondition + " And wt.WT_WorkType Like  '%" + filterObj.Report_WO_Filter_Ch_FeildValue + "'" : " And wt.WT_WorkType Like  '%" + filterObj.Report_WO_Filter_Ch_FeildValue + "'";
                                           break;
                                        }
                                    case "Ends with":
                                        {
                                            wherecondition = wherecondition != null ? wherecondition + " And wt.WT_WorkType Like  '" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'" : " And wt.WT_WorkType Like '" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'";

                                            break;
                                        }
                                }
                            }
                            else
                            {
                                wherecondition = wherecondition != null ? wherecondition + " And wt.WT_WorkType" + filterObj.Report_WO_Filter_Ch_FeildOperator + "'" + filterObj.Report_WO_Filter_Ch_FeildValue + "'" : " And wt.WT_WorkType" + filterObj.Report_WO_Filter_Ch_FeildOperator + "'" + filterObj.Report_WO_Filter_Ch_FeildValue + "'";


                            }
                           
                        }
                        else if (filterObj.Report_WO_Filter_Ch_FeildId == 127 && !string.IsNullOrWhiteSpace(filterObj.Report_WO_Filter_Ch_FeildValue)) // Contractor
                        {

                            if (filterObj.Report_WO_Filter_Ch_FeildOperator == "Contains" || filterObj.Report_WO_Filter_Ch_FeildOperator == "Does not contain" || filterObj.Report_WO_Filter_Ch_FeildOperator == "Starts with" || filterObj.Report_WO_Filter_Ch_FeildOperator == "Ends with")
                            {


                                switch (filterObj.Report_WO_Filter_Ch_FeildOperator)
                                {
                                    case "Contains":
                                        {
                                            wherecondition = wherecondition != null ? wherecondition + " And isnull(umc.User_FirstName,'')+''+isnull(umc.User_LastName,'')  Like '%" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'" : " And isnull(umc.User_FirstName,'')+''+isnull(umc.User_LastName,'') Like '%" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'";
                                            
                                            break;
                                        }
                                    case "Does not contain":
                                        {
                                            wherecondition = wherecondition != null ? wherecondition + " And isnull(umc.User_FirstName,'')+''+isnull(umc.User_LastName,'') Not Like '%" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'" : " And isnull(umc.User_FirstName,'')+''+isnull(umc.User_LastName,'') Not Like '%" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'";
                                           
                                            break;
                                        }
                                    case "Starts with":
                                        {
                                            wherecondition = wherecondition != null ? wherecondition + " And isnull(umc.User_FirstName,'')+''+isnull(umc.User_LastName,'') Like '%" + filterObj.Report_WO_Filter_Ch_FeildValue + "'" : " And isnull(umc.User_FirstName,'')+''+isnull(umc.User_LastName,'') Like '%" + filterObj.Report_WO_Filter_Ch_FeildValue + "'";
                                            
                                            break;
                                        }
                                    case "Ends with":
                                        {
                                            wherecondition = wherecondition != null ? wherecondition + " And isnull(umc.User_FirstName,'')+''+isnull(umc.User_LastName,'') Like'" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'" : " And isnull(umc.User_FirstName,'')+''+isnull(umc.User_LastName,'') Like '" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'";
                                           
                                            break;
                                        }
                                }
                            }
                            else
                            {
                                wherecondition = wherecondition != null ? wherecondition + " And isnull(umc.User_FirstName,'')+''+isnull(umc.User_LastName,'')" + filterObj.Report_WO_Filter_Ch_FeildOperator + "'" + filterObj.Report_WO_Filter_Ch_FeildValue + "'" : " And isnull(umc.User_FirstName,'')+''+isnull(umc.User_LastName,'')" + filterObj.Report_WO_Filter_Ch_FeildOperator + "'" + filterObj.Report_WO_Filter_Ch_FeildValue + "'";
                                

                            }

                          }
                        else if (filterObj.Report_WO_Filter_Ch_FeildId == 128 && !string.IsNullOrWhiteSpace(filterObj.Report_WO_Filter_Ch_FeildValue)) // Cordinator
                        {
                            if (filterObj.Report_WO_Filter_Ch_FeildOperator == "Contains" || filterObj.Report_WO_Filter_Ch_FeildOperator == "Does not contain" || filterObj.Report_WO_Filter_Ch_FeildOperator == "Starts with" || filterObj.Report_WO_Filter_Ch_FeildOperator == "Ends with")
                            {


                                switch (filterObj.Report_WO_Filter_Ch_FeildOperator)
                                {
                                    case "Contains":
                                        {
                                            wherecondition = wherecondition != null ? wherecondition + " And isnull(cor.User_FirstName,'')+''+isnull(cor.User_LastName,'')  Like '%" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'" : " And isnull(cor.User_FirstName,'')+''+isnull(cor.User_LastName,'') Like '%" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'";
                                            break;
                                        }
                                    case "Does not contain":
                                        {
                                            wherecondition = wherecondition != null ? wherecondition + " And isnull(cor.User_FirstName,'')+''+isnull(cor.User_LastName,'') Not Like '%" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'" : " And isnull(cor.User_FirstName,'')+''+isnull(cor.User_LastName,'') Not Like '%" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'";
                                            break;
                                        }
                                    case "Starts with":
                                        {
                                            wherecondition = wherecondition != null ? wherecondition + " And isnull(cor.User_FirstName,'')+''+isnull(cor.User_LastName,'') Like '%" + filterObj.Report_WO_Filter_Ch_FeildValue + "'" : " And isnull(cor.User_FirstName,'')+''+isnull(cor.User_LastName,'') Like '%" + filterObj.Report_WO_Filter_Ch_FeildValue + "'";
                                            break;
                                        }
                                    case "Ends with":
                                        {

                                            wherecondition = wherecondition != null ? wherecondition + " And isnull(cor.User_FirstName,'')+''+isnull(cor.User_LastName,'') Like'" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'" : " And isnull(cor.User_FirstName,'')+''+isnull(cor.User_LastName,'') Like '" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'";
                                            break;
                                        }
                                }
                            }
                            else
                            {
                                wherecondition = wherecondition != null ? wherecondition + " And isnull(cor.User_FirstName,'')+''+isnull(cor.User_LastName,'')" + filterObj.Report_WO_Filter_Ch_FeildOperator + "'" + filterObj.Report_WO_Filter_Ch_FeildValue + "'" : " And isnull(cor.User_FirstName,'')+''+isnull(cor.User_LastName,'')" + filterObj.Report_WO_Filter_Ch_FeildOperator + "'" + filterObj.Report_WO_Filter_Ch_FeildValue + "'";
                               
                            }
                            
                        }
                        else if (filterObj.Report_WO_Filter_Ch_FeildId == 129 && !string.IsNullOrWhiteSpace(filterObj.Report_WO_Filter_Ch_FeildValue)) // Processor
                        {
                            
                            
                            if (filterObj.Report_WO_Filter_Ch_FeildOperator == "Contains" || filterObj.Report_WO_Filter_Ch_FeildOperator == "Does not contain" || filterObj.Report_WO_Filter_Ch_FeildOperator == "Starts with" || filterObj.Report_WO_Filter_Ch_FeildOperator == "Ends with")
                            {


                                switch (filterObj.Report_WO_Filter_Ch_FeildOperator)
                                {
                                    case "Contains":
                                        {
                                            wherecondition = wherecondition != null ? wherecondition + " And isnull(pro.User_FirstName,'')+''+isnull(pro.User_LastName,'') Like '%" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'" : " And isnull(pro.User_FirstName,'')+''+isnull(pro.User_LastName,'') Like '%" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'";
                                            break;
                                        }
                                    case "Does not contain":
                                        {
                                            wherecondition = wherecondition != null ? wherecondition + " And isnull(pro.User_FirstName,'')+''+isnull(pro.User_LastName,'')  Not Like '%" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'" : " And isnull(pro.User_FirstName,'')+''+isnull(pro.User_LastName,'') Not  Like '%" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'";
                                            break;
                                        }
                                    case "Starts with":
                                        {
                                            wherecondition = wherecondition != null ? wherecondition + " And isnull(pro.User_FirstName,'')+''+isnull(pro.User_LastName,'') Like '%" + filterObj.Report_WO_Filter_Ch_FeildValue + "'" : " And isnull(pro.User_FirstName,'')+''+isnull(pro.User_LastName,'') Like '%" + filterObj.Report_WO_Filter_Ch_FeildValue + "'";
                                            break;
                                        }
                                    case "Ends with":
                                        {
                                            
                                            wherecondition = wherecondition != null ? wherecondition + " And isnull(pro.User_FirstName,'')+''+isnull(pro.User_LastName,'') Like '" + filterObj.Report_WO_Filter_Ch_FeildValue + "'%" : " And isnull(pro.User_FirstName,'')+''+isnull(pro.User_LastName,'') Like '" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'";
                                            break;
                                        }
                                }
                            }
                            else
                            {
                                wherecondition = wherecondition != null ? wherecondition + " And isnull(pro.User_FirstName,'')+''+isnull(pro.User_LastName,'')" + filterObj.Report_WO_Filter_Ch_FeildOperator + "'" + filterObj.Report_WO_Filter_Ch_FeildValue + "'" : " And isnull(pro.User_FirstName,'')+''+isnull(pro.User_LastName,'')" + filterObj.Report_WO_Filter_Ch_FeildOperator + "'" + filterObj.Report_WO_Filter_Ch_FeildValue + "'";
                            }
                            
                        }
                        else if (filterObj.Report_WO_Filter_Ch_FeildId == 160 && !string.IsNullOrWhiteSpace(filterObj.Report_WO_Filter_Ch_FeildValue))//Task
                        {


                            if (filterObj.Report_WO_Filter_Ch_FeildOperator == "Contains" || filterObj.Report_WO_Filter_Ch_FeildOperator == "Does not contain" || filterObj.Report_WO_Filter_Ch_FeildOperator == "Starts with" || filterObj.Report_WO_Filter_Ch_FeildOperator == "Ends with")
                            {

                            
                            switch (filterObj.Report_WO_Filter_Ch_FeildOperator)
                            {
                                case "Contains":
                                    {
                                        wherecondition = wherecondition != null ? wherecondition + " And task_master.Task_Name Like '%" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'" : " And task_master.Task_Name Like  '%" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'";
                                        break;
                                    }
                                case "Does not contain":
                                    {
                                        wherecondition = wherecondition != null ? wherecondition + " And task_master.Task_Name Not Like '%" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'" : " And task_master.Task_Name Not Like  '%" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'";
                                        break;
                                    }
                                case "Starts with":
                                    {
                                        wherecondition = wherecondition != null ? wherecondition + " And task_master.Task_Name Like '%" + filterObj.Report_WO_Filter_Ch_FeildValue + "'" : " And task_master.Task_Name Like  '%" + filterObj.Report_WO_Filter_Ch_FeildValue + "'";
                                        break;
                                    }
                                case "Ends with":
                                    {
                                        wherecondition = wherecondition != null ? wherecondition + " And task_master.Task_Name Like '" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'" : " And task_master.Task_Name Like  '" + filterObj.Report_WO_Filter_Ch_FeildValue + "%'";
                                        break;
                                    }
                            }
                            }
                            else
                            {
                                wherecondition = wherecondition != null ? wherecondition + " And task_master.Task_Name" + filterObj.Report_WO_Filter_Ch_FeildOperator + "'" + filterObj.Report_WO_Filter_Ch_FeildValue + "'" : " And task_master.Task_Name" + filterObj.Report_WO_Filter_Ch_FeildOperator + "'" + filterObj.Report_WO_Filter_Ch_FeildValue + "'";
                            }
                            
                        }
                        else
                        {
                            wherecondition = wherecondition != null ? wherecondition + " And work." + filterObj.Report_WO_Filter_Ch_FeildName + filterObj.Report_WO_Filter_Ch_FeildOperator + "'" + filterObj.Report_WO_Filter_Ch_FeildValue + "'" : " And work." + filterObj.Report_WO_Filter_Ch_FeildName + filterObj.Report_WO_Filter_Ch_FeildOperator + "'" + filterObj.Report_WO_Filter_Ch_FeildValue + "'";
                        }
                       
                    }


                    AdvanceReportMasterDTO reportMasterDTO = new AdvanceReportMasterDTO();

                    reportMasterDTO.Type = 2;
                    reportMasterDTO.whereClause = wherecondition;
                    reportMasterDTO.UserId = model.UserId;
                    DataSet ds = GetFilterReportData(reportMasterDTO);

                    if (ds != null && ds.Tables.Count > 0)
                    {
                        var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                        List<AdvanceFilterReportMasterDTO> ReportDetails =
                           (from item in myEnumerableFeaprd
                            select new AdvanceFilterReportMasterDTO
                            {
                                workOrder_ID = item.Field<Int64>("workOrder_ID"),
                                IPLNO = item.Field<String>("IPLNO"),
                                workOrderNumber = item.Field<String>("workOrderNumber"),
                                Contractor = item.Field<Int64?>("Contractor"),
                                address1 = item.Field<String>("address1"),
                                city = item.Field<String>("city"),
                                state = item.Field<Int64?>("state"),
                                zip = item.Field<Int64?>("zip"),
                                WorkType = item.Field<Int64?>("WorkType"),
                                dueDate = item.Field<DateTime?>("dueDate"),
                                Field_complete_date = item.Field<DateTime?>("Field_complete_date"),
                                SentToClient_date = item.Field<DateTime?>("SentToClient_date"),
                                OfficeApproved_date = item.Field<DateTime?>("OfficeApproved_date"),
                                EstimatedDate = item.Field<DateTime?>("EstimatedDate"),
                                clientDueDate = item.Field<DateTime?>("clientDueDate"),
                                Cancel_Date = item.Field<DateTime?>("Cancel_Date"),
                                DateCreated = item.Field<DateTime?>("DateCreated"),
                                Loan_Number = item.Field<String>("Loan_Number"),
                                Lotsize = item.Field<String>("Lotsize"),
                                Lock_Code = item.Field<String>("Lock_Code"),
                                status = item.Field<Int32?>("status"),

                                ContractorName = item.Field<String>("ContractorName"),
                                ProcessorName = item.Field<String>("ProcessorName"),
                                CordinatorName = item.Field<String>("CordinatorName"),


                                Client_Company_Name = item.Field<String>("Client_Company_Name"),
                                IPL_StateName = item.Field<String>("IPL_StateName"),
                                WT_WorkType = item.Field<String>("WT_WorkType"),
                                CategoryName = item.Field<String>("CategoryName"),
                                Status_Name = item.Field<String>("Status_Name"),

                            }).ToList();


                        objDynamic.Add(ReportDetails);
                    }
                }



                else
                {
                    if (model.InvoiceDateFrom != null && model.InvoiceDateTo != null)
                    {
                        wherecondition = " And CAST(clientInv.Inv_Client_CreatedOn as date) >=   CONVERT(date,'" + model.InvoiceDateFrom.Value.ToString("yyyy-MM-dd") + "')  And CAST(clientInv.Inv_Client_CreatedOn as date) <=   CONVERT(date,'" + model.InvoiceDateTo.Value.ToString("yyyy-MM-dd") + "')";
                    }
                    if (model.GroupById != null && model.GroupById > 0)
                    {
                        //if (model.ReportTypeId == 2)
                        //{
                        //    wherecondition = wherecondition != null ? wherecondition + " And work.WorkType IN(" + model.GroupById.ToString() + ")" : " And work.WorkType IN(" + model.GroupById.ToString() + ")";
                        //}
                        //else
                        //{
                        //    wherecondition = wherecondition != null ? wherecondition + " And work.state IN(" + model.GroupById.ToString() + ")" : " And work.state IN(" + model.GroupById.ToString() + ")";
                        //}

                        switch (model.ReportTypeId)
                        {
                            case 2:
                                {
                                    wherecondition = wherecondition != null ? wherecondition + " And work.WorkType IN(" + model.GroupById.ToString() + ")" : " And work.WorkType IN(" + model.GroupById.ToString() + ")";
                                    break;
                                }
                            case 3:
                                {
                                    wherecondition = wherecondition != null ? wherecondition + " And work.state IN(" + model.GroupById.ToString() + ")" : " And work.state IN(" + model.GroupById.ToString() + ")";
                                    break;
                                }
                            case 4:
                                {
                                    wherecondition = wherecondition != null ? wherecondition + " And work.Company IN(" + model.GroupById.ToString() + ")" : " And work.Company IN(" + model.GroupById.ToString() + ")";
                                    break;
                                }
                            case 5:
                                {
                                    //wherecondition = wherecondition != null ? wherecondition + " And task_master.Task_Name Like '%"+ model.GroupById.ToString()  + "%'" : " And  task_master.Task_Name Like '%" + model.GroupById.ToString() + "%'";
                                    wherecondition = wherecondition != null ? wherecondition + " And task_master.Task_pkeyID IN(" + model.GroupById.ToString() + ")" : " And task_master.Task_pkeyID IN(" + model.GroupById.ToString() + ")";
                                    break;
                                }
                        }

                    }


                    //use by Invoice date
                    if (Data.InvoiceDateFromstatus != null && Data.InvoiceDateTostatus != null)
                    {
                        // DateTime InvoiceDateFromstatus = Convert.ToDateTime(Data.InvoiceDateTostatus).AddDays(1);
                        wherecondition  = wherecondition != null ? wherecondition +  " And CAST(clientInv.Inv_Client_CreatedOn as date) >=   CONVERT(date,'" + Data.InvoiceDateFromstatus.Value.ToString("yyyy-MM-dd") + "')  And CAST(clientInv.Inv_Client_CreatedOn as date) <=   CONVERT(date,'" + Data.InvoiceDateTostatus.Value.ToString("yyyy-MM-dd") + "')" :
                             " And CAST(clientInv.Inv_Client_CreatedOn as date) >=   CONVERT(date,'" + Data.InvoiceDateFromstatus.Value.ToString("yyyy-MM-dd") + "')  And CAST(clientInv.Inv_Client_CreatedOn as date) <=   CONVERT(date,'" + Data.InvoiceDateTostatus.Value.ToString("yyyy-MM-dd") + "')";
                    }

                    //use by Field complete date
                    if (Data.ReadyOfficeDateFrom != null && Data.ReadyOfficeDateTo != null)
                    {
                        // DateTime ReadyOfficeDateTo = Convert.ToDateTime(Data.ReadyOfficeDateTo).AddDays(1);
                        wherecondition = wherecondition != null ? wherecondition + " And CAST(work.Field_complete_date as date) >=   CONVERT(date,'" + Data.ReadyOfficeDateFrom.Value.ToString("yyyy-MM-dd") + "')  And CAST(work.Field_complete_date as date) <=   CONVERT(date,'" + Data.ReadyOfficeDateTo.Value.ToString("yyyy-MM-dd") + "')" :
                            " And CAST(work.Field_complete_date as date) >=   CONVERT(date,'" + Data.ReadyOfficeDateFrom.Value.ToString("yyyy-MM-dd") + "')  And CAST(work.Field_complete_date as date) <=   CONVERT(date,'" + Data.ReadyOfficeDateTo.Value.ToString("yyyy-MM-dd") + "')";
                            
                    }

                    //use by SentToClient date
                    if (Data.SentToClientDateFrom != null && Data.SentToClientDateTo != null)
                    {
                        // DateTime SentToClientDateTo = Convert.ToDateTime(Data.SentToClientDateTo).AddDays(1);
                        wherecondition = wherecondition != null ? wherecondition + "  And CAST(work.SentToClient_date as date) >=   CONVERT(date,'" + Data.SentToClientDateFrom.Value.ToString("yyyy-MM-dd") + "')  And CAST(work.SentToClient_date as date) <=   CONVERT(date,'" + Data.SentToClientDateTo.Value.ToString("yyyy-MM-dd") + "')" :
                            "  And CAST(work.SentToClient_date as date) >=   CONVERT(date,'" + Data.SentToClientDateFrom.Value.ToString("yyyy-MM-dd") + "')  And CAST(work.SentToClient_date as date) <=   CONVERT(date,'" + Data.SentToClientDateTo.Value.ToString("yyyy-MM-dd") + "')";
                    }

                    //use by Complete Date
                    if (Data.CompletedDateFrom != null && Data.CompletedDateTo != null)
                    {
                        wherecondition = wherecondition != null ? wherecondition + "  And CAST(work.Complete_Date as date) >=   CONVERT(date,'" + Data.CompletedDateFrom.Value.ToString("yyyy-MM-dd") + "')  And CAST(work.Complete_Date as date) <=   CONVERT(date,'" + Data.CompletedDateTo.Value.ToString("yyyy-MM-dd") + "')" :
                            "  And CAST(work.Complete_Date as date) >=   CONVERT(date,'" + Data.CompletedDateFrom.Value.ToString("yyyy-MM-dd") + "')  And CAST(work.Complete_Date as date) <=   CONVERT(date,'" + Data.CompletedDateTo.Value.ToString("yyyy-MM-dd") + "')";
                    }

                    //use by Created Date
                    if (Data.CreatedDateFrom != null && Data.CreatedDateTo != null)
                    {
                        //wherecondition = " And CAST(work.CreatedOn as date) >=   CONVERT(date,'" + Data.CreatedDateFrom.Value.ToString("yyyy-MM-dd") + "')  And CAST(work.CreatedOn as date) <=   CONVERT(date,'" + Data.CreatedDateTo.Value.ToString("yyyy-MM-dd") + "')";
                        wherecondition = wherecondition != null ? wherecondition + " And CAST(work.DateCreated as date) >=   CONVERT(date,'" + Data.CreatedDateFrom.Value.ToString("yyyy-MM-dd") + "')  And CAST(work.DateCreated as date) <=   CONVERT(date,'" + Data.CreatedDateTo.Value.ToString("yyyy-MM-dd") + "')" :
                            " And CAST(work.DateCreated as date) >=   CONVERT(date,'" + Data.CreatedDateFrom.Value.ToString("yyyy-MM-dd") + "')  And CAST(work.DateCreated as date) <=   CONVERT(date,'" + Data.CreatedDateTo.Value.ToString("yyyy-MM-dd") + "')";
                    }

                    //use by Office Approve Date
                    if (Data.OfficeApproveDateFrom != null && Data.OfficeApproveDateTo != null)
                    {
                        wherecondition = wherecondition != null ? wherecondition + " And work.status = 6 And CAST(work.OfficeApproved_date as date) >=   CONVERT(date,'" + Data.OfficeApproveDateFrom.Value.ToString("yyyy-MM-dd") + "')  And CAST(work.OfficeApproved_date as date) <=   CONVERT(date,'" + Data.OfficeApproveDateTo.Value.ToString("yyyy-MM-dd") + "')" :
                             " And work.status = 6 And CAST(work.OfficeApproved_date as date) >=   CONVERT(date,'" + Data.OfficeApproveDateFrom.Value.ToString("yyyy-MM-dd") + "')  And CAST(work.OfficeApproved_date as date) <=   CONVERT(date,'" + Data.OfficeApproveDateTo.Value.ToString("yyyy-MM-dd") + "')";
                    }

                    //use by Client check Date
                   if (Data.ClientCheckDateFrom != null && Data.ClientCheckDateTo != null)
                        {
                        wherecondition = wherecondition != null ? wherecondition + " And CAST((select max(Client_Pay_Payment_Date) from [dbo].[Client_Invoice_Patyment] where Client_Pay_Wo_Id = work.[workOrder_ID] and Client_Pay_IsActive = 1 GROUP BY Client_Pay_Wo_Id) as date) >=  CONVERT(date,'" + Data.ClientCheckDateFrom.Value.ToString("yyyy-MM-dd") + "')  And CAST((select max(Client_Pay_Payment_Date) from [dbo].[Client_Invoice_Patyment] where Client_Pay_Wo_Id = work.[workOrder_ID] and Client_Pay_IsActive = 1 GROUP BY Client_Pay_Wo_Id) as date) <=  CONVERT(date,'" + Data.ClientCheckDateTo.Value.ToString("yyyy-MM-dd") + "')" :
                            " And CAST((select max(Client_Pay_Payment_Date) from [dbo].[Client_Invoice_Patyment] where Client_Pay_Wo_Id = work.[workOrder_ID] and Client_Pay_IsActive = 1 GROUP BY Client_Pay_Wo_Id) as date) >=  CONVERT(date,'" + Data.ClientCheckDateFrom.Value.ToString("yyyy-MM-dd") + "')  And CAST((select max(Client_Pay_Payment_Date) from [dbo].[Client_Invoice_Patyment] where Client_Pay_Wo_Id = work.[workOrder_ID] and Client_Pay_IsActive = 1 GROUP BY Client_Pay_Wo_Id) as date) <=  CONVERT(date,'" + Data.ClientCheckDateTo.Value.ToString("yyyy-MM-dd") + "')";
                    }






                    if (model.StatusId != null && model.StatusId > 0)
                    {
                        wherecondition = wherecondition != null ? wherecondition + " And work.status IN(" + model.StatusId.ToString() + ")" : " And work.status IN(" + model.StatusId.ToString() + ")";
                    }

                    if (model.ClientId != null && model.ClientId > 0)
                    {
                        wherecondition = wherecondition != null ? wherecondition + " And work.Company IN(" + model.ClientId.ToString() + ")" : " And work.Company IN(" + model.ClientId.ToString() + ")";
                    }
                    if (model.CustomerId != null && model.CustomerId > 0)
                    {
                        wherecondition = wherecondition != null ? wherecondition + " And work.Customer_Number IN(" + model.CustomerId.ToString() + ")" : " And work.Customer_Number IN(" + model.CustomerId.ToString() + ")";
                    }
                    if (model.ContractorId != null && model.ContractorId > 0)
                    {
                        wherecondition = wherecondition != null ? wherecondition + " And work.Contractor IN(" + model.ContractorId.ToString() + ")" : " And work.Contractor IN(" + model.ContractorId.ToString() + ")";
                    }
                    if (model.CordinatorId != null && model.CordinatorId > 0)
                    {
                        wherecondition = wherecondition != null ? wherecondition + " And work.Cordinator IN(" + model.CordinatorId.ToString() + ")" : " And work.Cordinator IN(" + model.CordinatorId.ToString() + ")";
                    }
                    if (model.ProcessorId != null && model.ProcessorId > 0)
                    {
                        wherecondition = wherecondition != null ? wherecondition + " And work.Processor IN(" + model.ProcessorId.ToString() + ")" : " And work.Processor IN(" + model.ProcessorId.ToString() + ")";
                    }


                    AdvanceReportMasterDTO reportMasterDTO = new AdvanceReportMasterDTO();
                    switch(model.ReportTypeId)
                    {
                        case 5:
                            {
                                reportMasterDTO.Type = 3;
                                break;
                            }
                        default:
                            {
                                reportMasterDTO.Type = 2;
                                break;
                            }
                    }

                  
                    reportMasterDTO.whereClause = wherecondition;
                    reportMasterDTO.UserId = model.UserId;
                    DataSet ds = GetGroupByReportData(reportMasterDTO);

                    if (ds != null && ds.Tables.Count > 0)
                    {
                        var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                        List<AdvanceReportMasterDTO> ReportDetails =
                           (from item in myEnumerableFeaprd
                            select new AdvanceReportMasterDTO
                            {
                                workOrder_ID = item.Field<Int64>("workOrder_ID"),
                                IPLNO = item.Field<String>("IPLNO"),
                                workOrderNumber = item.Field<String>("workOrderNumber"),
                                //Contractor = item.Field<Int64?>("Contractor"),
                                address1 = item.Field<String>("address1"),
                                //city = item.Field<String>("city"),
                                //state = item.Field<String>("state"),
                                //zip = item.Field<Int64?>("zip"),
                                //WorkType = item.Field<Int64?>("WorkType"),
                                //dueDate = item.Field<DateTime?>("dueDate"),
                                //Field_complete_date = item.Field<DateTime?>("Field_complete_date"),
                                //SentToClient_date = item.Field<DateTime?>("SentToClient_date"),
                                //OfficeApproved_date = item.Field<DateTime?>("OfficeApproved_date"),
                                //status = item.Field<Int32?>("status"),

                                //Client_Pay_Invoice_Id = item.Field<Int64?>("Client_Pay_Invoice_Id"),
                                Client_Invoice_Number = item.Field<String>("Client_Invoice_Number"),
                                //Client_InvoiceDate = item.Field<DateTime?>("Client_InvoiceDate"),
                                //Client_InvoicePaid_Date = item.Field<DateTime?>("Client_InvoicePaid_Date"),
                                Client_InvoiceTotal = item.Field<Decimal?>("Client_InvoiceTotal"),
                                //Client_InvoicePaid = item.Field<Decimal?>("Client_InvoicePaid"),

                                Client_Pay_Amount = item.Field<Decimal?>("Client_Pay_Amount"),

                                //Con_Pay_Invoice_Id = item.Field<Int64?>("Con_Pay_Invoice_Id"),
                                //Con_Invoice_Number = item.Field<String>("Con_Invoice_Number"),
                                //Con_InvoiceDate = item.Field<DateTime?>("Con_InvoiceDate"),
                                Con_InvoiceTotal = item.Field<Decimal?>("Con_InvoiceTotal"),
                                //Con_InvoicePaid_Date = item.Field<DateTime?>("Con_InvoicePaid_Date"),
                                //Con_InvoicePaid = item.Field<Decimal?>("Con_InvoicePaid"),

                                ContractorName = item.Field<String>("ContractorName"),
                                //ProcessorName = item.Field<String>("ProcessorName"),
                                //CordinatorName = item.Field<String>("CordinatorName"),


                                Client_Company_Name = item.Field<String>("Client_Company_Name"),
                                //IPL_StateName = item.Field<String>("IPL_StateName"),
                                WT_WorkType = item.Field<String>("WT_WorkType"),
                                //CategoryName = item.Field<String>("CategoryName"),
                                //Status_Name = item.Field<String>("Status_Name"),

                            }).ToList();


                        objDynamic.Add(ReportDetails);
                        var totalAmount = ReportDetails.Sum(r => r.Client_InvoiceTotal);
                        var paymentAmount = ReportDetails.Sum(r => r.Client_Pay_Amount);
                        Decimal? BalanceAmount = 0;
                        objDynamic.Add(totalAmount);
                        objDynamic.Add(paymentAmount);
                        if (totalAmount != null && totalAmount != 0  && ReportDetails.Count != 0)
                        {
                            var avgAmount = totalAmount / ReportDetails.Count;
                            objDynamic.Add(avgAmount);
                            if (paymentAmount != null && paymentAmount != 0 && ReportDetails.Count != 0)
                            {
                                BalanceAmount =  totalAmount - paymentAmount;
                            }
                            else
                            {
                                BalanceAmount = totalAmount;
                            }
                            objDynamic.Add(BalanceAmount);
                        }
                        else
                        {
                            var avgAmount =0;
                            objDynamic.Add(avgAmount);
                            objDynamic.Add(BalanceAmount);
                        }
                       
                       
                    }
                }                
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objDynamic;
         }
        private DataSet GetGroupByReportData(AdvanceReportMasterDTO model)
        {
            DataSet ds = null;
            try
            {
                // Added by Dipali
                string selectProcedure = "[GetAdvanceGroupByReportMaster]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@whereClause", 1 + "#varchar#" + model.whereClause);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ds;
        }

        public List<dynamic> GetAdvanceProTimeReportDetails(AdvanceReportMasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            string wherecondition = string.Empty;


           // var Data = JsonConvert.DeserializeObject<AdvanceReportMasterDTO>(model.whereClause);

            try
            {
                

                    if (model.InvoiceDateFrom != null && model.InvoiceDateTo != null)
                    {
                        wherecondition = " And CAST(work.SentToClient_date as date) >=   CONVERT(date,'" + model.InvoiceDateFrom.Value.ToString("yyyy-MM-dd") + "')  And CAST(work.SentToClient_date as date) <=   CONVERT(date,'" + model.InvoiceDateTo.Value.ToString("yyyy-MM-dd") + "')";
                    }


                //wherecondition = wherecondition != null ? wherecondition + " And work.WorkType IN(" + model.GroupById.ToString() + ")" : " And work.WorkType IN(" + model.GroupById.ToString() + ")";

                    if (!string.IsNullOrWhiteSpace(model.WorkTypeMultiple)  && model.WorkTypeMultiple != "[]" )
                    {
                    var workTypeVal = JsonConvert.DeserializeObject<List<WorkTypeReport>>(model.WorkTypeMultiple);
                    string worktype = string.Empty;
                    for (int i = 0; i < workTypeVal.Count; i++)
                    {
                       if(!string.IsNullOrWhiteSpace(worktype))
                        {
                            worktype = worktype +" , " + workTypeVal[i].WT_pkeyID;
                        }
                        else
                        {
                            worktype = workTypeVal[i].WT_pkeyID.ToString();
                        }
                    }
                    wherecondition = wherecondition != null ? wherecondition + " And work.WorkType IN(" + worktype + ")" : " And work.WorkType IN(" + worktype + ")";
                    }

                    if (model.ClientId != null && model.ClientId > 0)
                    {
                        wherecondition = wherecondition != null ? wherecondition + " And work.Company IN(" + model.ClientId.ToString() + ")" : " And work.Company IN(" + model.ClientId.ToString() + ")";
                    }
                    
                    if (model.ContractorId != null && model.ContractorId > 0)
                    {
                        wherecondition = wherecondition != null ? wherecondition + " And work.Contractor IN(" + model.ContractorId.ToString() + ")" : " And work.Contractor IN(" + model.ContractorId.ToString() + ")";
                    }
                    if (model.CordinatorId != null && model.CordinatorId > 0)
                    {
                        wherecondition = wherecondition != null ? wherecondition + " And work.Cordinator IN(" + model.CordinatorId.ToString() + ")" : " And work.Cordinator IN(" + model.CordinatorId.ToString() + ")";
                    }
                    if (model.ProcessorId != null && model.ProcessorId > 0)
                    {
                        wherecondition = wherecondition != null ? wherecondition + " And work.Processor IN(" + model.ProcessorId.ToString() + ")" : " And work.Processor IN(" + model.ProcessorId.ToString() + ")";
                    }


                    AdvanceReportMasterDTO reportMasterDTO = new AdvanceReportMasterDTO();
                    reportMasterDTO.StausReportType = model.StausReportType;
                    reportMasterDTO.Type = 1;
                    reportMasterDTO.whereClause = wherecondition;
                    reportMasterDTO.UserId = model.UserId;
                    DataSet ds = GetGroupProTimeReportData(reportMasterDTO);

                    if (ds != null && ds.Tables.Count > 0)
                    {
                      objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[0]));
                      var Rowcount = ds.Tables[0].Rows.Count;
                    var minDays = 0;
                    var maxDays = 0;
                    var averageDays = 0.0;
                    if (Rowcount >0)
                    {
                        minDays = ds.Tables[0].AsEnumerable().Min(row => row.Field<int>("Diff"));
                         maxDays = ds.Tables[0].AsEnumerable().Max(row => row.Field<int>("Diff"));
                        averageDays = ds.Tables[0].AsEnumerable().Average(row => row.Field<int>("Diff"));
                    }
                     
                    
                    objDynamic.Add(Rowcount);
                    objDynamic.Add(minDays);
                    objDynamic.Add(maxDays);
                    objDynamic.Add(averageDays);
                    objDynamic.Add(model.StausReportType);
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objDynamic;
        }
        private DataSet GetGroupProTimeReportData(AdvanceReportMasterDTO model)
        {
            DataSet ds = null;
            try
            {
                // Added by Dipali
                string selectProcedure = "[GetAdvanceProcessingTimeReportMaster]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@StausReportType", 1 + "#int#" + model.StausReportType);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserId);
                input_parameters.Add("@whereClause", 1 + "#varchar#" + model.whereClause);
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ds;
        }
        private DataSet GetFilterReportData(AdvanceReportMasterDTO model)
        {
            DataSet ds = null;
            try
            {
                // Added by Dipali
                string selectProcedure = "[GetAdvanceFilterReportMaster]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@whereClause", 1 + "#varchar#" + model.whereClause);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ds;
        }
        public List<dynamic> AddWorkOrderFilterData(Report_WO_Filter_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objAddData = new List<dynamic>();
            Report_WO_Filter_MasterDTO report_WO_Filter_MasterDTO = new Report_WO_Filter_MasterDTO();
            Int64 Report_WO_Filter_PkeyId = 0;
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();

            try
            {
                if (model != null && !string.IsNullOrEmpty(model.Report_WO_Filter_Name))
                {
                    report_WO_Filter_MasterDTO.Report_WO_Filter_PkeyId = model.Report_WO_Filter_PkeyId;
                    report_WO_Filter_MasterDTO.Report_WO_Filter_Name = model.Report_WO_Filter_Name;
                    report_WO_Filter_MasterDTO.Report_WO_Filter_IsActive = true;
                    report_WO_Filter_MasterDTO.Report_WO_Filter_IsDelete = false;
                    report_WO_Filter_MasterDTO.UserID = model.UserID;
                    report_WO_Filter_MasterDTO.Type = model.Type;

                    objData = report_WO_Filter_MasterData.AddReportWOFilterMasterData(report_WO_Filter_MasterDTO);
                    if (model.Type != 4)
                    {
                        if (objData != null && objData[0].Status != "0")
                        {
                            if (!string.IsNullOrEmpty(objData[0].Report_WO_Filter_PkeyId))
                            {
                                Report_WO_Filter_PkeyId = Convert.ToInt64(objData[0].Report_WO_Filter_PkeyId);
                                foreach (var child in model.ArrayWOFilter)
                                {
                                    Report_WO_Filter_ChildDTO report_WO_Filter_ChildDTO = new Report_WO_Filter_ChildDTO();
                                    report_WO_Filter_ChildDTO.Report_WO_Filter_Ch_PkeyId = 0;
                                    report_WO_Filter_ChildDTO.Report_WO_Filter_Ch_Master_FKeyId = Report_WO_Filter_PkeyId;
                                    report_WO_Filter_ChildDTO.Report_WO_Filter_Ch_FeildId = child.Report_WO_Filter_Ch_FeildId;
                                    report_WO_Filter_ChildDTO.Report_WO_Filter_Ch_FeildName = child.Report_WO_Filter_Ch_FeildName;
                                    report_WO_Filter_ChildDTO.Report_WO_Filter_Ch_FeildOperator = child.Report_WO_Filter_Ch_FeildOperator;
                                    report_WO_Filter_ChildDTO.Report_WO_Filter_Ch_FeildValue = child.Report_WO_Filter_Ch_FeildValue;
                                    report_WO_Filter_ChildDTO.Report_WO_Filter_Ch_FeildDateValue = child.Report_WO_Filter_Ch_FeildDateValue;
                                    report_WO_Filter_ChildDTO.Report_WO_Filter_Ch_IsActive = true;
                                    report_WO_Filter_ChildDTO.Report_WO_Filter_Ch_IsDelete = false;
                                    report_WO_Filter_ChildDTO.UserID = model.UserID;
                                    report_WO_Filter_ChildDTO.Type = 1;

                                    var objChildData = report_WO_Filter_MasterData.AddReportWOFilterChildData(report_WO_Filter_ChildDTO);
                                }
                            }
                        }
                    }
                                        
                }
                objAddData.Add(objData);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objAddData;
        }

        public List<dynamic> DeleteWorkOrderFilterChildData(Report_WO_Filter_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objAddData = new List<dynamic>();
            Report_WO_Filter_MasterDTO report_WO_Filter_MasterDTO = new Report_WO_Filter_MasterDTO();

            try
            {
                Report_WO_Filter_ChildDTO report_WO_Filter_ChildDTO = new Report_WO_Filter_ChildDTO();
                report_WO_Filter_ChildDTO.Type = 4;
                report_WO_Filter_ChildDTO.Report_WO_Filter_Ch_IsDelete = true;
                report_WO_Filter_ChildDTO.Report_WO_Filter_Ch_IsActive = false;
                report_WO_Filter_ChildDTO.Report_WO_Filter_Ch_PkeyId = model.Report_WO_Filter_ChId;
                var objChildData = report_WO_Filter_MasterData.AddReportWOFilterChildData(report_WO_Filter_ChildDTO);

                
                objAddData.Add(objChildData);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objAddData;
        }

        //Advance report pdf
        public static byte[] GenerateRuntimePDF(string html)
        {
            #region NReco.PdfGenerator
            HtmlToPdfConverter nRecohtmltoPdfObj = new HtmlToPdfConverter();
            return nRecohtmltoPdfObj.GeneratePdf(html);
            #endregion

        }
        public async Task<CustomReportPDF> GeneratePDFAdvanceReportdetails(AdvanceReportMasterDTO model)
        {
            CustomReportPDF custom = new CustomReportPDF();
            try
            {
                StringBuilder html = new StringBuilder();
                StringBuilder table = new StringBuilder();
                string theader = "";
                string tfooter = "";
                int index = 0;
                string tbody = "<tbody>";

                /////////////////////////////////////////////////////////////////////////////////
                
                //theader += "<table width = '100%' border='1' cellpadding='1' cellspacing='0' bgcolor='#efefef'>";
                //table.Append(theader);
                //tbody += "<tr> <td  bgcolor = '#CCCCCC' style='text-align:center;height:50px;  border: none;'><span class='MsoNormal1' style='text-align:center; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px'><b><u> </u></b></span>  <div align='center;' style='font - family: Verdana, Arial, Helvetica, sans - serif; font - size: 10px; '>    <span class='MsoNormal1' style='text - align:center; font - size: 35px'><b><u>Advanced Reports</u></b></span></div> </td></tr>";
                ////tbody += "<tr><td ><table width='100%'  border='0'> <tr height ='30px'><td width='10%' bgcolor='#CCCCCC' style='font-size: 12px'><div align='center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'>&nbsp;Sr No </span></b></span></div><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'></span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align='center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'>&nbsp;WO # </span></b></span></div><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align='center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;' > &nbsp; Status </span ></b ></span ></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; IPL#</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td>  <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Contractor</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Address</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td><td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; WorkType</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Client</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td><td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Due Date</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Coordinator</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Processor</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td>   </tr>";
                //tbody += "<tr><td ><table width='100%'  border='0'> <tr height ='30px'><td width='10%' bgcolor='#CCCCCC' style='font-size: 12px'><div align='center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'>&nbsp;Sr No </span></b></span></div><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'></span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align='center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'>&nbsp;Invoice # </span></b></span></div><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align='center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;' > &nbsp; IPL# </span ></b ></span ></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Contractor </span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td>  <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; WO #</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Address</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td><td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; WorkType</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Client</span></b></span></div><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Client Total</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Client Pay Amount</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td>   </tr>";
                //foreach (var item in adreport[0])
                //{
                //    var sr = 1 + index++;
                //    //tbody += "<tr height ='30px'> <td style = 'text-align: center;'width='10%'>" + sr + "</td ><td style = 'text-align: center;'>" + item.workOrderNumber + "</td ><td style = 'text-align: center;'>" + item.Status_Name + "</td><td style = 'text-align: center;'>" + item.IPLNO + "</td><td style = 'text-align: center;'>" + item.ContractorName + "%" + "</td><td style = 'text-align: center;'>" + item.address1 + "</td><td style = 'text-align: center;'>" + item.WT_WorkType + "</td><td style = 'text-align: center;'>" + item.Client_Company_Name + "</td><td style = 'text-align: center;'>" + item.dueDate + "</td><td style = 'text-align: center;'>" + item.CordinatorName + "</td><td style = 'text-align: center;'>" + item.ProcessorName + "</td></tr>";
                //    tbody += "<tr height ='30px'> <td style = 'text-align: center;'width='10'>" + sr + "</td ><td style = 'text-align: center;'>" + item.Client_Invoice_Number + "</td><td style = 'text-align: center;'>" + item.IPLNO + "</td><td style = 'text-align: center;'>" + item.ContractorName + "</td><td style = 'text-align: center;'>" + item.workOrderNumber + "</td ><td style = 'text-align: center;'>" + item.address1 + "</td><td style = 'text-align: center;'>" + item.WT_WorkType + "</td><td style = 'text-align: center;'>" + item.Client_Company_Name + "</td><td style = 'text-align: center;'>" + item.Client_InvoiceTotal + "</td><td style = 'text-align: center;'>" + item.Client_Pay_Amount + "</td></tr>";
                //}


                ///////////////////////////////////////////////////////////////////
                ///

                switch (model.ReportTypeId)
                {

                    case 1:
                        {
                            var adreport = GetAdvanceReportDetails(model);
                            theader += "<table width = '100%' border='1' cellpadding='1' cellspacing='0' bgcolor='#efefef'>";
                            table.Append(theader);
                            tbody += "<tr> <td  bgcolor = '#CCCCCC' style='text-align:center;height:50px;  border: none;'><span class='MsoNormal1' style='text-align:center; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px'><b><u> </u></b></span>  <div align='center;' style='font - family: Verdana, Arial, Helvetica, sans - serif; font - size: 10px; '>    <span class='MsoNormal1' style='text - align:center; font - size: 35px'><b><u>Advanced Reports</u></b></span></div> </td></tr>";
                            tbody += "<tr><td ><table width='100%'  border='0'> <tr height ='30px'><td width='10%' bgcolor='#CCCCCC' style='font-size: 12px'><div align='center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'>&nbsp;Sr No </span></b></span></div><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'></span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align='center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'>&nbsp;WO # </span></b></span></div><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align='center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;' > &nbsp; Status </span ></b ></span ></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; IPL#</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td>  <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Contractor</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Address</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td><td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; WorkType</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Client</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td><td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Due Date</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Coordinator</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Processor</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td>   </tr>";
                            //tbody += "<tr><td ><table width='100%'  border='0'> <tr height ='30px'><td width='10%' bgcolor='#CCCCCC' style='font-size: 12px'><div align='center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'>&nbsp;Sr No </span></b></span></div><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'></span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align='center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'>&nbsp;Invoice # </span></b></span></div><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align='center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;' > &nbsp; IPL# </span ></b ></span ></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Contractor </span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td>  <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; WO #</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Address</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td><td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; WorkType</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Client</span></b></span></div><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Client Total</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Client Pay Amount</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td>   </tr>";
                            foreach (var item in adreport[0])
                            {
                                var sr = 1 + index++;
                                tbody += "<tr height ='30px'> <td style = 'text-align: center;'width='10%'>" + sr + "</td ><td style = 'text-align: center;'>" + item.workOrderNumber + "</td ><td style = 'text-align: center;'>" + item.Status_Name + "</td><td style = 'text-align: center;'>" + item.IPLNO + "</td><td style = 'text-align: center;'>" + item.ContractorName + "%" + "</td><td style = 'text-align: center;'>" + item.address1 + "</td><td style = 'text-align: center;'>" + item.WT_WorkType + "</td><td style = 'text-align: center;'>" + item.Client_Company_Name + "</td><td style = 'text-align: center;'>" + item.dueDate + "</td><td style = 'text-align: center;'>" + item.CordinatorName + "</td><td style = 'text-align: center;'>" + item.ProcessorName + "</td></tr>";
                                //tbody += "<tr height ='30px'> <td style = 'text-align: center;'width='10'>" + sr + "</td ><td style = 'text-align: center;'>" + item.Client_Invoice_Number + "</td><td style = 'text-align: center;'>" + item.IPLNO + "</td><td style = 'text-align: center;'>" + item.ContractorName + "</td><td style = 'text-align: center;'>" + item.workOrderNumber + "</td ><td style = 'text-align: center;'>" + item.address1 + "</td><td style = 'text-align: center;'>" + item.WT_WorkType + "</td><td style = 'text-align: center;'>" + item.Client_Company_Name + "</td><td style = 'text-align: center;'>" + item.Client_InvoiceTotal + "</td><td style = 'text-align: center;'>" + item.Client_Pay_Amount + "</td></tr>";
                            }

                            break;
                        }
                    case 6:
                        {
                            switch (model.StausReportType)
                            {

                                case 1:
                                    {
                                        var adreport = GetAdvanceProTimeReportDetails(model);
                                        theader += "<table width = '100%' border='1' cellpadding='1' cellspacing='0' bgcolor='#efefef'>";
                                        table.Append(theader);
                                        tbody += "<tr> <td  bgcolor = '#CCCCCC' style='text-align:center;height:50px;  border: none;'><span class='MsoNormal1' style='text-align:center; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px'><b><u> </u></b></span>  <div align='center;' style='font - family: Verdana, Arial, Helvetica, sans - serif; font - size: 10px; '>    <span class='MsoNormal1' style='text - align:center; font - size: 35px'><b><u>Advanced Reports</u></b></span></div> </td></tr>";
                                        tbody += "<tr><td ><table width='100%'  border='0'> <tr height ='30px'><td width='10%' bgcolor='#CCCCCC' style='font-size: 12px'><div align='center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'>&nbsp;Sr No </span></b></span></div><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'></span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align='center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'>&nbsp;IPL # </span></b></span></div><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align='center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;' > &nbsp; WO # </span ></b ></span ></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Client</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td>  <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Address</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; WorkType</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td><td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Contractor</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Coordinator</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td><td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Processor</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Field Complete Date</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Sent To Client Date</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td>   </tr>";

                                        foreach (var item in adreport[0])
                                        {
                                            var sr = 1 + index++;
                                            tbody += "<tr height ='30px'> <td style = 'text-align: center;'width='10%'>" + sr + "</td ><td style = 'text-align: center;'>" + item.IPLNO + "</td ><td style = 'text-align: center;'>" + item.workOrderNumber + "</td><td style = 'text-align: center;'>" + item.Client_Company_Name + "</td><td style = 'text-align: center;'>" + item.address1 + "%" + "</td><td style = 'text-align: center;'>" + item.WT_WorkType + "</td><td style = 'text-align: center;'>" + item.ContractorName + "</td><td style = 'text-align: center;'>" + item.CondinatorName + "</td><td style = 'text-align: center;'>" + item.ProcessorName + "</td><td style = 'text-align: center;'>" + item.Field_complete_date + "</td><td style = 'text-align: center;'>" + item.SentToClient_date + "</td></tr>";
                                            //tbody += "<tr height ='30px'> <td style = 'text-align: center;'width='10'>" + sr + "</td ><td style = 'text-align: center;'>" + item.Client_Invoice_Number + "</td><td style = 'text-align: center;'>" + item.IPLNO + "</td><td style = 'text-align: center;'>" + item.ContractorName + "</td><td style = 'text-align: center;'>" + item.workOrderNumber + "</td ><td style = 'text-align: center;'>" + item.address1 + "</td><td style = 'text-align: center;'>" + item.WT_WorkType + "</td><td style = 'text-align: center;'>" + item.Client_Company_Name + "</td><td style = 'text-align: center;'>" + item.Client_InvoiceTotal + "</td><td style = 'text-align: center;'>" + item.Client_Pay_Amount + "</td></tr>";
                                        }
                                        break;
                                    }

                                case 2:
                                    {
                                        var adreport = GetAdvanceProTimeReportDetails(model);
                                        theader += "<table width = '100%' border='1' cellpadding='1' cellspacing='0' bgcolor='#efefef'>";
                                        table.Append(theader);
                                        tbody += "<tr> <td  bgcolor = '#CCCCCC' style='text-align:center;height:50px;  border: none;'><span class='MsoNormal1' style='text-align:center; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px'><b><u> </u></b></span>  <div align='center;' style='font - family: Verdana, Arial, Helvetica, sans - serif; font - size: 10px; '>    <span class='MsoNormal1' style='text - align:center; font - size: 35px'><b><u>Advanced Reports</u></b></span></div> </td></tr>";
                                        tbody += "<tr><td ><table width='100%'  border='0'> <tr height ='30px'><td width='10%' bgcolor='#CCCCCC' style='font-size: 12px'><div align='center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'>&nbsp;Sr No </span></b></span></div><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'></span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align='center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'>&nbsp;IPL # </span></b></span></div><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align='center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;' > &nbsp; WO # </span ></b ></span ></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Client</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td>  <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Address</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; WorkType</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td><td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Contractor</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Coordinator</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td><td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Processor</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Field Complete Date</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Office Approved</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td>   </tr>";

                                        foreach (var item in adreport[0])
                                        {
                                            var sr = 1 + index++;
                                            tbody += "<tr height ='30px'> <td style = 'text-align: center;'width='10%'>" + sr + "</td ><td style = 'text-align: center;'>" + item.IPLNO + "</td ><td style = 'text-align: center;'>" + item.workOrderNumber + "</td><td style = 'text-align: center;'>" + item.Client_Company_Name + "</td><td style = 'text-align: center;'>" + item.address1 + "%" + "</td><td style = 'text-align: center;'>" + item.WT_WorkType + "</td><td style = 'text-align: center;'>" + item.ContractorName + "</td><td style = 'text-align: center;'>" + item.CondinatorName + "</td><td style = 'text-align: center;'>" + item.ProcessorName + "</td><td style = 'text-align: center;'>" + item.Field_complete_date + "</td><td style = 'text-align: center;'>" + item.OfficeApproved_date + "</td></tr>";
                                            //tbody += "<tr height ='30px'> <td style = 'text-align: center;'width='10'>" + sr + "</td ><td style = 'text-align: center;'>" + item.Client_Invoice_Number + "</td><td style = 'text-align: center;'>" + item.IPLNO + "</td><td style = 'text-align: center;'>" + item.ContractorName + "</td><td style = 'text-align: center;'>" + item.workOrderNumber + "</td ><td style = 'text-align: center;'>" + item.address1 + "</td><td style = 'text-align: center;'>" + item.WT_WorkType + "</td><td style = 'text-align: center;'>" + item.Client_Company_Name + "</td><td style = 'text-align: center;'>" + item.Client_InvoiceTotal + "</td><td style = 'text-align: center;'>" + item.Client_Pay_Amount + "</td></tr>";
                                        }
                                        break;
                                    }

                                case 3:
                                    {
                                        var adreport = GetAdvanceProTimeReportDetails(model);
                                        theader += "<table width = '100%' border='1' cellpadding='1' cellspacing='0' bgcolor='#efefef'>";
                                        table.Append(theader);
                                        tbody += "<tr> <td  bgcolor = '#CCCCCC' style='text-align:center;height:50px;  border: none;'><span class='MsoNormal1' style='text-align:center; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px'><b><u> </u></b></span>  <div align='center;' style='font - family: Verdana, Arial, Helvetica, sans - serif; font - size: 10px; '>    <span class='MsoNormal1' style='text - align:center; font - size: 35px'><b><u>Advanced Reports</u></b></span></div> </td></tr>";
                                        tbody += "<tr><td ><table width='100%'  border='0'> <tr height ='30px'><td width='10%' bgcolor='#CCCCCC' style='font-size: 12px'><div align='center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'>&nbsp;Sr No </span></b></span></div><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'></span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align='center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'>&nbsp;IPL # </span></b></span></div><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align='center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;' > &nbsp; WO # </span ></b ></span ></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Client</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td>  <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Address</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; WorkType</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td><td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Contractor</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Coordinator</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td><td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Processor</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Office Approved</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; SentToClient_date</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td>   </tr>";

                                        foreach (var item in adreport[0])
                                        {
                                            var sr = 1 + index++;
                                            tbody += "<tr height ='30px'> <td style = 'text-align: center;'width='10%'>" + sr + "</td ><td style = 'text-align: center;'>" + item.IPLNO + "</td ><td style = 'text-align: center;'>" + item.workOrderNumber + "</td><td style = 'text-align: center;'>" + item.Client_Company_Name + "</td><td style = 'text-align: center;'>" + item.address1 + "%" + "</td><td style = 'text-align: center;'>" + item.WT_WorkType + "</td><td style = 'text-align: center;'>" + item.ContractorName + "</td><td style = 'text-align: center;'>" + item.CondinatorName + "</td><td style = 'text-align: center;'>" + item.ProcessorName + "</td><td style = 'text-align: center;'>" + item.OfficeApproved_date + "</td><td style = 'text-align: center;'>" + item.SentToClient_date + "</td></tr>";
                                            //tbody += "<tr height ='30px'> <td style = 'text-align: center;'width='10'>" + sr + "</td ><td style = 'text-align: center;'>" + item.Client_Invoice_Number + "</td><td style = 'text-align: center;'>" + item.IPLNO + "</td><td style = 'text-align: center;'>" + item.ContractorName + "</td><td style = 'text-align: center;'>" + item.workOrderNumber + "</td ><td style = 'text-align: center;'>" + item.address1 + "</td><td style = 'text-align: center;'>" + item.WT_WorkType + "</td><td style = 'text-align: center;'>" + item.Client_Company_Name + "</td><td style = 'text-align: center;'>" + item.Client_InvoiceTotal + "</td><td style = 'text-align: center;'>" + item.Client_Pay_Amount + "</td></tr>";
                                        }
                                        break;
                                    }

                                case 4:
                                    {
                                        var adreport = GetAdvanceProTimeReportDetails(model);
                                        theader += "<table width = '100%' border='1' cellpadding='1' cellspacing='0' bgcolor='#efefef'>";
                                        table.Append(theader);
                                        tbody += "<tr> <td  bgcolor = '#CCCCCC' style='text-align:center;height:50px;  border: none;'><span class='MsoNormal1' style='text-align:center; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px'><b><u> </u></b></span>  <div align='center;' style='font - family: Verdana, Arial, Helvetica, sans - serif; font - size: 10px; '>    <span class='MsoNormal1' style='text - align:center; font - size: 35px'><b><u>Advanced Reports</u></b></span></div> </td></tr>";
                                        tbody += "<tr><td ><table width='100%'  border='0'> <tr height ='30px'><td width='10%' bgcolor='#CCCCCC' style='font-size: 12px'><div align='center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'>&nbsp;Sr No </span></b></span></div><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'></span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align='center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'>&nbsp;IPL # </span></b></span></div><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align='center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;' > &nbsp; WO # </span ></b ></span ></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Client</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td>  <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Address</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; WorkType</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td><td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Contractor</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Coordinator</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td><td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Processor</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Assigned Date</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Field Complete Date</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td>   </tr>";

                                        foreach (var item in adreport[0])
                                        {
                                            var sr = 1 + index++;
                                            tbody += "<tr height ='30px'> <td style = 'text-align: center;'width='10%'>" + sr + "</td ><td style = 'text-align: center;'>" + item.IPLNO + "</td ><td style = 'text-align: center;'>" + item.workOrderNumber + "</td><td style = 'text-align: center;'>" + item.Client_Company_Name + "</td><td style = 'text-align: center;'>" + item.address1 + "%" + "</td><td style = 'text-align: center;'>" + item.WT_WorkType + "</td><td style = 'text-align: center;'>" + item.ContractorName + "</td><td style = 'text-align: center;'>" + item.CondinatorName + "</td><td style = 'text-align: center;'>" + item.ProcessorName + "</td><td style = 'text-align: center;'>" + item.Assigned_date + "</td><td style = 'text-align: center;'>" + item.Field_complete_date + "</td></tr>";
                                            //tbody += "<tr height ='30px'> <td style = 'text-align: center;'width='10'>" + sr + "</td ><td style = 'text-align: center;'>" + item.Client_Invoice_Number + "</td><td style = 'text-align: center;'>" + item.IPLNO + "</td><td style = 'text-align: center;'>" + item.ContractorName + "</td><td style = 'text-align: center;'>" + item.workOrderNumber + "</td ><td style = 'text-align: center;'>" + item.address1 + "</td><td style = 'text-align: center;'>" + item.WT_WorkType + "</td><td style = 'text-align: center;'>" + item.Client_Company_Name + "</td><td style = 'text-align: center;'>" + item.Client_InvoiceTotal + "</td><td style = 'text-align: center;'>" + item.Client_Pay_Amount + "</td></tr>";
                                        }
                                        break;
                                    }
                                case 5:
                                    {
                                        var adreport = GetAdvanceProTimeReportDetails(model);
                                        theader += "<table width = '100%' border='1' cellpadding='1' cellspacing='0' bgcolor='#efefef'>";
                                        table.Append(theader);
                                        tbody += "<tr> <td  bgcolor = '#CCCCCC' style='text-align:center;height:50px;  border: none;'><span class='MsoNormal1' style='text-align:center; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px'><b><u> </u></b></span>  <div align='center;' style='font - family: Verdana, Arial, Helvetica, sans - serif; font - size: 10px; '>    <span class='MsoNormal1' style='text - align:center; font - size: 35px'><b><u>Advanced Reports</u></b></span></div> </td></tr>";
                                        tbody += "<tr><td ><table width='100%'  border='0'> <tr height ='30px'><td width='10%' bgcolor='#CCCCCC' style='font-size: 12px'><div align='center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'>&nbsp;Sr No </span></b></span></div><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'></span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align='center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'>&nbsp;IPL # </span></b></span></div><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align='center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;' > &nbsp; WO # </span ></b ></span ></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Client</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td>  <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Address</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; WorkType</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td><td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Contractor</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Coordinator</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td><td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Processor</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Sent To Client Date</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Complete Date</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td>   </tr>";

                                        foreach (var item in adreport[0])
                                        {
                                            var sr = 1 + index++;
                                            tbody += "<tr height ='30px'> <td style = 'text-align: center;'width='10%'>" + sr + "</td ><td style = 'text-align: center;'>" + item.IPLNO + "</td ><td style = 'text-align: center;'>" + item.workOrderNumber + "</td><td style = 'text-align: center;'>" + item.Client_Company_Name + "</td><td style = 'text-align: center;'>" + item.address1 + "%" + "</td><td style = 'text-align: center;'>" + item.WT_WorkType + "</td><td style = 'text-align: center;'>" + item.ContractorName + "</td><td style = 'text-align: center;'>" + item.CondinatorName + "</td><td style = 'text-align: center;'>" + item.ProcessorName + "</td><td style = 'text-align: center;'>" + item.SentToClient_date + "</td><td style = 'text-align: center;'>" + item.Complete_Date + "</td></tr>";
                                            //tbody += "<tr height ='30px'> <td style = 'text-align: center;'width='10'>" + sr + "</td ><td style = 'text-align: center;'>" + item.Client_Invoice_Number + "</td><td style = 'text-align: center;'>" + item.IPLNO + "</td><td style = 'text-align: center;'>" + item.ContractorName + "</td><td style = 'text-align: center;'>" + item.workOrderNumber + "</td ><td style = 'text-align: center;'>" + item.address1 + "</td><td style = 'text-align: center;'>" + item.WT_WorkType + "</td><td style = 'text-align: center;'>" + item.Client_Company_Name + "</td><td style = 'text-align: center;'>" + item.Client_InvoiceTotal + "</td><td style = 'text-align: center;'>" + item.Client_Pay_Amount + "</td></tr>";
                                        }
                                        break;
                                    }

                            }
                                      

                            break;
                        }
                    default:
                        {
                            var adreport = GetAdvanceReportDetails(model);
                            theader += "<table width = '100%' border='1' cellpadding='1' cellspacing='0' bgcolor='#efefef'>";
                            table.Append(theader);
                            tbody += "<tr> <td  bgcolor = '#CCCCCC' style='text-align:center;height:50px;  border: none;'><span class='MsoNormal1' style='text-align:center; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px'><b><u> </u></b></span>  <div align='center;' style='font - family: Verdana, Arial, Helvetica, sans - serif; font - size: 10px; '>    <span class='MsoNormal1' style='text - align:center; font - size: 35px'><b><u>Advanced Reports</u></b></span></div> </td></tr>";
                            //tbody += "<tr><td ><table width='100%'  border='0'> <tr height ='30px'><td width='10%' bgcolor='#CCCCCC' style='font-size: 12px'><div align='center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'>&nbsp;Sr No </span></b></span></div><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'></span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align='center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'>&nbsp;WO # </span></b></span></div><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align='center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;' > &nbsp; Status </span ></b ></span ></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; IPL#</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td>  <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Contractor</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Address</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td><td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; WorkType</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Client</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td><td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Due Date</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Coordinator</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Processor</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td>   </tr>";
                            tbody += "<tr><td ><table width='100%'  border='0'> <tr height ='30px'><td width='10%' bgcolor='#CCCCCC' style='font-size: 12px'><div align='center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'>&nbsp;Sr No </span></b></span></div><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'></span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align='center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'>&nbsp;Invoice # </span></b></span></div><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align='center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;' > &nbsp; IPL# </span ></b ></span ></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Contractor </span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td>  <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; WO #</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Address</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td><td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; WorkType</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Client</span></b></span></div><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Client Total</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Client Pay Amount</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td>   </tr>";
                            foreach (var item in adreport[0])
                            {
                                var sr = 1 + index++;
                                //tbody += "<tr height ='30px'> <td style = 'text-align: center;'width='10%'>" + sr + "</td ><td style = 'text-align: center;'>" + item.workOrderNumber + "</td ><td style = 'text-align: center;'>" + item.Status_Name + "</td><td style = 'text-align: center;'>" + item.IPLNO + "</td><td style = 'text-align: center;'>" + item.ContractorName + "%" + "</td><td style = 'text-align: center;'>" + item.address1 + "</td><td style = 'text-align: center;'>" + item.WT_WorkType + "</td><td style = 'text-align: center;'>" + item.Client_Company_Name + "</td><td style = 'text-align: center;'>" + item.dueDate + "</td><td style = 'text-align: center;'>" + item.CordinatorName + "</td><td style = 'text-align: center;'>" + item.ProcessorName + "</td></tr>";
                                tbody += "<tr height ='30px'> <td style = 'text-align: center;'width='10'>" + sr + "</td ><td style = 'text-align: center;'>" + item.Client_Invoice_Number + "</td><td style = 'text-align: center;'>" + item.IPLNO + "</td><td style = 'text-align: center;'>" + item.ContractorName + "</td><td style = 'text-align: center;'>" + item.workOrderNumber + "</td ><td style = 'text-align: center;'>" + item.address1 + "</td><td style = 'text-align: center;'>" + item.WT_WorkType + "</td><td style = 'text-align: center;'>" + item.Client_Company_Name + "</td><td style = 'text-align: center;'>" + item.Client_InvoiceTotal + "</td><td style = 'text-align: center;'>" + item.Client_Pay_Amount + "</td></tr>";
                            }

                            break;
                        }
                }
                tbody += "</table></td></tr>";
                
                tfooter += "</table >";
                tbody += "</tbody>";
                table.Append(tbody);



             
                html.Append(table.ToString());
                byte[] pdffile = GenerateRuntimePDF(html.ToString());
                custom.Data = pdffile;
                custom.HttpStatusCode = System.Net.HttpStatusCode.OK;
                return custom;
            }
            catch (Exception ex)
            {
                custom.Message = ex.Message;
                custom.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                return custom;
            }
        }
    }
}