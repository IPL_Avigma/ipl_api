﻿using Avigma.Repository.Lib;
using IPLApp.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPLApp.Repository.data
{
    public class MenuMasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public List<dynamic> AddMenuData(MenuMasterDTO model)
        {
           
            string insertProcedure = "[CreateUpdateMenuMaster]";
           
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Ipre_MenuID", 1 + "#bigint#" + model.Ipre_MenuID);
                input_parameters.Add("@Ipre_MenuName", 1 + "#nvarchar#" + model.Ipre_MenuName);
                input_parameters.Add("@Ipre_PageName", 1 + "#nvarchar#" + model.Ipre_PageName);
                input_parameters.Add("@Ipre_PageUrl", 1 + "#nvarchar#" + model.Ipre_PageUrl);
                input_parameters.Add("@Ipre_IsActive", 1 + "#bit#" + model.Ipre_IsActive);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Ipre_MenuID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

               
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return obj.SqlCRUD(insertProcedure, input_parameters);



        }

        private DataSet GetMenuMasterdata(MenuMasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[GetMenuMaster]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Ipre_MenuID", 1 + "#bigint#" + model.Ipre_MenuID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        private DataSet GetGroupMenuMaster(MenuMasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[GetGroupMenuMaster]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Ipre_MenuID", 1 + "#bigint#" + model.Ipre_MenuID);
                input_parameters.Add("@Mgr_Group_Id", 1 + "#bigint#" + model.Mgr_Group_Id);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> MenuSelectForRole(MenuMasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            List<dynamic> finalobjDynamic = new List<dynamic>();
            try
            {
                GroupMastersDTO groupMastersDTO = new GroupMastersDTO();
                GroupMastersData groupMastersData = new GroupMastersData();
                groupMastersDTO.Grp_pkeyID = model.Mgr_Group_Id;
                groupMastersDTO.Type = 2;
                finalobjDynamic.Add(groupMastersData.GetGroupDetails(groupMastersDTO));

                #region comment
                //var myEnumerablex = ds.Tables[0].AsEnumerable();
                //List<CommanLoginMenu> dynareturn =
                //    (from item in myEnumerablex
                //     select new CommanLoginMenu
                //     {
                //         // MenuId = item.Field<Int64?>("MenuId"),
                //         MenuName = item.Field<string>("MenuName"),
                //         URL = item.Field<string>("URL"),
                //         IconTag = item.Field<string>("IconTag"),
                //     }).ToList();
                //objDynamic.Add(dynareturn);
                #endregion
                DataSet ds = GetGroupMenuMaster(model);
                


                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    List<ParentLoginMenu> lstparentLoginMenu = new List<ParentLoginMenu>();
                    ParentLoginMenu parentLoginMenu = new ParentLoginMenu();
                    int MenuMasterID = Convert.ToInt32(ds.Tables[0].Rows[i]["Ipre_MenuMasterID"].ToString());

                    if (MenuMasterID == 0)
                    {
                        List<CommanLoginMenu> lstcommanLoginMenu = new List<CommanLoginMenu>();
                        parentLoginMenu.MenuName = ds.Tables[0].Rows[i]["Ipre_MenuName"].ToString();
                       // parentLoginMenu.IconTag = ds.Tables[0].Rows[i]["Ipre_IconTag"].ToString();
                        parentLoginMenu.URL = ds.Tables[0].Rows[i]["Ipre_PageUrl"].ToString();
                        parentLoginMenu.MenuId = Convert.ToInt64(ds.Tables[0].Rows[i]["Ipre_MenuID"].ToString());
                        var rows = ds.Tables[0].Select("Ipre_MenuMasterID = " + parentLoginMenu.MenuId + "");
                        foreach (var row in rows)
                        {
                            CommanLoginMenu commanLoginMenu = new CommanLoginMenu();
                            commanLoginMenu.MenuId = Convert.ToInt64(row["Ipre_MenuID"].ToString());
                            commanLoginMenu.MenuName = row["Ipre_MenuName"].ToString();
                            //commanLoginMenu.IconTag = row["Ipre_IconTag"].ToString();
                            commanLoginMenu.URL = row["Ipre_PageUrl"].ToString();

                            commanLoginMenu.IsAssignedMenu = false;
                            if (ds.Tables[1].Rows.Count>0)
                            {
                                for (int j = 0; j < ds.Tables[1].Rows.Count; j++)
                                {
                                    Int64 menuID = Convert.ToInt64(ds.Tables[1].Rows[j]["Mgr_Menu_Id"].ToString());
                                    if (commanLoginMenu.MenuId == menuID)
                                    {
                                        commanLoginMenu.IsAssignedMenu = true;
                                        break;
                                    }

                                }
                            }
                            else
                            {
                                commanLoginMenu.IsAssignedMenu = false;
                            }
                            //if (!string.IsNullOrEmpty(row["Mgr_Group_Id"].ToString()))
                            //{

                            //    Int64 GroupID = Convert.ToInt64(row["Mgr_Group_Id"].ToString());
                            //    if (GroupID == model.Mgr_Group_Id)
                            //    {
                            //        commanLoginMenu.IsAssignedMenu = true;
                            //    }
                            //    else
                            //    {
                            //        commanLoginMenu.IsAssignedMenu = false;
                            //    }
                            //}
                            //else
                            //{
                            //    commanLoginMenu.IsAssignedMenu = false;
                            //}


                            lstcommanLoginMenu.Add(commanLoginMenu);



                        }
                        parentLoginMenu.ChildMenu = lstcommanLoginMenu;
                        lstparentLoginMenu.Add(parentLoginMenu);
                        objDynamic.Add(lstparentLoginMenu);
                    }



                }
                finalobjDynamic.Add(objDynamic);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.Message);
            }
            return finalobjDynamic;
        }


        public List<dynamic> GetMenuDetails(MenuMasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetMenuMasterdata(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<MenuMasterDTO> MenuDetails =
                   (from item in myEnumerableFeaprd
                    select new MenuMasterDTO
                    {
                        Ipre_MenuID = item.Field<Int64>("Ipre_MenuID"),
                        Ipre_MenuName = item.Field<String>("Ipre_MenuName"),
                        Ipre_PageName = item.Field<String>("Ipre_PageName"),
                        Ipre_PageUrl = item.Field<String>("Ipre_PageUrl"),
                        Ipre_IsActive = item.Field<Boolean?>("Ipre_IsActive"),
                       

                    }).ToList();

                objDynamic.Add(MenuDetails);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}