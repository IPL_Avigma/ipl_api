﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class PCR_NFR_Dump_Receipt_Form_Data
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public List<dynamic> CreateUpdate_Nfr_Dump_ReceiptData(PCR_NFR_Dump_Receipt_Form_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objclntData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_PCR_NFR_Dump_Receipt]";
            PCR_LoggerMessage pCR_Logger = new PCR_LoggerMessage();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@NF_PkeyID", 1 + "#bigint#" + model.NF_PkeyID);
                input_parameters.Add("@NF_NFR_WO_ID", 1 + "#bigint#" + model.NF_NFR_WO_ID);
                input_parameters.Add("@NF_NFR_Dump_Receipt", 1 + "#nvarchar#" + model.NF_NFR_Dump_Receipt);
                input_parameters.Add("@NF_IsActive", 1 + "#bit#" + model.NF_IsActive);
                input_parameters.Add("@NF_IsDelete", 1 + "#bit#" + model.NF_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@fwo_pkyeId", 1 + "#bigint#" + model.fwo_pkyeId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@NF_PkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    pCR_Logger.PCR_PkeyID = "0";
                    pCR_Logger.Status = "0";
                    pCR_Logger.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    pCR_Logger.PCR_PkeyID = Convert.ToString(objData[0]);
                    pCR_Logger.Status = Convert.ToString(objData[1]);


                }
                objclntData.Add(pCR_Logger);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objclntData;



        }

        private DataSet GetNfr_Dump_ReceiptData(PCR_NFR_Dump_Receipt_Form_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_PCR_NFR_Dump_Receipt]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@NF_PkeyID", 1 + "#bigint#" + model.NF_PkeyID);
                input_parameters.Add("@NF_NFR_WO_ID", 1 + "#bigint#" + model.NF_NFR_WO_ID);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ds;
        }

        public List<dynamic> GetNfr_Dump_ReceipDetails(PCR_NFR_Dump_Receipt_Form_DTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetNfr_Dump_ReceiptData(model);

                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                }

                //var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                //List<PCR_NFR_Dump_Receipt_Form_DTO> pcrDTP =
                //   (from item in myEnumerableFeaprd
                //    select new PCR_NFR_Dump_Receipt_Form_DTO
                //    {
                //        NF_PkeyID = item.Field<Int64>("NF_PkeyID"),
                //        NF_NFR_WO_ID = item.Field<Int64>("NF_NFR_WO_ID"),
                //        NF_NFR_CompanyId = item.Field<Int64>("NF_NFR_CompanyId"),
                //        NF_NFR_Dump_Receipt = item.Field<String>("NF_NFR_Dump_Receipt"),
                //        NF_IsActive = item.Field<Boolean?>("NF_IsActive"),
                //    }).ToList();

                //objDynamic.Add(pcrDTP);

                //if (ds.Tables.Count > 1)
                //{
                //    var pcrHistory = ds.Tables[0].AsEnumerable();
                //    List<PCR_NFR_Dump_Receipt_Form_DTO> pcrhistoryDTP =
                //       (from item in pcrHistory
                //        select new PCR_NFR_Dump_Receipt_Form_DTO
                //        {
                //            NF_PkeyID = item.Field<Int64>("NF_PkeyID"),
                //            NF_NFR_WO_ID = item.Field<Int64>("NF_NFR_WO_ID"),
                //            NF_NFR_CompanyId = item.Field<Int64>("NF_NFR_CompanyId"),
                //            NF_NFR_Dump_Receipt = item.Field<String>("NF_NFR_Dump_Receipt"),
                //            NF_IsActive = item.Field<Boolean?>("NF_IsActive"),
                //        }).ToList();

                //    objDynamic.Add(pcrhistoryDTP);
                //}
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}