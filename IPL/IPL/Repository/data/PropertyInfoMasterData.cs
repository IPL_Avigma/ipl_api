﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class PropertyInfoMasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public List<dynamic> AddPCR_Propert_Info_Data(PropertyInfoMasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objclntData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_PCR_PropertyInfo_Master]";
            PropertyInfoDTO propertyInfoDTO = new PropertyInfoDTO();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@PCR_Pro_PkeyID", 1 + "#bigint#" + model.PCR_Pro_PkeyID);
                input_parameters.Add("@PCR_Prop_WO_ID", 1 + "#bigint#" + model.PCR_Prop_WO_ID);
                input_parameters.Add("@PCR_Prop_MasterID", 1 + "#bigint#" + model.PCR_Prop_MasterID);
                input_parameters.Add("@PCR_Prop_ValType", 1 + "#int#" + model.PCR_Prop_ValType);
                input_parameters.Add("@PCR_Prop_ForSale", 1 + "#varchar#" + model.PCR_Prop_ForSale);
                input_parameters.Add("@PCR_Prop_Sold", 1 + "#varchar#" + model.PCR_Prop_Sold);
                input_parameters.Add("@PCR_Prop_Broker_Phone", 1 + "#varchar#" + model.PCR_Prop_Broker_Phone);
                input_parameters.Add("@PCR_Prop_Broker_Name", 1 + "#varchar#" + model.PCR_Prop_Broker_Name);
                input_parameters.Add("@PCR_Prop_Maintained", 1 + "#varchar#" + model.PCR_Prop_Maintained);
                //input_parameters.Add("@PCR_Prop_Maintained_Not_Maintained", 1 + "#bit#" + model.PCR_Prop_Maintained_Not_Maintained);
                //input_parameters.Add("@PCR_Prop_Maintained_By_Broker", 1 + "#bit#" + model.PCR_Prop_Maintained_By_Broker);
                //input_parameters.Add("@PCR_Prop_Maintained_ByBroker_HOA", 1 + "#bit#" + model.PCR_Prop_Maintained_ByBroker_HOA);
                //input_parameters.Add("@PCR_Prop_Maintained_ByHOA", 1 + "#bit#" + model.PCR_Prop_Maintained_ByHOA);
                input_parameters.Add("@PCR_Prop_Maintained_ByOther", 1 + "#varchar#" + model.PCR_Prop_Maintained_ByOther);
                input_parameters.Add("@PCR_Prop_Maintained_Items_Utilities", 1 + "#bit#" + model.PCR_Prop_Maintained_Items_Utilities);
                input_parameters.Add("@PCR_Prop_Maintained_Items_Grass", 1 + "#bit#" + model.PCR_Prop_Maintained_Items_Grass);
                input_parameters.Add("@PCR_Prop_Maintained_Items_Snow_Removal", 1 + "#bit#" + model.PCR_Prop_Maintained_Items_Snow_Removal);
                input_parameters.Add("@PCR_Prop_Maintained_Items_Interior_Repaiars", 1 + "#bit#" + model.PCR_Prop_Maintained_Items_Interior_Repaiars);
                input_parameters.Add("@PCR_Prop_Maintained_Items_Exterior_Repairs", 1 + "#bit#" + model.PCR_Prop_Maintained_Items_Exterior_Repairs);
                input_parameters.Add("@PCR_Prop_Active_Listing", 1 + "#varchar#" + model.PCR_Prop_Active_Listing);
                input_parameters.Add("@PCR_Prop_Basement_Present", 1 + "#varchar#" + model.PCR_Prop_Basement_Present);
                input_parameters.Add("@PCR_OurBuildings_Garages", 1 + "#varchar#" + model.PCR_OurBuildings_Garages);
                input_parameters.Add("@PCR_OurBuildings_Sheds", 1 + "#varchar#" + model.PCR_OurBuildings_Sheds);
                input_parameters.Add("@PCR_OurBuildings_Caports", 1 + "#varchar#" + model.PCR_OurBuildings_Caports);
                input_parameters.Add("@PCR_OurBuildings_Bams", 1 + "#varchar#" + model.PCR_OurBuildings_Bams);
                input_parameters.Add("@PCR_OurBuildings_Pool_House", 1 + "#varchar#" + model.PCR_OurBuildings_Pool_House);
                input_parameters.Add("@PCR_OurBuildings_Other_Building", 1 + "#varchar#" + model.PCR_OurBuildings_Other_Building);
                input_parameters.Add("@PCR_Prop_Property_Type_Vacant_Land", 1 + "#varchar#" + model.PCR_Prop_Property_Type_Vacant_Land);
                input_parameters.Add("@PCR_Prop_Property_Type_Single_Family", 1 + "#bit#" + model.PCR_Prop_Property_Type_Single_Family);
                input_parameters.Add("@PCR_Prop_Property_Type_Multi_Family", 1 + "#bit#" + model.PCR_Prop_Property_Type_Multi_Family);
                input_parameters.Add("@PCR_Prop_Property_Type_Mobile_Home", 1 + "#bit#" + model.PCR_Prop_Property_Type_Mobile_Home);
                input_parameters.Add("@PCR_Prop_Property_Type_Condo", 1 + "#bit#" + model.PCR_Prop_Property_Type_Condo);
                input_parameters.Add("@PCR_Prop_Permit_Required", 1 + "#varchar#" + model.PCR_Prop_Permit_Required);
                input_parameters.Add("@PCR_Prop_Permit_Number", 1 + "#varchar#" + model.PCR_Prop_Permit_Number);
                input_parameters.Add("@PCR_Prop_Condo_Association_Property", 1 + "#varchar#" + model.PCR_Prop_Condo_Association_Property);
                input_parameters.Add("@PCR_HOA_Name", 1 + "#varchar#" + model.PCR_HOA_Name);
                input_parameters.Add("@PCR_HOA_Phone", 1 + "#varchar#" + model.PCR_HOA_Phone);
                //input_parameters.Add("@PCR_Prop_Condo_Association_Property", 1 + "#bit#" + model.PCR_Prop_Condo_Association_Property);
                input_parameters.Add("@PCR_Prop_No_Of_Unit", 1 + "#varchar#" + model.PCR_Prop_No_Of_Unit);
                input_parameters.Add("@PCR_Prop_Common_Entry", 1 + "#varchar#" + model.PCR_Prop_Common_Entry);
                input_parameters.Add("@PCR_Prop_Garage", 1 + "#varchar#" + model.PCR_Prop_Garage);

                input_parameters.Add("@PCR_Prop_Unit1", 1 + "#varchar#" + model.PCR_Prop_Unit1);
                input_parameters.Add("@PCR_Prop_Unit1_Occupied", 1 + "#varchar#" + model.PCR_Prop_Unit1_Occupied);
                input_parameters.Add("@PCR_Prop_Unit2", 1 + "#varchar#" + model.PCR_Prop_Unit2);
                input_parameters.Add("@PCR_Prop_Unit2_Occupied", 1 + "#varchar#" + model.PCR_Prop_Unit2_Occupied);
                input_parameters.Add("@PCR_Prop_Unit3", 1 + "#varchar#" + model.PCR_Prop_Unit3);
                input_parameters.Add("@PCR_Prop_Unit3_Occupied", 1 + "#varchar#" + model.PCR_Prop_Unit3_Occupied);
                input_parameters.Add("@PCR_Prop_Unit4", 1 + "#varchar#" + model.PCR_Prop_Unit4);
                input_parameters.Add("@PCR_Prop_Unit4_Occupied", 1 + "#varchar#" + model.PCR_Prop_Unit4_Occupied);
                input_parameters.Add("@PCR_Prop_Property_Vacant", 1 + "#varchar#" + model.PCR_Prop_Property_Vacant);
                input_parameters.Add("@PCR_Prop_Property_Vacant_Notes", 1 + "#varchar#" + model.PCR_Prop_Property_Vacant_Notes);

                //input_parameters.Add("@PCR_Prop_Garage_Attached", 1 + "#bit#" + model.PCR_Prop_Garage_Attached);
                //input_parameters.Add("@PCR_Prop_Garage_Carport", 1 + "#bit#" + model.PCR_Prop_Garage_Carport);
                //input_parameters.Add("@PCR_Prop_Garage_Detached", 1 + "#bit#" + model.PCR_Prop_Garage_Detached);
                //input_parameters.Add("@PCR_Prop_Garage_None", 1 + "#bit#" + model.PCR_Prop_Garage_None);
                //input_parameters.Add("@PCR_Prop_Property_Vacant_Yes", 1 + "#bit#" + model.PCR_Prop_Property_Vacant_Yes);
                //input_parameters.Add("@PCR_Prop_Property_Vacant_No", 1 + "#bit#" + model.PCR_Prop_Property_Vacant_No);
                //input_parameters.Add("@PCR_Prop_Property_Vacant_Land", 1 + "#bit#" + model.PCR_Prop_Property_Vacant_Land);
                //input_parameters.Add("@PCR_Prop_Property_Vacant_Bad_Address", 1 + "#bit#" + model.PCR_Prop_Property_Vacant_Bad_Address);
                //input_parameters.Add("@PCR_Prop_Property_Vacant_Bad_Address_Commend", 1 + "#varchar#" + model.PCR_Prop_Property_Vacant_Bad_Address_Commend);
                //input_parameters.Add("@PCR_Prop_Property_Vacant_Partial", 1 + "#bit#" + model.PCR_Prop_Property_Vacant_Partial);
                //input_parameters.Add("@PCR_Prop_Occupancy_Verified_Contact_Owner", 1 + "#bit#" + model.PCR_Prop_Occupancy_Verified_Contact_Owner);
                input_parameters.Add("@PCR_Prop_Occupancy_Verified_Personal_Visible", 1 + "#bit#" + model.PCR_Prop_Occupancy_Verified_Personal_Visible);
                input_parameters.Add("@PCR_Prop_Occupancy_Verified_Neighbor", 1 + "#bit#" + model.PCR_Prop_Occupancy_Verified_Neighbor);
                input_parameters.Add("@PCR_Prop_Occupancy_Verified_Utilities_On", 1 + "#bit#" + model.PCR_Prop_Occupancy_Verified_Utilities_On);
                input_parameters.Add("@PCR_Prop_Occupancy_Verified_Visual", 1 + "#bit#" + model.PCR_Prop_Occupancy_Verified_Visual);
                input_parameters.Add("@PCR_Prop_Occupancy_Verified_Direct_Con_Tenant", 1 + "#bit#" + model.PCR_Prop_Occupancy_Verified_Direct_Con_Tenant);
                input_parameters.Add("@PCR_Prop_Occupancy_Verified_Direct_Con_Mortgagor", 1 + "#bit#" + model.PCR_Prop_Occupancy_Verified_Direct_Con_Mortgagor);
                input_parameters.Add("@PCR_Prop_Occupancy_Verified_Direct_Con_Unknown", 1 + "#bit#" + model.PCR_Prop_Occupancy_Verified_Direct_Con_Unknown);
                input_parameters.Add("@PCR_Prop_Owner_Maintaining_Property", 1 + "#bit#" + model.PCR_Prop_Owner_Maintaining_Property);
                input_parameters.Add("@PCR_Prop_Other", 1 + "#bit#" + model.PCR_Prop_Other);
                input_parameters.Add("@PCR_Prop_Property", 1 + "#varchar#" + model.PCR_Prop_Property);
                input_parameters.Add("@PRC_Prop_IsActive", 1 + "#bit#" + model.PRC_Prop_IsActive);
                //input_parameters.Add("@PRC_Prop_IsDelete", 1 + "#bit#" + model.PRC_Prop_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@PRC_Prop_dateCompleted", 1 + "#datetime#" + model.PRC_Prop_dateCompleted);
                input_parameters.Add("@PRC_Prop_PropertyVacantBadAddressProvide_dtls", 1 + "#nvarchar#" + model.PRC_Prop_PropertyVacantBadAddressProvide_dtls);
                input_parameters.Add("@PRC_Prop_badAddress", 1 + "#nvarchar#" + model.PRC_Prop_badAddress);
                input_parameters.Add("@PRC_Prop_orderCompleted", 1 + "#nvarchar#" + model.PRC_Prop_orderCompleted);
                input_parameters.Add("@fwo_pkyeId", 1 + "#bigint#" + model.fwo_pkyeId);
                input_parameters.Add("@PCR_Pro_PkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    propertyInfoDTO.PCR_Pro_PkeyID = "0";
                    propertyInfoDTO.Status = "0";
                    propertyInfoDTO.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    propertyInfoDTO.PCR_Pro_PkeyID = Convert.ToString(objData[0]);
                    propertyInfoDTO.Status = Convert.ToString(objData[1]);


                }
                objclntData.Add(propertyInfoDTO);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objclntData;



        }

        private DataSet GetProperty_Info_Master(PropertyInfoMasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_PCR_Property_Info_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@PCR_Pro_PkeyID", 1 + "#bigint#" + model.PCR_Pro_PkeyID);
                input_parameters.Add("@PCR_Prop_WO_ID", 1 + "#bigint#" + model.PCR_Prop_WO_ID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetProperty_Info_Master_Details(PropertyInfoMasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetProperty_Info_Master(model);

                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                }


                ////Table[0]
                //var propertyInfo = DatasetToModel(ds.Tables[0]);
                //objDynamic.Add(propertyInfo);

                ////table [1] History 
                //if (ds.Tables.Count > 0)
                //{
                //    var pcrHistory = DatasetToModel(ds.Tables[1]);
                //    objDynamic.Add(pcrHistory);
            //}


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
        private List<PropertyInfoMasterDTO> DatasetToModel(DataTable table)
        {
            var myEnumerableFeaprd = table.AsEnumerable();
            List<PropertyInfoMasterDTO> propertyInfo =
               (from item in myEnumerableFeaprd
                select new PropertyInfoMasterDTO
                {
                    PCR_Pro_PkeyID = item.Field<Int64>("PCR_Pro_PkeyID"),
                    PCR_Prop_WO_ID = item.Field<Int64>("PCR_Prop_WO_ID"),
                    PCR_Prop_MasterID = item.Field<Int64>("PCR_Prop_MasterID"),
                    PCR_Prop_ValType = item.Field<int?>("PCR_Prop_ValType"),
                    PCR_Prop_ForSale = item.Field<string>("PCR_Prop_ForSale"),
                    PCR_Prop_Sold = item.Field<string>("PCR_Prop_Sold"),
                    PCR_Prop_Broker_Phone = item.Field<string>("PCR_Prop_Broker_Phone"),
                    PCR_Prop_Broker_Name = item.Field<string>("PCR_Prop_Broker_Name"),
                    PCR_Prop_Maintained = item.Field<string>("PCR_Prop_Maintained"),
                    PCR_Prop_Maintained_ByOther = item.Field<string>("PCR_Prop_Maintained_ByOther"),
                    PCR_Prop_Maintained_Items_Utilities = item.Field<Boolean?>("PCR_Prop_Maintained_Items_Utilities"),
                    PCR_Prop_Maintained_Items_Grass = item.Field<Boolean?>("PCR_Prop_Maintained_Items_Grass"),
                    PCR_Prop_Maintained_Items_Snow_Removal = item.Field<Boolean?>("PCR_Prop_Maintained_Items_Snow_Removal"),
                    PCR_Prop_Maintained_Items_Interior_Repaiars = item.Field<Boolean?>("PCR_Prop_Maintained_Items_Interior_Repaiars"),
                    PCR_Prop_Maintained_Items_Exterior_Repairs = item.Field<Boolean?>("PCR_Prop_Maintained_Items_Exterior_Repairs"),
                    PCR_Prop_Active_Listing = item.Field<string>("PCR_Prop_Active_Listing"),
                    PCR_Prop_Basement_Present = item.Field<string>("PCR_Prop_Basement_Present"),
                    PCR_OurBuildings_Garages = item.Field<string>("PCR_OurBuildings_Garages"),
                    PCR_OurBuildings_Sheds = item.Field<string>("PCR_OurBuildings_Sheds"),
                    PCR_OurBuildings_Caports = item.Field<string>("PCR_OurBuildings_Caports"),
                    PCR_OurBuildings_Bams = item.Field<string>("PCR_OurBuildings_Bams"),
                    PCR_OurBuildings_Pool_House = item.Field<string>("PCR_OurBuildings_Pool_House"),
                    PCR_OurBuildings_Other_Building = item.Field<string>("PCR_OurBuildings_Other_Building"),
                    PCR_Prop_Property_Type_Vacant_Land = item.Field<string>("PCR_Prop_Property_Type_Vacant_Land"),
                    PCR_Prop_Property_Type_Single_Family = item.Field<Boolean?>("PCR_Prop_Property_Type_Single_Family"),
                    PCR_Prop_Property_Type_Multi_Family = item.Field<Boolean?>("PCR_Prop_Property_Type_Multi_Family"),
                    PCR_Prop_Property_Type_Mobile_Home = item.Field<Boolean?>("PCR_Prop_Property_Type_Mobile_Home"),
                    PCR_Prop_Property_Type_Condo = item.Field<Boolean?>("PCR_Prop_Property_Type_Condo"),
                    PCR_Prop_Permit_Required = item.Field<string>("PCR_Prop_Permit_Required"),
                    PCR_Prop_Permit_Number = item.Field<string>("PCR_Prop_Permit_Number"),
                    PCR_Prop_Condo_Association_Property = item.Field<string>("PCR_Prop_Condo_Association_Property"),
                    PCR_HOA_Name = item.Field<string>("PCR_HOA_Name"),
                    PCR_HOA_Phone = item.Field<string>("PCR_HOA_Phone"),
                    PCR_Prop_No_Of_Unit = item.Field<string>("PCR_Prop_No_Of_Unit"),
                    PCR_Prop_Common_Entry = item.Field<string>("PCR_Prop_Common_Entry"),
                    PCR_Prop_Garage = item.Field<string>("PCR_Prop_Garage"),
                    PCR_Prop_Unit1 = item.Field<string>("PCR_Prop_Unit1"),
                    PCR_Prop_Unit1_Occupied = item.Field<string>("PCR_Prop_Unit1_Occupied"),
                    PCR_Prop_Unit2 = item.Field<string>("PCR_Prop_Unit2"),
                    PCR_Prop_Unit2_Occupied = item.Field<string>("PCR_Prop_Unit2_Occupied"),
                    PCR_Prop_Unit3 = item.Field<string>("PCR_Prop_Unit3"),
                    PCR_Prop_Unit3_Occupied = item.Field<string>("PCR_Prop_Unit3_Occupied"),
                    PCR_Prop_Unit4 = item.Field<string>("PCR_Prop_Unit4"),
                    PCR_Prop_Unit4_Occupied = item.Field<string>("PCR_Prop_Unit4_Occupied"),
                    PCR_Prop_Property_Vacant = item.Field<string>("PCR_Prop_Property_Vacant"),
                    PCR_Prop_Property_Vacant_Notes = item.Field<string>("PCR_Prop_Property_Vacant_Notes"),
                    PCR_Prop_Occupancy_Verified_Personal_Visible = item.Field<Boolean?>("PCR_Prop_Occupancy_Verified_Personal_Visible"),
                    PCR_Prop_Occupancy_Verified_Neighbor = item.Field<Boolean?>("PCR_Prop_Occupancy_Verified_Neighbor"),
                    PCR_Prop_Occupancy_Verified_Utilities_On = item.Field<Boolean?>("PCR_Prop_Occupancy_Verified_Utilities_On"),
                    PCR_Prop_Occupancy_Verified_Visual = item.Field<Boolean?>("PCR_Prop_Occupancy_Verified_Visual"),
                    PCR_Prop_Occupancy_Verified_Direct_Con_Tenant = item.Field<Boolean?>("PCR_Prop_Occupancy_Verified_Direct_Con_Tenant"),
                    PCR_Prop_Occupancy_Verified_Direct_Con_Mortgagor = item.Field<Boolean?>("PCR_Prop_Occupancy_Verified_Direct_Con_Mortgagor"),
                    PCR_Prop_Occupancy_Verified_Direct_Con_Unknown = item.Field<Boolean?>("PCR_Prop_Occupancy_Verified_Direct_Con_Unknown"),
                    PCR_Prop_Owner_Maintaining_Property = item.Field<Boolean?>("PCR_Prop_Owner_Maintaining_Property"),
                    PCR_Prop_Other = item.Field<Boolean?>("PCR_Prop_Other"),
                    PCR_Prop_Property = item.Field<string>("PCR_Prop_Property"),
                    PRC_Prop_IsActive = item.Field<Boolean?>("PRC_Prop_IsActive"),
                    PRC_Prop_dateCompleted = item.Field<DateTime?>("PRC_Prop_dateCompleted"),
                    PRC_Prop_badAddress = item.Field<String>("PRC_Prop_badAddress"),
                    PRC_Prop_orderCompleted = item.Field<String>("PRC_Prop_orderCompleted"),
                    PRC_Prop_PropertyVacantBadAddressProvide_dtls = item.Field<String>("PRC_Prop_PropertyVacantBadAddressProvide_dtls"),


                }).ToList();

            return propertyInfo;
        }
    }
}