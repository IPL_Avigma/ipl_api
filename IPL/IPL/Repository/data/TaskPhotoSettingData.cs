﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class TaskPhotoSettingData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public List<dynamic> AddUpdateDeletedTaskPhotoSetting(TaskPhotoSetting model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_TaskPhotoSettingMaster]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();


            try
            {
                input_parameters.Add("@TPS_pkeyID", 1 + "#bigint#" + model.TPS_pkeyID);
                input_parameters.Add("@TPS_TaskID", 1 + "#bigint#" + model.TPS_TaskID);
                input_parameters.Add("@TPS_MeasurementBidStatus", 1 + "#bit#" + model.TPS_MeasurementBidStatus);
                input_parameters.Add("@TPS_LoadPhotosCompletionStatus", 1 + "#bit#" + model.TPS_LoadPhotosCompletionStatus);
                input_parameters.Add("@TPS_MeasurementCompletionStatus", 1 + "#bit#" + model.TPS_MeasurementCompletionStatus);
                input_parameters.Add("@TPS_PhotoRequirementStatus", 1 + "#bit#" + model.TPS_PhotoRequirementStatus);
                input_parameters.Add("@TPS_Bid", 1 + "#decimal#" + model.TPS_Bid);
                input_parameters.Add("@TPS_Inspection", 1 + "#decimal#" + model.TPS_Inspection);
                input_parameters.Add("@TPS_BidMeasurement", 1 + "#decimal#" + model.TPS_BidMeasurement);
                input_parameters.Add("@TPS_CompletionAfter", 1 + "#decimal#" + model.TPS_CompletionAfter);
                input_parameters.Add("@TPS_CompletionDuring", 1 + "#decimal#" + model.TPS_CompletionDuring);
                input_parameters.Add("@TPS_CompletionBefore", 1 + "#decimal#" + model.TPS_CompletionBefore);
                input_parameters.Add("@TPS_CompletionLoad", 1 + "#decimal#" + model.TPS_CompletionLoad);
                input_parameters.Add("@TPS_CompletionMeasurement", 1 + "#decimal#" + model.TPS_CompletionMeasurement);
                input_parameters.Add("@TPS_PhotoRequirementMax", 1 + "#decimal#" + model.TPS_PhotoRequirementMax);
                input_parameters.Add("@TPS_PhotoRequirementMin", 1 + "#decimal#" + model.TPS_PhotoRequirementMin);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@TPS_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objData;

        }
    }
}