﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Avigma.Repository.Lib;
using Avigma.Models;
using Newtonsoft.Json;
using IPL.Models;
using System.Net;
using System.Net.Mail;
using System.IO;
using IPL.Repository.Lib;

namespace IPL.Repository.data
{
    public class EmailData
    {
        EmailManager emailManager = new EmailManager();
        Log log = new Log();


        // forget password by userid se
        // CHANGE PASSWORD - REQUEST // SENT EMAIL auto generate pwd

        public async Task<List<dynamic>> SendRequestPassword(AuthLoginDTO authLoginDTO)
        {
            List<dynamic> objDynamic = new List<dynamic>();

            try
            {
                //AuthLoginDTO authLoginDTO = new AuthLoginDTO();
                //EmailDTO model = new EmailDTO();
                DyanmicEmailDTO model = new DyanmicEmailDTO();
                string Touserid = ConfigurationManager.AppSettings["EmailTo"].ToString();
                StringBuilder sb = new StringBuilder();

                model.Subject = " Password Changed Successfully – iPreservation Live";

                string strtemplate1 = "<p>ipreservationlive.com</p>";
                sb.Append(strtemplate1);
                string strtemplate2 = "We have received a request to reset your forgotten password";
                sb.Append(strtemplate2);

                string strtemplate3 = "<p>Your User Id is  " + authLoginDTO.User_LoginName + "</p>";
                sb.Append(strtemplate3);

                string strtemplate3x = "<p>Your New Password is   " + authLoginDTO.User_Password + "</p>";
                sb.Append(strtemplate3x);

                model.Message = sb.ToString();
                model.To = authLoginDTO.User_Email;
                model.Cc = Touserid;

                int emailval = await DynamicEmailManager.SendDynamicEmail(model,1);

                //int emailval = emailManager.SendEmail(model);
                objDynamic.Add(emailval);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        //memo sent wia contractor section

        // Pradeep

        public async Task<List<dynamic>> SendEmail(AuthLoginDTO authLoginDTO, string mess, int Type)
        {
            EmailManager emailManager = new EmailManager();
            List<dynamic> objDynamic = new List<dynamic>();

            try
            {

                //EmailDTO model = new EmailDTO();
                DyanmicEmailDTO model = new DyanmicEmailDTO();
                string Touserid = ConfigurationManager.AppSettings["EmailTo"].ToString();
                StringBuilder sb = new StringBuilder();

                switch (Type)
                {
                    case 1:
                        {
                            model.Subject = "IPL  New User Register";
                            break;
                        }
                    case 2:
                        {
                            model.Subject = "IPL Password Change Request for the username  " + authLoginDTO.User_LoginName;
                            break;
                        }
                }

                sb.Append(mess);
                string strtemplate1 = "<br/><p>https://ipreservationlive.com/</p>";
                sb.Append(strtemplate1);

                model.Message = sb.ToString();


                model.To = authLoginDTO.User_Email;
                model.Cc = Touserid;

                int emailval = await DynamicEmailManager.SendDynamicEmail(model,2);
                //int emailval = emailManager.SendEmail(model);
                objDynamic.Add(emailval);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

    }
}