﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class WorkTypeTaskChildData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public List<dynamic> AddWorkTypeTaskChildData(WorkTypeTaskChildDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objAddData = new List<dynamic>();
            string insertProcedure = "[CreateUpdateWorkTypeTaskSetting]";
            WorkTypeTaskChildMaster workTypeTaskChildMaster = new WorkTypeTaskChildMaster();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();


            try
            {
                input_parameters.Add("@WT_Task_pkeyID", 1 + "#bigint#" + model.WT_Task_pkeyID);
                input_parameters.Add("@WT_Task_ID", 1 + "#bigint#" + model.WT_Task_ID);
                input_parameters.Add("@WT_Task_Company", 1 + "#varchar#" + model.WT_Task_Company);
                input_parameters.Add("@WT_Task_State", 1 + "#varchar#" + model.WT_Task_State);
                input_parameters.Add("@WT_Task_Customer", 1 + "#varchar#" + model.WT_Task_Customer);
                input_parameters.Add("@WT_Task_LoneType", 1 + "#varchar#" + model.WT_Task_LoneType);
                input_parameters.Add("@WT_Task_WorkType", 1 + "#varchar#" + model.WT_Task_WorkType);
                input_parameters.Add("@WT_Task_WorkType_Group", 1 + "#varchar#" + model.WT_Task_WorkType_Group);
                input_parameters.Add("@WT_Task_ClientDueDateTo", 1 + "#datetime#" + model.WT_Task_ClientDueDateTo);
                input_parameters.Add("@WT_Task_ClientDueDateFrom", 1 + "#datetime#" + model.WT_Task_ClientDueDateFrom);
                input_parameters.Add("@WT_Task_IsActive", 1 + "#bit#" + model.WT_Task_IsActive);
                input_parameters.Add("@WT_Task_IsDelete", 1 + "#bit#" + model.WT_Task_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@WT_Task_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    workTypeTaskChildMaster.WT_Task_pkeyID = "0";
                    workTypeTaskChildMaster.Status = "0";
                    workTypeTaskChildMaster.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    workTypeTaskChildMaster.WT_Task_pkeyID = Convert.ToString(objData[0]);
                    workTypeTaskChildMaster.Status = Convert.ToString(objData[1]);


                }


                objAddData.Add(workTypeTaskChildMaster);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return objAddData;



        }

        private DataSet GetWorkTypeTaskChildMaster(WorkTypeTaskChildDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_WorkTypeTaskSettingChild]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@WT_Task_pkeyID", 1 + "#bigint#" + model.WT_Task_pkeyID);
                input_parameters.Add("@WT_Task_ID", 1 + "#bigint#" + model.WT_Task_ID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetWorkTypeTaskChildDetails(WorkTypeTaskChildDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetWorkTypeTaskChildMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<WorkTypeTaskChildDTO> WTTaskChildDetails =
                   (from item in myEnumerableFeaprd
                    select new WorkTypeTaskChildDTO
                    {
                        WT_Task_pkeyID = item.Field<Int64>("WT_Task_pkeyID"),
                        WT_Task_ID = item.Field<Int64>("WT_Task_ID"),
                        WT_Task_Company = item.Field<String>("WT_Task_Company"),
                        WT_Task_State = item.Field<String>("WT_Task_State"),
                        WT_Task_Customer = item.Field<String>("WT_Task_Customer"),
                        WT_Task_LoneType = item.Field<String>("WT_Task_LoneType"),
                        WT_Task_WorkType = item.Field<String>("WT_Task_WorkType"),
                        WT_Task_WorkType_Group = item.Field<String>("WT_Task_WorkType_Group"),
                        WT_Task_ClientDueDateTo = item.Field<DateTime?>("WT_Task_ClientDueDateTo"),
                        WT_Task_ClientDueDateFrom = item.Field<DateTime?>("WT_Task_ClientDueDateFrom"),
                        WT_Task_IsActive = item.Field<Boolean?>("WT_Task_IsActive"),
                        


                    }).ToList();

                objDynamic.Add(WTTaskChildDetails);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}