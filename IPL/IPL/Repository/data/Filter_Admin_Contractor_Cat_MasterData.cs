﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class Filter_Admin_Contractor_Cat_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        public List<dynamic> AddUpdate_Filter_Admin_Concategory(Filter_Admin_Contractor_Cat_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_Filter_Admin_Contractor_Cat_Master]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@ConCat_Filter_PkeyId", 1 + "#bigint#" + model.ConCat_Filter_PkeyId);
                input_parameters.Add("@ConCat_Filter_Name", 1 + "#nvarchar#" + model.ConCat_Filter_Name);
                input_parameters.Add("@ConCat_Filter_CatIsActive", 1 + "#bit#" + model.ConCat_Filter_CatIsActive);
                input_parameters.Add("@ConCat_Filter_IsActive", 1 + "#bit#" + model.ConCat_Filter_IsActive);
                input_parameters.Add("@ConCat_Filter_IsDelete", 1 + "#bit#" + model.ConCat_Filter_IsDelete);
                input_parameters.Add("@ConCat_Filter_CompanyId", 1 + "#bigint#" + model.ConCat_Filter_CompanyId);
                input_parameters.Add("@ConCat_Filter_UserId", 1 + "#bigint#" + model.ConCat_Filter_UserId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserId);
                input_parameters.Add("@ConCat_Filter_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        private DataSet Get_Filter_Admin_ConCat(Filter_Admin_Contractor_Cat_MasterDTO model)
        {
            DataSet ds = null;
            try
            {

                string selectProcedure = "[Get_Filter_Admin_Contractor_Cat_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@ConCat_Filter_PkeyId", 1 + "#bigint#" + model.ConCat_Filter_PkeyId);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> Get_Filter_Admin_ConCategoryDetails(Filter_Admin_Contractor_Cat_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = Get_Filter_Admin_ConCat(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Filter_Admin_Contractor_Cat_MasterDTO> FilterGroup =
                   (from item in myEnumerableFeaprd
                    select new Filter_Admin_Contractor_Cat_MasterDTO
                    {
                        ConCat_Filter_PkeyId = item.Field<Int64>("ConCat_Filter_PkeyId"),
                        ConCat_Filter_Name = item.Field<String>("ConCat_Filter_Name"),
                        ConCat_Filter_IsActive = item.Field<Boolean?>("ConCat_Filter_IsActive")

                    }).ToList();

                objDynamic.Add(FilterGroup);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}