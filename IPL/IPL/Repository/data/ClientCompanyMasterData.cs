﻿using Avigma.Repository.Lib;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using IPLApp.Models;
using IPL.Models;

namespace IPLApp.Repository.data
{
    public class ClientCompanyMasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        //Add Company Client Details
        public List<dynamic> AddClientCompanyData(ClientCompanyMasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();
            string insertProcedure = "[CreateUpdateClientCompany_Master]";
            ClientCompanyMaster clientCompanyMaster = new ClientCompanyMaster();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Client_pkeyID", 1 + "#bigint#" + model.Client_pkeyID);
                input_parameters.Add("@Client_Company_Name", 1 + "#nvarchar#" + model.Client_Company_Name);
                input_parameters.Add("@Client_Discount", 1 + "#decimal#" + model.Client_Discount);
                input_parameters.Add("@Client_Contractor_Discount", 1 + "#decimal#" + model.Client_Contractor_Discount);
                input_parameters.Add("@Client_Billing_Address", 1 + "#varchar#" + model.Client_Billing_Address);
                input_parameters.Add("@Client_City", 1 + "#varchar#" + model.Client_City);
                input_parameters.Add("@Client_StateId", 1 + "#int#" + model.Client_StateId);
                input_parameters.Add("@Client_ZipCode", 1 + "#bigint#" + model.Client_ZipCode);
                input_parameters.Add("@Client_DateTimeOverlay", 1 + "#int#" + model.Client_DateTimeOverlay);
                input_parameters.Add("@Client_BackgroundProvider", 1 + "#varchar#" + model.Client_BackgroundProvider);
                input_parameters.Add("@Client_ContactType", 1 + "#varchar#" + model.Client_ContactType);
                input_parameters.Add("@Client_ContactName", 1 + "#varchar#" + model.Client_ContactName);
                input_parameters.Add("@Client_ContactEmail", 1 + "#varchar#" + model.Client_ContactEmail);
                input_parameters.Add("@Client_ContactPhone", 1 + "#varchar#" + model.Client_ContactPhone);
                input_parameters.Add("@Client_Comments", 1 + "#varchar#" + model.Client_Comments);
                //input_parameters.Add("@Client_Comments", 1 + "#varchar#" + model.Client_Comments);
                input_parameters.Add("@Client_Due_Date_Offset", 1 + "#bigint#" + model.Client_Due_Date_Offset);
                input_parameters.Add("@Client_Photo_Resize_width", 1 + "#bigint#" + model.Client_Photo_Resize_width);
                input_parameters.Add("@Client_Photo_Resize_height", 1 + "#bigint#" + model.Client_Photo_Resize_height);
                input_parameters.Add("@Client_Invoice_Total", 1 + "#decimal#" + model.Client_Invoice_Total);
                input_parameters.Add("@Client_Login", 1 + "#bit#" + model.Client_Login);
                input_parameters.Add("@Client_Login_Id", 1 + "#varchar#" + model.Client_Login_Id);
                input_parameters.Add("@Client_Password", 1 + "#varchar#" + model.Client_Password);
                input_parameters.Add("@Client_Rep_Id", 1 + "#varchar#" + model.Client_Rep_Id);
                input_parameters.Add("@Client_Lock_Order", 1 + "#bit#" + model.Client_Lock_Order);
                input_parameters.Add("@Client_Lock_Order_Reason", 1 + "#nvarchar#" + model.Client_Lock_Order_Reason);
                input_parameters.Add("@Client_IPL_Mobile", 1 + "#int#" + model.Client_IPL_Mobile);
                input_parameters.Add("@Client_Provider", 1 + "#int#" + model.Client_Provider);
                input_parameters.Add("@Client_Active", 1 + "#int#" + model.Client_Active);
                input_parameters.Add("@Client_ClientPhone", 1 + "#varchar#" + model.Client_ClientPhone);
                input_parameters.Add("@Client_FaxNumbar", 1 + "#varchar#" + model.Client_FaxNumbar);
                input_parameters.Add("@Client_Website_Link", 1 + "#nvarchar#" + model.Client_Website_Link);
                input_parameters.Add("@Client_IsActive", 1 + "#bit#" + model.Client_IsActive);
                input_parameters.Add("@Client_IsDelete", 1 + "#bit#" + model.Client_IsDelete);
                input_parameters.Add("@Client_IsDeleteAllow", 1 + "#bit#" + model.Client_IsDeleteAllow);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Client_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);


                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    clientCompanyMaster.Client_pkeyID = "0";
                    clientCompanyMaster.Status = "0";
                    clientCompanyMaster.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    clientCompanyMaster.Client_pkeyID = Convert.ToString(objData[0]);
                    clientCompanyMaster.Status = Convert.ToString(objData[1]);


                }
                objcltData.Add(clientCompanyMaster);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;



        }
        //Get Comapany Client all Details for Showing in table 
        private DataSet GetClientCompanyMaster(ClientCompanyMasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Client_Comapny_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Client_pkeyID", 1 + "#bigint#" + model.Client_pkeyID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@WhereClause", 1 + "#nvarchar#" + null);
                input_parameters.Add("@UserID", 1 + "#bigint#" + null);
                input_parameters.Add("@MenuID", 1 + "#bigint#" + null);
                input_parameters.Add("@FilterData", 1 + "#nvarchar#" + null);


                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetClientCompanyMasterDetails(ClientCompanyMasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetClientCompanyMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<ClientCompanyMasterDTO> ClientDetails =
                   (from item in myEnumerableFeaprd
                    select new ClientCompanyMasterDTO
                    {
                        Client_pkeyID = item.Field<Int64>("Client_pkeyID"),
                        Client_Company_Name = item.Field<String>("Client_Company_Name"),
                        Client_Discount = item.Field<Decimal?>("Client_Discount"),
                        Client_Contractor_Discount = item.Field<Decimal?>("Client_Contractor_Discount"),
                        Client_Billing_Address = item.Field<String>("Client_Billing_Address"),
                        Client_City = item.Field<String>("Client_City"),

                        //IPL_StateName = item.Field<String>("IPL_StateName"),
                        Client_StateId = item.Field<int?>("Client_StateId"),

                        Client_ZipCode = item.Field<Int64?>("Client_ZipCode"),
                        Client_DateTimeOverlay = item.Field<int?>("Client_DateTimeOverlay"),
                        Client_BackgroundProvider = item.Field<String>("Client_BackgroundProvider"),
                        Client_ContactType = item.Field<String>("Client_ContactType"),
                        Client_ContactName = item.Field<String>("Client_ContactName"),
                        Client_ContactEmail = item.Field<String>("Client_ContactEmail"),
                        Client_ContactPhone = item.Field<String>("Client_ContactPhone"),
                        Client_Comments = item.Field<String>("Client_Comments"),
                        Client_Due_Date_Offset = item.Field<Int64?>("Client_Due_Date_Offset"),
                        Client_Photo_Resize_width = item.Field<Int64?>("Client_Photo_Resize_width"),
                        Client_Photo_Resize_height = item.Field<Int64?>("Client_Photo_Resize_height"),
                        Client_Invoice_Total = item.Field<Decimal?>("Client_Invoice_Total"),
                        Client_Login = item.Field<Boolean?>("Client_Login"),
                        Client_Login_Id = item.Field<String>("Client_Login_Id"),
                        Client_Password = item.Field<String>("Client_Password"),
                        Client_Rep_Id = item.Field<String>("Client_Rep_Id"),
                        Client_Lock_Order = item.Field<Boolean?>("Client_Lock_Order"),
                        Client_Lock_Order_Reason = item.Field<String>("Client_Lock_Order_Reason"),
                        Client_IPL_Mobile = item.Field<int?>("Client_IPL_Mobile"),
                        Client_Provider = item.Field<int?>("Client_Provider"),
                        Client_Active = item.Field<int?>("Client_Active"),
                        Client_IsDelete = item.Field<Boolean?>("Client_IsDelete"),
                        Client_ClientPhone = item.Field<String>("Client_ClientPhone"),
                        Client_FaxNumbar = item.Field<String>("Client_FaxNumbar"),
                        Client_Website_Link = item.Field<String>("Client_Website_Link"),
                        Client_IsActive = item.Field<Boolean?>("Client_IsActive"),
                        Client_IsDeleteAllow = item.Field<Boolean?>("Client_IsDeleteAllow"),
                        Client_CreatedBy = item.Field<String>("Client_CreatedBy"),
                        Client_ModifiedBy = item.Field<String>("Client_ModifiedBy"),

                    }).ToList();

                objDynamic.Add(ClientDetails);

                if (ds.Tables.Count > 1)
                {
                    var myEnumerableFeaprdx = ds.Tables[1].AsEnumerable();
                    List<ClientContactListDTO> ClientContactDetailsx =
                       (from item in myEnumerableFeaprdx
                        select new ClientContactListDTO
                        {
                            Clnt_Con_List_pkeyID = item.Field<Int64>("Clnt_Con_List_pkeyID"),
                            Clnt_Con_List_ClientComID = item.Field<Int64>("Clnt_Con_List_ClientComID"),
                            Clnt_Con_List_Name = item.Field<String>("Clnt_Con_List_Name"),
                            Clnt_Con_List_Email = item.Field<String>("Clnt_Con_List_Email"),
                            Clnt_Con_List_Phone = item.Field<String>("Clnt_Con_List_Phone"),
                            Clnt_Con_List_TypeName = item.Field<String>("Clnt_Con_List_TypeName"),
                            Clnt_Con_List_IsActive = item.Field<Boolean?>("Clnt_Con_List_IsActive"),
                            Clnt_Con_List_IsDelete = item.Field<Boolean?>("Clnt_Con_List_IsDelete"),
                            Clnt_Con_List_CreatedBy = item.Field<String>("Clnt_Con_List_CreatedBy"),
                            Clnt_Con_List_ModifiedBy = item.Field<String>("Clnt_Con_List_ModifiedBy"),

                        }).ToList();

                    objDynamic.Add(ClientContactDetailsx);
                }



            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        private DataSet GetClientCompanydrop()
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Client_Comapny_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();
                input_parameters.Add("@Client_pkeyID", 1 + "#bigint#" + 0);
                input_parameters.Add("@Type", 1 + "#int#" + 1);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return ds;
        }

        public List<dynamic> GetClientCompanydropdown()
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetClientCompanydrop();

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<ClientMasterdrop> Clientddrlst =
                   (from item in myEnumerableFeaprd
                    select new ClientMasterdrop
                    {
                        Client_pkeyID = item.Field<Int64>("Client_pkeyID"),
                        Client_Company_Name = item.Field<String>("Client_Company_Name"),

                    }).ToList();

                objDynamic.Add(Clientddrlst);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        //Add Client Contact List Details
        public List<dynamic> AddClientContactListData(ClientContactListDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();
            string insertProcedure = "[CreateUpdateClientContactList]";
            ClientContact clientContact = new ClientContact();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Clnt_Con_List_pkeyID", 1 + "#bigint#" + model.Clnt_Con_List_pkeyID);
                input_parameters.Add("@Clnt_Con_List_ClientComID", 1 + "#bigint#" + model.Clnt_Con_List_ClientComID);
                input_parameters.Add("@Clnt_Con_List_Name", 1 + "#nvarchar#" + model.Clnt_Con_List_Name);
                input_parameters.Add("@Clnt_Con_List_Email", 1 + "#nvarchar#" + model.Clnt_Con_List_Email);
                input_parameters.Add("@Clnt_Con_List_Phone", 1 + "#nvarchar#" + model.Clnt_Con_List_Phone);
                input_parameters.Add("@Clnt_Con_List_TypeName", 1 + "#nvarchar#" + model.Clnt_Con_List_TypeName);
                input_parameters.Add("@Clnt_Con_List_IsActive", 1 + "#bit#" + model.Clnt_Con_List_IsActive);
                input_parameters.Add("@Clnt_Con_List_IsDelete", 1 + "#bit#" + model.Clnt_Con_List_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Clnt_Con_List_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);


                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    clientContact.Clnt_Con_List_pkeyID = "0";
                    clientContact.Status = "0";
                    clientContact.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    clientContact.Clnt_Con_List_pkeyID = Convert.ToString(objData[0]);
                    clientContact.Status = Convert.ToString(objData[1]);


                }
                objcltData.Add(clientContact);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;



        }
        //Get  Client Contact details for Showing in table 
        private DataSet GetClientContactMaster(ClientContactListDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_ClientContactList]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Clnt_Con_List_pkeyID", 1 + "#bigint#" + model.Clnt_Con_List_pkeyID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetClientContactMasterDetails(ClientContactListDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetClientContactMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<ClientContactListDTO> ClientContactDetails =
                   (from item in myEnumerableFeaprd
                    select new ClientContactListDTO
                    {
                        Clnt_Con_List_pkeyID = item.Field<Int64>("Clnt_Con_List_pkeyID"),
                        Clnt_Con_List_ClientComID = item.Field<Int64>("Clnt_Con_List_ClientComID"),
                        Clnt_Con_List_Name = item.Field<String>("Clnt_Con_List_Name"),
                        Clnt_Con_List_Email = item.Field<String>("Clnt_Con_List_Email"),
                        Clnt_Con_List_Phone = item.Field<String>("Clnt_Con_List_Phone"),
                        Clnt_Con_List_TypeName = item.Field<String>("Clnt_Con_List_TypeName"),
                        Clnt_Con_List_IsActive = item.Field<Boolean?>("Clnt_Con_List_IsActive"),
                        Clnt_Con_List_IsDelete = item.Field<Boolean?>("Clnt_Con_List_IsDelete"),


                    }).ToList();

                objDynamic.Add(ClientContactDetails);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        private DataSet GetClientContactListMaster(ClientCompanyDTO model, SearchMasterDTO searchmodel)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Client_Comapny_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Client_pkeyID", 1 + "#bigint#" + model.Client_pkeyID);
                if (model.SearchMaster != null)
                {
                    input_parameters.Add("@WhereClause", 1 + "#varchar#" + model.SearchMaster.WhereClause);//searchmodel.WhereClause
                    input_parameters.Add("@UserID", 1 + "#bigint#" + model.SearchMaster.UserID);//searchmodel.UserID
                    input_parameters.Add("@MenuID", 1 + "#bigint#" + model.SearchMaster.MenuID);//searchmodel.MenuID
                    input_parameters.Add("@FilterData", 1 + "#varchar#" + model.SearchMaster.FilterData);//searchmodel.FilterData
                }
                else
                {
                    input_parameters.Add("@WhereClause", 1 + "#varchar#" + null);//searchmodel.WhereClause
                    input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserId);//searchmodel.UserID
                    input_parameters.Add("@MenuID", 1 + "#bigint#" + null);//searchmodel.MenuID
                    input_parameters.Add("@FilterData", 1 + "#varchar#" + null);//searchmodel.FilterData
                }

                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetClientCompanyFilterDetails(ClientCompanyDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            string wherecondition = string.Empty;
            var Data = JsonConvert.DeserializeObject<ClientCompanyDTO>(model.FilterData);
            try
            {

                if (!string.IsNullOrEmpty(Data.Client_Company_Name))
                {
                    wherecondition = " And Client_Company_Name LIKE '%" + Data.Client_Company_Name.Trim() + "%'";
                }

                if (!string.IsNullOrEmpty(Data.Client_City))
                {
                    wherecondition = wherecondition + "  And Client_City LIKE '%" + Data.Client_City.Trim() + "%'";
                }

                if (!string.IsNullOrEmpty(Data.Client_Billing_Address))
                {
                    wherecondition = wherecondition + "  And dbo.fn_CleanAndTrim(Client_Billing_Address) LIKE '%" + Data.Client_Billing_Address.Trim() + "%'";
                }

                if (!string.IsNullOrEmpty(Data.Client_ContactName))
                {
                    wherecondition = wherecondition + "  And Client_ContactName LIKE '%" + Data.Client_ContactName.Trim() + "%'";
                }

                if (Data.Client_ZipCode != 0 && Data.Client_ZipCode != null)
                {
                    wherecondition = wherecondition + "  And Client_ZipCode LIKE '%" + Data.Client_ZipCode + "%'";
                }
                if (Data.Client_StateId != 0 && Data.Client_StateId != null)
                {
                    wherecondition = wherecondition + "  And Client_StateId LIKE '%" + Data.Client_StateId + "%'";
                }

                if (Data.Client_IsActive == true)
                {
                    wherecondition = wherecondition + "  And Client_IsActive =  '1'";
                }
                if (Data.Client_IsActive == false)
                {
                    wherecondition = wherecondition + "  And Client_IsActive =  '0'";
                }
                //ClientCompanyDTO clientCompanyDTO = new ClientCompanyDTO();
                SearchMasterDTO searchMasterDTO = new SearchMasterDTO();
                //clientCompanyDTO.Type = 4;
                //searchMasterDTO.MenuID = model.SearchMaster.MenuID;
                //searchMasterDTO.UserID = model.SearchMaster.UserID;
                //searchMasterDTO.WhereClause = wherecondition;
                model.Type = 4;
                model.SearchMaster.WhereClause = wherecondition;
                DataSet ds = GetClientContactListMaster(model, searchMasterDTO);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<ClientCompanyDTO> ClientContactlstDetails =
                   (from item in myEnumerableFeaprd
                    select new ClientCompanyDTO
                    {
                        Client_pkeyID = item.Field<Int64>("Client_pkeyID"),
                        Client_Company_Name = item.Field<String>("Client_Company_Name"),
                        Client_Billing_Address = item.Field<String>("Client_Billing_Address"),
                        Client_City = item.Field<String>("Client_City"),
                        IPL_StateName = item.Field<String>("IPL_StateName"),
                        Client_ZipCode = item.Field<Int64?>("Client_ZipCode"),
                        Client_ContactName = item.Field<String>("Client_ContactName"),
                        Client_Website_Link = item.Field<String>("Client_Website_Link"),
                        Client_IsActive = item.Field<Boolean?>("Client_IsActive"),
                        Client_IsDeleteAllow = item.Field<Boolean?>("Client_IsDeleteAllow"),
                        Client_CreatedBy = item.Field<String>("Client_CreatedBy"),
                        Client_ModifiedBy = item.Field<String>("Client_ModifiedBy"),


                    }).ToList();

                objDynamic.Add(ClientContactlstDetails);




            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objDynamic;
        }

        public List<dynamic> GetClientContactlstMasterDetails(ClientCompanyDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetClientContactListMaster(model, null);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<ClientCompanyDTO> ClientContactlstDetails =
                   (from item in myEnumerableFeaprd
                    select new ClientCompanyDTO
                    {
                        Client_pkeyID = item.Field<Int64>("Client_pkeyID"),
                        Client_Company_Name = item.Field<String>("Client_Company_Name"),
                        Client_Billing_Address = item.Field<String>("Client_Billing_Address"),
                        Client_City = item.Field<String>("Client_City"),
                        IPL_StateName = item.Field<String>("IPL_StateName"),
                        Client_ZipCode = item.Field<Int64?>("Client_ZipCode"),
                        Client_ContactName = item.Field<String>("Client_ContactName"),
                        Client_Website_Link = item.Field<String>("Client_Website_Link"),

                        Client_IsActive = item.Field<Boolean?>("Client_IsActive"),
                        ViewUrl = null,
                        Client_IsDeleteAllow = item.Field<Boolean?>("Client_IsDeleteAllow"),
                        Client_CreatedBy = item.Field<String>("Client_CreatedBy"),
                        Client_ModifiedBy = item.Field<String>("Client_ModifiedBy"),
                    }).ToList();

                objDynamic.Add(ClientContactlstDetails);
                if (ds.Tables.Count > 1)
                {
                    var myEnumerable = ds.Tables[1].AsEnumerable();
                    List<UserFilterDTO> UserFilterDetails =
                       (from item in myEnumerable
                        select new UserFilterDTO
                        {
                            usr_filter_pkeyID = item.Field<Int64>("usr_filter_pkeyID"),
                            usr_filter_WhereData = item.Field<String>("usr_filter_WhereData"),
                            usr_filter_FilterData = item.Field<String>("usr_filter_FilterData"),
                            usr_filter_MenuId = item.Field<Int64?>("usr_filter_MenuId"),
                            usr_filter_UserID = item.Field<Int64?>("usr_filter_UserID"),
                            Client_Filter_CreatedBy = item.Field<String>("Client_Filter_CreatedBy"),
                            Client_Filter_ModifiedBy = item.Field<String>("Client_Filter_ModifiedBy"),


                        }).ToList();

                    objDynamic.Add(UserFilterDetails);
                }
                if (model.Type == 3)
                {
                    if (ds.Tables.Count > 1)
                    {
                        var myEnumerableFeaprd1 = ds.Tables[2].AsEnumerable();
                        List<Filter_Admin_Client_MasterDTO> Filterclient =
                    (from item in myEnumerableFeaprd1
                     select new Filter_Admin_Client_MasterDTO
                     {
                         Client_Filter_PkeyID = item.Field<Int64>("Client_Filter_PkeyID"),
                         Client_Filter_ClientName = item.Field<String>("Client_Filter_ClientName"),
                         Client_Filter_Address = item.Field<String>("Client_Filter_Address"),
                         Client_Filter_City = item.Field<String>("Client_Filter_City"),
                         Client_Filter_State = item.Field<Int64?>("Client_Filter_State"),
                         Client_Filter_CLTIsActive = item.Field<Boolean?>("Client_Filter_CLTIsActive"),
                         Client_Filter_CreatedBy = item.Field<String>("Client_Filter_CreatedBy"),
                         Client_Filter_ModifiedBy = item.Field<String>("Client_Filter_ModifiedBy"),
                     }).ToList();

                        objDynamic.Add(Filterclient);
                    }
                }

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }




        /// <summary>
        /// This Method is USe to Get Client Details For DropDown
        /// </summary>
        /// <returns></returns>

        public List<dynamic> GetClientDataForDropDown(ClientCompanyDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                //ClientCompanyDTO model = new ClientCompanyDTO();
                //model.UserId = 
                //model.Type = 5;
                DataSet ds = GetClientContactListMaster(model, null);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<ClientCompanyDTOForDropDowm> ClientContactlstDetails =
                   (from item in myEnumerableFeaprd
                    select new ClientCompanyDTOForDropDowm
                    {
                        Client_pkeyID = item.Field<Int64>("Client_pkeyID"),
                        Client_Company_Name = item.Field<String>("Client_Company_Name"),



                    }).ToList();

                objDynamic.Add(ClientContactlstDetails);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }


        public List<dynamic> GetDefaultClientDataForDropDown(ClientCompanyDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {

                DataSet ds = GetClientContactListMaster(model, null);

                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[0]));
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

    }
}