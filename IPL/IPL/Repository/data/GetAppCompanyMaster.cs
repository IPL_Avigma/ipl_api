﻿using Avigma.Repository.Lib;
using IPLApp.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class GetAppCompanyMaster
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        //Get single data 
        private DataSet GetSingleAppCompanyMaster(AppCompanyMasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Single_AppComapanyInfo]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@YR_Company_UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@YR_Company_pkeyID", 1 + "#bigint#" + model.YR_Company_pkeyID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }
        //Get App Company Details 
        public List<dynamic> GetSingleAppCompanyMasterDetails(AppCompanyMasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetSingleAppCompanyMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<AppCompanyMasterDTO> AppClientDetails =
                   (from item in myEnumerableFeaprd
                    select new AppCompanyMasterDTO
                    {
                        YR_Company_pkeyID = item.Field<Int64>("YR_Company_pkeyID"),
                        YR_Company_Name = item.Field<String>("YR_Company_Name"),
                        YR_Company_Con_Name = item.Field<String>("YR_Company_Con_Name"),
                        YR_Company_Email = item.Field<String>("YR_Company_Email"),
                        YR_Company_Phone = item.Field<String>("YR_Company_Phone"),
                        YR_Company_Address = item.Field<String>("YR_Company_Address"),
                        YR_Company_City = item.Field<String>("YR_Company_City"),
                        YR_Company_State = item.Field<int?>("YR_Company_State"),
                        YR_Company_Zip = item.Field<Int64?>("YR_Company_Zip"),
                        YR_Company_Logo = item.Field<String>("YR_Company_Logo"),
                        YR_Company_Support_Email = item.Field<String>("YR_Company_Support_Email"),
                        YR_Company_Support_Phone = item.Field<String>("YR_Company_Support_Phone"),
                        YR_Company_PDF_Heading = item.Field<String>("YR_Company_PDF_Heading"),
                        YR_Company_App_logo = item.Field<String>("YR_Company_App_logo"),
                        YR_Company_IsActive = item.Field<Boolean?>("YR_Company_IsActive"),
                        YR_Company_CreatedBy = item.Field<String>("YR_Company_CreatedBy"),
                        YR_Company_ModifiedBy = item.Field<String>("YR_Company_ModifiedBy"),

                    }).ToList();

                objDynamic.Add(AppClientDetails);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }


        





    }
}