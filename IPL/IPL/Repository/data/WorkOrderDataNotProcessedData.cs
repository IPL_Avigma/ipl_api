﻿using Avigma.Repository.Lib;
using IPLApp.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using AutoMapper;
using IPL.Models;

namespace IPL.Repository.data
{
    public class WorkOrderDataNotProcessedData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        private DataSet Get_WorkOrderData_NotProcessed(WorkOrderDataNotProcessedDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_WorkOrderData_NotProcessed]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@ImportFrom", 1 + "#bigint#" + model.ImportFrom);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Companyid", 1 + "#bigint#" + model.Companyid);
                input_parameters.Add("@WI_Pkey_ID", 1 + "#bigint#" + model.WI_Pkey_ID);
                input_parameters.Add("@Pkey_ID_Out", 1 + "#bigint#" + model.Pkey_ID_Out);
                input_parameters.Add("@ReturnValue", 1 + "#int#" + model.ReturnValue);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }
        

        public List<dynamic> GetWorkOrderDataNotProcessedDetails(WorkOrderDataNotProcessedDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = Get_WorkOrderData_NotProcessed(model);
                if (ds.Tables.Count > 0)
                {
                    for (int i = 0; i < ds.Tables.Count; i++)
                    {
                        objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                    }
                }

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}