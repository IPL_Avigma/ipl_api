﻿using Avigma.Repository.Lib;
using Avigma.Repository.Lib.FireBase;
using IPL.Models;
using IPLApp.Models;
using IPLApp.Repository.data;
using iTextSharp.text.pdf.qrcode;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace IPL.Repository.data
{
    public class UpdateWorkOrderMAsterStatusData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        WorkOrderMasterData workOrderMasterData = new WorkOrderMasterData();
        private DataSet UpdateWorkOderstatusMaster(WorkOrderUpdateStatusDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Updateworkorderstatus]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@workOrder_ID", 1 + "#bigint#" + model.workOrder_ID);
                input_parameters.Add("@status", 1 + "#nvarchar#" + model.status);
                input_parameters.Add("@Wo_Start", 1 + "#int#" + model.Wo_Start);
                input_parameters.Add("@IsActive", 1 + "#bit#" + model.IsActive);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }
        public List<dynamic> UpdateWorkorderStatusForMobile(WorkOrderUpdateStatusDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();

            try
            {
                DataSet dataSet = UpdateWorkOderstatusMaster(model);
                objDynamic.Add(obj.AsDynamicEnumerable(dataSet.Tables[0]));
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objDynamic;
        }


                public async Task<List<dynamic>> UpdateWorkOrderStatus(WorkOrderUpdateStatusDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                string IPLNO = string.Empty, UserToken = string.Empty , Address = string.Empty;

                //get old workorder details
                var Old_workorderData = workOrderMasterData.GetWorkorderForNotification(model.workOrder_ID, model.UserId);
                // Sending Notification to the old contractor when workorder is UnAssigned
                if (model.status == "1")
                {
                    IPLNO = Old_workorderData.IPLNO;
                    Address = Old_workorderData.address1 + " " + Old_workorderData.city + " " + Old_workorderData.state + " " + Old_workorderData.zip;
                    NotificationGetData notificationGetData = new NotificationGetData();
                    var notificationResult = notificationGetData.SendNotification(Old_workorderData.User_Token_val, "Work order with IPL number #" + IPLNO + "  "+ Address + " has been UnAssigned .", "Work Order UnAssigned", model.workOrder_ID.ToString(),1, IPLNO);
                }


                //Check if assign status then need Contractor
                if (model.status == "2" && Old_workorderData.Contractor == "0")
                {
                    return objDynamic;
                }

                //Send Unassigned Status email before the status update 
                if (model.status == "1")
                {
                    EmailTemplateData emailTemplateData = new EmailTemplateData();
                    EmailWorkOderDTO emailWorkOderDTO = new EmailWorkOderDTO();
                    emailWorkOderDTO.workOrder_ID = model.workOrder_ID;
                    emailWorkOderDTO.UserID = model.UserId;
                    emailWorkOderDTO.Type = 1;
                    emailWorkOderDTO.Val_Type = 1;
                    await emailTemplateData.GetEmailWorkOderDetail(emailWorkOderDTO);
                }



                DataSet ds = UpdateWorkOderstatusMaster(model);

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    IPLNO = ds.Tables[0].Rows[i]["IPLNO"].ToString();
                    UserToken = ds.Tables[0].Rows[i]["User_Token_val"].ToString();
                }

                //workorder notification
                workOrderMasterData.WorkOrderFirebaseNotification(Old_workorderData);

                switch (model.status)
                {
                    case "10": // Cancelled 
                        {
                            //NotificationGetData notificationGetData = new NotificationGetData();
                            //var notificationResult = notificationGetData.SendNotification(UserToken, "Work order with IPL number " + IPLNO + " has been cancelled.", "Work Order Cancelled");

                            EmailWorkOderDTO emailWorkOderDTO = new EmailWorkOderDTO();
                            emailWorkOderDTO.Val_Type = 2;
                            emailWorkOderDTO.workOrder_ID = model.workOrder_ID;
                            emailWorkOderDTO.UserID = model.UserId;
                            emailWorkOderDTO.Type = 1;
                            await workOrderMasterData.GetEmailCancelWorkOderDetail(emailWorkOderDTO);
                            break;
                        }
                    //case "1": // Unassigned
                    //    {
                    //        EmailTemplateData emailTemplateData = new EmailTemplateData();
                    //        EmailWorkOderDTO emailWorkOderDTO = new EmailWorkOderDTO();
                    //        emailWorkOderDTO.workOrder_ID = model.workOrder_ID;
                    //        emailWorkOderDTO.UserID = model.UserId;
                    //        emailWorkOderDTO.Type = 1;
                    //        emailWorkOderDTO.Val_Type = 1;
                    //        await emailTemplateData.GetEmailWorkOderDetail(emailWorkOderDTO);
                    //        break;
                    //    }
                    case "5": // Field Complete
                    case "7": // Sent to Client
                    case "2": //Assigned
                        {
                            EmailWorkOderDTO emailWorkOderDTO = new EmailWorkOderDTO();
                            emailWorkOderDTO.Val_Type = 2;
                            emailWorkOderDTO.workOrder_ID = model.workOrder_ID;
                            emailWorkOderDTO.UserID = model.UserId;
                            emailWorkOderDTO.Type = 1;
                            await workOrderMasterData.SendWorkOrderEmail(emailWorkOderDTO);
                            break;
                        }
                }

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<statusDetailsDTO> updatedetails =
                   (from item in myEnumerableFeaprd
                    select new statusDetailsDTO
                    {
                        workOrder_ID = item.Field<Int64>("workOrder_ID"),
                        status = item.Field<int>("status"),
                        Status_Name = item.Field<String>("Status_Name"),
                        Received_Date = item.Field<DateTime?>("Received_Date"),
                        Complete_Date = item.Field<DateTime?>("Complete_Date"),
                        Cancel_Date = item.Field<DateTime?>("Cancel_Date"),
                        SentToClient_date = item.Field<DateTime?>("SentToClient_date"),
                        OfficeApproved_date = item.Field<DateTime?>("OfficeApproved_date"),
                        Field_complete_date = item.Field<DateTime?>("Field_complete_date"),
                        Status_UserID = item.Field<Int64?>("Status_UserID"),


                    }).ToList();

                objDynamic.Add(updatedetails);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage("UpdateWorkOrderStatus-------" + model);
            }

            return objDynamic;
        }

        public List<dynamic> SendCancelledWebNotification(WorkOrderUpdateStatusDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();

            try
            {
                Workorder_Notification_MasterDTO workorder_Notification_MasterDTO = new Workorder_Notification_MasterDTO();

                workorder_Notification_MasterDTO.WN_WoId = model.workOrder_ID;
                workorder_Notification_MasterDTO.WN_UserId = model.WN_UserId;
                workorder_Notification_MasterDTO.WN_Title = "Work Order Cancelled";
                workorder_Notification_MasterDTO.WN_Message = "Work order with IPL number " + model.IPLNO + " has been cancelled.";
                workorder_Notification_MasterDTO.WN_IsActive = true;
                workorder_Notification_MasterDTO.WN_IsRead = false;
                workorder_Notification_MasterDTO.WN_IsDelete = false;
                workorder_Notification_MasterDTO.UserID = model.UserId;
                workorder_Notification_MasterDTO.Type = 1;

                Workorder_Notification_MasterData workorder_Notification_MasterData = new Workorder_Notification_MasterData();
                var NoObj = workorder_Notification_MasterData.AddWorkorderNotificationMaster(workorder_Notification_MasterDTO);
                objDynamic.Add(NoObj);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        public List<dynamic> SendAssignedWebNotification(WorkOrderUpdateStatusDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();

            try
            {
                Workorder_Notification_MasterDTO workorder_Notification_MasterDTO = new Workorder_Notification_MasterDTO();

                workorder_Notification_MasterDTO.WN_WoId = model.workOrder_ID;
                workorder_Notification_MasterDTO.WN_UserId = model.WN_UserId;
                workorder_Notification_MasterDTO.WN_Title = "Work Order Assigned";
                workorder_Notification_MasterDTO.WN_Message = "Work order with IPL number " + model.IPLNO + " has been Assigned.";
                workorder_Notification_MasterDTO.WN_IsActive = true;
                workorder_Notification_MasterDTO.WN_IsRead = false;
                workorder_Notification_MasterDTO.WN_IsDelete = false;
                workorder_Notification_MasterDTO.UserID = model.UserId;
                workorder_Notification_MasterDTO.Type = 1;

                Workorder_Notification_MasterData workorder_Notification_MasterData = new Workorder_Notification_MasterData();
                var NoObj = workorder_Notification_MasterData.AddWorkorderNotificationMaster(workorder_Notification_MasterDTO);
                objDynamic.Add(NoObj);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

    }
}