﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class Service_Link_Form_Master_Data
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        public List<dynamic> PostServiceLinkForm(Service_Link_Form_Master_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_Service_Link_Form_Master]";
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@SL_Pkey_ID", 1 + "#bigint#" + model.SL_Pkey_ID);
                input_parameters.Add("@SL_WO_ID", 1 + "#bigint#" + model.SL_WO_ID);
                input_parameters.Add("@SL_General_Information", 1 + "#nvarchar#" + model.SL_General_Information);
                input_parameters.Add("@SL_Property_Condition_Report1", 1 + "#nvarchar#" + model.SL_Property_Condition_Report1);
                input_parameters.Add("@SL_Property_Condition_Report2", 1 + "#nvarchar#" + model.SL_Property_Condition_Report2);
                input_parameters.Add("@SL_Bids", 1 + "#nvarchar#" + model.SL_Bids);
                input_parameters.Add("@SL_Other_Result", 1 + "#nvarchar#" + model.SL_Other_Result);
                input_parameters.Add("@SL_Summary", 1 + "#nvarchar#" + model.SL_Summary);
                input_parameters.Add("@SL_IsActive", 1 + "#bit#" + model.SL_IsActive);
                input_parameters.Add("@SL_IsDelete", 1 + "#bit#" + model.SL_IsDelete);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@fwo_pkyeId", 1 + "#bigint#" + model.fwo_pkyeId);
                input_parameters.Add("@SL_Pkey_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);
                objData = obj.SqlCRUD(insertProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        private DataSet GetServiceLinkForm(Service_Link_Form_Master_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Service_Link_Form_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@SL_Pkey_ID", 1 + "#bigint#" + model.SL_Pkey_ID);
                input_parameters.Add("@SL_WO_ID", 1 + "#bigint#" + model.SL_WO_ID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetServiceLinkFormDetails(Service_Link_Form_Master_DTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetServiceLinkForm(model);

                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                }

                //var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                //List<Service_Link_Form_Master_DTO> cyprexxGrassdata =
                //   (from item in myEnumerableFeaprd
                //    select new Service_Link_Form_Master_DTO
                //    {
                //        SL_Pkey_ID = item.Field<Int64>("SL_Pkey_ID"),
                //        SL_WO_ID = item.Field<Int64>("SL_WO_ID"),
                //        SL_CompanyID = item.Field<Int64>("SL_CompanyID"),
                //        SL_General_Information = item.Field<String>("SL_General_Information"),
                //        SL_Property_Condition_Report1 = item.Field<String>("SL_Property_Condition_Report1"),
                //        SL_Property_Condition_Report2 = item.Field<String>("SL_Property_Condition_Report2"),
                //        SL_Bids = item.Field<String>("SL_Bids"),
                //        SL_Other_Result = item.Field<String>("SL_Other_Result"),
                //        SL_Summary = item.Field<String>("SL_Summary"),
                //        SL_IsActive = item.Field<Boolean>("SL_IsActive"),
                //    }).ToList();

                //objDynamic.Add(cyprexxGrassdata);

                //if (ds.Tables.Count > 1)
                //{
                //    var pcrHistory = ds.Tables[1].AsEnumerable();
                //    List<Service_Link_Form_Master_DTO> cyprexxGrassdata_history =
                //       (from item in pcrHistory
                //        select new Service_Link_Form_Master_DTO
                //        {
                //            SL_Pkey_ID = item.Field<Int64>("SL_Pkey_ID"),
                //            SL_WO_ID = item.Field<Int64>("SL_WO_ID"),
                //            SL_CompanyID = item.Field<Int64>("SL_CompanyID"),
                //            SL_General_Information = item.Field<String>("SL_General_Information"),
                //            SL_Property_Condition_Report1 = item.Field<String>("SL_Property_Condition_Report1"),
                //            SL_Property_Condition_Report2 = item.Field<String>("SL_Property_Condition_Report2"),
                //            SL_Bids = item.Field<String>("SL_Bids"),
                //            SL_Other_Result = item.Field<String>("SL_Other_Result"),
                //            SL_Summary = item.Field<String>("SL_Summary"),
                //            SL_IsActive = item.Field<Boolean>("SL_IsActive"),
                //        }).ToList();

                //    objDynamic.Add(cyprexxGrassdata_history);

                //}
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}