﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace IPL.Repository.data
{
    public class WorkOrder_CustomPCRPhotoLabelData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();


        private List<dynamic> CreateUpdateWorkOrderCustomPCRPhotoLabel(WorkOrder_CustomPCRPhotoLabelDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_WorkOrder_CustomPCRPhotoLabel]";
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@WorkOrderPCRPhotoLabel_pkeyID", 1 + "#bigint#" + model.WorkOrderPCRPhotoLabel_pkeyID);
                input_parameters.Add("@WorkOrderPCRPhotoLabel_PhotoLabel_ID", 1 + "#bigint#" + model.WorkOrderPCRPhotoLabel_PhotoLabel_ID);
                input_parameters.Add("@WorkOrderPCRPhotoLabel_WO_ID", 1 + "#bigint#" + model.WorkOrderPCRPhotoLabel_WO_ID);
                input_parameters.Add("@WorkOrderPCRPhotoLabel_IsActive", 1 + "#bit#" + model.WorkOrderPCRPhotoLabel_IsActive);
                input_parameters.Add("@WorkOrderPCRPhotoLabel_IsDelete", 1 + "#bit#" + model.WorkOrderPCRPhotoLabel_IsDelete);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@WorkOrderPCRPhotoLabel_Pkey_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);
                objData = obj.SqlCRUD(insertProcedure, input_parameters);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        public List<dynamic>AddWorkOrderCustomPCRPhotoLabelData(WorkOrder_CustomPCRPhotoLabelDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                objData = CreateUpdateWorkOrderCustomPCRPhotoLabel(model);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }
            return objData;
        }

        private DataSet GetWorkOrderCustomPCRPhotoLabeldetail(WorkOrder_CustomPCRPhotoLabelDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[WorkOrder_CustomPCRPhotoLabelDTO]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();
                input_parameters.Add("@WorkOrderPCRPhotoLabel_pkeyID", 1 + "#bigint#" + model.WorkOrderPCRPhotoLabel_pkeyID);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);


                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return ds;
        }

        public List<dynamic>GetWorkOrderCustomPCRPhotoLabeldetails(WorkOrder_CustomPCRPhotoLabelDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {

                DataSet ds = GetWorkOrderCustomPCRPhotoLabeldetail(model);
                if (ds.Tables.Count > 0)
                {
                    for (int i = 0; i < ds.Tables.Count; i++)
                    {
                        objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                    }

                }

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objDynamic;
        }

    }
}