﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class Work_Order_Setting_Data
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        private List<dynamic> CreateUpdate_Work_Order_Setting_Master(Work_Order_SettingDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objclntData = new List<dynamic>();
            Work_Order_Setting_Master work_Order_Setting_Master = new Work_Order_Setting_Master();
            string insertProcedure = "[CreateUpdate_Work_Order_Setting_Master]";
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@WOS_PkeyID", 1 + "#bigint#" + model.WOS_PkeyID);
                input_parameters.Add("@WOS_Contractor_Work_Order_Rejection", 1 + "#bit#" + model.WOS_Contractor_Work_Order_Rejection);
                input_parameters.Add("@WOS_Work_Order_To_Previous_Contractor", 1 + "#bit#" + model.WOS_Work_Order_To_Previous_Contractor);
                input_parameters.Add("@WOS_Contractor_Compancy_Name", 1 + "#bit#" + model.WOS_Contractor_Compancy_Name);
                input_parameters.Add("@WOS_Contractor_Vendor_Id", 1 + "#bit#" + model.WOS_Contractor_Vendor_Id);
                input_parameters.Add("@WOS_Contractor_list_Sorting", 1 + "#nvarchar#" + model.WOS_Contractor_list_Sorting);
                input_parameters.Add("@WOS_EST_Completion_Date", 1 + "#bit#" + model.WOS_EST_Completion_Date);
                input_parameters.Add("@WOS_EST_Date_Allowed_Past", 1 + "#bit#" + model.WOS_EST_Date_Allowed_Past);
                input_parameters.Add("@WOS_EST_Date_List_Shows", 1 + "#bit#" + model.WOS_EST_Date_List_Shows);
                input_parameters.Add("@WOS_New_Contractor_Priorty_WorkOrders", 1 + "#int#" + model.WOS_New_Contractor_Priorty_WorkOrders);

                input_parameters.Add("@WOS_Browser_Tab_Name", 1 + "#nvarchar#" + model.WOS_Browser_Tab_Name);
                input_parameters.Add("@WOS_Company_Name_On_Work_Order", 1 + "#bit#" + model.WOS_Company_Name_On_Work_Order);
                input_parameters.Add("@WOS_Default_Timezone", 1 + "#int#" + model.WOS_Default_Timezone);
                input_parameters.Add("@WOS_Client_Name_To_Office_Staff", 1 + "#bit#" + model.WOS_Client_Name_To_Office_Staff);
                input_parameters.Add("@WOS_Remove_Pricing", 1 + "#bit#" + model.WOS_Remove_Pricing);
                input_parameters.Add("@WOS_Highlight_Pricing", 1 + "#bit#" + model.WOS_Highlight_Pricing);
                input_parameters.Add("@WOS_Loan_On_Work_Orders", 1 + "#bit#" + model.WOS_Loan_On_Work_Orders);
                input_parameters.Add("@WOS_Client_Company_On_Work_Orders", 1 + "#bit#" + model.WOS_Client_Company_On_Work_Orders);
                input_parameters.Add("@WOS_Customer_On_Work_Orders", 1 + "#bit#" + model.WOS_Customer_On_Work_Orders);
                input_parameters.Add("@WOS_Priorty_WorkOrder_Que", 1 + "#bit#" + model.WOS_Priorty_WorkOrder_Que);

                input_parameters.Add("@WOS_Documents_Expiration", 1 + "#nvarchar#" + model.WOS_Documents_Expiration);
                input_parameters.Add("@WOS_Contractor_Availability", 1 + "#nvarchar#" + model.WOS_Contractor_Availability);
                input_parameters.Add("@WOS_Escalated_Work_Order", 1 + "#nvarchar#" + model.WOS_Escalated_Work_Order);
                input_parameters.Add("@WOS_Client_Cancelled_order", 1 + "#nvarchar#" + model.WOS_Client_Cancelled_order);
                input_parameters.Add("@WOS_Client_Workorder_Changed", 1 + "#nvarchar#" + model.WOS_Client_Workorder_Changed);

              
                input_parameters.Add("@WOS_Work_Order_Assigned_Coordinator", 1 + "#bit#" + model.WOS_Work_Order_Assigned_Coordinator);
                input_parameters.Add("@WOS_Work_Order_Assigned_Processor", 1 + "#bit#" + model.WOS_Work_Order_Assigned_Processor);
                input_parameters.Add("@WOS_Work_Order_Email", 1 + "#bit#" + model.WOS_Work_Order_Email);
                input_parameters.Add("@WOS_Contractor_Alert_for_U_read", 1 + "#int#" + model.WOS_Contractor_Alert_for_U_read);
                input_parameters.Add("@WOS_Contractor_Late_Orders", 1 + "#bit#" + model.WOS_Contractor_Late_Orders);
                input_parameters.Add("@WOS_Contractor_Opens_Orders", 1 + "#bit#" + model.WOS_Contractor_Opens_Orders);

                input_parameters.Add("@WOS_Allows_Duplicate_Work_Order", 1 + "#bit#" + model.WOS_Allows_Duplicate_Work_Order);
                input_parameters.Add("@WOS_Work_Orders_To_Previous_Contractor", 1 + "#bit#" + model.WOS_Work_Orders_To_Previous_Contractor);
                input_parameters.Add("@WOS_Show_Recomended_Contractorlist", 1 + "#bit#" + model.WOS_Show_Recomended_Contractorlist);
                input_parameters.Add("@WOS_Customerdata", 1 + "#nvarchar#" + model.WOS_Customerdata);
                input_parameters.Add("@WOS_Loan_Number", 1 + "#nvarchar#" + model.WOS_Loan_Number);
                input_parameters.Add("@WOS_LockBox_Code", 1 + "#nvarchar#" + model.WOS_LockBox_Code);
                input_parameters.Add("@WOS_Key_Code", 1 + "#nvarchar#" + model.WOS_Key_Code);
                input_parameters.Add("@WOS_LotSize", 1 + "#nvarchar#" + model.WOS_LotSize);

                input_parameters.Add("@WOS_Photos_be_Labeled", 1 + "#bit#" + model.WOS_Photos_be_Labeled);
                input_parameters.Add("@WOS_Allow_Date_Stamps", 1 + "#bit#" + model.WOS_Allow_Date_Stamps);
                input_parameters.Add("@WOS_Allow_Metadata", 1 + "#bit#" + model.WOS_Allow_Metadata);
                input_parameters.Add("@WOS_Date_Stamp_Format", 1 + "#int#" + model.WOS_Date_Stamp_Format);
                input_parameters.Add("@WOS_Minimum_Number_of_Photos", 1 + "#nvarchar#" + model.WOS_Minimum_Number_of_Photos); //
                input_parameters.Add("@WOS_Bid_Photo_Labels", 1 + "#bit#" + model.WOS_Bid_Photo_Labels);
                input_parameters.Add("@WOS_Completion_Photo_Lables", 1 + "#bit#" + model.WOS_Completion_Photo_Lables);
                input_parameters.Add("@WOS_Damage_Photo_Lables", 1 + "#bit#" + model.WOS_Damage_Photo_Lables);
                input_parameters.Add("@WOS_Inspection_Photo_Lables", 1 + "#bit#" + model.WOS_Inspection_Photo_Lables);
                input_parameters.Add("@WOS_Custom_Photo_Lables", 1 + "#bit#" + model.WOS_Custom_Photo_Lables);
                input_parameters.Add("@WOS_Allow_Invoice_Number", 1 + "#bit#" + model.WOS_Allow_Invoice_Number);

                input_parameters.Add("@WOS_Show_Completion_Comments", 1 + "#bit#" + model.WOS_Show_Completion_Comments);
      
                input_parameters.Add("@WOS_Dafault_Transfer_Completion_Task", 1 + "#bit#" + model.WOS_Dafault_Transfer_Completion_Task);
                input_parameters.Add("@WOS_Contractor_Invoice_Flat_Fee", 1 + "#bit#" + model.WOS_Contractor_Invoice_Flat_Fee);
                input_parameters.Add("@WOS_Client_Invoice_Flat_Fee", 1 + "#bit#" + model.WOS_Client_Invoice_Flat_Fee);
                input_parameters.Add("@WOS_Print_Company_Logo_Invoice", 1 + "#bit#" + model.WOS_Print_Company_Logo_Invoice);
                input_parameters.Add("@WOS_Print_ABC_Number_on_Invoice", 1 + "#bit#" + model.WOS_Print_ABC_Number_on_Invoice);
                input_parameters.Add("@WOS_Print_Invoice_Date", 1 + "#bit#" + model.WOS_Print_Invoice_Date);
                input_parameters.Add("@WOS_Approved_Invoice", 1 + "#bit#" + model.WOS_Approved_Invoice);

                input_parameters.Add("@WOS_Property_Alert_App", 1 + "#bit#" + model.WOS_Property_Alert_App);
                input_parameters.Add("@WOS_Occpancy_App", 1 + "#bit#" + model.WOS_Occpancy_App);

                input_parameters.Add("@WOS_IsActive", 1 + "#bit#" + model.WOS_IsActive);
                input_parameters.Add("@WOS_Is_Delete", 1 + "#bit#" + model.WOS_Is_Delete);

   


                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@WOS_Pkey_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    work_Order_Setting_Master.WOS_PkeyID = "0";
                    work_Order_Setting_Master.Status = "0";
                    work_Order_Setting_Master.ErrorMessage = "Error while Saviving in the Database";
                }
                else
                {
                    work_Order_Setting_Master.WOS_PkeyID = Convert.ToString(objData[0]);
                    work_Order_Setting_Master.Status = Convert.ToString(objData[1]);
                }
                objclntData.Add(work_Order_Setting_Master);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objclntData;



        }

        private DataSet GetWork_Order_Setting_Master(Work_Order_SettingDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Work_Order_Setting_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@WOS_PkeyID", 1 + "#bigint#" + model.WOS_PkeyID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserID);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }


        public List<dynamic> CreateUpdateWorkOrderSettingMaster(Work_Order_SettingDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                objData = CreateUpdate_Work_Order_Setting_Master(model);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }
            return objData;
        }

        public List<dynamic> GetWorkOrderSettingData(Work_Order_SettingDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetWork_Order_Setting_Master(model);

               
                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));

                }


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

    }
}