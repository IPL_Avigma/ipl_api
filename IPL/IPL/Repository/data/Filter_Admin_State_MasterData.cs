﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class Filter_Admin_State_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        public List<dynamic> AddUpdate_Filter_Admin_State(Filter_Admin_State_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_Filter_Admin_State_Master]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@State_Filter_PkeyId", 1 + "#bigint#" + model.State_Filter_PkeyId);
                input_parameters.Add("@State_Filter_Name", 1 + "#nvarchar#" + model.State_Filter_Name);
                input_parameters.Add("@State_Filter_IsStateActive", 1 + "#bit#" + model.State_Filter_IsStateActive);
                input_parameters.Add("@State_Filter_IsActive", 1 + "#bit#" + model.State_Filter_IsActive);
                input_parameters.Add("@State_Filter_IsDelete", 1 + "#bit#" + model.State_Filter_IsDelete);
                input_parameters.Add("@State_Filter_CompanyId", 1 + "#bigint#" + model.State_Filter_CompanyId);
                input_parameters.Add("@State_Filter_UserId", 1 + "#bigint#" + model.State_Filter_UserId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserId);
                input_parameters.Add("@State_Filter_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        private DataSet Get_Filter_Admin_State(Filter_Admin_State_MasterDTO model)
        {
            DataSet ds = null;
            try
            {

                string selectProcedure = "[Get_Filter_Admin_State_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@State_Filter_PkeyId", 1 + "#bigint#" + model.State_Filter_PkeyId);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> Get_Filter_Admin_StateDetails(Filter_Admin_State_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = Get_Filter_Admin_State(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Filter_Admin_State_MasterDTO> Filterstate =
                   (from item in myEnumerableFeaprd
                    select new Filter_Admin_State_MasterDTO
                    {
                        State_Filter_PkeyId = item.Field<Int64>("State_Filter_PkeyId"),
                        State_Filter_Name = item.Field<String>("State_Filter_Name"),
                        State_Filter_IsStateActive = item.Field<Boolean?>("State_Filter_IsStateActive")

                    }).ToList();

                objDynamic.Add(Filterstate);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}