﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace IPL.Repository.data
{
    public class Message_User_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();


        private List<dynamic> CreateUpdateMessageUserMaster(Message_User_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_Message_User_Master]";
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@MUM_PkeyID", 1 + "#bigint#" + model.MUM_PkeyID);
                input_parameters.Add("@MUM_User_PkeyID", 1 + "#bigint#" + model.MUM_User_PkeyID);
                input_parameters.Add("@MUM_User_Group_PkeyID", 1 + "#bigint#" + model.MUM_User_Group_PkeyID);
                input_parameters.Add("@MUM_User_Status", 1 + "#int#" + model.MUM_User_Status);
                input_parameters.Add("@MUM_User_Flag", 1 + "#int#" + model.MUM_User_Flag);
                input_parameters.Add("@MUM_User_IsActive", 1 + "#bit#" + model.MUM_User_IsActive);
                input_parameters.Add("@MUM_User_IsDelete", 1 + "#bit#" + model.MUM_User_IsDelete);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@MUM_Pkey_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);
                objData = obj.SqlCRUD(insertProcedure, input_parameters);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        public List<dynamic> AddMessageUserMaster(Message_User_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                objData = CreateUpdateMessageUserMaster(model);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }
            return objData;
        }
    }
}