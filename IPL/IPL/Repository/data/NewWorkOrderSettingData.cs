﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class NewWorkOrderSettingData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        //AddNew WorkOrder Setting Details
        public List<dynamic> AddNewWorkOrderSettingData(NewWorkOrderSettingDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();
            string insertProcedure = "[CreateUpdateNewWorkOrderSetting]";
            NewWorkOrderSetting newWorkOrderSetting = new NewWorkOrderSetting();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@WO_Sett_pkeyID", 1 + "#bigint#" + model.WO_Sett_pkeyID);
                input_parameters.Add("@WO_Sett_CompanyID", 1 + "#bigint#" + model.WO_Sett_CompanyID);
                input_parameters.Add("@WO_Sett_Allow_Dup_Num", 1 + "#bit#" + model.WO_Sett_Allow_Dup_Num);
                input_parameters.Add("@WO_Sett_Auto_Inc_GoBack", 1 + "#bit#" + model.WO_Sett_Auto_Inc_GoBack);
                input_parameters.Add("@WO_Sett_Auto_Inc_NeedInfo", 1 + "#bit#" + model.WO_Sett_Auto_Inc_NeedInfo);
                input_parameters.Add("@WO_Sett_Auto_Inc_Dup", 1 + "#bit#" + model.WO_Sett_Auto_Inc_Dup);
                input_parameters.Add("@WO_Sett_Auto_Inc_Recurring", 1 + "#bit#" + model.WO_Sett_Auto_Inc_Recurring);
                input_parameters.Add("@WO_Sett_Auto_Assign", 1 + "#bit#" + model.WO_Sett_Auto_Assign);
                input_parameters.Add("@WO_Sett_Detect_Pricing", 1 + "#bit#" + model.WO_Sett_Detect_Pricing);
                input_parameters.Add("@WO_Sett_Remove_Doller", 1 + "#bit#" + model.WO_Sett_Remove_Doller);
                input_parameters.Add("@WO_Sett_IsDelete", 1 + "#bit#" + model.WO_Sett_IsDelete);
                input_parameters.Add("@WO_Sett_IsActive", 1 + "#bit#" + model.WO_Sett_IsActive);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@WO_Sett_UserID", 1 + "#bigint#" + model.WO_Sett_UserId);
                input_parameters.Add("@Wo_Sett_Comapny_SAlert", 1 + "#varchar#" + model.Wo_Sett_Comapny_SAlert);
                input_parameters.Add("@Wo_Sett_Custom_Titlebar", 1 + "#varchar#" + model.Wo_Sett_Custom_Titlebar);
                input_parameters.Add("@Wo_Sett_Default_Time", 1 + "#bigint#" + model.Wo_Sett_Default_Time);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@WO_Sett_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    newWorkOrderSetting.WO_Sett_pkeyID = "0";
                    newWorkOrderSetting.Status = "0";
                    newWorkOrderSetting.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    newWorkOrderSetting.WO_Sett_pkeyID = Convert.ToString(objData[0]);
                    newWorkOrderSetting.Status = Convert.ToString(objData[1]);


                }
                objcltData.Add(newWorkOrderSetting);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;



        }

        private DataSet GetNewworkordersettingMaster(NewWorkOrderSettingDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_NewWorkOrderSetting]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@WO_Sett_pkeyID", 1 + "#bigint#" + model.WO_Sett_pkeyID);
                input_parameters.Add("@WO_Sett_UserID", 1 + "#bigint#" + model.WO_Sett_UserId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }
        //Get New work order setting Details 
        public List<dynamic> GetNewworkordersettingDetails(NewWorkOrderSettingDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {

                DataSet ds = GetNewworkordersettingMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<NewWorkOrderSettingDTO> newworkordersetting =
                   (from item in myEnumerableFeaprd
                    select new NewWorkOrderSettingDTO
                    {
                        WO_Sett_pkeyID = item.Field<Int64>("WO_Sett_pkeyID"),
                        WO_Sett_CompanyID = item.Field<Int64?>("WO_Sett_CompanyID"),
                        WO_Sett_Allow_Dup_Num = item.Field<Boolean?>("WO_Sett_Allow_Dup_Num"),
                        WO_Sett_Auto_Inc_GoBack = item.Field<Boolean?>("WO_Sett_Auto_Inc_GoBack"),
                        WO_Sett_Auto_Inc_NeedInfo = item.Field<Boolean?>("WO_Sett_Auto_Inc_NeedInfo"),
                        WO_Sett_Auto_Inc_Dup = item.Field<Boolean?>("WO_Sett_Auto_Inc_Dup"),
                        WO_Sett_Auto_Inc_Recurring = item.Field<Boolean?>("WO_Sett_Auto_Inc_Recurring"),
                        WO_Sett_Auto_Assign = item.Field<Boolean?>("WO_Sett_Auto_Assign"),
                        WO_Sett_Detect_Pricing = item.Field<Boolean?>("WO_Sett_Detect_Pricing"),
                        WO_Sett_Remove_Doller = item.Field<Boolean?>("WO_Sett_Remove_Doller"),
                        WO_Sett_IsActive = item.Field<Boolean?>("WO_Sett_IsActive"),
                        WO_Sett_UserId = item.Field<Int64?>("WO_Sett_UserId"),
                        Wo_Sett_Comapny_SAlert = item.Field<String>("Wo_Sett_Comapny_SAlert"),
                        Wo_Sett_Custom_Titlebar = item.Field<String>("Wo_Sett_Custom_Titlebar"),
                        Wo_Sett_Default_Time = item.Field<Int64?>("Wo_Sett_Default_Time"),
                        WO_Sett_CreatedBy = item.Field<String>("WO_Sett_CreatedBy"),
                        WO_Sett_ModifiedBy = item.Field<String>("WO_Sett_ModifiedBy"),

                    }).ToList();

                objDynamic.Add(newworkordersetting);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}