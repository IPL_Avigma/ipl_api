﻿using Avigma.Repository.Lib;
using IPL.Models;
using IPLApp.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class Invoice_ContractorData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public List<dynamic> AddInvoiceContractorData(Invoice_ContractorDTO model)
        {

            string insertProcedure = "[CreateUpdateInvoice_Contractor]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Inv_Con_pkeyId", 1 + "#bigint#" + model.Inv_Con_pkeyId);
                input_parameters.Add("@Inv_Con_Invoice_Id", 1 + "#bigint#" + model.Inv_Con_Invoice_Id);
                input_parameters.Add("@Inv_Con_TaskId", 1 + "#bigint#" + model.Inv_Con_TaskId);
                input_parameters.Add("@Inv_Con_Wo_ID", 1 + "#bigint#" + model.Inv_Con_Wo_ID);
                input_parameters.Add("@Inv_Con_Uom_Id", 1 + "#bigint#" + model.Inv_Con_Uom_Id);
                input_parameters.Add("@Inv_Con_Sub_Total", 1 + "#decimal#" + model.Inv_Con_Sub_Total);
                input_parameters.Add("@Inv_Con_ContDiscount", 1 + "#int#" + model.Inv_Con_ContDiscount);
                input_parameters.Add("@Inv_Con_ContTotal", 1 + "#decimal#" + model.Inv_Con_ContTotal);
                input_parameters.Add("@Inv_Con_Short_Note", 1 + "#varchar#" + model.Inv_Con_Short_Note);
                input_parameters.Add("@Inv_Con_Inv_Followup", 1 + "#bit#" + model.Inv_Con_Inv_Followup);
                input_parameters.Add("@Inv_Con_Inv_Comment", 1 + "#varchar#" + model.Inv_Con_Inv_Comment);
                input_parameters.Add("@Inv_Con_Ref_ID", 1 + "#varchar#" + model.Inv_Con_Ref_ID);
                input_parameters.Add("@Inv_Con_Followup_Com", 1 + "#bit#" + model.Inv_Con_Followup_Com);
                input_parameters.Add("@Inv_Con_Invoce_Num", 1 + "#varchar#" + model.Inv_Con_Invoce_Num);
                input_parameters.Add("@Inv_Con_Inv_Date", 1 + "#datetime#" + model.Inv_Con_Inv_Date);
                input_parameters.Add("@Inv_Con_Inv_Hold_Date", 1 + "#datetime#" + model.Inv_Con_Inv_Hold_Date);

                input_parameters.Add("@Inv_Con_Status", 1 + "#int#" + model.Inv_Con_Status);
                input_parameters.Add("@Inv_Con_DiscountAmount", 1 + "#decimal#" + model.Inv_Con_DiscountAmount);
                input_parameters.Add("@Inv_Con_IsActive", 1 + "#bit#" + model.Inv_Con_IsActive);
                input_parameters.Add("@Inv_Con_IsDelete", 1 + "#bit#" + model.Inv_Con_IsDelete);
                input_parameters.Add("@Inv_Con_Auto_Invoice", 1 + "#bit#" + model.Inv_Con_Auto_Invoice);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);

                input_parameters.Add("@Inv_Con_Inv_Approve_Date", 1 + "#datetime#" + model.Inv_Con_Inv_Approve_Date);
                input_parameters.Add("@Inv_Con_Inv_Approve", 1 + "#bit#" + model.Inv_Con_Inv_Approve);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Inv_Con_pkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return obj.SqlCRUD(insertProcedure, input_parameters);



        }
        private DataSet GetInvoiceContractorMaster(Invoice_ContractorDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Invoice_Contractor]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Inv_Con_pkeyId", 1 + "#bigint#" + model.Inv_Con_pkeyId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetInvoiceContractorDetails(Invoice_ContractorDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetInvoiceContractorMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Invoice_ContractorDTO> InvoiceContractor =
                   (from item in myEnumerableFeaprd
                    select new Invoice_ContractorDTO
                    {
                        Inv_Con_pkeyId = item.Field<Int64>("Inv_Con_pkeyId"),
                        Inv_Con_Invoice_Id = item.Field<Int64?>("Inv_Con_Invoice_Id"),
                        Inv_Con_TaskId = item.Field<Int64?>("Inv_Con_TaskId"),
                        Inv_Con_Wo_ID = item.Field<Int64?>("Inv_Con_Wo_ID"),
                        Inv_Con_Uom_Id = item.Field<Int64?>("Inv_Con_Uom_Id"),
                        Inv_Con_Sub_Total = item.Field<Decimal?>("Inv_Con_Sub_Total"),
                        Inv_Con_ContDiscount = item.Field<int?>("Inv_Con_ContDiscount"),
                        Inv_Con_ContTotal = item.Field<Decimal?>("Inv_Con_ContTotal"),
                        Inv_Con_Short_Note = item.Field<String>("Inv_Con_Short_Note"),
                        Inv_Con_Inv_Followup = item.Field<Boolean?>("Inv_Con_Inv_Followup"),
                        Inv_Con_Inv_Comment = item.Field<String>("Inv_Con_Inv_Comment"),
                        Inv_Con_Ref_ID = item.Field<String>("Inv_Con_Ref_ID"),
                        Inv_Con_Followup_Com = item.Field<Boolean?>("Inv_Con_Followup_Com"),
                        Inv_Con_Invoce_Num = item.Field<String>("Inv_Con_Invoce_Num"),
                        Inv_Con_Inv_Date = item.Field<DateTime?>("Inv_Con_Inv_Date"),
                        Inv_Con_Inv_Hold_Date = item.Field<DateTime?>("Inv_Con_Inv_Hold_Date"),
                        Inv_Con_Status = item.Field<int?>("Inv_Con_Status"),
                        Inv_Con_DiscountAmount = item.Field<Decimal?>("Inv_Con_DiscountAmount"),
                        Inv_Con_IsActive = item.Field<Boolean?>("Inv_Con_IsActive"),
                        Inv_Con_Inv_Approve_Date = item.Field<DateTime?>("Inv_Con_Inv_Approve_Date"),
                        Inv_Con_Inv_Approve = item.Field<Boolean?>("Inv_Con_Inv_Approve"),

                    }).ToList();

                objDynamic.Add(InvoiceContractor);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }



        private DataSet GetInvoiceContractor_ClinetMaster(Invoice_ContractorDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Invoice_Client_Con_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();
                input_parameters.Add("@Inv_Con_Wo_ID", 1 + "#bigint#" + model.Inv_Con_Wo_ID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);



                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetInvoiceContractor_ClientData_Details(Invoice_ContractorDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                model.Type = 1;

                DataSet ds = GetInvoiceContractor_ClinetMaster(model);
                if (ds.Tables.Count > 0)
                {
                    var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                    List<Invoice_Contractor_ChildDTO> InvoiceContractorChild =
                       (from item in myEnumerableFeaprd
                        select new Invoice_Contractor_ChildDTO
                        {
                            Inv_Con_Ch_pkeyId = item.Field<Int64>("Inv_Con_Ch_pkeyId"),
                            Inv_Con_Ch_ContractorId = item.Field<Int64?>("Inv_Con_Ch_ContractorId"),
                            Inv_Con_Ch_InvoiceId = item.Field<Int64?>("Inv_Con_Ch_InvoiceId"),
                            Inv_Con_Ch_TaskId = item.Field<Int64?>("Inv_Con_Ch_TaskId"),
                            //Inv_Con_Ch_Wo_Id = item.Field<Int64?>("Inv_Con_Ch_Wo_Id"),
                            //Inv_Con_Ch_Uom_Id = item.Field<Int64?>("Inv_Con_Ch_Uom_Id"),
                            Inv_Con_Ch_Qty = item.Field<String>("Inv_Con_Ch_Qty"),
                            Inv_Con_Ch_Price = item.Field<Decimal?>("Inv_Con_Ch_Price"),
                            Inv_Con_Ch_Total = item.Field<Decimal?>("Inv_Con_Ch_Total"),
                            Inv_Con_Ch_Adj_Price = item.Field<Decimal?>("Inv_Con_Ch_Adj_Price"),
                            Inv_Con_Ch_Adj_Total = item.Field<Decimal?>("Inv_Con_Ch_Adj_Total"),
                            Inv_Con_Ch_Comment = item.Field<String>("Inv_Con_Ch_Comment"),
                            Inv_Con_Ch_IsActive = item.Field<Boolean?>("Inv_Con_Ch_IsActive"),
                            Inv_Con_Ch_IsDelete = item.Field<Boolean?>("Inv_Con_Ch_IsDelete"),
                            Inv_Con_Ch_Flate_fee = item.Field<Boolean?>("Inv_Con_Ch_Flate_fee"),
                            Inv_Con_Ch_Discount = item.Field<Decimal?>("Inv_Con_Ch_Discount"),
                            Inv_Con_Ch_Client_ID = item.Field<Int64?>("Inv_Con_Ch_Client_ID"),



                        }).ToList();

                    objDynamic.Add(InvoiceContractorChild);
                }

                if (ds.Tables.Count > 1)
                {
                    var myEnumerableFeaprdx = ds.Tables[1].AsEnumerable();
                    List<Invoice_Client_ChildDTO> InvoiceClientChild =
                       (from item in myEnumerableFeaprdx
                        select new Invoice_Client_ChildDTO
                        {
                            Inv_Client_Ch_pkeyId = item.Field<Int64>("Inv_Client_Ch_pkeyId"),
                            Inv_Client_Ch_ClientID = item.Field<Int64?>("Inv_Client_Ch_ClientID"),
                            //Inv_Client_Ch_Wo_Id = item.Field<Int64?>("Inv_Client_Ch_Wo_Id"),
                            Inv_Client_Ch_Invoice_Id = item.Field<Int64?>("Inv_Client_Ch_Invoice_Id"),
                            Inv_Client_Ch_Task_Id = item.Field<Int64?>("Inv_Client_Ch_Task_Id"),

                            //Inv_Client_Ch_Uom_Id = item.Field<Int64?>("Inv_Client_Ch_Uom_Id"),
                            Inv_Client_Ch_Qty = item.Field<String>("Inv_Client_Ch_Qty"),
                            Inv_Client_Ch_Price = item.Field<Decimal?>("Inv_Client_Ch_Price"),
                            Inv_Client_Ch_Total = item.Field<Decimal?>("Inv_Client_Ch_Total"),
                            Inv_Client_Ch_Adj_Price = item.Field<Decimal?>("Inv_Client_Ch_Adj_Price"),
                            Inv_Client_Ch_Adj_Total = item.Field<Decimal?>("Inv_Client_Ch_Adj_Total"),
                            Inv_Client_Ch_Comment = item.Field<String>("Inv_Client_Ch_Comment"),
                            Inv_Client_Ch_IsActive = item.Field<Boolean?>("Inv_Client_Ch_IsActive"),
                            Inv_Client_Ch_IsDelete = item.Field<Boolean?>("Inv_Client_Ch_IsDelete"),
                            Inv_Client_Ch_Flate_Fee = item.Field<Boolean?>("Inv_Client_Ch_Flate_Fee"),
                            Inv_Client_Ch_Discount = item.Field<Decimal?>("Inv_Client_Ch_Discount"),

                        }).ToList();

                    objDynamic.Add(InvoiceClientChild);
                }

                if (ds.Tables.Count > 2)
                {
                    var myEnumerableFeaprdxxx = ds.Tables[2].AsEnumerable();
                    List<Invoice_ContractorDTO> InvoiceContractor =
                       (from item in myEnumerableFeaprdxxx
                        select new Invoice_ContractorDTO
                        {
                            Inv_Con_pkeyId = item.Field<Int64>("Inv_Con_pkeyId"),
                            Inv_Con_Invoice_Id = item.Field<Int64?>("Inv_Con_Invoice_Id"),
                            Inv_Con_TaskId = item.Field<Int64?>("Inv_Con_TaskId"),
                            Inv_Con_Wo_ID = item.Field<Int64?>("Inv_Con_Wo_ID"),
                            Inv_Con_Uom_Id = item.Field<Int64?>("Inv_Con_Uom_Id"),
                            Inv_Con_Sub_Total = item.Field<Decimal?>("Inv_Con_Sub_Total"),
                            Inv_Con_ContDiscount = item.Field<int?>("Inv_Con_ContDiscount"),
                            Inv_Con_ContTotal = item.Field<Decimal?>("Inv_Con_ContTotal"),
                            Inv_Con_Short_Note = item.Field<String>("Inv_Con_Short_Note"),
                            Inv_Con_Inv_Followup = item.Field<Boolean?>("Inv_Con_Inv_Followup"),
                            Inv_Con_Inv_Comment = item.Field<String>("Inv_Con_Inv_Comment"),
                            Inv_Con_Ref_ID = item.Field<String>("Inv_Con_Ref_ID"),
                            Inv_Con_Followup_Com = item.Field<Boolean?>("Inv_Con_Followup_Com"),
                            Inv_Con_Invoce_Num = item.Field<String>("Inv_Con_Invoce_Num"),
                            Inv_Con_Inv_Date = item.Field<DateTime?>("Inv_Con_Inv_Date"),
                            Inv_Con_Inv_Hold_Date = item.Field<DateTime?>("Inv_Con_Inv_Hold_Date"),
                            Inv_Con_Status = item.Field<int?>("Inv_Con_Status"),
                            Inv_Con_DiscountAmount = item.Field<Decimal?>("Inv_Con_DiscountAmount"),
                            Inv_Con_IsActive = item.Field<Boolean?>("Inv_Con_IsActive"),
                            Inv_Con_IsDelete = item.Field<Boolean?>("Inv_Con_IsDelete"),
                            Inv_Con_Auto_Invoice = item.Field<Boolean?>("Inv_Con_Auto_Invoice"),
                            Inv_Con_Inv_Approve_Date = item.Field<DateTime?>("Inv_Con_Inv_Approve_Date"),
                            Inv_Con_Inv_Approve = item.Field<Boolean?>("Inv_Con_Inv_Approve"),


                        }).ToList();

                    objDynamic.Add(InvoiceContractor);
                }


                if (ds.Tables.Count > 3)
                {
                    var myEnumerableFeaprdxs = ds.Tables[3].AsEnumerable();
                    List<Invoice_ClientDTO> InvoiceClient =
                       (from item in myEnumerableFeaprdxs
                        select new Invoice_ClientDTO
                        {
                            Inv_Client_pkeyId = item.Field<Int64>("Inv_Client_pkeyId"),
                            Inv_Client_Invoice_Id = item.Field<Int64?>("Inv_Client_Invoice_Id"),
                            Inv_Client_WO_Id = item.Field<Int64?>("Inv_Client_WO_Id"),
                            Inv_Client_Task_Id = item.Field<Int64?>("Inv_Client_Task_Id"),
                            Inv_Client_Uom_Id = item.Field<Int64?>("Inv_Client_Uom_Id"),
                            Inv_Client_Sub_Total = item.Field<Decimal?>("Inv_Client_Sub_Total"),
                            Inv_Client_Client_Dis = item.Field<int?>("Inv_Client_Client_Dis"),
                            Inv_Client_Client_Total = item.Field<Decimal?>("Inv_Client_Client_Total"),
                            Inv_Client_Short_Note = item.Field<String>("Inv_Client_Short_Note"),
                            Inv_Client_Inv_Complete = item.Field<Boolean?>("Inv_Client_Inv_Complete"),
                            Inv_Client_Credit_Memo = item.Field<Boolean?>("Inv_Client_Credit_Memo"),
                            Inv_Client_Sent_Client = item.Field<DateTime?>("Inv_Client_Sent_Client"),
                            Inv_Client_Comp_Date = item.Field<DateTime?>("Inv_Client_Comp_Date"),


                            Inv_Client_Invoice_Number = item.Field<String>("Inv_Client_Invoice_Number"),
                            Inv_Client_Inv_Date = item.Field<DateTime?>("Inv_Client_Inv_Date"),
                            Inv_Client_Internal_Note = item.Field<String>("Inv_Client_Internal_Note"),
                            Inv_Client_Status = item.Field<int?>("Inv_Client_Status"),
                            Inv_Client_Discout_Amount = item.Field<decimal?>("Inv_Client_Discout_Amount"),
                            Inv_Client_IsActive = item.Field<Boolean?>("Inv_Client_IsActive"),
                            Inv_Client_IsDelete = item.Field<Boolean?>("Inv_Client_IsDelete"),
                            Inv_Client_Auto_Invoice = item.Field<Boolean?>("Inv_Client_Auto_Invoice"),
                            Inv_Client_Followup = item.Field<Boolean?>("Inv_Client_Followup"),
                            Inv_Client_Hold_Date = item.Field<DateTime?>("Inv_Client_Hold_Date"),
                            //Inv_Con_Inv_Approve_Date = item.Field<DateTime?>("Inv_Con_Inv_Approve_Date"),
                            //Inv_Con_Inv_Approve = item.Field<Boolean?>("Inv_Con_Inv_Approve"),

                            Inv_Client_IsNoCharge = item.Field<bool?>("Inv_Client_IsNoCharge"),
                            Inv_Client_NoChargeDate = item.Field<DateTime?>("Inv_Client_NoChargeDate"),

                        }).ToList();

                    objDynamic.Add(InvoiceClient);
                }

                if (ds.Tables.Count > 4)
                {

                    //for common hedder meta data
                    var myEnumerableFeaprdmd = ds.Tables[4].AsEnumerable();
                    List<MetadataWorkOrder> medata =
                       (from item in myEnumerableFeaprdmd
                        select new MetadataWorkOrder
                        {
                            workOrder_ID = item.Field<Int64>("workOrder_ID"),
                            workOrderNumber = item.Field<String>("workOrderNumber"),
                            workOrderInfo = item.Field<String>("workOrderInfo"),
                            address1 = item.Field<String>("address1"),
                            address2 = item.Field<String>("address2"),
                            zip = item.Field<Int64?>("zip"),
                            country = item.Field<String>("country"),
                            status = item.Field<String>("status"),
                            dueDate = item.Field<DateTime?>("dueDate"),
                            startDate = item.Field<DateTime?>("startDate"),
                            // clientInstructions = item.Field<String>("clientInstructions"),
                            clientStatus = item.Field<String>("clientStatus"),
                            clientDueDate = item.Field<DateTime?>("clientDueDate"),
                            finaladdress = item.Field<String>("finaladdress"),
                            fulladdress = item.Field<String>("fulladdress"),
                            SM_Name = item.Field<String>("SM_Name"),
                            WT_WorkType = item.Field<String>("WT_WorkType"),
                            Client_Company_Name = item.Field<String>("Client_Company_Name"),
                            Cont_Name = item.Field<String>("Cont_Name"),
                            Cordinator_Name = item.Field<String>("Cordinator_Name"),
                            Work_Type_Name = item.Field<String>("Work_Type_Name"),
                            Cust_Num_Number = item.Field<String>("Cust_Num_Number"),
                            ClientMetaData = item.Field<String>("ClientMetaData"),
                            statusid = item.Field<int?>("statusid"),
                            ClientAddress = item.Field<String>("ClientAddress"),
                            ContractorAddress = item.Field<String>("ContractorAddress"),
                            Loan_Number = item.Field<String>("Loan_Number"),
                            ClientName = item.Field<String>("ClientName"),
                            Client_Discount = item.Field<decimal?>("Client_Discount"),
                            Client_Contractor_Discount = item.Field<decimal?>("Client_Contractor_Discount"),


                        }).ToList();

                    objDynamic.Add(medata);
                }

                if (ds.Tables.Count > 5)
                {
                    //contractor payment

                    var myEnumerableFeaprcon = ds.Tables[5].AsEnumerable();
                    List<Contractor_Invoice_PaymentDTO> conpay =
                       (from item in myEnumerableFeaprcon
                        select new Contractor_Invoice_PaymentDTO
                        {
                            Con_Pay_PkeyId = item.Field<Int64>("Con_Pay_PkeyId"),
                            Con_Pay_Invoice_Id = item.Field<Int64?>("Con_Pay_Invoice_Id"),
                            Con_Pay_Wo_Id = item.Field<Int64?>("Con_Pay_Wo_Id"),
                            Con_Pay_Payment_Date = item.Field<DateTime>("Con_Pay_Payment_Date"),
                            Con_Pay_Amount = item.Field<Decimal?>("Con_Pay_Amount"),
                            Con_Pay_CheckNumber = item.Field<String>("Con_Pay_CheckNumber"),
                            Con_Pay_Comment = item.Field<String>("Con_Pay_Comment"),
                            Con_Pay_EnteredBy = item.Field<String>("Con_Pay_EnteredBy"),
                            Con_Pay_Balance_Due = item.Field<Decimal?>("Con_Pay_Balance_Due"),
                            Con_Pay_IsActive = item.Field<Boolean?>("Con_Pay_IsActive"),


                        }).ToList();

                    objDynamic.Add(conpay);
                }

                if (ds.Tables.Count > 6)
                {
                    //client payment

                    var myEnumerableFeaprclient = ds.Tables[6].AsEnumerable();
                    List<Client_Invoice_PatymentDTO> clientpay =
                       (from item in myEnumerableFeaprclient
                        select new Client_Invoice_PatymentDTO
                        {
                            Client_Pay_PkeyId = item.Field<Int64>("Client_Pay_PkeyId"),
                            Client_Pay_Invoice_Id = item.Field<Int64?>("Client_Pay_Invoice_Id"),
                            Client_Pay_Wo_Id = item.Field<Int64?>("Client_Pay_Wo_Id"),
                            Client_Pay_Payment_Date = item.Field<DateTime>("Client_Pay_Payment_Date"),
                            Client_Pay_Amount = item.Field<Decimal?>("Client_Pay_Amount"),
                            Client_Pay_CheckNumber = item.Field<String>("Client_Pay_CheckNumber"),
                            Client_Pay_Comment = item.Field<String>("Client_Pay_Comment"),
                            Client_Pay_EnteredBy = item.Field<String>("Client_Pay_EnteredBy"),
                            Client_Pay_Balance_Due = item.Field<Decimal?>("Client_Pay_Balance_Due"),
                            Client_Pay_IsActive = item.Field<Boolean?>("Client_Pay_IsActive"),


                        }).ToList();

                    objDynamic.Add(clientpay);
                }

                if (ds.Tables.Count > 7)
                {
                    //Contractor_Client_Expense_Paymen

                    var myEnumerableFeaprconclient = ds.Tables[7].AsEnumerable();
                    List<Contractor_Client_Expense_Payment_DTO> conclientpay =
                       (from item in myEnumerableFeaprconclient
                        select new Contractor_Client_Expense_Payment_DTO
                        {
                            CCE_Pay_PkeyId = item.Field<Int64>("CCE_Pay_PkeyId"),
                            CCE_Client_Pay_Invoice_Id = item.Field<Int64?>("CCE_Client_Pay_Invoice_Id"),
                            CCE_Con_Pay_Invoice_Id = item.Field<Int64?>("CCE_Con_Pay_Invoice_Id"),
                            CCE_Pay_Wo_Id = item.Field<Int64?>("CCE_Pay_Wo_Id"),
                            CCE_Pay_Payment_Date = item.Field<DateTime?>("CCE_Pay_Payment_Date"),
                            CCE_Pay_Amount = item.Field<Decimal?>("CCE_Pay_Amount"),
                            CCE_Pay_EnteredBy = item.Field<Int64?>("CCE_Pay_EnteredBy"),
                            CCE_Pay_Comment = item.Field<String>("CCE_Pay_Comment"),
                            CCE_Pay_Balance_Due = item.Field<Decimal?>("CCE_Pay_Balance_Due"),
                            CCE_Pay_IsActive = item.Field<Boolean?>("CCE_Pay_IsActive"),
                            CCE_Pay_Expense_Name = item.Field<String>("CCE_Pay_Expense_Name"),



                        }).ToList();

                    objDynamic.Add(conclientpay);
                }

                if (ds.Tables.Count > 8)
                {
                    if (ds.Tables[8].Rows.Count > 0)
                    {
                        var myEnumeraleScoreData = ds.Tables[8].AsEnumerable();
                        List<ScoreCard_DataDTO> ScoreCard_DataDTO =
                           (from item in myEnumeraleScoreData
                            select new ScoreCard_DataDTO
                            {
                                Scd_pkeyId = item.Field<Int64>("Scd_pkeyId"),
                                Scd_Con_ID = item.Field<Int64?>("Scd_Con_ID"),
                                Scd_Comment = item.Field<String>("Scd_Comment"),
                                Scd_Status_Id = item.Field<int>("Scd_Status_Id"),
                                ScoreCard_DTO = GetScoreCard(ds.Tables[9])



                            }).ToList();
                        objDynamic.Add(ScoreCard_DataDTO);
                    }
                    else
                    {
                        List<ScoreCard_DataDTO> lstScoreCard_DataDTO = new List<ScoreCard_DataDTO>();
                        ScoreCard_DataDTO scoreCard_DataDTO = new ScoreCard_DataDTO();
                        scoreCard_DataDTO.ScoreCard_DTO = GetScoreCard(ds.Tables[9]);
                        lstScoreCard_DataDTO.Add(scoreCard_DataDTO);
                        objDynamic.Add(lstScoreCard_DataDTO);

                    }




                }



            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }


        private List<ScoreCard_DTO> GetScoreCard(DataTable dt)
        {
            try
            {
                var myEnumeraleScoreData = dt.AsEnumerable();
                List<ScoreCard_DTO> ScoreCard_DTO =
                   (from item in myEnumeraleScoreData
                    select new ScoreCard_DTO
                    {
                        Scdc_pkeyId = item.Field<Int64>("Scdc_pkeyId"),
                        Sna_pkeyId = item.Field<Int64?>("Sna_pkeyId"),
                        Sna_Name = item.Field<String>("Sna_Name"),
                        str_Scdc_Status_Id = item.Field<String>("str_Scdc_Status_Id"),




                    }).ToList();

                return ScoreCard_DTO;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return null;
            }


        }

        public List<dynamic> GetInvoiceContractor_Mobile_Details(Invoice_ContractorDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {

                model.Type = 2;
                DataSet ds = GetInvoiceContractor_ClinetMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Invoice_Contractor_ChildDTO> InvoiceContractorChild =
                   (from item in myEnumerableFeaprd
                    select new Invoice_Contractor_ChildDTO
                    {
                        Inv_Con_Ch_pkeyId = item.Field<Int64>("Inv_Con_Ch_pkeyId"),
                        Inv_Con_Ch_ContractorId = item.Field<Int64?>("Inv_Con_Ch_ContractorId"),
                        Inv_Con_Ch_InvoiceId = item.Field<Int64?>("Inv_Con_Ch_InvoiceId"),
                        Inv_Con_Ch_TaskId = item.Field<Int64?>("Inv_Con_Ch_TaskId"),
                        //Inv_Con_Ch_Wo_Id = item.Field<Int64?>("Inv_Con_Ch_Wo_Id"),
                        //Inv_Con_Ch_Uom_Id = item.Field<Int64?>("Inv_Con_Ch_Uom_Id"),
                        Inv_Con_Ch_Qty = item.Field<String>("Inv_Con_Ch_Qty"),
                        Inv_Con_Ch_Price = item.Field<Decimal?>("Inv_Con_Ch_Price"),
                        Inv_Con_Ch_Total = item.Field<Decimal?>("Inv_Con_Ch_Total"),
                        Inv_Con_Ch_Adj_Price = item.Field<Decimal?>("Inv_Con_Ch_Adj_Price"),
                        Inv_Con_Ch_Adj_Total = item.Field<Decimal?>("Inv_Con_Ch_Adj_Total"),
                        Inv_Con_Ch_Comment = item.Field<String>("Inv_Con_Ch_Comment"),
                        Inv_Con_Ch_IsActive = item.Field<Boolean?>("Inv_Con_Ch_IsActive"),
                        Inv_Con_Ch_IsDelete = item.Field<Boolean?>("Inv_Con_Ch_IsDelete"),
                        Inv_Con_Ch_Flate_fee = item.Field<Boolean?>("Inv_Con_Ch_Flate_fee"),
                        Inv_Con_Ch_Discount = item.Field<Decimal?>("Inv_Con_Ch_Discount"),
                        Inv_Con_Ch_Client_ID = item.Field<Int64?>("Inv_Con_Ch_Client_ID"),
                        Inv_Task_Name = item.Field<string>("Inv_Task_Name"),


                    }).ToList();

                objDynamic.Add(InvoiceContractorChild);




                var myEnumerableFeaprdxxx = ds.Tables[1].AsEnumerable();
                List<Invoice_ContractorDTO> InvoiceContractor =
                   (from item in myEnumerableFeaprdxxx
                    select new Invoice_ContractorDTO
                    {
                        Inv_Con_pkeyId = item.Field<Int64>("Inv_Con_pkeyId"),
                        Inv_Con_Invoice_Id = item.Field<Int64?>("Inv_Con_Invoice_Id"),
                        Inv_Con_TaskId = item.Field<Int64?>("Inv_Con_TaskId"),
                        Inv_Con_Wo_ID = item.Field<Int64?>("Inv_Con_Wo_ID"),
                        Inv_Con_Uom_Id = item.Field<Int64?>("Inv_Con_Uom_Id"),
                        Inv_Con_Sub_Total = item.Field<Decimal?>("Inv_Con_Sub_Total"),
                        Inv_Con_ContDiscount = item.Field<int?>("Inv_Con_ContDiscount"),
                        Inv_Con_ContTotal = item.Field<Decimal?>("Inv_Con_ContTotal"),
                        Inv_Con_Short_Note = item.Field<String>("Inv_Con_Short_Note"),
                        Inv_Con_Inv_Followup = item.Field<Boolean?>("Inv_Con_Inv_Followup"),
                        Inv_Con_Inv_Comment = item.Field<String>("Inv_Con_Inv_Comment"),
                        Inv_Con_Ref_ID = item.Field<String>("Inv_Con_Ref_ID"),
                        Inv_Con_Followup_Com = item.Field<Boolean?>("Inv_Con_Followup_Com"),
                        Inv_Con_Invoce_Num = item.Field<String>("Inv_Con_Invoce_Num"),
                        Inv_Con_Inv_Date = item.Field<DateTime?>("Inv_Con_Inv_Date"),
                        Inv_Con_Inv_Hold_Date = item.Field<DateTime?>("Inv_Con_Inv_Hold_Date"),
                        Inv_Con_Status = item.Field<int?>("Inv_Con_Status"),
                        Inv_Con_DiscountAmount = item.Field<Decimal?>("Inv_Con_DiscountAmount"),
                        Inv_Con_IsActive = item.Field<Boolean?>("Inv_Con_IsActive"),
                        Inv_Con_IsDelete = item.Field<Boolean?>("Inv_Con_IsDelete"),
                        Inv_Con_Auto_Invoice = item.Field<Boolean?>("Inv_Con_Auto_Invoice"),
                        Inv_Con_Inv_Approve_Date = item.Field<DateTime?>("Inv_Con_Inv_Approve_Date"),
                        Inv_Con_Inv_Approve = item.Field<Boolean?>("Inv_Con_Inv_Approve"),


                    }).ToList();

                objDynamic.Add(InvoiceContractor);



                //for common hedder meta data
                var myEnumerableFeaprdmd = ds.Tables[2].AsEnumerable();
                List<MetadataWorkOrder> medata =
                   (from item in myEnumerableFeaprdmd
                    select new MetadataWorkOrder
                    {
                        workOrder_ID = item.Field<Int64>("workOrder_ID"),
                        workOrderNumber = item.Field<String>("workOrderNumber"),
                        workOrderInfo = item.Field<String>("workOrderInfo"),
                        address1 = item.Field<String>("address1"),
                        address2 = item.Field<String>("address2"),
                        zip = item.Field<Int64?>("zip"),
                        country = item.Field<String>("country"),
                        status = item.Field<String>("status"),
                        dueDate = item.Field<DateTime?>("dueDate"),
                        startDate = item.Field<DateTime?>("startDate"),
                        // clientInstructions = item.Field<String>("clientInstructions"),
                        clientStatus = item.Field<String>("clientStatus"),
                        clientDueDate = item.Field<DateTime?>("clientDueDate"),
                        finaladdress = item.Field<String>("finaladdress"),
                        fulladdress = item.Field<String>("fulladdress"),
                        SM_Name = item.Field<String>("SM_Name"),
                        WT_WorkType = item.Field<String>("WT_WorkType"),
                        Client_Company_Name = item.Field<String>("Client_Company_Name"),
                        Cont_Name = item.Field<String>("Cont_Name"),
                        Cordinator_Name = item.Field<String>("Cordinator_Name"),
                        Work_Type_Name = item.Field<String>("Work_Type_Name"),
                        Cust_Num_Number = item.Field<String>("Cust_Num_Number"),
                        ClientMetaData = item.Field<String>("ClientMetaData"),
                        statusid = item.Field<int?>("statusid"),


                    }).ToList();

                objDynamic.Add(medata);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        //get import file for innstruction


        private DataSet GetworkorderimportfileMaster(WorkOrder_Import_FilesDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_WorkOrder_Import_Files]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();


                input_parameters.Add("@WIF_Pkey", 1 + "#bigint#" + model.WIF_Pkey);
                input_parameters.Add("@WIF_workOrder_ID", 1 + "#bigint#" + model.WIF_workOrder_ID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);



                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }
        public List<dynamic> Getworkorderimportfile_Details(WorkOrder_Import_FilesDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetworkorderimportfileMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<WorkOrder_Import_FilesDTO> importfiles =
                   (from item in myEnumerableFeaprd
                    select new WorkOrder_Import_FilesDTO
                    {
                        WIF_Pkey = item.Field<Int64>("WIF_Pkey"),
                        WIF_workOrder_ID = item.Field<Int64?>("WIF_workOrder_ID"),
                        WIF_FileName = item.Field<String>("WIF_FileName"),
                        WIF_FilePath = item.Field<String>("WIF_FilePath"),
                        WIF_IsActive = item.Field<Boolean?>("WIF_IsActive"),

                    }).ToList();

                objDynamic.Add(importfiles);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        public List<dynamic> GetWoContractorInvoiceDetail(Invoice_ContractorWoList model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                string wherecondition = string.Empty;
                string wo = null;


                if (model.WorkOrderIDList != null)
                {
                    for (int j = 0; j < model.WorkOrderIDList.Count; j++)
                    {
                        wo = wo == null ? model.WorkOrderIDList[j].WorkOrderID : wo + "," + model.WorkOrderIDList[j].WorkOrderID;
                    }
                }
                if (wo != null)
                {
                    wherecondition = " where inv.[Inv_Con_Wo_ID] IN (" + wo + ")";
                }
                model.whereClause = wherecondition;
                DataSet ds = GetWoContractorInvoiceData(model);
                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Invoice_ContractorWoDTO> InvoiceContractor =
                   (from item in myEnumerableFeaprd
                    select new Invoice_ContractorWoDTO
                    {
                        Inv_Con_pkeyId = item.Field<Int64>("Inv_Con_pkeyId"),
                        Inv_Con_Invoice_Id = item.Field<Int64?>("Inv_Con_Invoice_Id"),
                        Inv_Con_Wo_ID = item.Field<Int64?>("Inv_Con_Wo_ID"),
                        Inv_Con_Sub_Total = item.Field<Decimal?>("Inv_Con_Sub_Total"),
                        Inv_Con_Invoce_Num = item.Field<String>("Inv_Con_Invoce_Num"),
                        IPLNO = item.Field<String>("IPLNO"),
                        Pay_Amount = 0,
                        Pay_Comment = "",

                    }).ToList();

                objDynamic.Add(InvoiceContractor);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        private DataSet GetWoContractorInvoiceData(Invoice_ContractorWoList model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_MultipleContractorInvoice_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@whereClause", 1 + "#varchar#" + model.whereClause);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserId);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

    }
}