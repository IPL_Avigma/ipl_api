﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class Report_WO_Filter_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        public List<dynamic> AddReportWOFilterMasterData(Report_WO_Filter_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_Report_WO_Filter_Master]";
            Report_WO_Filter_Master report_WO_Filter_Master = new Report_WO_Filter_Master();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Report_WO_Filter_PkeyId", 1 + "#bigint#" + model.Report_WO_Filter_PkeyId);
                input_parameters.Add("@Report_WO_Filter_Name", 1 + "#varchar#" + model.Report_WO_Filter_Name);
                input_parameters.Add("@Report_WO_Filter_IsActive", 1 + "#bit#" + model.Report_WO_Filter_IsActive);
                input_parameters.Add("@Report_WO_Filter_IsDelete", 1 + "#bit#" + model.Report_WO_Filter_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Report_WO_Filter_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    report_WO_Filter_Master.Report_WO_Filter_PkeyId = "0";
                    report_WO_Filter_Master.Status = "0";
                    report_WO_Filter_Master.ErrorMessage = "Error while Saviving in the Database";
                }
                else
                {
                    report_WO_Filter_Master.Report_WO_Filter_PkeyId = Convert.ToString(objData[0]);
                    report_WO_Filter_Master.Status = Convert.ToString(objData[1]);
                }
                objcltData.Add(report_WO_Filter_Master);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;
        }
        private DataSet GetReportWOFilterMaster(Report_WO_Filter_MasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Report_WO_Filter_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Report_WO_Filter_PkeyId", 1 + "#bigint#" + model.Report_WO_Filter_PkeyId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ds;
        }
        public List<dynamic> GetReportWOFilterMasterData(Report_WO_Filter_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetReportWOFilterMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Report_WO_Filter_MasterDTO> report_WO_Filter_MasterDTO =
                   (from item in myEnumerableFeaprd
                    select new Report_WO_Filter_MasterDTO
                    {
                        Report_WO_Filter_PkeyId = item.Field<Int64>("Report_WO_Filter_PkeyId"),
                        Report_WO_Filter_Name = item.Field<String>("Report_WO_Filter_Name"),
                        Report_WO_Filter_IsActive = item.Field<Boolean?>("Report_WO_Filter_IsActive"),

                    }).ToList();

                objDynamic.Add(report_WO_Filter_MasterDTO);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        public List<dynamic> AddReportWOFilterChildData(Report_WO_Filter_ChildDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_Report_WO_Filter_Child]";
            Report_WO_Filter_Child report_WO_Filter_Child = new Report_WO_Filter_Child();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Report_WO_Filter_Ch_PkeyId", 1 + "#bigint#" + model.Report_WO_Filter_Ch_PkeyId);
                input_parameters.Add("@Report_WO_Filter_Ch_Master_FKeyId", 1 + "#bigint#" + model.Report_WO_Filter_Ch_Master_FKeyId);
                input_parameters.Add("@Report_WO_Filter_Ch_FeildName", 1 + "#varchar#" + model.Report_WO_Filter_Ch_FeildName);
                input_parameters.Add("@Report_WO_Filter_Ch_FeildId", 1 + "#bigint#" + model.Report_WO_Filter_Ch_FeildId);
                input_parameters.Add("@Report_WO_Filter_Ch_FeildOperator", 1 + "#varchar#" + model.Report_WO_Filter_Ch_FeildOperator);
                input_parameters.Add("@Report_WO_Filter_Ch_FeildValue", 1 + "#varchar#" + model.Report_WO_Filter_Ch_FeildValue);
                input_parameters.Add("@Report_WO_Filter_Ch_IsActive", 1 + "#bit#" + model.Report_WO_Filter_Ch_IsActive);
                input_parameters.Add("@Report_WO_Filter_Ch_IsDelete", 1 + "#bit#" + model.Report_WO_Filter_Ch_IsDelete);
                input_parameters.Add("@Report_WO_Filter_Ch_FeildDateValue", 1 + "#datetime#" + model.Report_WO_Filter_Ch_FeildDateValue);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Report_WO_Filter_Ch_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    report_WO_Filter_Child.Report_WO_Filter_Ch_PkeyId = "0";
                    report_WO_Filter_Child.Status = "0";
                    report_WO_Filter_Child.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    report_WO_Filter_Child.Report_WO_Filter_Ch_PkeyId = Convert.ToString(objData[0]);
                    report_WO_Filter_Child.Status = Convert.ToString(objData[1]);


                }
                objcltData.Add(report_WO_Filter_Child);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;



        }
        private DataSet GetReportWOFilterChild(Report_WO_Filter_ChildDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Report_WO_Filter_Child]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Report_WO_Filter_Ch_PkeyId", 1 + "#bigint#" + model.Report_WO_Filter_Ch_PkeyId);
                input_parameters.Add("@Report_WO_Filter_Ch_Master_FKeyId", 1 + "#bigint#" + model.Report_WO_Filter_Ch_Master_FKeyId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ds;
        }
        public List<dynamic> GetReportWOFilterChildData(Report_WO_Filter_ChildDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetReportWOFilterChild(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Report_WO_Filter_ChildDTO> report_WO_Filter_ChildDTO =
                   (from item in myEnumerableFeaprd
                    select new Report_WO_Filter_ChildDTO
                    {
                        Report_WO_Filter_Ch_PkeyId = item.Field<Int64>("Report_WO_Filter_Ch_PkeyId"),
                        Report_WO_Filter_Ch_Master_FKeyId = item.Field<Int64?>("Report_WO_Filter_Ch_Master_FKeyId"),
                        Report_WO_Filter_Ch_FeildName = item.Field<String>("Report_WO_Filter_Ch_FeildName"),
                        Report_WO_Filter_Ch_FeildId = item.Field<Int64?>("Report_WO_Filter_Ch_FeildId"),
                        Report_WO_Filter_Ch_FeildOperator = item.Field<String>("Report_WO_Filter_Ch_FeildOperator"),
                        Report_WO_Filter_Ch_FeildValue = item.Field<String>("Report_WO_Filter_Ch_FeildValue"),
                        Report_WO_Filter_Ch_IsActive = item.Field<Boolean?>("Report_WO_Filter_Ch_IsActive"),
                        Report_WO_Filter_Ch_FeildDateValue = item.Field<DateTime?>("Report_WO_Filter_Ch_FeildDateValue")
                    }).ToList();

                objDynamic.Add(report_WO_Filter_ChildDTO);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        public List<Report_WO_Filter_ChildDTO> GetReportWOFilterChildList(Report_WO_Filter_ChildDTO model)
        {
            try
            {
                DataSet ds = GetReportWOFilterChild(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Report_WO_Filter_ChildDTO> report_WO_Filter_ChildDTO =
                   (from item in myEnumerableFeaprd
                    select new Report_WO_Filter_ChildDTO
                    {
                        Report_WO_Filter_Ch_PkeyId = item.Field<Int64>("Report_WO_Filter_Ch_PkeyId"),
                        Report_WO_Filter_Ch_Master_FKeyId = item.Field<Int64?>("Report_WO_Filter_Ch_Master_FKeyId"),
                        Report_WO_Filter_Ch_FeildName = item.Field<String>("Report_WO_Filter_Ch_FeildName"),
                        Report_WO_Filter_Ch_FeildId = item.Field<Int64?>("Report_WO_Filter_Ch_FeildId"),
                        Report_WO_Filter_Ch_FeildOperator = item.Field<String>("Report_WO_Filter_Ch_FeildOperator"),
                        Report_WO_Filter_Ch_FeildValue = item.Field<String>("Report_WO_Filter_Ch_FeildValue"),
                        Report_WO_Filter_Ch_IsActive = item.Field<Boolean?>("Report_WO_Filter_Ch_IsActive"),
                        Report_WO_Filter_Ch_FeildDateValue = item.Field<DateTime?>("Report_WO_Filter_Ch_FeildDateValue")
                    }).ToList();

                return report_WO_Filter_ChildDTO;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return null;
        }
    }
}