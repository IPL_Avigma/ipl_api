﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class TaskViolationMasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();


        public List<dynamic> AddTaskViolationMasterData(TaskViolationMaster model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();

            string insertProcedure = "[CreateUpdateTask_Violation_Master]";
            TaskViolation taskViolation = new TaskViolation();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Task_Violation_pkeyID", 1 + "#bigint#" + model.Task_Violation_pkeyID);
                input_parameters.Add("@Task_Violation_WO_ID", 1 + "#bigint#" + model.Task_Violation_WO_ID);
                input_parameters.Add("@Task_Violation_Name", 1 + "#varchar#" + model.Task_Violation_Name);
                input_parameters.Add("@Task_Violation_Date", 1 + "#datetime#" + model.Task_Violation_Date);
                input_parameters.Add("@Task_Violation_Deadline", 1 + "#datetime#" + model.Task_Violation_Deadline);
                input_parameters.Add("@Task_Violation_Id", 1 + "#varchar#" + model.Task_Violation_Id);
                input_parameters.Add("@Task_Violation_Date_Discovered", 1 + "#datetime#" + model.Task_Violation_Date_Discovered);
                input_parameters.Add("@Task_Violation_Fine_Amount", 1 + "#decimal#" + model.Task_Violation_Fine_Amount);
                input_parameters.Add("@Task_Violation_Contact", 1 + "#varchar#" + model.Task_Violation_Contact);
                input_parameters.Add("@Task_Violation_Comment", 1 + "#varchar#" + model.Task_Violation_Comment);


                //input_parameters.Add("@Task_Violation_IsActive", 1 + "#bit#" + model.Task_Violation_IsActive);
                //input_parameters.Add("@Task_Violation_IsDelete", 1 + "#bit#" + model.Task_Violation_IsDelete);

                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Task_Violation_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters); ;
                if (objData[1] == 0)
                {
                    taskViolation.Task_Violation_pkeyID = "0";
                    taskViolation.Status = "0";
                    taskViolation.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    taskViolation.Task_Violation_pkeyID = Convert.ToString(objData[0]);
                    taskViolation.Status = Convert.ToString(objData[1]);


                }
                objcltData.Add(taskViolation);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;



        }

        public List<dynamic> GetTaskViolationMasterDetails(TaskViolationMaster model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetTaskViolationMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<TaskViolationMaster> taskDamageMasterDetails =
                   (from item in myEnumerableFeaprd
                    select new TaskViolationMaster
                    {
                        Task_Violation_pkeyID = item.Field<Int64>("Task_Violation_pkeyID"),
                        Task_Violation_WO_ID = item.Field<Int64?>("Task_Violation_WO_ID"),
                        Task_Violation_Name = item.Field<String>("Task_Violation_Name"),
                        Task_Violation_Comment = item.Field<String>("Task_Violation_Comment"),
                        Task_Violation_Contact = item.Field<String>("Task_Violation_Contact"),
                        Task_Violation_Date = item.Field<DateTime?>("Task_Violation_Date"),
                        Task_Violation_Date_Discovered = item.Field<DateTime?>("Task_Violation_Date_Discovered"),
                        Task_Violation_Deadline = item.Field<DateTime?>("Task_Violation_Deadline"),
                        Task_Violation_Fine_Amount = item.Field<Decimal?>("Task_Violation_Fine_Amount"),
                        Task_Violation_Id = item.Field<String>("Task_Violation_Id"),
                        Task_Violation_IsActive = item.Field<Boolean?>("Task_Violation_IsActive"),

                    }).ToList();

                objDynamic.Add(taskDamageMasterDetails);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        private DataSet GetTaskViolationMaster(TaskViolationMaster model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Task_Violation_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Task_Violation_pkeyID", 1 + "#bigint#" + model.Task_Violation_pkeyID);
                input_parameters.Add("@Task_Violation_WO_ID", 1 + "#bigint#" + model.Task_Violation_WO_ID);
                input_parameters.Add("@Task_Violation_Status", 1 + "#int#" + model.Task_Violation_Status);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }
    }
}