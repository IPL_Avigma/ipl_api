﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class Filter_Admin_User_MasterDatacs
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        public List<dynamic> AddUpdate_Filter_Admin_User(Filter_Admin_User_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_Filter_Admin_User_Master]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@User_Filter_PkeyID", 1 + "#bigint#" + model.User_Filter_PkeyID);
                input_parameters.Add("@User_Filter_First_Name", 1 + "#nvarchar#" + model.User_Filter_First_Name);
                input_parameters.Add("@User_Filter_Last_Name", 1 + "#nvarchar#" + model.User_Filter_Last_Name);
                input_parameters.Add("@User_Filter_Company", 1 + "#nvarchar#" + model.User_Filter_Company);
                input_parameters.Add("@User_Filter_Login_email", 1 + "#nvarchar#" + model.User_Filter_Login_email);
                input_parameters.Add("@User_Filter_Group", 1 + "#bigint#" + model.User_Filter_Group);
                input_parameters.Add("@User_Filter_Mobile", 1 + "#nvarchar#" + model.User_Filter_Mobile);
                input_parameters.Add("@User_Filter_Company_ID", 1 + "#bigint#" + model.User_Filter_Company_ID);
                input_parameters.Add("@User_Filter_UserId", 1 + "#bigint#" + model.User_Filter_UserId);
                input_parameters.Add("@User_Filter_USRIsActive", 1 + "#bit#" + model.User_Filter_USRIsActive);
                input_parameters.Add("@User_Filter_IsActive", 1 + "#bit#" + model.User_Filter_IsActive);
                input_parameters.Add("@User_Filter_IsDelete", 1 + "#bit#" + model.User_Filter_IsDelete);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserId);
                input_parameters.Add("@User_Filter_PkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        private DataSet Get_Filter_Admin_User(Filter_Admin_User_MasterDTO model)
        {
            DataSet ds = null;
            try
            {

                string selectProcedure = "[Get_Filter_Admin_User_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@User_Filter_PkeyID", 1 + "#bigint#" + model.User_Filter_PkeyID);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> Get_Filter_Admin_UserDetails(Filter_Admin_User_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = Get_Filter_Admin_User(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Filter_Admin_User_MasterDTO> Filteruser =
                   (from item in myEnumerableFeaprd
                    select new Filter_Admin_User_MasterDTO
                    {
                        User_Filter_PkeyID = item.Field<Int64>("User_Filter_PkeyID"),
                        User_Filter_First_Name = item.Field<String>("User_Filter_First_Name"),
                        User_Filter_Last_Name = item.Field<String>("User_Filter_Last_Name"),
                        User_Filter_Company = item.Field<String>("User_Filter_Company"),
                        User_Filter_Login_email = item.Field<String>("User_Filter_Login_email"),
                        User_Filter_Group = item.Field<Int64?>("User_Filter_Group"),
                        User_Filter_Mobile = item.Field<String>("User_Filter_Mobile"),
                        User_Filter_USRIsActive = item.Field<Boolean?>("User_Filter_USRIsActive")

                    }).ToList();

                objDynamic.Add(Filteruser);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}