﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class PCR_MCS_Forms_Master_Data
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        public List<dynamic> PostMCSFormMaster(PCR_MCS_Forms_Master_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_PCR_MCS_Forms_Master]";
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@MCS_PkeyID", 1 + "#bigint#" + model.MCS_PkeyID);
                input_parameters.Add("@MCS_WO_ID", 1 + "#bigint#" + model.MCS_WO_ID);
                input_parameters.Add("@MCS_Property_Info", 1 + "#nvarchar#" + model.MCS_Property_Info);
                input_parameters.Add("@MCS_Completion_Info", 1 + "#nvarchar#" + model.MCS_Completion_Info);
                input_parameters.Add("@MCS_Utilities", 1 + "#nvarchar#" + model.MCS_Utilities);
                input_parameters.Add("@MCS_VCL", 1 + "#nvarchar#" + model.MCS_VCL);
                input_parameters.Add("@MCS_Check_Ins", 1 + "#nvarchar#" + model.MCS_Check_Ins);
                input_parameters.Add("@MCS_IsActive", 1 + "#bit#" + model.MCS_IsActive);
                input_parameters.Add("@MCS_IsDelete", 1 + "#bit#" + model.MCS_IsDelete);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@fwo_pkyeId", 1 + "#bigint#" + model.fwo_pkyeId);
                input_parameters.Add("@MCS_PkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);
                objData = obj.SqlCRUD(insertProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        private DataSet GetMCSFormMaster(PCR_MCS_Forms_Master_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_PCR_MCS_Forms_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@MCS_PkeyID", 1 + "#bigint#" + model.MCS_PkeyID);
                input_parameters.Add("@MCS_WO_ID", 1 + "#bigint#" + model.MCS_WO_ID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetMCSFormMasterDetails(PCR_MCS_Forms_Master_DTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetMCSFormMaster(model);


                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                }

                //var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                //List<PCR_MCS_Forms_Master_DTO> pcrdata =
                //   (from item in myEnumerableFeaprd
                //    select new PCR_MCS_Forms_Master_DTO
                //    {
                //        MCS_PkeyID = item.Field<Int64>("MCS_PkeyID"),
                //        MCS_WO_ID = item.Field<Int64>("MCS_WO_ID"),
                //        MCS_CompanyID = item.Field<Int64>("MCS_CompanyID"),
                //        MCS_Property_Info = item.Field<String>("MCS_Property_Info"),
                //        MCS_Completion_Info = item.Field<String>("MCS_Completion_Info"),
                //        MCS_Utilities = item.Field<String>("MCS_Utilities"),
                //        MCS_VCL = item.Field<String>("MCS_VCL"),
                //        MCS_Check_Ins = item.Field<String>("MCS_Check_Ins"),
                //        MCS_IsActive = item.Field<Boolean>("MCS_IsActive"),
                //    }).ToList();

                //objDynamic.Add(pcrdata);

                //if (ds.Tables.Count > 1)
                //{

                //    var pcrHistory = ds.Tables[1].AsEnumerable();
                //    List<PCR_MCS_Forms_Master_DTO> pcrdataHistory =
                //       (from item in pcrHistory
                //        select new PCR_MCS_Forms_Master_DTO
                //        {
                //            MCS_PkeyID = item.Field<Int64>("MCS_PkeyID"),
                //            MCS_WO_ID = item.Field<Int64>("MCS_WO_ID"),
                //            MCS_CompanyID = item.Field<Int64>("MCS_CompanyID"),
                //            MCS_Property_Info = item.Field<String>("MCS_Property_Info"),
                //            MCS_Completion_Info = item.Field<String>("MCS_Completion_Info"),
                //            MCS_Utilities = item.Field<String>("MCS_Utilities"),
                //            MCS_VCL = item.Field<String>("MCS_VCL"),
                //            MCS_Check_Ins = item.Field<String>("MCS_Check_Ins"),
                //            MCS_IsActive = item.Field<Boolean>("MCS_IsActive"),
                //        }).ToList();

                //    objDynamic.Add(pcrdataHistory);
                //}
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}