﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class Insert_User_Access_log_MasterData
    {

        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        private List<dynamic> AddUser_Access_logdata(Insert_User_Access_log_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objclntData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_Insert_User_Access_log_Master]";
            Insert_User_Access_log insert_User_Access_log = new Insert_User_Access_log();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@User_Acc_Log_PkeyId", 1 + "#bigint#" + model.User_Acc_Log_PkeyId);
                input_parameters.Add("@User_Acc_Log_Logged_In", 1 + "#datetime#" + model.User_Acc_Log_Logged_In);
                input_parameters.Add("@User_Acc_Log_Logged_Out", 1 + "#datetime#" + model.User_Acc_Log_Logged_Out);
                input_parameters.Add("@User_Acc_Log_UserID", 1 + "#bigint#" + model.User_Acc_Log_UserID);
                input_parameters.Add("@User_Acc_Log_Device_Name", 1 + "#nvarchar#" + model.User_Acc_Log_Device_Name);
                input_parameters.Add("@User_Acc_Log_GpsLatitude", 1 + "#nvarchar#" + model.User_Acc_Log_GpsLatitude);
                input_parameters.Add("@User_Acc_Log_GpsLongitude", 1 + "#nvarchar#" + model.User_Acc_Log_GpsLongitude);
                input_parameters.Add("@User_Acc_Log_Event", 1 + "#int#" + model.User_Acc_Log_Event);
                input_parameters.Add("@User_Acc_Log_GroupId", 1 + "#int#" + model.User_Acc_Log_GroupId);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@User_Acc_Log_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    insert_User_Access_log.User_Acc_Log_PkeyId = "0";
                    insert_User_Access_log.Status = "0";
                    insert_User_Access_log.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    insert_User_Access_log.User_Acc_Log_PkeyId = Convert.ToString(objData[0]);
                    insert_User_Access_log.Status = Convert.ToString(objData[1]);


                }
                objclntData.Add(insert_User_Access_log);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objclntData;



        }

        public List<dynamic> AddUser_Access_logdataDetails(Insert_User_Access_log_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                objData = AddUser_Access_logdata(model);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }
            return objData;
        }
    }
}