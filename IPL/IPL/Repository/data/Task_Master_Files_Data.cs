﻿
using Avigma.Repository.Lib;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using IPL.Models;
using Newtonsoft.Json;

namespace IPL.Repository.data
{
    public class Task_Master_Files_Data
    {

        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        public List<dynamic> AddTaskMasterDOcument(Task_Master_Files_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_Task_Master_Files]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@TMF_Task_Pkey", 1 + "#bigint#" + model.TMF_Task_Pkey);
                input_parameters.Add("@TMF_Task_pkeyID", 1 + "#bigint#" + model.TMF_Task_pkeyID);


                input_parameters.Add("@TMF_Task_objectName", 1 + "#nvarchar#" + model.TMF_Task_objectName);
                input_parameters.Add("@TMF_Task_FolderName", 1 + "#nvarchar#" + model.TMF_Task_FolderName);
                input_parameters.Add("@TMF_Task_FileName", 1 + "#nvarchar#" + model.TMF_Task_FileName);
                input_parameters.Add("@TMF_Task_localPath", 1 + "#nvarchar#" + model.TMF_Task_localPath);
                input_parameters.Add("@TMF_Task_BucketName", 1 + "#nvarchar#" + model.TMF_Task_BucketName);
                input_parameters.Add("@TMF_Task_ProjectID", 1 + "#bigint#" + model.TMF_Task_ProjectID);
                input_parameters.Add("@TMF_Task_FileSize", 1 + "#int#" + model.TMF_Task_FileSize);
                input_parameters.Add("@TMF_Task_FileType", 1 + "#int#" + model.TMF_Task_FileType);
                input_parameters.Add("@TMF_Task_UploadTimestamp", 1 + "#datetime#" + model.TMF_Task_UploadTimestamp);
                input_parameters.Add("@TMF_Task_UploadBy", 1 + "#bigint#" + model.TMF_Task_UploadBy);

                input_parameters.Add("@TMF_Task_CustPkey", 1 + "#bigint#" + model.TMF_Task_CustPkey);   

                input_parameters.Add("@TMF_Task_ClientPkey", 1 + "#bigint#" + model.TMF_Task_ClientPkey);


                input_parameters.Add("@TMF_Task_IsActive", 1 + "#bit#" + model.TMF_Task_IsActive);
                input_parameters.Add("@TMF_Task_IsDelete", 1 + "#bit#" + model.TMF_Task_IsDelete);
                input_parameters.Add("@TMF_Status", 1 + "#bit#" + model.TMF_Status);

                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserId);
                input_parameters.Add("@TMF_Task_Pkey_Out", 1 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);
                objData = obj.SqlCRUD(insertProcedure, input_parameters);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;



        }


        public List<dynamic> AddUpdateTaskMasterData(Client_Result_PhotoDTO client_Result_PhotoDTO)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                log.logDebugMessage("Method Called AddUpdateTaskMasterData");

                Task_Master_Files_DTO task_Master_Files_DTO = new Task_Master_Files_DTO();
                task_Master_Files_DTO.TMF_Task_BucketName = client_Result_PhotoDTO.Client_Result_Photo_BucketName;
                task_Master_Files_DTO.TMF_Task_ClientPkey = client_Result_PhotoDTO.Client_TMF_Task_ClientPkey;
                task_Master_Files_DTO.TMF_Task_CustPkey = client_Result_PhotoDTO.Client_TMF_Task_CustPkey;
                task_Master_Files_DTO.TMF_Task_FileName = client_Result_PhotoDTO.Client_Result_Photo_FileName;
                if (!string.IsNullOrEmpty(client_Result_PhotoDTO.Client_Result_Photo_FileSize))
                {
                    task_Master_Files_DTO.TMF_Task_FileSize = Convert.ToInt32(client_Result_PhotoDTO.Client_Result_Photo_FileSize);
                }
                 
                task_Master_Files_DTO.TMF_Task_FileType = Convert.ToInt32(client_Result_PhotoDTO.Client_Result_Photo_FileType);
                task_Master_Files_DTO.TMF_Task_FolderName = client_Result_PhotoDTO.Client_Result_Photo_FolderName;
                task_Master_Files_DTO.TMF_Task_IsActive = true;
                task_Master_Files_DTO.TMF_Task_IsDelete = false;
                task_Master_Files_DTO.TMF_Status = false;
                task_Master_Files_DTO.TMF_Task_localPath = client_Result_PhotoDTO.Client_Result_Photo_localPath;
                task_Master_Files_DTO.TMF_Task_objectName = client_Result_PhotoDTO.Client_Result_Photo_objectName;
                task_Master_Files_DTO.TMF_Task_pkeyID = client_Result_PhotoDTO.Client_Result_Photo_Ch_ID;  // Task master primary key
                task_Master_Files_DTO.TMF_Task_Pkey = client_Result_PhotoDTO.Client_Result_Photo_ID; // own primary key
                task_Master_Files_DTO.TMF_Task_ProjectID = Convert.ToInt64(client_Result_PhotoDTO.Client_Result_Photo_ProjectID);
                task_Master_Files_DTO.TMF_Task_UploadBy = client_Result_PhotoDTO.UserID;
                task_Master_Files_DTO.TMF_Task_UploadTimestamp = client_Result_PhotoDTO.Client_Result_Photo_UploadTimestamp;
                task_Master_Files_DTO.UserId = client_Result_PhotoDTO.UserID;
                task_Master_Files_DTO.Type = client_Result_PhotoDTO.Type;


                objData = AddTaskMasterDOcument(task_Master_Files_DTO);
            }
                catch (Exception ex)
                {
                    log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);



            }
            return objData;
        }

        private DataSet Get_Task_Master_Data(Task_Master_Files_DTO model)
        {
            DataSet ds = null;
            try
            {

                string selectProcedure = "[Get_Task_Master_Files]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@TMF_Task_Pkey", 1 + "#bigint#" + model.TMF_Task_Pkey);

                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> Get_Task_MasterDetails(Task_Master_Files_DTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = Get_Task_Master_Data(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Task_Master_Files_DTO> Task_Master_File=
                   (from item in myEnumerableFeaprd
                    select new Task_Master_Files_DTO
                    {
                        TMF_Task_Pkey = item.Field<Int64>("TMF_Task_Pkey"),
                        TMF_Task_pkeyID = item.Field<Int64>("TMF_Task_pkeyID"),
                        TMF_Task_objectName = item.Field<String>("TMF_Task_objectName"),
                        TMF_Task_FolderName = item.Field<String>("TMF_Task_FolderName"),
                        TMF_Task_FileName = item.Field<String>("TMF_Task_FileName"),
                        TMF_Task_localPath = item.Field<String>("TMF_Task_localPath"),
                        TMF_Task_BucketName = item.Field<String>("TMF_Task_BucketName"),
                        TMF_Task_ProjectID = item.Field<Int64?>("TMF_Task_ProjectID"),
                        TMF_Task_FileSize = item.Field<int?>("TMF_Task_FileSize"),
                        TMF_Task_FileType = item.Field<int?>("TMF_Task_FileType"),

                        TMF_Task_UploadTimestamp = item.Field<DateTime?>("TMF_Task_UploadTimestamp"),
                        TMF_Task_UploadBy = item.Field<Int64?>("TMF_Task_UploadBy"),
                        TMF_Task_CustPkey = item.Field<Int64?>("TMF_Task_CustPkey"),
                        TMF_Task_ClientPkey = item.Field<Int64?>("TMF_Task_ClientPkey"),
                        TMF_Task_IsActive = item.Field<Boolean?>("TMF_Task_IsActive"),
                        TMF_Task_IsDelete = item.Field<Boolean?>("TMF_Task_IsDelete"),
                        TMF_Status = item.Field<Boolean?>("TMF_Status"),  




                    }).ToList();

                objDynamic.Add(Task_Master_File);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        public List<dynamic> FilestatusUpdateTaskMasterData(String  FileArray)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                if (FileArray != null)
            {
                var Data = JsonConvert.DeserializeObject<List<Task_Master_Files_DTO>>(FileArray);
                    for (int i = 0; i < Data.Count; i++)
                    {
                        Data[i].TMF_Status = Data[i].chkflag;
                        Data[i].Type = 5;

                        objData = AddTaskMasterDOcument(Data[i]);

                    }

                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }


    }
}