﻿using Avigma.Repository.Lib;
using IPL.Models;
using IPLApp.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPLApp.Repository.data
{
    public class WorkTypeMasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public List<dynamic> AddWorkTypeData(WorkTypeMasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objworkData = new List<dynamic>();
            string insertProcedure = "[CreateUpdateWork_Type]";
            WorkTypeMaster workTypeMaster = new WorkTypeMaster();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@WT_pkeyID", 1 + "#bigint#" + model.WT_pkeyID);
                input_parameters.Add("@WT_WorkType", 1 + "#nvarchar#" + model.WT_WorkType);
                input_parameters.Add("@WT_CategoryID", 1 + "#bigint#" + model.WT_CategoryID);
                input_parameters.Add("@WT_YardMaintenance", 1 + "#bit#" + model.WT_YardMaintenance);
                input_parameters.Add("@WT_Active", 1 + "#int#" + model.WT_Active);
                input_parameters.Add("@WT_Always_recurring", 1 + "#bit#" + model.WT_Always_recurring);
                input_parameters.Add("@WT_Recurs_Every", 1 + "#varchar#" + model.WT_Recurs_Every);
                input_parameters.Add("@WT_Recurs_WeekID", 1 + "#int#" + model.WT_Recurs_WeekID);
                input_parameters.Add("@WT_Limit_to", 1 + "#varchar#" + model.WT_Limit_to);
                input_parameters.Add("@WT_Cutoff_Date", 1 + "#datetime#" + model.WT_Cutoff_Date);
                input_parameters.Add("@WT_WO_ItemID", 1 + "#bigint#" + model.WT_WO_ItemID);
                input_parameters.Add("@WT_Contractor_AssignmentID", 1 + "#bigint#" + model.WT_Contractor_AssignmentID);
                input_parameters.Add("@WT_Ready_for_FieldID", 1 + "#bigint#" + model.WT_Ready_for_FieldID);
                input_parameters.Add("@WT_IsInspection", 1 + "#bit#" + model.WT_IsInspection);
                input_parameters.Add("@WT_AutoInvoice", 1 + "#bit#" + model.WT_AutoInvoice);
                input_parameters.Add("@WT_IsActive", 1 + "#bit#" + model.WT_IsActive);
                input_parameters.Add("@WT_assign_upon_comple", 1 + "#bit#" + model.WT_assign_upon_comple);
                // input_parameters.Add("@WT_Ready_Quantity", 1 + "#bigint#" + model.WT_Ready_Quantity);
                input_parameters.Add("@Client_Work_Type_Name", 1 + "#varchar#" + model.Client_Work_Type_Name);
                input_parameters.Add("@WT_Template_Id", 1 + "#bigint#" + model.WT_Template_Id);
                input_parameters.Add("@Work_Type_Group", 1 + "#varchar#" + JsonConvert.SerializeObject(model.WT_CategoryMultiple));
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@WT_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    workTypeMaster.WT_pkeyID = "0";
                    workTypeMaster.Status = "0";
                    workTypeMaster.ErrorMessage = "Error while Saviving in the Database";

                }

                else
                {
                    workTypeMaster.WT_pkeyID = Convert.ToString(objData[0]);
                    workTypeMaster.Status = "1";

                }
                //else
                //{
                //    workTypeMaster.WT_pkeyID = Convert.ToString(objData[0]);
                //    workTypeMaster.Status = Convert.ToString(objData[1]);

                //    if (model.WT_CategoryMultiple != null && model.WT_CategoryMultiple.Count > 0)
                //    {
                //        foreach (var item in model.WT_CategoryMultiple)
                //        {
                //            WorkTypeGroupRelationDTO workTypeGroupRelationDTO = new WorkTypeGroupRelationDTO();
                //            workTypeGroupRelationDTO.WTGroupRel_pkeyId = 0;
                //            workTypeGroupRelationDTO.WTGroupRel_WorkTypeId = Convert.ToInt64(workTypeMaster.WT_pkeyID);
                //            workTypeGroupRelationDTO.WTGroupRel_WorkGroupId = item.Work_Type_Cat_pkeyID;
                //            workTypeGroupRelationDTO.UserID = model.UserID;
                //            workTypeGroupRelationDTO.Type = 1;

                //            AddWorkTypeGroupRelationMaster(workTypeGroupRelationDTO);
                //        }
                //    }

                //}
                objworkData.Add(workTypeMaster);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objworkData;



        }

        private DataSet GetworkMaster(WorkTypeMasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_WorkType]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@WT_pkeyID", 1 + "#bigint#" + model.WT_pkeyID);
                if (model.SearchMaster != null)
                {
                    input_parameters.Add("@WhereClause", 1 + "#nvarchar#" + model.SearchMaster.WhereClause);
                    input_parameters.Add("@UserID", 1 + "#bigint#" + model.SearchMaster.UserID);
                    input_parameters.Add("@MenuID", 1 + "#int#" + model.SearchMaster.MenuID);
                    input_parameters.Add("@FilterData", 1 + "#nvarchar#" + model.SearchMaster.FilterData);
                }
                else
                {
                    input_parameters.Add("@WhereClause", 1 + "#nvarchar#" + DBNull.Value);
                    input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                    input_parameters.Add("@MenuID", 1 + "#int#" + DBNull.Value);
                    input_parameters.Add("@FilterData", 1 + "#nvarchar#" + DBNull.Value);
                }

                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetWorkTypeDetails(WorkTypeMasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            string wherecondition = string.Empty;
            DataSet ds = null;
            List<WorkTypeMasterDTO> WorkTypeDetails = null;
            List<Filter_Admin_WorkType_MasterDTO> Filterworktype = null;

            try
            {
                switch (model.Type)
                {
                    case 1:
                        {
                            ds = GetworkMaster(model);
                            break;
                        }
                    case 2:
                        {
                            ds = GetworkMaster(model);
                            var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                            WorkTypeDetails =
                            (from item in myEnumerableFeaprd
                             select new WorkTypeMasterDTO
                             {
                                 WT_pkeyID = item.Field<Int64>("WT_pkeyID"),
                                 WT_WorkType = item.Field<String>("WT_WorkType"),
                                 WT_CategoryID = item.Field<Int64?>("WT_CategoryID"),
                                 WT_YardMaintenance = item.Field<Boolean?>("WT_YardMaintenance"),
                                 WT_Active = item.Field<int?>("WT_Active"),
                                 WT_Always_recurring = item.Field<Boolean?>("WT_Always_recurring"),
                                 WT_Recurs_Every = item.Field<String>("WT_Recurs_Every"),
                                 WT_Recurs_WeekID = item.Field<Int64?>("WT_Recurs_WeekID"),
                                 WT_Limit_to = item.Field<String>("WT_Limit_to"),
                                 WT_Cutoff_Date = item.Field<DateTime?>("WT_Cutoff_Date"),
                                 WT_WO_ItemID = item.Field<Int64?>("WT_WO_ItemID"),
                                 WT_Contractor_AssignmentID = item.Field<Int64?>("WT_Contractor_AssignmentID"),
                                 WT_Ready_for_FieldID = item.Field<Int64?>("WT_Ready_for_FieldID"),
                                 WT_IsInspection = item.Field<Boolean?>("WT_IsInspection"),
                                 WT_AutoInvoice = item.Field<Boolean?>("WT_AutoInvoice"),
                                 WT_IsActive = item.Field<Boolean?>("WT_IsActive"),
                                 WT_assign_upon_comple = item.Field<Boolean?>("WT_assign_upon_comple"),
                                 // WT_Ready_Quantity = item.Field<Int64?>("WT_Ready_Quantity"),
                                 Client_Work_Type_Name = item.Field<String>("Client_Work_Type_Name"),
                                // WT_Template_Id = item.Field<Int64?>("WT_Template_Id"),
                                 WT_CreatedBy = item.Field<String>("WT_CreatedBy"),
                                 WT_Type_ModifiedBy = item.Field<String>("WT_Type_ModifiedBy"),
                                 WT_CategoryJson=item.Field<String>("WT_CategoryJson"),
                                 CategoryCount = item.Field<int>("CategoryCount"),
                             }).ToList();

                            //foreach (var item in WorkTypeDetails)
                            //{
                            //    item.Type = 1;
                            //    item.WT_CategoryMultiple = GetWorkTypeGroupRelationDetails(item);
                            //}


                            break;
                        }
                    case 3:
                        {
                            ds = GetworkMaster(model);
                            var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                            WorkTypeDetails =
                            (from item in myEnumerableFeaprd
                             select new WorkTypeMasterDTO
                             {
                                 WT_pkeyID = item.Field<Int64>("WT_pkeyID"),
                                 WT_WorkType = item.Field<String>("WT_WorkType"),
                                 //Work_Category_Name = item.Field<string>("Work_Category_Name"),
                                 //WT_CategoryID = item.Field<Int64>("WT_CategoryID"),


                                 WT_Always_recurring = item.Field<Boolean?>("WT_Always_recurring"),


                                 WT_IsInspection = item.Field<Boolean?>("WT_IsInspection"),

                                 WT_IsActive = item.Field<Boolean?>("WT_IsActive"),
                                 WT_assign_upon_comple = item.Field<Boolean?>("WT_assign_upon_comple"),
                                 // WT_Ready_Quantity = item.Field<Int64?>("WT_Ready_Quantity"),
                                 Client_Work_Type_Name = item.Field<String>("Client_Work_Type_Name"),
                                // WT_Template_Id = item.Field<Int64?>("WT_Template_Id"),
                                 WT_CreatedBy = item.Field<String>("WT_CreatedBy"),
                                 WT_Type_ModifiedBy = item.Field<String>("WT_Type_ModifiedBy"),
                                 WT_CategoryJson = item.Field<String>("WT_CategoryJson"),
                                 CategoryCount = item.Field<int>("CategoryCount"),
                             }).ToList();
                            if (model.Type == 3)
                            {
                                if (ds.Tables.Count > 1)
                                {

                                    var myEnumerableFeaprd1 = ds.Tables[1].AsEnumerable();
                                    Filterworktype =
                                      (from item in myEnumerableFeaprd1
                                       select new Filter_Admin_WorkType_MasterDTO
                                       {
                                           WT_Filter_PkeyId = item.Field<Int64>("WT_Filter_PkeyId"),
                                           WT_Filter_Name = item.Field<String>("WT_Filter_Name"),
                                           WT_Filter_Group = item.Field<Int64?>("WT_Filter_Group"),
                                           WT_Filter_WTIsActive = item.Field<Boolean?>("WT_Filter_WTIsActive")

                                       }).ToList();



                                }
                            }
                            break;
                        }
                    case 4:
                        {
                            var Data = JsonConvert.DeserializeObject<WorkTypeMasterDTO>(model.FilterData);
                            if (!string.IsNullOrEmpty(Data.WT_WorkType))
                            {
                                wherecondition = " And WT_WorkType LIKE '%" + Data.WT_WorkType + "%'";

                            }
                            if (Data.WT_CategoryID != null && Data.WT_CategoryID != 0)
                            {
                                wherecondition = wherecondition != null ? wherecondition + " And WTGR.WTGroupRel_WorkGroupId LIKE '%" + Data.WT_CategoryID + "%'" : " And WTGR.WTGroupRel_WorkGroupId LIKE '%" + Data.WT_CategoryID + "%'";
                            }


                            //if (Data.WT_Always_recurring != null)
                            //{
                            //    if (Data.WT_Always_recurring == true)
                            //    {
                            //        wherecondition = wherecondition + "  And WT_Always_recurring =  ''";
                            //    }
                            //    if (Data.WT_Always_recurring == false)
                            //    {
                            //        wherecondition = wherecondition + "  And WT_Always_recurring =  ''";
                            //    }
                            //}
                            //if (Data.WT_IsInspection != null)
                            //{
                            //    if (Data.WT_IsInspection == true)
                            //    {
                            //        wherecondition = wherecondition + "  And WT_IsInspection =  ''";
                            //    }
                            //    if (Data.WT_IsInspection == false)
                            //    {
                            //        wherecondition = wherecondition + "  And WT_IsInspection =  ''";
                            //    }
                            //}

                            if (Data.WT_IsActive == true)
                            {
                                wherecondition = wherecondition + "  And WT_IsActive  =  '1'";
                            }
                            if (Data.WT_IsActive == false)
                            {
                                wherecondition = wherecondition + "  And WT_IsActive  =  '0'";
                            }
                            if (!string.IsNullOrEmpty(Data.Client_Work_Type_Name))
                            {
                                wherecondition = " And Client_Work_Type_Name LIKE '%" + Data.Client_Work_Type_Name + "%'";
                            }


                            SearchMasterDTO searchMasterDTO = new SearchMasterDTO();

                            searchMasterDTO.MenuID = model.SearchMaster.MenuID;
                            searchMasterDTO.UserID = model.SearchMaster.UserID;
                            model.SearchMaster.WhereClause = wherecondition;

                            ds = GetworkMaster(model);
                            var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                            WorkTypeDetails =
                            (from item in myEnumerableFeaprd
                             select new WorkTypeMasterDTO
                             {
                                 WT_pkeyID = item.Field<Int64>("WT_pkeyID"),
                                 WT_WorkType = item.Field<String>("WT_WorkType"),
                                 //Work_Category_Name = item.Field<string>("Work_Category_Name"),
                                 //WT_CategoryID = item.Field<Int64>("WT_CategoryID"),


                                 WT_Always_recurring = item.Field<Boolean?>("WT_Always_recurring"),


                                 WT_IsInspection = item.Field<Boolean?>("WT_IsInspection"),

                                 WT_IsActive = item.Field<Boolean?>("WT_IsActive"),
                                 WT_assign_upon_comple = item.Field<Boolean?>("WT_assign_upon_comple"),
                                 // WT_Ready_Quantity = item.Field<Int64?>("WT_Ready_Quantity"),
                                 Client_Work_Type_Name = item.Field<String>("Client_Work_Type_Name"),
                              //   WT_Template_Id = item.Field<Int64?>("WT_Template_Id"),
                                 WT_CreatedBy = item.Field<String>("WT_CreatedBy"),
                                 WT_Type_ModifiedBy = item.Field<String>("WT_Type_ModifiedBy"),
                                 WT_CategoryJson = item.Field<String>("WT_CategoryJson"),
                                 CategoryCount = item.Field<int>("CategoryCount"),
                             }).ToList();
                            break;
                        }
                }





                objDynamic.Add(WorkTypeDetails);
                objDynamic.Add(Filterworktype);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        private void AddWorkTypeGroupRelationMaster(WorkTypeGroupRelationDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objAddData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_WorkTypeGroupRelationMaster]";
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@WTGroupRel_pkeyId", 1 + "#bigint#" + model.WTGroupRel_pkeyId);
                input_parameters.Add("@WTGroupRel_WorkTypeId", 1 + "#bigint#" + model.WTGroupRel_WorkTypeId);
                input_parameters.Add("@WTGroupRel_WorkGroupId", 1 + "#bigint#" + model.WTGroupRel_WorkGroupId);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@WTGroupRel_pkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
        }


        private DataSet GetWorkTypeGroupRelation(WorkTypeMasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_WorkTypeGroupRelationMaster]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();
                input_parameters.Add("@WT_pkeyID", 1 + "#bigint#" + model.WT_pkeyID);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }
        private List<WT_CategoryMultiple> GetWorkTypeGroupRelationDetails(WorkTypeMasterDTO model)
        {

            List<WT_CategoryMultiple> AddWorkTypeGroupRelationMaster = new List<WT_CategoryMultiple>();
            try
            {
                DataSet ds = GetWorkTypeGroupRelation(model);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                    AddWorkTypeGroupRelationMaster =
                    (from item in myEnumerableFeaprd
                     select new WT_CategoryMultiple
                     {
                         Work_Type_Cat_pkeyID = item.Field<Int64>("Work_Type_Cat_pkeyID"),
                         Work_Type_Name = item.Field<string>("Work_Type_Name"),
                     }).ToList();
                }
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return AddWorkTypeGroupRelationMaster;
        }
    }
}