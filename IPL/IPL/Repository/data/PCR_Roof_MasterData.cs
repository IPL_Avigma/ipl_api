﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class PCR_Roof_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public List<dynamic> AddPCR_Roof_Data(PCR_Roof_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objclntData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_PCR_Roof_Master]";
            PCR_Roof_Master pCR_Roof_Master = new PCR_Roof_Master();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@PCR_Roof_pkeyId", 1 + "#bigint#" + model.PCR_Roof_pkeyId);
                input_parameters.Add("@PCR_Roof_MasterId", 1 + "#bigint#" + model.PCR_Roof_MasterId);
                input_parameters.Add("@PCR_Roof_WO_Id", 1 + "#bigint#" + model.PCR_Roof_WO_Id);
                input_parameters.Add("@PCR_Roof_ValType", 1 + "#int#" + model.PCR_Roof_ValType);

                input_parameters.Add("@PCR_Roof_Roof_Shape_Pitched_Roof", 1 + "#varchar#" + model.PCR_Roof_Roof_Shape_Pitched_Roof);
                input_parameters.Add("@PCR_Roof_Leak", 1 + "#varchar#" + model.PCR_Roof_Leak);
                input_parameters.Add("@PCR_Roof_Leak_Case", 1 + "#varchar#" + model.PCR_Roof_Leak_Case);
                input_parameters.Add("@PCR_Roof_Leak_Other", 1 + "#varchar#" + model.PCR_Roof_Leak_Other);
                input_parameters.Add("@PCR_Roof_Location_Dimensions", 1 + "#varchar#" + model.PCR_Roof_Location_Dimensions);
                input_parameters.Add("@PCR_Roof_Roof_Damage", 1 + "#varchar#" + model.PCR_Roof_Roof_Damage);
                input_parameters.Add("@PCR_Roof_Water_Strains", 1 + "#varchar#" + model.PCR_Roof_Water_Strains);

                input_parameters.Add("@PCR_Roof_Water_Strains_Case", 1 + "#varchar#" + model.PCR_Roof_Water_Strains_Case);
                input_parameters.Add("@PCR_Roof_Water_Strains_Dimension", 1 + "#varchar#" + model.PCR_Roof_Water_Strains_Dimension);
                input_parameters.Add("@PCR_Roof_Did_You_Perform_Roof_Repair", 1 + "#varchar#" + model.PCR_Roof_Did_You_Perform_Roof_Repair);
                input_parameters.Add("@PCR_Roof_Bid_To_Repair", 1 + "#varchar#" + model.PCR_Roof_Bid_To_Repair);
                input_parameters.Add("@PCR_Roof_Did_You_Perform_Emergency_Traping", 1 + "#varchar#" + model.PCR_Roof_Did_You_Perform_Emergency_Traping);
                input_parameters.Add("@PCR_Roof_Explain_Bid_Trap", 1 + "#varchar#" + model.PCR_Roof_Explain_Bid_Trap);

                input_parameters.Add("@PCR_Roof_Bid_To_Trap_Dimension_size_x", 1 + "#int#" + model.PCR_Roof_Bid_To_Trap_Dimension_size_x);
                input_parameters.Add("@PCR_Roof_Bid_To_Trap_Dimension_size_y", 1 + "#int#" + model.PCR_Roof_Bid_To_Trap_Dimension_size_y);
                input_parameters.Add("@PCR_Roof_Bid_To_Trap_Location", 1 + "#int#" + model.PCR_Roof_Bid_To_Trap_Location);
                input_parameters.Add("@PCR_Roof_Bid_To_Trap_Description", 1 + "#varchar#" + model.PCR_Roof_Bid_To_Trap_Description);
                input_parameters.Add("@PCR_Roof_Bid_To_Trap_Bid_Amount", 1 + "#decimal#" + model.PCR_Roof_Bid_To_Trap_Bid_Amount);

                input_parameters.Add("@PCR_Roof_Bid_To_Tar_Patch_Dimension_size_x", 1 + "#int#" + model.PCR_Roof_Bid_To_Tar_Patch_Dimension_size_x);
                input_parameters.Add("@PCR_Roof_Bid_To_Tar_Patch_Dimension_size_y", 1 + "#int#" + model.PCR_Roof_Bid_To_Tar_Patch_Dimension_size_y);
                input_parameters.Add("@PCR_Roof_Bid_To_Tar_Patch_Location", 1 + "#int#" + model.PCR_Roof_Bid_To_Tar_Patch_Location);
                input_parameters.Add("@PCR_Roof_Bid_To_Tar_Patch_dias", 1 + "#varchar#" + model.PCR_Roof_Bid_To_Tar_Patch_dias);
                input_parameters.Add("@PCR_Roof_Bid_To_Tar_Patch_Bid_Amount", 1 + "#decimal#" + model.PCR_Roof_Bid_To_Tar_Patch_Bid_Amount);

                input_parameters.Add("@PCR_Roof_Bid_To_Replace", 1 + "#varchar#" + model.PCR_Roof_Bid_To_Replace);
                input_parameters.Add("@PCR_Roof_Reason_Cant_Repair_Due_To", 1 + "#varchar#" + model.PCR_Roof_Reason_Cant_Repair_Due_To);
                input_parameters.Add("@PCR_Roof_Reason_Cant_Repair_Due_To_TEXT", 1 + "#varchar#" + model.PCR_Roof_Reason_Cant_Repair_Due_To_TEXT);
                input_parameters.Add("@PCR_Roof_Reason_Preventive_Due_To", 1 + "#varchar#" + model.PCR_Roof_Reason_Preventive_Due_To);
                input_parameters.Add("@PCR_Roof_Reason_Leaking", 1 + "#varchar#" + model.PCR_Roof_Reason_Leaking);
                input_parameters.Add("@PCR_Roof_Reason_Other", 1 + "#varchar#" + model.PCR_Roof_Reason_Other);
                input_parameters.Add("@PCR_Roof_Bid_To_Description", 1 + "#varchar#" + model.PCR_Roof_Bid_To_Description);

                input_parameters.Add("@PCR_Roof_Bid_To_Location_Entire_Roof", 1 + "#bit#" + model.PCR_Roof_Bid_To_Location_Entire_Roof);
                input_parameters.Add("@PCR_Roof_Bid_To_Location_Front", 1 + "#bit#" + model.PCR_Roof_Bid_To_Location_Front);
                input_parameters.Add("@PCR_Roof_Bid_To_Location_Back", 1 + "#bit#" + model.PCR_Roof_Bid_To_Location_Back);
                input_parameters.Add("@PCR_Roof_Bid_To_Location_Left_Side", 1 + "#bit#" + model.PCR_Roof_Bid_To_Location_Left_Side);

                input_parameters.Add("@PCR_Roof_Bid_To_Location_Right_Side", 1 + "#bit#" + model.PCR_Roof_Bid_To_Location_Right_Side);
                input_parameters.Add("@PCR_Roof_Building_House", 1 + "#bit#" + model.PCR_Roof_Building_House);
                input_parameters.Add("@PCR_Roof_Building_Garage", 1 + "#bit#" + model.PCR_Roof_Building_Garage);
                input_parameters.Add("@PCR_Roof_Building_Out_Building", 1 + "#bit#" + model.PCR_Roof_Building_Out_Building);

                input_parameters.Add("@PCR_Roof_Building_Pool_House", 1 + "#bit#" + model.PCR_Roof_Building_Pool_House);
                input_parameters.Add("@PCR_Roof_Building_Shed", 1 + "#bit#" + model.PCR_Roof_Building_Shed);
                input_parameters.Add("@PCR_Roof_Building_Bam", 1 + "#bit#" + model.PCR_Roof_Building_Bam);


                input_parameters.Add("@PCR_Roof_Item_Used_Roof_Type", 1 + "#varchar#" + model.PCR_Roof_Item_Used_Roof_Type);
                input_parameters.Add("@PCR_Roof_Item_Used_DRD", 1 + "#int#" + model.PCR_Roof_Item_Used_DRD);
                input_parameters.Add("@PCR_Roof_Item_Used_Size", 1 + "#int#" + model.PCR_Roof_Item_Used_Size);
                input_parameters.Add("@PCR_Roof_Item_Used_Amount", 1 + "#decimal#" + model.PCR_Roof_Item_Used_Amount);


                input_parameters.Add("@PCR_Roof_Item_Used_Felt_Type", 1 + "#varchar#" + model.PCR_Roof_Item_Used_Felt_Type);
                input_parameters.Add("@PCR_Roof_Item_Used_Felt_Type_DRD", 1 + "#int#" + model.PCR_Roof_Item_Used_Felt_Type_DRD);
                input_parameters.Add("@PCR_Roof_Item_Used_Felt_Type_Size", 1 + "#int#" + model.PCR_Roof_Item_Used_Felt_Type_Size);
                input_parameters.Add("@PCR_Roof_Item_Used_Felt_Type_Amount", 1 + "#decimal#" + model.PCR_Roof_Item_Used_Felt_Type_Amount);
                input_parameters.Add("@PCR_Roof_Leak_Location_Dimension", 1 + "#varchar#" + model.PCR_Roof_Leak_Location_Dimension);

                input_parameters.Add("@PCR_Roof_Item_Used_Sheathing_DRD", 1 + "#int#" + model.PCR_Roof_Item_Used_Sheathing_DRD);
                input_parameters.Add("@PCR_Roof_Item_Used_Sheathing_Size", 1 + "#int#" + model.PCR_Roof_Item_Used_Sheathing_Size);
                input_parameters.Add("@PCR_Roof_Item_Used_Sheathing_Amount", 1 + "#decimal#" + model.PCR_Roof_Item_Used_Sheathing_Amount);


                input_parameters.Add("@PCR_Roof_Item_Used_Deck_Thikness", 1 + "#varchar#" + model.PCR_Roof_Item_Used_Deck_Thikness);
                input_parameters.Add("@PCR_Roof_Item_Used_Deck_Thikness_DRD", 1 + "#int#" + model.PCR_Roof_Item_Used_Deck_Thikness_DRD);

                input_parameters.Add("@PCR_Roof_Item_Used_Drip_Edge", 1 + "#varchar#" + model.PCR_Roof_Item_Used_Drip_Edge);
                input_parameters.Add("@PCR_Roof_Item_Used_Drip_Edge_Size", 1 + "#int#" + model.PCR_Roof_Item_Used_Drip_Edge_Size);
                input_parameters.Add("@PCR_Roof_Item_Used_Drip_Edge_Amount", 1 + "#int#" + model.PCR_Roof_Item_Used_Drip_Edge_Amount);

                input_parameters.Add("@PCR_Roof_Item_Used_Ice_Water_Barrier", 1 + "#varchar#" + model.PCR_Roof_Item_Used_Ice_Water_Barrier);
                input_parameters.Add("@PCR_Roof_Item_Used_Ice_Water_Barrier_Size", 1 + "#int#" + model.PCR_Roof_Item_Used_Ice_Water_Barrier_Size);
                input_parameters.Add("@PCR_Roof_Item_Used_Ice_Water_Barrier_Amount", 1 + "#int#" + model.PCR_Roof_Item_Used_Ice_Water_Barrier_Amount);


                input_parameters.Add("@PCR_Roof_Item_Used_No_Of_Vents", 1 + "#varchar#" + model.PCR_Roof_Item_Used_No_Of_Vents);
                input_parameters.Add("@PCR_Roof_Item_Used_No_Of_Vents_Text", 1 + "#varchar#" + model.PCR_Roof_Item_Used_No_Of_Vents_Text);
                input_parameters.Add("@PCR_Roof_Item_Used_No_Of_Vents_Amount", 1 + "#decimal#" + model.PCR_Roof_Item_Used_No_Of_Vents_Amount);

                input_parameters.Add("@PCR_Roof_Item_Used_Roof_Debris", 1 + "#varchar#" + model.PCR_Roof_Item_Used_Roof_Debris);
                input_parameters.Add("@PCR_Roof_Item_Used_Roof_Debris_Size", 1 + "#int#" + model.PCR_Roof_Item_Used_Roof_Debris_Size);
                input_parameters.Add("@PCR_Roof_Item_Used_Roof_Debris_Amount", 1 + "#decimal#" + model.PCR_Roof_Item_Used_Roof_Debris_Amount);

                input_parameters.Add("@PCR_Roof_Item_Used_Dempster_Rental", 1 + "#varchar#" + model.PCR_Roof_Item_Used_Dempster_Rental);
                input_parameters.Add("@PCR_Roof_Item_Used_Dempster_Rental_Size", 1 + "#int#" + model.PCR_Roof_Item_Used_Dempster_Rental_Size);
                input_parameters.Add("@PCR_Roof_Item_Used_Dempster_Rental_Amount", 1 + "#decimal#" + model.PCR_Roof_Item_Used_Dempster_Rental_Amount);

                input_parameters.Add("@PCR_Bid_Amount", 1 + "#decimal#" + model.PCR_Bid_Amount);
                input_parameters.Add("@PCR_Roof_IsActive", 1 + "#bit#" + model.PCR_Roof_IsActive);
                input_parameters.Add("@PCR_Roof_IsDelete", 1 + "#bit#" + model.PCR_Roof_IsDelete);



                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@fwo_pkyeId", 1 + "#bigint#" + model.fwo_pkyeId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@PCR_Roof_pkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    pCR_Roof_Master.PCR_Roof_pkeyId = "0";
                    pCR_Roof_Master.Status = "0";
                    pCR_Roof_Master.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    pCR_Roof_Master.PCR_Roof_pkeyId = Convert.ToString(objData[0]);
                    pCR_Roof_Master.Status = Convert.ToString(objData[1]);


                }
                objclntData.Add(pCR_Roof_Master);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objclntData;



        }

        private DataSet GetPCRRoofMaster(PCR_Roof_MasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_PCR_Roof_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@PCR_Roof_pkeyId", 1 + "#bigint#" + model.PCR_Roof_pkeyId);
                input_parameters.Add("@PCR_Roof_WO_Id", 1 + "#bigint#" + model.PCR_Roof_WO_Id);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetPCRRoofDetails(PCR_Roof_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetPCRRoofMaster(model);

                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                }

                //var PCRRoof = DatatableToModel(ds.Tables[0]);
                //objDynamic.Add(PCRRoof);

                //if (ds.Tables.Count > 1)
                //{
                //    var History = DatatableToModel(ds.Tables[1]);
                //    objDynamic.Add(History);
                //}
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
        private List<PCR_Roof_MasterDTO> DatatableToModel(DataTable dataTable)
        {
            var myEnumerableFeaprd = dataTable.AsEnumerable();
            List<PCR_Roof_MasterDTO> PCRRoof =
               (from item in myEnumerableFeaprd
                select new PCR_Roof_MasterDTO
                {
                    PCR_Roof_pkeyId = item.Field<Int64>("PCR_Roof_pkeyId"),
                    PCR_Roof_MasterId = item.Field<Int64>("PCR_Roof_MasterId"),
                    PCR_Roof_WO_Id = item.Field<Int64>("PCR_Roof_WO_Id"),
                    PCR_Roof_ValType = item.Field<int?>("PCR_Roof_ValType"),
                    PCR_Roof_Roof_Shape_Pitched_Roof = item.Field<String>("PCR_Roof_Roof_Shape_Pitched_Roof"),
                    PCR_Roof_Leak = item.Field<String>("PCR_Roof_Leak"),
                    PCR_Roof_Leak_Case = item.Field<String>("PCR_Roof_Leak_Case"),
                    PCR_Roof_Leak_Other = item.Field<String>("PCR_Roof_Leak_Other"),
                    PCR_Roof_Location_Dimensions = item.Field<String>("PCR_Roof_Location_Dimensions"),
                    PCR_Roof_Leak_Location_Dimension = item.Field<String>("PCR_Roof_Leak_Location_Dimension"),
                    PCR_Roof_Roof_Damage = item.Field<String>("PCR_Roof_Roof_Damage"),   //new
                    PCR_Roof_Water_Strains = item.Field<String>("PCR_Roof_Water_Strains"),
                    PCR_Roof_Water_Strains_Case = item.Field<String>("PCR_Roof_Water_Strains_Case"),
                    PCR_Roof_Water_Strains_Dimension = item.Field<String>("PCR_Roof_Water_Strains_Dimension"),
                    PCR_Roof_Did_You_Perform_Roof_Repair = item.Field<String>("PCR_Roof_Did_You_Perform_Roof_Repair"),
                    PCR_Roof_Bid_To_Repair = item.Field<String>("PCR_Roof_Bid_To_Repair"),
                    PCR_Roof_Did_You_Perform_Emergency_Traping = item.Field<String>("PCR_Roof_Did_You_Perform_Emergency_Traping"),
                    PCR_Roof_Explain_Bid_Trap = item.Field<String>("PCR_Roof_Explain_Bid_Trap"),
                    PCR_Roof_Bid_To_Trap_Dimension_size_x = item.Field<int?>("PCR_Roof_Bid_To_Trap_Dimension_size_x"),
                    PCR_Roof_Bid_To_Trap_Dimension_size_y = item.Field<int?>("PCR_Roof_Bid_To_Trap_Dimension_size_y"),
                    PCR_Roof_Bid_To_Trap_Location = item.Field<int?>("PCR_Roof_Bid_To_Trap_Location"),
                    PCR_Roof_Bid_To_Trap_Description = item.Field<String>("PCR_Roof_Bid_To_Trap_Description"),
                    PCR_Roof_Bid_To_Trap_Bid_Amount = item.Field<Decimal>("PCR_Roof_Bid_To_Trap_Bid_Amount"),

                    PCR_Roof_Bid_To_Tar_Patch_Dimension_size_x = item.Field<int?>("PCR_Roof_Bid_To_Tar_Patch_Dimension_size_x"),
                    PCR_Roof_Bid_To_Tar_Patch_Dimension_size_y = item.Field<int?>("PCR_Roof_Bid_To_Tar_Patch_Dimension_size_y"),
                    PCR_Roof_Bid_To_Tar_Patch_Location = item.Field<int?>("PCR_Roof_Bid_To_Tar_Patch_Location"),
                    PCR_Roof_Bid_To_Tar_Patch_dias = item.Field<String>("PCR_Roof_Bid_To_Tar_Patch_dias"),
                    PCR_Roof_Bid_To_Tar_Patch_Bid_Amount = item.Field<Decimal?>("PCR_Roof_Bid_To_Tar_Patch_Bid_Amount"),

                    PCR_Roof_Bid_To_Replace = item.Field<String>("PCR_Roof_Bid_To_Replace"),
                    PCR_Roof_Reason_Cant_Repair_Due_To = item.Field<String>("PCR_Roof_Reason_Cant_Repair_Due_To"),
                    PCR_Roof_Reason_Cant_Repair_Due_To_TEXT = item.Field<String>("PCR_Roof_Reason_Cant_Repair_Due_To_TEXT"), //new
                    PCR_Roof_Reason_Preventive_Due_To = item.Field<String>("PCR_Roof_Reason_Preventive_Due_To"),

                    PCR_Roof_Reason_Leaking = item.Field<String>("PCR_Roof_Reason_Leaking"),
                    PCR_Roof_Reason_Other = item.Field<String>("PCR_Roof_Reason_Other"),
                    PCR_Roof_Bid_To_Description = item.Field<String>("PCR_Roof_Bid_To_Description"),

                    PCR_Roof_Bid_To_Location_Entire_Roof = item.Field<Boolean?>("PCR_Roof_Bid_To_Location_Entire_Roof"),
                    PCR_Roof_Bid_To_Location_Front = item.Field<Boolean?>("PCR_Roof_Bid_To_Location_Front"),
                    PCR_Roof_Bid_To_Location_Back = item.Field<Boolean?>("PCR_Roof_Bid_To_Location_Back"),
                    PCR_Roof_Bid_To_Location_Left_Side = item.Field<Boolean?>("PCR_Roof_Bid_To_Location_Left_Side"),
                    PCR_Roof_Bid_To_Location_Right_Side = item.Field<Boolean?>("PCR_Roof_Bid_To_Location_Right_Side"),
                    PCR_Roof_Building_House = item.Field<Boolean?>("PCR_Roof_Building_House"),
                    PCR_Roof_Building_Garage = item.Field<Boolean?>("PCR_Roof_Building_Garage"),
                    PCR_Roof_Building_Out_Building = item.Field<Boolean?>("PCR_Roof_Building_Out_Building"),
                    PCR_Roof_Building_Pool_House = item.Field<Boolean?>("PCR_Roof_Building_Pool_House"),
                    PCR_Roof_Building_Shed = item.Field<Boolean?>("PCR_Roof_Building_Shed"),
                    PCR_Roof_Building_Bam = item.Field<Boolean?>("PCR_Roof_Building_Bam"),


                    PCR_Roof_Item_Used_Roof_Type = item.Field<String>("PCR_Roof_Item_Used_Roof_Type"),
                    PCR_Roof_Item_Used_DRD = item.Field<int?>("PCR_Roof_Item_Used_DRD"),
                    PCR_Roof_Item_Used_Size = item.Field<int?>("PCR_Roof_Item_Used_Size"),
                    PCR_Roof_Item_Used_Amount = item.Field<Decimal?>("PCR_Roof_Item_Used_Amount"),


                    PCR_Roof_Item_Used_Felt_Type = item.Field<String>("PCR_Roof_Item_Used_Felt_Type"),
                    PCR_Roof_Item_Used_Felt_Type_DRD = item.Field<int?>("PCR_Roof_Item_Used_Felt_Type_DRD"),
                    PCR_Roof_Item_Used_Felt_Type_Size = item.Field<int?>("PCR_Roof_Item_Used_Felt_Type_Size"),
                    PCR_Roof_Item_Used_Felt_Type_Amount = item.Field<Decimal?>("PCR_Roof_Item_Used_Felt_Type_Amount"),
                    PCR_Roof_Item_Used_Sheathing_DRD = item.Field<int?>("PCR_Roof_Item_Used_Sheathing_DRD"),
                    PCR_Roof_Item_Used_Sheathing_Size = item.Field<int?>("PCR_Roof_Item_Used_Sheathing_Size"),
                    PCR_Roof_Item_Used_Sheathing_Amount = item.Field<Decimal?>("PCR_Roof_Item_Used_Sheathing_Amount"),




                    PCR_Roof_Item_Used_Deck_Thikness = item.Field<String>("PCR_Roof_Item_Used_Deck_Thikness"),
                    PCR_Roof_Item_Used_Deck_Thikness_DRD = item.Field<int?>("PCR_Roof_Item_Used_Deck_Thikness_DRD"),

                    PCR_Roof_Item_Used_Drip_Edge = item.Field<String>("PCR_Roof_Item_Used_Drip_Edge"),
                    PCR_Roof_Item_Used_Drip_Edge_Size = item.Field<int?>("PCR_Roof_Item_Used_Drip_Edge_Size"),
                    PCR_Roof_Item_Used_Drip_Edge_Amount = item.Field<Decimal?>("PCR_Roof_Item_Used_Drip_Edge_Amount"),

                    PCR_Roof_Item_Used_Ice_Water_Barrier = item.Field<String>("PCR_Roof_Item_Used_Ice_Water_Barrier"),
                    PCR_Roof_Item_Used_Ice_Water_Barrier_Size = item.Field<int?>("PCR_Roof_Item_Used_Ice_Water_Barrier_Size"),
                    PCR_Roof_Item_Used_Ice_Water_Barrier_Amount = item.Field<Decimal?>("PCR_Roof_Item_Used_Ice_Water_Barrier_Amount"),



                    PCR_Roof_Item_Used_No_Of_Vents = item.Field<String>("PCR_Roof_Item_Used_No_Of_Vents"),
                    PCR_Roof_Item_Used_No_Of_Vents_Text = item.Field<String>("PCR_Roof_Item_Used_No_Of_Vents_Text"),
                    PCR_Roof_Item_Used_No_Of_Vents_Amount = item.Field<Decimal?>("PCR_Roof_Item_Used_No_Of_Vents_Amount"),

                    PCR_Roof_Item_Used_Roof_Debris = item.Field<String>("PCR_Roof_Item_Used_Roof_Debris"),
                    PCR_Roof_Item_Used_Roof_Debris_Size = item.Field<int?>("PCR_Roof_Item_Used_Roof_Debris_Size"),
                    PCR_Roof_Item_Used_Roof_Debris_Amount = item.Field<Decimal?>("PCR_Roof_Item_Used_Roof_Debris_Amount"),

                    PCR_Roof_Item_Used_Dempster_Rental = item.Field<String>("PCR_Roof_Item_Used_Dempster_Rental"),
                    PCR_Roof_Item_Used_Dempster_Rental_Size = item.Field<int?>("PCR_Roof_Item_Used_Dempster_Rental_Size"),
                    PCR_Roof_Item_Used_Dempster_Rental_Amount = item.Field<Decimal?>("PCR_Roof_Item_Used_Dempster_Rental_Amount"),
                    PCR_Bid_Amount = item.Field<Decimal?>("PCR_Bid_Amount"),
                    PCR_Roof_IsActive = item.Field<Boolean?>("PCR_Roof_IsActive"),



                }).ToList();
            return PCRRoof;
        }
    }
}