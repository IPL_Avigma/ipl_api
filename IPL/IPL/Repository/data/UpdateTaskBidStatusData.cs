﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
namespace IPL.Repository.data
{
    public class UpdateTaskBidStatusData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        WorkOrderHistoryData workOrderHistoryData = new WorkOrderHistoryData();
        Instruction_DocumentData instruction_DocumentData = new Instruction_DocumentData();


        public List<dynamic> UpdateTaskBid_Add_status(TaskBidMasterDTO model)
        {
             List<dynamic> objData = new List<dynamic>();
           

            string insertProcedure = "[Update_TaskBid_Status]";
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Task_Bid_pkeyID", 1 + "#bigint#" + model.Task_Bid_pkeyID);
                input_parameters.Add("@Task_Bid_TaskID", 1 + "#bigint#" + model.Task_Bid_TaskID);
                input_parameters.Add("@Task_Bid_WO_ID", 1 + "#bigint#" + model.Task_Bid_WO_ID);
                input_parameters.Add("@Task_Bid_IsActive", 1 + "#bit#" + model.Task_Bid_IsActive);
                input_parameters.Add("@Task_Bid_IsDelete", 1 + "#bit#" + model.Task_Bid_IsDelete);
                input_parameters.Add("@Task_Bid_Status", 1 + "#int#" + model.Task_Bid_Status);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Task_Bid_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
              


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;



        }

        public List<dynamic> UpdateTaskBidstatus(UpdateTaskBidstatusDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                if (!string.IsNullOrEmpty(model.Task_Bid_Data))
                {
                    var Data = JsonConvert.DeserializeObject<List<TaskBidMasterDTO>>(model.Task_Bid_Data);
                    for (int i = 0; i < Data.Count; i++)
                    {
                        objData = UpdateTaskBid_Add_status(Data[i]);

                        if (Data[i].Task_Bid_Status == 2) // Approve
                        {
                            var workOrderHistoryPhotoDTOs = workOrderHistoryData.GetWorkOrderHistoryPhotoData(4, Data[i].Task_Bid_pkeyID, model.UserID);

                            foreach (var workOrderHistoryPhoto in workOrderHistoryPhotoDTOs)
                            {
                                Instruction_DocumentDTO instruction_DocumentDTO = new Instruction_DocumentDTO();
                                instruction_DocumentDTO.Inst_Doc_Wo_ID = Data[i].Task_Bid_WO_ID;
                                instruction_DocumentDTO.Inst_Doc_File_Path = workOrderHistoryPhoto.Client_Result_Photo_FilePath;
                                instruction_DocumentDTO.Inst_Doc_File_Name = workOrderHistoryPhoto.Client_Result_Photo_FileName;
                                instruction_DocumentDTO.Inst_Doc_Object_Name = workOrderHistoryPhoto.Client_Result_Photo_FileName;
                                instruction_DocumentDTO.Inst_Doc_Folder_Name = workOrderHistoryPhoto.Client_Result_Photo_FolderName;
                                instruction_DocumentDTO.Inst_Doc_UploadedBy = model.UserID.ToString();
                                instruction_DocumentDTO.Inst_Doc_IsActive = true;
                                instruction_DocumentDTO.Inst_Doc_IsDelete = false;
                                instruction_DocumentDTO.UserID = model.UserID;
                                instruction_DocumentDTO.Type = 1;
                                var fileObj = instruction_DocumentData.UploadInstructionDocumentData(instruction_DocumentDTO);
                            }
                        }
                    }
                }
          
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                log.logErrorMessage("UpdateTaskBidstatus---------" + model.Task_Bid_Data);
            }
            return objData;
        }

        }
}