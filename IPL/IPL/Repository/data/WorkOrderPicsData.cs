﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace IPL.Repository.data
{
    public class WorkOrderPicsData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        private DataSet GetPicsByWorkOrderID(WorkOrderPicsDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Z_GetPicsByWorkOrderID]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@WorkOrder_ID", 1 + "#bigint#" + model.WorkOrder_ID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        private List<dynamic> UpdatePicsPath(WorkOrderPicsDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();
            string insertProcedure = "[Z_UpdatePicPathByID]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Client_Result_Photo_ID", 1 + "#bigint#" + model.Client_Result_Photo_ID);
                input_parameters.Add("@Client_Result_Photo_localPath", 1 + "#nvarchar#" + model.Client_Result_Photo_localPath);
              

                objData = obj.SqlCRUD(insertProcedure, input_parameters);

                objcltData.Add(objData);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;



        }

        public List<dynamic> UpdateByWorkorderID(WorkOrderPicsDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            DataSet ds = GetPicsByWorkOrderID(model);
            try
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    model = new WorkOrderPicsDTO();
                    model.Client_Result_Photo_FileName = ds.Tables[0].Rows[i]["Client_Result_Photo_FileName"].ToString();
                    model.Client_Result_Photo_FolderName = ds.Tables[0].Rows[i]["Client_Result_Photo_FolderName"].ToString();
                    model.Client_Result_Photo_ID = Convert.ToInt64(ds.Tables[0].Rows[i]["Client_Result_Photo_ID"].ToString());

                    string strURL = System.Configuration.ConfigurationManager.AppSettings["IntegrationURL"] + "download";
                    FileUploadDownload fileUploadDownload = new FileUploadDownload();
                    string ReqData = "{\r\n \"FileName\": \"" + model.Client_Result_Photo_FileName + "\",\r\n \"FolderName\": \"" + model.Client_Result_Photo_FolderName + "\"\r\n}";

                    string strPath = fileUploadDownload.CallAPI(strURL, ReqData);
                    model.Client_Result_Photo_localPath = strPath;
                    UpdatePicsPath(model);

                }

                if (ds.Tables.Count > 1)
                {
                    Client_Result_PhotoData client_Result_PhotoData = new Client_Result_PhotoData();
                    
                    for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
                    {
                        Client_Result_PhotoDTO client_Result_PhotoDTO = new Client_Result_PhotoDTO();
                        client_Result_PhotoDTO.Client_Result_Photo_FileName = ds.Tables[1].Rows[i]["Client_Result_Photo_FileName"].ToString();
                        client_Result_PhotoDTO.Client_Result_Photo_FolderName = ds.Tables[1].Rows[i]["IPLNO"].ToString();
                        string strURL = System.Configuration.ConfigurationManager.AppSettings["IntegrationURL"] + "download";
                        FileUploadDownload fileUploadDownload = new FileUploadDownload();
                        string ReqData = "{\r\n \"FileName\": \"" + client_Result_PhotoDTO.Client_Result_Photo_FileName + "\",\r\n \"FolderName\": \"" + client_Result_PhotoDTO.Client_Result_Photo_FolderName + "\"\r\n}";
                        string strPath = fileUploadDownload.CallAPI(strURL, ReqData);
                        client_Result_PhotoDTO.Client_Result_Photo_FilePath = strPath;
                        client_Result_PhotoDTO.Client_Result_Photo_localPath = strPath;
                        int.TryParse(ds.Tables[1].Rows[i]["Orginal_Type"].ToString(), out int Type);
                        client_Result_PhotoDTO.Type = Type;
                        if (Type == 0)
                        {
                            client_Result_PhotoDTO.Type = 10;
                        }
                        if (!string.IsNullOrWhiteSpace(ds.Tables[1].Rows[i]["Client_Result_Photo_Wo_ID"].ToString()))
                        {
                            client_Result_PhotoDTO.Client_Result_Photo_Wo_ID = Convert.ToInt64(ds.Tables[1].Rows[i]["Client_Result_Photo_Wo_ID"].ToString());
                        }
                        if (!string.IsNullOrWhiteSpace(ds.Tables[1].Rows[i]["Client_Result_Photo_Type"].ToString()))
                        {
                            client_Result_PhotoDTO.Client_Result_Photo_Type = Convert.ToInt32(ds.Tables[1].Rows[i]["Client_Result_Photo_Type"].ToString());
                        }
                        if (!string.IsNullOrWhiteSpace(ds.Tables[1].Rows[i]["Client_Result_Photo_Ch_ID"].ToString()))
                        {
                            client_Result_PhotoDTO.Client_Result_Photo_Ch_ID = Convert.ToInt64(ds.Tables[1].Rows[i]["Client_Result_Photo_Ch_ID"].ToString());
                        }
                        client_Result_PhotoDTO.Client_Result_Photo_GPSLongitude = ds.Tables[1].Rows[i]["Client_Result_Photo_GPSLongitude"].ToString();
                        client_Result_PhotoDTO.Client_Result_Photo_GPSLatitude = ds.Tables[1].Rows[i]["Client_Result_Photo_GPSLatitude"].ToString();
                        if (!string.IsNullOrWhiteSpace(ds.Tables[1].Rows[i]["Client_Result_Photo_DateTimeOriginal"].ToString()))
                        {
                            client_Result_PhotoDTO.Client_Result_Photo_DateTimeOriginal = Convert.ToDateTime(ds.Tables[1].Rows[i]["Client_Result_Photo_DateTimeOriginal"].ToString());
                        }
                        if (!string.IsNullOrWhiteSpace(ds.Tables[1].Rows[i]["Client_Result_Photo_StatusType"].ToString()))
                        {
                            client_Result_PhotoDTO.Client_Result_Photo_StatusType = Convert.ToInt32(ds.Tables[1].Rows[i]["Client_Result_Photo_StatusType"].ToString());
                        }
                        if (!string.IsNullOrWhiteSpace(ds.Tables[1].Rows[i]["Client_Result_Photo_Task_Bid_pkeyID"].ToString()))
                        {
                            client_Result_PhotoDTO.Client_Result_Photo_Task_Bid_pkeyID = Convert.ToInt64(ds.Tables[1].Rows[i]["Client_Result_Photo_Task_Bid_pkeyID"].ToString());
                        }
                        client_Result_PhotoDTO.Client_Result_Photo_Model = ds.Tables[1].Rows[i]["Client_Result_Photo_Model"].ToString();
                        client_Result_PhotoDTO.Client_Result_Photo_Make = ds.Tables[1].Rows[i]["Client_Result_Photo_Make"].ToString();
                        client_Result_PhotoDTO.Client_Result_Photo_GPSAltitude = ds.Tables[1].Rows[i]["Client_Result_Photo_GPSAltitude"].ToString();
                        if (!string.IsNullOrWhiteSpace(ds.Tables[1].Rows[i]["Client_Result_Photo_MeteringMode"].ToString()))
                        {
                            client_Result_PhotoDTO.Client_Result_Photo_MeteringMode = Convert.ToInt32(ds.Tables[1].Rows[i]["Client_Result_Photo_MeteringMode"].ToString());
                        }
                        client_Result_PhotoData.AddClientResultPhotoData(client_Result_PhotoDTO);
                    }
                }
                objData.Add(1);
            }
            catch (Exception ex)
            {
                objData.Add(0);
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objData;
        }


        public List<dynamic> UpdateByWorkorderIDFromAPI(WorkOrderPicsDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            DataSet ds = GetPicsByWorkOrderID(model);
            try
            {

                if (ds.Tables.Count > 0)
                {
                    Client_Result_PhotoData client_Result_PhotoData = new Client_Result_PhotoData();

                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        Client_Result_PhotoDTO client_Result_PhotoDTO = new Client_Result_PhotoDTO();
                        client_Result_PhotoDTO.Client_Result_Photo_FileName = ds.Tables[0].Rows[i]["Client_Result_Photo_FileName"].ToString();
                        client_Result_PhotoDTO.Client_Result_Photo_FolderName = ds.Tables[0].Rows[i]["IPLNO"].ToString();
                        string strURL = System.Configuration.ConfigurationManager.AppSettings["IntegrationURL"] + "download";
                        FileUploadDownload fileUploadDownload = new FileUploadDownload();
                        string ReqData = "{\r\n \"FileName\": \"" + client_Result_PhotoDTO.Client_Result_Photo_FileName + "\",\r\n \"FolderName\": \"" + client_Result_PhotoDTO.Client_Result_Photo_FolderName + "\"\r\n}";
                        string strPath = fileUploadDownload.CallAPI(strURL, ReqData);
                        client_Result_PhotoDTO.Client_Result_Photo_FilePath = strPath;
                        client_Result_PhotoDTO.Client_Result_Photo_localPath = strPath;
                        int.TryParse(ds.Tables[0].Rows[i]["Orginal_Type"].ToString(), out int Type);
                        client_Result_PhotoDTO.Type = Type;
                        if (Type == 0)
                        {
                            client_Result_PhotoDTO.Type = 1;
                        }
                        if (!string.IsNullOrWhiteSpace(ds.Tables[0].Rows[i]["Client_Result_Photo_Wo_ID"].ToString()))
                        {
                            client_Result_PhotoDTO.Client_Result_Photo_Wo_ID = Convert.ToInt64(ds.Tables[0].Rows[i]["Client_Result_Photo_Wo_ID"].ToString());
                        }
                        if (!string.IsNullOrWhiteSpace(ds.Tables[0].Rows[i]["Client_Result_Photo_Type"].ToString()))
                        {
                            client_Result_PhotoDTO.Client_Result_Photo_Type = Convert.ToInt32(ds.Tables[0].Rows[i]["Client_Result_Photo_Type"].ToString());
                        }
                        if (!string.IsNullOrWhiteSpace(ds.Tables[0].Rows[i]["Client_Result_Photo_Ch_ID"].ToString()))
                        {
                            client_Result_PhotoDTO.Client_Result_Photo_Ch_ID = Convert.ToInt64(ds.Tables[0].Rows[i]["Client_Result_Photo_Ch_ID"].ToString());
                        }
                        client_Result_PhotoDTO.Client_Result_Photo_GPSLongitude = ds.Tables[0].Rows[i]["Client_Result_Photo_GPSLongitude"].ToString();
                        client_Result_PhotoDTO.Client_Result_Photo_GPSLatitude = ds.Tables[0].Rows[i]["Client_Result_Photo_GPSLatitude"].ToString();
                        if (!string.IsNullOrWhiteSpace(ds.Tables[0].Rows[i]["Client_Result_Photo_DateTimeOriginal"].ToString()))
                        {
                            client_Result_PhotoDTO.Client_Result_Photo_DateTimeOriginal = Convert.ToDateTime(ds.Tables[0].Rows[i]["Client_Result_Photo_DateTimeOriginal"].ToString());
                        }
                        if (!string.IsNullOrWhiteSpace(ds.Tables[0].Rows[i]["Client_Result_Photo_StatusType"].ToString()))
                        {
                            client_Result_PhotoDTO.Client_Result_Photo_StatusType = Convert.ToInt32(ds.Tables[0].Rows[i]["Client_Result_Photo_StatusType"].ToString());
                        }
                        if (!string.IsNullOrWhiteSpace(ds.Tables[0].Rows[i]["Client_Result_Photo_Task_Bid_pkeyID"].ToString()))
                        {
                            client_Result_PhotoDTO.Client_Result_Photo_Task_Bid_pkeyID = Convert.ToInt64(ds.Tables[0].Rows[i]["Client_Result_Photo_Task_Bid_pkeyID"].ToString());
                        }
                        client_Result_PhotoDTO.Client_Result_Photo_Model = ds.Tables[0].Rows[i]["Client_Result_Photo_Model"].ToString();
                        client_Result_PhotoDTO.Client_Result_Photo_Make = ds.Tables[0].Rows[i]["Client_Result_Photo_Make"].ToString();
                        client_Result_PhotoDTO.Client_Result_Photo_GPSAltitude = ds.Tables[0].Rows[i]["Client_Result_Photo_GPSAltitude"].ToString();
                        if (!string.IsNullOrWhiteSpace(ds.Tables[0].Rows[i]["Client_Result_Photo_MeteringMode"].ToString()))
                        {
                            client_Result_PhotoDTO.Client_Result_Photo_MeteringMode = Convert.ToInt32(ds.Tables[0].Rows[i]["Client_Result_Photo_MeteringMode"].ToString());
                        }
                        client_Result_PhotoData.AddClientResultPhotoData(client_Result_PhotoDTO);
                    }
                }
                objData.Add(1);
            }
            catch (Exception ex)
            {
                objData.Add(0);
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objData;
        }

    }
}