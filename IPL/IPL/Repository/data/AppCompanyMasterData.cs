﻿using Avigma.Repository.Lib;
using IPL.Models;
using IPLApp.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPLApp.Repository.data
{
    public class AppCompanyMasterData
    {

        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        //Add app Company Details
        public List<dynamic> AddAppCompanyData(AppCompanyMasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();
            string insertProcedure = "[CreateUpdateAppCompanyInfo]";
            AppCompanyMaster appCompanyMaster = new AppCompanyMaster();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@YR_Company_pkeyID", 1 + "#bigint#" + model.YR_Company_pkeyID);
                input_parameters.Add("@YR_Company_Name", 1 + "#nvarchar#" + model.YR_Company_Name);
                input_parameters.Add("@YR_Company_Con_Name", 1 + "#nvarchar#" + model.YR_Company_Con_Name);
                input_parameters.Add("@YR_Company_Email", 1 + "#nvarchar#" + model.YR_Company_Email);
                input_parameters.Add("@YR_Company_Phone", 1 + "#nvarchar#" + model.YR_Company_Phone);
                input_parameters.Add("@YR_Company_Address", 1 + "#nvarchar#" + model.YR_Company_Address);
                input_parameters.Add("@YR_Company_City", 1 + "#nvarchar#" + model.YR_Company_City);
                input_parameters.Add("@YR_Company_State", 1 + "#int#" + model.YR_Company_State);
                input_parameters.Add("@YR_Company_Zip", 1 + "#bigint#" + model.YR_Company_Zip);
                input_parameters.Add("@YR_Company_Logo", 1 + "#nvarchar#" + model.YR_Company_Logo);
                input_parameters.Add("@YR_Company_Support_Email", 1 + "#nvarchar#" + model.YR_Company_Support_Email);
                input_parameters.Add("@YR_Company_Support_Phone", 1 + "#nvarchar#" + model.YR_Company_Support_Phone);
                input_parameters.Add("@YR_Company_PDF_Heading", 1 + "#nvarchar#" + model.YR_Company_PDF_Heading);
                input_parameters.Add("@YR_Company_IsActive", 1 + "#bit#" + model.YR_Company_IsActive);
                input_parameters.Add("@YR_Company_IsDelete", 1 + "#bit#" + model.YR_Company_IsDelete);
                input_parameters.Add("@YR_Company_UserID", 1 + "#bigint#" + model.YR_Company_UserID);
                input_parameters.Add("@YR_Company_App_logo", 1 + "#varchar#" + model.YR_Company_App_logo);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@YR_Company_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    appCompanyMaster.YR_Company_pkeyID = "0";
                    appCompanyMaster.Status = "0";
                    appCompanyMaster.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    appCompanyMaster.YR_Company_pkeyID = Convert.ToString(objData[0]);
                    appCompanyMaster.Status = Convert.ToString(objData[1]);


                }
                objcltData.Add(appCompanyMaster);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;



        }

        private DataSet GetAppCompanyMaster(AppCompanyMasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_AppComapanyInfo]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@YR_Company_pkeyID", 1 + "#bigint#" + model.YR_Company_pkeyID);
                input_parameters.Add("@WhereClause", 1 + "#varchar#" + null);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@MenuID", 1 + "#bigint#" + 0);
                input_parameters.Add("@FilterData", 1 + "#varchar#" + null);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }
        //Get App Company Details 
        public List<dynamic> GetAppCompanyMasterDetails(AppCompanyMasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetAppCompanyMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<AppCompanyMasterDTO> AppClientDetails =
                   (from item in myEnumerableFeaprd
                    select new AppCompanyMasterDTO
                    {
                        YR_Company_pkeyID = item.Field<Int64>("YR_Company_pkeyID"),
                        YR_Company_Name = item.Field<String>("YR_Company_Name"),
                        YR_Company_Con_Name = item.Field<String>("YR_Company_Con_Name"),
                        YR_Company_Email = item.Field<String>("YR_Company_Email"),
                        YR_Company_Phone = item.Field<String>("YR_Company_Phone"),
                        YR_Company_Address = item.Field<String>("YR_Company_Address"),
                        YR_Company_City = item.Field<String>("YR_Company_City"),
                        YR_Company_State = item.Field<int?>("YR_Company_State"),
                        YR_Company_Zip = item.Field<Int64?>("YR_Company_Zip"),
                        YR_Company_Logo = item.Field<String>("YR_Company_Logo"),
                        YR_Company_Support_Email = item.Field<String>("YR_Company_Support_Email"),
                        YR_Company_Support_Phone = item.Field<String>("YR_Company_Support_Phone"),
                        YR_Company_PDF_Heading = item.Field<String>("YR_Company_PDF_Heading"),
                        YR_Company_App_logo = item.Field<String>("YR_Company_App_logo"),
                        YR_Company_IsActive = item.Field<Boolean?>("YR_Company_IsActive"),
                        ViewUrl = null

                    }).ToList();

                objDynamic.Add(AppClientDetails);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }


        // ADD Your Company Image in data base
        private List<dynamic> AddAppCompanyImageData(AppCompanyImagesDTO model)
        {
           
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();
            string insertProcedure = "[CreateUpdateAppCompanyImage]";
            AppCompanyImages appCompanyImages = new AppCompanyImages();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@App_Com_Img_pkeyID", 1 + "#bigint#" + model.App_Com_Img_pkeyID);
                input_parameters.Add("@App_Com_Img_FileName", 1 + "#nvarchar#" + model.App_Com_Img_FileName);
                input_parameters.Add("@App_Com_Img_FilePath", 1 + "#nvarchar#" + model.App_Com_Img_FilePath);
                input_parameters.Add("@App_Com_Img_CompanyID", 1 + "#bigint#" + model.App_Com_Img_CompanyID);
                input_parameters.Add("@App_Com_Img_IsActive", 1 + "#bit#" + model.App_Com_Img_IsActive);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@App_Com_Img_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    appCompanyImages.App_Com_Img_pkeyID = "0";
                    appCompanyImages.Status = "0";
                    appCompanyImages.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    appCompanyImages.App_Com_Img_pkeyID = Convert.ToString(objData[0]);
                    appCompanyImages.Status = Convert.ToString(objData[1]);


                }
                objcltData.Add(appCompanyImages);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;



        }

        private DataSet GetAppCompanyImage(AppCompanyImagesDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_AppCompanyImageMaster]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@App_Com_Img_pkeyID", 1 + "#bigint#" + model.App_Com_Img_pkeyID);
                input_parameters.Add("@App_Com_Img_CompanyID", 1 + "#bigint#" + model.App_Com_Img_CompanyID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }
        //Get App Company Image Details 
        public List<dynamic> GetAppCompanyImageDetails(AppCompanyImagesDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetAppCompanyImage(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<AppCompanyImagesDTO> AppCompanyImage =
                   (from item in myEnumerableFeaprd
                    select new AppCompanyImagesDTO
                    {
                        App_Com_Img_pkeyID = item.Field<Int64>("App_Com_Img_pkeyID"),
                        App_Com_Img_FileName = item.Field<String>("App_Com_Img_FileName"),
                        App_Com_Img_FilePath = item.Field<String>("App_Com_Img_FilePath"),
                        App_Com_Img_CompanyID = item.Field<Int64?>("App_Com_Img_CompanyID"),
                        App_Com_Img_IsActive = item.Field<Boolean?>("App_Com_Img_IsActive")



                    }).ToList();

                objDynamic.Add(AppCompanyImage);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        // for filter data company info 
        private DataSet GetAppCompanylist(AppCompanyMasterDTO model, SearchMasterDTO searchmodel)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_AppComapanyInfo]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@YR_Company_pkeyID", 1 + "#bigint#" + model.YR_Company_pkeyID);
                input_parameters.Add("@WhereClause", 1 + "#varchar#" + searchmodel.WhereClause);
                input_parameters.Add("@UserID", 1 + "#bigint#" + searchmodel.UserID);
                input_parameters.Add("@MenuID", 1 + "#bigint#" + searchmodel.MenuID);
                input_parameters.Add("@FilterData", 1 + "#varchar#" + searchmodel.FilterData);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }
        public List<dynamic> GetClientCompanyFilterDetails(AppCompanyMasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            string wherecondition = string.Empty;
            var Data = JsonConvert.DeserializeObject<AppCompanyMasterDTO>(model.FilterData);
            try
            {

                if (!string.IsNullOrEmpty(Data.YR_Company_Name))
                {
                    //wherecondition = " And YR_Company_Name =    '" + Data.YR_Company_Name + "'";
                    wherecondition = " And YR_Company_Name LIKE '%" + Data.YR_Company_Name + "%'";
                }
                if (!string.IsNullOrEmpty(Data.YR_Company_Con_Name))
                {
                    wherecondition = wherecondition + "  And YR_Company_Con_Name LIKE '%" + Data.YR_Company_Con_Name + "%'";
                }
                if (!string.IsNullOrEmpty(Data.YR_Company_Email))
                {
                    wherecondition = wherecondition + "  And YR_Company_Email LIKE '%" + Data.YR_Company_Email + "%'";
                }
                if (!string.IsNullOrEmpty(Data.YR_Company_Phone))
                {
                    wherecondition = wherecondition + "  And YR_Company_Phone LIKE '%" + Data.YR_Company_Phone + "%'";
                }
                if (!string.IsNullOrEmpty(Data.YR_Company_Address))
                {
                    wherecondition = wherecondition + "  And YR_Company_Address LIKE '%" + Data.YR_Company_Address + "%'";
                }
                if (!string.IsNullOrEmpty(Data.YR_Company_City))
                {
                    wherecondition = wherecondition + "  And YR_Company_City LIKE '%" + Data.YR_Company_City + "%'";
                }
                if (Data.YR_Company_State != 0 && Data.YR_Company_State != null)
                {
                    wherecondition = wherecondition + "  And YR_Company_State LIKE '%" + Data.YR_Company_State + "%'";
                }
                if (Data.YR_Company_Zip != 0 && Data.YR_Company_Zip != null)
                {
                    wherecondition = wherecondition + "  And YR_Company_Zip LIKE '%" + Data.YR_Company_Zip + "%'";
                }
                if (!string.IsNullOrEmpty(Data.YR_Company_Logo))
                {
                    wherecondition = wherecondition + "  And YR_Company_Logo LIKE '%" + Data.YR_Company_Logo + "%'";
                }
                if (!string.IsNullOrEmpty(Data.YR_Company_Support_Email))
                {
                    wherecondition = wherecondition + "  And YR_Company_Support_Email LIKE '%" + Data.YR_Company_Support_Email + "%'";
                }
                if (!string.IsNullOrEmpty(Data.YR_Company_Support_Phone))
                {
                    wherecondition = wherecondition + "  And YR_Company_Support_Phone LIKE '%" + Data.YR_Company_Support_Phone + "%'";
                }
                if (!string.IsNullOrEmpty(Data.YR_Company_PDF_Heading))
                {
                    wherecondition = wherecondition + "  And YR_Company_PDF_Heading LIKE '%" + Data.YR_Company_PDF_Heading + "%'";
                }


                if (Data.YR_Company_IsActive == true)
                {
                    wherecondition = wherecondition + "  And YR_Company_IsActive =  '1'";
                }
                if (Data.YR_Company_IsActive == false)
                {
                    wherecondition = wherecondition + "  And YR_Company_IsActive =  '0'";
                }
                AppCompanyMasterDTO appCompanyMasterDTO = new AppCompanyMasterDTO();
                SearchMasterDTO searchMasterDTO = new SearchMasterDTO();
                appCompanyMasterDTO.Type = 3;
                searchMasterDTO.MenuID = model.SearchMaster.MenuID;
                searchMasterDTO.UserID = model.UserID;
                searchMasterDTO.WhereClause = wherecondition;
                DataSet ds = GetAppCompanylist(appCompanyMasterDTO, searchMasterDTO);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<AppCompanyMasterDTO> AppCompanyDetails =
                   (from item in myEnumerableFeaprd
                    select new AppCompanyMasterDTO
                    {
                        YR_Company_pkeyID = item.Field<Int64>("YR_Company_pkeyID"),
                        YR_Company_Name = item.Field<String>("YR_Company_Name"),
                        YR_Company_Con_Name = item.Field<String>("YR_Company_Con_Name"),
                        YR_Company_Email = item.Field<String>("YR_Company_Email"),
                        YR_Company_Phone = item.Field<String>("YR_Company_Phone"),
                        YR_Company_Address = item.Field<String>("YR_Company_Address"),
                        YR_Company_City = item.Field<String>("YR_Company_City"),
                        YR_Company_State = item.Field<int?>("YR_Company_State"),
                        YR_Company_Zip = item.Field<Int64?>("YR_Company_Zip"),
                        YR_Company_Logo = item.Field<String>("YR_Company_Logo"),
                        YR_Company_Support_Email = item.Field<String>("YR_Company_Support_Email"),
                        YR_Company_Support_Phone = item.Field<String>("YR_Company_Support_Phone"),
                        YR_Company_PDF_Heading = item.Field<String>("YR_Company_PDF_Heading"),
                        YR_Company_App_logo = item.Field<String>("YR_Company_App_logo"),
                        YR_Company_IsActive = item.Field<Boolean?>("YR_Company_IsActive"),


                    }).ToList();

                objDynamic.Add(AppCompanyDetails);

                var myEnumerable = ds.Tables[1].AsEnumerable();
                List<UserFilterDTO> UserFilterDetails =
                   (from item in myEnumerable
                    select new UserFilterDTO
                    {
                        usr_filter_pkeyID = item.Field<Int64>("usr_filter_pkeyID"),
                        usr_filter_WhereData = item.Field<String>("usr_filter_WhereData"),
                        usr_filter_FilterData = item.Field<String>("usr_filter_FilterData"),
                        usr_filter_MenuId = item.Field<Int64?>("usr_filter_MenuId"),
                        usr_filter_UserID = item.Field<Int64?>("usr_filter_UserID"),



                    }).ToList();

                objDynamic.Add(UserFilterDetails);



            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objDynamic;
        }

        //image upload
        public List<dynamic> AddUpdateCompanyPhotoData(Client_Result_PhotoDTO client_Result_PhotoDTO)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {

                AppCompanyImagesDTO appCompanyImagesDTO = new AppCompanyImagesDTO();

                appCompanyImagesDTO.App_Com_Img_pkeyID = client_Result_PhotoDTO.Client_Result_Photo_ID;
                appCompanyImagesDTO.App_Com_Img_FileName = client_Result_PhotoDTO.Client_Result_Photo_FileName;
                appCompanyImagesDTO.App_Com_Img_FilePath = client_Result_PhotoDTO.Client_Result_Photo_FilePath;
                appCompanyImagesDTO.App_Com_Img_CompanyID = client_Result_PhotoDTO.Client_Result_Photo_Wo_ID;
                appCompanyImagesDTO.App_Com_Img_IsActive = true;
                appCompanyImagesDTO.UserID = client_Result_PhotoDTO.UserID;
                appCompanyImagesDTO.Type = 1;


                objData = AddAppCompanyImageData(appCompanyImagesDTO);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        public List<dynamic> AddUpdateCompanyApplogoData(Client_Result_PhotoDTO client_Result_PhotoDTO)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {

                AppCompanyMasterDTO appCompanyMasterDTO = new AppCompanyMasterDTO();

                appCompanyMasterDTO.YR_Company_pkeyID = client_Result_PhotoDTO.Client_Result_Photo_ID;
                appCompanyMasterDTO.YR_Company_App_logo = client_Result_PhotoDTO.Client_Result_Photo_FilePath;
                appCompanyMasterDTO.Type = 5;


                objData = AddAppCompanyData(appCompanyMasterDTO);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }



    }
}