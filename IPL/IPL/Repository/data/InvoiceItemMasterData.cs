﻿using Avigma.Repository.Lib;
using IPLApp.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPLApp.Repository.data
{
    public class InvoiceItemMasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public List<dynamic> AddInvoiceItemData(InvoiceItemMasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objInvData = new List<dynamic>();
            string insertProcedure = "[CreateUpdateInvoiceItem]";
            InvoiceItemMaster invoiceItemMaster = new InvoiceItemMaster();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Inv_Itm_pkeyID", 1 + "#bigint#" + model.Inv_Itm_pkeyID);
                input_parameters.Add("@Inv_Itm_Name", 1 + "#nvarchar#" + model.Inv_Itm_Name);
                input_parameters.Add("@Inv_Itm_IsActive", 1 + "#bit#" + model.Inv_Itm_IsActive);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Inv_Itm_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    invoiceItemMaster.Inv_Itm_pkeyID = "0";
                    invoiceItemMaster.Status = "0";
                    invoiceItemMaster.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    invoiceItemMaster.Inv_Itm_pkeyID = Convert.ToString(objData[0]);
                    invoiceItemMaster.Status = Convert.ToString(objData[1]);


                }
                objInvData.Add(invoiceItemMaster);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objInvData;



        }

        private DataSet GetInvoiceItemMaster(InvoiceItemMasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_InvoiceItemMaster]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Inv_Itm_pkeyID", 1 + "#bigint#" + model.Inv_Itm_pkeyID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetInvoiceItemDetails(InvoiceItemMasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetInvoiceItemMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<InvoiceItemMasterDTO> InvoiceItemDetails =
                   (from item in myEnumerableFeaprd
                    select new InvoiceItemMasterDTO
                    {
                        Inv_Itm_pkeyID = item.Field<Int64>("Inv_Itm_pkeyID"),
                        Inv_Itm_Name = item.Field<String>("Inv_Itm_Name"),
                        Inv_Itm_IsActive = item.Field<Boolean?>("Inv_Itm_IsActive"),

                    }).ToList();

                objDynamic.Add(InvoiceItemDetails);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}