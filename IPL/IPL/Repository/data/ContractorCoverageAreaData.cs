﻿using Avigma.Models;
using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class ContractorCoverageAreaData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        public List<dynamic> AddContractorCoverageAreaData(ContractorCoverageAreaDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();
            string insertProcedure = "[CreateUpdateContractorCoverageAreaMaster]";
            ContractorCoverageArea contractorCoverageArea = new ContractorCoverageArea();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Cont_Coverage_Area_PkeyId", 1 + "#bigint#" + model.Cont_Coverage_Area_PkeyId);
                input_parameters.Add("@Cont_Coverage_Area_UserID", 1 + "#bigint#" + model.Cont_Coverage_Area_UserID);
                input_parameters.Add("@Cont_Coverage_Area_State_Id", 1 + "#bigint#" + model.Cont_Coverage_Area_State_Id);
                input_parameters.Add("@Cont_Coverage_Area_County_Id", 1 + "#bigint#" + model.Cont_Coverage_Area_County_Id);
                input_parameters.Add("@Cont_Coverage_Area_Zip_Code", 1 + "#bigint#" + model.Cont_Coverage_Area_Zip_Code);
                input_parameters.Add("@Cont_Coverage_Area_IsActive", 1 + "#bit#" + model.Cont_Coverage_Area_IsActive);
                input_parameters.Add("@Cont_Coverage_Area_IsDelete", 1 + "#bit#" + model.Cont_Coverage_Area_IsDelete);
                input_parameters.Add("@Cont_Coverage_Area_UserAddressID", 1 + "#bigint#" + model.Cont_Coverage_Area_UserAddressID);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Cont_Coverage_Area_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    contractorCoverageArea.Cont_Coverage_Area_PkeyId = "0";
                    contractorCoverageArea.Status = "0";
                    contractorCoverageArea.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    contractorCoverageArea.Cont_Coverage_Area_PkeyId = Convert.ToString(objData[0]);
                    contractorCoverageArea.Status = Convert.ToString(objData[1]);


                }
                objcltData.Add(contractorCoverageArea);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;



        }

        private DataSet GetContractorCoverageAreaMaster(ContractorCoverageAreaDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_ContractorCoverageAreaMaster]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Cont_Coverage_Area_PkeyId", 1 + "#bigint#" + model.Cont_Coverage_Area_PkeyId);
                input_parameters.Add("@whereClause", 1 + "#nvarchar#" + model.whereClause);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return ds;
        }

        public List<dynamic> GetContractorCoverageAreaDetails(ContractorCoverageAreaDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetContractorCoverageAreaMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<ContractorCoverageAreaDTO> ContractorCoverageAreaDetails =
                   (from item in myEnumerableFeaprd
                    select new ContractorCoverageAreaDTO
                    {
                        Cont_Coverage_Area_PkeyId = item.Field<Int64>("Cont_Coverage_Area_PkeyId"),
                        Cont_Coverage_Area_UserID = item.Field<Int64?>("Cont_Coverage_Area_UserID"),
                        Cont_Coverage_Area_State_Id = item.Field<Int64?>("Cont_Coverage_Area_State_Id"),
                        Cont_Coverage_Area_County_Id = item.Field<Int64?>("Cont_Coverage_Area_County_Id"),
                        Cont_Coverage_Area_Zip_Code = item.Field<Int64?>("Cont_Coverage_Area_Zip_Code"),
                        Cont_Coverage_Area_IsActive = item.Field<Boolean?>("Cont_Coverage_Area_IsActive"),

                    }).ToList();

                objDynamic.Add(ContractorCoverageAreaDetails);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        public List<dynamic> AddUserZipData(ZipMasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                GoogleLocation googleLocation = new GoogleLocation();
                GoogleLocationDTO googleLocationDTO = new GoogleLocationDTO();
                googleLocationDTO.Address = model.Zip_zip.ToString() ;
                googleLocationDTO = googleLocation.GetLatitudeLogitudeWithAPIKey(googleLocationDTO);
                model.Zip_lat = googleLocationDTO.Latitude;
                model.Zip_lng = googleLocationDTO.Longitude;
                objDynamic = AddUserZipCode(model);
                
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }


        public List<dynamic> AddUserZipCode(ZipMasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
          
            string insertProcedure = "[InsertZipCode]";
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Zip_ID", 1 + "#bigint#" + model.Zip_ID);
                input_parameters.Add("@Zip_zip", 1 + "#bigint#" + model.Zip_zip);
                input_parameters.Add("@Zip_lat", 1 + "#varchar#" + model.Zip_lat);
                input_parameters.Add("@Zip_lng", 1 + "#varchar#" + model.Zip_lng);
                input_parameters.Add("@Zip_city", 1 + "#varchar#" + model.Zip_city);
                input_parameters.Add("@Zip_state_id", 1 + "#varchar#" + model.Zip_state_id);
                input_parameters.Add("@Zip_state_name", 1 + "#varchar#" + model.Zip_state_name);
                input_parameters.Add("@Zip_zcta", 1 + "#varchar#" + model.Zip_zcta);
                input_parameters.Add("@Zip_parent_zcta", 1 + "#varchar#" + model.Zip_parent_zcta);
                input_parameters.Add("@Zip_population", 1 + "#varchar#" + model.Zip_population);
                input_parameters.Add("@Zip_density", 1 + "#varchar#" + model.Zip_density);
                input_parameters.Add("@Zip_county_fips", 1 + "#varchar#" + model.Zip_county_fips);
                input_parameters.Add("@Zip_county_name", 1 + "#varchar#" + model.Zip_county_name);
                input_parameters.Add("@Zip_county_weights", 1 + "#varchar#" + model.Zip_county_weights);
                input_parameters.Add("@Zip_county_names_all", 1 + "#varchar#" + model.Zip_county_names_all);
                input_parameters.Add("@Zip_county_fips_all", 1 + "#varchar#" + model.Zip_county_fips_all);
                input_parameters.Add("@Zip_imprecise", 1 + "#varchar#" + model.Zip_imprecise);
                input_parameters.Add("@Zip_military", 1 + "#varchar#" + model.Zip_military);
                input_parameters.Add("@Zip_timezone", 1 + "#varchar#" + model.Zip_timezone);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Zip_ID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);

              
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;



        }

        public DataSet GetZipAddress(ZipMasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_ZipAddress]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Zip_zip", 1 + "#bigint#" + model.Zip_zip);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ds;
        }

        public List<dynamic> GetZipAddressDetails(ZipMasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetZipAddress(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<ZipMasterDTO> zipAddress =
                   (from item in myEnumerableFeaprd
                    select new ZipMasterDTO
                    {
                        Zip_zip = item.Field<Int64?>("Zip_zip"),
                        ZipState_id = item.Field<Int64?>("Zip_state_id"),
                        Zip_city = item.Field<String>("Zip_city"),

                    }).ToList();

                objDynamic.Add(zipAddress);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

    }
}