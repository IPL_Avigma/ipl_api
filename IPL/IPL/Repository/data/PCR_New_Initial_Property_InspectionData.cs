﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class PCR_New_Initial_Property_InspectionData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();


        private DataSet GetPCRNewInitialPropertyInspection(PCR_New_Initial_Property_InspectionDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_PCR_New_Initial_Property_Inspection]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> PCRNewInitialPropertyInspectionDetails(PCR_New_Initial_Property_InspectionDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetPCRNewInitialPropertyInspection(model);

                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                }


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}