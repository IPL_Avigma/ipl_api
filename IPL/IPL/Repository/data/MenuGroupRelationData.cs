﻿
using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPLApp.Repository.data
{
    public class MenuGroupRelationData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        public List<dynamic> AddMenuGroupData(MenuGroupRelationDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();
            MenuGroupRelation menuGroupRelation = new MenuGroupRelation();
            string insertProcedure = "[CreateUpdateMenuGroupMaster]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Mgr_MenuRoleRelation_Id", 1 + "#bigint#" + model.Mgr_MenuRoleRelation_Id);
                input_parameters.Add("@Mgr_Group_Id", 1 + "#bigint#" + model.Mgr_Group_Id);
                input_parameters.Add("@Mgr_Menu_Id", 1 + "#bigint#" + model.Mgr_Menu_Id);
                input_parameters.Add("@Mgr_IsActive", 1 + "#bit#" + model.Mgr_IsActive);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Mgr_MenuRoleRelation_Id_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters); 
                if (objData[1] == 0)
                {
                    menuGroupRelation.Mgr_MenuRoleRelation_Id = "0";
                    menuGroupRelation.Status = "0";
                    menuGroupRelation.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    menuGroupRelation.Mgr_MenuRoleRelation_Id = Convert.ToString(objData[0]);
                    menuGroupRelation.Status = Convert.ToString(objData[1]);


                }
                objcltData.Add(menuGroupRelation);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;



        }

        private DataSet Get_Menu_Group_Relation(MenuGroupRelationDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Menu_Group_Relation]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Mgr_MenuRoleRelation_Id", 1 + "#bigint#" + model.Mgr_MenuRoleRelation_Id);

                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }
        public List<dynamic> Get_Menu_Group_RelationDetails(MenuGroupRelationDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = Get_Menu_Group_Relation(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<MenuGroupRelationDTO> MenuGroupRelation =
                   (from item in myEnumerableFeaprd
                    select new MenuGroupRelationDTO
                    {
                        Mgr_MenuRoleRelation_Id = item.Field<Int64>("Mgr_MenuRoleRelation_Id"),
                        Mgr_Group_Id = item.Field<Int64?>("Mgr_Group_Id"),
                        Mgr_Menu_Id = item.Field<Int64?>("Mgr_Menu_Id"),
                        Mgr_IsActive = item.Field<Boolean?>("Mgr_IsActive"),




                    }).ToList();

                objDynamic.Add(MenuGroupRelation);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}