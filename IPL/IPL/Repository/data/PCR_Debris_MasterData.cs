﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class PCR_Debris_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public List<dynamic> AddPCR_Debris_Data(PCR_Debris_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objclntData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_PCR_Debris_Master]";
            PCR_Debris_Master pCR_Debris_Master = new PCR_Debris_Master();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@PCR_Debris_pkeyId", 1 + "#bigint#" + model.PCR_Debris_pkeyId);
                input_parameters.Add("@PCR_Debris_Master_Id", 1 + "#bigint#" + model.PCR_Debris_Master_Id);
                input_parameters.Add("@PCR_Debris_WO_Id", 1 + "#bigint#" + model.PCR_Debris_WO_Id);
                input_parameters.Add("@PCR_Debris_ValType", 1 + "#int#" + model.PCR_Debris_ValType);
                //input_parameters.Add("@PCR_Debris_Interior_Debris", 1 + "#varchar#" + model.PCR_Debris_Interior_Debris);
                input_parameters.Add("@PCR_Debris_Is_There_Interior_Debris_Present", 1 + "#varchar#" + model.PCR_Debris_Is_There_Interior_Debris_Present);
                input_parameters.Add("@PCR_Debris_Remove_Any_Interior_Debris", 1 + "#varchar#" + model.PCR_Debris_Remove_Any_Interior_Debris);
                input_parameters.Add("@PCR_Debris_Describe", 1 + "#varchar#" + model.PCR_Debris_Describe);
                input_parameters.Add("@PCR_Debris_Cubic_Yards", 1 + "#varchar#" + model.PCR_Debris_Cubic_Yards);
                input_parameters.Add("@PCR_Debris_Broom_Swept_Condition", 1 + "#varchar#" + model.PCR_Debris_Broom_Swept_Condition);
                input_parameters.Add("@PCR_Debris_Broom_Swept_Condition_Describe", 1 + "#varchar#" + model.PCR_Debris_Broom_Swept_Condition_Describe);
                //input_parameters.Add("@PCR_Debris_Exterior_Debris", 1 + "#varchar#" + model.PCR_Debris_Exterior_Debris);
                input_parameters.Add("@PCR_Debris_Remove_Exterior_Debris", 1 + "#varchar#" + model.PCR_Debris_Remove_Exterior_Debris);
                input_parameters.Add("@PCR_Debris_Exterior_Debris_Present", 1 + "#varchar#" + model.PCR_Debris_Exterior_Debris_Present);
                input_parameters.Add("@PCR_Debris_Exterior_Debris_Describe", 1 + "#varchar#" + model.PCR_Debris_Exterior_Debris_Describe);
                input_parameters.Add("@PCR_Debris_Exterior_Debris_Cubic_Yard", 1 + "#varchar#" + model.PCR_Debris_Exterior_Debris_Cubic_Yard);
                input_parameters.Add("@PCR_Debris_Exterior_Debris_Visible_From_Street", 1 + "#varchar#" + model.PCR_Debris_Exterior_Debris_Visible_From_Street);
                input_parameters.Add("@PCR_Debris_Exterior_On_The_Lawn", 1 + "#varchar#" + model.PCR_Debris_Exterior_On_The_Lawn);
                input_parameters.Add("@PCR_Debris_Exterior_Vehicles_Present", 1 + "#varchar#" + model.PCR_Debris_Exterior_Vehicles_Present);
                input_parameters.Add("@PCR_Debris_Exterior_Vehicles_Present_Describe", 1 + "#varchar#" + model.PCR_Debris_Exterior_Vehicles_Present_Describe);
                input_parameters.Add("@PCR_Debris_Dump_Recipt_Name", 1 + "#varchar#" + model.PCR_Debris_Dump_Recipt_Name);
                input_parameters.Add("@PCR_Debris_Dump_Recipt_Address", 1 + "#varchar#" + model.PCR_Debris_Dump_Recipt_Address);
                input_parameters.Add("@PCR_Debris_Dump_Recipt_Phone", 1 + "#varchar#" + model.PCR_Debris_Dump_Recipt_Phone);
                input_parameters.Add("@PCR_Debris_Dump_Recipt_Desc_what_was_Dump", 1 + "#varchar#" + model.PCR_Debris_Dump_Recipt_Desc_what_was_Dump);
                input_parameters.Add("@PCR_Debris_Dump_Recipt_Means_Of_Disposal", 1 + "#varchar#" + model.PCR_Debris_Dump_Recipt_Means_Of_Disposal);
                input_parameters.Add("@PCR_Debris_InteriorHazards_Health_Present", 1 + "#varchar#" + model.PCR_Debris_InteriorHazards_Health_Present);
                input_parameters.Add("@PCR_Debris_InteriorHazards_Health_Present_Describe", 1 + "#varchar#" + model.PCR_Debris_InteriorHazards_Health_Present_Describe);
                input_parameters.Add("@PCR_Debris_InteriorHazards_Health_Present_Cubic_Yard", 1 + "#varchar#" + model.PCR_Debris_InteriorHazards_Health_Present_Cubic_Yard);
                input_parameters.Add("@PCR_Debris_Exterior_Hazards_Health_Present", 1 + "#varchar#" + model.PCR_Debris_Exterior_Hazards_Health_Present);
                input_parameters.Add("@PCR_Debris_Exterior_Hazards_Health_Present_Describe", 1 + "#varchar#" + model.PCR_Debris_Exterior_Hazards_Health_Present_Describe);
                input_parameters.Add("@PCR_Debris_Exterior_Hazards_Health_PresentCubic_Yards", 1 + "#varchar#" + model.PCR_Debris_Exterior_Hazards_Health_PresentCubic_Yards);
                input_parameters.Add("@PCR_Debris_IsActive", 1 + "#bit#" + model.PCR_Debris_IsActive);
                input_parameters.Add("@PCR_Debris_IsDelete", 1 + "#bit#" + model.PCR_Debris_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@fwo_pkyeId", 1 + "#bigint#" + model.fwo_pkyeId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@PCR_Debris_pkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    pCR_Debris_Master.PCR_Debris_pkeyId = "0";
                    pCR_Debris_Master.Status = "0";
                    pCR_Debris_Master.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    pCR_Debris_Master.PCR_Debris_pkeyId = Convert.ToString(objData[0]);
                    pCR_Debris_Master.Status = Convert.ToString(objData[1]);


                }
                objclntData.Add(pCR_Debris_Master);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objclntData;



        }

        private DataSet GetPCRDebrisMaster(PCR_Debris_MasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_PCR_Debris_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@PCR_Debris_pkeyId", 1 + "#bigint#" + model.PCR_Debris_pkeyId);
                input_parameters.Add("@PCR_Debris_WO_Id", 1 + "#bigint#" + model.PCR_Debris_WO_Id);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetPCRDebrisDetails(PCR_Debris_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetPCRDebrisMaster(model);

                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                }


                //var PCRDebris = DatatableToModel(ds.Tables[0]);
                //objDynamic.Add(PCRDebris);

                //if (ds.Tables.Count > 1)
                //{
                //    var history = DatatableToModel(ds.Tables[1]);
                //    objDynamic.Add(history);
                //}
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
        private List<PCR_Debris_MasterDTO> DatatableToModel(DataTable dataTable)
        {
            var myEnumerableFeaprd = dataTable.AsEnumerable();
            List<PCR_Debris_MasterDTO> PCRDebris =
                   (from item in myEnumerableFeaprd
                    select new PCR_Debris_MasterDTO
                    {
                        PCR_Debris_pkeyId = item.Field<Int64>("PCR_Debris_pkeyId"),
                        PCR_Debris_Master_Id = item.Field<Int64>("PCR_Debris_Master_Id"),
                        PCR_Debris_WO_Id = item.Field<Int64>("PCR_Debris_WO_Id"),
                        PCR_Debris_ValType = item.Field<int>("PCR_Debris_ValType"),
                        //PCR_Debris_Interior_Debris = item.Field<String>("PCR_Debris_Interior_Debris"),
                        PCR_Debris_Is_There_Interior_Debris_Present = item.Field<String>("PCR_Debris_Is_There_Interior_Debris_Present"),
                        PCR_Debris_Remove_Any_Interior_Debris = item.Field<String>("PCR_Debris_Remove_Any_Interior_Debris"),
                        PCR_Debris_Describe = item.Field<String>("PCR_Debris_Describe"),
                        PCR_Debris_Cubic_Yards = item.Field<String>("PCR_Debris_Cubic_Yards"),
                        PCR_Debris_Broom_Swept_Condition = item.Field<String>("PCR_Debris_Broom_Swept_Condition"),
                        PCR_Debris_Broom_Swept_Condition_Describe = item.Field<String>("PCR_Debris_Broom_Swept_Condition_Describe"),
                        //PCR_Debris_Exterior_Debris = item.Field<String>("PCR_Debris_Exterior_Debris"),
                        PCR_Debris_Remove_Exterior_Debris = item.Field<String>("PCR_Debris_Remove_Exterior_Debris"),
                        PCR_Debris_Exterior_Debris_Present = item.Field<String>("PCR_Debris_Exterior_Debris_Present"),
                        PCR_Debris_Exterior_Debris_Describe = item.Field<String>("PCR_Debris_Exterior_Debris_Describe"),
                        PCR_Debris_Exterior_Debris_Cubic_Yard = item.Field<String>("PCR_Debris_Exterior_Debris_Cubic_Yard"),
                        PCR_Debris_Exterior_Debris_Visible_From_Street = item.Field<String>("PCR_Debris_Exterior_Debris_Visible_From_Street"),
                        PCR_Debris_Exterior_On_The_Lawn = item.Field<String>("PCR_Debris_Exterior_On_The_Lawn"),
                        PCR_Debris_Exterior_Vehicles_Present = item.Field<String>("PCR_Debris_Exterior_Vehicles_Present"),
                        PCR_Debris_Exterior_Vehicles_Present_Describe = item.Field<String>("PCR_Debris_Exterior_Vehicles_Present_Describe"),
                        PCR_Debris_Dump_Recipt_Name = item.Field<String>("PCR_Debris_Dump_Recipt_Name"),
                        PCR_Debris_Dump_Recipt_Address = item.Field<String>("PCR_Debris_Dump_Recipt_Address"),
                        PCR_Debris_Dump_Recipt_Phone = item.Field<String>("PCR_Debris_Dump_Recipt_Phone"),
                        PCR_Debris_Dump_Recipt_Desc_what_was_Dump = item.Field<String>("PCR_Debris_Dump_Recipt_Desc_what_was_Dump"),
                        PCR_Debris_Dump_Recipt_Means_Of_Disposal = item.Field<String>("PCR_Debris_Dump_Recipt_Means_Of_Disposal"),
                        PCR_Debris_InteriorHazards_Health_Present = item.Field<String>("PCR_Debris_InteriorHazards_Health_Present"),
                        PCR_Debris_InteriorHazards_Health_Present_Describe = item.Field<String>("PCR_Debris_InteriorHazards_Health_Present_Describe"),
                        PCR_Debris_InteriorHazards_Health_Present_Cubic_Yard = item.Field<String>("PCR_Debris_InteriorHazards_Health_Present_Cubic_Yard"),
                        PCR_Debris_Exterior_Hazards_Health_Present = item.Field<String>("PCR_Debris_Exterior_Hazards_Health_Present"),
                        PCR_Debris_Exterior_Hazards_Health_Present_Describe = item.Field<String>("PCR_Debris_Exterior_Hazards_Health_Present_Describe"),
                        PCR_Debris_Exterior_Hazards_Health_PresentCubic_Yards = item.Field<String>("PCR_Debris_Exterior_Hazards_Health_PresentCubic_Yards"),
                        PCR_Debris_IsActive = item.Field<Boolean?>("PCR_Debris_IsActive"),


                    }).ToList();
            return PCRDebris;
        }
    }
}