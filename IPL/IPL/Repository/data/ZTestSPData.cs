﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;


namespace IPL.Repository.data
{
    public class ZTestSPData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        private List<dynamic> Testupload(int i)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();
            string insertProcedure = "[Z_Testupload]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@inputval", 1 + "#int#" + i);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);


                objData = obj.SqlCRUD(insertProcedure, input_parameters);

                objcltData.Add(objData);
                log.logDebugMessage(i);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;



        }

        public void ChkLoad()
        {
            try
            {
                for (int i = 0; i < 500; i++)
                {
                    Testupload(i);
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

        }
    }
}