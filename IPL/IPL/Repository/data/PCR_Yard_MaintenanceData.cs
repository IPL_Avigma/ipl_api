﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class PCR_Yard_MaintenanceData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public List<dynamic> AddPCR_Yard_Maintenance_Data(PCR_Yard_MaintenanceDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objclntData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_PCR_Yard_Maintenance_Master]";
            PCR_Yard_Maintenance pCR_Yard_Maintenance = new PCR_Yard_Maintenance();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {

                input_parameters.Add("@PCR_Yard_Maintenance_pkeyId", 1 + "#bigint#" + model.PCR_Yard_Maintenance_pkeyId);
                input_parameters.Add("@PCR_Yard_Maintenance_MasterId", 1 + "#bigint#" + model.PCR_Yard_Maintenance_MasterId);
                input_parameters.Add("@PCR_Yard_Maintenance_WO_Id", 1 + "#bigint#" + model.PCR_Yard_Maintenance_WO_Id);
                input_parameters.Add("@PCR_Yard_Maintenance_ValType", 1 + "#int#" + model.PCR_Yard_Maintenance_ValType);

                input_parameters.Add("@PCR_Yard_Maintenance_Grass_Cut_Completed", 1 + "#varchar#" + model.PCR_Yard_Maintenance_Grass_Cut_Completed);
                input_parameters.Add("@PCR_Yard_Maintenance_Lot_Size", 1 + "#varchar#" + model.PCR_Yard_Maintenance_Lot_Size);
                input_parameters.Add("@PCR_Yard_Maintenance_Cuttable_Area", 1 + "#varchar#" + model.PCR_Yard_Maintenance_Cuttable_Area);
                input_parameters.Add("@PCR_Yard_Maintenance_Bit_To_Cut_Grass_Lot_Dimension_Lenght", 1 + "#varchar#" + model.PCR_Yard_Maintenance_Bit_To_Cut_Grass_Lot_Dimension_Lenght);
                input_parameters.Add("@PCR_Yard_Maintenance_Bit_To_Cut_Grass_Lot_Dimension_Width", 1 + "#varchar#" + model.PCR_Yard_Maintenance_Bit_To_Cut_Grass_Lot_Dimension_Width);
                input_parameters.Add("@PCR_Yard_Maintenance_Bit_To_Cut_Grass_Lot_Dimension_Height", 1 + "#varchar#" + model.PCR_Yard_Maintenance_Bit_To_Cut_Grass_Lot_Dimension_Height);
                input_parameters.Add("@PCR_Yard_Maintenance_Bit_To_Cut_Grass_Bid_For_Inital_Cut", 1 + "#varchar#" + model.PCR_Yard_Maintenance_Bit_To_Cut_Grass_Bid_For_Inital_Cut);
                input_parameters.Add("@PCR_Yard_Maintenance_Bit_To_Cut_Grass_Reason_For_Inital_Cut", 1 + "#varchar#" + model.PCR_Yard_Maintenance_Bit_To_Cut_Grass_Reason_For_Inital_Cut);
                input_parameters.Add("@PCR_Yard_Maintenance_Bid_Recut", 1 + "#varchar#" + model.PCR_Yard_Maintenance_Bid_Recut);
                input_parameters.Add("@PCR_Yard_Maintenance_Reason_For_Recut", 1 + "#varchar#" + model.PCR_Yard_Maintenance_Reason_For_Recut);
                input_parameters.Add("@PCR_Yard_Maintenance_Trees_Cut_Back_Order", 1 + "#varchar#" + model.PCR_Yard_Maintenance_Trees_Cut_Back_Order);
                input_parameters.Add("@PCR_Yard_Maintenance_Arrival_Shrubs_Touching_House", 1 + "#varchar#" + model.PCR_Yard_Maintenance_Arrival_Shrubs_Touching_House);
                input_parameters.Add("@PCR_Yard_Maintenance_Arrival_Trees_Touching_House", 1 + "#varchar#" + model.PCR_Yard_Maintenance_Arrival_Trees_Touching_House);
                input_parameters.Add("@PCR_Yard_Maintenance_Depature_Trees", 1 + "#varchar#" + model.PCR_Yard_Maintenance_Depature_Trees);
                input_parameters.Add("@PCR_Yard_Maintenance_Were_Trimmed_Insurer_Guidlines", 1 + "#varchar#" + model.PCR_Yard_Maintenance_Were_Trimmed_Insurer_Guidlines);
                input_parameters.Add("@PCR_Yard_Maintenance_Grass_Maintained_No", 1 + "#varchar#" + model.PCR_Yard_Maintenance_Grass_Maintained_No);
                input_parameters.Add("@PCR_Yard_Maintenance_Bid_To_Shrubs_Dimensions_Length", 1 + "#varchar#" + model.PCR_Yard_Maintenance_Bid_To_Shrubs_Dimensions_Length);
                input_parameters.Add("@PCR_Yard_Maintenance_Bid_To_Shrubs_Dimensions_Width", 1 + "#varchar#" + model.PCR_Yard_Maintenance_Bid_To_Shrubs_Dimensions_Width);
                input_parameters.Add("@PCR_Yard_Maintenance_Bid_To_Shrubs_Dimensions_Height", 1 + "#varchar#" + model.PCR_Yard_Maintenance_Bid_To_Shrubs_Dimensions_Height);
                input_parameters.Add("@PCR_Yard_Maintenance_Bid_To_Shrubs_Quantity", 1 + "#varchar#" + model.PCR_Yard_Maintenance_Bid_To_Shrubs_Quantity);
                input_parameters.Add("@PCR_Yard_Maintenance_Bid_To_Shrubs_Unit_Price", 1 + "#varchar#" + model.PCR_Yard_Maintenance_Bid_To_Shrubs_Unit_Price);
                input_parameters.Add("@PCR_Yard_Maintenance_Bid_To_Shrubs_Bid_Amount", 1 + "#varchar#" + model.PCR_Yard_Maintenance_Bid_To_Shrubs_Bid_Amount);
                input_parameters.Add("@PCR_Yard_Maintenance_Bid_To_Shrubs_Location", 1 + "#varchar#" + model.PCR_Yard_Maintenance_Bid_To_Shrubs_Location);

                input_parameters.Add("@PCR_Yard_Maintenance_Bid_To_Shrubs_Reasons_Touching_House", 1 + "#bit#" + model.PCR_Yard_Maintenance_Bid_To_Shrubs_Reasons_Touching_House);
                input_parameters.Add("@PCR_Yard_Maintenance_Bid_To_Shrubs_Reasons_Touching_Other_Structure", 1 + "#bit#" + model.PCR_Yard_Maintenance_Bid_To_Shrubs_Reasons_Touching_Other_Structure);
                input_parameters.Add("@PCR_Yard_Maintenance_Bid_To_Shrubs_Reasons_Within_Street_View", 1 + "#bit#" + model.PCR_Yard_Maintenance_Bid_To_Shrubs_Reasons_Within_Street_View);
                input_parameters.Add("@PCR_Yard_Maintenance_Bid_To_Shrubs_Reasons_Affecting_Fencing", 1 + "#bit#" + model.PCR_Yard_Maintenance_Bid_To_Shrubs_Reasons_Affecting_Fencing);

                input_parameters.Add("@PCR_Yard_Maintenance_Bid_To_Shrubs_Causing_Damage", 1 + "#varchar#" + model.PCR_Yard_Maintenance_Bid_To_Shrubs_Causing_Damage);
                input_parameters.Add("@PCR_Yard_Maintenance_Bid_To_Shrubs_Describe", 1 + "#varchar#" + model.PCR_Yard_Maintenance_Bid_To_Shrubs_Describe);
                input_parameters.Add("@PCR_Yard_Maintenance_Bid_To_Trim_Dimensions_Length", 1 + "#varchar#" + model.PCR_Yard_Maintenance_Bid_To_Trim_Dimensions_Length);
                input_parameters.Add("@PCR_Yard_Maintenance_Bid_To_Trim_Dimensions_Width", 1 + "#varchar#" + model.PCR_Yard_Maintenance_Bid_To_Trim_Dimensions_Width);
                input_parameters.Add("@PCR_Yard_Maintenance_Bid_To_Trim_Dimensions_Height", 1 + "#varchar#" + model.PCR_Yard_Maintenance_Bid_To_Trim_Dimensions_Height);
                input_parameters.Add("@PCR_Yard_Maintenance_Bid_To_Trim_Quantity", 1 + "#varchar#" + model.PCR_Yard_Maintenance_Bid_To_Trim_Quantity);
                input_parameters.Add("@PCR_Yard_Maintenance_Bid_To_Trim_Unit_Price", 1 + "#varchar#" + model.PCR_Yard_Maintenance_Bid_To_Trim_Unit_Price);
                input_parameters.Add("@PCR_Yard_Maintenance_Bid_To_Trim_Bid_Amount", 1 + "#varchar#" + model.PCR_Yard_Maintenance_Bid_To_Trim_Bid_Amount);
                input_parameters.Add("@PCR_Yard_Maintenance_Bid_To_Trim_Location", 1 + "#varchar#" + model.PCR_Yard_Maintenance_Bid_To_Trim_Location);

                input_parameters.Add("@PCR_Yard_Maintenance_Bid_To_Trim_Reasons_Touching_House", 1 + "#bit#" + model.PCR_Yard_Maintenance_Bid_To_Trim_Reasons_Touching_House);
                input_parameters.Add("@PCR_Yard_Maintenance_Bid_To_Trim_Reasons_Touching_Other_Structure", 1 + "#bit#" + model.PCR_Yard_Maintenance_Bid_To_Trim_Reasons_Touching_Other_Structure);
                input_parameters.Add("@PCR_Yard_Maintenance_Bid_To_Trim_Reasons_Within_Street_View", 1 + "#bit#" + model.PCR_Yard_Maintenance_Bid_To_Trim_Reasons_Within_Street_View);
                input_parameters.Add("@PCR_Yard_Maintenance_Bid_To_Trim_Reasons_Affecting_Fencing", 1 + "#bit#" + model.PCR_Yard_Maintenance_Bid_To_Trim_Reasons_Affecting_Fencing);

                input_parameters.Add("@PCR_Yard_Maintenance_Bid_To_Trim_Causing_Damage", 1 + "#varchar#" + model.PCR_Yard_Maintenance_Bid_To_Trim_Causing_Damage);
                input_parameters.Add("@PCR_Yard_Maintenance_Bid_To_Trim_Describe", 1 + "#varchar#" + model.PCR_Yard_Maintenance_Bid_To_Trim_Describe);
                input_parameters.Add("@PCR_Yard_Grass_LotSize", 1 + "#varchar#" + model.PCR_Yard_Grass_LotSize);

                input_parameters.Add("@PCR_Yard_Maintenance_IsActive", 1 + "#bit#" + model.PCR_Yard_Maintenance_IsActive);
                input_parameters.Add("@PCR_Yard_Maintenance_IsDelete", 1 + "#bit#" + model.PCR_Yard_Maintenance_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@fwo_pkyeId", 1 + "#bigint#" + model.fwo_pkyeId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@PCR_Yard_Maintenance_pkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    pCR_Yard_Maintenance.PCR_Yard_Maintenance_pkeyId = "0";
                    pCR_Yard_Maintenance.Status = "0";
                    pCR_Yard_Maintenance.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    pCR_Yard_Maintenance.PCR_Yard_Maintenance_pkeyId = Convert.ToString(objData[0]);
                    pCR_Yard_Maintenance.Status = Convert.ToString(objData[1]);


                }
                objclntData.Add(pCR_Yard_Maintenance);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objclntData;



        }

        private DataSet GetPCRYardMaster(PCR_Yard_MaintenanceDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_PCR_Yard_Maintenance_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@PCR_Yard_Maintenance_pkeyId", 1 + "#bigint#" + model.PCR_Yard_Maintenance_pkeyId);
                input_parameters.Add("@PCR_Yard_Maintenance_WO_Id", 1 + "#bigint#" + model.PCR_Yard_Maintenance_WO_Id);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetPCYardDetails(PCR_Yard_MaintenanceDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetPCRYardMaster(model);

                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                }


                //var PCRYardMaintenance = DatatableToModel(ds.Tables[0]);
                //objDynamic.Add(PCRYardMaintenance);

                //if (ds.Tables.Count > 1)
                //{
                //    var history = DatatableToModel(ds.Tables[1]);
                //    objDynamic.Add(history);
                //}
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
        private List<PCR_Yard_MaintenanceDTO> DatatableToModel(DataTable dataTable)
        {

            var myEnumerableFeaprd = dataTable.AsEnumerable();
            List<PCR_Yard_MaintenanceDTO> PCRYardMaintenance =
               (from item in myEnumerableFeaprd
                select new PCR_Yard_MaintenanceDTO
                {

                    PCR_Yard_Maintenance_pkeyId = item.Field<Int64>("PCR_Yard_Maintenance_pkeyId"),
                    PCR_Yard_Maintenance_MasterId = item.Field<Int64>("PCR_Yard_Maintenance_MasterId"),
                    PCR_Yard_Maintenance_WO_Id = item.Field<Int64>("PCR_Yard_Maintenance_WO_Id"),
                    PCR_Yard_Maintenance_ValType = item.Field<int?>("PCR_Yard_Maintenance_ValType"),

                    PCR_Yard_Maintenance_Grass_Cut_Completed = item.Field<string>("PCR_Yard_Maintenance_Grass_Cut_Completed"),
                    PCR_Yard_Maintenance_Lot_Size = item.Field<string>("PCR_Yard_Maintenance_Lot_Size"),
                    PCR_Yard_Maintenance_Cuttable_Area = item.Field<string>("PCR_Yard_Maintenance_Cuttable_Area"),
                    PCR_Yard_Maintenance_Bit_To_Cut_Grass_Lot_Dimension_Lenght = item.Field<string>("PCR_Yard_Maintenance_Bit_To_Cut_Grass_Lot_Dimension_Lenght"),
                    PCR_Yard_Maintenance_Bit_To_Cut_Grass_Lot_Dimension_Width = item.Field<string>("PCR_Yard_Maintenance_Bit_To_Cut_Grass_Lot_Dimension_Width"),
                    PCR_Yard_Maintenance_Bit_To_Cut_Grass_Lot_Dimension_Height = item.Field<string>("PCR_Yard_Maintenance_Bit_To_Cut_Grass_Lot_Dimension_Height"),
                    PCR_Yard_Maintenance_Bit_To_Cut_Grass_Bid_For_Inital_Cut = item.Field<string>("PCR_Yard_Maintenance_Bit_To_Cut_Grass_Bid_For_Inital_Cut"),
                    PCR_Yard_Maintenance_Bit_To_Cut_Grass_Reason_For_Inital_Cut = item.Field<string>("PCR_Yard_Maintenance_Bit_To_Cut_Grass_Reason_For_Inital_Cut"),
                    PCR_Yard_Maintenance_Bid_Recut = item.Field<string>("PCR_Yard_Maintenance_Bid_Recut"),
                    PCR_Yard_Maintenance_Reason_For_Recut = item.Field<string>("PCR_Yard_Maintenance_Reason_For_Recut"),
                    PCR_Yard_Maintenance_Trees_Cut_Back_Order = item.Field<string>("PCR_Yard_Maintenance_Trees_Cut_Back_Order"),
                    PCR_Yard_Maintenance_Arrival_Shrubs_Touching_House = item.Field<string>("PCR_Yard_Maintenance_Arrival_Shrubs_Touching_House"),
                    PCR_Yard_Maintenance_Arrival_Trees_Touching_House = item.Field<string>("PCR_Yard_Maintenance_Arrival_Trees_Touching_House"),
                    PCR_Yard_Maintenance_Depature_Trees = item.Field<string>("PCR_Yard_Maintenance_Depature_Trees"),
                    PCR_Yard_Maintenance_Were_Trimmed_Insurer_Guidlines = item.Field<string>("PCR_Yard_Maintenance_Were_Trimmed_Insurer_Guidlines"),



                    PCR_Yard_Maintenance_Grass_Maintained_No = item.Field<string>("PCR_Yard_Maintenance_Grass_Maintained_No"),

                    PCR_Yard_Maintenance_Bid_To_Shrubs_Dimensions_Length = item.Field<string>("PCR_Yard_Maintenance_Bid_To_Shrubs_Dimensions_Length"),
                    PCR_Yard_Maintenance_Bid_To_Shrubs_Dimensions_Width = item.Field<string>("PCR_Yard_Maintenance_Bid_To_Shrubs_Dimensions_Width"),
                    PCR_Yard_Maintenance_Bid_To_Shrubs_Dimensions_Height = item.Field<string>("PCR_Yard_Maintenance_Bid_To_Shrubs_Dimensions_Height"),
                    PCR_Yard_Maintenance_Bid_To_Shrubs_Quantity = item.Field<string>("PCR_Yard_Maintenance_Bid_To_Shrubs_Quantity"),
                    PCR_Yard_Maintenance_Bid_To_Shrubs_Unit_Price = item.Field<string>("PCR_Yard_Maintenance_Bid_To_Shrubs_Unit_Price"),
                    PCR_Yard_Maintenance_Bid_To_Shrubs_Bid_Amount = item.Field<string>("PCR_Yard_Maintenance_Bid_To_Shrubs_Bid_Amount"),
                    PCR_Yard_Maintenance_Bid_To_Shrubs_Location = item.Field<string>("PCR_Yard_Maintenance_Bid_To_Shrubs_Location"),
                    PCR_Yard_Maintenance_Bid_To_Shrubs_Reasons_Touching_House = item.Field<Boolean?>("PCR_Yard_Maintenance_Bid_To_Shrubs_Reasons_Touching_House"),
                    PCR_Yard_Maintenance_Bid_To_Shrubs_Reasons_Touching_Other_Structure = item.Field<Boolean?>("PCR_Yard_Maintenance_Bid_To_Shrubs_Reasons_Touching_Other_Structure"),
                    PCR_Yard_Maintenance_Bid_To_Shrubs_Reasons_Within_Street_View = item.Field<Boolean?>("PCR_Yard_Maintenance_Bid_To_Shrubs_Reasons_Within_Street_View"),
                    PCR_Yard_Maintenance_Bid_To_Shrubs_Reasons_Affecting_Fencing = item.Field<Boolean?>("PCR_Yard_Maintenance_Bid_To_Shrubs_Reasons_Affecting_Fencing"),
                    PCR_Yard_Maintenance_Bid_To_Shrubs_Causing_Damage = item.Field<string>("PCR_Yard_Maintenance_Bid_To_Shrubs_Causing_Damage"),
                    PCR_Yard_Maintenance_Bid_To_Shrubs_Describe = item.Field<string>("PCR_Yard_Maintenance_Bid_To_Shrubs_Describe"),

                    PCR_Yard_Maintenance_Bid_To_Trim_Dimensions_Length = item.Field<string>("PCR_Yard_Maintenance_Bid_To_Trim_Dimensions_Length"),
                    PCR_Yard_Maintenance_Bid_To_Trim_Dimensions_Width = item.Field<string>("PCR_Yard_Maintenance_Bid_To_Trim_Dimensions_Width"),
                    PCR_Yard_Maintenance_Bid_To_Trim_Dimensions_Height = item.Field<string>("PCR_Yard_Maintenance_Bid_To_Trim_Dimensions_Height"),
                    PCR_Yard_Maintenance_Bid_To_Trim_Quantity = item.Field<string>("PCR_Yard_Maintenance_Bid_To_Trim_Quantity"),
                    PCR_Yard_Maintenance_Bid_To_Trim_Unit_Price = item.Field<string>("PCR_Yard_Maintenance_Bid_To_Trim_Unit_Price"),
                    PCR_Yard_Maintenance_Bid_To_Trim_Bid_Amount = item.Field<string>("PCR_Yard_Maintenance_Bid_To_Trim_Bid_Amount"),
                    PCR_Yard_Maintenance_Bid_To_Trim_Location = item.Field<string>("PCR_Yard_Maintenance_Bid_To_Trim_Location"),
                    PCR_Yard_Maintenance_Bid_To_Trim_Reasons_Touching_House = item.Field<Boolean?>("PCR_Yard_Maintenance_Bid_To_Trim_Reasons_Touching_House"),
                    PCR_Yard_Maintenance_Bid_To_Trim_Reasons_Touching_Other_Structure = item.Field<Boolean?>("PCR_Yard_Maintenance_Bid_To_Trim_Reasons_Touching_Other_Structure"),
                    PCR_Yard_Maintenance_Bid_To_Trim_Reasons_Within_Street_View = item.Field<Boolean?>("PCR_Yard_Maintenance_Bid_To_Trim_Reasons_Within_Street_View"),
                    PCR_Yard_Maintenance_Bid_To_Trim_Reasons_Affecting_Fencing = item.Field<Boolean?>("PCR_Yard_Maintenance_Bid_To_Trim_Reasons_Affecting_Fencing"),
                    PCR_Yard_Maintenance_Bid_To_Trim_Causing_Damage = item.Field<string>("PCR_Yard_Maintenance_Bid_To_Trim_Causing_Damage"),
                    PCR_Yard_Maintenance_Bid_To_Trim_Describe = item.Field<string>("PCR_Yard_Maintenance_Bid_To_Trim_Describe"),
                    PCR_Yard_Grass_LotSize = item.Field<string>("PCR_Yard_Grass_LotSize"),
                    PCR_Yard_Maintenance_IsActive = item.Field<Boolean?>("PCR_Yard_Maintenance_IsActive"),


                }).ToList();
            return PCRYardMaintenance;
        }
    }
}