﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class Task_Company_TBLData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public List<dynamic> AddTaskCompanyData(Task_Company_TBLDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
          
            string insertProcedure = "[CreateUpdate_Task_Company_TBL]";
           
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@TC_pkeyID", 1 + "#bigint#" + model.TC_pkeyID);
                input_parameters.Add("@TC_Task_pkeyID", 1 + "#bigint#" + model.TC_Task_pkeyID);
                input_parameters.Add("@TC_Task_sett_ID", 1 + "#bigint#" + model.TC_Task_sett_ID);
                input_parameters.Add("@TC_Task_sett_pkeyID", 1 + "#bigint#" + model.TC_Task_sett_pkeyID);
                input_parameters.Add("@TC_IsActive", 1 + "#bit#" + model.TC_IsActive);
                input_parameters.Add("@TC_IsDelete", 1 + "#bit#" + model.TC_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@TC_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
               
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;



        }
        public List<dynamic> AddTaskContractorData(Task_Contractor_TBLDTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_Task_Contractor_TBL]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@TCon_pkeyID", 1 + "#bigint#" + model.TCon_pkeyID);
                input_parameters.Add("@TCon_Task_pkeyID", 1 + "#bigint#" + model.TCon_Task_pkeyID);
                input_parameters.Add("@TCon_Task_sett_ID", 1 + "#bigint#" + model.TCon_Task_sett_ID);
                input_parameters.Add("@TCon_Task_sett_pkeyID", 1 + "#bigint#" + model.TCon_Task_sett_pkeyID);
                input_parameters.Add("@TCon_IsActive", 1 + "#bit#" + model.TCon_IsActive);
                input_parameters.Add("@TCon_IsDelete", 1 + "#bit#" + model.TCon_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@TCon_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;



        }
        public List<dynamic> AddTaskCountryData(Task_Country_TBLDTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_Task_Country_TBL]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@TCoun_pkeyID", 1 + "#bigint#" + model.TCoun_pkeyID);
                input_parameters.Add("@TCoun_Task_pkeyID", 1 + "#bigint#" + model.TCoun_Task_pkeyID);
                input_parameters.Add("@TCoun_Task_sett_ID", 1 + "#bigint#" + model.TCoun_Task_sett_ID);
                input_parameters.Add("@TCoun_Task_sett_pkeyID", 1 + "#bigint#" + model.TCoun_Task_sett_pkeyID);
                input_parameters.Add("@TCoun_IsActive", 1 + "#bit#" + model.TCoun_IsActive);
                input_parameters.Add("@TCoun_IsDelete", 1 + "#bit#" + model.TCoun_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@TCoun_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;



        }
        public List<dynamic> AddTaskCutomerData(Task_Cutomer_TBLDTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_Task_Cutomer_TBL]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@TCust_pkeyID", 1 + "#bigint#" + model.TCust_pkeyID);
                input_parameters.Add("@TCust_Task_pkeyID", 1 + "#bigint#" + model.TCust_Task_pkeyID);
                input_parameters.Add("@TCust_Task_sett_ID", 1 + "#bigint#" + model.TCust_Task_sett_ID);
                input_parameters.Add("@TCust_Task_sett_pkeyID", 1 + "#bigint#" + model.TCust_Task_sett_pkeyID);
                input_parameters.Add("@TCust_IsActive", 1 + "#bit#" + model.TCust_IsActive);
                input_parameters.Add("@TCust_IsDelete", 1 + "#bit#" + model.TCust_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@TCust_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;



        }
        public List<dynamic> AddTaskGroupData(Task_Group_TBLDTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_Task_Group_TBL]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@TG_pkeyID", 1 + "#bigint#" + model.TG_pkeyID);
                input_parameters.Add("@TG_Task_pkeyID", 1 + "#bigint#" + model.TG_Task_pkeyID);
                input_parameters.Add("@TG_Task_sett_ID", 1 + "#bigint#" + model.TG_Task_sett_ID);
                input_parameters.Add("@TG_Task_sett_pkeyID", 1 + "#bigint#" + model.TG_Task_sett_pkeyID);
                input_parameters.Add("@TG_IsActive", 1 + "#bit#" + model.TG_IsActive);
                input_parameters.Add("@TG_IsDelete", 1 + "#bit#" + model.TG_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@TG_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;



        }
        public List<dynamic> AddTaskLoanData(Task_Loan_TBLDTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_Task_Loan_TBL]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@TLoan_pkeyID", 1 + "#bigint#" + model.TLoan_pkeyID);
                input_parameters.Add("@TLoan_Task_pkeyID", 1 + "#bigint#" + model.TLoan_Task_pkeyID);
                input_parameters.Add("@TLoan_Task_sett_ID", 1 + "#bigint#" + model.TLoan_Task_sett_ID);
                input_parameters.Add("@TLoan_Task_sett_pkeyID", 1 + "#bigint#" + model.TLoan_Task_sett_pkeyID);
                input_parameters.Add("@TLoan_IsActive", 1 + "#bit#" + model.TLoan_IsActive);
                input_parameters.Add("@TLoan_IsDelete", 1 + "#bit#" + model.TLoan_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@TLoan_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;



        }
        public List<dynamic> AddTaskStateData(Task_State_TBLDTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_Task_State_TBL]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@TS_pkeyID", 1 + "#bigint#" + model.TS_pkeyID);
                input_parameters.Add("@TS_Task_pkeyID", 1 + "#bigint#" + model.TS_Task_pkeyID);
                input_parameters.Add("@TS_Task_sett_ID", 1 + "#bigint#" + model.TS_Task_sett_ID);
                input_parameters.Add("@TS_Task_sett_pkeyID", 1 + "#bigint#" + model.TS_Task_sett_pkeyID);
                input_parameters.Add("@TS_IsActive", 1 + "#bit#" + model.TS_IsActive);
                input_parameters.Add("@TS_IsDelete", 1 + "#bit#" + model.TS_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@TS_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

            }
            return objData;
        }
        public List<dynamic> AddTaskWorkTypeData(Task_WorkType_TBLDTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_Task_WorkType_TBL]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@TW_pkeyID", 1 + "#bigint#" + model.TW_pkeyID);
                input_parameters.Add("@TW_Task_pkeyID", 1 + "#bigint#" + model.TW_Task_pkeyID);
                input_parameters.Add("@TW_Task_sett_ID", 1 + "#bigint#" + model.TW_Task_sett_ID);
                input_parameters.Add("@TW_Task_sett_pkeyID", 1 + "#bigint#" + model.TW_Task_sett_pkeyID);
                input_parameters.Add("@TW_IsActive", 1 + "#bit#" + model.TW_IsActive);
                input_parameters.Add("@TW_IsDelete", 1 + "#bit#" + model.TW_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@TW_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        public List<dynamic> AddTaskLotPriceData(Task_LotPrice_TBLDTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_Task_LotPrice_TBL]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@TLP_pkeyID", 1 + "#bigint#" + model.TLP_pkeyID);
                input_parameters.Add("@TLP_Task_pkeyID", 1 + "#bigint#" + model.TLP_Task_pkeyID);
                input_parameters.Add("@TLP_Task_sett_ID", 1 + "#bigint#" + model.TLP_Task_sett_ID);
                input_parameters.Add("@TLP_Task_sett_pkeyID", 1 + "#bigint#" + model.TLP_Task_sett_pkeyID);
                input_parameters.Add("@TLP_IsActive", 1 + "#bit#" + model.TLP_IsActive);
                input_parameters.Add("@TLP_IsDelete", 1 + "#bit#" + model.TLP_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@TLP_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

    }
}