﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class PCR_Utilities_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public List<dynamic> AddPCR_Utilities_Data(PCR_Utilities_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objclntData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_PCR_Utilities_Master]";
            PCR_Utilities_Master pCR_Utilities_Master = new PCR_Utilities_Master();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@PCR_Utilities_pkeyId", 1 + "#bigint#" + model.PCR_Utilities_pkeyId);
                input_parameters.Add("@PCR_Utilities_MasterId", 1 + "#bigint#" + model.PCR_Utilities_MasterId);
                input_parameters.Add("@PCR_Utilities_WO_Id", 1 + "#bigint#" + model.PCR_Utilities_WO_Id);
                input_parameters.Add("@PCR_Utilities_ValType", 1 + "#int#" + model.PCR_Utilities_ValType);

                input_parameters.Add("@PCR_Utilities_On_Arrival_Water", 1 + "#varchar#" + model.PCR_Utilities_On_Arrival_Water);
                input_parameters.Add("@PCR_Utilities_On_Departure_Water", 1 + "#varchar#" + model.PCR_Utilities_On_Departure_Water);
                input_parameters.Add("@PCR_Utilities_On_Arrival_Gas", 1 + "#varchar#" + model.PCR_Utilities_On_Arrival_Gas);
                input_parameters.Add("@PCR_Utilities_On_Departure_Gas", 1 + "#varchar#" + model.PCR_Utilities_On_Departure_Gas);
                input_parameters.Add("@PCR_Utilities_On_Arrival_Electric", 1 + "#varchar#" + model.PCR_Utilities_On_Arrival_Electric);
                input_parameters.Add("@PCR_Utilities_On_Departure_Electric", 1 + "#varchar#" + model.PCR_Utilities_On_Departure_Electric);

                input_parameters.Add("@PCR_Utilities_Sump_Pump", 1 + "#varchar#" + model.PCR_Utilities_Sump_Pump);
                input_parameters.Add("@PCR_Utilities_Sump_Pump_Commend", 1 + "#varchar#" + model.PCR_Utilities_Sump_Pump_Commend);
                input_parameters.Add("@PCR_Utilities_Sump_Pump_Sump_Test", 1 + "#varchar#" + model.PCR_Utilities_Sump_Pump_Sump_Test);

                input_parameters.Add("@PCR_Utilities_Main_Breaker_And_Operational", 1 + "#varchar#" + model.PCR_Utilities_Main_Breaker_And_Operational);
                input_parameters.Add("@PCR_Utilities_Sump_Pump_Missing_Bid_To_Replace", 1 + "#varchar#" + model.PCR_Utilities_Sump_Pump_Missing_Bid_To_Replace);
                input_parameters.Add("@PCR_Utilities_Transferred_Activated", 1 + "#varchar#" + model.PCR_Utilities_Transferred_Activated);
                input_parameters.Add("@PCR_Utilities_Reason_UtilitiesNot_Transferred", 1 + "#varchar#" + model.PCR_Utilities_Reason_UtilitiesNot_Transferred);
                input_parameters.Add("@PCR_Utilities_Reason_UtilitiesNot_Transferred_Other_Notes", 1 + "#varchar#" + model.PCR_Utilities_Reason_UtilitiesNot_Transferred_Other_Notes);


                input_parameters.Add("@PCR_Utilities_Transferred_Water_Co_Name", 1 + "#varchar#" + model.PCR_Utilities_Transferred_Water_Co_Name);
                input_parameters.Add("@PCR_Utilities_Transferred_Water_Address", 1 + "#varchar#" + model.PCR_Utilities_Transferred_Water_Address);
                input_parameters.Add("@PCR_Utilities_Transferred_Water_Phone", 1 + "#varchar#" + model.PCR_Utilities_Transferred_Water_Phone);
                input_parameters.Add("@PCR_Utilities_Transferred_Water_Acct", 1 + "#varchar#" + model.PCR_Utilities_Transferred_Water_Acct);
                input_parameters.Add("@PCR_Utilities_Transferred_Gas_Co_Name", 1 + "#varchar#" + model.PCR_Utilities_Transferred_Gas_Co_Name);
                input_parameters.Add("@PCR_Utilities_Transferred_Gas_Address", 1 + "#varchar#" + model.PCR_Utilities_Transferred_Gas_Address);
                input_parameters.Add("@PCR_Utilities_Transferred_Gas_Phone", 1 + "#varchar#" + model.PCR_Utilities_Transferred_Gas_Phone);
                input_parameters.Add("@PCR_Utilities_Transferred_Gas_Acct", 1 + "#varchar#" + model.PCR_Utilities_Transferred_Gas_Acct);
                input_parameters.Add("@PCR_Utilities_Transferred_Electric_Co_Name", 1 + "#varchar#" + model.PCR_Utilities_Transferred_Electric_Co_Name);
                input_parameters.Add("@PCR_Utilities_Transferred_Electric_Address", 1 + "#varchar#" + model.PCR_Utilities_Transferred_Electric_Address);
                input_parameters.Add("@PCR_Utilities_Transferred_Electric_Phone", 1 + "#varchar#" + model.PCR_Utilities_Transferred_Electric_Phone);
                input_parameters.Add("@PCR_Utilities_Transferred_Electric_Acct", 1 + "#varchar#" + model.PCR_Utilities_Transferred_Electric_Acct);

                input_parameters.Add("@PCR_Utilities_IsActive", 1 + "#varchar#" + model.PCR_Utilities_IsActive);
                input_parameters.Add("@PCR_Utilities_IsDelete", 1 + "#varchar#" + model.PCR_Utilities_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@fwo_pkyeId", 1 + "#bigint#" + model.fwo_pkyeId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@PCR_Utilities_pkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    pCR_Utilities_Master.PCR_Utilities_pkeyId = "0";
                    pCR_Utilities_Master.Status = "0";
                    pCR_Utilities_Master.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    pCR_Utilities_Master.PCR_Utilities_pkeyId = Convert.ToString(objData[0]);
                    pCR_Utilities_Master.Status = Convert.ToString(objData[1]);


                }
                objclntData.Add(pCR_Utilities_Master);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objclntData;



        }

        private DataSet GetPCRUtilitiesMaster(PCR_Utilities_MasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_PCR_Utilities_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@PCR_Utilities_pkeyId", 1 + "#bigint#" + model.PCR_Utilities_pkeyId);
                input_parameters.Add("@PCR_Utilities_WO_Id", 1 + "#bigint#" + model.PCR_Utilities_WO_Id);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetPCRUtilitisDetails(PCR_Utilities_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetPCRUtilitiesMaster(model);

                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                }

                //var PCRUtility = DatatableToModel(ds.Tables[0]);
                //objDynamic.Add(PCRUtility);

                //if (ds.Tables.Count > 1)
                //{
                //    var history = DatatableToModel(ds.Tables[1]);
                //    objDynamic.Add(history);
                //}
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
        private List<PCR_Utilities_MasterDTO> DatatableToModel(DataTable dataTable)
        {

            var myEnumerableFeaprd = dataTable.AsEnumerable();
            List<PCR_Utilities_MasterDTO> PCRUtility =
               (from item in myEnumerableFeaprd
                select new PCR_Utilities_MasterDTO
                {
                    PCR_Utilities_pkeyId = item.Field<Int64>("PCR_Utilities_pkeyId"),
                    PCR_Utilities_MasterId = item.Field<Int64>("PCR_Utilities_MasterId"),
                    PCR_Utilities_WO_Id = item.Field<Int64>("PCR_Utilities_WO_Id"),
                    PCR_Utilities_ValType = item.Field<int>("PCR_Utilities_ValType"),

                    PCR_Utilities_On_Arrival_Water = item.Field<String>("PCR_Utilities_On_Arrival_Water"),
                    PCR_Utilities_On_Arrival_Gas = item.Field<String>("PCR_Utilities_On_Arrival_Gas"),
                    PCR_Utilities_On_Arrival_Electric = item.Field<String>("PCR_Utilities_On_Arrival_Electric"),
                    PCR_Utilities_On_Departure_Water = item.Field<String>("PCR_Utilities_On_Departure_Water"),
                    PCR_Utilities_On_Departure_Gas = item.Field<String>("PCR_Utilities_On_Departure_Gas"),
                    PCR_Utilities_On_Departure_Electric = item.Field<String>("PCR_Utilities_On_Departure_Electric"),

                    PCR_Utilities_Sump_Pump = item.Field<String>("PCR_Utilities_Sump_Pump"),
                    PCR_Utilities_Sump_Pump_Commend = item.Field<String>("PCR_Utilities_Sump_Pump_Commend"),
                    PCR_Utilities_Sump_Pump_Sump_Test = item.Field<String>("PCR_Utilities_Sump_Pump_Sump_Test"),
                    PCR_Utilities_Main_Breaker_And_Operational = item.Field<String>("PCR_Utilities_Main_Breaker_And_Operational"),
                    PCR_Utilities_Sump_Pump_Missing_Bid_To_Replace = item.Field<String>("PCR_Utilities_Sump_Pump_Missing_Bid_To_Replace"),
                    PCR_Utilities_Transferred_Activated = item.Field<String>("PCR_Utilities_Transferred_Activated"),
                    PCR_Utilities_Reason_UtilitiesNot_Transferred = item.Field<String>("PCR_Utilities_Reason_UtilitiesNot_Transferred"),
                    PCR_Utilities_Reason_UtilitiesNot_Transferred_Other_Notes = item.Field<String>("PCR_Utilities_Reason_UtilitiesNot_Transferred_Other_Notes"),

                    PCR_Utilities_Transferred_Water_Co_Name = item.Field<String>("PCR_Utilities_Transferred_Water_Co_Name"),
                    PCR_Utilities_Transferred_Water_Address = item.Field<String>("PCR_Utilities_Transferred_Water_Address"),
                    PCR_Utilities_Transferred_Water_Phone = item.Field<String>("PCR_Utilities_Transferred_Water_Phone"),
                    PCR_Utilities_Transferred_Water_Acct = item.Field<String>("PCR_Utilities_Transferred_Water_Acct"),
                    PCR_Utilities_Transferred_Gas_Co_Name = item.Field<String>("PCR_Utilities_Transferred_Gas_Co_Name"),
                    PCR_Utilities_Transferred_Gas_Address = item.Field<String>("PCR_Utilities_Transferred_Gas_Address"),
                    PCR_Utilities_Transferred_Gas_Phone = item.Field<String>("PCR_Utilities_Transferred_Gas_Phone"),
                    PCR_Utilities_Transferred_Gas_Acct = item.Field<String>("PCR_Utilities_Transferred_Gas_Acct"),
                    PCR_Utilities_Transferred_Electric_Co_Name = item.Field<String>("PCR_Utilities_Transferred_Electric_Co_Name"),
                    PCR_Utilities_Transferred_Electric_Address = item.Field<String>("PCR_Utilities_Transferred_Electric_Address"),
                    PCR_Utilities_Transferred_Electric_Phone = item.Field<String>("PCR_Utilities_Transferred_Electric_Phone"),
                    PCR_Utilities_Transferred_Electric_Acct = item.Field<String>("PCR_Utilities_Transferred_Electric_Acct"),

                    PCR_Utilities_IsActive = item.Field<Boolean?>("PCR_Utilities_IsActive"),


                }).ToList();

            return PCRUtility;
        }
    }
}