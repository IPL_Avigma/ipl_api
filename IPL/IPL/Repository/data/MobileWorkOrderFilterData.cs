﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class MobileWorkOrderFilterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        private DataSet GetHistoryDataForMobile(MobileWorkOrderFilterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[GetHistoryDataForMobile]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@workOrder_ID", 1 + "#bigint#" + model.workOrder_ID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@WhereClause", 1 + "#nvarchar#" + model.WhereClause);
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetHistoryDataForMobileDetails(MobileWorkOrderFilterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();

            try
            {
                string strWhere = string.Empty;
                if (!string.IsNullOrWhiteSpace(model.IPLNO))
                {
                    strWhere = " And wo.IPLNo  =  '" + model.IPLNO + "'  ";
                }
                 
                if (!string.IsNullOrWhiteSpace(model.WorkOrderNumber))
                {
                    strWhere = strWhere != null ?  strWhere + " And wo.workOrderNumber  =  '" + model.WorkOrderNumber + "'" : " And wo.workOrderNumber  =  '" + model.WorkOrderNumber + "'";
                }
                if (!string.IsNullOrWhiteSpace(model.Address))
                {
                    strWhere = strWhere != null ? strWhere + " And wo.address1  Like  '%" + model.Address + "%' ": " And wo.address1  Like  '%" + model.Address + "%' ";
                }
                if (!string.IsNullOrWhiteSpace(model.City))
                {
                    strWhere = strWhere != null ? strWhere + " And wo.city  Like  '%" + model.City + "%' " : " And wo.city  Like  '%" + model.City + "%' ";
                }
                if (!string.IsNullOrWhiteSpace(model.State))
                {
                    //strWhere = strWhere != null ? strWhere + " And ST.IPL_StateName  Like  '%" + model.State + "%' " : " And ST.IPL_StateName  Like  '%" + model.State + "%' ";
                    strWhere = strWhere != null ? strWhere + " And wo.state  =  '" + model.State + "' " : " And wo.state  =  '" + model.State + "' ";
                }
                if (model.Zip != null && model.Zip !=0)
                {
                    strWhere = strWhere != null ? strWhere + " And wo.zip  =  '" + model.Zip + "'" : " And wo.zip  =  '" + model.Zip + "'";
                }
                model.WhereClause = strWhere;
                DataSet ds = GetHistoryDataForMobile(model);
                if (ds.Tables.Count > 0)
                {
                    for (int i = 0; i < ds.Tables.Count; i++)
                    {
                        objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                    }
                }

                if (model.Type == 2)
                {
                    GetClientResultPhoto_DTO getClientResultPhoto_DTO = new GetClientResultPhoto_DTO();
                    Client_Result_PhotoData client_Result_PhotoData = new Client_Result_PhotoData();
                    getClientResultPhoto_DTO.Client_Result_Photo_Wo_ID = model.workOrder_ID;
                    getClientResultPhoto_DTO.Type = 1;
                    objDynamic.Add(client_Result_PhotoData.GetClientResultPhotoDetails(getClientResultPhoto_DTO, 2));
                   
                }

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objDynamic;
        }
    }
}