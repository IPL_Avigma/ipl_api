﻿using Avigma.Repository.Lib;
using IPLApp.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPLApp.Repository.data
{
    public class WorkOrderColumnData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();


        private DataSet GetWorkOrderColumnData()
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_WorkOrderColumn]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();


                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetWorkOrderColumnDetails()
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetWorkOrderColumnData();

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<WorkOrderColumnDTO> CompanyDetails =
                   (from item in myEnumerableFeaprd
                    select new WorkOrderColumnDTO
                    {
                        Wo_Column_Name = item.Field<String>("Wo_Column_Name"),
                        Wo_Column_Table_ColumnName = item.Field<String>("Wo_Column_Table_ColumnName"),
                        Wo_Column_IsActive = item.Field<Boolean?>("Wo_Column_IsActive"),

                    }).ToList();

                objDynamic.Add(CompanyDetails);

              
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}