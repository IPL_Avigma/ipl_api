﻿using Avigma.Repository.Lib;
using IPL.Models;
using IPLApp.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class TaskBidForPhotoData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        private DataSet GetbitfortaskMaster(TaskBidforimg model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Invoice_Bidfortask_bak]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Task_Bid_WO_ID", 1 + "#bigint#" + model.Task_Bid_WO_ID);
                input_parameters.Add("@Task_Bid_TaskID", 1 + "#bigint#" + model.Task_Bid_TaskID);


                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

            }



            return ds;
        }


        private DataSet GetbitfortaskMaster_bak(TaskBidforimg model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Invoice_Bidfortask]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Task_Bid_WO_ID", 1 + "#bigint#" + model.Task_Bid_WO_ID);
                input_parameters.Add("@Task_Bid_TaskID", 1 + "#bigint#" + model.Task_Bid_TaskID);


                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }


        private DataSet Get_Invoice_Bidfortask_MobApp(TaskBidforimg model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Invoice_Bidfortask_MobApp]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Task_Bid_WO_ID", 1 + "#bigint#" + model.Task_Bid_WO_ID);
                input_parameters.Add("@Task_Bid_TaskID", 1 + "#bigint#" + model.Task_Bid_TaskID);
                input_parameters.Add("@Task_Bid_Sys_Type", 1 + "#int#" + model.Task_Bid_Sys_Type);
                input_parameters.Add("@UserID", 1 + "#int#" + model.UserID);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        #region comment
        //public List<dynamic> GetInvoiceBidfortaskMobApp(TaskBidforimg model)
        //{
        //    List<dynamic> objDynamic = new List<dynamic>();
        //    try
        //    {
        //        WorkOrderMaster_Details_MobileApp_Data workOrderMaster_Details_MobileApp_Data = new WorkOrderMaster_Details_MobileApp_Data();
        //        workOrderxDTO workOrderxDTO = new workOrderxDTO();
        //        workOrderxDTO.workOrder_ID = Convert.ToInt64(model.Task_Bid_WO_ID);
        //        workOrderxDTO.Type = 8;
        //        workOrderxDTO.UserID = model.UserID;
        //        objDynamic.Add(workOrderMaster_Details_MobileApp_Data.GetWorkOrderDataDetailsForMobile(workOrderxDTO));

        //        //Notification coden to  be added here

        //        DataSet ds = Get_Invoice_Bidfortask_MobApp(model);

        //        #region Comment
        //        //var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
        //        //List<TaskBidforimg> taskPhoto =
        //        //   (from item in myEnumerableFeaprd
        //        //    select new TaskBidforimg
        //        //    {
        //        //        Task_Bid_TaskID = item.Field<Int64>("Task_Bid_TaskID"),
        //        //        Task_Photo_Label_Name = item.Field<String>("Task_Photo_Label_Name"),
        //        //        Task_Bid_pkeyID = item.Field<Int64?>("Task_Bid_pkeyID"),
        //        //        ButtonName1 = item.Field<String>("ButtonName1"),
        //        //        ButtonName2 = item.Field<String>("ButtonName2"),
        //        //        ButtonName3 = item.Field<String>("ButtonName3"),




        //        //    }).ToList();
        //        #endregion

        //        List<TaskInvoiceBid_MobApp> lsttaskBidforimg = new List<TaskInvoiceBid_MobApp>();
        //        if (ds.Tables.Count > 0 && ds.Tables[0] != null)           {
        //            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
        //            {
        //                TaskInvoiceBid_MobApp taskBidforimg = new TaskInvoiceBid_MobApp();
        //                if (!string.IsNullOrEmpty(ds.Tables[0].Rows[i]["Task_Bid_TaskID"].ToString()))
        //                {
        //                    taskBidforimg.Task_Bid_TaskID = Convert.ToInt64(ds.Tables[0].Rows[i]["Task_Bid_TaskID"].ToString());
        //                }
        //                taskBidforimg.ButtonName1 = ds.Tables[0].Rows[i]["ButtonName1"].ToString();
        //                taskBidforimg.ButtonName2 = ds.Tables[0].Rows[i]["ButtonName2"].ToString();
        //                taskBidforimg.ButtonName3 = ds.Tables[0].Rows[i]["ButtonName3"].ToString();
        //                taskBidforimg.Comments = ds.Tables[0].Rows[i]["Comments"].ToString();
        //                taskBidforimg.TMF_Task_FileName = ds.Tables[0].Rows[i]["TMF_Task_FileName"].ToString();
        //                taskBidforimg.TMF_Task_localPath = ds.Tables[0].Rows[i]["TMF_Task_localPath"].ToString();
        //                //if (!string.IsNullOrEmpty(ds.Tables[0].Rows[i]["Countval"].ToString()))
        //                //{
        //                //    taskBidforimg.Countval = Convert.ToInt32(ds.Tables[0].Rows[i]["Countval"].ToString());
        //                //}
        //                taskBidforimg.Task_Label_Name = ds.Tables[0].Rows[i]["Task_Photo_Label_Name"].ToString();
        //                switch (taskBidforimg.ButtonName1)
        //                {
        //                    case "Bid":
        //                        {
        //                            taskBidforimg.Task_Photo_Label_Name = ds.Tables[0].Rows[i]["Task_Photo_Label_Name"].ToString() + " (0)";
        //                            break;
        //                        }
        //                    case "Before":
        //                        {
        //                            taskBidforimg.Task_Photo_Label_Name = ds.Tables[0].Rows[i]["Task_Photo_Label_Name"].ToString() + "  B(0) D(0) A(0)";
        //                            break;
        //                        }
        //                    case "Damage":
        //                        {
        //                            taskBidforimg.Task_Photo_Label_Name = ds.Tables[0].Rows[i]["Task_Photo_Label_Name"].ToString() + " (0)";
        //                            break;
        //                        }
        //                    case "Inspection":
        //                        {
        //                            taskBidforimg.Task_Photo_Label_Name = ds.Tables[0].Rows[i]["Task_Photo_Label_Name"].ToString() + " (0)";
        //                            break;
        //                        }
        //                    case "Label":
        //                        {
        //                            taskBidforimg.Task_Photo_Label_Name = ds.Tables[0].Rows[i]["Task_Photo_Label_Name"].ToString() + " (0)";
        //                            break;
        //                        }

        //                }

        //                if (!string.IsNullOrEmpty(ds.Tables[0].Rows[i]["Task_Bid_pkeyID"].ToString()))
        //                {
        //                    taskBidforimg.Task_Bid_pkeyID = Convert.ToInt64(ds.Tables[0].Rows[i]["Task_Bid_pkeyID"].ToString());
        //                }

        //                taskBidforimg.Task_Bid_Qty = ds.Tables[0].Rows[i]["Qty"].ToString();
        //                taskBidforimg.IsReject = false;
        //                if (!string.IsNullOrEmpty(ds.Tables[0].Rows[i]["IsReject"].ToString()))
        //                {
        //                    taskBidforimg.IsReject = Convert.ToBoolean(ds.Tables[0].Rows[i]["IsReject"].ToString());
        //                }


        //                if (!string.IsNullOrEmpty(ds.Tables[0].Rows[i]["Cont_Price"].ToString()))

        //                {
        //                    taskBidforimg.Task_Bid_Cont_Price = Convert.ToDecimal(ds.Tables[0].Rows[i]["Cont_Price"].ToString());
        //                }
        //                if (!string.IsNullOrEmpty(ds.Tables[0].Rows[i]["Cont_Total"].ToString()))
        //                {
        //                    taskBidforimg.Task_Bid_Cont_Total = Convert.ToDecimal(ds.Tables[0].Rows[i]["Cont_Total"].ToString());
        //                }
        //                if (!string.IsNullOrEmpty(ds.Tables[0].Rows[i]["Clnt_Price"].ToString()))
        //                {
        //                    taskBidforimg.Task_Bid_Clnt_Price = Convert.ToDecimal(ds.Tables[0].Rows[i]["Clnt_Price"].ToString());
        //                }
        //                if (!string.IsNullOrEmpty(ds.Tables[0].Rows[i]["Clnt_Total"].ToString()))
        //                {
        //                    taskBidforimg.Task_Bid_Clnt_Total = Convert.ToDecimal(ds.Tables[0].Rows[i]["Clnt_Total"].ToString());
        //                }


        //                int Status_Type = 0;
        //                int intBcount = 0, intDcount = 0, intAcount = 0, intcount = 0;
        //                string strBefore = string.Empty, strAfter = string.Empty, strDuring = string.Empty;
        //                if (ds.Tables.Count > 1)
        //                {
        //                    if (ds.Tables[1].Rows.Count == 0)
        //                    {

        //                        switch (taskBidforimg.ButtonName1)
        //                        {
        //                            case "Bid":
        //                                {
        //                                    if (taskBidforimg.IsReject == true)
        //                                    {
        //                                        taskBidforimg.TaskStatus = 1;
        //                                    }
        //                                    break;
        //                                }
        //                            case "Before":
        //                                {
        //                                    if (taskBidforimg.IsReject == true)
        //                                    {
        //                                        taskBidforimg.TaskStatus = 1;
        //                                    }
        //                                    break;
        //                                }
        //                            case "Damage":
        //                                {
        //                                    if (taskBidforimg.IsReject == true)
        //                                    {
        //                                        taskBidforimg.TaskStatus = 1;
        //                                    }
        //                                    break;
        //                                }
        //                            case "Inspection":
        //                                {
        //                                    if (taskBidforimg.IsReject == true)
        //                                    {
        //                                        taskBidforimg.TaskStatus = 1;
        //                                    }
        //                                    break;
        //                                }
        //                            case "Label":
        //                                {
        //                                    if (taskBidforimg.IsReject == true)
        //                                    {
        //                                        taskBidforimg.TaskStatus = 1;
        //                                    }
        //                                    break;
        //                                }

        //                        }
        //                    }
        //                    else
        //                    {
        //                        DataRow[] dr = ds.Tables[1].Select("Task_Bid_pkeyID = " + taskBidforimg.Task_Bid_pkeyID);
        //                        string countval = string.Empty;
        //                        countval = "0";
        //                        foreach (var item in dr)
        //                        {
        //                            countval = item["countval"].ToString();
        //                            if (!string.IsNullOrEmpty(item["CRP_New_Status_Type"].ToString()))
        //                            {
        //                                Status_Type = Convert.ToInt32(item["CRP_New_Status_Type"].ToString());
        //                            }

        //                            //Int64 Task_Bid_pkeyID = Convert.ToInt64(ds.Tables[1].Rows[j]["Task_Bid_pkeyID"].ToString());
        //                            //string countval = ds.Tables[1].Rows[j]["countval"].ToString();

        //                            //if (!string.IsNullOrEmpty(ds.Tables[1].Rows[i]["CRP_New_Status_Type"].ToString()))
        //                            //{
        //                            //    Status_Type = Convert.ToInt32(ds.Tables[1].Rows[i]["CRP_New_Status_Type"].ToString());
        //                            //}

        //                            switch (Status_Type)
        //                            {
        //                                case 1: // Before
        //                                    {
        //                                        strBefore = "B (" + countval + ")";
        //                                        intBcount = Convert.ToInt32(countval);
        //                                        taskBidforimg.TaskStatusB = intBcount;
        //                                        break;
        //                                    }
        //                                case 2: // During
        //                                    {
        //                                        strDuring = "D (" + countval + ")";
        //                                        intAcount = Convert.ToInt32(countval);
        //                                        taskBidforimg.TaskStatusD = intAcount;
        //                                        break;
        //                                    }
        //                                case 3: // After
        //                                    {
        //                                        strAfter = "A (" + countval + ")";
        //                                        intDcount = Convert.ToInt32(countval);
        //                                        taskBidforimg.TaskStatusA = intDcount;
        //                                        break;
        //                                    }
        //                                case 4: // Bid
        //                                    {

        //                                        taskBidforimg.Task_Photo_Label_Name = ds.Tables[0].Rows[i]["Task_Photo_Label_Name"].ToString() + "  [(" + countval + ")]";
        //                                        intcount = Convert.ToInt32(countval);
        //                                        taskBidforimg.TaskStatusB = intcount;
        //                                        break;
        //                                    }
        //                                case 5: // Damage
        //                                    {
        //                                        taskBidforimg.Task_Photo_Label_Name = ds.Tables[0].Rows[i]["Task_Photo_Label_Name"].ToString() + " [(" + countval + ")]";
        //                                        intcount = Convert.ToInt32(countval);
        //                                        taskBidforimg.TaskStatusB = intcount;
        //                                        break;
        //                                    }
        //                                case 6: // Custom
        //                                    {
        //                                        taskBidforimg.Task_Photo_Label_Name = ds.Tables[0].Rows[i]["Task_Photo_Label_Name"].ToString() + "  [(" + countval + ")]";
        //                                        intcount = Convert.ToInt32(countval);
        //                                        taskBidforimg.TaskStatusB = intcount;
        //                                        break;
        //                                    }
        //                                case 7: // Inspection
        //                                    {
        //                                        taskBidforimg.Task_Photo_Label_Name = ds.Tables[0].Rows[i]["Task_Photo_Label_Name"].ToString() + "  [(" + countval + ")]";
        //                                        intcount = Convert.ToInt32(countval);
        //                                        taskBidforimg.TaskStatusB = intcount;
        //                                        break;
        //                                    }






        //                            }
        //                        }

        //                    }
        //                    if (Status_Type != 0)
        //                    {
        //                        switch (Status_Type)
        //                        {
        //                            case 1:
        //                            case 2:
        //                            case 3:
        //                                {
        //                                    if (taskBidforimg.IsReject == true)
        //                                    {
        //                                        taskBidforimg.TaskStatus = 1;
        //                                    }
        //                                    else
        //                                    {
        //                                        string val = strBefore + " " + strDuring + " " + strAfter;
        //                                        taskBidforimg.TaskStatus = 0;
        //                                        if (intAcount != 0 && intBcount != 0 && intDcount != 0)
        //                                        {
        //                                            taskBidforimg.TaskStatus = 1;
        //                                        }
        //                                        taskBidforimg.Task_Photo_Label_Name = ds.Tables[0].Rows[i]["Task_Photo_Label_Name"].ToString() + "  " + val;
        //                                    }

        //                                    break;
        //                                }


        //                            case 4:
        //                            case 5:
        //                            case 6:
        //                            case 7:
        //                                {
        //                                    if (taskBidforimg.IsReject == true)
        //                                    {
        //                                        taskBidforimg.TaskStatus = 1;
        //                                    }
        //                                    else
        //                                    {
        //                                        taskBidforimg.TaskStatus = 0;
        //                                        if (intcount != 0)
        //                                        {
        //                                            taskBidforimg.TaskStatus = 1;
        //                                        }

        //                                    }

        //                                    break;
        //                                }


        //                        }
        //                    }
        //                    else
        //                    {
        //                        switch (taskBidforimg.ButtonName1)
        //                        {
        //                            case "Bid":
        //                                {
        //                                    if (taskBidforimg.IsReject == true)
        //                                    {
        //                                        taskBidforimg.TaskStatus = 1;
        //                                    }
        //                                    break;
        //                                }
        //                            case "Before":
        //                                {
        //                                    if (taskBidforimg.IsReject == true)
        //                                    {
        //                                        taskBidforimg.TaskStatus = 1;
        //                                    }
        //                                    break;
        //                                }
        //                            case "Damage":
        //                                {
        //                                    if (taskBidforimg.IsReject == true)
        //                                    {
        //                                        taskBidforimg.TaskStatus = 1;
        //                                    }
        //                                    break;
        //                                }
        //                            case "Inspection":
        //                                {
        //                                    if (taskBidforimg.IsReject == true)
        //                                    {
        //                                        taskBidforimg.TaskStatus = 1;
        //                                    }
        //                                    break;
        //                                }
        //                            case "Label":
        //                                {
        //                                    if (taskBidforimg.IsReject == true)
        //                                    {
        //                                        taskBidforimg.TaskStatus = 1;
        //                                    }
        //                                    break;
        //                                }

        //                        }
        //                    }




        //                }
        //                lsttaskBidforimg.Add(taskBidforimg);


        //            }
        //        }


        //        objDynamic.Add(lsttaskBidforimg);
        //    }
        //    catch (Exception ex)
        //    {
        //        log.logErrorMessage(ex.StackTrace);
        //        log.logErrorMessage(ex.Message);
        //    }

        //    return objDynamic;
        //}
        #endregion

        public List<dynamic> GetInvoiceBidfortaskMobApp(TaskBidforimg model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                WorkOrderMaster_Details_MobileApp_Data workOrderMaster_Details_MobileApp_Data = new WorkOrderMaster_Details_MobileApp_Data();
                workOrderxDTO workOrderxDTO = new workOrderxDTO();
                workOrderxDTO.workOrder_ID = Convert.ToInt64(model.Task_Bid_WO_ID);
                workOrderxDTO.Type = 8;
                workOrderxDTO.UserID = model.UserID;
                objDynamic.Add(workOrderMaster_Details_MobileApp_Data.GetWorkOrderDataDetailsForMobile(workOrderxDTO));

                objDynamic.Add(GetInvoiceBidfortaskMobAppDetails(model));
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        public List<TaskInvoiceBid_MobApp> GetInvoiceBidfortaskMobAppDetailsForOffline(Int64 WorkOrderID, Int64 UserID, int Task_Bid_Sys_Type, Int64 Task_Bid_TaskID)
        {
            List<TaskInvoiceBid_MobApp> lsttaskBidforimg = new List<TaskInvoiceBid_MobApp>();
            try
            {
                TaskBidforimg model = new TaskBidforimg();
                model.Task_Bid_TaskID = Task_Bid_TaskID;
                model.Task_Bid_WO_ID = WorkOrderID;
                model.UserID = UserID;
                model.Task_Bid_Sys_Type = Task_Bid_Sys_Type;
                lsttaskBidforimg = GetInvoiceBidfortaskMobAppDetails(model);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return lsttaskBidforimg;
        }

        private TaskPhotoSettingDTO GettaskPhotoSettingDTOValues(string strphotosetting)
        {
            TaskPhotoSettingDTO taskPhotoSettingDTO = new TaskPhotoSettingDTO();
            try
            {
                if (!string.IsNullOrEmpty(strphotosetting) && strphotosetting != "")
                {
                    var data = JsonConvert.DeserializeObject<List<TaskPhotoSettingDTO>>(strphotosetting);
                    for (int i = 0; i < data.Count; i++)
                    {
                        taskPhotoSettingDTO = data[i];
                    }
                }
                 
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return taskPhotoSettingDTO;
        }

        public List<TaskInvoiceBid_MobApp> GetInvoiceBidfortaskMobAppDetails(TaskBidforimg model)
        {
            List<TaskInvoiceBid_MobApp> lsttaskBidforimg = new List<TaskInvoiceBid_MobApp>();

            try
            {


                //Notification coden to  be added here

                DataSet ds = Get_Invoice_Bidfortask_MobApp(model);

                if (ds.Tables.Count > 0 && ds.Tables[0] != null)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        TaskInvoiceBid_MobApp taskBidforimg = new TaskInvoiceBid_MobApp();
                        TaskPhotoSettingDTO taskPhotoSettingDTO = new TaskPhotoSettingDTO();
                        if (!string.IsNullOrEmpty(ds.Tables[0].Rows[i]["Task_Bid_TaskID"].ToString()))
                        {
                            taskBidforimg.Task_Bid_TaskID = Convert.ToInt64(ds.Tables[0].Rows[i]["Task_Bid_TaskID"].ToString());
                        }
                        taskBidforimg.ButtonName1 = ds.Tables[0].Rows[i]["ButtonName1"].ToString();
                        taskBidforimg.ButtonName2 = ds.Tables[0].Rows[i]["ButtonName2"].ToString();
                        taskBidforimg.ButtonName3 = ds.Tables[0].Rows[i]["ButtonName3"].ToString();
                        taskBidforimg.Comments = ds.Tables[0].Rows[i]["Comments"].ToString();
                        taskBidforimg.TMF_Task_FileName = ds.Tables[0].Rows[i]["TMF_Task_FileName"].ToString();
                        taskBidforimg.TMF_Task_localPath = ds.Tables[0].Rows[i]["TMF_Task_localPath"].ToString();
                        //if (!string.IsNullOrEmpty(ds.Tables[0].Rows[i]["Countval"].ToString()))
                        //{
                        //    taskBidforimg.Countval = Convert.ToInt32(ds.Tables[0].Rows[i]["Countval"].ToString());
                        //}
                        taskBidforimg.Task_Label_Name = ds.Tables[0].Rows[i]["Task_Photo_Label_Name"].ToString();
                        taskBidforimg.Task_Photo_Setting_details = ds.Tables[0].Rows[i]["Task_Photo_Setting_details"].ToString();
                        switch (taskBidforimg.ButtonName1)
                        {
                            case "Bid":
                                {
                                    taskBidforimg.Task_Photo_Label_Name = ds.Tables[0].Rows[i]["Task_Photo_Label_Name"].ToString() + "Bid(0)";
                                    if (!string.IsNullOrWhiteSpace(taskBidforimg.Task_Photo_Setting_details))
                                    {
                                        taskBidforimg.Task_Photo_Label_Name = ds.Tables[0].Rows[i]["Task_Photo_Label_Name"].ToString() + " Bid(0)  M(0)";
                                        taskPhotoSettingDTO = GettaskPhotoSettingDTOValues(taskBidforimg.Task_Photo_Setting_details);
                                        if (taskPhotoSettingDTO.TPS_MeasurementBidStatus == true)
                                        {
                                            taskBidforimg.ButtonName4 = "MeasureMent";
                                        }
                                    }
                                    break;
                                }
                            case "Before":
                                {
                                    taskBidforimg.Task_Photo_Label_Name = ds.Tables[0].Rows[i]["Task_Photo_Label_Name"].ToString() + "  B(0) D(0) A(0)";
                                    taskPhotoSettingDTO = GettaskPhotoSettingDTOValues(taskBidforimg.Task_Photo_Setting_details);
                                    if (taskPhotoSettingDTO.TPS_MeasurementCompletionStatus == true)
                                    {
                                        taskBidforimg.Task_Photo_Label_Name = ds.Tables[0].Rows[i]["Task_Photo_Label_Name"].ToString() + "  B(0) D(0) A(0) M(0)";
                                        taskBidforimg.ButtonName4 = "MeasureMent";
                                    }
                                    if (taskPhotoSettingDTO.TPS_LoadPhotosCompletionStatus == true)
                                    {
                                        taskBidforimg.Task_Photo_Label_Name = ds.Tables[0].Rows[i]["Task_Photo_Label_Name"].ToString() + "  B(0) D(0) A(0) L0)";
                                        taskBidforimg.ButtonName5 = "Load";
                                    }
                                    if (taskPhotoSettingDTO.TPS_MeasurementCompletionStatus == true && taskPhotoSettingDTO.TPS_LoadPhotosCompletionStatus == true)
                                    {
                                        taskBidforimg.Task_Photo_Label_Name = ds.Tables[0].Rows[i]["Task_Photo_Label_Name"].ToString() + "  B(0) D(0) A(0) M(0) L0)";
                                    }
                                    break;
                                }
                            case "Damage":
                                {
                                    taskBidforimg.Task_Photo_Label_Name = ds.Tables[0].Rows[i]["Task_Photo_Label_Name"].ToString() + " (0)";
                                    break;
                                }
                            case "Inspection":
                                {
                                    taskBidforimg.Task_Photo_Label_Name = ds.Tables[0].Rows[i]["Task_Photo_Label_Name"].ToString() + " (0)";
                                    break;
                                }
                            case "Violation":
                                {
                                    taskBidforimg.Task_Photo_Label_Name = ds.Tables[0].Rows[i]["Task_Photo_Label_Name"].ToString() + " (0)";
                                    break;
                                }
                            case "Hazard":
                                {
                                    taskBidforimg.Task_Photo_Label_Name = ds.Tables[0].Rows[i]["Task_Photo_Label_Name"].ToString() + " (0)";
                                    break;
                                }
                            case "Label":
                                {
                                    taskBidforimg.Task_Photo_Label_Name = ds.Tables[0].Rows[i]["Task_Photo_Label_Name"].ToString() + " (0)";
                                    break;
                                }
                            case "Labels":
                                {
                                    taskBidforimg.Task_Photo_Label_Name = ds.Tables[0].Rows[i]["Task_Photo_Label_Name"].ToString() + " (0)";
                                    break;
                                }

                        }

                        if (!string.IsNullOrEmpty(ds.Tables[0].Rows[i]["Task_Bid_pkeyID"].ToString()))
                        {
                            taskBidforimg.Task_Bid_pkeyID = Convert.ToInt64(ds.Tables[0].Rows[i]["Task_Bid_pkeyID"].ToString());
                        }

                        taskBidforimg.Task_Bid_Qty = ds.Tables[0].Rows[i]["Qty"].ToString();
                        taskBidforimg.IsReject = false;
                        if (!string.IsNullOrEmpty(ds.Tables[0].Rows[i]["IsReject"].ToString()))
                        {
                            taskBidforimg.IsReject = Convert.ToBoolean(ds.Tables[0].Rows[i]["IsReject"].ToString());
                        }


                        if (!string.IsNullOrEmpty(ds.Tables[0].Rows[i]["Cont_Price"].ToString()))

                        {
                            taskBidforimg.Task_Bid_Cont_Price = Convert.ToDecimal(ds.Tables[0].Rows[i]["Cont_Price"].ToString());
                        }
                        if (!string.IsNullOrEmpty(ds.Tables[0].Rows[i]["Cont_Total"].ToString()))
                        {
                            taskBidforimg.Task_Bid_Cont_Total = Convert.ToDecimal(ds.Tables[0].Rows[i]["Cont_Total"].ToString());
                        }
                        if (!string.IsNullOrEmpty(ds.Tables[0].Rows[i]["Clnt_Price"].ToString()))
                        {
                            taskBidforimg.Task_Bid_Clnt_Price = Convert.ToDecimal(ds.Tables[0].Rows[i]["Clnt_Price"].ToString());
                        }
                        if (!string.IsNullOrEmpty(ds.Tables[0].Rows[i]["Clnt_Total"].ToString()))
                        {
                            taskBidforimg.Task_Bid_Clnt_Total = Convert.ToDecimal(ds.Tables[0].Rows[i]["Clnt_Total"].ToString());
                        }


                        int Status_Type = 0;
                        int intBcount = 0, intDcount = 0, intAcount = 0, intcount = 0 , 
                            intBidcount = 0, intBidMeasurementcount = 0, intCompleteMeasurementcount = 0, intCompleteLoadcount = 0;
                        string strBefore = string.Empty, strAfter = string.Empty, strDuring = string.Empty , strBid = string.Empty , 
                            strBidMeasurement = string.Empty,strCompleteMeasurement = string.Empty,strCompleteLoad = string.Empty;

                        if (ds.Tables.Count > 1)
                        {
                            if (ds.Tables[1].Rows.Count == 0)
                            {

                                switch (taskBidforimg.ButtonName1)
                                {
                                    case "Bid":
                                        {
                                            if (taskBidforimg.IsReject == true)
                                            {
                                                taskBidforimg.TaskStatus = 1;
                                            }
                                            break;
                                        }
                                    case "Before":
                                        {
                                            if (taskBidforimg.IsReject == true)
                                            {
                                                taskBidforimg.TaskStatus = 1;
                                            }
                                            break;
                                        }
                                    case "Damage":
                                        {
                                            if (taskBidforimg.IsReject == true)
                                            {
                                                taskBidforimg.TaskStatus = 1;
                                            }
                                            break;
                                        }
                                    case "Inspection":
                                        {
                                            if (taskBidforimg.IsReject == true)
                                            {
                                                taskBidforimg.TaskStatus = 1;
                                            }
                                            break;
                                        }
                                    case "Violation":
                                        {
                                            if (taskBidforimg.IsReject == true)
                                            {
                                                taskBidforimg.TaskStatus = 1;
                                            }
                                            break;
                                        }
                                    case "Hazard":
                                        {
                                            if (taskBidforimg.IsReject == true)
                                            {
                                                taskBidforimg.TaskStatus = 1;
                                            }
                                            break;
                                        }
                                    case "Label":
                                        {
                                            if (taskBidforimg.IsReject == true)
                                            {
                                                taskBidforimg.TaskStatus = 1;
                                            }
                                            break;
                                        }
                                    case "Labels":
                                        {
                                            if (taskBidforimg.IsReject == true)
                                            {
                                                taskBidforimg.TaskStatus = 1;
                                            }
                                            break;
                                        }

                                }
                            }
                            else
                            {
                                DataRow[] dr = ds.Tables[1].Select("Task_Bid_pkeyID = " + taskBidforimg.Task_Bid_pkeyID);
                                string countval = string.Empty;
                                countval = "0";
                                foreach (var item in dr)
                                {
                                    countval = item["countval"].ToString();
                                    if (!string.IsNullOrEmpty(item["CRP_New_Status_Type"].ToString()))
                                    {
                                        Status_Type = Convert.ToInt32(item["CRP_New_Status_Type"].ToString());
                                    }

                                    //Int64 Task_Bid_pkeyID = Convert.ToInt64(ds.Tables[1].Rows[j]["Task_Bid_pkeyID"].ToString());
                                    //string countval = ds.Tables[1].Rows[j]["countval"].ToString();

                                    //if (!string.IsNullOrEmpty(ds.Tables[1].Rows[i]["CRP_New_Status_Type"].ToString()))
                                    //{
                                    //    Status_Type = Convert.ToInt32(ds.Tables[1].Rows[i]["CRP_New_Status_Type"].ToString());
                                    //}

                                    switch (Status_Type)
                                    {
                                        case 1: // Before
                                            {
                                                strBefore = "B (" + countval + ")";
                                                intBcount = Convert.ToInt32(countval);
                                                taskBidforimg.TaskStatusB = intBcount;
                                                break;
                                            }
                                        case 2: // During
                                            {
                                                strDuring = "D (" + countval + ")";
                                                intAcount = Convert.ToInt32(countval);
                                                taskBidforimg.TaskStatusD = intAcount;
                                                break;
                                            }
                                        case 3: // After
                                            {
                                                strAfter = "A (" + countval + ")";
                                                intDcount = Convert.ToInt32(countval);
                                                taskBidforimg.TaskStatusA = intDcount;
                                                break;
                                            }
                                        case 4: // Bid
                                            {   
                                                strBid = "Bid[(" + countval + ")]";
                                                intBidcount = Convert.ToInt32(countval);
                                                taskBidforimg.TaskStatusB = intcount;
                                                break;
                                            }
                                        case 5: // Damage
                                            {
                                                taskBidforimg.Task_Photo_Label_Name = ds.Tables[0].Rows[i]["Task_Photo_Label_Name"].ToString() + " [(" + countval + ")]";
                                                intcount = Convert.ToInt32(countval);
                                                taskBidforimg.TaskStatusB = intcount;
                                                break;
                                            }
                                        case 6: // Custom
                                            {
                                                taskBidforimg.Task_Photo_Label_Name = ds.Tables[0].Rows[i]["Task_Photo_Label_Name"].ToString() + "  [(" + countval + ")]";
                                                intcount = Convert.ToInt32(countval);
                                                taskBidforimg.TaskStatusB = intcount;
                                                break;
                                            }
                                        case 7: // Inspection
                                            {
                                                taskBidforimg.Task_Photo_Label_Name = ds.Tables[0].Rows[i]["Task_Photo_Label_Name"].ToString() + "  [(" + countval + ")]";
                                                intcount = Convert.ToInt32(countval);
                                                taskBidforimg.TaskStatusB = intcount;
                                                break;
                                            }
                                        case 8: // Violation
                                            {
                                                taskBidforimg.Task_Photo_Label_Name = ds.Tables[0].Rows[i]["Task_Photo_Label_Name"].ToString() + "  [(" + countval + ")]";
                                                intcount = Convert.ToInt32(countval);
                                                taskBidforimg.TaskStatusB = intcount;
                                                break;
                                            }
                                        case 9: // Hazard
                                            {
                                                taskBidforimg.Task_Photo_Label_Name = ds.Tables[0].Rows[i]["Task_Photo_Label_Name"].ToString() + "  [(" + countval + ")]";
                                                intcount = Convert.ToInt32(countval);
                                                taskBidforimg.TaskStatusB = intcount;
                                                break;
                                            }
                                        case 10: // PCRPhotoLabel
                                            {
                                                taskBidforimg.Task_Photo_Label_Name = ds.Tables[0].Rows[i]["Task_Photo_Label_Name"].ToString() + "  [(" + countval + ")]";
                                                intcount = Convert.ToInt32(countval);
                                                taskBidforimg.TaskStatusB = intcount;
                                                break;
                                            }
                                        case 11: // BidMeasurement
                                            {

                                                
                                                strBidMeasurement = "M[(" + countval + ")]";
                                                intBidMeasurementcount = Convert.ToInt32(countval);
                                                taskBidforimg.TaskStatusM = intcount;
                                                break;
                                            }
                                        case 12: // CompleteMeasurement
                                            {

                                                strCompleteMeasurement = "M[(" + countval + ")]";
                                                intCompleteMeasurementcount = Convert.ToInt32(countval);
                                                taskBidforimg.TaskStatusM = intcount;
                                                break;
                                            }
                                        case 13: // CompleteLoad
                                            {
                                                strCompleteLoad = "L[(" + countval + ")]";
                                                intCompleteLoadcount = Convert.ToInt32(countval);
                                                taskBidforimg.TaskStatusL = intcount;
                                                break;
                                            }






                                    }
                                }

                            }
                            if (Status_Type != 0)
                            {
                                switch (Status_Type)
                                {
                                    case 1:
                                    case 2:
                                    case 3:
                                    case 12:
                                    case 13:
                                        {
                                            if (taskBidforimg.IsReject == true)
                                            {
                                                taskBidforimg.TaskStatus = 1;
                                            }
                                            else
                                            {
                                                string val = strBefore + " " + strDuring + " " + strAfter + " "+ strCompleteMeasurement + " "+strCompleteLoad;
                                                taskBidforimg.TaskStatus = 0;
                                                if (intAcount != 0 && intBcount != 0 && intDcount != 0 && intCompleteMeasurementcount !=0 && intCompleteLoadcount !=0)
                                                {
                                                    taskBidforimg.TaskStatus = 1;
                                                }
                                                taskBidforimg.Task_Photo_Label_Name = ds.Tables[0].Rows[i]["Task_Photo_Label_Name"].ToString() + "  " + val;
                                            }

                                            break;
                                        }


                                    case 4:
                                    case 11:
                                        {
                                            if (taskBidforimg.IsReject == true)
                                            {
                                                taskBidforimg.TaskStatus = 1;
                                            }
                                            else
                                            {
                                                string val = strBid + " " + strBidMeasurement;
                                                taskBidforimg.TaskStatus = 0;
                                                if (intBidcount != 0 && intBidMeasurementcount !=0)
                                                {
                                                    taskBidforimg.TaskStatus = 1;
                                                }
                                                taskBidforimg.Task_Photo_Label_Name = ds.Tables[0].Rows[i]["Task_Photo_Label_Name"].ToString() + "  " + val;
                                            }

                                            break;
                                        }
                                    case 5:
                                    case 6:
                                    case 7:
                                    case 8:
                                    case 9:
                                    case 10:
                                        {
                                            if (taskBidforimg.IsReject == true)
                                            {
                                                taskBidforimg.TaskStatus = 1;
                                            }
                                            else
                                            {
                                                taskBidforimg.TaskStatus = 0;
                                                if (intcount != 0)
                                                {
                                                    taskBidforimg.TaskStatus = 1;
                                                }

                                            }

                                            break;
                                        }


                                }
                            }
                            else
                            {
                                switch (taskBidforimg.ButtonName1)
                                {
                                    case "Bid":
                                        {
                                            if (taskBidforimg.IsReject == true)
                                            {
                                                taskBidforimg.TaskStatus = 1;
                                            }
                                            break;
                                        }
                                    case "Before":
                                        {
                                            if (taskBidforimg.IsReject == true)
                                            {
                                                taskBidforimg.TaskStatus = 1;
                                            }
                                            break;
                                        }
                                    case "Damage":
                                        {
                                            if (taskBidforimg.IsReject == true)
                                            {
                                                taskBidforimg.TaskStatus = 1;
                                            }
                                            break;
                                        }
                                    case "Inspection":
                                        {
                                            if (taskBidforimg.IsReject == true)
                                            {
                                                taskBidforimg.TaskStatus = 1;
                                            }
                                            break;
                                        }
                                    case "Violation":
                                        {
                                            if (taskBidforimg.IsReject == true)
                                            {
                                                taskBidforimg.TaskStatus = 1;
                                            }
                                            break;
                                        }
                                    case "Hazard":
                                        {
                                            if (taskBidforimg.IsReject == true)
                                            {
                                                taskBidforimg.TaskStatus = 1;
                                            }
                                            break;
                                        }
                                    case "Label":
                                        {
                                            if (taskBidforimg.IsReject == true)
                                            {
                                                taskBidforimg.TaskStatus = 1;
                                            }
                                            break;
                                        }
                                    case "Labels":
                                        {
                                            if (taskBidforimg.IsReject == true)
                                            {
                                                taskBidforimg.TaskStatus = 1;
                                            }
                                            break;
                                        }

                                }
                            }




                        }
                        lsttaskBidforimg.Add(taskBidforimg);


                    }
                }



            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return lsttaskBidforimg;
        }


        public List<dynamic> GetbitfortaskDetails(TaskBidforimg model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetbitfortaskMaster_bak(model);
                #region Comment
                //var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                //List<TaskBidforimg> taskPhoto =
                //   (from item in myEnumerableFeaprd
                //    select new TaskBidforimg
                //    {
                //        Task_Bid_TaskID = item.Field<Int64>("Task_Bid_TaskID"),
                //        Task_Photo_Label_Name = item.Field<String>("Task_Photo_Label_Name"),
                //        Task_Bid_pkeyID = item.Field<Int64?>("Task_Bid_pkeyID"),
                //        ButtonName1 = item.Field<String>("ButtonName1"),
                //        ButtonName2 = item.Field<String>("ButtonName2"),
                //        ButtonName3 = item.Field<String>("ButtonName3"),




                //    }).ToList();
                #endregion
                List<TaskBidforimg> lsttaskBidforimg = new List<TaskBidforimg>();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    TaskBidforimg taskBidforimg = new TaskBidforimg();
                    if (!string.IsNullOrEmpty(ds.Tables[0].Rows[i]["Task_Bid_TaskID"].ToString()))
                    {
                        taskBidforimg.Task_Bid_TaskID = Convert.ToInt64(ds.Tables[0].Rows[i]["Task_Bid_TaskID"].ToString());
                    }
                    taskBidforimg.Task_Photo_Label_Name = ds.Tables[0].Rows[i]["Task_Photo_Label_Name"].ToString() + "  (0)";
                    if (!string.IsNullOrEmpty(ds.Tables[0].Rows[i]["Task_Bid_pkeyID"].ToString()))
                    {
                        taskBidforimg.Task_Bid_pkeyID = Convert.ToInt64(ds.Tables[0].Rows[i]["Task_Bid_pkeyID"].ToString());
                    }
                    taskBidforimg.ButtonName1 = ds.Tables[0].Rows[i]["ButtonName1"].ToString();
                    taskBidforimg.ButtonName2 = ds.Tables[0].Rows[i]["ButtonName2"].ToString();
                    taskBidforimg.ButtonName3 = ds.Tables[0].Rows[i]["ButtonName3"].ToString();
                    if (ds.Tables.Count > 1)
                    {
                        for (int j = 0; j < ds.Tables[1].Rows.Count; j++)
                        {
                            Int64 Task_Bid_pkeyID = Convert.ToInt64(ds.Tables[1].Rows[j]["Task_Bid_pkeyID"].ToString());
                            string countval = ds.Tables[1].Rows[j]["countval"].ToString();
                            if (taskBidforimg.Task_Bid_pkeyID == Task_Bid_pkeyID)
                            {
                                taskBidforimg.Task_Photo_Label_Name = ds.Tables[0].Rows[i]["Task_Photo_Label_Name"].ToString() + "  (" + countval + ")";
                            }
                        }
                    }
                    lsttaskBidforimg.Add(taskBidforimg);


                }

                objDynamic.Add(lsttaskBidforimg);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }



        public List<dynamic> GetbitfortaskDetails_bak(TaskBidforimg model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetbitfortaskMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<TaskBidforimg> taskPhoto =
                   (from item in myEnumerableFeaprd
                    select new TaskBidforimg
                    {
                        Task_Bid_TaskID = item.Field<Int64>("Task_Bid_TaskID"),
                        Task_Photo_Label_Name = item.Field<String>("Task_Photo_Label_Name"),
                        Task_Bid_pkeyID = item.Field<Int64?>("Task_Bid_pkeyID"),
                        ButtonName1 = item.Field<String>("ButtonName1"),
                        ButtonName2 = item.Field<String>("ButtonName2"),
                        ButtonName3 = item.Field<String>("ButtonName3"),
                        ButtonName4 = item.Field<String>("ButtonName4"),
                        ButtonName5 = item.Field<String>("ButtonName5"),
                        ButtonName6 = item.Field<String>("ButtonName6"),
                        ButtonName7 = item.Field<String>("ButtonName7"),


                    }).ToList();

                objDynamic.Add(taskPhoto);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        private DataSet GetPhotoTransferTaskList(TaskBidforimg model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_PhotoTransferTaskList]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@WO_IPLNO", 1 + "#varchar#" + model.WO_IPLNO);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetPhotoTransferBidTaskList(TaskBidforimg model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {

                //Notification coden to  be added here
                model.Type = 1;
                DataSet ds = GetPhotoTransferTaskList(model);

                #region Comment
                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<TaskBidforimg> taskPhoto =
                   (from item in myEnumerableFeaprd
                    select new TaskBidforimg
                    {
                        Task_Bid_TaskID = item.Field<Int64>("Task_Bid_TaskID"),
                        Task_Photo_Label_Name = item.Field<String>("Task_Photo_Label_Name"),
                        Task_Bid_pkeyID = item.Field<Int64?>("Task_Bid_pkeyID"),
                        ButtonName1 = item.Field<String>("ButtonName1"),
                        ButtonName2 = item.Field<String>("ButtonName2"),
                        ButtonName3 = item.Field<String>("ButtonName3"),

                    }).ToList();
                #endregion


                objDynamic.Add(taskPhoto);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }


    }
}