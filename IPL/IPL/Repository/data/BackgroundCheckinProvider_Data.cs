﻿using Avigma.Repository.Lib;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using IPLApp.Models;
using IPL.Models;
using System.Threading.Tasks;

namespace IPL.Repository.data
{
    public class BackgroundCheckinProvider_Data
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        SecurityHelper securityHelper = new SecurityHelper();
        private List<dynamic> CreateUpdate_BackgroundCheckinProvider(BackgroundCheckinProvider_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_BackgroundCheckinProvider]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Back_Chk_ProviderID", 1 + "#bigint#" + model.Back_Chk_ProviderID);
                input_parameters.Add("@Back_Chk_ProviderName", 1 + "#nvarchar#" + model.Back_Chk_ProviderName);
                input_parameters.Add("@Back_Chk_IsActive", 1 + "#bit#" + model.Back_Chk_IsActive);
                input_parameters.Add("@Back_Chk_IsDelete", 1 + "#bit#" + model.Back_Chk_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Back_Chk_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);
                objData = obj.SqlCRUD(insertProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;



        }
        private DataSet Get_BackgroundCheckinProvider(BackgroundCheckinProvider_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_BackgroundCheckinProvider]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Back_Chk_ProviderID", 1 + "#bigint#" + model.Back_Chk_ProviderID);
                input_parameters.Add("@WhereClause", 1 + "#varchar#" + model.WhereClause);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }
        public List<dynamic> Update_BackgroundCheckinProvider(BackgroundCheckinProvider_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                objData = CreateUpdate_BackgroundCheckinProvider(model);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }
            return objData;
        }

        public List<dynamic> Get_BackgroundCheckinProviderDetails(BackgroundCheckinProvider_DTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {

                string wherecondition = string.Empty;
                if (model.Type == 3 && !string.IsNullOrEmpty(model.FilterData))
                {
                    var Data = JsonConvert.DeserializeObject<BackgroundCheckinProvider_DTO>(model.FilterData);
                    if (!string.IsNullOrEmpty(Data.Back_Chk_ProviderName))
                    {
                        wherecondition = " And bk.Back_Chk_ProviderName LIKE '%" + Data.Back_Chk_ProviderName + "%'";
                    }
                    if (Data.Back_Chk_IsActive == true)
                    {
                        wherecondition = wherecondition + "  And bk.Back_Chk_IsActive =  1 ";
                    }
                    if (Data.Back_Chk_IsActive == false)
                    {
                        wherecondition = wherecondition + "  And bk.Back_Chk_IsActive =  0";
                    }
                    model.WhereClause = wherecondition;
                }


                DataSet ds = Get_BackgroundCheckinProvider(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<BackgroundCheckinProvider_DTO> Get_Details =
                   (from item in myEnumerableFeaprd
                    select new BackgroundCheckinProvider_DTO
                    {
                        Back_Chk_ProviderID = item.Field<Int64>("Back_Chk_ProviderID"),
                        Back_Chk_ProviderName = item.Field<String>("Back_Chk_ProviderName"),
                        Back_Chk_IsActive = item.Field<Boolean?>("Back_Chk_IsActive"),
                        Back_Chk_CreatedBy = item.Field<String>("Back_Chk_CreatedBy"),
                        Back_Chk_ModifiedBy = item.Field<String>("Back_Chk_ModifiedBy"),

                    }).ToList();

                objDynamic.Add(Get_Details);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}
