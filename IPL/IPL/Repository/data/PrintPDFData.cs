﻿using Avigma.Repository.Lib;
using IPL.Models;
using IPL.Repository.PdfGernerator;
using IPLApp.Models;
using IPLApp.Repository.data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
namespace IPL.Repository.data
{
    public class PrintPDFData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        private DataSet WorkOrderPrintData(PrintPDFDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_WorkOrder_PrintMaster]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@workOrder_ID", 1 + "#bigint#" + model.workOrder_ID);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> WorkOrderPrintDataDetails(PrintPDFDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = WorkOrderPrintData(model);
                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objDynamic;
        }

        //public List<dynamic> GenerateWork_Order_Pdf(PrintPDFDTO model, HttpContext context, string pdfpath)
        public async Task<CustomReportPDF> GenerateWork_Order_Pdf(PrintPDFDTO model, HttpContext context, string pdfpath)
        {
            CustomReportPDF custom = new CustomReportPDF();
            List<dynamic> objData = new List<dynamic>();
            List<WorkOrderTaskMaster> workOrderTaskMasters = new List<WorkOrderTaskMaster>();
            WorkOrdersMaster workOrdersMaster = new WorkOrdersMaster();
            GenerateHtmlToPdf generateHtmlToPdf = new GenerateHtmlToPdf();
            try
            {
                DataSet ds = WorkOrderPrintData(model);
                Decimal grandTotalAmt = 0;
                Decimal grandTotalAmt2 = 0;
                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    //objData.Add(obj.AsDynamicEnumerable(ds.Tables[i]));

                    if (i == 0)
                    {
                        workOrdersMaster.workOrder_ID = Convert.ToInt64(ds.Tables[i].Rows[0]["workOrder_ID"].ToString());
                        workOrdersMaster.workOrderNumber = ds.Tables[i].Rows[0]["workOrderNumber"].ToString();
                        //workOrdersMaster.workOrderInfo = ds.Tables[i].Rows[0]["workOrderInfo"].ToString();
                        workOrdersMaster.address1 = ds.Tables[i].Rows[0]["address1"].ToString();
                        //workOrdersMaster.address2 = ds.Tables[i].Rows[0]["address2"].ToString();
                        workOrdersMaster.city = ds.Tables[i].Rows[0]["city"].ToString();
                        workOrdersMaster.state = ds.Tables[i].Rows[0]["state"].ToString();
                        workOrdersMaster.zip = ds.Tables[i].Rows[0]["zip"].ToString();
                        //workOrdersMaster.country = ds.Tables[i].Rows[0]["country"].ToString();
                        workOrdersMaster.dueDate = ds.Tables[i].Rows[0]["dueDate"].ToString();
                        //workOrdersMaster.startDate = ds.Tables[i].Rows[0]["startDate"].ToString();
                        workOrdersMaster.WorkType = ds.Tables[i].Rows[0]["WorkType"].ToString();
                        workOrdersMaster.Company = ds.Tables[i].Rows[0]["Company"].ToString();
                        workOrdersMaster.Comments = ds.Tables[i].Rows[0]["Comments"].ToString();
                        workOrdersMaster.stateName = ds.Tables[i].Rows[0]["stateName"].ToString();
                        workOrdersMaster.ContractorName = ds.Tables[i].Rows[0]["ContractorName"].ToString();
                        workOrdersMaster.ContractorEmail = ds.Tables[i].Rows[0]["ContractorEmail"].ToString();
                        workOrdersMaster.Cordinator_Name = ds.Tables[i].Rows[0]["Cordinator_Name"].ToString();
                        workOrdersMaster.CordinatorEmail = ds.Tables[i].Rows[0]["CordinatorEmail"].ToString();
                        workOrdersMaster.Cordinator_Phone = ds.Tables[i].Rows[0]["Cordinator_Phone"].ToString();
                        workOrdersMaster.status = ds.Tables[i].Rows[0]["status"].ToString();
                        workOrdersMaster.User_FirstName = ds.Tables[i].Rows[0]["User_FirstName"].ToString();
                        workOrdersMaster.Inst_Task_Name = ds.Tables[i].Rows[0]["Inst_Task_Name"].ToString();

                        workOrdersMaster.IPLNO = ds.Tables[i].Rows[0]["IPLNO"].ToString();
                        workOrdersMaster.Lotsize = ds.Tables[i].Rows[0]["Lotsize"].ToString();
                        workOrdersMaster.Lock_Code = ds.Tables[i].Rows[0]["Lock_Code"].ToString();
                        workOrdersMaster.Key_Code = ds.Tables[i].Rows[0]["Key_Code"].ToString();
                        workOrdersMaster.Mortgagor = ds.Tables[i].Rows[0]["Mortgagor"].ToString();
                        workOrdersMaster.Received_Date = ds.Tables[i].Rows[0]["Received_Date"].ToString();
                        workOrdersMaster.assigned_date = ds.Tables[i].Rows[0]["assigned_date"].ToString();
                        workOrdersMaster.Loan_Number = ds.Tables[i].Rows[0]["Loan_Number"].ToString();
                        workOrdersMaster.WT_WorkType = ds.Tables[i].Rows[0]["WT_WorkType"].ToString();
                        workOrdersMaster.Com_Phone = ds.Tables[i].Rows[0]["Com_Phone"].ToString();
                        workOrdersMaster.Cust_Num_Number = ds.Tables[i].Rows[0]["Cust_Num_Number"].ToString();
                    }
                    else if (i == 1)
                    {
                        int count = ds.Tables[i].Rows.Count;
                        if (model.Type == 1)
                        {
                            for (int j = 0; j < count; j++)
                            {
                                if (model.IsOfficeResult)
                                {
                                    Decimal totalsum = Convert.ToDecimal(ds.Tables[i].Rows[j]["Task_Bid_Clnt_Total"].ToString());
                                    grandTotalAmt += totalsum;
                                }
                                else if (model.IsFiledResult)
                                {
                                    Decimal totalsum = Convert.ToDecimal(ds.Tables[i].Rows[j]["Task_Bid_Cont_Total"].ToString());
                                    grandTotalAmt += totalsum;
                                }

                                workOrderTaskMasters.Add(new WorkOrderTaskMaster
                                {
                                    Task_Name = ds.Tables[i].Rows[j]["Task_Name"].ToString(),
                                    Task_Bid_Qty = ds.Tables[i].Rows[j]["Task_Bid_Qty"].ToString(),
                                    Task_Bid_Cont_Price = Convert.ToDecimal(ds.Tables[i].Rows[j]["Task_Bid_Cont_Price"].ToString()),
                                    Task_Bid_Cont_Total = Convert.ToDecimal(ds.Tables[i].Rows[j]["Task_Bid_Cont_Total"].ToString()),
                                    Task_Bid_Clnt_Price = Convert.ToDecimal(ds.Tables[i].Rows[j]["Task_Bid_Clnt_Price"].ToString()),
                                    Task_Bid_Clnt_Total = Convert.ToDecimal(ds.Tables[i].Rows[j]["Task_Bid_Clnt_Total"].ToString()),
                                    Task_Bid_Comments = ds.Tables[i].Rows[j]["Task_Bid_Comments"].ToString(),
                                });
                            }
                        }
                        else if (model.Type == 2)
                        {

                            for (int j = 0; j < count; j++)
                            {


                                if (model.IsOfficeResult)
                                {
                                    Decimal totalsum = Convert.ToDecimal(ds.Tables[i].Rows[j]["Task_Inv_Clnt_Total"].ToString());
                                    grandTotalAmt += totalsum;
                                }
                                else if (model.IsFiledResult)
                                {
                                    Decimal totalsum = Convert.ToDecimal(ds.Tables[i].Rows[j]["Task_Inv_Cont_Total"].ToString());
                                    grandTotalAmt += totalsum;
                                }
                                workOrderTaskMasters.Add(new WorkOrderTaskMaster
                                {
                                    Task_Name = ds.Tables[i].Rows[j]["Task_Name"].ToString(),
                                    Task_Inv_Qty = ds.Tables[i].Rows[j]["Task_Inv_Qty"].ToString(),
                                    Task_Inv_Cont_Price = Convert.ToDecimal(ds.Tables[i].Rows[j]["Task_Inv_Cont_Price"].ToString()),
                                    Task_Inv_Cont_Total = Convert.ToDecimal(ds.Tables[i].Rows[j]["Task_Inv_Cont_Total"].ToString()),
                                    Task_Inv_Clnt_Price = Convert.ToDecimal(ds.Tables[i].Rows[j]["Task_Inv_Clnt_Price"].ToString()),
                                    Task_Inv_Clnt_Total = Convert.ToDecimal(ds.Tables[i].Rows[j]["Task_Inv_Clnt_Total"].ToString()),
                                    Task_Inv_Comments = ds.Tables[i].Rows[j]["Task_Inv_Comments"].ToString(),
                                });
                            }
                        }
                        else if (model.Type == 3)
                        {

                            for (int j = 0; j < count; j++)
                            {
                                try
                                {
                                    Decimal totalsum = Convert.ToDecimal(ds.Tables[i].Rows[j]["Task_Damage_Estimate"].ToString());
                                    grandTotalAmt += totalsum;
                                }
                                catch (Exception ex)
                                {
                                    log.logErrorMessage(ex.Message);
                                    log.logErrorMessage(ex.StackTrace);
                                }
                                workOrderTaskMasters.Add(new WorkOrderTaskMaster
                                {
                                    Damage_Type = ds.Tables[i].Rows[j]["Damage_Type"].ToString(),
                                    Task_Damage_Qty = ds.Tables[i].Rows[j]["Task_Damage_Qty"].ToString(),
                                    Task_Damage_Disc = ds.Tables[i].Rows[j]["Task_Damage_Disc"].ToString(),
                                    Task_Damage_Estimate = ds.Tables[i].Rows[j]["Task_Damage_Estimate"].ToString(),
                                    Task_Damage_Int = ds.Tables[i].Rows[j]["Task_Damage_Int"].ToString() == "1" ? "Int" : "Ext",
                                    Task_Damage_Location = ds.Tables[i].Rows[j]["Task_Damage_Location"].ToString(),
                                    Task_Damage_Type = ds.Tables[i].Rows[j]["Task_Damage_Type"].ToString(),
                                });
                            }
                        }
                        else if (model.Type == 4)
                        {

                            for (int j = 0; j < count; j++)
                            {
                                Decimal totalsum = Convert.ToDecimal(ds.Tables[i].Rows[j]["Instr_Contractor_Total"].ToString());
                                grandTotalAmt += totalsum;

                                Decimal total_instruction = Convert.ToDecimal(ds.Tables[i].Rows[j]["Instr_Total_Text"].ToString());
                                grandTotalAmt2 += total_instruction;

                                workOrderTaskMasters.Add(new WorkOrderTaskMaster
                                {
                                    Task_Name = ds.Tables[i].Rows[j]["Task_Name"].ToString(),
                                    Instr_Qty = ds.Tables[i].Rows[j]["Instr_Qty"].ToString(),
                                    Instr_Contractor_Price = Convert.ToDecimal(ds.Tables[i].Rows[j]["Instr_Contractor_Price"].ToString()),
                                    Instr_Contractor_Total = Convert.ToDecimal(ds.Tables[i].Rows[j]["Instr_Contractor_Total"].ToString()),
                                    Inst_Comand_Mobile_details = ds.Tables[i].Rows[j]["Inst_Comand_Mobile_details"].ToString(),


                                    Instr_ValType = Convert.ToInt32(ds.Tables[i].Rows[j]["Instr_ValType"].ToString()),
                                    Instr_Qty_Text = ds.Tables[i].Rows[j]["Instr_Qty_Text"].ToString(),
                                    Instr_Price_Text = Convert.ToDecimal(ds.Tables[i].Rows[j]["Instr_Price_Text"].ToString()),
                                    Instr_Total_Text = Convert.ToDecimal(ds.Tables[i].Rows[j]["Instr_Total_Text"].ToString()),



                                    //Task_Bid_Clnt_Price = Convert.ToDecimal(ds.Tables[i].Rows[j]["Task_Bid_Clnt_Price"].ToString()),
                                    //Task_Bid_Clnt_Total = Convert.ToDecimal(ds.Tables[i].Rows[j]["Task_Bid_Clnt_Total"].ToString()),
                                    //Task_Bid_Comments = ds.Tables[i].Rows[j]["Task_Bid_Comments"].ToString(),
                                    //Task_Bid_Status = ds.Tables[i].Rows[0]["Task_Bid_Status"].ToString(),
                                    //Task_Bid_Hazards = ds.Tables[i].Rows[0]["Task_Bid_Hazards"].ToString(),
                                    //Task_Ext_Location = ds.Tables[i].Rows[0]["Task_Ext_Location"].ToString(),

                                });
                            }
                        }

                        else if (model.Type == 5)
                        {

                            for (int j = 0; j < count; j++)
                            {
                                Decimal totalsum = Convert.ToDecimal(ds.Tables[i].Rows[j]["Task_Violation_Fine_Amount"].ToString());
                                grandTotalAmt += totalsum;
                                workOrderTaskMasters.Add(new WorkOrderTaskMaster
                                {
                                    Task_Violation_Name = ds.Tables[i].Rows[j]["Task_Violation_Name"].ToString(),
                                    Task_Violation_Fine_Amount = Convert.ToDecimal(ds.Tables[i].Rows[j]["Task_Violation_Fine_Amount"].ToString()),
                                    Task_Violation_Comment = ds.Tables[i].Rows[j]["Task_Violation_Comment"].ToString(),
                                    Task_Violation_Contact = ds.Tables[i].Rows[j]["Task_Violation_Contact"].ToString(),
                                    Task_Violation_Id = ds.Tables[i].Rows[j]["Task_Violation_Id"].ToString(),
                                    Task_Violation_Date = Convert.ToDateTime(ds.Tables[i].Rows[j]["Task_Violation_Date"].ToString()),
                                    Task_Violation_Date_Discovered = Convert.ToDateTime(ds.Tables[i].Rows[j]["Task_Violation_Date_Discovered"].ToString()),
                                    Task_Violation_Deadline = Convert.ToDateTime(ds.Tables[i].Rows[j]["Task_Violation_Deadline"].ToString()),
                                });
                            }
                        }

                        else if (model.Type == 6)
                        {
                            for (int j = 0; j < count; j++)
                            {
                                workOrderTaskMasters.Add(new WorkOrderTaskMaster
                                {
                                    Task_Hazard_Name = ds.Tables[i].Rows[j]["Task_Hazard_Name"].ToString(),
                                    Task_Hazard_Date_Discovered = Convert.ToDateTime(ds.Tables[i].Rows[j]["Task_Hazard_Date_Discovered"].ToString()),
                                    Task_Hazard_Description = ds.Tables[i].Rows[j]["Task_Hazard_Description"].ToString(),
                                    Task_Hazard_Comment = ds.Tables[i].Rows[j]["Task_Hazard_Comment"].ToString(),
                                });
                            }
                        }

                        workOrdersMaster.workOrderTaskMastersList = workOrderTaskMasters;
                        workOrdersMaster.GrandTotalAmt = grandTotalAmt;
                        workOrdersMaster.GrandTotalAmt2 = grandTotalAmt2;
                        workOrdersMaster.Type = model.Type;
                        workOrdersMaster.UserId = model.UserID;

                    }
                }
                //string WorkOrderPdfpath=generateHtmlToPdf.CreateWorkOrderPdf(workOrdersMaster, context, pdfpath);
                workOrdersMaster.IsOfficeResult = model.IsOfficeResult;
                workOrdersMaster.IsFiledResult = model.IsFiledResult;
                custom.Data = generateHtmlToPdf.CreateWorkOrderPdf(workOrdersMaster, context, pdfpath);
                custom.HttpStatusCode = System.Net.HttpStatusCode.OK;

                AddAccesslogForPrint(workOrdersMaster);

                //objData.Add(WorkOrderPdfpath);
                return custom;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }
            return custom.Data;//objData;
        }


        private void AddAccesslogForPrint(WorkOrdersMaster workOrdersMaster)
        {
            Access_log_MasterData access_Log = new Access_log_MasterData();
            Access_log_MasterDTO dto = new Access_log_MasterDTO();
            dto.Alm_Pkey = 0;
            dto.Alm_workOrder_ID = workOrdersMaster.workOrder_ID;
            dto.Alm_userID = workOrdersMaster.UserId;
            dto.UserId = workOrdersMaster.UserId;
            dto.Alm_AccessDate = DateTime.Now;
            dto.Alm_PrevStatusDate = DateTime.Now;
            dto.Alm_Status = string.IsNullOrEmpty(workOrdersMaster.status) || string.IsNullOrWhiteSpace(workOrdersMaster.status) ? 0 : Convert.ToInt32(workOrdersMaster.status);
            dto.Alm_IsActive = true;
            dto.Alm_IsDelete = false;
            dto.Type = 1;


            //New Access Log 
            NewAccessLogMaster newAccessLogMaster = new NewAccessLogMaster();
            newAccessLogMaster.Access_WorkerOrderID = Convert.ToInt64(workOrdersMaster.workOrder_ID);
            newAccessLogMaster.Access_UserID = workOrdersMaster.UserId;
            newAccessLogMaster.Type = 1;


            if (workOrdersMaster.Type == 1)
            {
                dto.Alm_Remark = "Contractor Bids print";
                dto.Alm_log = dto.Alm_Remark;

                newAccessLogMaster.Access_Master_ID = 46;
                access_Log.AddNewAccessLogMaster(newAccessLogMaster);
            }
            else if (workOrdersMaster.Type == 2)
            {
                dto.Alm_Remark = "Contractor Completion print";
                dto.Alm_log = dto.Alm_Remark;

                newAccessLogMaster.Access_Master_ID = 47;
                access_Log.AddNewAccessLogMaster(newAccessLogMaster);
            }
            else if (workOrdersMaster.Type == 3)
            {
                dto.Alm_Remark = "Contractor Damage print";
                dto.Alm_log = dto.Alm_Remark;

                newAccessLogMaster.Access_Master_ID = 48;
                access_Log.AddNewAccessLogMaster(newAccessLogMaster);
            }
            else if (workOrdersMaster.Type == 5)
            {
                dto.Alm_Remark = "Contractor Violation print";
                dto.Alm_log = dto.Alm_Remark;

                newAccessLogMaster.Access_Master_ID = 50;
                access_Log.AddNewAccessLogMaster(newAccessLogMaster);
            }
            else if (workOrdersMaster.Type == 6)
            {
                dto.Alm_Remark = "Contractor Hazard print";
                dto.Alm_log = dto.Alm_Remark;

                newAccessLogMaster.Access_Master_ID = 49;
                access_Log.AddNewAccessLogMaster(newAccessLogMaster);
            }
            else if (workOrdersMaster.Type == 4)
            {
                newAccessLogMaster.Access_Master_ID = 55;
                access_Log.AddNewAccessLogMaster(newAccessLogMaster);
            }

            if (!string.IsNullOrEmpty(dto.Alm_Remark))
            {
                access_Log.AddAccess_log_Master(dto);
            }



        }
    }
}