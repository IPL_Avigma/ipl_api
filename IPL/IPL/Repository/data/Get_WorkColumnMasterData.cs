﻿using Avigma.Repository.Lib;
using IPLApp.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using AutoMapper;
using IPL.Models;

namespace IPL.Repository.data
{
    public class Get_WorkColumnMasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        private DataSet Get_Work_ColumnMaster(Get_WorkColumnMasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Work_Column_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserId);
                input_parameters.Add("@CompanyId", 1 + "#bigint#" + model.CompanyId);
                input_parameters.Add("@Wo_Column_PkeyId", 1 + "#bigint#" + model.Wo_Column_PkeyId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> Get_Work_ColumnMasterDetails(Get_WorkColumnMasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = Get_Work_ColumnMaster(model);
                if (ds.Tables.Count > 0)
                {
                    for (int i = 0; i < ds.Tables.Count; i++)
                    {
                        objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                    }
                }



            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}