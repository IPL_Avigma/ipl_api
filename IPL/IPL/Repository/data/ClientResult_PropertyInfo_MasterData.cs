﻿using Avigma.Repository.Lib;
using IPL.Models;
using IPLApp.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class ClientResult_PropertyInfo_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        private List<dynamic> AddClientResult_PropertyInfoData(ClientResult_PropertyInfo_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            //List<dynamic> objclntData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_ClientResult_PropertyInfo_Master]";
            ClientResult_PropertyInfo_Master clientResult_PropertyInfo_Master = new ClientResult_PropertyInfo_Master();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@CRPI_PkeyID", 1 + "#bigint#" + model.CRPI_PkeyID);
                input_parameters.Add("@CRPI_WO_ID", 1 + "#bigint#" + model.CRPI_WO_ID);
                input_parameters.Add("@CRPI_LockCode", 1 + "#nvarchar#" + model.CRPI_LockCode);
                input_parameters.Add("@CRPI_LockBox", 1 + "#nvarchar#" + model.CRPI_LockBox);
                input_parameters.Add("@CRPI_LotSize", 1 + "#nvarchar#" + model.CRPI_LotSize);
                input_parameters.Add("@CRPI_InitDefaultDate", 1 + "#datetime#" + model.CRPI_InitDefaultDate);
                input_parameters.Add("@CRPI_PtvDate", 1 + "#datetime#" + model.CRPI_PtvDate);
                input_parameters.Add("@CRPI_InitSecureDate", 1 + "#datetime#" + model.CRPI_InitSecureDate);
                input_parameters.Add("@CRPI_DidRecDate", 1 + "#datetime#" + model.CRPI_DidRecDate);
                input_parameters.Add("@CRPI_IsActive", 1 + "#bit#" + model.CRPI_IsActive);
                input_parameters.Add("@CRPI_IsDelete", 1 + "#bit#" + model.CRPI_IsDelete);


                input_parameters.Add("@CRPI_OrCovDate", 1 + "#datetime#" + model.CRPI_OrCovDate);
                input_parameters.Add("@CRPI_ExtReqDate", 1 + "#datetime#" + model.CRPI_ExtReqDate);
                input_parameters.Add("@CRPI_NewCovDate", 1 + "#datetime#" + model.CRPI_NewCovDate);
                input_parameters.Add("@CRPI_ExtReq", 1 + "#bit#" + model.CRPI_ExtReq);


                input_parameters.Add("@CRPI_Gason", 1 + "#bit#" + model.CRPI_Gason);
                input_parameters.Add("@CRPI_Wateron", 1 + "#bit#" + model.CRPI_Wateron);
                input_parameters.Add("@CRPI_Elcton", 1 + "#bit#" + model.CRPI_Elcton);
                input_parameters.Add("@CRPI_GasLR", 1 + "#nvarchar#" + model.CRPI_GasLR);
                input_parameters.Add("@CRPI_GasTS", 1 + "#nvarchar#" + model.CRPI_GasTS);
                input_parameters.Add("@CRPI_WaterLR", 1 + "#nvarchar#" + model.CRPI_WaterLR);
                input_parameters.Add("@CRPI_WaterTS", 1 + "#nvarchar#" + model.CRPI_WaterTS);
                input_parameters.Add("@CRPI_ElctLR", 1 + "#nvarchar#" + model.CRPI_ElctLR);
                input_parameters.Add("@CRPI_ElctTS", 1 + "#nvarchar#" + model.CRPI_ElctTS);

                #region comment
                //input_parameters.Add("@CRPI_ICC", 1 + "#bit#" + model.CRPI_ICC);
                //input_parameters.Add("@CRPI_ICCDate", 1 + "#datetime#" + model.CRPI_ICCDate);
                //input_parameters.Add("@CRPI_DaysInDefault", 1 + "#nvarchar#" + model.CRPI_DaysInDefault);
                //input_parameters.Add("@CRPI_VPRRequired", 1 + "#bit#" + model.CRPI_VPRRequired);
                //input_parameters.Add("@CRPI_VPRField", 1 + "#bit#" + model.CRPI_VPRField);
                //input_parameters.Add("@CRPI_VPRExpDate", 1 + "#datetime#" + model.CRPI_VPRExpDate);
                //input_parameters.Add("@CRPI_RecurringDate", 1 + "#datetime#" + model.CRPI_RecurringDate);
                //input_parameters.Add("@CRPI_BrokerInfo", 1 + "#nvarchar#" + model.CRPI_BrokerInfo);
                //input_parameters.Add("@CRPI_LoanNumber", 1 + "#nvarchar#" + model.CRPI_LoanNumber);
                //input_parameters.Add("@CRPI_LoanType", 1 + "#nvarchar#" + model.CRPI_LoanType);
                //input_parameters.Add("@CRPI_Mortgagor", 1 + "#nvarchar#" + model.CRPI_Mortgagor);
                //input_parameters.Add("@CRPI_Loan_Status", 1 + "#bigint#" + model.CRPI_Loan_Status);
                //input_parameters.Add("@CRPI_Client", 1 + "#nvarchar#" + model.CRPI_Client);
                //input_parameters.Add("@CRPI_Customer", 1 + "#nvarchar#" + model.CRPI_Customer);
                #endregion

                input_parameters.Add("@CRPI_Property_Status", 1 + "#bigint#" + model.CRPI_Property_Status);
                input_parameters.Add("@CRPI_Front_Of_HouseImagePath", 1 + "#nvarchar#" + model.CRPI_Front_Of_HouseImagePath);
                input_parameters.Add("@CRPI_Front_Of_HouseImageName", 1 + "#nvarchar#" + model.CRPI_Front_Of_HouseImageName);

                input_parameters.Add("@CRPI_Occupanct_Status", 1 + "#bigint#" + model.CRPI_Occupanct_Status);


                input_parameters.Add("@CRPI_Property_Locked", 1 + "#bit#" + model.CRPI_Property_Locked);


                input_parameters.Add("@CRPI_Property_Alert", 1 + "#nvarchar#" + model.CRPI_Property_Alert);
                input_parameters.Add("@CRPI_Property_Type", 1 + "#bigint#" + model.CRPI_Property_Type);
                input_parameters.Add("@CRPI_GPS_Latitude", 1 + "#nvarchar#" + model.CRPI_GPS_Latitude);
                input_parameters.Add("@CRPI_GPS_longitude", 1 + "#nvarchar#" + model.CRPI_GPS_longitude);
                input_parameters.Add("@CRPI_VPSCode", 1 + "#nvarchar#" + model.CRPI_VPSCode);


                input_parameters.Add("@CRPI_PropertyLockReason", 1 + "#nvarchar#" + model.CRPI_PropertyLockReason);
                input_parameters.Add("@CRPI_Winterized", 1 + "#bit#" + model.CRPI_Winterized);


                input_parameters.Add("@CRPI_WinterizedDate", 1 + "#datetime#" + model.CRPI_WinterizedDate);
                input_parameters.Add("@CRPI_ConveyanceCondition", 1 + "#nvarchar#" + model.CRPI_ConveyanceCondition);
                input_parameters.Add("@CRPI_OccupancyDate", 1 + "#datetime#" + model.CRPI_OccupancyDate);



                input_parameters.Add("@CRPI_Stop_Work_Date", 1 + "#datetime#" + model.CRPI_Stop_Work_Date);
                input_parameters.Add("@CRPI_Stop_Work_Reason", 1 + "#nvarchar#" + model.CRPI_Stop_Work_Reason);
                input_parameters.Add("@CRPI_DaysInDefault", 1 + "#nvarchar#" + model.CRPI_DaysInDefault);
                input_parameters.Add("@CRPI_VPRExpirationDate", 1 + "#datetime#" + model.CRPI_VPRExpirationDate);
                input_parameters.Add("@CRPI_VPRFiled", 1 + "#nvarchar#" + model.CRPI_VPRFiled);
                input_parameters.Add("@CRPI_ConfirmedSaleDate", 1 + "#datetime#" + model.CRPI_ConfirmedSaleDate);
                input_parameters.Add("@CRPI_REODate", 1 + "#datetime#" + model.CRPI_REODate);
                input_parameters.Add("@CRPI_FirstInspectionDate", 1 + "#datetime#" + model.CRPI_FirstInspectionDate);

                input_parameters.Add("@CRPI_LockChangeDate", 1 + "#datetime#" + model.CRPI_LockChangeDate);
                input_parameters.Add("@CRPI_LastGrasscutDate", 1 + "#datetime#" + model.CRPI_LastGrasscutDate);
                input_parameters.Add("@CRPI_ForeclosureSaleDate", 1 + "#datetime#" + model.CRPI_ForeclosureSaleDate);
                input_parameters.Add("@CRPI_DeedRecordedDate", 1 + "#datetime#" + model.CRPI_DeedRecordedDate);
                input_parameters.Add("@CRPI_RoutingDate", 1 + "#datetime#" + model.CRPI_RoutingDate);
                input_parameters.Add("@CRPI_ICC", 1 + "#bit#" + model.CRPI_ICC);
                input_parameters.Add("@CRPI_ICCDate", 1 + "#datetime#" + model.CRPI_ICCDate);

                input_parameters.Add("@CRPI_DateLoanFellOutOfICC", 1 + "#datetime#" + model.CRPI_DateLoanFellOutOfICC);
                input_parameters.Add("@CRPI_LatestICCDate", 1 + "#datetime#" + model.CRPI_LatestICCDate);
                input_parameters.Add("@CRPI_ConveyanceDueDate", 1 + "#datetime#" + model.CRPI_ConveyanceDueDate);
                input_parameters.Add("@CRPI_ExtensionApprovalDate", 1 + "#datetime#" + model.CRPI_ExtensionApprovalDate);
                input_parameters.Add("@CRPI_NewConveyanceDueDate", 1 + "#datetime#" + model.CRPI_NewConveyanceDueDate);
                input_parameters.Add("@CRPI_OrgEstimatedClosingDate", 1 + "#datetime#" + model.CRPI_OrgEstimatedClosingDate);
                input_parameters.Add("@CRPI_Lot_Size_Pricing", 1 + "#nvarchar#" + model.CRPI_Lot_Size_Pricing);


                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@CRPI_PkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                //if (objData[1] == 0)
                //{
                //    clientResult_PropertyInfo_Master.CRPI_PkeyID = "0";
                //    clientResult_PropertyInfo_Master.Status = "0";
                //    clientResult_PropertyInfo_Master.ErrorMessage = "Error while Saviving in the Database";
                //}
                //else
                //{
                //    clientResult_PropertyInfo_Master.CRPI_PkeyID = Convert.ToString(objData[0]);
                //    clientResult_PropertyInfo_Master.Status = Convert.ToString(objData[1]);
                //}
               // objclntData.Add(clientResult_PropertyInfo_Master);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;



        }
        private List<dynamic> AddPropertyInfo_Alert_RelationShipMaster(PropertyInfo_Alert_RelationShipMasterDTO model)
        {
            List<dynamic> objclntData = new List<dynamic>();
            //string insertProcedure = "[CreateUpdate_ClientResult_PropertyInfo_Master]";
            string insertProcedure = "[CreateUpdate_PropertyInfo_Alert_RelationShipMaster]";
            ClientResult_PropertyInfo_Master clientResult_PropertyInfo_Master = new ClientResult_PropertyInfo_Master();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@PIAR_PkeyID", 1 + "#bigint#" + model.PIAR_PkeyID);
                input_parameters.Add("@PIAR_CRPI_PkeyID", 1 + "#bigint#" + model.PIAR_CRPI_PkeyID);
                input_parameters.Add("@PIAR_PA_PkeyID", 1 + "#bigint#" + model.PIAR_PA_PkeyID);
                input_parameters.Add("@PIAR_AddressID", 1 + "#bigint#" + model.PIAR_AddressID);
                input_parameters.Add("@PIAR_IsActive", 1 + "#bit#" + model.PIAR_IsActive);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@PIAR_Pkey_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objclntData = obj.SqlCRUD(insertProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objclntData;
        }


            public List<dynamic> AddClientResultPropertyInfoData(ClientResult_PropertyInfo_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objclntData = new List<dynamic>();
            try
            {
                
                objData = AddClientResult_PropertyInfoData(model);
                ClientResult_PropertyInfo_Master clientResult_PropertyInfo_Master = new ClientResult_PropertyInfo_Master();
                if (objData[1] == 0)
                {
                    clientResult_PropertyInfo_Master.CRPI_PkeyID = "0";
                    clientResult_PropertyInfo_Master.Status = "0";
                    clientResult_PropertyInfo_Master.ErrorMessage = "Error while Saviving in the Database";
                }
                else
                {
                    clientResult_PropertyInfo_Master.CRPI_PkeyID = Convert.ToString(objData[0]);
                    clientResult_PropertyInfo_Master.Status = Convert.ToString(objData[1]);

                    if (!string.IsNullOrWhiteSpace(model.CRPI_Property_Alert) && model.CRPI_Property_Alert != "[]")
                    {
                        var Data = JsonConvert.DeserializeObject<List<Property_Alert_MasterDTO>>(model.CRPI_Property_Alert);
                        for (int i = 0; i < Data.Count; i++)
                        {
                            PropertyInfo_Alert_RelationShipMasterDTO propertyInfo_Alert_RelationShipMasterDTO = new PropertyInfo_Alert_RelationShipMasterDTO();
                            propertyInfo_Alert_RelationShipMasterDTO.PIAR_CRPI_PkeyID = Convert.ToInt64(clientResult_PropertyInfo_Master.CRPI_PkeyID);
                            propertyInfo_Alert_RelationShipMasterDTO.PIAR_PA_PkeyID = Data[i].PA_PkeyID;
                            propertyInfo_Alert_RelationShipMasterDTO.UserID = model.UserID;
                            propertyInfo_Alert_RelationShipMasterDTO.Type = 1;
                            AddPropertyInfo_Alert_RelationShipMaster(propertyInfo_Alert_RelationShipMasterDTO);
                        }

                    }
                }
                
                objclntData.Add(clientResult_PropertyInfo_Master);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objclntData;
        }

            private DataSet GetClientResult_PropertyInfo(ClientResult_PropertyInfo_MasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_ClientResult_PropertyInfo_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@CRPI_PkeyID", 1 + "#bigint#" + model.CRPI_PkeyID);
                input_parameters.Add("@CRPI_WO_ID", 1 + "#bigint#" + model.CRPI_WO_ID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserID);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetClientResult_PropertyInfoData(ClientResult_PropertyInfo_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetClientResult_PropertyInfo(model);

                //Dynamic Model binding 17-01-2023
                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    if (model.Type != 3)
                    {
                        objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                    }
                    else
                    {
                        //if (i > 0)
                        //{
                        //    objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                        //}
                        objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                    }

                }


                //this working code commented on 17-01-2023  

                //if (ds.Tables.Count > 0 && model.Type != 3)
                //{
                //    var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                //    List<ClientResult_PropertyInfo_MasterDTO> piData =
                //       (from item in myEnumerableFeaprd
                //        select new ClientResult_PropertyInfo_MasterDTO
                //        {

                //            CRPI_PkeyID = item.Field<Int64>("CRPI_PkeyID"),
                //            CRPI_WO_ID = item.Field<Int64>("CRPI_WO_ID"),
                //            CRPI_LockCode = item.Field<String>("CRPI_LockCode"),
                //            CRPI_LockBox = item.Field<String>("CRPI_LockBox"),
                //            CRPI_LotSize = item.Field<String>("CRPI_LotSize"),
                //            CRPI_InitDefaultDate = item.Field<DateTime?>("CRPI_InitDefaultDate"),
                //            CRPI_PtvDate = item.Field<DateTime?>("CRPI_PtvDate"),
                //            CRPI_InitSecureDate = item.Field<DateTime?>("CRPI_InitSecureDate"),
                //            CRPI_DidRecDate = item.Field<DateTime?>("CRPI_DidRecDate"),
                //            CRPI_IsActive = item.Field<Boolean>("CRPI_IsActive"),
                //            CRPI_OrCovDate = item.Field<DateTime?>("CRPI_OrCovDate"),
                //            CRPI_ExtReqDate = item.Field<DateTime?>("CRPI_ExtReqDate"),
                //            CRPI_NewCovDate = item.Field<DateTime?>("CRPI_NewCovDate"),
                //            CRPI_ExtReq = item.Field<Boolean>("CRPI_ExtReq"),
                //            CRPI_VPSCode = item.Field<String>("CRPI_VPSCode"),
                //            CRPI_ConveyanceCondition = item.Field<String>("CRPI_ConveyanceCondition"),
                //            CRPI_Client = item.Field<String>("CRPI_Client"),
                //            CRPI_Customer = item.Field<String>("CRPI_Customer"),
                //            CRPI_Winterized = item.Field<String>("CRPI_Winterized"),
                //            CRPI_WinterizedDate = item.Field<DateTime?>("CRPI_WinterizedDate"),
                //            CRPI_PropertyLockReason = item.Field<Int64?>("CRPI_PropertyLockReason"),
                //            CRPI_OccupancyDate = item.Field<DateTime?>("CRPI_OccupancyDate"),
                //            CRPI_Gason = item.Field<bool?>("CRPI_Gason"),
                //            CRPI_GasTS = item.Field<string>("CRPI_GasTS"),
                //            CRPI_GasLR = item.Field<string>("CRPI_GasLR"),
                //            CRPI_Wateron = item.Field<bool?>("CRPI_Wateron"),
                //            CRPI_WaterTS = item.Field<string>("CRPI_WaterTS"),
                //            CRPI_WaterLR = item.Field<string>("CRPI_WaterLR"),
                //            CRPI_Elcton = item.Field<bool?>("CRPI_Elcton"),
                //            CRPI_ElctLR = item.Field<string>("CRPI_ElctLR"),
                //            CRPI_ElctTS = item.Field<string>("CRPI_ElctTS")
                //        }).ToList();

                //    objDynamic.Add(piData);
                //}
                //else if (model.Type == 3)
                //{
                //    if (ds.Tables.Count > 1)
                //    {
                //        var myEnumerableFeaprd = ds.Tables[1].AsEnumerable();
                //        List<ClientResult_PropertyInfo_MasterDTO> piData =
                //           (from item in myEnumerableFeaprd
                //            select new ClientResult_PropertyInfo_MasterDTO
                //            {

                //                CRPI_PkeyID = item.Field<Int64>("CRPI_PkeyID"),
                //                CRPI_WO_ID = item.Field<Int64>("CRPI_WO_ID"),
                //                CRPI_LockCode = item.Field<String>("CRPI_LockCode"),
                //                CRPI_LockBox = item.Field<String>("CRPI_LockBox"),
                //                CRPI_LotSize = item.Field<String>("CRPI_LotSize"),
                //                CRPI_InitDefaultDate = item.Field<DateTime?>("CRPI_InitDefaultDate"),
                //                CRPI_PtvDate = item.Field<DateTime?>("CRPI_PtvDate"),
                //                CRPI_InitSecureDate = item.Field<DateTime?>("CRPI_InitSecureDate"),
                //                CRPI_DidRecDate = item.Field<DateTime?>("CRPI_DidRecDate"),
                //                CRPI_IsActive = item.Field<Boolean>("CRPI_IsActive"),
                //                CRPI_OrCovDate = item.Field<DateTime?>("CRPI_OrCovDate"),
                //                CRPI_ExtReqDate = item.Field<DateTime?>("CRPI_ExtReqDate"),
                //                CRPI_NewCovDate = item.Field<DateTime?>("CRPI_NewCovDate"),
                //                CRPI_ExtReq = item.Field<Boolean>("CRPI_ExtReq"),
                //                CRPI_Occupanct_Status = item.Field<Int64>("CRPI_Occupanct_Status"),
                //                CRPI_Property_Locked = item.Field<String>("CRPI_Property_Locked"),
                //                CRPI_Property_Alert = item.Field<Int64>("CRPI_Property_Alert"),
                //                CRPI_Property_Type = item.Field<Int64>("CRPI_Property_Type"),
                //                CRPI_GPS_Latitude = item.Field<String>("CRPI_GPS_Latitude"),
                //                CRPI_GPS_longitude = item.Field<String>("CRPI_GPS_longitude"),
                //                CRPI_VPSCode = item.Field<String>("CRPI_VPSCode"),
                //                CRPI_ConveyanceCondition = item.Field<String>("CRPI_ConveyanceCondition"),
                //                CRPI_Client = item.Field<String>("CRPI_Client"),
                //                CRPI_Customer = item.Field<String>("CRPI_Customer"),
                //                CRPI_Winterized = item.Field<String>("CRPI_Winterized"),
                //                CRPI_WinterizedDate = item.Field<DateTime?>("CRPI_WinterizedDate"),
                //                CRPI_PropertyLockReason = item.Field<Int64>("CRPI_PropertyLockReason"),
                //                CRPI_OccupancyDate = item.Field<DateTime?>("CRPI_OccupancyDate"),

                //                CRPI_Gason = item.Field<bool?>("CRPI_Gason"),
                //                CRPI_GasTS = item.Field<string>("CRPI_GasTS"),
                //                CRPI_GasLR = item.Field<string>("CRPI_GasLR"),
                //                CRPI_Wateron = item.Field<bool?>("CRPI_Wateron"),
                //                CRPI_WaterTS = item.Field<string>("CRPI_WaterTS"),
                //                CRPI_WaterLR = item.Field<string>("CRPI_WaterLR"),
                //                CRPI_Elcton = item.Field<bool?>("CRPI_Elcton"),
                //                CRPI_ElctLR = item.Field<string>("CRPI_ElctLR"),
                //                CRPI_ElctTS = item.Field<string>("CRPI_ElctTS"),
                //            }).ToList();

                //        objDynamic.Add(piData);
                //    }
                //    if (ds.Tables.Count > 2)
                //    {
                //        var myEnumerableFeaprd = ds.Tables[2].AsEnumerable();
                //        List<ClientResult_PropertyInfo_InvDTO> invData =
                //           (from item in myEnumerableFeaprd
                //            select new ClientResult_PropertyInfo_InvDTO
                //            {

                //                Inv_Con_Sub_Total = item.Field<Decimal?>("Inv_Con_Sub_Total"),
                //                Con_InvoicePaid = item.Field<Decimal?>("Con_InvoicePaid"),
                //                Client_InvoiceTotal = item.Field<Decimal?>("Client_InvoiceTotal"),
                //                Client_InvoicePaid = item.Field<Decimal?>("Client_InvoicePaid")
                //            }).ToList();

                //        objDynamic.Add(invData);
                //    }
                //    if (ds.Tables.Count > 3)
                //    {
                //        var myEnumerableFeaprd = ds.Tables[3].AsEnumerable();
                //        List<ClientResult_PropertyInfo_BidDTO> bidData =
                //           (from item in myEnumerableFeaprd
                //            select new ClientResult_PropertyInfo_BidDTO
                //            {
                //                BidCount = item.Field<Int32>("BidCount"),
                //                PendingCount = item.Field<Int32>("PendingCount"),
                //                ApproveCount = item.Field<Int32>("ApproveCount"),
                //                RejectCount = item.Field<Int32>("RejectCount")
                //            }).ToList();

                //        objDynamic.Add(bidData);
                //    }

                //}

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

    }
}