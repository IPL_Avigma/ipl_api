﻿using Avigma.Repository.Lib;
using IPL.Models;
using IPLApp.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class WorkOrder_Office_DocumentData
    {
        Log log = new Log();
        MyDataSourceFactory obj = new MyDataSourceFactory();
        private List<dynamic> AddWorkOrderOfficeDocumentData(WorkOrder_Office_DocumentDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objAddData = new List<dynamic>();
            string insertProcedure = "[CreateUpdateWorkOrder_Office_Document]";
            WorkOrder_Office_Document workOrder_Office_Document = new WorkOrder_Office_Document();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Wo_Office_Doc_PkeyId", 1 + "#bigint#" + model.Wo_Office_Doc_PkeyId);
                input_parameters.Add("@Wo_Office_Doc_Wo_ID", 1 + "#bigint#" + model.Wo_Office_Doc_Wo_ID);
                input_parameters.Add("@Wo_Office_Doc_File_Path", 1 + "#nvarchar#" + model.Wo_Office_Doc_File_Path);
                input_parameters.Add("@Wo_Office_Doc_File_Size", 1 + "#nvarchar#" + model.Wo_Office_Doc_File_Size);
                input_parameters.Add("@Wo_Office_Doc_File_Name", 1 + "#nvarchar#" + model.Wo_Office_Doc_File_Name);
                input_parameters.Add("@Wo_Office_Doc_BucketName", 1 + "#nvarchar#" + model.Wo_Office_Doc_BucketName);
                input_parameters.Add("@Wo_Office_Doc_ProjectID", 1 + "#nvarchar#" + model.Wo_Office_Doc_ProjectID);
                input_parameters.Add("@Wo_Office_Doc_Object_Name", 1 + "#nvarchar#" + model.Wo_Office_Doc_Object_Name);
                input_parameters.Add("@Wo_Office_Doc_Folder_Name", 1 + "#nvarchar#" + model.Wo_Office_Doc_Folder_Name);
                input_parameters.Add("@Wo_Office_Doc_UploadedBy", 1 + "#nvarchar#" + model.Wo_Office_Doc_UploadedBy);
                input_parameters.Add("@Wo_Folder_File_Master_FKId", 1 + "#bigint#" + model.Wo_Folder_File_Master_FKId);
                input_parameters.Add("@Wo_Office_Doc_Ch_ID", 1 + "#bigint#" + model.Wo_Office_Doc_Ch_ID);
                input_parameters.Add("@Wo_Office_Doc_Company_Id", 1 + "#bigint#" + model.Wo_Office_Doc_Company_Id);

                input_parameters.Add("@Wo_Office_Doc_IsActive", 1 + "#bit#" + model.Wo_Office_Doc_IsActive);
                input_parameters.Add("@Wo_Office_Doc_IsDelete", 1 + "#bit#" + model.Wo_Office_Doc_IsDelete);

                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Wo_Office_Doc_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    workOrder_Office_Document.Wo_Office_Doc_PkeyId = "0";
                    workOrder_Office_Document.Status = "0";
                    workOrder_Office_Document.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    workOrder_Office_Document.Wo_Office_Doc_PkeyId = Convert.ToString(objData[0]);
                    workOrder_Office_Document.Status = Convert.ToString(objData[1]);


                }
                objAddData.Add(workOrder_Office_Document);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objAddData;



        }

        // call workorder office documents

        public List<dynamic> AddUpdateWorkOrderOfficeDocsData(Client_Result_PhotoDTO client_Result_PhotoDTO)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                log.logDebugMessage("Method Called AddUpdateWorkOrderOfficeDocsData");
                WorkOrder_Office_DocumentDTO workOrder_Office_DocumentDTO = new WorkOrder_Office_DocumentDTO();

                workOrder_Office_DocumentDTO.Wo_Office_Doc_PkeyId = client_Result_PhotoDTO.Client_Result_Photo_ID;
                workOrder_Office_DocumentDTO.Wo_Office_Doc_Ch_ID = client_Result_PhotoDTO.Client_Result_Photo_Ch_ID;
                workOrder_Office_DocumentDTO.Wo_Office_Doc_Wo_ID = Convert.ToInt64(client_Result_PhotoDTO.Client_Result_Photo_Wo_ID);
                workOrder_Office_DocumentDTO.Wo_Office_Doc_File_Path = client_Result_PhotoDTO.Client_Result_Photo_FilePath;
                workOrder_Office_DocumentDTO.Wo_Office_Doc_File_Size = client_Result_PhotoDTO.Client_Result_Photo_FileSize;
                workOrder_Office_DocumentDTO.Wo_Office_Doc_File_Name = client_Result_PhotoDTO.Client_Result_Photo_FileName;
                workOrder_Office_DocumentDTO.Wo_Office_Doc_BucketName = client_Result_PhotoDTO.Client_Result_Photo_BucketName;
                workOrder_Office_DocumentDTO.Wo_Office_Doc_ProjectID = client_Result_PhotoDTO.Client_Result_Photo_ProjectID;
                workOrder_Office_DocumentDTO.Wo_Office_Doc_Object_Name = client_Result_PhotoDTO.Client_Result_Photo_objectName;
                workOrder_Office_DocumentDTO.Wo_Office_Doc_Folder_Name = client_Result_PhotoDTO.Client_Result_Photo_FolderName;
                workOrder_Office_DocumentDTO.Wo_Office_Doc_UploadedBy = Convert.ToString(client_Result_PhotoDTO.UserID);
                workOrder_Office_DocumentDTO.Wo_Folder_File_Master_FKId = client_Result_PhotoDTO.Folder_File_Master_FKId;
                workOrder_Office_DocumentDTO.Wo_Office_Doc_Company_Id = client_Result_PhotoDTO.Wo_Office_Doc_Company_Id;
                workOrder_Office_DocumentDTO.Wo_Office_Doc_IsActive = true;
                workOrder_Office_DocumentDTO.Wo_Office_Doc_IsDelete = false;


                workOrder_Office_DocumentDTO.UserID = client_Result_PhotoDTO.UserID;
                workOrder_Office_DocumentDTO.Type = 1;


                objData = AddWorkOrderOfficeDocumentData(workOrder_Office_DocumentDTO);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }



        private DataSet GetWorkOrderOfficeDocumentMaster(WorkOrder_Office_DocumentDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_WorkOrder_Office_Document]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Wo_Office_Doc_PkeyId", 1 + "#bigint#" + model.Wo_Office_Doc_PkeyId);
                input_parameters.Add("@Wo_Office_Doc_Wo_ID", 1 + "#bigint#" + model.Wo_Office_Doc_Wo_ID);
                input_parameters.Add("@Wo_Office_Doc_Company_Id", 1 + "#bigint#" + model.Wo_Office_Doc_Company_Id);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetWorkOrderOfficeDocumentDetails(WorkOrder_Office_DocumentDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetWorkOrderOfficeDocumentMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<WorkOrder_Office_DocumentDTO> WorkOrderOfficeDoc =
                   (from item in myEnumerableFeaprd
                    select new WorkOrder_Office_DocumentDTO
                    {
                        Wo_Office_Doc_PkeyId = item.Field<Int64>("Wo_Office_Doc_PkeyId"),
                        Wo_Office_Doc_Wo_ID = item.Field<Int64>("Wo_Office_Doc_Wo_ID"),
                        Wo_Office_Doc_File_Path = item.Field<String>("Wo_Office_Doc_File_Path"),
                        Wo_Office_Doc_File_Size = item.Field<String>("Wo_Office_Doc_File_Size"),
                        Wo_Office_Doc_File_Name = item.Field<String>("Wo_Office_Doc_File_Name"),
                        Wo_Office_Doc_BucketName = item.Field<String>("Wo_Office_Doc_BucketName"),
                        Wo_Office_Doc_ProjectID = item.Field<String>("Wo_Office_Doc_ProjectID"),
                        Wo_Office_Doc_Object_Name = item.Field<String>("Wo_Office_Doc_Object_Name"),
                        Wo_Office_Doc_Folder_Name = item.Field<String>("Wo_Office_Doc_Folder_Name"),
                        Wo_Office_Doc_UploadedBy = item.Field<String>("Wo_Office_Doc_UploadedBy"),
                        Wo_Office_Doc_Ch_ID = item.Field<Int64?>("Wo_Office_Doc_Ch_ID"),
                        //Wo_Office_Doc_IsActive = item.Field<Boolean?>("Wo_Office_Doc_IsActive"),


                    }).ToList();

                objDynamic.Add(WorkOrderOfficeDoc);

                var workOrderOfficeEnumerable= ds.Tables[1].AsEnumerable();
                List<WorkOrder_Office_DocumentDTO> WorkOrderOfficeDocList =
                   (from item in workOrderOfficeEnumerable
                    select new WorkOrder_Office_DocumentDTO
                    {
                        Wo_Office_Doc_PkeyId = item.Field<Int64>("Wo_Office_Doc_PkeyId"),
                        Wo_Office_Doc_Wo_ID = item.Field<Int64>("Wo_Office_Doc_Wo_ID"),
                        Wo_Office_Doc_File_Path = item.Field<String>("Wo_Office_Doc_File_Path"),
                        Wo_Office_Doc_File_Size = item.Field<String>("Wo_Office_Doc_File_Size"),
                        Wo_Office_Doc_File_Name = item.Field<String>("Wo_Office_Doc_File_Name"),
                        Wo_Office_Doc_BucketName = item.Field<String>("Wo_Office_Doc_BucketName"),
                        Wo_Office_Doc_ProjectID = item.Field<String>("Wo_Office_Doc_ProjectID"),
                        Wo_Office_Doc_Object_Name = item.Field<String>("Wo_Office_Doc_Object_Name"),
                        Wo_Office_Doc_Folder_Name = item.Field<String>("Wo_Office_Doc_Folder_Name"),
                        Wo_Office_Doc_UploadedBy = item.Field<String>("Wo_Office_Doc_UploadedBy"),
                        Wo_Office_Doc_Ch_ID = item.Field<Int64?>("Wo_Office_Doc_Ch_ID"),
                        //Wo_Office_Doc_IsActive = item.Field<Boolean?>("Wo_Office_Doc_IsActive"),


                    }).ToList();

                objDynamic.Add(WorkOrderOfficeDocList);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
        public List<dynamic> DeleteOfficeDocument(WorkOrder_Office_DocumentDTO workOrder_Office_DocumentDTO)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                workOrder_Office_DocumentDTO.Wo_Office_Doc_IsActive = false;
                workOrder_Office_DocumentDTO.Wo_Office_Doc_IsDelete = true;

                objData = AddWorkOrderOfficeDocumentData(workOrder_Office_DocumentDTO);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }
    }
}