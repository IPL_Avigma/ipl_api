﻿using Avigma.Repository.Lib;
using IPLApp.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPLApp.Repository.data
{
    public class WorkOrderFilterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        private List<dynamic> AddUpdate_WorkOrderFilterQueryMaster(WorkOrderFilterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();

            string insertProcedure = "[CreateUpdateWorkOrderFilterQueryMaster]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@WF_PkeyID", 1 + "#bigint#" + model.WF_PkeyID);
                input_parameters.Add("@WF_QueryName", 1 + "#nvarchar#" + model.WF_QueryName);
                input_parameters.Add("@WF_IsLoad", 1 + "#bit#" + model.@WF_IsLoad);
                input_parameters.Add("@WF_Query", 1 + "#nvarchar#" + model.WF_Query);
                input_parameters.Add("@WF_Json", 1 + "#nvarchar#" + model.WF_Json);
                input_parameters.Add("@WF_IsActive", 1 + "#bit#" + model.WF_IsActive);
                input_parameters.Add("@WF_IsDelete", 1 + "#bit#" + model.WF_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@WF_PkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objDynamic=  obj.SqlCRUD(insertProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }


            return objDynamic;

        }

        private DataSet Get_WorkOrderFilterQueryMaster(DropDownMasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_WorkOrderFilterQueryMaster]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetWorkOrderFilterQueryMaster(DropDownMasterDTO dropDownMasterDTO)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = Get_WorkOrderFilterQueryMaster(dropDownMasterDTO);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<WorkOrderFilter> WorkOrderFilter =
                   (from item in myEnumerableFeaprd
                    select new WorkOrderFilter
                    {
                        WF_PkeyID = item.Field<Int64>("WF_PkeyID"),
                        WF_QueryName = item.Field<String>("WF_QueryName"),
                        WF_IsLoad = item.Field<Boolean>("WF_IsLoad"),

                    }).ToList();

                objDynamic.Add(WorkOrderFilter);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;

        }

            public List<dynamic> AddUpdateWorkOrderFilterQueryMaster(WorkOrderFilterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                objDynamic = AddUpdate_WorkOrderFilterQueryMaster(model);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objDynamic;
        }

       

        }
}