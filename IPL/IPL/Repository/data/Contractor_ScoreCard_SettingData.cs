﻿using Avigma.Repository.Lib;
using IPL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class Contractor_ScoreCard_SettingData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();


        public List<dynamic> AddContractorScorecardSettingData(Contractor_ScoreCard_SettingDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();

            string insertProcedure = "[CreateUpdateContractor_Score_Setting_Master]";
            Contractor_ScoreCard contractor_ScoreCard = new Contractor_ScoreCard();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Con_score_setting_pkeyId", 1 + "#bigint#" + model.Con_score_setting_pkeyId);
                input_parameters.Add("@Con_score_setting_picturequality", 1 + "#int#" + model.Con_score_setting_picturequality);
                input_parameters.Add("@Con_score_setting_follow_inst", 1 + "#int#" + model.Con_score_setting_follow_inst);
                input_parameters.Add("@Con_score_setting_workquality", 1 + "#int#" + model.Con_score_setting_workquality);
                input_parameters.Add("@Con_score_setting_duadate", 1 + "#int#" + model.Con_score_setting_duadate);
                input_parameters.Add("@Con_score_setting_estdate", 1 + "#int#" + model.Con_score_setting_estdate);
                input_parameters.Add("@Con_score_setting_total", 1 + "#int#" + model.Con_score_setting_total);
                input_parameters.Add("@Con_score_setting_Info_needed", 1 + "#int#" + model.Con_score_setting_Info_needed);
                input_parameters.Add("@Con_score_setting_retern_property", 1 + "#int#" + model.Con_score_setting_retern_property);
                input_parameters.Add("@Con_score_setting_number_day", 1 + "#int#" + model.Con_score_setting_number_day);
                input_parameters.Add("@Con_score_setting_IsActive", 1 + "#bit#" + model.Con_score_setting_IsActive);
                input_parameters.Add("@Con_score_setting_IsDelete", 1 + "#bit#" + model.Con_score_setting_IsDelete);
                input_parameters.Add("@Con_score_setting_CompanyId", 1 + "#bigint#" + model.Con_score_setting_CompanyId);
                input_parameters.Add("@Con_score_setting_UserId", 1 + "#bigint#" + model.Con_score_setting_UserId);
                input_parameters.Add("@Con_score_setting_escalated_penalty", 1 + "#int#" + model.Con_score_setting_escalated_penalty);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Con_score_setting_pkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters); ;
                if (objData[1] == 0)
                {
                    contractor_ScoreCard.Con_score_setting_pkeyId = "0";
                    contractor_ScoreCard.Status = "0";
                    contractor_ScoreCard.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    contractor_ScoreCard.Con_score_setting_pkeyId = Convert.ToString(objData[0]);
                    contractor_ScoreCard.Status = Convert.ToString(objData[1]);


                }
                objcltData.Add(contractor_ScoreCard);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;



        }

        private DataSet GetscorecardsettingMaster(Contractor_ScoreCard_SettingDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Contractor_Score_Setting_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Con_score_setting_pkeyId", 1 + "#bigint#" + model.Con_score_setting_pkeyId);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
            }



            return ds;
        }

        //public List<dynamic> GetContractorScoreCardSettingDetails(Contractor_ScoreCard_SettingDTO model)
        //{
        //    List<dynamic> objDynamic = new List<dynamic>();
        //    try
        //    {


        //        DataSet ds = GetscorecardsettingMaster(model);

        //        var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
        //        List<Contractor_ScoreCard_SettingDTO> contractor_ScoreCard_Settingsdetails =
        //           (from item in myEnumerableFeaprd
        //            select new Contractor_ScoreCard_SettingDTO
        //            {
        //                Con_score_setting_pkeyId = item.Field<Int64>("Con_score_setting_pkeyId"),
        //                Con_score_setting_picturequality = item.Field<int?>("Con_score_setting_picturequality"),
        //                Con_score_setting_follow_inst = item.Field<int?>("Con_score_setting_follow_inst"),
        //                Con_score_setting_workquality = item.Field<int?>("Con_score_setting_workquality"),
        //                Con_score_setting_duadate = item.Field<int?>("Con_score_setting_duadate"),
        //                Con_score_setting_estdate = item.Field<int?>("Con_score_setting_estdate"),
        //                Con_score_setting_total = item.Field<int?>("Con_score_setting_total"),
        //                Con_score_setting_Info_needed = item.Field<int?>("Con_score_setting_Info_needed"),
        //                Con_score_setting_retern_property = item.Field<int?>("Con_score_setting_retern_property"),
        //                Con_score_setting_number_day = item.Field<int?>("Con_score_setting_number_day"),
        //                Con_score_setting_IsActive = item.Field<Boolean?>("Con_score_setting_IsActive"),
        //                Con_score_setting_CreatedBy = item.Field<String>("Con_score_setting_CreatedBy"),
        //                Con_score_setting_ModifiedBy = item.Field<String>("Con_score_setting_ModifiedBy"),
        //                Con_score_setting_escalated_penalty = item.Field<int?>("Con_score_setting_escalated_penalty"),

        //            }).ToList();

        //        objDynamic.Add(contractor_ScoreCard_Settingsdetails);
        //    }
        //    catch (Exception ex)
        //    {
        //        log.logErrorMessage(ex.StackTrace);
        //    }

        //    return objDynamic;
        //}

        public List<dynamic> GetContractorScoreCardSettingDetails(Contractor_ScoreCard_SettingDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetscorecardsettingMaster(model);
                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        public List<dynamic> AddContractorAccountPaySettingData(ContractorAccountPayDTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_Contractor_accounts_payable]";
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Con_Account_Pay_PkeyID", 1 + "#bigint#" + model.Con_Account_Pay_PkeyID);
                input_parameters.Add("@Inv_Payout_Criteria", 1 + "#int#" + model.Inv_Payout_Criteria);
                input_parameters.Add("@Payout_Frequency", 1 + "#int#" + model.Payout_Frequency);
                input_parameters.Add("@Next_Payout_End_Date", 1 + "#datetime#" + model.Next_Payout_End_Date);
                input_parameters.Add("@Sent_Contractor_Pay_Report", 1 + "#int#" + model.Sent_Contractor_Pay_Report);
                input_parameters.Add("@CompanyID", 1 + "#bigint#" + model.CompanyID);
                input_parameters.Add("@IsActive", 1 + "#bit#" + model.IsActive);
                input_parameters.Add("@IsDelete", 1 + "#bit#" + model.IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Con_Account_Pay_PkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters); ;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
            }
            return objData;



        }

        private DataSet GetContractorAccountPaySettingMaster(ContractorAccountPayDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Contractor_accounts_payable_settings_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Con_Account_Pay_PkeyID", 1 + "#bigint#" + model.Con_Account_Pay_PkeyID);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
            }



            return ds;
        }

        public List<dynamic> GetContractorAccountPaySettingDetails(ContractorAccountPayDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetContractorAccountPaySettingMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<ContractorAccountPayDTO> contractor_Account_Settingsdetails =
                   (from item in myEnumerableFeaprd
                    select new ContractorAccountPayDTO
                    {
                        Con_Account_Pay_PkeyID = item.Field<Int64>("Con_Account_Pay_PkeyID"),
                        Inv_Payout_Criteria = item.Field<int?>("Inv_Payout_Criteria"),
                        Payout_Frequency = item.Field<int?>("Payout_Frequency"),
                        Next_Payout_End_Date = item.Field<DateTime?>("Next_Payout_End_Date"),
                        Sent_Contractor_Pay_Report = item.Field<int?>("Sent_Contractor_Pay_Report"),
                        CompanyID = item.Field<Int64?>("CompanyID"),
                        IsActive = item.Field<Boolean?>("IsActive"),
                        CreatetedBy= item.Field<String>("CreatetedBy"),
                        ModifiedBy = item.Field<String>("ModifiedBy"),
                    }).ToList();

                objDynamic.Add(contractor_Account_Settingsdetails);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
            }

            return objDynamic;
        }


        ///Final scorecard data 

        private DataSet GetFinalScoreCardMaster(FinalScrorCardDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_WorkOrder_Final_ContractorScore]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Wo_Con_FScore_PkeyID", 1 + "#bigint#" + model.Wo_Con_FScore_PkeyID);
                input_parameters.Add("@WhereClause", 1 + "#varchar#" + model.WhereClause);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }



            return ds;
        }
        public List<dynamic> GetFinalScoreCardFilterDetails(FinalScrorCardDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();


            string wherecondition = string.Empty;
            try
            {

                var Data = JsonConvert.DeserializeObject<FinalScrorCardDTO>(model.FilterData);
                if (Data.Wo_Con_FScore_Month != "0")
                {
                    if (!string.IsNullOrEmpty(Data.Wo_Con_FScore_Month))
                    {
                        wherecondition = " And Wo_Con_FScore_Month =    '" + Data.Wo_Con_FScore_Month + "'";
                    }
                }
                if (Data.Wo_Con_FScore_Year != "0")
                {
                    if (!string.IsNullOrEmpty(Data.Wo_Con_FScore_Year))
                    {
                        wherecondition = wherecondition + " And Wo_Con_FScore_Year =    '" + Data.Wo_Con_FScore_Year + "'";
                    }
                }
            
                if (Data.Wo_Con_FScore_ConID != 0 && Data.Wo_Con_FScore_ConID != null)
                {
                    wherecondition = wherecondition + " And Wo_Con_FScore_ConID =    '" + Data.Wo_Con_FScore_ConID + "'";
                }

                if (Data.Wo_Con_FScore_IsActive == true)
                {
                    wherecondition = wherecondition + "  And Wo_Con_FScore_IsActive =  '1'";
                }
                if (Data.Wo_Con_FScore_IsActive == false)
                {
                    wherecondition = wherecondition + "  And Wo_Con_FScore_IsActive =  '0'";
                }


                FinalScrorCardDTO finalScrorCardDTO = new FinalScrorCardDTO();

                finalScrorCardDTO.WhereClause = wherecondition;
                finalScrorCardDTO.Type = 2;
                finalScrorCardDTO.UserID = model.UserID;
                DataSet ds = GetFinalScoreCardMaster(finalScrorCardDTO);

                var myEnumerableFeaprde = ds.Tables[0].AsEnumerable();
                List<FinalScrorCardDTO> Finalscorefilter =
                   (from item in myEnumerableFeaprde
                    select new FinalScrorCardDTO
                    {
                        Wo_Con_FScore_PkeyID = item.Field<Int64>("Wo_Con_FScore_PkeyID"),
                        Wo_Con_FScore_Month = item.Field<String>("Wo_Con_FScore_Month"),
                        Wo_Con_FScore_ConID = item.Field<Int64?>("Wo_Con_FScore_ConID"),
                        Wo_Con_FScore_Value = item.Field<Decimal?>("Wo_Con_FScore_Value"),
                        Cont_Name = item.Field<String>("Cont_Name"),
                        Wo_Con_Score_DueDate = item.Field<Decimal?>("Wo_Con_Score_DueDate"),
                        Wo_Con_Score_EstDate = item.Field<Decimal?>("Wo_Con_Score_EstDate"),
                        Wo_Con_Score_Follow_Inst = item.Field<Decimal?>("Wo_Con_Score_Follow_Inst"),
                        Wo_Con_Score_Picturequality = item.Field<Decimal?>("Wo_Con_Score_Picturequality"),
                        Wo_Con_Score_Work_Quality = item.Field<Decimal?>("Wo_Con_Score_Work_Quality"),
                        Wo_Con_Score_Info_Needed = item.Field<Decimal?>("Wo_Con_Score_Info_Needed"),
                        Wo_Con_FScore_Wo_Count = item.Field<int?>("Wo_Con_FScore_Wo_Count"),
                        Wo_Con_FScore_Retern_Property = item.Field<Decimal?>("Wo_Con_FScore_Retern_Property"),

                    }).ToList();

                objDynamic.Add(Finalscorefilter);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }

            return objDynamic;
        }

        private DataSet GetFinalScoreCardDRD()
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_FinalScorecardDrd]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }



            return ds;
        }

        public List<dynamic> GetCurrentScoreCardDetails(FinalScrorCardDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {

                DataSet ds = GetFinalScoreCardMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<FinalScrorCardDTO> currentData =
                   (from item in myEnumerableFeaprd
                    select new FinalScrorCardDTO
                    {
                        Wo_Con_FScore_PkeyID = item.Field<Int64>("Wo_Con_FScore_PkeyID"),
                        Wo_Con_FScore_Month = item.Field<String>("Wo_Con_FScore_Month"),
                        Wo_Con_FScore_ConID = item.Field<Int64?>("Wo_Con_FScore_ConID"),
                        Wo_Con_FScore_Value = item.Field<Decimal?>("Wo_Con_FScore_Value"),
                        Cont_Name = item.Field<String>("Cont_Name"),
                        Wo_Con_Score_DueDate = item.Field<Decimal?>("Wo_Con_Score_DueDate"),
                        Wo_Con_Score_EstDate = item.Field<Decimal?>("Wo_Con_Score_EstDate"),
                        Wo_Con_Score_Follow_Inst = item.Field<Decimal?>("Wo_Con_Score_Follow_Inst"),
                        Wo_Con_Score_Picturequality = item.Field<Decimal?>("Wo_Con_Score_Picturequality"),
                        Wo_Con_Score_Work_Quality = item.Field<Decimal?>("Wo_Con_Score_Work_Quality"),
                        Wo_Con_Score_Info_Needed = item.Field<Decimal?>("Wo_Con_Score_Info_Needed"),
                        Wo_Con_FScore_Wo_Count = item.Field<int?>("Wo_Con_FScore_Wo_Count"),
                        Wo_Con_FScore_Retern_Property = item.Field<Decimal?>("Wo_Con_FScore_Retern_Property"),

                    }).ToList();

                objDynamic.Add(currentData);
              
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
            }

            return objDynamic;
        }




        public List<dynamic>GetWorkOrderFinalContractorScoreForMobile(FinalScrorCardDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {

                DataSet ds = GetFinalScoreCardMaster(model);
                if (ds.Tables.Count > 0)
                {
                    for (int i = 0; i < ds.Tables.Count; i++)
                    {
                        objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                    }

                }

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objDynamic;
        }


        public List<dynamic> GetContractorScoreCardDropdown()
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {

                DataSet ds = GetFinalScoreCardDRD();

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<FinalScrorCardDTO> monthlist =
                   (from item in myEnumerableFeaprd
                    select new FinalScrorCardDTO
                    {
                        Wo_Con_FScore_Month = item.Field<String>("Wo_Con_FScore_Month"),

                    }).ToList();

                objDynamic.Add(monthlist);
                var myEnumerableFeaprdy = ds.Tables[1].AsEnumerable();
                List<FinalScrorCardDTO> yearlist =
                   (from item in myEnumerableFeaprdy
                    select new FinalScrorCardDTO
                    {
                        Wo_Con_FScore_Year = item.Field<String>("Wo_Con_FScore_Year"),

                    }).ToList();

                objDynamic.Add(yearlist);
                var myEnumerableFeaprdc = ds.Tables[2].AsEnumerable();
                List<FinalScrorCardDTO> contractorlist =
                   (from item in myEnumerableFeaprdc
                    select new FinalScrorCardDTO
                    {
                        Cont_Name = item.Field<String>("Cont_Name"),
                        Wo_Con_FScore_ConID = item.Field<Int64?>("Wo_Con_FScore_ConID"),

                    }).ToList();

                objDynamic.Add(contractorlist);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
            }

            return objDynamic;
        }

        private DataSet GetChildscoreData(ChildScorecardDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_ScoreCard_ChildData]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Wo_Con_Score_FScorePkeyId", 1 + "#bigint#" + model.Wo_Con_Score_FScorePkeyId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }



            return ds;
        }

        public List<dynamic> GetChildscoreDetails(ChildScorecardDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {

                DataSet ds = GetChildscoreData(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<ChildScorecardDTO> childData =
                   (from item in myEnumerableFeaprd
                    select new ChildScorecardDTO
                    {
                        workOrderNumber = item.Field<String>("workOrderNumber"),
                        IPLNO = item.Field<String>("IPLNO"),
                        WT_WorkType = item.Field<String>("WT_WorkType"),
                        Cont_Name = item.Field<String>("Cont_Name"),
                        Wo_Con_Score_Contractor_Score = item.Field<int?>("Wo_Con_Score_Contractor_Score"),
                        Wo_Con_Score_Picturequality = item.Field<int?>("Wo_Con_Score_Picturequality"),
                        Wo_Con_Score_Follow_Inst = item.Field<int?>("Wo_Con_Score_Follow_Inst"),
                        Wo_Con_Score_Work_Quality = item.Field<int?>("Wo_Con_Score_Work_Quality"),
                        Wo_Con_Score_DueDate = item.Field<int?>("Wo_Con_Score_DueDate"),
                        Wo_Con_Score_EstDate = item.Field<int?>("Wo_Con_Score_EstDate"),
                        Wo_Con_Score_Info_Needed = item.Field<int?>("Wo_Con_Score_Info_Needed"),
                        Wo_Con_Score_Retern_Property = item.Field<int?>("Wo_Con_Score_Retern_Property"),
                        Wo_Con_Score_Contractor_Date = item.Field<DateTime?>("Wo_Con_Score_Contractor_Date"),
                        Wo_Con_Score_Wo_Id = item.Field<Int64?>("Wo_Con_Score_Wo_Id"),
                    

                    }).ToList();

                objDynamic.Add(childData);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
            }

            return objDynamic;
        }


    }
}