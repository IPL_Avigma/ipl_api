﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class Folder_WorkType_Group_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        public List<dynamic> AddFolder_WorkType_Group_Master(Folder_WorkType_Group_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_Folder_WorkType_Group_Master]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Fold_WorkType_Group_PkeyId", 1 + "#bigint#" + model.Fold_WorkType_Group_PkeyId);
                input_parameters.Add("@Fold_WorkType_Group_Id", 1 + "#bigint#" + model.Fold_WorkType_Group_Id);
                input_parameters.Add("@Fold_WorkType_Group_Folder_Id", 1 + "#bigint#" + model.Fold_WorkType_Group_Folder_Id);
                input_parameters.Add("@Fold_WorkType_Group_File_Id", 1 + "#bigint#" + model.Fold_WorkType_Group_File_Id);
                input_parameters.Add("@Fold_WorkType_Group_Auto_Assine_Id", 1 + "#bigint#" + model.Fold_WorkType_Group_Auto_Assine_Id);

                input_parameters.Add("@Fold_WorkType_Group_IsActive", 1 + "#bit#" + model.Fold_WorkType_Group_IsActive);
                input_parameters.Add("@Fold_WorkType_Group_IsDelete", 1 + "#bit#" + model.Fold_WorkType_Group_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Fold_WorkType_Group_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);
                input_parameters.Add("@Folder_File_Master_FK_Id", 1 + "#bigint#" + model.Folder_File_Master_FK_Id);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;



        }
    }
}