﻿using Avigma.Repository.Lib;
using IPL.Models;
using IPLApp.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class MultipleClientInvoicePrintData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        private DataSet GetMultipleClientInvoice(MultipleClientInvoice model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Client_Invoice_Print]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Inv_Client_WO_Id", 1 + "#bigint#" + model.Inv_Client_WO_Id);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }


        public List<dynamic> PrintMultipleClientInvoice(WorkoderActionItems model)
        {
            try
            {
                List<dynamic> objdata = new List<dynamic>();
                workoderaction workoderaction = new workoderaction();
                var Data = JsonConvert.DeserializeObject<List<WorkOrderIDItems>>(model.Arr_WorkOrderID);
                string strwhere = string.Empty;
                for (int i = 0; i < Data.Count; i++)
                {
                    MultipleClientInvoice multipleClientInvoice = new MultipleClientInvoice();
                    multipleClientInvoice.Inv_Client_WO_Id = Convert.ToInt64(Data[i].WorkOrderID);
                    multipleClientInvoice.Type = 1;
                    objdata.Add(GetMultipleClientInvoiceDetails(multipleClientInvoice));
                }


                return objdata;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return null;
            }



        }


        public List<dynamic> GetMultipleClientInvoiceDetails(MultipleClientInvoice model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetMultipleClientInvoice(model);

                var myEnumerableFeaprda = ds.Tables[0].AsEnumerable();
                List<printAddressDTO> addressdata =
                   (from item in myEnumerableFeaprda
                    select new printAddressDTO
                    {
                        Ipl_PkeyID = item.Field<Int64>("Ipl_PkeyID"),
                        Ipl_companyname = item.Field<String>("Ipl_companyname"),
                        Ipl_IPLAddress = item.Field<String>("Ipl_IPLAddress"),
                        Ipl_IPLcity = item.Field<String>("Ipl_IPLcity"),
                        Ipl_IPLstate = item.Field<String>("Ipl_IPLstate"),
                        Ipl_IPLZip = item.Field<String>("Ipl_IPLZip"),
                        IPLContactnumber = item.Field<String>("IPLContactnumber"),

                    }).ToList();

                objDynamic.Add(addressdata);


                var myEnumerableFeaprdw = ds.Tables[1].AsEnumerable();
                List<MultipleClientInvoiceWo> ClientwoInvoice =
                   (from item in myEnumerableFeaprdw
                    select new MultipleClientInvoiceWo
                    {
                        workOrderNumber = item.Field<String>("workOrderNumber"),
                        Complete_Date = item.Field<DateTime?>("Complete_Date"),
                        Loan_Number = item.Field<String>("Loan_Number"),
                        Loan_Info = item.Field<String>("Loan_Info"),
                        fulladdress = item.Field<String>("fulladdress"),
                        Work_Type_Name = item.Field<String>("Work_Type_Name"),
                        ClientMetaData = item.Field<String>("ClientMetaData"),
                        ClientName = item.Field<String>("ClientName"),
                        ClientAddress = item.Field<String>("ClientAddress"),
                        ContractorAddress = item.Field<String>("ContractorAddress"),
                        IPLNO = item.Field<String>("IPLNO"),


                    }).ToList();

                objDynamic.Add(ClientwoInvoice);

                var myEnumerableFeaprd = ds.Tables[2].AsEnumerable();
                List<MultipleClient_ch_InvoicePrintDTO> ClientInvoice =
                   (from item in myEnumerableFeaprd
                    select new MultipleClient_ch_InvoicePrintDTO
                    {
                        Inv_Client_Ch_pkeyId = item.Field<Int64>("Inv_Client_Ch_pkeyId"),
                        Inv_Client_Ch_Qty = item.Field<String>("Inv_Client_Ch_Qty"),
                        Task_Name = item.Field<String>("Task_Name"),
                        Inv_Client_Ch_Price = item.Field<Decimal?>("Inv_Client_Ch_Price"),
                        Inv_Client_Ch_Total = item.Field<Decimal?>("Inv_Client_Ch_Total"),
                        Inv_Client_Ch_Adj_Price = item.Field<Decimal?>("Inv_Client_Ch_Adj_Price"),
                        Inv_Client_Ch_Adj_Total = item.Field<Decimal?>("Inv_Client_Ch_Adj_Total"),
                        Inv_Client_Ch_Discount = item.Field<int?>("Inv_Client_Ch_Discount"),


                    }).ToList();

                objDynamic.Add(ClientInvoice);

                var myEnumerableFeaprd1 = ds.Tables[3].AsEnumerable();
                List<MultipleClientInvoice> ClientmainInvoice =
                   (from item in myEnumerableFeaprd1
                    select new MultipleClientInvoice
                    {
                        Inv_Client_pkeyId = item.Field<Int64>("Inv_Client_pkeyId"),
                        //Inv_Client_WO_Id = item.Field<Int64?>("Inv_Client_WO_Id"),
                        Inv_Client_Sub_Total = item.Field<Decimal?>("Inv_Client_Sub_Total"),
                        Inv_Client_Client_Dis = item.Field<int?>("Inv_Client_Client_Dis"),
                        Inv_Client_Client_Total = item.Field<Decimal?>("Inv_Client_Client_Total"),
                        Task_Name = item.Field<String>("Task_Name"),
                        Inv_Client_Comp_Date = item.Field<DateTime?>("Inv_Client_Comp_Date"),
                        Inv_Client_Invoice_Number = item.Field<String>("Inv_Client_Invoice_Number"),
                        Inv_Client_Inv_Date = item.Field<DateTime?>("Inv_Client_Inv_Date"),
                        Inv_Client_Discout_Amount = item.Field<Decimal?>("Inv_Client_Discout_Amount"),

                    }).ToList();

                objDynamic.Add(ClientmainInvoice);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}