﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IPL.Models;
using Avigma.Repository.Lib;
using System.Data;
using Newtonsoft.Json;
using IPLApp.Models;
using IPLApp.Repository.data;

namespace IPL.Repository.data
{
    public class Contractor_Client_Expense_Payment_Data
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        //SecurityHelper SecurityHelper = new SecurityHelper();
        Log log = new Log();

        public List<dynamic> AddUpdateContractor_Client_Expense_Payment(Contractor_Client_Expense_Payment_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_Contractor_Client_Expense_Payment]";
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();

            try
            {

                input_parameters.Add("@CCE_Pay_PkeyId", 1 + "#bigint#" + model.CCE_Pay_PkeyId);
                input_parameters.Add("@CCE_Client_Pay_Invoice_Id", 1 + "#bigint#" + model.CCE_Client_Pay_Invoice_Id);
                input_parameters.Add("@CCE_Con_Pay_Invoice_Id", 1 + "#bigint#" + model.CCE_Con_Pay_Invoice_Id);
                input_parameters.Add("@CCE_Pay_Wo_Id ", 1 + "#bigint#" + model.CCE_Pay_Wo_Id);
                input_parameters.Add("@CCE_Pay_Payment_Date", 1 + "#datetime#" + model.CCE_Pay_Payment_Date);
                input_parameters.Add("@CCE_Pay_Amount", 1 + "#decimal#" + model.CCE_Pay_Amount);
                input_parameters.Add("@CCE_Pay_Comment", 1 + "#varchar#" + model.CCE_Pay_Comment);
                input_parameters.Add("@CCE_Pay_EnteredBy", 1 + "#bigint#" + model.CCE_Pay_EnteredBy);
                input_parameters.Add("@CCE_Pay_Balance_Due", 1 + "#decimal#" + model.CCE_Pay_Balance_Due);
                input_parameters.Add("@CCE_Pay_IsActive", 1 + "#bit#" + model.CCE_Pay_IsActive);
                input_parameters.Add("@CCE_Pay_IsDelete", 1 + "#bit#" + model.CCE_Pay_IsDelete);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserId);
                input_parameters.Add("@CCE_Pay_Expense_Name", 1 + "#varchar#" + model.CCE_Pay_Expense_Name);

                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@CCE_Pay_PkeyId_out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);
                objData = obj.SqlCRUD(insertProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        public List<dynamic> AddContractor_Client_Expense_Payment_Data(Contractor_Client_Expense_Payment model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                if (!string.IsNullOrWhiteSpace(model.CCEP_Data))
                {
                    var Data = JsonConvert.DeserializeObject<List<Contractor_Client_Expense_Payment_DTO>>(model.CCEP_Data);
                    for (int i = 0; i < Data.Count; i++)
                    {
                        Data[i].UserId = model.UserId;
                        Data[i].CCE_Pay_EnteredBy = model.UserId;
                        if (Data[i].CCE_Pay_PkeyId != 0)
                        {

                            if (Data[i].Type == 1)
                            {
                                Data[i].Type = 2;
                            }
                            objData = AddUpdateContractor_Client_Expense_Payment(Data[i]);
                        }
                        else
                        {
                            Data[i].Type = 1;
                            objData = AddUpdateContractor_Client_Expense_Payment(Data[i]);
                        }

                    }


                    #region New Access Log

                    Int64 WorkOrderId = Convert.ToInt64(Data[0].CCE_Pay_Wo_Id);
                    if (WorkOrderId > 0)
                    {
                        Access_log_MasterData access_Log_MasterData = new Access_log_MasterData();
                        NewAccessLogMaster newAccessLogMaster = new NewAccessLogMaster();
                        newAccessLogMaster.Access_WorkerOrderID = Convert.ToInt64(Data[0].CCE_Pay_Wo_Id);
                        newAccessLogMaster.Access_Master_ID = 45;
                        newAccessLogMaster.Access_UserID = Convert.ToInt64(model.UserId);
                        newAccessLogMaster.Type = 1;
                        access_Log_MasterData.AddNewAccessLogMaster(newAccessLogMaster);
                    }
                    #endregion
                }


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        public List<dynamic> AddContractor_Client_Expense_Payment_Data_Multiple(Contractor_Client_Expense_Payment model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                if (!string.IsNullOrWhiteSpace(model.CCEP_Data))
                {
                    var Data = JsonConvert.DeserializeObject<List<Contractor_Client_Expense_Payment_DTO>>(model.CCEP_Data);
                    for (int i = 0; i < Data.Count; i++)
                    {

                        List<Int64> workorderIdList = Data[i].WorkOrderID_mul.Split(',')?.Select(Int64.Parse).ToList();
                        foreach (Int64 workorderId in workorderIdList)
                        {
                            Data[i].CCE_Pay_Wo_Id = workorderId;
                            Data[i].UserId = model.UserId;
                            Data[i].CCE_Pay_EnteredBy = model.UserId;
                            if (Data[i].CCE_Pay_PkeyId != 0)
                            {

                                if (Data[i].Type == 1)
                                {
                                    Data[i].Type = 2;
                                }
                                objData = AddUpdateContractor_Client_Expense_Payment(Data[i]);
                            }
                            else
                            {
                                Data[i].Type = 5;
                                objData = AddUpdateContractor_Client_Expense_Payment(Data[i]);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }
        public List<dynamic> AddUpdate_SingleContractor_Client_Expense_Payment_Data(Contractor_Client_Expense_Payment_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {

                model.CCE_Pay_EnteredBy = model.UserId;

                objData = AddUpdateContractor_Client_Expense_Payment(model);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }


        private DataSet Get_Contractor_Client_Expense_Payment(Contractor_Client_Expense_Payment_DTO model)
        {
            DataSet ds = null;
            try
            {

                string selectProcedure = "[Get_Contractor_Client_Expense_Payment]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@CCE_Pay_PkeyId", 1 + "#bigint#" + model.CCE_Pay_PkeyId);
                input_parameters.Add("@CCE_Pay_Wo_Id", 1 + "#bigint#" + model.CCE_Pay_Wo_Id);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> Get_Contractor_Client_Expense_PaymentDetails(Contractor_Client_Expense_Payment_DTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = Get_Contractor_Client_Expense_Payment(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Contractor_Client_Expense_Payment_DTO> User_Child =
                   (from item in myEnumerableFeaprd
                    select new Contractor_Client_Expense_Payment_DTO
                    {
                        CCE_Pay_PkeyId = item.Field<Int64>("CCE_Pay_PkeyId"),
                        CCE_Client_Pay_Invoice_Id = item.Field<Int64?>("CCE_Client_Pay_Invoice_Id"),
                        CCE_Con_Pay_Invoice_Id = item.Field<Int64?>("CCE_Con_Pay_Invoice_Id"),
                        CCE_Pay_Wo_Id = item.Field<Int64?>("CCE_Pay_Wo_Id"),
                        CCE_Pay_Payment_Date = item.Field<DateTime?>("CCE_Pay_Payment_Date"),
                        CCE_Pay_Amount = item.Field<decimal?>("CCE_Pay_Amount"),
                        CCE_Pay_Comment = item.Field<String>("CCE_Pay_Comment"),
                        CCE_Pay_EnteredBy = item.Field<Int64?>("CCE_Pay_EnteredBy"),
                        CCE_Pay_Balance_Due = item.Field<decimal?>("CCE_Pay_Balance_Due"),
                        CCE_Pay_Expense_Name = item.Field<String>("CCE_Pay_Expense_Name"),






                    }).ToList();

                objDynamic.Add(User_Child);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}