﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class PCR_Cyprexx_Property_Condition_Checklist_Data
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        private List<dynamic> CreateUpdateCyprexxPropertyConditionChecklist(PCR_Cyprexx_Property_Condition_Checklist_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_PCR_Cyprexx_property-condition-checklist]";
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@PCR_PC_PkeyID", 1 + "#bigint#" + model.PCR_PC_PkeyID);
                input_parameters.Add("@PCR_PC_WO_ID", 1 + "#bigint#" + model.PCR_PC_WO_ID);
                input_parameters.Add("@PCR_PC_CompanyID", 1 + "#bigint#" + model.PCR_PC_CompanyID);
                input_parameters.Add("@PCR_PC_General_Property_Questions", 1 + "#nvarchar#" + model.PCR_PC_General_Property_Questions);
                input_parameters.Add("@PCR_PC_Signature", 1 + "#nvarchar#" + model.PCR_PC_Signature);
                input_parameters.Add("@PCR_PC_IsActive", 1 + "#bit#" + model.PCR_PC_IsActive);
                input_parameters.Add("@PCR_PC_IsDelete", 1 + "#bit#" + model.PCR_PC_IsDelete);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@PCR_PC_Pkey_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);
                objData = obj.SqlCRUD(insertProcedure, input_parameters);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        private DataSet GetCyprexxPropertyConditionChecklist(PCR_Cyprexx_Property_Condition_Checklist_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_PCR_Cyprexx_property-condition-checklist]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@PCR_PC_PkeyID", 1 + "#bigint#" + model.PCR_PC_PkeyID);
                input_parameters.Add("@PCR_PC_WO_ID", 1 + "#bigint#" + model.PCR_PC_WO_ID);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);

                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }


        public List<dynamic> PostCyprexxPropertyConditionChecklistDetails(PCR_Cyprexx_Property_Condition_Checklist_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                objData = CreateUpdateCyprexxPropertyConditionChecklist(model);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }
            return objData;
        }

        public List<dynamic> GetCyprexxPropertyConditionChecklistDetails(PCR_Cyprexx_Property_Condition_Checklist_DTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetCyprexxPropertyConditionChecklist(model);

                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                }

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}