﻿using Avigma.Repository.Lib;
using IPL.Models;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

//changes
namespace IPL.Repository.data
{
    public class Task_ConfigurationData
    {
        TaskSettingChildData taskSettingChildData = new TaskSettingChildData();
        Task_Company_TBLData task_Company_TBLData = new Task_Company_TBLData();
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public List<dynamic> AddTask_ConfigurationData(Task_Configuration_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objAddData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_Task_Configuration_Master]";
            Task_Configuration task_Configuration = new Task_Configuration();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Task_Configuration_PkeyId", 1 + "#bigint#" + model.Task_Configuration_PkeyId);
                input_parameters.Add("@Task_Configuration_Task_Id", 1 + "#bigint#" + model.Task_Configuration_Task_Id);
                input_parameters.Add("@Task_Configuration_LoanType", 1 + "#nvarchar#" + model.Task_Configuration_LoanType);
                input_parameters.Add("@Task_Configuration_ItemCode_Id", 1 + "#bigint#" + model.Task_Configuration_ItemCode_Id);
                input_parameters.Add("@Task_Configuration_BidCategory_Id", 1 + "#bigint#" + model.Task_Configuration_BidCategory_Id);
                input_parameters.Add("@Task_Configuration_BidDamage_Id", 1 + "#bigint#" + model.Task_Configuration_BidDamage_Id);
                input_parameters.Add("@Task_Configuration_CategoryCode_Id", 1 + "#bigint#" + model.Task_Configuration_CategoryCode_Id);
                input_parameters.Add("@Task_Configuration_Import_From_Id", 1 + "#bigint#" + model.Task_Configuration_Import_From_Id);
                input_parameters.Add("@Task_Configuration_Client_Id", 1 + "#bigint#" + model.Task_Configuration_Client_Id);
                input_parameters.Add("@Task_Configuration_Company_Id", 1 + "#bigint#" + model.Task_Configuration_Company_Id);
                input_parameters.Add("@Task_Configuration_User_Id", 1 + "#bigint#" + model.Task_Configuration_User_Id);
                input_parameters.Add("@Task_Configuration_IsActive", 1 + "#bit#" + model.Task_Configuration_IsActive);
                input_parameters.Add("@Task_Configuration_IsDelete", 1 + "#bit#" + model.Task_Configuration_IsDelete);
                input_parameters.Add("@Task_Configuration_ItemCode", 1 + "#nvarchar#" + model.Task_Configuration_ItemCode);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Task_Configuration_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    task_Configuration.Task_Configuration_PkeyId = "0";
                    task_Configuration.Status = "0";
                    task_Configuration.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    task_Configuration.Task_Configuration_PkeyId = Convert.ToString(objData[0]);
                    task_Configuration.Status = Convert.ToString(objData[1]);
                    //var filterObj = AddTaskConfigure_CustomFilter(model.Task_Configuration_Task_Id.GetValueOrDefault(0), Convert.ToInt64(objData[0]), model);          

                }
                objAddData.Add(task_Configuration);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objAddData;



        }

        private DataSet GetTask_ConfigurationMaster(Task_Configuration_MasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Task_Configuration_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Task_Configuration_PkeyId", 1 + "#bigint#" + model.Task_Configuration_PkeyId);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Task_Configuration_Company_Id", 1 + "#bigint#" + model.Task_Configuration_Company_Id);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return ds;
        }

        public List<dynamic> GetTask_ConfigurationMasterDetails(Task_Configuration_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetTask_ConfigurationMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Task_Configuration_MasterDTO> Task_Configuration =
                   (from item in myEnumerableFeaprd
                    select new Task_Configuration_MasterDTO
                    {
                        Task_Configuration_PkeyId = item.Field<Int64>("Task_Configuration_PkeyId"),
                        Task_Configuration_Task_Id = item.Field<Int64?>("Task_Configuration_Task_Id"),
                        Task_Configuration_LoanType = item.Field<String>("Task_Configuration_LoanType"),
                        Task_Configuration_ItemCode_Id = item.Field<Int64?>("Task_Configuration_ItemCode_Id"),
                        Task_Configuration_BidCategory_Id = item.Field<Int64?>("Task_Configuration_BidCategory_Id"),
                        Task_Configuration_BidDamage_Id = item.Field<Int64?>("Task_Configuration_BidDamage_Id"),
                        Task_Configuration_CategoryCode_Id = item.Field<Int64?>("Task_Configuration_CategoryCode_Id"),
                        Task_Configuration_Import_From_Id = item.Field<Int64?>("Task_Configuration_Import_From_Id"),
                        Task_Configuration_Client_Id = item.Field<Int64?>("Task_Configuration_Client_Id"),
                        Task_Configuration_Company_Id = item.Field<Int64?>("Task_Configuration_Company_Id"),
                        Task_Configuration_User_Id = item.Field<Int64?>("Task_Configuration_User_Id"),
                        Task_Configuration_IsActive = true,
                        Task_Configuration_ItemCode = item.Field<String>("Task_Configuration_ItemCode"),
                        Task_Configuration_ItemCode_Desc = item.Field<String>("Task_Configuration_ItemCode_Desc"),
                        Task_Configuration_CreatedBy = item.Field<String>("Task_Configuration_CreatedBy"),
                        Task_Configuration_ModifiedBy = item.Field<String>("Task_Configuration_ModifiedBy"),


                    }).ToList();

                objDynamic.Add(Task_Configuration);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        public List<dynamic> PostTaskConfigurationMasterDetails(Task_Configuration_ListDTO modelList)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                foreach (var model in modelList.Task_Configuration_List)
                {
                    model.Type = model.Task_Configuration_PkeyId > 0 ? 2 : 1;
                    model.Task_Configuration_IsActive = true;
                    model.Task_Configuration_IsDelete = false;
                    if (model.Task_Configuration_Task_Id > 0)
                    {
                        model.Task_Configuration_Import_From_Id = 4;
                        var objData = AddTask_ConfigurationData(model);
                        objDynamic.Add(objData);
                    }
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        public List<dynamic> AddTaskConfigure_CustomFilter(Int64 taskId, Int64 taskConfigurationId, Task_Configuration_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            TaskSettingChildDTO taskSettingChildDTO = new TaskSettingChildDTO();
            var Task_sett_Company = new List<TaskSettCompany>();
            TaskSettCompany company = new TaskSettCompany();
            company.Client_Company_Name = "Five Brothers";
            company.Client_pkeyID = 127;
            Task_sett_Company.Add(company);

            taskSettingChildDTO.Task_sett_pkeyID = 0;
            taskSettingChildDTO.Task_sett_ID = taskId;
            taskSettingChildDTO.Task_sett_Company = JsonConvert.SerializeObject(Task_sett_Company);
            taskSettingChildDTO.Task_sett_State = "[]";
            taskSettingChildDTO.Task_sett_Country = "[]";
            taskSettingChildDTO.Task_sett_Contractor = "[]";
            taskSettingChildDTO.Task_sett_Customer = "[]";
            taskSettingChildDTO.Task_sett_Lone = "[]";
            taskSettingChildDTO.Task_sett_Con_Unit_Price = string.IsNullOrEmpty(model.Task_Configuration_ItemCode_Price) ? 0 : Convert.ToDecimal(model.Task_Configuration_ItemCode_Price);
            taskSettingChildDTO.Task_sett_CLI_Unit_Price = string.IsNullOrEmpty(model.Task_Configuration_ItemCode_Price) ? 0 : Convert.ToDecimal(model.Task_Configuration_ItemCode_Price);
            taskSettingChildDTO.Task_sett_Flat_Free = false;
            taskSettingChildDTO.Task_sett_Price_Edit = false;
            taskSettingChildDTO.Task_sett_Disable_Default = false;
            taskSettingChildDTO.Task_Work_TypeGroup = "[]";
            taskSettingChildDTO.Task_sett_IsActive = true;
            taskSettingChildDTO.Task_sett_IsDelete = false;
            taskSettingChildDTO.UserID = model.UserID;
            taskSettingChildDTO.Type = 5;
            taskSettingChildDTO.Task_sett_WorkType = "[]";
            taskSettingChildDTO.Task_sett_LOT_Min = 0;
            taskSettingChildDTO.Task_sett_LOT_Max = 0;
            taskSettingChildDTO.Task_sett_MapTaskFkeyId = taskConfigurationId;
            var objData = taskSettingChildData.AddTaskSettingChildData(taskSettingChildDTO);

            if (objData[0].Status != "0")
            {
                objDynamic.Add(objData);
                if (Task_sett_Company != null && Task_sett_Company.Count > 0)
                {
                    for (int p = 0; p < Task_sett_Company.Count; p++)
                    {
                        Task_Company_TBLDTO task_Company_TBLDTO = new Task_Company_TBLDTO();
                        task_Company_TBLDTO.TC_pkeyID = 0;
                        task_Company_TBLDTO.TC_Task_pkeyID = taskId;
                        task_Company_TBLDTO.TC_Task_sett_ID = Task_sett_Company[p].Client_pkeyID;
                        task_Company_TBLDTO.TC_Task_sett_pkeyID = Convert.ToInt64(objData[0].Task_sett_pkeyID); ;
                        task_Company_TBLDTO.TC_IsActive = true;
                        task_Company_TBLDTO.TC_IsDelete = false;
                        task_Company_TBLDTO.UserID = model.UserID;
                        task_Company_TBLDTO.Type = 1;

                        var returnkeyCompany = task_Company_TBLData.AddTaskCompanyData(task_Company_TBLDTO);
                    }
                }
            }
            return objDynamic;
        }
    }
}