﻿using Avigma.Repository.Lib;
using IPLApp.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPLApp.Repository.data
{
    public class WorkOrderCustomizeData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public List<dynamic> AddWorkOrderCustomizedetails(WorkOrderCustomizeDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objuserData = new List<dynamic>();
            string insertProcedure = "[CreateUpdateWorkOrderCutomize]";
            WorkOrderCustomize workOrderCustomize = new WorkOrderCustomize();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@wo_Custo_pkeyID", 1 + "#bigint#" + model.wo_Custo_pkeyID);
                input_parameters.Add("@wo_Custo_DocType", 1 + "#nvarchar#" + model.wo_Custo_DocType);
                input_parameters.Add("@wo_Custo_RecievedDate", 1 + "#datetime#" + model.wo_Custo_RecievedDate);
                input_parameters.Add("@wo_Custo_ExpDate", 1 + "#datetime#" + model.wo_Custo_ExpDate);
                input_parameters.Add("@wo_Custo_NotificationDate", 1 + "#datetime#" + model.wo_Custo_NotificationDate);
                input_parameters.Add("@wo_Custo_AlertUser", 1 + "#bit#" + model.wo_Custo_AlertUser);
                input_parameters.Add("@wo_Custo_DocPath", 1 + "#varchar#" + model.wo_Custo_DocPath);
                input_parameters.Add("@wo_Custo_FileName", 1 + "#varchar#" + model.wo_Custo_FileName);
                input_parameters.Add("@wo_Custo_UserId", 1 + "#bigint#" + model.wo_Custo_UserId);
                input_parameters.Add("@wo_Custo_IsActive", 1 + "#bit#" + model.wo_Custo_IsActive);
                input_parameters.Add("@wo_Custo_IsDelete", 1 + "#bit#" + model.wo_Custo_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@wo_Custo_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    workOrderCustomize.wo_Custo_pkeyID = "0";
                    workOrderCustomize.Status = "0";
                    workOrderCustomize.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    workOrderCustomize.wo_Custo_pkeyID = Convert.ToString(objData[0]);
                    workOrderCustomize.Status = Convert.ToString(objData[1]);


                }
                objuserData.Add(workOrderCustomize);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objuserData;



        }

        private DataSet GetWorkOrderCutomizeData(WorkOrderCustomizeDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_WorkOrderCustomize]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@wo_Custo_pkeyID", 1 + "#bigint#" + model.wo_Custo_pkeyID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetWorkOrderCutomizeDetails(WorkOrderCustomizeDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetWorkOrderCutomizeData(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<WorkOrderCustomizeDTO> wocutomize =
                   (from item in myEnumerableFeaprd
                    select new WorkOrderCustomizeDTO
                    {
                        wo_Custo_pkeyID = item.Field<Int64>("wo_Custo_pkeyID"),
                        wo_Custo_DocType = item.Field<String>("wo_Custo_DocType"),
                        wo_Custo_RecievedDate = item.Field<DateTime?>("wo_Custo_RecievedDate"),
                        wo_Custo_ExpDate = item.Field<DateTime?>("wo_Custo_ExpDate"),
                        wo_Custo_NotificationDate = item.Field<DateTime?>("wo_Custo_NotificationDate"),
                        wo_Custo_AlertUser = item.Field<Boolean?>("wo_Custo_AlertUser"),
                        wo_Custo_DocPath = item.Field<String>("wo_Custo_DocPath"),
                        wo_Custo_FileName = item.Field<String>("wo_Custo_FileName"),
                        wo_Custo_UserId = item.Field<Int64?>("wo_Custo_UserId"),
                        wo_Custo_IsActive = item.Field<Boolean?>("wo_Custo_IsActive"),
                        wo_Custo_IsDelete = item.Field<Boolean?>("wo_Custo_IsDelete"),

                    }).ToList();

                objDynamic.Add(wocutomize);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}