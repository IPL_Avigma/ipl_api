﻿using Avigma.Repository.Lib;
using IPL.Models;
using NReco.PdfGenerator;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace IPL.Repository.data
{
    public class Client_Invoice_Print_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        private DataSet GetClientPrintData(Client_Invoice_Print_MasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Client_Invoice_Print_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Inv_Client_WO_Id", 1 + "#bigint#" + model.Inv_Client_WO_Id);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);

                ds = obj.SelectSql(selectProcedure, input_parameters);

            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }
        public static byte[] GenerateRuntimePDF(string html)
        {
            #region NReco.PdfGenerator
            HtmlToPdfConverter nRecohtmltoPdfObj = new HtmlToPdfConverter();
            return nRecohtmltoPdfObj.GeneratePdf(html);
            #endregion

        }
        public async Task<CustomReportPDF> GeneratePDFClientInvoicePrint(Client_Invoice_Print_MasterDTO model)
        {
            CustomReportPDF custom = new CustomReportPDF();
            try
            {
                StringBuilder html = new StringBuilder();
                StringBuilder table = new StringBuilder();
                string theader = "";
                string tfooter = "";
                int index = 0;
                string tbody = "<tbody>";
                decimal tdata = 0;
                var reportList = pdfGetClientInvoicePrintDetail(model);

                theader += "<table width = '100%' border='1' cellpadding='1' cellspacing='0' bgcolor='#efefef'>";
                table.Append(theader);

                if (reportList[2] != null)
                {
                    var wodata = reportList[2];
                    foreach (var wodetails in wodata)
                    {
                        tbody += "<tr><td bgcolor = '#CCCCCC' style = 'text-align:left; border: none; width:75px;' > <img src =" + wodetails.YR_Company_App_logo + " style = 'width:50px; height:50px; margin-top:1px; margin-left:1px; border: none;' /> </td> <td colspan='1' bgcolor = '#CCCCCC' style='text-align:left;height:50px;  border: none;'><span class='MsoNormal1' style='text-align:center; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px'><b><u> </u></b></span>  <div align='center;' style='font - family: Verdana, Arial, Helvetica, sans - serif; font - size: 10px; '>    <span class='MsoNormal1' style='text - align:center; font - size: 35px'><b><u>INVOICE</u></b></span></div> </td></tr>";

                        tbody += "<tr  height='118px'>   <td bgcolor='#FFFFFF' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px;width:50%;'>  <strong >" + wodetails.IPL_Company_Name + "</strong> <br />" + wodetails.CompanyAddress + "<br/>" + wodetails.IPL_StateName + "<br/>" + wodetails.IPL_Company_PinCode + "<br/>" + wodetails.IPL_Company_Mobile + "<br/> </td> <td bgcolor='#FFFFFF' valign = 'top' colspan = '2' style = 'font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px;' > <table width = '100%' border = '0' cellpadding = '4'> <tr><td width = '33%' style = 'text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px; font-weight: bold;' height = '22' bgcolor = '#CCCCCC' > Invoice Number </td>  <td width = '33%' style = 'text-align: center;font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px; font-weight: bold;'  height = '22' bgcolor = '#CCCCCC'> Invoice Date </td> </tr>";

                    }
                }
                if (reportList[0] != null)
                {
                    var invoicedata = reportList[0];
                    foreach (var item in invoicedata)
                    {
                        string Inv_Client_Inv_Date = string.Empty;
                        if (item.Inv_Client_Inv_Date != null)
                        {
                            Inv_Client_Inv_Date = item.Inv_Client_Inv_Date.ToString("dd/MM/yyyy");
                        }
                        tbody += "<tr > <td style = 'text-align: center;'> <strong>" +
                            item.Inv_Client_Invoice_Number + "</strong> </td > <td style = 'text-align: center;'><strong>" +
                            Inv_Client_Inv_Date + "</strong></td> </tr> </table></td> </tr>";
                        tdata = item.Inv_Client_Sub_Total;
                    }
                }
                if (reportList[2] != null)
                {
                    var wodata = reportList[2];
                    foreach (var wodetails in wodata)
                    {
                        string Complete_Date = string.Empty;
                        if (wodetails.Complete_Date != null)
                        {
                            Complete_Date = wodetails.Complete_Date.ToString("dd/MM/yyyy");
                        }
                        tbody += "<tr><td colspan='2' height='26'><table width='100%' border='0'><tr height = '40px'> <td   bgcolor='#CCCCCC' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 16px; font-weight: bold;width:50%;'>Bill To </td><td  bgcolor='#CCCCCC' style='font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 16px; font-weight: bold;'>Work Order Info</td> </tr> <tr> <td  bgcolor='#FFFFFF' style = 'font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 14px;'>" + wodetails.ClientName + ",<br>" + wodetails.ClientAddress + "<br>" +
                            wodetails.ClientState + "<br>"
                            + wodetails.ClientZip + "<br>" + "</td> <td  bgcolor='#FFFFFF' style = 'font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px;'> <table width ='150' border='0'></td><tr> <td><strong>WO # </strong> :" + wodetails.workOrderNumber + "<br> <strong>Loan # </strong> :" + wodetails.Loan_Info + "<br>" + wodetails.address1 + ","
                            + wodetails.city + ","
                            + wodetails.SM_Name + ","
                            + wodetails.zip + "," + "<br> <strong>Complete Date</strong> :" +
                            Complete_Date + " <br> </td></tr> </table> </td> </tr></table></td> </tr>";
                    }
                }
                if (reportList[1] != null)
                {
                    var chinvoicedata = reportList[1];
                    tbody += "<tr><td colspan='2'><table width='100%' height ='900px' border='0'> <tr height ='30px'><td width='10%' bgcolor='#CCCCCC' style='font-size: 12px'><div align='center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'>&nbsp;Sr No </span></b></span></div><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'></span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align='center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'>&nbsp;Description </span></b></span></div><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align='center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;' > &nbsp; Qty </span ></b ></span ></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Price</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td>  <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Disc</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Total</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> </tr> ";


                    foreach (var childdetails in chinvoicedata)
                    {
                        var sr = 1 + index++;
                        //tdata = tdata + childdetails.Inv_Client_Ch_Price;
                        //tdata = tdata + childdetails.Inv_Client_Ch_Total;
                        tbody += "<tr height ='30px'> <td style = 'text-align: center;'width='10%'>" + sr + "</td >" +
                            "<td style = 'text-align: center;'>" + childdetails.Task_Name + "<br/><span style = 'font-size: 12px'>" + childdetails.Inv_Client_Ch_Comment + "</span></td >" +
                            "<td style = 'text-align: center;'>" + childdetails.Inv_Client_Ch_Qty + "</td>" +
                            "<td style = 'text-align: center;'>" + Math.Round(childdetails.Inv_Client_Ch_Price, 2) + "</td>" +
                            "<td style = 'text-align: center;'>" + Math.Round(childdetails.Inv_Client_Ch_Discount, 2) + "%" + "</td>" +
                            "<td style = 'text-align: center;'>" + Math.Round(childdetails.Inv_Client_Ch_Adj_Total, 2) + "</td>" +
                            "</tr>";

                    }
                    tbody += "<tr><td></td></tr>";

                    tbody += "<tr height ='50px'> <td colspan='5'  bgcolor='#CCCCCC' style='text-align: right; font-size: 18px; font-weight: bold; border: 1;'>Total</td><td  bgcolor='#CCCCCC' style='text-align: center; border: 1;'>" + "<strong>$" + Math.Round(tdata, 2) + "</strong></td></tr>";

                    tbody += "</table</td></tr>";
                }




                tfooter += "</ table > ";
                tbody += "</tbody>";
                table.Append(tbody);



                html.Append(table.ToString());
                byte[] pdffile = GenerateRuntimePDF(html.ToString());
                custom.Data = pdffile;
                custom.HttpStatusCode = System.Net.HttpStatusCode.OK;
                return custom;
            }
            catch (Exception ex)
            {
                custom.Message = ex.Message;
                custom.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return custom;
            }
        }

        public List<dynamic> pdfGetClientInvoicePrintDetail(Client_Invoice_Print_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();

            try
            {
                DataSet ds = GetClientPrintData(model);

                if (ds != null && ds.Tables.Count > 0)
                {
                    var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                    List<Client_Invoice_Print_MasterDTO> client =
                       (from item in myEnumerableFeaprd
                        select new Client_Invoice_Print_MasterDTO
                        {
                            Inv_Client_pkeyId = item.Field<Int64>("Inv_Client_pkeyId"),
                            Inv_Client_WO_Id = item.Field<Int64>("Inv_Client_WO_Id"),
                            Inv_Client_Invoice_Number = item.Field<String>("Inv_Client_Invoice_Number"),
                            Inv_Client_Inv_Date = item.Field<DateTime?>("Inv_Client_Inv_Date"),
                            Inv_Client_Sub_Total = item.Field<Decimal?>("Inv_Client_Sub_Total"),

                        }).ToList();
                    objDynamic.Add(client);
                }
                if (ds != null && ds.Tables.Count > 1)
                {
                    var myEnumerableFeaprd = ds.Tables[1].AsEnumerable();
                    List<Client_Child_Invoice_Print_DTO> clientch =
                       (from item in myEnumerableFeaprd
                        select new Client_Child_Invoice_Print_DTO
                        {
                            Inv_Client_Ch_pkeyId = item.Field<Int64>("Inv_Client_Ch_pkeyId"),
                            Inv_Client_Ch_Qty = item.Field<String>("Inv_Client_Ch_Qty"),
                            Inv_Client_Ch_Price = item.Field<Decimal?>("Inv_Client_Ch_Price"),
                            Inv_Client_Ch_Total = item.Field<Decimal?>("Inv_Client_Ch_Total"),
                            Inv_Client_Ch_Adj_Price = item.Field<Decimal?>("Inv_Client_Ch_Adj_Price"),
                            Inv_Client_Ch_Adj_Total = item.Field<Decimal?>("Inv_Client_Ch_Adj_Total"),
                            Inv_Client_Ch_Comment = item.Field<String>("Inv_Client_Ch_Comment"),
                            Inv_Client_Ch_Discount = item.Field<Decimal?>("Inv_Client_Ch_Discount"),
                            Task_Name = item.Field<String>("Task_Name"),

                        }).ToList();
                    objDynamic.Add(clientch);
                }
                if (ds != null && ds.Tables.Count > 2)
                {
                    var myEnumerableFeaprd = ds.Tables[2].AsEnumerable();
                    List<ContractorInvoiceWoDetailPrintDTO> contractorwo =
                       (from item in myEnumerableFeaprd
                        select new ContractorInvoiceWoDetailPrintDTO
                        {
                            workOrderNumber = item.Field<String>("workOrderNumber"),
                            workOrderInfo = item.Field<String>("workOrderInfo"),
                            address1 = item.Field<String>("address1"),
                            IPLNO = item.Field<String>("IPLNO"),
                            city = item.Field<String>("city"),
                            SM_Name = item.Field<String>("SM_Name"),
                            zip = item.Field<Int64?>("zip"),
                            country = item.Field<String>("country"),
                            Complete_Date = item.Field<DateTime?>("Complete_Date"),
                            Loan_Number = item.Field<String>("Loan_Number"),
                            Loan_Info = item.Field<String>("Loan_Info"),
                            fulladdress = item.Field<String>("fulladdress"),
                            WT_WorkType = item.Field<String>("WT_WorkType"),
                            Client_Company_Name = item.Field<String>("Client_Company_Name"),
                            Cont_Name = item.Field<String>("Cont_Name"),
                            Cordinator_Name = item.Field<String>("Cordinator_Name"),
                            Work_Type_Name = item.Field<String>("Work_Type_Name"),
                            Cust_Num_Number = item.Field<String>("Cust_Num_Number"),
                            ClientMetaData = item.Field<String>("ClientMetaData"),
                            ClientName = item.Field<String>("ClientName"),
                            ClientAddress = item.Field<String>("ClientAddress"),
                            ContractorAddress = item.Field<String>("ContractorAddress"),
                            IPL_Company_Name = item.Field<String>("IPL_Company_Name"),
                            CompanyAddress = item.Field<String>("CompanyAddress"),
                            IPL_Company_Mobile = item.Field<String>("IPL_Company_Mobile"),
                            ClientZip = item.Field<String>("ClientZip"),
                            ClientState = item.Field<String>("ClientState"),
                            ConZip = item.Field<String>("ConZip"),
                            ConState = item.Field<String>("ConState"),
                            IPL_StateName = item.Field<String>("IPL_StateName"),
                            IPL_Company_PinCode = item.Field<String>("IPL_Company_PinCode"),
                            YR_Company_App_logo = item.Field<String>("YR_Company_App_logo"),

                        }).ToList();
                    objDynamic.Add(contractorwo);
                }

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objDynamic;
        }
    }
}


