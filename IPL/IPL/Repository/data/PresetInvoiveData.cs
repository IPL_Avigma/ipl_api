﻿using Avigma.Repository.Lib;
using IPLApp.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPLApp.Repository.data
{
    public class PresetInvoiveData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public List<dynamic> AddPresetInvoiceData(PresetInvoiceDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objrkdData = new List<dynamic>();
            string insertProcedure = "[CreateUpdatePresetInvoice]";
            PresetInvoice presetInvoice = new PresetInvoice();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Preset_pkeyID", 1 + "#bigint#" + model.Preset_pkeyID);
                input_parameters.Add("@Preset_Name", 1 + "#nvarchar#" + model.Preset_Name);
                input_parameters.Add("@Preset_InvoiceItemID", 1 + "#bigint#" + model.Preset_InvoiceItemID);
                input_parameters.Add("@Preset_IsActive", 1 + "#bit#" + model.Preset_IsActive);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Preset_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    presetInvoice.Preset_pkeyID = "0";
                    presetInvoice.Status = "0";
                    presetInvoice.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    presetInvoice.Preset_pkeyID = Convert.ToString(objData[0]);
                    presetInvoice.Status = Convert.ToString(objData[1]);


                }
                objrkdData.Add(presetInvoice);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objrkdData;



        }

        private DataSet GetPresetInvoiceMaster(PresetInvoiceDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_PresetInvoice_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Preset_pkeyID", 1 + "#bigint#" + model.Preset_pkeyID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetPresetInvoiceDetails(PresetInvoiceDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetPresetInvoiceMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<PresetInvoiceDTO> PresetInvoice =
                   (from item in myEnumerableFeaprd
                    select new PresetInvoiceDTO
                    {
                        Preset_pkeyID = item.Field<Int64>("Preset_pkeyID"),
                        Preset_Name = item.Field<String>("Preset_Name"),
                        Preset_InvoiceItemID = item.Field<Int64?>("Preset_InvoiceItemID"),
                        Preset_IsActive = item.Field<Boolean?>("Preset_IsActive"),

                    }).ToList();

                objDynamic.Add(PresetInvoice);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}