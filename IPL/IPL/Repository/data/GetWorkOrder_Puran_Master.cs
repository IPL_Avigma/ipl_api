﻿using Avigma.Models;
using Avigma.Repository.Lib;
using IPL.Models;
using IPLApp.Models;
using IPLApp.Repository;
using IPLApp.Repository.data;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace IPL.Repository.data
{
    public class GetWorkOrder_Puran_Master
    {
        WorkOrder_Puran_MasterData workOrder_Puran_MasterData = new WorkOrder_Puran_MasterData();
        ServiceMasterData serviceMasterData = new ServiceMasterData();
        Survey_Question_MasterData survey_Question_MasterData = new Survey_Question_MasterData();
        Survey_CHQuestion_MasterData survey_CHQuestion_MasterData = new Survey_CHQuestion_MasterData();

        Log log = new Log();

        //public List<dynamic> GetPPWWorkOdersDetails(LoginDTO Model)
        //{
        //    List<dynamic> objdynamicobj = new List<dynamic>();
        //    try
        //    {
        //        log.logDebugMessage("-------------Scrapper Called Start ----------------");
        //        log.logDebugMessage("---------URL ----------------" + Model.URL);

        //        //string strURL = System.Configuration.ConfigurationManager.AppSettings["PPW"];
        //        string strURL = Model.URL;
        //        string Schedulertime = System.Configuration.ConfigurationManager.AppSettings["AddSchedulertime"];
        //        var client = new RestClient(strURL);
        //        var request = new RestRequest(Method.POST);
        //        client.Timeout = -1;

        //        log.logDebugMessage("---------username ----------------" + Model.username);
        //        log.logDebugMessage("---------password ----------------" + Model.password);
        //        log.logDebugMessage("---------image_download ----------------" + Model.image_download.ToString().ToLower());


        //        request.AlwaysMultipartFormData = true;
        //        request.AddParameter("ppw_username", Model.username);
        //        request.AddParameter("ppw_password", Model.password);
        //        request.AddParameter("image_download", Model.image_download.ToString().ToLower());


        //        IRestResponse response = client.Execute(request);
        //        DateTime dateTime = System.DateTime.Now.AddMinutes(Convert.ToInt32(Schedulertime) + 5);

        //        log.logDebugMessage(dateTime.ToString());

        //        Avigma.Repository.Scheduler.JobSchedulerStart.Start(dateTime, Model.username);

        //        log.logDebugMessage("-------------Scrapper Called End ----------------");
        //        return objdynamicobj;
        //    }
        //    catch (Exception ex)
        //    {
        //        log.logErrorMessage(ex.StackTrace);
        //        log.logErrorMessage(ex.Message);
        //        return objdynamicobj;

        //    }
        //}


        //public List<dynamic> GetPPWWorkOdersDetailsByUser(LoginDTO Model)
        //{
        //    List<dynamic> objdynamicobj = new List<dynamic>();
        //    try
        //    {
        //        log.logDebugMessage("-------------Manual Scrapper Called Start ----------------");
        //        log.logDebugMessage("---------Manual URL ----------------" + Model.URL);

        //        //string strURL = System.Configuration.ConfigurationManager.AppSettings["PPW"];
        //        string strURL = Model.URL;
        //        var client = new RestClient(strURL);
        //        var request = new RestRequest(Method.POST);
        //        client.Timeout = -1;

        //        log.logDebugMessage("--------- Manual username ----------------" + Model.username);
        //        log.logDebugMessage("---------Manual password ----------------" + Model.password);
        //        log.logDebugMessage("--------- Manual image_download ----------------" + Model.image_download.ToString().ToLower());


        //        request.AlwaysMultipartFormData = true;
        //        request.AddParameter("ppw_username", Model.username);
        //        request.AddParameter("ppw_password", Model.password);
        //        request.AddParameter("image_download", Model.image_download.ToString().ToLower());


        //        IRestResponse response = client.Execute(request);


        //        log.logDebugMessage("-------------Manual Scrapper Called End ----------------");





        //        return objdynamicobj;
        //    }
        //    catch (Exception ex)
        //    {
        //        log.logErrorMessage(ex.StackTrace);
        //        log.logErrorMessage(ex.Message);
        //        return objdynamicobj;

        //    }
        //}


        public List<dynamic> GetWorkOdersPuranDetails(LoginDTO Model)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {

                //var client = new RestClient("https://www.direct.pruvan.com/ps2/getWorkOrders.html");
                ////var client = new RestClient(Model.URL);
                //var request = new RestRequest(Method.POST);
                //request.AddHeader("cache-control", "no-cache");
                //request.AddHeader("Connection", "keep-alive");
                //request.AddHeader("content-length", "99");
                //request.AddHeader("accept-encoding", "gzip, deflate");
                //request.AddHeader("Host", "www.direct.pruvan.com");
                //request.AddHeader("Postman-Token", "d41379fa-fe1a-4a8e-b980-f06b5a2b6c4f,fe0023ba-c60a-44a7-8e1a-c64285d08e85");
                //request.AddHeader("Cache-Control", "no-cache");
                //request.AddHeader("Accept", "*/*");
                //request.AddHeader("User-Agent", "PostmanRuntime/7.15.0");
                //request.AddHeader("User-Agent", "PostmanRuntime/7.15.0");
                //request.AddHeader("Content-Type", "application/x-www-form-urlencoded");//Willwebhooks  //Webhooks2019
                //request.AddParameter("undefined", "payload=%7B%0A%22username%22%3A%20%22" + Model.username + "test_webhook" + Model.token + "Will2020", ParameterType.RequestBody);
                // request.AddParameter("undefined", "payload=%7B%0A%22username%22%3A%20%22" + Model.username + "%22%2C%0A%22token%22%3A%20%22" + Model.token + "%22%0A%7D", ParameterType.RequestBody);
                //IRestResponse response = client.Execute(request);

                var client = new RestClient("https://www.direct.pruvan.com/ps2/getWorkOrders.html");
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
                request.AddParameter("payload", "{\"username\":\"test_webhook\",\"token\":\"Will2020\"}");
                IRestResponse response = client.Execute(request);
               

                string val = Convert.ToString(response.Content);


                var Data = JsonConvert.DeserializeObject<RootObject>(val);

                workOrderxDTO workOrderxDTO = new workOrderxDTO();

                // here update query

                if (Data.error != "invalid username or password")
                {

                    for (int i = 0; i < Data.workOrders.Count; i++)
                    {

                        workOrderxDTO.address1 = Data.workOrders[i].address1;
                        workOrderxDTO.address2 = Data.workOrders[i].address2;
                        //workOrderxDTO.attribute7 = Data.workOrders[i].attribute7;
                        //workOrderxDTO.attribute8 = Data.workOrders[i].attribute8;
                        workOrderxDTO.city = Data.workOrders[i].city;
                        workOrderxDTO.clientInstructions = Data.workOrders[i].clientInstructions;
                        workOrderxDTO.clientStatus = Data.workOrders[i].clientStatus;
                        //workOrderxDTO.controlConfig = Data.workOrders[1].controlConfig;
                        workOrderxDTO.country = Data.workOrders[i].country;
                        workOrderxDTO.description = Data.workOrders[i].description;
                        workOrderxDTO.reference = Data.workOrders[i].reference;
                        //workOrderxDTO.source_wo_id = Convert.ToInt64(Data.workOrders[i].source_wo_id);
                        //workOrderxDTO.source_wo_number = Data.workOrders[i].source_wo_number;
                        //workOrderxDTO.source_wo_provider = Data.workOrders[i].source_wo_provider;
                        workOrderxDTO.state = Data.workOrders[i].state;
                        workOrderxDTO.status = Data.workOrders[i].status;
                        workOrderxDTO.workOrderInfo = Data.workOrders[i].workOrderInfo;
                        workOrderxDTO.workOrderNumber = Data.workOrders[i].workOrderNumber;
                        workOrderxDTO.zip = Convert.ToInt64(Data.workOrders[i].zip);

                        workOrderxDTO.dueDate = new DateTime(1970, 1, 1, 0, 0, 0, 0).AddSeconds(Data.workOrders[i].dueDate.GetValueOrDefault(0)).ToLocalTime();


                        workOrderxDTO.Type = 1;
                        workOrderxDTO.IsActive = true;


                        //var WorkOrdePkey = workOrder_Puran_MasterData.AddWorkorderPuranData(workOrderxDTO);

                        //if (WorkOrdePkey[0].workOrder_ID != "" && WorkOrdePkey[0].Status == "1")
                        //{

                        //    ServiceDTO serviceDTO = new ServiceDTO();


                        //    for (int k = 0; k < Data.workOrders[i].services.Count; k++)
                        //    {

                        //        serviceDTO.workOrder_ID = Convert.ToInt64(WorkOrdePkey[0].workOrder_ID);

                        //        //serviceDTO.attribute1 = Data.workOrders[i].services[i].attribute1;
                        //        //serviceDTO.attribute2 = Data.workOrders[i].services[i].attribute2;
                        //        //serviceDTO.attribute3 = Data.workOrders[i].services[i].attribute3;
                        //        //serviceDTO.attribute4 = Data.workOrders[i].services[i].attribute4;

                        //        serviceDTO.instructions = Data.workOrders[i].services[k].instructions;
                        //        serviceDTO.IsActive = true;

                        //        if (Data.workOrders[i].services[k].source_service_id != null)
                        //        {
                        //            if (Data.workOrders[i].services[k].source_service_id != "")
                        //            {
                        //                serviceDTO.serviceID = Convert.ToInt64(Data.workOrders[i].services[k].source_service_id);
                        //            }
                        //        }

                        //        serviceDTO.survey = Data.workOrders[i].services[k].survey;
                        //        serviceDTO.Type = 1;
                        //        serviceDTO.serviceName = Data.workOrders[i].services[k].serviceName;



                        //        var ServicePkey = serviceMasterData.AddServiceData(serviceDTO);

                        //        if (Data.workOrders[i].services[k].surveyDynamic != "")
                        //        {

                        //            if (ServicePkey[0].serviceID != "")
                        //            {

                        //                //SurveyDynamicDTO surveyDynamic = new SurveyDynamicDTO();// parent table 

                        //                Survey_Question_MasterDTO surveyDynamic = new Survey_Question_MasterDTO();

                        //                var Datax = JsonConvert.DeserializeObject<ServicesRootUI>(Data.workOrders[i].services[k].surveyDynamic);

                        //                for (int j = 0; j < Datax.questions.Count; j++)
                        //                {
                        //                    surveyDynamic.que_serviceID = Convert.ToInt64(ServicePkey[0].serviceID);
                        //                    surveyDynamic.que_anonymous = Datax.questions[j].anonymous;
                                           
                        //                    surveyDynamic.que_enabledByDefault = Datax.questions[j].enabledByDefault;
                        //                    surveyDynamic.que_enableTotal = Datax.questions[j].enableTotal;
                        //                    surveyDynamic.que_expand = Datax.questions[j].expand;
                                          
                        //                    surveyDynamic.que_id = Datax.questions[j].id;
                        //                    //surveyDynamic.mode = Datax.questions[j].mode;
                        //                    //surveyDynamic.modeOptions = Convert.ToString(Datax.questions[j].modeOptions);
                        //                    surveyDynamic.que_name = Datax.questions[j].name;
                        //                    //surveyDynamic.options = Convert.ToString(JsonConvert.SerializeObject(Datax.questions[j].options));// comment this 
                        //                    surveyDynamic.que_prompt = Datax.questions[j].prompt;
                        //                    //surveyDynamic.question = Datax.questions[j].question;
                        //                    //surveyDynamic.questions = Convert.ToString(Datax.questions[j].questions);
                        //                    surveyDynamic.que_repeat = Convert.ToString(Datax.questions[j].repeat);
                        //                    //surveyDynamic.required = Convert.ToString(Datax.questions[j].required);

                        //                    surveyDynamic.que_uuid = Datax.questions[j].uuid;
                        //                    surveyDynamic.que_type = Datax.questions[j].type;
                        //                    surveyDynamic.Type = 1;
                        //                    surveyDynamic.que_IsActive = true;

                        //                    // peky set
                        //                    var parentquestionPkey = survey_Question_MasterData.AddIPL_Survey_Question(surveyDynamic);


                        //                    if (Datax.questions[j].questions != null)
                        //                    {
                        //                        if (parentquestionPkey[0].Status == "1")
                        //                        {
                        //                            //ChildquestionDTO childquestionDTO = new ChildquestionDTO();
                        //                            Survey_CHQuestion_MasterDTO childquestionDTO = new Survey_CHQuestion_MasterDTO();
                        //                            for (int c = 0; c < Datax.questions[j].questions.Count; c++)
                        //                            {
                        //                                childquestionDTO.ParentSur_que_pkeyId = parentquestionPkey[0].que_pkey_Id;
                        //                                //childquestionDTO.Sur_que_answerRequiresPics = 
                        //                                //Sur_que_enabledByDefault

                        //                                //childquestionDTO.hint = Datax.questions[j].questions[c].hint;
                        //                                childquestionDTO.Sur_que_answerEnables = Convert.ToString(Datax.questions[j].questions[c].answerEnables);
                        //                                childquestionDTO.Sur_que_answerType = Datax.questions[j].questions[c].answerType;
                        //                                childquestionDTO.Sur_que_choices = Convert.ToString(JsonConvert.SerializeObject(Datax.questions[j].questions[c].choices));
                        //                                childquestionDTO.Sur_que_commentEnabled = Datax.questions[j].questions[c].commentEnabled;
                        //                                childquestionDTO.Sur_que_id = Datax.questions[j].questions[c].id;
                        //                                childquestionDTO.Sur_que_mode = Datax.questions[j].questions[c].mode;
                        //                                childquestionDTO.Sur_que_modeOptions = Convert.ToString(Datax.questions[j].questions[c].modeOptions);
                        //                                childquestionDTO.Sur_que_name = Datax.questions[j].questions[c].name;
                        //                                childquestionDTO.Sur_que_options = Convert.ToString(JsonConvert.SerializeObject(Datax.questions[j].questions[c].options));
                        //                                childquestionDTO.Sur_que_question = Datax.questions[j].questions[c].question;
                        //                                childquestionDTO.Sur_que_required = Datax.questions[j].questions[c].required;
                        //                                childquestionDTO.Sur_que_text = Datax.questions[j].questions[c].text;
                        //                                childquestionDTO.Sur_que_uuid = Datax.questions[j].questions[c].uuid;
                        //                                childquestionDTO.Sur_que_IsActive = true;
                        //                                childquestionDTO.Type = 1;

                        //                                var returnKey = survey_CHQuestion_MasterData.AddIPL_ChildMeta_Data(childquestionDTO);


                        //                            }
                        //                        }

                        //                    }


                        //                }

                        //            }


                        //        }

                        //    }

                        //}

                        objdynamicobj.Add(workOrderxDTO);

                    }
                }

                return objdynamicobj;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return objdynamicobj;

            }

        }

      
    }
}