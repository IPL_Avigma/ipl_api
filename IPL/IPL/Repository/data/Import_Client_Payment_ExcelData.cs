﻿using Avigma.Repository.Lib;
using IPL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class Import_Client_Payment_ExcelData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        private List<dynamic> CreateUpdateImportClientPaymentExcel(Import_Client_Payment_ExcelDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_Import_Client_Payment_Excel]";
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@ICP_PkeyID", 1 + "#bigint#" + model.ICP_PkeyID);
                input_parameters.Add("@Client_Pay_CheckNumber", 1 + "#nvarchar#" + model.Client_Pay_CheckNumber);
                input_parameters.Add("@Client_Pay_Payment_Date", 1 + "#varchar#" + model.Client_Pay_Payment_Date);
                input_parameters.Add("@Client_Pay_Invoice_Id", 1 + "#varchar#" + model.Client_Pay_Invoice_Id);
                input_parameters.Add("@Inv_Client_Inv_Date", 1 + "#varchar#" + model.Inv_Client_Inv_Date);
                input_parameters.Add("@Client_Pay_Amount", 1 + "#nvarchar#" + model.Client_Pay_Amount);
                input_parameters.Add("@Client_Pay_Comment", 1 + "#varchar#" + model.Client_Pay_Comment);
                input_parameters.Add("@IPLNO", 1 + "#nvarchar#" + model.IPLNO);
                input_parameters.Add("@workOrderNumber", 1 + "#nvarchar#" + model.workOrderNumber);
                input_parameters.Add("@Initial_Comments", 1 + "#varchar#" + model.Initial_Comments);
                input_parameters.Add("@Write_Off", 1 + "#bit#" + model.Write_Off);
                input_parameters.Add("@ICP_IsActive", 1 + "#bit#" + model.ICP_IsActive);
                input_parameters.Add("@ICP_IsDelete", 1 + "#bit#" + model.ICP_IsDelete);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@ICP_Pkey_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);
                objData = obj.SqlCRUD(insertProcedure, input_parameters);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        public List<dynamic> PostCreateUpdateImportClientPaymentExcel(Import_Client_Payment_ExcelDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                if (!string.IsNullOrWhiteSpace(model.ExcelData))
                {
                    var Data = JsonConvert.DeserializeObject<List<Import_Client_Payment_ExcelDTO>>(model.ExcelData);
                    for (int i = 0; i < Data.Count; i++)
                    {
                        Import_Client_Payment_ExcelDTO import_Client_Payment_ExcelDTO = new Import_Client_Payment_ExcelDTO();
                        import_Client_Payment_ExcelDTO = Data[i];
                        import_Client_Payment_ExcelDTO.UserID = model.UserID;
                        import_Client_Payment_ExcelDTO.Type = 1;
                        objData = CreateUpdateImportClientPaymentExcel(import_Client_Payment_ExcelDTO);
                    }

                }
               

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }
            return objData;
        }
    }
}