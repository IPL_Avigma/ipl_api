﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class PCR_InitialPropertyInspection_Data
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        public List<dynamic> CreateUpdatePCRInitialPropertyInspection(PCR_InitialPropertyInspection_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_PCR_Initial_Property_Inspection]";
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@PCR_PkeyID", 1 + "#bigint#" + model.PCR_PkeyID);
                input_parameters.Add("@PCR_WO_ID", 1 + "#bigint#" + model.PCR_WO_ID);
                input_parameters.Add("@PCR_General", 1 + "#nvarchar#" + model.PCR_General);
                input_parameters.Add("@PCR_IsActive", 1 + "#bit#" + model.PCR_IsActive);
                input_parameters.Add("@PCR_IsDelete", 1 + "#bit#" + model.PCR_IsDelete);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@fwo_pkyeId", 1 + "#bigint#" + model.fwo_pkyeId);
                input_parameters.Add("@PCR_Pkey_out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);
                objData = obj.SqlCRUD(insertProcedure, input_parameters);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        private DataSet GetInitialPropertyInspection(PCR_InitialPropertyInspection_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_PCR_Initial_Property_Inspection]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@PCR_PkeyID", 1 + "#bigint#" + model.PCR_PkeyID);
                input_parameters.Add("@PCR_WO_ID", 1 + "#bigint#" + model.PCR_WO_ID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetInitialPropertyInspectionDetails(PCR_InitialPropertyInspection_DTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetInitialPropertyInspection(model);

                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                }    


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}