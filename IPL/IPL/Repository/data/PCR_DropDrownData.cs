﻿using Avigma.Repository.Lib;
using IPL.Models;
using IPLApp.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class PCR_DropDrownData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        private DataSet GetPCRDropdownMaster(DropDownMasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_PCR_Dropdown]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);


                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetPCRDropdownDetails(DropDownMasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetPCRDropdownMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<PCR_DropDrownDTO> PCRdrdDamge =
                   (from item in myEnumerableFeaprd
                    select new PCR_DropDrownDTO
                    {
                        PCR_Damage_Drd_pkeyID = item.Field<Int64>("PCR_Damage_Drd_pkeyID"),
                        PCR_Damage_Drd_Name = item.Field<String>("PCR_Damage_Drd_Name"),


                    }).ToList();

                objDynamic.Add(PCRdrdDamge);

                var myEnumerableFeapr = ds.Tables[1].AsEnumerable();
                List<PCR_CauseDTO> PCRdrdcause =
                   (from item in myEnumerableFeapr
                    select new PCR_CauseDTO
                    {
                        PCR_Cause_pkeyID = item.Field<Int64>("PCR_Cause_pkeyID"),
                        PCR_Cause__Name = item.Field<String>("PCR_Cause__Name"),


                    }).ToList();

                objDynamic.Add(PCRdrdcause);

                var myEnumerableFea = ds.Tables[2].AsEnumerable();
                List<PCR_BuildingDTO> PCRdrdbuild =
                   (from item in myEnumerableFea
                    select new PCR_BuildingDTO
                    {
                        PCR_Building_pkeyID = item.Field<Int64>("PCR_Building_pkeyID"),
                        PCR_Building_Name = item.Field<String>("PCR_Building_Name"),


                    }).ToList();

                objDynamic.Add(PCRdrdbuild);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}