﻿using Avigma.Repository.Lib;
using IPLApp.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPLApp.Repository.data
{
    public class CustomerMasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        //Add Customer Data 
        public List<dynamic> AddCustomeMasyerData(CustomerMasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcustData = new List<dynamic>();
            string insertProcedure = "[CreateUpdateCustomer_Master]";
            CustomerMaster customerMaster = new CustomerMaster();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Cust_pkeyId", 1 + "#bigint#" + model.Cust_pkeyId);
                input_parameters.Add("@Cust_Name", 1 + "#nvarchar#" + model.Cust_Name);
                input_parameters.Add("@Cust_IsActive", 1 + "#bit#" + model.Cust_IsActive);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Cust_pkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    customerMaster.Cust_pkeyId = "0";
                    customerMaster.Status = "0";
                    customerMaster.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    customerMaster.Cust_pkeyId = Convert.ToString(objData[0]);
                    customerMaster.Status = Convert.ToString(objData[1]);


                }
                objcustData.Add(customerMaster);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcustData;



        }
        //Get Customer Master Data 
        private DataSet GetCustomerMaster(CustomerMasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Customer_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Cust_pkeyId", 1 + "#bigint#" + model.Cust_pkeyId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetCustomerMasterDetails(CustomerMasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetCustomerMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<CustomerMasterDTO> CustomerDetails =
                   (from item in myEnumerableFeaprd
                    select new CustomerMasterDTO
                    {
                        Cust_pkeyId = item.Field<Int64>("Cust_pkeyId"),
                        Cust_Name = item.Field<String>("Cust_Name"),
                        Cust_IsActive = item.Field<Boolean?>("Cust_IsActive"),

                    }).ToList();

                objDynamic.Add(CustomerDetails);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}