﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class User_Payment_Trans_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public List<dynamic> AddUserPaymentTransData(User_Payment_Trans_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objclntData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_User_Payment_Trans_Master]";
            User_Payment_Trans user_Payment_Trans = new User_Payment_Trans();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@User_Pay_PkeyID", 1 + "#bigint#" + model.User_Pay_PkeyID);
                input_parameters.Add("@User_Pay_Ac_Holder_Name", 1 + "#nvarchar#" + model.User_Pay_Ac_Holder_Name);
                input_parameters.Add("@User_Pay_Payment_Type", 1 + "#bigint#" + model.User_Pay_Payment_Type);
                input_parameters.Add("@User_Pay_Card_Number", 1 + "#nvarchar#" + model.User_Pay_Card_Number);
                input_parameters.Add("@User_Pay_Cvv_Number", 1 + "#nvarchar#" + model.User_Pay_Cvv_Number);
                input_parameters.Add("@User_Pay_Expiry_Month", 1 + "#bigint#" + model.User_Pay_Expiry_Month); 
                input_parameters.Add("@User_Pay_Expiry_Year", 1 + "#datetime#" + model.User_Pay_Expiry_Year); 
                input_parameters.Add("@User_Pay_Key", 1 + "#nvarchar#" + model.User_Pay_Key); 
                input_parameters.Add("@User_Pay_Merchant_ID", 1 + "#nvarchar#" + model.User_Pay_Merchant_ID); 
                input_parameters.Add("@User_Pay_Status", 1 + "#bigint#" + model.User_Pay_Status); 
                input_parameters.Add("@User_Pay_IsActive", 1 + "#bit#" + model.User_Pay_IsActive);
                input_parameters.Add("@User_Pay_IsDelete", 1 + "#bit#" + model.User_Pay_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@User_Pay_PkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    user_Payment_Trans.User_Pay_PkeyID = "0";
                    user_Payment_Trans.Status = "0";
                    user_Payment_Trans.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    user_Payment_Trans.User_Pay_PkeyID = Convert.ToString(objData[0]);
                    user_Payment_Trans.Status = Convert.ToString(objData[1]);


                }
                objclntData.Add(user_Payment_Trans);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objclntData;

        }

        private DataSet GetUserPaymentTransData(User_Payment_Trans_MasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_User_Payment_Trans_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@User_Pay_PkeyID", 1 + "#bigint#" + model.User_Pay_PkeyID);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return ds;
        }

        public List<dynamic> GetUserPaymentTransDetails(User_Payment_Trans_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetUserPaymentTransData(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<User_Payment_Trans_MasterDTO> UserPaymentTrans =
                   (from item in myEnumerableFeaprd
                    select new User_Payment_Trans_MasterDTO
                    {
                        User_Pay_PkeyID = item.Field<Int64>("User_Pay_PkeyID"),
                        User_Pay_Ac_Holder_Name = item.Field<String>("User_Pay_Ac_Holder_Name"),
                        User_Pay_Payment_Type = item.Field<Int64>("User_Pay_Payment_Type"),
                        User_Pay_Card_Number = item.Field<String>("User_Pay_Card_Number"),
                        User_Pay_Cvv_Number = item.Field<String>("User_Pay_Cvv_Number"),
                        User_Pay_Expiry_Month = item.Field<Int64>("User_Pay_Expiry_Month"),
                        User_Pay_Expiry_Year = item.Field<DateTime>("User_Pay_Expiry_Year"),
                        User_Pay_Key = item.Field<String>("User_Pay_Key"),
                        User_Pay_Merchant_ID = item.Field<String>("User_Pay_Merchant_ID"),
                        User_Pay_Status = item.Field<Int64?>("User_Pay_Status"),
                        User_Pay_IsActive = item.Field<Boolean?>("User_Pay_IsActive"),

                    }).ToList();

                objDynamic.Add(UserPaymentTrans);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}