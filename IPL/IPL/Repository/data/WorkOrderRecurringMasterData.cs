﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class WorkOrderRecurringMasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        public List<dynamic> AddWorkOrderRecurringMaster(WorkOrderRecurringMasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_WorkOrderRecurringMaster]";
            WorkOrderRecurringMaster workOrderRecurringMaster = new WorkOrderRecurringMaster();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Wo_Rec_PkeyId", 1 + "#bigint#" + model.Wo_Rec_PkeyId);
                input_parameters.Add("@Wo_Rec_WoId", 1 + "#bigint#" + model.Wo_Rec_WoId);
                input_parameters.Add("@Wo_Rec_ReceiveDate", 1 + "#datetime#" + model.Wo_Rec_ReceiveDate);
                input_parameters.Add("@Wo_Rec_DueDate", 1 + "#datetime#" + model.Wo_Rec_DueDate);
                input_parameters.Add("@Wo_Rec_IsProcess", 1 + "#bit#" + model.Wo_Rec_IsProcess);
                input_parameters.Add("@Wo_Rec_IsActive", 1 + "#bit#" + model.Wo_Rec_IsActive);
                input_parameters.Add("@Wo_Rec_IsDelete", 1 + "#bit#" + model.Wo_Rec_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Wo_Rec_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    workOrderRecurringMaster.Wo_Rec_PkeyId = "0";
                    workOrderRecurringMaster.Status = "0";
                    workOrderRecurringMaster.ErrorMessage = "Error while Saviving in the Database";
                }
                else
                {
                    workOrderRecurringMaster.Wo_Rec_PkeyId = Convert.ToString(objData[0]);
                    workOrderRecurringMaster.Status = Convert.ToString(objData[1]);
                }
                objcltData.Add(workOrderRecurringMaster);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;
        }
        private DataSet GetWorkOrderRecurringMaster(WorkOrderRecurringMasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_WorkOrderRecurringMaster]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Wo_Rec_PkeyId", 1 + "#bigint#" + model.Wo_Rec_PkeyId);
                input_parameters.Add("@Wo_Rec_WoId", 1 + "#bigint#" + model.Wo_Rec_WoId);                
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ds;
        }

        public List<dynamic> GetReportWOFilterChildData(WorkOrderRecurringMasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetWorkOrderRecurringMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<WorkOrderRecurringMasterDTO> workOrderRecurringMasterDTO =
                   (from item in myEnumerableFeaprd
                    select new WorkOrderRecurringMasterDTO
                    {
                        Wo_Rec_PkeyId = item.Field<Int64>("Wo_Rec_PkeyId"),
                        Wo_Rec_WoId = item.Field<Int64?>("Wo_Rec_WoId"),
                        Wo_Rec_ReceiveDate = item.Field<DateTime?>("Wo_Rec_ReceiveDate"),
                        Wo_Rec_DueDate = item.Field<DateTime?>("Wo_Rec_DueDate"),
                        Wo_Rec_IsProcess = item.Field<Boolean?>("Wo_Rec_IsProcess"),
                        Wo_Rec_IsActive = item.Field<Boolean?>("Wo_Rec_IsActive"),
                    }).ToList();

                objDynamic.Add(workOrderRecurringMasterDTO);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}