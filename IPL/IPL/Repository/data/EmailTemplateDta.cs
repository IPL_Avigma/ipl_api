﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class EmailTemplateDta
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
       
        public List<dynamic> AddEmailTempData(EmailTemplateDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_EmailTemplateMaster]";
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Email_Temp_PkeyId", 1 + "#bigint#" + model.Email_Temp_PkeyId);
                input_parameters.Add("@Email_Temp_HTML", 1 + "#nvarchar#" + model.Email_Temp_HTML);
                input_parameters.Add("@Email_Temp_Subject", 1 + "#nvarchar#" + model.Email_Temp_Subject);
                input_parameters.Add("@Email_Temp_IsActive", 1 + "#bit#" + model.Email_Temp_IsActive);
                input_parameters.Add("@Email_Temp_Delete", 1 + "#bit#" + model.Email_Temp_Delete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Val_Type", 1 + "#int#" + model.Val_Type);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@YR_Company_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
              
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;



        }

        private DataSet GetEmailTempMaster(EmailTemplateDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[GetEmailTemplateMaster]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Email_Temp_PkeyId", 1 + "#bigint#" + model.Email_Temp_PkeyId);
                input_parameters.Add("@Val_Type", 1 + "#int#" + model.Val_Type);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }
        //Get App Company Details 
        public List<dynamic> GetEmailTempMasterDetails(EmailTemplateDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetEmailTempMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<EmailTemplateDTO> EmailTempDetails =
                   (from item in myEnumerableFeaprd
                    select new EmailTemplateDTO
                    {
                        Email_Temp_PkeyId = item.Field<Int64>("Email_Temp_PkeyId"),
                        Email_Temp_HTML = item.Field<String>("Email_Temp_HTML"),
                        Email_Temp_Subject = item.Field<String>("Email_Temp_Subject"),
                        Email_Temp_IsActive = item.Field<Boolean?>("Email_Temp_IsActive"),


                    }).ToList();

                objDynamic.Add(EmailTempDetails);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        private DataSet GetEmailheadingData()
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_EmailHeading_MasterDrd]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();


                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }
        public List<dynamic> GetEmailheadingDetail()
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetEmailheadingData();

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<EmailHeadingDTO> EmailHeading =
                   (from item in myEnumerableFeaprd
                    select new EmailHeadingDTO
                    {
                        Eml_PkeyId = item.Field<Int64>("Eml_PkeyId"),
                        Eml_Name = item.Field<String>("Eml_Name"),

                    }).ToList();

                objDynamic.Add(EmailHeading);

                WorkOrder_Column_Data workOrder_Column_Data = new WorkOrder_Column_Data();
                WorkOrder_Column_DTO workOrder_Column_DTO = new WorkOrder_Column_DTO();
                workOrder_Column_DTO.UserID = 0;
                workOrder_Column_DTO.Type = 4;
                objDynamic.Add(workOrder_Column_Data.GetEmailMetaData(workOrder_Column_DTO));

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }


    }
}