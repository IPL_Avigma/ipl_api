﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using IPLApp.Models;
using Avigma.Repository.Lib;

namespace IPLApp.Repository
{
    public class WorkOrderMaster_Data
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public List<dynamic> WorkOrderItemDetails(WorkOrderMaster_DTO WorkOrderMaster_DTO)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                string insertProcedure = "[CreateUpdateWorkOrderMaster_API_Data]";
                Dictionary<string, string> inputParameter = new Dictionary<string, string>();
                inputParameter.Add("@Pkey_Id", 1 + "#bigint#" + WorkOrderMaster_DTO.Pkey_Id);
                inputParameter.Add("@Category", 1 + "#varchar#" + WorkOrderMaster_DTO.Category);
                inputParameter.Add("@Wo", 1 + "#varchar#" + WorkOrderMaster_DTO.Wo);
                inputParameter.Add("@address", 1 + "#varchar#" + WorkOrderMaster_DTO.address);
                inputParameter.Add("@estimated_complete_date", 1 + "#varchar#" + WorkOrderMaster_DTO.estimated_complete_date);
                inputParameter.Add("@loan_info", 1 + "#varchar#" + WorkOrderMaster_DTO.loan_info);
                inputParameter.Add("@office_locked", 1 + "#varchar#" + WorkOrderMaster_DTO.office_locked);
                inputParameter.Add("@due_date", 1 + "#varchar#" + WorkOrderMaster_DTO.due_date);
                inputParameter.Add("@ready_for_office_Contractor", 1 + "#varchar#" + WorkOrderMaster_DTO.ready_for_office_Contractor);
                inputParameter.Add("@client_Due_date", 1 + "#varchar#" + WorkOrderMaster_DTO.client_Due_date);
                inputParameter.Add("@complete_date", 1 + "#varchar#" + WorkOrderMaster_DTO.complete_date);
                inputParameter.Add("@comments", 1 + "#varchar#" + WorkOrderMaster_DTO.comments);
                inputParameter.Add("@username", 1 + "#varchar#" + WorkOrderMaster_DTO.username);
                inputParameter.Add("@follow_up_complete", 1 + "#varchar#" + WorkOrderMaster_DTO.follow_up_complete);
                inputParameter.Add("@broker_info", 1 + "#varchar#" + WorkOrderMaster_DTO.broker_info);
                inputParameter.Add("@lot_size", 1 + "#varchar#" + WorkOrderMaster_DTO.lot_size);
                inputParameter.Add("@received_date", 1 + "#datetime#" + WorkOrderMaster_DTO.received_date);
                inputParameter.Add("@customer", 1 + "#varchar#" + WorkOrderMaster_DTO.customer);
                inputParameter.Add("@client_company", 1 + "#varchar#" + WorkOrderMaster_DTO.client_company);
                inputParameter.Add("@freeze_property", 1 + "#varchar#" + WorkOrderMaster_DTO.freeze_property);
                inputParameter.Add("@ppw", 1 + "#bit#" + WorkOrderMaster_DTO.ppw);
                inputParameter.Add("@start_date", 1 + "#varchar#" + WorkOrderMaster_DTO.start_date);
                inputParameter.Add("@contractor", 1 + "#varchar#" + WorkOrderMaster_DTO.contractor);
                inputParameter.Add("@ready_for_office", 1 + "#varchar#" + WorkOrderMaster_DTO.ready_for_office);
                inputParameter.Add("@missing_info", 1 + "#varchar#" + WorkOrderMaster_DTO.missing_info);
                inputParameter.Add("@BATF", 1 + "#varchar#" + WorkOrderMaster_DTO.BATF);
                inputParameter.Add("@sub_contractor_follow_up", 1 + "#varchar#" + WorkOrderMaster_DTO.sub_contractor_follow_up);
                inputParameter.Add("@work_type", 1 + "#varchar#" + WorkOrderMaster_DTO.work_type);
                inputParameter.Add("@cancel_date", 1 + "#varchar#" + WorkOrderMaster_DTO.cancel_date);
                inputParameter.Add("@is_Inspection", 1 + "#varchar#" + WorkOrderMaster_DTO.is_Inspection);
                inputParameter.Add("@assigned_admin", 1 + "#varchar#" + WorkOrderMaster_DTO.assigned_admin);
                inputParameter.Add("@lklg", 1 + "#varchar#" + WorkOrderMaster_DTO.lklg);
                inputParameter.Add("@IsActive", 1 + "#bit#" + WorkOrderMaster_DTO.IsActive);
                inputParameter.Add("@IsDelete", 1 + "#bit#" + WorkOrderMaster_DTO.IsDelete);


                inputParameter.Add("@UserId", 1 + "#int#" + WorkOrderMaster_DTO.UserId);
                inputParameter.Add("@Type", 1 + "#int#" + WorkOrderMaster_DTO.Type);
                inputParameter.Add("@Pkey_IdOut", 2 + "#bigint#" + null);
                inputParameter.Add("@ReturnValue", 2 + "#int#" + null);

                objDynamic = obj.SqlCRUD(insertProcedure, inputParameter);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objDynamic;

        }

        #region comment
        //private DataSet GetWorkOrderMasterData(WorkOrderMaster_DTO model)
        //{
        //    DataSet ds = null;
        //    try
        //    {
        //        string selectProcedure = "[GetWorkOrderMaster_API_Data]";
        //        Dictionary<string, string> input_parameters = new Dictionary<string, string>();

        //        input_parameters.Add("@Pkey_Id", 1 + "#bigint#" + model.Pkey_Id);
        //        input_parameters.Add("@WhereClause", 1 + "#varchar#" + model.WhereClause);
        //        input_parameters.Add("@PageNumber", 1 + "#int#" + model.PageNumber);
        //        input_parameters.Add("@NoofRows", 1 + "#int#" + model.NoofRows);
        //        input_parameters.Add("@Imrt_PkeyId", 1 + "#bigint#" + model.Imrt_PkeyId);
        //        input_parameters.Add("@Type", 1 + "#int#" + model.Type);

        //        ds = obj.SelectSql(selectProcedure, input_parameters);
        //    }

        //    catch (Exception ex)
        //    {
        //        log.logErrorMessage(ex.StackTrace);
        //        log.logErrorMessage(ex.Message);
        //    }



        //    return ds;
        //}
        //public List<dynamic> GetWorkOrderItemDetails(WorkOrderMaster_DTO model)
        //{
        //    List<dynamic> objDynamic = new List<dynamic>();
        //    try
        //    {


        //        DataSet ds = GetWorkOrderMasterData(model);
        //        if (ds.Tables.Count > 0)
        //        {
        //            var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
        //            List<WorkOrderMaster_DTO> StudentDocumentData =
        //               (from item in myEnumerableFeaprd
        //                select new WorkOrderMaster_DTO
        //                {
        //                    Pkey_Id = item.Field<Int64>("Pkey_Id"),
        //                    Category = item.Field<string>("Category"),
        //                    Wo = item.Field<string>("Wo"),
        //                    address = item.Field<string>("address"),
        //                    estimated_complete_date = item.Field<string>("estimated_complete_date"),
        //                    loan_info = item.Field<string>("loan_info"),
        //                    office_locked = item.Field<string>("office_locked"),
        //                    due_date = item.Field<string>("due_date"),

        //                    ready_for_office_Contractor = item.Field<string>("ready_for_office_Contractor"),
        //                    client_Due_date = item.Field<string>("client_Due_date"),
        //                    complete_date = item.Field<string>("complete_date"),
        //                    comments = item.Field<string>("comments"),
        //                    username = item.Field<string>("username"),
        //                    follow_up_complete = item.Field<string>("follow_up_complete"),
        //                    broker_info = item.Field<string>("broker_info"),
        //                    lot_size = item.Field<string>("lot_size"),
        //                    received_date = item.Field<DateTime?>("received_date"),
        //                    customer = item.Field<string>("customer"),
        //                    client_company = item.Field<string>("client_company"),
        //                    freeze_property = item.Field<string>("freeze_property"),
        //                    ppw = item.Field<string>("ppw"),
        //                    start_date = item.Field<string>("start_date"),
        //                    contractor = item.Field<string>("contractor"),
        //                    ready_for_office = item.Field<string>("ready_for_office"),
        //                    missing_info = item.Field<string>("missing_info"),
        //                    BATF = item.Field<string>("BATF"),
        //                    sub_contractor_follow_up = item.Field<string>("sub_contractor_follow_up"),
        //                    work_type = item.Field<string>("work_type"),
        //                    cancel_date = item.Field<string>("cancel_date"),
        //                    is_Inspection = item.Field<string>("is_Inspection"),
        //                    assigned_admin = item.Field<string>("assigned_admin"),
        //                    lklg = item.Field<string>("lklg"),
        //                    Mortgagor = item.Field<string>("Mortgagor"),
        //                    IsActive = item.Field<Boolean?>("IsActive"),





        //                }).ToList();
        //            objDynamic.Add(StudentDocumentData);
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        log.logErrorMessage(ex.StackTrace);
        //        log.logErrorMessage(ex.Message);
        //    }

        //    return objDynamic;
        //}
        #endregion
    }
}