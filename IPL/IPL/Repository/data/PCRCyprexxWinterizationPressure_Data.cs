﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class PCRCyprexxWinterizationPressure_Data
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        public List<dynamic> AddPCRCyprexxWinterizationPressureData(PCRCyprexxWinterizationPressure_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_PCR_Cyprexx_Winterization_Pressure]";
            
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@PCR_CW_PkeyID", 1 + "#bigint#" + model.PCR_CW_PkeyID);
                input_parameters.Add("@PCR_CW_Pressure_Test", 1 + "#nvarchar#" + model.PCR_CW_Pressure_Test);
                input_parameters.Add("@PCR_CW_Upload_photo", 1 + "#nvarchar#" + model.PCR_CW_Upload_photo);
                

                input_parameters.Add("@PCR_CW_IsActive", 1 + "#bit#" + model.PCR_CW_IsActive);
                input_parameters.Add("@PCR_CW_IsDelete", 1 + "#bit#" + model.PCR_CW_IsDelete);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@PCR_CW_Pkey_out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        private DataSet GetPCRCyprexxWinterizationPressure_Master(PCRCyprexxWinterizationPressure_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_PCR_Cyprexx_Winterization_Pressure]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@PCR_CW_PkeyID", 1 + "#bigint#" + model.PCR_CW_PkeyID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                //input_parameters.Add("@ReturnValue", 2 + "#int#" + null);
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetPCRCyprexxWinterizationPressureDetails(PCRCyprexxWinterizationPressure_DTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetPCRCyprexxWinterizationPressure_Master(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<PCRCyprexxWinterizationPressure_DTO> pCRCyprexxWinterizationPressure_DTOs =
                   (from item in myEnumerableFeaprd
                    select new PCRCyprexxWinterizationPressure_DTO
                    {
                        PCR_CW_PkeyID = item.Field<Int64>("PCR_CW_PkeyID"),
                        PCR_CW_Pressure_Test = item.Field<String>("PCR_CW_Pressure_Test"),
                        PCR_CW_Upload_photo = item.Field<String>("PCR_CW_Upload_photo"),
                        PCR_CW_IsActive = item.Field<Boolean>("PCR_CW_IsActive"),
                        PCR_CW_IsDelete = item.Field<Boolean>("PCR_CW_IsDelete"),
                    }).ToList();

                objDynamic.Add(pCRCyprexxWinterizationPressure_DTOs);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}