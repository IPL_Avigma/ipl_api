﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class Custom_PhotoLabel_Group_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public List<dynamic> CreateUpdateCustomPhotoLabelGroupData(Custom_PhotoLabel_Group_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            foreach (var group in model.Custom_PhotoLabel_Group_Arr)
            {
                Custom_PhotoLabel_Group_MasterDTO custom_PhotoLabel_Group_MasterDTO = new Custom_PhotoLabel_Group_MasterDTO();
                custom_PhotoLabel_Group_MasterDTO.Custom_PhotoLabel_Group_pkeyID = group.Custom_PhotoLabel_Group_pkeyID;
                custom_PhotoLabel_Group_MasterDTO.Custom_PhotoLabel_Group_Name = group.Custom_PhotoLabel_Group_Name;
                custom_PhotoLabel_Group_MasterDTO.Custom_PhotoLabel_Group_IsActive = model.Custom_PhotoLabel_Group_IsActive;
                custom_PhotoLabel_Group_MasterDTO.Custom_PhotoLabel_Group_IsDelete = model.Custom_PhotoLabel_Group_IsDelete;
                custom_PhotoLabel_Group_MasterDTO.UserID = model.UserID;
                custom_PhotoLabel_Group_MasterDTO.Type = group.Custom_PhotoLabel_Group_pkeyID == 0 ? 1 : 2;
                var objdynamicobj = CreateUpdateCustomPhotoLabelGroup(custom_PhotoLabel_Group_MasterDTO);
                objData.Add(objdynamicobj);
            }
           
            return objData;
        }
        public List<dynamic> CreateUpdateCustomPhotoLabelGroup(Custom_PhotoLabel_Group_MasterDTO model)
        {
            List<dynamic> objcltData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_Custom_PhotoLabel_Group]";
            Custom_PhotoLabel_Group_Master_Result custom_PhotoLabel_Group_Master_Result = new Custom_PhotoLabel_Group_Master_Result();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Custom_PhotoLabel_Group_pkeyID", 1 + "#bigint#" + model.Custom_PhotoLabel_Group_pkeyID);
                input_parameters.Add("@Custom_PhotoLabel_Group_Name", 1 + "#nvarchar#" + model.Custom_PhotoLabel_Group_Name);
                input_parameters.Add("@Custom_PhotoLabel_Group_IsActive", 1 + "#bit#" + model.Custom_PhotoLabel_Group_IsActive);
                input_parameters.Add("@Custom_PhotoLabel_Group_IsDelete", 1 + "#bit#" + model.Custom_PhotoLabel_Group_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Custom_PhotoLabel_Group_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                var objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[0] == 0)
                {
                    custom_PhotoLabel_Group_Master_Result.Custom_PhotoLabel_Group_pkeyID = "0";
                    custom_PhotoLabel_Group_Master_Result.Status = "0";
                    custom_PhotoLabel_Group_Master_Result.ErrorMessage = "Error while Saviving in the Database";
                }
                else
                {
                    custom_PhotoLabel_Group_Master_Result.Custom_PhotoLabel_Group_pkeyID = Convert.ToString(objData[0]);
                    custom_PhotoLabel_Group_Master_Result.Status = Convert.ToString(objData[1]);
                }
                objcltData.Add(custom_PhotoLabel_Group_Master_Result);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;
        }

        public List<dynamic> GetCustomPhotoLabelGroupData(Custom_PhotoLabel_Group_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                string selectProcedure = "[Get_Custom_PhotoLabel_Group]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Custom_PhotoLabel_Group_pkeyID", 1 + "#bigint#" + model.Custom_PhotoLabel_Group_pkeyID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                DataSet ds = obj.SelectSql(selectProcedure, input_parameters);

                if (ds.Tables.Count > 0)
                {
                    var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                    List<Custom_PhotoLabel_Group_MasterDTO> WorkCategoryDetails =
                       (from item in myEnumerableFeaprd
                        select new Custom_PhotoLabel_Group_MasterDTO
                        {
                            Custom_PhotoLabel_Group_pkeyID = item.Field<Int64>("Custom_PhotoLabel_Group_pkeyID"),
                            Custom_PhotoLabel_Group_Name = item.Field<String>("Custom_PhotoLabel_Group_Name"),

                        }).ToList();

                    objDynamic.Add(WorkCategoryDetails);
                }

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objDynamic;
        }
    }
}