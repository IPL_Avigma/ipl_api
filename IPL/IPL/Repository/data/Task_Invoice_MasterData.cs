﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class Task_Invoice_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        TaskBidMasterData taskBidMasterData = new TaskBidMasterData();
        Log log = new Log();


        public List<dynamic> AddTaskInvoiceData(Task_Invoice_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();

            string insertProcedure = "[CreateUpdateTask_Invoice_Master]";
            TaskInvMaster taskInvMaster = new TaskInvMaster();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Task_Inv_pkeyID", 1 + "#bigint#" + model.Task_Inv_pkeyID);
                input_parameters.Add("@Task_Inv_TaskID", 1 + "#bigint#" + model.Task_Inv_TaskID);
                input_parameters.Add("@Task_Inv_WO_ID", 1 + "#bigint#" + model.Task_Inv_WO_ID);
                input_parameters.Add("@Task_Inv_Qty", 1 + "#varchar#" + model.Task_Inv_Qty);
                input_parameters.Add("@Task_Inv_Uom_ID", 1 + "#bigint#" + model.Task_Inv_Uom_ID);
                input_parameters.Add("@Task_Inv_Cont_Price", 1 + "#decimal#" + model.Task_Inv_Cont_Price);
                input_parameters.Add("@Task_Inv_Cont_Total", 1 + "#decimal#" + model.Task_Inv_Cont_Total);
                input_parameters.Add("@Task_Inv_Clnt_Price", 1 + "#decimal#" + model.Task_Inv_Clnt_Price);
                input_parameters.Add("@Task_Inv_Clnt_Total", 1 + "#decimal#" + model.Task_Inv_Clnt_Total);
                input_parameters.Add("@Task_Inv_Comments", 1 + "#varchar#" + model.Task_Inv_Comments);
                input_parameters.Add("@Task_Inv_Violation", 1 + "#bit#" + model.Task_Inv_Violation);
                input_parameters.Add("@Task_Inv_damage", 1 + "#bit#" + model.Task_Inv_damage);
                input_parameters.Add("@Task_Inv_IsActive", 1 + "#bit#" + model.Task_Inv_IsActive);
                input_parameters.Add("@Task_Inv_IsDelete", 1 + "#bit#" + model.Task_Inv_IsDelete);
                input_parameters.Add("@Task_Inv_Status", 1 + "#int#" + model.Task_Inv_Status);
                input_parameters.Add("@Task_Inv_Auto_Invoice", 1 + "#bit#" + model.Task_Inv_Auto_Invoice);
                input_parameters.Add("@Com_Other_Task_Name", 1 + "#varchar#" + model.Com_Other_Task_Name);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Task_Inv_Hazards", 1 + "#bit#" + model.Task_Inv_Hazards);
                input_parameters.Add("@Task_Inv_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    taskInvMaster.Task_Inv_pkeyID = "0";
                    taskInvMaster.Status = "0";
                    taskInvMaster.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    taskInvMaster.Task_Inv_pkeyID = Convert.ToString(objData[0]);
                    taskInvMaster.Status = Convert.ToString(objData[1]);


                }
                objcltData.Add(taskInvMaster);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;



        }

        private DataSet GetTaskInvoiceMasterdata(Task_Invoice_MasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Task_Invoice_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Task_Inv_pkeyID", 1 + "#bigint#" + model.Task_Inv_pkeyID);
                input_parameters.Add("@Task_Inv_WO_ID", 1 + "#bigint#" + model.Task_Inv_WO_ID);
                input_parameters.Add("@Task_Inv_Status", 1 + "#int#" + model.Task_Inv_Status);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetTaskInvoiceDetails(Task_Invoice_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetTaskInvoiceMasterdata(model);

                if (model.Type == 3)
                {
                    var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                    List<Task_Invoice_MasterDTO> invoiceMasterDetails =
                       (from item in myEnumerableFeaprd
                        select new Task_Invoice_MasterDTO
                        {
                            Task_Inv_pkeyID = item.Field<Int64>("Task_Inv_pkeyID"),
                            Task_Inv_TaskID = item.Field<Int64?>("Task_Inv_TaskID"),
                            Task_Inv_WO_ID = item.Field<Int64?>("Task_Inv_WO_ID"),
                            Task_Inv_Qty = item.Field<String>("Task_Inv_Qty"),
                            Task_Inv_Uom_ID = item.Field<Int64?>("Task_Inv_Uom_ID"),
                            Task_Inv_Cont_Price = item.Field<Decimal?>("Task_Inv_Cont_Price"),
                            Task_Inv_Cont_Total = item.Field<Decimal?>("Task_Inv_Cont_Total"),
                            Task_Inv_Clnt_Price = item.Field<Decimal?>("Task_Inv_Clnt_Price"),
                            Task_Inv_Clnt_Total = item.Field<Decimal?>("Task_Inv_Clnt_Total"),
                            Task_Inv_Comments = item.Field<String>("Task_Inv_Comments"),
                            Task_Inv_Violation = item.Field<Boolean?>("Task_Inv_Violation"),
                            Task_Inv_damage = item.Field<Boolean?>("Task_Inv_damage"),
                            Task_Inv_IsActive = item.Field<Boolean?>("Task_Inv_IsActive"),
                            Task_Inv_Status = item.Field<int?>("Task_Inv_Status"),
                            Task_Inv_Hazards = item.Field<Boolean?>("Task_Inv_Hazards"),
                            Task_Ext_pkeyID = item.Field<Int64>("Task_Ext_pkeyID"),
                            Task_Ext_CompletionID = item.Field<Int64?>("Task_Ext_CompletionID"),
                            Task_Ext_WO_ID = item.Field<Int64?>("Task_Ext_WO_ID"),
                            Task_Ext_Location = item.Field<String>("Task_Ext_Location"),
                            Task_Ext_DamageCauseId = item.Field<Int64?>("Task_Ext_DamageCauseId"),
                            Task_Ext_Length = item.Field<String>("Task_Ext_Length"),
                            Task_Ext_Width = item.Field<String>("Task_Ext_Width"),
                            Task_Ext_Height = item.Field<String>("Task_Ext_Height"),
                            Task_Ext_Men = item.Field<String>("Task_Ext_Men"),
                            Task_Ext_Hours = item.Field<String>("Task_Ext_Hours"),
                            Task_Ext_IsActive = item.Field<Boolean?>("Task_Ext_IsActive"),
                        }).ToList();

                    objDynamic.Add(invoiceMasterDetails);
                }
                else
                {
                    var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                    List<Task_Invoice_MasterDTO> invoiceMasterDetails =
                       (from item in myEnumerableFeaprd
                        select new Task_Invoice_MasterDTO
                        {
                            Task_Inv_pkeyID = item.Field<Int64>("Task_Inv_pkeyID"),
                            Task_Inv_TaskID = item.Field<Int64?>("Task_Inv_TaskID"),
                            Task_Inv_WO_ID = item.Field<Int64?>("Task_Inv_WO_ID"),
                            Task_Inv_Qty = item.Field<String>("Task_Inv_Qty"),
                            Task_Inv_Uom_ID = item.Field<Int64?>("Task_Inv_Uom_ID"),
                            Task_Inv_Cont_Price = item.Field<Decimal?>("Task_Inv_Cont_Price"),
                            Task_Inv_Cont_Total = item.Field<Decimal?>("Task_Inv_Cont_Total"),
                            Task_Inv_Clnt_Price = item.Field<Decimal?>("Task_Inv_Clnt_Price"),
                            Task_Inv_Clnt_Total = item.Field<Decimal?>("Task_Inv_Clnt_Total"),
                            Task_Inv_Comments = item.Field<String>("Task_Inv_Comments"),
                            Task_Inv_Violation = item.Field<Boolean?>("Task_Inv_Violation"),
                            Task_Inv_damage = item.Field<Boolean?>("Task_Inv_damage"),
                            Task_Inv_IsActive = item.Field<Boolean?>("Task_Inv_IsActive"),
                            Task_Inv_Status = item.Field<int?>("Task_Inv_Status"),
                            Task_Inv_Hazards = item.Field<Boolean?>("Task_Inv_Hazards"),
                        }).ToList();

                    objDynamic.Add(invoiceMasterDetails);
                }

                
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
        public List<dynamic> AddTaskCompletionExtDetail(Task_Invoice_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                Task_Completion_ExtensionDTO task_Completion_ExtensionDTO = new Task_Completion_ExtensionDTO();
                task_Completion_ExtensionDTO.Task_Cmp_Ext_pkeyID = model.Task_Ext_pkeyID;
                task_Completion_ExtensionDTO.Task_Cmp_Ext_CompletionID = model.Task_Ext_CompletionID;
                task_Completion_ExtensionDTO.Task_Cmp_Ext_WO_ID = model.Task_Ext_WO_ID;
                task_Completion_ExtensionDTO.Task_Cmp_Ext_DamageCauseId = model.Task_Ext_DamageCauseId;
                task_Completion_ExtensionDTO.Task_Cmp_Ext_Location = model.Task_Ext_Location;
                task_Completion_ExtensionDTO.Task_Cmp_Ext_Length = model.Task_Ext_Length;
                task_Completion_ExtensionDTO.Task_Cmp_Ext_Width = model.Task_Ext_Width;
                task_Completion_ExtensionDTO.Task_Cmp_Ext_Height = model.Task_Ext_Height;
                task_Completion_ExtensionDTO.Task_Cmp_Ext_Men = model.Task_Ext_Men;
                task_Completion_ExtensionDTO.Task_Cmp_Ext_Hours = model.Task_Ext_Hours;
                task_Completion_ExtensionDTO.Task_Cmp_Ext_IsActive = model.Task_Ext_IsActive;
                task_Completion_ExtensionDTO.Type = model.Type;
                task_Completion_ExtensionDTO.UserID = model.UserID;
                objDynamic = taskBidMasterData.AddTaskCompletionExtData(task_Completion_ExtensionDTO);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }


    }
}