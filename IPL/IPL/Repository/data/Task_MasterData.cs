﻿using Avigma.Repository.Lib;
using IPL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class Task_MasterData
    {

        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        //Add app Company Details
        public List<dynamic> AddTaskMasterData(TaskMasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();
            TaskMaster taskMaster = new TaskMaster();
            string insertProcedure = "[CreateUpdate_TaskMaster]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Task_pkeyID", 1 + "#bigint#" + model.Task_pkeyID);
                input_parameters.Add("@Task_Name", 1 + "#nvarchar#" + model.Task_Name);
                input_parameters.Add("@Task_Type", 1 + "#int#" + model.Task_Type);
                input_parameters.Add("@Task_Group", 1 + "#int#" + model.Task_Group);
                input_parameters.Add("@Task_UOM", 1 + "#int#" + model.Task_UOM);
                input_parameters.Add("@Task_Contractor_UnitPrice", 1 + "#decimal#" + model.Task_Contractor_UnitPrice);
                input_parameters.Add("@Task_Client_UnitPrice", 1 + "#decimal#" + model.Task_Client_UnitPrice);
                input_parameters.Add("@Task_Photo_Label_Name", 1 + "#nvarchar#" + model.Task_Photo_Label_Name);
                input_parameters.Add("@Task_AutoInvoiceComplete", 1 + "#bit#" + model.Task_AutoInvoiceComplete);
                input_parameters.Add("@Task_Work_Type_Group", 1 + "#nvarchar#" + model.Task_Work_Type_Group);
                input_parameters.Add("@Task_IsActive", 1 + "#bit#" + model.Task_IsActive);
                input_parameters.Add("@Task_System", 1 + "#int#" + model.Task_System);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Task_Flat_Free", 1 + "#bit#" + model.Task_Flat_Free);
                input_parameters.Add("@Task_Price_Edit", 1 + "#bit#" + model.Task_Price_Edit);
                input_parameters.Add("@Task_Disable_Default", 1 + "#bit#" + model.Task_Disable_Default);
                input_parameters.Add("@Task_Auto_Assign", 1 + "#bit#" + model.Task_Auto_Assign);
                input_parameters.Add("@Task_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    taskMaster.Task_pkeyID = "0";
                    taskMaster.Status = "0";
                    taskMaster.ErrorMessage = "Error while Saviving in the Database";
                }
                else
                {
                    taskMaster.Task_pkeyID = Convert.ToString(objData[0]);
                    taskMaster.Status = Convert.ToString(objData[1]);
                }

                objcltData.Add(taskMaster);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;
        }


        private DataSet GetTaskMaster(TaskMasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_TaskMaster]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Task_Group_pkeyID", 1 + "#bigint#" + model.Task_pkeyID);
                input_parameters.Add("@WhereClause", 1 + "#varchar#" + model.WhereClause);
                input_parameters.Add("@WorkOrderID", 1 + "#bigint#" + model.WorkOrderID);
                input_parameters.Add("@UserID", 1 + "#int#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);


                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }
        //Get Task group list 
        public List<dynamic> GetTaskMasterrDetails(TaskMasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            List<Task_Master_Files_DTO> TaskDocument = new List<Task_Master_Files_DTO>();
            try
            {
                DataSet ds = GetTaskMaster(model);

                if (ds.Tables.Count > 0)
                {
                    log.logDebugMessage("0");

                    if (ds.Tables[0].Rows.Count > 0) {

                        //var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                        //List<TaskMasterDTO> AppClientDetails =
                        //   (from item in myEnumerableFeaprd
                        //    select new TaskMasterDTO
                        //    {
                        //        Task_pkeyID = item.Field<Int64>("Task_pkeyID"),
                        //        Task_Name = item.Field<String>("Task_Name"),
                        //        Task_Type = item.Field<int?>("Task_Type"),
                        //        Task_TypeName = item.Field<String>("Task_TypeName"),
                        //        Task_Group = item.Field<int?>("Task_Group"),
                        //        Task_UOM = item.Field<int?>("Task_UOM"),
                        //        Task_Group_Name = item.Field<String>("Task_Group_Name"),
                        //        Task_Contractor_UnitPrice = item.Field<Decimal?>("Task_Contractor_UnitPrice"),
                        //        Task_Client_UnitPrice = item.Field<Decimal?>("Task_Client_UnitPrice"),
                        //        Task_Photo_Label_Name = item.Field<String>("Task_Photo_Label_Name"),
                        //        Task_IsActive = item.Field<Boolean?>("Task_IsActive"),
                        //        Task_AutoInvoiceComplete = item.Field<Boolean?>("Task_AutoInvoiceComplete"),
                        //        Task_Flat_Free = item.Field<Boolean?>("Task_Flat_Free"),
                        //        Task_Price_Edit = item.Field<Boolean?>("Task_Price_Edit"),
                        //        Task_Disable_Default = item.Field<Boolean?>("Task_Disable_Default"),
                        //        Task_CreatedBy = item.Field<String>("Task_CreatedBy"),
                        //        Task_ModifiedBy = item.Field<String>("Task_ModifiedBy"),
                        //        Task_Auto_Assign = item.Field<Boolean?>("Task_Auto_Assign"),
                        //        Task_Auto_Assign_str = item.Field<String>("Task_Auto_Assign_str"),
                        //        ViewUrl = null

                        //    }).ToList();

                        //objDynamic.Add(AppClientDetails);


                        objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[0]));

                    }

                    if (model.Type == 1)
                    {
                        if (ds.Tables.Count > 1)
                        {
                            var myEnumerabletask = ds.Tables[1].AsEnumerable();
                            List<Filter_Admin_Task_MasterDTO> taskFilterList =
                               (from item in myEnumerabletask
                                select new Filter_Admin_Task_MasterDTO
                                {
                                    Task_Filter_PkeyID = item.Field<Int64>("Task_Filter_PkeyID"),
                                    Task_Filter_TaskName = item.Field<String>("Task_Filter_TaskName"),
                                    Task_Filter_TaskType = item.Field<Int32>("Task_Filter_TaskType"),
                                    Task_Filter_TaskPhName = item.Field<String>("Task_Filter_TaskPhName"),
                                    Task_Filter_TaskIsActive = item.Field<Boolean?>("Task_Filter_TaskIsActive"),
                                    Task_Filter_CreatedBy = item.Field<String>("Task_Filter_CreatedBy"),
                                    Task_Filter_ModifiedBy = item.Field<String>("Task_Filter_ModifiedBy"),
                                }).ToList();

                            objDynamic.Add(taskFilterList);
                        }
                    }
                }

                if (model.Type == 2)
                {


                    if (ds.Tables.Count > 1)
                    {
                        log.logDebugMessage("1");
                        if (ds.Tables[1].Rows.Count > 0)
                        {

                            var myEnumerableFeaprd1 = ds.Tables[1].AsEnumerable();
                            TaskDocument =
                               (from item in myEnumerableFeaprd1
                                select new Task_Master_Files_DTO
                                {
                                    TMF_Task_Pkey = item.Field<Int64>("TMF_Task_Pkey"),
                                    TMF_Task_pkeyID = item.Field<Int64?>("TMF_Task_pkeyID"),
                                    TMF_Task_objectName = item.Field<String>("TMF_Task_objectName"),
                                    TMF_Task_FolderName = item.Field<String>("TMF_Task_FolderName"),
                                    TMF_Task_FileName = item.Field<String>("TMF_Task_FileName"),
                                    TMF_Task_localPath = item.Field<String>("TMF_Task_localPath"),
                                    TMF_Task_BucketName = item.Field<String>("TMF_Task_BucketName"),
                                    TMF_Task_ProjectID = item.Field<Int64?>("TMF_Task_ProjectID"),
                                    TMF_Task_FileSize = item.Field<int?>("TMF_Task_FileSize"),
                                    TMF_Task_FileType = item.Field<int?>("TMF_Task_FileType"),
                                    TMF_Task_UploadTimestamp = item.Field<DateTime?>("TMF_Task_UploadTimestamp"),
                                    TMF_Task_UploadBy = item.Field<Int64?>("TMF_Task_UploadBy"),
                                    TMF_Task_CustPkey = item.Field<Int64?>("TMF_Task_CustPkey"),
                                    TMF_Task_ClientPkey = item.Field<Int64?>("TMF_Task_ClientPkey"),
                                    TMF_Task_IsDelete = item.Field<Boolean?>("TMF_Task_IsDelete"),
                                    TMF_Task_CreatedBy = item.Field<String>("TMF_Task_CreatedBy"),
                                    TMF_Task_ModifiedBy = item.Field<String>("TMF_Task_ModifiedBy"),

                                    chkflag = item.Field<Boolean?>("TMF_Status"),


                                }).ToList();

                            //objDynamic.Add(TaskDocument);
                        }
                    }
                }
                objDynamic.Add(TaskDocument);
                if (model.Type == 2)
                {
                    if (ds.Tables.Count > 2)
                    {
                        log.logDebugMessage("2");
                        if (ds.Tables[2].Rows.Count > 0)
                        {
                            var myEnumerableFeaprd = ds.Tables[2].AsEnumerable();
                            List<TaskSettingChildDTO> TaskChildDetails =
                               (from item in myEnumerableFeaprd
                                select new TaskSettingChildDTO
                                {
                                    Task_sett_pkeyID = item.Field<Int64>("Task_sett_pkeyID"),
                                    Task_sett_ID = item.Field<Int64>("Task_sett_ID"),
                                    Task_sett_Company = item.Field<String>("Task_sett_Company"),
                                    Task_sett_State = item.Field<String>("Task_sett_State"),
                                    Task_sett_Country = item.Field<String>("Task_sett_Country"),
                                    Task_sett_Zip = item.Field<String>("Task_sett_Zip"),
                                    Task_sett_Contractor = item.Field<String>("Task_sett_Contractor"),
                                    Task_sett_Customer = item.Field<String>("Task_sett_Customer"),
                                    Task_sett_Lone = item.Field<String>("Task_sett_Lone"),
                                    Task_sett_Con_Unit_Price = item.Field<Decimal?>("Task_sett_Con_Unit_Price"),
                                    Task_sett_CLI_Unit_Price = item.Field<Decimal?>("Task_sett_CLI_Unit_Price"),
                                    Task_sett_Flat_Free = item.Field<Boolean?>("Task_sett_Flat_Free"),
                                    Task_sett_Price_Edit = item.Field<Boolean?>("Task_sett_Price_Edit"),
                                    Task_Work_TypeGroup = item.Field<String>("Task_Work_TypeGroup"),
                                    Task_sett_IsActive = item.Field<Boolean?>("Task_sett_IsActive"),
                                    Task_sett_Disable_Default = item.Field<Boolean?>("Task_sett_Disable_Default"),
                                    Task_sett_WorkType = item.Field<String>("Task_sett_WorkType"),
                                    Task_sett_LOT_Min = item.Field<Decimal?>("Task_sett_LOT_Min"),
                                    Task_sett_LOT_Max = item.Field<Decimal?>("Task_sett_LOT_Max"),
                                    Task_sett_CreatedBy = item.Field<String>("Task_sett_CreatedBy"),
                                    Task_sett_ModifiedBy = item.Field<String>("Task_sett_ModifiedBy"),
                                }).ToList();

                            objDynamic.Add(TaskChildDetails);
                        }
                    }


                }


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage("--------------->" + model.Type + "<-----------------------");
                log.logErrorMessage("--------------->" + model.UserID + "<-----------------------");
                log.logErrorMessage("GetTaskMasterrDetails" + model.Task_pkeyID + "<-----------------------");

                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        public List<dynamic> GetTaskFilterDetails(TaskMasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();


            string wherecondition = string.Empty;
            try
            {

                var Data = JsonConvert.DeserializeObject<TaskMasterDTO>(model.FilterData);
                if (!string.IsNullOrEmpty(Data.Task_Name))
                {
                    // wherecondition = " And Task_Name =    '" + Data.Task_Name + "'";
                    wherecondition = " And Task_Name LIKE '%" + Data.Task_Name + "%'";
                }
                if (!string.IsNullOrEmpty(Data.Task_Photo_Label_Name))
                {
                   // wherecondition = " And Task_Photo_Label_Name =    '" + Data.Task_Photo_Label_Name + "'";
                    wherecondition = wherecondition + " And Task_Photo_Label_Name LIKE '%" + Data.Task_Photo_Label_Name + "%'";

                }
                if (Data.Task_Type != 0 && Data.Task_Type != null)
                {
                    //wherecondition =  "  And Task_Type = '" + Data.Task_Type + "'";
                    wherecondition = wherecondition + " And Task_Type LIKE '%" + Data.Task_Type + "%'";

                }

                if (Data.Task_IsActive == true)
                {
                    wherecondition = wherecondition + "  And Task_IsActive =  '1'";
                }
                if (Data.Task_IsActive == false)
                {
                    wherecondition = wherecondition + "  And Task_IsActive =  '0'";
                }


                TaskMasterDTO taskMasterDTO = new TaskMasterDTO();

                taskMasterDTO.WhereClause = wherecondition;
                taskMasterDTO.Type = 3;
                taskMasterDTO.UserID = model.UserID;
                DataSet ds = GetTaskMaster(taskMasterDTO);

                //var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                //List<TaskMasterDTO> Taskinvfilter =
                //   (from item in myEnumerableFeaprd
                //    select new TaskMasterDTO
                //    {
                //        Task_pkeyID = item.Field<Int64>("Task_pkeyID"),
                //        Task_Name = item.Field<String>("Task_Name"),
                //        Task_Type = item.Field<int?>("Task_Type"),
                //        Task_TypeName = item.Field<String>("Task_TypeName"),
                //        Task_Group = item.Field<int?>("Task_Group"),
                //        Task_UOM = item.Field<int?>("Task_UOM"),
                //        Task_Group_Name = item.Field<String>("Task_Group_Name"),
                //        Task_Contractor_UnitPrice = item.Field<Decimal?>("Task_Contractor_UnitPrice"),
                //        Task_Client_UnitPrice = item.Field<Decimal?>("Task_Client_UnitPrice"),
                //        Task_Photo_Label_Name = item.Field<String>("Task_Photo_Label_Name"),
                //        Task_IsActive = item.Field<Boolean?>("Task_IsActive"),
                //        Task_AutoInvoiceComplete = item.Field<Boolean?>("Task_AutoInvoiceComplete"),
                //        ViewUrl = null,
                //        Task_Auto_Assign = item.Field<Boolean?>("Task_Auto_Assign"),
                //        Task_Auto_Assign_str = item.Field<String>("Task_Auto_Assign_str"),
                //        Task_CreatedBy = item.Field<String>("Task_CreatedBy"),
                //        Task_ModifiedBy = item.Field<String>("Task_ModifiedBy"),
                //    }).ToList();

                //objDynamic.Add(Taskinvfilter);

                objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[0]));
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }

            return objDynamic;
        }


        private List<dynamic> Update_TaskStatus_DamageViolationHazard(Task_Status_Damage_Violation_Hazard model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();

            string insertProcedure = "[Update_Task_Status_Damage_Violation_Hazard]";
            TaskMaster taskMaster = new TaskMaster();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Task_pkeyID", 1 + "#bigint#" + model.Task_pkeyID);
                input_parameters.Add("@Task_WO_ID", 1 + "#bigint#" + model.Task_WO_ID);
                input_parameters.Add("@Task_IsActive", 1 + "#bit#" + model.Task_IsActive);
                input_parameters.Add("@Task_IsDelete", 1 + "#bit#" + model.Task_IsDelete);
                input_parameters.Add("@Task_Status", 1 + "#int#" + model.Task_Status);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Task_Type", 1 + "#int#" + model.Task_Type);
                input_parameters.Add("@Task_Comma", 1 + "#nvarchar#" + model.Task_Comma);
                input_parameters.Add("@Task_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters); ;
                if (objData[1] == 0)
                {
                    taskMaster.Task_pkeyID = "0";
                    taskMaster.Status = "0";
                    taskMaster.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    taskMaster.Task_pkeyID = Convert.ToString(objData[0]);
                    taskMaster.Status = Convert.ToString(objData[1]);


                }
                objcltData.Add(taskMaster);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;



        }

        public List<dynamic> UpdateTaskStatusDamageViolationHazard(Task_Status_Damage_Violation_Hazard model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                if (!string.IsNullOrWhiteSpace(model.TaskArrayJson))
                {
                    List<Task_Status_Damage_Violation_Hazard> _Task_Status_Damage_Violation_Hazard = JsonConvert.DeserializeObject<List<Task_Status_Damage_Violation_Hazard>>(model.TaskArrayJson);
                    model.Task_Comma = string.Join(",", _Task_Status_Damage_Violation_Hazard.Where(x => x.Task_pkeyID > 0).Select(x => x.Task_pkeyID.ToString()).ToArray());
                }
                
                objData =  Update_TaskStatus_DamageViolationHazard(model);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objData;
        }

        }
}