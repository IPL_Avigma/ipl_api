﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class PCR_CyprexxWinterizationPressureData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        public List<dynamic> PostCyprexxWinterizationPressure(PCR_CyprexxWinterizationPressure_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_PCR_Cyprexx_Winterization_Pressure]";
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@PCR_CW_PkeyID", 1 + "#bigint#" + model.PCR_CW_PkeyID);
                input_parameters.Add("@PCR_CW_WO_ID", 1 + "#bigint#" + model.PCR_CW_WO_ID);
                input_parameters.Add("@PCR_CW_Pressure_Test", 1 + "#nvarchar#" + model.PCR_CW_Pressure_Test);
                input_parameters.Add("@PCR_CW_Upload_photo", 1 + "#nvarchar#" + model.PCR_CW_Upload_photo);
                input_parameters.Add("@PCR_CW_IsActive", 1 + "#bit#" + model.PCR_CW_IsActive);
                input_parameters.Add("@PCR_CW_IsDelete", 1 + "#bit#" + model.PCR_CW_IsDelete);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@fwo_pkyeId", 1 + "#bigint#" + model.fwo_pkyeId);
                input_parameters.Add("@PCR_CW_Pkey_out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);
                objData = obj.SqlCRUD(insertProcedure, input_parameters);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        private DataSet GetCyprexxWinterizationPressure(PCR_CyprexxWinterizationPressure_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_PCR_Cyprexx_Winterization_Pressure]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@PCR_CW_PkeyID", 1 + "#bigint#" + model.PCR_CW_PkeyID);
                input_parameters.Add("@PCR_CW_WO_ID", 1 + "#bigint#" + model.PCR_CW_WO_ID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetCyprexxWinterizationPressureDetails(PCR_CyprexxWinterizationPressure_DTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetCyprexxWinterizationPressure(model);

                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                }


                //var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                //List<PCR_CyprexxWinterizationPressure_DTO> cyprexxGrassdata =
                //   (from item in myEnumerableFeaprd
                //    select new PCR_CyprexxWinterizationPressure_DTO
                //    {
                //        PCR_CW_PkeyID = item.Field<Int64>("PCR_CW_PkeyID"),
                //        PCR_CW_WO_ID = item.Field<Int64>("PCR_CW_WO_ID"),
                //        PCR_CW_CompanyID = item.Field<Int64>("PCR_CW_CompanyID"),
                //        PCR_CW_Pressure_Test = item.Field<String>("PCR_CW_Pressure_Test"),
                //        PCR_CW_Upload_photo = item.Field<String>("PCR_CW_Upload_photo"),
                //        PCR_CW_IsActive = item.Field<Boolean>("PCR_CW_IsActive"),
                //    }).ToList();


                //objDynamic.Add(cyprexxGrassdata);

                //if (ds.Tables.Count > 1)
                //{
                //    var gethistoryData = ds.Tables[1].AsEnumerable();

                //    for (int i = 0; i < ds.Tables.Count; i++)
                //    {
                //        objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                //    }

                //    List<PCR_CyprexxWinterizationPressure_DTO> cyprexxGrassdata_History =
                //       (from item in gethistoryData
                //        select new PCR_CyprexxWinterizationPressure_DTO
                //        {
                //            PCR_CW_PkeyID = item.Field<Int64>("PCR_CW_PkeyID"),
                //            PCR_CW_WO_ID = item.Field<Int64>("PCR_CW_WO_ID"),
                //            PCR_CW_CompanyID = item.Field<Int64>("PCR_CW_CompanyID"),
                //            PCR_CW_Pressure_Test = item.Field<String>("PCR_CW_Pressure_Test"),
                //            PCR_CW_Upload_photo = item.Field<String>("PCR_CW_Upload_photo"),
                //            PCR_CW_IsActive = item.Field<Boolean>("PCR_CW_IsActive"),
                //            PCR_CW_ModifiedBy = item.Field<String>("PCR_CW_ModifiedBy"),
                //        }).ToList();

                //    objDynamic.Add(cyprexxGrassdata_History);

                //}


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}