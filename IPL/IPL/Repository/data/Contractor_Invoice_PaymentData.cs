﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class Contractor_Invoice_PaymentData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public List<dynamic> AddContractorPaymentData(Contractor_Invoice_PaymentDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
        
            string insertProcedure = "[CreateUpdate_Contractor_Invoice_Payment]";
      
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Con_Pay_PkeyId", 1 + "#bigint#" + model.Con_Pay_PkeyId);
                input_parameters.Add("@Con_Pay_Invoice_Id", 1 + "#bigint#" + model.Con_Pay_Invoice_Id);
                input_parameters.Add("@Con_Pay_Wo_Id", 1 + "#bigint#" + model.Con_Pay_Wo_Id);
                input_parameters.Add("@Con_Pay_Payment_Date", 1 + "#datetime#" + model.Con_Pay_Payment_Date);
                input_parameters.Add("@Con_Pay_Amount", 1 + "#decimal#" + model.Con_Pay_Amount);
                input_parameters.Add("@Con_Pay_CheckNumber", 1 + "#varchar#" + model.Con_Pay_CheckNumber);
                input_parameters.Add("@Con_Pay_Comment", 1 + "#varchar#" + model.Con_Pay_Comment);
                input_parameters.Add("@Con_Pay_EnteredBy", 1 + "#varchar#" + model.Con_Pay_EnteredBy);
                input_parameters.Add("@Con_Pay_Balance_Due", 1 + "#decimal#" + model.Con_Pay_Balance_Due);
                input_parameters.Add("@Con_Pay_IsActive", 1 + "#bit#" + model.Con_Pay_IsActive);
                input_parameters.Add("@Con_Pay_IsDelete", 1 + "#bit#" + model.Con_Pay_IsDelete);
              
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Con_Pay_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
               
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;



        }

        private DataSet GetContractorPaymentMaster(Contractor_Invoice_PaymentDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Contractor_Invoice_Payment]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Con_Pay_PkeyId", 1 + "#bigint#" + model.Con_Pay_PkeyId);
                input_parameters.Add("@Con_Pay_Wo_Id", 1 + "#bigint#" + model.Con_Pay_Wo_Id);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetContraactorPaymentDetails(Contractor_Invoice_PaymentDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                model.Con_Pay_Wo_Id = model.Pay_Wo_Id;
                model.Con_Pay_PkeyId = model.Pay_PkeyId; 

                 DataSet ds = GetContractorPaymentMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Contractor_Invoice_PaymentDTO> ContractorPayment =
                   (from item in myEnumerableFeaprd
                    select new Contractor_Invoice_PaymentDTO
                    {
                        Con_Pay_PkeyId = item.Field<Int64>("Con_Pay_PkeyId"),
                        Con_Pay_Invoice_Id = item.Field<Int64?>("Con_Pay_Invoice_Id"),
                        Con_Pay_Wo_Id = item.Field<Int64?>("Con_Pay_Wo_Id"),
                        Con_Pay_Payment_Date = item.Field<DateTime?>("Con_Pay_Payment_Date"),
                        Con_Pay_Amount = item.Field<Decimal?>("Con_Pay_Amount"),
                        Con_Pay_CheckNumber = item.Field<String>("Con_Pay_CheckNumber"),
                        Con_Pay_Comment = item.Field<String>("Con_Pay_Comment"),
                        Con_Pay_EnteredBy = item.Field<String>("Con_Pay_EnteredBy"),
                        Con_Pay_Balance_Due = item.Field<Decimal?>("Con_Pay_Balance_Due"),
                        Con_Pay_IsActive = item.Field<Boolean?>("Con_Pay_IsActive"),
                      

                    }).ToList();

              

                objDynamic.Add(ContractorPayment);

                //var myEnumerableFeaprd1 = ds.Tables[1].AsEnumerable();
                //List<Client_Invoice_PatymentDTO> ClientPayment =
                //   (from item in myEnumerableFeaprd1
                //    select new Client_Invoice_PatymentDTO
                //    {
                //        Client_Pay_PkeyId = item.Field<Int64>("Client_Pay_PkeyId"),
                //        Client_Pay_Invoice_Id = item.Field<Int64?>("Client_Pay_Invoice_Id"),
                //        Client_Pay_Wo_Id = item.Field<Int64?>("Client_Pay_Wo_Id"),
                //        Client_Pay_Payment_Date = item.Field<DateTime?>("Client_Pay_Payment_Date"),
                //        Client_Pay_Amount = item.Field<Decimal?>("Client_Pay_Amount"),
                //        Client_Pay_CheckNumber = item.Field<String>("Client_Pay_CheckNumber"),
                //        Client_Pay_Comment = item.Field<String>("Client_Pay_Comment"),
                //        Client_Pay_EnteredBy = item.Field<String>("Client_Pay_EnteredBy"),
                //        Client_Pay_Balance_Due = item.Field<Decimal?>("Client_Pay_Balance_Due"),
                //        Client_Pay_IsActive = item.Field<Boolean?>("Client_Pay_IsActive"),


                //    }).ToList();

                //objDynamic.Add(ClientPayment);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
        // Added By Dipali
        public List<dynamic> AddContractorPaymentDataList(List<Contractor_Invoice_PaymentDTO> modelList)
        {
            List<dynamic> objDataModel = new List<dynamic>();
            if (modelList != null)
            {
                foreach (var model in modelList)
                {
                    try
                    {
                        var objData = AddContractorPaymentData(model);
                        objDataModel.Add(objData);
                    }
                    catch (Exception ex)
                    {
                        log.logErrorMessage(ex.StackTrace);
                        log.logErrorMessage(ex.Message);
                    }
                }
            }                     
            return objDataModel;
        }
    }
}