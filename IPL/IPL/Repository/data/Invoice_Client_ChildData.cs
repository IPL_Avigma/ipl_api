﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class Invoice_Client_ChildData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public List<dynamic> AddInvoiceClientChildData(Invoice_Client_ChildDTO model)
        {

            string insertProcedure = "[CreateUpdateInvoice_Client_Child]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Inv_Client_Ch_pkeyId", 1 + "#bigint#" + model.Inv_Client_Ch_pkeyId);
                input_parameters.Add("@Inv_Client_Ch_ClientID", 1 + "#bigint#" + model.Inv_Client_Ch_ClientID);
                input_parameters.Add("@Inv_Client_Ch_Wo_Id", 1 + "#bigint#" + model.Inv_Client_Ch_Wo_Id);
                input_parameters.Add("@Inv_Client_Ch_Task_Id", 1 + "#bigint#" + model.Inv_Client_Ch_Task_Id);
                input_parameters.Add("@Inv_Client_Ch_Invoice_Id", 1 + "#bigint#" + model.Inv_Client_Ch_Invoice_Id);
                input_parameters.Add("@Inv_Client_Ch_Uom_Id", 1 + "#bigint#" + model.Inv_Client_Ch_Uom_Id);
                input_parameters.Add("@Inv_Client_Ch_Qty", 1 + "#nvarchar#" + model.Inv_Client_Ch_Qty);
                input_parameters.Add("@Inv_Client_Ch_Price", 1 + "#decimal#" + model.Inv_Client_Ch_Price);
                input_parameters.Add("@Inv_Client_Ch_Total", 1 + "#decimal#" + model.Inv_Client_Ch_Total);
                input_parameters.Add("@Inv_Client_Ch_Adj_Price", 1 + "#decimal#" + model.Inv_Client_Ch_Adj_Price);
                input_parameters.Add("@Inv_Client_Ch_Adj_Total", 1 + "#decimal#" + model.Inv_Client_Ch_Adj_Total);
                input_parameters.Add("@Inv_Client_Ch_Comment", 1 + "#nvarchar#" + model.Inv_Client_Ch_Comment);
                input_parameters.Add("@Inv_Client_Ch_IsActive", 1 + "#bit#" + model.Inv_Client_Ch_IsActive);
                input_parameters.Add("@Inv_Client_Ch_IsDelete", 1 + "#bit#" + model.Inv_Client_Ch_IsDelete);
                input_parameters.Add("@Inv_Client_Ch_Auto_Invoice", 1 + "#bit#" + model.Inv_Client_Ch_Auto_Invoice);
                input_parameters.Add("@Inv_Client_Ch_Flate_Fee", 1 + "#bit#" + model.Inv_Client_Ch_Flate_Fee);
                input_parameters.Add("@Inv_Client_Ch_Discount", 1 + "#decimal#" + model.Inv_Client_Ch_Discount);
                input_parameters.Add("@Inv_Client_Ch_Other_Task_Name", 1 + "#varchar#" + model.Inv_Client_Ch_Other_Task_Name);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Inv_Client_Ch_pkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return obj.SqlCRUD(insertProcedure, input_parameters);



        }
        private DataSet GetInvoiceClientChildMaster(Invoice_Client_ChildDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Invoice_Client_Child]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Inv_Client_Ch_pkeyId", 1 + "#bigint#" + model.Inv_Client_Ch_pkeyId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetInvoiceClientChildDetails(Invoice_Client_ChildDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetInvoiceClientChildMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Invoice_Client_ChildDTO> InvoiceClientChild =
                   (from item in myEnumerableFeaprd
                    select new Invoice_Client_ChildDTO
                    {
                        Inv_Client_Ch_pkeyId = item.Field<Int64>("Inv_Client_Ch_pkeyId"),
                        Inv_Client_Ch_ClientID = item.Field<Int64?>("Inv_Client_Ch_ClientID"),
                        Inv_Client_Ch_Wo_Id = item.Field<Int64?>("Inv_Client_Ch_Wo_Id"),
                        Inv_Client_Ch_Task_Id = item.Field<Int64?>("Inv_Client_Ch_Task_Id"),
                        Inv_Client_Ch_Invoice_Id = item.Field<Int64?>("Inv_Client_Ch_Invoice_Id"),
                        Inv_Client_Ch_Uom_Id = item.Field<Int64?>("Inv_Client_Ch_Uom_Id"),
                        Inv_Client_Ch_Qty = item.Field<String>("Inv_Client_Ch_Qty"),
                        Inv_Client_Ch_Price = item.Field<Decimal?>("Inv_Client_Ch_Price"),
                        Inv_Client_Ch_Total = item.Field<Decimal?>("Inv_Client_Ch_Total"),
                        Inv_Client_Ch_Adj_Price = item.Field<Decimal?>("Inv_Client_Ch_Adj_Price"),
                        Inv_Client_Ch_Adj_Total = item.Field<Decimal?>("Inv_Client_Ch_Adj_Total"),
                        Inv_Client_Ch_Comment = item.Field<String>("Inv_Client_Ch_Comment"),
                        Inv_Client_Ch_IsActive = item.Field<Boolean?>("Inv_Client_Ch_IsActive"),

                    }).ToList();

                objDynamic.Add(InvoiceClientChild);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}