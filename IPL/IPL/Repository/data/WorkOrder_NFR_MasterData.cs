﻿using Avigma.Repository.Lib;
using IPL.Models;
using IPL.Models.FiveBrothers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class WorkOrder_NFR_MasterData

    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        public List<dynamic> AddWorkOrder_NFRData(WorkOrder_NFR_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_WorkOrder_NFR_Master]";
            WorkOrder_NFR_Master workOrder_NFR_Master = new WorkOrder_NFR_Master();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@WNFR_PkeyID", 1 + "#bigint#" + model.WNFR_PkeyID);
                input_parameters.Add("@WNFR_Order", 1 + "#varchar#" + model.WNFR_Order);
             
                input_parameters.Add("@WNFR_To", 1 + "#nvarchar#" + model.WNFR_To);
                input_parameters.Add("@WNFR_Date_Rec_Contact", 1 + "#datetime#" + model.WNFR_Date_Rec_Contact);
                input_parameters.Add("@WNFR_Due_Date", 1 + "#datetime#" + model.WNFR_Due_Date);
                input_parameters.Add("@WNFR_MTG_Co", 1 + "#nvarchar#" + model.WNFR_MTG_Co);
                input_parameters.Add("@WNFR_Account", 1 + "#nvarchar#" + model.WNFR_Account);
                input_parameters.Add("@WNFR_LoanType", 1 + "#nvarchar#" + model.WNFR_LoanType);
                input_parameters.Add("@WNFR_MTGR_Name", 1 + "#nvarchar#" + model.WNFR_MTGR_Name);
                input_parameters.Add("@WNFR_MTGR_Address", 1 + "#nvarchar#" + model.WNFR_MTGR_Address);
                input_parameters.Add("@WNFR_Requierment_Details", 1 + "#nvarchar#" + model.WNFR_Requierment_Details);
                input_parameters.Add("@WNFR_Requierment_Name", 1 + "#nvarchar#" + model.WNFR_Requierment_Name);
                input_parameters.Add("@WNFR_WorkType", 1 + "#nvarchar#" + model.WNFR_WorkType);
                input_parameters.Add("@WNFR_gpsLatitude", 1 + "#nvarchar#" + model.WNFR_gpsLatitude);
                input_parameters.Add("@WNFR_gpsLongitude", 1 + "#nvarchar#" + model.WNFR_gpsLongitude);
                input_parameters.Add("@WNFR_ImportMaster_Pkey", 1 + "#bigint#" + model.WNFR_ImportMaster_Pkey);
                input_parameters.Add("@WNFR_Import_File_Name", 1 + "#nvarchar#" + model.WNFR_Import_File_Name);
                input_parameters.Add("@WNFR_Import_FilePath", 1 + "#nvarchar#" + model.WNFR_Import_FilePath);
                input_parameters.Add("@WNFR_Import_Pdf_Name", 1 + "#nvarchar#" + model.WNFR_Import_Pdf_Name);
                input_parameters.Add("@WNFR_Import_Pdf_Path", 1 + "#nvarchar#" + model.WNFR_Import_Pdf_Path);
                input_parameters.Add("@WNFR_City", 1 + "#nvarchar#" + model.WNFR_City);
                input_parameters.Add("@WNFR_State", 1 + "#nvarchar#" + model.WNFR_State);
                input_parameters.Add("@WNFR_Zip", 1 + "#nvarchar#" + model.WNFR_Zip);


                input_parameters.Add("@WNFR_wo_id", 1 + "#nvarchar#" + model.WNFR_wo_id);
                input_parameters.Add("@WNFR_username", 1 + "#nvarchar#" + model.WNFR_username);
                input_parameters.Add("@WNFR_keycode", 1 + "#nvarchar#" + model.WNFR_keycode);
                input_parameters.Add("@WNFR_lockbox", 1 + "#nvarchar#" + model.WNFR_lockbox);
                input_parameters.Add("@WNFR_address", 1 + "#nvarchar#" + model.WNFR_address);

                input_parameters.Add("@WNFR_NFR_ID", 1 + "#nvarchar#" + model.WNFR_NFR_ID);

             
                input_parameters.Add("@WNFR_IsActive", 1 + "#bit#" + model.WNFR_IsActive);
                input_parameters.Add("@WNFR_IsDelete", 1 + "#bit#" + model.WNFR_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@WNFR_PkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    workOrder_NFR_Master.WNFR_PkeyID = "0";
                    workOrder_NFR_Master.Status = "0";
                    workOrder_NFR_Master.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    workOrder_NFR_Master.WNFR_PkeyID = Convert.ToString(objData[0]);
                    workOrder_NFR_Master.Status = Convert.ToString(objData[1]);


                }
                objcltData.Add(workOrder_NFR_Master);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;

        }

        public List<dynamic> AddWorkOrder_NFRItemData(WorkOrder_NFR_Item_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_WorkOrder_NFR_Item_Master]";
            WorkOrder_NFR_Item_Master workOrder_NFR_Item_Master = new WorkOrder_NFR_Item_Master();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@WNFRINS_PkeyId", 1 + "#bigint#" + model.WNFRINS_PkeyId);
                input_parameters.Add("@WNFRINS_Ins_Name", 1 + "#nvarchar#" + model.WNFRINS_Ins_Name);
                input_parameters.Add("@WNFRINS_Ins_Details", 1 + "#nvarchar#" + model.WNFRINS_Ins_Details);
                input_parameters.Add("@WNFRINS_Qty", 1 + "#bigint#" + model.WNFRINS_Qty);
                input_parameters.Add("@WNFRINS_Price", 1 + "#decimal#" + model.WNFRINS_Price);
                input_parameters.Add("@WNFRINS_Total", 1 + "#decimal#" + model.WNFRINS_Total);
                input_parameters.Add("@WNFRINS_FkeyID", 1 + "#bigint#" + model.WNFRINS_FkeyID);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@WNFRINS_PkeyIdOut", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    workOrder_NFR_Item_Master.WNFRINS_PkeyId = "0";
                    workOrder_NFR_Item_Master.Status = "0";
                    workOrder_NFR_Item_Master.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    workOrder_NFR_Item_Master.WNFRINS_PkeyId = Convert.ToString(objData[0]);
                    workOrder_NFR_Item_Master.Status = Convert.ToString(objData[1]);

                }
                objcltData.Add(workOrder_NFR_Item_Master);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;



        }

        public List<dynamic> AddNFROrders(FBAPI_RequestDTO model)
        {
            List<dynamic> objDataModel = new List<dynamic>();
            // model.ConPassword = securityHelper.GetMD5Hash(model.ConPassword);

            //string URL = model.ApiUrl.Trim() + "orders" + "?contractor=" + model.ConUserName + "&key=" + model.ConPassword;
            try
            {
                //byte[] credentials = UTF8Encoding.UTF8.GetBytes(model.AuthUserName + ":" + model.AuthPassword);
                //System.Net.Http.HttpClient client = new System.Net.Http.HttpClient();

                //client.BaseAddress = new System.Uri(URL);

                //client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(credentials));

                //client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                //HttpResponseMessage messge = client.GetAsync(URL).Result;
                //if (messge.IsSuccessStatusCode)
                //{
                //string result = messge.Content.ReadAsStringAsync().Result;    


               // string result = "{\"work_order\":{\"order\":\"8204279\",\"property_id\":\"592738\",\"order_type\":\"PRESALE BID APPROVAL\",\"assigned_date\":\"10/30/2020\",\"due_date\":\"11/2/2020\",\"priority\":\"72 Hour Standard\"},\"instructions\":[{\"instruction_name\":\"key_or_lock_instructions\",\"description\":\"**********WF01 -  BID APPROVAL**********\",\"additional_details\":\"\u2022 A PROPERTY VALIDATION PHOTO IS PROVIDED AT THE BOTTOM OF THIS WORK ORDER. THIS IS TO ENSURE YOU ARE AT THE CORRECT LOCATION AND PROPERTY FOR ANY AND ALL WORK PERFORMED.  YOU MUST VALIDATE YOU ARE AT THE PROVIDED PROPERTY BEFORE ANY WORK IS STARTED. IF THERE ARE ANY QUESTIONS REGARDING PROPERTY VALIDATION, PLEASE CALL MSI FROM SITE FOR DIRECTION AT 1-800-825-4101 (OPTION 9 FOR REGION 9).********DO NOT ENTER PROPERTY UNLESS THERE IS A POST SECURE NOTICE POSTED UPON ARRIVAL AT PROPERTY. IF THERE IS NO POST SECURE NOTICE, CALL FROM SITE 1-800-825-4101 (OPTION 9 FOR REGION 9)**********   \u2022 PROPERTY MAY ALREADY BE SECURED: LOCKBOX _______0552__________/ KEY CODE ________67767___________                                                                                                                                                     \u2022 IF UNABLE TO ENTER USING THE EXISTING KEY CODE, CALL FROM SITE 1-800-825-4101 (OPTION 9 FOR REGION 9).         \u2022 IF UNABLE TO GAIN ACCESS DUE TO GATED COMMUNITY CALL FROM SITE 1-800-825-4101 (OPTION 9 FOR REGION 9) (OBTAIN NAME/PHONE #/FAX # OF HOA, COA, ETC. AS NEEDED).\u2022 IF REPORTED AS OCCUPIED, ATTEMPT DIRECT CONTACT AND REPORT OCCUPIED STATUS ACCESS CODE: A389/67767/0552\"},{\"instruction_name\":\"general_instructions\",\"description\":\"********** WF01 - BID APPROVAL**********\",\"additional_details\":\"RECEIVED BID APPROVAL FOR THE FOLLOWING: (PLEASE COMPLETE AND INVOICE WITHIN THE APPROVED AMOUNT ONLY).1.) BOARD BROKEN WINDOWS. MEASUREMENTS ARE 36X36, 26X17, 36X32, 75X34 (292 UNITED INCHES). WILL REQUIRE PLYWOOD, BOARDS, BOLTS, AND BASIC TOOLS. 2 MEN X 2 HOURS $262.802.) TOTAL $262.803.) PROVIDE PHOTOS TO SUPPORT WORK COMPLETED4.) BID FOR ANY OTHER DAMAGES AT PROPERTY5.)6.)7.)8.)9.)10.)*****PLEASE ADVISE OF ANY HEALTH/HAZARD CONCERNS (INCLUDING ZIKA/WEST NILE)*****\u2022 IF UNABLE TO COMPLETE WITHIN REQUIRED TIMEFRAMES THE REP IS RESPONSIBLE FOR NOTIFYING MSI WITHIN 48 HOURS.\u2022 EMERGENT CONDITIONS: CALL FROM SITE  1-800-825-4101 (OPTION 9 FOR REGION 9) TO REPORT EMERGENT CONDITIONS .  (BASEMENT FLOODING, ACTIVELY LEAKING ROOF WITH VISIBLE HOLES, OPENINGS TO PROPERTY OR OUT BUILDINGS, UNSECURED POOL ETC.)\u2022 VIOLATIONS: IF PROPERTY IS POSTED WITH ANY VIOLATIONS, CONDEMNATIONS, OR DEMOLITION ORDERS, CALL MSI (WF01 REQUIREMENT) AT 1-800-825-4101 - OPTION 9 FOR REGION 9 TO REPORT THE VIOLATION.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       \u2022 BIG 6 DAMAGES: REPORT AND PROVIDE PHOTOS FOR ALL BIG 6 DAMAGES AND STRUCTURAL DAMAGE.                                                                                                                                                                                                                                                                                                                                                                          \u2022 BID REQUIREMENTS: SUBMIT ALL BIDS TO ADDRESS DAMAGES WITH DETAILS INCLUDING BUT NOT LIMITED TO: DIMENSIONS, LOCATION, DESCRIPTION, MATERIALS TO BE USED, ETC.  BIDS MUST BE SUBMITTED FOR ALL ICC ISSUES ON EACH ORDER REGARDLESS IF THEY\u2019VE BEEN BID PREVIOUSLY. BIDS SHOULD INCLUDE REPAIRS TO PREVENT DAMAGE TO PROPERTY. ALL BIDS MUST BE ITEMIZED AND NOT COMBINED (INTERIOR/EXTERIOR, DEBRIS/PERSONALS, REPAIR/REPLACE, ETC.) OR THEY WILL BE REJECTED. ALL BID LINE ITEMS MUST BE SUPPORTED AND JUSTIFIABLE BY A GOVERNMENT COST ESTIMATOR.*****THE REP WILL BE HELD RESPONSIBLE FOR NOT BIDDING ALL CONVEYANCE ITEMS. IF ADDITIONAL ITEMS ARE NEEDED AND PHOTOS DON\u2019T JUSTIFY THE CONDITION CHANGE THEN REP WILL COMPLETE WORK AT THEIR EXPENSE.*****\"},{\"instruction_name\":\"winterization_instructions\",\"description\":\"\",\"additional_details\":\"PLEASE SEE GENERAL INSTRUCTIONS FOR DIRECTION\"},{\"instruction_name\":\"photo_instructions\",\"description\":\"********** WF01 - BID APPROVAL**********\",\"additional_details\":\"SUBMIT CLEAR AND DETAILED PHOTOS WITH DATE STAMPS, AND LABEL BY ROOM AND LOCATION************BEST PRACTICES TO CONSIDER: PHOTOS OF WHITEBOARD OR PAPER WITH #1, #2 TO SUPPORT THE NUMBER OF TRUCK LOADS, BAGS OF DEBRIS, NUMBER OF TREES, ETC.; PHOTOS OF A TAPE MEASURE SHOWING THE SIZE OF TRUCK TRAILER, AREA OF MOLD REMEDIATED, HEIGHT OF GRASS, ETC**           PHOTOS SHOULD INCLUDE BUT ARE NOT LIMITED TO THE FOLLOWING:\u2022 PROPERTY ADDRESS AND STREET SIGN (REQUIRED)\u2022 PHOTO OF STREET SCENE\u2022 SECURE POSTING (REQUIRED) \u2013 MUST BE CLEAR AND ABLE TO BE READ. (REQUIRED IN ORDER TO RECEIVE PAYMENT)\u2022 LOCK CHANGE: BEFORE, DURING, AND AFTER PHOTOS ARE REQUIRED OF THE LOCK CHANGE PROCESS (IF APPLICABLE).\u2022 DAMAGES AND REPAIRS: PHOTOS MUST SUPPORT THE DESCRIPTION, LOCATION, MEASUREMENTS, AND MATERIALS NEEDED AS LISTED IN BIDS (INCLUDING MOLD DAMAGES).\u2022 MOLD: IF MOLD DAMAGES ARE REPORTED, CLEAR PHOTOS USING TAPE MEASURE, PAINTERS TAPE, OR CHALK SHOWING DIMENSIONS ARE REQUIRED. \u2022 ROOF AND CEILING: PHOTOS OF EXTERIOR ROOF AND INTERIOR CEILINGS OF EVERY ROOM REQUIRED REGARDLESS OF CONDITION (MUST SHOW DAMAGES IF DAMAGES ARE REPORTED). \u2022 EXTERIOR REQUIREMENTS: EACH SIDE OF PROPERTY FROM THE SAME ANGLE (FRONT, REAR, AND SIDES). PHOTOS OF ALL EXTERIOR STRUCTURES REQUIRED. PHOTOS SHOULD ALSO BE TAKEN FROM THE HOUSE OUT TOWARD THE PROPERTY LINE.  IF THERE IS A PRIVACY FENCE, PHOTOS SHOULD BE TAKEN ON BOTH SIDES OF FENCE IF POSSIBLE.\u2022 INTERIOR REQUIREMENTS: PHOTOS (WIDE ANGLE) OF EVERY ROOM, INCLUDING: CLOSETS, CEILINGS AND FLOORS, CORNERS, ATTICS, BASEMENTS, OPENED DRAWERS AND CABINETS, ALL APPLIANCES, ELECTRICAL PANELS, STAIRWAYS, HALLWAYS AND FIREPLACES.  \u2022 PERSONAL PROPERTY: MUST HAVE CLEAR PHOTOS OF ALL INTERIOR AND EXTERIOR PERSONALS TO SUPPORT CONDITION OF PROPERTY OR WORK COMPLETED. PROVIDE PHOTOS OF ALL ENTRANCES THAT HAVE PERSONAL PROPERTY POSTINGS AND NOTICE OF RIGHT TO RECLAIM POSTINGS. MUST PROVIDE PHOTOS TO SUPPORT THE NUMBER OF TRUCK LOADS, BAGS OF PERSONALS AND PHOTOS OF TAPE MEASUREMENTS SHOWING THE SIZE OF TRUCK TRAILER ETC.\u2022 STORAGE OF PERSONAL PROPERTY: BEFORE, DURING AND AFTER PHOTOS SHOWING THE STORAGE UNIT WITH THE PERSONAL PROPERTY INSIDE. AFTER PHOTOS ALSO MUST SUPPORT THE STORAGE UNIT WITH THE PERSONAL PROPERTY INSIDE AND THE UNIT IS CLOSED AND LOCKED. MUST PROVIDE A COPY OF STORAGE AGREEMENT/RECEIPT.\u2022 DEBRIS: MUST HAVE CLEAR PHOTOS OF ALL INTERIOR AND EXTERIOR DEBRIS TO SUPPORT CONDITION OF PROPERTY AND WORK COMPLETED. MUST PROVIDE PHOTOS TO SUPPORT THE NUMBER OF TRUCK LOADS, BAGS OF PERSONALS AND PHOTOS OF TAPE MEASUREMENTS SHOWING THE SIZE OF TRUCK TRAILER ETC.\u2022 MULTI UNIT: INCLUDE PHOTOS OF COMMON AREAS SUCH AS STAIRWELLS, HALLWAYS, LOBBIES, ETC. AND LABEL PHOTOS WITH UNIT NUMBER.\u2022 WORK COMPLETED: BEFORE, DURING, AND AFTER PHOTOS (WIDE ANGLE) ARE REQUIRED OF ALL WORK COMPLETED, INCLUDING TRASH AND DEBRIS REMOVAL (TRUCK EMPTY AND FULL, ROOMS FULL AND EMPTY, ETC.)\u2022 BIDS: PHOTOS MUST SUPPORT THE DESCRIPTION, LOCATION, MEASUREMENTS, AND MATERIALS NEEDED AS LISTED IN BIDS.\u2022 APPLIANCES: BEFORE, DURING, AND AFTER PHOTOS REQUIRED OF ALL FIXTURES, INCLUDING ICE MAKERS, DISH WASHERS, WATER HEATER, BLOWING LINES, TURNING OFF BREAKERS, SUMP PUMPS, GENERATOR HOOKED TO COMPRESSOR, GAUGES OF PRESSURE TESTING (AT 35 PSI), ETC.\u2022 ELECTRIC METER: MUST BE ABLE TO READ DETAILS ON METER.\u2022 MOBILE HOMES: MUST PROVIDE CLEAR PHOTOS OF DATA PLATE INCLUDING VIN AND SERIAL NUMBERS. \u2022**********WINTERIZATION SECTION BELOW**********\u2022 BEFORE, DURING, AND AFTER PHOTOS FROM THE SAME ANGLE, OF THE ENTIRE WINTERIZATION PROCESS ARE REQUIRED.\u2022 FROZEN PROPERTY: PHOTOS MUST BE PROVIDED THAT CONFIRM THE PROPERTY IS FROZEN (TANK FROZEN, LINES BURST AND FROSTED, ETC.) \u2022 BREAKERS: PHOTOS OF BREAKERS TURNED TO OFF POSITION, UNLESS SUMP PUMP OR DEHUMIDIFIER IS PRESENT.\u2022 DRAINING: PHOTOS MUST SHOW THE WATER COMING OUT OF A HOSE ATTACHED TO THE HOT WATER HEATER AND DRAINING TO THE EXTERIOR OF THE PROPERTY.\u2022 PLUMBING: PHOTOS TO CONFIRM WATER IS BLOWN OUT OF PIPES AND THAT THE TOILET WATER HAS ALSO BEEN DRAINED BOTH IN THE TANK AND IN THE BOWL.\u2022 TOILETS: IF TOILETS ARE REPORTED DIRTY AND CANNOT COME CLEAN, ACTION SHOTS OF CLEANING ARE REQUIRED.\u2022 PRESSURE GAUGE: PHOTOS OF THE PRESSURE GAUGE ARE REQUIRED ON EVERY WINTERIZATION. THE PRESSURE TEST IS REQUIRED TO HOLD 35 PSI FOR 20 MINUTES.\u2022 NON-TOXIC ANTIFREEZE: PHOTOS OF EVERY TRAP HAVING NON-TOXIC ANTIFREEZE POURED IN THEM MUST BE PROVIDED. THE TRAPS INCLUDE, BUT ARE NOT LIMITED TO: TOILET BOWLS AND TOILET TANKS, DISHWASHER DRAINS, ALL SINKS, SHOWER/TUB DRAINS, WASHING MACHINE DRAIN, ANY EXTERIOR APPLIANCE ATTACHED TO INTERIOR PLUMBING (HOT TUBS), ETC.\u2022 WINTERIZATION STICKERS: PHOTOS OF ALL STICKERS PLACED WITH WELLS FARGO CONTACT INFORMATION.\"}],\"point_of_service_question\":[{\"question\":\"1. Has a code violation or official notice been posted at the property?\",\"answer\":\"1.YES 2. NO 3. N/A\"},{\"question\":\"2. Exterior debris, junk, trash or dead vegetation found at the property?\",\"answer\":\"1.YES 2. NO 3. N/A\"},{\"question\":\"3. Is the lawn landscaped and maintained?\",\"answer\":\"1.YES 2. NO 3. N/A\"},{\"question\":\"4. Any observed Escalated Events or Issues at the property?\",\"answer\":\"1.YES 2. NO 3. N/A\"},{\"question\":\"5. Any unsecured openings,broken windows or doors at the property?\",\"answer\":\"1.YES 2. NO 3. N/A\"},{\"question\":\"6. Is there roof damage that needs repair?\",\"answer\":\"1.YES 2. NO 3. N/A\"},{\"question\":\"7. Is the property vacant?\",\"answer\":\"1.YES 2. NO 3. UNKNOWN\"},{\"question\":\"8. Does the property have indications of vandalism or an obvious crime scene?\",\"answer\":\"1.YES 2. NO 3. N/A\"},{\"question\":\"9. Is there any obvious health and safety issue at the property?\",\"answer\":\"1.YES 2. NO 3. N/A\"},{\"question\":\"10. Does the following damage exist at the property that meets the Big Six definition: Fire, Flood, Tornado, Hurricane Boiler(Multi-Unit), Earthquake?\",\"answer\":\"1.YES 2. NO 3. N/A\"}],\"vendor\":\"MOMDLEES SUMMIT, MO 64064\",\"mortgagor\":\"PUGH, JOSEPH D409  CHESTNUT, MONROE CITY, MO 63456\",\"client\":{\"client_code\":\"WF01\",\"loan\":\"****0552\",\"pos_required\":\"Yes\",\"loan_or_investor_type\":\"VA\",\"send_to_aspen\":\"Yes\",\"hoa_contact\":\"\",\"hoa_phone\":\"\"},\"property_info\":{\"ftv_date\":\"09/22/2020\",\"initial_secure_date\":\"07/30/2020\",\"last_winterization_date\":\"\",\"last_inspected_date\":\"10/28/2020\",\"last_lock_change_date\":\"07/30/2020\",\"last_snow_removal_date\":\"\",\"last_insp_occupancy\":\"VACANT\",\"lockbox\":\"0552\",\"initial_grass_cut_date\":\"\",\"property_type\":\"SINGLE FAMILY\",\"keycode\":\"A389/67767/0552\",\"last_grass_cut_date\":\"10/27/2020\",\"color\":\"\",\"security_access_code\":\"\",\"lawn_size_sq_ft\":\"9,440\"}}";
                string result = "{\n   \"work_order\":\"838036\",\n   \"to\":\"PT-024-MO (Jamie M. Wheatley WILL Enteprises, LLC)\",\n   \"date_rep_contact\":\"05/21/2021\",\n   \"date_due\":\"05/26/2021\",\n   \"mtg_co\":\"Alabama Housing Finance A\",\n   \"account\":\"3692-072-515\",\n   \"loan_type\":\"FHA  MO-16\",\n   \"mtgr_name\":\"Dyanna Klinger\",\n   \"mtgr_address\":\"112N Polk St Jefferson City, MO 65101\",\n   \"epa_lead_requirements\":\"06/10/2021 UPDATE\\nPlease advise why front door was secured instead of a secondary door as per instructions\\nPlease advise if broken window is secure with a 2nd pane, if window is broken through you need\\nto reglaze for the allowable\\nPlease confirm there is a door behind the damaged storm door\\nNeed all mold/discoloration treated as this is within the allowable. Not sure why you only\\ntreated one area\\nNeed after photos clearly showing caps on the water lines\\nNeed clear photos of saplings so we can see the number of saplings\\nNeed handrail installed at side door for the HUD allowable if 18\\\" or over from the ground\\nPlease advise if there are any outbuildings on site-if so need full interior & exterior photos\\nand need them secured\\nNeed photo showing the water is off at the main & curb\\nNeed detailed bid to repair/replace any damaged fencing\\nNeed bid to replace the drywall & the insulation you are biding to remove\",\n   \"requirements_for_this_work_order\":{\n      \"points\":[\n         \"Secure\",\n         \"Board/Screen\",\n         \"Pool/Spa/Pond\",\n         \"Roof Leak\",\n         \"Debris\",\n         \"Utility Reporting\",\n         \"Moisture\",\n         \"Flooding\",\n         \"Sump pump\",\n         \"Lawn Care\",\n         \"Trimming\",\n         \"Damages\",\n         \"Lockbox\",\n         \"Pressure test\"\n      ],\n      \"title\":\"\"\n   },\n   \"instructions\":[\n      {\n         \"instruction_name\":\"Initial Secure\",\n         \"instruction_details\":\"If occupied; direct contact must be made and contact information obtained\\n\\nIF ABANDONED LIVE ANIMALS, NOTIFY AUTHORITIES provide photos to support removal.\\nProvide name and number of who you have contacted and condition of animals.\\n\\nIF ANY GUNS OR AMMO, NOTIFY AUTHORITIES have the police remove with photos to\\nsupport removal; provide name and badge number of the officer completing removal.\\n\\nIF VIOLATION POSTED a clear & legible photo of the violation must be taken and\\n  sent to NFR.  You must:\\n- Attempt contact with code enforcement;\\n- Abate any violation issues;\\n- If unable to address within HUD allowable;  provide estimates and specify for\\n  violation abatement\\n- Provide name and number for code enforcement.\\n\\nIF MOBILE HOME provide clear photos of data sheet, provide VIN#(s), photos of\\nunderpinning/foundation, axles and tongue and tags\"\n      },\n      {\n         \"instruction_name\":\"SECURING\",\n         \"instruction_details\":\"- Secure one secondary entry with Kwikset 35453 knob lock and deadbolt if applicable\\n- Install 1750 or OCN lockbox with 2 working keys inside on the secured door\\n- Unplug automatic garage door openers and secure door(s)\\n- Secure detached garage and outbuilding with padlock A389 if no other locking\\n  mechanism exists – If unable to secure or if the door is damaged, provide a bid to\\n  repair/replace\\n* DO NOT BRACE / DO NOT NAIL DOORS OR WINDOWS SHUT\\n-Double lock sliding glass doors using HUD allowable\\n-Lock all windows with existing hardware.  If unable, install window locks using HUD\\n  allowable.  Remove ALL broken glass from interior and exterior\\n-If abandoned live animals, notify authorities. Provide photos to support removal.    Provide\\nname and number of who removed and the condition of animals\\n-If any guns or ammo notify authorities for proper removal with photos to support.    Provide\\nname and badge number\\n-If violation posted, a clear and legible photo of the violation must be provided.    You must:\\nattempt contact with code enforcement, provide a bid to abate violation(s).  Provide contact\\ninformation for code enforcement\"\n      },\n      {\n         \"instruction_name\":\"PRESSURE TESTING\",\n         \"instruction_details\":\"-Complete the Initial Water Supply Line Pressure Test for the FHA allowable of $20.\\n-Provide date and time stamp photos showing the elapsed time of the test; 35PSI for 30  minutes\"\n      },\n      {\n         \"instruction_name\":\"BOARDING/REGLAZING\",\n         \"instruction_details\":\"-Ensure all windows are closed and locked\\n-Reglaze broken or cracked windows within HUD allowable of $1.50/U.I.  If unable to\\n reglaze due to damage; Bolt board opening with CDX plywood at $0.90/U.I.\\n provide photos to justify the damage to the frame and provide a bid to replace the\\n window. Include window dimensions and window type; bid should be for single pane only\\n-If unable to reglaze due to safety reason or high vandal area, where boarding is\\nacceptable; Bolt board opening with CDX plywood at $0.90/U.I. and provide an estimate  to\\nreglaze. The need for boarding must be proven with a copy of local code,    neighborhood shots\\ndocumenting other boarded properties, criminal statistics, etc.\\n\\n-Remove all broken window glass from interior & exterior\"\n      },\n      {\n         \"instruction_name\":\"POOLS/SPA/PONDS\",\n         \"instruction_details\":\"- Install mesh safety cover on in-ground pools, built in hot tubs and spas within HUD\\n  allowable\\n- Secure free standing hot tubs and spas with existing cover or bid to secure\\n- Cover above-ground pools surrounded by decking only within HUD allowable of $500\\n- If above ground pool is not surrounded by decking; drain & remove pool within HUD\\n  allowable. Provide bid to remove built up decking and fill in ground depression\\n  as separate line items to be justified with CE\\n- Provide bids to address pools per local code (secure, drain, maintenance, etc) if\\n  they exceed HUD's requirements\\n- Sufficient documentation must be provided if required by AHJ (Authority Having\\n  Jurisdiction).\\n- Provide bid to winterize pool/spas/hot tubs\\n- Provide bids to address small backyard ponds, water gardens or other water features\"\n      },\n      {\n         \"instruction_name\":\"ROOF/GUTTERS\",\n         \"instruction_details\":\"- If roof leak present (including attached garages, porches, patios, detached\\n  garages and any secondary  structures), patch/repair within HUD allowable of\\n  $1000.00 for permanent repair.  If chimney cap is necessary to stop water\\n  infiltration, install/replace within HUD allowable of $100/per.\\n- If unable to patch/repair, install brown or black tarp as temporary fix within HUD\\n  allowable ($600.00) and provide bid for permanent repair.\\n\\n*Do not tarp flat roofs that do not allow water to run off*\\n\\t\\t\\t\\n- Clean/repair/reattach/replace existing gutters/downspouts as needed $1.00/foot for\\n  cleaning/repair ($100 max), $4.70/foot for replacement ($400 max). Photos must\\n  support scope of work  completed\\n- Provide both interior & exterior photos to document condition of roof (photos are\\n  needed at roof level and from under side of decking if accessible)\\n\\n*IMPORTANT:  On all roof related issues, be sure to provide ALL DETAILS needed\\n for CE on the NFR Roofing Bid form.\"\n      },\n      {\n         \"instruction_name\":\"DEBRIS\",\n         \"instruction_details\":\"-Provide separate estimates to remove interior/exterior debris/hazards/personals\\n*All items are to be bid per cubic yard)\\n*All items are to be bid separated by scope (interior vs. exterior)(personals vs.   debris)\\n*Interior of outbuildings are considered interior debris/personals\\n-PERSONALS MUST BE ADDRESSED PER LOCAL LAW/CODE AND COPY OF LAW/CODE MUST BE\\n PROVIDED TO DOCUMENT REIMBURSABLE EXPENSES\\n- Unplug refrigerators and remove doors if required by local ordinance\\n- Provide bid to clean refrigerators and freezers\\n- Advise if abandoned autos on site to be addressed at conveyance\\n\\n*Provide bids to address any other items with HUD allowables\"\n      },\n      {\n         \"instruction_name\":\"UTILITIES\",\n         \"instruction_details\":\"-Electric, gas, and water must be turned off unless it is necessary for a\\n dehumidifier, sump pump, or heat. Provide utility providers name & contact\\n information\\n-Cap lines within allowable; water/sewer/gas lines with no shutoff valve $15.00/per  (up to 6\\ncaps per property). If more, do none and provide an estimate for all caps\\n-Cap bare wires up to 25 wires at $1.00/per if electric being transferred. If more,\\n do none and provide an estimate for all caps\\n-Provide photos of all meters (electric, water, etc)\"\n      },\n      {\n         \"instruction_name\":\"MOISTURE ISSUES\",\n         \"instruction_details\":\"- If there is standing water or flooding, pump and dry the affected area within the\\n  HUD allowable up to $500.00 (CE must justify)\\n- If there are any signs of dampness including high/trapped humidity, mildew, mold,\\n  etc. advise of the cause of the moisture issue (roof leak, flooding, etc.)\\n- Provide bid to install a dehumidifier if it will address moisture and provide\\n  utility provider's name and contact info for transfer/restore\\n- Note location and provide description of water infiltration/moisture\\n\\n- If mold is present, remediate mold within the HUD allowable ($300.00)\\n*ONLY IF SOURCE HAS BEEN REMEDIATED\\n- If source has not been remediated, provide bids to remediate source and mold\\n- If necessary bid to install absorbent moisture desiccants (e.g. DampRid)\\n- Bleach clean visible affected area.  KILZ paint visible affected area\\n- Advise location (room), description (wall/ceiling/floor), and dimensions\"\n      },\n      {\n         \"instruction_name\":\"FLOODING\",\n         \"instruction_details\":\"*If there is an ACTIVE leak or flooding anywhere within the main structure:\\n-Address the source of the water (shut off water at the main, street, fixture, etc)\\n-Pump any standing water within $500 allowable; with documentation/details/photos to\\n  support\\n-Ensure entire area is dried to prevent mold growth\\n-If there are any wet/moisture damages (i.e: Insulation, sheetrock, flooring, debris)   caused\\nby the flooding; be sure to provide a bid to remove, dry out, replace, etc as  appropriate\"\n      },\n      {\n         \"instruction_name\":\"SUMP PUMP\",\n         \"instruction_details\":\"- Advise if there is a sump pump on site and provide utility company contact info with\\n  a photo of the meter\\n- If sump pump found INOPERABLE and electric is ON REPAIR/REPLACE within ALLOWABLE\\n ($50.00 for repair, $300.00 for replacement) Photos and details must support\\n invoiced amount\\n-If sump pump found INOPERABLE and electric is OFF, provide bid to REPAIR/REPLACE   with photos\\nand details to   support\\n- Advise if the electric is on and provide the utility provider's name & contact info.\\n-If there are any wet/moisture damages (ie: insulation, sheetrock, flooring,\\n debris) caused by the flooding; be sure to provide a bid to address\"\n      },\n      {\n         \"instruction_name\":\"YARD CARE\",\n         \"instruction_details\":\"*If lot is distinctly wooded, only address the maintainable portion of the yard.\\n-Maintain lawn and yard areas by performing grass cutting, weeding, edge trimming,\\n sweeping/blowing of all paved areas, and removal of all lawn clippings, related\\n clippings, and incidental debris removal.\\n\\n-PROVIDE PHOTOS OF TAPE MEASURE/RULER DOCUMENTING BEFORE AND AFTER GRASS HEIGHT.\\n FRONT, REAR, AND SIDE YARDS MUST BE CAPTURED IN BEFORE/DURING/AFTER PHOTOS.\\n\\n-Grass must be cut to a maximum of 2 inches in height\\n-Grass and weeds must be cut to the edge of the property line and trimmed around\\n  foundations, bushes, trees, and planting beds\\n-Desert landscaping must be completed through the removal or spraying of weeds, grass\\n  trimming or cutting, and the removal of related cuttings and incidental debris\\n-Recuts will be generated on a separate order\"\n      },\n      {\n         \"instruction_name\":\"TRIMMING\",\n         \"instruction_details\":\"- Trim shrubs/trees touching the house, garage, outbuildings or obstructing public\\n  right of way within HUD allowables ($250max for trees and $200 max for shrubs) A CE\\n  will be ran to support amount invoiced, please provide necessary details\\n- If unable to complete trimming for the allowable, provide detailed bids.  Be sure to\\n  provide:\\n  SHRUBS: location, if touching,  lf, height and cyds of clippings;\\n  TREES:location, if touching, height, qty and cyds of clippings\\n- Provide detailed bids to address saplings and vines.\\n  Vines – ON STRUCTURE: provide lf and height (condensed), advise poisonous or non\\n  Poisonous; ON GROUND: provide sqft and height, advise poisonous or non-poisonous\"\n      },\n      {\n         \"instruction_name\":\"DAMAGES\",\n         \"instruction_details\":\"- Address ALL damages with a BID TO REPAIR including ALL APPLICABLE DETAILS such as:\\n*Size of area; Materials being used (HUD expects BASIC GRADE materials for all\\n repairs); Size and quantity of materials being used (i.e. 3 wood steps 4' wide);\\n Man hours, etc.\\n*IF UNABLE TO PROVIDE A BID TO REPAIR: ADVISE AND PROVIDE AN EYEBALL ESTIMATE and\\n provide photos to support\\n\\n*It is important that all conditions and damages are documented and photographed on\\n this initial order and all subsequent visits to avoid the risk of neglect and\\n potential liability\\n\\n* Advise on the overall condition of the property.\\n  - Be sure to note any broken hand-rails, broken/loose steps, exposed\\n    electrical wiring/panel, loose/detached gutters, mold, glass shards.\"\n      },\n      {\n         \"instruction_name\":\"PHOTO REQUIREMENTS\",\n         \"instruction_details\":\"*PLEASE PROVIDE CLEAR, WELL LIT (USE FLASH WHEN NEEDED), DATE & TIME STAMPED PHOTOS\\n\\n*Property condition must be documented at every trip to the property, including but  not\\nlimited to:\\n- Quality photos including lighting, angle, clarity & scope.\\n- Street sign, house number, and front of property.\\n- All exterior sides of structures and all exterior grounds as well as improvements\\n  (pool, fence, swing set, shed, etc.) to document conditions & that vegetation is\\n  not touching.\\n- Secured entries with keys in lockbox (if applicable) & the key from lockbox (if\\n  applicable) in lock\\n- All interior rooms including closets, cabinets, drawers, attic, crawlspace,\\n  basement, garage, shed outbuilding, etc.; with walls, ceilings & floors of each.\\n- Photos to document status of winterization and utilities\\n- All existing appliances/mechanicals or location where appliances/mechanicals are\\n  missing (includes but not limited to HVAC/AC/Furnace/Boiler/Swamp Cooler, Water\\n  Heater, Sump Pump, Dehumidifier, Electrical Panel, Garage Door opener,\\n  Refrigerator, Freezer, Dishwasher, Washer/Dryer, Microwave, Stove, Oven, etc.)\\n-Damages including structure, roof, missing/damaged plumbing or electrical, mold,\\n  water damage, missing/damaged gutters/downspouts, broken or cracked windowpanes,etc.\\n- Legible photo of any & all notices/postings\\n\\n*Completion/estimate/damage photos:\\n-Before, during & after, taken from the same angle of all work completed at time\\n   of work\\n\\n*LOAD PHOTO REQUIREMENTS*\\n*Before photos must show:\\n-Specific amount & location(s) of debris being removed;\\n-Truck bed, trailer, dumpster, etc.(\\\"vehicle\\\") dimensions;\\n-Vehicle before loading debris;\\n-Vehicle at subject property;\\n-Load number if multiple loads.\\n-During photos must show items shown in before photos while loading.\\n*After photos must clearly:\\n-Match before photo location(s);\\n-Document amount of debris removed;\\n-Vehicle with items shown in before photos;\\n-Vehicle with completed load at subject property;\\n-Load number if multiple loads.\\n-To justify any estimates provided\\n-To document damages reported\\n\\n*ALL photos must document the following to ensure prompt processing of your invoice:\\n-Location of performed work\\n-Enough surrounding area to clearly document, identify & support similar items\\n-ALL measurements (tape measure photos are required) to support the scope of work,\\n  materials, area, and any item that references a dimension\\n-ALL materials that are removed or installed\\n-ALL steps or actions performed\\n\\n*Whiteboard use is suggested to alleviate confusion\\n\\n*Failure to satisfy the above photo requirements & expectations may result in\\n adjustments to your invoice\\n   THANKS,  Pamela Ext 2300\"\n      }\n   ]\n}\n";



                WorkOrder_NFR_Item_JsonDTO orders = JsonConvert.DeserializeObject<WorkOrder_NFR_Item_JsonDTO>(result);
                if (orders != null)
                {
                    WorkOrder_NFR_MasterDTO workOrder_NFR_MasterDTO = new WorkOrder_NFR_MasterDTO();
                    workOrder_NFR_MasterDTO.WNFR_Order = orders.work_order;
                    workOrder_NFR_MasterDTO.WNFR_To = orders.to;
                    workOrder_NFR_MasterDTO.WNFR_Account = orders.account;
                    workOrder_NFR_MasterDTO.WNFR_Date_Rec_Contact = orders.date_rep_contact;
                    workOrder_NFR_MasterDTO.WNFR_Due_Date = orders.date_due;
                    workOrder_NFR_MasterDTO.WNFR_MTG_Co = orders.mtg_co;
                    workOrder_NFR_MasterDTO.WNFR_LoanType = orders.loan_type;
                    workOrder_NFR_MasterDTO.WNFR_MTGR_Name = orders.mtgr_name;
                    workOrder_NFR_MasterDTO.WNFR_MTGR_Address = orders.mtgr_address;
                    workOrder_NFR_MasterDTO.WNFR_Requierment_Details = orders.requirement_details;
                    workOrder_NFR_MasterDTO.WNFR_gpsLatitude = "";
                    workOrder_NFR_MasterDTO.WNFR_gpsLongitude = "";
                    workOrder_NFR_MasterDTO.WNFR_Import_File_Name = "";
                    workOrder_NFR_MasterDTO.WNFR_Import_FilePath = "";
                    workOrder_NFR_MasterDTO.WNFR_Import_Pdf_Name = "";
                    workOrder_NFR_MasterDTO.WNFR_Import_Pdf_Path = "";
                    workOrder_NFR_MasterDTO.WNFR_City= orders.city;
                    workOrder_NFR_MasterDTO.WNFR_State = orders.state;
                    workOrder_NFR_MasterDTO.WNFR_Zip = orders.zip;

                    workOrder_NFR_MasterDTO.WNFR_wo_id = orders.wo_id;
                    workOrder_NFR_MasterDTO.WNFR_username = orders.username;
                    workOrder_NFR_MasterDTO.WNFR_keycode = orders.keycode;
                    workOrder_NFR_MasterDTO.WNFR_lockbox = orders.lockbox;
                    workOrder_NFR_MasterDTO.WNFR_address = orders.address;

                    workOrder_NFR_MasterDTO.WNFR_NFR_ID = orders.nfr_id;

                    workOrder_NFR_MasterDTO.WNFR_ImportMaster_Pkey = model.WI_Pkey_ID;
                    workOrder_NFR_MasterDTO.WNFR_IsActive = true;
                    workOrder_NFR_MasterDTO.WNFR_IsDelete = false;
                    workOrder_NFR_MasterDTO.WNFR_IsProcessed = false;
                    workOrder_NFR_MasterDTO.Type = 1;
                    var objData = AddWorkOrder_NFRData(workOrder_NFR_MasterDTO);
                    if (objData[0].WNFR_PkeyID != "0")
                    {
                        if (orders.instructions != null && orders.instructions != null && orders.instructions.Count > 0)
                        {
                            foreach (var ins in orders.instructions)
                            {
                                WorkOrder_NFR_Item_MasterDTO workOrder_NFR_Item_MasterDTO = new WorkOrder_NFR_Item_MasterDTO();
                                workOrder_NFR_Item_MasterDTO.WNFRINS_Ins_Name = ins.instruction_name;
                                workOrder_NFR_Item_MasterDTO.WNFRINS_Ins_Details = ins.instruction_details;
                                workOrder_NFR_Item_MasterDTO.WNFRINS_Qty = 0;
                                workOrder_NFR_Item_MasterDTO.WNFRINS_Price = 0;
                                workOrder_NFR_Item_MasterDTO.WNFRINS_Total = 0;
                                workOrder_NFR_Item_MasterDTO.WNFRINS_FkeyID = Convert.ToInt64(objData[0].WNFR_PkeyID);
                                workOrder_NFR_Item_MasterDTO.UserID = model.UserID;
                                workOrder_NFR_Item_MasterDTO.Type = 1;
                                var ObjItemData = AddWorkOrder_NFRItemData(workOrder_NFR_Item_MasterDTO);
                            }
                        }
                    }
                    objDataModel.Add(objData);
                }
                else
                {
                    log.logDebugMessage("<-------------------No NFR Open Work orders Found----------------->");
                }

                // }
                //string result1 = messge.Content.ReadAsStringAsync().Result;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objDataModel;
        }
    }
}