﻿using Avigma.Repository.Lib;
using IPLApp.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace AdminDemo.Repositotry.Data
{
    public class Forms_Master_FolderData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        public List<dynamic> AddForms_Master_Folder(Forms_Master_FolderDTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_Forms_Master_Folder]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@FMF_Pkey", 1 + "#bigint#" + model.FMF_Pkey);
                input_parameters.Add("@FMF_Name", 1 + "#nvarchar#" + model.FMF_Name);
                input_parameters.Add("@FMF_CustPkey", 1 + "#bigint#" + model.FMF_CustPkey);
                input_parameters.Add("@FMF_ClientPkey", 1 + "#bigint#" + model.FMF_ClientPkey);
                input_parameters.Add("@FMF_IsActive", 1 + "#bit#" + model.FMF_IsActive);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserId);
                input_parameters.Add("@FMF_Pkey_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;



        }

        private DataSet Get_Forms_Master_Folder(Forms_Master_FolderDTO model)
        {
            DataSet ds = null;
            try
            {

                string selectProcedure = "[Get_Forms_Master_Folder]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@FMF_Pkey", 1 + "#bigint#" + model.FMF_Pkey);

                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> Get_Forms_Master_FolderDetails(Forms_Master_FolderDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = Get_Forms_Master_Folder(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Forms_Master_FolderDTO> Forms_Master_Folder =
                   (from item in myEnumerableFeaprd
                    select new Forms_Master_FolderDTO
                    {
                        FMF_Pkey = item.Field<Int64>("FMF_Pkey"),
                        FMF_Name = item.Field<String>("FMF_Name"),
                        FMF_CustPkey = item.Field<Int64?>("FMF_CustPkey"),
                        FMF_ClientPkey = item.Field<Int64?>("FMF_ClientPkey"),
                       



                    }).ToList();

                objDynamic.Add(Forms_Master_Folder);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}