﻿using Avigma.Repository.Lib;
using IPL.Models;
using IPL.Models.FiveBrothers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class WorkOrder_Cyprexx_History_MasterData
    {

        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        public List<dynamic> AddWorkOrder_Cyprexx_HistoryData(WorkOrder_Cyprexx_History_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_WorkOrder_Cyprexx_History_Master]";
            WorkOrder_Cyprexx_History_Master WorkOrder_Cyprexx_History_Master = new WorkOrder_Cyprexx_History_Master();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@WCyprexx_HS_PkeyID", 1 + "#bigint#" + model.WCyprexx_HS_PkeyID);
                input_parameters.Add("@WCyprexx_HS_wo_id", 1 + "#nvarchar#" + model.WCyprexx_HS_wo_id);

                input_parameters.Add("@WCyprexx_HS_address", 1 + "#nvarchar#" + model.WCyprexx_HS_address);
                input_parameters.Add("@WCyprexx_HS_City", 1 + "#nvarchar#" + model.WCyprexx_HS_City);
                input_parameters.Add("@WCyprexx_HS_State", 1 + "#nvarchar#" + model.WCyprexx_HS_State);
                input_parameters.Add("@WCyprexx_HS_Zip", 1 + "#nvarchar#" + model.WCyprexx_HS_Zip);
                input_parameters.Add("@WCyprexx_HS_Customer", 1 + "#nvarchar#" + model.WCyprexx_HS_Customer);
                input_parameters.Add("@WCyprexx_HS_Loan_Type", 1 + "#nvarchar#" + model.WCyprexx_HS_Loan_Type);
                input_parameters.Add("@WCyprexx_HS_Lock_Code", 1 + "#nvarchar#" + model.WCyprexx_HS_Lock_Code);
                input_parameters.Add("@WCyprexx_HS_Key_Code", 1 + "#nvarchar#" + model.WCyprexx_HS_Key_Code);
                input_parameters.Add("@WCyprexx_HS_Received_Date", 1 + "#datetime#" + model.WCyprexx_HS_Received_Date);
                input_parameters.Add("@WCyprexx_HS_Due_Date", 1 + "#datetime#" + model.WCyprexx_HS_Due_Date);

                input_parameters.Add("@WCyprexx_HS_Comments", 1 + "#varchar#" + model.WCyprexx_HS_Comments);
                input_parameters.Add("@WCyprexx_HS_gpsLatitude", 1 + "#varchar#" + model.WCyprexx_HS_gpsLatitude);
                input_parameters.Add("@WCyprexx_HS_gpsLongitude", 1 + "#varchar#" + model.WCyprexx_HS_gpsLongitude);
                input_parameters.Add("@WCyprexx_HS_ImportMaster_Pkey", 1 + "#varchar#" + model.WCyprexx_HS_ImportMaster_Pkey);
                input_parameters.Add("@WCyprexx_HS_Import_File_Name", 1 + "#varchar#" + model.WCyprexx_HS_Import_File_Name);
                input_parameters.Add("@WCyprexx_HS_Import_FilePath", 1 + "#varchar#" + model.WCyprexx_HS_Import_FilePath);
                input_parameters.Add("@WCyprexx_HS_Import_Pdf_Name", 1 + "#varchar#" + model.WCyprexx_HS_Import_Pdf_Name);
                input_parameters.Add("@WCyprexx_HS_Import_Pdf_Path", 1 + "#varchar#" + model.WCyprexx_HS_Import_Pdf_Path);





                input_parameters.Add("@WCyprexx_HS_Cyprexx_ID", 1 + "#nvarchar#" + model.WCyprexx_HS_Cyprexx_ID);

                input_parameters.Add("@WCyprexx_HS_IsProcessed", 1 + "#bit#" + model.WCyprexx_HS_IsProcessed);
                input_parameters.Add("@WCyprexx_HS_IsActive", 1 + "#bit#" + model.WCyprexx_HS_IsActive);
                input_parameters.Add("@WCyprexx_HS_IsDelete", 1 + "#bit#" + model.WCyprexx_HS_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@WCyprexx_HS_Pkey_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    WorkOrder_Cyprexx_History_Master.WCyprexx_HS_PkeyID = "0";
                    WorkOrder_Cyprexx_History_Master.Status = "0";
                    WorkOrder_Cyprexx_History_Master.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    WorkOrder_Cyprexx_History_Master.WCyprexx_HS_PkeyID = Convert.ToString(objData[0]);
                    WorkOrder_Cyprexx_History_Master.Status = Convert.ToString(objData[1]);


                }
                objcltData.Add(WorkOrder_Cyprexx_History_Master);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;

        }


        public List<dynamic> AddWorkOrder_Cyprexx_Item_HistoryData(WorkOrder_Cyprexx_Item_History_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_WorkOrder_Cyprexx_Item_History_Master]";
            WorkOrder_Cyprexx_Item_History_Master WorkOrder_Cyprexx_Item_History_Master = new WorkOrder_Cyprexx_Item_History_Master();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@WCYPREXXINS_HS_PkeyId", 1 + "#bigint#" + model.WCYPREXXINS_HS_PkeyId);
                input_parameters.Add("@WCYPREXXINS_HS_Ins_Name", 1 + "#nvarchar#" + model.WCYPREXXINS_HS_Ins_Name);
                input_parameters.Add("@WCYPREXXINS_HS_Ins_Details", 1 + "#nvarchar#" + model.WCYPREXXINS_HS_Ins_Details);
                input_parameters.Add("@WCYPREXXINS_HS_Qty", 1 + "#bigint#" + model.WCYPREXXINS_HS_Qty);
                input_parameters.Add("@WCYPREXXINS_HS_Price", 1 + "#decimal#" + model.WCYPREXXINS_HS_Price);
                input_parameters.Add("@WCYPREXXINS_HS_Total", 1 + "#decimal#" + model.WCYPREXXINS_HS_Total);
                input_parameters.Add("@WCYPREXXINS_HS_FkeyID", 1 + "#bigint#" + model.WCYPREXXINS_HS_FkeyID);
                input_parameters.Add("@WCYPREXXINS_HS_IsProcessed", 1 + "#boolean#" + model.WCYPREXXINS_HS_IsProcessed);
                input_parameters.Add("@WCYPREXXINS_HS_IsActive", 1 + "#boolean#" + model.WCYPREXXINS_HS_IsActive);
                input_parameters.Add("@WCYPREXXINS_HS_IsDelete", 1 + "#boolean#" + model.WCYPREXXINS_HS_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@WCYPREXXINS_HS_Pkey_Outt", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    WorkOrder_Cyprexx_Item_History_Master.WCYPREXXINS_HS_PkeyId = "0";
                    WorkOrder_Cyprexx_Item_History_Master.Status = "0";
                    WorkOrder_Cyprexx_Item_History_Master.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    WorkOrder_Cyprexx_Item_History_Master.WCYPREXXINS_HS_PkeyId = Convert.ToString(objData[0]);
                    WorkOrder_Cyprexx_Item_History_Master.Status = Convert.ToString(objData[1]);

                }
                objcltData.Add(WorkOrder_Cyprexx_Item_History_Master);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;



        }
    }
}