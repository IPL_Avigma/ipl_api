﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class ContractorReportPendingData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        Contractor_Paid_InvoiceData contractor_Paid_InvoiceData = new Contractor_Paid_InvoiceData();
        Contractor_ScoreCard_SettingData contractor_ScoreCard_SettingData = new Contractor_ScoreCard_SettingData();
        private DataSet GetContractorInvoicePendingMaster(Contractor_Paid_InvoiceDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Pending_Hold_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();
                input_parameters.Add("@whereClause", 1 + "#nvarchar#" + model.whereClause);
                input_parameters.Add("@Inv_Con_Inv_Followup", 1 + "#bit#" + model.Inv_Con_Inv_Followup);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }
            return ds;
        }

        private List<Contractor_Paid_InvoiceDTO> GetContratorPendingDetails(Contractor_Paid_InvoiceDTO model)
        {
            List<Contractor_Paid_InvoiceDTO> contractorinvoicepaid = new List<Contractor_Paid_InvoiceDTO>();
            string wherecondition = string.Empty;
            string contractor = string.Empty;

            try
            {
                Contractor_Paid_InvoiceDTO contractor_Paid_InvoiceDTO = new Contractor_Paid_InvoiceDTO();
                contractor_Paid_InvoiceDTO.Inv_Con_Inv_Followup = model.Inv_Con_Inv_Followup;
                contractor_Paid_InvoiceDTO.Type = 1;
                var followup = model.Inv_Con_Inv_Followup == true ? 1 : 0;
                if (model.From_InvoiceDate != null && model.To_InvoiceDate != null)
                {
                    wherecondition = " And CAST(paid.Inv_Con_Inv_Date as date) >=   CONVERT(date,'" + model.From_InvoiceDate.Value.ToString("yyyy-MM-dd") + "')  And CAST(paid.Inv_Con_Inv_Date as date) <=   CONVERT(date,'" + model.To_InvoiceDate.Value.ToString("yyyy-MM-dd") + "')";
                }
                if (model != null && model.PendingTabCon != null && model.PendingTabCon.Count > 0)
                {
                    for (int i = 0; i < model.PendingTabCon.Count; i++)
                    {
                        contractor = string.IsNullOrEmpty(contractor) ? model.PendingTabCon[i].User_pkeyID.ToString() : contractor + "," + model.PendingTabCon[i].User_pkeyID.ToString();
                    }
                }
                if (!string.IsNullOrEmpty(contractor))
                {
                    wherecondition = !string.IsNullOrEmpty(wherecondition)? wherecondition + " And wo.Contractor IN(" + contractor + ")" : " And wo.Contractor IN(" + contractor + ")";
                }
                wherecondition = !string.IsNullOrEmpty(wherecondition) ? wherecondition + " And paid.[Inv_Con_Inv_Followup] = " + followup : " And paid.[Inv_Con_Inv_Followup] = " + followup;

                contractor_Paid_InvoiceDTO.whereClause = wherecondition;
                contractor_Paid_InvoiceDTO.UserID = model.UserID;
                DataSet ds = GetContractorInvoicePendingMaster(contractor_Paid_InvoiceDTO);

                var myEnumerableFeaprdc = ds.Tables[0].AsEnumerable();
                contractorinvoicepaid =
                   (from item in myEnumerableFeaprdc
                    select new Contractor_Paid_InvoiceDTO
                    {
                        Inv_Con_pkeyId = item.Field<Int64>("Inv_Con_pkeyId"),
                        Inv_Con_Invoice_Id = item.Field<Int64?>("Inv_Con_Invoice_Id"),
                        Inv_Con_TaskId = item.Field<Int64?>("Inv_Con_TaskId"),
                        Inv_Con_Wo_ID = item.Field<Int64?>("Inv_Con_Wo_ID"),
                        Inv_Con_Sub_Total = item.Field<Decimal?>("Inv_Con_Sub_Total"),
                        Inv_Con_ContTotal = item.Field<Decimal?>("Inv_Con_ContTotal"),
                        Con_Pay_Amount = item.Field<Decimal?>("Con_Pay_Amount"),
                        Inv_Con_Invoce_Num = item.Field<String>("Inv_Con_Invoce_Num"),
                        Inv_Con_Inv_Date = item.Field<DateTime?>("Inv_Con_Inv_Date"),
                        Inv_Con_Status = item.Field<int?>("Inv_Con_Status"),
                        dueDate = item.Field<DateTime?>("dueDate"),
                        IPLNO = item.Field<String>("IPLNO"),
                        workOrderNumber = item.Field<String>("workOrderNumber"),
                        address1 = item.Field<String>("address1"),
                        city = item.Field<String>("city"),
                        SM_Name = item.Field<String>("SM_Name"),
                        zip = item.Field<Int64?>("zip"),
                        ContractorName = item.Field<String>("ContractorName"),
                        WT_WorkType = item.Field<String>("WT_WorkType"),
                        Client_Company_Name = item.Field<String>("Client_Company_Name"),
                        assigned_admin = item.Field<Int64?>("assigned_admin"),
                        Inv_Con_Inv_Comment =  item.Field<string>("Inv_Con_Inv_Comment"),
                        Contractor_Paid_Invoice_ChildDTO = contractor_Paid_InvoiceData.GetContractorInvoice_Paid_Child_Details(item.Field<Int64>("Inv_Con_pkeyId"))

                    }).ToList();


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);

            }

            return contractorinvoicepaid;
        }

        public List<dynamic> GetContractorInvoice_Details(Contractor_Paid_InvoiceDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                objDynamic.Add(GetContratorPendingDetails(model));
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);

            }
            return objDynamic;
        }
        public List<dynamic> GetPendingReportForMobileUser(Contractor_Paid_InvoiceDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                ContractorPendingInvoiceDTO contractorPendingInvoiceDTO = new ContractorPendingInvoiceDTO();
                List<Contractorarr> contractorarrs = new List<Contractorarr>();
                Contractorarr contractorarr = new Contractorarr();
                contractorarr.User_pkeyID = model.UserID;
                contractorarrs.Add(contractorarr);
                model.PendingTabCon = contractorarrs;
                model.From_InvoiceDate = System.DateTime.Now.AddMonths(-1);
                model.To_InvoiceDate = System.DateTime.Now;
                model.Inv_Con_Inv_Followup = true;

                contractorPendingInvoiceDTO.CurrentlyDue = GetCurrentlyDueContractorReportData(model);
                contractorPendingInvoiceDTO.OnHold = GetContratorPendingDetails(model);
                model.Inv_Con_Inv_Followup = false;
                contractorPendingInvoiceDTO.Completed =  GetContratorPendingDetails(model);
                objDynamic.Add(contractorPendingInvoiceDTO);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);

            }
            return objDynamic;
        }

        public List<dynamic> GetPendingReport(Contractor_Paid_InvoiceDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                ContractorPendingInvoiceDTO contractorPendingInvoiceDTO = new ContractorPendingInvoiceDTO();
                model.Inv_Con_Inv_Followup = true;
                contractorPendingInvoiceDTO.CurrentlyDue = GetCurrentlyDueContractorReportData(model);
                contractorPendingInvoiceDTO.OnHold = GetContratorPendingDetails(model);
                model.Inv_Con_Inv_Followup = false;
                contractorPendingInvoiceDTO.Completed = GetContratorPendingDetails(model);
                objDynamic.Add(contractorPendingInvoiceDTO);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);

            }
            return objDynamic;
        }

        public List<Contractor_Paid_InvoiceDTO> GetCurrentlyDueContractorReportData(Contractor_Paid_InvoiceDTO model)
        {
            List<Contractor_Paid_InvoiceDTO> contractorinvoicepaid = new List<Contractor_Paid_InvoiceDTO>();
            string wherecondition = string.Empty;
            string contractor = string.Empty;

            try
            {
                Contractor_Paid_InvoiceDTO contractor_Paid_InvoiceDTO = new Contractor_Paid_InvoiceDTO();

                if (model.From_InvoiceDate != null && model.To_InvoiceDate != null)
                {
                    int days = 1;
                    ContractorAccountPayDTO contractorAccountPayDTO = new ContractorAccountPayDTO();
                    contractorAccountPayDTO.Type = 2;
                    contractorAccountPayDTO.UserID = model.UserID;
                    contractorAccountPayDTO.Con_Account_Pay_PkeyID = 0;
                    var con_payable_setting = contractor_ScoreCard_SettingData.GetContractorAccountPaySettingDetails(contractorAccountPayDTO);
                    if (con_payable_setting != null && con_payable_setting.Count > 0)
                    {
                        if (con_payable_setting[0] != null && con_payable_setting[0].Count > 0)
                        {
                            days = con_payable_setting[0][0].Payout_Frequency;

                            if (con_payable_setting[0][0].Inv_Payout_Criteria == 1) // Field Complete Date
                            {
                                wherecondition = " And DATEADD(day," + days + ",CAST(work.Field_complete_date as date)) >= CONVERT(date,'" + model.From_InvoiceDate.Value.ToString("yyyy-MM-dd") + "')  And DATEADD(day," + days + ",CAST(work.Field_complete_date as date)) <= CONVERT(date,'" + model.To_InvoiceDate.Value.ToString("yyyy-MM-dd") + "')";
                            }
                            else if (con_payable_setting[0][0].Inv_Payout_Criteria == 2) // Sent To Client
                            {
                                wherecondition = " And DATEADD(day," + days + ",CAST(work.SentToClient_date as date)) >= CONVERT(date,'" + model.From_InvoiceDate.Value.ToString("yyyy-MM-dd") + "')  And DATEADD(day," + days + ",CAST(work.SentToClient_date as date)) <= CONVERT(date,'" + model.To_InvoiceDate.Value.ToString("yyyy-MM-dd") + "')";
                            }
                            else if (con_payable_setting[0][0].Inv_Payout_Criteria == 3) // Complete Date
                            {
                                wherecondition = " And DATEADD(day," + days + ",CAST(work.Complete_Date as date)) >= CONVERT(date,'" + model.From_InvoiceDate.Value.ToString("yyyy-MM-dd") + "')  And DATEADD(day," + days + ",CAST(work.Complete_Date as date)) <= CONVERT(date,'" + model.To_InvoiceDate.Value.ToString("yyyy-MM-dd") + "')";
                            }
                            else if (con_payable_setting[0][0].Inv_Payout_Criteria == 4) // Invoice Date
                            {
                                wherecondition = " And DATEADD(day," + days + ",CAST(inv.Inv_Con_CreatedOn as date)) >= CONVERT(date,'" + model.From_InvoiceDate.Value.ToString("yyyy-MM-dd") + "')  And DATEADD(day," + days + ",CAST(inv.Inv_Con_CreatedOn as date)) <= CONVERT(date,'" + model.To_InvoiceDate.Value.ToString("yyyy-MM-dd") + "')";
                            }
                            else if (con_payable_setting[0][0].Inv_Payout_Criteria == 5) // Invoice Approved
                            {
                                wherecondition = " And DATEADD(day," + days + ",CAST(inv.Inv_Con_Inv_Approve_Date as date)) >= CONVERT(date,'" + model.From_InvoiceDate.Value.ToString("yyyy-MM-dd") + "')  And DATEADD(day," + days + ",CAST(inv.Inv_Con_Inv_Approve_Date as date)) <= CONVERT(date,'" + model.To_InvoiceDate.Value.ToString("yyyy-MM-dd") + "')";
                            }
                        }
                    }
                }
                if (model != null && model.PendingTabCon != null && model.PendingTabCon.Count > 0)
                {
                    for (int i = 0; i < model.PendingTabCon.Count; i++)
                    {
                        contractor = string.IsNullOrEmpty(contractor) ? model.PendingTabCon[i].User_pkeyID.ToString() : contractor + "," + model.PendingTabCon[i].User_pkeyID.ToString();
                    }
                }
                if (!string.IsNullOrEmpty(contractor))
                {
                    wherecondition = wherecondition != null ? wherecondition + " And work.Contractor IN(" + contractor + ")" : " And work.Contractor IN(" + contractor + ")";
                }

                contractor_Paid_InvoiceDTO.Type = 1;
                contractor_Paid_InvoiceDTO.whereClause = wherecondition;
                contractor_Paid_InvoiceDTO.UserID = model.UserID;
                DataSet ds = GetCurrentlyDueContractorInvoiceList(contractor_Paid_InvoiceDTO);

                var myEnumerableFeaprdc = ds.Tables[0].AsEnumerable();
                contractorinvoicepaid =
                   (from item in myEnumerableFeaprdc
                    select new Contractor_Paid_InvoiceDTO
                    {
                        Inv_Con_pkeyId = item.Field<Int64>("Inv_Con_pkeyId"),
                        Inv_Con_Invoice_Id = item.Field<Int64?>("Inv_Con_Invoice_Id"),
                        Inv_Con_TaskId = item.Field<Int64?>("Inv_Con_TaskId"),
                        Inv_Con_Wo_ID = item.Field<Int64?>("Inv_Con_Wo_ID"),
                        Inv_Con_Sub_Total = item.Field<Decimal?>("Inv_Con_Sub_Total"),
                        Inv_Con_ContTotal = item.Field<Decimal?>("Inv_Con_ContTotal"),
                        Con_Pay_Amount = item.Field<Decimal?>("Con_Pay_Amount"),
                        Inv_Con_Invoce_Num = item.Field<String>("Inv_Con_Invoce_Num"),
                        Inv_Con_Inv_Date = item.Field<DateTime?>("Inv_Con_Inv_Date"),
                        Inv_Con_Status = item.Field<int?>("Inv_Con_Status"),
                        dueDate = item.Field<DateTime?>("dueDate"),
                        IPLNO = item.Field<String>("IPLNO"),
                        workOrderNumber = item.Field<String>("workOrderNumber"),
                        address1 = item.Field<String>("address1"),
                        city = item.Field<String>("city"),
                        SM_Name = item.Field<String>("SM_Name"),
                        zip = item.Field<Int64?>("zip"),
                        ContractorName = item.Field<String>("ContractorName"),
                        WT_WorkType = item.Field<String>("WT_WorkType"),
                        Client_Company_Name = item.Field<String>("Client_Company_Name"),
                        assigned_admin = item.Field<Int64?>("assigned_admin"),
                        Contractor_Paid_Invoice_ChildDTO = contractor_Paid_InvoiceData.GetContractorInvoice_Paid_Child_Details(item.Field<Int64>("Inv_Con_pkeyId"))

                    }).ToList();
               

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);

            }
            return contractorinvoicepaid;


        }
        public List<dynamic> GetCurrentlyDueContractorReport(Contractor_Paid_InvoiceDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            string wherecondition = string.Empty;
            string contractor = string.Empty;

            try
            {
                Contractor_Paid_InvoiceDTO contractor_Paid_InvoiceDTO = new Contractor_Paid_InvoiceDTO();
                
                if (model.From_InvoiceDate != null && model.To_InvoiceDate != null)
                {
                    int days = 1;
                   ContractorAccountPayDTO contractorAccountPayDTO = new ContractorAccountPayDTO();
                    contractorAccountPayDTO.Type = 2;
                    contractorAccountPayDTO.UserID = model.UserID;
                    contractorAccountPayDTO.Con_Account_Pay_PkeyID = 0;
                    var con_payable_setting = contractor_ScoreCard_SettingData.GetContractorAccountPaySettingDetails(contractorAccountPayDTO);
                    if (con_payable_setting != null && con_payable_setting.Count > 0)
                    {
                        if (con_payable_setting[0] != null && con_payable_setting[0].Count > 0 )
                        {
                            days = con_payable_setting[0][0].Payout_Frequency;

                            if (con_payable_setting[0][0].Inv_Payout_Criteria == 1) // Field Complete Date
                            {
                                wherecondition = " And DATEADD(day," + days + ",CAST(work.Field_complete_date as date)) >= CONVERT(date,'" + model.From_InvoiceDate.Value.ToString("yyyy-MM-dd") + "')  And DATEADD(day," + days + ",CAST(work.Field_complete_date as date)) <= CONVERT(date,'" + model.To_InvoiceDate.Value.ToString("yyyy-MM-dd") + "')";
                            }
                            else if (con_payable_setting[0][0].Inv_Payout_Criteria == 2) // Sent To Client
                            {
                                wherecondition = " And DATEADD(day," + days + ",CAST(work.SentToClient_date as date)) >= CONVERT(date,'" + model.From_InvoiceDate.Value.ToString("yyyy-MM-dd") + "')  And DATEADD(day," + days + ",CAST(work.SentToClient_date as date)) <= CONVERT(date,'" + model.To_InvoiceDate.Value.ToString("yyyy-MM-dd") + "')";
                            }
                            else if (con_payable_setting[0][0].Inv_Payout_Criteria == 3) // Complete Date
                            {
                                wherecondition = " And DATEADD(day," + days + ",CAST(work.Complete_Date as date)) >= CONVERT(date,'" + model.From_InvoiceDate.Value.ToString("yyyy-MM-dd") + "')  And DATEADD(day," + days + ",CAST(work.Complete_Date as date)) <= CONVERT(date,'" + model.To_InvoiceDate.Value.ToString("yyyy-MM-dd") + "')";
                            }
                            else if (con_payable_setting[0][0].Inv_Payout_Criteria == 4) // Invoice Date
                            {
                                wherecondition = " And DATEADD(day," + days + ",CAST(inv.Inv_Con_CreatedOn as date)) >= CONVERT(date,'" + model.From_InvoiceDate.Value.ToString("yyyy-MM-dd") + "')  And DATEADD(day," + days + ",CAST(inv.Inv_Con_CreatedOn as date)) <= CONVERT(date,'" + model.To_InvoiceDate.Value.ToString("yyyy-MM-dd") + "')";
                            }
                            else if (con_payable_setting[0][0].Inv_Payout_Criteria == 5) // Invoice Approved
                            {
                                wherecondition = " And DATEADD(day," + days + ",CAST(inv.Inv_Con_Inv_Approve_Date as date)) >= CONVERT(date,'" + model.From_InvoiceDate.Value.ToString("yyyy-MM-dd") + "')  And DATEADD(day," + days + ",CAST(inv.Inv_Con_Inv_Approve_Date as date)) <= CONVERT(date,'" + model.To_InvoiceDate.Value.ToString("yyyy-MM-dd") + "')";
                            }
                        } 
                    }  
                }
                if (model != null && model.PendingTabCon != null && model.PendingTabCon.Count > 0)
                {
                    for (int i = 0; i < model.PendingTabCon.Count; i++)
                    {
                        contractor = string.IsNullOrEmpty(contractor) ? model.PendingTabCon[i].User_pkeyID.ToString() : contractor + "," + model.PendingTabCon[i].User_pkeyID.ToString();
                    }
                }
                if (!string.IsNullOrEmpty(contractor))
                {
                    wherecondition = wherecondition != null ? wherecondition + " And work.Contractor IN(" + contractor + ")" : " And work.Contractor IN(" + contractor + ")";
                }

                contractor_Paid_InvoiceDTO.Type = 1;
                contractor_Paid_InvoiceDTO.whereClause = wherecondition;
                contractor_Paid_InvoiceDTO.UserID = model.UserID;
                DataSet ds = GetCurrentlyDueContractorInvoiceList(contractor_Paid_InvoiceDTO);

                var myEnumerableFeaprdc = ds.Tables[0].AsEnumerable();
                var contractorinvoicepaid =
                   (from item in myEnumerableFeaprdc
                    select new Contractor_Paid_InvoiceDTO
                    {
                        Inv_Con_pkeyId = item.Field<Int64>("Inv_Con_pkeyId"),
                        Inv_Con_Invoice_Id = item.Field<Int64?>("Inv_Con_Invoice_Id"),
                        Inv_Con_TaskId = item.Field<Int64?>("Inv_Con_TaskId"),
                        Inv_Con_Wo_ID = item.Field<Int64?>("Inv_Con_Wo_ID"),
                        Inv_Con_Sub_Total = item.Field<Decimal?>("Inv_Con_Sub_Total"),
                        Inv_Con_ContTotal = item.Field<Decimal?>("Inv_Con_ContTotal"),
                        Con_Pay_Amount = item.Field<Decimal?>("Con_Pay_Amount"),
                        Inv_Con_Invoce_Num = item.Field<String>("Inv_Con_Invoce_Num"),
                        Inv_Con_Inv_Date = item.Field<DateTime?>("Inv_Con_Inv_Date"),
                        Inv_Con_Status = item.Field<int?>("Inv_Con_Status"),
                        dueDate = item.Field<DateTime?>("dueDate"),
                        IPLNO = item.Field<String>("IPLNO"),
                        workOrderNumber = item.Field<String>("workOrderNumber"),
                        address1 = item.Field<String>("address1"),
                        city = item.Field<String>("city"),
                        SM_Name = item.Field<String>("SM_Name"),
                        zip = item.Field<Int64?>("zip"),
                        ContractorName = item.Field<String>("ContractorName"),
                        WT_WorkType = item.Field<String>("WT_WorkType"),
                        Client_Company_Name = item.Field<String>("Client_Company_Name"),
                        assigned_admin = item.Field<Int64?>("assigned_admin"),
                        Contractor_Paid_Invoice_ChildDTO = contractor_Paid_InvoiceData.GetContractorInvoice_Paid_Child_Details(item.Field<Int64>("Inv_Con_pkeyId"))

                    }).ToList();
                objDynamic.Add(contractorinvoicepaid);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);

            }
            return objDynamic;
        }

        private DataSet GetCurrentlyDueContractorInvoiceList(Contractor_Paid_InvoiceDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_CurrentlyDue_ContractorReport]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@whereClause", 1 + "#nvarchar#" + model.whereClause);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }
            return ds;
        }

    }
}