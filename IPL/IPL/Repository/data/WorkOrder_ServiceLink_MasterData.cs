﻿using Avigma.Repository.Lib;
using IPL.Models;
using IPL.Models.FiveBrothers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class WorkOrder_ServiceLink_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        private List<dynamic> AddWorkOrder_ServiceLinkData(WorkOrder_ServiceLink_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_WorkOrder_ServiceLink_Master]";
            WorkOrder_ServiceLink_Master WorkOrder_ServiceLink_Master = new WorkOrder_ServiceLink_Master();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@WServiceLink_PkeyID", 1 + "#bigint#" + model.WServiceLink_PkeyID);
                input_parameters.Add("@WServiceLink_wo_id", 1 + "#nvarchar#" + model.WServiceLink_wo_id);

                input_parameters.Add("@WServiceLink_Address", 1 + "#nvarchar#" + model.WServiceLink_Address);
                input_parameters.Add("@WServiceLink_City", 1 + "#nvarchar#" + model.WServiceLink_City);
                input_parameters.Add("@WServiceLink_State", 1 + "#nvarchar#" + model.WServiceLink_State);
                input_parameters.Add("@WServiceLink_ZipCode", 1 + "#nvarchar#" + model.WServiceLink_ZipCode);
                input_parameters.Add("@WServiceLink_DueDate", 1 + "#datetime#" + model.WServiceLink_DueDate);
                input_parameters.Add("@WServiceLink_KeyCode", 1 + "#nvarchar#" + model.WServiceLink_KeyCode);
                input_parameters.Add("@WServiceLink_LockBox", 1 + "#nvarchar#" + model.WServiceLink_LockBox);
                input_parameters.Add("@WServiceLink_ClientID", 1 + "#nvarchar#" + model.WServiceLink_ClientID);
                input_parameters.Add("@WServiceLink_Loan", 1 + "#nvarchar#" + model.WServiceLink_Loan);
                input_parameters.Add("@WServiceLink_LoanType", 1 + "#nvarchar#" + model.WServiceLink_LoanType);

                input_parameters.Add("@WServiceLink_Owner", 1 + "#nvarchar#" + model.WServiceLink_Owner);
                input_parameters.Add("@WServiceLink_CoverageID", 1 + "#nvarchar#" + model.WServiceLink_CoverageID);
                input_parameters.Add("@WServiceLink_Investor", 1 + "#nvarchar#" + model.WServiceLink_Investor);
                input_parameters.Add("@WServiceLink_services", 1 + "#nvarchar#" + model.WServiceLink_services);
                input_parameters.Add("@WServiceLink_id", 1 + "#nvarchar#" + model.WServiceLink_id);
                input_parameters.Add("@WServiceLink_username", 1 + "#nvarchar#" + model.WServiceLink_username);

                input_parameters.Add("@WServiceLink_Comments", 1 + "#varchar#" + model.WServiceLink_Comments); 
                input_parameters.Add("@WServiceLink_gpsLatitude", 1 + "#varchar#" + model.WServiceLink_gpsLatitude);
                input_parameters.Add("@WServiceLink_gpsLongitude", 1 + "#varchar#" + model.WServiceLink_gpsLongitude);
                input_parameters.Add("@WServiceLink_ImportMaster_Pkey", 1 + "#bigint#" + model.WServiceLink_ImportMaster_Pkey);
                input_parameters.Add("@WServiceLink_Import_File_Name", 1 + "#varchar#" + model.WServiceLink_Import_File_Name);
                input_parameters.Add("@WServiceLink_Import_FilePath", 1 + "#varchar#" + model.WServiceLink_Import_FilePath);
                input_parameters.Add("@WServiceLink_Import_Pdf_Name", 1 + "#varchar#" + model.WServiceLink_Import_Pdf_Name);
                input_parameters.Add("@WServiceLink_Import_Pdf_Path", 1 + "#varchar#" + model.WServiceLink_Import_Pdf_Path);








                input_parameters.Add("@WServiceLink_IsActive", 1 + "#bit#" + model.WServiceLink_IsActive);
                input_parameters.Add("@WServiceLink_IsDelete", 1 + "#bit#" + model.WServiceLink_IsDelete);
                input_parameters.Add("@WServiceLink_Service_ID", 1 + "#nvarchar#" + model.WServiceLink_Service_ID);

                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@WServiceLink_Pkey_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    WorkOrder_ServiceLink_Master.WServiceLink_PkeyID = "0";
                    WorkOrder_ServiceLink_Master.Status = "0";
                    WorkOrder_ServiceLink_Master.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    WorkOrder_ServiceLink_Master.WServiceLink_PkeyID = Convert.ToString(objData[0]);
                    WorkOrder_ServiceLink_Master.Status = Convert.ToString(objData[1]);


                }
                objcltData.Add(WorkOrder_ServiceLink_Master);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;

        }

        private List<dynamic> AddWorkOrder_ServiceLinkItemData(WorkOrder_ServiceLink_Item_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_WorkOrder_ServiceLink_Item_Master]";
            WorkOrder_ServiceLink_Item_Master WorkOrder_ServiceLink_Item_Master = new WorkOrder_ServiceLink_Item_Master();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@WSERVICEINS_PkeyId", 1 + "#bigint#" + model.WSERVICEINS_PkeyId);
                input_parameters.Add("@WSERVICEINS_Ins_Name", 1 + "#varchar#" + model.WSERVICEINS_Ins_Name);
                input_parameters.Add("@WSERVICEINS_Ins_Details", 1 + "#varchar#" + model.WSERVICEINS_Ins_Details);
                input_parameters.Add("@WSERVICEINS_Qty", 1 + "#bigint#" + model.WSERVICEINS_Qty);
                input_parameters.Add("@WSERVICEINS_Price", 1 + "#decimal#" + model.WSERVICEINS_Price);
                input_parameters.Add("@WSERVICEINS_Total", 1 + "#decimal#" + model.WSERVICEINS_Total);
                input_parameters.Add("@WSERVICEINS_FkeyID", 1 + "#bigint#" + model.WSERVICEINS_FkeyID);

                input_parameters.Add("@WSERVICEINS_IsActive", 1 + "#bit#" + model.WSERVICEINS_IsActive);
                input_parameters.Add("@WSERVICEINS_IsDelete", 1 + "#bit#" + model.WSERVICEINS_IsDelete);
                input_parameters.Add("@WSERVICEINS_Additional_Details", 1 + "#nvarchar#" + model.WSERVICEINS_Additional_Details);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@WSERVICEINS_Pkey_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    WorkOrder_ServiceLink_Item_Master.WSERVICEINS_PkeyId = "0";
                    WorkOrder_ServiceLink_Item_Master.Status = "0";
                    WorkOrder_ServiceLink_Item_Master.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    WorkOrder_ServiceLink_Item_Master.WSERVICEINS_PkeyId = Convert.ToString(objData[0]);
                    WorkOrder_ServiceLink_Item_Master.Status = Convert.ToString(objData[1]);

                }
                objcltData.Add(WorkOrder_ServiceLink_Item_Master);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;



        }

        public List<dynamic> AddWorkOrderServiceLinkData(WorkOrder_ServiceLink_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                objData = AddWorkOrder_ServiceLinkData(model);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        public List<dynamic> AddWorkOrderServiceLinkItemData(WorkOrder_ServiceLink_Item_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                objData = AddWorkOrder_ServiceLinkItemData(model);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }
    }

    
}