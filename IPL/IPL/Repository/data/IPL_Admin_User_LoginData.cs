﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace IPL.Repository.data
{
    public class IPL_Admin_User_LoginData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        public List<dynamic> AddAdminUserData(IPL_Admin_User_LoginDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            string insertProcedure = "[CreateUpdateIPL_Admin_User_Master]";
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Ipl_Ad_User_PkeyID", 1 + "#bigint#" + model.Ipl_Ad_User_PkeyID);
                input_parameters.Add("@Ipl_Ad_User_First_Name", 1 + "#varchar#" + model.Ipl_Ad_User_First_Name);
                input_parameters.Add("@Ipl_Ad_User_Last_Name", 1 + "#varchar#" + model.Ipl_Ad_User_Last_Name);
                input_parameters.Add("@Ipl_Ad_User_Email", 1 + "#varchar#" + model.Ipl_Ad_User_Email);
                input_parameters.Add("@Ipl_Ad_User_Address", 1 + "#varchar#" + model.Ipl_Ad_User_Address);
                input_parameters.Add("@Ipl_Ad_User_City", 1 + "#varchar#" + model.Ipl_Ad_User_City);
                input_parameters.Add("@Ipl_Ad_User_State", 1 + "#varchar#" + model.Ipl_Ad_User_State);
                input_parameters.Add("@Ipl_Ad_User_Mobile", 1 + "#varchar#" + model.Ipl_Ad_User_Mobile);
                input_parameters.Add("@Ipl_Ad_User_Login_Name", 1 + "#varchar#" + model.Ipl_Ad_User_Login_Name);
                input_parameters.Add("@Ipl_Ad_User_Password", 1 + "#varchar#" + model.Ipl_Ad_User_Password);
                input_parameters.Add("@Ipl_Ad_User_IsActive", 1 + "#bit#" + model.Ipl_Ad_User_IsActive);
                input_parameters.Add("@Ipl_Ad_User_IsDelete", 1 + "#bit#" + model.Ipl_Ad_User_IsDelete);
                input_parameters.Add("@Ipl_Ad_User_UserVal", 1 + "#varchar#" + model.Ipl_Ad_User_UserVal);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Ipl_Ad_User_PkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;


        }



        private DataSet GetAdminLoginMaster(IPL_Admin_User_LoginDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[GetAdminUserLogin]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Ipl_Ad_User_Login_Name", 1 + "#varchar#" + model.Ipl_Ad_User_Login_Name);
                input_parameters.Add("@Ipl_Ad_User_PkeyID", 1 + "#bigint#" + model.Ipl_Ad_User_PkeyID);
                input_parameters.Add("@Ipl_Ad_User_Password", 1 + "#varchar#" + model.Ipl_Ad_User_Password);
                input_parameters.Add("@Ipl_Ad_User_UserVal", 1 + "#varchar#" + model.Ipl_Ad_User_UserVal);

                input_parameters.Add("@WhereClause", 1 + "#nvarchar#" + model.WhereClause);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@PageNumber", 1 + "#int#" + model.PageNumber);
                input_parameters.Add("@Rowcount", 1 + "#int#" + model.Rowcount);
               
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }


            return ds;
        }
        public List<dynamic> GetAuthenticateAdminLoginDetails(IPL_Admin_User_LoginDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetAdminLoginMaster(model);
                if (ds.Tables.Count > 0)
                {
                    var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                    List<IPL_Admin_User_LoginDTO> adusersDetails =
                       (from item in myEnumerableFeaprd
                        select new IPL_Admin_User_LoginDTO
                        {
                            Ipl_Ad_User_PkeyID = item.Field<Int64>("Ipl_Ad_User_PkeyID"),
                            Ipl_Ad_User_First_Name = item.Field<String>("Ipl_Ad_User_First_Name"),
                            Ipl_Ad_User_Last_Name = item.Field<String>("Ipl_Ad_User_Last_Name"),

                        }).ToList();

                    objDynamic.Add(adusersDetails);
                }
                else
                {
                    objDynamic.Add("No UserRecord Found");
                    log.logDebugMessage("No UserRecord Found For" + model.Ipl_Ad_User_Login_Name);
                }

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

            }

            return objDynamic;
        }
        public List<dynamic> GetLoginDetails(IPL_Admin_User_LoginDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetAdminLoginMaster(model);
                if (ds.Tables.Count > 0)
                {
                    var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                    List<IPL_Admin_User_LoginDTO> adusersDetails =
                       (from item in myEnumerableFeaprd
                        select new IPL_Admin_User_LoginDTO
                        {
                            Ipl_Ad_User_PkeyID = item.Field<Int64>("Ipl_Ad_User_PkeyID"),
                            Ipl_Ad_User_First_Name = item.Field<String>("Ipl_Ad_User_First_Name"),
                            Ipl_Ad_User_Last_Name = item.Field<String>("Ipl_Ad_User_Last_Name"),
                            Ipl_Ad_User_Password = item.Field<String>("Ipl_Ad_User_Password"),
                            Ipl_Ad_User_Login_Name = item.Field<String>("Ipl_Ad_User_Login_Name"),
                            Ipl_Ad_User_Mobile = item.Field<String>("Ipl_Ad_User_Mobile"),

                        }).ToList();

                    objDynamic.Add(adusersDetails);
                }
              
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

            }

            return objDynamic;
        }

        //get user details
        public List<dynamic> GetUserDetails(IPL_Admin_User_LoginDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                string wherecondition = string.Empty;
                if (model.Type == 5 && !string.IsNullOrEmpty(model.FilterData))
                {
                    var Data = JsonConvert.DeserializeObject<NewIPLAdminUserLogin_Filter>(model.FilterData);
                    if (!string.IsNullOrEmpty(Data.Ipl_Ad_User_First_Name))
                    {
                        wherecondition = " And au.Ipl_Ad_User_First_Name LIKE '%" + Data.Ipl_Ad_User_First_Name + "%'";
                    }
                    if (!string.IsNullOrEmpty(Data.Ipl_Ad_User_Last_Name))
                    {
                        wherecondition = " And au.Ipl_Ad_User_Last_Name LIKE '%" + Data.Ipl_Ad_User_Last_Name + "%'";
                    }
                    if (!string.IsNullOrEmpty(Data.Ipl_Ad_User_Login_Name))
                    {
                        wherecondition = " And au.Ipl_Ad_User_Login_Name LIKE '%" + Data.Ipl_Ad_User_Login_Name + "%'";
                    }
                    if (!string.IsNullOrEmpty(Data.Ipl_Ad_User_Address))
                    {
                        wherecondition = " And au.Ipl_Ad_User_Address LIKE '%" + Data.Ipl_Ad_User_Address + "%'";
                    }
                    if (!string.IsNullOrEmpty(Data.Ipl_Ad_User_State))
                    {
                        wherecondition = " And au.Ipl_Ad_User_State LIKE '%" + Data.Ipl_Ad_User_State + "%'";
                    }

                    model.WhereClause = wherecondition;
                }

                DataSet ds = GetAdminLoginMaster(model);
                if (ds.Tables.Count > 0)
                {
                    var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                    List<IPL_Admin_User_LoginDTO> getuser =
                       (from item in myEnumerableFeaprd
                        select new IPL_Admin_User_LoginDTO
                        {
                            Ipl_Ad_User_PkeyID = item.Field<Int64>("Ipl_Ad_User_PkeyID"),
                            Ipl_Ad_User_First_Name = item.Field<String>("Ipl_Ad_User_First_Name"),
                            Ipl_Ad_User_Last_Name = item.Field<String>("Ipl_Ad_User_Last_Name"),
                            Ipl_Ad_User_Password = item.Field<String>("Ipl_Ad_User_Password"),
                            Ipl_Ad_User_Login_Name = item.Field<String>("Ipl_Ad_User_Login_Name"),
                            Ipl_Ad_User_Email = item.Field<String>("Ipl_Ad_User_Email"),
                            Ipl_Ad_User_Mobile = item.Field<String>("Ipl_Ad_User_Mobile"),
                            Ipl_Ad_User_Address = item.Field<String>("Ipl_Ad_User_Address"),
                            Ipl_Ad_User_City = item.Field<String>("Ipl_Ad_User_City"),
                            Ipl_Ad_User_State = item.Field<String>("Ipl_Ad_User_State"),
                            Ipl_Ad_User_IsActive = item.Field<Boolean?>("Ipl_Ad_User_IsActive"),

                        }).ToList();

                    objDynamic.Add(getuser);
                }

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

            }

            return objDynamic;
        }
    }
}