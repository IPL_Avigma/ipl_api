﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

//changes
namespace IPL.Repository.data
{
    public class WorkType_ConfigurationData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public List<dynamic> AddWorkType_ConfigurationData(WorkType_Configuration_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objAddData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_WorkType_Configuration_Master]";
            WorkType_Configuration_Master workType_Configuration_Master = new WorkType_Configuration_Master();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@WorkType_Configuration_PkeyId", 1 + "#bigint#" + model.WorkType_Configuration_PkeyId);
                input_parameters.Add("@WorkType_Configuration_WorkType_Id", 1 + "#bigint#" + model.WorkType_Configuration_WorkType_Id);
                input_parameters.Add("@WorkType_Configuration_OrderType_Id", 1 + "#bigint#" + model.WorkType_Configuration_OrderType_Id);
                input_parameters.Add("@WorkType_Configuration_Import_From_Id", 1 + "#bigint#" + model.WorkType_Configuration_Import_From_Id);
                input_parameters.Add("@WorkType_Configuration_Client_Id", 1 + "#bigint#" + model.WorkType_Configuration_Client_Id);
                input_parameters.Add("@WorkType_Configuration_Company_Id", 1 + "#bigint#" + model.WorkType_Configuration_Company_Id);
                input_parameters.Add("@WorkType_Configuration_User_Id", 1 + "#bigint#" + model.WorkType_Configuration_User_Id);
                input_parameters.Add("@WorkType_Configuration_IsActive", 1 + "#bit#" + model.WorkType_Configuration_IsActive);
                input_parameters.Add("@WorkType_Configuration_IsDelete", 1 + "#bit#" + model.WorkType_Configuration_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@WorkType_Configuration_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    workType_Configuration_Master.WorkType_Configuration_PkeyId = "0";
                    workType_Configuration_Master.Status = "0";
                    workType_Configuration_Master.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    workType_Configuration_Master.WorkType_Configuration_PkeyId = Convert.ToString(objData[0]);
                    workType_Configuration_Master.Status = Convert.ToString(objData[1]);


                }
                objAddData.Add(workType_Configuration_Master);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objAddData;
        }

        private DataSet GetWorkType_ConfigurationMaster(WorkType_Configuration_MasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_WorkType_Configuration_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@WorkType_Configuration_PkeyId", 1 + "#bigint#" + model.WorkType_Configuration_PkeyId);
                input_parameters.Add("@WorkType_Configuration_Company_Id", 1 + "#bigint#" + model.WorkType_Configuration_Company_Id);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return ds;
        }

        public List<dynamic> GetWorkType_ConfigurationMasterDetails(WorkType_Configuration_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetWorkType_ConfigurationMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<WorkType_Configuration_MasterDTO> Task_Configuration =
                   (from item in myEnumerableFeaprd
                    select new WorkType_Configuration_MasterDTO
                    {
                        WorkType_Configuration_PkeyId = item.Field<Int64>("WorkType_Configuration_PkeyId"),
                        WorkType_Configuration_WorkType_Id = item.Field<Int64?>("WorkType_Configuration_WorkType_Id"),
                        WorkType_Configuration_OrderType_Desc = item.Field<String>("WorkType_Configuration_OrderType_Desc"),
                        WorkType_Configuration_OrderType_Id = item.Field<Int64?>("WorkType_Configuration_OrderType_Id"),
                        WorkType_Configuration_Import_From_Id = item.Field<Int64?>("WorkType_Configuration_Import_From_Id"),
                        WorkType_Configuration_Client_Id = item.Field<Int64?>("WorkType_Configuration_Client_Id"),
                        WorkType_Configuration_Company_Id = item.Field<Int64?>("WorkType_Configuration_Company_Id"),
                        WorkType_Configuration_User_Id = item.Field<Int64?>("WorkType_Configuration_User_Id"),
                        WorkType_Configuration_IsActive = true,
                        WorkType_Configuration_CreatedBy = item.Field<string>("WorkType_Configuration_CreatedBy"),
                        WorkType_Configuration_ModifiedBy = item.Field<string>("WorkType_Configuration_ModifiedBy"),


                    }).ToList();

                objDynamic.Add(Task_Configuration);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        public List<dynamic> PostWorkTypeConfigurationMasterDetails(WorkType_Configuration_ListDTO modelList)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                foreach (var model in modelList.WorkType_Configuration_List)
                {
                    model.Type = model.WorkType_Configuration_PkeyId > 0 ? 2 : 1;
                    model.WorkType_Configuration_IsActive = true;
                    model.WorkType_Configuration_IsDelete = false;
                    var objData = AddWorkType_ConfigurationData(model);
                    objDynamic.Add(objData);
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}