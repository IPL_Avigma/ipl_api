﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class CyprexxJobDocumentationChecklist_Data
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public List<dynamic> CreateUpdate_CyprexxJobDocumentationChecklist(CyprexxJobDocumentationChecklist_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objclntData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_PCR_Cyprexx_Job_Documentation_Checklist]";
            PCR_LoggerMessage pCR_Logger = new PCR_LoggerMessage();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@PCR_JD_PkeyID", 1 + "#bigint#" + model.PCR_JD_PkeyID);
                input_parameters.Add("@PCR_JD_WO_ID", 1 + "#bigint#" + model.PCR_JD_WO_ID);
                input_parameters.Add("@PCR_JD_Job_Info", 1 + "#nvarchar#" + model.PCR_JD_Job_Info);
                input_parameters.Add("@PCR_JD_Upload_Photos", 1 + "#nvarchar#" + model.PCR_JD_Upload_Photos);
                input_parameters.Add("@PCR_JD_IsActive", 1 + "#bit#" + model.PCR_JD_IsActive);
                input_parameters.Add("@PCR_JD_IsDelete", 1 + "#bit#" + model.PCR_JD_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@fwo_pkyeId", 1 + "#bigint#" + model.fwo_pkyeId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@PCR_JD_PkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    pCR_Logger.PCR_PkeyID = "0";
                    pCR_Logger.Status = "0";
                    pCR_Logger.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    pCR_Logger.PCR_PkeyID = Convert.ToString(objData[0]);
                    pCR_Logger.Status = Convert.ToString(objData[1]);


                }
                objclntData.Add(pCR_Logger);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objclntData;



        }

        private DataSet GetCyprexxJobDocumentationChecklist(CyprexxJobDocumentationChecklist_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_PCR_Cyprexx_Job_Documentation_Checklist]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@PCR_JD_PkeyID", 1 + "#bigint#" + model.PCR_JD_PkeyID);
                input_parameters.Add("@PCR_JD_WO_ID", 1 + "#bigint#" + model.PCR_JD_WO_ID);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ds;
        }

        public List<dynamic> GetCyprexxJobDocumentationChecklistData(CyprexxJobDocumentationChecklist_DTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetCyprexxJobDocumentationChecklist(model);

                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                }

                //var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                //List<CyprexxJobDocumentationChecklist_DTO> pcrDTP =
                //   (from item in myEnumerableFeaprd
                //    select new CyprexxJobDocumentationChecklist_DTO
                //    {
                //        PCR_JD_PkeyID = item.Field<Int64>("PCR_JD_PkeyID"),
                //        PCR_JD_WO_ID = item.Field<Int64>("PCR_JD_WO_ID"),
                //        PCR_JD_CompanyId = item.Field<Int64>("PCR_JD_CompanyId"),
                //        PCR_JD_Job_Info = item.Field<String>("PCR_JD_Job_Info"),
                //        PCR_JD_Upload_Photos = item.Field<String>("PCR_JD_Upload_Photos"),
                //        PCR_JD_IsActive = item.Field<Boolean?>("PCR_JD_IsActive"),
                //    }).ToList();

                //objDynamic.Add(pcrDTP);

                //if (ds.Tables.Count > 1)
                //{
                //    var pcrHistory = ds.Tables[1].AsEnumerable();
                //    List<CyprexxJobDocumentationChecklist_DTO> pcrDTP_history =
                //       (from item in pcrHistory
                //        select new CyprexxJobDocumentationChecklist_DTO
                //        {
                //            PCR_JD_PkeyID = item.Field<Int64>("PCR_JD_PkeyID"),
                //            PCR_JD_WO_ID = item.Field<Int64>("PCR_JD_WO_ID"),
                //            PCR_JD_CompanyId = item.Field<Int64>("PCR_JD_CompanyId"),
                //            PCR_JD_Job_Info = item.Field<String>("PCR_JD_Job_Info"),
                //            PCR_JD_Upload_Photos = item.Field<String>("PCR_JD_Upload_Photos"),
                //            PCR_JD_IsActive = item.Field<Boolean?>("PCR_JD_IsActive"),
                //        }).ToList();

                //    objDynamic.Add(pcrDTP_history);
                //}
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}