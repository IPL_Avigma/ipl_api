﻿using Avigma.Repository.Lib;
using IPL.Models;
using Newtonsoft.Json;
using NReco.PdfGenerator;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace IPL.Repository.data
{
    public class ReportMasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        private DataSet GetReportData(ReportMasterDTO model)
        {
            DataSet ds = null;
            try
            {
                // Added by Dipali
                string selectProcedure = "[GetReportMaster]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@whereClause", 1 + "#varchar#" + model.whereClause);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserId);

                ds = obj.SelectSql(selectProcedure, input_parameters);
                //switch (model.Valtype)
                //{
                //    case 1:
                //        {
                //            string selectProcedure = "[GetReportMaster]";
                //            Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                //            input_parameters.Add("@whereClause", 1 + "#varchar#" + model.whereClause);
                //            input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                //            ds = obj.SelectSql(selectProcedure, input_parameters);
                //            break;
                //        }
                //    case 2:
                //        {
                //            string selectProcedure = "[GetContractorReportMaster]";
                //            Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                //            input_parameters.Add("@whereClause", 1 + "#varchar#" + model.whereClause);
                //            input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                //            ds = obj.SelectSql(selectProcedure, input_parameters);
                //            break;
                //        }
                //}


            }

            catch (Exception ex)
            {

                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetReportDetails(ReportMasterDTO model )
        {
            List<dynamic> objDynamic = new List<dynamic>();
            string wherecondition = string.Empty;
            string company = null; // Added By Dipali
            string customer = null;
            string contractor = null;
            string admin = null;
            string category = null;
            string state = null;

            var Data = JsonConvert.DeserializeObject<ReportMasterDTO>(model.whereClause);
            try
            {

                if (Data.InvoiceDateFrom != null && Data.InvoiceDateTo != null)
                {
                    if (model.IsClientCheck && model.IsContractorCheck)
                    {
                        wherecondition = " And ((CAST(clientInv.Inv_Client_CreatedOn as date) >=   CONVERT(date,'" + Data.InvoiceDateFrom.Value.ToString("yyyy-MM-dd") + "')  And CAST(clientInv.Inv_Client_CreatedOn as date) <=   CONVERT(date,'" + Data.InvoiceDateTo.Value.ToString("yyyy-MM-dd") + "')) or (CAST(inv.Inv_Con_CreatedOn as date) >= CONVERT(date,'" + Data.InvoiceDateFrom.Value.ToString("yyyy-MM-dd") + "') And CAST(inv.Inv_Con_CreatedOn as date) <=   CONVERT(date,'" + Data.InvoiceDateTo.Value.ToString("yyyy-MM-dd") + "')))";
                    }
                    else if (model.IsClientCheck)
                    {
                        wherecondition = " And CAST(clientInv.Inv_Client_CreatedOn as date) >=   CONVERT(date,'" + Data.InvoiceDateFrom.Value.ToString("yyyy-MM-dd") + "')  And CAST(clientInv.Inv_Client_CreatedOn as date) <=   CONVERT(date,'" + Data.InvoiceDateTo.Value.ToString("yyyy-MM-dd") + "')";
                         
                    }
                    else
                    {
                        wherecondition = " And CAST(inv.Inv_Con_CreatedOn as date) >=   CONVERT(date,'" + Data.InvoiceDateFrom.Value.ToString("yyyy-MM-dd") + "')  And CAST(inv.Inv_Con_CreatedOn as date) <=   CONVERT(date,'" + Data.InvoiceDateTo.Value.ToString("yyyy-MM-dd") + "')";
                    }
                    //switch (model.Valtype)
                    //{
                    //    case 1:
                    //        {
                    //            wherecondition = " And CAST(inv.Inv_Client_CreatedOn as date) >=   CONVERT(date,'" + Data.InvoiceDateFrom.Value.ToString("yyyy-MM-dd") + "')  And CAST(inv.Inv_Client_CreatedOn as date) <=   CONVERT(date,'" + Data.InvoiceDateTo.Value.ToString("yyyy-MM-dd") + "')";
                    //            break;
                    //        }
                    //    case 2:
                    //        {
                    //            wherecondition = " And CAST(inv.Inv_Con_CreatedOn as date) >=   CONVERT(date,'" + Data.InvoiceDateFrom.Value.ToString("yyyy-MM-dd") + "')  And CAST(inv.Inv_Con_CreatedOn as date) <=   CONVERT(date,'" + Data.InvoiceDateTo.Value.ToString("yyyy-MM-dd") + "')";
                    //            break;
                    //        }
                    //}    
                }

                if (Data.ReadyOfficeDateFrom != null && Data.ReadyOfficeDateTo != null)
                {
                   // DateTime ReadyOfficeDateTo = Convert.ToDateTime(Data.ReadyOfficeDateTo).AddDays(1);
                    wherecondition = " And CAST(work.Field_complete_date as date) >=   CONVERT(date,'" + Data.ReadyOfficeDateFrom.Value.ToString("yyyy-MM-dd") + "')  And CAST(work.Field_complete_date as date) <=   CONVERT(date,'" + Data.ReadyOfficeDateTo.Value.ToString("yyyy-MM-dd") + "')";                   
                }
                if (Data.SentToClientDateFrom != null && Data.SentToClientDateTo != null)
                {
                   // DateTime SentToClientDateTo = Convert.ToDateTime(Data.SentToClientDateTo).AddDays(1);
                    wherecondition = "  And CAST(work.SentToClient_date as date) >=   CONVERT(date,'" + Data.SentToClientDateFrom.Value.ToString("yyyy-MM-dd") + "')  And CAST(work.SentToClient_date as date) <=   CONVERT(date,'" + Data.SentToClientDateTo.Value.ToString("yyyy-MM-dd") + "')";                    
                }
                if (Data.CompletedDateFrom != null && Data.CompletedDateTo != null)
                {
                     wherecondition = "  And CAST(work.Complete_Date as date) >=   CONVERT(date,'" + Data.CompletedDateFrom.Value.ToString("yyyy-MM-dd") + "')  And CAST(work.Complete_Date as date) <=   CONVERT(date,'" + Data.CompletedDateTo.Value.ToString("yyyy-MM-dd") + "')";
                }
                if (Data.CreatedDateFrom != null && Data.CreatedDateTo != null)
                {
                    //wherecondition = " And CAST(work.CreatedOn as date) >=   CONVERT(date,'" + Data.CreatedDateFrom.Value.ToString("yyyy-MM-dd") + "')  And CAST(work.CreatedOn as date) <=   CONVERT(date,'" + Data.CreatedDateTo.Value.ToString("yyyy-MM-dd") + "')";
                    wherecondition = " And CAST(work.DateCreated as date) >=   CONVERT(date,'" + Data.CreatedDateFrom.Value.ToString("yyyy-MM-dd") + "')  And CAST(work.DateCreated as date) <=   CONVERT(date,'" + Data.CreatedDateTo.Value.ToString("yyyy-MM-dd") + "')";
                }
                if (Data.OfficeApproveDateFrom != null && Data.OfficeApproveDateTo != null)
                {
                    wherecondition = " And work.status = 6 And CAST(work.OfficeApproved_date as date) >=   CONVERT(date,'" + Data.OfficeApproveDateFrom.Value.ToString("yyyy-MM-dd") + "')  And CAST(work.OfficeApproved_date as date) <=   CONVERT(date,'" + Data.OfficeApproveDateTo.Value.ToString("yyyy-MM-dd") + "')";
                }
                if (Data.ClientCheckDateFrom != null && Data.ClientCheckDateTo != null)
                {
                    if (model.IsClientCheck && model.IsContractorCheck)
                    {
                        wherecondition = " And ((CAST((select max(Client_Pay_Payment_Date) from [dbo].[Client_Invoice_Patyment] where Client_Pay_Wo_Id = work.[workOrder_ID] and Client_Pay_IsActive = 1 GROUP BY Client_Pay_Wo_Id) as date) >=   CONVERT(date,'" + Data.ClientCheckDateFrom.Value.ToString("yyyy-MM-dd") + "') And CAST((select max(Client_Pay_Payment_Date) from [dbo].[Client_Invoice_Patyment] where Client_Pay_Wo_Id = work.[workOrder_ID] and Client_Pay_IsActive = 1 GROUP BY Client_Pay_Wo_Id) as date) <=   CONVERT(date,'" + Data.ClientCheckDateTo.Value.ToString("yyyy-MM-dd") + "')) or (CAST((select max(Con_Pay_Payment_Date) from [dbo].Contractor_Invoice_Payment where Con_Pay_Wo_Id = work.[workOrder_ID] and Con_Pay_IsActive = 1 GROUP BY Con_Pay_Wo_Id) as date) >= CONVERT(date,'" + Data.ClientCheckDateFrom.Value.ToString("yyyy-MM-dd") + "') And CAST((select max(Con_Pay_Payment_Date) from [dbo].Contractor_Invoice_Payment where Con_Pay_Wo_Id = work.[workOrder_ID] and Con_Pay_IsActive = 1 GROUP BY Con_Pay_Wo_Id) as date) <=   CONVERT(date,'" + Data.ClientCheckDateTo.Value.ToString("yyyy-MM-dd") + "')))";
                    }
                    else if (model.IsClientCheck)
                    {
                        wherecondition = " And CAST((select max(Client_Pay_Payment_Date) from [dbo].[Client_Invoice_Patyment] where Client_Pay_Wo_Id = work.[workOrder_ID] and Client_Pay_IsActive = 1 GROUP BY Client_Pay_Wo_Id) as date) >=  CONVERT(date,'" + Data.ClientCheckDateFrom.Value.ToString("yyyy-MM-dd") + "')  And CAST((select max(Client_Pay_Payment_Date) from [dbo].[Client_Invoice_Patyment] where Client_Pay_Wo_Id = work.[workOrder_ID] and Client_Pay_IsActive = 1 GROUP BY Client_Pay_Wo_Id) as date) <=  CONVERT(date,'" + Data.ClientCheckDateTo.Value.ToString("yyyy-MM-dd") + "')";
                    }
                    else
                    {
                        wherecondition = " And CAST((select max(Con_Pay_Payment_Date) from [dbo].Contractor_Invoice_Payment where Con_Pay_Wo_Id = work.[workOrder_ID] and Con_Pay_IsActive = 1 GROUP BY Con_Pay_Wo_Id) as date) >=  CONVERT(date,'" + Data.ClientCheckDateFrom.Value.ToString("yyyy-MM-dd") + "')  And CAST((select max(Con_Pay_Payment_Date) from [dbo].Contractor_Invoice_Payment where Con_Pay_Wo_Id = work.[workOrder_ID] and Con_Pay_IsActive = 1 GROUP BY Con_Pay_Wo_Id) as date) <=  CONVERT(date,'" + Data.ClientCheckDateTo.Value.ToString("yyyy-MM-dd") + "')";
                    }
                }
                //Added by Niral Start

                if (model.Inv_Client_Paid == true)
                {
                    if (model.Inv_Client_Paid_Date.HasValue && model.Inv_Client_Paid_Date_Through.HasValue)
                    {
                        string startDate = model.Inv_Client_Paid_Date.Value.ToString("yyyy-MM-dd");
                        string endDate = model.Inv_Client_Paid_Date_Through.Value.ToString("yyyy-MM-dd");

                        wherecondition = wherecondition != null
                            ? wherecondition + " And CAST(clip.Client_Pay_Payment_Date as date) >= CONVERT(date, '" + startDate + "') And CAST(clip.Client_Pay_Payment_Date as date) <= CONVERT(date, '" + endDate + "')"
                            : "CAST(clip.Client_Pay_Payment_Date as date) >= CONVERT(date, '" + startDate + "') And CAST(clip.Client_Pay_Payment_Date as date) <= CONVERT(date, '" + endDate + "')";
                    }
                    if (!string.IsNullOrWhiteSpace(model.Inv_Client_Paid_Check))
                    {
                        wherecondition = wherecondition != null ? wherecondition + " And clip.Client_Pay_CheckNumber Like '%"+ model.Inv_Client_Paid_Check+"%'" : 
                            " And clip.Client_Pay_CheckNumber Like '%" + model.Inv_Client_Paid_Check + "%'";
                    }
                }

                if (model.Inv_Contractor_Paid == true)
                {
                    if (model.Inv_Contractor_Paid_Date.HasValue && model.Inv_Contractor_Paid_Date_Through.HasValue)
                    {
                        string startDate = model.Inv_Contractor_Paid_Date.Value.ToString("yyyy-MM-dd");
                        string endDate = model.Inv_Contractor_Paid_Date_Through.Value.ToString("yyyy-MM-dd");

                        wherecondition = wherecondition != null
                            ? wherecondition + " And CAST(conip.Con_Pay_Payment_Date as date) >= CONVERT(date, '" + startDate + "') And CAST(conip.Con_Pay_Payment_Date as date) <= CONVERT(date, '" + endDate + "')"
                            : "CAST(conip.Con_Pay_Payment_Date as date) >= CONVERT(date, '" + startDate + "') And CAST(conip.Con_Pay_Payment_Date as date) <= CONVERT(date, '" + endDate + "')";
                    }

                    if (!string.IsNullOrWhiteSpace(model.Inv_Contractor_Paid_Check))
                    {
                        wherecondition = wherecondition != null ? wherecondition + " And conip.Con_Pay_CheckNumber Like '%" + model.Inv_Contractor_Paid_Check + "%'" :
                            " And conip.Con_Pay_CheckNumber Like '%" + model.Inv_Contractor_Paid_Check + "%'";
                    }
                }

                // by Niral End

                // Added By Dipali
                if (model != null && model.ReportAutoAssinArray != null && model.ReportAutoAssinArray.Count > 0)
                {
                    for (int i = 0; i < model.ReportAutoAssinArray.Count; i++)
                    {                        
                        if (model.ReportAutoAssinArray[i].Task_sett_Company != null)
                        {
                            for (int j = 0; j < model.ReportAutoAssinArray[i].Task_sett_Company.Count; j++)
                            {
                                company = company == null ? model.ReportAutoAssinArray[i].Task_sett_Company[j].Client_pkeyID.ToString() : company + "," + model.ReportAutoAssinArray[i].Task_sett_Company[j].Client_pkeyID.ToString();
                            }
                        }
                        if (model.ReportAutoAssinArray[i].Task_sett_Customer != null)
                        {
                            for (int k = 0; k < model.ReportAutoAssinArray[i].Task_sett_Customer.Count; k++)
                            {
                                customer = customer == null ? model.ReportAutoAssinArray[i].Task_sett_Customer[k].Cust_Num_pkeyId.ToString() : customer + "," + model.ReportAutoAssinArray[i].Task_sett_Customer[k].Cust_Num_pkeyId.ToString();
                            }
                        }
                        if (model.ReportAutoAssinArray[i].Task_sett_Contractor != null)
                        {
                            for (int m = 0; m < model.ReportAutoAssinArray[i].Task_sett_Contractor.Count; m++)
                            {
                                contractor = contractor == null ? model.ReportAutoAssinArray[i].Task_sett_Contractor[m].User_pkeyID.ToString() : contractor + "," + model.ReportAutoAssinArray[i].Task_sett_Contractor[m].User_pkeyID.ToString();
                            }
                        }
                        if (model.ReportAutoAssinArray[i].Task_sett_Admin != null)
                        {
                            for (int n = 0; n < model.ReportAutoAssinArray[i].Task_sett_Admin.Count; n++)
                            {
                                admin = admin == null ? model.ReportAutoAssinArray[i].Task_sett_Admin[n].User_pkeyID.ToString() : admin + "," + model.ReportAutoAssinArray[i].Task_sett_Admin[n].User_pkeyID.ToString();

                            }
                        }
                        if (model.ReportAutoAssinArray[i].Task_sett_Category != null)
                        {
                            for (int p = 0; p < model.ReportAutoAssinArray[i].Task_sett_Category.Count; p++)
                            {
                                category = category == null ? model.ReportAutoAssinArray[i].Task_sett_Category[p].Cat_ID.ToString() : category + "," + model.ReportAutoAssinArray[i].Task_sett_Category[p].Cat_ID.ToString();
                            }
                        }
                        if (model.ReportAutoAssinArray[i].Task_sett_State != null)
                        {
                            for (int q = 0; q < model.ReportAutoAssinArray[i].Task_sett_State.Count; q++)
                            {
                                state = state == null ? model.ReportAutoAssinArray[i].Task_sett_State[q].IPL_StateID.ToString() : state + "," + model.ReportAutoAssinArray[i].Task_sett_State[q].IPL_StateID.ToString();
                            }
                        }                        
                    }
                }

                if (!string.IsNullOrEmpty(company))
                {
                    wherecondition = wherecondition != null ? wherecondition + " And work.Company IN(" + company + ")" : " And work.Company IN(" + company + ")";
                }
                if (!string.IsNullOrEmpty(customer))
                {
                    wherecondition = wherecondition != null ? wherecondition + " And work.Customer_Number IN(" + customer + ")" : " And work.Customer_Number IN(" + customer + ")";
                }
                if (!string.IsNullOrEmpty(contractor))
                {
                    wherecondition = wherecondition != null ? wherecondition + " And work.Contractor IN(" + contractor + ")" : " And work.Contractor IN(" + contractor + ")";
                }
                if (!string.IsNullOrEmpty(admin))
                {
                    wherecondition = wherecondition != null ? wherecondition + " And work.Processor IN(" + admin + ")" : " And work.Processor IN(" + admin + ")";
                }
                if (!string.IsNullOrEmpty(category))
                {
                    wherecondition = wherecondition != null ? wherecondition + " And work.Category IN(" + category + ")" : " And work.Category IN(" + category + ")";
                }
                if (!string.IsNullOrEmpty(state))
                {
                    wherecondition = wherecondition != null ? wherecondition + " And work.state IN(" + state + ")" : " And work.state IN(" + state + ")";
                }
                if (!string.IsNullOrEmpty(model.InvoiceRangeStart))
                {
                    wherecondition = wherecondition != null ? wherecondition + " And IPLNO >= " + model.InvoiceRangeStart : " And IPLNO >= " + model.InvoiceRangeStart;
                }
                if (!string.IsNullOrEmpty(model.InvoiceRangeEnd))
                {
                    wherecondition = wherecondition != null ? wherecondition + " And IPLNO <= " + model.InvoiceRangeEnd : " And IPLNO <= " + model.InvoiceRangeEnd;
                }
                if (!string.IsNullOrEmpty(model.ClientInvoiceRangeStart) && model.Valtype == 1)
                {
                    wherecondition = wherecondition != null ? wherecondition + " And clientInv.Inv_Client_Invoice_Number >= '" + model.ClientInvoiceRangeStart + "' " : "  And clientInv.Inv_Client_Invoice_Number >=  '" + model.ClientInvoiceRangeStart + "'";
                }
                if (!string.IsNullOrEmpty(model.ClientInvoiceRangeEnd) && model.Valtype == 1)
                {
                    wherecondition = wherecondition != null ? wherecondition + " And clientInv.Inv_Client_Invoice_Number <= '" + model.ClientInvoiceRangeEnd + "' " : " And clientInv.Inv_Client_Invoice_Number <= '" + model.ClientInvoiceRangeEnd + "'";
                }

                if (!string.IsNullOrEmpty(model.ContractorInvoiceRangeStart) && model.Valtype == 1)
                {
                    wherecondition = wherecondition != null ? wherecondition + " And inv.Inv_Con_Invoce_Num >= '" + model.ContractorInvoiceRangeStart + "' " : " And inv.Inv_Con_Invoce_Num >= '" + model.ContractorInvoiceRangeStart + "'";
                }
                if (!string.IsNullOrEmpty(model.ContractorInvoiceRangeEnd) && model.Valtype == 1)
                {
                    wherecondition = wherecondition != null ? wherecondition + " And inv.Inv_Con_Invoce_Num <= '" + model.ContractorInvoiceRangeEnd + "' " : " And inv.Inv_Con_Invoce_Num <= '" + model.ContractorInvoiceRangeEnd + "'";
                }


                if (model.Inv_Con_Inv_Followup == true) 
                {
                    // wherecondition = wherecondition != null ? wherecondition + " And inv.Inv_Con_Inv_Followup  = " + model.Inv_Con_Inv_Followup + "" : " And inv.Inv_Con_Inv_Followup  =" + model.Inv_Con_Inv_Followup + "";
                    wherecondition = wherecondition != null ? wherecondition + " And inv.Inv_Con_Inv_Followup  = 1 " : " And inv.Inv_Con_Inv_Followup  = 1";
                }

                if (model.Inv_Con_Inv_Approve == true)
                {
                    //wherecondition = wherecondition != null ? wherecondition + " And inv.Inv_Con_Inv_Approve  = " + model.Inv_Con_Inv_Approve + "" : " And inv.Inv_Con_Inv_Approve  =" + model.Inv_Con_Inv_Approve + "";
                    wherecondition = wherecondition != null ? wherecondition + " And inv.Inv_Con_Inv_Approve  = 1" : " And inv.Inv_Con_Inv_Approve  = 1";
                }
                if (model.Inv_Client_IsNoCharge == true)
                {
                    //wherecondition = wherecondition != null ? wherecondition + " And clientInv.Inv_Client_IsNoCharge  = " + model.Inv_Client_IsNoCharge + "" : " And clientInv.Inv_Client_IsNoCharge  =" + model.Inv_Client_IsNoCharge + "";
                    wherecondition = wherecondition != null ? wherecondition + " And clientInv.Inv_Client_IsNoCharge  = 1" : " And clientInv.Inv_Client_IsNoCharge  = 1";
                }

                

                ReportMasterDTO reportMasterDTO = new ReportMasterDTO();

                reportMasterDTO.Type = 2;
                reportMasterDTO.Valtype = model.Valtype;
                reportMasterDTO.whereClause = wherecondition;
                reportMasterDTO.UserId = model.UserId;
                DataSet ds = GetReportData(reportMasterDTO);

                if (ds != null && ds.Tables.Count > 0)
                {
                    var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                    List<ReportMasterDTO> ReportDetails =
                       (from item in myEnumerableFeaprd
                        select new ReportMasterDTO
                        {
                            workOrder_ID = item.Field<Int64>("workOrder_ID"),
                            IPLNO = item.Field<String>("IPLNO"),
                            workOrderNumber = item.Field<String>("workOrderNumber"),
                            Contractor = item.Field<Int64?>("Contractor"),
                            address1 = item.Field<String>("address1"),
                            city = item.Field<String>("city"),
                            state = item.Field<String>("state"),
                            zip = item.Field<Int64?>("zip"),
                            WorkType = item.Field<Int64?>("WorkType"),
                            dueDate = item.Field<DateTime?>("dueDate"),   
                            Field_complete_date = item.Field<DateTime?>("Field_complete_date"),
                            SentToClient_date = item.Field<DateTime?>("SentToClient_date"),
                            OfficeApproved_date = item.Field<DateTime?>("OfficeApproved_date"),

                            Client_Pay_Invoice_Id = item.Field<Int64?>("Client_Pay_Invoice_Id"),
                            Client_Invoice_Number = item.Field<String>("Client_Invoice_Number"),
                            Client_InvoiceDate = item.Field<DateTime?>("Client_InvoiceDate"),
                            Client_InvoicePaid_Date = item.Field<DateTime?>("Client_InvoicePaid_Date"),
                            Client_InvoiceTotal = item.Field<Decimal?>("Client_InvoiceTotal"),                            
                            Client_InvoicePaid = item.Field<Decimal?>("Client_InvoicePaid"),
                            
                            Con_Pay_Invoice_Id = item.Field<Int64?>("Con_Pay_Invoice_Id"),
                            Con_Invoice_Number = item.Field<String>("Con_Invoice_Number"),
                            Con_InvoiceDate = item.Field<DateTime?>("Con_InvoiceDate"),
                            Con_InvoiceTotal = item.Field<Decimal?>("Con_InvoiceTotal"),
                            Con_InvoicePaid_Date = item.Field<DateTime?>("Con_InvoicePaid_Date"),
                            Con_InvoicePaid = item.Field<Decimal?>("Con_InvoicePaid"),

                            ContractorName = item.Field<String>("ContractorName"),
                            ProcessorName = item.Field<String>("ProcessorName"),
                            CordinatorName = item.Field<String>("CordinatorName"),


                            Client_Company_Name = item.Field<String>("Client_Company_Name"),
                            IPL_StateName = item.Field<String>("IPL_StateName"),
                            WT_WorkType = item.Field<String>("WT_WorkType"),
                            CategoryName = item.Field<String>("CategoryName"),
                            RowCheckBox = false,
                            RowAmount = item.Field<Decimal?>("Client_InvoiceTotal"),
                            RowComment = "",
                            // IsActive = item.Field<Boolean?>("IsActive"),                           

                        }).ToList();

                    foreach (var rp in ReportDetails)
                    {
                        if (model.IsClientCheck)
                        {
                            rp.Client_InvoiceTotal = rp.Client_InvoiceTotal == null ? 0: rp.Client_InvoiceTotal;
                            rp.Client_InvoicePaid = rp.Client_InvoicePaid == null ? 0 : rp.Client_InvoicePaid;
                            rp.RowAmount = rp.Client_InvoiceTotal - rp.Client_InvoicePaid;
                        }
                        else
                        {
                            rp.Con_InvoiceTotal = rp.Con_InvoiceTotal == null ? 0 : rp.Con_InvoiceTotal;
                            rp.Con_InvoicePaid = rp.Con_InvoicePaid == null ? 0 : rp.Con_InvoicePaid;
                            rp.RowAmount = rp.Con_InvoiceTotal - rp.Con_InvoicePaid;
                        }                        
                    }

                    objDynamic.Add(ReportDetails);
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objDynamic;
        }

        public static byte[] GenerateRuntimePDF(string html)
        {
            #region NReco.PdfGenerator
            HtmlToPdfConverter nRecohtmltoPdfObj = new HtmlToPdfConverter();
            return nRecohtmltoPdfObj.GeneratePdf(html);
            #endregion

        }
        public async Task<CustomReportPDF> GeneratePDFStringreportdetails(ReportMasterDTO model)
        {
            CustomReportPDF custom = new CustomReportPDF();
            try
            {
                StringBuilder html = new StringBuilder();
                StringBuilder table = new StringBuilder();
                string theader = "";
                string tfooter = "";
                int index = 0;
                string tbody = "<tbody>";

             
                if (model.IsClientCheck)
                {
                    var reportList = pdfGetReportDetails(model);
                    theader += "<table width = '100%' border='1' cellpadding='1' cellspacing='0' bgcolor='#efefef'>";
                    table.Append(theader);
                    tbody += "<tr> <td  bgcolor = '#CCCCCC' style='text-align:center;height:50px;  border: none;'><span class='MsoNormal1' style='text-align:center; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px'><b><u> </u></b></span>  <div align='center;' style='font - family: Verdana, Arial, Helvetica, sans - serif; font - size: 10px; '>    <span class='MsoNormal1' style='text - align:center; font - size: 35px'><b><u> Report Details</u></b></span></div> </td></tr>";
                    tbody += "<tr><td ><table width='100%'  border='0'> <tr height ='30px'><td width='10%' bgcolor='#CCCCCC' style='font-size: 12px'><div align='center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'>&nbsp;Sr No </span></b></span></div><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'></span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align='center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'>&nbsp;Client Invoice </span></b></span></div><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align='center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;' > &nbsp; Client Invoice Date </span ></b ></span ></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; IPL#</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td>  <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Contractor</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Address</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td><td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; City</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; State</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td><td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Zip</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; WO</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Work Type</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td><td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp;Due Date</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td><td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Client Paid Date</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp;Client Paid</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp;Client Total</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td>     </tr>";

                    foreach (var item in reportList)
                    {
                        var sr = 1 + index++;
                     
                        tbody += "<tr height ='30px'> <td style = 'text-align: center;'width='10%'>" + sr + "</td ><td style = 'text-align: center;'>" + item.Client_Invoice_Number + "</td ><td style = 'text-align: center;'>" + String.Format("{0:dd/MM/yyyy}", item.Client_InvoiceDate) + "</td><td style = 'text-align: center;'>" + item.IPLNO + "</td><td style = 'text-align: center;'>" + item.ContractorName + "%" + "</td><td style = 'text-align: center;'>" + item.address1 + "</td><td style = 'text-align: center;'>" + item.city + "</td><td style = 'text-align: center;'>" + item.IPL_StateName + "</td><td style = 'text-align: center;'>" + item.zip + "</td><td style = 'text-align: center;'>" + item.workOrderNumber + "</td><td style = 'text-align: center;'>" + item.WT_WorkType + "</td><td style = 'text-align: center;'>" + String.Format("{0:dd/MM/yyyy}", item.dueDate) + "</td><td style = 'text-align: center;'>" + String.Format("{0:dd/MM/yyyy}", item.Client_InvoicePaid_Date)  + "</td><td style = 'text-align: center;'>" + item.Client_InvoicePaid + "</td><td style = 'text-align: center;'>" + String.Format("{0:0.##}", item.Client_InvoiceTotal) + "</td></tr>";



                    }
                    tbody += "</table></td></tr>";

                    tfooter += "</table >";
                    tbody += "</tbody>";
                    table.Append(tbody);
                }
                else
                {
                    var reportList = pdfGetReportDetails(model);
                    theader += "<table width = '100%' border='1' cellpadding='1' cellspacing='0' bgcolor='#efefef'>";
                    table.Append(theader);
                    tbody += "<tr> <td  bgcolor = '#CCCCCC' style='text-align:center;height:50px;  border: none;'><span class='MsoNormal1' style='text-align:center; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px'><b><u> </u></b></span>  <div align='center;' style='font - family: Verdana, Arial, Helvetica, sans - serif; font - size: 10px; '>    <span class='MsoNormal1' style='text - align:center; font - size: 35px'><b><u> Report Details</u></b></span></div> </td></tr>";
                    tbody += "<tr><td ><table width='100%'  border='0'> <tr height ='30px'><td width='10%' bgcolor='#CCCCCC' style='font-size: 12px'><div align='center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'>&nbsp;Sr No </span></b></span></div><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'></span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align='center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'>&nbsp;Contractor Invoice </span></b></span></div><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align='center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;' > &nbsp; Contractor Invoice Date </span ></b ></span ></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; IPL#</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td>  <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Contractor</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Address</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td><td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; City</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; State</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td><td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Zip</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; WO</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Work Type</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td><td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp;Due Date</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td><td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Contractor Paid Date</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp;Contractor Paid</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp;Contractor Total</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td>     </tr>";

                    foreach (var item in reportList)
                    {
                        var sr = 1 + index++;
                        tbody += "<tr height ='30px;margin-bottom=0px;'> <td style = 'text-align: center;'width='10%;margin-bottom=0px;'>" + sr + "</td ><td style = 'text-align: center;margin-bottom=0px;'>" + item.Con_Invoice_Number + "</td ><td style = 'text-align: center;margin-bottom=0px;'>" + String.Format("{0:dd/MM/yyyy}", item.Con_InvoiceDate)  + "</td><td style = 'text-align: center;margin-bottom=0px;'>" + item.IPLNO + "</td><td style = 'text-align: center;margin-bottom=0px;'>" + item.ContractorName + "%" + "</td><td style = 'text-align: center;margin-bottom=0px;'>" + item.address1 + "</td><td style = 'text-align: center;margin-bottom=0px;'>" + item.city + "</td><td style = 'text-align: center;margin-bottom=0px;'>" + item.IPL_StateName + "</td><td style = 'text-align: center;margin-bottom=0px;'>" + item.zip + "</td><td style = 'text-align: center;margin-bottom=0px;'>" + item.workOrderNumber + "</td><td style = 'text-align: center;margin-bottom=0px;'>" + item.WT_WorkType + "</td><td style = 'text-align: center;margin-bottom=0px;'>" + String.Format("{0:dd/MM/yyyy}", item.dueDate)  + "</td><td style = 'text-align: center;margin-bottom=0px;'>" + String.Format("{0:dd/MM/yyyy}", item.Con_InvoicePaid_Date)  + "</td><td style = 'text-align: center;margin-bottom=0px;'>" + item.Con_InvoicePaid + "</td><td style = 'text-align: center;margin-bottom=0px;'>" + String.Format("{0:0.##}", item.Con_InvoiceTotal) + "</td></tr>";



                    }
                    tbody += "</table></td></tr>";

                    tfooter += "</table >";
                    tbody += "</tbody>";
                    table.Append(tbody);
                }
               
            
                html.Append(table.ToString());
                byte[] pdffile = GenerateRuntimePDF(html.ToString());
                custom.Data = pdffile;
                custom.HttpStatusCode = System.Net.HttpStatusCode.OK;
                return custom;
            }
            catch (Exception ex)
            {
                custom.Message = ex.Message;
                custom.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                return custom;
            }
        }

        public List<ReportMasterDTO> pdfGetReportDetails(ReportMasterDTO model)
        {
            string wherecondition = string.Empty;
            string company = null; // Added By Dipali
            string customer = null;
            string contractor = null;
            string admin = null;
            string category = null;
            string state = null;

            var Data = JsonConvert.DeserializeObject<ReportMasterDTO>(model.whereClause);
            try
            {

                if (Data.InvoiceDateFrom != null && Data.InvoiceDateTo != null)
                {
                    if (model.IsClientCheck && model.IsContractorCheck)
                    {
                        wherecondition = " And ((CAST(clientInv.Inv_Client_CreatedOn as date) >=   CONVERT(date,'" + Data.InvoiceDateFrom.Value.ToString("yyyy-MM-dd") + "')  And CAST(clientInv.Inv_Client_CreatedOn as date) <=   CONVERT(date,'" + Data.InvoiceDateTo.Value.ToString("yyyy-MM-dd") + "')) or (CAST(inv.Inv_Con_CreatedOn as date) >= CONVERT(date,'" + Data.InvoiceDateFrom.Value.ToString("yyyy-MM-dd") + "') And CAST(inv.Inv_Con_CreatedOn as date) <=   CONVERT(date,'" + Data.InvoiceDateTo.Value.ToString("yyyy-MM-dd") + "')))";
                    }
                    else if (model.IsClientCheck)
                    {
                        wherecondition = " And CAST(clientInv.Inv_Client_CreatedOn as date) >=   CONVERT(date,'" + Data.InvoiceDateFrom.Value.ToString("yyyy-MM-dd") + "')  And CAST(clientInv.Inv_Client_CreatedOn as date) <=   CONVERT(date,'" + Data.InvoiceDateTo.Value.ToString("yyyy-MM-dd") + "')";
                    }
                    else
                    {
                        wherecondition = " And CAST(inv.Inv_Con_CreatedOn as date) >=   CONVERT(date,'" + Data.InvoiceDateFrom.Value.ToString("yyyy-MM-dd") + "')  And CAST(inv.Inv_Con_CreatedOn as date) <=   CONVERT(date,'" + Data.InvoiceDateTo.Value.ToString("yyyy-MM-dd") + "')";
                    }
                    //switch (model.Valtype)
                    //{
                    //    case 1:
                    //        {
                    //            wherecondition = " And CAST(inv.Inv_Client_CreatedOn as date) >=   CONVERT(date,'" + Data.InvoiceDateFrom.Value.ToString("yyyy-MM-dd") + "')  And CAST(inv.Inv_Client_CreatedOn as date) <=   CONVERT(date,'" + Data.InvoiceDateTo.Value.ToString("yyyy-MM-dd") + "')";
                    //            break;
                    //        }
                    //    case 2:
                    //        {
                    //            wherecondition = " And CAST(inv.Inv_Con_CreatedOn as date) >=   CONVERT(date,'" + Data.InvoiceDateFrom.Value.ToString("yyyy-MM-dd") + "')  And CAST(inv.Inv_Con_CreatedOn as date) <=   CONVERT(date,'" + Data.InvoiceDateTo.Value.ToString("yyyy-MM-dd") + "')";
                    //            break;
                    //        }
                    //}    
                }

                if (Data.ReadyOfficeDateFrom != null && Data.ReadyOfficeDateTo != null)
                {
                    // DateTime ReadyOfficeDateTo = Convert.ToDateTime(Data.ReadyOfficeDateTo).AddDays(1);
                    wherecondition = " And CAST(work.Field_complete_date as date) >=   CONVERT(date,'" + Data.ReadyOfficeDateFrom.Value.ToString("yyyy-MM-dd") + "')  And CAST(work.Field_complete_date as date) <=   CONVERT(date,'" + Data.ReadyOfficeDateTo.Value.ToString("yyyy-MM-dd") + "')";
                }
                if (Data.SentToClientDateFrom != null && Data.SentToClientDateTo != null)
                {
                    // DateTime SentToClientDateTo = Convert.ToDateTime(Data.SentToClientDateTo).AddDays(1);
                    wherecondition = " And CAST(work.SentToClient_date as date) >=   CONVERT(date,'" + Data.SentToClientDateFrom.Value.ToString("yyyy-MM-dd") + "')  And CAST(work.SentToClient_date as date) <=   CONVERT(date,'" + Data.SentToClientDateTo.Value.ToString("yyyy-MM-dd") + "')";
                }
                if (Data.CompletedDateFrom != null && Data.CompletedDateTo != null)
                {
                    wherecondition = " And CAST(work.Complete_Date as date) >=   CONVERT(date,'" + Data.CompletedDateFrom.Value.ToString("yyyy-MM-dd") + "')  And CAST(work.Complete_Date as date) <=   CONVERT(date,'" + Data.CompletedDateTo.Value.ToString("yyyy-MM-dd") + "')";
                }
                if (Data.CreatedDateFrom != null && Data.CreatedDateTo != null)
                {
                    //wherecondition = " And CAST(work.CreatedOn as date) >=   CONVERT(date,'" + Data.CreatedDateFrom.Value.ToString("yyyy-MM-dd") + "')  And CAST(work.CreatedOn as date) <=   CONVERT(date,'" + Data.CreatedDateTo.Value.ToString("yyyy-MM-dd") + "')";
                    wherecondition = " And CAST(work.DateCreated as date) >=   CONVERT(date,'" + Data.CreatedDateFrom.Value.ToString("yyyy-MM-dd") + "')  And CAST(work.DateCreated as date) <=   CONVERT(date,'" + Data.CreatedDateTo.Value.ToString("yyyy-MM-dd") + "')";
                }
                if (Data.OfficeApproveDateFrom != null && Data.OfficeApproveDateTo != null)
                {
                    wherecondition = " And CAST(work.OfficeApproved_date as date) >=   CONVERT(date,'" + Data.OfficeApproveDateFrom.Value.ToString("yyyy-MM-dd") + "')  And CAST(work.OfficeApproved_date as date) <=   CONVERT(date,'" + Data.OfficeApproveDateTo.Value.ToString("yyyy-MM-dd") + "')";
                }
                if (Data.ClientCheckDateFrom != null && Data.ClientCheckDateTo != null)
                {
                    if (model.IsClientCheck && model.IsContractorCheck)
                    {
                        wherecondition = " And ((CAST((select max(Client_Pay_Payment_Date) from [dbo].[Client_Invoice_Patyment] where Client_Pay_Wo_Id = work.[workOrder_ID] and Client_Pay_IsActive = 1 GROUP BY Client_Pay_Wo_Id) as date) >=   CONVERT(date,'" + Data.ClientCheckDateFrom.Value.ToString("yyyy-MM-dd") + "') And CAST((select max(Client_Pay_Payment_Date) from [dbo].[Client_Invoice_Patyment] where Client_Pay_Wo_Id = work.[workOrder_ID] and Client_Pay_IsActive = 1 GROUP BY Client_Pay_Wo_Id) as date) <=   CONVERT(date,'" + Data.ClientCheckDateTo.Value.ToString("yyyy-MM-dd") + "')) or (CAST((select max(Con_Pay_Payment_Date) from [dbo].Contractor_Invoice_Payment where Con_Pay_Wo_Id = work.[workOrder_ID] and Con_Pay_IsActive = 1 GROUP BY Con_Pay_Wo_Id) as date) >= CONVERT(date,'" + Data.ClientCheckDateFrom.Value.ToString("yyyy-MM-dd") + "') And CAST((select max(Con_Pay_Payment_Date) from [dbo].Contractor_Invoice_Payment where Con_Pay_Wo_Id = work.[workOrder_ID] and Con_Pay_IsActive = 1 GROUP BY Con_Pay_Wo_Id) as date) <=   CONVERT(date,'" + Data.ClientCheckDateTo.Value.ToString("yyyy-MM-dd") + "')))";
                    }
                    else if (model.IsClientCheck)
                    {
                        wherecondition = " And CAST((select max(Client_Pay_Payment_Date) from [dbo].[Client_Invoice_Patyment] where Client_Pay_Wo_Id = work.[workOrder_ID] and Client_Pay_IsActive = 1 GROUP BY Client_Pay_Wo_Id) as date) >=  CONVERT(date,'" + Data.ClientCheckDateFrom.Value.ToString("yyyy-MM-dd") + "')  And CAST((select max(Client_Pay_Payment_Date) from [dbo].[Client_Invoice_Patyment] where Client_Pay_Wo_Id = work.[workOrder_ID] and Client_Pay_IsActive = 1 GROUP BY Client_Pay_Wo_Id) as date) <=  CONVERT(date,'" + Data.ClientCheckDateTo.Value.ToString("yyyy-MM-dd") + "')";
                    }
                    else
                    {
                        wherecondition = " And CAST((select max(Con_Pay_Payment_Date) from [dbo].Contractor_Invoice_Payment where Con_Pay_Wo_Id = work.[workOrder_ID] and Con_Pay_IsActive = 1 GROUP BY Con_Pay_Wo_Id) as date) >=  CONVERT(date,'" + Data.ClientCheckDateFrom.Value.ToString("yyyy-MM-dd") + "')  And CAST((select max(Con_Pay_Payment_Date) from [dbo].Contractor_Invoice_Payment where Con_Pay_Wo_Id = work.[workOrder_ID] and Con_Pay_IsActive = 1 GROUP BY Con_Pay_Wo_Id) as date) <=  CONVERT(date,'" + Data.ClientCheckDateTo.Value.ToString("yyyy-MM-dd") + "')";
                    }
                }

                // Added By Dipali
                if (model != null && model.ReportAutoAssinArray != null && model.ReportAutoAssinArray.Count > 0)
                {
                    for (int i = 0; i < model.ReportAutoAssinArray.Count; i++)
                    {
                        if (model.ReportAutoAssinArray[i].Task_sett_Company != null)
                        {
                            for (int j = 0; j < model.ReportAutoAssinArray[i].Task_sett_Company.Count; j++)
                            {
                                company = company == null ? model.ReportAutoAssinArray[i].Task_sett_Company[j].Client_pkeyID.ToString() : company + "," + model.ReportAutoAssinArray[i].Task_sett_Company[j].Client_pkeyID.ToString();
                            }
                        }
                        if (model.ReportAutoAssinArray[i].Task_sett_Customer != null)
                        {
                            for (int k = 0; k < model.ReportAutoAssinArray[i].Task_sett_Customer.Count; k++)
                            {
                                customer = customer == null ? model.ReportAutoAssinArray[i].Task_sett_Customer[k].Cust_Num_pkeyId.ToString() : customer + "," + model.ReportAutoAssinArray[i].Task_sett_Customer[k].Cust_Num_pkeyId.ToString();
                            }
                        }
                        if (model.ReportAutoAssinArray[i].Task_sett_Contractor != null)
                        {
                            for (int m = 0; m < model.ReportAutoAssinArray[i].Task_sett_Contractor.Count; m++)
                            {
                                contractor = contractor == null ? model.ReportAutoAssinArray[i].Task_sett_Contractor[m].User_pkeyID.ToString() : contractor + "," + model.ReportAutoAssinArray[i].Task_sett_Contractor[m].User_pkeyID.ToString();
                            }
                        }
                        if (model.ReportAutoAssinArray[i].Task_sett_Admin != null)
                        {
                            for (int n = 0; n < model.ReportAutoAssinArray[i].Task_sett_Admin.Count; n++)
                            {
                                admin = admin == null ? model.ReportAutoAssinArray[i].Task_sett_Admin[n].User_pkeyID.ToString() : admin + "," + model.ReportAutoAssinArray[i].Task_sett_Admin[n].User_pkeyID.ToString();

                            }
                        }
                        if (model.ReportAutoAssinArray[i].Task_sett_Category != null)
                        {
                            for (int p = 0; p < model.ReportAutoAssinArray[i].Task_sett_Category.Count; p++)
                            {
                                category = category == null ? model.ReportAutoAssinArray[i].Task_sett_Category[p].Cat_ID.ToString() : category + "," + model.ReportAutoAssinArray[i].Task_sett_Category[p].Cat_ID.ToString();
                            }
                        }
                        if (model.ReportAutoAssinArray[i].Task_sett_State != null)
                        {
                            for (int q = 0; q < model.ReportAutoAssinArray[i].Task_sett_State.Count; q++)
                            {
                                state = state == null ? model.ReportAutoAssinArray[i].Task_sett_State[q].IPL_StateID.ToString() : state + "," + model.ReportAutoAssinArray[i].Task_sett_State[q].IPL_StateID.ToString();
                            }
                        }
                    }
                }

                if (!string.IsNullOrEmpty(company))
                {
                    wherecondition = wherecondition != null ? wherecondition + " And work.Company IN(" + company + ")" : " And work.Company IN(" + company + ")";
                }
                if (!string.IsNullOrEmpty(customer))
                {
                    wherecondition = wherecondition != null ? wherecondition + " And work.Customer_Number IN(" + customer + ")" : " And work.Customer_Number IN(" + customer + ")";
                }
                if (!string.IsNullOrEmpty(contractor))
                {
                    wherecondition = wherecondition != null ? wherecondition + " And work.Contractor IN(" + contractor + ")" : " And work.Contractor IN(" + contractor + ")";
                }
                if (!string.IsNullOrEmpty(admin))
                {
                    wherecondition = wherecondition != null ? wherecondition + " And work.Processor IN(" + admin + ")" : " And work.Processor IN(" + admin + ")";
                }
                if (!string.IsNullOrEmpty(category))
                {
                    wherecondition = wherecondition != null ? wherecondition + " And work.Category IN(" + category + ")" : " And work.Category IN(" + category + ")";
                }
                if (!string.IsNullOrEmpty(state))
                {
                    wherecondition = wherecondition != null ? wherecondition + " And work.state IN(" + state + ")" : " And work.state IN(" + state + ")";
                }
                if (!string.IsNullOrEmpty(model.InvoiceRangeStart))
                {
                    wherecondition = wherecondition != null ? wherecondition + " And IPLNO >= " + model.InvoiceRangeStart : " And IPLNO >= " + model.InvoiceRangeStart;
                }
                if (!string.IsNullOrEmpty(model.InvoiceRangeEnd))
                {
                    wherecondition = wherecondition != null ? wherecondition + " And IPLNO <= " + model.InvoiceRangeEnd : " And IPLNO <= " + model.InvoiceRangeEnd;
                }
                if (!string.IsNullOrEmpty(model.ClientInvoiceRangeStart) && model.Valtype == 1)
                {
                    wherecondition = wherecondition != null ? wherecondition + " And clientInv.Inv_Client_Invoice_Number >= '" + model.ClientInvoiceRangeStart + "' " : "  And clientInv.Inv_Client_Invoice_Number >=  '" + model.ClientInvoiceRangeStart + "'";
                }
                if (!string.IsNullOrEmpty(model.ClientInvoiceRangeEnd) && model.Valtype == 1)
                {
                    wherecondition = wherecondition != null ? wherecondition + " And clientInv.Inv_Client_Invoice_Number <= '" + model.ClientInvoiceRangeEnd + "' " : " And clientInv.Inv_Client_Invoice_Number <= '" + model.ClientInvoiceRangeEnd + "'";
                }

                if (!string.IsNullOrEmpty(model.ContractorInvoiceRangeStart) && model.Valtype == 1)
                {
                    wherecondition = wherecondition != null ? wherecondition + " And inv.Inv_Con_Invoce_Num >= '" + model.ContractorInvoiceRangeStart + "' " : " And inv.Inv_Con_Invoce_Num >= '" + model.ContractorInvoiceRangeStart + "'";
                }
                if (!string.IsNullOrEmpty(model.ContractorInvoiceRangeEnd) && model.Valtype == 1)
                {
                    wherecondition = wherecondition != null ? wherecondition + " And inv.Inv_Con_Invoce_Num <= '" + model.ContractorInvoiceRangeEnd + "' " : " And inv.Inv_Con_Invoce_Num <= '" + model.ContractorInvoiceRangeEnd + "'";
                }


                if (model.Inv_Con_Inv_Followup == true)
                {
                    // wherecondition = wherecondition != null ? wherecondition + " And inv.Inv_Con_Inv_Followup  = " + model.Inv_Con_Inv_Followup + "" : " And inv.Inv_Con_Inv_Followup  =" + model.Inv_Con_Inv_Followup + "";
                    wherecondition = wherecondition != null ? wherecondition + " And inv.Inv_Con_Inv_Followup  = 1 " : " And inv.Inv_Con_Inv_Followup  = 1";
                }

                if (model.Inv_Con_Inv_Approve == true)
                {
                    //wherecondition = wherecondition != null ? wherecondition + " And inv.Inv_Con_Inv_Approve  = " + model.Inv_Con_Inv_Approve + "" : " And inv.Inv_Con_Inv_Approve  =" + model.Inv_Con_Inv_Approve + "";
                    wherecondition = wherecondition != null ? wherecondition + " And inv.Inv_Con_Inv_Approve  = 1" : " And inv.Inv_Con_Inv_Approve  = 1";
                }
                if (model.Inv_Client_IsNoCharge == true)
                {
                    //wherecondition = wherecondition != null ? wherecondition + " And clientInv.Inv_Client_IsNoCharge  = " + model.Inv_Client_IsNoCharge + "" : " And clientInv.Inv_Client_IsNoCharge  =" + model.Inv_Client_IsNoCharge + "";
                    wherecondition = wherecondition != null ? wherecondition + " And clientInv.Inv_Client_IsNoCharge  = 1" : " And clientInv.Inv_Client_IsNoCharge  = 1";
                }

                ReportMasterDTO reportMasterDTO = new ReportMasterDTO();

                reportMasterDTO.Type = 2;
                reportMasterDTO.Valtype = model.Valtype;
                reportMasterDTO.whereClause = wherecondition;
                reportMasterDTO.UserId = model.UserId;
                DataSet ds = GetReportData(reportMasterDTO);

                if (ds != null && ds.Tables.Count > 0)
                {
                    var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                    List<ReportMasterDTO> ReportDetails =
                       (from item in myEnumerableFeaprd
                        select new ReportMasterDTO
                        {
                            workOrder_ID = item.Field<Int64>("workOrder_ID"),
                            IPLNO = item.Field<String>("IPLNO"),
                            workOrderNumber = item.Field<String>("workOrderNumber"),
                            Contractor = item.Field<Int64?>("Contractor"),
                            address1 = item.Field<String>("address1"),
                            city = item.Field<String>("city"),
                            state = item.Field<String>("state"),
                            zip = item.Field<Int64?>("zip"),
                            WorkType = item.Field<Int64?>("WorkType"),
                            dueDate = item.Field<DateTime?>("dueDate"),
                            Field_complete_date = item.Field<DateTime?>("Field_complete_date"),
                            SentToClient_date = item.Field<DateTime?>("SentToClient_date"),
                            OfficeApproved_date = item.Field<DateTime?>("OfficeApproved_date"),

                            Client_Pay_Invoice_Id = item.Field<Int64?>("Client_Pay_Invoice_Id"),
                            Client_Invoice_Number = item.Field<String>("Client_Invoice_Number"),
                            Client_InvoiceDate = item.Field<DateTime?>("Client_InvoiceDate"),
                            Client_InvoicePaid_Date = item.Field<DateTime?>("Client_InvoicePaid_Date"),
                            Client_InvoiceTotal = item.Field<Decimal?>("Client_InvoiceTotal"),
                            Client_InvoicePaid = item.Field<Decimal?>("Client_InvoicePaid"),

                            Con_Pay_Invoice_Id = item.Field<Int64?>("Con_Pay_Invoice_Id"),
                            Con_Invoice_Number = item.Field<String>("Con_Invoice_Number"),
                            Con_InvoiceDate = item.Field<DateTime?>("Con_InvoiceDate"),
                            Con_InvoiceTotal = item.Field<Decimal?>("Con_InvoiceTotal"),
                            Con_InvoicePaid_Date = item.Field<DateTime?>("Con_InvoicePaid_Date"),
                            Con_InvoicePaid = item.Field<Decimal?>("Con_InvoicePaid"),

                            ContractorName = item.Field<String>("ContractorName"),
                            ProcessorName = item.Field<String>("ProcessorName"),
                            CordinatorName = item.Field<String>("CordinatorName"),


                            Client_Company_Name = item.Field<String>("Client_Company_Name"),
                            IPL_StateName = item.Field<String>("IPL_StateName"),
                            WT_WorkType = item.Field<String>("WT_WorkType"),
                            CategoryName = item.Field<String>("CategoryName"),
                            RowCheckBox = false,
                            RowAmount = item.Field<Decimal?>("Client_InvoiceTotal"),
                            RowComment = "",
                            // IsActive = item.Field<Boolean?>("IsActive"),                           

                        }).ToList();

                    foreach (var rp in ReportDetails)
                    {
                        if (model.IsClientCheck)
                        {
                            rp.Client_InvoiceTotal = rp.Client_InvoiceTotal == null ? 0 : rp.Client_InvoiceTotal;
                            rp.Client_InvoicePaid = rp.Client_InvoicePaid == null ? 0 : rp.Client_InvoicePaid;
                            rp.RowAmount = rp.Client_InvoiceTotal - rp.Client_InvoicePaid;
                        }
                        else
                        {
                            rp.Con_InvoiceTotal = rp.Con_InvoiceTotal == null ? 0 : rp.Con_InvoiceTotal;
                            rp.Con_InvoicePaid = rp.Con_InvoicePaid == null ? 0 : rp.Con_InvoicePaid;
                            rp.RowAmount = rp.Con_InvoiceTotal - rp.Con_InvoicePaid;
                        }
                    }

                    return ReportDetails;
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return null;
        }
    }
}