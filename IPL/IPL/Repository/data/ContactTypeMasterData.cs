﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class ContactTypeMasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        public List<dynamic> AddUpdate_ContactTypeMaster(ContactTypeMasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_ContactTypeMaster]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@CT_PkeyId", 1 + "#bigint#" + model.CT_PkeyId);
                input_parameters.Add("@CT_Name", 1 + "#nvarchar#" + model.CT_Name);
                input_parameters.Add("@CT_IsActive", 1 + "#nvarchar#" + model.CT_IsActive);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserId);
                input_parameters.Add("@CT_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);
                objData = obj.SqlCRUD(insertProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        private DataSet GetContactTypeMaster(ContactTypeMasterDTO model)
        {
            DataSet ds = null;
            try
            {

                string selectProcedure = "[GetContactTypeMasters]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@CT_PkeyId", 1 + "#bigint#" + model.CT_PkeyId);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ds;
        }

        public List<dynamic> GetContactTypeMasterDetails(ContactTypeMasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetContactTypeMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<ContactTypeMasterDTO> Forms_Master_Files =
                   (from item in myEnumerableFeaprd
                    select new ContactTypeMasterDTO
                    {
                        CT_PkeyId = item.Field<Int64>("CT_PkeyId"),
                        CT_Name = item.Field<string>("CT_Name"),
                        CT_IsActive = item.Field<bool>("CT_IsActive"),
                        CT_CreatedBy = item.Field<string>("CT_CreatedBy"),
                        CT_ModifiedBy = item.Field<string>("CT_ModifiedBy"),

                    }).ToList();

                objDynamic.Add(Forms_Master_Files);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}