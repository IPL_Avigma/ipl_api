﻿using Avigma.Repository.Lib;
using Firebase.Database;
using Firebase.Database.Query;
using IPL.Models;
using IPLApp.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;

namespace IPL.Repository.data
{
    public class LiveUserLocationData
    {
        Log log = new Log();
        MyDataSourceFactory obj = new MyDataSourceFactory();

        public async Task<List<dynamic>> GetFirebaseLocation(FirebaseUserDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                string firebaseUrl = System.Configuration.ConfigurationManager.AppSettings["FirebaseUrl"];

                var firebaseClient = new FirebaseClient(firebaseUrl);

                var userDetail = GetFirebaseUser(model);
                if (model.UserID > 0 && userDetail != null && userDetail.LoggedUserDetail != null && userDetail.LoggedUserDetail.Count > 0)
                {
                    var objData = await firebaseClient                    
                    .Child("locations")
                    .Child(userDetail.LoggedUserDetail[0].IPL_Company_ID)
                    .OnceAsync<dynamic>();

                    var locationList = new List<LiveUserLocationDTO>();
                    foreach (var location in objData)
                    {
                        LiveUserLocationDTO liveUserLocationDTO = new LiveUserLocationDTO();
                        liveUserLocationDTO.Name = location.Key;
                        locationList.Add(liveUserLocationDTO);
                    }
                    objDynamic.Add(locationList);
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objDynamic;
        }

        public async Task<List<dynamic>> DeleteFirebaseLocation(LiveUserLocationDTO liveUserLocationDTO)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                string firebaseUrl = System.Configuration.ConfigurationManager.AppSettings["FirebaseUrl"];

                var firebaseClient = new FirebaseClient(firebaseUrl);
                FirebaseUserDTO model = new FirebaseUserDTO { UserID = liveUserLocationDTO.UserID, Type = 1 };

                var userDetail = GetFirebaseUser(model);
                if (liveUserLocationDTO.UserID > 0 && userDetail != null && userDetail.LoggedUserDetail != null && userDetail.LoggedUserDetail.Count > 0)
                {
                    await firebaseClient                    
                     .Child("locations")
                     .Child(userDetail.LoggedUserDetail[0].IPL_Company_ID)
                     .Child(liveUserLocationDTO.Name)
                     .DeleteAsync();

                    objDynamic.Add(liveUserLocationDTO);
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objDynamic;
        }

        public async Task<List<dynamic>> DeleteFirebaseLocationList(List<LiveUserLocationDTO> liveUserLocationDTO, Int64 userId)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                string firebaseUrl = System.Configuration.ConfigurationManager.AppSettings["FirebaseUrl"];

                var firebaseClient = new FirebaseClient(firebaseUrl);
                FirebaseUserDTO model = new FirebaseUserDTO { UserID = userId, Type = 1 };

                var userDetail = GetFirebaseUser(model);
                if (userId > 0 && userDetail != null && userDetail.LoggedUserDetail != null && userDetail.LoggedUserDetail.Count > 0)
                {
                    foreach (var liveUserLocation in liveUserLocationDTO)
                    {
                        await firebaseClient                         
                         .Child("locations")
                         .Child(userDetail.LoggedUserDetail[0].IPL_Company_ID)
                         .Child(liveUserLocation.Name)
                         .DeleteAsync();
                    }

                    objDynamic.Add(liveUserLocationDTO);
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objDynamic;
        }

        public UserDetailListDTO GetFirebaseUser(FirebaseUserDTO model)
        {
            DataSet ds = null;
            UserDetailListDTO userDetailListDTO = new UserDetailListDTO();
            model.Type = 1;
            try
            {
                string selectProcedure = "[Get_Firebase_User]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@WhereClause", 1 + "#varchar#" + model.WhereClause);

                ds = obj.SelectSql(selectProcedure, input_parameters);

                if (ds.Tables.Count > 0)
                {
                    var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();

                    List<UserDetailDTO> userList =
                       (from item in myEnumerableFeaprd
                        select new UserDetailDTO
                        {
                            User_LoginName = item.Field<String>("User_LoginName"),
                            UserName = item.Field<String>("UserName"),
                            IPL_Company_ID = item.Field<String>("IPL_Company_ID"),
                            GroupRoleId = item.Field<Int64?>("GroupRoleId"),
                        }).ToList();

                    userDetailListDTO.LoggedUserDetail = userList;
                }
                return userDetailListDTO;
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return null;
        }

        public async Task<List<dynamic>> GetFirebaseRouteLocation(WorkoderActionItems model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                var Data = JsonConvert.DeserializeObject<List<WorkOrderIDItems>>(model.Arr_WorkOrderID);
                string strwhere = string.Empty;
                for (int i = 0; i < Data.Count; i++)
                {
                    strwhere = !string.IsNullOrEmpty(strwhere) ? strwhere + "," + Data[i].WorkOrderID.ToString()  : Data[i].WorkOrderID.ToString();
                }
                if (!string.IsNullOrEmpty(strwhere))
                {
                    strwhere = "and u.User_pkeyID IN (select ISNULL(Contractor,0) from WorkOrderMaster Where workOrder_Id IN(" + strwhere + "))";
                }
                string firebaseUrl = System.Configuration.ConfigurationManager.AppSettings["FirebaseUrl"];

                var firebaseClient = new FirebaseClient(firebaseUrl);
                FirebaseUserDTO firebaseUserDTO = new FirebaseUserDTO();
                firebaseUserDTO.UserID = model.UserId;
                firebaseUserDTO.WhereClause = strwhere;

                var userDetail = GetFirebaseUser(firebaseUserDTO);
                if (firebaseUserDTO.UserID > 0 && userDetail != null && userDetail.LoggedUserDetail != null && userDetail.LoggedUserDetail.Count > 0)
                {
                    var objData = await firebaseClient
                    .Child("locations")
                    .Child(userDetail.LoggedUserDetail[0].IPL_Company_ID)
                    .OnceAsync<dynamic>();

                    var conList = GetFirebaseUserList(firebaseUserDTO);

                    foreach (var location in objData)
                    {
                        var locUser = conList.FirstOrDefault(w => w.User_LoginName == location.Key);
                        if (locUser != null)
                        {
                            var json = JsonConvert.SerializeObject(location.Object);
                            var c = JsonConvert.DeserializeObject<LiveUserLocationRouteDTO>(json);
                            objDynamic.Add(c);
                        }
                    }                   
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objDynamic;
        }
        public List<UserDetailDTO> GetFirebaseUserList(FirebaseUserDTO model)
        {
            DataSet ds = null;
            model.Type = 2;
            try
            {
                string selectProcedure = "[Get_Firebase_User]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@WhereClause", 1 + "#varchar#" + model.WhereClause);

                ds = obj.SelectSql(selectProcedure, input_parameters);

                if (ds.Tables.Count > 0)
                {
                    var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();

                    List<UserDetailDTO> userList =
                       (from item in myEnumerableFeaprd
                        select new UserDetailDTO
                        {
                            User_LoginName = item.Field<String>("User_LoginName"),
                            UserName = item.Field<String>("UserName"),
                            IPL_Company_ID = item.Field<String>("IPL_Company_ID"),
                        }).ToList();

                    return userList;
                }                
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return null;
        }
    }
}