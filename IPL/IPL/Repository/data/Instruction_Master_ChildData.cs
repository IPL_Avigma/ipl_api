﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class Instruction_Master_ChildData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        public List<dynamic> AddInstructionChildData(Instruction_Master_ChildDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_InstructionMasterChild]";
            Instruction_Master_Child instruction_Master_Child = new Instruction_Master_Child();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Inst_Ch_pkeyId", 1 + "#bigint#" + model.Inst_Ch_pkeyId);
                input_parameters.Add("@Inst_Ch_Wo_Id", 1 + "#bigint#" + model.Inst_Ch_Wo_Id);
                input_parameters.Add("@Inst_Ch_Text", 1 + "#varchar#" + model.Inst_Ch_Text);
                input_parameters.Add("@Inst_Ch_Comand_Mobile", 1 + "#varchar#" + model.Instr_Comand_Mobile);
                input_parameters.Add("@Inst_Ch_IsActive", 1 + "#bit#" + model.Inst_Ch_IsActive);
                input_parameters.Add("@Inst_Ch_Delete", 1 + "#bit#" + model.Inst_Ch_Delete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Inst_Ch_pkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    instruction_Master_Child.Inst_Ch_pkeyId = "0";
                    instruction_Master_Child.Status = "0";
                    instruction_Master_Child.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    instruction_Master_Child.Inst_Ch_pkeyId = Convert.ToString(objData[0]);
                    instruction_Master_Child.Status = Convert.ToString(objData[1]);


                }
                objcltData.Add(instruction_Master_Child);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;



        }

        private DataSet GetInstructionChildMaster(Instruction_Master_ChildDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Instruction_Master_child]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Inst_Ch_pkeyId", 1 + "#bigint#" + model.Inst_Ch_pkeyId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetInstructionChildMasterDetails(Instruction_Master_ChildDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetInstructionChildMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Instruction_Master_ChildDTO> InstructionChildDetails =
                   (from item in myEnumerableFeaprd
                    select new Instruction_Master_ChildDTO
                    {
                        Inst_Ch_pkeyId = item.Field<Int64>("Inst_Ch_pkeyId"),
                        Inst_Ch_Wo_Id = item.Field<Int64?>("Inst_Ch_Wo_Id"),
                        Inst_Ch_Text = item.Field<String>("Inst_Ch_Text"),
                        Inst_Ch_Comand_Mobile = item.Field<String>("Inst_Ch_Comand_Mobile"),
                        Inst_Ch_IsActive = item.Field<Boolean?>("Inst_Ch_IsActive"),
                      



                    }).ToList();

                objDynamic.Add(InstructionChildDetails);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}