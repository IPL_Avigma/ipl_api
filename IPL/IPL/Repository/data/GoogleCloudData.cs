﻿using Google.Apis.Auth.OAuth2;
using Google.Cloud.Storage.V1;
using IPL.Models;
using IPLApp.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using Avigma.Repository.Lib;
using Avigma.Models;
using IPL.Models.FiveBrothers;

namespace IPL.Repository.data
{
    public class GoogleCloudData
    {
        Log log = new Log();
        public Task<List<dynamic>> CreateBucket(CreateBucketDTO createBucketDTO)
        {
            List<dynamic> obj = new List<dynamic>();
            // Your Google Cloud Platform project ID.
            string projectId = "rare-lambda-245821";
            //GoogleCredential credential = GoogleCredential.GetApplicationDefault();

            //Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", "E:\\office work\\IPL_Version_New\\IPL\\IPL\\iPreservation Live-c01a5b13cbb2.json");
            Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", "E:\\IPL_BACKEND_VER1.0.0.1\\IPL\\IPLiPreservation Live-c01a5b13cbb2.json");
            //  string strcredential = Environment.GetEnvironmentVariable("E:\\office work\\google_drive\\googlestorage\\iPreservation Live-c01a5b13cbb2.json");
            var credential = GoogleCredential.GetApplicationDefault();
            //var storageClient = StorageClient.Create(credential);



            // Instantiates a client.
            StorageClient storageClient = StorageClient.Create();


            // The name for the new bucket.
            string bucketName = projectId + createBucketDTO.BucketName;
            try
            {
                // Creates the new bucket.
                storageClient.CreateBucket(projectId, bucketName);

            }
            catch (Google.GoogleApiException e)
            when (e.Error.Code == 409)
            {
                // The bucket already exists.  That's fine.
                log.logErrorMessage(e.Message);
                log.logErrorMessage(e.StackTrace);
            }

            return null;
        }


        public object AuthImplicit(string projectId)
        {
            // If you don't specify credentials when constructing the client, the
            // client library will look for credentials in the environment.
            var credential = GoogleCredential.GetApplicationDefault();
            var storage = StorageClient.Create(credential);
            // Make an authenticated API request.
            var buckets = storage.ListBuckets(projectId);
            foreach (var bucket in buckets)
            {
                log.logInfoMessage(bucket.Name);
            }
            return null;
        }




        /// <summary>
        ///  post img to goolge cloud
        /// </summary>
        /// <returns></returns>

        public List<dynamic> UploadDocument(FileUploadBucketDTO fileUploadBucketDTO)
        {
            List<dynamic> obj = new List<dynamic>();
            try
            {

                string bucketName = fileUploadBucketDTO.BucketName;

                string sharedkeyFilePath = fileUploadBucketDTO.SharedKeyFilePath;
                GoogleCredential credential = null;
                using (var jsonStream = new FileStream(sharedkeyFilePath, FileMode.Open,
                    FileAccess.Read, FileShare.Read))
                {
                    credential = GoogleCredential.FromStream(jsonStream);
                }
                var storageClient = StorageClient.Create(credential);


                fileUploadBucketDTO.UploadFilePath = fileUploadBucketDTO.IPLNO + "/" + fileUploadBucketDTO.UploadFilePath;
                fileUploadBucketDTO.UploadFileType = "";

                using (var fileStream = new FileStream(fileUploadBucketDTO.filePath, FileMode.Open,
                    FileAccess.Read, FileShare.Read))
                {

                    storageClient.UploadObject(bucketName, fileUploadBucketDTO.UploadFilePath, fileUploadBucketDTO.UploadFileType, fileStream);
                }

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }


            return obj;
        }
        public string GetOAuthToken()
        {
            GoogleCredential googleCredential = null;
            return googleCredential.UnderlyingCredential.GetAccessTokenForRequestAsync("https://accounts.google.com/o/oauth2/auth").Result;
        }

        public async Task<string> UploadMediaToCloud(string filePath, string objectName, string bucketName)
        {
            string bearerToken = GetOAuthToken();
            string googleCloudStorageBaseUrl = "https://www.googleapis.com/upload/storage/v1/b/";
            byte[] fileBytes = File.ReadAllBytes(filePath);
            objectName = objectName ?? Path.GetFileName(filePath);

            var baseUrl = new Uri(string.Format(googleCloudStorageBaseUrl + "" + bucketName + "/o?uploadType=media&name=" + objectName + ""));

            using (WebClient client = new WebClient())
            {
                client.Headers.Add(HttpRequestHeader.Authorization, "Bearer " + bearerToken);
                client.Headers.Add(HttpRequestHeader.ContentType, "application/octet-stream");

                byte[] response = await Task.Run(() => client.UploadData(baseUrl, "POST", fileBytes));
                string responseInString = Encoding.UTF8.GetString(response);
                return responseInString;
            }
        }





        //download Bucket Image to local system
        public List<dynamic> DownloadObject(dynamic GetClientPhoto, DownloadImageDTO downloadImageDTO)
        {

            List<dynamic> obj = new List<dynamic>();
            List<dynamic> objReturn = new List<dynamic>();
            try
            {




                string jsonpath = System.Configuration.ConfigurationManager.AppSettings["GooglebucketJsom"];

                string SaveImgPath = System.Configuration.ConfigurationManager.AppSettings["GooglePhotosSave"];

                string BucketName = System.Configuration.ConfigurationManager.AppSettings["BucketName"];

                downloadImageDTO.BucketName = BucketName;

                Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", jsonpath);
                var credential = GoogleCredential.GetApplicationDefault();
                var storage = StorageClient.Create();

                string LocalPathSet = SaveImgPath + downloadImageDTO.IPLFolder;// IPL NO

                if (!Directory.Exists(LocalPathSet))
                {

                    Directory.CreateDirectory(LocalPathSet);
                }


                for (int i = 0; i < GetClientPhoto[0].Count; i++)
                {

                    Client_Result_PhotoDTO client_Result_PhotoDTO = new Client_Result_PhotoDTO();

                    string check = GetClientPhoto[0][i].Client_Result_Photo_FilePath;
                    string[] Name = check.Split('/');
                    string OnlyImgName = Name[3];

                    downloadImageDTO.objectName = downloadImageDTO.IPLFolder + "/" + OnlyImgName;

                    downloadImageDTO.localPath = LocalPathSet + "\\" + OnlyImgName;



                    downloadImageDTO.localPath = downloadImageDTO.localPath ?? Path.GetFileName(downloadImageDTO.objectName);
                    using (var outputFile = File.OpenWrite(downloadImageDTO.localPath))
                    {
                        storage.DownloadObject(downloadImageDTO.BucketName, downloadImageDTO.objectName, outputFile);

                        client_Result_PhotoDTO.Client_Result_Photo_FilePath = downloadImageDTO.localPath;
                        client_Result_PhotoDTO.Client_Result_Photo_ID = GetClientPhoto[0][i].Client_Result_Photo_ID;

                        obj.Add(client_Result_PhotoDTO);
                    }

                }

                objReturn.Add(obj);



                return objReturn;

            }
            catch (Exception ex)
            {
                objReturn.Add("Google Cloud Image not found");
                objReturn.Add(ex.Message);
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);

                return objReturn;

            }
        }

        // delete bucket 
        public List<dynamic> DeleteBucket(FileUploadBucketDTO model)
        {
            

            // var storage = StorageClient.Create();
            var credential = GoogleCredential.GetApplicationDefault();
            var storage = StorageClient.Create();
            storage.DeleteBucket(model.BucketName);
            //Console.WriteLine($"Deleted {bucketName}.");
            return null;
        }

        public String DeleteFolder(FileUploadBucketDTO model)
        {
            try
            {
                string jsonpath = System.Configuration.ConfigurationManager.AppSettings["GooglebucketJsom"];
                Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", jsonpath);
                // var storage = StorageClient.Create();
                var credential = GoogleCredential.GetApplicationDefault();
                var storage = StorageClient.Create();
                // List all objects in the folder
                foreach (var obj in storage.ListObjects(model.BucketName, model.FolderName))
                {
                    storage.DeleteObject(model.BucketName, obj.Name);
                   
                }


              
                return "1";
            }
            catch (Exception ex)
            {   
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                return "0";
            }
           
        }

        //create folder in backet
        public List<dynamic> AddFolder(CreateFolderDTO model)
        {
            string jsonpath = System.Configuration.ConfigurationManager.AppSettings["GooglebucketJsom"];
            Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", jsonpath);
        

            var credential = GoogleCredential.GetApplicationDefault();
            StorageClient storageClient = StorageClient.Create();
            if (!model.FolderName.EndsWith("/"))
                model.FolderName += "/";

            var content = Encoding.UTF8.GetBytes("");

            storageClient.UploadObject(model.BucketName, model.FolderName, "application/x-directory", new MemoryStream(content));
            //var uploadStream = new MemoryStream(Encoding.UTF8.GetBytes(""));
            //Storage.Objects.Insert(
            //    bucket: BucketName,
            //    stream: uploadStream,
            //    contentType: "application/x-directory",
            //    body: new Google.Apis.Storage.v1.Data.Object() { Name = model.FolderName }
            //    ).Upload();

            return null;
        }

        //get list of object 
        public List<dynamic> ListObjects(FileUploadBucketDTO model, int Flag)
        {
            List<dynamic> objdata = new List<dynamic>();
            try
            {

                log.logDebugMessage("----------------Bucket Called Start----------------");
                List<dynamic> workobjdata = new List<dynamic>();
                WorkOrderMaster_ImportAPI_Data workOrderMaster_ImportAPI_Data = new WorkOrderMaster_ImportAPI_Data();
                string jsonpath = System.Configuration.ConfigurationManager.AppSettings["GooglebucketJsom"];

                string SaveImgPath = System.Configuration.ConfigurationManager.AppSettings["GooglePhotosSave"];

                string BucketName = System.Configuration.ConfigurationManager.AppSettings["ImportBucketName"];
                string BucketFolderName = System.Configuration.ConfigurationManager.AppSettings["BucketFolderName"];
                string DestBucketFolderName = System.Configuration.ConfigurationManager.AppSettings["DestBucketFolderName"];

                string DestObjectName = string.Empty;
                model.BucketName = BucketName;
                model.objectName = BucketFolderName;

                Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", jsonpath);

                FileUploadDownload fileUploadDownload = new FileUploadDownload();
                FileUploadDownloadDTO fileUploadDownloadDTO;

              var credential = GoogleCredential.GetApplicationDefault();
                var storage = StorageClient.Create();
                var countlistobj = storage.ListObjects(model.BucketName, model.objectName);



                if (countlistobj != null)
                {
                    var val = countlistobj.Count().ToString();
                    log.logDebugMessage("---------------Scrapper Recrods Found in Bucket count----------------" + countlistobj.Count().ToString());
                }
                else
                {
                    log.logDebugMessage("----------------No Scrapper Recrods Found in Bucket----------------");
                }


                foreach (var storageObject in storage.ListObjects(model.BucketName, model.objectName))
                {
                    List<dynamic> objDynamic = new List<dynamic>();

                    if (storageObject.Name != model.objectName + "/")
                    {
                        if (storageObject.Name.Split('/').Length > 1)
                        {

                            string chkfilenmae = storageObject.Name;
                            bool chkjson = Regex.IsMatch(chkfilenmae, ".json");
                            if (chkjson)
                            {
                                var Filename = storageObject.Name.Split('/')[1];
                                string ppwnummber = Filename.Split('.')[0];
                                //string ppwname = "new/ ppw_67804	";


                                DestObjectName = DestBucketFolderName + "/" + Filename;
                                //model.localPath = SaveImgPath ?? Path.GetFileName(Filename);
                                model.localPath = SaveImgPath + Filename;
                                using (var outputFile = File.OpenWrite(model.localPath))
                                {

                                    storage.DownloadObject(model.BucketName, storageObject.Name, outputFile);
                                    outputFile.Close();
                                    outputFile.Dispose();

                                    storage.CopyObject(BucketName, storageObject.Name, BucketName, DestObjectName);
                                    #region FileAccess
                                    string strURL = System.Configuration.ConfigurationManager.AppSettings["IntegrationURL"] + "downloadFromBucket";
                                    string ReqData = "{\r\n \"FileName\": \"" + Filename + "\",\r\n \"FolderName\": \"" + DestBucketFolderName + "\"\r\n,\r\n \"BucketName\": \"" + BucketName + "\"\r\n}";
                                    string FilePath = fileUploadDownload.CallAPI(strURL, ReqData);
                                    #endregion


                                    //string path = "D:\\Projects\\PPW\\Code\\IPL_App\\IPL\\IPL\\Document\\JsonFile\\63588.json";
                                    long length = new FileInfo(model.localPath).Length;
                                    if (length > 0)
                                    {
                                        using (StreamReader sr = new StreamReader(model.localPath))
                                        //using (StreamReader sr = new StreamReader(path))
                                        {
                                            // Read the stream to a string, and write the string to the console.
                                            String Data = sr.ReadToEnd();
                                            Data = Data.Replace("Additional Instructions", "AdditionalInstructions");
                                            objdata.Add(Data);

                                            log.logDebugMessage("------------------------Json Data Start------------------------------------");
                                            log.logDebugMessage(Filename);
                                            log.logDebugMessage(Data);
                                            log.logDebugMessage("------------------------Json Data End------------------------------------");

                                            objDynamic = workOrderMaster_ImportAPI_Data.APIWorkOrderItemDetails(Data, model.WI_Pkey_ID, Filename, FilePath,model.UserID);
                                            workobjdata.Add(objDynamic);
                                            sr.Close();
                                            sr.Dispose();




                                        }
                                    }
                                    else
                                    {
                                        log.logDebugMessage("------------------------InValid File Size 0 -------------------" + Filename + "-----------------");
                                        //File.Delete(model.localPath);
                                    }


                                }
                                 
                                File.Delete(model.localPath);
                                try
                                {
                                    storage.DeleteObject(BucketName, storageObject.Name);
                                }
                                catch (Exception ex)
                                {
                                    log.logErrorMessage("---Error While deleting object---------------------");
                                    log.logErrorMessage(ex.Message);
                                    log.logErrorMessage(ex.StackTrace);
                                }
                               

                                #region Image Download code  
                                //Image Download code starts 

                                // string ImageFoldername = model.objectName + "/ppw_" + "67804"; /// for testing

                                if (model.image_download == true)
                                {

                                string ImageFoldername = model.objectName + "/ppw_" + ppwnummber;
                                string SubImageFoldername = "ppw_" + ppwnummber;
                                log.logDebugMessage("Image Folder Name SubImageFoldername -------->" + SubImageFoldername);
                                var Imagefolderlist = storage.ListObjects(model.BucketName, model.objectName);
                                string strPPWFoldername = string.Empty;

                                foreach (var storageObjectval in Imagefolderlist)
                                {
                                    if (storageObjectval.Name != model.objectName + "/")
                                    {
                                        var Filenamelist = storageObjectval.Name.Split('/');
                                        if (Filenamelist.Length > 1)
                                        {
                                            strPPWFoldername = storageObjectval.Name.Split('/')[1];
                                        }

                                        if (SubImageFoldername == strPPWFoldername)
                                        {
                                            ImageGenerator imageGenerator = new ImageGenerator();
                                            string vaal = storageObjectval.Name;
                                            string zipfilename = vaal.Split('/')[2];
                                            model.ImageZiplocalPath = SaveImgPath + zipfilename;
                                            if (File.Exists(model.ImageZiplocalPath))
                                            {
                                                File.Delete(model.ImageZiplocalPath);
                                            }
                                            using (var outputFile = File.OpenWrite(model.ImageZiplocalPath))
                                            {
                                                storage.DownloadObject(model.BucketName, vaal, outputFile);
                                                outputFile.Close();
                                                outputFile.Dispose();
                                            }


                                            string newdirectorypath = SaveImgPath + "ppw_" + ppwnummber;
                                            // string newdirectorypath = SaveImgPath + "ppw_67804";
                                            if (Directory.Exists(newdirectorypath))
                                            {
                                                Directory.Delete(newdirectorypath);
                                                log.logDebugMessage("Delete Folder ------------------->" + "ppw_" + ppwnummber);
                                            }

                                            Directory.CreateDirectory(newdirectorypath);

                                            fileUploadDownloadDTO = new FileUploadDownloadDTO();

                                            fileUploadDownloadDTO.zipPath = model.ImageZiplocalPath;
                                            fileUploadDownloadDTO.extractPath = newdirectorypath;

                                            fileUploadDownload.ExtractZipFile(fileUploadDownloadDTO);

                                            fileUploadDownloadDTO.importpkey = objDynamic[0];
                                            fileUploadDownloadDTO.WI_PkeyID = model.WI_Pkey_ID;
                                            fileUploadDownloadDTO.PPW_Number = ppwnummber;
                                            fileUploadDownloadDTO.DirectoryPath = newdirectorypath;

                                            fileUploadDownload.UploadPPWphotos(fileUploadDownloadDTO);
                                            string DelImageFoldername = model.objectName + "/ppw_" + ppwnummber + "/" + zipfilename;
                                            storage.DeleteObject(BucketName, DelImageFoldername);
                                        }


                                    }



                                }

                            }





                                #endregion
                                //Console.WriteLine(storageObject.Name);
                            }
                        }
                    }
                }
                int countworkorder = workobjdata.Count;
                string strworkOrder = string.Empty;

                //for (int i = 0; i < workobjdata.Count; i++)
                //{
                //    if (!string.IsNullOrEmpty(strworkOrder))
                //    {
                //        strworkOrder = strworkOrder + "," + workobjdata[i][0];
                //    }
                //    else
                //    {
                //        strworkOrder = workobjdata[i][0];
                //    }

                //}
                // The Code to insert need to be written here [Import_Queue_Transaction 

                Import_Queue_TransData import_Queue_TransData = new Import_Queue_TransData();
                Import_Queue_Trans_DTO import_Queue_Trans_DTO = new Import_Queue_Trans_DTO();
                import_Queue_Trans_DTO.Imrt_Wo_Import_ID = model.WI_Pkey_ID;
               
                //import_Queue_Trans_DTO.Imrt_Wo_ID = strworkOrder;
                import_Queue_Trans_DTO.Type = 1;
                import_Queue_Trans_DTO.Imrt_Import_From_ID = 1;
                if (Flag == 2)
                {
                    import_Queue_TransData.UpdateAutoImportQueueTransaction(import_Queue_Trans_DTO);
                }
                else
                {
                    import_Queue_Trans_DTO.UserID = model.UserID;
                    import_Queue_TransData.UpdateAutoImportQueueTransaction(import_Queue_Trans_DTO);
                   // import_Queue_TransData.AddUpdateImportQueueTransData(import_Queue_Trans_DTO);
                }
               
                log.logDebugMessage("----------------Bucket Called End----------------");


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);

            }
            return objdata;
        }


        public List<dynamic> ListPPWNewObjects(FileUploadBucketDTO model, int Flag)
        {
            List<dynamic> objdata = new List<dynamic>();
            try
            {

                log.logDebugMessage("----------------Bucket Called Start----------------");
                List<dynamic> workobjdata = new List<dynamic>();
                WorkOrderMaster_ImportAPI_Data workOrderMaster_ImportAPI_Data = new WorkOrderMaster_ImportAPI_Data();
                string jsonpath = System.Configuration.ConfigurationManager.AppSettings["GooglebucketJsom"];

                string SaveImgPath = System.Configuration.ConfigurationManager.AppSettings["GooglePhotosSave"];

              //  string BucketName = System.Configuration.ConfigurationManager.AppSettings["ImportBucketName"];
              //  string BucketFolderName = System.Configuration.ConfigurationManager.AppSettings["BucketFolderName"];
               // string DestBucketFolderName = System.Configuration.ConfigurationManager.AppSettings["DestBucketFolderName"];

                string BucketName = model.Import_BucketName;
                string BucketFolderName = model.Import_BucketFolderName;
                string DestBucketFolderName = model.Import_DestBucketFolderName;

                string DestObjectName = string.Empty;
                model.BucketName = BucketName;
                model.objectName = BucketFolderName;

                Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", jsonpath);

                FileUploadDownload fileUploadDownload = new FileUploadDownload();
                FileUploadDownloadDTO fileUploadDownloadDTO;

                var credential = GoogleCredential.GetApplicationDefault();
                var storage = StorageClient.Create();
                log.logDebugMessage("Bucket Name ---->" + model.BucketName+"----------?" + model.WI_Pkey_ID);
                if (string.IsNullOrEmpty(model.BucketName))
                {
                    log.logDebugMessage("Bucket Name is Null For" + model.WI_Pkey_ID);
                    log.logDebugMessage("Bucket Name is Null For Flag" + Flag);
                }

                var countlistobj = storage.ListObjects(model.BucketName, model.objectName);



                if (countlistobj != null)
                {
                    var val = countlistobj.Count().ToString();
                    log.logDebugMessage("---------------Scrapper Recrods Found in Bucket count----------------" + countlistobj.Count().ToString());
                }
                else
                {
                    log.logDebugMessage("----------------No Scrapper Recrods Found in Bucket----------------");
                }


                foreach (var storageObject in storage.ListObjects(model.BucketName, model.objectName))
                {
                    List<dynamic> objDynamic = new List<dynamic>();

                    if (storageObject.Name != model.objectName + "/")
                    {
                        if (storageObject.Name.Split('/').Length > 1)
                        {

                            string chkfilenmae = storageObject.Name;
                            bool chkjson = Regex.IsMatch(chkfilenmae, ".json");
                            if (chkjson)
                            {
                                //var Filename = storageObject.Name.Split('/')[1];
                                var Filename = storageObject.Name.Split('/')[2];
                                string ppwnummber = Filename.Split('.')[0];
                                //string ppwname = "new/ ppw_67804	";


                                DestObjectName = DestBucketFolderName + "/" + Filename;
                                //model.localPath = SaveImgPath ?? Path.GetFileName(Filename);
                                model.localPath = SaveImgPath + Filename;
                                using (var outputFile = File.OpenWrite(model.localPath))
                                {

                                    storage.DownloadObject(model.BucketName, storageObject.Name, outputFile);
                                    outputFile.Close();
                                    outputFile.Dispose();

                                    storage.CopyObject(BucketName, storageObject.Name, BucketName, DestObjectName);
                                    #region FileAccess
                                    string strURL = System.Configuration.ConfigurationManager.AppSettings["IntegrationURL"] + "downloadFromBucket";
                                    string ReqData = "{\r\n \"FileName\": \"" + Filename + "\",\r\n \"FolderName\": \"" + DestBucketFolderName + "\"\r\n,\r\n \"BucketName\": \"" + BucketName + "\"\r\n}";
                                    string FilePath = fileUploadDownload.CallAPI(strURL, ReqData);
                                    #endregion


                                    //string path = "D:\\Projects\\PPW\\Code\\IPL_App\\IPL\\IPL\\Document\\JsonFile\\63588.json";
                                    long length = new FileInfo(model.localPath).Length;
                                    if (length > 0)
                                    {
                                        using (StreamReader sr = new StreamReader(model.localPath))
                                        //using (StreamReader sr = new StreamReader(path))
                                        {
                                            // Read the stream to a string, and write the string to the console.
                                            String Data = sr.ReadToEnd();
                                            Data = Data.Replace("Additional Instructions", "AdditionalInstructions");
                                            objdata.Add(Data);

                                            log.logDebugMessage("------------------------Json Data Start------------------------------------");
                                            log.logDebugMessage(Filename);
                                            log.logDebugMessage(Data);
                                            log.logDebugMessage("------------------------Json Data End------------------------------------");

                                            objDynamic = workOrderMaster_ImportAPI_Data.APIWorkOrderItemDetails(Data, model.WI_Pkey_ID, Filename, FilePath,model.UserID);
                                            workobjdata.Add(objDynamic);
                                            sr.Close();
                                            sr.Dispose();




                                        }
                                    }
                                    else
                                    {
                                        log.logDebugMessage("------------------------InValid File Size 0 -------------------" + Filename + "-----------------");
                                        //File.Delete(model.localPath);
                                    }


                                }

                                File.Delete(model.localPath);
                                try
                                {
                                    storage.DeleteObject(BucketName, storageObject.Name);
                                }
                                catch (Exception ex)
                                {
                                    log.logErrorMessage("---Error While deleting object---------------------");
                                    log.logErrorMessage(ex.Message);
                                    log.logErrorMessage(ex.StackTrace);
                                }


                                #region Image Download code  
                                //Image Download code starts 

                                // string ImageFoldername = model.objectName + "/ppw_" + "67804"; /// for testing

                                if (model.image_download == true)
                                {

                                    string ImageFoldername = model.objectName + "/ppw_" + ppwnummber;
                                    string SubImageFoldername = "ppw_" + ppwnummber;
                                    log.logDebugMessage("Image Folder Name SubImageFoldername -------->" + SubImageFoldername);
                                    var Imagefolderlist = storage.ListObjects(model.BucketName, model.objectName);
                                    string strPPWFoldername = string.Empty;

                                    foreach (var storageObjectval in Imagefolderlist)
                                    {
                                        if (storageObjectval.Name != model.objectName + "/")
                                        {
                                            var Filenamelist = storageObjectval.Name.Split('/');
                                            if (Filenamelist.Length > 1)
                                            {
                                                strPPWFoldername = storageObjectval.Name.Split('/')[1];
                                            }

                                            if (SubImageFoldername == strPPWFoldername)
                                            {
                                                ImageGenerator imageGenerator = new ImageGenerator();
                                                string vaal = storageObjectval.Name;
                                                string zipfilename = vaal.Split('/')[2];
                                                model.ImageZiplocalPath = SaveImgPath + zipfilename;
                                                if (File.Exists(model.ImageZiplocalPath))
                                                {
                                                    File.Delete(model.ImageZiplocalPath);
                                                }
                                                using (var outputFile = File.OpenWrite(model.ImageZiplocalPath))
                                                {
                                                    storage.DownloadObject(model.BucketName, vaal, outputFile);
                                                    outputFile.Close();
                                                    outputFile.Dispose();
                                                }


                                                string newdirectorypath = SaveImgPath + "ppw_" + ppwnummber;
                                                // string newdirectorypath = SaveImgPath + "ppw_67804";
                                                if (Directory.Exists(newdirectorypath))
                                                {
                                                    Directory.Delete(newdirectorypath);
                                                    log.logDebugMessage("Delete Folder ------------------->" + "ppw_" + ppwnummber);
                                                }

                                                Directory.CreateDirectory(newdirectorypath);

                                                fileUploadDownloadDTO = new FileUploadDownloadDTO();

                                                fileUploadDownloadDTO.zipPath = model.ImageZiplocalPath;
                                                fileUploadDownloadDTO.extractPath = newdirectorypath;

                                                fileUploadDownload.ExtractZipFile(fileUploadDownloadDTO);

                                                fileUploadDownloadDTO.importpkey = objDynamic[0];
                                                fileUploadDownloadDTO.WI_PkeyID = model.WI_Pkey_ID;
                                                fileUploadDownloadDTO.PPW_Number = ppwnummber;
                                                fileUploadDownloadDTO.DirectoryPath = newdirectorypath;

                                                fileUploadDownload.UploadPPWphotos(fileUploadDownloadDTO);
                                                string DelImageFoldername = model.objectName + "/ppw_" + ppwnummber + "/" + zipfilename;
                                                storage.DeleteObject(BucketName, DelImageFoldername);
                                            }


                                        }



                                    }

                                }





                                #endregion
                                //Console.WriteLine(storageObject.Name);
                            }
                        }
                    }
                }
                int countworkorder = workobjdata.Count;
                string strworkOrder = string.Empty;

                //for (int i = 0; i < workobjdata.Count; i++)
                //{
                //    if (!string.IsNullOrEmpty(strworkOrder))
                //    {
                //        strworkOrder = strworkOrder + "," + workobjdata[i][0];
                //    }
                //    else
                //    {
                //        strworkOrder = workobjdata[i][0];
                //    }

                //}
                // The Code to insert need to be written here [Import_Queue_Transaction 

                Import_Queue_TransData import_Queue_TransData = new Import_Queue_TransData();
                Import_Queue_Trans_DTO import_Queue_Trans_DTO = new Import_Queue_Trans_DTO();
                import_Queue_Trans_DTO.Imrt_Wo_Import_ID = model.WI_Pkey_ID;

                //import_Queue_Trans_DTO.Imrt_Wo_ID = strworkOrder;
                import_Queue_Trans_DTO.Type = 1;
                import_Queue_Trans_DTO.Imrt_Import_From_ID = 1;
                import_Queue_Trans_DTO.Imrt_Status_Msg = model.Scrapper_rep_code;
                if (Flag == 2)
                {
                    import_Queue_TransData.UpdateAutoImportQueueTransaction(import_Queue_Trans_DTO);
                }
                else
                {
                    import_Queue_Trans_DTO.UserID = model.UserID;
                    import_Queue_TransData.UpdateAutoImportQueueTransaction(import_Queue_Trans_DTO);
                    // import_Queue_TransData.AddUpdateImportQueueTransData(import_Queue_Trans_DTO);
                }

                log.logDebugMessage("----------------Bucket Called End----------------");


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);

            }
            return objdata;
        }

        public List<dynamic> ListImageObjects(FileUploadBucketDTO model)
        {
            List<dynamic> objdata = new List<dynamic>();
            try
            {
                string jsonpath = System.Configuration.ConfigurationManager.AppSettings["GooglebucketJsom"];

                string SaveImgPath = System.Configuration.ConfigurationManager.AppSettings["GooglePhotosSave"];

                //string BucketName = System.Configuration.ConfigurationManager.AppSettings["ImportBucketName"];
                //string BucketFolderName = System.Configuration.ConfigurationManager.AppSettings["BucketFolderName"];
                //string DestBucketFolderName = System.Configuration.ConfigurationManager.AppSettings["DestBucketFolderName"];

                string BucketName = model.Import_BucketName;
                string BucketFolderName = model.Import_BucketFolderName;
                string DestBucketFolderName = model.Import_DestBucketFolderName;

                string DestObjectName = string.Empty;
                model.BucketName = BucketName;
                model.objectName = BucketFolderName;

                Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", jsonpath);

                FileUploadDownload fileUploadDownload = new FileUploadDownload();
                FileUploadDownloadDTO fileUploadDownloadDTO;

                var credential = GoogleCredential.GetApplicationDefault();
                var storage = StorageClient.Create();

                #region Image Download code  
                //Image Download code starts 

                // string ImageFoldername = model.objectName + "/ppw_" + "67804"; /// for testing

                var Imagefolderlist = storage.ListObjects(model.BucketName, model.objectName);
                string strPPWFoldername = string.Empty, ppwnummber = string.Empty;

                foreach (var storageObjectval in Imagefolderlist)
                {
                    if (storageObjectval.Name != model.objectName + "/")
                    {
                        var Filenamelist = storageObjectval.Name.Split('/');
                        if (Filenamelist.Length > 1)
                        {
                            strPPWFoldername = storageObjectval.Name.Split('/')[1];
                            ppwnummber = strPPWFoldername.Split('_')[1];
                        }

                        string ImageFoldername = model.objectName + "/ppw_" + ppwnummber;
                        ImageGenerator imageGenerator = new ImageGenerator();
                        string vaal = storageObjectval.Name;
                        string zipfilename = vaal.Split('/')[2];
                        model.ImageZiplocalPath = SaveImgPath + zipfilename;
                        if (File.Exists(model.ImageZiplocalPath))
                        {
                            File.Delete(model.ImageZiplocalPath);
                        }

                        using (var outputFile = File.OpenWrite(model.ImageZiplocalPath))
                        {

                            outputFile.Close();
                            outputFile.Dispose();

                        }


                        string newdirectorypath = SaveImgPath + "ppw_" + ppwnummber;
                        // string newdirectorypath = SaveImgPath + "ppw_67804";
                        if (Directory.Exists(newdirectorypath))
                        {
                            Directory.Delete(newdirectorypath);
                        }

                        Directory.CreateDirectory(newdirectorypath);

                        fileUploadDownloadDTO = new FileUploadDownloadDTO();

                        fileUploadDownloadDTO.zipPath = model.ImageZiplocalPath;
                        fileUploadDownloadDTO.extractPath = newdirectorypath;


                        fileUploadDownload.ExtractZipFile(fileUploadDownloadDTO);



                        WorkOrderMaster_ImportAPI_Data workOrderMaster_ImportAPI_Data = new WorkOrderMaster_ImportAPI_Data();
                        WorkOrderMaster_API_DTO workOrderMaster_API_DTO = new WorkOrderMaster_API_DTO();
                        workOrderMaster_API_DTO.ppw = ppwnummber;
                        workOrderMaster_API_DTO = workOrderMaster_ImportAPI_Data.GetImportIDFromPPW(workOrderMaster_API_DTO);
                        fileUploadDownloadDTO.importpkey = workOrderMaster_API_DTO.Pkey_Id;
                        fileUploadDownloadDTO.WI_PkeyID = workOrderMaster_API_DTO.ImportMasterPkey.GetValueOrDefault(0);
                        fileUploadDownloadDTO.PPW_Number = ppwnummber;
                        fileUploadDownloadDTO.DirectoryPath = newdirectorypath;

                        fileUploadDownload.UploadPPWphotos(fileUploadDownloadDTO);
                        string DelImageFoldername = model.objectName + "/ppw_" + ppwnummber + "/" + zipfilename;
                        storage.DeleteObject(BucketName, DelImageFoldername);



                    }



                }







                #endregion




            }
            catch (Exception ex)
            {

                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);


            }
            return objdata;
        }
        public int DownloadJsonFile(DownloadImageDTO downloadImageDTO)
        {
            int intreturn = 0;
            try
            {
                string jsonpath = System.Configuration.ConfigurationManager.AppSettings["GooglebucketJsom"];

                string SaveImgPath = System.Configuration.ConfigurationManager.AppSettings["GooglePhotosSave"];

                string BucketName = System.Configuration.ConfigurationManager.AppSettings["BucketName"];

                downloadImageDTO.BucketName = BucketName;

                Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", jsonpath);
                var credential = GoogleCredential.GetApplicationDefault();
                var storage = StorageClient.Create();

                string LocalPathSet = SaveImgPath + downloadImageDTO.IPLFolder;// IPL NO


                downloadImageDTO.localPath = downloadImageDTO.localPath ?? Path.GetFileName(downloadImageDTO.objectName);
                using (var outputFile = File.OpenWrite(downloadImageDTO.localPath))
                {
                    storage.DownloadObject(downloadImageDTO.BucketName, downloadImageDTO.objectName, outputFile);



                }

                intreturn = 1;

                return intreturn;

            }
            catch (Exception ex)
            {
                intreturn = 0;
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);

                return intreturn;

            }
        }
        public int MoveObject(MovedFilesDTO movedFilesDTO)
        {
            int intreturnval = 0;
            try
            {
                Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", "E:\\office work\\IPL_Version_New\\IPL\\IPL\\iPreservation Live-c01a5b13cbb2.json");

                var credential = GoogleCredential.GetApplicationDefault();
                var storage = StorageClient.Create();
                storage.CopyObject(movedFilesDTO.BucketName, movedFilesDTO.sourceObjectName, movedFilesDTO.BucketName,
                    movedFilesDTO.destObjectName);
                storage.DeleteObject(movedFilesDTO.BucketName, movedFilesDTO.sourceObjectName);
                intreturnval = 1;
            }
            catch (Exception ex)
            {

                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                intreturnval = 0;
            }
            return intreturnval;


        }
        public List<dynamic> SaveCloudPhotoInLocal(string folderName, string fileName)
        {
            FileUploadBucketDTO model = new FileUploadBucketDTO();
            List<dynamic> objdata = new List<dynamic>();
            try
            {
                string jsonpath = System.Configuration.ConfigurationManager.AppSettings["GooglebucketJsom"];

                string SaveImgPath = System.Configuration.ConfigurationManager.AppSettings["ClientResultPhotosSave"];

                //  model.BucketName = "rare-lambda-245821.appspot.com";
                model.BucketName = System.Configuration.ConfigurationManager.AppSettings["BucketName"];
                model.objectName = folderName;

                Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", jsonpath);

                FileUploadDownload fileUploadDownload = new FileUploadDownload();

                var credential = GoogleCredential.GetApplicationDefault();
                var storage = StorageClient.Create();

                #region Image Download code  
                //Image Download code starts 

                string folderPath = SaveImgPath + folderName; // IPL NO

                if (!Directory.Exists(folderPath))
                {
                    Directory.CreateDirectory(folderPath);
                }
                var strpath = folderPath + "\\" + fileName;
                using (var fileStream = File.Create(strpath))
                {
                    storage.DownloadObject(model.BucketName, folderName + "/" + fileName, fileStream);
                }

                #endregion
            }
            catch (Exception ex)
            {

                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);


            }
            return objdata;
        }
        //get list of object 
        public List<dynamic> MSIListObjects(FileUploadBucketDTO model, int Flag)
        {
            List<dynamic> objdata = new List<dynamic>();
            try
            {

                log.logDebugMessage("----------------Bucket Called Start----------------");
                List<dynamic> workobjdata = new List<dynamic>();
                WorkOrderMaster_ImportAPI_Data workOrderMaster_ImportAPI_Data = new WorkOrderMaster_ImportAPI_Data();
                string jsonpath = System.Configuration.ConfigurationManager.AppSettings["GooglebucketJsom"];

                string SaveImgPath = System.Configuration.ConfigurationManager.AppSettings["GooglePhotosSave"];
                string strURL = System.Configuration.ConfigurationManager.AppSettings["IntegrationURL"] + "downloadFromBucket";

                string BucketName = model.Import_BucketName;
                string BucketFolderName = model.Import_BucketFolderName;
                string DestBucketFolderName = model.Import_DestBucketFolderName;
                string PDFBucketFolderName = "msi/pdf";

                string DestObjectName = string.Empty;
                model.BucketName = BucketName;
                model.objectName = BucketFolderName;

                Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", jsonpath);

                FileUploadDownload fileUploadDownload = new FileUploadDownload();
              

                var credential = GoogleCredential.GetApplicationDefault();
                var storage = StorageClient.Create();
                var countlistobj = storage.ListObjects(model.BucketName, model.objectName);



                if (countlistobj != null)
                {
                    var val = countlistobj.Count().ToString();
                    log.logDebugMessage("---------------MSI Scrapper Recrods Found in Bucket count----------------" + countlistobj.Count().ToString());
                }
                else
                {
                    log.logDebugMessage("----------------No Scrapper Recrods Found in Bucket----------------");
                }


                foreach (var storageObject in storage.ListObjects(model.BucketName, model.objectName))
                {
                    List<dynamic> objDynamic = new List<dynamic>();

                    if (storageObject.Name != model.objectName + "/")
                    {
                        if (storageObject.Name.Split('/').Length > 1)
                        {

                            string chkfilenmae = storageObject.Name;
                            bool chkjson = Regex.IsMatch(chkfilenmae, ".json");
                            if (chkjson)
                            {
                              
                                var pdfFilename = storageObject.Name.Split('/');
                                string pdf = pdfFilename[1].Split('.')[0];
                                //var Filename = storageObject.Name.Split('_')[1];
                                var Filename = pdfFilename[1];
                                //string msiNumber = Filename.Split('.')[0].Split('_')[1];
                                var pdfname = pdf + ".pdf";
                                //string ppwname = "new/ ppw_67804	";


                                DestObjectName = DestBucketFolderName + "/" + Filename;
                                //model.localPath = SaveImgPath ?? Path.GetFileName(Filename);
                                model.localPath = SaveImgPath + "MSI_"+Filename;
                                using (var outputFile = File.OpenWrite(model.localPath))
                                {

                                    storage.DownloadObject(model.BucketName, storageObject.Name, outputFile);
                                    outputFile.Close();
                                    outputFile.Dispose();

                                    storage.CopyObject(BucketName, storageObject.Name, BucketName, DestObjectName);

                                    #region FileAccess
                            
                                    string ReqData = "{\r\n \"FileName\": \"" + Filename + "\",\r\n \"FolderName\": \"" + DestBucketFolderName + "\"\r\n,\r\n \"BucketName\": \"" + BucketName + "\"\r\n}";
                                    string FilePath = fileUploadDownload.CallAPI(strURL, ReqData);
                                    #endregion
                                    // pdf store
                                    ///
                                   
                                    string ReqPdfData = "{\r\n \"FileName\": \"" + pdfname + "\",\r\n \"FolderName\": \"" + PDFBucketFolderName + "\"\r\n,\r\n \"BucketName\": \"" + BucketName + "\"\r\n}";
                                    string PdfFilePath = fileUploadDownload.CallAPI(strURL, ReqPdfData);


                                    //string path = "D:\\Projects\\PPW\\Code\\IPL_App\\IPL\\IPL\\Document\\JsonFile\\63588.json";
                                    long length = new FileInfo(model.localPath).Length;
                                    if (length > 0)
                                    {
                                        using (StreamReader sr = new StreamReader(model.localPath))
                                        //using (StreamReader sr = new StreamReader(path))
                                        {
                                            // Read the stream to a string, and write the string to the console.
                                            String Data = sr.ReadToEnd();
                                            //Data = Data.Replace("Additional Instructions", "AdditionalInstructions");
                                            objdata.Add(Data);

                                            log.logDebugMessage("------------------------Json Data Start------------------------------------");
                                            log.logDebugMessage("MSI_"+Filename);
                                            log.logDebugMessage(Data);
                                            log.logDebugMessage("------------------------Json Data End------------------------------------");

                                            objDynamic = workOrderMaster_ImportAPI_Data.MSIWorkOrderItemDetails(Data, model.WI_Pkey_ID, Filename, FilePath, PdfFilePath, pdfname,model.UserID);
                                            workobjdata.Add(objDynamic);
                                            sr.Close();
                                            sr.Dispose();
                                        }
                                    }
                                    else
                                    {
                                        log.logDebugMessage("------------------------InValid File Size 0 -------------------" + Filename + "-----------------");
                                        //File.Delete(model.localPath);
                                    }


                                }
                                File.Delete(model.localPath);
                                storage.DeleteObject(BucketName, storageObject.Name);

                                //Console.WriteLine(storageObject.Name);
                            }
                        }
                    }
                }
                int countworkorder = workobjdata.Count;
                string strworkOrder = string.Empty;

                //for (int i = 0; i < workobjdata.Count; i++)
                //{
                //    if (!string.IsNullOrEmpty(strworkOrder))
                //    {
                //        strworkOrder = strworkOrder + "," + workobjdata[i][0];
                //    }
                //    else
                //    {
                //        strworkOrder = workobjdata[i][0];
                //    }

                //}
                // The Code to insert need to be written here [Import_Queue_Transaction 

                Import_Queue_TransData import_Queue_TransData = new Import_Queue_TransData();
                Import_Queue_Trans_DTO import_Queue_Trans_DTO = new Import_Queue_Trans_DTO();
                import_Queue_Trans_DTO.Imrt_Wo_Import_ID = model.WI_Pkey_ID;
                //import_Queue_Trans_DTO.Imrt_Wo_ID = strworkOrder;
                import_Queue_Trans_DTO.Type = 1;
                import_Queue_Trans_DTO.Imrt_Import_From_ID = 3;
                import_Queue_Trans_DTO.Imrt_Status_Msg = model.Scrapper_rep_code;
                if (Flag == 2)
                {
                    import_Queue_TransData.UpdateAutoImportQueueTransaction(import_Queue_Trans_DTO);
                }
                else
                {
                    import_Queue_Trans_DTO.UserID = model.UserID;
                    import_Queue_TransData.UpdateAutoImportQueueTransaction(import_Queue_Trans_DTO);
                    //import_Queue_TransData.AddUpdateImportQueueTransData(import_Queue_Trans_DTO);
                }

                log.logDebugMessage("----------------Bucket Called End----------------");


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);

            }
            return objdata;
        }

        //get file details for nfr
        public List<dynamic> NFRListObjects(FileUploadBucketDTO model, int Flag)
        {
            List<dynamic> objdata = new List<dynamic>();
            try
            {

                log.logDebugMessage("----------------Bucket Called Start----------------");
                List<dynamic> workobjdata = new List<dynamic>();
                WorkOrderMaster_ImportAPI_Data workOrderMaster_ImportAPI_Data = new WorkOrderMaster_ImportAPI_Data();
                string jsonpath = System.Configuration.ConfigurationManager.AppSettings["GooglebucketJsom"];

                string SaveImgPath = System.Configuration.ConfigurationManager.AppSettings["GooglePhotosSave"];
                string strURL = System.Configuration.ConfigurationManager.AppSettings["IntegrationURL"] + "downloadFromBucket";

                string BucketName = model.Import_BucketName;
                string BucketFolderName = model.Import_BucketFolderName;
                string DestBucketFolderName = model.Import_DestBucketFolderName;
                string PDFBucketFolderName = "nfr/pdf";

                string DestObjectName = string.Empty;
                model.BucketName = BucketName;
                model.objectName = BucketFolderName;

                Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", jsonpath);

                FileUploadDownload fileUploadDownload = new FileUploadDownload();
                FileUploadDownloadDTO fileUploadDownloadDTO;

                var credential = GoogleCredential.GetApplicationDefault();
                var storage = StorageClient.Create();
                var countlistobj = storage.ListObjects(model.BucketName, model.objectName);



                if (countlistobj != null)
                {
                    var val = countlistobj.Count().ToString();
                    log.logDebugMessage("---------------NFR Scrapper Records Found in Bucket count----------------" + countlistobj.Count().ToString());
                }
                else
                {
                    log.logDebugMessage("----------------No Scrapper Recrods Found in Bucket----------------");
                }


                foreach (var storageObject in storage.ListObjects(model.BucketName, model.objectName))
                {
                    List<dynamic> objDynamic = new List<dynamic>();

                    if (storageObject.Name != model.objectName + "/")
                    {
                        if (storageObject.Name.Split('/').Length > 1)
                        {

                            string chkfilenmae = storageObject.Name;
                            bool chkjson = Regex.IsMatch(chkfilenmae, ".json");
                            if (chkjson)
                            {

                                var pdfFilename = storageObject.Name.Split('/');
                                string pdf = pdfFilename[2].Split('.')[0];
                                var Filename = pdfFilename[2];
                                var pdfname = pdf + ".pdf";
                                string nfrNumber = Filename.Split('.')[0].Split('_')[1];
                                //string ppwname = "new/ ppw_67804	";


                          

                                DestObjectName = DestBucketFolderName + "/" + Filename;
                                //model.localPath = SaveImgPath ?? Path.GetFileName(Filename);
                                model.localPath = SaveImgPath + "NFR_"+Filename;
                                using (var outputFile = File.OpenWrite(model.localPath))
                                {

                                    storage.DownloadObject(model.BucketName, storageObject.Name, outputFile);
                                    outputFile.Close();
                                    outputFile.Dispose();

                                    storage.CopyObject(BucketName, storageObject.Name, BucketName, DestObjectName);
                                    #region FileAccess
                                  
                                    string ReqData = "{\r\n \"FileName\": \"" + Filename + "\",\r\n \"FolderName\": \"" + DestBucketFolderName + "\"\r\n,\r\n \"BucketName\": \"" + BucketName + "\"\r\n}";
                                    string FilePath = fileUploadDownload.CallAPI(strURL, ReqData);
                                    #endregion
                                    // pdf store
                                    ///

                                    string ReqPdfData = "{\r\n \"FileName\": \"" + pdfname + "\",\r\n \"FolderName\": \"" + PDFBucketFolderName + "\"\r\n,\r\n \"BucketName\": \"" + BucketName + "\"\r\n}";
                                    string PdfFilePath = fileUploadDownload.CallAPI(strURL, ReqPdfData);

                                    //string path = "D:\\Projects\\PPW\\Code\\IPL_App\\IPL\\IPL\\Document\\JsonFile\\63588.json";
                                    long length = new FileInfo(model.localPath).Length;
                                    if (length > 0)
                                    {
                                        using (StreamReader sr = new StreamReader(model.localPath))
                                        //using (StreamReader sr = new StreamReader(path))
                                        {
                                            // Read the stream to a string, and write the string to the console.
                                            String Data = sr.ReadToEnd();
                                            //Data = Data.Replace("Additional Instructions", "AdditionalInstructions");
                                            objdata.Add(Data);

                                            log.logDebugMessage("------------------------Json Data Start------------------------------------");
                                            log.logDebugMessage("NFR_" + Filename);
                                            log.logDebugMessage(Data);
                                            log.logDebugMessage("------------------------Json Data End------------------------------------");

                                            objDynamic = workOrderMaster_ImportAPI_Data.NFRWorkOrderItemDetails(Data, model.WI_Pkey_ID, Filename, FilePath , PdfFilePath , pdfname,model.UserID);
                                            workobjdata.Add(objDynamic);
                                            sr.Close();
                                            sr.Dispose();
                                        }
                                    }
                                    else
                                    {
                                        log.logDebugMessage("------------------------InValid File Size 0 -------------------" + Filename + "-----------------");
                                        //File.Delete(model.localPath);
                                    }


                                }
                                File.Delete(model.localPath);
                                storage.DeleteObject(BucketName, storageObject.Name);

                                #region Image Download code  
                                //Image Download code starts 

                                // string ImageFoldername = model.objectName + "/ppw_" + "67804"; /// for testing

                                if (model.image_download == true)
                                {

                                    string ImageFoldername = model.objectName + "/nfr_" + nfrNumber;
                                    string SubImageFoldername = "nfr_" + nfrNumber;
                                    log.logDebugMessage("Image Folder Name SubImageFoldername -------->" + SubImageFoldername);
                                    var Imagefolderlist = storage.ListObjects(model.BucketName, model.objectName);
                                    string strPPWFoldername = string.Empty;

                                    foreach (var storageObjectval in Imagefolderlist)
                                    {
                                        if (storageObjectval.Name != model.objectName + "/")
                                        {
                                            var Filenamelist = storageObjectval.Name.Split('/');
                                            if (Filenamelist.Length > 1)
                                            {
                                                strPPWFoldername = storageObjectval.Name.Split('/')[1];
                                            }

                                            if (SubImageFoldername == strPPWFoldername)
                                            {
                                                ImageGenerator imageGenerator = new ImageGenerator();
                                                string vaal = storageObjectval.Name;
                                                string zipfilename = vaal.Split('/')[2];
                                                model.ImageZiplocalPath = SaveImgPath + zipfilename;
                                                if (File.Exists(model.ImageZiplocalPath))
                                                {
                                                    File.Delete(model.ImageZiplocalPath);
                                                }
                                                using (var outputFile = File.OpenWrite(model.ImageZiplocalPath))
                                                {
                                                    storage.DownloadObject(model.BucketName, vaal, outputFile);
                                                    outputFile.Close();
                                                    outputFile.Dispose();
                                                }


                                                string newdirectorypath = SaveImgPath + "nfr_" + nfrNumber;
                                                if (Directory.Exists(newdirectorypath))
                                                {
                                                    Directory.Delete(newdirectorypath);
                                                    log.logDebugMessage("Delete Folder ------------------->" + "nfr_" + nfrNumber);
                                                }

                                                Directory.CreateDirectory(newdirectorypath);

                                                fileUploadDownloadDTO = new FileUploadDownloadDTO();

                                                fileUploadDownloadDTO.zipPath = model.ImageZiplocalPath;
                                                fileUploadDownloadDTO.extractPath = newdirectorypath;

                                                fileUploadDownload.ExtractZipFile(fileUploadDownloadDTO);

                                                fileUploadDownloadDTO.importpkey = objDynamic[0];
                                                fileUploadDownloadDTO.WI_PkeyID = model.WI_Pkey_ID;
                                                fileUploadDownloadDTO.PPW_Number = nfrNumber;
                                                fileUploadDownloadDTO.DirectoryPath = newdirectorypath;

                                                fileUploadDownload.UploadPPWphotos(fileUploadDownloadDTO);
                                                string DelImageFoldername = model.objectName + "/nfr_" + nfrNumber + "/" + zipfilename;
                                                storage.DeleteObject(BucketName, DelImageFoldername);
                                            }


                                        }



                                    }

                                }

                                #endregion
                               
                            }
                        }
                    }
                }
                int countworkorder = workobjdata.Count;
                string strworkOrder = string.Empty;


                Import_Queue_TransData import_Queue_TransData = new Import_Queue_TransData();
                Import_Queue_Trans_DTO import_Queue_Trans_DTO = new Import_Queue_Trans_DTO();
                import_Queue_Trans_DTO.Imrt_Wo_Import_ID = model.WI_Pkey_ID;
               
                import_Queue_Trans_DTO.Type = 1;
                import_Queue_Trans_DTO.Imrt_Import_From_ID = 7;
                import_Queue_Trans_DTO.Imrt_Status_Msg = model.Scrapper_rep_code;
                if (Flag == 2)
                {
                    import_Queue_TransData.UpdateAutoImportQueueTransaction(import_Queue_Trans_DTO);
                }
                else
                {
                    import_Queue_Trans_DTO.UserID = model.UserID;
                    import_Queue_TransData.UpdateAutoImportQueueTransaction(import_Queue_Trans_DTO);
                    //import_Queue_TransData.AddUpdateImportQueueTransData(import_Queue_Trans_DTO);
                }

                log.logDebugMessage("----------------Bucket Called End----------------");


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);

            }
            return objdata;
        }


        //GET FILE DETAILS MCS

        public List<dynamic> MCSListObjects(FileUploadBucketDTO model, int Flag)
        {
            List<dynamic> objdata = new List<dynamic>();
            try
            {

                log.logDebugMessage("----------------Bucket Called Start----------------");
                List<dynamic> workobjdata = new List<dynamic>();
                WorkOrderMaster_ImportAPI_Data workOrderMaster_ImportAPI_Data = new WorkOrderMaster_ImportAPI_Data();
                string jsonpath = System.Configuration.ConfigurationManager.AppSettings["GooglebucketJsom"];

                string SaveImgPath = System.Configuration.ConfigurationManager.AppSettings["GooglePhotosSave"];
                string strURL = System.Configuration.ConfigurationManager.AppSettings["IntegrationURL"] + "downloadFromBucket";

                string BucketName = model.Import_BucketName;
                string BucketFolderName = model.Import_BucketFolderName;
                string DestBucketFolderName = model.Import_DestBucketFolderName;
                string PDFBucketFolderName = "mcs/pdf";

                string DestObjectName = string.Empty;
                model.BucketName = BucketName;
                model.objectName = BucketFolderName;

                Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", jsonpath);

                FileUploadDownload fileUploadDownload = new FileUploadDownload();
                FileUploadDownloadDTO fileUploadDownloadDTO;

                var credential = GoogleCredential.GetApplicationDefault();
                var storage = StorageClient.Create();
                var countlistobj = storage.ListObjects(model.BucketName, model.objectName);



                if (countlistobj != null)
                {
                    var val = countlistobj.Count().ToString();
                    log.logDebugMessage("---------------MCS Scrapper Records Found in Bucket count----------------" + countlistobj.Count().ToString());
                }
                else
                {
                    log.logDebugMessage("----------------No Scrapper Recrods Found in Bucket----------------");
                }


                foreach (var storageObject in storage.ListObjects(model.BucketName, model.objectName))
                {
                    List<dynamic> objDynamic = new List<dynamic>();

                    if (storageObject.Name != model.objectName + "/")
                    {
                        if (storageObject.Name.Split('/').Length > 1)
                        {

                            string chkfilenmae = storageObject.Name;
                            bool chkjson = Regex.IsMatch(chkfilenmae, ".json");
                            if (chkjson)
                            {

                                var pdfFilename = storageObject.Name.Split('/');
                                string pdf = pdfFilename[2].Split('.')[0];
                                var Filename = pdfFilename[2];
                                var pdfname = pdf + ".pdf";
                                //string nfrNumber = Filename.Split('.')[0].Split('_')[1];
                                string nfrNumber = Filename.Split('.')[0];
                                //string ppwname = "new/ ppw_67804	";




                                DestObjectName = DestBucketFolderName + "/" + Filename;
                                //model.localPath = SaveImgPath ?? Path.GetFileName(Filename);
                                model.localPath = SaveImgPath + "MCS_" + Filename;
                                using (var outputFile = File.OpenWrite(model.localPath))
                                {

                                    storage.DownloadObject(model.BucketName, storageObject.Name, outputFile);
                                    outputFile.Close();
                                    outputFile.Dispose();

                                    storage.CopyObject(BucketName, storageObject.Name, BucketName, DestObjectName);
                                    #region FileAccess

                                    string ReqData = "{\r\n \"FileName\": \"" + Filename + "\",\r\n \"FolderName\": \"" + DestBucketFolderName + "\"\r\n,\r\n \"BucketName\": \"" + BucketName + "\"\r\n}";
                                    string FilePath = fileUploadDownload.CallAPI(strURL, ReqData);
                                    #endregion
                                    // pdf store
                                    ///

                                    //string ReqPdfData = "{\r\n \"FileName\": \"" + pdfname + "\",\r\n \"FolderName\": \"" + PDFBucketFolderName + "\"\r\n,\r\n \"BucketName\": \"" + BucketName + "\"\r\n}";
                                    //string PdfFilePath = fileUploadDownload.CallAPI(strURL, ReqPdfData);
                                    string ReqPdfData = string.Empty, PdfFilePath = string.Empty;
                                    //string path = "D:\\Projects\\PPW\\Code\\IPL_App\\IPL\\IPL\\Document\\JsonFile\\63588.json";
                                    long length = new FileInfo(model.localPath).Length;
                                    if (length > 0)
                                    {
                                        using (StreamReader sr = new StreamReader(model.localPath))
                                        //using (StreamReader sr = new StreamReader(path))
                                        {
                                            // Read the stream to a string, and write the string to the console.
                                            String Data = sr.ReadToEnd();
                                            //Data = Data.Replace("Additional Instructions", "AdditionalInstructions");
                                            objdata.Add(Data);

                                            log.logDebugMessage("------------------------Json Data Start------------------------------------");
                                            log.logDebugMessage("MCS_" + Filename);
                                            log.logDebugMessage(Data);
                                            log.logDebugMessage("------------------------Json Data End------------------------------------");

                                            objDynamic = workOrderMaster_ImportAPI_Data.MCSWorkOrderItemDetails(Data, model.WI_Pkey_ID, Filename, FilePath, PdfFilePath, pdfname, model.UserID);
                                            workobjdata.Add(objDynamic);
                                            sr.Close();
                                            sr.Dispose();
                                        }
                                    }
                                    else
                                    {
                                        log.logDebugMessage("------------------------InValid File Size 0 -------------------" + Filename + "-----------------");
                                        //File.Delete(model.localPath);
                                    }


                                }
                                File.Delete(model.localPath);
                                storage.DeleteObject(BucketName, storageObject.Name);

                                #region Image Download code  
                                //Image Download code starts 

                                // string ImageFoldername = model.objectName + "/ppw_" + "67804"; /// for testing

                                if (model.image_download == true)
                                {

                                    string ImageFoldername = model.objectName + "/nfr_" + nfrNumber;
                                    string SubImageFoldername = "nfr_" + nfrNumber;
                                    log.logDebugMessage("Image Folder Name SubImageFoldername -------->" + SubImageFoldername);
                                    var Imagefolderlist = storage.ListObjects(model.BucketName, model.objectName);
                                    string strPPWFoldername = string.Empty;

                                    foreach (var storageObjectval in Imagefolderlist)
                                    {
                                        if (storageObjectval.Name != model.objectName + "/")
                                        {
                                            var Filenamelist = storageObjectval.Name.Split('/');
                                            if (Filenamelist.Length > 1)
                                            {
                                                strPPWFoldername = storageObjectval.Name.Split('/')[1];
                                            }

                                            if (SubImageFoldername == strPPWFoldername)
                                            {
                                                ImageGenerator imageGenerator = new ImageGenerator();
                                                string vaal = storageObjectval.Name;
                                                string zipfilename = vaal.Split('/')[2];
                                                model.ImageZiplocalPath = SaveImgPath + zipfilename;
                                                if (File.Exists(model.ImageZiplocalPath))
                                                {
                                                    File.Delete(model.ImageZiplocalPath);
                                                }
                                                using (var outputFile = File.OpenWrite(model.ImageZiplocalPath))
                                                {
                                                    storage.DownloadObject(model.BucketName, vaal, outputFile);
                                                    outputFile.Close();
                                                    outputFile.Dispose();
                                                }


                                                string newdirectorypath = SaveImgPath + "nfr_" + nfrNumber;
                                                if (Directory.Exists(newdirectorypath))
                                                {
                                                    Directory.Delete(newdirectorypath);
                                                    log.logDebugMessage("Delete Folder ------------------->" + "nfr_" + nfrNumber);
                                                }

                                                Directory.CreateDirectory(newdirectorypath);

                                                fileUploadDownloadDTO = new FileUploadDownloadDTO();

                                                fileUploadDownloadDTO.zipPath = model.ImageZiplocalPath;
                                                fileUploadDownloadDTO.extractPath = newdirectorypath;

                                                fileUploadDownload.ExtractZipFile(fileUploadDownloadDTO);

                                                fileUploadDownloadDTO.importpkey = objDynamic[0];
                                                fileUploadDownloadDTO.WI_PkeyID = model.WI_Pkey_ID;
                                                fileUploadDownloadDTO.PPW_Number = nfrNumber;
                                                fileUploadDownloadDTO.DirectoryPath = newdirectorypath;

                                                fileUploadDownload.UploadPPWphotos(fileUploadDownloadDTO);
                                                string DelImageFoldername = model.objectName + "/nfr_" + nfrNumber + "/" + zipfilename;
                                                storage.DeleteObject(BucketName, DelImageFoldername);
                                            }


                                        }



                                    }

                                }

                                #endregion

                            }
                        }
                    }
                }
                int countworkorder = workobjdata.Count;
                string strworkOrder = string.Empty;


                Import_Queue_TransData import_Queue_TransData = new Import_Queue_TransData();
                Import_Queue_Trans_DTO import_Queue_Trans_DTO = new Import_Queue_Trans_DTO();
                import_Queue_Trans_DTO.Imrt_Wo_Import_ID = model.WI_Pkey_ID;

                import_Queue_Trans_DTO.Type = 1;
                import_Queue_Trans_DTO.Imrt_Import_From_ID = 6;
                import_Queue_Trans_DTO.Imrt_Status_Msg = model.Scrapper_rep_code;
                if (Flag == 2)
                {
                    import_Queue_TransData.UpdateAutoImportQueueTransaction(import_Queue_Trans_DTO);
                }
                else
                {
                    import_Queue_Trans_DTO.UserID = model.UserID;
                    import_Queue_TransData.UpdateAutoImportQueueTransaction(import_Queue_Trans_DTO);
                    //import_Queue_TransData.AddUpdateImportQueueTransData(import_Queue_Trans_DTO);
                }

                log.logDebugMessage("----------------Bucket Called End----------------");


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);

            }
            return objdata;
        }

        //Get file details for AG

        public List<dynamic> AGListObjects(FileUploadBucketDTO model, int Flag)
        {
            List<dynamic> objdata = new List<dynamic>();
            try
            {

                log.logDebugMessage("----------------Bucket Called Start----------------");
                List<dynamic> workobjdata = new List<dynamic>();
                WorkOrderMaster_ImportAPI_Data workOrderMaster_ImportAPI_Data = new WorkOrderMaster_ImportAPI_Data();
                string jsonpath = System.Configuration.ConfigurationManager.AppSettings["GooglebucketJsom"];

                string SaveImgPath = System.Configuration.ConfigurationManager.AppSettings["GooglePhotosSave"];
                string strURL = System.Configuration.ConfigurationManager.AppSettings["IntegrationURL"] + "downloadFromBucket";

                string BucketName = model.Import_BucketName;
                string BucketFolderName = model.Import_BucketFolderName;
                string DestBucketFolderName = model.Import_DestBucketFolderName;
                string PDFBucketFolderName = "AG/pdf";

                string DestObjectName = string.Empty;
                model.BucketName = BucketName;
                model.objectName = BucketFolderName;

                Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", jsonpath);

                FileUploadDownload fileUploadDownload = new FileUploadDownload();
                FileUploadDownloadDTO fileUploadDownloadDTO;

                var credential = GoogleCredential.GetApplicationDefault();
                var storage = StorageClient.Create();
                var countlistobj = storage.ListObjects(model.BucketName, model.objectName);



                if (countlistobj != null)
                {
                    var val = countlistobj.Count().ToString();
                    log.logDebugMessage("---------------AG Scrapper Records Found in Bucket count----------------" + countlistobj.Count().ToString());
                }
                else
                {
                    log.logDebugMessage("----------------No Scrapper Recrods Found in Bucket----------------");
                }


                foreach (var storageObject in storage.ListObjects(model.BucketName, model.objectName))
                {
                    List<dynamic> objDynamic = new List<dynamic>();

                    if (storageObject.Name != model.objectName + "/")
                    {
                        if (storageObject.Name.Split('/').Length > 1)
                        {

                            string chkfilenmae = storageObject.Name;
                            bool chkjson = Regex.IsMatch(chkfilenmae, ".json");
                            if (chkjson)
                            {

                                var pdfFilename = storageObject.Name.Split('/');
                                string pdf = pdfFilename[2].Split('.')[0];
                                var Filename = pdfFilename[2];
                                var pdfname = pdf + ".pdf";
                                //string nfrNumber = Filename.Split('.')[0].Split('_')[1];
                                string nfrNumber = Filename.Split('.')[0];
                                //string ppwname = "new/ ppw_67804	";




                                DestObjectName = DestBucketFolderName + "/" + Filename;
                                //model.localPath = SaveImgPath ?? Path.GetFileName(Filename);
                                model.localPath = SaveImgPath + "AG_" + Filename;
                                using (var outputFile = File.OpenWrite(model.localPath))
                                {

                                    storage.DownloadObject(model.BucketName, storageObject.Name, outputFile);
                                    outputFile.Close();
                                    outputFile.Dispose();

                                    storage.CopyObject(BucketName, storageObject.Name, BucketName, DestObjectName);
                                    #region FileAccess

                                    string ReqData = "{\r\n \"FileName\": \"" + Filename + "\",\r\n \"FolderName\": \"" + DestBucketFolderName + "\"\r\n,\r\n \"BucketName\": \"" + BucketName + "\"\r\n}";
                                    string FilePath = fileUploadDownload.CallAPI(strURL, ReqData);
                                    #endregion
                                    // pdf store
                                    ///

                                    //string ReqPdfData = "{\r\n \"FileName\": \"" + pdfname + "\",\r\n \"FolderName\": \"" + PDFBucketFolderName + "\"\r\n,\r\n \"BucketName\": \"" + BucketName + "\"\r\n}";
                                    //string PdfFilePath = fileUploadDownload.CallAPI(strURL, ReqPdfData);
                                    string ReqPdfData = string.Empty, PdfFilePath = string.Empty;
                                    //string path = "D:\\Projects\\PPW\\Code\\IPL_App\\IPL\\IPL\\Document\\JsonFile\\63588.json";
                                    long length = new FileInfo(model.localPath).Length;
                                    if (length > 0)
                                    {
                                        using (StreamReader sr = new StreamReader(model.localPath))
                                        //using (StreamReader sr = new StreamReader(path))
                                        {
                                            // Read the stream to a string, and write the string to the console.
                                            String Data = sr.ReadToEnd();
                                            //Data = Data.Replace("Additional Instructions", "AdditionalInstructions");
                                            objdata.Add(Data);

                                            log.logDebugMessage("------------------------Json Data Start------------------------------------");
                                            log.logDebugMessage("AG_" + Filename);
                                            log.logDebugMessage(Data);
                                            log.logDebugMessage("------------------------Json Data End------------------------------------");

                                            objDynamic = workOrderMaster_ImportAPI_Data.AGWorkOrderItemDetails(Data, model.WI_Pkey_ID, Filename, FilePath, PdfFilePath, pdfname, model.UserID);
                                            workobjdata.Add(objDynamic);
                                            sr.Close();
                                            sr.Dispose();
                                        }
                                    }
                                    else
                                    {
                                        log.logDebugMessage("------------------------InValid File Size 0 -------------------" + Filename + "-----------------");
                                        //File.Delete(model.localPath);
                                    }


                                }
                                File.Delete(model.localPath);
                                storage.DeleteObject(BucketName, storageObject.Name);

                                #region Image Download code  
                                //Image Download code starts 

                                // string ImageFoldername = model.objectName + "/ppw_" + "67804"; /// for testing

                                if (model.image_download == true)
                                {

                                    string ImageFoldername = model.objectName + "/nfr_" + nfrNumber;
                                    string SubImageFoldername = "AG_" + nfrNumber;
                                    log.logDebugMessage("Image Folder Name SubImageFoldername -------->" + SubImageFoldername);
                                    var Imagefolderlist = storage.ListObjects(model.BucketName, model.objectName);
                                    string strPPWFoldername = string.Empty;

                                    foreach (var storageObjectval in Imagefolderlist)
                                    {
                                        if (storageObjectval.Name != model.objectName + "/")
                                        {
                                            var Filenamelist = storageObjectval.Name.Split('/');
                                            if (Filenamelist.Length > 1)
                                            {
                                                strPPWFoldername = storageObjectval.Name.Split('/')[1];
                                            }

                                            if (SubImageFoldername == strPPWFoldername)
                                            {
                                                ImageGenerator imageGenerator = new ImageGenerator();
                                                string vaal = storageObjectval.Name;
                                                string zipfilename = vaal.Split('/')[2];
                                                model.ImageZiplocalPath = SaveImgPath + zipfilename;
                                                if (File.Exists(model.ImageZiplocalPath))
                                                {
                                                    File.Delete(model.ImageZiplocalPath);
                                                }
                                                using (var outputFile = File.OpenWrite(model.ImageZiplocalPath))
                                                {
                                                    storage.DownloadObject(model.BucketName, vaal, outputFile);
                                                    outputFile.Close();
                                                    outputFile.Dispose();
                                                }


                                                string newdirectorypath = SaveImgPath + "AG_" + nfrNumber;
                                                if (Directory.Exists(newdirectorypath))
                                                {
                                                    Directory.Delete(newdirectorypath);
                                                    log.logDebugMessage("Delete Folder ------------------->" + "AG_" + nfrNumber);
                                                }

                                                Directory.CreateDirectory(newdirectorypath);

                                                fileUploadDownloadDTO = new FileUploadDownloadDTO();

                                                fileUploadDownloadDTO.zipPath = model.ImageZiplocalPath;
                                                fileUploadDownloadDTO.extractPath = newdirectorypath;

                                                fileUploadDownload.ExtractZipFile(fileUploadDownloadDTO);

                                                fileUploadDownloadDTO.importpkey = objDynamic[0];
                                                fileUploadDownloadDTO.WI_PkeyID = model.WI_Pkey_ID;
                                                fileUploadDownloadDTO.PPW_Number = nfrNumber;
                                                fileUploadDownloadDTO.DirectoryPath = newdirectorypath;

                                                fileUploadDownload.UploadPPWphotos(fileUploadDownloadDTO);
                                                string DelImageFoldername = model.objectName + "/AG_" + nfrNumber + "/" + zipfilename;
                                                storage.DeleteObject(BucketName, DelImageFoldername);
                                            }


                                        }



                                    }

                                }

                                #endregion

                            }
                        }
                    }
                }
                int countworkorder = workobjdata.Count;
                string strworkOrder = string.Empty;


                Import_Queue_TransData import_Queue_TransData = new Import_Queue_TransData();
                Import_Queue_Trans_DTO import_Queue_Trans_DTO = new Import_Queue_Trans_DTO();
                import_Queue_Trans_DTO.Imrt_Wo_Import_ID = model.WI_Pkey_ID;

                import_Queue_Trans_DTO.Type = 1;
                import_Queue_Trans_DTO.Imrt_Import_From_ID = 19;
                import_Queue_Trans_DTO.Imrt_Status_Msg = model.Scrapper_rep_code;
                if (Flag == 2)
                {
                    import_Queue_TransData.UpdateAutoImportQueueTransaction(import_Queue_Trans_DTO);
                }
                else
                {
                    import_Queue_Trans_DTO.UserID = model.UserID;
                    import_Queue_TransData.UpdateAutoImportQueueTransaction(import_Queue_Trans_DTO);
                    //import_Queue_TransData.AddUpdateImportQueueTransData(import_Queue_Trans_DTO);
                }

                log.logDebugMessage("----------------Bucket Called End----------------");


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);

            }
            return objdata;
        }

        //get file details for Cyprexx
        public List<dynamic> CyprexxListObjects(FileUploadBucketDTO model, int Flag)
        {
            List<dynamic> objdata = new List<dynamic>();
            try
            {

                log.logDebugMessage("----------------Bucket Called Start----------------");
                List<dynamic> workobjdata = new List<dynamic>();
                WorkOrderMaster_ImportAPI_Data workOrderMaster_ImportAPI_Data = new WorkOrderMaster_ImportAPI_Data();
                string jsonpath = System.Configuration.ConfigurationManager.AppSettings["GooglebucketJsom"];

                string SaveImgPath = System.Configuration.ConfigurationManager.AppSettings["GooglePhotosSave"];
                string strURL = System.Configuration.ConfigurationManager.AppSettings["IntegrationURL"] + "downloadFromBucket";

                string BucketName = model.Import_BucketName;
                string BucketFolderName = model.Import_BucketFolderName;
                string DestBucketFolderName = model.Import_DestBucketFolderName;
                string PDFBucketFolderName = "cyprexx/pdf";

                string DestObjectName = string.Empty;
                model.BucketName = BucketName;
                model.objectName = BucketFolderName;

                Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", jsonpath);

                FileUploadDownload fileUploadDownload = new FileUploadDownload();
                FileUploadDownloadDTO fileUploadDownloadDTO;

                var credential = GoogleCredential.GetApplicationDefault();
                var storage = StorageClient.Create();
                var countlistobj = storage.ListObjects(model.BucketName, model.objectName);



                if (countlistobj != null)
                {
                    var val = countlistobj.Count().ToString();
                    log.logDebugMessage("---------------Cyprexx Scrapper Records Found in Bucket count----------------" + countlistobj.Count().ToString());
                }
                else
                {
                    log.logDebugMessage("----------------No Scrapper Recrods Found in Bucket----------------");
                }


                foreach (var storageObject in storage.ListObjects(model.BucketName, model.objectName))
                {
                    List<dynamic> objDynamic = new List<dynamic>();

                    if (storageObject.Name != model.objectName + "/")
                    {
                        if (storageObject.Name.Split('/').Length > 1)
                        {

                            string chkfilenmae = storageObject.Name;
                            bool chkjson = Regex.IsMatch(chkfilenmae, ".json");
                            if (chkjson)
                            {

                                var pdfFilename = storageObject.Name.Split('/');
                                string pdf = pdfFilename[2].Split('.')[0];
                                var Filename = pdfFilename[2];
                                var pdfname = pdf + ".pdf";
                                string nfrNumber = Filename.Split('.')[0].Split('-')[0];
                                //string ppwname = "new/ ppw_67804	";




                                DestObjectName = DestBucketFolderName + "/" + Filename;
                                //model.localPath = SaveImgPath ?? Path.GetFileName(Filename);
                                model.localPath = SaveImgPath + "Cyprexx_" + Filename;
                                using (var outputFile = File.OpenWrite(model.localPath))
                                {

                                    storage.DownloadObject(model.BucketName, storageObject.Name, outputFile);
                                    outputFile.Close();
                                    outputFile.Dispose();

                                    storage.CopyObject(BucketName, storageObject.Name, BucketName, DestObjectName);
                                    #region FileAccess

                                    string ReqData = "{\r\n \"FileName\": \"" + Filename + "\",\r\n \"FolderName\": \"" + DestBucketFolderName + "\"\r\n,\r\n \"BucketName\": \"" + BucketName + "\"\r\n}";
                                    string FilePath = fileUploadDownload.CallAPI(strURL, ReqData);
                                    #endregion
                                    // pdf store
                                    ///

                                    string ReqPdfData = "{\r\n \"FileName\": \"" + pdfname + "\",\r\n \"FolderName\": \"" + PDFBucketFolderName + "\"\r\n,\r\n \"BucketName\": \"" + BucketName + "\"\r\n}";
                                    string PdfFilePath = fileUploadDownload.CallAPI(strURL, ReqPdfData);

                                    //string path = "D:\\Projects\\PPW\\Code\\IPL_App\\IPL\\IPL\\Document\\JsonFile\\63588.json";
                                    long length = new FileInfo(model.localPath).Length;
                                    if (length > 0)
                                    {
                                        using (StreamReader sr = new StreamReader(model.localPath))
                                        //using (StreamReader sr = new StreamReader(path))
                                        {
                                            // Read the stream to a string, and write the string to the console.
                                            String Data = sr.ReadToEnd();
                                            //Data = Data.Replace("Additional Instructions", "AdditionalInstructions");
                                            objdata.Add(Data);

                                            log.logDebugMessage("------------------------Json Data Start------------------------------------");
                                            log.logDebugMessage("Cyprexx_" + Filename);
                                            log.logDebugMessage(Data);
                                            log.logDebugMessage("------------------------Json Data End------------------------------------");

                                            objDynamic = workOrderMaster_ImportAPI_Data.CyprexxkOrderItemDetails(Data, model.WI_Pkey_ID, Filename, FilePath, PdfFilePath, pdfname,model.UserID);
                                            workobjdata.Add(objDynamic);
                                            sr.Close();
                                            sr.Dispose();
                                        }
                                    }
                                    else
                                    {
                                        log.logDebugMessage("------------------------InValid File Size 0 -------------------" + Filename + "-----------------");
                                        //File.Delete(model.localPath);
                                    }


                                }
                                File.Delete(model.localPath);
                                storage.DeleteObject(BucketName, storageObject.Name);

                                #region Image Download code  
                                //Image Download code starts 

                                // string ImageFoldername = model.objectName + "/ppw_" + "67804"; /// for testing

                                if (model.image_download == true)
                                {

                                    string ImageFoldername = model.objectName + "/cyprexx_" + nfrNumber;
                                    string SubImageFoldername = "cyprexx_" + nfrNumber;
                                    log.logDebugMessage("Image Folder Name SubImageFoldername -------->" + SubImageFoldername);
                                    var Imagefolderlist = storage.ListObjects(model.BucketName, model.objectName);
                                    string strPPWFoldername = string.Empty;

                                    foreach (var storageObjectval in Imagefolderlist)
                                    {
                                        if (storageObjectval.Name != model.objectName + "/")
                                        {
                                            var Filenamelist = storageObjectval.Name.Split('/');
                                            if (Filenamelist.Length > 1)
                                            {
                                                strPPWFoldername = storageObjectval.Name.Split('/')[1];
                                            }

                                            if (SubImageFoldername == strPPWFoldername)
                                            {
                                                ImageGenerator imageGenerator = new ImageGenerator();
                                                string vaal = storageObjectval.Name;
                                                string zipfilename = vaal.Split('/')[2];
                                                model.ImageZiplocalPath = SaveImgPath + zipfilename;
                                                if (File.Exists(model.ImageZiplocalPath))
                                                {
                                                    File.Delete(model.ImageZiplocalPath);
                                                }
                                                using (var outputFile = File.OpenWrite(model.ImageZiplocalPath))
                                                {
                                                    storage.DownloadObject(model.BucketName, vaal, outputFile);
                                                    outputFile.Close();
                                                    outputFile.Dispose();
                                                }


                                                string newdirectorypath = SaveImgPath + "cyprexx_" + nfrNumber;
                                                if (Directory.Exists(newdirectorypath))
                                                {
                                                    Directory.Delete(newdirectorypath);
                                                    log.logDebugMessage("Delete Folder ------------------->" + "cyprexx_" + nfrNumber);
                                                }

                                                Directory.CreateDirectory(newdirectorypath);

                                                fileUploadDownloadDTO = new FileUploadDownloadDTO();

                                                fileUploadDownloadDTO.zipPath = model.ImageZiplocalPath;
                                                fileUploadDownloadDTO.extractPath = newdirectorypath;

                                                fileUploadDownload.ExtractZipFile(fileUploadDownloadDTO);

                                                fileUploadDownloadDTO.importpkey = objDynamic[0];
                                                fileUploadDownloadDTO.WI_PkeyID = model.WI_Pkey_ID;
                                                fileUploadDownloadDTO.PPW_Number = nfrNumber;
                                                fileUploadDownloadDTO.DirectoryPath = newdirectorypath;

                                                fileUploadDownload.UploadPPWphotos(fileUploadDownloadDTO);
                                                string DelImageFoldername = model.objectName + "/cyprexx_" + nfrNumber + "/" + zipfilename;
                                                storage.DeleteObject(BucketName, DelImageFoldername);
                                            }


                                        }



                                    }

                                }

                                #endregion

                            }
                        }
                    }
                }
                int countworkorder = workobjdata.Count;
                string strworkOrder = string.Empty;


                Import_Queue_TransData import_Queue_TransData = new Import_Queue_TransData();
                Import_Queue_Trans_DTO import_Queue_Trans_DTO = new Import_Queue_Trans_DTO();
                import_Queue_Trans_DTO.Imrt_Wo_Import_ID = model.WI_Pkey_ID;

                import_Queue_Trans_DTO.Type = 1;
                import_Queue_Trans_DTO.Imrt_Import_From_ID = 10;
                import_Queue_Trans_DTO.Imrt_Status_Msg = model.Scrapper_rep_code;
                if (Flag == 2)
                {
                    import_Queue_TransData.UpdateAutoImportQueueTransaction(import_Queue_Trans_DTO);
                }
                else
                {
                    import_Queue_Trans_DTO.UserID = model.UserID;
                    import_Queue_TransData.UpdateAutoImportQueueTransaction(import_Queue_Trans_DTO);
                    //import_Queue_TransData.AddUpdateImportQueueTransData(import_Queue_Trans_DTO);
                }

                log.logDebugMessage("----------------Bucket Called End----------------");


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);

            }
            return objdata;
        }

        //get file details for Servicelink

        public List<dynamic> ServicelinkListObjects(FileUploadBucketDTO model, int Flag)
        {
            List<dynamic> objdata = new List<dynamic>();
            try
            {

                log.logDebugMessage("----------------Bucket Called Start----------------");
                List<dynamic> workobjdata = new List<dynamic>();
                WorkOrderMaster_ImportAPI_Data workOrderMaster_ImportAPI_Data = new WorkOrderMaster_ImportAPI_Data();
                string jsonpath = System.Configuration.ConfigurationManager.AppSettings["GooglebucketJsom"];

                string SaveImgPath = System.Configuration.ConfigurationManager.AppSettings["GooglePhotosSave"];
                string strURL = System.Configuration.ConfigurationManager.AppSettings["IntegrationURL"] + "downloadFromBucket";

                string BucketName = model.Import_BucketName;
                string BucketFolderName = model.Import_BucketFolderName;
                string DestBucketFolderName = model.Import_DestBucketFolderName;
                string PDFBucketFolderName = "service_link/pdf";

                string DestObjectName = string.Empty;
                model.BucketName = BucketName;
                model.objectName = BucketFolderName;

                Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", jsonpath);

                FileUploadDownload fileUploadDownload = new FileUploadDownload();
                FileUploadDownloadDTO fileUploadDownloadDTO;

                var credential = GoogleCredential.GetApplicationDefault();
                var storage = StorageClient.Create();
                var countlistobj = storage.ListObjects(model.BucketName, model.objectName);



                if (countlistobj != null)
                {
                    var val = countlistobj.Count().ToString();
                    log.logDebugMessage("---------------Servicelink Scrapper Records Found in Bucket count----------------" + countlistobj.Count().ToString());
                }
                else
                {
                    log.logDebugMessage("----------------No Scrapper Recrods Found in Bucket----------------");
                }


                foreach (var storageObject in storage.ListObjects(model.BucketName, model.objectName))
                {
                    List<dynamic> objDynamic = new List<dynamic>();

                    if (storageObject.Name != model.objectName + "/")
                    {
                        if (storageObject.Name.Split('/').Length > 1)
                        {

                            string chkfilenmae = storageObject.Name;
                            bool chkjson = Regex.IsMatch(chkfilenmae, ".json");
                            if (chkjson)
                            {

                                var pdfFilename = storageObject.Name.Split('/');
                                string pdf = pdfFilename[2].Split('.')[0];
                                var Filename = pdfFilename[2];
                                var pdfname = pdf + ".pdf";
                                string nfrNumber = Filename.Split('.')[0];
                                //string ppwname = "new/ ppw_67804	";




                                DestObjectName = DestBucketFolderName + "/" + Filename;
                                //model.localPath = SaveImgPath ?? Path.GetFileName(Filename);
                                model.localPath = SaveImgPath + "Servicelink_" + Filename;
                                using (var outputFile = File.OpenWrite(model.localPath))
                                {

                                    storage.DownloadObject(model.BucketName, storageObject.Name, outputFile);
                                    outputFile.Close();
                                    outputFile.Dispose();

                                    storage.CopyObject(BucketName, storageObject.Name, BucketName, DestObjectName);
                                    #region FileAccess

                                    string ReqData = "{\r\n \"FileName\": \"" + Filename + "\",\r\n \"FolderName\": \"" + DestBucketFolderName + "\"\r\n,\r\n \"BucketName\": \"" + BucketName + "\"\r\n}";
                                    string FilePath = fileUploadDownload.CallAPI(strURL, ReqData);
                                    #endregion
                                    // pdf store
                                    ///

                                    string ReqPdfData = "{\r\n \"FileName\": \"" + pdfname + "\",\r\n \"FolderName\": \"" + PDFBucketFolderName + "\"\r\n,\r\n \"BucketName\": \"" + BucketName + "\"\r\n}";
                                    string PdfFilePath = fileUploadDownload.CallAPI(strURL, ReqPdfData);

                                    //string path = "D:\\Projects\\PPW\\Code\\IPL_App\\IPL\\IPL\\Document\\JsonFile\\63588.json";
                                    long length = new FileInfo(model.localPath).Length;
                                    if (length > 0)
                                    {
                                        using (StreamReader sr = new StreamReader(model.localPath))
                                        //using (StreamReader sr = new StreamReader(path))
                                        {
                                            // Read the stream to a string, and write the string to the console.
                                            String Data = sr.ReadToEnd();
                                            //Data = Data.Replace("Additional Instructions", "AdditionalInstructions");
                                            objdata.Add(Data);

                                            log.logDebugMessage("------------------------Json Data Start------------------------------------");
                                            log.logDebugMessage("Servicelink_" + Filename);
                                            log.logDebugMessage(Data);
                                            log.logDebugMessage("------------------------Json Data End------------------------------------");

                                            objDynamic = workOrderMaster_ImportAPI_Data.ServiceLinkOrderItemDetails(Data, model.WI_Pkey_ID, Filename, FilePath, PdfFilePath, pdfname, model.UserID);
                                            workobjdata.Add(objDynamic);
                                            sr.Close();
                                            sr.Dispose();
                                        }
                                    }
                                    else
                                    {
                                        log.logDebugMessage("------------------------InValid File Size 0 -------------------" + Filename + "-----------------");
                                        //File.Delete(model.localPath);
                                    }


                                }
                                File.Delete(model.localPath);
                                storage.DeleteObject(BucketName, storageObject.Name);

                                #region Image Download code  
                                //Image Download code starts 

                                // string ImageFoldername = model.objectName + "/ppw_" + "67804"; /// for testing

                                if (model.image_download == true)
                                {

                                    string ImageFoldername = model.objectName + "/servicelink_" + nfrNumber;
                                    string SubImageFoldername = "servicelink_" + nfrNumber;
                                    log.logDebugMessage("Image Folder Name SubImageFoldername -------->" + SubImageFoldername);
                                    var Imagefolderlist = storage.ListObjects(model.BucketName, model.objectName);
                                    string strPPWFoldername = string.Empty;

                                    foreach (var storageObjectval in Imagefolderlist)
                                    {
                                        if (storageObjectval.Name != model.objectName + "/")
                                        {
                                            var Filenamelist = storageObjectval.Name.Split('/');
                                            if (Filenamelist.Length > 1)
                                            {
                                                strPPWFoldername = storageObjectval.Name.Split('/')[1];
                                            }

                                            if (SubImageFoldername == strPPWFoldername)
                                            {
                                                ImageGenerator imageGenerator = new ImageGenerator();
                                                string vaal = storageObjectval.Name;
                                                string zipfilename = vaal.Split('/')[2];
                                                model.ImageZiplocalPath = SaveImgPath + zipfilename;
                                                if (File.Exists(model.ImageZiplocalPath))
                                                {
                                                    File.Delete(model.ImageZiplocalPath);
                                                }
                                                using (var outputFile = File.OpenWrite(model.ImageZiplocalPath))
                                                {
                                                    storage.DownloadObject(model.BucketName, vaal, outputFile);
                                                    outputFile.Close();
                                                    outputFile.Dispose();
                                                }


                                                string newdirectorypath = SaveImgPath + "servicelink_" + nfrNumber;
                                                if (Directory.Exists(newdirectorypath))
                                                {
                                                    Directory.Delete(newdirectorypath);
                                                    log.logDebugMessage("Delete Folder ------------------->" + "servicelink_" + nfrNumber);
                                                }

                                                Directory.CreateDirectory(newdirectorypath);

                                                fileUploadDownloadDTO = new FileUploadDownloadDTO();

                                                fileUploadDownloadDTO.zipPath = model.ImageZiplocalPath;
                                                fileUploadDownloadDTO.extractPath = newdirectorypath;

                                                fileUploadDownload.ExtractZipFile(fileUploadDownloadDTO);

                                                fileUploadDownloadDTO.importpkey = objDynamic[0];
                                                fileUploadDownloadDTO.WI_PkeyID = model.WI_Pkey_ID;
                                                fileUploadDownloadDTO.PPW_Number = nfrNumber;
                                                fileUploadDownloadDTO.DirectoryPath = newdirectorypath;

                                                fileUploadDownload.UploadPPWphotos(fileUploadDownloadDTO);
                                                string DelImageFoldername = model.objectName + "/servicelink_" + nfrNumber + "/" + zipfilename;
                                                storage.DeleteObject(BucketName, DelImageFoldername);
                                            }


                                        }



                                    }

                                }

                                #endregion

                            }
                        }
                    }
                }
                int countworkorder = workobjdata.Count;
                string strworkOrder = string.Empty;

                Import_Queue_TransData import_Queue_TransData = new Import_Queue_TransData();
                Import_Queue_Trans_DTO import_Queue_Trans_DTO = new Import_Queue_Trans_DTO();
                import_Queue_Trans_DTO.Imrt_Wo_Import_ID = model.WI_Pkey_ID;
                import_Queue_Trans_DTO.Imrt_Status_Msg = model.Scrapper_rep_code;
                import_Queue_Trans_DTO.Type = 1;
                import_Queue_Trans_DTO.Imrt_Import_From_ID = 12;
                if (Flag == 2)
                {
                    import_Queue_TransData.UpdateAutoImportQueueTransaction(import_Queue_Trans_DTO);
                }
                else
                {
                    import_Queue_Trans_DTO.UserID = model.UserID;
                    import_Queue_TransData.UpdateAutoImportQueueTransaction(import_Queue_Trans_DTO);
                    //import_Queue_TransData.AddUpdateImportQueueTransData(import_Queue_Trans_DTO);
                }

                log.logDebugMessage("----------------Bucket Called End----------------");


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);

            }
            return objdata;
        }

        //Get file details for Altisource

        public List<dynamic> AltisourceListObjects(FileUploadBucketDTO model, int Flag)
        {
            List<dynamic> objdata = new List<dynamic>();
            try
            {

                log.logDebugMessage("----------------Bucket Called Start----------------");
                List<dynamic> workobjdata = new List<dynamic>();
                WorkOrderMaster_ImportAPI_Data workOrderMaster_ImportAPI_Data = new WorkOrderMaster_ImportAPI_Data();
                string jsonpath = System.Configuration.ConfigurationManager.AppSettings["GooglebucketJsom"];

                string SaveImgPath = System.Configuration.ConfigurationManager.AppSettings["GooglePhotosSave"];
                string strURL = System.Configuration.ConfigurationManager.AppSettings["IntegrationURL"] + "downloadFromBucket";

                string BucketName = model.Import_BucketName;
                string BucketFolderName = model.Import_BucketFolderName;
                string DestBucketFolderName = model.Import_DestBucketFolderName;
                string PDFBucketFolderName = "Altisource/pdf";

                string DestObjectName = string.Empty;
                model.BucketName = BucketName;
                model.objectName = BucketFolderName;

                Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", jsonpath);

                FileUploadDownload fileUploadDownload = new FileUploadDownload();
                FileUploadDownloadDTO fileUploadDownloadDTO;

                var credential = GoogleCredential.GetApplicationDefault();
                var storage = StorageClient.Create();
                var countlistobj = storage.ListObjects(model.BucketName, model.objectName);



                if (countlistobj != null)
                {
                    var val = countlistobj.Count().ToString();
                    log.logDebugMessage("---------------Altisource Scrapper Records Found in Bucket count----------------" + countlistobj.Count().ToString());
                }
                else
                {
                    log.logDebugMessage("----------------No Scrapper Recrods Found in Bucket----------------");
                }


                foreach (var storageObject in storage.ListObjects(model.BucketName, model.objectName))
                {
                    List<dynamic> objDynamic = new List<dynamic>();

                    if (storageObject.Name != model.objectName + "/")
                    {
                        if (storageObject.Name.Split('/').Length > 1)
                        {

                            string chkfilenmae = storageObject.Name;
                            bool chkjson = Regex.IsMatch(chkfilenmae, ".json");
                            if (chkjson)
                            {

                                var pdfFilename = storageObject.Name.Split('/');
                                string pdf = pdfFilename[2].Split('.')[0];
                                var Filename = pdfFilename[2];
                                var pdfname = pdf + ".pdf";
                                //string nfrNumber = Filename.Split('.')[0].Split('_')[1];
                                string nfrNumber = Filename.Split('.')[0];
                                //string ppwname = "new/ ppw_67804	";




                                DestObjectName = DestBucketFolderName + "/" + Filename;
                                //model.localPath = SaveImgPath ?? Path.GetFileName(Filename);
                                model.localPath = SaveImgPath + "Altisource_" + Filename;
                                using (var outputFile = File.OpenWrite(model.localPath))
                                {

                                    storage.DownloadObject(model.BucketName, storageObject.Name, outputFile);
                                    outputFile.Close();
                                    outputFile.Dispose();

                                    storage.CopyObject(BucketName, storageObject.Name, BucketName, DestObjectName);
                                    #region FileAccess

                                    string ReqData = "{\r\n \"FileName\": \"" + Filename + "\",\r\n \"FolderName\": \"" + DestBucketFolderName + "\"\r\n,\r\n \"BucketName\": \"" + BucketName + "\"\r\n}";
                                    string FilePath = fileUploadDownload.CallAPI(strURL, ReqData);
                                    #endregion
                                    // pdf store
                                    ///

                                    //string ReqPdfData = "{\r\n \"FileName\": \"" + pdfname + "\",\r\n \"FolderName\": \"" + PDFBucketFolderName + "\"\r\n,\r\n \"BucketName\": \"" + BucketName + "\"\r\n}";
                                    //string PdfFilePath = fileUploadDownload.CallAPI(strURL, ReqPdfData);
                                    string ReqPdfData = string.Empty, PdfFilePath = string.Empty;
                                    //string path = "D:\\Projects\\PPW\\Code\\IPL_App\\IPL\\IPL\\Document\\JsonFile\\63588.json";
                                    long length = new FileInfo(model.localPath).Length;
                                    if (length > 0)
                                    {
                                        using (StreamReader sr = new StreamReader(model.localPath))
                                        //using (StreamReader sr = new StreamReader(path))
                                        {
                                            // Read the stream to a string, and write the string to the console.
                                            String Data = sr.ReadToEnd();
                                            //Data = Data.Replace("Additional Instructions", "AdditionalInstructions");
                                            objdata.Add(Data);

                                            log.logDebugMessage("------------------------Json Data Start------------------------------------");
                                            log.logDebugMessage("Altisource_" + Filename);
                                            log.logDebugMessage(Data);
                                            log.logDebugMessage("------------------------Json Data End------------------------------------");

                                            objDynamic = workOrderMaster_ImportAPI_Data.AltisourceWorkOrderItemDetails(Data, model.WI_Pkey_ID, Filename, FilePath, PdfFilePath, pdfname, model.UserID);
                                            workobjdata.Add(objDynamic);
                                            sr.Close();
                                            sr.Dispose();
                                        }
                                    }
                                    else
                                    {
                                        log.logDebugMessage("------------------------InValid File Size 0 -------------------" + Filename + "-----------------");
                                        //File.Delete(model.localPath);
                                    }


                                }
                                File.Delete(model.localPath);
                                storage.DeleteObject(BucketName, storageObject.Name);

                                #region Image Download code  
                                //Image Download code starts 

                                // string ImageFoldername = model.objectName + "/ppw_" + "67804"; /// for testing

                                if (model.image_download == true)
                                {

                                    string ImageFoldername = model.objectName + "/nfr_" + nfrNumber;
                                    string SubImageFoldername = "Altisource_" + nfrNumber;
                                    log.logDebugMessage("Image Folder Name SubImageFoldername -------->" + SubImageFoldername);
                                    var Imagefolderlist = storage.ListObjects(model.BucketName, model.objectName);
                                    string strPPWFoldername = string.Empty;

                                    foreach (var storageObjectval in Imagefolderlist)
                                    {
                                        if (storageObjectval.Name != model.objectName + "/")
                                        {
                                            var Filenamelist = storageObjectval.Name.Split('/');
                                            if (Filenamelist.Length > 1)
                                            {
                                                strPPWFoldername = storageObjectval.Name.Split('/')[1];
                                            }

                                            if (SubImageFoldername == strPPWFoldername)
                                            {
                                                ImageGenerator imageGenerator = new ImageGenerator();
                                                string vaal = storageObjectval.Name;
                                                string zipfilename = vaal.Split('/')[2];
                                                model.ImageZiplocalPath = SaveImgPath + zipfilename;
                                                if (File.Exists(model.ImageZiplocalPath))
                                                {
                                                    File.Delete(model.ImageZiplocalPath);
                                                }
                                                using (var outputFile = File.OpenWrite(model.ImageZiplocalPath))
                                                {
                                                    storage.DownloadObject(model.BucketName, vaal, outputFile);
                                                    outputFile.Close();
                                                    outputFile.Dispose();
                                                }


                                                string newdirectorypath = SaveImgPath + "Altisource_" + nfrNumber;
                                                if (Directory.Exists(newdirectorypath))
                                                {
                                                    Directory.Delete(newdirectorypath);
                                                    log.logDebugMessage("Delete Folder ------------------->" + "Altisource_" + nfrNumber);
                                                }

                                                Directory.CreateDirectory(newdirectorypath);

                                                fileUploadDownloadDTO = new FileUploadDownloadDTO();

                                                fileUploadDownloadDTO.zipPath = model.ImageZiplocalPath;
                                                fileUploadDownloadDTO.extractPath = newdirectorypath;

                                                fileUploadDownload.ExtractZipFile(fileUploadDownloadDTO);

                                                fileUploadDownloadDTO.importpkey = objDynamic[0];
                                                fileUploadDownloadDTO.WI_PkeyID = model.WI_Pkey_ID;
                                                fileUploadDownloadDTO.PPW_Number = nfrNumber;
                                                fileUploadDownloadDTO.DirectoryPath = newdirectorypath;

                                                fileUploadDownload.UploadPPWphotos(fileUploadDownloadDTO);
                                                string DelImageFoldername = model.objectName + "/Altisource_" + nfrNumber + "/" + zipfilename;
                                                storage.DeleteObject(BucketName, DelImageFoldername);
                                            }


                                        }



                                    }

                                }

                                #endregion

                            }
                        }
                    }
                }
                int countworkorder = workobjdata.Count;
                string strworkOrder = string.Empty;


                Import_Queue_TransData import_Queue_TransData = new Import_Queue_TransData();
                Import_Queue_Trans_DTO import_Queue_Trans_DTO = new Import_Queue_Trans_DTO();
                import_Queue_Trans_DTO.Imrt_Wo_Import_ID = model.WI_Pkey_ID;

                import_Queue_Trans_DTO.Type = 1;
                import_Queue_Trans_DTO.Imrt_Import_From_ID = 13;
                import_Queue_Trans_DTO.Imrt_Status_Msg = model.Scrapper_rep_code;
                if (Flag == 2)
                {
                    import_Queue_TransData.UpdateAutoImportQueueTransaction(import_Queue_Trans_DTO);
                }
                else
                {
                    import_Queue_Trans_DTO.UserID = model.UserID;
                    import_Queue_TransData.UpdateAutoImportQueueTransaction(import_Queue_Trans_DTO);
                    //import_Queue_TransData.AddUpdateImportQueueTransData(import_Queue_Trans_DTO);
                }

                log.logDebugMessage("----------------Bucket Called End----------------");


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);

            }
            return objdata;
        }

    }
}