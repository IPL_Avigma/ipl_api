﻿using Avigma.Repository.Lib;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using IPLApp.Models;
using IPL.Models;
using System.Threading.Tasks;


namespace IPL.Repository.data
{
    public class Message_WorkOrder_User_Data
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        SecurityHelper securityHelper = new SecurityHelper();
        private List<dynamic> CreateUpdate_Message_WorkOrder_User(Message_WorkOrder_User_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_Message_WorkOrder_User]";
            Message_WorkOrder_User_Data message_WorkOrder_User_Data = new Message_WorkOrder_User_Data();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@MWU_pkeyID", 1 + "#bigint#" + model.MWU_pkeyID);
                input_parameters.Add("@MWU_User_ID", 1 + "#bigint#" + model.MWU_User_ID);
                input_parameters.Add("@MWU_workOrder_ID", 1 + "#bigint#" + model.MWU_workOrder_ID);
                input_parameters.Add("@MWU_IsRead", 1 + "#bit#" + model.MWU_IsRead);
                input_parameters.Add("@MWU_Role", 1 + "#int#" + model.MWU_Role);
                input_parameters.Add("@MWU_IsActive", 1 + "#bit#" + model.MWU_IsActive);
                input_parameters.Add("@MWU_IsDelete", 1 + "#bit#" + model.MWU_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@MWU_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);
                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;



        }
        private DataSet Get_Message_WorkOrder_User(Message_WorkOrder_User_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Message_WorkOrder_User]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@MWU_pkeyID", 1 + "#bigint#" + model.MWU_pkeyID);
                input_parameters.Add("@MWU_User_ID", 1 + "#bigint#" + model.MWU_User_ID);
                input_parameters.Add("@MWU_workOrder_ID", 1 + "#bigint#" + model.MWU_workOrder_ID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }
        
        public List<dynamic> Get_Message_WorkOrder_UserDetails(Message_WorkOrder_User_DTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = Get_Message_WorkOrder_User(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Message_WorkOrder_User_DTO > Get_Details =
                   (from item in myEnumerableFeaprd
                    select new Message_WorkOrder_User_DTO
                    {
                        MWU_pkeyID = item.Field<Int64>("MWU_pkeyID"),
                        MWU_User_ID = item.Field<Int64>("MWU_User_ID"),
                        MWU_workOrder_ID = item.Field<Int64>("MWU_workOrder_ID"),
                        MWU_IsRead = item.Field<Boolean?>("MWU_IsRead"),
                        MWU_Role = item.Field<int?>("MWU_Role"),
                        MWU_IsActive = item.Field<Boolean?>("MWU_IsActive"),

                    }).ToList();

                objDynamic.Add(Get_Details);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        public List<dynamic> Create_Update_Message_WorkOrder_User(Message_WorkOrder_User_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();
           // Message_WorkOrder_User_DTO message_WorkOrder_User_DTO = new Message_WorkOrder_User_DTO();
            try
            {
                //for deleting
                model.Type = 5;
                objData = CreateUpdate_Message_WorkOrder_User(model);

                if (model.lstmessage_Admin_UserDTO != null)
                {
                    for (int i = 0; i < model.lstmessage_Admin_UserDTO.Count; i++)
                    {

                        model.MWU_User_ID = model.lstmessage_Admin_UserDTO[i].User_pkeyID;
                        model.MWU_workOrder_ID = model.lstmessage_Admin_UserDTO[i].MWU_workOrder_ID;
                        model.MWU_IsRead = model.lstmessage_Admin_UserDTO[i].MWU_IsRead;
                        model.MWU_IsActive = true;
                        model.MWU_Role = 1;
                        model.Type = 1;
                     
                        objData = CreateUpdate_Message_WorkOrder_User(model);
                    }
                }

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }
            return objData;
        }
        private DataSet Get_Message_AdminUser(Message_Admin_UserDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Message_AdminUser]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();


                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@MWU_workOrder_ID", 1 + "#bigint#" + model.MWU_workOrder_ID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }



            return ds;
        }
        
        public List<dynamic> Get_Message_AdminUserDetails(Message_Admin_UserDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = Get_Message_AdminUser(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Message_Admin_UserDTO> Get_details =
                   (from item in myEnumerableFeaprd
                    select new Message_Admin_UserDTO
                    {
                        User_pkeyID = item.Field<Int64>("User_pkeyID"),
                        User_FirstName = item.Field<String>("User_FirstName"),
                        User_LastName = item.Field<String>("User_LastName"),
                        User_LoginName = item.Field<String>("User_LoginName"),
                        MWU_workOrder_ID = item.Field<Int64?>("MWU_workOrder_ID"),
                        MWU_IsRead = item.Field<Boolean?>("MWU_IsRead"),
                        

                    }).ToList();

                objDynamic.Add(Get_details);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }

            return objDynamic;






        }
     



    }
}
