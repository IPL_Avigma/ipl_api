﻿using Avigma.Repository.Lib;
using IPLApp.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPLApp.Repository.data
{
    public class WorkTypeAutoInvItemData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public List<dynamic> AddWorkTypeAutoInvItemeData(WorkTypeAutoInvItemDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objwoInvData = new List<dynamic>();
            string insertProcedure = "[CreateUpdateWorkTypeAutoInvItem]";
            WorkTypeAutoInvItem workTypeAutoInvItem = new WorkTypeAutoInvItem();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Auto_Inv_Itm_PkeyID", 1 + "#bigint#" + model.Auto_Inv_Itm_PkeyID);
                input_parameters.Add("@Auto_Inv_Itm_ID", 1 + "#bigint#" + model.Auto_Inv_Itm_ID);
                input_parameters.Add("@Auto_Inv_Itm_Quantity", 1 + "#bigint#" + model.Auto_Inv_Itm_Quantity);
                input_parameters.Add("@Auto_Inv_Itm_Flag", 1 + "#int#" + model.Auto_Inv_Itm_Flag);
                input_parameters.Add("@Auto_Inv_Itm_WorkTypeID", 1 + "#bigint#" + model.Auto_Inv_Itm_WorkTypeID);
                input_parameters.Add("@Auto_Inv_Itm_IsActive", 1 + "#bit#" + model.Auto_Inv_Itm_IsActive);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Auto_Inv_Itm_PkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    workTypeAutoInvItem.Auto_Inv_Itm_PkeyID = "0";
                    workTypeAutoInvItem.Status = "0";
                    workTypeAutoInvItem.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    workTypeAutoInvItem.Auto_Inv_Itm_PkeyID = Convert.ToString(objData[0]);
                    workTypeAutoInvItem.Status = Convert.ToString(objData[1]);


                }
                objwoInvData.Add(workTypeAutoInvItem);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objwoInvData;



        }

        private DataSet GetWorkTypeAutoInvItemMaster(WorkTypeAutoInvItemDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_WorkTypeAutoInvItem]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Auto_Inv_Itm_PkeyID", 1 + "#bigint#" + model.Auto_Inv_Itm_PkeyID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetWorkTypeAutoInvItemDetails(WorkTypeAutoInvItemDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetWorkTypeAutoInvItemMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<WorkTypeAutoInvItemDTO> WorkTypeInvDetails =
                   (from item in myEnumerableFeaprd
                    select new WorkTypeAutoInvItemDTO
                    {
                        Auto_Inv_Itm_PkeyID = item.Field<Int64>("Auto_Inv_Itm_PkeyID"),
                        Auto_Inv_Itm_ID = item.Field<Int64?>("Auto_Inv_Itm_ID"),
                        Auto_Inv_Itm_Quantity = item.Field<Int64?>("Auto_Inv_Itm_Quantity"),
                        Auto_Inv_Itm_Flag = item.Field<int?>("Auto_Inv_Itm_Flag"),
                        Auto_Inv_Itm_WorkTypeID = item.Field<Int64?>("Auto_Inv_Itm_WorkTypeID"),
                        Auto_Inv_Itm_IsActive = item.Field<Boolean?>("Auto_Inv_Itm_IsActive"),

                    }).ToList();

                objDynamic.Add(WorkTypeInvDetails);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}