﻿
using Avigma.Repository.Lib;
using IPLApp.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class WorkOrderHistoryData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        private DataSet Get_WorkOrder_HistoryData(WorkOrderHistoryDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_WorkOrder_HistoryData]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@workOrder_ID", 1 + "#bigint#" + model.workOrder_ID);

                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        private DataSet Get_WorkOrder_HistoryPhotoData(WorkOrderHistoryPhotoDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_WorkOrder_HistoryPhotoData]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Client_Result_Photo_Wo_ID", 1 + "#bigint#" + model.Client_Result_Photo_Wo_ID);
                input_parameters.Add("@CRP_New_Task_Bid_pkeyID", 1 + "#bigint#" + model.CRP_New_Task_Bid_pkeyID);
                input_parameters.Add("@CRP_New_Status_Type", 1 + "#int#" + model.CRP_New_Status_Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);


                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }
        public List<WorkOrderHistoryPhotoDTO> GetWorkOrderHistoryPhotoData(int CRP_New_Status_Type, Int64 CRP_New_Task_Bid_pkeyID, Int64? UserID)
        {
            List<WorkOrderHistoryPhotoDTO> WorkOrderHistoryPhotoDTO = new List<WorkOrderHistoryPhotoDTO>(); ;
            WorkOrderHistoryPhotoDTO model = new WorkOrderHistoryPhotoDTO();
            try
            {
                model.CRP_New_Status_Type = CRP_New_Status_Type;
                model.CRP_New_Task_Bid_pkeyID = CRP_New_Task_Bid_pkeyID;
                model.Type = 1;
                model.UserID = Convert.ToInt64(UserID);
                DataSet ds = Get_WorkOrder_HistoryPhotoData(model);
                var Enumerablephoto = ds.Tables[0].AsEnumerable();

                WorkOrderHistoryPhotoDTO = (from item in Enumerablephoto
                                            select new WorkOrderHistoryPhotoDTO
                                            {

                                                Client_Result_Photo_ID = item.Field<Int64>("Client_Result_Photo_ID"),
                                                Client_Result_Photo_Wo_ID = item.Field<Int64?>("Client_Result_Photo_Wo_ID"),
                                                Client_Result_Photo_FileName = item.Field<String>("Client_Result_Photo_FileName"),
                                                Client_Result_Photo_FilePath = item.Field<String>("Client_Result_Photo_FilePath"),
                                                Client_Result_Photo_FileType = item.Field<String>("Client_Result_Photo_FileType"),
                                                Client_Result_Photo_FileSize = item.Field<String>("Client_Result_Photo_FileSize"),
                                                Client_Result_Photo_StatusType = item.Field<int?>("Client_Result_Photo_StatusType"),
                                                Client_Result_Photo_TaskLable_Name = item.Field<String>("Client_Result_Photo_TaskLable_Name"),
                                              
                                                Client_Result_Photo_BucketName = item.Field<String>("Client_Result_Photo_BucketName"),
                                              
                                                Client_Result_Photo_FolderName = item.Field<String>("Client_Result_Photo_FolderName"),
                                                Client_Result_Photo_GPSLatitude = item.Field<String>("Client_Result_Photo_GPSLatitude"),
                                                Client_Result_Photo_GPSLongitude = item.Field<String>("Client_Result_Photo_GPSLongitude"),
                                                Client_Result_Photo_UploadTimestamp = item.Field<DateTime?>("Client_Result_Photo_UploadTimestamp"),
                                                Client_Result_Photo_UploadBy = item.Field<String>("Client_Result_Photo_UploadBy"),
                                                Client_Result_Photo_Make = item.Field<String>("Client_Result_Photo_Make"),
                                                Client_Result_Photo_Model = item.Field<String>("Client_Result_Photo_Model"),
                                                Client_Result_Photo_UploadFrom = item.Field<String>("Client_Result_Photo_UploadFrom"),
                                                Client_Result_Photo_Caption = item.Field<String>("Client_Result_Photo_Caption"),
                                                Client_Result_Photo_DateTimeOriginal = item.Field<DateTime?>("Client_Result_Photo_Caption"),
                                                Client_Result_Photo_FlaggedTo = item.Field<String>("Client_Result_Photo_Caption"),
                                                
                                                

                                                CRP_New_pkeyId = item.Field<Int64?>("CRP_New_pkeyId"),
                                                CRP_New_Bid_Id = item.Field<Int64?>("CRP_New_Bid_Id"),
                                                CRP_New_Inv_Id = item.Field<Int64?>("CRP_New_Inv_Id"),
                                                CRP_New_Damage_Id = item.Field<Int64?>("CRP_New_Damage_Id"),
                                                CRP_WorkOrderPhotoLabel_ID = item.Field<Int64?>("CRP_WorkOrderPhotoLabel_ID"),
                                                CRP_Inspection_Id = item.Field<Int64?>("CRP_Inspection_Id"),
                                                CRP_New_Status_Type = item.Field<int>("CRP_New_Status_Type"),
                                                CRP_New_Task_Bid_pkeyID = item.Field<Int64?>("CRP_Inspection_Id"),


                                            }).ToList();

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return WorkOrderHistoryPhotoDTO;
        }

        public List<dynamic> Get_WorkOrderHistoryDetails(WorkOrderHistoryDTO model)

        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = Get_WorkOrder_HistoryData(model);

                log.logDebugMessage(ds.Tables.Count.ToString());

                if (ds.Tables.Count > 0)
                {


                    var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                    List<WorkOrderbidHistoryDTO> WorkOrderHistoryDetails =
                       (from item in myEnumerableFeaprd
                        select new WorkOrderbidHistoryDTO
                        {
                            Task_Bid_pkeyID = item.Field<Int64>("Task_Bid_pkeyID"),
                            Task_Bid_TaskID = item.Field<Int64?>("Task_Bid_TaskID"),
                            Task_Name = item.Field<String>("Task_Name"),
                            Task_Bid_WO_ID = item.Field<Int64?>("Task_Bid_WO_ID"),
                            Task_Bid_Qty = item.Field<String>("Task_Bid_Qty"),
                            Task_Bid_Uom_ID = item.Field<Int64?>("Task_Bid_Uom_ID"),
                            UOM_Name = item.Field<String>("UOM_Name"),
                            Task_Bid_Cont_Price = item.Field<Decimal?>("Task_Bid_Cont_Price"),
                            Task_Bid_Cont_Total = item.Field<Decimal?>("Task_Bid_Cont_Total"),
                            Task_Bid_Clnt_Price = item.Field<Decimal?>("Task_Bid_Clnt_Price"),
                            Task_Bid_Clnt_Total = item.Field<Decimal?>("Task_Bid_Clnt_Total"),
                            Task_Bid_Comments = item.Field<String>("Task_Bid_Comments"),
                            Task_Bid_Violation_val = item.Field<String>("Task_Bid_Violation_val"),
                            Task_Bid_damage = item.Field<Boolean?>("Task_Bid_damage"),
                            Task_Bid_damage_val = item.Field<String>("Task_Bid_damage_val"),
                            Task_Bid_Status = item.Field<int?>("Task_Bid_Status"),
                            Task_Bid_Instr_pkeyId = item.Field<Int64?>("Task_Bid_Instr_pkeyId"),
                            workOrderNumber = item.Field<String>("workOrderNumber"),
                            IPLNO = item.Field<String>("IPLNO"),
                            CORNT_User_FirstName = item.Field<String>("CORNT_User_FirstName"),
                            Task_Bid_Status_val = item.Field<String>("Task_Bid_Status_val"),
                            CountPhotos = item.Field<int>("CountPhotos"),
                            workOrderHistoryPhotoDTOs = GetWorkOrderHistoryPhotoData(4, item.Field<Int64>("Task_Bid_pkeyID"), model.UserID),
                            WT_WorkType = item.Field<String>("WT_WorkType"),
                            BIDDate = item.Field<DateTime?>("BIDDate"),

                        }).ToList();

                    objDynamic.Add(WorkOrderHistoryDetails);
                }
                //}

                if (ds.Tables.Count > 1)
                {


                    var MyEnumerableFeaprd = ds.Tables[1].AsEnumerable();
                    List<WorkOrderHistoryCompletionDTO> WorkOrderHistoryCompletionDetails =
                       (from item in MyEnumerableFeaprd
                        select new WorkOrderHistoryCompletionDTO
                        {
                            Task_Inv_pkeyID = item.Field<Int64>("Task_Inv_pkeyID"),
                            Task_Inv_TaskID = item.Field<Int64?>("Task_Inv_TaskID"),
                            Task_Name = item.Field<String>("Task_Name"),
                            Task_Inv_WO_ID = item.Field<Int64?>("Task_Inv_WO_ID"),
                            Task_Inv_Qty = item.Field<String>("Task_Inv_Qty"),
                            Task_Inv_Uom_ID = item.Field<Int64?>("Task_Inv_Uom_ID"),
                            UOM_Name = item.Field<String>("UOM_Name"),
                            Task_Inv_Cont_Price = item.Field<Decimal?>("Task_Inv_Cont_Price"),
                            Task_Inv_Cont_Total = item.Field<Decimal?>("Task_Inv_Cont_Total"),
                            Task_Inv_Clnt_Price = item.Field<Decimal?>("Task_Inv_Clnt_Price"),
                            Task_Inv_Clnt_Total = item.Field<Decimal?>("Task_Inv_Clnt_Total"),
                            Task_Inv_Comments = item.Field<String>("Task_Inv_Comments"),
                            Task_Inv_Violation_val = item.Field<String>("Task_Inv_Violation_val"),
                            Task_Inv_damage_val = item.Field<String>("Task_Inv_damage_val"),
                            Task_Inv_Auto_Invoice_val = item.Field<String>("Task_Inv_Auto_Invoice_val"),
                            Task_Inv_Status = item.Field<int?>("Task_Inv_Status"),
                            Task_Inv_Instr_pkeyId = item.Field<Int64?>("Task_Inv_Instr_pkeyId"),
                            INVDate = item.Field<DateTime?>("INVDate"),
                            workOrderNumber = item.Field<String>("workOrderNumber"),
                            IPLNO = item.Field<String>("IPLNO"),
                            CORNT_User_FirstName = item.Field<String>("CORNT_User_FirstName"),
                            Task_Inv_Status_val = item.Field<String>("Task_Inv_Status_val"),
                            CountPhotos = item.Field<int>("CountPhotos"),
                            workOrderHistoryPhotoDTOs = GetWorkOrderHistoryPhotoData(1, item.Field<Int64>("Task_Inv_pkeyID"), model.UserID),
                            WT_WorkType = item.Field<String>("WT_WorkType"),

                        }).ToList();

                    objDynamic.Add(WorkOrderHistoryCompletionDetails);
                    // }
                }
                if (ds.Tables.Count > 2)
                {


                    var MYEnumerableFeaprd = ds.Tables[2].AsEnumerable();
                    List<WorkOrderHistoryDamageDTO> WorkOrderHistoryDamageDetails =
                       (from item in MYEnumerableFeaprd
                        select new WorkOrderHistoryDamageDTO
                        {
                            Task_Damage_pkeyID = item.Field<Int64>("Task_Damage_pkeyID"),
                            Task_Damage_WO_ID = item.Field<Int64?>("Task_Damage_WO_ID"),
                            Task_Damage_Task_ID = item.Field<Int64?>("Task_Damage_Task_ID"),
                            Task_Damage_ID = item.Field<Int64?>("Task_Damage_ID"),
                            Task_Damage_Type = item.Field<String>("Task_Damage_Type"),
                                //Task_Damage_Int = item.Field<String>("Task_Damage_Int"),
                                Task_Damage_Int_val = item.Field<String>("Task_Damage_Int_val"),
                            Task_Damage_Location = item.Field<String>("Task_Damage_Location"),
                            Task_Damage_Qty = item.Field<String>("Task_Damage_Qty"),
                            Task_Damage_Estimate = item.Field<String>("Task_Damage_Estimate"),
                            Task_Damage_Disc = item.Field<String>("Task_Damage_Disc"),
                            Damage_Type = item.Field<String>("Damage_Type"),
                            Task_Damage_Status = item.Field<int?>("Task_Damage_Status"),
                            DamageDate = item.Field<DateTime?>("DamageDate"),
                            IPLNO = item.Field<String>("IPLNO"),
                            workOrderNumber = item.Field<String>("workOrderNumber"),
                            CORNT_User_FirstName = item.Field<String>("CORNT_User_FirstName"),
                            Task_Damage_Status_val = item.Field<String>("Task_Damage_Status_val"),
                            CountPhotos = item.Field<int>("CountPhotos"),
                            workOrderHistoryPhotoDTOs = GetWorkOrderHistoryPhotoData(5, item.Field<Int64>("Task_Damage_pkeyID"), model.UserID),
                            WT_WorkType = item.Field<String>("WT_WorkType"),
                        }).ToList();

                    objDynamic.Add(WorkOrderHistoryDamageDetails);
                    // }
                }
                if (ds.Tables.Count > 3)
                {


                    var MYEnumerablefeaprd = ds.Tables[3].AsEnumerable();
                    List<WorkOrderHistoryApplianceDTO> WorkOrderHistoryApplianceDetails =
                       (from item in MYEnumerablefeaprd
                        select new WorkOrderHistoryApplianceDTO
                        {
                            Appl_pkeyId = item.Field<Int64>("Appl_pkeyId"),
                            Appl_Wo_Id = item.Field<Int64?>("Appl_Wo_Id"),
                            Appl_App_Id = item.Field<Int64?>("Appl_App_Id"),
                            Appl_Apliance_Name = item.Field<String>("Appl_Apliance_Name"),
                            Appl_Comment = item.Field<String>("Appl_Comment"),
                                // Appl_Status_Id = item.Field<int?>("Appl_Status_Id"),
                                ApplDate = item.Field<DateTime?>("ApplDate"),
                            Appl_Status_Id_val = item.Field<String>("Appl_Status_Id_val"),
                            Appl_IsActive = item.Field<Boolean?>("Appl_IsActive"),
                            Appl_IsDelete = item.Field<Boolean?>("Appl_IsDelete"),


                        }).ToList();

                    objDynamic.Add(WorkOrderHistoryApplianceDetails);
                    // }
                }

                for (int i = 4; i < ds.Tables.Count; i++)
                {
                    objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                }

            }


            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);

            }

            return objDynamic;
        }
    }
}

