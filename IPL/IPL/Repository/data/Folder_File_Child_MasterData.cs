﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class Folder_File_Child_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        private DataSet FilefolderData(Folder_Get_Parent_MasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Folder_File_Child_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Client_ID", 1 + "#bigint#" + model.Client_ID);
                input_parameters.Add("@Type", 1 + "#bigint#" + model.Type);


                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        private Folder_Get_Parent_MasterDTO GetFolderData(DataRow dr)
        {
            Folder_Get_Parent_MasterDTO folder_Parent_MasterDTO = new Folder_Get_Parent_MasterDTO();

            try
            {
                if (!string.IsNullOrEmpty(dr["Fold_Pkey_Id"].ToString()))
                {
                    folder_Parent_MasterDTO.Fold_Pkey_Id = Convert.ToInt64(dr["Fold_Pkey_Id"].ToString());
                }
                if (!string.IsNullOrEmpty(dr["Fold_Parent_Id"].ToString()))
                {
                    folder_Parent_MasterDTO.Fold_Parent_Id = Convert.ToInt64(dr["Fold_Parent_Id"].ToString());
                }

                folder_Parent_MasterDTO.Fold_Name = dr["Fold_Name"].ToString();
                folder_Parent_MasterDTO.Fold_Desc = dr["Fold_Desc"].ToString(); //Added By Dipali
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }





            return folder_Parent_MasterDTO;
        }

        //for file data

        private Folder_File_MasterDTO GetFolderFileData(DataRow dr)
        {
            Folder_File_MasterDTO folder_File_MasterDTO = new Folder_File_MasterDTO();

            try
            {
                if (!string.IsNullOrEmpty(dr["Fold_File_Pkey_Id"].ToString()))
                {
                    folder_File_MasterDTO.Fold_File_Pkey_Id = Convert.ToInt64(dr["Fold_File_Pkey_Id"].ToString());
                }
                if (!string.IsNullOrEmpty(dr["Fold_File_ParentId"].ToString()))
                {
                    folder_File_MasterDTO.Fold_File_ParentId = Convert.ToInt64(dr["Fold_File_ParentId"].ToString());
                }
                if (!string.IsNullOrEmpty(dr["Fold_File_Role_Folder_Id"].ToString()))
                {
                    folder_File_MasterDTO.Fold_File_Role_Folder_Id = Convert.ToInt64(dr["Fold_File_Role_Folder_Id"].ToString());
                }

                folder_File_MasterDTO.Fold_File_Name = dr["Fold_File_Name"].ToString();
                folder_File_MasterDTO.Fold_File_Local_Path = dr["Fold_File_Local_Path"].ToString();
                folder_File_MasterDTO.Fold_File_Bucket_Name = dr["Fold_File_Bucket_Name"].ToString();
                folder_File_MasterDTO.Fold_File_ProjectId = dr["Fold_File_ProjectId"].ToString();
                folder_File_MasterDTO.Fold_File_Object_Name = dr["Fold_File_Object_Name"].ToString();
                folder_File_MasterDTO.Fold_File_Folder_Name = dr["Fold_File_Folder_Name"].ToString();
                folder_File_MasterDTO.Fold_File_Desc = dr["Fold_File_Desc"].ToString(); //Added By Dipali
                folder_File_MasterDTO.Fold_File_CreatedBy = dr["Fold_File_CreatedBy"].ToString();
                folder_File_MasterDTO.Fold_File_ModifiedBy = dr["Fold_File_ModifiedBy"].ToString();
                folder_File_MasterDTO.Fold_Is_AutoAssign = Convert.ToBoolean(dr["Fold_Is_AutoAssign"].ToString());
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return folder_File_MasterDTO;
        }


        public List<dynamic> Get_FileFolderDataForMobile(Folder_Get_Parent_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {

                DataSet ds = FilefolderData(model);
                if (ds.Tables.Count > 0)
                {
                    var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                    List<Folder_Get_Parent_MasterDTO_Mobile> folderList =
                       (from item in myEnumerableFeaprd
                        select new Folder_Get_Parent_MasterDTO_Mobile
                        {
                            Fold_Pkey_Id = item.Field<Int64>("Fold_Pkey_Id"),
                            Fold_Name = item.Field<String>("Fold_Name"),
                            Fold_Parent_Id = item.Field<Int64?>("Fold_Parent_Id"),
                            Fold_Desc = item.Field<String>("Fold_Desc"),
                            File_Data = item.Field<String>("File_Data"),
                            Level = item.Field<int>("Level"),

                        }).ToList();

                    var result = GetParentChildListForMobile(folderList, ds);
                    objDynamic.Add(result);

                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objDynamic;
        }


        public List<Folder_Get_Parent_MasterDTO_Mobile> GetParentChildListForMobile(IEnumerable<Folder_Get_Parent_MasterDTO_Mobile> list, DataSet ds, Int64 parentId = 0)
        {
            return (from i in list
                    where i.Fold_Parent_Id == parentId
                    select new Folder_Get_Parent_MasterDTO_Mobile
                    {
                        Fold_Pkey_Id = i.Fold_Pkey_Id,
                        Fold_Parent_Id = i.Fold_Parent_Id,
                        Fold_Name = i.Fold_Name,
                        Fold_Desc = i.Fold_Desc,
                        File_Data = i.File_Data,
                        Level = i.Level,
                        lstFolder_Get_Parent_MasterDTO = GetParentChildListForMobile(list, ds, i.Fold_Pkey_Id)
                    }).ToList();
        }

        public List<dynamic> Get_FileFolderData(Folder_Get_Parent_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = FilefolderData(model);
                if (ds.Tables.Count > 0)
                {
                    var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                    List<Folder_Get_Parent_MasterDTO> folderList =
                       (from item in myEnumerableFeaprd
                        select new Folder_Get_Parent_MasterDTO
                        {
                            Fold_Pkey_Id = item.Field<Int64>("Fold_Pkey_Id"),
                            Fold_Name = item.Field<String>("Fold_Name"),
                            Fold_Parent_Id = item.Field<Int64?>("Fold_Parent_Id"),
                            Fold_Desc = item.Field<String>("Fold_Desc"),
                            Fold_CreatedBy = item.Field<String>("Fold_CreatedBy"),
                            Fold_ModifiedBy = item.Field<String>("Fold_ModifiedBy"),

                        }).ToList();

                    var result = GetParentChildList(folderList, ds);
                    objDynamic.Add(result);
                    //DataRow[] rootFolder = ds.Tables[0].Select("Level = 1");
                    //for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    //{
                    //    Folder_Get_Parent_MasterDTO Parentfolder_Get_Parent_MasterDTO = new Folder_Get_Parent_MasterDTO();
                    //    List<Folder_Get_Parent_MasterDTO> lstFolder_Get_Parent_MasterDTO = new List<Folder_Get_Parent_MasterDTO>();
                    //    List<Folder_File_MasterDTO> lstFolder_File_MasterDTO = new List<Folder_File_MasterDTO>();

                    //    Parentfolder_Get_Parent_MasterDTO.Fold_Pkey_Id = Convert.ToInt64(ds.Tables[0].Rows[i]["Fold_Pkey_Id"].ToString());
                    //    Parentfolder_Get_Parent_MasterDTO.Fold_Parent_Id = Convert.ToInt64(ds.Tables[0].Rows[i]["Fold_Parent_Id"].ToString());
                    //    Parentfolder_Get_Parent_MasterDTO.Fold_Name = ds.Tables[0].Rows[i]["Fold_Name"].ToString();
                    //    Parentfolder_Get_Parent_MasterDTO.Fold_Desc = ds.Tables[0].Rows[i]["Fold_Desc"].ToString(); //Added By Dipali

                    //    DataRow[] drFile = ds.Tables[2].Select("Fold_File_ParentId = " + Parentfolder_Get_Parent_MasterDTO.Fold_Pkey_Id);
                    //    foreach (var itemfile in drFile)
                    //    {
                    //        Folder_File_MasterDTO folder_File_MasterDTO = new Folder_File_MasterDTO();
                    //        folder_File_MasterDTO = GetFolderFileData(itemfile);
                    //        lstFolder_File_MasterDTO.Add(folder_File_MasterDTO);
                    //    }

                    //    DataRow[] drchild = ds.Tables[1].Select("Fold_Parent_Id = " + Parentfolder_Get_Parent_MasterDTO.Fold_Pkey_Id);
                    //    foreach (var itemchild in drchild)
                    //    {
                    //        Folder_Get_Parent_MasterDTO childfolder_Get_Parent_MasterDTO = new Folder_Get_Parent_MasterDTO();
                    //        List<Folder_File_MasterDTO> lstchildFolder_File_MasterDTO = new List<Folder_File_MasterDTO>();
                    //        childfolder_Get_Parent_MasterDTO = GetFolderData(itemchild);


                    //        DataRow[] drchildfile = ds.Tables[2].Select("Fold_File_ParentId = " + childfolder_Get_Parent_MasterDTO.Fold_Pkey_Id);
                    //        foreach (var itemchildfile in drchildfile)
                    //        {
                    //            Folder_File_MasterDTO folder_File_MasterDTO = new Folder_File_MasterDTO();
                    //            folder_File_MasterDTO = GetFolderFileData(itemchildfile);
                    //            lstchildFolder_File_MasterDTO.Add(folder_File_MasterDTO);
                    //        }
                    //        lstFolder_Get_Parent_MasterDTO.Add(childfolder_Get_Parent_MasterDTO);
                    //        childfolder_Get_Parent_MasterDTO.lstFolder_File_MasterDTO = lstchildFolder_File_MasterDTO;

                    //    }
                    //    Parentfolder_Get_Parent_MasterDTO.lstFolder_Get_Parent_MasterDTO = lstFolder_Get_Parent_MasterDTO;
                    //    Parentfolder_Get_Parent_MasterDTO.lstFolder_File_MasterDTO = lstFolder_File_MasterDTO;

                    //    objDynamic.Add(Parentfolder_Get_Parent_MasterDTO);
                    //}
                }
                else
                {
                    log.logDebugMessage("No Data Found in Child Folder");
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;

        }

        public List<Folder_Get_Parent_MasterDTO> GetParentChildList(IEnumerable<Folder_Get_Parent_MasterDTO> list, DataSet ds, Int64 parentId = 0)
        {
            return (from i in list
                    where i.Fold_Parent_Id == parentId
                    select new Folder_Get_Parent_MasterDTO
                    {
                        Fold_Pkey_Id = i.Fold_Pkey_Id,
                        Fold_Parent_Id = i.Fold_Parent_Id,
                        Fold_Name = i.Fold_Name,
                        Fold_Desc = i.Fold_Desc,
                        lstFolder_File_MasterDTO = GetFolderFile(ds, i.Fold_Pkey_Id),
                        Fold_CreatedBy = i.Fold_CreatedBy,
                        Fold_ModifiedBy = i.Fold_ModifiedBy,

                        lstFolder_Get_Parent_MasterDTO = GetParentChildList(list, ds, i.Fold_Pkey_Id)
                    }).ToList();
        }

        public List<Folder_File_MasterDTO> GetFolderFile(DataSet ds, Int64 parentId)
        {
            DataRow[] drFile = ds.Tables[1].Select("Fold_File_ParentId = " + parentId);
            List<Folder_File_MasterDTO> fileList = new List<Folder_File_MasterDTO>();
            foreach (var itemfile in drFile)
            {
                Folder_File_MasterDTO folder_File_MasterDTO = new Folder_File_MasterDTO();
                folder_File_MasterDTO = GetFolderFileData(itemfile);
                fileList.Add(folder_File_MasterDTO);
            }
            return fileList;
        }
    }



}