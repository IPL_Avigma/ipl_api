﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class MainCategory_ConfigurationData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public List<dynamic> AddMainCategoroy_ConfigurationData(MainCategoroy_Configuration_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objAddData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_MainCategoroy_Configuration_Master]";
            MainCategoroy_Configuration_Master mainCategoroy_Configuration_Master = new MainCategoroy_Configuration_Master();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@MainCategoroy_Configuration_PkeyId", 1 + "#bigint#" + model.MainCategoroy_Configuration_PkeyId);
                input_parameters.Add("@MainCategoroy_Configuration_MainCategoroy_Id", 1 + "#bigint#" + model.MainCategoroy_Configuration_MainCategoroy_Id);
                input_parameters.Add("@MainCategoroy_Configuration_CategoryCode_Id", 1 + "#bigint#" + model.MainCategoroy_Configuration_CategoryCode_Id);
                input_parameters.Add("@MainCategoroy_Configuration_Import_From_Id", 1 + "#bigint#" + model.MainCategoroy_Configuration_Import_From_Id);
                input_parameters.Add("@MainCategoroy_Configuration_Client_Id", 1 + "#bigint#" + model.MainCategoroy_Configuration_Client_Id);
                input_parameters.Add("@MainCategoroy_Configuration_Company_Id", 1 + "#bigint#" + model.MainCategoroy_Configuration_Company_Id);
                input_parameters.Add("@MainCategoroy_Configuration_User_Id", 1 + "#bigint#" + model.MainCategoroy_Configuration_User_Id);
                input_parameters.Add("@MainCategoroy_Configuration_IsActive", 1 + "#bit#" + model.MainCategoroy_Configuration_IsActive);
                input_parameters.Add("@MainCategoroy_Configuration_IsDelete", 1 + "#bit#" + model.MainCategoroy_Configuration_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@MainCategoroy_Configuration_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    mainCategoroy_Configuration_Master.MainCategoroy_Configuration_PkeyId = "0";
                    mainCategoroy_Configuration_Master.Status = "0";
                    mainCategoroy_Configuration_Master.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    mainCategoroy_Configuration_Master.MainCategoroy_Configuration_PkeyId = Convert.ToString(objData[0]);
                    mainCategoroy_Configuration_Master.Status = Convert.ToString(objData[1]);


                }
                objAddData.Add(mainCategoroy_Configuration_Master);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objAddData;
        }

        private DataSet GetMainCategoroy_ConfigurationMaster(MainCategoroy_Configuration_MasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_MainCategoroy_Configuration_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@MainCategoroy_Configuration_PkeyId", 1 + "#bigint#" + model.MainCategoroy_Configuration_PkeyId);
                input_parameters.Add("@MainCategoroy_Configuration_Company_Id", 1 + "#bigint#" + model.MainCategoroy_Configuration_Company_Id);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return ds;
        }

        public List<dynamic> GetMainCategoroy_ConfigurationMasterDetails(MainCategoroy_Configuration_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetMainCategoroy_ConfigurationMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<MainCategoroy_Configuration_MasterDTO> Task_Configuration =
                   (from item in myEnumerableFeaprd
                    select new MainCategoroy_Configuration_MasterDTO
                    {
                        MainCategoroy_Configuration_PkeyId = item.Field<Int64>("MainCategoroy_Configuration_PkeyId"),
                        MainCategoroy_Configuration_MainCategoroy_Id = item.Field<Int64?>("MainCategoroy_Configuration_MainCategoroy_Id"),
                        Main_Cat_Name = item.Field<String>("Main_Cat_Name"),
                        MainCategoroy_Configuration_CategoryCode_Id = item.Field<Int64?>("MainCategoroy_Configuration_CategoryCode_Id"),
                        MainCategoroy_Configuration_Import_From_Id = item.Field<Int64?>("MainCategoroy_Configuration_Import_From_Id"),
                        MainCategoroy_Configuration_Client_Id = item.Field<Int64?>("MainCategoroy_Configuration_Client_Id"),
                        MainCategoroy_Configuration_Company_Id = item.Field<Int64?>("MainCategoroy_Configuration_Company_Id"),
                        MainCategoroy_Configuration_User_Id = item.Field<Int64?>("MainCategoroy_Configuration_User_Id"),
                        MainCategoroy_Configuration_IsActive = true
                    }).ToList();

                objDynamic.Add(Task_Configuration);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        public List<dynamic> PostMainCategoroyConfigurationMasterDetails(MainCategoroy_Configuration_ListDTO modelList)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                foreach (var model in modelList.MainCategoroy_Configuration_List)
                {
                    model.Type = model.MainCategoroy_Configuration_PkeyId > 0 ? 2 : 1;
                    model.MainCategoroy_Configuration_IsActive = true;
                    model.MainCategoroy_Configuration_IsDelete = false;
                    var objData = AddMainCategoroy_ConfigurationData(model);
                    objDynamic.Add(objData);
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}