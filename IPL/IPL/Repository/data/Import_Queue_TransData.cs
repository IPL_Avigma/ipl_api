﻿using Avigma.Repository.Lib;
using IPLApp.Repository.data;
using IPL.Models;
using IPLApp.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using System.Threading.Tasks;
using Avigma.Repository.Lib.FireBase;

namespace IPL.Repository.data
{
    public class Import_Queue_TransData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        WorkOrderMasterData WorkOrderMasterData = new WorkOrderMasterData();
        public List<dynamic> AddUpdateImportQueueTransData(Import_Queue_Trans_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objclntData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_Import_Queue_Transaction]";
            Import_Queue_Trans_Details import_Queue_Trans_Details = new Import_Queue_Trans_Details();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Imrt_PkeyId", 1 + "#bigint#" + model.Imrt_PkeyId);
                input_parameters.Add("@Imrt_Wo_ID", 1 + "#varchar#" + model.Imrt_Wo_ID);
                input_parameters.Add("@Imrt_Wo_Import_ID", 1 + "#bigint#" + model.Imrt_Wo_Import_ID);
                input_parameters.Add("@Imrt_Wo_Count", 1 + "#int#" + model.Imrt_Wo_Count);
                input_parameters.Add("@Imrt_Last_Run", 1 + "#datetime#" + model.Imrt_Last_Run);
                input_parameters.Add("@Imrt_Next_Run", 1 + "#datetime#" + model.Imrt_Next_Run);
                input_parameters.Add("@Imrt_Status_Msg", 1 + "#varchar#" + model.Imrt_Status_Msg);
                input_parameters.Add("@Imrt_State_Filter", 1 + "#varchar#" + model.Imrt_State_Filter);
                input_parameters.Add("@Imrt_Active", 1 + "#bit#" + model.Imrt_Active);
                input_parameters.Add("@Imrt_IsDelete", 1 + "#bit#" + model.Imrt_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Imrt_Import_From_ID", 1 + "#bigint#" + model.Imrt_Import_From_ID);
                input_parameters.Add("@Imrt_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    import_Queue_Trans_Details.Imrt_PkeyId = "0";
                    import_Queue_Trans_Details.Status = "0";
                    import_Queue_Trans_Details.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    import_Queue_Trans_Details.Imrt_PkeyId = Convert.ToString(objData[0]);
                    import_Queue_Trans_Details.Status = Convert.ToString(objData[1]);


                }
                objclntData.Add(import_Queue_Trans_Details);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objclntData;



        }

        private List<dynamic> Update_AutoImportQueueTransaction(Import_Queue_Trans_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objclntData = new List<dynamic>();
            string insertProcedure = "[Update_Auto_Import_Queue_Transaction]";
            Import_Queue_Trans_Details import_Queue_Trans_Details = new Import_Queue_Trans_Details();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Imrt_Import_From_ID", 1 + "#bigint#" + model.Imrt_Import_From_ID);
                input_parameters.Add("@Imrt_Wo_Import_ID", 1 + "#bigint#" + model.Imrt_Wo_Import_ID);
                input_parameters.Add("@Imrt_Status_Msg", 1 + "#varchar#" + model.Imrt_Status_Msg);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Imrt_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    import_Queue_Trans_Details.Imrt_PkeyId = "0";
                    import_Queue_Trans_Details.Status = "0";
                    import_Queue_Trans_Details.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    import_Queue_Trans_Details.Imrt_PkeyId = Convert.ToString(objData[0]);
                    import_Queue_Trans_Details.Status = Convert.ToString(objData[1]);


                }
                objclntData.Add(import_Queue_Trans_Details);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objclntData;



        }

        public List<dynamic> UpdateAutoImportQueueTransaction(Import_Queue_Trans_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                objData = Update_AutoImportQueueTransaction(model);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objData;
        }

        public List<dynamic> AddImportQueueTransData(Import_Queue_Trans_DTO model, int flag)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objDynamic = new List<dynamic>();
            List<dynamic> objDynamicStatus = new List<dynamic>();
            ScrapperResponseDTO scrapperResponseDTO = new ScrapperResponseDTO();
            try
            {
                objData = AddUpdateImportQueueTransData(model);

                if (flag == 1)
                {


                    WorkOrderImport_MasterData_DTO workOrderImport_MasterData_DTO = new WorkOrderImport_MasterData_DTO();
                    WorkOrderImport_MasterData workOrderImport_MasterData = new WorkOrderImport_MasterData();
                    workOrderImport_MasterData_DTO.Type = 5;
                    workOrderImport_MasterData_DTO.WI_Pkey_ID = Convert.ToInt64(model.Imrt_Wo_Import_ID);
                    workOrderImport_MasterData_DTO.UserId = model.UserID;
                    objDynamic = workOrderImport_MasterData.GetWorkOrderImportDetails(workOrderImport_MasterData_DTO);

                    if (objDynamic.Count > 0)
                    {
                        if (objDynamic[0].Count > 0)
                        {

                            for (int i = 0; i < objDynamic[0].Count; i++)
                            {
                                Import_Queue_Trans_DTO import_Queue_Trans_DTO = new Import_Queue_Trans_DTO();
                                GetWorkOrders getWorkOrders = new GetWorkOrders();
                                LoginDTO loginDTO = new LoginDTO();

                                loginDTO.username = objDynamic[0][i].WI_LoginName;
                                loginDTO.password = objDynamic[0][i].WI_Password;
                                loginDTO.rep_code = objDynamic[0][i].WI_Res_Code;
                                loginDTO.URL = objDynamic[0][i].Import_Form_URL_Name;
                                loginDTO.image_download = objDynamic[0][i].WI_ImageDownload;
                                loginDTO.WI_Pkey_ID = Convert.ToInt64(model.Imrt_Wo_Import_ID);
                                loginDTO.FBUsername = objDynamic[0][i].WI_FB_LoginName;
                                loginDTO.FBPassword = objDynamic[0][i].WI_FB_Password;
                                Int64 intimportfrom = objDynamic[0][i].WI_ImportFrom;
                                loginDTO.importfrom = intimportfrom;
                                model.Imrt_Import_From_ID = intimportfrom;
                                model.image_download = loginDTO.image_download;
                                model.Import_BucketName = objDynamic[0][i].Import_BucketName;
                                model.Import_BucketFolderName = objDynamic[0][i].Import_BucketFolderName;
                                model.Import_DestBucketFolderName = objDynamic[0][i].Import_DestBucketFolderName;
                                loginDTO.UserID = model.UserID;
                                switch (intimportfrom)
                                {
                                    case 1:
                                        {
                                            string strSchedulertime = System.Configuration.ConfigurationManager.AppSettings["AddSchedulertime"];
                                            int Schedulertime = Convert.ToInt32(strSchedulertime);
                                            objDynamicStatus =  getWorkOrders.GetPPWWorkOdersDetailsByUser(loginDTO);
                                            if (objDynamicStatus.Count == 1)
                                            {
                                                model.Imrt_Status_Msg = ((IPLApp.Models.ScrapperResponseDTO)objDynamicStatus[0]).Responseval;
                                                scrapperResponseDTO.Responseval = model.Imrt_Status_Msg;
                                                scrapperResponseDTO.ResponseMessage = GetScrapperErrorMessage(scrapperResponseDTO.Responseval);
                                            }
                                            if (model.Imrt_Status_Msg == "1")
                                            {
                                                DateTime dateTime = System.DateTime.Now.AddMinutes(Schedulertime);
                                                log.logDebugMessage("Manual scheduler Time" + dateTime.ToString());
                                                Avigma.Repository.Scheduler.PPWScheduleJobStart.Start(model, dateTime);
                                            }
                                            else
                                            {
                                                import_Queue_Trans_DTO.Imrt_Wo_Import_ID = loginDTO.WI_Pkey_ID;
                                                import_Queue_Trans_DTO.Type = 1;
                                                import_Queue_Trans_DTO.Imrt_Import_From_ID = 1;
                                                import_Queue_Trans_DTO.Imrt_Status_Msg = model.Imrt_Status_Msg;
                                                UpdateAutoImportQueueTransaction(import_Queue_Trans_DTO);
                                            }
                                            objData.Add(scrapperResponseDTO);
                                            break;
                                        }
                                    case 2:
                                        {
                                            getWorkOrders.GetWorkOdersDetails(loginDTO, 1);
                                            model.Type = 1;
                                            //objData = AddUpdateImportQueueTransData(model);
                                            objData = UpdateAutoImportQueueTransaction(model);
                                            
                                            break;
                                        }
                                    case 3:
                                        {
                                            string strSchedulertime = System.Configuration.ConfigurationManager.AppSettings["AddSchedulertime"];
                                            int Schedulertime = Convert.ToInt32(strSchedulertime);
                                            objDynamicStatus = getWorkOrders.GetMsiWorkOdersDetailsByUser(loginDTO);
                                            if (objDynamicStatus.Count == 1)
                                            {
                                                model.Imrt_Status_Msg = ((IPLApp.Models.ScrapperResponseDTO)objDynamicStatus[0]).Responseval;
                                                scrapperResponseDTO.Responseval = model.Imrt_Status_Msg;
                                                scrapperResponseDTO.ResponseMessage = GetScrapperErrorMessage(scrapperResponseDTO.Responseval);
                                            }
                                            if (model.Imrt_Status_Msg == "1")
                                            {
                                                DateTime dateTime = System.DateTime.Now.AddMinutes(Schedulertime);
                                                log.logDebugMessage("Manual scheduler Time" + dateTime.ToString());
                                                Avigma.Repository.Scheduler.PPWScheduleJobStart.Start(model, dateTime);
                                            }
                                            else
                                            {
                                                import_Queue_Trans_DTO.Imrt_Wo_Import_ID = loginDTO.WI_Pkey_ID;
                                                import_Queue_Trans_DTO.Type = 1;
                                                import_Queue_Trans_DTO.Imrt_Import_From_ID = 3;
                                                import_Queue_Trans_DTO.Imrt_Status_Msg = model.Imrt_Status_Msg;
                                                UpdateAutoImportQueueTransaction(import_Queue_Trans_DTO);
                                            }
                                            objData.Add(scrapperResponseDTO);
                                            break;
                                        }
                                    case 4:
                                        {
                                            objDynamicStatus = getWorkOrders.GetFBWorkOdersDetails(loginDTO, 1);
                                            model.Type = 1;
                                            if (objDynamicStatus.Count == 2)
                                            {
                                                model.Imrt_Status_Msg = ((IPLApp.Models.ScrapperResponseDTO)objDynamicStatus[1]).Responseval;
                                                scrapperResponseDTO.Responseval = model.Imrt_Status_Msg;
                                                scrapperResponseDTO.ResponseMessage = GetScrapperErrorMessage(scrapperResponseDTO.Responseval);
                                            }

                                            //objData = AddUpdateImportQueueTransData(model);
                                            objData = UpdateAutoImportQueueTransaction(model);
                                            log.logDebugMessage("FB AddUpdateImportQueueTransData End");
                                             objData.Add(scrapperResponseDTO);
                                            break;
                                        }
                                    case 6:
                                        {
                                            string strSchedulertime = System.Configuration.ConfigurationManager.AppSettings["AddSchedulertime"];
                                            int Schedulertime = Convert.ToInt32(strSchedulertime);
                                            objDynamicStatus = getWorkOrders.GetMCSWorkOdersDetailsByUser(loginDTO);
                                            if (objDynamicStatus.Count == 1)
                                            {
                                                model.Imrt_Status_Msg = ((IPLApp.Models.ScrapperResponseDTO)objDynamicStatus[0]).Responseval;
                                                scrapperResponseDTO.Responseval = model.Imrt_Status_Msg;
                                                scrapperResponseDTO.ResponseMessage = GetScrapperErrorMessage(scrapperResponseDTO.Responseval);
                                            }
                                            if (model.Imrt_Status_Msg == "1")
                                            {
                                                DateTime dateTime = System.DateTime.Now.AddMinutes(Schedulertime);
                                                log.logDebugMessage("Manual scheduler Time" + dateTime.ToString());
                                                Avigma.Repository.Scheduler.PPWScheduleJobStart.Start(model, dateTime);
                                            }
                                            else
                                            {
                                                import_Queue_Trans_DTO.Imrt_Wo_Import_ID = loginDTO.WI_Pkey_ID;
                                                import_Queue_Trans_DTO.Type = 1;
                                                import_Queue_Trans_DTO.Imrt_Import_From_ID = 6;
                                                import_Queue_Trans_DTO.Imrt_Status_Msg = model.Imrt_Status_Msg;
                                                UpdateAutoImportQueueTransaction(import_Queue_Trans_DTO);
                                            }
                                            objData.Add(scrapperResponseDTO);
                                            break;
                                        }
                                    case 7:
                                        {
                                            string strSchedulertime = System.Configuration.ConfigurationManager.AppSettings["AddSchedulertime"];
                                            int Schedulertime = Convert.ToInt32(strSchedulertime);
                                            objDynamicStatus = getWorkOrders.GetNFRWorkOdersDetailsByUser(loginDTO);
                                            if (objDynamicStatus.Count == 1)
                                            {
                                                model.Imrt_Status_Msg = ((IPLApp.Models.ScrapperResponseDTO)objDynamicStatus[0]).Responseval;
                                                scrapperResponseDTO.Responseval = model.Imrt_Status_Msg;
                                                scrapperResponseDTO.ResponseMessage = GetScrapperErrorMessage(scrapperResponseDTO.Responseval);
                                            }
                                            if (model.Imrt_Status_Msg == "1")
                                            {
                                                DateTime dateTime = System.DateTime.Now.AddMinutes(Schedulertime);
                                                log.logDebugMessage("Manual scheduler Time" + dateTime.ToString());
                                                Avigma.Repository.Scheduler.PPWScheduleJobStart.Start(model, dateTime);
                                            }
                                            else
                                            {
                                                import_Queue_Trans_DTO.Imrt_Wo_Import_ID = loginDTO.WI_Pkey_ID;
                                                import_Queue_Trans_DTO.Type = 1;
                                                import_Queue_Trans_DTO.Imrt_Import_From_ID = 7;
                                                import_Queue_Trans_DTO.Imrt_Status_Msg = model.Imrt_Status_Msg;
                                                UpdateAutoImportQueueTransaction(import_Queue_Trans_DTO);
                                            }
                                            objData.Add(scrapperResponseDTO);
                                            break;
                                        }
                                    case 10:
                                        {
                                            string strSchedulertime = System.Configuration.ConfigurationManager.AppSettings["AddSchedulertime"];
                                            int Schedulertime = Convert.ToInt32(strSchedulertime);
                                            objDynamicStatus = getWorkOrders.GetCyprexxWorkOdersDetailsByUser(loginDTO);
                                            if (objDynamicStatus.Count == 1)
                                            {
                                                model.Imrt_Status_Msg = ((IPLApp.Models.ScrapperResponseDTO)objDynamicStatus[0]).Responseval;
                                                scrapperResponseDTO.Responseval = model.Imrt_Status_Msg;
                                                scrapperResponseDTO.ResponseMessage = GetScrapperErrorMessage(scrapperResponseDTO.Responseval);
                                            }
                                            if (model.Imrt_Status_Msg == "1")
                                            {
                                                DateTime dateTime = System.DateTime.Now.AddMinutes(Schedulertime);
                                                log.logDebugMessage("Manual scheduler Time" + dateTime.ToString());
                                                Avigma.Repository.Scheduler.PPWScheduleJobStart.Start(model, dateTime);
                                            }
                                            else
                                            {
                                                import_Queue_Trans_DTO.Imrt_Wo_Import_ID = loginDTO.WI_Pkey_ID;
                                                import_Queue_Trans_DTO.Type = 1;
                                                import_Queue_Trans_DTO.Imrt_Import_From_ID = 10;
                                                import_Queue_Trans_DTO.Imrt_Status_Msg = model.Imrt_Status_Msg;
                                                UpdateAutoImportQueueTransaction(import_Queue_Trans_DTO);
                                            }
                                            objData.Add(scrapperResponseDTO);
                                            break;
                                        }

                                    case 12:
                                        {
                                            string strSchedulertime = System.Configuration.ConfigurationManager.AppSettings["AddSchedulertime"];
                                            int Schedulertime = Convert.ToInt32(strSchedulertime);
                                            objDynamicStatus = getWorkOrders.GetServiceLinkWorkOdersDetailsByUser(loginDTO);
                                            if (objDynamicStatus.Count == 1)
                                            {
                                                model.Imrt_Status_Msg = ((IPLApp.Models.ScrapperResponseDTO)objDynamicStatus[0]).Responseval;
                                                scrapperResponseDTO.Responseval = model.Imrt_Status_Msg;
                                                scrapperResponseDTO.ResponseMessage = GetScrapperErrorMessage(scrapperResponseDTO.Responseval);
                                            }
                                            if (model.Imrt_Status_Msg == "1")
                                            {
                                                DateTime dateTime = System.DateTime.Now.AddMinutes(Schedulertime);
                                                log.logDebugMessage("Manual scheduler Time" + dateTime.ToString());
                                                Avigma.Repository.Scheduler.PPWScheduleJobStart.Start(model, dateTime);
                                            }
                                            else
                                            {
                                                import_Queue_Trans_DTO.Imrt_Wo_Import_ID = loginDTO.WI_Pkey_ID;
                                                import_Queue_Trans_DTO.Type = 1;
                                                import_Queue_Trans_DTO.Imrt_Import_From_ID = 12;
                                                import_Queue_Trans_DTO.Imrt_Status_Msg = model.Imrt_Status_Msg;
                                                UpdateAutoImportQueueTransaction(import_Queue_Trans_DTO);
                                            }
                                            objData.Add(scrapperResponseDTO);
                                            break;
                                        }

                                    case 13:
                                        {
                                            string strSchedulertime = System.Configuration.ConfigurationManager.AppSettings["AddSchedulertime"];
                                            int Schedulertime = Convert.ToInt32(strSchedulertime);
                                            objDynamicStatus = getWorkOrders.GetAltisourceWorkOdersDetailsByUser(loginDTO);
                                            if (objDynamicStatus.Count == 1)
                                            {
                                                model.Imrt_Status_Msg = ((IPLApp.Models.ScrapperResponseDTO)objDynamicStatus[0]).Responseval;
                                                scrapperResponseDTO.Responseval = model.Imrt_Status_Msg;
                                                scrapperResponseDTO.ResponseMessage = GetScrapperErrorMessage(scrapperResponseDTO.Responseval);
                                            }
                                            if (model.Imrt_Status_Msg == "1")
                                            {
                                                DateTime dateTime = System.DateTime.Now.AddMinutes(Schedulertime);
                                                log.logDebugMessage("Manual scheduler Time" + dateTime.ToString());
                                                Avigma.Repository.Scheduler.PPWScheduleJobStart.Start(model, dateTime);
                                            }
                                            else
                                            {
                                                import_Queue_Trans_DTO.Imrt_Wo_Import_ID = loginDTO.WI_Pkey_ID;
                                                import_Queue_Trans_DTO.Type = 1;
                                                import_Queue_Trans_DTO.Imrt_Import_From_ID = 10;
                                                import_Queue_Trans_DTO.Imrt_Status_Msg = model.Imrt_Status_Msg;
                                                UpdateAutoImportQueueTransaction(import_Queue_Trans_DTO);
                                            }
                                            objData.Add(scrapperResponseDTO);
                                            break;
                                        }

                                    case 19:
                                        {
                                            string strSchedulertime = System.Configuration.ConfigurationManager.AppSettings["AddSchedulertime"];
                                            int Schedulertime = Convert.ToInt32(strSchedulertime);
                                            objDynamicStatus = getWorkOrders.GetAGWorkOdersDetailsByUser(loginDTO);
                                            if (objDynamicStatus.Count == 1)
                                            {
                                                model.Imrt_Status_Msg = ((IPLApp.Models.ScrapperResponseDTO)objDynamicStatus[0]).Responseval;
                                                scrapperResponseDTO.Responseval = model.Imrt_Status_Msg;
                                                scrapperResponseDTO.ResponseMessage = GetScrapperErrorMessage(scrapperResponseDTO.Responseval);
                                            }
                                            if (model.Imrt_Status_Msg == "1")
                                            {
                                                DateTime dateTime = System.DateTime.Now.AddMinutes(Schedulertime);
                                                log.logDebugMessage("Manual scheduler Time" + dateTime.ToString());
                                                Avigma.Repository.Scheduler.PPWScheduleJobStart.Start(model, dateTime);
                                            }
                                            else
                                            {
                                                import_Queue_Trans_DTO.Imrt_Wo_Import_ID = loginDTO.WI_Pkey_ID;
                                                import_Queue_Trans_DTO.Type = 1;
                                                import_Queue_Trans_DTO.Imrt_Import_From_ID = 6;
                                                import_Queue_Trans_DTO.Imrt_Status_Msg = model.Imrt_Status_Msg;
                                                UpdateAutoImportQueueTransaction(import_Queue_Trans_DTO);
                                            }
                                            objData.Add(scrapperResponseDTO);
                                            break;
                                        }
                                }
                            }

                        }
                    }

                    #region comment
                    //GoogleCloudData googleCloudData = new GoogleCloudData();
                    //FileUploadBucketDTO fileUploadBucketDTO = new FileUploadBucketDTO();
                    //var startTimeSpan = TimeSpan.FromMinutes(1);
                    //var periodTimeSpan = TimeSpan.FromMinutes(1);

                    //fileUploadBucketDTO.WI_Pkey_ID = Convert.ToInt64(model.Imrt_Wo_Import_ID);
                    //var timer = new System.Threading.Timer((e) =>
                    //{
                    //    log.logDebugMessage("-------------Manual Bucket Code Called Asyn ----------------");
                    //    googleCloudData.ListObjects(fileUploadBucketDTO);
                    //    objData = AddUpdateImportQueueTransData(model);



                    //}, null, startTimeSpan, periodTimeSpan);

                    #endregion

                }

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }


        //for import workorder data click on import button 
        public async Task<List<dynamic>> AddImportWorkOrderData(ImportWorkOrderNewDTO model)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                switch (model.Imrt_Import_From_ID)
                {

                    case 1: //PPW
                        {
                            objdynamicobj = AddImportPPWWorkOrderData(model);
                            if (objdynamicobj[0] != null)
                            {
                                var woId = ((IPL.Models.ImportWorkOrderNew)objdynamicobj[0]).Pkey_Id;
                                if (!string.IsNullOrWhiteSpace(woId))
                                {
                                    await AddFirebaseData(model, Convert.ToInt64(woId));
                                }
                            }
                           
                           
                            break;
                        }
                    case 2: //Purvan
                        {
                            objdynamicobj = AddImportPruvanWorkOrderMasterData(model);
                            if (objdynamicobj[0] != null)
                            {
                                var woId = ((IPL.Models.ImportWorkOrderNew)objdynamicobj[0]).Pkey_Id;
                                if (!string.IsNullOrWhiteSpace(woId))
                                {
                                    await AddFirebaseData(model, Convert.ToInt64(woId));
                                }
                            }
                            break;
                        }
                    case 3: // MSI
                        {
                            objdynamicobj = AddImportMSIWorkOrderMasterData(model);
                            if (objdynamicobj[0] != null)
                            {
                                var woId = ((IPL.Models.ImportWorkOrderNew)objdynamicobj[0]).Pkey_Id;
                                if (!string.IsNullOrWhiteSpace(woId))
                                {
                                    await AddFirebaseData(model, Convert.ToInt64(woId));
                                }
                            }
                            break;
                        }
                    case 4:// Five Brothers
                        {
                            objdynamicobj = AddImportFiveBrotherWorkOrderMasterData(model);
                            if (objdynamicobj[0] != null)
                            {
                                var woId = ((IPL.Models.ImportWorkOrderNew)objdynamicobj[0]).Pkey_Id;
                                if (!string.IsNullOrWhiteSpace(woId))
                                {
                                    await AddFirebaseData(model, Convert.ToInt64(woId));
                                }
                            }
                            break;
                        }
                    case 6: //MCS
                        {
                            objdynamicobj = AddImportMCSWorkOrderMasterData(model);
                            if (objdynamicobj[0] != null)
                            {
                                var woId = ((IPL.Models.ImportWorkOrderNew)objdynamicobj[0]).Pkey_Id;
                                if (!string.IsNullOrWhiteSpace(woId))
                                {
                                    await AddFirebaseData(model, Convert.ToInt64(woId));
                                }
                            }
                            break;

                        } // NFR
                    case 7:
                        {
                            objdynamicobj = AddImportNFRWorkOrderMasterData(model);
                            if (objdynamicobj[0] != null)
                            {
                                var woId = ((IPL.Models.ImportWorkOrderNew)objdynamicobj[0]).Pkey_Id;
                                if (!string.IsNullOrWhiteSpace(woId))
                                {
                                    await AddFirebaseData(model, Convert.ToInt64(woId));
                                }
                            }
                            break;
                        }

                    case 10: // Cyperx
                        {
                            objdynamicobj = AddImportCyprexxWorkOrderMasterData(model);
                            if (objdynamicobj[0] != null)
                            {
                                var woId = ((IPL.Models.ImportWorkOrderNew)objdynamicobj[0]).Pkey_Id;
                                if (!string.IsNullOrWhiteSpace(woId))
                                {
                                    await AddFirebaseData(model, Convert.ToInt64(woId));
                                }
                            }
                            break;
                            
                        }
                    case 12:  // Service Linl
                        {
                            objdynamicobj = AddImportServiceLinkWorkOrderMasterData(model);
                            if (objdynamicobj[0] != null)
                            {
                                var woId = ((IPL.Models.ImportWorkOrderNew)objdynamicobj[0]).Pkey_Id;
                                if (!string.IsNullOrWhiteSpace(woId))
                                {
                                    await AddFirebaseData(model, Convert.ToInt64(woId));
                                }
                            }
                            break;

                        }
                    case 13: //Altisource
                        {
                            objdynamicobj = AddImportAltisourceWorkOrderMasterData(model);
                            if (objdynamicobj[0] != null)
                            {
                                var woId = ((IPL.Models.ImportWorkOrderNew)objdynamicobj[0]).Pkey_Id;
                                if (!string.IsNullOrWhiteSpace(woId))
                                {
                                    await AddFirebaseData(model, Convert.ToInt64(woId));
                                }
                            }
                            break;

                        }
                    case 19: //AG
                        {
                            objdynamicobj = AddImportAGWorkOrderMasterData(model);
                            if (objdynamicobj[0] != null)
                            {
                                var woId = ((IPL.Models.ImportWorkOrderNew)objdynamicobj[0]).Pkey_Id;
                                if (!string.IsNullOrWhiteSpace(woId))
                                {
                                    await AddFirebaseData(model, Convert.ToInt64(woId));
                                }
                            }
                            break;

                        }
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage("Work Order Import Failed for " + model.Pkey_Id);
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objdynamicobj;

        }

        private DataSet GetImportQueueTrans(Import_Queue_Trans_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Import_Queue_Transaction]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Imrt_PkeyId", 1 + "#bigint#" + model.Imrt_PkeyId);
                input_parameters.Add("@WhereClause", 1 + "#varchar#" + model.WhereClause);
                input_parameters.Add("@PageNumber", 1 + "#int#" + model.PageNumber);
                input_parameters.Add("@NoofRows", 1 + "#int#" + model.NoofRows);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetImportQueueTransDetails(Import_Queue_Trans_DTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            string wherecondition = string.Empty;
            var Data = JsonConvert.DeserializeObject<Import_Queue_Trans_DTO>(model.FilterData);
            try
            {
                if (Data.ClientId > 0)
                {
                    wherecondition = wherecondition != null ? wherecondition + "  And wim.WI_SetClientCompany = '" + Data.ClientId + "'" : "  And wim.WI_SetClientCompany = '" + Data.ClientId + "'";
                }
                if (Data.WI_ImportFrom != null && Data.WI_ImportFrom > 0)
                {
                    wherecondition = wherecondition != null ? wherecondition + " And wim.WI_ImportFrom =    '" + Data.WI_ImportFrom + "'" : wherecondition + " And wim.WI_ImportFrom =    '" + Data.WI_ImportFrom + "'";
                }
                if (!string.IsNullOrEmpty(Data.WI_FriendlyName))
                {
                    wherecondition = wherecondition != null ? wherecondition + " And wim.WI_FriendlyName =    '" + Data.WI_FriendlyName + "'" : " And wim.WI_FriendlyName =    '" + Data.WI_FriendlyName + "'";
                }
                Import_Queue_Trans_DTO import_Queue_Trans_DTO = new Import_Queue_Trans_DTO();
                import_Queue_Trans_DTO.Imrt_PkeyId = model.Imrt_PkeyId;
                import_Queue_Trans_DTO.WhereClause = wherecondition;
                import_Queue_Trans_DTO.PageNumber = model.PageNumber;
                import_Queue_Trans_DTO.NoofRows = model.NoofRows;
                import_Queue_Trans_DTO.Type = model.Type;
                import_Queue_Trans_DTO.UserID = model.UserID;
                DataSet ds = GetImportQueueTrans(import_Queue_Trans_DTO);


                if (ds.Tables.Count > 0)
                {


                    var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                    List<Import_Queue_Trans_DTO> ImportQueue =
                       (from item in myEnumerableFeaprd
                        select new Import_Queue_Trans_DTO
                        {
                            Imrt_PkeyId = item.Field<Int64>("Imrt_PkeyId"),
                            Imrt_Wo_ID = item.Field<String>("Imrt_Wo_ID"),
                            Imrt_Wo_Import_ID = item.Field<Int64?>("Imrt_Wo_Import_ID"),
                            Client_Company_Name = item.Field<String>("Client_Company_Name"),
                            WI_FriendlyName = item.Field<String>("WI_FriendlyName"),
                            Imrt_Wo_Count = item.Field<int?>("Imrt_Wo_Count"),
                            Imrt_Last_Run = item.Field<DateTime?>("Imrt_Last_Run"),
                            Imrt_Next_Run = item.Field<DateTime?>("Imrt_Next_Run"),
                            Imrt_Status_Msg = item.Field<String>("Imrt_Status_Msg"),
                            Imrt_Status_Msg_code = item.Field<String>("Imrt_Status_Msg_code"),
                            Imrt_State_Filter = item.Field<String>("Imrt_State_Filter"),
                            Imrt_Active = item.Field<Boolean?>("Imrt_Active"),
                            Imrt_Import_From_ID = item.Field<Int64?>("Imrt_Import_From_ID"),
                            Imrt_NewOrderCheck = item.Field<Boolean>("Imrt_NewOrderCheck"),
                            Imrt_IPL_CompanyId = item.Field<Int64?>("Imrt_IPL_CompanyId"),

                        }).ToList();

                    objDynamic.Add(ImportQueue);

                    if (ds.Tables.Count > 1)
                    {
                        string Totalcount = ds.Tables[1].Rows[0]["count"].ToString();
                        objDynamic.Add(Totalcount);
                    }

                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        public List<dynamic> AddImportPPWWorkOrderData(ImportWorkOrderNewDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objclntData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_Import_WorkOrderMaster_Data]";
            ImportWorkOrderNew importWorkOrderNew = new ImportWorkOrderNew();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();

            log.logInfoMessage("Work Order Import Started for " + model.Pkey_Id);
            try
            {
                input_parameters.Add("@Pkey_Id", 1 + "#bigint#" + model.Pkey_Id);
                input_parameters.Add("@WorkType", 1 + "#bigint#" + model.WorkType);
                input_parameters.Add("@Contractor", 1 + "#varchar#" + model.Contractor);
                input_parameters.Add("@Processor", 1 + "#bigint#" + model.Processor);
                input_parameters.Add("@cordinator", 1 + "#bigint#" + model.cordinator);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Pkey_IdOut", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    importWorkOrderNew.Pkey_Id = "0";
                    importWorkOrderNew.Status = "0";
                    importWorkOrderNew.ErrorMessage = "Error while Saviving in the Database";

                    log.logErrorMessage("Work Order Import Falied for " + model.Pkey_Id);
                }
                else
                {
                    importWorkOrderNew.Pkey_Id = Convert.ToString(objData[0]);
                    importWorkOrderNew.Status = Convert.ToString(objData[1]);
                    log.logDebugMessage("Work Order Import Sucesss for " + model.Pkey_Id);
                    log.logDebugMessage("Work Order Import Status " + importWorkOrderNew.Status);

                }
                log.logDebugMessage("Work Order Import Sucesss for " + model.Pkey_Id);
                objclntData.Add(importWorkOrderNew);
            }
            catch (Exception ex)
            {
                log.logErrorMessage("Work Order Import Failed for " + model.Pkey_Id);
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objclntData;

        }
        public List<dynamic> AddImportPruvanWorkOrderMasterData(ImportWorkOrderNewDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objclntData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_Import_Pruvan_WorkOrderMaster_Data]";
            ImportWorkOrderNew importWorkOrderNew = new ImportWorkOrderNew();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();

            log.logInfoMessage("Work Order Import Started for " + model.Pkey_Id);
            try
            {
                input_parameters.Add("@Pkey_Id", 1 + "#bigint#" + model.Pkey_Id);
                input_parameters.Add("@WorkType", 1 + "#bigint#" + model.WorkType);
                input_parameters.Add("@Contractor", 1 + "#varchar#" + model.Contractor);
                input_parameters.Add("@Processor", 1 + "#bigint#" + model.Processor);
                input_parameters.Add("@cordinator", 1 + "#bigint#" + model.cordinator);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Pkey_IdOut", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    importWorkOrderNew.Pkey_Id = "0";
                    importWorkOrderNew.Status = "0";
                    importWorkOrderNew.ErrorMessage = "Error while Saviving in the Database";

                    log.logErrorMessage("Work Order Import Falied for " + model.Pkey_Id);
                }
                else
                {
                    importWorkOrderNew.Pkey_Id = Convert.ToString(objData[0]);
                    importWorkOrderNew.Status = Convert.ToString(objData[1]);
                    log.logDebugMessage("Work Order Import Sucesss for " + model.Pkey_Id);
                    log.logDebugMessage("Work Order Import Status " + importWorkOrderNew.Status);

                }
                log.logDebugMessage("Work Order Import Sucesss for " + model.Pkey_Id);
                objclntData.Add(importWorkOrderNew);
            }
            catch (Exception ex)
            {
                log.logErrorMessage("Work Order Import Failed for " + model.Pkey_Id);
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objclntData;

        }
        public List<dynamic> AddImportFiveBrotherWorkOrderMasterData(ImportWorkOrderNewDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objclntData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_Import_FiveBrother_WorkOrderMaster_Data]";
            ImportWorkOrderNew importWorkOrderNew = new ImportWorkOrderNew();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();

            log.logInfoMessage("Work Order Import Started for " + model.Pkey_Id);
            try
            {
                input_parameters.Add("@Pkey_Id", 1 + "#bigint#" + model.Pkey_Id);
                input_parameters.Add("@WorkType", 1 + "#bigint#" + model.WorkType);
                input_parameters.Add("@Contractor", 1 + "#varchar#" + model.Contractor);
                input_parameters.Add("@Processor", 1 + "#bigint#" + model.Processor);
                input_parameters.Add("@cordinator", 1 + "#bigint#" + model.cordinator);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Pkey_IdOut", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    importWorkOrderNew.Pkey_Id = "0";
                    importWorkOrderNew.Status = "0";
                    importWorkOrderNew.ErrorMessage = "Error while Saviving in the Database";

                    log.logDebugMessage("Work Order Import Falied for " + model.Pkey_Id);
                }
                else
                {
                    importWorkOrderNew.Pkey_Id = Convert.ToString(objData[0]);
                    importWorkOrderNew.Status = Convert.ToString(objData[1]);
                    log.logDebugMessage("Work Order Import Sucesss for " + model.Pkey_Id);
                    log.logDebugMessage("Work Order Import Status " + importWorkOrderNew.Status);

                }
                log.logDebugMessage("Work Order Import Sucesss for " + model.Pkey_Id);
                objclntData.Add(importWorkOrderNew);
            }
            catch (Exception ex)
            {
                log.logErrorMessage("Work Order Import Failed for " + model.Pkey_Id);
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objclntData;

        }
        public List<dynamic> AddUpdateAllImportQueueTransData(Import_Queue_Trans_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objclntData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_All_Import_Queue_Transaction]";
            Import_Queue_Trans_Details import_Queue_Trans_Details = new Import_Queue_Trans_Details();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Imrt_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    import_Queue_Trans_Details.Imrt_PkeyId = "0";
                    import_Queue_Trans_Details.Status = "0";
                    import_Queue_Trans_Details.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    import_Queue_Trans_Details.Imrt_PkeyId = Convert.ToString(objData[0]);
                    import_Queue_Trans_Details.Status = Convert.ToString(objData[1]);


                }
                objclntData.Add(import_Queue_Trans_Details);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objclntData;



        }

        public async Task AddFirebaseData(ImportWorkOrderNewDTO workOrder, Int64 workOrder_ID)
        {
            try
            {
                UserDetailDTO userDetailDTO = new UserDetailDTO();
                userDetailDTO.UserID = workOrder.UserID;
                if (workOrder.Contractor != "")
                {
                    userDetailDTO.ContractorID = Convert.ToInt64(workOrder.Contractor);
                }
                else
                {
                    userDetailDTO.ContractorID = 0;
                   
                }
                
                userDetailDTO.CordinatorID = workOrder.cordinator.GetValueOrDefault(0);
                userDetailDTO.ProcessorID = workOrder.Processor.GetValueOrDefault(0);
                userDetailDTO.WoID = workOrder_ID;
                userDetailDTO.Type = 1;

                var userDetail = WorkOrderMasterData.GetMessageUserDetail(userDetailDTO);
                WorkOrderUpdateStatusDTO workOrderUpdateStatusDTO = new WorkOrderUpdateStatusDTO();
                workOrderUpdateStatusDTO.UserId = workOrder.UserID;
                workOrderUpdateStatusDTO.workOrder_ID = workOrder_ID;
                workOrderUpdateStatusDTO.IPLNO = userDetail.IPLNo;
                workOrderUpdateStatusDTO.address1 = userDetail.address1 + " " + userDetail.city + " " + userDetail.state + " " + userDetail.zip;
                if (workOrder.UserID > 0 && userDetail != null && userDetail.LoggedUserDetail != null && userDetail.LoggedUserDetail.Count > 0)
                {
                    FirebaseMessageDTO msg = new FirebaseMessageDTO();
                    msg.avatar = "";
                    msg.from = userDetail.LoggedUserDetail[0].User_LoginName.ToLower();
                    //msg.message = "Work Order #"+ userDetail.IPLNo +" "+ workOrderUpdateStatusDTO.address1+ " Created ";
                    msg.message = "Work order  with IPL number " + userDetail.IPLNo + " " + workOrderUpdateStatusDTO.address1 + " Created ";
                    msg.name = userDetail.LoggedUserDetail[0].UserName.ToLower();
                    msg.status = "unread";
                    msg.threadid = "0_internal";
                    msg.threadtype = "internal";
                    // msg.time = DateTime.Now;
                    msg.time = new Dictionary<string, string> { { ".sv", "timestamp" } };// Assign the Firebase ServerValue
                    var msgResult = await WorkOrderMasterData.AddFirbaseMessage(msg, userDetail.IPLNo, userDetail.LoggedUserDetail[0].IPL_Company_ID, workOrderUpdateStatusDTO.address1);
                }
                
                if (userDetailDTO.ContractorID > 0 && userDetail != null && userDetail.ContractorDetail != null && userDetail.ContractorDetail.Count > 0)
                {
                    if (!string.IsNullOrWhiteSpace(userDetail.ContractorDetail[0].UserToken) && userDetail.status == 2)
                    {
                        NotificationGetData notificationGetData = new NotificationGetData();
                        var notificationResult = notificationGetData.SendNotification(userDetail.ContractorDetail[0].UserToken, "Work order with IPL number #" + userDetail.IPLNo +" " + workOrderUpdateStatusDTO.address1 + " has been created.", "Work Order Created", workOrder_ID.ToString(),1, userDetail.IPLNo);
                    }

                    userDetail.ContractorDetail[0].User_LoginName = userDetail.ContractorDetail[0].User_LoginName.Replace(@".", "");
                    FirebaseUserGroupDTO userGroup = new FirebaseUserGroupDTO();
                    userGroup.id = userDetail.ContractorDetail[0].User_LoginName.ToLower();
                    userGroup.avatar = "http://gravatar.com/avatar/" + WorkOrderMasterData.GenerateMD5(userGroup.id) + "?d=identicon";
                    await WorkOrderMasterData.AddFirebaseUserGroupIPLNO(userGroup, userDetail.IPLNo, userDetail.ContractorDetail[0].IPL_Company_ID, userDetail.ContractorDetail[0].User_LoginName.ToLower(), workOrderUpdateStatusDTO.address1);

                   // workOrderUpdateStatusDTO.WN_UserId = Convert.ToInt64(workOrder.Contractor);
                //    var nObj = WorkOrderMasterData.SendCreatedWebNotification(workOrderUpdateStatusDTO);

                }
                if (workOrder.cordinator > 0 && userDetail != null && userDetail.CordinatorDetail != null && userDetail.CordinatorDetail.Count > 0)
                {
                    userDetail.CordinatorDetail[0].User_LoginName = userDetail.CordinatorDetail[0].User_LoginName.Replace(@".", "");
                    FirebaseUserGroupDTO userGroup = new FirebaseUserGroupDTO();
                    userGroup.id = userDetail.CordinatorDetail[0].User_LoginName.ToLower();
                    userGroup.avatar = "http://gravatar.com/avatar/" + WorkOrderMasterData.GenerateMD5(userGroup.id) + "?d=identicon";
                    await WorkOrderMasterData.AddFirebaseUserGroupIPLNO(userGroup, userDetail.IPLNo, userDetail.CordinatorDetail[0].IPL_Company_ID, userDetail.CordinatorDetail[0].User_LoginName.ToLower(), workOrderUpdateStatusDTO.address1);

                    //workOrderUpdateStatusDTO.WN_UserId = Convert.ToInt64(workOrder.cordinator);
                    //var nObj = WorkOrderMasterData.SendCreatedWebNotification(workOrderUpdateStatusDTO);
                }
                if (workOrder.Processor > 0 && userDetail != null && userDetail.ProcessorDetail != null && userDetail.ProcessorDetail.Count > 0)
                {
                    userDetail.ProcessorDetail[0].User_LoginName = userDetail.ProcessorDetail[0].User_LoginName.Replace(@".", "");
                    FirebaseUserGroupDTO userGroup = new FirebaseUserGroupDTO();
                    userGroup.id = userDetail.ProcessorDetail[0].User_LoginName.ToLower();
                    userGroup.avatar = "http://gravatar.com/avatar/" + WorkOrderMasterData.GenerateMD5(userGroup.id) + "?d=identicon";
                    await WorkOrderMasterData.AddFirebaseUserGroupIPLNO(userGroup, userDetail.IPLNo , userDetail.ProcessorDetail[0].IPL_Company_ID, userDetail.ProcessorDetail[0].User_LoginName.ToLower(), workOrderUpdateStatusDTO.address1);

                    //workOrderUpdateStatusDTO.WN_UserId = Convert.ToInt64(workOrder.Processor);
                   // var nObj = WorkOrderMasterData.SendCreatedWebNotification(workOrderUpdateStatusDTO);
                }

                if (userDetail != null && userDetail.AdminUserDetail != null && userDetail.AdminUserDetail.Count > 0)
                {
                   
                    foreach (var adminUser in userDetail.AdminUserDetail)
                    {
                        //workOrderUpdateStatusDTO.WN_UserId = adminUser.User_pkeyID;
                      //  var nObj = WorkOrderMasterData.SendCreatedWebNotification(workOrderUpdateStatusDTO);

                        adminUser.User_LoginName = adminUser.User_LoginName.Replace(@".", "");
                        FirebaseUserGroupDTO userGroup = new FirebaseUserGroupDTO();
                        userGroup.id = adminUser.User_LoginName.ToLower();
                        userGroup.avatar = "http://gravatar.com/avatar/" + WorkOrderMasterData.GenerateMD5(userGroup.id) + "?d=identicon";
                        await WorkOrderMasterData.AddFirebaseUserGroupIPLNO(userGroup, userDetail.IPLNo , adminUser.IPL_Company_ID, adminUser.User_LoginName.ToLower(), workOrderUpdateStatusDTO.address1);
                    }
                }

            }
            catch (Exception ex)
            {
                log.logErrorMessage("AddFirebaseData");
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
        
        }

        public List<dynamic> AddImportMSIWorkOrderMasterData(ImportWorkOrderNewDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objclntData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_Import_MSI_WorkOrderMaster_Data]";
            ImportWorkOrderNew importWorkOrderNew = new ImportWorkOrderNew();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();

            log.logInfoMessage("Work Order Import Started for " + model.Pkey_Id);
            try
            {
                input_parameters.Add("@Pkey_Id", 1 + "#bigint#" + model.Pkey_Id);
                input_parameters.Add("@WorkType", 1 + "#bigint#" + model.WorkType);
                input_parameters.Add("@Contractor", 1 + "#varchar#" + model.Contractor);
                input_parameters.Add("@Processor", 1 + "#bigint#" + model.Processor);
                input_parameters.Add("@cordinator", 1 + "#bigint#" + model.cordinator);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Pkey_IdOut", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    importWorkOrderNew.Pkey_Id = "0";
                    importWorkOrderNew.Status = "0";
                    importWorkOrderNew.ErrorMessage = "Error while Saviving in the Database";

                    log.logDebugMessage("Work Order Import Falied for " + model.Pkey_Id);
                }
                else
                {
                    importWorkOrderNew.Pkey_Id = Convert.ToString(objData[0]);
                    importWorkOrderNew.Status = Convert.ToString(objData[1]);
                    log.logDebugMessage("Work Order Import Sucesss for " + model.Pkey_Id);
                    log.logDebugMessage("Work Order Import Status " + importWorkOrderNew.Status);

                }
                log.logDebugMessage("Work Order Import Sucesss for " + model.Pkey_Id);
                objclntData.Add(importWorkOrderNew);
            }
            catch (Exception ex)
            {
                log.logErrorMessage("Work Order Import Failed for " + model.Pkey_Id);
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objclntData;

        }


        //MCS details add 
        public List<dynamic> AddImportMCSWorkOrderMasterData(ImportWorkOrderNewDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objclntData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_Import_MCS_WorkOrderMaster_Data]";
            ImportWorkOrderNew importWorkOrderNew = new ImportWorkOrderNew();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();

            log.logInfoMessage("Work Order Import Started for " + model.Pkey_Id);
            try
            {
                input_parameters.Add("@Pkey_Id", 1 + "#bigint#" + model.Pkey_Id);
                input_parameters.Add("@WorkType", 1 + "#bigint#" + model.WorkType);
                input_parameters.Add("@Contractor", 1 + "#varchar#" + model.Contractor);
                input_parameters.Add("@Processor", 1 + "#bigint#" + model.Processor);
                input_parameters.Add("@cordinator", 1 + "#bigint#" + model.cordinator);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Pkey_IdOut", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    importWorkOrderNew.Pkey_Id = "0";
                    importWorkOrderNew.Status = "0";
                    importWorkOrderNew.ErrorMessage = "Error while Saviving in the Database";

                    log.logDebugMessage("Work Order Import Falied for " + model.Pkey_Id);
                }
                else
                {
                    importWorkOrderNew.Pkey_Id = Convert.ToString(objData[0]);
                    importWorkOrderNew.Status = Convert.ToString(objData[1]);
                    log.logDebugMessage("Work Order Import Sucesss for " + model.Pkey_Id);
                    log.logDebugMessage("Work Order Import Status " + importWorkOrderNew.Status);

                }
                log.logDebugMessage("Work Order Import Sucesss for " + model.Pkey_Id);
                objclntData.Add(importWorkOrderNew);
            }
            catch (Exception ex)
            {
                log.logErrorMessage("Work Order Import Failed for " + model.Pkey_Id);
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objclntData;

        }

        //AG details add 
        public List<dynamic> AddImportAGWorkOrderMasterData(ImportWorkOrderNewDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objclntData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_Import_AG_WorkOrderMaster_Data]";
            ImportWorkOrderNew importWorkOrderNew = new ImportWorkOrderNew();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();

            log.logInfoMessage("Work Order Import Started for " + model.Pkey_Id);
            try
            {
                input_parameters.Add("@Pkey_Id", 1 + "#bigint#" + model.Pkey_Id);
                input_parameters.Add("@WorkType", 1 + "#bigint#" + model.WorkType);
                input_parameters.Add("@Contractor", 1 + "#varchar#" + model.Contractor);
                input_parameters.Add("@Processor", 1 + "#bigint#" + model.Processor);
                input_parameters.Add("@cordinator", 1 + "#bigint#" + model.cordinator);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Pkey_IdOut", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    importWorkOrderNew.Pkey_Id = "0";
                    importWorkOrderNew.Status = "0";
                    importWorkOrderNew.ErrorMessage = "Error while Saviving in the Database";

                    log.logDebugMessage("Work Order Import Falied for " + model.Pkey_Id);
                }
                else
                {
                    importWorkOrderNew.Pkey_Id = Convert.ToString(objData[0]);
                    importWorkOrderNew.Status = Convert.ToString(objData[1]);
                    log.logDebugMessage("Work Order Import Sucesss for " + model.Pkey_Id);
                    log.logDebugMessage("Work Order Import Status " + importWorkOrderNew.Status);

                }
                log.logDebugMessage("Work Order Import Sucesss for " + model.Pkey_Id);
                objclntData.Add(importWorkOrderNew);
            }
            catch (Exception ex)
            {
                log.logErrorMessage("Work Order Import Failed for " + model.Pkey_Id);
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objclntData;

        }

        //nfr details add 
        public List<dynamic> AddImportNFRWorkOrderMasterData(ImportWorkOrderNewDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objclntData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_Import_NFR_WorkOrderMaster_Data]";
            ImportWorkOrderNew importWorkOrderNew = new ImportWorkOrderNew();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();

            log.logInfoMessage("Work Order Import Started for " + model.Pkey_Id);
            try
            {
                input_parameters.Add("@Pkey_Id", 1 + "#bigint#" + model.Pkey_Id);
                input_parameters.Add("@WorkType", 1 + "#bigint#" + model.WorkType);
                input_parameters.Add("@Contractor", 1 + "#varchar#" + model.Contractor);
                input_parameters.Add("@Processor", 1 + "#bigint#" + model.Processor);
                input_parameters.Add("@cordinator", 1 + "#bigint#" + model.cordinator);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Pkey_IdOut", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    importWorkOrderNew.Pkey_Id = "0";
                    importWorkOrderNew.Status = "0";
                    importWorkOrderNew.ErrorMessage = "Error while Saviving in the Database";

                    log.logDebugMessage("Work Order Import Falied for " + model.Pkey_Id);
                }
                else
                {
                    importWorkOrderNew.Pkey_Id = Convert.ToString(objData[0]);
                    importWorkOrderNew.Status = Convert.ToString(objData[1]);
                    log.logDebugMessage("Work Order Import Sucesss for " + model.Pkey_Id);
                    log.logDebugMessage("Work Order Import Status " + importWorkOrderNew.Status);

                }
                log.logDebugMessage("Work Order Import Sucesss for " + model.Pkey_Id);
                objclntData.Add(importWorkOrderNew);
            }
            catch (Exception ex)
            {
                log.logErrorMessage("Work Order Import Failed for " + model.Pkey_Id);
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objclntData;

        }


        //Cypress details add 
        public List<dynamic> AddImportCyprexxWorkOrderMasterData(ImportWorkOrderNewDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objclntData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_Import_Cyprexx_WorkOrderMaster_Data]";
            ImportWorkOrderNew importWorkOrderNew = new ImportWorkOrderNew();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();

            log.logInfoMessage("Work Order Import Started for " + model.Pkey_Id);
            try
            {
                input_parameters.Add("@Pkey_Id", 1 + "#bigint#" + model.Pkey_Id);
                input_parameters.Add("@WorkType", 1 + "#bigint#" + model.WorkType);
                input_parameters.Add("@Contractor", 1 + "#varchar#" + model.Contractor);
                input_parameters.Add("@Processor", 1 + "#bigint#" + model.Processor);
                input_parameters.Add("@cordinator", 1 + "#bigint#" + model.cordinator);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Pkey_IdOut", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    importWorkOrderNew.Pkey_Id = "0";
                    importWorkOrderNew.Status = "0";
                    importWorkOrderNew.ErrorMessage = "Error while Saviving in the Database";

                    log.logDebugMessage("Work Order Import Falied for " + model.Pkey_Id);
                }
                else
                {
                    importWorkOrderNew.Pkey_Id = Convert.ToString(objData[0]);
                    importWorkOrderNew.Status = Convert.ToString(objData[1]);
                    log.logDebugMessage("Work Order Import Sucesss for " + model.Pkey_Id);
                    log.logDebugMessage("Work Order Import Status " + importWorkOrderNew.Status);

                }
                log.logDebugMessage("Work Order Import Sucesss for " + model.Pkey_Id);
                objclntData.Add(importWorkOrderNew);
            }
            catch (Exception ex)
            {
                log.logErrorMessage("Work Order Import Failed for " + model.Pkey_Id);
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objclntData;

        }

        //ServiceLink details add 
        public List<dynamic> AddImportServiceLinkWorkOrderMasterData(ImportWorkOrderNewDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objclntData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_Import_servicelink_WorkOrderMaster_Data]";
            ImportWorkOrderNew importWorkOrderNew = new ImportWorkOrderNew();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();

            log.logInfoMessage("Work Order Import Started for " + model.Pkey_Id);
            try
            {
                input_parameters.Add("@Pkey_Id", 1 + "#bigint#" + model.Pkey_Id);
                input_parameters.Add("@WorkType", 1 + "#bigint#" + model.WorkType);
                input_parameters.Add("@Contractor", 1 + "#varchar#" + model.Contractor);
                input_parameters.Add("@Processor", 1 + "#bigint#" + model.Processor);
                input_parameters.Add("@cordinator", 1 + "#bigint#" + model.cordinator);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Pkey_IdOut", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    importWorkOrderNew.Pkey_Id = "0";
                    importWorkOrderNew.Status = "0";
                    importWorkOrderNew.ErrorMessage = "Error while Saviving in the Database";

                    log.logDebugMessage("Work Order Import Falied for " + model.Pkey_Id);
                }
                else
                {
                    importWorkOrderNew.Pkey_Id = Convert.ToString(objData[0]);
                    importWorkOrderNew.Status = Convert.ToString(objData[1]);
                    log.logDebugMessage("Work Order Import Sucesss for " + model.Pkey_Id);
                    log.logDebugMessage("Work Order Import Status " + importWorkOrderNew.Status);

                }
                log.logDebugMessage("Work Order Import Sucesss for " + model.Pkey_Id);
                objclntData.Add(importWorkOrderNew);
            }
            catch (Exception ex)
            {
                log.logErrorMessage("Work Order Import Failed for " + model.Pkey_Id);
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objclntData;

        }

        //Altisource details add 
        public List<dynamic> AddImportAltisourceWorkOrderMasterData(ImportWorkOrderNewDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objclntData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_Import_Altisource_WorkOrderMaster_Data]";
            ImportWorkOrderNew importWorkOrderNew = new ImportWorkOrderNew();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();

            log.logInfoMessage("Work Order Import Started for " + model.Pkey_Id);
            try
            {
                input_parameters.Add("@Pkey_Id", 1 + "#bigint#" + model.Pkey_Id);
                input_parameters.Add("@WorkType", 1 + "#bigint#" + model.WorkType);
                input_parameters.Add("@Contractor", 1 + "#varchar#" + model.Contractor);
                input_parameters.Add("@Processor", 1 + "#bigint#" + model.Processor);
                input_parameters.Add("@cordinator", 1 + "#bigint#" + model.cordinator);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Pkey_IdOut", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    importWorkOrderNew.Pkey_Id = "0";
                    importWorkOrderNew.Status = "0";
                    importWorkOrderNew.ErrorMessage = "Error while Saviving in the Database";

                    log.logDebugMessage("Work Order Import Falied for " + model.Pkey_Id);
                }
                else
                {
                    importWorkOrderNew.Pkey_Id = Convert.ToString(objData[0]);
                    importWorkOrderNew.Status = Convert.ToString(objData[1]);
                    log.logDebugMessage("Work Order Import Sucesss for " + model.Pkey_Id);
                    log.logDebugMessage("Work Order Import Status " + importWorkOrderNew.Status);

                }
                log.logDebugMessage("Work Order Import Sucesss for " + model.Pkey_Id);
                objclntData.Add(importWorkOrderNew);
            }
            catch (Exception ex)
            {
                log.logErrorMessage("Work Order Import Failed for " + model.Pkey_Id);
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objclntData;

        }

        private String GetScrapperErrorMessage(string Code)
        {
            string Message = string.Empty;
            try
            {
                switch (Code)
                {
                    case "1":
                        {
                            Message = "Success, Import Completed";
                            break;
                        }
                    case "2":
                        {
                            Message = "Unable to log in, verify password or site availability";
                            break;
                        }
                    case "3":
                        {
                            Message = "Error with Import, contact support";
                            break;
                        }
                    case "4":
                        {
                            Message = "Import Does not contain Workorders";
                            break;
                        }
                    default:
                        {
                            Message = "Import Not Refresh";
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return Message;
        }

         

    }
}