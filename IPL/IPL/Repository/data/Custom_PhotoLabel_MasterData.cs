﻿using Avigma.Repository.Lib;
using IPL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace IPL.Repository.data
{
    public class Custom_PhotoLabel_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();


        public List<dynamic> AddCustomPhotoLableData(Custom_PhotoLabel_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();

            string insertProcedure = "[CreateUpdateCustom_PhotoLabel_Master]";
            Custom_PhotoLabelOut custom_PhotoLabelOut = new Custom_PhotoLabelOut();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@PhotoLabel_pkeyID", 1 + "#bigint#" + model.PhotoLabel_pkeyID);
                input_parameters.Add("@workOrder_ID", 1 + "#bigint#" + model.workOrder_ID);
                input_parameters.Add("@PhotoLabel_Name", 1 + "#varchar#" + model.PhotoLabel_Name);
                input_parameters.Add("@PhotoLabel_IsCustom", 1 + "#int#" + model.PhotoLabel_IsCustom);
                input_parameters.Add("@PhotoLabel_IsAutoAssign", 1 + "#bit#" + model.PhotoLabel_IsAutoAssign);
                input_parameters.Add("@PhotoLabel_IsActive", 1 + "#bit#" + model.PhotoLabel_IsActive);
                input_parameters.Add("@PhotoLabel_IsDelete", 1 + "#bit#" + model.PhotoLabel_IsDelete);
                input_parameters.Add("@PhotoLabel_Valtype", 1 + "#int#" + model.PhotoLabel_Valtype);
                input_parameters.Add("@PhotoLabel_Client_Id", 1 + "#bigint#" + model.PhotoLabel_Client_Id);
                input_parameters.Add("@PhotoLabel_WorkType_Id", 1 + "#bigint#" + model.PhotoLabel_WorkType_Id);
                input_parameters.Add("@PhotoLabel_Customer_Id", 1 + "#bigint#" + model.PhotoLabel_Customer_Id);
                input_parameters.Add("@PhotoLabel_Loan_Id", 1 + "#bigint#" + model.PhotoLabel_Loan_Id);
                input_parameters.Add("@PhotoLabel_Group_Id", 1 + "#bigint#" + model.PhotoLabel_Group_Id); // Added by dipali
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@PhotoLabel_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@WorkOrderPhotoLabel_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);

                if (objData[2] == 0)
                {
                    custom_PhotoLabelOut.PhotoLabel_pkeyID = "0";
                    custom_PhotoLabelOut.Status = "0";
                    custom_PhotoLabelOut.ErrorMessage = "Error while Saviving in the Database";
                }
                else
                {
                    if (model.Type == 1 || model.Type == 2)
                    {
                        AddUpdate_CustomeLabel_Filter(objData[0], model);
                    }

                    custom_PhotoLabelOut.PhotoLabel_pkeyID = Convert.ToString(objData[0]);
                    custom_PhotoLabelOut.WorkOrderPhotoLabel_pkeyID = Convert.ToString(objData[1]);
                    custom_PhotoLabelOut.Status = Convert.ToString(objData[2]);
                }
                objcltData.Add(custom_PhotoLabelOut);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;



        }

        public List<dynamic> AddCustomPhotoLableDataForMobile(Custom_PhotoLabel_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                model.Type = 5;
                model.PhotoLabel_IsActive = true;
                model.PhotoLabel_IsDelete = false;
                model.PhotoLabel_IsCustom = 0;
                model.PhotoLabel_Valtype = 1;
                objData = AddCustomPhotoLableData(model);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        private DataSet GetCustomLabelMaster(Custom_PhotoLabel_MasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Custom_PhotoLabel_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@PhotoLabel_pkeyID", 1 + "#bigint#" + model.PhotoLabel_pkeyID);
                input_parameters.Add("@WhereClause", 1 + "#varchar#" + model.WhereClause);
                input_parameters.Add("@workOrder_ID", 1 + "#bigint#" + model.workOrder_ID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return ds;
        }

        public List<dynamic> GetCustomlabelDetails(Custom_PhotoLabel_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetCustomLabelMaster(model);

                if (model.Type == 4)
                {
                    List<Custom_PhotoLabel_MasterDTO> CustomeLDetails = new List<Custom_PhotoLabel_MasterDTO>();
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        Custom_PhotoLabel_MasterDTO custom_PhotoLabel_MasterDTO = new Custom_PhotoLabel_MasterDTO();
                        custom_PhotoLabel_MasterDTO.PhotoLabel_pkeyID = Convert.ToInt64(ds.Tables[0].Rows[i]["PhotoLabel_pkeyID"].ToString());
                        custom_PhotoLabel_MasterDTO.PhotoLabel_Name = ds.Tables[0].Rows[i]["PhotoLabel_Name"].ToString();
                        custom_PhotoLabel_MasterDTO.Custom_label_Check = false;
                        if (!string.IsNullOrEmpty(ds.Tables[0].Rows[i]["PhotoLabel_Valtype"].ToString()))
                        {
                            custom_PhotoLabel_MasterDTO.PhotoLabel_Valtype = Convert.ToInt32(ds.Tables[0].Rows[i]["PhotoLabel_Valtype"].ToString());
                        }

                        for (int j = 0; j < ds.Tables[1].Rows.Count; j++)
                        {
                            Int64 PhotoLabel_ID = Convert.ToInt64(ds.Tables[1].Rows[j]["WorkOrderPhotoLabel_PhotoLabel_ID"].ToString());
                            if (PhotoLabel_ID == custom_PhotoLabel_MasterDTO.PhotoLabel_pkeyID)
                            {
                                custom_PhotoLabel_MasterDTO.Custom_label_Check = true;
                                break;
                            }
                        }

                        CustomeLDetails.Add(custom_PhotoLabel_MasterDTO);
                    }
                    objDynamic.Add(CustomeLDetails);

                }
                else if (model.Type == 2 && ds.Tables.Count > 0)
                {
                    var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                    List<Custom_PhotoLabel_MasterDTO> CustomeLDetails =
                       (from item in myEnumerableFeaprd
                        select new Custom_PhotoLabel_MasterDTO
                        {
                            PhotoLabel_pkeyID = item.Field<Int64>("PhotoLabel_pkeyID"),
                            PhotoLabel_Name = item.Field<String>("PhotoLabel_Name"),
                            PhotoLabel_IsActive = item.Field<Boolean?>("PhotoLabel_IsActive"),
                            PhotoLabel_Valtype = item.Field<int>("PhotoLabel_Valtype"),
                            PhotoLabel_Client_Id = item.Field<Int64>("PhotoLabel_Client_Id"),
                            PhotoLabel_WorkType_Id = item.Field<Int64>("PhotoLabel_WorkType_Id"),
                            PhotoLabel_Customer_Id = item.Field<Int64>("PhotoLabel_Customer_Id"),
                            PhotoLabel_Loan_Id = item.Field<Int64>("PhotoLabel_Loan_Id"),
                            PhotoLabel_Group_Id = item.Field<Int64?>("PhotoLabel_Group_Id"), //Added By dipali
                            CustomPhotoLabel_Filter_Master_PkeyId = item.Field<Int64?>("CustomPhotoLabel_Filter_Master_PkeyId"), //Added By dipali
                            PhotoLabel_CreatedBy = item.Field<String>("PhotoLabel_CreatedBy"),
                            PhotoLabel_ModifiedBy = item.Field<String>("PhotoLabel_ModifiedBy"),
                            PhotoLabel_IsAutoAssign = item.Field<Boolean?>("PhotoLabel_IsAutoAssign"),
                        }).ToList();

                    objDynamic.Add(CustomeLDetails);

                    var myEnumerableFeaprdass = ds.Tables[1].AsEnumerable();
                    List<CustomPhotoLabel_FilterDTO> assdata =
                       (from item in myEnumerableFeaprdass
                        select new CustomPhotoLabel_FilterDTO
                        {
                            CustomPhotoLabel_Filter_Master_PkeyId = item.Field<Int64>("CustomPhotoLabel_Filter_Master_PkeyId"),
                            CustomPhotoLabel_Filter_Client = item.Field<String>("CustomPhotoLabel_Filter_Client"),
                            CustomPhotoLabel_Filter_Customer = item.Field<String>("CustomPhotoLabel_Filter_Customer"),
                            CustomPhotoLabel_Filter_LoanType = item.Field<String>("CustomPhotoLabel_Filter_LoanType"),
                            CustomPhotoLabel_Filter_WorkType = item.Field<String>("CustomPhotoLabel_Filter_WorkType"),
                            CustomPhotoLabel_Filter_WorkTypeGroup = item.Field<String>("CustomPhotoLabel_Filter_WorkTypeGroup"),
                            CustomPhotoLabel_Filter_State = item.Field<String>("CustomPhotoLabel_Filter_State"),
                            CustomPhotoLabel_Filter_County = item.Field<String>("CustomPhotoLabel_Filter_County"),
                            CustomPhotoLabel_Filter_IsActive = item.Field<Boolean?>("CustomPhotoLabel_Filter_IsActive"),
                            Custom_PhotoLabel_Master_FkeyId = item.Field<Int64?>("Custom_PhotoLabel_Master_FkeyId"),
                            CustomPhotoLabel_Filter_CreatedBy = item.Field<String>("CustomPhotoLabel_Filter_CreatedBy"),
                            CustomPhotoLabel_Filter_ModifiedBy = item.Field<String>("CustomPhotoLabel_Filter_ModifiedBy"),
                        }).ToList();

                    objDynamic.Add(assdata);
                }
                else
                {
                    if (ds.Tables.Count > 0)
                    {
                        var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                        List<Custom_PhotoLabel_MasterDTO> CustomeLDetails =
                           (from item in myEnumerableFeaprd
                            select new Custom_PhotoLabel_MasterDTO
                            {
                                PhotoLabel_pkeyID = item.Field<Int64>("PhotoLabel_pkeyID"),
                                PhotoLabel_Name = item.Field<String>("PhotoLabel_Name"),
                                PhotoLabel_IsActive = item.Field<Boolean?>("PhotoLabel_IsActive"),
                                PhotoLabel_Valtype = item.Field<int>("PhotoLabel_Valtype"),
                                PhotoLabel_Client_Id = item.Field<Int64>("PhotoLabel_Client_Id"),
                                PhotoLabel_WorkType_Id = item.Field<Int64>("PhotoLabel_WorkType_Id"),
                                PhotoLabel_Customer_Id = item.Field<Int64>("PhotoLabel_Customer_Id"),
                                PhotoLabel_Loan_Id = item.Field<Int64>("PhotoLabel_Loan_Id"),
                                PhotoLabel_Group_Id = item.Field<Int64?>("PhotoLabel_Group_Id"), //Added By dipali
                                PhotoLabel_CreatedBy = item.Field<String>("PhotoLabel_CreatedBy"),
                                PhotoLabel_ModifiedBy = item.Field<String>("PhotoLabel_ModifiedBy"),
                                PhotoLabel_IsAutoAssign = item.Field<Boolean?>("PhotoLabel_IsAutoAssign"),
                                ViewUrl = null

                            }).ToList();

                        objDynamic.Add(CustomeLDetails);
                    }

                    if (model.Type == 1)
                    {
                        if (ds.Tables.Count > 1)
                        {
                            var myEnumerableFeaprd = ds.Tables[1].AsEnumerable();
                            List<Filter_Admin_CustPhLbl_MasterDTO> Forms_Master_Files =
                               (from item in myEnumerableFeaprd
                                select new Filter_Admin_CustPhLbl_MasterDTO
                                {
                                    CustPhLbl_Filter_PkeyID = item.Field<Int64>("CustPhLbl_Filter_PkeyID"),
                                    CustPhLbl_Filter_CustPhLblName = item.Field<String>("CustPhLbl_Filter_CustPhLblName"),
                                    CustPhLbl_Filter_CustPhLblIsActive = item.Field<Boolean?>("CustPhLbl_Filter_CustPhLblIsActive")

                                }).ToList();

                            objDynamic.Add(Forms_Master_Files);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        // filter arr
        public List<dynamic> GetCustomPhotoFilterDetails(Custom_PhotoLabel_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();


            string wherecondition = string.Empty;
            try
            {

                var Data = JsonConvert.DeserializeObject<Custom_PhotoLabel_MasterDTO>(model.FilterData);
                if (!string.IsNullOrEmpty(Data.PhotoLabel_Name))
                {
                    wherecondition = " And PhotoLabel_Name LIKE '%" + Data.PhotoLabel_Name + "%'";
                }


                if (Data.PhotoLabel_IsActive == true)
                {
                    wherecondition = wherecondition + "  And PhotoLabel_IsActive =  '1'";
                }
                if (Data.PhotoLabel_IsActive == false)
                {
                    wherecondition = wherecondition + "  And PhotoLabel_IsActive =  '0'";
                }


                Custom_PhotoLabel_MasterDTO custom_PhotoLabel_MasterDTO = new Custom_PhotoLabel_MasterDTO();

                custom_PhotoLabel_MasterDTO.WhereClause = wherecondition;
                custom_PhotoLabel_MasterDTO.Type = 5;
                custom_PhotoLabel_MasterDTO.UserID = model.UserID;

                DataSet ds = GetCustomLabelMaster(custom_PhotoLabel_MasterDTO);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Custom_PhotoLabel_MasterDTO> customfilter =
                   (from item in myEnumerableFeaprd
                    select new Custom_PhotoLabel_MasterDTO
                    {
                        PhotoLabel_pkeyID = item.Field<Int64>("PhotoLabel_pkeyID"),
                        PhotoLabel_Name = item.Field<String>("PhotoLabel_Name"),
                        PhotoLabel_IsActive = item.Field<Boolean?>("PhotoLabel_IsActive"),
                        PhotoLabel_Valtype = item.Field<int>("PhotoLabel_Valtype"),
                        PhotoLabel_Client_Id = item.Field<Int64>("PhotoLabel_Client_Id"),
                        PhotoLabel_WorkType_Id = item.Field<Int64>("PhotoLabel_WorkType_Id"),
                        PhotoLabel_Customer_Id = item.Field<Int64>("PhotoLabel_Customer_Id"),
                        PhotoLabel_Loan_Id = item.Field<Int64>("PhotoLabel_Loan_Id"),
                        PhotoLabel_Group_Id = item.Field<Int64?>("PhotoLabel_Group_Id"), //Added By dipali
                        PhotoLabel_CreatedBy = item.Field<String>("PhotoLabel_CreatedBy"),
                        PhotoLabel_ModifiedBy = item.Field<String>("PhotoLabel_ModifiedBy"),
                        ViewUrl = null
                    }).ToList();

                objDynamic.Add(customfilter);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }

            return objDynamic;
        }

        public List<dynamic> AddUpdate_CustomeLabel_Filter(Int64 photoLabel_pkeyID, Custom_PhotoLabel_MasterDTO model)
        {
            List<dynamic> objAddData = new List<dynamic>();

            try
            {
                CustomPhotoLabel_Filter_Data customPhotoLabel_Filter_Data = new CustomPhotoLabel_Filter_Data();
                if (model != null && model.AutoAssinArray != null && model.AutoAssinArray.Count > 0)
                {
                    for (int i = 0; i < model.AutoAssinArray.Count; i++)
                    {
                        CustomPhotoLabel_FilterDTO customPhotoLabel_FilterDTO = new CustomPhotoLabel_FilterDTO();


                        customPhotoLabel_FilterDTO.CustomPhotoLabel_Filter_Master_PkeyId = model.CustomPhotoLabel_Filter_Master_PkeyId.GetValueOrDefault(0);
                        customPhotoLabel_FilterDTO.Custom_PhotoLabel_Master_FkeyId = photoLabel_pkeyID;
                        customPhotoLabel_FilterDTO.CustomPhotoLabel_Filter_Client = JsonConvert.SerializeObject(model.AutoAssinArray[i].Task_sett_Company);
                        customPhotoLabel_FilterDTO.CustomPhotoLabel_Filter_County = JsonConvert.SerializeObject(model.AutoAssinArray[i].Task_sett_Country);
                        customPhotoLabel_FilterDTO.CustomPhotoLabel_Filter_Customer = JsonConvert.SerializeObject(model.AutoAssinArray[i].Task_sett_Customer);
                        customPhotoLabel_FilterDTO.CustomPhotoLabel_Filter_LoanType = JsonConvert.SerializeObject(model.AutoAssinArray[i].Task_sett_Lone);
                        customPhotoLabel_FilterDTO.CustomPhotoLabel_Filter_State = JsonConvert.SerializeObject(model.AutoAssinArray[i].Task_sett_State);
                        customPhotoLabel_FilterDTO.CustomPhotoLabel_Filter_WorkType = JsonConvert.SerializeObject(model.AutoAssinArray[i].WTTaskWorkType);
                        customPhotoLabel_FilterDTO.CustomPhotoLabel_Filter_WorkTypeGroup = JsonConvert.SerializeObject(model.AutoAssinArray[i].Task_Work_TypeGroup);
                        customPhotoLabel_FilterDTO.CustomPhotoLabel_Filter_IsActive = true;
                        customPhotoLabel_FilterDTO.CustomPhotoLabel_Filter_IsDelete = false;
                        customPhotoLabel_FilterDTO.UserID = model.UserID;
                        customPhotoLabel_FilterDTO.Type = model.CustomPhotoLabel_Filter_Master_PkeyId != 0 ? 2 : 1;

                        var objautoasn = customPhotoLabel_Filter_Data.AddUpdate_CustomPhotoLabel_Filter_Master(customPhotoLabel_FilterDTO);


                        if (model.AutoAssinArray[i].Task_sett_Company != null)
                        {
                            for (int j = 0; j < model.AutoAssinArray[i].Task_sett_Company.Count; j++)
                            {
                                CustomPhotoLabel_Filter_ClientDTO customPhotoLabel_Filter_ClientDTO = new CustomPhotoLabel_Filter_ClientDTO();
                                customPhotoLabel_Filter_ClientDTO.CustomPhotoLabel_Filter_Client_PkeyId = 0;
                                customPhotoLabel_Filter_ClientDTO.CustomPhotoLabel_Filter_Client_CId = model.AutoAssinArray[i].Task_sett_Company[j].Client_pkeyID;
                                customPhotoLabel_Filter_ClientDTO.CustomPhotoLabel_Filter_Master_FkeyId = objautoasn[0];
                                customPhotoLabel_Filter_ClientDTO.Custom_PhotoLabel_Master_FkeyId = photoLabel_pkeyID;
                                customPhotoLabel_Filter_ClientDTO.CustomPhotoLabel_Filter_Client_IsActive = true;
                                customPhotoLabel_Filter_ClientDTO.CustomPhotoLabel_Filter_Client_IsDelete = false;
                                customPhotoLabel_Filter_ClientDTO.UserID = model.UserID;
                                customPhotoLabel_Filter_ClientDTO.Type = 1;

                                var objclient = customPhotoLabel_Filter_Data.AddUpdate_CustomPhotoLabel_Filter_Client(customPhotoLabel_Filter_ClientDTO);
                            }
                        }

                        if (model.AutoAssinArray[i].Task_sett_Country != null)
                        {
                            for (int k = 0; k < model.AutoAssinArray[i].Task_sett_Country.Count; k++)
                            {
                                CustomPhotoLabel_Filter_CountyDTO customPhotoLabel_Filter_CountyDTO = new CustomPhotoLabel_Filter_CountyDTO();

                                customPhotoLabel_Filter_CountyDTO.CustomPhotoLabel_Filter_Master_FkeyId = objautoasn[0];
                                customPhotoLabel_Filter_CountyDTO.Custom_PhotoLabel_Master_FkeyId = photoLabel_pkeyID;
                                customPhotoLabel_Filter_CountyDTO.CustomPhotoLabel_Filter_County_CnId = model.AutoAssinArray[i].Task_sett_Country[k].ID;
                                customPhotoLabel_Filter_CountyDTO.CustomPhotoLabel_Filter_County_PkeyId = 0;
                                customPhotoLabel_Filter_CountyDTO.CustomPhotoLabel_Filter_County_IsActive = true;
                                customPhotoLabel_Filter_CountyDTO.CustomPhotoLabel_Filter_County_IsDelete = false;
                                customPhotoLabel_Filter_CountyDTO.UserID = model.UserID;
                                customPhotoLabel_Filter_CountyDTO.Type = 1;

                                var countydata = customPhotoLabel_Filter_Data.AddUpdate_CustomPhotoLabel_Filter_County(customPhotoLabel_Filter_CountyDTO);
                            }

                        }
                        if (model.AutoAssinArray[i].Task_sett_Customer != null)
                        {
                            for (int m = 0; m < model.AutoAssinArray[i].Task_sett_Customer.Count; m++)
                            {
                                CustomPhotoLabel_Filter_CustomerDTO customPhotoLabel_Filter_CustomerDTO = new CustomPhotoLabel_Filter_CustomerDTO();
                                customPhotoLabel_Filter_CustomerDTO.CustomPhotoLabel_Filter_Master_FkeyId = objautoasn[0];
                                customPhotoLabel_Filter_CustomerDTO.Custom_PhotoLabel_Master_FkeyId = photoLabel_pkeyID;
                                customPhotoLabel_Filter_CustomerDTO.CustomPhotoLabel_Filter_Customer_CustId = model.AutoAssinArray[i].Task_sett_Customer[m].Cust_Num_pkeyId;
                                customPhotoLabel_Filter_CustomerDTO.CustomPhotoLabel_Filter_Customer_PkeyId = 0;
                                customPhotoLabel_Filter_CustomerDTO.CustomPhotoLabel_Filter_Customer_IsActive = true;
                                customPhotoLabel_Filter_CustomerDTO.CustomPhotoLabel_Filter_Customer_IsDelete = false;
                                customPhotoLabel_Filter_CustomerDTO.UserID = model.UserID;
                                customPhotoLabel_Filter_CustomerDTO.Type = 1;

                                var customerdata = customPhotoLabel_Filter_Data.AddUpdate_CustomPhotoLabel_Filter_Customer(customPhotoLabel_Filter_CustomerDTO);
                            }
                        }
                        if (model.AutoAssinArray[i].Task_sett_Lone != null)
                        {
                            for (int n = 0; n < model.AutoAssinArray[i].Task_sett_Lone.Count; n++)
                            {
                                CustomPhotoLabel_Filter_LoanTypeDTO customPhotoLabel_Filter_LoanTypeDTO = new CustomPhotoLabel_Filter_LoanTypeDTO();

                                customPhotoLabel_Filter_LoanTypeDTO.CustomPhotoLabel_Filter_Master_FkeyId = objautoasn[0];
                                customPhotoLabel_Filter_LoanTypeDTO.Custom_PhotoLabel_Master_FkeyId = photoLabel_pkeyID;
                                customPhotoLabel_Filter_LoanTypeDTO.CustomPhotoLabel_Filter_LoanType_LId = model.AutoAssinArray[i].Task_sett_Lone[n].Loan_pkeyId;
                                customPhotoLabel_Filter_LoanTypeDTO.CustomPhotoLabel_Filter_LoanType_PkeyId = 0;
                                customPhotoLabel_Filter_LoanTypeDTO.CustomPhotoLabel_Filter_LoanType_IsActive = true;
                                customPhotoLabel_Filter_LoanTypeDTO.CustomPhotoLabel_Filter_LoanType_IsDelete = false;
                                customPhotoLabel_Filter_LoanTypeDTO.UserID = model.UserID;
                                customPhotoLabel_Filter_LoanTypeDTO.Type = 1;

                                var lonedata = customPhotoLabel_Filter_Data.AddUpdate_CustomPhotoLabel_Filter_LoanType(customPhotoLabel_Filter_LoanTypeDTO);

                            }
                        }
                        if (model.AutoAssinArray[i].Task_sett_State != null)
                        {
                            for (int p = 0; p < model.AutoAssinArray[i].Task_sett_State.Count; p++)
                            {
                                CustomPhotoLabel_Filter_StateDTO customPhotoLabel_Filter_StateDTO = new CustomPhotoLabel_Filter_StateDTO();

                                customPhotoLabel_Filter_StateDTO.CustomPhotoLabel_Filter_Master_FkeyId = objautoasn[0];
                                customPhotoLabel_Filter_StateDTO.Custom_PhotoLabel_Master_FkeyId = photoLabel_pkeyID;
                                customPhotoLabel_Filter_StateDTO.CustomPhotoLabel_Filter_State_SId = model.AutoAssinArray[i].Task_sett_State[p].IPL_StateID;
                                customPhotoLabel_Filter_StateDTO.CustomPhotoLabel_Filter_State_PkeyId = 0;
                                customPhotoLabel_Filter_StateDTO.CustomPhotoLabel_Filter_State_IsActive = true;
                                customPhotoLabel_Filter_StateDTO.CustomPhotoLabel_Filter_State_IsDelete = false;
                                customPhotoLabel_Filter_StateDTO.UserID = model.UserID;
                                customPhotoLabel_Filter_StateDTO.Type = 1;

                                var statedata = customPhotoLabel_Filter_Data.AddUpdate_CustomPhotoLabel_Filter_State(customPhotoLabel_Filter_StateDTO);
                            }
                        }
                        if (model.AutoAssinArray[i].WTTaskWorkType != null)
                        {
                            for (int q = 0; q < model.AutoAssinArray[i].WTTaskWorkType.Count; q++)
                            {
                                CustomPhotoLabel_Filter_WorkTypeDTO customPhotoLabel_Filter_WorkTypeDTO = new CustomPhotoLabel_Filter_WorkTypeDTO();

                                customPhotoLabel_Filter_WorkTypeDTO.CustomPhotoLabel_Filter_Master_FkeyId = objautoasn[0];
                                customPhotoLabel_Filter_WorkTypeDTO.Custom_PhotoLabel_Master_FkeyId = photoLabel_pkeyID;
                                customPhotoLabel_Filter_WorkTypeDTO.CustomPhotoLabel_Filter_WorkType_WId = model.AutoAssinArray[i].WTTaskWorkType[q].WT_pkeyID;
                                customPhotoLabel_Filter_WorkTypeDTO.CustomPhotoLabel_Filter_WorkType_PkeyId = 0;
                                customPhotoLabel_Filter_WorkTypeDTO.CustomPhotoLabel_Filter_WorkType_IsActive = true;
                                customPhotoLabel_Filter_WorkTypeDTO.CustomPhotoLabel_Filter_WorkType_IsDelete = false;
                                customPhotoLabel_Filter_WorkTypeDTO.UserID = model.UserID;
                                customPhotoLabel_Filter_WorkTypeDTO.Type = 1;

                                var worktypedata = customPhotoLabel_Filter_Data.AddUpdate_CustomPhotoLabel_Filter_WorkType(customPhotoLabel_Filter_WorkTypeDTO);
                            }
                        }
                        if (model.AutoAssinArray[i].Task_Work_TypeGroup != null)
                        {
                            for (int r = 0; r < model.AutoAssinArray[i].Task_Work_TypeGroup.Count; r++)
                            {
                                CustomPhotoLabel_Filter_WorkTypeGroupDTO customPhotoLabel_Filter_WorkTypeGroupDTO = new CustomPhotoLabel_Filter_WorkTypeGroupDTO();

                                customPhotoLabel_Filter_WorkTypeGroupDTO.CustomPhotoLabel_Filter_Master_FkeyId = objautoasn[0];
                                customPhotoLabel_Filter_WorkTypeGroupDTO.Custom_PhotoLabel_Master_FkeyId = photoLabel_pkeyID;
                                customPhotoLabel_Filter_WorkTypeGroupDTO.CustomPhotoLabel_Filter_WorkTypeGroup_WGId = model.AutoAssinArray[i].Task_Work_TypeGroup[r].Work_Type_Cat_pkeyID;
                                customPhotoLabel_Filter_WorkTypeGroupDTO.CustomPhotoLabel_Filter_WorkTypeGroup_PkeyId = 0;
                                customPhotoLabel_Filter_WorkTypeGroupDTO.CustomPhotoLabel_Filter_WorkTypeGroup_IsActive = true;
                                customPhotoLabel_Filter_WorkTypeGroupDTO.CustomPhotoLabel_Filter_WorkTypeGroup_IsDelete = false;
                                customPhotoLabel_Filter_WorkTypeGroupDTO.UserID = model.UserID;
                                customPhotoLabel_Filter_WorkTypeGroupDTO.Type = 1;

                                var workgroup = customPhotoLabel_Filter_Data.AddUpdate_CustomPhotoLabel_Filter_WorkTypeGroup(customPhotoLabel_Filter_WorkTypeGroupDTO);
                            }
                        }

                        objAddData.Add(objautoasn);
                    }
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objAddData;
        }
    }
}