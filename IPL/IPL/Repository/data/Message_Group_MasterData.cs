﻿using Avigma.Repository.Lib;
using IPL.Models;
using IPLApp.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace IPL.Repository.data
{
    public class Message_Group_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        private List<dynamic> CreateUpdateMessageGroupMaster(Message_Group_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_Message_Group_Master]";
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@MGM_PkeyID", 1 + "#bigint#" + model.MGM_PkeyID);
                input_parameters.Add("@MGM_Group_UserID", 1 + "#bigint#" + model.MGM_Group_UserID);
                input_parameters.Add("@MGM_Group_Name", 1 + "#nvarchar#" + model.MGM_Group_Name);
                input_parameters.Add("@MGM_Group_Creation_Date", 1 + "#datetime#" + model.MGM_Group_Creation_Date);
                input_parameters.Add("@MGM_Group_Status", 1 + "#int#" + model.MGM_Group_Status);
                input_parameters.Add("@MGM_Group_Description", 1 + "#nvarchar#" + model.MGM_Group_Description);
                input_parameters.Add("@MGM_IsActive", 1 + "#bit#" + model.MGM_IsActive);
                input_parameters.Add("@MGM_IsDelete", 1 + "#bit#" + model.MGM_IsDelete);
                input_parameters.Add("@MGM_UserjsonData", 1 + "#nvarchar#" + model.MGM_UserjsonData);
                input_parameters.Add("@MGM_Group_id", 1 + "#nvarchar#" + model.MGM_Group_id);
                input_parameters.Add("@MGM_Group_chat_type", 1 + "#int#" + model.MGM_Group_chat_type);
                //input_parameters.Add("@MGM_Group_CompanyId", 1 + "#bigint#" + model.MGM_Group_CompanyId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
              
                input_parameters.Add("@MGM_Pkey_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@MGM_Group_id_out", 2 + "#nvarchar#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);
                objData = obj.SqlCRUD(insertProcedure, input_parameters);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        public List<dynamic> AddMessageGroupMaster(Message_Group_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                Message_User_MasterData message_User_MasterData = new Message_User_MasterData();
                objData = CreateUpdateMessageGroupMaster(model);
                if (model.Type == 1 || model.Type == 2 || model.Type == 5 )
                {
                    if (!string.IsNullOrWhiteSpace(model.MGM_UserjsonData))
                    {
                        var Data = JsonConvert.DeserializeObject<List<UserMasterDTO>>(model.MGM_UserjsonData);
                        for (int i = 0; i < Data.Count; i++)
                        {
                            Message_User_MasterDTO message_User_MasterDTO = new Message_User_MasterDTO();
                            message_User_MasterDTO.MUM_User_PkeyID = Data[i].User_pkeyID;
                            message_User_MasterDTO.UserID = model.UserID;
                            message_User_MasterDTO.Type = 1;
                            message_User_MasterDTO.MUM_User_Group_PkeyID = objData[0];
                            message_User_MasterData.AddMessageUserMaster(message_User_MasterDTO);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }
            return objData;
        }


        private DataSet GetMessageGroupMaster(Message_Group_MasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Message_Group_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();
                input_parameters.Add("@MGM_PkeyID", 1 + "#bigint#" + model.MGM_PkeyID);
                input_parameters.Add("@whereclause", 1 + "#nvarchar#" + model.FilterName);
                input_parameters.Add("@Pagenumber", 1 + "#int#" + model.Pagenumber);
                input_parameters.Add("@Rownumber", 1 + "#int#" + model.Rownumber);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return ds;
        }
        public List<dynamic> GetMessageChatGroupMaster(Message_Group_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                string whereclause = string.Empty;
                if (model.Type == 4)
                {
                    if (!string.IsNullOrWhiteSpace(model.MGM_Group_Name))
                    {
                        whereclause = "And mgm.MGM_Group_Name like  '%" + model.MGM_Group_Name + "%'";
                    }
                    model.FilterName = whereclause;
                   
                    
                }
                DataSet ds = GetMessageGroupMaster(model);
                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

       

        private DataSet GetMessageGroupDropdowndata(Message_User_MasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Groupchat_UserList_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@MUM_User_Group_PkeyID", 1 + "#bigint#" + model.MUM_User_Group_PkeyID);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@whereclause", 1 + "#nvarchar#" + model.FilterUserName);
                input_parameters.Add("@Pagenumber", 1 + "#int#" + model.Pagenumber);
                input_parameters.Add("@Rownumber", 1 + "#int#" + model.Rownumber);
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return ds;
        }


        public List<dynamic>GetMessageChatGroupDrodown(Message_User_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                string whereclause = string.Empty;
                if (model.Type == 3)
                {
                    if (!string.IsNullOrWhiteSpace(model.User_FirstName))
                    {
                        whereclause = " AND (Usr.User_FirstName + ' ' + Usr.User_LastName) like  '%"+model.User_FirstName.Trim()+"%'";
                    }
                    model.FilterUserName = whereclause;


                }

                DataSet ds = GetMessageGroupDropdowndata(model);
                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

       

    
    }
}