﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class ContractorMapLatLondData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        private DataSet GetContractorMaplatlongData(ContractorMapLatLondDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_ContractorMapLatLong]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Zip_zip", 1 + "#bigint#" + model.Zip_zip);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetContractorMaplatlongDetails(ContractorMapLatLondDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {

                DataSet ds = GetContractorMaplatlongData(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<ContractorMapLatLondDTO> ContractorMaplatDetails =
                   (from item in myEnumerableFeaprd
                    select new ContractorMapLatLondDTO
                    {
                        Zip_ID = item.Field<Int64>("Zip_ID"),
                        Zip_zip = item.Field<Int64?>("Zip_zip"),
                        Zip_lat = item.Field<String>("Zip_lat"),
                        Zip_lng = item.Field<String>("Zip_lng"),
                        Zip_city = item.Field<String>("Zip_city"),
                        Zip_state_name = item.Field<String>("Zip_state_name"),
                     

                    }).ToList();

                objDynamic.Add(ContractorMaplatDetails);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}