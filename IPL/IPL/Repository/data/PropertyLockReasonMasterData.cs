﻿using Avigma.Repository.Lib;
using IPL.Models;
using IPLApp.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class PropertyLockReasonMasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        public List<dynamic> CreateUpdatePropertyLockReason(PropertyLockReasonMasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();
            PropertyLockReasonMaster propertyLockReasonMaster = new PropertyLockReasonMaster();
            string insertProcedure = "[CreateUpdate_PropertyLockReasonMaster]";
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@LockReason_PkeyID", 1 + "#bigint#" + model.LockReason_PkeyID);
                input_parameters.Add("@LockReason_Name", 1 + "#varchar#" + model.LockReason_Name);
                input_parameters.Add("@LockReason_IsActive", 1 + "#bit#" + model.LockReason_IsActive);
                input_parameters.Add("@LockReason_IsDelete", 1 + "#bit#" + model.LockReason_IsDelete);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@LockReason_PkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    propertyLockReasonMaster.LockReason_PkeyID = "0";
                    propertyLockReasonMaster.Status = "0";
                    propertyLockReasonMaster.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    propertyLockReasonMaster.LockReason_PkeyID = Convert.ToString(objData[0]);
                    propertyLockReasonMaster.Status = Convert.ToString(objData[1]);


                }
                objcltData.Add(propertyLockReasonMaster);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;



        }
        private DataSet GetPropertyLockReason(PropertyLockReasonMasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[GetPropertyLockReasonMaster]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@LockReason_PkeyID", 1 + "#bigint#" + model.LockReason_PkeyID);
                input_parameters.Add("@WhereClause", 1 + "#varchar#" + model.WhereClause);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetPropertyAlertMasterDetails(PropertyLockReasonMasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                string wherecondition = string.Empty;
                if (model.Type == 3 && !string.IsNullOrEmpty(model.FilterData))
                {
                    var Data = JsonConvert.DeserializeObject<PropertyLockReasonMasterDTO>(model.FilterData);
                    if (!string.IsNullOrEmpty(Data.LockReason_Name))
                    {
                        wherecondition = " And pa.LockReason_Name LIKE '%" + Data.LockReason_Name + "%'";
                    }
                    if (Data.LockReason_IsActive == true)
                    {
                        wherecondition = wherecondition + "  And pa.LockReason_IsActive =  1 ";
                    }
                    if (Data.LockReason_IsActive == false)
                    {
                        wherecondition = wherecondition + "  And pa.LockReason_IsActive =  0";
                    }
                    model.WhereClause = wherecondition;
                }

                DataSet ds = GetPropertyLockReason(model);

                if (ds.Tables.Count > 0)
                {
                    for (int i = 0; i < ds.Tables.Count; i++)
                    {
                        objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                    }

                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }

            return objDynamic;
        }
    }
}