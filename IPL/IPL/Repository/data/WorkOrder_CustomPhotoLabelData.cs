﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
namespace IPL.Repository.data
{
    public class WorkOrder_CustomPhotoLabelData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();


        public List<dynamic> AddWorkOrderPhotoLableData(WorkOrder_CustomPhotoLabelDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();

            string insertProcedure = "[CreateUpdateWorkOrder_CustomPhotoLabel]";
            WorkOrder_CustomPhotoLabeOut workOrder_CustomPhotoLabeOut = new WorkOrder_CustomPhotoLabeOut();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@WorkOrderPhotoLabel_pkeyID", 1 + "#bigint#" + model.WorkOrderPhotoLabel_pkeyID);
                input_parameters.Add("@WorkOrderPhotoLabel_PhotoLabel_ID", 1 + "#bigint#" + model.WorkOrderPhotoLabel_PhotoLabel_ID);
                input_parameters.Add("@WorkOrderPhotoLabel_WO_ID", 1 + "#bigint#" + model.WorkOrderPhotoLabel_WO_ID);
                input_parameters.Add("@WorkOrderPhotoLabel_IsActive", 1 + "#bit#" + model.WorkOrderPhotoLabel_IsActive);
                input_parameters.Add("@WorkOrderPhotoLabel_IsDelete", 1 + "#bit#" + model.WorkOrderPhotoLabel_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@WorkOrderPhotoLabel_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters); 
                if (objData[1] == 0)
                {
                    workOrder_CustomPhotoLabeOut.WorkOrderPhotoLabel_pkeyID = "0";
                    workOrder_CustomPhotoLabeOut.Status = "0";
                    workOrder_CustomPhotoLabeOut.ErrorMessage = "Error while Saviving in the Database";
                }
                else
                {
                    workOrder_CustomPhotoLabeOut.WorkOrderPhotoLabel_pkeyID = Convert.ToString(objData[0]);
                    workOrder_CustomPhotoLabeOut.Status = Convert.ToString(objData[1]);
                }
                objcltData.Add(workOrder_CustomPhotoLabeOut);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;



        }


        public List<dynamic> AddWorkOrderPhotoLableDataMutiple(WorkOrder_CustomPhotoLabelDTO model)
        {
            
            List<dynamic> objcltData = new List<dynamic>();

            WorkOrder_CustomPhotoLabelDTO workOrder_CustomPhotoLabelDTO = new WorkOrder_CustomPhotoLabelDTO();



            try
            {
                for (int i = 0; i < model.Custom_PhotoLabel_MasterDTO.Count; i++)
                {
                    if (model.Custom_PhotoLabel_MasterDTO[i].Custom_label_Check == true)
                    {
                        workOrder_CustomPhotoLabelDTO.WorkOrderPhotoLabel_IsDelete = false;
                    }
                    else
                    {
                        workOrder_CustomPhotoLabelDTO.WorkOrderPhotoLabel_IsDelete = true;
                    }
                    workOrder_CustomPhotoLabelDTO.WorkOrderPhotoLabel_pkeyID = model.WorkOrderPhotoLabel_pkeyID;
                    workOrder_CustomPhotoLabelDTO.WorkOrderPhotoLabel_PhotoLabel_ID = model.Custom_PhotoLabel_MasterDTO[i].PhotoLabel_pkeyID;
                    workOrder_CustomPhotoLabelDTO.WorkOrderPhotoLabel_WO_ID = model.WorkOrderPhotoLabel_WO_ID;
                    workOrder_CustomPhotoLabelDTO.WorkOrderPhotoLabel_IsActive = model.WorkOrderPhotoLabel_IsActive;
                    
                    
                    workOrder_CustomPhotoLabelDTO.UserID = model.UserID;
                    workOrder_CustomPhotoLabelDTO.Type = model.Type;

                    var xxx = AddWorkOrderPhotoLableData(workOrder_CustomPhotoLabelDTO);
                }

               
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;



        }

        //private DataSet GetCustomLabelMaster(Custom_PhotoLabel_MasterDTO model)
        //{
        //    DataSet ds = null;
        //    try
        //    {
        //        string selectProcedure = "[Get_Custom_PhotoLabel_Master]";
        //        Dictionary<string, string> input_parameters = new Dictionary<string, string>();

        //        input_parameters.Add("@Custom_PhotoLabel_MasterDTO", 1 + "#bigint#" + model.PhotoLabel_pkeyID);
        //        input_parameters.Add("@Type", 1 + "#int#" + model.Type);

        //        ds = obj.SelectSql(selectProcedure, input_parameters);
        //    }
        //    catch (Exception ex)
        //    {
        //        log.logErrorMessage(ex.StackTrace);
        //    }

        //    return ds;
        //}

        //public List<dynamic> GetCustomlabelDetails(Custom_PhotoLabel_MasterDTO model)
        //{
        //    List<dynamic> objDynamic = new List<dynamic>();
        //    try
        //    {
        //        DataSet ds = GetCustomLabelMaster(model);

        //        var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
        //        List<Custom_PhotoLabel_MasterDTO> CustomeLDetails =
        //           (from item in myEnumerableFeaprd
        //            select new Custom_PhotoLabel_MasterDTO
        //            {
        //                PhotoLabel_pkeyID = item.Field<Int64>("PhotoLabel_pkeyID"),
        //                PhotoLabel_Name = item.Field<String>("PhotoLabel_Name"),
        //                PhotoLabel_IsActive = item.Field<Boolean?>("PhotoLabel_IsActive"),
        //                Custom_label_Check = true,

        //            }).ToList();

        //        objDynamic.Add(CustomeLDetails);
        //    }
        //    catch (Exception ex)
        //    {
        //        log.logErrorMessage(ex.StackTrace);
        //log.logErrorMessage(ex.Message);
        //    }

        //    return objDynamic;
        //}
    }
}