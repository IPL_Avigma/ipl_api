﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace IPL.Repository.data
{
    public class WorkType_Template_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        private List<dynamic> CreateUpdateWorkTypeTemplateMaster(WorkType_Template_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_WorkType_Template_Master]";
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@WTT_pkeyID", 1 + "#bigint#" + model.WTT_pkeyID);
                input_parameters.Add("@WTT_Template_Name", 1 + "#nvarchar#" + model.WTT_Template_Name);
                input_parameters.Add("@WTT_IPL_Company_PkeyId", 1 + "#bigint#" + model.WTT_IPL_Company_PkeyId);
                input_parameters.Add("@WTT_Template_Description", 1 + "#nvarchar#" + model.WTT_Template_Description);
                input_parameters.Add("@WTT_IsActive", 1 + "#bit#" + model.WTT_IsActive);
                input_parameters.Add("@WTT_Delete", 1 + "#bit#" + model.WTT_Delete);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@WTT_pkey_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);
                objData = obj.SqlCRUD(insertProcedure, input_parameters);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        public List<dynamic> AddWorkTypeTemplateMaster(WorkType_Template_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                objData = CreateUpdateWorkTypeTemplateMaster(model);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }
            return objData;
        }


        private DataSet GetWorkTypeTemplateMaster(WorkType_Template_MasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_WorkType_Template_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@WTT_pkeyID", 1 + "#bigint#" + model.WTT_pkeyID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }


        public List<dynamic> GetWorkTypeTemplateDetails(WorkType_Template_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetWorkTypeTemplateMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<WorkType_Template_MasterDTO> UserDocDetails =
                   (from item in myEnumerableFeaprd
                    select new WorkType_Template_MasterDTO
                    {
                        WTT_pkeyID = item.Field<Int64>("WTT_pkeyID"),
                        WTT_Template_Name = item.Field<String>("WTT_Template_Name"),
                        WTT_IPL_Company_PkeyId = item.Field<Int64>("WTT_IPL_Company_PkeyId"),
                        WTT_Template_Description = item.Field<String>("WTT_Template_Description"),
                        WTT_IsDeleteAllow = item.Field<Boolean>("WTT_IsDeleteAllow"),
                        WTT_IsActive = item.Field<Boolean>("WTT_IsActive"),
                        WTT_CreatedBy = item.Field<String>("WTT_CreatedBy"),
                        WTT_ModifiedBy = item.Field<String>("WTT_ModifiedBy"),

                    }).ToList();

                objDynamic.Add(UserDocDetails);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}