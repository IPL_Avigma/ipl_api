﻿using Avigma.Repository.Lib;
using IPL.Models;
using IPLApp.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class Loan_Status_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        private List<dynamic> CreateUpdateLoanStatusMaster(Loan_Status_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();
            Loan_Status_Master loan_Status_Master = new Loan_Status_Master();
            string insertProcedure = "[CreateUpdate_Loan_Status_Master]";
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@LS_PkeyID", 1 + "#bigint#" + model.LS_PkeyID);
                input_parameters.Add("@LS_Name", 1 + "#varchar#" + model.LS_Name);
                input_parameters.Add("@LS_IsActive", 1 + "#bit#" + model.LS_IsActive);
                input_parameters.Add("@LS_IsDelete", 1 + "#bit#" + model.LS_IsDelete);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@LS_Pkey_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters); ;
                if (objData[1] == 0)
                {
                    loan_Status_Master.LS_PkeyID = "0";
                    loan_Status_Master.Status = "0";
                    loan_Status_Master.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    loan_Status_Master.LS_PkeyID = Convert.ToString(objData[0]);
                    loan_Status_Master.Status = Convert.ToString(objData[1]);


                }
                objcltData.Add(loan_Status_Master);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;



        }


        private DataSet GetLoanStatusMaster(Loan_Status_MasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Loan_Status_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@LS_PkeyID", 1 + "#bigint#" + model.LS_PkeyID);
                input_parameters.Add("@WhereClause", 1 + "#varchar#" + model.WhereClause);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> CreateUpdateLoanStatusMasterDetails(Loan_Status_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                objData = CreateUpdateLoanStatusMaster(model);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }
            return objData;
        }

        public List<dynamic> GetLoanStatusMasterDetails(Loan_Status_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {

                string wherecondition = string.Empty;
                if (model.Type == 3 && !string.IsNullOrEmpty(model.FilterData))
                {
                    var Data = JsonConvert.DeserializeObject<Loan_Status_MasterDTO>(model.FilterData);
                    if (!string.IsNullOrEmpty(Data.LS_Name))
                    {
                        wherecondition = " And ls.LS_Name LIKE '%" + Data.LS_Name + "%'";
                        //wherecondition = " And Task_Name LIKE '%" + Data.Task_Name + "%'";
                    }

                    if (Data.LS_IsActive == true)
                    {
                        wherecondition = wherecondition + "  And ls.LS_IsActive =  1 ";
                    }
                    if (Data.LS_IsActive == false)
                    {
                        wherecondition = wherecondition + "  And ls.LS_IsActive =  0";
                    }
                    model.WhereClause = wherecondition;
                }


                DataSet ds = GetLoanStatusMaster(model);

                if (ds.Tables.Count > 0)
                {
                    for (int i = 0; i < ds.Tables.Count; i++)
                    {
                        objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                    }

                }
                //objDynamic.Add(Get_details);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }

            return objDynamic;
        }
    }
}