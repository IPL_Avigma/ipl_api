﻿using Avigma.Repository.Lib;
using IPL.Models;
using IPLApp.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPLApp.Repository.data
{
    public class IPLStateData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();


        public List<dynamic> AddSateData(IPLStateDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();

            string insertProcedure = "[CreateUpdateStateMaster]";
            IPLState iPLState = new IPLState();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@IPL_StateID", 1 + "#bigint#" + model.IPL_StateID);
                input_parameters.Add("@IPL_StateName", 1 + "#varchar#" + model.IPL_StateName);
                input_parameters.Add("@IPL_State_IsActive", 1 + "#bit#" + model.IPL_State_IsActive);
                input_parameters.Add("@IPL_IsDelete", 1 + "#bit#" + model.IPL_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@IPL_StateID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData =  obj.SqlCRUD(insertProcedure, input_parameters); ;
                if (objData[1] == 0)
                {
                    iPLState.IPL_StateID = "0";
                    iPLState.Status = "0";
                    iPLState.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    iPLState.IPL_StateID = Convert.ToString(objData[0]);
                    iPLState.Status = Convert.ToString(objData[1]);


                }
                objcltData.Add(iPLState);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;



        }

        private DataSet GetStateMaster(IPLStateDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_State_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@IPL_StateID", 1 + "#bigint#" + model.IPL_StateID);
                input_parameters.Add("@WhereClause", 1 + "#varchar#" + model.WhereClause);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetStateDetails(IPLStateDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetStateMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<IPLStateDTO> StateDetails =
                   (from item in myEnumerableFeaprd
                    select new IPLStateDTO
                    {
                        IPL_StateID = item.Field<Int64>("IPL_StateID"),
                        IPL_StateName = item.Field<String>("IPL_StateName"),
                        IPL_State_IsActive = item.Field<Boolean?>("IPL_State_IsActive"),
                        IPL_State_CreatedBy = item.Field<String>("IPL_State_CreatedBy"),
                        IPL_State_ModifiedBy = item.Field<String>("IPL_State_ModifiedBy"),
                        IPL_State_IsDeleteAllow = item.Field<Boolean?>("IPL_State_IsDeleteAllow")
                    }).ToList();

                objDynamic.Add(StateDetails);
                if (model.Type == 1)
                {
                    if (ds.Tables.Count > 1)
                    {
                        var myEnumerableFeaprd1 = ds.Tables[1].AsEnumerable();
                        List<Filter_Admin_State_MasterDTO> Filterstate =
                           (from item in myEnumerableFeaprd1
                            select new Filter_Admin_State_MasterDTO
                            {
                                State_Filter_PkeyId = item.Field<Int64>("State_Filter_PkeyId"),
                                State_Filter_Name = item.Field<String>("State_Filter_Name"),
                                State_Filter_IsStateActive = item.Field<Boolean?>("State_Filter_IsStateActive"),
                                State_Filter_CreatedBy = item.Field<String>("State_Filter_CreatedBy"),
                                State_Filter_ModifiedBy = item.Field<String>("State_Filter_ModifiedBy"),
                            }).ToList();

                        objDynamic.Add(Filterstate);
                    }
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
        // filter arr
        public List<dynamic> GetStateFilterDetails(IPLStateDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();


            string wherecondition = string.Empty;
            try
            {

                var Data = JsonConvert.DeserializeObject<IPLStateDTO>(model.FilterData);
                if (!string.IsNullOrEmpty(Data.IPL_StateName))
                {
                    wherecondition = " And st.IPL_StateName LIKE '%" + Data.IPL_StateName + "%'";
                }


                if (Data.IPL_State_IsActive == true)
                {
                    wherecondition = wherecondition + "  And st.IPL_State_IsActive =  '1'";
                }
                if (Data.IPL_State_IsActive == false)
                {
                    wherecondition = wherecondition + "  And st.IPL_State_IsActive =  '0'";
                }


                IPLStateDTO iPLStateDTO = new IPLStateDTO();

                iPLStateDTO.WhereClause = wherecondition;
                iPLStateDTO.Type = 3;
                iPLStateDTO.UserID = model.UserID;
                DataSet ds = GetStateMaster(iPLStateDTO);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<IPLStateDTO> statefilter =
                   (from item in myEnumerableFeaprd
                    select new IPLStateDTO
                    {

                        IPL_StateID = item.Field<Int64>("IPL_StateID"),
                        IPL_StateName = item.Field<String>("IPL_StateName"),
                        IPL_State_IsActive = item.Field<Boolean?>("IPL_State_IsActive"),
                        IPL_State_CreatedBy = item.Field<String>("IPL_State_CreatedBy"),
                        IPL_State_ModifiedBy = item.Field<String>("IPL_State_ModifiedBy"),
                    }).ToList();

                objDynamic.Add(statefilter);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }

            return objDynamic;
        }
    }
}