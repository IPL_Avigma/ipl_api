﻿using Avigma.Repository.Lib;
using IPL.Models;
using IPL.Models.FiveBrothers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class WorkOrder_Altisource_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        private List<dynamic> AddWorkOrder_AltisourceData(WorkOrder_Altisource_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_WorkOrder_Altisource_Master]";
            WorkOrder_Altisource_Master WorkOrder_Altisource_Master = new WorkOrder_Altisource_Master();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@WAltisource_PkeyID", 1 + "#bigint#" + model.WAltisource_PkeyID);
                input_parameters.Add("@WAltisource_wo_id", 1 + "#nvarchar#" + model.WAltisource_wo_id);
                input_parameters.Add("@WAltisource_category", 1 + "#nvarchar#" + model.WAltisource_category);

                input_parameters.Add("@WAltisource_address", 1 + "#nvarchar#" + model.WAltisource_address);
                input_parameters.Add("@WAltisource_city", 1 + "#nvarchar#" + model.WAltisource_city);
                input_parameters.Add("@WAltisource_state", 1 + "#nvarchar#" + model.WAltisource_state);
                input_parameters.Add("@WAltisource_zip", 1 + "#nvarchar#" + model.WAltisource_zip);
                input_parameters.Add("@WAltisource_estimated_complete_date", 1 + "#datetime#" + model.WAltisource_estimated_complete_date);
                input_parameters.Add("@WAltisource_loan_number", 1 + "#nvarchar#" + model.WAltisource_loan_number);
                input_parameters.Add("@WAltisource_loan_type", 1 + "#nvarchar#" + model.WAltisource_loan_type);
                input_parameters.Add("@WAltisource_due_date", 1 + "#datetime#" + model.WAltisource_due_date);
                input_parameters.Add("@WAltisource_client_Due_date", 1 + "#datetime#" + model.WAltisource_client_Due_date);
                input_parameters.Add("@WAltisource_username", 1 + "#nvarchar#" + model.WAltisource_username);
                input_parameters.Add("@WAltisource_lot_size", 1 + "#nvarchar#" + model.WAltisource_lot_size);
                input_parameters.Add("@WAltisource_received_date", 1 + "#datetime#" + model.WAltisource_received_date);
                input_parameters.Add("@WAltisource_customer", 1 + "#nvarchar#" + model.WAltisource_customer);
                input_parameters.Add("@WAltisource_client_company", 1 + "#nvarchar#" + model.WAltisource_client_company);
                input_parameters.Add("@WAltisource_ppw", 1 + "#nvarchar#" + model.WAltisource_ppw);
                input_parameters.Add("@WAltisource_start_date", 1 + "#datetime#" + model.WAltisource_start_date);
                input_parameters.Add("@WAltisource_contractor", 1 + "#nvarchar#" + model.WAltisource_contractor);
                input_parameters.Add("@WAltisource_missing_info", 1 + "#nvarchar#" + model.WAltisource_missing_info);
                input_parameters.Add("@WAltisource_work_type", 1 + "#nvarchar#" + model.WAltisource_work_type);
                input_parameters.Add("@WAltisource_assigned_admin", 1 + "#nvarchar#" + model.WAltisource_assigned_admin);
                input_parameters.Add("@WAltisource_lock_code", 1 + "#nvarchar#" + model.WAltisource_lock_code);
                input_parameters.Add("@WAltisource_key_code", 1 + "#nvarchar#" + model.WAltisource_key_code);
                input_parameters.Add("@WAltisource_wo_status", 1 + "#nvarchar#" + model.WAltisource_wo_status);
                input_parameters.Add("@WAltisource_mortgager", 1 + "#nvarchar#" + model.WAltisource_mortgager);
                input_parameters.Add("@WAltisource_ready_for_office_date", 1 + "#datetime#" + model.WAltisource_ready_for_office_date);
                input_parameters.Add("@WAltisource_id", 1 + "#nvarchar#" + model.WAltisource_id);



                input_parameters.Add("@WAltisource_comments", 1 + "#nvarchar#" + model.WAltisource_comments);
                input_parameters.Add("@WAltisource_gpsLatitude", 1 + "#varchar#" + model.WAltisource_gpsLatitude);
                input_parameters.Add("@WAltisource_gpsLongitude", 1 + "#varchar#" + model.WAltisource_gpsLongitude);
                input_parameters.Add("@WAltisource_ImportMaster_Pkey", 1 + "#bigint#" + model.WAltisource_ImportMaster_Pkey);
                input_parameters.Add("@WAltisource_Import_File_Name", 1 + "#nvarchar#" + model.WAltisource_Import_File_Name);
                input_parameters.Add("@WAltisource_Import_FilePath", 1 + "#varchar#" + model.WAltisource_Import_FilePath);
                input_parameters.Add("@WAltisource_Import_Pdf_Name", 1 + "#varchar#" + model.WAltisource_Import_Pdf_Name);
                input_parameters.Add("@WAltisource_Import_Pdf_Path", 1 + "#varchar#" + model.WAltisource_Import_Pdf_Path);








                input_parameters.Add("@WAltisource_IsActive", 1 + "#bit#" + model.WAltisource_IsActive);
                input_parameters.Add("@WAltisource_IsDelete", 1 + "#bit#" + model.WAltisource_IsDelete);

                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@WAltisource_Pkey_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    WorkOrder_Altisource_Master.WAltisource_PkeyID = "0";
                    WorkOrder_Altisource_Master.Status = "0";
                    WorkOrder_Altisource_Master.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    WorkOrder_Altisource_Master.WAltisource_PkeyID = Convert.ToString(objData[0]);
                    WorkOrder_Altisource_Master.Status = Convert.ToString(objData[1]);


                }
                objcltData.Add(WorkOrder_Altisource_Master);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;

        }

        private List<dynamic> AddWorkOrder_AltisourceItemData(WorkOrder_Altisource_Item_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_WorkOrder_Altisource_Item_Master]";
            WorkOrder_Altisource_Item_Master WorkOrder_Altisource_Item_Master = new WorkOrder_Altisource_Item_Master();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@WALTISOURCEINS_PkeyId", 1 + "#bigint#" + model.WALTISOURCEINS_PkeyId);
                input_parameters.Add("@WALTISOURCEINS_Ins_Name", 1 + "#varchar#" + model.WALTISOURCEINS_Ins_Name);
                input_parameters.Add("@WALTISOURCEINS_Ins_Details", 1 + "#varchar#" + model.WALTISOURCEINS_Ins_Details);
                input_parameters.Add("@WALTISOURCEINS_Qty", 1 + "#bigint#" + model.WALTISOURCEINS_Qty);
                input_parameters.Add("@WALTISOURCEINS_Price", 1 + "#decimal#" + model.WALTISOURCEINS_Price);
                input_parameters.Add("@WALTISOURCEINS_Total", 1 + "#decimal#" + model.WALTISOURCEINS_Total);
                input_parameters.Add("@WALTISOURCEINS_FkeyID", 1 + "#bigint#" + model.WALTISOURCEINS_FkeyID);

                input_parameters.Add("@WALTISOURCEINS_IsActive", 1 + "#bit#" + model.WALTISOURCEINS_IsActive);
                input_parameters.Add("@WALTISOURCEINS_IsDelete", 1 + "#bit#" + model.WALTISOURCEINS_IsDelete);
                input_parameters.Add("@WALTISOURCEINS_Additional_Details", 1 + "#nvarchar#" + model.WALTISOURCEINS_Additional_Details);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@WALTISOURCEINS_Pkey_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    WorkOrder_Altisource_Item_Master.WALTISOURCEINS_PkeyId = "0";
                    WorkOrder_Altisource_Item_Master.Status = "0";
                    WorkOrder_Altisource_Item_Master.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    WorkOrder_Altisource_Item_Master.WALTISOURCEINS_PkeyId = Convert.ToString(objData[0]);
                    WorkOrder_Altisource_Item_Master.Status = Convert.ToString(objData[1]);

                }
                objcltData.Add(WorkOrder_Altisource_Item_Master);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;



        }

        public List<dynamic> AddWorkOrderAltisourceData(WorkOrder_Altisource_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                objData = AddWorkOrder_AltisourceData(model);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }


        public List<dynamic> AddWorkOrderAltisourceItemData(WorkOrder_Altisource_Item_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                objData = AddWorkOrder_AltisourceItemData(model);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }
    }
}