﻿using Avigma.Repository.Lib;
using IPL.Models;
using IPL.Models.FiveBrothers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class WorkOrder_ServiceLink_History_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();


        private List<dynamic> AddWorkOrder_ServiceLinkHistoryData(WorkOrder_ServiceLink_History_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_WorkOrder_ServiceLink_History_Master]";
            WorkOrder_ServiceLink_History_Master WorkOrder_ServiceLink_History_Master = new WorkOrder_ServiceLink_History_Master();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@WServiceLink_HS_PkeyID ", 1 + "#bigint#" + model.WServiceLink_HS_PkeyID);
                input_parameters.Add("@WServiceLink_HS_wo_id", 1 + "#nvarchar#" + model.WServiceLink_HS_wo_id);

                input_parameters.Add("@WServiceLink_HS_Address", 1 + "#nvarchar#" + model.WServiceLink_HS_Address);
                input_parameters.Add("@WServiceLink_HS_City", 1 + "#nvarchar#" + model.WServiceLink_HS_City);
                input_parameters.Add("@WServiceLink_HS_State", 1 + "#nvarchar#" + model.WServiceLink_HS_State);
                input_parameters.Add("@WServiceLink_HS_ZipCode", 1 + "#nvarchar#" + model.WServiceLink_HS_ZipCode);
                input_parameters.Add("@WServiceLink_HS_DueDate", 1 + "#datetime#" + model.WServiceLink_HS_DueDate);
                input_parameters.Add("@WServiceLink_HS_KeyCode", 1 + "#nvarchar#" + model.WServiceLink_HS_KeyCode);
                input_parameters.Add("@WServiceLink_HS_LockBox", 1 + "#nvarchar#" + model.WServiceLink_HS_LockBox);
                input_parameters.Add("@WServiceLink_HS_ClientID", 1 + "#nvarchar#" + model.WServiceLink_HS_ClientID);
                input_parameters.Add("@WServiceLink_HS_Loan", 1 + "#nvarchar#" + model.WServiceLink_HS_Loan);
                input_parameters.Add("@WServiceLink_HS_LoanType", 1 + "#nvarchar#" + model.WServiceLink_HS_LoanType);

                input_parameters.Add("@WServiceLink_HS_Owner", 1 + "#nvarchar#" + model.WServiceLink_HS_Owner);
                input_parameters.Add("@WServiceLink_HS_CoverageID", 1 + "#nvarchar#" + model.WServiceLink_HS_CoverageID);
                input_parameters.Add("@WServiceLink_HS_Investor", 1 + "#nvarchar#" + model.WServiceLink_HS_Investor);
                input_parameters.Add("@WServiceLink_HS_services", 1 + "#nvarchar#" + model.WServiceLink_HS_services);
                input_parameters.Add("@WServiceLink_HS_id", 1 + "#nvarchar#" + model.WServiceLink_HS_id);
                input_parameters.Add("@WServiceLink_HS_username", 1 + "#nvarchar#" + model.WServiceLink_HS_username);

                input_parameters.Add("@WServiceLink_HS_Comments", 1 + "#varchar#" + model.WServiceLink_HS_Comments);
                input_parameters.Add("@WServiceLink_HS_gpsLatitude", 1 + "#varchar#" + model.WServiceLink_HS_gpsLatitude);
                input_parameters.Add("@WServiceLink_HS_gpsLongitude", 1 + "#varchar#" + model.WServiceLink_HS_gpsLongitude);
                input_parameters.Add("@WServiceLink_HS_ImportMaster_Pkey", 1 + "#bigint#" + model.WServiceLink_HS_ImportMaster_Pkey);
                input_parameters.Add("@WServiceLink_HS_Import_File_Name", 1 + "#varchar#" + model.WServiceLink_HS_Import_File_Name);
                input_parameters.Add("@WServiceLink_HS_Import_FilePath", 1 + "#varchar#" + model.WServiceLink_HS_Import_FilePath);
                input_parameters.Add("@WServiceLink_HS_Import_Pdf_Name", 1 + "#varchar#" + model.WServiceLink_HS_Import_Pdf_Name);
                input_parameters.Add("@WServiceLink_HS_Import_Pdf_Path", 1 + "#varchar#" + model.WServiceLink_HS_Import_Pdf_Path);





                input_parameters.Add("@WServiceLink_HS_IsActive", 1 + "#bit#" + model.WServiceLink_HS_IsActive);
                input_parameters.Add("@WServiceLink_HS_IsDelete", 1 + "#bit#" + model.WServiceLink_HS_IsDelete);
                input_parameters.Add("@WServiceLink_HS_Service_ID", 1 + "#nvarchar#" + model.WServiceLink_HS_Service_ID);

                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@WServiceLink_HS_Pkey_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    WorkOrder_ServiceLink_History_Master.WServiceLink_HS_PkeyID = "0";
                    WorkOrder_ServiceLink_History_Master.Status = "0";
                    WorkOrder_ServiceLink_History_Master.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    WorkOrder_ServiceLink_History_Master.WServiceLink_HS_PkeyID = Convert.ToString(objData[0]);
                    WorkOrder_ServiceLink_History_Master.Status = Convert.ToString(objData[1]);


                }
                objcltData.Add(WorkOrder_ServiceLink_History_Master);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;

        }


        private List<dynamic> AddWorkOrder_ServiceLinkItemHistoryData(WorkOrder_ServiceLink_Item_History_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_WorkOrder_ServiceLink_Item_History_Master]";
            WorkOrder_ServiceLink_Item_History_Master WorkOrder_ServiceLink_Item_History_Master = new WorkOrder_ServiceLink_Item_History_Master();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@WSERVICEINS_HS_PkeyId", 1 + "#bigint#" + model.WSERVICEINS_HS_PkeyId);
                input_parameters.Add("@WSERVICEINS_HS_Ins_Name", 1 + "#varchar#" + model.WSERVICEINS_HS_Ins_Name);
                input_parameters.Add("@WSERVICEINS_HS_Ins_Details", 1 + "#varchar#" + model.WSERVICEINS_HS_Ins_Details);
                input_parameters.Add("@WSERVICEINS_HS_Qty", 1 + "#bigint#" + model.WSERVICEINS_HS_Qty);
                input_parameters.Add("@WSERVICEINS_HS_Price", 1 + "#decimal#" + model.WSERVICEINS_HS_Price);
                input_parameters.Add("@WSERVICEINS_HS_Total", 1 + "#decimal#" + model.WSERVICEINS_HS_Total);
                input_parameters.Add("@WSERVICEINS_HS_FkeyID", 1 + "#bigint#" + model.WSERVICEINS_HS_FkeyID);

                input_parameters.Add("@WSERVICEINS_HS_IsActive", 1 + "#bit#" + model.WSERVICEINS_HS_IsActive);
                input_parameters.Add("@WSERVICEINS_HS_IsDelete", 1 + "#bit#" + model.WSERVICEINS_HS_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@WSERVICEINS_HS_Pkey_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    WorkOrder_ServiceLink_Item_History_Master.WSERVICEINS_HS_PkeyId = "0";
                    WorkOrder_ServiceLink_Item_History_Master.Status = "0";
                    WorkOrder_ServiceLink_Item_History_Master.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    WorkOrder_ServiceLink_Item_History_Master.WSERVICEINS_HS_PkeyId = Convert.ToString(objData[0]);
                    WorkOrder_ServiceLink_Item_History_Master.Status = Convert.ToString(objData[1]);

                }
                objcltData.Add(WorkOrder_ServiceLink_Item_History_Master);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;



        }

        public List<dynamic> AddWorkOrderServiceLinkHistoryData(WorkOrder_ServiceLink_History_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                objData = AddWorkOrder_ServiceLinkHistoryData(model);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        public List<dynamic> AddWorkOrderServiceLinkItemHistoryData(WorkOrder_ServiceLink_Item_History_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                objData = AddWorkOrder_ServiceLinkItemHistoryData(model);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }
    }
}