﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class Instruction_Client_Data
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();


        public List<dynamic> Addinstruction_Json_Master(Instruction_Json_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_Instruction_Json_Master]";


            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Instruction_Json_PkeyId", 1 + "#bigint#" + model.Instruction_Json_PkeyId);
                input_parameters.Add("@Instruction_Json_Inst_Id", 1 + "#bigint#" + model.Instruction_Json_Inst_Id);
                input_parameters.Add("@Instruction_Json_Client", 1 + "#nvarchar#" + model.Instruction_Json_Client);
                input_parameters.Add("@Instruction_Json_WorkType", 1 + "#nvarchar#" + model.Instruction_Json_WorkType);
                input_parameters.Add("@Instruction_Json_LoanType", 1 + "#nvarchar#" + model.Instruction_Json_LoanType);
                input_parameters.Add("@Instruction_Json_Customer", 1 + "#nvarchar#" + model.Instruction_Json_Customer);
                input_parameters.Add("@Instruction_Json_Work_Group", 1 + "#nvarchar#" + model.Instruction_Json_Work_Group);
                input_parameters.Add("@Instruction_Json_IsActive", 1 + "#bit#" + model.Instruction_Json_IsActive);
                input_parameters.Add("@Instruction_Json_IsDelete", 1 + "#bit#" + model.Instruction_Json_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Instruction_Json_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;



        }
        public List<dynamic> Addinstruction_Client_Master(Instruction_ClientDTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_Instruction_Client_Master]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Intruction_Client_PkeyId", 1 + "#bigint#" + model.Intruction_Client_PkeyId);
                input_parameters.Add("@Intruction_Client_CilentId", 1 + "#bigint#" + model.Intruction_Client_CilentId);
                input_parameters.Add("@Intruction_Client_instId", 1 + "#bigint#" + model.Intruction_Client_instId);
                input_parameters.Add("@Intruction_Client_InstJsonId", 1 + "#bigint#" + model.Intruction_Client_InstJsonId);
                input_parameters.Add("@Intruction_Client_IsActive", 1 + "#bit#" + model.Intruction_Client_IsActive);
                input_parameters.Add("@Intruction_Client_IsDelete", 1 + "#bit#" + model.Intruction_Client_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Intruction_Client_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;



        }

        public List<dynamic> Addinstruction_WorkType_Master(Instruction_WorkType_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_Instruction_WorkType_Master]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Instruction_workType_PkeyId", 1 + "#bigint#" + model.Instruction_workType_PkeyId);
                input_parameters.Add("@Instruction_workType_WT_Id", 1 + "#bigint#" + model.Instruction_workType_WT_Id);
                input_parameters.Add("@Instruction_workType_InstId", 1 + "#bigint#" + model.Instruction_workType_InstId);
                input_parameters.Add("@Instruction_workType_Inst_Json_Id", 1 + "#bigint#" + model.Instruction_workType_Inst_Json_Id);
                input_parameters.Add("@Instruction_workType_IsActive", 1 + "#bit#" + model.Instruction_workType_IsActive);
                input_parameters.Add("@Instruction_workType_IsDelete", 1 + "#bit#" + model.Instruction_workType_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Instruction_workType_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;



        }
        public List<dynamic> Addinstruction_LoanType_Master(Instruction_LoanType_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_Instruction_LoanType_Master]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Instruction_Loan_PkeyId", 1 + "#bigint#" + model.Instruction_Loan_PkeyId);
                input_parameters.Add("@Instruction_Loan_LoanId", 1 + "#bigint#" + model.Instruction_Loan_LoanId);
                input_parameters.Add("@Instruction_Loan_InstId", 1 + "#bigint#" + model.Instruction_Loan_InstId);
                input_parameters.Add("@Instruction_Loan_Inst_JsonId", 1 + "#bigint#" + model.Instruction_Loan_Inst_JsonId);
                input_parameters.Add("@Instruction_Loan_IsActive", 1 + "#bit#" + model.Instruction_Loan_IsActive);
                input_parameters.Add("@Instruction_Loan_IsDelete", 1 + "#bit#" + model.Instruction_Loan_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Instruction_Loan_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;



        }

        public List<dynamic> Addinstruction_Customer_Master(Instruction_Customer_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_Instruction_Customer_Master]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Instruction_Cust_PkeyId", 1 + "#bigint#" + model.Instruction_Cust_PkeyId);
                input_parameters.Add("@Instruction_Cust_CustId", 1 + "#bigint#" + model.Instruction_Cust_CustId);
                input_parameters.Add("@Instruction_Cust_InstId", 1 + "#bigint#" + model.Instruction_Cust_InstId);
                input_parameters.Add("@Instruction_Cust_InstJson_Id", 1 + "#bigint#" + model.Instruction_Cust_InstJson_Id);
                input_parameters.Add("@Instruction_Cust_IsActive", 1 + "#bit#" + model.Instruction_Cust_IsActive);
                input_parameters.Add("@Instruction_Cust_IsDelete", 1 + "#bit#" + model.Instruction_Cust_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Instruction_Cust_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;



        }

        public List<dynamic> Addinstruction_WorkType_Group_Master(Instruction_WT_Group_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_Instruction_WT_Group_Master]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Instruction_Wt_Group_PkeyId", 1 + "#bigint#" + model.Instruction_Wt_Group_PkeyId);
                input_parameters.Add("@Instruction_Wt_Group_Wtg_Id", 1 + "#bigint#" + model.Instruction_Wt_Group_Wtg_Id);
                input_parameters.Add("@Instruction_Wt_Group_Inst_Id", 1 + "#bigint#" + model.Instruction_Wt_Group_Inst_Id);
                input_parameters.Add("@Instruction_Wt_Group_InstJsonId", 1 + "#bigint#" + model.Instruction_Wt_Group_InstJsonId);
                input_parameters.Add("@Instruction_Wt_Group_IsActive", 1 + "#bit#" + model.Instruction_Wt_Group_IsActive);
                input_parameters.Add("@Instruction_Wt_Group_IsDelete", 1 + "#bit#" + model.Instruction_Wt_Group_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Instruction_Wt_Group_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;



        }


        private DataSet GetInstructionJsonDetails(Instruction_Json_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Instruction_Json_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Instruction_Json_PkeyId", 1 + "#bigint#" + model.Instruction_Json_PkeyId);
                input_parameters.Add("@Instruction_Json_Inst_Id", 1 + "#bigint#" + model.Instruction_Json_Inst_Id);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return ds;
        }

        public List<dynamic> GetInstructionJsonMasterDetails(Instruction_Json_DTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetInstructionJsonDetails(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Instruction_Json_DTO> instructionjsonDetails =
                   (from item in myEnumerableFeaprd
                    select new Instruction_Json_DTO
                    {
                        Instruction_Json_PkeyId = item.Field<Int64>("Instruction_Json_PkeyId"),
                        Instruction_Json_Inst_Id = item.Field<Int64?>("Instruction_Json_Inst_Id"),
                        Instruction_Json_Client = item.Field<String>("Instruction_Json_Client"),
                        Instruction_Json_WorkType = item.Field<String>("Instruction_Json_WorkType"),
                        Instruction_Json_LoanType = item.Field<String>("Instruction_Json_LoanType"),
                        Instruction_Json_Customer = item.Field<String>("Instruction_Json_Customer"),
                        Instruction_Json_Work_Group = item.Field<String>("Instruction_Json_Work_Group"),
                        Instruction_Json_IsActive = item.Field<Boolean?>("Instruction_Json_IsActive"),

                    }).ToList();

                objDynamic.Add(instructionjsonDetails);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

    }
}