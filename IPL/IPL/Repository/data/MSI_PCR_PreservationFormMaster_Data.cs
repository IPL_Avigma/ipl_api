﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class MSI_PCR_PreservationFormMaster_Data
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        public List<dynamic> PostMsiPreservationPcrForm(MSI_PCR_PreservationFormMaster_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_MSI_Preservation_PCR_FORMS_MASTER]";
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@MSI_Preservation_PkeyId", 1 + "#bigint#" + model.MSI_Preservation_PkeyId);
                input_parameters.Add("@MSI_Preservation_WO_ID", 1 + "#bigint#" + model.MSI_Preservation_WO_ID);
                input_parameters.Add("@MSI_Preservation_SubjectProperty", 1 + "#nvarchar#" + model.MSI_Preservation_SubjectProperty);
                input_parameters.Add("@MSI_Preservation_ConditionReport", 1 + "#nvarchar#" + model.MSI_Preservation_ConditionReport);
                input_parameters.Add("@MSI_Preservation_PhotoManager", 1 + "#nvarchar#" + model.MSI_Preservation_PhotoManager);
                input_parameters.Add("@MSI_Preservation_BidItems", 1 + "#nvarchar#" + model.MSI_Preservation_BidItems);
                input_parameters.Add("@MSI_Preservation_Comments", 1 + "#nvarchar#" + model.MSI_Preservation_Comments);
                input_parameters.Add("@MSI_Preservation_FinalReviews", 1 + "#nvarchar#" + model.MSI_Preservation_FinalReviews);
                input_parameters.Add("@MSI_Preservation_IsActive", 1 + "#bit#" + model.MSI_Preservation_IsActive);
                input_parameters.Add("@MSI_Preservation_IsDelete", 1 + "#bit#" + model.MSI_Preservation_IsDelete);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@fwo_pkyeId", 1 + "#bigint#" + model.fwo_pkyeId);
                input_parameters.Add("@MSI_Preservation_PkeyId_out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);
                objData = obj.SqlCRUD(insertProcedure, input_parameters);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        private DataSet GetMsiPreservationPcrForm(MSI_PCR_PreservationFormMaster_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_MSI_Preservation_PCR_FORMS_MASTER]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@MSI_Preservation_PkeyId", 1 + "#bigint#" + model.MSI_Preservation_PkeyId);
                input_parameters.Add("@MSI_Preservation_WO_ID", 1 + "#bigint#" + model.MSI_Preservation_WO_ID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetMsiPreservationPcrFormDetails(MSI_PCR_PreservationFormMaster_DTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetMsiPreservationPcrForm(model);

                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                }

                //var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                //List<MSI_PCR_PreservationFormMaster_DTO> cyprexxGrassdata =
                //   (from item in myEnumerableFeaprd
                //    select new MSI_PCR_PreservationFormMaster_DTO
                //    {
                //        MSI_Preservation_PkeyId = item.Field<Int64>("MSI_Preservation_PkeyId"),
                //        MSI_Preservation_WO_ID = item.Field<Int64>("MSI_Preservation_WO_ID"),
                //        MSI_Preservation_CompanyId = item.Field<Int64>("MSI_Preservation_CompanyId"),
                //        MSI_Preservation_SubjectProperty = item.Field<String>("MSI_Preservation_SubjectProperty"),
                //        MSI_Preservation_ConditionReport = item.Field<String>("MSI_Preservation_ConditionReport"),
                //        MSI_Preservation_PhotoManager = item.Field<String>("MSI_Preservation_PhotoManager"),
                //        MSI_Preservation_BidItems = item.Field<String>("MSI_Preservation_BidItems"),
                //        MSI_Preservation_Comments = item.Field<String>("MSI_Preservation_Comments"),
                //        MSI_Preservation_FinalReviews = item.Field<String>("MSI_Preservation_FinalReviews"),
                //        MSI_Preservation_IsActive = item.Field<Boolean>("MSI_Preservation_IsActive"),
                //    }).ToList();

                //objDynamic.Add(cyprexxGrassdata);

                //if (ds.Tables.Count > 1)
                //{
                //    var pcrHistory = ds.Tables[1].AsEnumerable();
                //    List<MSI_PCR_PreservationFormMaster_DTO> history =
                //       (from item in pcrHistory
                //        select new MSI_PCR_PreservationFormMaster_DTO
                //        {
                //            MSI_Preservation_PkeyId = item.Field<Int64>("MSI_Preservation_PkeyId"),
                //            MSI_Preservation_WO_ID = item.Field<Int64>("MSI_Preservation_WO_ID"),
                //            MSI_Preservation_CompanyId = item.Field<Int64>("MSI_Preservation_CompanyId"),
                //            MSI_Preservation_SubjectProperty = item.Field<String>("MSI_Preservation_SubjectProperty"),
                //            MSI_Preservation_ConditionReport = item.Field<String>("MSI_Preservation_ConditionReport"),
                //            MSI_Preservation_PhotoManager = item.Field<String>("MSI_Preservation_PhotoManager"),
                //            MSI_Preservation_BidItems = item.Field<String>("MSI_Preservation_BidItems"),
                //            MSI_Preservation_Comments = item.Field<String>("MSI_Preservation_Comments"),
                //            MSI_Preservation_FinalReviews = item.Field<String>("MSI_Preservation_FinalReviews"),
                //            MSI_Preservation_IsActive = item.Field<Boolean>("MSI_Preservation_IsActive"),
                //        }).ToList();

                //    objDynamic.Add(history);
                //}
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}