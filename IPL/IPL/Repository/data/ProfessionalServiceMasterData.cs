﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class ProfessionalServiceMasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        public List<dynamic> AddUpdate_ProfessionalServiceMaster(ProfessionalServiceMasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_ProfessionalServiceMaster]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@PS_PkeyId", 1 + "#bigint#" + model.PS_PkeyId);
                input_parameters.Add("@PS_ContactName", 1 + "#nvarchar#" + model.PS_ContactName);
                input_parameters.Add("@PS_CompanyName", 1 + "#nvarchar#" + model.PS_CompanyName);
                input_parameters.Add("@PS_Address", 1 + "#nvarchar#" + model.PS_Address);
                input_parameters.Add("@PS_Phone", 1 + "#nvarchar#" + model.PS_Phone);
                input_parameters.Add("@PS_Email", 1 + "#nvarchar#" + model.PS_Email);
                input_parameters.Add("@PS_Website", 1 + "#nvarchar#" + model.PS_Website);
                input_parameters.Add("@PS_Notes", 1 + "#nvarchar#" + model.PS_Notes);
                input_parameters.Add("@PS_ContactType", 1 + "#bigint#" + model.PS_ContactType);
                input_parameters.Add("@PS_IsActive", 1 + "#bit#" + model.PS_IsActive);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@PS_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);
                objData = obj.SqlCRUD(insertProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        private DataSet GetProfessionalServiceMaster(ProfessionalServiceMasterDTO model)
        {
            DataSet ds = null;
            try
            {

                string selectProcedure = "[GetProfessionalServiceMasters]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@PS_PkeyId", 1 + "#bigint#" + model.PS_PkeyId);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ds;
        }

        public List<dynamic> GetProfessionalServiceMasterDetails(ProfessionalServiceMasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetProfessionalServiceMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<ProfessionalServiceMasterDTO> Forms_Master_Files =
                   (from item in myEnumerableFeaprd
                    select new ProfessionalServiceMasterDTO
                    {
                        PS_PkeyId = item.Field<Int64>("PS_PkeyId"),
                        PS_CompanyName = item.Field<string>("PS_CompanyName"),
                        PS_ContactName = item.Field<string>("PS_ContactName"),
                        PS_ContactType = item.Field<Int64>("PS_ContactType"),
                        ContactType = item.Field<string>("ContactType"),
                        PS_Address = item.Field<string>("PS_Address"),
                        PS_Notes = item.Field<string>("PS_Notes"),
                        PS_Email = item.Field<string>("PS_Email"),
                        PS_Website = item.Field<string>("PS_Website"),
                        PS_Phone = item.Field<string>("PS_Phone"),
                        PS_IsActive = item.Field<bool>("PS_IsActive"),
                        PS_CreatedBy = item.Field<string>("PS_CreatedBy"),
                        PS_ModifiedBy = item.Field<string>("PS_ModifiedBy")
                    }).ToList();

                objDynamic.Add(Forms_Master_Files);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}