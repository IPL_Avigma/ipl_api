﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class Instruction_DocumentData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();


        public List<dynamic>UploadInstructionDocumentData(Instruction_DocumentDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_Instruction_Document]";
            uploaddata uploaddata = new uploaddata();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Inst_Doc_PkeyID", 1 + "#bigint#" + model.Inst_Doc_PkeyID);
                input_parameters.Add("@Inst_Doc_Inst_Ch_ID", 1 + "#bigint#" + model.Inst_Doc_Inst_Ch_ID);
                input_parameters.Add("@Inst_Doc_Wo_ID", 1 + "#bigint#" + model.Inst_Doc_Wo_ID);
                input_parameters.Add("@Inst_Doc_File_Path", 1 + "#varchar#" + model.Inst_Doc_File_Path);
                input_parameters.Add("@Inst_Doc_File_Size", 1 + "#varchar#" + model.Inst_Doc_File_Size);
                input_parameters.Add("@Inst_Doc_File_Name", 1 + "#varchar#" + model.Inst_Doc_File_Name);
                input_parameters.Add("@Inst_Doc_BucketName", 1 + "#varchar#" + model.Inst_Doc_BucketName);
                input_parameters.Add("@Inst_Doc_ProjectID", 1 + "#varchar#" + model.Inst_Doc_ProjectID);
                input_parameters.Add("@Inst_Doc_Object_Name", 1 + "#varchar#" + model.Inst_Doc_Object_Name);
                input_parameters.Add("@Inst_Doc_Folder_Name", 1 + "#varchar#" + model.Inst_Doc_Folder_Name);
                input_parameters.Add("@Inst_Doc_UploadedBy", 1 + "#varchar#" + model.Inst_Doc_UploadedBy);
                input_parameters.Add("@Inst_Doc_IsActive", 1 + "#bit#" + model.Inst_Doc_IsActive);
                input_parameters.Add("@Inst_Doc_IsDelete", 1 + "#bit#" + model.Inst_Doc_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Folder_File_Master_FKId", 1 + "#bigint#" + model.Folder_File_Master_FKId); // Added by Dipali
                input_parameters.Add("@Inst_Doc_PkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters); 
                if (objData[0] == 0)
                {
                    uploaddata.Inst_Doc_PkeyID = "0";
                    uploaddata.Status = "0";
                    uploaddata.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    uploaddata.Inst_Doc_PkeyID = Convert.ToString(objData[0]);
                    uploaddata.Status = Convert.ToString(objData[1]);


                }
                objcltData.Add(uploaddata);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;



        }
        private DataSet GetInstructionDocument(Instruction_DocumentDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Instruction_Document_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Inst_Doc_PkeyID", 1 + "#bigint#" + model.Inst_Doc_PkeyID);
                input_parameters.Add("@Inst_Doc_Inst_Ch_ID", 1 + "#bigint#" + model.Inst_Doc_Inst_Ch_ID);
                input_parameters.Add("@Inst_Doc_Wo_ID", 1 + "#bigint#" + model.Inst_Doc_Wo_ID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetInstructionDocumentData(Instruction_DocumentDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetInstructionDocument(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Instruction_DocumentDTO>InstructionDocumentDetails =
                   (from item in myEnumerableFeaprd
                    select new Instruction_DocumentDTO
                    {
                        Inst_Doc_PkeyID = item.Field<Int64>("Inst_Doc_PkeyID"),
                        Inst_Doc_Inst_Ch_ID = item.Field<Int64?>("Inst_Doc_Inst_Ch_ID"),
                        Inst_Doc_Wo_ID = item.Field<Int64?>("Inst_Doc_Wo_ID"),
                        Inst_Doc_File_Path = item.Field<String>("Inst_Doc_File_Path"),
                        Inst_Doc_File_Size = item.Field<String>("Inst_Doc_File_Size"),
                        Inst_Doc_File_Name = item.Field<String>("Inst_Doc_File_Name"),
                        Inst_Doc_BucketName = item.Field<String>("Inst_Doc_BucketName"),
                        Inst_Doc_ProjectID = item.Field<String>("Inst_Doc_ProjectID"),
                        Inst_Doc_Object_Name = item.Field<String>("Inst_Doc_Object_Name"),
                        Inst_Doc_Folder_Name = item.Field<String>("Inst_Doc_Folder_Name"),
                        Inst_Doc_UploadedBy = item.Field<String>("Inst_Doc_UploadedBy"),
                        Inst_Doc_IsActive = item.Field<Boolean?>("Inst_Doc_IsActive"),

                    }).ToList();

                objDynamic.Add(InstructionDocumentDetails);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}