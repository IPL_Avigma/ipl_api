﻿using Avigma.Repository.Lib;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using IPLApp.Models;
using IPL.Repository.data;
using IPL.Models;
using Newtonsoft.Json;

namespace IPLApp.Repository.data
{
    public class Access_log_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        public List<dynamic> AddAccess_log_Master(Access_log_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_Access_log_Master]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Alm_Pkey", 1 + "#bigint#" + model.Alm_Pkey);
                input_parameters.Add("@Alm_workOrder_ID", 1 + "#bigint#" + model.Alm_workOrder_ID);
                input_parameters.Add("@Alm_userID", 1 + "#bigint#" + model.Alm_userID);
                input_parameters.Add("@Alm_Remark", 1 + "#nvarchar#" + model.Alm_Remark);
                input_parameters.Add("@Alm_AccessDate", 1 + "#datetime#" + model.Alm_AccessDate);
                input_parameters.Add("@Alm_PrevStatusDate", 1 + "#datetime#" + model.Alm_PrevStatusDate);
                input_parameters.Add("@Alm_Status", 1 + "#int#" + model.Alm_Status);
                input_parameters.Add("@Alm_log", 1 + "#nvarchar#" + model.Alm_log);
                input_parameters.Add("@Alm_IsActive", 1 + "#bit#" + model.Alm_IsActive);
                input_parameters.Add("@Alm_IsDelete", 1 + "#bit#" + model.Alm_IsDelete);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserId);
                input_parameters.Add("@Alm_Pkey_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;



        }

        private DataSet Get_Access_log_Master(Access_log_MasterDTO model)
        {
            DataSet ds = null;
            try
            {

                string selectProcedure = "[Get_Access_log_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Alm_Pkey", 1 + "#bigint#" + model.Alm_Pkey);
                input_parameters.Add("@Alm_workOrder_ID", 1 + "#bigint#" + model.Alm_workOrder_ID);

                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        private DataSet Get_Access_User_Log_New_Master(Access_User_Log_New_Master model)
        {
            DataSet ds = null;
            try
            {

                string selectProcedure = "[Get_Access_User_Log_New_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Acc_PkeyID", 1 + "#bigint#" + model.Acc_PkeyID);
                input_parameters.Add("@WhereClause", 1 + "#nvarchar#" + model.WhereClause);

                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserId);
                input_parameters.Add("@PageNumber", 1 + "#int#" + model.PageNumber);
                input_parameters.Add("@Rowcount", 1 + "#int#" + model.Rowcount);



                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> Get_AccessUserLogNewMaster(Access_User_Log_New_Master model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                string wherecondition = string.Empty;
                if (model.Type == 3 && !string.IsNullOrEmpty(model.FilterData))
                {
                    var Data = JsonConvert.DeserializeObject<NewAccessLogMaster_Filter>(model.FilterData);
                    if (!string.IsNullOrEmpty(Data.Acc_UserAction))
                    {
                        wherecondition = " And ac.Acc_UserAction LIKE '%" + Data.Acc_UserAction + "%'";
                    }
                    if (!string.IsNullOrEmpty(Data.Acc_Access_Log_Data))
                    {
                        wherecondition = " And ac.Acc_Access_Log_Data LIKE '%" + Data.Acc_Access_Log_Data + "%'";
                    }
                    if (!string.IsNullOrEmpty(Data.Acc_Type_of_Log))
                    {
                        wherecondition = " And ac.Acc_Type_of_Log LIKE '%" + Data.Acc_Type_of_Log + "%'";
                    }
                    if (!string.IsNullOrEmpty(Data.Acc_Log_Details))
                    {
                        wherecondition = " And ac.Acc_Log_Details LIKE '%" + Data.Acc_Log_Details + "%'";
                    }
                    model.WhereClause = wherecondition;
                }

                DataSet ds = Get_Access_User_Log_New_Master(model);

                if (ds.Tables.Count > 0)
                {
                    for (int i = 0; i < ds.Tables.Count; i++)
                    {
                        objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                    }

                }
                //objDynamic.Add(Get_details);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }

            return objDynamic;
        }

        public List<dynamic> Get_Access_log_MasterDetails(Access_log_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();

            Invoice_ContractorData invoice_ContractorData = new Invoice_ContractorData();

            try
            {

                DataSet ds = Get_Access_log_Master(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Access_log_MasterDTO> Access_log_Master =
                   (from item in myEnumerableFeaprd
                    select new Access_log_MasterDTO
                    {
                        Alm_Pkey = item.Field<Int64>("Alm_Pkey"),
                        Alm_workOrder_ID = item.Field<Int64?>("Alm_workOrder_ID"),
                        Alm_userID = item.Field<Int64?>("Alm_userID"),
                        Alm_Remark = item.Field<String>("Alm_Remark"),
                        Alm_AccessDate = item.Field<DateTime?>("Alm_AccessDate"),
                        Alm_Status = item.Field<int?>("Alm_Status"),
                        Alm_log = item.Field<String>("Alm_log"),

                        Status_Name = item.Field<String>("Status_Name"),
                        LogUserName = item.Field<String>("LogUserName"),
                        LogDate = item.Field<String>("LogDate"),

                    }).ToList();

                objDynamic.Add(Access_log_Master);
                WorkOrder_Import_FilesDTO workOrder_Import_FilesDTO = new WorkOrder_Import_FilesDTO();
                workOrder_Import_FilesDTO.WIF_workOrder_ID = model.Alm_workOrder_ID;
                workOrder_Import_FilesDTO.Type = 1;
                objDynamic.Add(invoice_ContractorData.Getworkorderimportfile_Details(workOrder_Import_FilesDTO));

                if (ds.Tables.Count > 1)
                {
                    var myEnumerableFeaprdx1 = ds.Tables[1].AsEnumerable();
                    List<Instruction_DocumentDTO> instructiondocument =
                       (from item in myEnumerableFeaprdx1
                        select new Instruction_DocumentDTO
                        {
                            Inst_Doc_PkeyID = item.Field<Int64>("Inst_Doc_PkeyID"),
                            Inst_Doc_Inst_Ch_ID = item.Field<Int64?>("Inst_Doc_Inst_Ch_ID"),
                            Inst_Doc_Wo_ID = item.Field<Int64?>("Inst_Doc_Wo_ID"),
                            Inst_Doc_File_Path = item.Field<String>("Inst_Doc_File_Path"),
                            Inst_Doc_File_Size = item.Field<String>("Inst_Doc_File_Size"),
                            Inst_Doc_File_Name = item.Field<String>("Inst_Doc_File_Name"),
                            Inst_Doc_BucketName = item.Field<String>("Inst_Doc_BucketName"),
                            Inst_Doc_ProjectID = item.Field<String>("Inst_Doc_ProjectID"),
                            Inst_Doc_Object_Name = item.Field<String>("Inst_Doc_Object_Name"),
                            Inst_Doc_Folder_Name = item.Field<String>("Inst_Doc_Folder_Name"),
                            Inst_Doc_UploadedBy = item.Field<String>("Inst_Doc_UploadedBy"),
                            Inst_Doc_IsActive = item.Field<Boolean?>("Inst_Doc_IsActive"),

                        }).ToList();

                    objDynamic.Add(instructiondocument);
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }



        public List<dynamic> AddNewAccessLogMaster(NewAccessLogMaster model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_AccessLog]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Access_PkeyID", 1 + "#bigint#" + model.Access_PkeyID);
                input_parameters.Add("@Access_WorkerOrderID", 1 + "#bigint#" + model.Access_WorkerOrderID);
                input_parameters.Add("@Access_Master_ID", 1 + "#bigint#" + model.Access_Master_ID);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.Access_UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Access_PkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);
                objData = obj.SqlCRUD(insertProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;



        }

        private DataSet Get_New_Access_log_Master(NewAccessLogMaster model)
        {
            DataSet ds = null;
            try
            {

                string selectProcedure = "[GetNewAccessLogMaster]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Access_PkeyID", 1 + "#bigint#" + model.Access_PkeyID);
                input_parameters.Add("@Access_WorkerOrderID", 1 + "#bigint#" + model.Access_WorkerOrderID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> Get_New_Access_log_MasterDetails(NewAccessLogMaster model)
        {
            List<dynamic> objDynamic = new List<dynamic>();

            Invoice_ContractorData invoice_ContractorData = new Invoice_ContractorData();

            try
            {

                DataSet ds = Get_New_Access_log_Master(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<NewAccessLogMaster> Access_log_Master =
                   (from item in myEnumerableFeaprd
                    select new NewAccessLogMaster
                    {
                        Access_PkeyID = item.Field<Int64>("Access_PkeyID"),
                        Access_WorkerOrderID = item.Field<Int64>("Access_WorkerOrderID"),
                        Access_UserID = item.Field<Int64?>("Access_UserID"),
                        Access_CompanyID = item.Field<Int64?>("Access_CompanyID"),

                        Access_Platform = item.Field<String>("Access_Platform"),
                        Access_User_Action = item.Field<String>("Access_User_Action"),
                        Access_Log_data = item.Field<String>("Access_Log_data"),
                        Access_Type_of_Log = item.Field<String>("Access_Type_of_Log"),
                        Access_Log_Details = item.Field<String>("Access_Log_Details"), 
                        Access_CreatedBy = item.Field<String>("Access_CreatedBy"),
                        Access_Date = item.Field<DateTime>("Access_Date"),

                    }).ToList();

                objDynamic.Add(Access_log_Master);


                WorkOrder_Import_FilesDTO workOrder_Import_FilesDTO = new WorkOrder_Import_FilesDTO();
                workOrder_Import_FilesDTO.WIF_workOrder_ID = model.Access_WorkerOrderID;
                workOrder_Import_FilesDTO.Type = 1;
                objDynamic.Add(invoice_ContractorData.Getworkorderimportfile_Details(workOrder_Import_FilesDTO));

                if (ds.Tables.Count > 1)
                {
                    var myEnumerableFeaprdx1 = ds.Tables[1].AsEnumerable();
                    List<Instruction_DocumentDTO> instructiondocument =
                       (from item in myEnumerableFeaprdx1
                        select new Instruction_DocumentDTO
                        {
                            Inst_Doc_PkeyID = item.Field<Int64>("Inst_Doc_PkeyID"),
                            Inst_Doc_Inst_Ch_ID = item.Field<Int64?>("Inst_Doc_Inst_Ch_ID"),
                            Inst_Doc_Wo_ID = item.Field<Int64?>("Inst_Doc_Wo_ID"),
                            Inst_Doc_File_Path = item.Field<String>("Inst_Doc_File_Path"),
                            Inst_Doc_File_Size = item.Field<String>("Inst_Doc_File_Size"),
                            Inst_Doc_File_Name = item.Field<String>("Inst_Doc_File_Name"),
                            Inst_Doc_BucketName = item.Field<String>("Inst_Doc_BucketName"),
                            Inst_Doc_ProjectID = item.Field<String>("Inst_Doc_ProjectID"),
                            Inst_Doc_Object_Name = item.Field<String>("Inst_Doc_Object_Name"),
                            Inst_Doc_Folder_Name = item.Field<String>("Inst_Doc_Folder_Name"),
                            Inst_Doc_UploadedBy = item.Field<String>("Inst_Doc_UploadedBy"),
                            Inst_Doc_IsActive = item.Field<Boolean?>("Inst_Doc_IsActive"),

                        }).ToList();

                    objDynamic.Add(instructiondocument);
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

    }
}