﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class PCR_Pool_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public List<dynamic> AddPCR_Pool_Data(PCR_Pool_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objclntData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_PCR_Pool_Master]";
            PCR_Pool_Master pCR_Pool_Master = new PCR_Pool_Master();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@PCR_Pool_pkeyId", 1 + "#bigint#" + model.PCR_Pool_pkeyId);
                input_parameters.Add("@PCR_Pool_MasterId", 1 + "#bigint#" + model.PCR_Pool_MasterId);
                input_parameters.Add("@PCR_Pool_WO_Id", 1 + "#bigint#" + model.PCR_Pool_WO_Id);
                input_parameters.Add("@PCR_Pool_ValType", 1 + "#int#" + model.PCR_Pool_ValType);
                input_parameters.Add("@PCR_Pool_Info_Pool_Present", 1 + "#varchar#" + model.PCR_Pool_Info_Pool_Present);
                input_parameters.Add("@PCR_Pool_Diameter_Ft", 1 + "#varchar#" + model.PCR_Pool_Diameter_Ft);
                input_parameters.Add("@PCR_Pool_Length_Ft", 1 + "#varchar#" + model.PCR_Pool_Length_Ft);
                input_parameters.Add("@PCR_Pool_Width_Ft", 1 + "#varchar#" + model.PCR_Pool_Width_Ft);
                input_parameters.Add("@PCR_Pool_Condition_Good", 1 + "#varchar#" + model.PCR_Pool_Condition_Good);
                // input_parameters.Add("@PCR_Pool_Condition_Fair", 1 + "#bit#" + model.PCR_Pool_Condition_Fair);
                // input_parameters.Add("@PCR_Pool_Condition_Poor", 1 + "#bit#" + model.PCR_Pool_Condition_Poor);
                // input_parameters.Add("@PCR_Pool_Condition_Damaged", 1 + "#bit#" + model.PCR_Pool_Condition_Damaged);
                input_parameters.Add("@PCR_Pool_Type_InGround", 1 + "#varchar#" + model.PCR_Pool_Type_InGround);
                // input_parameters.Add("@PCR_Pool_Type_Above_Ground", 1 + "#bit#" + model.PCR_Pool_Type_Above_Ground);
                input_parameters.Add("@PCR_Pool_Secure_On_This_Order", 1 + "#varchar#" + model.PCR_Pool_Secure_On_This_Order);
                input_parameters.Add("@PCR_Pool_Is_There_Fence", 1 + "#varchar#" + model.PCR_Pool_Is_There_Fence);
                input_parameters.Add("@PCR_Pool_Is_It_Locked", 1 + "#varchar#" + model.PCR_Pool_Is_It_Locked);
                input_parameters.Add("@PCR_Pool_Water_Level_Full", 1 + "#varchar#" + model.PCR_Pool_Water_Level_Full);
                //  input_parameters.Add("@PCR_Pool_Water_Level_Partially_Filled", 1 + "#bit#" + model.PCR_Pool_Water_Level_Partially_Filled);
                //  input_parameters.Add("@PCR_Pool_Water_Level_Empty", 1 + "#bit#" + model.PCR_Pool_Water_Level_Empty);
                input_parameters.Add("@PCR_Pool_Did_You_Drain_It", 1 + "#varchar#" + model.PCR_Pool_Did_You_Drain_It);
                input_parameters.Add("@PCR_Pool_Dismantled_Removed", 1 + "#varchar#" + model.PCR_Pool_Dismantled_Removed);
                input_parameters.Add("@PCR_Pool_Is_There_Depression_Left", 1 + "#varchar#" + model.PCR_Pool_Is_There_Depression_Left);
                input_parameters.Add("@PCR_Pool_Secured_Per_Guidelines", 1 + "#varchar#" + model.PCR_Pool_Secured_Per_Guidelines);
                input_parameters.Add("@PCR_Pool_Is_The_Pool_Converted_Prevents_Entry", 1 + "#varchar#" + model.PCR_Pool_Is_The_Pool_Converted_Prevents_Entry);
                input_parameters.Add("@PCR_Pool_Hot_Tub_Present", 1 + "#varchar#" + model.PCR_Pool_Hot_Tub_Present);
                input_parameters.Add("@PCR_Pool_Bids_Drain_Shock_Install_Safety_Cover", 1 + "#varchar#" + model.PCR_Pool_Bids_Drain_Shock_Install_Safety_Cover);
                input_parameters.Add("@PCR_Pool_Bid_To_Install_Safety_Cover", 1 + "#varchar#" + model.PCR_Pool_Bid_To_Install_Safety_Cover);
                input_parameters.Add("@PCR_Pool_Bid_To_Drain", 1 + "#varchar#" + model.PCR_Pool_Bid_To_Drain);
                input_parameters.Add("@PCR_Pool_Bid_To_Dismantle", 1 + "#varchar#" + model.PCR_Pool_Bid_To_Dismantle);
                input_parameters.Add("@PCR_Pool_Drain_Remove", 1 + "#varchar#" + model.PCR_Pool_Drain_Remove);
                input_parameters.Add("@PCR_Pool_Bid_To_Fill_Hole", 1 + "#varchar#" + model.PCR_Pool_Bid_To_Fill_Hole);
                input_parameters.Add("@PCR_Pool_Size_Of_Hole", 1 + "#varchar#" + model.PCR_Pool_Size_Of_Hole);
                input_parameters.Add("@PCR_Pool_Cubic_Yds_Of_Dirt", 1 + "#varchar#" + model.PCR_Pool_Cubic_Yds_Of_Dirt);
                input_parameters.Add("@PCR_Pool_Secure_This_Order_No_Secure_By_FiveBrothers", 1 + "#varchar#" + model.PCR_Pool_Secure_This_Order_No_Secure_By_FiveBrothers);
                //  input_parameters.Add("@PCR_Pool_Secure_This_Order_No_Bid_To_Secure", 1 + "#bit#" + model.PCR_Pool_Secure_This_Order_No_Bid_To_Secure);
                //  input_parameters.Add("@PCR_Pool_Secure_This_Order_No_Previously_Secure_By_Order", 1 + "#bit#" + model.PCR_Pool_Secure_This_Order_No_Previously_Secure_By_Order);
                // input_parameters.Add("@PCR_Pool_Secure_This_Order_No_Other", 1 + "#bit#" + model.PCR_Pool_Secure_This_Order_No_Other);
                input_parameters.Add("@PCR_Pool_Hot_Tub_Present_Yes_Covered_Drained", 1 + "#varchar#" + model.PCR_Pool_Hot_Tub_Present_Yes_Covered_Drained);
                input_parameters.Add("@PCR_Pool_Hot_Tub_Did_You_Secure", 1 + "#varchar#" + model.PCR_Pool_Hot_Tub_Did_You_Secure);
                input_parameters.Add("@PCR_Pool_Hot_Tub_Bids_Diameter_Ft", 1 + "#varchar#" + model.PCR_Pool_Hot_Tub_Bids_Diameter_Ft);
                input_parameters.Add("@PCR_Pool_Hot_Tub_Bids_Length_Ft", 1 + "#varchar#" + model.PCR_Pool_Hot_Tub_Bids_Length_Ft);
                input_parameters.Add("@PCR_Pool_Hot_Tub_Bids_Width_Ft", 1 + "#varchar#" + model.PCR_Pool_Hot_Tub_Bids_Width_Ft);
                input_parameters.Add("@PCR_Pool_Hot_Tub_Bids_Bid_To_Drain", 1 + "#varchar#" + model.PCR_Pool_Hot_Tub_Bids_Bid_To_Drain);
                input_parameters.Add("@PCR_Pool_Hot_Tub_Bids_Bit_To_Install_Cover", 1 + "#varchar#" + model.PCR_Pool_Hot_Tub_Bids_Bit_To_Install_Cover);
                input_parameters.Add("@PCR_Pool_Hot_Tub_Bids_Drain_Secure", 1 + "#varchar#" + model.PCR_Pool_Hot_Tub_Bids_Drain_Secure);
                input_parameters.Add("@PCR_Pool_IsActive", 1 + "#bit#" + model.PCR_Pool_IsActive);
                input_parameters.Add("@PCR_Pool_IsDelete", 1 + "#bit#" + model.PCR_Pool_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@fwo_pkyeId", 1 + "#bigint#" + model.fwo_pkyeId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@PCR_Pool_pkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    pCR_Pool_Master.PCR_Pool_pkeyId = "0";
                    pCR_Pool_Master.Status = "0";
                    pCR_Pool_Master.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    pCR_Pool_Master.PCR_Pool_pkeyId = Convert.ToString(objData[0]);
                    pCR_Pool_Master.Status = Convert.ToString(objData[1]);


                }
                objclntData.Add(pCR_Pool_Master);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objclntData;



        }

        private DataSet GetPCRPoolMaster(PCR_Pool_MasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_PCR_Pool_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@PCR_Pool_pkeyId", 1 + "#bigint#" + model.PCR_Pool_pkeyId);
                input_parameters.Add("@PCR_Pool_WO_Id", 1 + "#bigint#" + model.PCR_Pool_WO_Id);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetPCRPoolDetails(PCR_Pool_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetPCRPoolMaster(model);

                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                }

                //var PCRPool = DatatableToModel(ds.Tables[0]);
                //objDynamic.Add(PCRPool);
                //if (ds.Tables.Count > 1)
                //{
                //    var History = DatatableToModel(ds.Tables[1]);
                //    objDynamic.Add(History);
                //}
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
        private List<PCR_Pool_MasterDTO> DatatableToModel(DataTable dataTable)
        {
            var myEnumerableFeaprd = dataTable.AsEnumerable();
            List<PCR_Pool_MasterDTO> PCRPool =
               (from item in myEnumerableFeaprd
                select new PCR_Pool_MasterDTO
                {
                    PCR_Pool_pkeyId = item.Field<Int64>("PCR_Pool_pkeyId"),
                    PCR_Pool_MasterId = item.Field<Int64>("PCR_Pool_MasterId"),
                    PCR_Pool_WO_Id = item.Field<Int64>("PCR_Pool_WO_Id"),
                    PCR_Pool_ValType = item.Field<int?>("PCR_Pool_ValType"),
                    PCR_Pool_Info_Pool_Present = item.Field<String>("PCR_Pool_Info_Pool_Present"),
                    PCR_Pool_Diameter_Ft = item.Field<String>("PCR_Pool_Diameter_Ft"),
                    PCR_Pool_Length_Ft = item.Field<String>("PCR_Pool_Length_Ft"),
                    PCR_Pool_Width_Ft = item.Field<String>("PCR_Pool_Width_Ft"),
                    PCR_Pool_Condition_Good = item.Field<String>("PCR_Pool_Condition_Good"),
                    //   PCR_Pool_Condition_Fair = item.Field<Boolean?>("PCR_Pool_Condition_Fair"),
                    //    PCR_Pool_Condition_Poor = item.Field<Boolean?>("PCR_Pool_Condition_Poor"),
                    //  PCR_Pool_Condition_Damaged = item.Field<Boolean?>("PCR_Pool_Condition_Damaged"),
                    PCR_Pool_Type_InGround = item.Field<String>("PCR_Pool_Type_InGround"),
                    //  PCR_Pool_Type_Above_Ground = item.Field<String>("PCR_Pool_Type_Above_Ground"),
                    PCR_Pool_Secure_On_This_Order = item.Field<String>("PCR_Pool_Secure_On_This_Order"),
                    PCR_Pool_Is_There_Fence = item.Field<String>("PCR_Pool_Is_There_Fence"),
                    PCR_Pool_Is_It_Locked = item.Field<String>("PCR_Pool_Is_It_Locked"),
                    PCR_Pool_Water_Level_Full = item.Field<String>("PCR_Pool_Water_Level_Full"),
                    //  PCR_Pool_Water_Level_Partially_Filled = item.Field<String>("PCR_Pool_Water_Level_Partially_Filled"),
                    //   PCR_Pool_Water_Level_Empty = item.Field<String>("PCR_Pool_Water_Level_Empty"),
                    PCR_Pool_Did_You_Drain_It = item.Field<String>("PCR_Pool_Did_You_Drain_It"),
                    PCR_Pool_Dismantled_Removed = item.Field<String>("PCR_Pool_Dismantled_Removed"),
                    PCR_Pool_Is_There_Depression_Left = item.Field<String>("PCR_Pool_Is_There_Depression_Left"),
                    PCR_Pool_Secured_Per_Guidelines = item.Field<String>("PCR_Pool_Secured_Per_Guidelines"),
                    PCR_Pool_Is_The_Pool_Converted_Prevents_Entry = item.Field<String>("PCR_Pool_Is_The_Pool_Converted_Prevents_Entry"),
                    PCR_Pool_Hot_Tub_Present = item.Field<String>("PCR_Pool_Hot_Tub_Present"),
                    PCR_Pool_Bids_Drain_Shock_Install_Safety_Cover = item.Field<String>("PCR_Pool_Bids_Drain_Shock_Install_Safety_Cover"),
                    PCR_Pool_Bid_To_Install_Safety_Cover = item.Field<String>("PCR_Pool_Bid_To_Install_Safety_Cover"),
                    PCR_Pool_Bid_To_Drain = item.Field<String>("PCR_Pool_Bid_To_Drain"),
                    PCR_Pool_Bid_To_Dismantle = item.Field<String>("PCR_Pool_Bid_To_Dismantle"),
                    PCR_Pool_Drain_Remove = item.Field<String>("PCR_Pool_Drain_Remove"),
                    PCR_Pool_Bid_To_Fill_Hole = item.Field<String>("PCR_Pool_Bid_To_Fill_Hole"),
                    PCR_Pool_Size_Of_Hole = item.Field<String>("PCR_Pool_Size_Of_Hole"),
                    PCR_Pool_Cubic_Yds_Of_Dirt = item.Field<String>("PCR_Pool_Cubic_Yds_Of_Dirt"),
                    PCR_Pool_Secure_This_Order_No_Secure_By_FiveBrothers = item.Field<String>("PCR_Pool_Secure_This_Order_No_Secure_By_FiveBrothers"),
                    //  PCR_Pool_Secure_This_Order_No_Bid_To_Secure = item.Field<String>("PCR_Pool_Secure_This_Order_No_Bid_To_Secure"),
                    //  PCR_Pool_Secure_This_Order_No_Previously_Secure_By_Order = item.Field<String>("PCR_Pool_Secure_This_Order_No_Previously_Secure_By_Order"),
                    //  PCR_Pool_Secure_This_Order_No_Other = item.Field<String>("PCR_Pool_Secure_This_Order_No_Other"),
                    PCR_Pool_Hot_Tub_Present_Yes_Covered_Drained = item.Field<String>("PCR_Pool_Hot_Tub_Present_Yes_Covered_Drained"),
                    PCR_Pool_Hot_Tub_Did_You_Secure = item.Field<String>("PCR_Pool_Hot_Tub_Did_You_Secure"),
                    PCR_Pool_Hot_Tub_Bids_Diameter_Ft = item.Field<String>("PCR_Pool_Hot_Tub_Bids_Diameter_Ft"),
                    PCR_Pool_Hot_Tub_Bids_Length_Ft = item.Field<String>("PCR_Pool_Hot_Tub_Bids_Length_Ft"),
                    PCR_Pool_Hot_Tub_Bids_Width_Ft = item.Field<String>("PCR_Pool_Hot_Tub_Bids_Width_Ft"),
                    PCR_Pool_Hot_Tub_Bids_Bid_To_Drain = item.Field<String>("PCR_Pool_Hot_Tub_Bids_Bid_To_Drain"),
                    PCR_Pool_Hot_Tub_Bids_Bit_To_Install_Cover = item.Field<String>("PCR_Pool_Hot_Tub_Bids_Bit_To_Install_Cover"),
                    PCR_Pool_Hot_Tub_Bids_Drain_Secure = item.Field<String>("PCR_Pool_Hot_Tub_Bids_Drain_Secure"),
                    PCR_Pool_IsActive = item.Field<Boolean?>("PCR_Pool_IsActive"),



                }).ToList();
            return PCRPool;
        }
    }
}