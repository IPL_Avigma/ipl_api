﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class Filter_Admin_WorkType_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        public List<dynamic> AddUpdate_Filter_Admin_WorkType(Filter_Admin_WorkType_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_Filter_Admin_WorkType_Master]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@WT_Filter_PkeyId", 1 + "#bigint#" + model.WT_Filter_PkeyId);
                input_parameters.Add("@WT_Filter_Name", 1 + "#nvarchar#" + model.WT_Filter_Name);
                input_parameters.Add("@WT_Filter_Group", 1 + "#bigint#" + model.WT_Filter_Group);
                input_parameters.Add("@WT_Filter_WTIsActive", 1 + "#bit#" + model.WT_Filter_WTIsActive);
                input_parameters.Add("@WT_Filter_IsActive", 1 + "#bit#" + model.WT_Filter_IsActive);
                input_parameters.Add("@WT_Filter_IsDelete", 1 + "#bit#" + model.WT_Filter_IsDelete);
                input_parameters.Add("@WT_Filter_CompanyId", 1 + "#bigint#" + model.WT_Filter_CompanyId);
                input_parameters.Add("@WT_Filter_UserID", 1 + "#bigint#" + model.WT_Filter_UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserId);
                input_parameters.Add("@WT_Filter_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        private DataSet Get_Filter_Admin_WorkType(Filter_Admin_WorkType_MasterDTO model)
        {
            DataSet ds = null;
            try
            {

                string selectProcedure = "[Get_Filter_Admin_WorkType_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@WT_Filter_PkeyId", 1 + "#bigint#" + model.WT_Filter_PkeyId);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> Get_Filter_Admin_WorkTypeDetails(Filter_Admin_WorkType_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = Get_Filter_Admin_WorkType(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Filter_Admin_WorkType_MasterDTO> Filterworktype =
                   (from item in myEnumerableFeaprd
                    select new Filter_Admin_WorkType_MasterDTO
                    {
                        WT_Filter_PkeyId = item.Field<Int64>("WT_Filter_PkeyId"),
                        WT_Filter_Name = item.Field<String>("WT_Filter_Name"),
                        WT_Filter_Group = item.Field<Int64?>("WT_Filter_Group"),
                        WT_Filter_WTIsActive = item.Field<Boolean?>("WT_Filter_WTIsActive")

                    }).ToList();

                objDynamic.Add(Filterworktype);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}