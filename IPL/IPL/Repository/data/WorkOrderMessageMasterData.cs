﻿using Avigma.Repository.Lib;
using Avigma.Repository.Lib.FireBase;
using IPL.Models;
using IPLApp.Models;
using IPLApp.Repository.data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace IPL.Repository.data
{
    public class WorkOrderMessageMasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        WorkOrderMasterData workOrderMasterData = new WorkOrderMasterData();
        public List<dynamic> AddMessageDetailsData(WorkOrderMessageMasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_WorkOrderMessage_Master]";
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Msg_PkeyId", 1 + "#bigint#" + model.Msg_PkeyId);
                input_parameters.Add("@Msg_Wo_Id", 1 + "#bigint#" + model.Msg_Wo_Id);
                input_parameters.Add("@Msg_From_UserId", 1 + "#bigint#" + model.Msg_From_UserId);
                input_parameters.Add("@Msg_To_UserId", 1 + "#bigint#" + model.Msg_To_UserId);

                input_parameters.Add("@Msg_To_UserId_A ", 1 + "#bigint#" + model.Msg_To_UserId_A);
                input_parameters.Add("@Msg_To_UserId_B  ", 1 + "#bigint#" + model.Msg_To_UserId_B);

                input_parameters.Add("@Msg_Message_text", 1 + "#nvarchar#" + model.Msg_Message_text);
                input_parameters.Add("@Msg_Time", 1 + "#datetime#" + model.Msg_Time);
                input_parameters.Add("@Msg_Status", 1 + "#int#" + model.Msg_Status);
                input_parameters.Add("@Msg_Message_Id", 1 + "#nvarchar#" + model.Msg_Message_Id);
                input_parameters.Add("@Msg_IsActive", 1 + "#nvarchar#" + model.Msg_IsActive);
                input_parameters.Add("@Msg_IsDelete", 1 + "#nvarchar#" + model.Msg_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Msg_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        public async Task<List<dynamic>> AddFirebaseMessageData(FirebaseMessageWoDTO msgList, Int64 UserID)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            string strAddress = string.Empty;
            UserDetailDTO userDetailDTO = new UserDetailDTO();
            userDetailDTO.UserID = UserID;
            userDetailDTO.Type = 1;
            try
            {
                var userDetail = workOrderMasterData.GetMessageUserDetail(userDetailDTO);
                strAddress = userDetail.address1 + " " + userDetail.city + " " + userDetail.state + " " + userDetail.zip;
                if (UserID > 0 && userDetail != null && userDetail.LoggedUserDetail != null && userDetail.LoggedUserDetail.Count > 0)
                {
                    foreach (var msg in msgList.WoMessageList)
                    {
                        FirebaseMessageDTO msgData = new FirebaseMessageDTO();
                        msgData.avatar = msg.avatar;
                        msgData.from = userDetail.LoggedUserDetail[0].User_LoginName.ToLower();
                        msgData.message = msg.message;
                        msgData.name = userDetail.LoggedUserDetail[0].UserName.ToLower();
                        msgData.status = msg.status;
                        msgData.threadid = msg.threadid;
                        msgData.threadtype = msg.threadtype;
                        // msgData.time = DateTime.Now;
                        // msg.time = new { ".sv" = "timestamp" }; // Assign the Firebase ServerValue// Assign the Firebase ServerValue
                        msg.time = new Dictionary<string, string> { { ".sv", "timestamp" } };// Assign the Firebase ServerValue
                        msgData.readByAdmin = userDetail.LoggedUserDetail[0].GroupRoleId == 1 ? true : false;
                        msgData.readByContractor = userDetail.LoggedUserDetail[0].GroupRoleId == 2 ? true : false;
                        msgData.readByCoordinator = userDetail.LoggedUserDetail[0].GroupRoleId == 3 ? true : false;
                        msgData.readByProcessor = userDetail.LoggedUserDetail[0].GroupRoleId == 4 ? true : false;
                        msgData.readByClient = userDetail.LoggedUserDetail[0].GroupRoleId == 5 ? true : false;
                        msgData.messagesType = msg.messagesType;


                        var msgResult = await workOrderMasterData.AddFirbaseMessage(msgData, msg.IPLNo, userDetail.LoggedUserDetail[0].IPL_Company_ID, strAddress);
                        objdynamicobj.Add(msgResult);
                        userDetail.LoggedUserDetail[0].User_LoginName = userDetail.LoggedUserDetail[0].User_LoginName.Replace(@".", "");
                        FirebaseUserGroupDTO userGroup = new FirebaseUserGroupDTO();
                        userGroup.id = userDetail.LoggedUserDetail[0].User_LoginName;
                        userGroup.avatar = "http://gravatar.com/avatar/" + workOrderMasterData.GenerateMD5(userGroup.id) + "?d=identicon";
                        string FullDetails = msg.IPLNo + " " + strAddress;

                        var firebaseUserIPLDTO = new FirebaseUserIPLDTO { IPLNO = msg.IPLNo, lastUpdate = "" ,Details = FullDetails };

                        await workOrderMasterData.AddFirebaseUserGroupIPLNO(userGroup, msg.IPLNo, userDetail.LoggedUserDetail[0].IPL_Company_ID, userDetail.LoggedUserDetail[0].User_LoginName, strAddress, firebaseUserIPLDTO);

                    }
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objdynamicobj;
        }

        public List<dynamic> SendMessageNotoficationData(workOrderxDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                string Address = string.Empty;
                model.Type = 6;

                DataSet wods = workOrderMasterData.PvtGetWorkOrderData(model);

                if (wods.Tables.Count > 0)
                {
                    var myEnumerable = wods.Tables[0].AsEnumerable();
                    List<UserDetailDTO> WorkOrderData =
                        (from item in myEnumerable
                         select new UserDetailDTO
                         {
                             WoID = item.Field<Int64>("workOrder_ID"),
                             IPLNO = item.Field<String>("IPLNO"),
                             UserToken = item.Field<String>("User_Token_val"),
                             address1 = item.Field<String>("address1"),
                             city = item.Field<String>("city"),
                             state = item.Field<String>("state"),
                             zip = item.Field<Int64>("zip"),
                         }).ToList();
                    Address = WorkOrderData[0].address1 + " " + WorkOrderData[0].city + " " + WorkOrderData[0].state + " " + +WorkOrderData[0].zip;
                    if (WorkOrderData != null && WorkOrderData.Count > 0 && !string.IsNullOrWhiteSpace(WorkOrderData[0].UserToken))
                    {
                        NotificationGetData notificationGetData = new NotificationGetData();
                        var notificationResult = notificationGetData.SendNotification(WorkOrderData[0].UserToken, "New message received for IPL number #" + WorkOrderData[0].IPLNO +" "+ Address + ".", "New message Received", WorkOrderData[0].WoID.ToString(),9, WorkOrderData[0].IPLNO);
                        objDynamic.Add(notificationResult);
                    }
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage("UpdateWorkOrderStatus-------" + model);
            }

            return objDynamic;
        }
    }
}