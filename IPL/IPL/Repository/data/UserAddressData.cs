﻿using Avigma.Repository.Lib;
using IPLApp.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPLApp.Repository.data
{
    public class UserAddressData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public List<dynamic> AddUserAddressData(UserAddressDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objAddData = new List<dynamic>();
            string insertProcedure = "[CreateUpdateUserAddress_Master]";
            UserAddress userAddress = new UserAddress();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@IPL_PkeyID", 1 + "#bigint#" + model.IPL_PkeyID);
                input_parameters.Add("@IPL_Primary_radius", 1 + "#nvarchar#" + model.IPL_Primary_radius);
                input_parameters.Add("@IPL_Primary_Zip_Code", 1 + "#bigint#" + model.IPL_Primary_Zip_Code);
                input_parameters.Add("@IPL_Primary_Countries", 1 + "#bigint#" + model.IPL_Primary_Countries);
                input_parameters.Add("@IPL_Primary_Latitude", 1 + "#varchar#" + model.IPL_Primary_Latitude);
                input_parameters.Add("@IPL_Primary_Longitude", 1 + "#varchar#" + model.IPL_Primary_Longitude);
                input_parameters.Add("@IPL_Secondary_radius", 1 + "#varchar#" + model.IPL_Secondary_radius);
                input_parameters.Add("@IPL_Secondary_Zip_Codes", 1 + "#bigint#" + model.IPL_Secondary_Zip_Codes);
                input_parameters.Add("@IPL_Secondary_Countries", 1 + "#bigint#" + model.IPL_Secondary_Countries);
                input_parameters.Add("@IPL_Secondary_Latitude", 1 + "#varchar#" + model.IPL_Secondary_Latitude);
                input_parameters.Add("@IPL_Secondary_Longitude", 1 + "#varchar#" + model.IPL_Secondary_Longitude);
                input_parameters.Add("@IPL_UserID", 1 + "#bigint#" + model.IPL_UserID);
                input_parameters.Add("@IPL_Address", 1 + "#varchar#" + model.IPL_Address);
                input_parameters.Add("@IPL_City", 1 + "#varchar#" + model.IPL_City);
                input_parameters.Add("@IPL_State", 1 + "#bigint#" + model.IPL_State);
                input_parameters.Add("@IPL_County", 1 + "#bigint#" + model.IPL_County);
                input_parameters.Add("@IPL_Address_Val", 1 + "#varchar#" + model.IPL_Address_Val);
                input_parameters.Add("@IPL_IsActive", 1 + "#bit#" + model.IPL_IsActive);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@IPL_PkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    userAddress.IPL_PkeyID = "0";
                    userAddress.Status = "0";
                    userAddress.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    userAddress.IPL_PkeyID = Convert.ToString(objData[0]);
                    userAddress.Status = Convert.ToString(objData[1]);


                }
                objAddData.Add(userAddress);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objAddData;



        }

        private DataSet GetUserAddressMaster(UserAddressDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_UserAddress_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@IPL_PkeyID", 1 + "#bigint#" + model.IPL_PkeyID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#int#" + model.UserID);
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetUserAddressDetails(UserAddressDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetUserAddressMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<UserAddressDTO> UserAddressDetails =
                   (from item in myEnumerableFeaprd
                    select new UserAddressDTO
                    {
                        IPL_PkeyID = item.Field<Int64>("IPL_PkeyID"),
                        IPL_Primary_radius = item.Field<String>("IPL_Primary_radius"),
                        IPL_Primary_Zip_Code = item.Field<Int64?>("IPL_Primary_Zip_Code"),
                        IPL_Primary_Countries = item.Field<Int64?>("IPL_Primary_Countries"),
                        IPL_Primary_Latitude = item.Field<String>("IPL_Primary_Latitude"),
                        IPL_Primary_Longitude = item.Field<String>("IPL_Primary_Longitude"),
                        IPL_Secondary_radius = item.Field<String>("IPL_Secondary_radius"),
                        IPL_Secondary_Zip_Codes = item.Field<Int64?>("IPL_Secondary_Zip_Codes"),
                        IPL_Secondary_Countries = item.Field<Int64?>("IPL_Secondary_Countries"),
                        IPL_Secondary_Latitude = item.Field<String>("IPL_Secondary_Latitude"),
                        IPL_Secondary_Longitude = item.Field<String>("IPL_Secondary_Longitude"),
                        IPL_UserID = item.Field<Int64?>("IPL_UserID"),
                        IPL_Address = item.Field<String>("IPL_Address"),
                        IPL_City = item.Field<String>("IPL_City"),
                        IPL_State = item.Field<Int64?>("IPL_State"),
                        IPL_County = item.Field<Int64?>("IPL_County"),
                        IPL_IsActive = item.Field<Boolean?>("IPL_IsActive")


                    }).ToList();

                objDynamic.Add(UserAddressDetails);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

       
    }
}