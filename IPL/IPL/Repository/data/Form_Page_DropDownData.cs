﻿using Avigma.Repository.Lib;
using IPL.Models;
using IPLApp.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class Form_Page_DropDownData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        private DataSet GetFormPageDrd(CommonDropdownDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_FormPage_DropDown]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@FormId", 1 + "#bigint#" + model.FormId);
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }


        public List<dynamic> GetDropdownFormPage(CommonDropdownDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                //CommonDropdownDTO commonDropdownDTO = new CommonDropdownDTO();
                model.Type = 1;
                DataSet ds = GetFormPageDrd(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<WorkClientMaster> CompanyDetails =
                   (from item in myEnumerableFeaprd
                    select new WorkClientMaster
                    {
                        Client_pkeyID = item.Field<Int64>("Client_pkeyID"),
                        Client_Company_Name = item.Field<String>("Client_Company_Name"),

                    }).ToList();

                objDynamic.Add(CompanyDetails);
                var myEnumerableFeaprdx = ds.Tables[1].AsEnumerable();
                List<WorkTypeMasterDataValue> worktype =
                   (from item in myEnumerableFeaprdx
                    select new WorkTypeMasterDataValue
                    {
                        WT_pkeyID = item.Field<Int64>("WT_pkeyID"),
                        WT_WorkType = item.Field<String>("WT_WorkType"),

                    }).ToList();

                objDynamic.Add(worktype);

                var myEnumerableLoantype = ds.Tables[2].AsEnumerable();
                List<LoanTypeMasterDTO> LoanType =
                   (from item in myEnumerableLoantype
                    select new LoanTypeMasterDTO
                    {
                        Loan_pkeyId = item.Field<Int64>("Loan_pkeyId"),
                        Loan_Type = item.Field<String>("Loan_Type"),

                    }).ToList();

                objDynamic.Add(LoanType);


                var myEnumerableCustnumber = ds.Tables[3].AsEnumerable();
                List<CustomerNumberDTO> CustomerNumber =
                   (from item in myEnumerableCustnumber
                    select new CustomerNumberDTO
                    {
                        Cust_Num_pkeyId = item.Field<Int64>("Cust_Num_pkeyId"),
                        Cust_Num_Number = item.Field<String>("Cust_Num_Number"),

                    }).ToList();

                objDynamic.Add(CustomerNumber);

                var myEnumerableWorkType = ds.Tables[4].AsEnumerable();
                List<WorkTypeCategoryDTO> WorkType =
                   (from item in myEnumerableWorkType
                    select new WorkTypeCategoryDTO
                    {
                        Work_Type_Cat_pkeyID = item.Field<Int64>("Work_Type_Cat_pkeyID"),
                        Work_Type_Name = item.Field<String>("Work_Type_Name"),

                    }).ToList();

                objDynamic.Add(WorkType);

                //get que type
                var myEnumerableQueType = ds.Tables[5].AsEnumerable();
                List<Forms_QuestionTypeDTO> QueType =
                   (from item in myEnumerableQueType
                    select new Forms_QuestionTypeDTO
                    {
                        QuestionTypeId = item.Field<Int64>("QuestionTypeId"),
                        QuestionType = item.Field<String>("QuestionType"),
                        QueType_IsMultiple = item.Field<bool>("QueType_IsMultiple"),

                    }).ToList();

                objDynamic.Add(QueType);

                if (ds.Tables.Count > 5)
                {
                    //get pdf field
                    var myEnumerablepdfField = ds.Tables[6].AsEnumerable();
                    List<Question_PDFField_MasterDTO> pdffieldList =
                       (from item in myEnumerablepdfField
                        select new Question_PDFField_MasterDTO
                        {
                            PF_pkeyId = item.Field<Int64>("PF_pkeyId"),
                            PF_Name = item.Field<String>("PF_Name"),
                            PF_FileId = item.Field<Int64?>("PF_FileId"),

                        }).ToList();

                    objDynamic.Add(pdffieldList);
                }

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}