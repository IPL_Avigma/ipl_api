﻿using Avigma.Repository.Lib;
using IPLApp.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPLApp.Repository.data
{
    public class GroupMenuRelationData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public List<dynamic> AddGroupMenuRelationData(GroupMenuRelationDTO model)
        {
          
            string insertProcedure = "[CreateUpdateGroupMenuMaster]";
           
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@GroupMenuRelationID", 1 + "#bigint#" + model.GroupMenuRelationID);
                input_parameters.Add("@GroupID", 1 + "#bigint#" + model.GroupID);
                input_parameters.Add("@MenuID", 1 + "#bigint#" + model.MenuID);
                input_parameters.Add("@IsActive", 1 + "#bit#" + model.IsActive);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@GroupMenuRelationID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

               
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return obj.SqlCRUD(insertProcedure, input_parameters);



        }

        private DataSet GetGroupMenuRelationMaster(GroupMenuRelationDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[GetGroupMenuRelationMaster]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@GroupMenuRelationID", 1 + "#bigint#" + model.GroupMenuRelationID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetGroupMenuRelationDetails(GroupMenuRelationDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetGroupMenuRelationMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<GroupMenuRelationDTO> GroupMenuRelationDetails =
                   (from item in myEnumerableFeaprd
                    select new GroupMenuRelationDTO
                    {
                        GroupMenuRelationID = item.Field<Int64>("GroupMenuRelationID"),
                        GroupID = item.Field<Int64?>("GroupID"),
                        Grp_Name = item.Field<String>("Grp_Name"),
                        MenuID = item.Field<Int64?>("MenuID"),
                        Ipre_MenuName = item.Field<String>("Ipre_MenuName"),
                        IsActive = item.Field<Boolean?>("IsActive"),

                    }).ToList();

                objDynamic.Add(GroupMenuRelationDetails);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }



        // for user access menu 
        private List<dynamic> GetUserAcessData(UserMenuAcessDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                string selectProcedure = "[GetUserMenuRelation]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Mgr_Menu_Id", 1 + "#bigint#" + model.MenuID);
                input_parameters.Add("@User_pkeyID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objDynamic = obj.SqlCRUD(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return objDynamic;
        }



        public List<dynamic> GetUserAcessDetails(UserMenuAcessDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {

                objDynamic = GetUserAcessData(model);

                //var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                //List<UserMenuAcessDTO> UserAcessMenu =
                //   (from item in myEnumerableFeaprd
                //    select new UserMenuAcessDTO
                //    {
                //        count = item.Field<int>("count"),
                    


                //    }).ToList();

                //objDynamic.Add(UserAcessMenu);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }


    }
}