﻿using Avigma.Repository.Lib;
using IPL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace IPL.Repository.data
{
    public class TaskFilterMaster
    {

        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        TaskSettingChildData taskSettingChildData = new TaskSettingChildData();

        Task_Company_TBLData task_Company_TBLData = new Task_Company_TBLData();

        Task_Company_TBLDTO task_Company_TBLDTO = new Task_Company_TBLDTO();
        Task_State_TBLDTO task_State_TBLDTO = new Task_State_TBLDTO();
        Task_Country_TBLDTO task_Country_TBLDTO = new Task_Country_TBLDTO();
        Task_Contractor_TBLDTO task_Contractor_TBLDTO = new Task_Contractor_TBLDTO();
        Task_Cutomer_TBLDTO task_Cutomer_TBLDTO = new Task_Cutomer_TBLDTO();
        Task_Loan_TBLDTO task_Loan_TBLDTO = new Task_Loan_TBLDTO();
        Task_Group_TBLDTO task_Group_TBLDTO = new Task_Group_TBLDTO();
        Task_WorkType_TBLDTO task_WorkType_TBLDTO = new Task_WorkType_TBLDTO();
        Task_LotPrice_TBLDTO task_LotPrice_TBLDTO = new Task_LotPrice_TBLDTO();

        WT_Company_TBLData wT_Company_TBLData = new WT_Company_TBLData();
        public List<dynamic> AddTaskSettingCustomPriceFilter(TaskFilterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objAddData = new List<dynamic>();            
            
            TaskSettingChildDTO taskSettingChildDTO = new TaskSettingChildDTO();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();

            try
            {
                if (model.ArrayCustomPriceFilter != null)
                {
                    if (model.ArrayCustomPriceFilter.Count > 0)
                    {
                        for (int i = 0; i < model.ArrayCustomPriceFilter.Count; i++)
                        {
                            taskSettingChildDTO.Task_sett_pkeyID = model.ArrayCustomPriceFilter[i].Task_sett_pkeyID;
                            taskSettingChildDTO.Task_sett_ID = model.Task_pkeyID;
                            taskSettingChildDTO.Task_sett_Company = JsonConvert.SerializeObject(model.ArrayCustomPriceFilter[i].Task_sett_Company);
                            taskSettingChildDTO.Task_sett_State = JsonConvert.SerializeObject(model.ArrayCustomPriceFilter[i].Task_sett_State);
                            taskSettingChildDTO.Task_sett_Country = JsonConvert.SerializeObject(model.ArrayCustomPriceFilter[i].Task_sett_Country);
                            taskSettingChildDTO.Task_sett_Zip = model.ArrayCustomPriceFilter[i].Task_sett_Zip;
                            taskSettingChildDTO.Task_sett_Contractor = JsonConvert.SerializeObject(model.ArrayCustomPriceFilter[i].Task_sett_Contractor);
                            taskSettingChildDTO.Task_sett_Customer = JsonConvert.SerializeObject(model.ArrayCustomPriceFilter[i].Task_sett_Customer);
                            taskSettingChildDTO.Task_sett_Lone = JsonConvert.SerializeObject(model.ArrayCustomPriceFilter[i].Task_sett_Lone);
                            taskSettingChildDTO.Task_Work_TypeGroup = JsonConvert.SerializeObject(model.ArrayCustomPriceFilter[i].Task_Work_TypeGroup);
                            taskSettingChildDTO.Task_sett_WorkType = JsonConvert.SerializeObject(model.ArrayCustomPriceFilter[i].Task_sett_WorkType);
                            taskSettingChildDTO.Task_sett_LotPrice = JsonConvert.SerializeObject(model.ArrayCustomPriceFilter[i].Task_sett_LotPrice);
                            taskSettingChildDTO.Task_sett_Con_Unit_Price = model.ArrayCustomPriceFilter[i].Task_sett_Con_Unit_Price;
                            taskSettingChildDTO.Task_sett_CLI_Unit_Price = model.ArrayCustomPriceFilter[i].Task_sett_CLI_Unit_Price;
                            taskSettingChildDTO.Task_sett_Flat_Free = model.ArrayCustomPriceFilter[i].Task_sett_Flat_Free;
                            taskSettingChildDTO.Task_sett_Price_Edit = model.ArrayCustomPriceFilter[i].Task_sett_Price_Edit;
                            taskSettingChildDTO.Task_sett_Disable_Default = model.ArrayCustomPriceFilter[i].Task_sett_Disable_Default;
                          
                            taskSettingChildDTO.Task_sett_IsActive = model.ArrayCustomPriceFilter[i].Task_sett_IsActive;
                            taskSettingChildDTO.Task_sett_IsDelete = model.ArrayCustomPriceFilter[i].Task_sett_IsDelete;
                            taskSettingChildDTO.UserID = model.UserID;
                            taskSettingChildDTO.Type = model.ArrayCustomPriceFilter[i].Task_sett_pkeyID != 0 ? 2 : model.Type;
                          
                            taskSettingChildDTO.Task_sett_LOT_Min = model.ArrayCustomPriceFilter[i].Task_sett_LOT_Min;
                            taskSettingChildDTO.Task_sett_LOT_Max = model.ArrayCustomPriceFilter[i].Task_sett_LOT_Max;
                            taskSettingChildDTO.Task_sett_MapTaskFkeyId = 0;

                            objData = taskSettingChildData.AddTaskSettingChildData(taskSettingChildDTO);

                            if (objData[0].Status != "0")
                            {
                                Int64 Task_sett_pkeyID = 0;
                                if (!string.IsNullOrEmpty(objData[0].Task_sett_pkeyID))
                                {
                                    Task_sett_pkeyID = Convert.ToInt64(objData[0].Task_sett_pkeyID);
                                }
                                if (model.ArrayCustomPriceFilter[i].Task_sett_Company != null && model.ArrayCustomPriceFilter[i].Task_sett_Company.Count > 0)
                                {
                                    for (int p = 0; p < model.ArrayCustomPriceFilter[i].Task_sett_Company.Count; p++)
                                    {
                                        task_Company_TBLDTO.TC_pkeyID = 0;
                                        task_Company_TBLDTO.TC_Task_pkeyID = model.Task_pkeyID;
                                        task_Company_TBLDTO.TC_Task_sett_ID = model.ArrayCustomPriceFilter[i].Task_sett_Company[p].Client_pkeyID;
                                        task_Company_TBLDTO.TC_Task_sett_pkeyID = Task_sett_pkeyID;
                                        task_Company_TBLDTO.TC_IsActive = true;
                                        task_Company_TBLDTO.TC_IsDelete = false;
                                        task_Company_TBLDTO.UserID = model.UserID;
                                        task_Company_TBLDTO.Type = 1;

                                        var returnkeyCompany = task_Company_TBLData.AddTaskCompanyData(task_Company_TBLDTO);
                                    }
                                }
                                if (model.ArrayCustomPriceFilter[i].Task_sett_State != null && model.ArrayCustomPriceFilter[i].Task_sett_State.Count > 0)
                                {
                                    for (int q = 0; q < model.ArrayCustomPriceFilter[i].Task_sett_State.Count; q++)
                                    {
                                        task_State_TBLDTO.TS_pkeyID = 0;
                                        task_State_TBLDTO.TS_Task_pkeyID = model.Task_pkeyID;
                                        task_State_TBLDTO.TS_Task_sett_ID = model.ArrayCustomPriceFilter[i].Task_sett_State[q].IPL_StateID;
                                        task_State_TBLDTO.TS_Task_sett_pkeyID = Task_sett_pkeyID;
                                        task_State_TBLDTO.TS_IsActive = true;
                                        task_State_TBLDTO.TS_IsDelete = false;
                                        task_State_TBLDTO.UserID = model.UserID;
                                        task_State_TBLDTO.Type = 1;

                                        var returnkeystate = task_Company_TBLData.AddTaskStateData(task_State_TBLDTO);
                                    }
                                }
                                if (model.ArrayCustomPriceFilter[i].Task_sett_Country != null && model.ArrayCustomPriceFilter[i].Task_sett_Country.Count > 0)
                                {
                                    for (int r = 0; r < model.ArrayCustomPriceFilter[i].Task_sett_Country.Count; r++)
                                    {
                                        task_Country_TBLDTO.TCoun_pkeyID = 0;
                                        task_Country_TBLDTO.TCoun_Task_pkeyID = model.Task_pkeyID;
                                        task_Country_TBLDTO.TCoun_Task_sett_ID = model.ArrayCustomPriceFilter[i].Task_sett_Country[r].ID;
                                        task_Country_TBLDTO.TCoun_Task_sett_pkeyID = Task_sett_pkeyID;
                                        task_Country_TBLDTO.TCoun_IsActive = true;
                                        task_Country_TBLDTO.TCoun_IsDelete = false;
                                        task_Country_TBLDTO.UserID = model.UserID;
                                        task_Country_TBLDTO.Type = 1;

                                        var returnkeyCountry = task_Company_TBLData.AddTaskCountryData(task_Country_TBLDTO);
                                    }
                                }
                                if (model.ArrayCustomPriceFilter[i].Task_sett_Contractor != null && model.ArrayCustomPriceFilter[i].Task_sett_Contractor.Count > 0)
                                {
                                    for (int s = 0; s < model.ArrayCustomPriceFilter[i].Task_sett_Contractor.Count; s++)
                                    {
                                        task_Contractor_TBLDTO.TCon_pkeyID = 0;
                                        task_Contractor_TBLDTO.TCon_Task_pkeyID = model.Task_pkeyID;
                                        task_Contractor_TBLDTO.TCon_Task_sett_ID = model.ArrayCustomPriceFilter[i].Task_sett_Contractor[s].User_pkeyID;
                                        task_Contractor_TBLDTO.TCon_Task_sett_pkeyID = Task_sett_pkeyID;
                                        task_Contractor_TBLDTO.TCon_IsActive = true;
                                        task_Contractor_TBLDTO.TCon_IsDelete = false;
                                        task_Contractor_TBLDTO.UserID = model.UserID;
                                        task_Contractor_TBLDTO.Type = 1;

                                        var returnkeyContractor = task_Company_TBLData.AddTaskContractorData(task_Contractor_TBLDTO);
                                    }
                                }
                                if (model.ArrayCustomPriceFilter[i].Task_sett_Customer != null && model.ArrayCustomPriceFilter[i].Task_sett_Customer.Count > 0)
                                {
                                    for (int t = 0; t < model.ArrayCustomPriceFilter[i].Task_sett_Customer.Count; t++)
                                    {
                                        task_Cutomer_TBLDTO.TCust_pkeyID = 0;
                                        task_Cutomer_TBLDTO.TCust_Task_pkeyID = model.Task_pkeyID;
                                        task_Cutomer_TBLDTO.TCust_Task_sett_ID = model.ArrayCustomPriceFilter[i].Task_sett_Customer[t].Cust_Num_pkeyId;
                                        task_Cutomer_TBLDTO.TCust_Task_sett_pkeyID = Task_sett_pkeyID;
                                        task_Cutomer_TBLDTO.TCust_IsActive = true;
                                        task_Cutomer_TBLDTO.TCust_IsDelete = false;
                                        task_Cutomer_TBLDTO.UserID = model.UserID;
                                        task_Cutomer_TBLDTO.Type = 1;

                                        var returnkeyCustomer = task_Company_TBLData.AddTaskCutomerData(task_Cutomer_TBLDTO);
                                    }
                                }
                                if (model.ArrayCustomPriceFilter[i].Task_sett_Lone != null && model.ArrayCustomPriceFilter[i].Task_sett_Lone.Count > 0)
                                {
                                    for (int u = 0; u < model.ArrayCustomPriceFilter[i].Task_sett_Lone.Count; u++)
                                    {
                                        task_Loan_TBLDTO.TLoan_pkeyID = 0;
                                        task_Loan_TBLDTO.TLoan_Task_pkeyID = model.Task_pkeyID;
                                        task_Loan_TBLDTO.TLoan_Task_sett_ID = model.ArrayCustomPriceFilter[i].Task_sett_Lone[u].Loan_pkeyId;
                                        task_Loan_TBLDTO.TLoan_Task_sett_pkeyID = Task_sett_pkeyID;
                                        task_Loan_TBLDTO.TLoan_IsActive = true;
                                        task_Loan_TBLDTO.TLoan_IsDelete = false;
                                        task_Loan_TBLDTO.UserID = model.UserID;
                                        task_Loan_TBLDTO.Type = 1;

                                        var returnkeyCustomer = task_Company_TBLData.AddTaskLoanData(task_Loan_TBLDTO);
                                    }
                                }
                                if (model.ArrayCustomPriceFilter[i].Task_Work_TypeGroup != null && model.ArrayCustomPriceFilter[i].Task_Work_TypeGroup.Count > 0)
                                {
                                    for (int x = 0; x < model.ArrayCustomPriceFilter[i].Task_Work_TypeGroup.Count; x++)
                                    {
                                        task_Group_TBLDTO.TG_pkeyID = 0;
                                        task_Group_TBLDTO.TG_Task_pkeyID = model.Task_pkeyID;
                                        task_Group_TBLDTO.TG_Task_sett_ID = model.ArrayCustomPriceFilter[i].Task_Work_TypeGroup[x].Work_Type_Cat_pkeyID;
                                        task_Group_TBLDTO.TG_Task_sett_pkeyID = Task_sett_pkeyID;
                                        task_Group_TBLDTO.TG_IsActive = true;
                                        task_Group_TBLDTO.TG_IsDelete = false;
                                        task_Group_TBLDTO.UserID = model.UserID;
                                        task_Group_TBLDTO.Type = 1;

                                        var returnkeyTaskGroup = task_Company_TBLData.AddTaskGroupData(task_Group_TBLDTO);
                                    }
                                }
                                if (model.ArrayCustomPriceFilter[i].Task_sett_WorkType != null && model.ArrayCustomPriceFilter[i].Task_sett_WorkType.Count > 0)
                                {
                                    for (int o = 0; o < model.ArrayCustomPriceFilter[i].Task_sett_WorkType.Count; o++)
                                    {
                                        task_WorkType_TBLDTO.TW_pkeyID = 0;
                                        task_WorkType_TBLDTO.TW_Task_pkeyID = model.Task_pkeyID;
                                        task_WorkType_TBLDTO.TW_Task_sett_ID = model.ArrayCustomPriceFilter[i].Task_sett_WorkType[o].WT_pkeyID;
                                        task_WorkType_TBLDTO.TW_Task_sett_pkeyID = Task_sett_pkeyID;
                                        task_WorkType_TBLDTO.TW_IsActive = true;
                                        task_WorkType_TBLDTO.TW_IsDelete = false;
                                        task_WorkType_TBLDTO.UserID = model.UserID;
                                        task_WorkType_TBLDTO.Type = 1;

                                        var returnkeyCompany = task_Company_TBLData.AddTaskWorkTypeData(task_WorkType_TBLDTO);
                                    }
                                }
                                if (model.ArrayCustomPriceFilter[i].Task_sett_LotPrice != null && model.ArrayCustomPriceFilter[i].Task_sett_LotPrice.Count > 0)
                                {
                                    for (int o = 0; o < model.ArrayCustomPriceFilter[i].Task_sett_LotPrice.Count; o++)
                                    {
                                        task_LotPrice_TBLDTO.TLP_pkeyID = 0;
                                        task_LotPrice_TBLDTO.TLP_Task_pkeyID = model.Task_pkeyID;
                                        task_LotPrice_TBLDTO.TLP_Task_sett_ID = model.ArrayCustomPriceFilter[i].Task_sett_LotPrice[o].Lot_Pricing_PkeyID;
                                        task_LotPrice_TBLDTO.TLP_Task_sett_pkeyID = Task_sett_pkeyID;
                                        task_LotPrice_TBLDTO.TLP_IsActive = true;
                                        task_LotPrice_TBLDTO.TLP_IsDelete = false;
                                        task_LotPrice_TBLDTO.UserID = model.UserID;
                                        task_LotPrice_TBLDTO.Type = 1;

                                        var returnkeyCompany = task_Company_TBLData.AddTaskLotPriceData(task_LotPrice_TBLDTO);
                                    }
                                }
                            }
                        }
                    }
                }
                if (model.ArrayDocument != null)
                {
                    WorkTypeTaskChildDTO workTypeTaskChildDTO = new WorkTypeTaskChildDTO();
                    WorkTypeTaskChildData workTypeTaskChildData = new WorkTypeTaskChildData();

                    for (int i = 0; i < model.ArrayDocument.Count; i++)
                    {
                        workTypeTaskChildDTO.WT_Task_pkeyID = model.ArrayDocument[i].WT_Task_pkeyID;
                        workTypeTaskChildDTO.WT_Task_ID = model.Task_pkeyID;
                        if (model.ArrayDocument[i].WT_Task_Company != null)
                        {
                            workTypeTaskChildDTO.WT_Task_Company = JsonConvert.SerializeObject(model.ArrayDocument[i].WT_Task_Company);
                        }
                        if (model.ArrayDocument[i].WT_Task_State != null)
                        {
                            workTypeTaskChildDTO.WT_Task_State = JsonConvert.SerializeObject(model.ArrayDocument[i].WT_Task_State);
                        }

                        if (model.ArrayDocument[i].WT_Task_Customer != null)
                        {
                            workTypeTaskChildDTO.WT_Task_Customer = JsonConvert.SerializeObject(model.ArrayDocument[i].WT_Task_Customer);
                        }

                        if (model.ArrayDocument[i].WT_Task_LoneType !=null)
                        {
                            workTypeTaskChildDTO.WT_Task_LoneType = JsonConvert.SerializeObject(model.ArrayDocument[i].WT_Task_LoneType);
                        }

                        if (model.ArrayDocument[i].WT_Task_WorkType != null)
                        {
                            workTypeTaskChildDTO.WT_Task_WorkType = JsonConvert.SerializeObject(model.ArrayDocument[i].WT_Task_WorkType);
                        }
                        if (model.ArrayDocument[i].WT_Task_WorkType_Group != null)
                        {
                            workTypeTaskChildDTO.WT_Task_WorkType_Group = JsonConvert.SerializeObject(model.ArrayDocument[i].WT_Task_WorkType_Group);
                        }
                       


                        workTypeTaskChildDTO.WT_Task_ClientDueDateTo = model.ArrayDocument[i].WT_Task_ClientDueDateTo;
                        workTypeTaskChildDTO.WT_Task_ClientDueDateFrom = model.ArrayDocument[i].WT_Task_ClientDueDateFrom;
                        workTypeTaskChildDTO.WT_Task_IsActive = model.ArrayDocument[i].WT_Task_IsActive;
                        workTypeTaskChildDTO.WT_Task_IsDelete = model.ArrayDocument[i].WT_Task_IsDelete;
                        workTypeTaskChildDTO.UserID = model.UserID;
                        workTypeTaskChildDTO.Type = model.ArrayDocument[i].WT_Task_pkeyID != 0 ? 2 : model.Type;

                        var objDatax = workTypeTaskChildData.AddWorkTypeTaskChildData(workTypeTaskChildDTO);

                        if (objDatax[0].Status != "0")
                        {
                            Int64 WT_sett_pkeyID = 0;
                            if (!string.IsNullOrEmpty(objDatax[0].WT_Task_pkeyID))
                            {
                                WT_sett_pkeyID = Convert.ToInt64(objDatax[0].WT_Task_pkeyID);
                            }

                            if (model.ArrayDocument[i].WT_Task_Company != null && model.ArrayDocument[i].WT_Task_Company.Count > 0)
                            {
                                WT_Company_TBLDTO wT_Company_TBLDTO = new WT_Company_TBLDTO();

                                for (int p = 0; p < model.ArrayDocument[i].WT_Task_Company.Count; p++)
                                {
                                    wT_Company_TBLDTO.WC_pkeyID = 0;
                                    wT_Company_TBLDTO.WC_Task_pkeyID = model.Task_pkeyID;
                                    wT_Company_TBLDTO.WC_Task_sett_ID = model.ArrayDocument[i].WT_Task_Company[p].Client_pkeyID;
                                    wT_Company_TBLDTO.WC_IsActive = true;
                                    wT_Company_TBLDTO.WC_Isdelete = false;
                                    wT_Company_TBLDTO.UserID = model.UserID;
                                    wT_Company_TBLDTO.Type = 1;
                                    wT_Company_TBLDTO.WC_WTTaskSett_Id = WT_sett_pkeyID;

                                    var returnkeyCompany = wT_Company_TBLData.AddWTCompanyData(wT_Company_TBLDTO);
                                }
                            }
                            if (model.ArrayDocument[i].WT_Task_State != null && model.ArrayDocument[i].WT_Task_State.Count > 0)
                            {
                                WT_State_TBLDTO wT_State_TBLDTO = new WT_State_TBLDTO();

                                for (int q = 0; q < model.ArrayDocument[i].WT_Task_State.Count; q++)
                                {
                                    wT_State_TBLDTO.WS_pkeyID = 0;
                                    wT_State_TBLDTO.WS_Task_pkeyID = model.Task_pkeyID;
                                    wT_State_TBLDTO.WS_Task_sett_ID = model.ArrayDocument[i].WT_Task_State[q].IPL_StateID;
                                    wT_State_TBLDTO.WS_IsActive = true;
                                    wT_State_TBLDTO.WS_Isdelete = false;
                                    wT_State_TBLDTO.UserID = model.UserID;
                                    wT_State_TBLDTO.Type = 1;
                                    wT_State_TBLDTO.WS_WTTaskSett_Id = WT_sett_pkeyID;

                                    var returnkeystate = wT_Company_TBLData.AddWTStateData(wT_State_TBLDTO);
                                }
                            }
                            if (model.ArrayDocument[i].WT_Task_Customer != null && model.ArrayDocument[i].WT_Task_Customer.Count > 0)
                            {
                                WT_Customer_TBLDTO wT_Customer_TBLDTO = new WT_Customer_TBLDTO();

                                for (int t = 0; t < model.ArrayDocument[i].WT_Task_Customer.Count; t++)
                                {

                                    wT_Customer_TBLDTO.WCust_pkeyID = 0;
                                    wT_Customer_TBLDTO.WCust_Task_pkeyID = model.Task_pkeyID;
                                    wT_Customer_TBLDTO.WCust_Task_sett_ID = model.ArrayDocument[i].WT_Task_Customer[t].Cust_Num_pkeyId;
                                    wT_Customer_TBLDTO.WCust_IsActive = true;
                                    wT_Customer_TBLDTO.WCust_Isdelete = false;
                                    wT_Customer_TBLDTO.UserID = model.UserID;
                                    wT_Customer_TBLDTO.Type = 1;
                                    wT_Customer_TBLDTO.WCust_WTTaskSett_Id = WT_sett_pkeyID;

                                    var returnkeyCustomer = wT_Company_TBLData.AddWTCustomerData(wT_Customer_TBLDTO);
                                }
                            }
                            if (model.ArrayDocument[i].WT_Task_LoneType != null && model.ArrayDocument[i].WT_Task_LoneType.Count > 0)
                            {
                                WT_Loan_TBLDTO wT_Loan_TBLDTO = new WT_Loan_TBLDTO();

                                for (int u = 0; u < model.ArrayDocument[i].WT_Task_LoneType.Count; u++)
                                {
                                    wT_Loan_TBLDTO.WL_pkeyID = 0;
                                    wT_Loan_TBLDTO.WL_Task_pkeyID = model.Task_pkeyID;
                                    wT_Loan_TBLDTO.WL_Task_sett_ID = model.ArrayDocument[i].WT_Task_LoneType[u].Loan_pkeyId;
                                    wT_Loan_TBLDTO.WL_IsActive = true;
                                    wT_Loan_TBLDTO.WL_Isdelete = false;
                                    wT_Loan_TBLDTO.UserID = model.UserID;
                                    wT_Loan_TBLDTO.Type = 1;
                                    wT_Loan_TBLDTO.WL_WTTaskSett_Id = WT_sett_pkeyID;

                                    var returnkeyCustomer = wT_Company_TBLData.AddWTLoanData(wT_Loan_TBLDTO);
                                }
                            }
                            if (model.ArrayDocument[i].WT_Task_WorkType != null && model.ArrayDocument[i].WT_Task_WorkType.Count > 0)
                            {
                                WT_WorkType_TBLDTO wT_WorkType_TBLDTO = new WT_WorkType_TBLDTO();

                                for (int g = 0; g < model.ArrayDocument[i].WT_Task_WorkType.Count; g++)
                                {
                                    wT_WorkType_TBLDTO.Wwork_pkeyID = 0;
                                    wT_WorkType_TBLDTO.Wwork_Task_pkeyID = model.Task_pkeyID;
                                    wT_WorkType_TBLDTO.Wwork_Task_sett_ID = model.ArrayDocument[i].WT_Task_WorkType[g].WT_pkeyID;
                                    wT_WorkType_TBLDTO.Wwork_IsActive = true;
                                    wT_WorkType_TBLDTO.Wwork_Isdelete = false;
                                    wT_WorkType_TBLDTO.UserID = model.UserID;
                                    wT_WorkType_TBLDTO.Type = 1;
                                    wT_WorkType_TBLDTO.Wwork_WTTaskSett_Id = WT_sett_pkeyID;

                                    var returnkeyCustomer = wT_Company_TBLData.AddWTWorkTypeData(wT_WorkType_TBLDTO);
                                }
                            }
                            if (model.ArrayDocument[i].WT_Task_WorkType_Group != null && model.ArrayDocument[i].WT_Task_WorkType_Group.Count > 0)
                            {
                                WT_WorkType_Group_TBLDTO wT_WorkType_Group_TBLDTO = new WT_WorkType_Group_TBLDTO();

                                for (int g = 0; g < model.ArrayDocument[i].WT_Task_WorkType_Group.Count; g++)
                                {
                                    wT_WorkType_Group_TBLDTO.Wwork_Group_pkeyID = 0;
                                    wT_WorkType_Group_TBLDTO.Wwork_Group_Task_pkeyID = model.Task_pkeyID;
                                    wT_WorkType_Group_TBLDTO.Wwork_Group_Task_sett_ID = model.ArrayDocument[i].WT_Task_WorkType_Group[g].Work_Type_Cat_pkeyID;
                                    wT_WorkType_Group_TBLDTO.Wwork_Group_IsActive = true;
                                    wT_WorkType_Group_TBLDTO.Wwork_Group_Isdelete = false;
                                    wT_WorkType_Group_TBLDTO.UserID = model.UserID;
                                    wT_WorkType_Group_TBLDTO.Type = 1;
                                    wT_WorkType_Group_TBLDTO.Wwork_Group_WTTaskSett_Id = WT_sett_pkeyID;

                                    var returnkeyCustomer = wT_Company_TBLData.AddWTWorkTypeGroupData(wT_WorkType_Group_TBLDTO);
                                }
                            }
                        }                        
                    }              
                }
                if (model.ArrayPreset != null)
                {
                    TaskPresetDTO taskPresetDTO = new TaskPresetDTO();
                    TaskPresetData taskPresetData = new TaskPresetData();

                    for (int i = 0; i < model.ArrayPreset.Count; i++)
                    {
                        taskPresetDTO.Task_Preset_pkeyId = model.ArrayPreset[i].Task_Preset_pkeyId;
                        taskPresetDTO.Task_Preset_ID = model.Task_pkeyID;
                        taskPresetDTO.Task_Preset_Text = model.ArrayPreset[i].Task_Preset_Text;
                        taskPresetDTO.Task_Preset_IsActive = model.ArrayPreset[i].Task_Preset_IsActive;
                        taskPresetDTO.Task_Preset_IsDelete = model.ArrayPreset[i].Task_Preset_IsDelete;
                        taskPresetDTO.Task_Preset_IsDefault = model.ArrayPreset[i].Task_Preset_IsDefault;
                        taskPresetDTO.Type = model.ArrayPreset[i].Task_Preset_pkeyId != 0 ? 2 : model.Type;

                        objData = taskPresetData.AddTaskPresetChildData(taskPresetDTO);
                    }
                }

                if (model.Task_File_Array != null)
                {
                    Task_Master_Files_Data task_Master_Files_Data = new Task_Master_Files_Data();
                    task_Master_Files_Data.FilestatusUpdateTaskMasterData(model.Task_File_Array);
                }
                if(model.TaskPhotoSetting!=null)
                {
                    TaskPhotoSetting taskPhotoSetting = new TaskPhotoSetting();
                    taskPhotoSetting.TPS_pkeyID= model.TaskPhotoSetting.TPS_pkeyID;
                    taskPhotoSetting.TPS_TaskID = model.Task_pkeyID;
                    taskPhotoSetting.TPS_LoadPhotosCompletionStatus = model.TaskPhotoSetting.TPS_LoadPhotosCompletionStatus;
                    taskPhotoSetting.TPS_MeasurementBidStatus = model.TaskPhotoSetting.TPS_MeasurementBidStatus;
                    taskPhotoSetting.TPS_MeasurementCompletionStatus = model.TaskPhotoSetting.TPS_MeasurementCompletionStatus;
                    taskPhotoSetting.TPS_PhotoRequirementStatus = model.TaskPhotoSetting.TPS_PhotoRequirementStatus;
                    taskPhotoSetting.TPS_Inspection = model.TaskPhotoSetting.TPS_Inspection;
                    taskPhotoSetting.TPS_Bid = model.TaskPhotoSetting.TPS_Bid;
                    taskPhotoSetting.TPS_BidMeasurement = model.TaskPhotoSetting.TPS_BidMeasurement;
                    taskPhotoSetting.TPS_CompletionAfter = model.TaskPhotoSetting.TPS_CompletionAfter;
                    taskPhotoSetting.TPS_CompletionBefore= model.TaskPhotoSetting.TPS_CompletionBefore;
                    taskPhotoSetting.TPS_CompletionDuring= model.TaskPhotoSetting.TPS_CompletionDuring;
                    taskPhotoSetting.TPS_CompletionLoad = model.TaskPhotoSetting.TPS_CompletionLoad;
                    taskPhotoSetting.TPS_CompletionMeasurement = model.TaskPhotoSetting.TPS_CompletionMeasurement;
                    taskPhotoSetting.TPS_PhotoRequirementMax = model.TaskPhotoSetting.TPS_PhotoRequirementMax;
                    taskPhotoSetting.TPS_PhotoRequirementMin= model.TaskPhotoSetting.TPS_PhotoRequirementMin;
                    
                    taskPhotoSetting.Type = model.TaskPhotoSetting.TPS_pkeyID != 0 ? 2 : model.Type;
                    taskPhotoSetting.UserID = model.UserID;

                    TaskPhotoSettingData taskPhotoSettingData = new TaskPhotoSettingData();
                    taskPhotoSettingData.AddUpdateDeletedTaskPhotoSetting(taskPhotoSetting);
                }
             

                objAddData.Add(objData);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objAddData;
        }

        private DataSet GetTaskSettingFilter(TaskFilterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_TaskFilterData]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Task_pkeyID", 1 + "#bigint#" + model.Task_pkeyID);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ds;
        }

        public List<dynamic> GetTaskFilterData(TaskFilterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetTaskSettingFilter(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<TaskSettingChildDTO> TaskChildDetails =
                   (from item in myEnumerableFeaprd
                    select new TaskSettingChildDTO
                    {
                        Task_sett_pkeyID = item.Field<Int64>("Task_sett_pkeyID"),
                        Task_sett_ID = item.Field<Int64>("Task_sett_ID"),
                        Task_sett_Company = item.Field<String>("Task_sett_Company"),
                        Task_sett_State = item.Field<String>("Task_sett_State"),
                        Task_sett_Country = item.Field<String>("Task_sett_Country"),
                        Task_sett_Zip = item.Field<String>("Task_sett_Zip"),
                        Task_sett_Contractor = item.Field<String>("Task_sett_Contractor"),
                        Task_sett_Customer = item.Field<String>("Task_sett_Customer"),
                        Task_sett_Lone = item.Field<String>("Task_sett_Lone"),
                        Task_sett_Con_Unit_Price = item.Field<Decimal?>("Task_sett_Con_Unit_Price"),
                        Task_sett_CLI_Unit_Price = item.Field<Decimal?>("Task_sett_CLI_Unit_Price"),
                        Task_sett_Flat_Free = item.Field<Boolean?>("Task_sett_Flat_Free"),
                        Task_sett_Price_Edit = item.Field<Boolean?>("Task_sett_Price_Edit"),
                        Task_Work_TypeGroup = item.Field<String>("Task_Work_TypeGroup"),
                        Task_sett_IsActive = item.Field<Boolean?>("Task_sett_IsActive"),
                        Task_sett_Disable_Default = item.Field<Boolean?>("Task_sett_Disable_Default"),
                        Task_sett_WorkType = item.Field<String>("Task_sett_WorkType"),
                        Task_sett_LotPrice = item.Field<String>("Task_sett_LotPrice"),
                        Task_sett_LOT_Min = item.Field<Decimal?>("Task_sett_LOT_Min"),
                        Task_sett_LOT_Max = item.Field<Decimal?>("Task_sett_LOT_Max"),
                    }).ToList();

                objDynamic.Add(TaskChildDetails);

                var myEnumerableFeaprdx = ds.Tables[1].AsEnumerable();
                List<WorkTypeTaskChildDTO> WTTaskChildDetails =
                   (from item in myEnumerableFeaprdx
                    select new WorkTypeTaskChildDTO
                    {
                        WT_Task_pkeyID = item.Field<Int64>("WT_Task_pkeyID"),
                        WT_Task_ID = item.Field<Int64>("WT_Task_ID"),
                        WT_Task_Company = item.Field<String>("WT_Task_Company"),
                        WT_Task_State = item.Field<String>("WT_Task_State"),
                        WT_Task_Customer = item.Field<String>("WT_Task_Customer"),
                        WT_Task_LoneType = item.Field<String>("WT_Task_LoneType"),
                        WT_Task_WorkType = item.Field<String>("WT_Task_WorkType"),
                        WT_Task_WorkType_Group = item.Field<String>("WT_Task_WorkType_Group"),
                        WT_Task_ClientDueDateTo = item.Field<DateTime?>("WT_Task_ClientDueDateTo"),
                        WT_Task_ClientDueDateFrom = item.Field<DateTime?>("WT_Task_ClientDueDateFrom"),
                        WT_Task_IsActive = item.Field<Boolean?>("WT_Task_IsActive"),
                    }).ToList();

                objDynamic.Add(WTTaskChildDetails);

                var myEnumerableFeaprdxx = ds.Tables[2].AsEnumerable();
                List<TaskPresetDTO> TaskpresetDetails =
                   (from item in myEnumerableFeaprdxx
                    select new TaskPresetDTO
                    {
                        Task_Preset_pkeyId = item.Field<Int64>("Task_Preset_pkeyId"),
                        Task_Preset_ID = item.Field<Int64>("Task_Preset_ID"),
                        Task_Preset_Text = item.Field<String>("Task_Preset_Text"),
                        Task_Preset_IsActive = item.Field<Boolean?>("Task_Preset_IsActive"),
                        Task_Preset_IsDefault = item.Field<Boolean?>("Task_Preset_IsDefault"),
                    }).ToList();

                objDynamic.Add(TaskpresetDetails);

                var myEnumerableTaskPhotoSettings = ds.Tables[3].AsEnumerable();
                TaskPhotoSettingDTO taskPhotoSettingDTOs =
                   (from item in myEnumerableTaskPhotoSettings
                    select new TaskPhotoSettingDTO
                    {
                        TPS_pkeyID= item.Field<Int64>("TPS_pkeyID"),
                        TPS_TaskID = item.Field<Int64>("TPS_TaskID"),
                        TPS_LoadPhotosCompletionStatus = item.Field<Boolean?>("TPS_LoadPhotosCompletionStatus"),
                        TPS_MeasurementBidStatus = item.Field<Boolean?>("TPS_MeasurementBidStatus"),
                        TPS_MeasurementCompletionStatus = item.Field<Boolean?>("TPS_MeasurementCompletionStatus"),
                        TPS_PhotoRequirementStatus = item.Field<Boolean?>("TPS_PhotoRequirementStatus"),
                        TPS_Bid = item.Field<Decimal?>("TPS_Bid"),
                        TPS_BidMeasurement = item.Field<Decimal?>("TPS_BidMeasurement"),
                        TPS_CompletionAfter = item.Field<Decimal?>("TPS_CompletionAfter"),
                        TPS_CompletionBefore = item.Field<Decimal?>("TPS_CompletionBefore"),
                        TPS_CompletionDuring = item.Field<Decimal?>("TPS_CompletionDuring"),
                        TPS_CompletionLoad = item.Field<Decimal?>("TPS_CompletionLoad"),
                        TPS_CompletionMeasurement = item.Field<Decimal?>("TPS_CompletionMeasurement"),
                        TPS_Inspection = item.Field<Decimal?>("TPS_Inspection"),
                        TPS_PhotoRequirementMax = item.Field<Decimal?>("TPS_PhotoRequirementMax"),
                        TPS_PhotoRequirementMin = item.Field<Decimal?>("TPS_PhotoRequirementMin"),
                    }).FirstOrDefault();

                objDynamic.Add(taskPhotoSettingDTOs);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objDynamic;
        }
    }
}