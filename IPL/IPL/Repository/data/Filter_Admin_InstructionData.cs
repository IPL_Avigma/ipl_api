﻿using Avigma.Repository.Lib;
using IPL.Models;
using IPLApp.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class Filter_Admin_InstructionData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        public List<dynamic> AddUpdate_Filter_Admin_Instruction(Filter_Admin_Instruction_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_Filter_Admin_Instruction_Master]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Ins_Filter_PkeyID", 1 + "#bigint#" + model.Ins_Filter_PkeyID);
                input_parameters.Add("@Ins_Filter_InsName", 1 + "#nvarchar#" + model.Ins_Filter_InsName);
                input_parameters.Add("@Ins_Filter_InsDesc", 1 + "#nvarchar#" + model.Ins_Filter_InsDesc);
                input_parameters.Add("@Ins_Filter_InsIsActive", 1 + "#bit#" + model.Ins_Filter_InsIsActive);
                input_parameters.Add("@Ins_Filter_IsActive", 1 + "#bit#" + model.Ins_Filter_IsActive);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserId);
                input_parameters.Add("@Ins_Filter_PkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        private DataSet Get_Filter_Admin_Instruction(Filter_Admin_Instruction_MasterDTO model)
        {
            DataSet ds = null;
            try
            {

                string selectProcedure = "[Get_Filter_Admin_Instruction_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Ins_Filter_PkeyID", 1 + "#bigint#" + model.Ins_Filter_PkeyID);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> Get_Filter_Admin_InstructionDetails(Filter_Admin_Instruction_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = Get_Filter_Admin_Instruction(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Filter_Admin_Instruction_MasterDTO> Forms_Master_Files =
                   (from item in myEnumerableFeaprd
                    select new Filter_Admin_Instruction_MasterDTO
                    {
                        Ins_Filter_PkeyID = item.Field<Int64>("Ins_Filter_PkeyID"),
                        Ins_Filter_InsName = item.Field<String>("Ins_Filter_InsName"),
                        Ins_Filter_InsDesc = item.Field<String>("Ins_Filter_InsDesc"),
                        Ins_Filter_InsIsActive = item.Field<Boolean?>("Ins_Filter_InsIsActive")

                    }).ToList();

                objDynamic.Add(Forms_Master_Files);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
    }
}