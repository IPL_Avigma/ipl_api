﻿using Avigma.Repository.Lib;
using IPL.Models;
using Avigma.Models;
using IPL.Repository.data;
using IPLApp.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using AutoMapper;
using IPL.Repository.Lib;
using IPL.Models.Avigma;
using System.Text;
using System.Configuration;
using System.Net;
using Firebase.Database;
using System.Threading.Tasks;
using Firebase.Database.Query;

namespace IPLApp.Repository.data
{
    public class UserMasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        ContractorCoverageAreaData contractorCoverageAreaData = new ContractorCoverageAreaData();
        UserVerificationMaster_Data userVerificationMaster_Data = new UserVerificationMaster_Data();
        Log log = new Log();
        private List<dynamic> AddupdateUserMasterdetails(UserMasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objuserData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_User_Master]";
            UserMaster userMaster = new UserMaster();

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@User_pkeyID", 1 + "#bigint#" + model.User_pkeyID);
                input_parameters.Add("@User_FirstName", 1 + "#nvarchar#" + model.User_FirstName);
                input_parameters.Add("@User_LastName", 1 + "#nvarchar#" + model.User_LastName);
                input_parameters.Add("@User_CompanyName", 1 + "#nvarchar#" + model.User_CompanyName);
                input_parameters.Add("@User_Address", 1 + "#nvarchar#" + model.User_Address);
                input_parameters.Add("@User_City", 1 + "#nvarchar#" + model.User_City);
                input_parameters.Add("@User_State", 1 + "#int#" + model.User_State);
                input_parameters.Add("@User_Zip", 1 + "#bigint#" + model.User_Zip);
                input_parameters.Add("@User_CellNumber", 1 + "#nvarchar#" + model.User_CellNumber);
                input_parameters.Add("@User_Sys_Record", 1 + "#int#" + model.User_Sys_Record);
                input_parameters.Add("@User_LoginName", 1 + "#nvarchar#" + model.User_LoginName);
                input_parameters.Add("@User_Password", 1 + "#nvarchar#" + model.User_Password);
                input_parameters.Add("@User_Email", 1 + "#nvarchar#" + model.User_Email);
                input_parameters.Add("@User_Group", 1 + "#int#" + model.User_Group);
                input_parameters.Add("@User_Contractor", 1 + "#bit#" + model.User_Contractor);
                input_parameters.Add("@User_Cordinator", 1 + "#bit#" + model.User_Cordinator);
                input_parameters.Add("@User_Processor", 1 + "#bit#" + model.User_Processor);
                input_parameters.Add("@User_OpenOrderDisCriteria", 1 + "#nvarchar#" + model.User_OpenOrderDisCriteria);
                input_parameters.Add("@User_PastWorkOrder", 1 + "#bit#" + model.User_PastWorkOrder);
                input_parameters.Add("@User_PastOrderDisCriteria", 1 + "#nvarchar#" + model.User_PastOrderDisCriteria);
                input_parameters.Add("@User_SelectOrderDisCriteria", 1 + "#nvarchar#" + model.User_SelectOrderDisCriteria);
                input_parameters.Add("@User_BackgroundCheckProvider", 1 + "#nvarchar#" + model.User_BackgroundCheckProvider);
                input_parameters.Add("@User_BackgroundCheckId", 1 + "#nvarchar#" + model.User_BackgroundCheckId);
                input_parameters.Add("@User_BackgroundDocPath", 1 + "#nvarchar#" + model.User_BackgroundDocPath);
                input_parameters.Add("@User_BackgroundDocName", 1 + "#nvarchar#" + model.User_BackgroundDocName);
                input_parameters.Add("@User_Comments", 1 + "#nvarchar#" + model.User_Comments);
                input_parameters.Add("@User_Assi_Admin", 1 + "#bit#" + model.User_Assi_Admin);
                input_parameters.Add("@User_Active", 1 + "#int#" + model.User_Active);
                input_parameters.Add("@User_WorkOrder", 1 + "#int#" + model.User_WorkOrder);
                input_parameters.Add("@User_Wo_History", 1 + "#int#" + model.User_Wo_History);
                input_parameters.Add("@User_Disc_percentage", 1 + "#decimal#" + model.User_Disc_percentage);
                input_parameters.Add("@User_Tme_Zone", 1 + "#int#" + model.User_Tme_Zone);
                input_parameters.Add("@User_Auto_Assign", 1 + "#bit#" + model.User_Auto_Assign);
                input_parameters.Add("@User_Leg_FirstName", 1 + "#nvarchar#" + model.User_Leg_FirstName);
                input_parameters.Add("@User_Leg_LastName", 1 + "#nvarchar#" + model.User_Leg_LastName);
                input_parameters.Add("@User_Leg_CellPhone", 1 + "#nvarchar#" + model.User_Leg_CellPhone);
                input_parameters.Add("@User_Leg_Address", 1 + "#nvarchar#" + model.User_Leg_Address);
                input_parameters.Add("@User_Leg_Address1", 1 + "#nvarchar#" + model.User_Leg_Address1);
                input_parameters.Add("@User_Leg_City", 1 + "#nvarchar#" + model.User_Leg_City);
                input_parameters.Add("@User_Leg_State", 1 + "#int#" + model.User_Leg_State);
                input_parameters.Add("@User_Leg_Notes", 1 + "#nvarchar#" + model.User_Leg_Notes);
                input_parameters.Add("@User_Email_Note", 1 + "#bit#" + model.User_Email_Note);
                input_parameters.Add("@User_Emai_Reminders", 1 + "#bit#" + model.User_Emai_Reminders);
                input_parameters.Add("@User_Email_New_Wo", 1 + "#bit#" + model.User_Email_New_Wo);
                input_parameters.Add("@User_Email_UnAssigned_Wo", 1 + "#bit#" + model.User_Email_UnAssigned_Wo);
                input_parameters.Add("@User_Email_FollowUp", 1 + "#bit#" + model.User_Email_FollowUp);
                input_parameters.Add("@User_Text_Note", 1 + "#bit#" + model.User_Text_Note);
                input_parameters.Add("@User_Text_Reminders", 1 + "#bit#" + model.User_Text_Reminders);
                input_parameters.Add("@User_Text_New_Wo", 1 + "#bit#" + model.User_Text_New_Wo);
                input_parameters.Add("@User_Text_UnAssigned_Wo", 1 + "#bit#" + model.User_Text_UnAssigned_Wo);
                input_parameters.Add("@User_Text_FollowUp", 1 + "#bit#" + model.User_Text_FollowUp);
                input_parameters.Add("@User_Alert_EmailReply", 1 + "#bit#" + model.User_Alert_EmailReply);
                input_parameters.Add("@User_Alert_Ready_Office", 1 + "#bit#" + model.User_Alert_Ready_Office);
                input_parameters.Add("@User_Misc_Contractor_Score", 1 + "#bigint#" + model.User_Misc_Contractor_Score);
                input_parameters.Add("@User_Misc_Insurance_Expire", 1 + "#nvarchar#" + model.User_Misc_Insurance_Expire);
                input_parameters.Add("@User_Misc_Pruvan_Username", 1 + "#nvarchar#" + model.User_Misc_Pruvan_Username);
                input_parameters.Add("@User_Misc_PushKey", 1 + "#nvarchar#" + model.User_Misc_PushKey);
                input_parameters.Add("@User_Misc_StartDate", 1 + "#int#" + model.User_Misc_StartDate);
                input_parameters.Add("@User_Misc_Device_Id", 1 + "#nvarchar#" + model.User_Misc_Device_Id);
                input_parameters.Add("@User_Misc_ABC", 1 + "#nvarchar#" + model.User_Misc_ABC);
                input_parameters.Add("@User_Misc_Service_Id", 1 + "#nvarchar#" + model.User_Misc_Service_Id);

                input_parameters.Add("@User_Email_Cancelled", 1 + "#bit#" + model.User_Email_Cancelled);
                input_parameters.Add("@User_Email_New_Message", 1 + "#bit#" + model.User_Email_New_Message);
                input_parameters.Add("@User_Email_Field_Complete", 1 + "#bit#" + model.User_Email_Field_Complete);
                input_parameters.Add("@User_Email_Daily_Digest", 1 + "#bit#" + model.User_Email_Daily_Digest);
                input_parameters.Add("@User_Text_Cancelled", 1 + "#bit#" + model.User_Text_Cancelled);
                input_parameters.Add("@User_Text_New_Message", 1 + "#bit#" + model.User_Text_New_Message);
                input_parameters.Add("@User_Text_Field_Complete", 1 + "#bit#" + model.User_Text_Field_Complete);

                input_parameters.Add("@User_AssignClient", 1 + "#nvarchar#" + model.User_AssignClient);
                input_parameters.Add("@User_Token_val", 1 + "#nvarchar#" + model.User_Token_val);
                input_parameters.Add("@User_ImagePath", 1 + "#nvarchar#" + model.User_ImagePath);
                input_parameters.Add("@User_State_strval", 1 + "#bigint#" + model.User_State_strval);
                input_parameters.Add("@User_Tracking", 1 + "#bit#" + model.User_Tracking);
                input_parameters.Add("@User_Tracking_Time", 1 + "#int#" + model.User_Tracking_Time);
                input_parameters.Add("@User_BusinessID_Social_No", 1 + "#nvarchar#" + model.User_BusinessID_Social_No);
                input_parameters.Add("@User_Track_Payment", 1 + "#bit#" + model.User_Track_Payment);
                input_parameters.Add("@User_Default_Expence_Account_Id", 1 + "#nvarchar#" + model.User_Default_Expence_Account_Id);
                input_parameters.Add("@User_Source", 1 + "#int#" + model.User_Source);



                input_parameters.Add("@User_IsActive", 1 + "#bit#" + model.User_IsActive);
                input_parameters.Add("@User_IsDelete", 1 + "#bit#" + model.User_IsDelete);
                input_parameters.Add("@User_Con_Cat_Id", 1 + "#bigint#" + model.User_Con_Cat_Id);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@User_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    userMaster.User_pkeyID = "0";
                    userMaster.Status = "0";
                    userMaster.ErrorMessage = "Error while Saving in the Database";

                }
                else if (objData[1] == -99 && model.Type == 1)
                {
                    userMaster.User_pkeyID = "0";
                    userMaster.Status = "-99";
                    userMaster.ErrorMessage = "UserName Already Exist";

                }
                else
                {
                    if (model.Type == 1)
                    {
                        userMaster.User_pkeyID = Convert.ToString(objData[0]);
                        model.User_pkeyID = objData[0];
                    }
                    else
                    {
                        userMaster.User_pkeyID = model.User_pkeyID.ToString();
                    }

                    userMaster.Status = Convert.ToString(objData[1]);


                    switch (model.Type)
                    {

                        case 1:
                        case 2:
                            {
                                if (model.StrAddressArray != null)
                                {
                                    UserAddressDTO userAddressDTO = new UserAddressDTO();
                                    UserAddressData UserAddressData = new UserAddressData();

                                    for (int i = 0; i < model.StrAddressArray.Count; i++)
                                    {
                                        GoogleLocation googleLocation = new GoogleLocation();
                                        GoogleLocationDTO googleLocationDTO = new GoogleLocationDTO();
                                        if (!string.IsNullOrEmpty(model.StrAddressArray[i].IPL_Address) && model.StrAddressArray[i].IPL_Primary_Zip_Code != 0)
                                        {
                                            WorkOrderMasterData workOrderMasterData = new WorkOrderMasterData();
                                            workOrderxDTO workOrderxDTO = new workOrderxDTO();
                                            workOrderxDTO.address1 = model.StrAddressArray[i].IPL_Address;
                                            workOrderxDTO.state = model.StrAddressArray[i].IPL_State.ToString();
                                            workOrderxDTO.zip = model.StrAddressArray[i].IPL_Primary_Zip_Code;
                                            workOrderxDTO.Type = 4;
                                            DataSet ds = workOrderMasterData.Get_WorkOrderLatLongByAddress(workOrderxDTO);
                                            if (ds.Tables.Count > 0)
                                            {
                                                if (ds.Tables[0].Rows.Count > 0)
                                                {
                                                    for (int j = 0; j < ds.Tables[0].Rows.Count; j++)
                                                    {
                                                        userAddressDTO.IPL_Primary_Latitude = ds.Tables[0].Rows[j]["gpsLatitude"].ToString();
                                                        userAddressDTO.IPL_Primary_Longitude = ds.Tables[0].Rows[j]["gpsLongitude"].ToString();
                                                    }
                                                }
                                                else
                                                {
                                                    string strAddress = model.StrAddressArray[i].IPL_Address + "  ," + model.StrAddressArray[i].IPL_State + "  " + model.StrAddressArray[i].IPL_Primary_Zip_Code.ToString();
                                                    googleLocationDTO.Address = strAddress;
                                                    googleLocationDTO = googleLocation.GetLatitudeLogitudeWithAPIKey(googleLocationDTO);
                                                    userAddressDTO.IPL_Primary_Latitude = googleLocationDTO.Latitude;
                                                    userAddressDTO.IPL_Primary_Longitude = googleLocationDTO.Longitude;
                                                }
                                            }
                                            else
                                            {
                                                string strAddress = model.StrAddressArray[i].IPL_Address + "  ," + model.StrAddressArray[i].IPL_State + "  " + model.StrAddressArray[i].IPL_Primary_Zip_Code.ToString();
                                                googleLocationDTO.Address = strAddress;
                                                googleLocationDTO = googleLocation.GetLatitudeLogitudeWithAPIKey(googleLocationDTO);
                                                userAddressDTO.IPL_Primary_Latitude = googleLocationDTO.Latitude;
                                                userAddressDTO.IPL_Primary_Longitude = googleLocationDTO.Longitude;
                                            }

                                           

                                            FirebaseUserDTO firebaseUserDTO = new FirebaseUserDTO { UserID = model.User_pkeyID, Type = 1 };
                                            LiveUserLocationData liveUserLocationData = new LiveUserLocationData();
                                            var userDetail = liveUserLocationData.GetFirebaseUser(firebaseUserDTO);
                                            if (userDetail != null && userDetail.LoggedUserDetail != null && userDetail.LoggedUserDetail.Count > 0)
                                            {
                                                if (userDetail.LoggedUserDetail[0].GroupRoleId == 2)
                                                {
                                                    LiveUserLocationFBDTO liveUserLocationFBDTO = new LiveUserLocationFBDTO();
                                                    liveUserLocationFBDTO.name = model.User_FirstName + " " + model.User_LastName;
                                                    liveUserLocationFBDTO.cellNumber = model.User_CellNumber;
                                                    liveUserLocationFBDTO.latitude = googleLocationDTO.Latitude;
                                                    liveUserLocationFBDTO.longitude = googleLocationDTO.Longitude;

                                                    AddFirebaseCompanyUserLocationAsync(liveUserLocationFBDTO, userDetail.LoggedUserDetail[0].IPL_Company_ID, userDetail.LoggedUserDetail[0].User_LoginName);
                                                }   
                                            }
                                        }

                                        userAddressDTO.IPL_PkeyID = model.StrAddressArray[i].IPL_PkeyID;
                                        userAddressDTO.IPL_UserID = Convert.ToInt64(userMaster.User_pkeyID);
                                        userAddressDTO.IPL_Address = model.StrAddressArray[i].IPL_Address;
                                        userAddressDTO.IPL_State = model.StrAddressArray[i].IPL_State;
                                        userAddressDTO.IPL_County = model.StrAddressArray[i].IPL_County;
                                        userAddressDTO.IPL_City = model.StrAddressArray[i].IPL_City;
                                        userAddressDTO.IPL_Primary_Zip_Code = model.StrAddressArray[i].IPL_Primary_Zip_Code;
                                        userAddressDTO.Type = userAddressDTO.IPL_PkeyID == 0 ? 1 : model.Type;
                                        userAddressDTO.IPL_IsActive = model.User_IsActive;
                                        var objAddress = UserAddressData.AddUserAddressData(userAddressDTO);

                                        ZipMasterDTO zipMasterDTO = new ZipMasterDTO();
                                        zipMasterDTO.Type = 1;
                                        zipMasterDTO.Zip_zip = model.StrAddressArray[i].IPL_Primary_Zip_Code;
                                        zipMasterDTO.Zip_lat = googleLocationDTO.Latitude;
                                        zipMasterDTO.Zip_lng = googleLocationDTO.Longitude;
                                        zipMasterDTO.Zip_state_id = model.StrAddressArray[i].IPL_StateName;
                                        zipMasterDTO.Zip_county_name = model.StrAddressArray[i].IPL_CountyName;
                                        var objZip = contractorCoverageAreaData.AddUserZipCode(zipMasterDTO);

                                        ContractorCoverageAreaDTO contractorCoverageAreaDTO = new ContractorCoverageAreaDTO();
                                        contractorCoverageAreaDTO.Type = userAddressDTO.IPL_PkeyID == 0 ? 1 : 5;
                                        contractorCoverageAreaDTO.Cont_Coverage_Area_UserID = model.User_pkeyID;
                                        contractorCoverageAreaDTO.Cont_Coverage_Area_Zip_Code = model.StrAddressArray[i].IPL_Primary_Zip_Code;
                                        contractorCoverageAreaDTO.Cont_Coverage_Area_IsActive = true;
                                        contractorCoverageAreaDTO.Cont_Coverage_Area_IsDelete = false;
                                        contractorCoverageAreaDTO.Cont_Coverage_Area_State_Id = model.StrAddressArray[i].IPL_State;
                                        contractorCoverageAreaDTO.Cont_Coverage_Area_County_Id = model.StrAddressArray[i].IPL_County;
                                        contractorCoverageAreaDTO.Cont_Coverage_Area_UserAddressID = Convert.ToInt64(objAddress[0].IPL_PkeyID);
                                        contractorCoverageAreaDTO.UserID = model.UserID.GetValueOrDefault(0);

                                        var objCoverage = contractorCoverageAreaData.AddContractorCoverageAreaData(contractorCoverageAreaDTO);

                                    }

                                }


                                if (model.User_AssignClient != null)
                                {

                                    var SizeData = JsonConvert.DeserializeObject<List<ClientCompanyDTOForDropDowm>>(model.User_AssignClient);
                                    UserClientRelationData userClientRelationData = new UserClientRelationData();
                                    UserClientRelationDTO userClientRelationDTO = new UserClientRelationDTO();

                                    userClientRelationDTO.Type = 4;
                                    userClientRelationDTO.UCM_UserID = Convert.ToInt64(userMaster.User_pkeyID);
                                    userClientRelationData.AddUserClientData(userClientRelationDTO);
                                    if (SizeData != null && SizeData.Count > 0)
                                        for (int i = 0; i < SizeData.Count; i++)
                                        {
                                            userClientRelationDTO = new UserClientRelationDTO();
                                            userClientRelationDTO.UCM_UserID = Convert.ToInt64(userMaster.User_pkeyID);
                                            userClientRelationDTO.UCM_ClientID = SizeData[i].Client_pkeyID;
                                            userClientRelationDTO.Type = 1;
                                            userClientRelationDTO.UCM_IsActive = true;
                                            userClientRelationData.AddUserClientData(userClientRelationDTO);

                                        }
                                }

                                if (model.UserDocumentArray != null)
                                {
                                    UserDocumentDTO userDocumentDTO = new UserDocumentDTO();
                                    UserDocumentData userDocumentData = new UserDocumentData();

                                    for (int i = 0; i < model.UserDocumentArray.Count; i++)
                                    {
                                        //userDocumentDTO.Type = model.Type;
                                        userDocumentDTO.UserID = model.UserDocumentArray[i].UserID;
                                        userDocumentDTO.User_Doc_AlertUser = model.UserDocumentArray[i].User_Doc_AlertUser;
                                        userDocumentDTO.User_Doc_DocPath = model.UserDocumentArray[i].User_Doc_DocPath;
                                        userDocumentDTO.User_Doc_Exp_Date = model.UserDocumentArray[i].User_Doc_Exp_Date;
                                        userDocumentDTO.User_Doc_FileName = model.UserDocumentArray[i].User_Doc_FileName;
                                        userDocumentDTO.User_Doc_IsActive = model.UserDocumentArray[i].User_Doc_IsActive;
                                        userDocumentDTO.User_Doc_IsDelete = model.UserDocumentArray[i].User_Doc_IsDelete;
                                        userDocumentDTO.User_Doc_NotificationDate = model.UserDocumentArray[i].User_Doc_NotificationDate;
                                        userDocumentDTO.User_Doc_RecievedDate = model.UserDocumentArray[i].User_Doc_RecievedDate;
                                        userDocumentDTO.User_Doc_ValType = model.UserDocumentArray[i].User_Doc_ValType;

                                        userDocumentDTO.User_Doc_UserID = Convert.ToInt64(userMaster.User_pkeyID);
                                        userDocumentDTO.User_Doc_pkeyID = model.UserDocumentArray[i].User_Doc_pkeyID;
                                        if (model.UserDocumentArray[i].User_Doc_pkeyID != 0)
                                        {
                                            userDocumentDTO.Type = 2;
                                        }
                                        else
                                        {
                                            userDocumentDTO.Type = 1;

                                        }

                                        userDocumentData.AddUserDocumentData(userDocumentDTO);

                                    }
                                }
                                break;
                            }
                    }
                }
                //if (model.Type != 8 )
                //{
                // GeneratePasswordLink(model, 1);
                // }

                objuserData.Add(userMaster);
                if (model.Type == 1)
                {
                    GeneratePasswordLink(model, 1);
                }

 
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objuserData;

        }

        public int GeneratePasswordLink(UserMasterDTO model,int Type)
        {
            int result = 0;
            try
            {
                //pradeep
                EmailData emailData = new EmailData();
                EmailFormatData emailFormatData = new EmailFormatData();
                EmailFormatDTO emailFormatDTO = new EmailFormatDTO();

                SecurityHelper securityHelper = new SecurityHelper();
                OTPGenerator oTPGenerator = new OTPGenerator();
                UserVerificationMaster_DTO userVerificationMaster_DTO = new UserVerificationMaster_DTO();

                int verificationCode = oTPGenerator.GenerateRandomNo();
                string encriptveryCode = securityHelper.Encrypt(verificationCode.ToString(), false);
                string encripUid = securityHelper.Encrypt(model.User_pkeyID.ToString(), false);
                userVerificationMaster_DTO.UserVerificationID = encripUid;
                userVerificationMaster_DTO.VerificationCode = encriptveryCode;
                userVerificationMaster_DTO.UserID = model.User_pkeyID; //model.UserID;
                userVerificationMaster_DTO.VUserId = model.User_pkeyID;
                userVerificationMaster_DTO.Verification_IsActive = true;
                userVerificationMaster_DTO.Verification_IsDelete = false;
                userVerificationMaster_DTO.Type = 1;
                userVerificationMaster_Data.AddUserVerificationMaster_Data(userVerificationMaster_DTO);

                //Send Email
                string sendEmailUrl= ConfigurationManager.AppSettings["sendEmailUrl"] != null ? ConfigurationManager.AppSettings["sendEmailUrl"].ToString() : null;

                string VerificationLink = sendEmailUrl + "/admin/change-password?ipluid=" + WebUtility.UrlEncode(encripUid) + "&iplcode=" + WebUtility.UrlEncode(encriptveryCode) + "&scid="+ model.User_Source;


                AuthLoginDTO authLoginDTO = new AuthLoginDTO();
                authLoginDTO.User_Email = model.User_Email;
                authLoginDTO.User_LoginName = model.User_LoginName;
                authLoginDTO.User_Password = model.User_Password;

                string Fullname = model.User_FirstName + " " + model.User_LastName;
                string mes = emailFormatData.GetEmailMessageText(VerificationLink, Fullname, model.User_Email, Type);
                emailData.SendEmail(authLoginDTO, mes, Type);
                result = 1;
                return result;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return result;
            }

        }

        public List<dynamic> AddUserMasterdetails(UserMasterDTO model)
        {
            List<dynamic> objuserData = new List<dynamic>();

            try
            {

                objuserData = AddupdateUserMasterdetails(model);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);

            }
            return objuserData;
        }

        public List<dynamic> AddUsersubcontractorMasterdetails(UserMasterDTO model)
        {
            List<dynamic> objuserData = new List<dynamic>();

            try
            {

                objuserData = AddupdateUsersubcontractorMasterdetails(model);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);

            }
            return objuserData;
        }

        private List<dynamic> AddupdateUsersubcontractorMasterdetails(UserMasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objuserData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_User_Sub-Contractor_Master]";
            UserMaster userMaster = new UserMaster();

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@User_pkeyID", 1 + "#bigint#" + model.User_pkeyID);
                input_parameters.Add("@User_SubContractor", 1 + "#bigint#" + model.User_SubContractor);
                input_parameters.Add("@User_FirstName", 1 + "#nvarchar#" + model.User_Sub_FirstName);
                input_parameters.Add("@User_LastName", 1 + "#nvarchar#" + model.User_Sub_LastName);
                input_parameters.Add("@User_Address", 1 + "#nvarchar#" + model.User_Sub_Address);
                input_parameters.Add("@User_City", 1 + "#nvarchar#" + model.User_Sub_City);
                input_parameters.Add("@User_State", 1 + "#int#" + model.User_Sub_State);
                input_parameters.Add("@User_County", 1 + "#int#" + model.User_Sub_County);
                input_parameters.Add("@User_Zip", 1 + "#bigint#" + model.User_Sub_Zip);
                input_parameters.Add("@User_CellNumber", 1 + "#nvarchar#" + model.User_Sub_CellNumber);
                input_parameters.Add("@User_LoginName", 1 + "#nvarchar#" + model.User_Sub_LoginName);
                input_parameters.Add("@User_Password", 1 + "#nvarchar#" + model.User_Sub_Password);
                input_parameters.Add("@User_Email", 1 + "#nvarchar#" + model.User_Sub_Email);
                input_parameters.Add("@User_Group", 1 + "#int#" + model.User_Sub_GroupID);
                input_parameters.Add("@User_Tracking", 1 + "#bit#" + model.User_Tracking);
                input_parameters.Add("@User_Tracking_Time", 1 + "#int#" + model.User_Tracking_Time);



                input_parameters.Add("@User_IsActive", 1 + "#bit#" + model.User_Sub_IsActive);
                input_parameters.Add("@User_IsDelete", 1 + "#bit#" + model.User_Sub_Delete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@User_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objuserData = obj.SqlCRUD(insertProcedure, input_parameters);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objuserData;

        }


        public List<dynamic> GetUsersubcontractorMasterdetails(UserMasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {

                DataSet ds = GetUsersubcontractordetails(model);
                if (ds.Tables.Count > 0)
                {
                    for (int i = 0; i < ds.Tables.Count; i++)
                    {
                        objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                    }

                }

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objDynamic;
        }


        private DataSet GetUsersubcontractordetails(UserMasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_User_Sub-Contractor]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();
                input_parameters.Add("@User_pkeyID", 1 + "#bigint#" + model.User_pkeyID);
                input_parameters.Add("@User_SubContractor", 1 + "#bigint#" + model.User_SubContractor);
                input_parameters.Add("@WhereClause", 1 + "#varchar#" + model.WhereClause);
                input_parameters.Add("@Pagenumber", 1 + "#int#" + model.Pagenumber);
                input_parameters.Add("@Rownumber", 1 + "#int#" + model.Rownumber);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
               

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return ds;
        }



        public List<dynamic> UpdateBackgrounduserMasterdetails(UserMasterDTO model)
        {
            List<dynamic> objuserData = new List<dynamic>();
            UserMasterDTO userMasterDTO = new UserMasterDTO();
            try
            {

                userMasterDTO.User_pkeyID = model.User_pkeyID;
                userMasterDTO.User_BackgroundCheckProvider = model.User_BackgroundCheckProvider;
                userMasterDTO.User_BackgroundDocPath = model.User_BackgroundDocPath;
                userMasterDTO.User_BackgroundCheckId = model.User_BackgroundCheckId;
                userMasterDTO.User_BackgroundDocName = model.User_BackgroundDocName;

                if (userMasterDTO.User_pkeyID != 0)
                {
                    userMasterDTO.Type = 5;
                }

                objuserData = AddupdateUserMasterdetails(userMasterDTO);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);

            }
            return objuserData;
        }


        public List<dynamic> UpdateProfile(ProfileUserMaster model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                var config = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<ProfileUserMaster, UserMasterDTO>();
                });

                IMapper mapper = config.CreateMapper();
                var dest = mapper.Map<ProfileUserMaster, UserMasterDTO>(model);
                objData = AddUserMasterdetails(dest);

            }
            catch (Exception ex)
            {

                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

            }

            return objData;
        }

        public List<IPLState_MobDTO> GetStateForMobile(DataTable dt)
        {
            List<IPLState_MobDTO> IPLState_MobDTO = new List<IPLState_MobDTO>();
            try
            {
                var mystate = dt.AsEnumerable();
                IPLState_MobDTO =
                 (from item in mystate
                  select new IPLState_MobDTO
                  {
                      IPL_StateID = item.Field<Int64>("IPL_StateID"),
                      IPL_StateName = item.Field<String>("IPL_StateName"),
                  }).ToList();
            }
            catch (Exception ex)
            {

                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

            }
            return IPLState_MobDTO;
        }
        public List<dynamic> GetUserProfile(ProfileUserMaster model)
        {
            List<dynamic> objData = new List<dynamic>();
            UserMasterDTO userMasterDTO = new UserMasterDTO();
            try
            {
                userMasterDTO.UserID = model.UserID;
                userMasterDTO.User_pkeyID = model.User_pkeyID;
                userMasterDTO.Type = 5;
                DataSet ds = GetUserData(userMasterDTO);
                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    objData.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                }
                #region comment
                //var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                //List<ProfileUserMaster> UserDetails =
                //  (from item in myEnumerableFeaprd
                //   select new ProfileUserMaster
                //   {
                //       User_pkeyID = item.Field<Int64>("User_pkeyID"),
                //       User_FirstName = item.Field<String>("User_FirstName"),
                //       User_LastName = item.Field<String>("User_LastName"),

                //       User_CellNumber = item.Field<String>("User_CellNumber"),
                //       User_CompanyName = item.Field<String>("User_CompanyName"),

                //       User_LoginName = item.Field<String>("User_LoginName"),
                //       User_Password = item.Field<String>("User_Password"),
                //       User_Email = item.Field<String>("User_Email"),
                //       User_Address = item.Field<String>("User_Address"),
                //       User_City = item.Field<String>("User_City"),
                //       User_Zip = item.Field<Int64?>("User_Zip"),
                //       User_State_strval = item.Field<Int64?>("User_State_strval"),
                //       User_ImagePath = item.Field<String>("User_ImagePath"),
                //       IPLState_Mob_Data = GetStateForMobile(ds.Tables[1])

                //   }).ToList();


                //objData.Add(UserDetails);
                #endregion


            }
            catch (Exception ex)
            {

                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                log.logErrorMessage("Profile Error-------->" + model.User_pkeyID);
            }

            return objData;
        }

        private DataSet GetUserData(UserMasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_User_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@User_pkeyID", 1 + "#bigint#" + model.User_pkeyID);
                input_parameters.Add("@WhereClause", 1 + "#varchar#" + null);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.User_pkeyID);
                input_parameters.Add("@MenuID", 1 + "#bigint#" + 0);
                input_parameters.Add("@FilterData", 1 + "#varchar#" + null);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetUserDetails(UserMasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetUserData(model);

                if (model.Type == 9)
                {
                    for (int i = 0; i < ds.Tables.Count; i++)
                    {
                        obj.AsDynamicEnumerable(ds.Tables[i]);
                    }
                   
                }
                else
                {
                    var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                    List<UserMasterDTO> UserDetails =
                       (from item in myEnumerableFeaprd
                        select new UserMasterDTO
                        {
                            User_pkeyID = item.Field<Int64>("User_pkeyID"),
                            User_FirstName = item.Field<String>("User_FirstName"),
                            User_LastName = item.Field<String>("User_LastName"),
                            User_Address = item.Field<String>("User_Address"),
                            User_City = item.Field<String>("User_City"),
                            User_State = item.Field<int?>("User_State"),
                            User_Zip = item.Field<Int64?>("User_Zip"),
                            User_CellNumber = item.Field<String>("User_CellNumber"),
                            User_CompanyName = item.Field<String>("User_CompanyName"),
                            User_Sys_Record = item.Field<int?>("User_Sys_Record"),
                            User_LoginName = item.Field<String>("User_LoginName"),
                            User_Password = item.Field<String>("User_Password"),
                            User_Email = item.Field<String>("User_Email"),
                            User_Group = item.Field<int?>("User_Group"),
                            User_Contractor = item.Field<Boolean?>("User_Contractor"),
                            User_Cordinator = item.Field<Boolean?>("User_Cordinator"),
                            User_Processor = item.Field<Boolean?>("User_Processor"),
                            User_OpenOrderDisCriteria = item.Field<String>("User_OpenOrderDisCriteria"),
                            User_PastWorkOrder = item.Field<Boolean?>("User_PastWorkOrder"),
                            User_PastOrderDisCriteria = item.Field<String>("User_PastOrderDisCriteria"),
                            User_SelectOrderDisCriteria = item.Field<String>("User_SelectOrderDisCriteria"),
                            User_BackgroundCheckProvider = item.Field<String>("User_BackgroundCheckProvider"),
                            User_BackgroundCheckId = item.Field<String>("User_BackgroundCheckId"),
                            User_BackgroundDocPath = item.Field<String>("User_BackgroundDocPath"),
                            User_BackgroundDocName = item.Field<String>("User_BackgroundDocName"),
                            User_Assi_Admin = item.Field<Boolean?>("User_Assi_Admin"),
                            User_Active = item.Field<int?>("User_Active"),
                            User_WorkOrder = item.Field<int?>("User_WorkOrder"),
                            User_Wo_History = item.Field<int?>("User_Wo_History"),
                            User_Disc_percentage = item.Field<Decimal?>("User_Disc_percentage"),
                            User_Tme_Zone = item.Field<int?>("User_Tme_Zone"),
                            User_Auto_Assign = item.Field<Boolean?>("User_Auto_Assign"),
                            User_Leg_FirstName = item.Field<String>("User_Leg_FirstName"),
                            User_Leg_LastName = item.Field<String>("User_Leg_LastName"),
                            User_Leg_CellPhone = item.Field<String>("User_Leg_CellPhone"),
                            User_Leg_Address = item.Field<String>("User_Leg_Address"),
                            User_Leg_Address1 = item.Field<String>("User_Leg_Address1"),
                            User_Leg_City = item.Field<String>("User_Leg_City"),
                            User_Leg_State = item.Field<int?>("User_Leg_State"),
                            User_Leg_Notes = item.Field<String>("User_Leg_Notes"),
                            User_Email_Note = item.Field<Boolean?>("User_Email_Note"),
                            User_Emai_Reminders = item.Field<Boolean?>("User_Emai_Reminders"),
                            User_Email_New_Wo = item.Field<Boolean?>("User_Email_New_Wo"),
                            User_Email_UnAssigned_Wo = item.Field<Boolean?>("User_Email_UnAssigned_Wo"),
                            User_Email_FollowUp = item.Field<Boolean?>("User_Email_FollowUp"),
                            User_Text_Note = item.Field<Boolean?>("User_Text_Note"),
                            User_Text_Reminders = item.Field<Boolean?>("User_Text_Reminders"),
                            User_Text_New_Wo = item.Field<Boolean?>("User_Text_New_Wo"),
                            User_Text_UnAssigned_Wo = item.Field<Boolean?>("User_Text_UnAssigned_Wo"),
                            User_Text_FollowUp = item.Field<Boolean?>("User_Text_FollowUp"),
                            User_Alert_EmailReply = item.Field<Boolean?>("User_Alert_EmailReply"),
                            User_Alert_Ready_Office = item.Field<Boolean?>("User_Alert_Ready_Office"),
                            User_Misc_Contractor_Score = item.Field<Int64?>("User_Misc_Contractor_Score"),
                            User_Misc_Insurance_Expire = item.Field<String>("User_Misc_Insurance_Expire"),
                            User_Misc_Pruvan_Username = item.Field<String>("User_Misc_Pruvan_Username"),
                            User_Misc_PushKey = item.Field<String>("User_Misc_PushKey"),
                            User_Misc_StartDate = item.Field<int?>("User_Misc_StartDate"),
                            User_Misc_Device_Id = item.Field<String>("User_Misc_Device_Id"),
                            User_Misc_ABC = item.Field<String>("User_Misc_ABC"),
                            User_Misc_Service_Id = item.Field<String>("User_Misc_Service_Id"),

                            User_Email_Cancelled = item.Field<Boolean?>("User_Email_Cancelled"),
                            User_Email_New_Message = item.Field<Boolean?>("User_Email_New_Message"),
                            User_Email_Field_Complete = item.Field<Boolean?>("User_Email_Field_Complete"),
                            User_Email_Daily_Digest = item.Field<Boolean?>("User_Email_Daily_Digest"),
                            User_Text_Cancelled = item.Field<Boolean?>("User_Text_Cancelled"),
                            User_Text_New_Message = item.Field<Boolean?>("User_Text_New_Message"),
                            User_Text_Field_Complete = item.Field<Boolean?>("User_Text_Field_Complete"),

                            User_AssignClient = item.Field<String>("User_AssignClient"),
                            User_IsActive = item.Field<Boolean?>("User_IsActive"),
                            User_IsDelete = item.Field<Boolean?>("User_IsDelete"),

                            User_Comments = item.Field<String>("User_Comments"),
                            User_Con_Cat_Id = item.Field<Int64?>("User_Con_Cat_Id"),
                            User_Tracking = item.Field<Boolean?>("User_Tracking"),
                            User_Tracking_Time = item.Field<int?>("User_Tracking_Time"),
                            User_BusinessID_Social_No = item.Field<String>("User_BusinessID_Social_No"),
                            User_Track_Payment = item.Field<Boolean?>("User_Track_Payment"),
                            User_Default_Expence_Account_Id = item.Field<String>("User_Default_Expence_Account_Id"),
                            User_Source = item.Field<int?>("User_Source"),
                            User_CreatedBy = item.Field<String>("User_CreatedBy"),
                            User_ModifiedBy = item.Field<String>("User_ModifiedBy"),
                            User_ImagePath = item.Field<String>("User_ImagePath"),
                            Group_DR_PkeyID = item.Field<Int64>("Group_DR_PkeyID"),

                        }).ToList();

                    objDynamic.Add(UserDetails);

                    if (ds.Tables.Count > 1)
                    {
                        var myEnumerableFeaprd1 = ds.Tables[1].AsEnumerable();
                        List<UserAddressDTO> UserAddressDetails =
                           (from item in myEnumerableFeaprd1
                            select new UserAddressDTO
                            {
                                IPL_PkeyID = item.Field<Int64>("IPL_PkeyID"),
                                IPL_Primary_radius = item.Field<String>("IPL_Primary_radius"),
                                IPL_Primary_Zip_Code = item.Field<Int64?>("IPL_Primary_Zip_Code"),
                                IPL_Primary_Countries = item.Field<Int64?>("IPL_Primary_Countries"),
                                IPL_Primary_Latitude = item.Field<String>("IPL_Primary_Latitude"),
                                IPL_Primary_Longitude = item.Field<String>("IPL_Primary_Longitude"),
                                IPL_Secondary_radius = item.Field<String>("IPL_Secondary_radius"),
                                IPL_Secondary_Zip_Codes = item.Field<Int64?>("IPL_Secondary_Zip_Codes"),
                                IPL_Secondary_Countries = item.Field<Int64?>("IPL_Secondary_Countries"),
                                IPL_Secondary_Latitude = item.Field<String>("IPL_Secondary_Latitude"),
                                IPL_Secondary_Longitude = item.Field<String>("IPL_Secondary_Longitude"),
                                IPL_UserID = item.Field<Int64?>("IPL_UserID"),
                                IPL_Address = item.Field<String>("IPL_Address"),
                                IPL_City = item.Field<String>("IPL_City"),
                                IPL_CreatedBy = item.Field<String>("IPL_CreatedBy"),
                                IPL_ModifiedBy = item.Field<String>("IPL_ModifiedBy"),
                                IPL_State = item.Field<Int64?>("IPL_State"),
                                IPL_County = item.Field<Int64?>("IPL_County"),
                                IPL_IsActive = item.Field<Boolean?>("IPL_IsActive")

                            }).ToList();

                        objDynamic.Add(UserAddressDetails);
                    }
                    if (ds.Tables.Count > 2)
                    {
                        var myEnumerableFeaprd1 = ds.Tables[2].AsEnumerable();
                        List<ContractorCoverageAreaDTO> conAddressDetails =
                           (from item in myEnumerableFeaprd1
                            select new ContractorCoverageAreaDTO
                            {
                                Cont_Coverage_Area_PkeyId = item.Field<Int64>("Cont_Coverage_Area_PkeyId"),
                                Cont_Coverage_Area_UserID = item.Field<Int64?>("Cont_Coverage_Area_UserID"),
                                Cont_Coverage_Area_State_Id = item.Field<Int64?>("Cont_Coverage_Area_State_Id"),
                                Cont_Coverage_Area_County_Id = item.Field<Int64?>("Cont_Coverage_Area_County_Id"),
                                Cont_Coverage_Area_Zip_Code = item.Field<Int64?>("Cont_Coverage_Area_Zip_Code"),
                                Cont_Coverage_Area_IsActive = item.Field<Boolean?>("Cont_Coverage_Area_IsActive"),
                                COUNTY = item.Field<String>("COUNTY"),
                                STATE_CODE = item.Field<String>("STATE_CODE"),
                                Cont_Coverage_Area_CreatedBy = item.Field<String>("Cont_Coverage_Area_CreatedBy"),
                                Cont_Coverage_Area_ModifiedBy = item.Field<String>("Cont_Coverage_Area_ModifiedBy")

                            }).ToList();

                        objDynamic.Add(conAddressDetails);
                    }
                    if (ds.Tables.Count > 3)
                    {
                        var myEnumerableFeaprder = ds.Tables[3].AsEnumerable();
                        List<User_Access_log_DeviceDTO> useraccesslog =
                           (from item in myEnumerableFeaprder
                            select new User_Access_log_DeviceDTO
                            {
                                User_Acc_Log_PkeyId = item.Field<Int64>("User_Acc_Log_PkeyId"),
                                User_Acc_Log_Logged_In = item.Field<DateTime?>("User_Acc_Log_Logged_In"),
                                User_Acc_Log_Logged_Out = item.Field<DateTime?>("User_Acc_Log_Logged_Out"),
                                User_Acc_Log_Device_Name = item.Field<String>("User_Acc_Log_Device_Name"),
                                User_Acc_Log_GpsLatitude = item.Field<String>("User_Acc_Log_GpsLatitude"),
                                User_Acc_Log_GpsLongitude = item.Field<String>("User_Acc_Log_GpsLongitude"),
                                User_Acc_Log_Event = item.Field<String>("User_Acc_Log_Event"),
                                User_Acc_Log_CreatedBy = item.Field<String>("User_Acc_Log_CreatedBy"),
                                User_Acc_Log_ModifiedBy = item.Field<String>("User_Acc_Log_ModifiedBy"),

                            }).ToList();

                        //objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[3]));
                        objDynamic.Add(useraccesslog);

                    }
                }
                

                #region comment
                //if (ds.Tables.Count > 3)
                //{
                //    var myEnumerableFeaprder = ds.Tables[3].AsEnumerable();
                //    List<User_Access_log_DeviceDTO> useraccesslog =
                //       (from item in myEnumerableFeaprder
                //        select new User_Access_log_DeviceDTO
                //        {
                //            User_Acc_Log_PkeyId = item.Field<Int64>("User_Acc_Log_PkeyId"),
                //            User_Acc_Log_Logged_In = item.Field<DateTime?>("User_Acc_Log_Logged_In"),
                //            User_Acc_Log_Logged_Out = item.Field<DateTime?>("User_Acc_Log_Logged_Out"),
                //            User_Acc_Log_UserName = item.Field<String>("User_Acc_Log_UserName"),
                //            User_Acc_Log_Device_Name = item.Field<String>("User_Acc_Log_Device_Name"),
                //            User_Acc_Log_CreatedBy = item.Field<String>("User_Acc_Log_CreatedBy"),
                //            User_Acc_Log_ModifiedBy = item.Field<String>("User_Acc_Log_ModifiedBy"),

                //        }).ToList();

                //    objDynamic.Add(useraccesslog);
                //}
                #endregion

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }






        // get filter Data user master
        private DataSet GetUserfilterData(UserFiltermasterDTO model, SearchMasterDTO searchmodel)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_User_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@User_pkeyID", 1 + "#bigint#" + model.User_pkeyID);
                if (model.Type == 6 || model.Type == 8 || model.Type == 9)
                {
                    input_parameters.Add("@WhereClause", 1 + "#varchar#" + null);
                    input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                    input_parameters.Add("@MenuID", 1 + "#bigint#" + null);
                    input_parameters.Add("@FilterData", 1 + "#varchar#" + null);
                
                }
                else
                {
                    if (model.SearchMaster != null)
                    {
                        input_parameters.Add("@WhereClause", 1 + "#varchar#" + model.SearchMaster.WhereClause);
                        input_parameters.Add("@UserID", 1 + "#bigint#" + model.SearchMaster.UserID);
                        input_parameters.Add("@MenuID", 1 + "#bigint#" + model.SearchMaster.MenuID);
                        input_parameters.Add("@FilterData", 1 + "#varchar#" + model.SearchMaster.FilterData);
                    }
                    else
                    {
                        input_parameters.Add("@WhereClause", 1 + "#varchar#" + searchmodel.WhereClause);
                        input_parameters.Add("@UserID", 1 + "#bigint#" + searchmodel.UserID);
                        input_parameters.Add("@MenuID", 1 + "#bigint#" + searchmodel.MenuID);
                        input_parameters.Add("@FilterData", 1 + "#varchar#" + searchmodel.FilterData);
                    }
                }




                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetUserMasterFilterDetails(UserFiltermasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            string wherecondition = string.Empty;
            var Data = JsonConvert.DeserializeObject<UserFiltermasterDTO>(model.FilterData);
            try
            {

                if (!string.IsNullOrEmpty(Data.User_FirstName))
                {
                    wherecondition = " And Usr.User_FirstName LIKE '%" + Data.User_FirstName + "%'";
                }

                if (!string.IsNullOrEmpty(Data.User_LastName))
                {
                    wherecondition = wherecondition + "  And Usr.User_LastName LIKE '%" + Data.User_LastName + "%'";
                }

                if (!string.IsNullOrEmpty(Data.User_Address))
                {
                    wherecondition = wherecondition + "  And Usr.User_Address LIKE '%" + Data.User_Address + "%'";
                }

                if (!string.IsNullOrEmpty(Data.User_City))
                {
                    wherecondition = wherecondition + "  And Usr.User_City LIKE '%" + Data.User_City + "%'";
                }

                if (Data.User_State != 0 && Data.User_State != null)
                {
                    wherecondition = wherecondition + "  And Usr.User_State LIKE '%" + Data.User_State + "%'";
                }
                if (Data.User_Zip != 0 && Data.User_Zip != null)
                {
                    wherecondition = wherecondition + "  And Usr.User_Zip LIKE '%" + Data.User_Zip + "%'";
                }
                if (!string.IsNullOrEmpty(Data.User_CellNumber))
                {
                    wherecondition = wherecondition + "  And Usr.User_CellNumber LIKE '%" + Data.User_CellNumber + "%'";
                }
                if (!string.IsNullOrEmpty(Data.User_CompanyName))
                {
                    wherecondition = wherecondition + "  And Usr.User_CompanyName LIKE '%" + Data.User_CompanyName + "%'";
                }
                if (!string.IsNullOrEmpty(Data.User_LoginName))
                {
                    wherecondition = wherecondition + "  And Usr.User_LoginName LIKE '%" + Data.User_LoginName + "%'";
                }
                if (!string.IsNullOrEmpty(Data.User_Email))
                {
                    wherecondition = wherecondition + "  And Usr.User_Email LIKE '%" + Data.User_Email + "%'";
                }
                if (Data.User_Group != 0 && Data.User_Group != null)
                {
                    wherecondition = wherecondition + "  And Usr.User_Group LIKE '%" + Data.User_Group + "%'";
                }
                if (Data.User_Misc_Contractor_Score != 0 && Data.User_Misc_Contractor_Score != null)
                {
                    wherecondition = wherecondition + "  And Usr.User_Misc_Contractor_Score LIKE '%" + Data.User_Misc_Contractor_Score + "%'";
                }

                if (Data.User_IsActive == true)
                {
                    wherecondition = wherecondition + "  And Usr.User_IsActive =  1";
                }
                if (Data.User_IsActive == false)
                {
                    wherecondition = wherecondition + "  And Usr.User_IsActive =  0";
                }
                UserFiltermasterDTO userFiltermasterDTO = new UserFiltermasterDTO();
                SearchMasterDTO searchMasterDTO = new SearchMasterDTO();
                userFiltermasterDTO.Type = 4;
                searchMasterDTO.MenuID = model.SearchMaster.MenuID;
                searchMasterDTO.UserID = model.SearchMaster.UserID;
                searchMasterDTO.WhereClause = wherecondition;
                DataSet ds = GetUserfilterData(userFiltermasterDTO, searchMasterDTO);

                if (ds.Tables.Count > 0)
                {
                    var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                    List<UserFiltermasterDTO> UserlstDetails =
                       (from item in myEnumerableFeaprd
                        select new UserFiltermasterDTO
                        {
                            User_pkeyID = item.Field<Int64>("User_pkeyID"),
                            User_FirstName = item.Field<String>("User_FirstName"),
                            User_LastName = item.Field<String>("User_LastName"),
                            User_Address = item.Field<String>("User_Address"),
                            User_City = item.Field<String>("User_City"),
                            User_State = item.Field<int?>("User_State"),
                            User_Zip = item.Field<Int64?>("User_Zip"),
                            User_CellNumber = item.Field<String>("User_CellNumber"),
                            User_CompanyName = item.Field<String>("User_CompanyName"),
                            User_LoginName = item.Field<String>("User_LoginName"),
                            User_Email = item.Field<String>("User_Email"),
                            //  User_Group = item.Field<int?>("User_Group"),
                            User_Group_Name = item.Field<String>("User_Group_Name"),
                            User_Misc_Contractor_Score = item.Field<Int64?>("User_Misc_Contractor_Score"),
                            User_IsActive = item.Field<Boolean?>("User_IsActive"),
                            User_BusinessID_Social_No = item.Field<String>("User_BusinessID_Social_No"),
                            User_Track_Payment = item.Field<Boolean?>("User_Track_Payment"),
                            User_Default_Expence_Account_Id = item.Field<String>("User_Default_Expence_Account_Id"),
                            User_CreatedBy = item.Field<String>("User_CreatedBy"),
                            User_ModifiedBy = item.Field<String>("User_ModifiedBy"),

                        }).ToList();

                    objDynamic.Add(UserlstDetails);
                }





            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objDynamic;
        }

        public List<dynamic> GetUserfilterDetails(UserFiltermasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetUserfilterData(model, null);
                if (ds.Tables.Count > 0)
                {
                    var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                    var dsta = myEnumerableFeaprd.FirstOrDefault();
                    List<UserFiltermasterDTO> UserfilterDetails =
                       (from item in myEnumerableFeaprd
                        select new UserFiltermasterDTO
                        {
                            User_pkeyID = item.Field<Int64>("User_pkeyID"),
                            User_FirstName = item.Field<String>("User_FirstName"),
                            User_LastName = item.Field<String>("User_LastName"),
                            User_Address = item.Field<String>("User_Address"),
                            User_City = item.Field<String>("User_City"),
                            User_State = item.Field<int?>("User_State"),
                            User_Zip = item.Field<Int64?>("User_Zip"),
                            User_CellNumber = item.Field<String>("User_CellNumber"),
                            User_CompanyName = item.Field<String>("User_CompanyName"),
                            User_LoginName = item.Field<String>("User_LoginName"),
                            User_Email = item.Field<String>("User_Email"),
                            //  User_Group = item.Field<int?>("User_Group"),
                            User_Group_Name = item.Field<String>("User_Group"),
                            User_Misc_Contractor_Score = item.Field<Int64?>("User_Misc_Contractor_Score"),
                            User_IsActive = item.Field<Boolean?>("User_IsActive"),
                            User_BusinessID_Social_No = item.Field<String>("User_BusinessID_Social_No"),
                            User_Track_Payment = item.Field<Boolean?>("User_Track_Payment"),
                            User_Default_Expence_Account_Id = item.Field<String>("User_Default_Expence_Account_Id"),
                            User_CreatedBy = item.Field<String>("User_CreatedBy"),
                            User_ModifiedBy = item.Field<String>("User_ModifiedBy"),
                            

                            ViewUrl = null
                        }).ToList();

                    objDynamic.Add(UserfilterDetails);
                }


                if (ds.Tables.Count > 1)
                {
                    var myEnumerable = ds.Tables[1].AsEnumerable();
                    List<UserFilterDTO> UserFilterDetails =
                       (from item in myEnumerable
                        select new UserFilterDTO
                        {
                            //usr_filter_pkeyID = item.Field<Int64>("usr_filter_pkeyID"),
                            usr_filter_WhereData = item.Field<String>("usr_filter_WhereData"),
                            usr_filter_FilterData = item.Field<String>("usr_filter_FilterData"),
                            //usr_filter_MenuId = item.Field<Int64?>("usr_filter_MenuId"),
                            //usr_filter_UserID = item.Field<Int64?>("usr_filter_UserID"),
                        }).ToList();

                    objDynamic.Add(UserFilterDetails);
                }
                if (model.Type == 3)
                {
                    if (ds.Tables.Count > 1)
                    {
                        var myEnumerableFeaprd = ds.Tables[2].AsEnumerable();
                        List<Filter_Admin_User_MasterDTO> Filteruser =
                           (from item in myEnumerableFeaprd
                            select new Filter_Admin_User_MasterDTO
                            {
                                User_Filter_PkeyID = item.Field<Int64>("User_Filter_PkeyID"),
                                User_Filter_First_Name = item.Field<String>("User_Filter_First_Name"),
                                User_Filter_Last_Name = item.Field<String>("User_Filter_Last_Name"),
                                User_Filter_Company = item.Field<String>("User_Filter_Company"),
                                User_Filter_Login_email = item.Field<String>("User_Filter_Login_email"),
                                User_Filter_Group = item.Field<Int64?>("User_Filter_Group"),
                                User_Filter_Mobile = item.Field<String>("User_Filter_Mobile"),
                                User_Filter_USRIsActive = item.Field<Boolean?>("User_Filter_USRIsActive"),
                                User_Filter_CreatedBy = item.Field<String>("User_Filter_CreatedBy"),
                                User_Filter_ModifiedBy = item.Field<String>("User_Filter_ModifiedBy"),
                            }).ToList();

                        objDynamic.Add(Filteruser);
                    }
                }


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        // User Document with cloud storage

        public List<dynamic> AddUserDocumentMaster(Client_Result_PhotoDTO client_Result_PhotoDTO)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {

                UserDocumentData userDocumentData = new UserDocumentData();
                UserDocumentDTO userDocumentDTO = new UserDocumentDTO();
                userDocumentDTO.User_Doc_pkeyID = client_Result_PhotoDTO.Client_Result_Photo_ID;
                userDocumentDTO.User_Doc_UserID = client_Result_PhotoDTO.Client_Result_Photo_Ch_ID;
                userDocumentDTO.User_Doc_ValType = client_Result_PhotoDTO.Client_Result_File_Desc;
                if (!string.IsNullOrEmpty (client_Result_PhotoDTO.User_Doc_RecievedDate))
                {
                    userDocumentDTO.User_Doc_RecievedDate = Convert.ToDateTime(client_Result_PhotoDTO.User_Doc_RecievedDate);
                }
                if (!string.IsNullOrEmpty(client_Result_PhotoDTO.User_Doc_Exp_Date))
                {
                    userDocumentDTO.User_Doc_Exp_Date = Convert.ToDateTime(client_Result_PhotoDTO.User_Doc_Exp_Date);
                }
                if (!string.IsNullOrEmpty(client_Result_PhotoDTO.Client_Result_Photo_FileType))
                {
                    userDocumentDTO.User_Doc_NotificationDate = Convert.ToDateTime(client_Result_PhotoDTO.Client_Result_Photo_FileType);
                }

                userDocumentDTO.User_Doc_DocPath = client_Result_PhotoDTO.Client_Result_Photo_FilePath;
                userDocumentDTO.User_Doc_FileName = client_Result_PhotoDTO.Client_Result_Photo_FolderName;
                userDocumentDTO.User_Doc_AlertUser = client_Result_PhotoDTO.User_Doc_AlertUser;
                userDocumentDTO.Type = 1;
                userDocumentDTO.User_Doc_IsActive = true;
                userDocumentDTO.User_Doc_IsDelete = false;

                objData = userDocumentData.AddUserDocumentData(userDocumentDTO);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        //background document with cloud
        public List<dynamic> AddBackgroundDocumentMaster(Client_Result_PhotoDTO client_Result_PhotoDTO)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                UserMasterDTO userMasterDTO = new UserMasterDTO();


                userMasterDTO.User_pkeyID = client_Result_PhotoDTO.Client_Result_Photo_ID;
                userMasterDTO.User_BackgroundCheckProvider = client_Result_PhotoDTO.User_BackgroundCheckProvider;
                userMasterDTO.User_BackgroundDocPath = client_Result_PhotoDTO.Client_Result_Photo_FilePath;
                userMasterDTO.User_BackgroundCheckId = client_Result_PhotoDTO.Client_Result_Photo_FileType;
                userMasterDTO.User_BackgroundDocName = client_Result_PhotoDTO.Client_Result_Photo_FileName;

                if (userMasterDTO.User_pkeyID != 0)
                {
                    userMasterDTO.Type = 5;
                }
                log.logDebugMessage(" client_Result_PhotoDTO.Client_Result_Photo_ID--------->" + client_Result_PhotoDTO.Client_Result_Photo_ID);
                log.logDebugMessage(" userMasterDTO.User_pkeyID--------->" + client_Result_PhotoDTO.Client_Result_Photo_ID);
                objData = AddupdateUserMasterdetails(userMasterDTO);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        // contractor map data
        private DataSet GeContractorareadrdMaster(contractorMapDRD model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Contractor_Map_Area_DRD]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@IPL_StateID", 1 + "#bigint#" + model.IPL_StateID);
                input_parameters.Add("@Zip_state_id", 1 + "#varchar#" + model.IPL_StateName);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetContractorMapAreaDRDDetails(contractorMapDRD model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GeContractorareadrdMaster(model);
                #region comment
                //var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                //List<contractorMapDRD> stateDrdDetails =
                //   (from item in myEnumerableFeaprd
                //    select new contractorMapDRD
                //    {
                //        IPL_StateID = item.Field<Int64>("IPL_StateID"),
                //        IPL_StateName = item.Field<String>("IPL_StateName"),


                //    }).ToList();

                //objDynamic.Add(stateDrdDetails);
                #endregion

                if (ds.Tables.Count > 0)
                {
                    for (int i = 0; i < ds.Tables.Count; i++)
                    {
                        objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                    }

                }

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        private DataSet GetzipcountyfilterMaster(CountyZipChangeDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_ZipListCounty]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Zip_county_name", 1 + "#varchar#" + model.Zip_county_name);
                input_parameters.Add("@whereclause", 1 + "#varchar#" + model.whereclause);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@IPL_StateID", 1 + "#bigint#" + model.IPL_StateID);
                input_parameters.Add("@Cont_Coverage_Area_UserID", 1 + "#bigint#" + model.Cont_Coverage_Area_UserID);
                input_parameters.Add("@County_ID", 1 + "#bigint#" + model.County_ID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }
        public List<dynamic> GetContractorcountyDRDDetails(CountyZipChangeDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            string wherecondition = string.Empty;
            CountyZipChangeDTO countyZipChangeDTO = new CountyZipChangeDTO();
            try
            {
                if (!string.IsNullOrWhiteSpace(model.FilterData))
            {
                    var Data = JsonConvert.DeserializeObject<CountyZipChangeDTO>(model.FilterData);

                    if (Data.IPL_StateID != 0)
                    {
                        wherecondition = wherecondition + "  And IPL_StateID = '" + Data.IPL_StateID + "'";
                    }
                    if (!string.IsNullOrEmpty(Data.Zip_state_id))
                    {
                        wherecondition = " And Zip_state_id =    '" + Data.Zip_state_id + "'";
                    }

                    if (!string.IsNullOrEmpty(Data.Zip_county_name))
                    {
                        wherecondition = wherecondition + "  And Zip_county_name = '" + Data.Zip_county_name + "'";
                    }
                    countyZipChangeDTO.IPL_StateID = Data.IPL_StateID;

                }



                countyZipChangeDTO.IPL_StateID =model.IPL_StateID;
                //countyZipChangeDTO.Type = 1;
                countyZipChangeDTO.Type = model.Type;
                
                countyZipChangeDTO.UserID = model.UserID;
                countyZipChangeDTO.Cont_Coverage_Area_UserID = model.Cont_Coverage_Area_UserID;
                countyZipChangeDTO.County_ID = model.County_ID;
                countyZipChangeDTO.whereclause = wherecondition;
                DataSet ds = GetzipcountyfilterMaster(countyZipChangeDTO);
                if (ds.Tables.Count > 0)
                {
                    for (int i = 0; i < ds.Tables.Count; i++)
                    {
                        objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                    }

                }

                #region comment
                //var myEnumerableFeaprdx = ds.Tables[0].AsEnumerable();
                //List<ContCoverageAreaCountyId> county =
                //   (from item in myEnumerableFeaprdx
                //    select new ContCoverageAreaCountyId
                //    {
                //        ID = item.Field<Int64>("ID"),
                //        COUNTY = item.Field<String>("COUNTY"),
                //    }).OrderBy(x=>x.COUNTY).ToList();

                //objDynamic.Add(county);

                //var myEnumerableFeaprds = ds.Tables[1].AsEnumerable();
                //List<CountyZipChangeDTO> countyzip =
                //   (from item in myEnumerableFeaprds
                //    select new CountyZipChangeDTO
                //    {
                //        // Zip_ID = item.Field<Int64>("Zip_ID"),
                //        Zip_zip = item.Field<Int64?>("Zip_zip"),
                //        Zip_state_id = item.Field<String>("Zip_state_id"),
                //        Zip_county_name = item.Field<String>("Zip_county_name"),
                //        Cont_Coverage_Area_IsActive = item.Field<Boolean?>("Cont_Coverage_Area_IsActive"),


                //    }).OrderBy(x => x.Zip_county_name).ToList();

                //objDynamic.Add(countyzip);
                #endregion



            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        public List<dynamic> AddContractorMapAddress(ContractorCoverageAreaDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            ContractorCoverageAreaDTO contractorCoverageAreaDTO = new ContractorCoverageAreaDTO();
            ContractorCoverageAreaData contractorCoverageAreaData = new ContractorCoverageAreaData();
            try
            {
                if (model.Type == 4)
                {
                    objDynamic = contractorCoverageAreaData.AddContractorCoverageAreaData(model);
                }
                if (model.StrAddressArray != null)
                {
                    for (int i = 0; i < model.StrAddressArray.Count; i++)
                    {
                        
                        contractorCoverageAreaDTO.Cont_Coverage_Area_State_Id = model.Cont_Coverage_Area_State_Id;
                        contractorCoverageAreaDTO.Cont_Coverage_Area_County_Id = model.Cont_Coverage_Area_County_Id;
                        contractorCoverageAreaDTO.Cont_Coverage_Area_UserAddressID = model.Cont_Coverage_Area_UserAddressID;
                        contractorCoverageAreaDTO.Cont_Coverage_Area_Zip_Code = Convert.ToInt64(model.StrAddressArray[i].Zip_zip);
                        contractorCoverageAreaDTO.Cont_Coverage_Area_IsActive = true;
                        contractorCoverageAreaDTO.Cont_Coverage_Area_IsDelete = false;
                        contractorCoverageAreaDTO.UserID = model.UserID;
                        contractorCoverageAreaDTO.Cont_Coverage_Area_UserID = model.Cont_Coverage_Area_UserID;

                        if (model.Cont_Coverage_Area_PkeyId == 0)
                        {
                            contractorCoverageAreaDTO.Type = 1;
                        }
                        else
                        {
                            contractorCoverageAreaDTO.Type = 2;
                        }


                        objDynamic = contractorCoverageAreaData.AddContractorCoverageAreaData(contractorCoverageAreaDTO);

                    }
                }
                else
                {
                    contractorCoverageAreaDTO.Cont_Coverage_Area_State_Id = model.Cont_Coverage_Area_State_Id;
                    contractorCoverageAreaDTO.Cont_Coverage_Area_County_Id = model.Cont_Coverage_Area_County_Id;
                    contractorCoverageAreaDTO.Cont_Coverage_Area_UserAddressID = model.Cont_Coverage_Area_UserAddressID;
                    contractorCoverageAreaDTO.Cont_Coverage_Area_Zip_Code = model.Cont_Coverage_Area_Zip_Code;
                    contractorCoverageAreaDTO.Cont_Coverage_Area_IsActive = true;
                    contractorCoverageAreaDTO.Cont_Coverage_Area_IsDelete = false;
                    contractorCoverageAreaDTO.UserID = model.UserID;
                    contractorCoverageAreaDTO.Cont_Coverage_Area_UserID = model.Cont_Coverage_Area_UserID;

                    if (model.Cont_Coverage_Area_PkeyId == 0)
                    {
                        contractorCoverageAreaDTO.Type = 1;
                    }
                    else
                    {
                        contractorCoverageAreaDTO.Type = 2;
                    }


                    objDynamic = contractorCoverageAreaData.AddContractorCoverageAreaData(contractorCoverageAreaDTO);
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }


            return objDynamic;
        }
        public List<dynamic> GetContractorcountyDRDDetails(Folder_Parent_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            List<ContCoverageAreaCountyId> countyList = new List<ContCoverageAreaCountyId>();
            string wherecondition = string.Empty;

            foreach (var state in model.AutoAssinArray[0].Task_sett_State)
            {
                try
                {
                    if (state.IPL_StateID != 0)
                    {
                        wherecondition = wherecondition + "  And IPL_StateID = '" + state.IPL_StateID + "'";
                    }


                    CountyZipChangeDTO countyZipChangeDTO = new CountyZipChangeDTO();

                    countyZipChangeDTO.Type = 1;
                    countyZipChangeDTO.IPL_StateID = state.IPL_StateID;
                    countyZipChangeDTO.UserID = model.UserID.GetValueOrDefault(0);

                    countyZipChangeDTO.whereclause = wherecondition;
                    DataSet ds = GetzipcountyfilterMaster(countyZipChangeDTO);

                    var myEnumerableFeaprdx = ds.Tables[0].AsEnumerable();
                    List<ContCoverageAreaCountyId> county =
                       (from item in myEnumerableFeaprdx
                        select new ContCoverageAreaCountyId
                        {
                            ID = item.Field<Int64>("ID"),
                            COUNTY = item.Field<String>("COUNTY"),
                        }).ToList();

                    countyList.AddRange(county);

                }
                catch (Exception ex)
                {
                    log.logErrorMessage(ex.StackTrace);
                    log.logErrorMessage(ex.Message);
                }
            }

            objDynamic.Add(countyList);


            return objDynamic;
        }
        // get state county for user 
        private DataSet GeUserStateMaster(UserStateDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_User_State_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();


                input_parameters.Add("@whereclause", 1 + "#varchar#" + model.whereclause);
                input_parameters.Add("@IPL_StateName", 1 + "#varchar#" + model.IPL_StateName);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@IPL_StateID", 1 + "#bigint#" + model.IPL_StateID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@CompanyId", 1 + "#bigint#" + model.CompanyId);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }
        public List<dynamic> GetUserStateDetails(UserStateDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GeUserStateMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<contractorMapDRD> stateDrdDetails =
                   (from item in myEnumerableFeaprd
                    select new contractorMapDRD
                    {
                        IPL_StateID = item.Field<Int64>("IPL_StateID"),
                        IPL_StateName = item.Field<String>("IPL_StateName"),
                        


                    }).ToList();

                objDynamic.Add(stateDrdDetails);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        public List<dynamic> GetUserUserCountyDetails(UserStateDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                model.Type = 2;

                DataSet ds = GeUserStateMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<UserStateCountDTO> stateDrdDetails =
                   (from item in myEnumerableFeaprd
                    select new UserStateCountDTO
                    {
                        ID = item.Field<Int64>("ID"),
                        COUNTY = item.Field<String>("COUNTY"),


                    }).ToList();

                objDynamic.Add(stateDrdDetails);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        public async Task AddFirebaseCompanyUserLocationAsync(LiveUserLocationFBDTO liveUserLocationFBDTO, string iplCompany, string loginName)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(iplCompany) && !string.IsNullOrWhiteSpace(loginName) && liveUserLocationFBDTO != null)
                {
                    string firebaseUrl = System.Configuration.ConfigurationManager.AppSettings["FirebaseUrl"];

                    string jsonStr = JsonConvert.SerializeObject(liveUserLocationFBDTO);
                    var firebaseClient = new FirebaseClient(firebaseUrl);
                   
                    await firebaseClient
                     .Child("locations")
                     .Child(iplCompany)
                     .Child(loginName)
                     .PutAsync(jsonStr);
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
    }
}