﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class TaskBidMasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        TaskPresetData taskPresetData = new TaskPresetData();

        public List<dynamic> AddTaskBidData(TaskBidMasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();

            string insertProcedure = "[CreateUpdateTask_Bid_Master]";
            TaskBidMaster taskBidMaster = new TaskBidMaster();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Task_Bid_pkeyID", 1 + "#bigint#" + model.Task_Bid_pkeyID);
                input_parameters.Add("@Task_Bid_TaskID", 1 + "#bigint#" + model.Task_Bid_TaskID);
                input_parameters.Add("@Task_Bid_WO_ID", 1 + "#bigint#" + model.Task_Bid_WO_ID);
                input_parameters.Add("@Task_Bid_Qty", 1 + "#varchar#" + model.Task_Bid_Qty);
                input_parameters.Add("@Task_Bid_Uom_ID", 1 + "#bigint#" + model.Task_Bid_Uom_ID);
                input_parameters.Add("@Task_Bid_Cont_Price", 1 + "#decimal#" + model.Task_Bid_Cont_Price);
                input_parameters.Add("@Task_Bid_Cont_Total", 1 + "#decimal#" + model.Task_Bid_Cont_Total);
                input_parameters.Add("@Task_Bid_Clnt_Price", 1 + "#decimal#" + model.Task_Bid_Clnt_Price);
                input_parameters.Add("@Task_Bid_Clnt_Total", 1 + "#decimal#" + model.Task_Bid_Clnt_Total);
                input_parameters.Add("@Task_Bid_Comments", 1 + "#varchar#" + model.Task_Bid_Comments);
                input_parameters.Add("@Task_Bid_Violation", 1 + "#bit#" + model.Task_Bid_Violation);
                input_parameters.Add("@Task_Bid_damage", 1 + "#bit#" + model.Task_Bid_damage);
                input_parameters.Add("@Task_Bid_IsActive", 1 + "#bit#" + model.Task_Bid_IsActive);
                input_parameters.Add("@Task_Bid_IsDelete", 1 + "#bit#" + model.Task_Bid_IsDelete);
                input_parameters.Add("@Task_Bid_Status", 1 + "#int#" + model.Task_Bid_Status);
                input_parameters.Add("@Bid_Other_Task_Name", 1 + "#varchar#" + model.Bid_Other_Task_Name);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Task_Bid_Hazards", 1 + "#bit#" + model.Task_Bid_Hazards);
                input_parameters.Add("@Task_Bid_DamageItem", 1 + "#int#" + model.Task_Bid_DamageItem);
                input_parameters.Add("@Task_Bid_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters); 
                if (objData[1] == 0)
                {
                    taskBidMaster.Task_Bid_pkeyID = "0";
                    taskBidMaster.Status = "0";
                    taskBidMaster.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    taskBidMaster.Task_Bid_pkeyID = Convert.ToString(objData[0]);
                    taskBidMaster.Status = Convert.ToString(objData[1]);


                }
                objcltData.Add(taskBidMaster);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;



        }



        public List<dynamic> Contractor_RejectTask(ContractorRejectTask model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();
            ContractorRejectTaskStatus contractorRejectTaskStatus = new ContractorRejectTaskStatus();
            string insertProcedure = "[CreateUpdate_ContractorRejectTask]";
       
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@TaskPkeyID", 1 + "#bigint#" + model.TaskPkeyID);
                input_parameters.Add("@TaskRemark", 1 + "#varchar#" + model.TaskRemark);
                input_parameters.Add("@TaskRejectBy", 1 + "#bigint#" + model.TaskRejectBy);
                input_parameters.Add("@TaskType", 1 + "#int#" + model.TaskType);
                input_parameters.Add("@Task_Bid_IsReject", 1 + "#bit#" + model.Task_Bid_IsReject);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
          
                input_parameters.Add("@pkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters); 
                if (objData[1] == 0)
                {

                    contractorRejectTaskStatus.Status = "0";
                    contractorRejectTaskStatus.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {

                    contractorRejectTaskStatus.Status = Convert.ToString(objData[1]);


                }
                objcltData.Add(contractorRejectTaskStatus);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;



        }




        private DataSet GetTaskBidMasterdata(TaskBidMasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Task_Bid_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Task_Bid_pkeyID", 1 + "#bigint#" + model.Task_Bid_pkeyID);
                input_parameters.Add("@Task_Bid_WO_ID", 1 + "#bigint#" + model.Task_Bid_WO_ID);
                input_parameters.Add("@Task_Bid_Status", 1 + "#int#" + model.Task_Bid_Status);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetTaskBidDetails(TaskBidMasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetTaskBidMasterdata(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<TaskBidMasterDTO> bidMasterDetails =
                   (from item in myEnumerableFeaprd
                    select new TaskBidMasterDTO
                    {
                        Task_Bid_pkeyID = item.Field<Int64>("Task_Bid_pkeyID"),
                        Task_Bid_TaskID = item.Field<Int64?>("Task_Bid_TaskID"),
                        Task_Bid_WO_ID = item.Field<Int64>("Task_Bid_WO_ID"),
                        Task_Bid_Qty = item.Field<String>("Task_Bid_Qty"),
                        Task_Bid_Uom_ID = item.Field<Int64?>("Task_Bid_Uom_ID"),
                        Task_Bid_Cont_Price = item.Field<Decimal?>("Task_Bid_Cont_Price"),
                        Task_Bid_Cont_Total = item.Field<Decimal?>("Task_Bid_Cont_Total"),
                        Task_Bid_Clnt_Price = item.Field<Decimal?>("Task_Bid_Clnt_Price"),
                        Task_Bid_Clnt_Total = item.Field<Decimal?>("Task_Bid_Clnt_Total"),
                        Task_Bid_Comments = item.Field<String>("Task_Bid_Comments"),
                        Task_Bid_Violation = item.Field<Boolean?>("Task_Bid_Violation"),
                        Task_Bid_damage = item.Field<Boolean?>("Task_Bid_damage"),
                        Task_Bid_IsActive = item.Field<Boolean?>("Task_Bid_IsActive"),
                        Task_Bid_Status = item.Field<int?>("Task_Bid_Status"),
                        Task_Bid_PreTextHide = false, // for text hide n show sathi pretext
                        Task_Bid_Hazards = item.Field<Boolean?>("Task_Bid_Hazards"),
                        Task_Bid_DamageItem = item.Field<int?>("Task_Bid_DamageItem"),
                        Task_Ext_pkeyID = item.Field<Int64>("Task_Ext_pkeyID"),
                        Task_Ext_BidID = item.Field<Int64?>("Task_Ext_BidID"),
                        Task_Ext_WO_ID = item.Field<Int64?>("Task_Ext_WO_ID"),
                        Task_Ext_Location = item.Field<String>("Task_Ext_Location"),
                        Task_Ext_DamageCauseId = item.Field<Int64?>("Task_Ext_DamageCauseId"),
                        Task_Ext_Length = item.Field<String>("Task_Ext_Length"),
                        Task_Ext_Width = item.Field<String>("Task_Ext_Width"),
                        Task_Ext_Height = item.Field<String>("Task_Ext_Height"),
                        Task_Ext_Men = item.Field<String>("Task_Ext_Men"),
                        Task_Ext_Hours = item.Field<String>("Task_Ext_Hours"),
                        Task_Ext_IsActive = item.Field<Boolean?>("Task_Ext_IsActive"),
                        TaskPresetDTO = taskPresetData.GetTaskPresetChildDetails(item.Field<Int64>("Task_Bid_TaskID"))
                    }).ToList();

                objDynamic.Add(bidMasterDetails);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                log.logErrorMessage("GetTaskBidDetails--------" + model.Task_Bid_WO_ID);
                log.logErrorMessage("GetTaskBidDetails--------" + model.Task_Bid_Status);

            }

            return objDynamic;
        }

        public List<dynamic> AddTaskBidExtDetail(TaskBidMasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                Task_Bid_ExtensionDTO task_Bid_ExtensionDTO = new Task_Bid_ExtensionDTO();
                task_Bid_ExtensionDTO.Task_Ext_pkeyID = model.Task_Ext_pkeyID;
                task_Bid_ExtensionDTO.Task_Ext_BidID = model.Task_Ext_BidID;
                task_Bid_ExtensionDTO.Task_Ext_WO_ID = model.Task_Ext_WO_ID;
                task_Bid_ExtensionDTO.Task_Ext_DamageCauseId = model.Task_Ext_DamageCauseId;
                task_Bid_ExtensionDTO.Task_Ext_Location = model.Task_Ext_Location;
                task_Bid_ExtensionDTO.Task_Ext_Length = model.Task_Ext_Length;
                task_Bid_ExtensionDTO.Task_Ext_Width = model.Task_Ext_Width;
                task_Bid_ExtensionDTO.Task_Ext_Height = model.Task_Ext_Height;
                task_Bid_ExtensionDTO.Task_Ext_Men = model.Task_Ext_Men;
                task_Bid_ExtensionDTO.Task_Ext_Hours = model.Task_Ext_Hours;
                task_Bid_ExtensionDTO.Task_Ext_IsActive = model.Task_Ext_IsActive;
                task_Bid_ExtensionDTO.Type = model.Type;
                task_Bid_ExtensionDTO.UserID = model.UserID;
                objDynamic = AddTaskBidExtData(task_Bid_ExtensionDTO);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        public List<dynamic> AddTaskBidExtData(Task_Bid_ExtensionDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_Task_Bid_Extension]";
            TaskBidExtension taskBidExtension = new TaskBidExtension();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Task_Ext_pkeyID", 1 + "#bigint#" + model.Task_Ext_pkeyID);
                input_parameters.Add("@Task_Ext_BidID", 1 + "#bigint#" + model.Task_Ext_BidID);
                input_parameters.Add("@Task_Ext_WO_ID", 1 + "#bigint#" + model.Task_Ext_WO_ID);
                input_parameters.Add("@Task_Ext_Location", 1 + "#varchar#" + model.Task_Ext_Location);
                input_parameters.Add("@Task_Ext_DamageCauseId", 1 + "#bigint#" + model.Task_Ext_DamageCauseId);
                input_parameters.Add("@Task_Ext_Length", 1 + "#decimal#" + model.Task_Ext_Length);
                input_parameters.Add("@Task_Ext_Width", 1 + "#decimal#" + model.Task_Ext_Width);
                input_parameters.Add("@Task_Ext_Height", 1 + "#decimal#" + model.Task_Ext_Height);
                input_parameters.Add("@Task_Ext_Men", 1 + "#decimal#" + model.Task_Ext_Men);
                input_parameters.Add("@Task_Ext_Hours", 1 + "#varchar#" + model.Task_Ext_Hours);
                input_parameters.Add("@Task_Ext_IsActive", 1 + "#bit#" + model.Task_Ext_IsActive);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Task_Ext_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);
                input_parameters.Add("@Task_Ext_CompletionID", 1 + "#bigint#" + model.Task_Ext_CompletionID);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    taskBidExtension.Task_Ext_pkeyID = "0";
                    taskBidExtension.Status = "0";
                    taskBidExtension.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    taskBidExtension.Task_Ext_pkeyID = Convert.ToString(objData[0]);
                    taskBidExtension.Status = Convert.ToString(objData[1]);


                }
                objcltData.Add(taskBidExtension);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;



        }

        public List<dynamic> AddTaskCompletionExtData(Task_Completion_ExtensionDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_Task_Completion_Extension]";
            TaskCompletionExtension taskCompletionExtension = new TaskCompletionExtension();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Task_Cmp_Ext_pkeyID", 1 + "#bigint#" + model.Task_Cmp_Ext_pkeyID);
                input_parameters.Add("@Task_Cmp_Ext_WO_ID", 1 + "#bigint#" + model.Task_Cmp_Ext_WO_ID);
                input_parameters.Add("@Task_Cmp_Ext_Location", 1 + "#nvarchar#" + model.Task_Cmp_Ext_Location);
                input_parameters.Add("@Task_Cmp_Ext_DamageCauseId", 1 + "#bigint#" + model.Task_Cmp_Ext_DamageCauseId);
                input_parameters.Add("@Task_Cmp_Ext_Length", 1 + "#varchar#" + model.Task_Cmp_Ext_Length);
                input_parameters.Add("@Task_Cmp_Ext_Width", 1 + "#varchar#" + model.Task_Cmp_Ext_Width);
                input_parameters.Add("@Task_Cmp_Ext_Height", 1 + "#varchar#" + model.Task_Cmp_Ext_Height);
                input_parameters.Add("@Task_Cmp_Ext_Men", 1 + "#varchar#" + model.Task_Cmp_Ext_Men);
                input_parameters.Add("@Task_Cmp_Ext_Hours", 1 + "#varchar#" + model.Task_Cmp_Ext_Hours);
                input_parameters.Add("@Task_Cmp_Ext_IsActive", 1 + "#bit#" + model.Task_Cmp_Ext_IsActive);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Task_Cmp_Ext_CompletionID", 1 + "#bigint#" + model.Task_Cmp_Ext_CompletionID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Task_Cmp_Ext_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);
                

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    taskCompletionExtension.Task_Cmp_Ext_pkeyID = "0";
                    taskCompletionExtension.Status = "0";
                    taskCompletionExtension.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    taskCompletionExtension.Task_Cmp_Ext_pkeyID = Convert.ToString(objData[0]);
                    taskCompletionExtension.Status = Convert.ToString(objData[1]);


                }
                objcltData.Add(taskCompletionExtension);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;



        }
    }
}