﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json;
using System.Data;
using System.Dynamic;
using Newtonsoft.Json.Linq;
using System.Text;
using iTextSharp.text.pdf;
using System.IO;
using Google.Cloud.Firestore;
using Google.Api.Gax.ResourceNames;
using Google.Apis.Auth.OAuth2;
using Google.Cloud.Storage.V1;
using System.Net.Http;
using iTextSharp.text;
using Microsoft.Ajax.Utilities;

namespace IPL.Repository.data
{
    public class FormData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        public string bucketName = System.Configuration.ConfigurationManager.AppSettings["BucketName"];
        public string jsonpath = System.Configuration.ConfigurationManager.AppSettings["GooglebucketJsom"];

        Dictionary<string, string> input_parameters = new Dictionary<string, string>();
        public async Task<List<dynamic>> Add_Form(FormsDTO model)
        {
            List<dynamic> objForm = new List<dynamic>();

            //save pcr form
            string insertProcedure = "[CreateUpdate_Forms_Master]";

            input_parameters.Add("@FormId", 1 + "#bigint#" + model.FormId);
            input_parameters.Add("@FormName", 1 + "#nvarchar#" + model.FormName);
            input_parameters.Add("@IsRequired", 1 + "#bit#" + model.IsRequired);
            input_parameters.Add("@OfficeResults", 1 + "#bit#" + model.OfficeResults);
            input_parameters.Add("@FieldResults", 1 + "#bit#" + model.FieldResults);
            input_parameters.Add("@Form_IsActive", 1 + "#bit#" + model.Form_IsActive);
            input_parameters.Add("@Pdf_form", 1 + "#nvarchar#" + model.Pdf_form);
            input_parameters.Add("@Form_IsDelete", 1 + "#bit#" + false);
            input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserId);
            input_parameters.Add("@FormNumber_Id", 1 + "#nvarchar#" + model.FormNumber_Id);
            input_parameters.Add("@Form_IsAutoAssign", 1 + "#bit#" + model.Form_IsAutoAssign);
            input_parameters.Add("@Type", 1 + "#int#" + model.Type);
            input_parameters.Add("@FormId_Out", 2 + "#bigint#" + null);
            input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

            //string s = JsonConvert.SerializeObject(model.WO_Filters);

            objForm = await obj.SqlCRUDAsync(insertProcedure, input_parameters);


            //save form wo filter
            if (objForm.Count > 0)
            {


                long FormId = objForm[0]; //inserted formId
                model.FormId = FormId;//setting form id for further reference


                string insert_WO_Filter_Procedure = "[CreateUpdate_Forms_WO_Filters]";

                int deleteFlag = 1;
                foreach (Models.WO_Filters WO_Filter in model.WO_Filters)
                {

                    ClearInputParameter();

                    if ((WO_Filter.FiltersCompany != null && WO_Filter.FiltersCompany.Count > 0) || (WO_Filter.FiltersWorkType != null && WO_Filter.FiltersWorkType.Count > 0)
                        || (WO_Filter.FiltersCustomer != null && WO_Filter.FiltersCustomer.Count > 0) || (WO_Filter.FiltersLoanType != null && WO_Filter.FiltersLoanType.Count > 0) || (WO_Filter.FiltersWorkTypeGroup != null && WO_Filter.FiltersWorkTypeGroup.Count > 0))
                    {
                        List<dynamic> objWOFForm = new List<dynamic>();

                        if (WO_Filter.FiltersCompany != null && WO_Filter.FiltersCompany.Count > 0)
                        {
                            foreach (Models.Forms_WO_Filter_CompanyDTO company in WO_Filter.FiltersCompany)
                            {
                                company.WOF_FormId = FormId;

                            }
                        }
                        if (WO_Filter.FiltersWorkType != null && WO_Filter.FiltersWorkType.Count > 0)
                        {
                            foreach (Models.Forms_WO_Filter_WorkTypeDTO workType in WO_Filter.FiltersWorkType)
                            {
                                workType.WOF_FormId = FormId;

                            }
                        }
                        if (WO_Filter.FiltersCustomer != null && WO_Filter.FiltersCustomer.Count > 0)
                        {
                            foreach (Models.Forms_WO_Filters_CustomerDTO customer in WO_Filter.FiltersCustomer)
                            {
                                customer.WOF_FormId = FormId;

                            }
                        }
                        if (WO_Filter.FiltersLoanType != null && WO_Filter.FiltersLoanType.Count > 0)
                        {
                            foreach (Models.Forms_WO_Filter_LoanTypeDTO loanType in WO_Filter.FiltersLoanType)
                            {
                                loanType.WOF_FormId = FormId;

                            }
                        }
                        if (WO_Filter.FiltersWorkTypeGroup != null && WO_Filter.FiltersWorkTypeGroup.Count > 0)
                        {
                            foreach (Models.Forms_WO_Filter_WorkTypeGroupDTO workTypeGroup in WO_Filter.FiltersWorkTypeGroup)
                            {
                                workTypeGroup.WOF_FormId = FormId;

                            }
                        }

                        input_parameters.Add("WOF_FilterId", 1 + "#bigint#" + WO_Filter.WO_FilterId);
                        input_parameters.Add("WOF_Company", 1 + "#nvarchar#" + JsonConvert.SerializeObject(WO_Filter.FiltersCompany));
                        input_parameters.Add("WOF_WorkType", 1 + "#nvarchar#" + JsonConvert.SerializeObject(WO_Filter.FiltersWorkType));
                        input_parameters.Add("WOF_Customer", 1 + "#nvarchar#" + JsonConvert.SerializeObject(WO_Filter.FiltersCustomer));
                        input_parameters.Add("WOF_LoanType", 1 + "#nvarchar#" + JsonConvert.SerializeObject(WO_Filter.FiltersLoanType));
                        input_parameters.Add("WOF_WorkTypeGroup", 1 + "#nvarchar#" + JsonConvert.SerializeObject(WO_Filter.FiltersWorkTypeGroup));
                        input_parameters.Add("WOF_FormId", 1 + "#bigint#" + FormId);
                        input_parameters.Add("WOF_IsActive", 1 + "#bit#" + true);

                        //first time 
                        if (deleteFlag == 1 && model.Type == 2)
                        {
                            input_parameters.Add("DeleteFlag", 1 + "#int#" + deleteFlag);
                            deleteFlag = 0;
                        }
                        else
                        {
                            input_parameters.Add("DeleteFlag", 1 + "#int#" + 0);
                        }

                        input_parameters.Add("UserId", 1 + "#bigint#" + model.UserId);
                        input_parameters.Add("WOF_IsDelete", 1 + "#bit#" + false);




                        input_parameters.Add("@Type", 1 + "#int#" + model.Type); //insert
                        input_parameters.Add("@FilterId_Out", 2 + "#bigint#" + null);
                        input_parameters.Add("@ReturnValue", 2 + "#int#" + null);


                        objWOFForm = await obj.SqlCRUDAsync(insert_WO_Filter_Procedure, input_parameters);


                        if (objWOFForm.Count > 0)
                        {
                            long FilterId = objWOFForm[0];

                            //insert into individual tables 
                            if (WO_Filter.FiltersCompany != null && WO_Filter.FiltersCompany.Count > 0)
                            {
                                foreach (Models.Forms_WO_Filter_CompanyDTO company in WO_Filter.FiltersCompany)
                                {
                                    Save_WO_Filters_Company(company, FormId, FilterId, model.UserId, model.Type);
                                }
                            }
                            if (WO_Filter.FiltersCustomer != null && WO_Filter.FiltersCustomer.Count > 0)
                            {
                                foreach (Models.Forms_WO_Filters_CustomerDTO customer in WO_Filter.FiltersCustomer)
                                {
                                    Save_WO_Filters_Customer(customer, FormId, FilterId, model.UserId, model.Type);
                                }
                            }
                            if (WO_Filter.FiltersLoanType != null && WO_Filter.FiltersLoanType.Count > 0)
                            {
                                foreach (Models.Forms_WO_Filter_LoanTypeDTO loanType in WO_Filter.FiltersLoanType)
                                {
                                    Save_WO_Filters_LoanType(loanType, FormId, FilterId, model.UserId, model.Type);
                                }
                            }
                            if (WO_Filter.FiltersWorkType != null && WO_Filter.FiltersWorkType.Count > 0)
                            {
                                foreach (Models.Forms_WO_Filter_WorkTypeDTO workType in WO_Filter.FiltersWorkType)
                                {
                                    Save_WO_Filters_WorkType(workType, FormId, FilterId, model.UserId, model.Type);
                                }
                            }
                            if (WO_Filter.FiltersWorkTypeGroup != null && WO_Filter.FiltersWorkTypeGroup.Count > 0)
                            {
                                foreach (Models.Forms_WO_Filter_WorkTypeGroupDTO workTypeGroup in WO_Filter.FiltersWorkTypeGroup)
                                {
                                    Save_WO_Filters_WorkTypeGroup(workTypeGroup, FormId, FilterId, model.UserId, model.Type);
                                }
                            }
                        }
                    }
                }
            }
            return objForm;
        }
        public void Save_WO_Filters_Company(Models.Forms_WO_Filter_CompanyDTO wo_company, long formId, long filterId, long UserId, int type)
        {
            ClearInputParameter();
            List<dynamic> objWOFilter = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_Forms_WO_Filters_Company]";

            List<dynamic> objWOFForm = new List<dynamic>();
            input_parameters.Add("WOF_Company_pkeyId", 1 + "#bigint#" + wo_company.WOF_Company_pkeyId);
            input_parameters.Add("WOF_CompanyId", 1 + "#bigint#" + wo_company.Client_pkeyID);
            input_parameters.Add("WOF_FilterId", 1 + "#bigint#" + filterId);
            input_parameters.Add("WOF_FormId", 1 + "#bigint#" + formId);
            input_parameters.Add("WOF_Company_IsActive", 1 + "#bit#" + true);
            input_parameters.Add("WOF_Company_IsDelete", 1 + "#bit#" + false);

            input_parameters.Add("UserId", 1 + "#bigint#" + UserId);

            input_parameters.Add("@Type", 1 + "#int#" + type); //insert
            input_parameters.Add("@WOF_Company_pkeyId_Out", 2 + "#bigint#" + null);
            input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

            objWOFForm = obj.SqlCRUD(insertProcedure, input_parameters);

        }
        public void Save_WO_Filters_Customer(Models.Forms_WO_Filters_CustomerDTO wo_customer, long formId, long filterId, long UserId, int type)
        {
            ClearInputParameter();
            List<dynamic> objWOFilter = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_Forms_WO_Filters_Customer]";

            List<dynamic> objWOFForm = new List<dynamic>();
            input_parameters.Add("WOF_Customer_pkeyId", 1 + "#bigint#" + wo_customer.WOF_Customer_pkeyId);
            input_parameters.Add("WOF_CustomerId", 1 + "#bigint#" + wo_customer.Cust_Num_pkeyId);
            input_parameters.Add("WOF_FilterId", 1 + "#bigint#" + filterId);
            input_parameters.Add("WOF_FormId", 1 + "#bigint#" + formId);
            input_parameters.Add("WOF_Customer_IsActive", 1 + "#bit#" + true);

            input_parameters.Add("WOF_Customer_IsDelete", 1 + "#bit#" + false);

            input_parameters.Add("UserId", 1 + "#bigint#" + UserId);

            input_parameters.Add("@Type", 1 + "#int#" + type); //insert
            input_parameters.Add("@WOF_Customer_pkeyId_Out", 2 + "#bigint#" + null);
            input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

            objWOFForm = obj.SqlCRUD(insertProcedure, input_parameters);

        }
        public void Save_WO_Filters_LoanType(Models.Forms_WO_Filter_LoanTypeDTO wo_loantype, long formId, long filterId, long UserId, int type)
        {
            ClearInputParameter();
            List<dynamic> objWOFilter = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_Forms_WO_Filters_LoanType]";

            List<dynamic> objWOFForm = new List<dynamic>();
            input_parameters.Add("WOF_LoanType_pkeyId", 1 + "#bigint#" + wo_loantype.WOF_LoanType_pkeyId);
            input_parameters.Add("WOF_LoanTypeId", 1 + "#bigint#" + wo_loantype.Loan_pkeyId);
            input_parameters.Add("WOF_FilterId", 1 + "#bigint#" + filterId);
            input_parameters.Add("WOF_FormId", 1 + "#bigint#" + formId);
            input_parameters.Add("WOF_LoanType_IsActive", 1 + "#bit#" + true);

            input_parameters.Add("WOF_LoanType_IsDelete", 1 + "#bit#" + false);
            input_parameters.Add("UserId", 1 + "#bigint#" + UserId);


            input_parameters.Add("@Type", 1 + "#int#" + type); //insert
            input_parameters.Add("@WOF_LoanType_pkeyId_Out", 2 + "#bigint#" + null);
            input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

            objWOFForm = obj.SqlCRUD(insertProcedure, input_parameters);

        }
        public void Save_WO_Filters_WorkType(Models.Forms_WO_Filter_WorkTypeDTO wo_worktype, long formId, long filterId, long UserId, int type)
        {
            ClearInputParameter();
            List<dynamic> objWOFilter = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_Forms_WO_Filters_WorkType]";

            List<dynamic> objWOFForm = new List<dynamic>();
            input_parameters.Add("WOF_WorkType_pkeyId", 1 + "#bigint#" + wo_worktype.WOF_WorkType_pkeyId);
            input_parameters.Add("WOF_WorkTypeId", 1 + "#bigint#" + wo_worktype.WT_pkeyID);
            input_parameters.Add("WOF_FilterId", 1 + "#bigint#" + filterId);
            input_parameters.Add("WOF_FormId", 1 + "#bigint#" + formId);
            input_parameters.Add("WOF_WorkType_IsActive", 1 + "#bit#" + true);

            input_parameters.Add("WOF_WorkType_IsDelete", 1 + "#bit#" + false);
            input_parameters.Add("UserId", 1 + "#bigint#" + UserId);


            input_parameters.Add("@Type", 1 + "#int#" + type); //insert
            input_parameters.Add("@WOF_WorkType_pkeyId_Out", 2 + "#bigint#" + null);
            input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

            objWOFForm = obj.SqlCRUD(insertProcedure, input_parameters);

        }
        public void Save_WO_Filters_WorkTypeGroup(Models.Forms_WO_Filter_WorkTypeGroupDTO wo_workTypeGroup, long formId, long filterId, long UserId, int type)
        {
            ClearInputParameter();
            List<dynamic> objWOFilter = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_Forms_WO_Filters_WorkTypeGroup]";

            List<dynamic> objWOFForm = new List<dynamic>();
            input_parameters.Add("WOF_WorkTypeGroup_pkeyId", 1 + "#bigint#" + wo_workTypeGroup.WOF_WorkTypeGroup_pkeyId);
            input_parameters.Add("WOF_WorkTypeGroupId", 1 + "#bigint#" + wo_workTypeGroup.Work_Type_Cat_pkeyID);
            input_parameters.Add("WOF_FilterId", 1 + "#bigint#" + filterId);
            input_parameters.Add("WOF_FormId", 1 + "#bigint#" + formId);
            input_parameters.Add("WOF_WorkTypeGroup_IsActive", 1 + "#bit#" + true);
            input_parameters.Add("WOF_WorkTypeGroup_IsDelete", 1 + "#bit#" + false);
            input_parameters.Add("UserId", 1 + "#bigint#" + UserId);


            input_parameters.Add("@Type", 1 + "#int#" + type); //insert
            input_parameters.Add("@WOF_WorkTypeGroup_pkeyId_Out", 2 + "#bigint#" + null);
            input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

            objWOFForm = obj.SqlCRUD(insertProcedure, input_parameters);

        }


        public async Task<List<dynamic>> Add_Question(Forms_Question_MasterDTO model)
        {
            List<dynamic> objQuestion = new List<dynamic>();

            ClearInputParameter();

            string insertProcedure = "[CreateUpdate_Forms_Question_Master]";

            input_parameters.Add("QuestionId", 1 + "#bigint#" + model.QuestionId);
            input_parameters.Add("Question", 1 + "#nvarchar#" + model.Question);
            input_parameters.Add("QuestionTypeId", 1 + "#bigint#" + model.QuestionTypeId);
            input_parameters.Add("FormId", 1 + "#bigint#" + model.FormId);
            input_parameters.Add("Instructions", 1 + "#nvarchar#" + model.Instructions);
            input_parameters.Add("Que_Show", 1 + "#bit#" + model.Que_Show);
            input_parameters.Add("Que_OfficeResults", 1 + "#bit#" + model.Que_OfficeResults);
            input_parameters.Add("Que_Required", 1 + "#bit#" + model.Que_Required);
            input_parameters.Add("Que_FieldResults", 1 + "#bit#" + model.Que_FieldResults);
            input_parameters.Add("Que_ClonePrevAnswer", 1 + "#int#" + model.Que_ClonePrevAnswer);

            input_parameters.Add("Que_IsActive", 1 + "#bit#" + model.Que_IsActive);
            input_parameters.Add("Que_Camera", 1 + "#bit#" + model.Que_Camera);
            input_parameters.Add("Que_IsDelete", 1 + "#bit#" + model.Que_IsDelete);

            input_parameters.Add("UserId", 1 + "#bigint#" + model.UserId);
            input_parameters.Add("SequenceNo", 1 + "#bigint#" + model.SequenceNo);
            input_parameters.Add("TextAreaWidth", 1 + "#int#" + model.TextAreaWidth);
            input_parameters.Add("TextAreaHeight", 1 + "#int#" + model.TextAreaHeight);
            input_parameters.Add("TextBoxSize", 1 + "#int#" + model.TextBoxSize);
            input_parameters.Add("@Type", 1 + "#int#" + model.Type);
            input_parameters.Add("@Que_PdfFiels", 1 + "#bigint#" + model.Que_PdfFiels);
            input_parameters.Add("@Que_RefId", 1 + "#bigint#" + model.Que_RefId);

            input_parameters.Add("Que_Photo_Label_Name", 1 + "#nvarchar#" + model.Que_Photo_Label_Name);
            input_parameters.Add("Que_Photo_min_count", 1 + "#int#" + model.Que_Photo_min_count);
            input_parameters.Add("Que_Photo_max_count", 1 + "#int#" + model.Que_Photo_max_count);

            input_parameters.Add("@QuestionId_Out", 2 + "#bigint#" + null);
            input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

            objQuestion = await obj.SqlCRUDAsync(insertProcedure, input_parameters);


            if (objQuestion.Count > 0)
            {
                long QuestionId = objQuestion[0]; //inserted formId
                model.QuestionId = QuestionId;//setting form id for further reference

                if (model.QueType_IsMultiple)
                {


                    insertProcedure = "[CreateUpdate_Forms_Que_Options]";

                    foreach (Forms_Que_Options option in model.Options)
                    {
                        ClearInputParameter();
                        input_parameters.Add("OptionId", 1 + "#bigint#" + option.OptionId);
                        input_parameters.Add("OptionName", 1 + "#nvarchar#" + option.OptionName);
                        input_parameters.Add("Option_QuestionId", 1 + "#bigint#" + model.QuestionId);
                        input_parameters.Add("Option_IsActive", 1 + "#bit#" + true);
                        input_parameters.Add("Option_IsDelete", 1 + "#bit#" + false);
                        input_parameters.Add("Option_RefId", 1 + "#bigint#" + option.Option_RefId);
                        input_parameters.Add("Option_FormId", 1 + "#bigint#" + option.Option_FormId);
                        input_parameters.Add("UserId", 1 + "#bigint#" + model.UserId);

                        input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                        input_parameters.Add("@OptionId_Out", 2 + "#bigint#" + null);
                        input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                        List<dynamic> objOptions = new List<dynamic>();
                        objOptions = await obj.SqlCRUDAsync(insertProcedure, input_parameters);

                    }
                }
            }

            return objQuestion;
        }


        public async Task<List<dynamic>> Update_Question(Forms_Question_MasterDTO model)
        {
            List<dynamic> objQuestion = new List<dynamic>();

            ClearInputParameter();

            string insertProcedure = "[CreateUpdate_Forms_Question_Master]";

            input_parameters.Add("QuestionId", 1 + "#bigint#" + model.QuestionId);
            input_parameters.Add("Question", 1 + "#nvarchar#" + model.Question);
            input_parameters.Add("QuestionTypeId", 1 + "#bigint#" + model.QuestionTypeId);
            input_parameters.Add("FormId", 1 + "#bigint#" + model.FormId);
            input_parameters.Add("Instructions", 1 + "#nvarchar#" + model.Instructions);
            input_parameters.Add("Que_Show", 1 + "#bit#" + model.Que_Show);
            input_parameters.Add("Que_OfficeResults", 1 + "#bit#" + model.Que_OfficeResults);
            input_parameters.Add("Que_Required", 1 + "#bit#" + model.Que_Required);
            input_parameters.Add("Que_FieldResults", 1 + "#bit#" + model.Que_FieldResults);
            input_parameters.Add("Que_ClonePrevAnswer", 1 + "#int#" + model.Que_ClonePrevAnswer);
            input_parameters.Add("Que_IsActive", 1 + "#bit#" + model.Que_IsActive);
            input_parameters.Add("Que_Camera", 1 + "#bit#" + model.Que_Camera);
            input_parameters.Add("Que_IsDelete", 1 + "#bit#" + model.Que_IsDelete);
            input_parameters.Add("UserId", 1 + "#bigint#" + model.UserId);
            input_parameters.Add("SequenceNo", 1 + "#bigint#" + model.SequenceNo);

            input_parameters.Add("TextAreaWidth", 1 + "#int#" + model.TextAreaWidth);
            input_parameters.Add("TextAreaHeight", 1 + "#int#" + model.TextAreaHeight);
            input_parameters.Add("TextBoxSize", 1 + "#int#" + model.TextBoxSize);
            input_parameters.Add("@Que_PdfFiels", 1 + "#bigint#" + model.Que_PdfFiels);
            input_parameters.Add("@Que_RefId", 1 + "#bigint#" + model.Que_RefId);

            input_parameters.Add("Que_Photo_Label_Name", 1 + "#nvarchar#" + model.Que_Photo_Label_Name);
            input_parameters.Add("Que_Photo_min_count", 1 + "#int#" + model.Que_Photo_min_count);
            input_parameters.Add("Que_Photo_max_count", 1 + "#int#" + model.Que_Photo_max_count);

            input_parameters.Add("@Type", 1 + "#int#" + model.Type);
            input_parameters.Add("@QuestionId_Out", 2 + "#bigint#" + null);
            input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

            objQuestion = await obj.SqlCRUDAsync(insertProcedure, input_parameters);

            return objQuestion;

        }
        public async Task<List<dynamic>> Add_Options(List<Forms_Que_Options> model)
        {
            ClearInputParameter();

            string insertProcedure = "[CreateUpdate_Forms_Que_Options]";
            List<dynamic> objOptionsList = new List<dynamic>();

            List<dynamic> objOptions = new List<dynamic>();
            foreach (Forms_Que_Options option in model)
            {
                if (option != null && !string.IsNullOrEmpty(option.OptionName))
                {
                    ClearInputParameter();
                    input_parameters.Add("OptionId", 1 + "#bigint#" + option.OptionId);
                    input_parameters.Add("OptionName", 1 + "#nvarchar#" + option.OptionName);
                    input_parameters.Add("Option_QuestionId", 1 + "#bigint#" + option.Option_QuestionId);
                    input_parameters.Add("Option_IsActive", 1 + "#bit#" + true);
                    input_parameters.Add("Option_IsDelete", 1 + "#bit#" + false);
                    input_parameters.Add("Option_RefId", 1 + "#bigint#" + option.Option_RefId);
                    input_parameters.Add("Option_FormId", 1 + "#bigint#" + option.Option_FormId);
                    input_parameters.Add("UserId", 1 + "#bigint#" + option.UserId);

                    input_parameters.Add("@Type", 1 + "#int#" + option.Type);
                    input_parameters.Add("@OptionId_Out", 2 + "#bigint#" + null);
                    input_parameters.Add("@ReturnValue", 2 + "#int#" + null);


                    objOptions = await obj.SqlCRUDAsync(insertProcedure, input_parameters);
                    objOptionsList.Add(objOptions);
                }

            }
            return objOptionsList;
        }

        //public async Task<List<dynamic>> GetQuestions()
        //{

        //}
        public async Task<List<dynamic>> Add_Forms_ActionRules(List<Forms_ActionRules_MasterDTO> model)
        {
            List<dynamic> objActionRules = new List<dynamic>();


            string insertProcedure = string.Empty;



            foreach (Forms_ActionRules_MasterDTO ActionRuleMaster in model)
            {
                insertProcedure = "[CreateUpdate_Forms_ActionRules_Master]";
                if (!string.IsNullOrEmpty(ActionRuleMaster.ActionRule_Operator))
                {
                    ClearInputParameter();
                    input_parameters.Add("@ActionRuleId", 1 + "#bigint#" + ActionRuleMaster.ActionRuleId);
                    input_parameters.Add("@ActionRule_Operator", 1 + "#nvarchar#" + ActionRuleMaster.ActionRule_Operator);
                    input_parameters.Add("@ActionRule_Value", 1 + "#nvarchar#" + ActionRuleMaster.ActionRule_Value);
                    input_parameters.Add("@ActionRule_IsActive", 1 + "#bit#" + true);
                    input_parameters.Add("@UserId", 1 + "#bigint#" + ActionRuleMaster.UserId);
                    input_parameters.Add("@ActionRule_IsDelete", 1 + "#bit#" + false);
                    input_parameters.Add("@FormId", 1 + "#bigint#" + ActionRuleMaster.FormId);
                    input_parameters.Add("@QuestionId", 1 + "#bigint#" + ActionRuleMaster.QuestionId);


                    input_parameters.Add("@Type", 1 + "#int#" + ActionRuleMaster.Type);
                    input_parameters.Add("@ActionRuleId_Out", 2 + "#bigint#" + null);
                    input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                    objActionRules = await obj.SqlCRUDAsync(insertProcedure, input_parameters);


                    if (objActionRules.Count > 0)
                    {
                        long ActionRuleId = objActionRules[0]; //insert primary key
                        ActionRuleMaster.ActionRuleId = ActionRuleId;//setting form id for further reference

                        if (ActionRuleMaster.ActionRules.Count > 0)
                        {

                            insertProcedure = "[CreateUpdate_Forms_ActionRules]";

                            foreach (Forms_ActionRules rules in ActionRuleMaster.ActionRules)
                            {

                                if (!string.IsNullOrEmpty(rules.ActionValue))
                                {

                                    ClearInputParameter();
                                    input_parameters.Add("@ActionId", 1 + "#bigint#" + rules.ActionId);
                                    input_parameters.Add("@ActionRuleId", 1 + "#bigint#" + ActionRuleId);
                                    input_parameters.Add("@ActionQuestionId", 1 + "#bigint#" + rules.ActionQuestionId);
                                    input_parameters.Add("@ActionValue", 1 + "#nvarchar#" + rules.ActionValue);
                                    input_parameters.Add("@QuestionId", 1 + "#bigint#" + rules.QuestionId);
                                    input_parameters.Add("@FormId", 1 + "#bigint#" + rules.FormId);

                                    input_parameters.Add("@Action_IsActive", 1 + "#bit#" + true);
                                    input_parameters.Add("@UserId", 1 + "#bigint#" + rules.UserId);
                                    input_parameters.Add("@Action_IsDelete", 1 + "#bit#" + false);


                                    input_parameters.Add("@Type", 1 + "#int#" + rules.Type);
                                    input_parameters.Add("@ActionId_Out", 2 + "#bigint#" + null);
                                    input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                                    await obj.SqlCRUDAsync(insertProcedure, input_parameters);

                                }
                            }
                        }
                    }

                }
            }
            return objActionRules;
        }


        public async Task<List<dynamic>> Add_Forms_AlertRules(List<Forms_AlertRulesDTO> model)
        {
            List<dynamic> objAlertRules = new List<dynamic>();



            string insertProcedure = "[CreateUpdate_Forms_AlertRules]";

            foreach (Forms_AlertRulesDTO AlertRules in model)
            {

                if (AlertRules != null && !string.IsNullOrEmpty(AlertRules.AlertRule_Operator))
                {
                    ClearInputParameter();
                    input_parameters.Add("@AlertRuleId", 1 + "#bigint#" + AlertRules.AlertRuleId);
                    input_parameters.Add("@AlertRule_Operator", 1 + "#nvarchar#" + AlertRules.AlertRule_Operator);
                    input_parameters.Add("@AlertRule_Value", 1 + "#bigint#" + AlertRules.AlertRule_Value);
                    input_parameters.Add("@AlertRule_IsActive", 1 + "#bit#" + true);
                    input_parameters.Add("@UserId", 1 + "#bigint#" + AlertRules.UserId);
                    input_parameters.Add("@AlertRule_IsDelete", 1 + "#bit#" + false);
                    input_parameters.Add("@FormId", 1 + "#bigint#" + AlertRules.FormId);
                    input_parameters.Add("@QuestionId", 1 + "#bigint#" + AlertRules.QuestionId);


                    input_parameters.Add("@Type", 1 + "#int#" + AlertRules.Type);
                    input_parameters.Add("@AlertRuleId_Out", 2 + "#bigint#" + null);
                    input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                    objAlertRules = await obj.SqlCRUDAsync(insertProcedure, input_parameters);
                }
            }
            return objAlertRules;
        }


        public async Task<List<dynamic>> Add_Forms_FieldRules(List<Forms_FieldRulesDTO> model)
        {
            List<dynamic> objFieldRules = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_Forms_FieldRules]";

            foreach (Forms_FieldRulesDTO FieldRules in model)
            {
                if (FieldRules != null && !string.IsNullOrEmpty(FieldRules.FieldRule_Operator))
                {
                    ClearInputParameter();
                    input_parameters.Add("@FieldRuleId", 1 + "#bigint#" + FieldRules.FieldRuleId);
                    input_parameters.Add("@FieldRule_Operator", 1 + "#nvarchar#" + FieldRules.FieldRule_Operator);
                    input_parameters.Add("@FieldRule_Value", 1 + "#nvarchar#" + FieldRules.FieldRule_Value);
                    input_parameters.Add("@FieldRule_IsActive", 1 + "#bit#" + true);
                    input_parameters.Add("@UserId", 1 + "#bigint#" + FieldRules.UserId);
                    input_parameters.Add("@FieldRule_IsDelete", 1 + "#bit#" + false);
                    input_parameters.Add("@FormId", 1 + "#bigint#" + FieldRules.FormId);
                    input_parameters.Add("@QuestionId", 1 + "#bigint#" + FieldRules.QuestionId);


                    input_parameters.Add("@Type", 1 + "#int#" + FieldRules.Type);
                    input_parameters.Add("@FieldRuleId_Out", 2 + "#bigint#" + null);
                    input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                    objFieldRules = await obj.SqlCRUDAsync(insertProcedure, input_parameters);

                }

            }
            return objFieldRules;
        }


        public async Task<List<dynamic>> Add_Forms_PhotoRules(List<Forms_PhotoRulesDTO> model)
        {
            List<dynamic> objPhotoRules = new List<dynamic>();



            string insertProcedure = "[CreateUpdate_Forms_PhotoRules]";

            foreach (Forms_PhotoRulesDTO PhotoRules in model)
            {
                if (PhotoRules != null && !string.IsNullOrEmpty(PhotoRules.PhotoRule_Operator))
                {
                    ClearInputParameter();

                    input_parameters.Add("PhotoRuleId", 1 + "#bigint#" + PhotoRules.PhotoRuleId);
                    input_parameters.Add("PhotoRule_Operator", 1 + "#nvarchar#" + PhotoRules.PhotoRule_Operator);
                    input_parameters.Add("PhotoRule_Value", 1 + "#bigint#" + PhotoRules.PhotoRule_Value);
                    input_parameters.Add("PhotoRule_Min", 1 + "#bigint#" + PhotoRules.PhotoRule_Min);
                    input_parameters.Add("PhotoRule_Max", 1 + "#bigint#" + PhotoRules.PhotoRule_Max);
                    input_parameters.Add("PhotoRule_IsActive", 1 + "#bit#" + true);
                    input_parameters.Add("UserId", 1 + "#bigint#" + PhotoRules.UserId);
                    input_parameters.Add("PhotoRule_IsDelete", 1 + "#bit#" + false);
                    input_parameters.Add("FormId", 1 + "#bigint#" + PhotoRules.FormId);
                    input_parameters.Add("QuestionId", 1 + "#bigint#" + PhotoRules.QuestionId);


                    input_parameters.Add("@Type", 1 + "#int#" + PhotoRules.Type);
                    input_parameters.Add("@PhotoRuleId_Out", 2 + "#bigint#" + null);
                    input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                    objPhotoRules = await obj.SqlCRUDAsync(insertProcedure, input_parameters);
                }
            }
            return objPhotoRules;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<List<dynamic>> AddUpdateQuestionAnswer(List<Forms_Question_AnswersDTO> model)
        {
            List<dynamic> objAnswers = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_Forms_Question_Answers]";

            foreach (Forms_Question_AnswersDTO Answers in model)
            {
                if (Answers != null && !string.IsNullOrEmpty(Answers.AnswerValue))
                {
                    ClearInputParameter();
                    Answers.Type = Answers.QuestionTypeId == 1 ? 2 : Answers.Type;
                    input_parameters.Add("AnswerId", 1 + "#bigint#" + Answers.AnswerId);
                    input_parameters.Add("QuestionId", 1 + "#bigint#" + Answers.QuestionId);
                    input_parameters.Add("Question", 1 + "#nvarchar#" + Answers.Question);
                    input_parameters.Add("OptionId", 1 + "#bigint#" + Answers.OptionId);
                    input_parameters.Add("AnswerValue", 1 + "#nvarchar#" + Answers.AnswerValue);
                    input_parameters.Add("QuestionTypeId", 1 + "#bigint#" + Answers.QuestionTypeId);
                    input_parameters.Add("UserId", 1 + "#bigint#" + Answers.UserId);
                    input_parameters.Add("Answer_IsActive", 1 + "#bit#" + true);
                    input_parameters.Add("Answer_IsDelete", 1 + "#bit#" + false);
                    input_parameters.Add("FormId", 1 + "#bigint#" + Answers.FormId);
                    input_parameters.Add("WorkOrderId", 1 + "#bigint#" + Answers.WorkOrderId);
                    if (Answers.Address != null)
                        input_parameters.Add("Address", 1 + "#nvarchar#" + Answers.Address.Replace(System.Environment.NewLine, string.Empty));
                    else
                        input_parameters.Add("Address", 1 + "#nvarchar#" + null);

                    input_parameters.Add("@Type", 1 + "#int#" + Answers.Type);
                    input_parameters.Add("@AnswerId_Out", 2 + "#bigint#" + null);
                    input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                    objAnswers = await obj.SqlCRUDAsync(insertProcedure, input_parameters);
                }
            }
            return objAnswers;
        }
        /// <summary>
        /// use  Type = 1. when requesting all data
        /// use Type= 2 when requesting particular form data
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public List<dynamic> Get_Form_Master(FormsDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            List<WO_Filters> objFilters = new List<WO_Filters>();

            DataSet ds = new DataSet();
            try
            {
                string selectProcedure = "[Get_Forms_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserId);
                input_parameters.Add("@whereClause", 1 + "#nvarchar#" + model.whereClause);
                //if user request for particular form data
                if (model.FormId != 0)
                {
                    input_parameters.Add("@FormId", 1 + "#bigint#" + model.FormId);
                }
                else
                {
                    input_parameters.Add("@FormId", 1 + "#bigint#" + DBNull.Value);
                }

                ds = obj.SelectSql(selectProcedure, input_parameters);

                //add tables to dynamic obj
                if (model.Type == 1)
                {
                    for (int i = 0; i < ds.Tables.Count; i++)
                    {
                        objDynamic.Add(AsDynamicEnumerable(ds.Tables[i]));
                    }
                }
                else
                {
                    for (int i = 0; i < ds.Tables.Count; i++)
                    {
                        if (i == 0)
                        {
                            //form details in this first table
                            objDynamic.Add(AsDynamicEnumerable(ds.Tables[i]));
                        }
                        else if (i == 1)
                        {
                            string response = string.Empty;
                            for (int j = 0; j < ds.Tables[i].Rows.Count; j++)
                            {
                                WO_Filters wo = new WO_Filters();

                                wo.WO_FilterId = Convert.ToInt64(ds.Tables[i].Rows[j][0].ToString());

                                wo.FiltersCompany = JsonConvert.DeserializeObject<List<Models.Forms_WO_Filter_CompanyDTO>>(ds.Tables[i].Rows[j][1].ToString());

                                wo.FiltersWorkType = JsonConvert.DeserializeObject<List<Models.Forms_WO_Filter_WorkTypeDTO>>(ds.Tables[i].Rows[j][2].ToString());

                                wo.FiltersCustomer = JsonConvert.DeserializeObject<List<Models.Forms_WO_Filters_CustomerDTO>>(ds.Tables[i].Rows[j][3].ToString());

                                wo.FiltersLoanType = JsonConvert.DeserializeObject<List<Models.Forms_WO_Filter_LoanTypeDTO>>(ds.Tables[i].Rows[j][4].ToString());

                                wo.FiltersWorkTypeGroup = JsonConvert.DeserializeObject<List<Models.Forms_WO_Filter_WorkTypeGroupDTO>>(ds.Tables[i].Rows[j][5].ToString());

                                objFilters.Add(wo);

                            }

                            objDynamic.Add(objFilters);
                        }
                        else if (i == 2)
                        {
                            StringBuilder sb = new StringBuilder();

                            for (int z = 0; z < ds.Tables[i].Rows.Count; z++)
                            {
                                sb.Append(ds.Tables[i].Rows[z][0].ToString());
                            }

                            // string s = ds.Tables[i].Rows[0][0].ToString();
                            var questions = JsonConvert.DeserializeObject<List<Models.Forms_Question_MasterDTO>>(sb.ToString());
                            objDynamic.Add(questions);
                        }
                        else if (i == 3)
                        {
                            StringBuilder sb = new StringBuilder();

                            for (int z = 0; z < ds.Tables[i].Rows.Count; z++)
                            {
                                sb.Append(ds.Tables[i].Rows[z][0].ToString());
                            }

                            // string s = ds.Tables[i].Rows[0][0].ToString();
                            var actionRule = JsonConvert.DeserializeObject<List<Models.Forms_Question_MasterDTO>>(sb.ToString());
                            if (actionRule != null && actionRule.Count > 0)
                                actionRule.RemoveAll(a => a.ActionRulesMaster.Count == 0);
                            objDynamic.Add(actionRule);
                        }
                        else if (i == 4)
                        {
                            StringBuilder sb = new StringBuilder();

                            for (int z = 0; z < ds.Tables[i].Rows.Count; z++)
                            {
                                sb.Append(ds.Tables[i].Rows[z][0].ToString());
                            }

                            // string s = ds.Tables[i].Rows[0][0].ToString();
                            var fieldRule = JsonConvert.DeserializeObject<List<Models.Forms_Question_MasterDTO>>(sb.ToString());
                            if (fieldRule != null && fieldRule.Count > 0)
                                fieldRule.RemoveAll(a => a.FieldRules.Count == 0);
                            objDynamic.Add(fieldRule);
                        }
                        else if (i == 5)
                        {
                            objDynamic.Add(AsDynamicEnumerable(ds.Tables[i]));
                        }
                    }
                }





            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        public string CreateWhereClause(FormsDTO model)
        {
            string strWhere = string.Empty;
            try
            {
                if (!string.IsNullOrWhiteSpace(model.FormName))
                {
                    strWhere = "  AND dbo.fn_CleanAndTrim(formmst.FormName) Like  dbo.fn_CleanAndTrim('%" + model.FormName + "%')";
                }
                if (model.IsRequired == true)
                {
                    strWhere = strWhere + "  And formmst.IsRequired = 1";
                }
                if (model.Published == true)
                {
                    strWhere = strWhere + "  And formmst.Form_IsPublished = 1";
                }

                if (model.OfficeResults == true)
                {
                    strWhere = strWhere + "  And formmst.OfficeResults = 1";
                }

                if (model.FieldResults == true)
                {
                    strWhere = strWhere + "  And formmst.FieldResults = 1";
                }

                if (model.Form_IsActive == true)
                {
                    strWhere = strWhere + "  And formmst.Form_IsVisible = 1";
                }

                if (model.Form_Version_No != null)
                {
                    strWhere = strWhere + "  And formmst.Form_Version_No = " + model.Form_Version_No;
                }
                if (model.FormAddedby != null)
                {
                    strWhere = strWhere + "  And formmst.Form_CreatedBy = " + model.FormAddedby;
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return strWhere;

        }
        /// <summary>
        /// Type=1
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        /// 

        public List<dynamic> Get_Filter_FormData(FormsDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                string strWhere = string.Empty;
                strWhere = CreateWhereClause(model);
                model.whereClause = strWhere;
                objDynamic = Get_Form_Master(model);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objDynamic;
        }

        /// <summary>
        /// Type=1
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public List<dynamic> Get_Form_Question(Forms_Question_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            DataSet ds = new DataSet();
            try
            {
                string selectProcedure = "[Get_Forms_QuestionPage]";
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserId);
                if (model.QuestionId != 0)
                {
                    input_parameters.Add("@QuestionId", 1 + "#bigint#" + model.QuestionId);
                    ds = obj.SelectSql(selectProcedure, input_parameters);

                    StringBuilder sb = new StringBuilder();

                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        sb.Append(ds.Tables[0].Rows[i][0].ToString());
                    }

                    string s = sb.ToString();
                    // string s = sb.ToString().Substring(1, sb.ToString().Length - 2);

                    //s = sb.ToString().Substring(1, sb.ToString().Length - 2);
                    // string s = ds.Tables[i].Rows[0][0].ToString();
                    dynamic questions = null;
                    if (model.Type == 1) //questions data
                    {
                        questions = JsonConvert.DeserializeObject<Models.Forms_Question_MasterDTO>(s.Substring(1, sb.ToString().Length - 2));
                    }
                    else if (model.Type == 2)
                    {
                        questions = JsonConvert.DeserializeObject<List<Models.Forms_PhotoRulesDTO>>(s);
                    }
                    else if (model.Type == 3)
                    {
                        questions = JsonConvert.DeserializeObject<List<Models.Forms_FieldRulesDTO>>(s);
                    }
                    else if (model.Type == 4)
                    {
                        questions = JsonConvert.DeserializeObject<List<Models.Forms_AlertRulesDTO>>(s);
                    }
                    else if (model.Type == 5)
                    {
                        questions = JsonConvert.DeserializeObject<List<Models.Forms_ActionRules_MasterDTO>>(s);
                    }
                    else if (model.Type == 6)
                    {
                        questions = JsonConvert.DeserializeObject<List<Models.Forms_Que_Options>>(s);
                    }
                    objDynamic.Add(questions);
                }

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objDynamic;
        }

        public List<dynamic> Get_Form_PreviewPage(FormsDTO model)
        {
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            List<dynamic> objDynamic = new List<dynamic>();
            DataSet ds = new DataSet();
            try
            {
                string selectProcedure = "[Get_Forms_PreviewData]";
                // input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserId);
                if (model.FormId != 0)
                {
                    input_parameters.Add("@FormId", 1 + "#bigint#" + model.FormId);
                    input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                    input_parameters.Add("@WorkOrderId", 1 + "#bigint#" + model.WorkOrderId);
                    input_parameters.Add("@Address", 1 + "#nvarchar#" + model.Address.Replace(System.Environment.NewLine, string.Empty));

                    ds = obj.SelectSql(selectProcedure, input_parameters);

                    StringBuilder sb = new StringBuilder();

                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        sb.Append(ds.Tables[0].Rows[i][0].ToString());
                    }


                    string s = sb.ToString();

                    //s = sb.ToString().Substring(1, sb.ToString().Length - 2);
                    // string s = ds.Tables[i].Rows[0][0].ToString();
                    var questions = JsonConvert.DeserializeObject<List<Models.Forms_Question_MasterDTO>>(s);


                    ///CONVERT PERVIOUS ANSWER VALUES 
                    foreach (var item in questions)
                    {
                        if (item.QuestionTypeId == 1 && item.PCR_History_PreviousAnswers.Any())
                        {
                            var getOptionNames = JsonConvert.DeserializeObject<List<dynamic>>(item.PCR_History_PreviousAnswers.FirstOrDefault().ActualAnswerValue);
                            string optionNames = string.Join(",", getOptionNames.Select(x=>x.OptionName));
                            item.PCR_History_PreviousAnswers.ForEach(x => x.ActualAnswerValue = optionNames);
                        }
                    }

                    objDynamic.Add(questions);
                }

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objDynamic;
        }

        public async Task<List<dynamic>> DeleteFilter(Models.WO_Filters WO_Filter)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                string selectProcedure = "[CreateUpdate_Forms_WO_Filters]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@WOF_Company", 1 + "#nvarchar#" + null);
                input_parameters.Add("@WOF_WorkType", 1 + "#nvarchar#" + null);
                input_parameters.Add("@WOF_Customer", 1 + "#nvarchar#" + null);
                input_parameters.Add("@WOF_LoanType", 1 + "#nvarchar#" + null);
                input_parameters.Add("@WOF_WorkTypeGroup", 1 + "#nvarchar#" + null);
                input_parameters.Add("@WOF_FormId", 1 + "#bigint#" + WO_Filter.WO_FormId);
                input_parameters.Add("@WOF_IsActive", 1 + "#bit#" + false);

                input_parameters.Add("@Type", 1 + "#int#" + 3);
                input_parameters.Add("@DeleteFlag", 1 + "#int#" + 0);

                input_parameters.Add("@UserId", 1 + "#bigint#" + WO_Filter.UserId);
                input_parameters.Add("@WOF_IsDelete", 1 + "#bit#" + true);

                input_parameters.Add("@FilterId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);
                //if user request for particular form data
                if (WO_Filter.WO_FilterId != 0)
                {
                    input_parameters.Add("@WOF_FilterId", 1 + "#bigint#" + WO_Filter.WO_FilterId);
                    objDynamic = await obj.SqlCRUDAsync(selectProcedure, input_parameters);
                }

                return objDynamic;
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
        public async Task<List<dynamic>> DeleteEditQuestionItems(Forms_Question_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                string selectProcedure = "[Delete_Forms_EditQuestionItem]";

                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserId);
                input_parameters.Add("@Id", 1 + "#bigint#" + model.Id);

                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                if (model.Id != 0)
                {
                    objDynamic = await obj.SqlCRUDAsync(selectProcedure, input_parameters);
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objDynamic;
        }

        public async Task<List<dynamic>> DeleteForm(FormsDTO form)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                string selectProcedure = "[CreateUpdate_Forms_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();
                input_parameters.Add("@FormName", 1 + "#nvarchar#" + form.FormName);
                input_parameters.Add("@IsRequired", 1 + "#bit#" + form.IsRequired);
                input_parameters.Add("@OfficeResults", 1 + "#bit#" + form.OfficeResults);
                input_parameters.Add("@FieldResults", 1 + "#bit#" + form.FieldResults);
                input_parameters.Add("@Form_IsActive", 1 + "#bit#" + form.Form_IsActive);
                input_parameters.Add("@Pdf_form", 1 + "#nvarchar#" + form.Pdf_form);
                input_parameters.Add("@Form_IsDelete", 1 + "#bit#" + false);
                input_parameters.Add("@UserId", 1 + "#bigint#" + form.UserId);
                input_parameters.Add("@FormNumber_Id", 1 + "#nvarchar#" + form.FormNumber_Id);
                input_parameters.Add("@Form_IsAutoAssign", 1 + "#bit#" + form.Form_IsAutoAssign);
                input_parameters.Add("@Type", 1 + "#int#" + form.Type);
                input_parameters.Add("@FormId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                //if user request for particular form data
                if (form.FormId != 0)
                {
                    input_parameters.Add("@FormId", 1 + "#bigint#" + form.FormId);
                    objDynamic = await obj.SqlCRUDAsync(selectProcedure, input_parameters);
                }

                return objDynamic;
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        public async Task<List<dynamic>> CopyForm(FormsDTO form)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                string selectProcedure = "[CreateUpdate_Forms_CopyForm]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("Form_Parent_ID", 1 + "#bigint#" + null);
                input_parameters.Add("Form_Version_No", 1 + "#bigint#" + null);
                input_parameters.Add("UserId", 1 + "#bigint#" + form.UserId);
                input_parameters.Add("@FormId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);


                if (form.FormId != 0)
                {
                    input_parameters.Add("@FormId", 1 + "#bigint#" + form.FormId);
                    objDynamic = await obj.SqlCRUDAsync(selectProcedure, input_parameters);
                }

                return objDynamic;
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        public async Task<List<dynamic>> UpdateFormStatus(FormsDTO form)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                string selectProcedure = "[Update_Form_Status]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("Type", 1 + "#int#" + form.Type);
                input_parameters.Add("Form_IsVisible", 1 + "#bit#" + false);
                input_parameters.Add("UserId", 1 + "#bigint#" + form.UserId);
                //if user request for particular form data
                if (form.FormId != 0)
                {
                    input_parameters.Add("@FormId", 1 + "#bigint#" + form.FormId);
                    objDynamic = await obj.SqlCRUDAsync(selectProcedure, input_parameters);
                }

                return objDynamic;
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        public async Task<List<dynamic>> UpdateQuestionStatus(Forms_Question_MasterDTO question)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                ClearInputParameter();
                string selectProcedure = "[Update_Form_QuestionStatus]";
                // Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                //input_parameters.Add("@Type", 1 + "#int#" + form.Type);
                input_parameters.Add("UserId", 1 + "#bigint#" + question.UserId);

                input_parameters.Add("UpdateField", 1 + "#nvarchar#" + question.UpdateField);

                //if user request for particular form data
                if (question.QuestionId != 0)
                {
                    input_parameters.Add("QuestionId", 1 + "#bigint#" + question.QuestionId);
                    input_parameters.Add("@QuestionId_Out", 2 + "#bigint#" + null);
                    objDynamic = await obj.SqlCRUDAsync(selectProcedure, input_parameters);
                }

                return objDynamic;
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
        public IEnumerable<dynamic> AsDynamicEnumerable(DataTable table)
        {
            if (table == null)
            {
                yield break;
            }

            foreach (DataRow row in table.Rows)
            {
                IDictionary<string, object> dRow = new ExpandoObject();

                foreach (DataColumn column in table.Columns)
                {
                    var value = row[column.ColumnName];
                    dRow[column.ColumnName] = Convert.IsDBNull(value) ? null : value;
                }

                yield return dRow;
            }
        }
        public List<dynamic> Get_Wo_Form_Master(FormsDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            List<WO_Filters> objFilters = new List<WO_Filters>();

            DataSet ds = new DataSet();
            try
            {
                string selectProcedure = "[Get_Forms_Wo_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserId);

                //if user request for particular form data
                if (model.WorkOrderId != 0)
                {
                    input_parameters.Add("@WoId", 1 + "#bigint#" + model.WorkOrderId);
                }
                else
                {
                    input_parameters.Add("@WoId", 1 + "#bigint#" + DBNull.Value);
                }

                ds = obj.SelectSql(selectProcedure, input_parameters);
                #region Comment
                //if (ds.Tables.Count > 0)
                //{
                //    objDynamic.Add(AsDynamicEnumerable(ds.Tables[0]));
                //}
                //if (ds.Tables.Count > 1)
                //{
                //    objDynamic.Add(AsDynamicEnumerable(ds.Tables[1]));
                //}
                #endregion
                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    objDynamic.Add(AsDynamicEnumerable(ds.Tables[i]));
                }

                #region comment
                //add tables to dynamic obj
                //for (int i = 0; i < ds.Tables.Count; i++)
                //{
                //    if (i == 0)
                //    {
                //        //form details in this first table
                //        objDynamic.Add(AsDynamicEnumerable(ds.Tables[i]));                        
                //    }
                //    else
                //    {
                //        StringBuilder sb = new StringBuilder();

                //        for (int z = 0; z < ds.Tables[i].Rows.Count; z++)
                //        {
                //            sb.Append(ds.Tables[i].Rows[z][0].ToString());
                //        }

                //        // string s = ds.Tables[i].Rows[0][0].ToString();
                //        var questions = JsonConvert.DeserializeObject<List<Models.Forms_Question_MasterDTO>>(sb.ToString());
                //        objDynamic.Add(questions);
                //    }                   
                //}
                #endregion

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        public List<dynamic> Add_Form_Wo_Relation(FormsWoDTO model)
        {
            List<dynamic> objForm = new List<dynamic>();
            try
            {

                Dictionary<string, string> input_parameter = new Dictionary<string, string>();
                //save pcr form
                string insertProcedure = "[CreateUpdate_Form_Wo_Relation_Master]";

                input_parameter.Add("@fwo_pkyeId", 1 + "#bigint#" + model.Fwo_pkyeId);
                input_parameter.Add("@fwo_PCRFormId", 1 + "#bigint#" + model.Fwo_PCRFormId);
                input_parameter.Add("@fwo_WoId", 1 + "#bigint#" + model.Fwo_WoId);
                input_parameter.Add("@fwo_IsActive", 1 + "#bit#" + model.Fwo_IsActive);
                input_parameter.Add("@UserID", 1 + "#bigint#" + model.UserId);
                input_parameter.Add("@Type", 1 + "#int#" + model.Type);
                input_parameter.Add("@fwo_IsOfficeResult", 1 + "#bit#" + model.Fwo_IsOfficeResult);
                input_parameter.Add("@fwo_IsFieldResult", 1 + "#bit#" + model.Fwo_IsFieldResult);
                input_parameter.Add("@fwo_IsPcrAdd", 1 + "#bit#" + model.Fwo_IsPcrAdd);
                input_parameter.Add("@IsDynamicForm", 1 + "#bit#" + model.IsDynamicForm);
                input_parameter.Add("@fwo_pkyeId_Out", 2 + "#bigint#" + null);
                input_parameter.Add("@ReturnValue", 2 + "#int#" + null);

                //string s = JsonConvert.SerializeObject(model.WO_Filters);

                objForm = obj.SqlCRUD(insertProcedure, input_parameter);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objForm;
        }

        private DataSet Get_FB_Forms(Fb_Dynamic_MasterDTO model)
        {
            DataSet ds = null;
            try
            {

                string selectProcedure = "[Get_Fb_Dynamic_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Fb_Dynamic_pkeyID", 1 + "#bigint#" + model.Fb_Dynamic_pkeyID);
                input_parameters.Add("@Fb_Dynamic_ImportFrom_Id", 1 + "#bigint#" + model.Fb_Dynamic_ImportFrom_Id);
                input_parameters.Add("@workOrder_ID", 1 + "#bigint#" + model.workOrder_ID);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ds;
        }
        public List<dynamic> Get_FB_FormsDetails(Fb_Dynamic_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = Get_FB_Forms(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Fb_Dynamic_MasterDTO> Forms_Master_Files =
                   (from item in myEnumerableFeaprd
                    select new Fb_Dynamic_MasterDTO
                    {
                        Fb_Dynamic_pkeyID = item.Field<Int64>("Fb_Dynamic_pkeyID"),
                        Fb_Dynamic_Tab_Name = item.Field<String>("Fb_Dynamic_Tab_Name"),
                        Fb_Dynamic_Office_Results = item.Field<Boolean>("Fb_Dynamic_Office_Results"),
                        Fb_Dynamic_FieldResults = item.Field<Boolean>("Fb_Dynamic_FieldResults"),
                        Fb_Dynamic_IsActive = item.Field<Boolean>("Fb_Dynamic_IsActive"),
                        Fb_Dynamic_ImportFrom_Id = item.Field<Int64?>("Fb_Dynamic_ImportFrom_Id"),
                        Fb_Dynamic_Company_Id = item.Field<Int64?>("Fb_Dynamic_Company_Id"),
                        Fb_Dynamic_WorkTypeId = item.Field<Int64?>("Fb_Dynamic_WorkTypeId"),
                    }).ToList();

                objDynamic.Add(Forms_Master_Files);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
        public List<dynamic> UpdateFBFormStatus(Fb_Dynamic_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                string selectProcedure = "[Update_FB_Form_Status]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Fb_Dynamic_pkeyID", 1 + "#bigint#" + model.Fb_Dynamic_pkeyID);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserId);

                objDynamic = obj.SqlCRUD(selectProcedure, input_parameters);
                return objDynamic;
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
        public List<dynamic> AddFb_Dynamic_Master(Fb_Dynamic_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objAddData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_Fb_Dynamic_Master]";
            Fb_Dynamic_Master fb_Dynamic_Master = new Fb_Dynamic_Master();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Fb_Dynamic_pkeyID", 1 + "#bigint#" + model.Fb_Dynamic_pkeyID);
                input_parameters.Add("@Fb_Dynamic_Tab_Name", 1 + "#nvarchar#" + model.Fb_Dynamic_Tab_Name);
                input_parameters.Add("@Fb_Dynamic_Office_Results", 1 + "#bit#" + model.Fb_Dynamic_Office_Results);
                input_parameters.Add("@Fb_Dynamic_FieldResults", 1 + "#bit#" + model.Fb_Dynamic_FieldResults);
                input_parameters.Add("@Fb_Dynamic_ImportFrom_Id", 1 + "#bigint#" + model.Fb_Dynamic_ImportFrom_Id);
                input_parameters.Add("@Fb_Dynamic_Company_Id", 1 + "#bigint#" + model.Fb_Dynamic_Company_Id);
                input_parameters.Add("@Fb_Dynamic_WorkTypeId", 1 + "#bigint#" + model.Fb_Dynamic_WorkTypeId);
                input_parameters.Add("@Fb_Dynamic_IsActive", 1 + "#bit#" + model.Fb_Dynamic_IsActive);
                input_parameters.Add("@Fb_Dynamic_IsDelete", 1 + "#bit#" + model.Fb_Dynamic_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Fb_Dynamic_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    fb_Dynamic_Master.Fb_Dynamic_pkeyID = "0";
                    fb_Dynamic_Master.Status = "0";
                    fb_Dynamic_Master.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    fb_Dynamic_Master.Fb_Dynamic_pkeyID = Convert.ToString(objData[0]);
                    fb_Dynamic_Master.Status = Convert.ToString(objData[1]);


                }
                objAddData.Add(fb_Dynamic_Master);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objAddData;
        }
        public List<dynamic> PostFb_Dynamic_Master(Fb_Dynamic_MasterListDTO modelList)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                foreach (var model in modelList.Fb_Dynamic_List)
                {
                    model.Type = 2;
                    model.Fb_Dynamic_IsDelete = false;
                    var objData = AddFb_Dynamic_Master(model);
                    objDynamic.Add(objData);
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        public void ClearInputParameter()
        {
            if (input_parameters != null)
            {
                input_parameters.Clear();
            }
        }
        public List<dynamic> GeneratePdfDocumentForForm(FormsWoDTO model)
        {
            List<dynamic> objForm = new List<dynamic>();
            try
            {

                var ds = GetPdfVersionDocQuestionAnswer(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Form_Question_Ans_Filed_DTO> pdfDocumentDetails =
                   (from item in myEnumerableFeaprd
                    select new Form_Question_Ans_Filed_DTO
                    {
                        AnswerId = item.Field<Int64>("AnswerId"),
                        QuestionId = item.Field<Int64>("QuestionId"),
                        Que_PdfFiels = item.Field<Int64>("Que_PdfFiels"),
                        FileId = item.Field<Int64>("FileId"),
                        QuestionTypeId = item.Field<Int64>("QuestionTypeId"),
                        OptionId = item.Field<Int64>("OptionId"),
                        AnswerValue = item.Field<string>("AnswerValue"),
                        FieldName = item.Field<string>("FieldName"),
                        FilePath = item.Field<string>("FilePath"),
                        FileName = item.Field<string>("FileName"),
                        OptionName = item.Field<string>("OptionName"),
                    }).ToList();

                if (pdfDocumentDetails.Count > 0)
                {
                    string pdfTemplate = pdfDocumentDetails[0].FilePath;
                    string static_folderPath = Path.Combine(HttpRuntime.AppDomainAppPath, "Document\\Download\\PDFForms\\" + model.Fwo_PCRFormId.ToString() + "\\");
                    if (!Directory.Exists(static_folderPath))
                    {
                        Directory.CreateDirectory(static_folderPath);
                    }


                    string newFile = static_folderPath + pdfDocumentDetails[0].FileName;

                    string folderName = model.Fwo_PCRFormId.ToString();
                    string folderPath = static_folderPath;



                    PdfReader pdfReader = new PdfReader(pdfTemplate);
                    PdfStamper pdfStamper = new PdfStamper(pdfReader, new FileStream(newFile, FileMode.Create));
                    AcroFields pdfFormFields = pdfStamper.AcroFields;
                    foreach (var item in pdfDocumentDetails)
                    {

                        switch (item.QuestionTypeId)
                        {
                            case 1: //Checkbox(es)
                                pdfFormFields.SetField(item.FieldName, item.AnswerValue == "0" ? "" : item.OptionName);
                                break;
                            case 3://Dropdown Menu
                            case 4://Radio Boxes
                                pdfFormFields.SetField(item.FieldName, item.OptionName);
                                break;
                            case 2://Comment Box     
                            case 5://Textbox
                            case 6://Title 
                            case 7://Date 
                            case 8://Numeric
                                pdfFormFields.SetField(item.FieldName, item.AnswerValue);
                                break;
                            default:
                                break;
                        }
                    }
                    pdfStamper.FormFlattening = false;
                    pdfStamper.Close();


                    #region upload Zip file to Storage
                    Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", jsonpath);
                    FileUploadDownload fileUploadDownload = new FileUploadDownload();
                    var credential = GoogleCredential.GetApplicationDefault();
                    var storage = StorageClient.Create();
                    var storageClient = StorageClient.Create(credential);
                    string UploadFilePath = folderName;
                    string UploadFileType = "";
                    string url = string.Empty;
                    using (var fileStream = new FileStream(newFile, FileMode.Open, FileAccess.Read, FileShare.Read))
                    {
                        var response = storage.UploadObject(bucketName, UploadFilePath + "/" + pdfDocumentDetails[0].FileName, UploadFileType, fileStream);
                        url = DownloadURL(folderName, pdfDocumentDetails[0].FileName);
                    }
                    #endregion

                    Directory.Delete(static_folderPath, true);
                    objForm.Add(url);
                }


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objForm;
        }
        private DataSet GetPdfVersionDocQuestionAnswer(FormsWoDTO model)
        {
            DataSet ds = new DataSet();
            try
            {
                string selectProcedure = "[GetPdfVersionDocFIeldAndAnswer]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@FormId", 1 + "#int#" + model.Fwo_PCRFormId);
                input_parameters.Add("@WorkOrderId", 1 + "#int#" + model.Fwo_WoId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserId);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return ds;
        }

        public string DownloadURL(string folderName, string fileName)
        {
            string url = string.Empty;
            try
            {
                UrlSigner urlSigner = UrlSigner.FromServiceAccountPath(jsonpath);
                // V4 is the default signing version.
                url = urlSigner.Sign(bucketName, folderName + "/" + fileName, TimeSpan.FromHours(24), HttpMethod.Get);
                // Console.WriteLine($"curl '{url}'");
            }
            catch (Exception ex)
            {

                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return url;
        }
    }
}