﻿using Avigma.Repository.Lib;
using IPL.Models;
using IPL.Models.FiveBrothers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Repository.data
{
    public class WorkOrder_Cyprexx_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        private List<dynamic> AddWorkOrder_CyprexxData(WorkOrder_Cyprexx_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_WorkOrder_Cyprexx_Master]";
            WorkOrder_Cyprexx_Master WorkOrder_Cyprexx_Master = new WorkOrder_Cyprexx_Master();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@WCyprexx_PkeyID", 1 + "#bigint#" + model.WCyprexx_PkeyID);
                input_parameters.Add("@WCyprexx_wo_id", 1 + "#nvarchar#" + model.WCyprexx_wo_id);

                input_parameters.Add("@WCyprexx_address", 1 + "#nvarchar#" + model.WCyprexx_address);
                input_parameters.Add("@WCyprexx_City", 1 + "#nvarchar#" + model.WCyprexx_City);
                input_parameters.Add("@WCyprexx_State", 1 + "#nvarchar#" + model.WCyprexx_State);
                input_parameters.Add("@WCyprexx_Zip", 1 + "#nvarchar#" + model.WCyprexx_Zip);
                input_parameters.Add("@WCyprexx_Customer", 1 + "#nvarchar#" + model.WCyprexx_Customer);
                input_parameters.Add("@WCyprexx_Loan_Type", 1 + "#nvarchar#" + model.WCyprexx_Loan_Type);
                input_parameters.Add("@WCyprexx_Lock_Code", 1 + "#nvarchar#" + model.WCyprexx_Lock_Code);
                input_parameters.Add("@WCyprexx_Key_Code", 1 + "#nvarchar#" + model.WCyprexx_Key_Code);
                input_parameters.Add("@WCyprexx_Received_Date", 1 + "#datetime#" + model.WCyprexx_Received_Date);
                input_parameters.Add("@WCyprexx_Due_Date", 1 + "#datetime#" + model.WCyprexx_Due_Date);

                input_parameters.Add("@WCyprexx_Comments", 1 + "#varchar#" + model.WCyprexx_Comments);
                input_parameters.Add("@WCyprexx_gpsLatitude", 1 + "#varchar#" + model.WCyprexx_gpsLatitude);
                input_parameters.Add("@WCyprexx_gpsLongitude", 1 + "#varchar#" + model.WCyprexx_gpsLongitude);
                input_parameters.Add("@WCyprexx_ImportMaster_Pkey", 1 + "#bigint#" + model.WCyprexx_ImportMaster_Pkey);
                input_parameters.Add("@WCyprexx_Import_File_Name", 1 + "#varchar#" + model.WCyprexx_Import_File_Name);
                input_parameters.Add("@WCyprexx_Import_FilePath", 1 + "#varchar#" + model.WCyprexx_Import_FilePath);
                input_parameters.Add("@WCyprexx_Import_Pdf_Name", 1 + "#varchar#" + model.WCyprexx_Import_Pdf_Name);
                input_parameters.Add("@WCyprexx_Import_Pdf_Path", 1 + "#varchar#" + model.WCyprexx_Import_Pdf_Path);





                

               
                input_parameters.Add("@WCyprexx_IsActive", 1 + "#bit#" + model.WCyprexx_IsActive);
                input_parameters.Add("@WCyprexx_IsDelete", 1 + "#bit#" + model.WCyprexx_IsDelete);
                input_parameters.Add("@WCyprexx_Cyprexx_ID", 1 + "#nvarchar#" + model.WCyprexx_Cyprexx_ID);

                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@WCyprexx_Pkey_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    WorkOrder_Cyprexx_Master.WCyprexx_PkeyID = "0";
                    WorkOrder_Cyprexx_Master.Status = "0";
                    WorkOrder_Cyprexx_Master.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    WorkOrder_Cyprexx_Master.WCyprexx_PkeyID = Convert.ToString(objData[0]);
                    WorkOrder_Cyprexx_Master.Status = Convert.ToString(objData[1]);


                }
                objcltData.Add(WorkOrder_Cyprexx_Master);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;

        }


        private List<dynamic> AddWorkOrder_CyprexxItemData(WorkOrder_Cyprexx_Item_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_WorkOrder_Cyprexx_Item_Master]";
            WorkOrder_Cyprexx_Item_Master WorkOrder_Cyprexx_Item_Master = new WorkOrder_Cyprexx_Item_Master();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@WCYPREXXINS_PkeyId", 1 + "#bigint#" + model.WCYPREXXINS_PkeyId);
                input_parameters.Add("@WCYPREXXINS_Ins_Name", 1 + "#varchar#" + model.WCYPREXXINS_Ins_Name);
                input_parameters.Add("@WCYPREXXINS_Ins_Details", 1 + "#varchar#" + model.WCYPREXXINS_Ins_Details);
                input_parameters.Add("@WCYPREXXINS_Qty", 1 + "#bigint#" + model.WCYPREXXINS_Qty);
                input_parameters.Add("@WCYPREXXINS_Price", 1 + "#decimal#" + model.WCYPREXXINS_Price);
                input_parameters.Add("@WCYPREXXINS_Total", 1 + "#decimal#" + model.WCYPREXXINS_Total);
                input_parameters.Add("@WCYPREXXINS_FkeyID", 1 + "#bigint#" + model.WCYPREXXINS_FkeyID);
                
                input_parameters.Add("@WCYPREXXINS_IsActive", 1 + "#bit#" + model.WCYPREXXINS_IsActive);
                input_parameters.Add("@WCYPREXXINS_IsDelete", 1 + "#bit#" + model.WCYPREXXINS_IsDelete);
                input_parameters.Add("@WCYPREXXINS_Additional_Details", 1 + "#nvarchar#" + model.WCYPREXXINS_Additional_Details);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@WCYPREXXINS_Pkey_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    WorkOrder_Cyprexx_Item_Master.WCYPREXXINS_PkeyId = "0";
                    WorkOrder_Cyprexx_Item_Master.Status = "0";
                    WorkOrder_Cyprexx_Item_Master.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    WorkOrder_Cyprexx_Item_Master.WCYPREXXINS_PkeyId = Convert.ToString(objData[0]);
                    WorkOrder_Cyprexx_Item_Master.Status = Convert.ToString(objData[1]);

                }
                objcltData.Add(WorkOrder_Cyprexx_Item_Master);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;



        }

        public List<dynamic> AddWorkOrderCyprexxData(WorkOrder_Cyprexx_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                objData = AddWorkOrder_CyprexxData(model);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        public List<dynamic> AddWorkOrderCyprexxItemData(WorkOrder_Cyprexx_Item_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                objData = AddWorkOrder_CyprexxItemData(model);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }
    }
}