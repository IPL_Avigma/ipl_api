﻿using Avigma.Repository.Lib;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace IPL.Repository.data
{
    public class Appliance_Name_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        public List<dynamic> AddAAppliancNameData(Appliance_Name_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_ApplianceNameMaster]";
            Appliance_NameMaster appliance_NameMaster = new Appliance_NameMaster();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@App_pkeyId", 1 + "#bigint#" + model.App_pkeyId);
                input_parameters.Add("@App_Apliance_Name", 1 + "#nvarchar#" + model.App_Apliance_Name);
                input_parameters.Add("@App_IsActive", 1 + "#bit#" + model.App_IsActive);
                input_parameters.Add("@App_IsDelete", 1 + "#bit#" + model.App_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@App_pkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    appliance_NameMaster.App_pkeyId = "0";
                    appliance_NameMaster.Status = "0";
                    appliance_NameMaster.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    appliance_NameMaster.App_pkeyId = Convert.ToString(objData[0]);
                    appliance_NameMaster.Status = Convert.ToString(objData[1]);


                }
                objcltData.Add(appliance_NameMaster);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;

        }


        public DataSet GetAppliance_NamenMaster(Appliance_Name_MasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Appliance_Name_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@App_pkeyId", 1 + "#bigint#" + model.App_pkeyId);

                input_parameters.Add("@WhereClause", 1 + "#nvarchar#" + model.WhereClause);
                input_parameters.Add("@PageNumber", 1 + "#int#" + model.PageNumber);
                input_parameters.Add("@Rowcount", 1 + "#int#" + model.Rowcount);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetAppliance_NamenDetails(Appliance_Name_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                string wherecondition = string.Empty;
                if (model.Type == 3 && !string.IsNullOrEmpty(model.FilterData))
                {
                    var Data = JsonConvert.DeserializeObject<NewApplianceNameMaster_Filter>(model.FilterData);
                    if (!string.IsNullOrEmpty(Data.App_Apliance_Name))
                    {
                        wherecondition = " And app.App_Apliance_Name LIKE '%" + Data.App_Apliance_Name + "%'";
                    }
                   

                    model.WhereClause = wherecondition;
                }


                DataSet ds = GetAppliance_NamenMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Appliance_Name_MasterDTO> Appliance_NameDetails =
                   (from item in myEnumerableFeaprd
                    select new Appliance_Name_MasterDTO
                    {
                        App_pkeyId = item.Field<Int64>("App_pkeyId"),
                        App_Apliance_Name = item.Field<String>("App_Apliance_Name"),
                        App_IsActive = item.Field<Boolean?>("App_IsActive"),

                    }).ToList();

                objDynamic.Add(Appliance_NameDetails);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        ////// group details for admin 
        ///
        public List<dynamic> AddgrouproleData(GroupRoleMasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_Group_Role_DropDown_Master]";
            GroupRoleMaster groupRoleMaster = new GroupRoleMaster();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Group_DR_PkeyID", 1 + "#bigint#" + model.Group_DR_PkeyID);
                input_parameters.Add("@Group_DR_Name", 1 + "#nvarchar#" + model.Group_DR_Name);
                input_parameters.Add("@Group_DR_IsActive", 1 + "#bit#" + model.Group_DR_IsActive);
                input_parameters.Add("@Group_DR_IsDelete", 1 + "#bit#" + model.Group_DR_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Group_DR_PkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    groupRoleMaster.Group_DR_PkeyID = "0";
                    groupRoleMaster.Status = "0";
                    groupRoleMaster.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    groupRoleMaster.Group_DR_PkeyID = Convert.ToString(objData[0]);
                    groupRoleMaster.Status = Convert.ToString(objData[1]);


                }
                objcltData.Add(groupRoleMaster);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;

        }


        public DataSet GetGroupRoleMaster(GroupRoleMasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Group_Role_DropDown_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Group_DR_PkeyID", 1 + "#bigint#" + model.Group_DR_PkeyID);

                input_parameters.Add("@WhereClause", 1 + "#nvarchar#" + model.WhereClause);
                input_parameters.Add("@PageNumber", 1 + "#int#" + model.PageNumber);
                input_parameters.Add("@Rowcount", 1 + "#int#" + model.Rowcount);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);

                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetGroupRoleDetails(GroupRoleMasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {

                string wherecondition = string.Empty;
                if (model.Type == 3 && !string.IsNullOrEmpty(model.FilterData))
                {
                    var Data = JsonConvert.DeserializeObject<NewGroupRoleMaster_Filter>(model.FilterData);
                    if (!string.IsNullOrEmpty(Data.Group_DR_Name))
                    {
                        wherecondition = " And gp.Group_DR_Name LIKE '%" + Data.Group_DR_Name + "%'";
                    }

                    model.WhereClause = wherecondition;
                }

                DataSet ds = GetGroupRoleMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<GroupRoleMasterDTO> grouproleDetails =
                   (from item in myEnumerableFeaprd
                    select new GroupRoleMasterDTO
                    {
                        Group_DR_PkeyID = item.Field<Int64>("Group_DR_PkeyID"),
                        Group_DR_Name = item.Field<String>("Group_DR_Name"),
                        Group_DR_IsActive = item.Field<Boolean?>("Group_DR_IsActive"),

                    }).ToList();

                objDynamic.Add(grouproleDetails);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

    }
}