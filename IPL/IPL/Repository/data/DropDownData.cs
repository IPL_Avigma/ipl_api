﻿using Avigma.Repository.Lib;
using IPL.Models;
using IPLApp.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Newtonsoft.Json;
using IPL.Models.FiveBrothers;

namespace IPLApp.Repository.data
{
    public class DropDownData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();


        private DataSet GetDropDownData(DropDownMasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_DropDown_master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        //public List<dynamic> GetDropdownDetails()
        //{
        //    List<dynamic> objDynamic = new List<dynamic>();
        //    try
        //    {
        //        DataSet ds = GetDropDownData();

        //        var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
        //        List<DropDownDTO> CompanyDetails =
        //           (from item in myEnumerableFeaprd
        //            select new DropDownDTO
        //            {
        //                Cmp_ID = item.Field<Int64>("Cmp_ID"),
        //                Cmp_Name = item.Field<String>("Cmp_Name"),

        //            }).ToList();

        //        objDynamic.Add(CompanyDetails);

        //        var myEnumerablecat = ds.Tables[1].AsEnumerable();
        //        List<CategoryDTO> CategoryDetails =
        //           (from item in myEnumerablecat
        //            select new CategoryDTO
        //            {
        //                Cat_ID = item.Field<Int64>("Cat_ID"),
        //                Cat_Name = item.Field<String>("Cat_Name"),

        //            }).ToList();

        //        objDynamic.Add(CategoryDetails);

        //        var myEnumerableAdmin = ds.Tables[2].AsEnumerable();
        //        List<AdminMaster> AdminDetails =
        //           (from item in myEnumerableAdmin
        //            select new AdminMaster
        //            {
        //                Assigned_Admin_ID = item.Field<Int64>("Assigned_Admin_ID"),
        //                Assigned_Admin_Name = item.Field<String>("Assigned_Admin_Name"),

        //            }).ToList();

        //        objDynamic.Add(AdminDetails);

        //        var myEnumerableCheckin = ds.Tables[3].AsEnumerable();
        //        List<CheckinProvider> CheckinDetails =
        //           (from item in myEnumerableCheckin
        //            select new CheckinProvider
        //            {
        //                Back_Chk_ProviderID = item.Field<Int64>("Back_Chk_ProviderID"),
        //                Back_Chk_ProviderName = item.Field<String>("Back_Chk_ProviderName"),

        //            }).ToList();

        //        objDynamic.Add(CheckinDetails);
        //        var myEnumerableContractor = ds.Tables[4].AsEnumerable();
        //        List<ContractorMaster> ContractorDetails =
        //           (from item in myEnumerableContractor
        //            select new ContractorMaster
        //            {
        //                Cont_ID = item.Field<Int64>("Cont_ID"),
        //                Cont_Name = item.Field<String>("Cont_Name"),

        //            }).ToList();

        //        objDynamic.Add(ContractorDetails);
        //        var myEnumerableWorkType = ds.Tables[5].AsEnumerable();
        //        List<WorkType> WorkTypeDetails =
        //           (from item in myEnumerableWorkType
        //            select new WorkType
        //            {
        //                Wrk_ID = item.Field<Int64>("Wrk_ID"),
        //                Wrk_Name = item.Field<String>("Wrk_Name"),

        //            }).ToList();

        //        objDynamic.Add(WorkTypeDetails);
        //        var myEnumerableState = ds.Tables[6].AsEnumerable();
        //        List<StateMaster> StateDetails =
        //           (from item in myEnumerableState
        //            select new StateMaster
        //            {
        //                SM_id = item.Field<Int64>("SM_id"),
        //                SM_Name = item.Field<String>("SM_Name"),

        //            }).ToList();

        //        objDynamic.Add(StateDetails);

        //    }
        //    catch (Exception ex)
        //    {
        //        log.logErrorMessage(ex.StackTrace);
        //    }

        //    return objDynamic;
        //}





        private DataSet GetDropDownDataWorkOrder(DropDownMasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_DropDown_WorkOrder]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();
                input_parameters.Add("@WorkOrderID", 1 + "#bigint#" + model.WorkOrderID);
                input_parameters.Add("@FilterID", 1 + "#bigint#" + model.FilterID);
                input_parameters.Add("@Imrt_PkeyId", 1 + "#bigint#" + model.Imrt_PkeyId);
                input_parameters.Add("@WorkOrderID_mul", 1 + "#varchar#" + model.WorkOrderID_mul);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@PageID", 1 + "#int#" + model.PageID);
               
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }


        private DataSet GetCountyByState(StateData model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[GetCountyByState]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();
                input_parameters.Add("@StateID", 1 + "#bigint#" + model.IPL_StateID);
                input_parameters.Add("@WhereClause", 1 + "#varchar#" + model.WhereClause);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetCountyByStateData(StateData stateData)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                string StrWhere = string.Empty, StateID = string.Empty;

                var Data = JsonConvert.DeserializeObject<List<StateMaster>>(stateData.StateMaster);
                for (int i = 0; i < Data.Count; i++)
                {
                    if (!string.IsNullOrEmpty(StateID))
                    {
                        StateID = StateID + " , " + Data[i].IPL_StateID;
                    }
                    else
                    {
                        StateID = Data[i].IPL_StateID.ToString();
                    }
                }
                StrWhere = "And County_StateID in ( " + StateID + ")";
                stateData.WhereClause = StrWhere;

                DataSet ds = GetCountyByState(stateData);
                var myEnumerableFeaprdc = ds.Tables[0].AsEnumerable();
                List<ContractorCouny> CountyDrdDetails =
                   (from item in myEnumerableFeaprdc
                    select new ContractorCouny
                    {
                        ID = item.Field<Int64>("ID"),
                        COUNTY = item.Field<String>("COUNTY"),


                    }).ToList();

                objDynamic.Add(CountyDrdDetails);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                log.logErrorMessage("GetCountyByStateData" + stateData + "<-----------------------");
            }
            return objDynamic;

        }

        public List<dynamic> GetDropdownWorkOrder(DropDownMasterDTO dropDownMasterDTO)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetDropDownDataWorkOrder(dropDownMasterDTO);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<WorkClientMaster> CompanyDetails =
                   (from item in myEnumerableFeaprd
                    select new WorkClientMaster
                    {
                        Client_pkeyID = item.Field<Int64>("Client_pkeyID"),
                        Client_Company_Name = item.Field<String>("Client_Company_Name"),

                    }).ToList();

                objDynamic.Add(CompanyDetails);
                var myEnumerableFeaprdx = ds.Tables[1].AsEnumerable();
                List<WorkTypeMasterDataValue> worktype =
                   (from item in myEnumerableFeaprdx
                    select new WorkTypeMasterDataValue
                    {
                        WT_pkeyID = item.Field<Int64>("WT_pkeyID"),
                        WT_WorkType = item.Field<String>("WT_WorkType"),

                    }).ToList();

                objDynamic.Add(worktype);

                var myEnumerablecat = ds.Tables[2].AsEnumerable();
                List<CategoryDTO> CategoryDetails =
                   (from item in myEnumerablecat
                    select new CategoryDTO
                    {
                        Cat_ID = item.Field<Int64>("Cat_ID"),
                        Cat_Name = item.Field<String>("Cat_Name"),

                    }).ToList();

                objDynamic.Add(CategoryDetails);

                var myEnumerableCont = ds.Tables[3].AsEnumerable();
                List<USerMasterData> UserContDetails =
                   (from item in myEnumerableCont
                    select new USerMasterData
                    {
                        User_pkeyID = item.Field<Int64>("User_pkeyID"),
                        User_FirstName = item.Field<String>("User_FirstName"),
                        User_LoginName = item.Field<String>("User_LoginName"),
                    }).ToList();

                objDynamic.Add(UserContDetails);

                var myEnumerableCond = ds.Tables[4].AsEnumerable();
                List<USerMasterData> UserCondDetails =
                   (from item in myEnumerableCond
                    select new USerMasterData
                    {
                        User_pkeyID = item.Field<Int64>("User_pkeyID"),
                        User_FirstName = item.Field<String>("User_FirstName"),
                        User_LoginName = item.Field<String>("User_LoginName"),
                    }).ToList();

                objDynamic.Add(UserCondDetails);


                var myEnumerableProce = ds.Tables[5].AsEnumerable();
                List<USerMasterData> UserProceDetails =
                   (from item in myEnumerableProce
                    select new USerMasterData
                    {
                        User_pkeyID = item.Field<Int64>("User_pkeyID"),
                        User_FirstName = item.Field<String>("User_FirstName"),
                        User_LoginName = item.Field<String>("User_LoginName"),
                    }).ToList();

                objDynamic.Add(UserProceDetails);


                var myEnumerableState = ds.Tables[6].AsEnumerable();
                List<StateMaster> StateDetails =
                   (from item in myEnumerableState
                    select new StateMaster
                    {
                        IPL_StateID = item.Field<Int64>("IPL_StateID"),
                        IPL_StateName = item.Field<String>("IPL_StateName"),

                    }).ToList();

                objDynamic.Add(StateDetails);


                var myEnumerableRush = ds.Tables[7].AsEnumerable();
                List<RushMaster> RushMaster =
                   (from item in myEnumerableRush
                    select new RushMaster
                    {
                        rus_pkeyID = item.Field<Int64>("rus_pkeyID"),
                        rus_Name = item.Field<String>("rus_Name"),

                    }).ToList();

                objDynamic.Add(RushMaster);


                var myEnumerableCheckin = ds.Tables[8].AsEnumerable();
                List<CheckinProvider> CheckinDetails =
                   (from item in myEnumerableCheckin
                    select new CheckinProvider
                    {
                        Back_Chk_ProviderID = item.Field<Int64>("Back_Chk_ProviderID"),
                        Back_Chk_ProviderName = item.Field<String>("Back_Chk_ProviderName"),

                    }).ToList();

                objDynamic.Add(CheckinDetails);




                var myEnumerableCustnumber = ds.Tables[9].AsEnumerable();
                List<CustomerNumberDTO> CustomerNumber =
                   (from item in myEnumerableCustnumber
                    select new CustomerNumberDTO
                    {
                        Cust_Num_pkeyId = item.Field<Int64>("Cust_Num_pkeyId"),
                        Cust_Num_Number = item.Field<String>("Cust_Num_Number"),

                    }).ToList();

                objDynamic.Add(CustomerNumber);

                var myEnumerableLoantype = ds.Tables[10].AsEnumerable();
                List<LoanTypeMasterDTO> LoanType =
                   (from item in myEnumerableLoantype
                    select new LoanTypeMasterDTO
                    {
                        Loan_pkeyId = item.Field<Int64>("Loan_pkeyId"),
                        Loan_Type = item.Field<String>("Loan_Type"),

                    }).ToList();

                objDynamic.Add(LoanType);

                var myEnumerableWorkType = ds.Tables[11].AsEnumerable();
                List<WorkTypeCategoryDTO> WorkType =
                   (from item in myEnumerableWorkType
                    select new WorkTypeCategoryDTO
                    {
                        Work_Type_Cat_pkeyID = item.Field<Int64>("Work_Type_Cat_pkeyID"),
                        Work_Type_Name = item.Field<String>("Work_Type_Name"),

                    }).ToList();

                objDynamic.Add(WorkType);

                var myEnumerableInfo = ds.Tables[12].AsEnumerable();
                List<ImportFormDto> Importform =
                   (from item in myEnumerableInfo
                    select new ImportFormDto
                    {
                        Import_Form_PkeyId = item.Field<Int64>("Import_Form_PkeyId"),
                        Import_Form_Name = item.Field<String>("Import_Form_Name"),

                    }).ToList();

                objDynamic.Add(Importform);

                var myEnumerableIg = ds.Tables[13].AsEnumerable();
                List<GroupRoleDRDMaster> groupinfo =
                   (from item in myEnumerableIg
                    select new GroupRoleDRDMaster
                    {
                        Group_DR_PkeyID = item.Field<Int64>("Group_DR_PkeyID"),
                        Group_DR_Name = item.Field<String>("Group_DR_Name"),

                    }).ToList();

                objDynamic.Add(groupinfo);

                var myEnumerablecon = ds.Tables[14].AsEnumerable();
                List<USerMasterData> conupdated =
                   (from item in myEnumerablecon
                    select new USerMasterData
                    {
                        User_pkeyID = item.Field<Int64>("User_pkeyID"),
                        User_FirstName = item.Field<String>("User_FirstName"),
                        User_LoginName = item.Field<String>("User_LoginName"),
                    }).ToList();

                objDynamic.Add(conupdated);


                var myEnumerableproc = ds.Tables[15].AsEnumerable();
                List<USerMasterData> procupdated =
                   (from item in myEnumerableproc
                    select new USerMasterData
                    {
                        User_pkeyID = item.Field<Int64>("User_pkeyID"),
                        User_FirstName = item.Field<String>("User_FirstName"),
                        User_LoginName = item.Field<String>("User_LoginName"),
                    }).ToList();

                objDynamic.Add(procupdated);

                var myEnumerablecor = ds.Tables[16].AsEnumerable();
                List<USerMasterData> corupdated =
                   (from item in myEnumerablecor
                    select new USerMasterData
                    {
                        User_pkeyID = item.Field<Int64>("User_pkeyID"),
                        User_FirstName = item.Field<String>("User_FirstName"),
                        User_LoginName = item.Field<String>("User_LoginName"),
                    }).ToList();

                objDynamic.Add(corupdated);

                var myEnumerableFeaprdc = ds.Tables[18].AsEnumerable();
                List<ContractorCouny> CountyDrdDetails =
                   (from item in myEnumerableFeaprdc
                    select new ContractorCouny
                    {
                        ID = item.Field<Int64>("ID"),
                        COUNTY = item.Field<String>("COUNTY"),


                    }).ToList();

                objDynamic.Add(CountyDrdDetails);


                var myEnumerableFeh = ds.Tables[19].AsEnumerable();
                List<WorkClientMaster> clientdetail =
                   (from item in myEnumerableFeh
                    select new WorkClientMaster
                    {
                        Client_pkeyID = item.Field<Int64>("Client_pkeyID"),
                        Client_Company_Name = item.Field<String>("Client_Company_Name"),

                    }).ToList();
                objDynamic.Add(clientdetail);

                var myEnumerableFeaprdimp = ds.Tables[20].AsEnumerable();

                List<ImportClient> importnamedetail =
                   (from item in myEnumerableFeaprdimp
                    select new ImportClient
                    {
                        WI_Pkey_ID = item.Field<Int64>("WI_Pkey_ID"),
                        WI_FriendlyName = item.Field<String>("WI_FriendlyName"),

                    }).ToList();
                objDynamic.Add(importnamedetail);

                var myEnumerableback = ds.Tables[21].AsEnumerable();
                List<CheckinProvider> background =
                   (from item in myEnumerableback
                    select new CheckinProvider
                    {
                        Back_Chk_ProviderID = item.Field<Int64>("Back_Chk_ProviderID"),
                        Back_Chk_ProviderName = item.Field<String>("Back_Chk_ProviderName"),

                    }).ToList();

                objDynamic.Add(background);


                var myEnumerablPT = ds.Tables[22].AsEnumerable();
                List<Property_Type_MasterDTO> PT =
                   (from item in myEnumerablPT
                    select new Property_Type_MasterDTO
                    {
                        PT_PkeyID = item.Field<Int64>("PT_PkeyID"),
                        PT_Name = item.Field<String>("PT_Name"),

                    }).ToList();

                objDynamic.Add(PT);


                var myEnumerablOS = ds.Tables[23].AsEnumerable();
                List<Occupancy_Status_MasterDTO> OS =
                   (from item in myEnumerablOS
                    select new Occupancy_Status_MasterDTO
                    {
                        OS_PkeyID = item.Field<Int64>("OS_PkeyID"),
                        OS_Name = item.Field<String>("OS_Name"),

                    }).ToList();

                objDynamic.Add(OS);


                var myEnumerablLS = ds.Tables[24].AsEnumerable();
                List<Loan_Status_MasterDTO> LS =
                   (from item in myEnumerablLS
                    select new Loan_Status_MasterDTO
                    {
                        LS_PkeyID = item.Field<Int64>("LS_PkeyID"),
                        LS_Name = item.Field<String>("LS_Name"),

                    }).ToList();

                objDynamic.Add(LS);



                var myEnumerablPA = ds.Tables[25].AsEnumerable();
                List<Property_Alert_MasterDTO> PA =
                   (from item in myEnumerablPA
                    select new Property_Alert_MasterDTO
                    {
                        PA_PkeyID = item.Field<Int64>("PA_PkeyID"),
                        PA_Name = item.Field<String>("PA_Name"),

                    }).ToList();

                objDynamic.Add(PA);


                var myEnumerablPS = ds.Tables[26].AsEnumerable();
                List<Property_Status_MasterDTO> PS =
                   (from item in myEnumerablPS
                    select new Property_Status_MasterDTO
                    {
                        PS_PkeyID = item.Field<Int64>("PS_PkeyID"),
                        PS_Name = item.Field<String>("PS_Name"),

                    }).ToList();

                objDynamic.Add(PS);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        public List<dynamic> GetDropdownWorkOrdernew(DropDownMasterDTO dropDownMasterDTO)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetDropDownDataWorkOrder(dropDownMasterDTO);
                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

                private DataSet GetClientRsultMaster(DropDownMasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_DropDown_ClientResult]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@WorkOrderID", 1 + "#bigint#" + model.WorkOrderID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@WorkOrderID_mul", 1 + "#varchar#" + model.WorkOrderID_mul);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }



        //Get drop down
        public List<dynamic> GetClientRsultMasterDetails(DropDownMasterDTO dropDownMasterDTO)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetClientRsultMaster(dropDownMasterDTO);

                if (ds.Tables.Count > 0)
                {
                    var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                    List<TaskMasterDTO> AppClientDetails =
                       (from item in myEnumerableFeaprd
                        select new TaskMasterDTO
                        {
                            Task_pkeyID = item.Field<Int64>("Task_pkeyID"),
                            Task_Name = item.Field<String>("Task_Name"),
                            Task_Disable_Default = item.Field<Boolean?>("Task_Disable_Default"),
                            Task_Price_Edit = item.Field<Boolean?>("Task_Price_Edit"),

                        }).ToList();

                    objDynamic.Add(AppClientDetails);

                }

                if (ds.Tables.Count > 1)
                {
                    var myEnumerableFeaprdx = ds.Tables[1].AsEnumerable();
                    List<DamageMasterDTO> DamageMasterDetails =
                       (from item in myEnumerableFeaprdx
                        select new DamageMasterDTO
                        {
                            Damage_pkeyID = item.Field<Int64>("Damage_pkeyID"),
                            strDamage_pkeyID = Convert.ToString(item.Field<Int64>("Damage_pkeyID")), // why bcoz clinet result save damage id as string get retun string  field not match
                            Damage_Type = item.Field<String>("Damage_Type"),
                            Damage_Int = item.Field<int?>("Damage_Int"),
                            Damage_Location = item.Field<String>("Damage_Location"),
                            Damage_Qty = item.Field<String>("Damage_Qty"),
                            Damage_Disc = item.Field<String>("Damage_Disc"),
                            Damage_Estimate = item.Field<Decimal?>("Damage_Estimate"),
                        }).ToList();

                    objDynamic.Add(DamageMasterDetails);
                }
                if (ds.Tables.Count > 2)
                {
                    var myEnumerableFeaprdxx = ds.Tables[2].AsEnumerable();
                    List<UOM_MasterDTO> UOM_MasterDetails =
                       (from item in myEnumerableFeaprdxx
                        select new UOM_MasterDTO
                        {
                            UOM_pkeyId = item.Field<Int64>("UOM_pkeyId"),
                            UOM_Name = item.Field<String>("UOM_Name"),

                        }).ToList();

                    objDynamic.Add(UOM_MasterDetails);

                }
                if (ds.Tables.Count > 3)
                {
                    var myEnumerableFeaprdzx = ds.Tables[3].AsEnumerable();
                    List<TaskPresetDTO> TaskpresetDetails =
                       (from item in myEnumerableFeaprdzx
                        select new TaskPresetDTO
                        {
                            Task_Preset_pkeyId = item.Field<Int64>("Task_Preset_pkeyId"),
                            Task_Preset_Text = item.Field<String>("Task_Preset_Text"),
                        }).ToList();

                    objDynamic.Add(TaskpresetDetails);
                }
                //Appliance Name
                if (ds.Tables.Count > 4)
                {
                    var myEnumerableAppliance = ds.Tables[4].AsEnumerable();
                    List<Appliance_Name_Dto> ApplianceNameMaster =
                       (from item in myEnumerableAppliance
                        select new Appliance_Name_Dto
                        {
                            App_pkeyId = item.Field<Int64>("App_pkeyId"),
                            App_Apliance_Name = item.Field<String>("App_Apliance_Name"),

                        }).ToList();

                    objDynamic.Add(ApplianceNameMaster);
                }

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                log.logErrorMessage("WorkOrderID----------->" + dropDownMasterDTO.WorkOrderID);
                log.logErrorMessage("UserID----------->" + dropDownMasterDTO.UserID);
                log.logErrorMessage("Type----------->" + dropDownMasterDTO.Type);
            }

            return objDynamic;
        }

        #region comment
        //Get drop down
        //public List<dynamic> GetTaskConfigurationDropDown(DropDownMasterDTO dropDownMasterDTO)
        //{
        //    List<dynamic> objDynamic = new List<dynamic>();
        //    try
        //    {
        //        DataSet ds = GetTaskConfigurationDropDownData(dropDownMasterDTO);

        //        if (dropDownMasterDTO.Type == 1)
        //        {
        //            if (ds.Tables.Count > 0)
        //            {
        //                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
        //                List<TaskMasterDTO> AppClientDetails =
        //                   (from item in myEnumerableFeaprd
        //                    select new TaskMasterDTO
        //                    {
        //                        Task_pkeyID = item.Field<Int64>("Task_pkeyID"),
        //                        Task_Name = item.Field<String>("Task_Name"),
        //                        Allow_Selection = true,

        //                    }).ToList();

        //                objDynamic.Add(AppClientDetails);

        //            }
        //            if (ds.Tables.Count > 1)
        //            {
        //                var myEnumerableFeaprdx = ds.Tables[1].AsEnumerable();
        //                List<Import_Client_ItemCode_MasterDTO> DamageMasterDetails =
        //                   (from item in myEnumerableFeaprdx
        //                    select new Import_Client_ItemCode_MasterDTO
        //                    {
        //                        Import_Client_ItemCode_LoanId = item.Field<String>("Import_Client_ItemCode_LoanId"),
        //                        Import_Client_ItemCode_LoanType = item.Field<String>("Import_Client_ItemCode_LoanType"),

        //                    }).ToList();

        //                objDynamic.Add(DamageMasterDetails);
        //            }
        //            if (ds.Tables.Count > 2)
        //            {
        //                var myEnumerableFeaprdxx = ds.Tables[2].AsEnumerable();
        //                List<Import_Client_ItemCode_MasterDTO> UOM_MasterDetails =
        //                   (from item in myEnumerableFeaprdxx
        //                    select new Import_Client_ItemCode_MasterDTO
        //                    {
        //                        Import_Client_ItemCode_PkeyId = item.Field<Int64>("Import_Client_ItemCode_PkeyId"),
        //                        Import_Client_ItemCode_LoanType = item.Field<String>("Import_Client_ItemCode_LoanType"),
        //                        Import_Client_ItemCode_Code = item.Field<String>("Import_Client_ItemCode_Code"),

        //                    }).ToList();

        //                objDynamic.Add(UOM_MasterDetails);
        //            }
        //            if (ds.Tables.Count > 3)
        //            {
        //                var myEnumerableFeaprdzx = ds.Tables[3].AsEnumerable();
        //                List<Import_Client_BidCategory_MasteDTO> TaskpresetDetails =
        //                   (from item in myEnumerableFeaprdzx
        //                    select new Import_Client_BidCategory_MasteDTO
        //                    {
        //                        Import_Client_BidCategory_PkeyId = item.Field<Int64>("Import_Client_BidCategory_PkeyId"),
        //                        Import_Client_BidCategory_Name = item.Field<String>("Import_Client_BidCategory_Name"),
        //                    }).ToList();

        //                objDynamic.Add(TaskpresetDetails);
        //            }
        //            if (ds.Tables.Count > 4)
        //            {
        //                var myEnumerableAppliance = ds.Tables[4].AsEnumerable();
        //                List<Import_Client_OrderTypeCode_MasterDTO> ApplianceNameMaster =
        //                   (from item in myEnumerableAppliance
        //                    select new Import_Client_OrderTypeCode_MasterDTO
        //                    {
        //                        Import_Client_OrderTypeCodes_PkeyID = item.Field<Int64>("Import_Client_OrderTypeCodes_PkeyID"),
        //                        Import_Client_OrderTypeCodes_Description = item.Field<String>("Import_Client_OrderTypeCodes_Description"),
        //                        Import_Client_OrderTypeCodes_MainType = item.Field<String>("Import_Client_OrderTypeCodes_MainType"),

        //                    }).ToList();

        //                objDynamic.Add(ApplianceNameMaster);
        //            }
        //            if (ds.Tables.Count > 5)
        //            {
        //                var myEnumerablecatcode = ds.Tables[5].AsEnumerable();
        //                List<Import_Client_CategoryCode_MasterDTO> CatCodeList =
        //                   (from item in myEnumerablecatcode
        //                    select new Import_Client_CategoryCode_MasterDTO
        //                    {
        //                        Import_Client_CategoryCodes_PkeyId = item.Field<Int64>("Import_Client_CategoryCodes_PkeyId"),
        //                        Import_Client_CategoryCodes_Code = item.Field<String>("Import_Client_CategoryCodes_Code"),

        //                    }).ToList();

        //                objDynamic.Add(CatCodeList);
        //            }
        //            if (ds.Tables.Count > 6)
        //            {
        //                var myEnumerableworkType = ds.Tables[6].AsEnumerable();
        //                List<WorkTypeMasterDTO> workTypeList =
        //                   (from item in myEnumerableworkType
        //                    select new WorkTypeMasterDTO
        //                    {
        //                        WT_pkeyID = item.Field<Int64>("WT_pkeyID"),
        //                        WT_WorkType = item.Field<String>("WT_WorkType"),
        //                        Allow_Selection = true,
        //                    }).ToList();

        //                objDynamic.Add(workTypeList);
        //            }
        //            if (ds.Tables.Count > 7)
        //            {
        //                var myEnumerableworkGrpType = ds.Tables[7].AsEnumerable();
        //                List<WorkTypeCategoryDTO> workTypeGrpList =
        //                   (from item in myEnumerableworkGrpType
        //                    select new WorkTypeCategoryDTO
        //                    {
        //                        Work_Type_Cat_pkeyID = item.Field<Int64>("Work_Type_Cat_pkeyID"),
        //                        Work_Type_Name = item.Field<String>("Work_Type_Name"),

        //                    }).ToList();

        //                objDynamic.Add(workTypeGrpList);
        //            }

        //        }
        //        else  if (dropDownMasterDTO.Type == 2)
        //        {
        //            for (int i = 0; i < ds.Tables.Count; i++)
        //            {
        //                objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
        //            }
        //        }



        //    }
        //    catch (Exception ex)
        //    {
        //        log.logErrorMessage(ex.StackTrace);
        //        log.logErrorMessage(ex.Message);
        //    }

        //    return objDynamic;
        //}
        #endregion
        //Get drop down
        public List<dynamic> GetTaskConfigurationDropDown(DropDownMasterDTO dropDownMasterDTO)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetTaskConfigurationDropDownData(dropDownMasterDTO);
                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        private DataSet GetTaskConfigurationDropDownData(DropDownMasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Task_Configuration_DropDown]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@WorkOrderID", 1 + "#bigint#" + model.WorkOrderID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        // import damage item drd
        private DataSet GetDropDownDamageItems()
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Import_Client_DamageItem_drd]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();


                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }
        public List<dynamic> GetImportDamageItemsDropDown()
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetDropDownDamageItems();

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Import_Client_DamageItems_Drd> damageitems =
                   (from item in myEnumerableFeaprd
                    select new Import_Client_DamageItems_Drd
                    {
                        Import_Client_DamageItem_PkeyID = item.Field<Int64>("Import_Client_DamageItem_PkeyID"),
                        Import_Client_DamageItem_Name = item.Field<String>("Import_Client_DamageItem_Name"),

                    }).ToList();

                objDynamic.Add(damageitems);



            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        private DataSet GetImportClientDrd(ImportClient model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Import_Client_Details]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();
                input_parameters.Add("@WI_ImportFrom", 1 + "#bigint#" + model.WI_ImportFrom);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetImportClientDrdDetails(ImportClient importClient)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetImportClientDrd(importClient);


                var myEnumerableFeaprdch = ds.Tables[0].AsEnumerable();
                List<WorkClientMaster> client =
                   (from item in myEnumerableFeaprdch
                    select new WorkClientMaster
                    {
                        Client_pkeyID = item.Field<Int64>("Client_pkeyID"),
                        Client_Company_Name = item.Field<String>("Client_Company_Name"),

                    }).ToList();
                objDynamic.Add(client);

                var myEnumerableFeaprd = ds.Tables[1].AsEnumerable();

                List<ImportClient> importname =
                   (from item in myEnumerableFeaprd
                    select new ImportClient
                    {
                        WI_Pkey_ID = item.Field<Int64>("WI_Pkey_ID"),
                        WI_FriendlyName = item.Field<String>("WI_FriendlyName"),

                    }).ToList();
                objDynamic.Add(importname);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        public List<dynamic> GetWorkOrderIPLNumberlist(DropDownMasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = WorkOrderIPLNumberlist(model);
                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objDynamic;
        }
        private DataSet WorkOrderIPLNumberlist(DropDownMasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_WorkOrder_IPLNumber_List]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@workOrder_ID", 1 + "#bigint#" + model.WorkOrderID);
                input_parameters.Add("@IPLNo", 1 + "#varchar#" + model.IPLNo);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> Createworktypeviewdependency(DropDownWorkTypeMasterDTO dropDownWorkTypeMasterDTO)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetDropDownDataWorkTypeData(dropDownWorkTypeMasterDTO);
                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        private DataSet GetDropDownDataWorkTypeData(DropDownWorkTypeMasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_DropDown_New_WorkType]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();
                input_parameters.Add("@WorkOrderID", 1 + "#bigint#" + model.WorkOrderID);
                input_parameters.Add("@ClientID", 1 + "#bigint#" + model.ClientID);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> CreateTasktypeviewdependency(DropDownWorkTypeMasterDTO dropDownWorkTypeMasterDTO)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetDropDownDataTaskTypeData(dropDownWorkTypeMasterDTO);
                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        private DataSet GetDropDownDataTaskTypeData(DropDownWorkTypeMasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_DropDown_New_TaskType]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();
                input_parameters.Add("@WorkOrderID", 1 + "#bigint#" + model.WorkOrderID);
                input_parameters.Add("@ClientID", 1 + "#bigint#" + model.ClientID);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetDropDownForAdminnew(DropDownNewMasterDTO dropDownNewMasterDTO)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetDropDownDataForAdmin(dropDownNewMasterDTO);
                for (int i = 0; i < ds.Tables.Count; i++)
                {
                    objDynamic.Add(obj.AsDynamicEnumerable(ds.Tables[i]));
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        private DataSet GetDropDownDataForAdmin(DropDownNewMasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_DropDown_For_Admin]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();
                input_parameters.Add("@WorkOrderID", 1 + "#bigint#" + model.WorkOrderID);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@PageID", 1 + "#int#" + model.PageID);
                input_parameters.Add("@WhereClause", 1 + "#nvarchar#" + model.WhereClause);
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }
    }
}