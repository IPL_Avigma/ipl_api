﻿using Avigma.Repository.Lib;
using IPL.Models.RepairBase;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.RepairBase
{
    public class RepairBase_Document_Maste_Data
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        private List<dynamic> AddUpdateRepairBase_Document_Maste_Data(RepairBase_Document_Maste_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_RepairBase_Document_Master]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Rep_Base_PKeyID", 1 + "#bigint#" + model.Rep_Base_PKeyID);
                input_parameters.Add("@Rep_Base_OrderId", 1 + "#bigint#" + model.Rep_Base_OrderId);
                input_parameters.Add("@Rep_Base_RepairAreaId", 1 + "#bigint#" + model.Rep_Base_RepairAreaId);
                input_parameters.Add("@Rep_Base_RepairId", 1 + "#bigint#" + model.Rep_Base_RepairId);
                input_parameters.Add("@Rep_Base_Doc_File_Path", 1 + "#nvarchar#" + model.Rep_Base_Doc_File_Path);
                input_parameters.Add("@Rep_Base_Doc_File_Size", 1 + "#nvarchar#" + model.Rep_Base_Doc_File_Size);
                input_parameters.Add("@Rep_Base_Doc_File_Name", 1 + "#nvarchar#" + model.Rep_Base_Doc_File_Name);
                input_parameters.Add("@Rep_Base_Doc_BucketName", 1 + "#nvarchar#" + model.Rep_Base_Doc_BucketName);
                input_parameters.Add("@Rep_Base_Doc_ProjectID", 1 + "#nvarchar#" + model.Rep_Base_Doc_ProjectID);
                input_parameters.Add("@Rep_Base_Doc_Object_Name", 1 + "#nvarchar#" + model.Rep_Base_Doc_Object_Name);
                input_parameters.Add("@Rep_Base_Doc_Folder_Name", 1 + "#nvarchar#" + model.Rep_Base_Doc_Folder_Name);
                input_parameters.Add("@Rep_Base_Doc_UploadedBy", 1 + "#nvarchar#" + model.Rep_Base_Doc_UploadedBy);
                input_parameters.Add("@Rep_Base_Doc_RepUserID_ID", 1 + "#bigint#" + model.Rep_Base_Doc_RepUserID_ID);
                input_parameters.Add("@Rep_Base_Doc_Company_Id", 1 + "#bigint#" + model.Rep_Base_Doc_Company_Id);
                input_parameters.Add("@Rep_Base_Doc_Type", 1 + "#int#" + model.Rep_Base_Doc_Type);
                input_parameters.Add("@Rep_Base_IsActive", 1 + "#bit#" + model.Rep_Base_IsActive);
                input_parameters.Add("@Rep_Base_IsDelete", 1 + "#bit#" + model.Rep_Base_IsDelete);

                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserId);
                input_parameters.Add("@Rep_Base_PKeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);
                objData = obj.SqlCRUD(insertProcedure, input_parameters);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

            }
            return objData;



        }
        public List<dynamic> AddRepairBase_Document_Maste_Data(RepairBase_Document_Maste_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                objData = AddUpdateRepairBase_Document_Maste_Data(model);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        private DataSet Get_RepairBase_Document_master(RepairBase_Document_Maste_DTO model)
        {
            DataSet ds = null;
            try
            {

                string selectProcedure = "[Get_RepairBase_Document_master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Rep_Base_PKeyID", 1 + "#bigint#" + model.Rep_Base_PKeyID);

                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {

                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }




            return ds;
        }

        public List<dynamic> Get_RepairBase_Document_masterDetails(RepairBase_Document_Maste_DTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = Get_RepairBase_Document_master(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<RepairBase_Document_Maste_DTO> RepairBase_Document_Maste =
                   (from item in myEnumerableFeaprd
                    select new RepairBase_Document_Maste_DTO
                    {
                        Rep_Base_PKeyID = item.Field<Int64>("Rep_Base_PKeyID"),
                        Rep_Base_OrderId = item.Field<Int64?>("Rep_Base_OrderId"),
                        Rep_Base_RepairAreaId = item.Field<Int64?>("Rep_Base_RepairAreaId"),
                        Rep_Base_RepairId = item.Field<Int64?>("Rep_Base_RepairId"),
                        Rep_Base_Doc_File_Path = item.Field<String>("Rep_Base_Doc_File_Path"),
                        Rep_Base_Doc_File_Size = item.Field<String>("Rep_Base_Doc_File_Size"),
                        Rep_Base_Doc_File_Name = item.Field<String>("Rep_Base_Doc_File_Name"),
                        Rep_Base_Doc_BucketName = item.Field<String>("Rep_Base_Doc_BucketName"),
                        Rep_Base_Doc_ProjectID = item.Field<String>("Rep_Base_Doc_ProjectID"),
                        Rep_Base_Doc_Object_Name = item.Field<String>("Rep_Base_Doc_Object_Name"),
                        Rep_Base_Doc_Folder_Name = item.Field<String>("Rep_Base_Doc_Folder_Name"),
                        Rep_Base_Doc_UploadedBy = item.Field<String>("Rep_Base_Doc_UploadedBy"),
                        Rep_Base_Doc_RepUserID_ID = item.Field<Int64?>("Rep_Base_Doc_RepUserID_ID"),
                        Rep_Base_Doc_Company_Id = item.Field<Int64?>("Rep_Base_Doc_Company_Id"),
                        Rep_Base_Doc_Type = item.Field<int?>("Rep_Base_Doc_Type"),
                        Rep_Base_IsActive = item.Field<Boolean?>("Rep_Base_IsActive"),






                    }).ToList();

                objDynamic.Add(RepairBase_Document_Maste);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

            }

            return objDynamic;
        }
    }
}