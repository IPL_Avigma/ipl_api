﻿using Avigma.Repository.Lib;
using IPL.Models.RepairBase;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.RepairBase
{
    public class RepairBase_User_Master_Data
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        private List<dynamic> AddUpdateRepairBase_User_Master_Data(RepairBase_User_Master_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_RepairBase_User_Master]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Rep_UM_PKeyID", 1 + "#bigint#" + model.Rep_UM_PKeyID);
                input_parameters.Add("@Rep_UM_Username", 1 + "#varchar#" + model.Rep_UM_Username);
                input_parameters.Add("@Rep_UM_Password", 1 + "#varchar#" + model.Rep_UM_Password);
                input_parameters.Add("@Rep_UM_ApiAuthorization", 1 + "#varchar#" + model.Rep_UM_ApiAuthorization);
                input_parameters.Add("@Rep_UM_Apikey", 1 + "#varchar#" + model.Rep_UM_Apikey);
                input_parameters.Add("@Rep_UM_ProductType", 1 + "#int#" + model.Rep_UM_ProductType);
                input_parameters.Add("@Rep_UM_Token", 1 + "#nvarchar#" + model.Rep_UM_Token);
                input_parameters.Add("@Rep_Um_Token_Count", 1 + "#bigint#" + model.Rep_Um_Token_Count);
                input_parameters.Add("@Rep_UM_Token_IsValid", 1 + "#bit#" + model.Rep_UM_Token_IsValid);
                input_parameters.Add("@Rep_UM_Token_StartDate", 1 + "#datetime#" + model.Rep_UM_Token_StartDate);
                input_parameters.Add("@Rep_UM_Token_ExpiryDate", 1 + "#datetime#" + model.Rep_UM_Token_ExpiryDate);
                input_parameters.Add("@Rep_UM_IsActive", 1 + "#bit#" + model.Rep_UM_IsActive);
                input_parameters.Add("@Rep_UM_IsDelete", 1 + "#bit#" + model.Rep_UM_IsDelete);

                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserId);
                input_parameters.Add("@Rep_UM_PKeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);
                objData = obj.SqlCRUD(insertProcedure, input_parameters);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

            }
            return objData;



        }
        public List<dynamic> AddRepairBase_User_Master_Data(RepairBase_User_Master_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                objData = AddUpdateRepairBase_User_Master_Data(model);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        private DataSet Get_RepairBase_User_Master(RepairBase_User_Master_DTO model)
        {
            DataSet ds = null;
            try
            {

                string selectProcedure = "[Get_RepairBase_User_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Rep_UM_PKeyID", 1 + "#bigint#" + model.Rep_UM_PKeyID);

                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {

                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }




            return ds;
        }

        public List<dynamic> Get_RepairBase_User_MasterDetails(RepairBase_User_Master_DTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = Get_RepairBase_User_Master(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<RepairBase_User_Master_DTO> RepairBase_User_Master =
                   (from item in myEnumerableFeaprd
                    select new RepairBase_User_Master_DTO
                    {
                        Rep_UM_PKeyID = item.Field<Int64>("Rep_UM_PKeyID"),
                        Rep_UM_Username = item.Field<String>("Rep_UM_Username"),
                        Rep_UM_Password = item.Field<String>("Rep_UM_Password"),
                        Rep_UM_ApiAuthorization = item.Field<String>("Rep_UM_ApiAuthorization"),
                        Rep_UM_Apikey = item.Field<String>("Rep_UM_Apikey"),
                        Rep_UM_ProductType = item.Field<int?>("Rep_UM_ProductType"),
                        Rep_UM_Token = item.Field<String>("Rep_UM_Token"),
                        Rep_Um_Token_Count = item.Field<Int64?>("Rep_Um_Token_Count"),
                        Rep_UM_Token_IsValid = item.Field<Boolean?>("Rep_UM_Token_IsValid"),
                        Rep_UM_Token_StartDate = item.Field<DateTime?>("Rep_UM_Token_StartDate"),
                        Rep_UM_Token_ExpiryDate = item.Field<DateTime?>("Rep_UM_Token_ExpiryDate"),
                        Rep_UM_IsActive = item.Field<Boolean?>("Rep_UM_IsActive"),
                        Rep_UM_TestApiUrl = item.Field<String>("Rep_UM_TestApiUrl"),
                        Rep_UM_LiveApiUrl = item.Field<String>("Rep_UM_LiveApiUrl"),
                        Rep_UM_CreatedBy = item.Field<String>("Rep_UM_CreatedBy"),
                        Rep_UM_ModifiedBy = item.Field<String>("Rep_UM_ModifiedBy"),

                    }).ToList();

                objDynamic.Add(RepairBase_User_Master);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

            }

            return objDynamic;
        }
    }
}