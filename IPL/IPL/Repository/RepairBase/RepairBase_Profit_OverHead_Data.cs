﻿using Avigma.Repository.Lib;
using IPL.Models.RepairBase;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Repository.RepairBase
{
    public class RepairBase_Profit_OverHead_Data
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        private List<dynamic> AddUpdateRepairBase_Profit_OverHead_Data(RepairBase_Profit_OverHead_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_RepairBase_Profit_OverHead]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Rep_PKeyID", 1 + "#bigint#" + model.Rep_PKeyID);
                input_parameters.Add("@Rep_OrderId", 1 + "#bigint#" + model.Rep_OrderId);
                input_parameters.Add("@Rep_Profit", 1 + "#int#" + model.Rep_Profit);
                input_parameters.Add("@Rep_Overhead", 1 + "#int#" + model.Rep_Overhead);
                input_parameters.Add("@Rep_Userid", 1 + "#bigint#" + model.Rep_Userid);
                input_parameters.Add("@Rep_CompanyID", 1 + "#bigint#" + model.Rep_CompanyID);
                input_parameters.Add("@Rep_IsActive", 1 + "#bit#" + model.Rep_IsActive);
                input_parameters.Add("@Rep_IsDelete", 1 + "#bit#" + model.Rep_IsDelete);

                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserId);
                input_parameters.Add("@Rep_PKeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);
                objData = obj.SqlCRUD(insertProcedure, input_parameters);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

            }
            return objData;



        }
        public List<dynamic> AddRepairBase_Profit_OverHead_Data(RepairBase_Profit_OverHead_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                objData = AddUpdateRepairBase_Profit_OverHead_Data(model);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        private DataSet Get_RepairBase_Profit_OverHead(RepairBase_Profit_OverHead_DTO model)
        {
            DataSet ds = null;
            try
            {

                string selectProcedure = "[Get_RepairBase_Profit_OverHead]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Rep_PKeyID", 1 + "#bigint#" + model.Rep_PKeyID);

                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {

                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return ds;
        }

        public List<dynamic> Get_RepairBase_Profit_OverHeadDetails(RepairBase_Profit_OverHead_DTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = Get_RepairBase_Profit_OverHead(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<RepairBase_Profit_OverHead_DTO> RepairBase_Profit_OverHead =
                   (from item in myEnumerableFeaprd
                    select new RepairBase_Profit_OverHead_DTO
                    {
                        Rep_PKeyID = item.Field<Int64>("Rep_PKeyID"),
                        Rep_OrderId = item.Field<Int64?>("Rep_OrderId"),
                        Rep_Profit = item.Field<int?>("Rep_Profit"),
                        Rep_Overhead = item.Field<int?>("Rep_Overhead"),
                        Rep_Userid = item.Field<Int64?>("Rep_Userid"),
                        Rep_CompanyID = item.Field<Int64?>("Rep_CompanyID"),
                        Rep_IsActive = item.Field<Boolean?>("Rep_IsActive"),

                    }).ToList();

                objDynamic.Add(RepairBase_Profit_OverHead);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

            }

            return objDynamic;
        }
    }
}