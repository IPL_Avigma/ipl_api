﻿using Avigma.Models;
using IPL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO.Compression;
using System.IO;
using RestSharp;
using System.Net;
using System.Net.Http;

namespace Avigma.Repository.Lib
{
    public class FileUploadDownload
    {
        Log log = new Log();
        public void ExtractZipFile(FileUploadDownloadDTO fileUploadDownloadDTO)
        {
            try
            {
                ZipFile.ExtractToDirectory(fileUploadDownloadDTO.zipPath, fileUploadDownloadDTO.extractPath);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            
        }

        public List<dynamic> UploadPPWphotos(FileUploadDownloadDTO fileUploadDownloadDTO)
        {
            List<dynamic> objdata = new List<dynamic>();
            string strURL = System.Configuration.ConfigurationManager.AppSettings["IntegrationURL"] + "upload";
            ImageGenerator imageGenerator = new ImageGenerator();
            try
            {
                DirectoryInfo di = new DirectoryInfo(fileUploadDownloadDTO.DirectoryPath);
                string filename = string.Empty, strsplit = string.Empty, withoutNumbers = string.Empty, 
                    Taskname = string.Empty,filepath = string.Empty,strBase64 = string.Empty, ReqData = string.Empty;
                bool boolBefore = false, boolDuring = false, boolAfter = false,boolBid = false;
                FileInfo[] Images = di.GetFiles();
                int labelid = 0;
                log.logDebugMessage("File upload PPW number--------------->"+ fileUploadDownloadDTO.PPW_Number);
                for (int i = 0; i < Images.Length; i++)
                {
                     filename = Images[i].Name;
                    log.logDebugMessage("File upload File Name------------>" + filename);
                    strsplit = filename.Split('.')[0];
                     boolBefore = System.Text.RegularExpressions.Regex.IsMatch(strsplit, "Before");  //1
                      boolDuring = System.Text.RegularExpressions.Regex.IsMatch(strsplit, "During"); //2
                      boolAfter = System.Text.RegularExpressions.Regex.IsMatch(strsplit, "After"); //3
                      boolBid = System.Text.RegularExpressions.Regex.IsMatch(strsplit, "Bid"); //3

                    if (boolBefore)
                    {
                        withoutNumbers = System.Text.RegularExpressions.Regex.Replace(strsplit, "[0-9]", "");
                        Taskname = withoutNumbers.Replace("Before", "").Replace("_", "");
                        labelid = 1;
                    }
                    else if (boolDuring)
                    {
                        withoutNumbers = System.Text.RegularExpressions.Regex.Replace(strsplit, "[0-9]", "");
                        Taskname = withoutNumbers.Replace("During", "").Replace("_", "");
                        labelid = 2;
                    }
                    else if (boolAfter)
                    {
                        withoutNumbers = System.Text.RegularExpressions.Regex.Replace(strsplit, "[0-9]", "");
                        Taskname = withoutNumbers.Replace("After", "").Replace("_", "");
                        labelid = 3;
                    }
                    else if (boolAfter)
                    {
                        withoutNumbers = System.Text.RegularExpressions.Regex.Replace(strsplit, "[0-9]", "");
                        Taskname = withoutNumbers.Replace("Bid", "").Replace("_", "");
                        labelid = 4;
                    }
                    filepath = fileUploadDownloadDTO.DirectoryPath + "\\"+filename;
                    strBase64 = imageGenerator.ImagefiletoBase64(filepath);
                    ReqData = "{\r\n \"IPLNO\": \"" + "ppw_"+fileUploadDownloadDTO.PPW_Number + "\"\r " +
                        ",\r\n \"Client_Result_Photo_StatusType\": " + labelid + 
                        ",\r\n \"Client_Result_Photo_Wo_ID\": \"" + fileUploadDownloadDTO.importpkey + "\"\r\n " +
                         ",\r\n \"Client_Result_Photo_FileName\": \"" + filename + "\"\r\n " +
                           ",\r\n \"Client_Result_Photo_IsActive\": " + 1 +
                             ",\r\n \"Client_Result_Photo_Type\": " + 1 +
                              ",\r\n \"ReqType\": " + 1 + 
                                 ",\r\n \"ContentType\": " + 4 +
                                   ",\r\n \"Client_Result_Photo_Ch_ID\": " + fileUploadDownloadDTO.WI_PkeyID +
                                     ",\r\n \"Client_Result_Photo_ID\": " + 0 +
                              ",\r\n \"Client_Result_Photo_FilePath\": \"" + "" + "\"\r\n " +
                                ",\r\n \"Client_Result_File_Desc\": \"" + Taskname + "\"\r\n " +
                                   ",\r\n \"Inst_Doc_Inst_Ch_ID\": " + labelid +
                                     ",\r\n \"Type\": " + 1 + 
                                  ",\r\n \"Image\": \"data:image/png;base64," + strBase64 + "\"\r\n " +
                                    ",\r\n \"ImageLg\": \"" + ""+ "\"\r\n " +
                                      ",\r\n \"ImageSm\": \"" + "" + "\"\r\n " +
                                        ",\r\n \"workOrderNumber\": \"" + fileUploadDownloadDTO.PPW_Number + "\"\r\n " +

                           "}";
                    string resp = CallAPI(strURL, ReqData);
                    log.logDebugMessage("File upload Response------------->" + resp);

                    objdata.Add(resp);
                    File.Delete(filepath);
                  
                }
                Directory.Delete(fileUploadDownloadDTO.DirectoryPath, true);
                File.Delete(fileUploadDownloadDTO.zipPath);

            }
            catch (Exception ex)
            {
                Directory.Delete(fileUploadDownloadDTO.DirectoryPath, true);
                File.Delete(fileUploadDownloadDTO.zipPath);
                log.logErrorMessage("Error in Folder ------------->"+ fileUploadDownloadDTO.DirectoryPath);
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
          }
            return objdata;

        }
        public List<dynamic> UploadEmailDocument(Mass_File_MasterDTO model)
        {
            List<dynamic> objdata = new List<dynamic>();
            string strURL = System.Configuration.ConfigurationManager.AppSettings["IntegrationURL"] + "upload";
            string ReqData = string.Empty , strBase64 = string.Empty,strApp = string.Empty;
            int ContentType = 0;
            ImageGenerator imageGenerator = new ImageGenerator();
            try
            {
                if(model.Mass_File_Ext.ToLower() == "png" || model.Mass_File_Ext.ToLower() == "jpeg" || model.Mass_File_Ext.ToLower() == "jpg")
                {
                    ContentType = 6;
                    strApp = "image/png";
                    strBase64 = imageGenerator.ImagefiletoBase64(model.Mass_File_FilePth);    
                }
                else
                {
                    ContentType = 5;
                    strApp = "application/" + model.Mass_File_Ext;
                    strBase64 = imageGenerator.ReadFileDataToBase64(model.Mass_File_FilePth);
                }
                
                ReqData = "{\r\n \"IPLNO\": \"" + "EmailDocuments" + "\"\r " +
                        ",\r\n \"Client_Result_Photo_StatusType\": " + "1" +
                        ",\r\n \"Client_Result_Photo_Wo_ID\": \"" + model.Mass_File_PkeyId + "\"\r\n " +
                         ",\r\n \"Client_Result_Photo_FileName\": \"" + model.Mass_File_FileName + "\"\r\n " +
                           ",\r\n \"Client_Result_Photo_IsActive\": " + 1 +
                             ",\r\n \"Client_Result_Photo_Type\": " + 1 +
                              ",\r\n \"ReqType\": " + 1 +
                                 ",\r\n \"ContentType\": " + ContentType +
                                   ",\r\n \"Client_Result_Photo_Ch_ID\": " + model.Mass_File_Email_ID +
                                     ",\r\n \"Client_Result_Photo_ID\": " + 0 +
                              ",\r\n \"Client_Result_Photo_FilePath\": \"" + "" + "\"\r\n " +
                                ",\r\n \"Client_Result_File_Desc\": \"" + "" + "\"\r\n " +
                                   ",\r\n \"Inst_Doc_Inst_Ch_ID\": " + "123" +
                                     ",\r\n \"Type\": " + model.Type +
                                    ",\r\n \"Image\": \"data:"+ strApp + ";base64," + strBase64 + "\"\r\n " +
                                    ",\r\n \"ImageLg\": \"" + "" + "\"\r\n " +
                                      ",\r\n \"ImageSm\": \"" + "" + "\"\r\n " +
                                        ",\r\n \"workOrderNumber\": \"" + "" + "\"\r\n " +

                           "}";

                string resp = CallAPI(strURL, ReqData);
                log.logDebugMessage("File upload Response------------->" + resp);
            }
            catch (Exception ex)
            {

                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }
            return objdata;

        }
            public string CallAPI(string URL, string DATA)
        {
            string responseval = string.Empty;

            try
            {
                var client = new RestClient(URL);
                var request = new RestRequest(Method.POST);

                log.logInfoMessage("Request FireBase URL ---------->"+URL);
                log.logInfoMessage("Request FireBase DATA ---------->" + DATA);

                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Connection", "keep-alive");
                request.AddHeader("Content-Length", "70");
                request.AddHeader("Accept-Encoding", "gzip, deflate");
                request.AddHeader("Host", "us-central1-rare-lambda-245821.cloudfunctions.net");
                request.AddHeader("Postman-Token", "9426244d-41e4-486e-aa06-55ed1abbeec0,1df2c66a-58d9-4e0e-b804-7585fdd70002");
                request.AddHeader("Cache-Control", "no-cache");
                request.AddHeader("Accept", "*/*");
                request.AddHeader("User-Agent", "PostmanRuntime/7.20.1");
                request.AddHeader("Content-Type", "application/json");
                ///request.AddParameter("undefined", "{\r\n \"FileName\": \"Test.jpg\",\r\n \"FolderName\": \"Name of Folder in IPL\"\r\n}", ParameterType.RequestBody);
                request.AddParameter("undefined", DATA, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                log.logDebugMessage("Request FireBase");
                log.logDebugMessage(request.ToString());
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    responseval = response.Content;
                }
                else
                {
                    responseval = "Error While Retving";
                    log.logErrorMessage("<-------------- API Response----------->" + response.StatusCode.ToString());
                }


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                responseval = "Error";
            }

            return responseval;

        }

    }
}