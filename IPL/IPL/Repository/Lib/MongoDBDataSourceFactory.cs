﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IPLApp;
using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Configuration;
namespace Avigma.Repository.Lib
{
    public class MongoDBDataSourceFactory
    {
        IPL_MongoDBconnectionString iPL_MongoDBconnectionString = new IPL_MongoDBconnectionString();
        Log log = new Log();
        string DataBaseName = ConfigurationManager.AppSettings["MongoDB"].ToString();
        public int InsertRecord<T>(string table, T record)
        {
            int Response = 1;
            try
            {
                IMongoDatabase db = iPL_MongoDBconnectionString.GetConnection(DataBaseName);
                var collection = db.GetCollection<T>(table);
                collection.InsertOne(record);
            }
            catch (Exception ex)
            {
                Response = 0;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message); 
            }
            return Response;
        }
        public int UpsertRecord<T>(string table, Guid id, T record)
        {
            int Response = 1;
            try
            {
                IMongoDatabase db = iPL_MongoDBconnectionString.GetConnection(DataBaseName);
                var collections = db.GetCollection<T>(table);
                var result = collections.ReplaceOne(
                     new BsonDocument("_id", id),
                     record,
                     new UpdateOptions { IsUpsert = true });
            }
            catch (Exception ex)
            {
                Response = 0;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return Response;

        }

        public int DeleteRecord<T>(string table, Guid id,string ColumnName)
        {
            int Response = 1;
            try
            {
                IMongoDatabase db = iPL_MongoDBconnectionString.GetConnection(DataBaseName);
                var collection = db.GetCollection<T>(table);
                var filter = Builders<T>.Filter.Eq(ColumnName, id);
                collection.DeleteOne(filter);
            }
            catch (Exception ex)
            {
                Response = 0;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return Response;
        }

        public List<T> LoadRecords<T>(string table)
        {
            try
            {
                IMongoDatabase db = iPL_MongoDBconnectionString.GetConnection(DataBaseName);
                var collection = db.GetCollection<T>(table);
                return collection.Find(new BsonDocument()).ToList();
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return null;
            }
           
        }
        public T LoadRecordById<T>(string table, Guid id, string ColumnName)
        {
            IMongoDatabase db = iPL_MongoDBconnectionString.GetConnection(DataBaseName);
            var collection = db.GetCollection<T>(table);
            var filter = Builders<T>.Filter.Eq(ColumnName, id);

            return collection.Find(filter).First();
        }

    }
}