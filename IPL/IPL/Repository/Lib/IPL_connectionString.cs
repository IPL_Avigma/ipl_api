﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using MongoDB.Driver;

namespace IPLApp
{
    public class IPL_connectionString
    {
        public static string connectionString;
       

        public static SqlConnection GetConnection()
        {
            connectionString = ConfigurationManager.ConnectionStrings["IPL_dBcon"].ToString();

            SqlConnection connection = new SqlConnection(connectionString);
            return connection;
        }


       

    }


    public class IPL_MongoDBconnectionString
    {
        public   IMongoDatabase IMongoDatabase;
        public IMongoDatabase GetConnection(string database)
        {
            string MongoDbconnectionString = ConfigurationManager.ConnectionStrings["IPL_MongodBcon"].ToString();
            var cilent = new MongoClient(MongoDbconnectionString + "&w=majority");
            IMongoDatabase = cilent.GetDatabase(database);
            return IMongoDatabase;
        }
    }


    }