﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Json.Net;
using Newtonsoft.Json.Linq;
using Avigma.Repository.Lib;
namespace Avigma.Repository.Lib
{
    public class JsonValidate
    {
        Log log = new Log();
        public bool IsValidJson(string strInput)
        {
            strInput = strInput.Trim();
            if ((strInput.StartsWith("{") && strInput.EndsWith("}")) || //For object
                (strInput.StartsWith("[") && strInput.EndsWith("]"))) //For array
            {
                try
                {
                   
                    var obj = JToken.Parse(strInput);
                    return true;
                }
                catch (Newtonsoft.Json.JsonReaderException jex)
                {
                    //Exception in parsing json
                    
                    log.logErrorMessage(jex.Message);
                    log.logErrorMessage(jex.StackTrace);
                    log.logErrorMessage(jex.LinePosition.ToString());
                    return false;
                }
                catch (Exception ex) //some other exception
                {
                    log.logErrorMessage(ex.Message);
                    log.logErrorMessage(ex.StackTrace);
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
    }
}