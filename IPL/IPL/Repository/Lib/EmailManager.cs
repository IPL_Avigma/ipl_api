﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Net.Mime;
using System.Net;
using System.Configuration;
using System.Text;
using System.IO;
using Avigma.Repository.Lib;
using Avigma.Models;
using IPL.Models;
using System.Threading.Tasks;

namespace Avigma.Repository.Lib
{
    public class EmailManager
    {
        SmtpClient smtpClient = new SmtpClient();
        MailMessage message = new MailMessage();

        Log log = new Log();

        public void AddAttachment(System.IO.Stream memorystream, string filename, string mediatype)
        {
            message.Attachments.Add(new System.Net.Mail.Attachment(memorystream, filename, mediatype));
        }


        public int SendEmail(EmailDTO emailDTO)
        {
            try
            {
                smtpClient.Host = ConfigurationManager.AppSettings["smtpServer"].ToString();
                smtpClient.Port = Convert.ToInt32(ConfigurationManager.AppSettings["smtpPort"]);
                smtpClient.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]);
                string fromuserid = ConfigurationManager.AppSettings["smtpUser"].ToString();
                string frompasswd = ConfigurationManager.AppSettings["smtpPass"].ToString();
                emailDTO.From = fromuserid;

                message.From = new MailAddress(emailDTO.From);
                if (!string.IsNullOrEmpty(emailDTO.To))
                {
                    message.To.Add(emailDTO.To);
                    if (!string.IsNullOrEmpty(emailDTO.CC))
                    {
                        message.CC.Add(emailDTO.CC);
                    }
                    emailDTO.Subject = emailDTO.Subject.Replace('\r', ' ').Replace('\n', ' ');
                    message.Subject = emailDTO.Subject;
                   
                    //message.IsBodyHtml = emailDTO.IsBodyHtml;
                    message.IsBodyHtml = true;
                    message.Body = emailDTO.Message + "<br/> <br/>  " + emailDTO.MainBody;

                    //smtpClient.Credentials = new NetworkCredential("pritiarora@interactcrm.com", "opa5678");
                    smtpClient.UseDefaultCredentials = false;
                    smtpClient.Credentials = new NetworkCredential(fromuserid, frompasswd);

                    smtpClient.Send(message);
                }
                else
                {
                    log.logDebugMessage("Address is Null ------------------> No Email sent");
                }
               

                return 1;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                return 0;
            }
        }

        // sent mail with attechments
        public async Task<List<dynamic>> email_send(EmailDTO emailDTO)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                smtpClient.Host = ConfigurationManager.AppSettings["smtpServer"].ToString();
                smtpClient.Port = Convert.ToInt32(ConfigurationManager.AppSettings["smtpPort"]);
                smtpClient.EnableSsl = Convert.ToBoolean(ConfigurationManager.AppSettings["EnableSsl"]);
                string fromuserid = ConfigurationManager.AppSettings["smtpUser"].ToString();
                string frompasswd = ConfigurationManager.AppSettings["smtpPass"].ToString();

                if (!string.IsNullOrEmpty(emailDTO.To))
                {


                    System.Net.Mail.Attachment attachment;
                    for (int i = 0; i < emailDTO.Attachmentarr.Count; i++)
                    {

                        attachment = new System.Net.Mail.Attachment(emailDTO.Attachmentarr[i]);
                        message.Attachments.Add(attachment);
                    }


                    message.From = new MailAddress(fromuserid);
                    message.To.Add(emailDTO.To);


                    message.Subject = emailDTO.Subject;
                    //message.IsBodyHtml = emailDTO.IsBodyHtml;
                    message.IsBodyHtml = true;
                    message.Body = emailDTO.MainBody;

                    //smtpClient.Credentials = new NetworkCredential("pritiarora@interactcrm.com", "opa5678");
                    smtpClient.UseDefaultCredentials = false;
                    smtpClient.Credentials = new NetworkCredential(fromuserid, frompasswd);


                    smtpClient.SendCompleted += (s, e) =>
                    {
                        //delete attached files
                        for (int j = 0; j < emailDTO.Attachmentarr.Count; j++)
                        {
                            message.Attachments.Dispose();
                            smtpClient.Dispose();
                            File.Delete(emailDTO.Attachmentarr[j]);
                        }
                    };
                    await Task.Run(() => smtpClient.SendMailAsync(message));
                }
                else
                {
                    log.logDebugMessage("Address is Null ------------------> No Email sent");
                }
                objDynamic.Add("1");
                return objDynamic;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                return objDynamic;
            }



           


        }
    }
}