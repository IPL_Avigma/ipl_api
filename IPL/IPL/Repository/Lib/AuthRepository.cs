﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

using Microsoft.Owin.Security.OAuth;
using System.Security.Claims;

using static Avigma.Repository.Lib.CustomAuthorizationServerProvider;
using IPL.Models;
using IPL.Repository.data;

namespace Avigma.Repository.Lib
{
  
    public class AuthRepository
    {
        public Int64 ValidateUser(AuthLoginDTO model)
        {
            Int64 retval = 0;
            Log log = new Log();
            try
            {
                UserLoginMaster userLoginMaster = new UserLoginMaster();

                List<dynamic> objDynamic = new List<dynamic>();
               
                switch (model.Type)
                {
                    case 3:
                        {
                            model.Type = 3;
                            break;
                        }
                    case 5:
                        {
                            model.Type = 5;
                            break;
                        }
                    default:
                        {
                            model.Type = 1;
                            break;
                        }
                }

                objDynamic = userLoginMaster.GetAuthenticateLoginDetails(model);
                if (objDynamic[0].Count > 0)
                {
                    retval = (objDynamic[0][0]).User_pkeyID;
                }
                //retval = objDynamic[0][0].Stud_login_pkeyId;
                
            }
            catch (Exception ex)
            {

                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }
            return retval;

        }

        public Int64 AdminValidateUser(IPL_Admin_User_LoginDTO model)
        {
            Int64 retval = 0;
            Log log = new Log();
            try
            {
                IPL_Admin_User_LoginData iPL_Admin_User_LoginData = new IPL_Admin_User_LoginData();

                List<dynamic> objDynamic = new List<dynamic>();
                model.Type = 1;
                objDynamic = iPL_Admin_User_LoginData.GetAuthenticateAdminLoginDetails(model);
                if (objDynamic[0].Count > 0)
                {
                    retval = (objDynamic[0][0]).Ipl_Ad_User_PkeyID;
                }
                //retval = objDynamic[0][0].Stud_login_pkeyId;

            }
            catch (Exception ex)
            {

                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }
            return retval;

        }
    }
}





