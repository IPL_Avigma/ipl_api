﻿using Microsoft.Owin.Security.OAuth;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http.Cors;
using System.Data;


using System;
using IPL.Models;

namespace Avigma.Repository.Lib

{
    //public class CustomAuthorizationServerProvider : OAuthAuthorizationServerProvider
    //{
    //    public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
    //    {
    //        context.Validated();
    //    }

    //    public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
    //    {
    //        var form = await context.Request.ReadFormAsync();

    //        context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });
    //        Studentlogin_Master model = new Studentlogin_Master();

    //        //AuthRepository authRepository = new AuthRepository();



    //        if (!string.IsNullOrEmpty(form.Get("ClientId")))
    //        {
    //            model.Type = Convert.ToInt32(form.Get("ClientId"));
    //        }
    //        switch (model.Type)
    //        {
    //            case 1:
    //            case 2:
    //                {
    //                    model.Stud_login_EmailId = context.UserName;
    //                    model.Stud_login_MobileNumber = context.UserName;
    //                    model.Stud_login_Password = context.Password;
    //                           break;

    //                }
    //            case 3:
    //                {
    //                    if (!string.IsNullOrEmpty(context.Password))
    //                    {
    //                        model.Stud_login_OTP = Convert.ToInt32(context.Password);
    //                        model.Stud_login_pkeyId = Convert.ToInt32(context.UserName);
    //                    }
    //                    break;
    //                }


    //        }
    //        Int64 isValid = 10;// authRepository.ValidateUser(model);

    //        if (isValid == 0)
    //        {
    //            context.SetError("invalid_grant", "The user name or password is incorrect.");
    //            return;
    //        }

    //        var identity = new ClaimsIdentity(context.Options.AuthenticationType);

    //        identity.AddClaim(new Claim("Id", isValid.ToString()));

    //        context.Validated(identity);

    //        #region comment
    //        //AuthRepository authRepository = new AuthRepository();
    //        //bool isValid = authRepository.ValidateUser(context.UserName, context.Password);
    //        //if (!isValid)
    //        //{
    //        //    context.SetError("invalid_grant", "The user name or password is incorrect.");
    //        //}


    //        //using (AuthRepository _repo = new AuthRepository())
    //        //{
    //        //    IdentityUser user = await _repo.FindUser(context.UserName, context.Password);

    //        //    if (user == null)
    //        //    {
    //        //        context.SetError("invalid_grant", "The user name or password is incorrect.");
    //        //        return;
    //        //    }
    //        //}


    //        //var identity = new ClaimsIdentity(context.Options.AuthenticationType);
    //        //identity.AddClaim(new Claim(ClaimTypes.Name, context.UserName));
    //        //identity.AddClaim(new Claim(ClaimTypes.Role, "User"));

    //        //context.Validated(identity);
    //        #endregion
    //    }


    //    public class Studentlogin_Master

    //    {
    //        public String Stud_login_MobileNumber { get; set; }
    //        public String Stud_login_EmailId { get; set; }
    //        public String Stud_login_Password { get; set; }
    //        public Int64 Stud_login_pkeyId { get; set; }
    //        public int? Stud_login_OTP { get; set; }
    //        public int Type { get; set; }
    //    }
    //}




    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class CustomAuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            Log log = new Log();
            try
            {
                var form = await context.Request.ReadFormAsync();



                var identity = new ClaimsIdentity(context.Options.AuthenticationType);

                // context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });
                int ID = 0;
                string name = string.Empty, IP = string.Empty, macid = string.Empty, DeviceName = string.Empty;
                Int64 User_pkeyID = 0;
                if (!string.IsNullOrEmpty(form.Get("ClientId")))
                {
                    ID = Convert.ToInt32(form.Get("ClientId"));
                }
                else if (!string.IsNullOrEmpty(form.Get("ClientName")))
                {
                     name = form.Get("ClientName");
                }
                 if (!string.IsNullOrEmpty(form.Get("macid")))
                {
                    macid = form.Get("macid");
                }
                 if (!string.IsNullOrEmpty(form.Get("ip")))
                {
                    IP = form.Get("ip");
                }
                if (!string.IsNullOrEmpty(form.Get("device")))
                {
                    DeviceName = form.Get("device");
                }

                switch (ID)
                {
                    case 1:
                        {
                            AuthRepository authRepository = new AuthRepository();
                            //bool isValid = true;
                            IPL_Admin_User_LoginDTO iPL_Admin_User_LoginDTO = new IPL_Admin_User_LoginDTO();
                            iPL_Admin_User_LoginDTO.Ipl_Ad_User_Login_Name = context.UserName;
                            iPL_Admin_User_LoginDTO.Ipl_Ad_User_Password = context.Password;
                            iPL_Admin_User_LoginDTO.Ipl_Ad_User_IP = IP;
                            iPL_Admin_User_LoginDTO.Ipl_Ad_User_MacId = macid;
                            iPL_Admin_User_LoginDTO.Ipl_Ad_User_Access_Device = DeviceName;

                            User_pkeyID = authRepository.AdminValidateUser(iPL_Admin_User_LoginDTO);
                            break;
                        }
                    case 2:
                        {
                            AuthRepository authRepository = new AuthRepository();
                            //bool isValid = true;
                            AuthLoginDTO authLoginDTO = new AuthLoginDTO();
                            authLoginDTO.User_LoginName = context.UserName;
                            authLoginDTO.User_Password = context.Password;
                            authLoginDTO.MacId = macid;
                            authLoginDTO.User_Acc_Log_Device_Name = DeviceName;
                            authLoginDTO.IP = IP;
                            authLoginDTO.Type = 5;
                            User_pkeyID = authRepository.ValidateUser(authLoginDTO);
                            break;
                        }
                    default:
                        {
                            if (name == "Jamie")
                            {
                                AuthRepository authRepository = new AuthRepository();
                                AuthLoginDTO authLoginDTO = new AuthLoginDTO();
                                authLoginDTO.User_LoginName = context.UserName;
                                authLoginDTO.MacId = macid;
                                authLoginDTO.IP = IP;
                                authLoginDTO.User_Acc_Log_Device_Name = DeviceName;
                                authLoginDTO.Type = 3;

                                User_pkeyID = authRepository.ValidateUser(authLoginDTO);
                            }
                            else
                            {
                                AuthRepository authRepository = new AuthRepository();
                                //bool isValid = true;
                                AuthLoginDTO authLoginDTO = new AuthLoginDTO();
                                authLoginDTO.User_LoginName = context.UserName;
                                authLoginDTO.User_Password = context.Password;
                                authLoginDTO.MacId = macid;
                                authLoginDTO.User_Acc_Log_Device_Name = DeviceName;
                                authLoginDTO.IP = IP;
                                User_pkeyID = authRepository.ValidateUser(authLoginDTO);

                            }
                            break;
                        }
                }

               
               

                if (User_pkeyID == 0)
                {
                    context.SetError("invalid_grant", "The user name or password is incorrect.");
                    return;
                }

                if (ID == 1)
                {
                    identity.AddClaim(new Claim("Id", "AD_" + User_pkeyID.ToString()));
                }
                else
                {
                    identity.AddClaim(new Claim("Id", "UD_" + User_pkeyID.ToString()));
                }
               

                //identity.AddClaim(new Claim(ClaimTypes.Name, User_pkeyID.ToString()));
                // identity.AddClaim(new Claim(ClaimTypes.Role, "User"));

                context.Validated(identity);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);


            }

        }
    }

}