﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Quartz;
using Quartz.Impl;
using Avigma.Repository.Lib;
using System.Threading.Tasks;
using IPL.Repository.data;
using IPL.Models;
using IPLApp.Models;
using IPLApp.Repository.data;
namespace Avigma.Repository.Scheduler
{
    public class ScheduleJob : IJob
    {
        Log log = new Log();
        ImportWorkOrderData ImportWorkOrderData = new ImportWorkOrderData();
        ImportWorkOrderDTO ImportWorkOrderDTO = new ImportWorkOrderDTO();

        public void Execute(Quartz.IJobExecutionContext context)
        {
            try
            {
                log.logDebugMessage("Import Scheduler Job Run Called");

                List<dynamic> objDynamic = new List<dynamic>();
                WorkOrderImport_MasterData_DTO workOrderImport_MasterData_DTO = new WorkOrderImport_MasterData_DTO();
                WorkOrderImport_MasterData workOrderImport_MasterData = new WorkOrderImport_MasterData();
                GoogleCloudData googleCloudData = new GoogleCloudData();
                FileUploadBucketDTO model = new FileUploadBucketDTO();
                JobDataMap dataMap = context.JobDetail.JobDataMap;
                Int64 WI_Pkey_ID = dataMap.GetLong("WI_Pkey_ID");
                Int64 importfrom_Val = dataMap.GetLong("importfrom");
                string ResCode = dataMap.GetString("Scrapper_rep_code");
                workOrderImport_MasterData_DTO.WI_Pkey_ID = WI_Pkey_ID;
             
                workOrderImport_MasterData_DTO.Type = 8;
                log.logDebugMessage("<------------------------Delete Data Function  Called for -------------------------->"+ importfrom_Val+"----------->"+ WI_Pkey_ID);
                ImportWorkOrderDTO.ImportFrom = importfrom_Val;
                ImportWorkOrderDTO.Type = 2;
                ImportWorkOrderDTO.UserID = 0;
                ImportWorkOrderDTO.WI_Pkey_ID = WI_Pkey_ID;
                var val = ImportWorkOrderData.DeleteWorkOrderAPIData(ImportWorkOrderDTO);
                log.logDebugMessage("<--------------------------Delete Data Function  End------------------------>");
                objDynamic = workOrderImport_MasterData.GetWorkOrderImportDetailsForScheduler(workOrderImport_MasterData_DTO);

                if (objDynamic.Count > 0)
                {
                    #region Comment
                    //for (int i = 0; i < objDynamic[0].Count; i++)
                    //{

                    //    model.WI_Pkey_ID = objDynamic[0][i].WI_Pkey_ID;

                    //    // Delete Unprocess Data

                    //    if (objDynamic[0][i].WI_ImportFrom == 1|| objDynamic[0][i].WI_ImportFrom == 3 || objDynamic[0][i].WI_ImportFrom == 7)
                    //    {
                    //        //ImportWorkOrderDTO.ImportFrom = objDynamic[0][i].WI_ImportFrom;
                    //        ImportWorkOrderDTO.Type = 2;
                    //        ImportWorkOrderDTO.UserID = 0;
                    //        ImportWorkOrderDTO.WI_Pkey_ID = model.WI_Pkey_ID;
                    //        log.logDebugMessage("<------------------------Delete Data Function  Called-------------------------->");
                    //        log.logDebugMessage(ImportWorkOrderDTO.ImportFrom);
                    //        log.logDebugMessage(ImportWorkOrderDTO.WI_Pkey_ID);
                    //        log.logDebugMessage("<--------------------------Delete Data Function  End------------------------>");

                    //        var val =  ImportWorkOrderData.DeleteWorkOrderAPIData(ImportWorkOrderDTO);
                    //        log.logDebugMessage(val.Count);


                    //    }

                    //}
                    #endregion

                    for (int j = 0; j < objDynamic[0].Count; j++)
                    {
                        log.logDebugMessage("<------------------------Bucket Data Function  Called for -------------------------->" + importfrom_Val + "----------->" + WI_Pkey_ID);
                        Int64 importfrom = objDynamic[0][j].WI_ImportFrom;

                        switch (importfrom)
                        {
                            case 1:
                        {
                                    model.WI_Pkey_ID = objDynamic[0][j].WI_Pkey_ID;
                                    model.image_download = objDynamic[0][j].WI_ImageDownload;
                                    model.Import_BucketName = objDynamic[0][j].Import_BucketName;
                                    model.Import_BucketFolderName = objDynamic[0][j].Import_BucketFolderName;
                                    model.Import_DestBucketFolderName = objDynamic[0][j].Import_DestBucketFolderName;
                                    model.Scrapper_rep_code = ResCode;
                                    //googleCloudData.ListObjects(model, 2);
                                    googleCloudData.ListPPWNewObjects(model, 2);
                                    break;
                        }

                            case 3:
                                {
                                    model.WI_Pkey_ID = objDynamic[0][j].WI_Pkey_ID;
                                    model.rep_code = objDynamic[0][j].WI_Res_Code;
                                    model.Import_BucketName = objDynamic[0][j].Import_BucketName;
                                    model.Import_BucketFolderName = objDynamic[0][j].Import_BucketFolderName;
                                    model.Import_DestBucketFolderName = objDynamic[0][j].Import_DestBucketFolderName;
                                    model.Scrapper_rep_code = ResCode;
                                    googleCloudData.MSIListObjects(model, 2);
                                    break;
                                }
                            case 6:
                                {
                                    model.WI_Pkey_ID = objDynamic[0][j].WI_Pkey_ID;
                                    model.rep_code = objDynamic[0][j].WI_Res_Code;
                                    model.Import_BucketName = objDynamic[0][j].Import_BucketName;
                                    model.Import_BucketFolderName = objDynamic[0][j].Import_BucketFolderName;
                                    model.Import_DestBucketFolderName = objDynamic[0][j].Import_DestBucketFolderName;
                                    model.Scrapper_rep_code = ResCode;
                                    googleCloudData.MCSListObjects(model, 2);
                                    break;
                                }
                            case 7:
                                {
                                    model.WI_Pkey_ID = objDynamic[0][j].WI_Pkey_ID;
                                    model.rep_code = objDynamic[0][j].WI_Res_Code;
                                    model.Import_BucketName = objDynamic[0][j].Import_BucketName;
                                    model.Import_BucketFolderName = objDynamic[0][j].Import_BucketFolderName;
                                    model.Import_DestBucketFolderName = objDynamic[0][j].Import_DestBucketFolderName;
                                    model.Scrapper_rep_code = ResCode;
                                    googleCloudData.NFRListObjects(model, 2);
                                    break;
                                }
                            
                            case 10:
                                {
                                    model.WI_Pkey_ID = objDynamic[0][j].WI_Pkey_ID;
                                    model.rep_code = objDynamic[0][j].WI_Res_Code;
                                    model.Import_BucketName = objDynamic[0][j].Import_BucketName;
                                    model.Import_BucketFolderName = objDynamic[0][j].Import_BucketFolderName;
                                    model.Import_DestBucketFolderName = objDynamic[0][j].Import_DestBucketFolderName;
                                    model.Scrapper_rep_code = ResCode;
                                    googleCloudData.CyprexxListObjects(model, 2);
                                    break;
                                }
                            case 12:
                                {
                                    model.WI_Pkey_ID = objDynamic[0][j].WI_Pkey_ID;
                                    model.rep_code = objDynamic[0][j].WI_Res_Code;
                                    model.Import_BucketName = objDynamic[0][j].Import_BucketName;
                                    model.Import_BucketFolderName = objDynamic[0][j].Import_BucketFolderName;
                                    model.Import_DestBucketFolderName = objDynamic[0][j].Import_DestBucketFolderName;
                                    model.Scrapper_rep_code = ResCode;
                                    googleCloudData.ServicelinkListObjects(model, 2);
                                    break;
                                }
                            case 13:
                                {
                                    model.WI_Pkey_ID = objDynamic[0][j].WI_Pkey_ID;
                                    model.rep_code = objDynamic[0][j].WI_Res_Code;
                                    model.Import_BucketName = objDynamic[0][j].Import_BucketName;
                                    model.Import_BucketFolderName = objDynamic[0][j].Import_BucketFolderName;
                                    model.Import_DestBucketFolderName = objDynamic[0][j].Import_DestBucketFolderName;
                                    model.Scrapper_rep_code = ResCode;
                                    googleCloudData.AltisourceListObjects(model, 2);
                                    break;
                                }
                            case 19:
                                {
                                    model.WI_Pkey_ID = objDynamic[0][j].WI_Pkey_ID;
                                    model.rep_code = objDynamic[0][j].WI_Res_Code;
                                    model.Import_BucketName = objDynamic[0][j].Import_BucketName;
                                    model.Import_BucketFolderName = objDynamic[0][j].Import_BucketFolderName;
                                    model.Import_DestBucketFolderName = objDynamic[0][j].Import_DestBucketFolderName;
                                    model.Scrapper_rep_code = ResCode;
                                    googleCloudData.AGListObjects(model, 2);
                                    break;
                                }
                        }
                        
                    }
                }
                else
                {
                    log.logDebugMessage("No Data found  in Import Scheduler ");
                }
               

                //googleCloudData.ListObjects(model);

                log.logDebugMessage("Import   Scheduler Job Run Sucessfully");
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

            }

        }
 
    }

    
    

 



    public class JobSchedulerStart
    {

        public static void Start(DateTime startDateStr, LoginDTO Model)
        {
            Random rnd = new Random();
            OTPGenerator oTPGenerator = new OTPGenerator();
            IScheduler scheduler = StdSchedulerFactory.GetDefaultScheduler();
            scheduler.Start();
            // String startDateStr = "2013-09-27 00:00:00.0";
            int number = oTPGenerator.GenerateRandomNo();
            DateTime startdateTime = Convert.ToDateTime(startDateStr).ToUniversalTime();
            string strdate = System.DateTime.Now.ToLongTimeString();
            JobKey jobKey = new JobKey(strdate + "_Job" + "_" + number.ToString() + "_" + Model.username);
            Boolean flagjob = scheduler.CheckExists(jobKey);
            JobDataMap JobDataMap = new JobDataMap();
            JobDataMap.Put("WI_Pkey_ID", Model.WI_Pkey_ID);
            JobDataMap.Put("importfrom", Model.importfrom);
            JobDataMap.Put("Scrapper_rep_code", Model.Scrapper_rep_code);
            IJobDetail job = JobBuilder.Create<ScheduleJob>()
                            .WithIdentity(jobKey)
                            .UsingJobData(JobDataMap)
                            .Build();
           

            //var minutesToFireDouble = (futureUTCtime - DateTime.UtcNow).TotalMinutes;
            //  var minutesToFire = (int)Math.Floor(minutesToFireDouble); // negative or zero value will set fire immediately
            // var timeToFire = DateBuilder.FutureDate(minutesToFire, IntervalUnit.Minute);
            TriggerKey triggerkey = new TriggerKey("triggerC", (strdate + "_ConStart" + "_" + number.ToString() + Model.username));
            Boolean flag = scheduler.CheckExists(triggerkey);


            var trigger = TriggerBuilder.Create()
                                        .WithIdentity(triggerkey)
                                        .StartAt(startdateTime)
                                        .WithSimpleSchedule(x => x.WithMisfireHandlingInstructionFireNow())
                                        .UsingJobData(JobDataMap)
                                        .Build();

            scheduler.ScheduleJob(job, trigger);

        }
    }



 
}