﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Quartz;
using Quartz.Impl;
using Avigma.Repository.Lib;
using IPL.Repository.data;
using IPL.Models;
using IPLApp.Models;
using IPLApp.Repository.data;

namespace Avigma.Repository.Scheduler
{
    public class IPLScheduler : IJob
    {
        Log log = new Log();

        public void Execute(Quartz.IJobExecutionContext context)
        {
            try
            {
                log.logDebugMessage("Import IPLScheduler Job Run Called");

                JobDataMap dataMap = context.JobDetail.JobDataMap;
                LoginDTO loginDTO = new LoginDTO();
                loginDTO.username = dataMap.GetString("PPWusername");
                loginDTO.password = dataMap.GetString("PPWPassword");
                loginDTO.URL = dataMap.GetString("URL");
                loginDTO.image_download = dataMap.GetBoolean("ImageDownload");
                loginDTO.WI_Pkey_ID = dataMap.GetLong("WI_Pkey_ID");
                loginDTO.FBUsername = dataMap.GetString("FBUsername");
                loginDTO.FBPassword = dataMap.GetString("FBPassword");
                loginDTO.rep_code = dataMap.GetString("WI_Res_Code");
                Int64 importfrom =dataMap.GetLong("importfrom");
                loginDTO.importfrom = importfrom;
                GetWorkOrders getWorkOrders = new GetWorkOrders();

                switch (importfrom)
                {
                    case 1:
                        {
                            getWorkOrders.GetPPWWorkOdersDetails(loginDTO);
                            break;
                        }
                    case 2:
                        {
                            getWorkOrders.GetWorkOdersDetails(loginDTO,2);
                            break;
                        }
                    case 3:
                        {
                            getWorkOrders.GetMsiWorkOdersDetails(loginDTO);
                            break;
                        }
                    case 4:
                        {
                            getWorkOrders.GetFBWorkOdersDetails(loginDTO,2);
                            break;
                        }
                    case 6:
                        {
                            getWorkOrders.GetMCSWorkOdersDetails(loginDTO);
                            break;
                        }
                    case 7:
                        {
                            getWorkOrders.GetNFRWorkOdersDetails(loginDTO);
                            break;
                        }
                    case 10:
                        {
                            getWorkOrders.GetCyprexxWorkOdersDetails(loginDTO);
                            break;
                        }
                    case 12:
                        {
                            getWorkOrders.GetServiceLinkWorkOdersDetails(loginDTO);
                            break;
                        }
                    case 13:
                        {
                            getWorkOrders.GetAltisourceWorkOdersDetails(loginDTO);
                            break;
                        }
                    case 19:
                        {
                            getWorkOrders.GetAGWorkOdersDetails(loginDTO);
                            break;
                        }
                }
                //googleCloudData.ListObjects(model);

                log.logDebugMessage("Import   IPLScheduler Job Run Sucessfully");
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

            }

        }

    }


    public class IPLJobScheduler 
    {

        public static void Start(IPLScheduleDTO iPLScheduleDTO)
        {
            log4net.ILog logger = log4net.LogManager.GetLogger("DebugLog");
            logger.Info("First Scheduler Called ");

            try
            {
                string Schedulertime = System.Configuration.ConfigurationManager.AppSettings["Schedulertime"];
                OTPGenerator oTPGenerator = new OTPGenerator();
                int number = oTPGenerator.GenerateRandomNo();
                IScheduler scheduler = StdSchedulerFactory.GetDefaultScheduler();
                scheduler.Start();
                DateTime startdateTime = Convert.ToDateTime(iPLScheduleDTO.startDateStr).ToUniversalTime();
                string strdate = System.DateTime.Now.ToLongTimeString();
                JobDataMap JobDataMap = new JobDataMap();
                JobDataMap.Put("PPWusername", iPLScheduleDTO.PPWusername);
                JobDataMap.Put("PPWPassword", iPLScheduleDTO.PPWPassword);
                JobDataMap.Put("URL", iPLScheduleDTO.URL);
                JobDataMap.Put("ImageDownload", iPLScheduleDTO.ImageDownload);
                JobDataMap.Put("importfrom", iPLScheduleDTO.importfrom);
                JobDataMap.Put("WI_Pkey_ID", iPLScheduleDTO.WI_Pkey_ID);
                JobDataMap.Put("FBUsername", iPLScheduleDTO.FBUsername);
                JobDataMap.Put("FBPassword", iPLScheduleDTO.FBPassword);
                JobDataMap.Put("WI_Res_Code", iPLScheduleDTO.WI_Res_Code);

                logger.Info("First Scheduler Called-----> " + startdateTime.ToString());

                JobKey jobKey =  new JobKey(iPLScheduleDTO.startDateStr + "_Job" + "_" + number.ToString() + "_" + iPLScheduleDTO.PPWusername);
                Boolean flagjob = scheduler.CheckExists(jobKey);
                if (flagjob)
                {
                    logger.Info("First Scheduler Job Called-----> true");
                }
                else
                {
                    logger.Info("First Scheduler Job Called-----> false");
                }
                

                IJobDetail job = JobBuilder.Create<IPLScheduler>()
                                 .WithIdentity(jobKey)
                                 .UsingJobData(JobDataMap)
                                 .Build();

                TriggerKey triggerkey = new TriggerKey("triggerC", (iPLScheduleDTO.startDateStr + "_ConStart" + "_" + number.ToString() + "_" + iPLScheduleDTO.PPWusername));
                Boolean flag = scheduler.CheckExists(triggerkey);

                var trigger = TriggerBuilder.Create()
                                       .WithIdentity(triggerkey)
                                       .StartAt(startdateTime)
                                       .WithSimpleSchedule(x => x.WithMisfireHandlingInstructionFireNow())
                                       .Build();
               
                if (!flag)
                {
                    logger.Info("First Scheduler trigger Called-----> false");
                    scheduler.ScheduleJob(job, trigger);
                }
                else
                {
                    logger.Info("First Scheduler trigger Called-----> true");
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                logger.Error(ex.StackTrace);

            }
           





        }
    }
}