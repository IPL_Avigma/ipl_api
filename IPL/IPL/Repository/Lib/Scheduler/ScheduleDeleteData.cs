﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Quartz;
using Quartz.Impl;
using Avigma.Repository.Lib;
using IPL.Repository.data;
using IPL.Models;
using IPLApp.Models;
using IPLApp.Repository.data;
using System.Data;

namespace Avigma.Repository.Scheduler
{
    public class ScheduleDeleteData : IJob
    {
        Log log = new Log();
        public void Execute(Quartz.IJobExecutionContext context)
        {
            try
            {
                GoogleCloudData googleCloudData = new GoogleCloudData();
                WorkOrderMasterData workOrderMasterData = new WorkOrderMasterData();
                FileUploadBucketDTO fileUploadBucketDTO = new FileUploadBucketDTO();
                GetdeleteWorkmasterDataDTO getdeleteWorkmasterDataDTO = new GetdeleteWorkmasterDataDTO();
                getdeleteWorkmasterDataDTO.Type = 1;
                DataSet ds = workOrderMasterData.GetdeleteWorkmasterData(getdeleteWorkmasterDataDTO);
                log.logInfoMessage("WorkOrderData IPLNo------>" + ds.Tables[0].Rows.Count);
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    fileUploadBucketDTO.BucketName = "ipl_live_data";
                    fileUploadBucketDTO.FolderName = ds.Tables[0].Rows[i]["iplno"].ToString();
                    log.logInfoMessage("Folder Name IPLNo------>" + fileUploadBucketDTO.FolderName);
                    googleCloudData.DeleteFolder(fileUploadBucketDTO);
                    log.logInfoMessage("Folder Name IPLNo------>" + fileUploadBucketDTO.FolderName+ "Deleted Sucessfully-------------------->");
                }
               
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

            }
        }
    }


    public class ScheduleDeleteDataStart
    {

        public static void Start(string PPWusername)
        {
            //string Schedulertime = System.Configuration.ConfigurationManager.AppSettings["Schedulertime"];
            int Schedulertime = 23 , Schedulermintime = 30,Day = 10;
            Day = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["photoSchedulertimeDayofMonth"]);
            Schedulertime = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["photoSchedulertimeHrs"]);
            Schedulermintime = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["photoSchedulertimemins"]);
            OTPGenerator oTPGenerator = new OTPGenerator();
            int number = oTPGenerator.GenerateRandomNo();
            IScheduler scheduler = StdSchedulerFactory.GetDefaultScheduler();
            scheduler.Start();

            IJobDetail job = JobBuilder.Create<ScheduleDeleteData>().Build();

            TriggerKey triggerkey = new TriggerKey("trigger1", PPWusername + "_" + number.ToString());
            Boolean flag = scheduler.CheckExists(triggerkey);
            #region comment
            //ITrigger trigger = TriggerBuilder.Create()
            //    .WithIdentity(triggerkey)
            //    .StartNow()
            //    .WithSimpleSchedule(x => x
            //   //  .WithIntervalInHours(12)
            //   //  .WithIntervalInSeconds(30)
            //   //.WithIntervalInMinutes
            //   .WithIntervalInHours(Convert.ToInt32(Schedulertime))
            //    .RepeatForever())
            //    .Build();

            //ITrigger trigger = TriggerBuilder.Create()
            //  .WithIdentity("triggerkey")
            //  .WithSchedule(CronScheduleBuilder.DailyAtHourAndMinute(Schedulertime, Schedulermintime))
            //  .Build();  // This Code will Run the Scheduler   on define  time daily.
            #endregion
            ITrigger trigger = TriggerBuilder.Create()
                .WithIdentity("monthlyTrigger", "group2")
                .WithSchedule(CronScheduleBuilder.MonthlyOnDayAndHourAndMinute(Day, Schedulertime, Schedulermintime))
                .Build(); // This Code will Run the Scheduler once in month on define date& time



            if (!flag)
            {
                scheduler.ScheduleJob(job, trigger);
            }


        }
    }
}