﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Quartz;
using Quartz.Impl;
using Avigma.Repository.Lib;
using System.Threading.Tasks;
using Quartz.Impl.Matchers;
using IPL.Repository.data;
using IPL.Models;
using IPLApp.Models;
using IPLApp.Repository.data;
using System.Text;

namespace Avigma.Repository.Scheduler
{
    public class CheckJob
    {
        Log log = new Log();
        public List<dynamic> getPrintJobs()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {

              
                IScheduler scheduler = StdSchedulerFactory.GetDefaultScheduler();
                IList<String> listGroupName = scheduler.GetJobGroupNames();
                foreach (var strGroupName in listGroupName)
                {
                    Quartz.Collection.ISet<JobKey> jobKeys =   scheduler.GetJobKeys(GroupMatcher<JobKey>.GroupEquals(strGroupName));
                    foreach (var item in jobKeys)
                    {
                        IJobDetail jobDetail = scheduler.GetJobDetail(item);
                        objdynamicobj.Add(jobDetail);
                    }
                }
                 
  
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }

            return objdynamicobj;
        }
    }
}