﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Quartz;
using Quartz.Impl;
using Avigma.Repository.Lib;
using IPL.Repository.data;
using IPL.Models;
using IPLApp.Models;
using IPLApp.Repository.data;
namespace Avigma.Repository.Scheduler
{
    public class Job : IJob
    {
        Log log = new Log();
        GoogleCloudData googleCloudData = new GoogleCloudData();
        public void Execute(Quartz.IJobExecutionContext context)
        {
            try
            {
                

                //log.logDebugMessage("Image Bukcet Called ");
                //GoogleCloudData googleCloudData = new GoogleCloudData();
                //FileUploadBucketDTO fileUploadBucketDTO = new FileUploadBucketDTO();
                //googleCloudData.ListImageObjects(fileUploadBucketDTO);

                log.logDebugMessage("Scrapper Scheduler Called");

                List<dynamic> objDynamic = new List<dynamic>();
                WorkOrderImport_MasterData_DTO workOrderImport_MasterData_DTO = new WorkOrderImport_MasterData_DTO();
                WorkOrderImport_MasterData workOrderImport_MasterData = new WorkOrderImport_MasterData();
                workOrderImport_MasterData_DTO.Type = 6;
                objDynamic =  workOrderImport_MasterData.GetWorkOrderImportDetailsForScheduler(workOrderImport_MasterData_DTO);
                string AddSchedulertime = System.Configuration.ConfigurationManager.AppSettings["AddSchedulertime"];
                int Schedulertime = 0, Schedulertime2 = 0, Schedulertime3 = 0 , Schedulertime4= 0 , Schedulertime7=0, Schedulertime6 = 0, Schedulertime10 = 0, Schedulertime12= 0;
                LoginDTO loginDTO;
                
                if (objDynamic.Count>0)
                {
                    if (objDynamic[0].Count > 0)
                    {
                        int count1 = 0, count2 = 0, count3=0, count4 = 0, count7=0, count10 = 0;
                        log.logDebugMessage("Scrapper Scheduler Count------>"+ objDynamic[0].Count);
                        for (int i = 0; i < objDynamic[0].Count; i++)
                        {
                            
                            loginDTO = new LoginDTO();
                            loginDTO.username = objDynamic[0][i].WI_LoginName;
                            loginDTO.password = objDynamic[0][i].WI_Password;
                            loginDTO.URL = objDynamic[0][i].Import_Form_URL_Name;
                            loginDTO.image_download = objDynamic[0][i].WI_ImageDownload;
                            loginDTO.WI_Pkey_ID =  objDynamic[0][i].WI_Pkey_ID;
                            loginDTO.FBUsername = objDynamic[0][i].WI_FB_LoginName;
                            loginDTO.FBPassword = objDynamic[0][i].WI_FB_Password;
                            loginDTO.rep_code = objDynamic[0][i].WI_Res_Code;

                            Int64 intimportfrom = objDynamic[0][i].WI_ImportFrom;

                            IPLScheduleDTO iPLScheduleDTO = new IPLScheduleDTO();
                            iPLScheduleDTO.PPWusername = loginDTO.username;
                            iPLScheduleDTO.PPWPassword = loginDTO.password;
                            iPLScheduleDTO.URL = loginDTO.URL;
                            iPLScheduleDTO.ImageDownload = loginDTO.image_download;
                            iPLScheduleDTO.importfrom = intimportfrom;
                            iPLScheduleDTO.WI_Pkey_ID = loginDTO.WI_Pkey_ID;
                            iPLScheduleDTO.FBUsername = loginDTO.FBUsername;
                            iPLScheduleDTO.FBPassword = loginDTO.FBPassword;
                            iPLScheduleDTO.WI_Res_Code = loginDTO.rep_code;




                            switch (intimportfrom)
                            {
                                case 1:
                                    {
                                        log.logDebugMessage("Scrapper Scheduler Count------> Case 1" );
                                        if (count1 == 0 )
                                        {
                                            Schedulertime =  5;
                                        }
                                        else
                                        {
                                            Schedulertime = Schedulertime + Convert.ToInt32(AddSchedulertime);
                                        }
                                        
                                        iPLScheduleDTO.startDateStr = System.DateTime.Now.AddMinutes(Convert.ToInt32(Schedulertime));
                                        Avigma.Repository.Scheduler.IPLJobScheduler.Start(iPLScheduleDTO);
                                        count1 = count1 + 1;
                                        break;
                                    }
                                case 2:
                                    {
                                        log.logDebugMessage("Scrapper Scheduler Count------> Case 2");
                                        if (count2 == 0)
                                        {
                                            Schedulertime2 = 6;
                                        }
                                        else
                                        {
                                            Schedulertime2 = Schedulertime2 + 5;
                                        }
                                        iPLScheduleDTO.startDateStr = System.DateTime.Now.AddMinutes(Convert.ToInt32(Schedulertime2));
                                        Avigma.Repository.Scheduler.IPLJobScheduler.Start(iPLScheduleDTO);
                                        count2 = count2 + 1;
                                        break;
                                    }
                                case 3:
                                    {
                                        log.logDebugMessage("Scrapper Scheduler Count------> Case 3");
                                        if (count3 == 0)
                                        {
                                            Schedulertime3 = 7;
                                        }
                                        else
                                        {
                                            Schedulertime3 = Schedulertime3 + 5;
                                        }
                                        iPLScheduleDTO.startDateStr = System.DateTime.Now.AddMinutes(Convert.ToInt32(Schedulertime3));
                                        Avigma.Repository.Scheduler.IPLJobScheduler.Start(iPLScheduleDTO);
                                        count3 = count3 + 1;
                                        break;
                                    }
                                case 4:
                                    {
                                        log.logDebugMessage("Scrapper Scheduler Count------> Case 4");
                                        if (count4 == 0)
                                        {
                                            Schedulertime4 = 9;
                                        }
                                        else
                                        {
                                            Schedulertime4 = Schedulertime4 + Convert.ToInt32(AddSchedulertime)+5;
                                        }
                                        iPLScheduleDTO.startDateStr = System.DateTime.Now.AddMinutes(Convert.ToInt32(Schedulertime4));
                                        Avigma.Repository.Scheduler.IPLJobScheduler.Start(iPLScheduleDTO);
                                        count4 = count4 + 1;
                                        break;
                                    }
                                case 7:
                                    {
                                        log.logDebugMessage("Scrapper Scheduler Count------> Case 6");
                                        if (count7 == 0)
                                        {
                                            Schedulertime7 = 15;
                                        }
                                        else
                                        {
                                            Schedulertime7 = Schedulertime7 + Convert.ToInt32(AddSchedulertime) + 7;
                                        }
                                        iPLScheduleDTO.startDateStr = System.DateTime.Now.AddMinutes(Convert.ToInt32(Schedulertime7));
                                        Avigma.Repository.Scheduler.IPLJobScheduler.Start(iPLScheduleDTO);
                                        count7 = count7 + 1;
                                        break;
                                    }
                                case 6:
                                    {
                                        log.logDebugMessage("Scrapper Scheduler Count------> Case 7");
                                        if (count7 == 0)
                                        {
                                            Schedulertime6 = 15;
                                        }
                                        else
                                        {
                                            Schedulertime6 = Schedulertime6 + Convert.ToInt32(AddSchedulertime) + 7;
                                        }
                                        iPLScheduleDTO.startDateStr = System.DateTime.Now.AddMinutes(Convert.ToInt32(Schedulertime6));
                                        Avigma.Repository.Scheduler.IPLJobScheduler.Start(iPLScheduleDTO);
                                        count7 = count7 + 1;
                                        break;
                                    }
                                case 10:
                                    {
                                        log.logDebugMessage("Scrapper Scheduler Count------> Case 10");
                                        if (count10 == 0)
                                        {
                                            Schedulertime10 = 15;
                                        }
                                        else
                                        {
                                            Schedulertime10 = Schedulertime10 + Convert.ToInt32(AddSchedulertime) + 9;
                                        }
                                        iPLScheduleDTO.startDateStr = System.DateTime.Now.AddMinutes(Convert.ToInt32(Schedulertime10));
                                        Avigma.Repository.Scheduler.IPLJobScheduler.Start(iPLScheduleDTO);
                                        count7 = count7 + 1;
                                        break;
                                    }
                                case 12:
                                    {
                                        log.logDebugMessage("Scrapper Scheduler Count------> Case 10");
                                        if (count10 == 0)
                                        {
                                            Schedulertime12 = 15;
                                        }
                                        else
                                        {
                                            Schedulertime12 = Schedulertime12 + Convert.ToInt32(AddSchedulertime) + 9;
                                        }
                                        iPLScheduleDTO.startDateStr = System.DateTime.Now.AddMinutes(Convert.ToInt32(Schedulertime12));
                                        Avigma.Repository.Scheduler.IPLJobScheduler.Start(iPLScheduleDTO);
                                        count7 = count7 + 1;
                                        break;
                                    }
                            }


                        }
                    }
                    else
                    {
                        log.logDebugMessage("No Data found in Scrapper Scheduler count ");
                    }
                }
                else
                {
                    log.logDebugMessage("No Data found in Scrapper Scheduler ");
                }

                //This Function send the Email of which workorder are still open and Due Date is passed 
                // Currently Commented to testing Purpose
                //LateWOrkOrderData lateWOrkOrderData = new LateWOrkOrderData();
                //lateWOrkOrderData.SentLateWoEmail();

                //This Funtion Corrects URL of all invalid path
                WorkOrderPicsData workOrderPicsData = new WorkOrderPicsData();
                WorkOrderPicsDTO workOrderPicsDTO = new WorkOrderPicsDTO();
                workOrderPicsDTO.Type = 2;
                workOrderPicsData.UpdateByWorkorderID(workOrderPicsDTO);


                log.logDebugMessage("Scrapper Scheduler Run Sucessfully");
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
       
        }
    }


    public class JobScheduler
    {
       
        public static void Start(string PPWusername)
        {

            string Schedulertime = System.Configuration.ConfigurationManager.AppSettings["Schedulertime"];
            OTPGenerator oTPGenerator = new OTPGenerator();
            int number = oTPGenerator.GenerateRandomNo();
            IScheduler scheduler = StdSchedulerFactory.GetDefaultScheduler();
                scheduler.Start();

                IJobDetail job = JobBuilder.Create<Job>().Build();

            TriggerKey triggerkey = new TriggerKey("trigger1", PPWusername+"_"+ number.ToString());
            Boolean flag = scheduler.CheckExists(triggerkey);
            
            ITrigger trigger = TriggerBuilder.Create()
                .WithIdentity(triggerkey)
                .StartNow()
                .WithSimpleSchedule(x => x
               //  .WithIntervalInHours(12)
               //  .WithIntervalInSeconds(30)
               //.WithIntervalInMinutes
               .WithIntervalInHours(Convert.ToInt32(Schedulertime))
                .RepeatForever())
                .Build();


            if (!flag)
            {
                scheduler.ScheduleJob(job, trigger);
            }

         
           
            
    
        }
    }

}