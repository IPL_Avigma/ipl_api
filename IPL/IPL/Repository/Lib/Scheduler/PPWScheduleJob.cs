﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Quartz;
using Quartz.Impl;
using Avigma.Repository.Lib;
using System.Threading.Tasks;
using IPL.Repository.data;
using IPL.Models;
using IPLApp.Models;
using IPLApp.Repository.data;

namespace Avigma.Repository.Scheduler
{
    public class PPWScheduleJob : IJob
    {
        Log log = new Log();        
        public void Execute(Quartz.IJobExecutionContext context)
        {
            Import_Queue_TransData import_Queue_TransData = new Import_Queue_TransData();
            Import_Queue_Trans_DTO import_Queue_Trans_DTO = new Import_Queue_Trans_DTO();
            ImportWorkOrderData ImportWorkOrderData = new ImportWorkOrderData();
            ImportWorkOrderDTO ImportWorkOrderDTO = new ImportWorkOrderDTO();

            try
            {
                log.logDebugMessage("Manual Scheduler Job Run Called Start ");
                GoogleCloudData googleCloudData = new GoogleCloudData();
                FileUploadBucketDTO fileUploadBucketDTO = new FileUploadBucketDTO();
                JobDataMap dataMap = context.JobDetail.JobDataMap;
                int Imrt_Wo_Import_ID = dataMap.GetInt("Imrt_Wo_Import_ID");

                import_Queue_Trans_DTO.Imrt_PkeyId = Convert.ToInt64(dataMap.GetInt("Imrt_PkeyId"));
                import_Queue_Trans_DTO.Imrt_Wo_Import_ID = Convert.ToInt64(Imrt_Wo_Import_ID);
                import_Queue_Trans_DTO.Imrt_Import_From_ID = Convert.ToInt64(dataMap.GetInt("Imrt_Import_From_ID"));
               
                import_Queue_Trans_DTO.Type = 6;
                fileUploadBucketDTO.WI_Pkey_ID = Convert.ToInt64(Imrt_Wo_Import_ID);
                fileUploadBucketDTO.image_download = dataMap.GetBooleanValue("image_download");

                fileUploadBucketDTO.Import_BucketName = dataMap.GetString("Import_BucketName");
                fileUploadBucketDTO.Import_BucketFolderName = dataMap.GetString("Import_BucketFolderName");
                fileUploadBucketDTO.Import_DestBucketFolderName = dataMap.GetString("Import_DestBucketFolderName");
                fileUploadBucketDTO.UserID = dataMap.GetLong("UserID");
                fileUploadBucketDTO.Scrapper_rep_code = dataMap.GetString("Imrt_Status_Msg");
                // Delete Unprocess Data
                ImportWorkOrderDTO.ImportFrom = Convert.ToInt64(import_Queue_Trans_DTO.Imrt_Import_From_ID);
                ImportWorkOrderDTO.Type = 1;
                ImportWorkOrderDTO.UserID = dataMap.GetLong("UserID");
                ImportWorkOrderDTO.WI_Pkey_ID = Convert.ToInt64(import_Queue_Trans_DTO.Imrt_Wo_Import_ID);
                ImportWorkOrderData.DeleteWorkOrderAPIData(ImportWorkOrderDTO);

                //if (import_Queue_Trans_DTO.Imrt_Import_From_ID == 1)
                //{
                //    //googleCloudData.ListObjects(fileUploadBucketDTO, 1);
                //    googleCloudData.ListPPWNewObjects(fileUploadBucketDTO, 1);
                //}
                //else if (import_Queue_Trans_DTO.Imrt_Import_From_ID == 3)
                //{
                //    googleCloudData.MSIListObjects(fileUploadBucketDTO, 1);
                //}
                //else if (import_Queue_Trans_DTO.Imrt_Import_From_ID == 7)
                //{
                //    googleCloudData.NFRListObjects(fileUploadBucketDTO, 1);
                //}

                switch (import_Queue_Trans_DTO.Imrt_Import_From_ID)
                {
                    case 1:
                        {
                            googleCloudData.ListPPWNewObjects(fileUploadBucketDTO, 1);
                            break;
                        }
                    
                    case 3:
                        {
                            googleCloudData.MSIListObjects(fileUploadBucketDTO, 1);
                            break;
                        }
                    case 6:
                        {
                            googleCloudData.MCSListObjects(fileUploadBucketDTO, 1);
                            break;
                        }
                    case 7:
                        {
                            googleCloudData.NFRListObjects(fileUploadBucketDTO, 1);
                            break;
                        }
                    case 10:
                        {
                            googleCloudData.CyprexxListObjects(fileUploadBucketDTO, 1);
                            break;
                        }
                    case 12:
                        {
                            googleCloudData.ServicelinkListObjects(fileUploadBucketDTO, 1);
                            break;
                        }
                    case 13:
                        {
                            googleCloudData.AltisourceListObjects(fileUploadBucketDTO, 1);
                            break;
                        }
                    case 19:
                        {
                            googleCloudData.AGListObjects(fileUploadBucketDTO, 1);
                            break;
                        }
                }


                //var objData = import_Queue_TransData.AddUpdateImportQueueTransData(import_Queue_Trans_DTO);
                //var objData = import_Queue_TransData.AddUpdateAllImportQueueTransData(import_Queue_Trans_DTO);
                log.logDebugMessage("Manual Scheduler Job Run Sucessfully");
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

            }

        }

    }

    public class PPWScheduleJobStart
    {       

        public static void Start(Import_Queue_Trans_DTO model,DateTime startDateStr)
        {
            Import_Queue_TransData import_Queue_TransData = new Import_Queue_TransData();
            Random rnd = new Random();
            OTPGenerator oTPGenerator = new OTPGenerator();
            IScheduler scheduler = StdSchedulerFactory.GetDefaultScheduler();
            scheduler.Start();
            // String startDateStr = "2013-09-27 00:00:00.0";
            int number = oTPGenerator.GenerateRandomNo();
            DateTime startdateTime = Convert.ToDateTime(startDateStr).ToUniversalTime();
            string strdate = System.DateTime.Now.ToLongTimeString();
            JobKey jobKey = new JobKey(strdate + "_Job" + "_" + number.ToString() + "_" + model.Imrt_Wo_Import_ID);
            Boolean flagjob = scheduler.CheckExists(jobKey);

            JobDataMap JobDataMap = new JobDataMap();
            JobDataMap.Put("Imrt_Wo_Import_ID", model.Imrt_Wo_Import_ID);
            JobDataMap.Put("Imrt_PkeyId", model.Imrt_PkeyId);
            JobDataMap.Put("Imrt_Import_From_ID", model.Imrt_Import_From_ID);
            JobDataMap.Put("image_download", model.image_download);
            JobDataMap.Put("Import_BucketName", model.Import_BucketName);
            JobDataMap.Put("Import_BucketFolderName", model.Import_BucketFolderName);
            JobDataMap.Put("Import_DestBucketFolderName", model.Import_DestBucketFolderName);
            JobDataMap.Put("UserID", model.UserID);
            JobDataMap.Put("Imrt_Status_Msg", model.Imrt_Status_Msg);

            IJobDetail job = JobBuilder.Create<PPWScheduleJob>()
                            .WithIdentity(jobKey)
                            .UsingJobData(JobDataMap)
                            .Build();

            TriggerKey triggerkey = new TriggerKey("triggerC", (strdate + "_ConStart" + "_" + number.ToString() + model.Imrt_Wo_Import_ID));
            Boolean flag = scheduler.CheckExists(triggerkey);


            var trigger = TriggerBuilder.Create()
                                        .WithIdentity(triggerkey)
                                        .StartAt(startdateTime)
                                        .WithSimpleSchedule(x => x.WithMisfireHandlingInstructionFireNow())
                                        .Build();

            scheduler.ScheduleJob(job, trigger);

            
        }
    }
}