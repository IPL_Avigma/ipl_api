﻿using IPL.Models.Avigma;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Repository.Lib
{
    public class EmailFormatData
    {
        private String GetMessageText(EmailFormatDTO model)
        {
            string Message = string.Empty;

            string str = string.Empty;
            str = "<html><head><meta name=\"viewport\" content=\"width=device-width\"/><title>Memo Details</title></head>";
            str += "<body>";
            str += "<style type=\"text/css\">";
            str += "import url(https://fonts.googleapis.com/css?family=Lato:400,700);";
            str += "body{font-size: 15px !important; font-family: 'Lato', sans-serif !important;color: #333; margin:0px; padding:0px; line-height: 24px !important;letter-spacing: 0.10px;}";
            str += "h1{font-size: 24px; font-weight: 700;font-family: 'Lato', sans-serif !important;}";
            str += "td{vertical-align: top;font-family: 'Lato', sans-serif !important;}";
            str += "a{text-decoration: none; color: #5cbd9e;}";
            str += ".gt{font-size: 100% !important;}";
            str += "</style>";
            str += "<table cellpadding=\"0\" cellspacing=\"0\" align=\"center\" width=\"600\" style=\"border:1px solid #ddd; background-color: #fff;\">";
            str += "<tr bgcolor=\"#fff\">";
            // str += "<td align=\"center\" style=\"padding: 30px 0;\"><a href=\"#\"><img src=\"http://win.infomanav.com/Content/User/images/logo.png\"></a></td>";
            str += "</tr><tr>";
            // str += "<td valign=\"center\" style=\"background: url(http://win.infomanav.com/Content/User/images/banner.jpg); width: 600px; height: 113px; vertical-align: middle; color: #fff; text-align: center;font-family: 'Lato', sans-serif !important;\">";
            str += "<h1>Share Memory</h1>";
            str += "</td></tr>";
            str += "<tr><td>";
            str += "<table cellpadding=\"0\" cellspacing=\"0\" width=\"600\" align=\"center\" style=\"padding-left:40px; padding-right: 40px; padding-bottom: 50px;\" bgcolor=\"#fff\">";
            str += "<tr><td>";
            str += "<p style=\"padding-top: 50px; color: #ea5a56; font-family: 'Lato', sans-serif !important;\">Hello " + ",</p>";
            str += "<p style=\"color: #333; font-family: 'Lato', sans-serif !important;\">";
            //str += "Please Find the Below Link of the Memory <br><br>";
            //str += "<br/>";
            // str += "Password = " + UserPassword + " <br>";
            str += "<p style=\"padding-top: 10px; font-family: 'Lato', sans-serif !important;\">";
            str += "Please click on the link below to Open the Memory’.<br><br>";
          
            str += "</p>";
            str += "<p style=\"padding-top: 10px; font-family: 'Lato', sans-serif !important;\">";
            // str += "For any further assistance please feel free to write to us at, contact@theartstrust.com or call us on 91-22 2204 8138/39. We will be glad to assist you.<br>";
            str += "</p>";
            str += "<p style=\"padding-top: 10px; font-family: 'Lato', sans-serif !important;\">";
            str += "Warm Regards,<br>";
            str += "Ipreservationlive.com";
            str += "</p>";
            str += "</tr></table></td></tr>";
            str += "<tr bgcolor=\"#f1f4f6\">";
            str += "<td style=\"padding: 10px 40px; color: #333333; font-size: 13px;font-family: 'Lato', sans-serif !important; line-height: 24px;\">Copyright 2020. All rights reserved.</td>";
            str += "</tr>";
            str += "</table></body></html>";
            Message = str;
            return Message;
        }

        //pradeep

        public String GetEmailMessageText(string changePasswordLink, string Name, string Email,int  Type)
        {
            string Message = string.Empty;

            string str = string.Empty;
            str = "<html><head><meta name=\"viewport\" content=\"width=device-width\"/><title>IPL New Register User</title></head>";
            str += "<body>";
            str += "<style type=\"text/css\">";
            str += "import url(https://fonts.googleapis.com/css?family=Lato:400,700);";
            str += "body{font-size: 15px !important; font-family: 'Lato', sans-serif !important;color: #333; margin:0px; padding:0px; line-height: 24px !important;letter-spacing: 0.10px;}";
            str += "h1{font-size: 24px; font-weight: 700;font-family: 'Lato', sans-serif !important;}";
            str += "td{vertical-align: top;font-family: 'Lato', sans-serif !important;}";
            str += "a{text-decoration: none; color: #5cbd9e;}";
            str += ".gt{font-size: 100% !important;}";
            str += "</style>";
            str += "<table cellpadding=\"0\" cellspacing=\"0\" align=\"center\" width=\"600\" style=\"border:1px solid #ddd; background-color: #fff;\">";
            str += "<tr bgcolor=\"#fff\">";
            // str += "<td align=\"center\" style=\"padding: 30px 0;\"><a href=\"#\"><img src=\"http://win.infomanav.com/Content/User/images/logo.png\"></a></td>";
            str += "</tr><tr>";
            // str += "<td valign=\"center\" style=\"background: url(http://win.infomanav.com/Content/User/images/banner.jpg); width: 600px; height: 113px; vertical-align: middle; color: #fff; text-align: center;font-family: 'Lato', sans-serif !important;\">";
            str += "<h1>      Change Your Password</h1>";
            str += "</td></tr>";
            str += "<tr><td>";
            str += "<table cellpadding=\"0\" cellspacing=\"0\" width=\"600\" align=\"center\" style=\"padding-left:40px; padding-right: 40px; padding-bottom: 50px;\" bgcolor=\"#fff\">";
            str += "<tr><td>";
            str += "<p style=\"padding-top: 50px; color: #ea5a56; font-family: 'Lato', sans-serif !important;\">Hello " + Name + ",</p>";
            str += "<p style=\"color: #333; font-family: 'Lato', sans-serif !important;\">";
            //str += "Please Find the Below Link of the Memory <br><br>";
            //str += "<br/>";
            // str += "Password = " + UserPassword + " <br>";
            str += "<p style=\"padding-top: 10px; font-family: 'Lato', sans-serif !important;\">";
            str += "Please click on the link below to change you password’.<br><br>";
            str += "<a type=\"button\" href=\"" + changePasswordLink + "\" style=\"border-radius: 3px;background:#ea5a56; border:0px; padding:5px 5px;color:#FFF;cursor:pointer;\"><input type=\"submit\" class=\"button_active\" value=\"Click Here\" style=\"border-radius: 3px;background:#ea5a56; border:0px; padding:5px 5px;;color:#FFF;cursor:pointer;\" /></a><br>";
           // str += " " + changePasswordLink + " ";
            str += "</p>";
            str += "<p style=\"padding-top: 10px; font-family: 'Lato', sans-serif !important;\">";
            // str += "For any further assistance please feel free to write to us at, contact@theartstrust.com or call us on 91-22 2204 8138/39. We will be glad to assist you.<br>";
            str += "</p>";
            str += "<p style=\"padding-top: 10px; font-family: 'Lato', sans-serif !important;\">";
            str += "Warm Regards,<br>";
            str += "Team IPL";
            str += "</p>";
            str += "</tr></table></td></tr>";
            str += "<tr bgcolor=\"#f1f4f6\">";
            str += "<td style=\"padding: 10px 40px; color: #333333; font-size: 13px;font-family: 'Lato', sans-serif !important; line-height: 24px;\">Copyright 2020. All rights reserved.</td>";
            str += "</tr>";
            str += "</table></body></html>";
            Message = str;
            return Message;
        }
    }
}