﻿using Avigma.Repository.Lib;
using IPL.Models.Avigma;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Net;
namespace IPL.Repository.Lib
{
    public class UserVerificationMaster_Data
    {

        MyDataSourceFactory obj = new MyDataSourceFactory();
        SecurityHelper securityHelper = new SecurityHelper();
        Log log = new Log();


        private List<dynamic> AddUpdateUserVerificationMaster_Data(UserVerificationMaster_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_UserVerificationMaster]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@UserVeriPKID", 1 + "#bigint#" + model.UserVeriPKID);
                input_parameters.Add("@UserVerificationID", 1 + "#varchar#" + model.UserVerificationID);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@VerificationCode", 1 + "#varchar#" + model.VerificationCode);
                input_parameters.Add("@Verification_IsActive", 1 + "#bit#" + model.Verification_IsActive);
                input_parameters.Add("@Verification_IsDelete", 1 + "#bit#" + model.Verification_IsDelete);

                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@VUserId", 1 + "#bigint#" + model.VUserId);
                input_parameters.Add("@UserVeriPKID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);
                objData = obj.SqlCRUD(insertProcedure, input_parameters);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;



        }
        public List<dynamic> AddUserVerificationMaster_Data(UserVerificationMaster_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                objData = AddUpdateUserVerificationMaster_Data(model);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;
        }

        private DataSet Get_UserVerificationMaster(UserVerificationMaster_DTO model)
        {
            DataSet ds = null;
            try
            {

                string selectProcedure = "[Get_UserVerificationMaster]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@UserVeriPKID", 1 + "#bigint#" + model.UserVeriPKID);

                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> Get_UserVerificationMasterDetails(UserVerificationMaster_DTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = Get_UserVerificationMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<UserVerificationMaster_DTO> UserVerificationMaster =
                   (from item in myEnumerableFeaprd
                    select new UserVerificationMaster_DTO
                    {
                        UserVeriPKID = item.Field<Int64>("UserVeriPKID"),
                        UserVerificationID = item.Field<String>("UserVerificationID"),
                        UserID = item.Field<Int64?>("UserID"),
                        VerificationCode = item.Field<String>("VerificationCode"),
                        Verification_IsActive = item.Field<Boolean?>("Verification_IsActive"),
                    }).ToList();

                objDynamic.Add(UserVerificationMaster);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        //pradeep
        private List<dynamic> ChangeUserPassword(UserVerificationMaster_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[Get_CheckUserVerificationData]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                
                input_parameters.Add("@UserVerificationID", 1 + "#varchar#" + model.UserVerificationID);
                input_parameters.Add("@VerificationCode", 1 + "#varchar#" + model.VerificationCode);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@User_LoginName", 2 + "#nvarchar#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);
                objData = obj.SqlCRUD(insertProcedure, input_parameters);


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objData;



        }
        public List<dynamic> ChangeUser_Password(UserVerificationMaster_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            try
            {
                #region comment
                //string decriptedUid = securityHelper.Decrypt(WebUtility.UrlDecode(model.UserVerificationID), false
                //    // string d = securityHelper.Decrypt("tVmd4ykoIE=", false);
                //string dercUserVerificationCode = securityHelper.Decrypt(model.VerificationCode.ToString(), false);
                //model.UserVerificationID = decriptedUid;
                //model.VerificationCode = dercUserVerificationCode;
                //model.UserID = model.UserID;
                #endregion

                if (!string.IsNullOrWhiteSpace(model.UserVerificationID))
                {
                    string decriptedUid = securityHelper.Decrypt(model.UserVerificationID, false);
                   model.UserID = Convert.ToInt64(decriptedUid);
                   
                    objData = ChangeUserPassword(model);

                }
               
                
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(model.UserVerificationID);
            }
            return objData;
        }
    }
}