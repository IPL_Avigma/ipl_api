﻿using Avigma.Models;
using Avigma.Repository.Lib;
using Google.Cloud.Firestore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using static iTextSharp.text.pdf.AcroFields;

namespace IPL.Repository.Lib
{
    public static class DynamicEmailManager
    {
        static string jsonpath = System.Configuration.ConfigurationManager.AppSettings["GooglebucketJsom"];
        public static async Task<int> SendDynamicEmail(DyanmicEmailDTO dyanmicEmailDTO,int Flag)
        {
            Log log = new Log();
            try
            {
                Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", jsonpath);
                FirestoreDb firestoreDb = FirestoreDb.Create("rare-lambda-245821");

                List<object> attachements = new List<object>();

                if (dyanmicEmailDTO.FileLinkes != null)
                {
                    foreach (var item in dyanmicEmailDTO.FileLinkes)
                    {
                        attachements.Add(new Dictionary<string, object> { { "path", item }, });
                    }
                }

                Dictionary<string, object> sendobject = new Dictionary<string, object>
                 {
                   { "from",dyanmicEmailDTO.From },
                   { "to", dyanmicEmailDTO.To },
                   { "cc", dyanmicEmailDTO.Cc },
                   { "bcc", dyanmicEmailDTO.Bcc },
                   { "message", new Dictionary<string, object>
                             {
                               { "subject", dyanmicEmailDTO.Subject},
                               { "html", dyanmicEmailDTO.Message },
                               { "attachments" , attachements}
                             }},
                 };

                if (!string.IsNullOrWhiteSpace(dyanmicEmailDTO.Subject) && !string.IsNullOrWhiteSpace(dyanmicEmailDTO.Message))
                {
                    DocumentReference addedDocRef = await firestoreDb.Collection("mail").AddAsync(sendobject);
                    log.logDebugMessage("Dynamic email sent  Reference Id :" + addedDocRef.Id + "--->" + Flag.ToString());
                }
                else
                {
                    log.logDebugMessage("Subject OR Message is Empty------------>Subject"+ dyanmicEmailDTO.Subject + "Message---->"+ dyanmicEmailDTO.Message+ "--->"+Flag.ToString());
                }
               

                return 1;
            }
            catch (Exception ex)
            {

                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                return 0;
            }
        }
    }

}