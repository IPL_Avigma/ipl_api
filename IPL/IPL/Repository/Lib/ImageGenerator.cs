﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.IO;
using Avigma.Repository.Lib;
using System.Drawing;

namespace Avigma.Repository.Lib
{
    public class ImageGenerator
    {
          Log log = new Log();
        public string Base64ToImage(string base64String)
        {
            string imgPath = "";
            string ImagePath = string.Empty;
            string strpath = System.Configuration.ConfigurationManager.AppSettings["UserImagePath"];
            string strDBpath = System.Configuration.ConfigurationManager.AppSettings["UserUploadImageDBPath"];

            try
            {
                if (!string.IsNullOrEmpty(base64String))
                {
                    string imageName = Regex.Match(base64String, @"data:image/(?<type>.+?),(?<data>.+)").Groups["data"].Value;
                    string imageextension = "";

                    //string imageName = ".jpg";

                    //if (imageextension==".jpg"|| imageextension==".bmp"|| imageextension==".jpeg"|| imageextension==".svg" || imageextension == ".png")
                    //{
                    //    imageName = imageextension;
                    //}
                    if (base64String.Split(',')[0].Contains("png"))
                    {
                        imageextension = ".png";
                    }

                    imageextension = ".png";
                    var newfileName = Guid.NewGuid() + imageextension;
                    ImagePath = strDBpath + newfileName;
                    var path = strpath + "\\" + newfileName;
                    //files.SaveAs(path);
                    //set the image path
                    imgPath = Path.Combine(path, newfileName);

                    byte[] imageBytes = Convert.FromBase64String(imageName);

                    File.WriteAllBytes(path, imageBytes);

                }
                return ImagePath;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                imgPath = ex.Message;
                return ImagePath;
            }


        }

        public string ImagefiletoBase64(string ImagePath)
        {
            string base64String = string.Empty;
            try
            {
                using (Image image = Image.FromFile(ImagePath))
                {
                    using (MemoryStream m = new MemoryStream())
                    {
                        image.Save(m, image.RawFormat);
                        byte[] imageBytes = m.ToArray();

                        // Convert byte[] to Base64 String
                        base64String = Convert.ToBase64String(imageBytes);
                        return base64String;
                    }
                }
            }
            catch (Exception ex)
            {

                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                return base64String;

            }
           
        }


        public string ReadFileDataToBase64(string FilePath)
        {
            string base64String = string.Empty;
            try
            {
                byte[] file = System.IO.File.ReadAllBytes(FilePath);
                base64String = Convert.ToBase64String(file);
            }
            catch (Exception ex)
            {

                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                return base64String;

            }
            return base64String;

        }

        public string Base64ToVideo(string base64String)
        {
            string imgPath = "";
            string ImagePath = string.Empty;
            string strpath = System.Configuration.ConfigurationManager.AppSettings["UserImagePath"];
            string strDBpath = System.Configuration.ConfigurationManager.AppSettings["UserUploadImageDBPath"];

            try
            {
                if (!string.IsNullOrEmpty(base64String))
                {
                    string imageName = Regex.Match(base64String, @"data:image/(?<type>.+?),(?<data>.+)").Groups["data"].Value;
                    string imageextension = "";

                    //string imageName = ".jpg";

                    //if (imageextension==".jpg"|| imageextension==".bmp"|| imageextension==".jpeg"|| imageextension==".svg" || imageextension == ".png")
                    //{
                    //    imageName = imageextension;
                    //}
                    if (base64String.Split(',')[0].Contains("mp4"))
                    {
                        imageextension = ".mp4";
                    }

                    imageextension = ".mp4";
                    var newfileName = Guid.NewGuid() + imageextension;
                    ImagePath = strDBpath + newfileName;
                    var path = strpath + "\\" + newfileName;
                    //files.SaveAs(path);
                    //set the image path
                    imgPath = Path.Combine(path, newfileName);

                    byte[] imageBytes = Convert.FromBase64String(imageName);

                    File.WriteAllBytes(path, imageBytes);

                }
                return ImagePath;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                imgPath = ex.Message;
                return ImagePath;
            }


        }

        public string Base64ToImageList(string base64String)
        {
            string imgPath = "";
            string ImagePath = string.Empty;
            string strpath = System.Configuration.ConfigurationManager.AppSettings["UserImagePath"];
            string strDBpath = System.Configuration.ConfigurationManager.AppSettings["UserUploadImageDBPath"];

            try
            {
                if (!string.IsNullOrEmpty(base64String))
                {
                    string imageName = base64String;
                    string imageextension = "";

                    //string imageName = ".jpg";

                    //if (imageextension==".jpg"|| imageextension==".bmp"|| imageextension==".jpeg"|| imageextension==".svg" || imageextension == ".png")
                    //{
                    //    imageName = imageextension;
                    //}
                    if (base64String.Split(',')[0].Contains("png"))
                    {
                        imageextension = ".png";
                    }

                    imageextension = ".png";
                    var newfileName = Guid.NewGuid() + imageextension;
                    ImagePath = strDBpath + newfileName;
                    var path = strpath + "\\" + newfileName;
                    //files.SaveAs(path);
                    //set the image path
                    imgPath = Path.Combine(path, newfileName);

                    byte[] imageBytes = Convert.FromBase64String(imageName);

                    File.WriteAllBytes(path, imageBytes);

                }
                return ImagePath;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                imgPath = ex.Message;
                return ImagePath;
            }


        }
    }
}