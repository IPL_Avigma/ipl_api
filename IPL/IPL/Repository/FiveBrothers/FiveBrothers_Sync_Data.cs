﻿using Avigma.Repository.Lib;
using IPL.Models;
using IPL.Models.FiveBrothers;
using IPL.Repository.data;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;

namespace IPL.Repository.FiveBrothers
{
    public class FiveBrothers_Sync_Data
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Invoice_Client_ChildData invoice_Client_ChildData = new Invoice_Client_ChildData();
        TaskBidMasterData taskBidMasterData = new TaskBidMasterData();
        Client_Result_PhotoData client_Result_PhotoData = new Client_Result_PhotoData();
        PCR_FiveBrotherData pCR_FiveBrotherData = new PCR_FiveBrotherData();
        PCR_Grass_Cut_MasterData pCR_Grass_Cut_MasterData = new PCR_Grass_Cut_MasterData();
        ImageGenerator imageGenerator = new ImageGenerator();
        SecurityHelper securityHelper = new SecurityHelper();
        Log log = new Log();
        private DataSet GetBidSyncData(Client_Sync_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Bid_ClientSync]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@WorkOrderId", 1 + "#bigint#" + model.WO_Id);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ds;
        }
        private DataSet GetClientSyncCredential(Client_Sync_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_WorkOrder_Import_Credential]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@WI_ImportFrom", 1 + "#bigint#" + model.WI_ImportFrom);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ds;
        }

        private List<Client_Sync_Credential> GetClientSyncCredentialList(Client_Sync_DTO model)
        {
            List<Client_Sync_Credential> client_Sync_CredentialList = null;
            try
            {
                DataSet crDs = GetClientSyncCredential(model);
                if (crDs.Tables.Count > 0)
                {
                    var myEnumerablerd = crDs.Tables[0].AsEnumerable();

                    client_Sync_CredentialList =
                       (from item in myEnumerablerd
                        select new Client_Sync_Credential
                        {
                            WI_ImportFrom = item.Field<Int64?>("WI_ImportFrom"),
                            Auto_IPL_Company_PkeyId = item.Field<Int64?>("Auto_IPL_Company_PkeyId"),
                            WI_LoginName = item.Field<String>("WI_LoginName"),
                            WI_Password = item.Field<String>("WI_Password"),
                            WI_FB_LoginName = item.Field<String>("WI_FB_LoginName"),
                            WI_FB_Password = item.Field<String>("WI_FB_Password"),
                            ApiUrl = item.Field<String>("ApiUrl"),
                        }).ToList();
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return client_Sync_CredentialList;
        }
        public List<dynamic> UploadBidSync(Client_Sync_DTO model)
        {
            List<dynamic> objDataModel = new List<dynamic>();
            int success = 0;
            int fail = 0;
            try
            {
                model.WI_ImportFrom = 4; // For FiveBrothers 

                List<Client_Sync_Credential> client_Sync_CredentialList = GetClientSyncCredentialList(model);

                if (client_Sync_CredentialList != null && client_Sync_CredentialList.Count > 0)
                {
                    client_Sync_CredentialList[0].WI_Password = securityHelper.GetMD5Hash(client_Sync_CredentialList[0].WI_Password);

                    string url = client_Sync_CredentialList[0].ApiUrl.Trim() + "bid" + "?contractor=" + client_Sync_CredentialList[0].WI_LoginName + "&key=" + client_Sync_CredentialList[0].WI_Password;

                    byte[] credentials = UTF8Encoding.UTF8.GetBytes(client_Sync_CredentialList[0].WI_FB_LoginName + ":" + client_Sync_CredentialList[0].WI_FB_Password);
                    System.Net.Http.HttpClientHandler handler = new System.Net.Http.HttpClientHandler();// Added this condition to disbale SSL Validation 
                    handler.ServerCertificateCustomValidationCallback += (sender, cert, chain, sslPolicyErrors) => true; // Added this condition to disbale SSL Validation 
                    System.Net.Http.HttpClient client = new System.Net.Http.HttpClient(handler);

                    client.BaseAddress = new Uri(url);

                    client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(credentials));

                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                    DataSet ds = GetBidSyncData(model);

                    if (ds.Tables.Count > 0)
                    {
                        var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();

                        List<Ipl_Fb_BidUploadDTO> ipl_Fb_BidUploadList =
                           (from item in myEnumerableFeaprd
                            select new Ipl_Fb_BidUploadDTO
                            {
                                ordernumber = item.Field<String>("Ordernumber"),
                                itemCode = item.Field<String>("ItemCode"),
                                category = item.Field<String>("Category"),
                                description = item.Field<String>("Description"),
                                quantity = item.Field<String>("Quantity"),
                                unit = item.Field<String>("Unit"),
                                unitPrice = item.Field<Decimal?>("UnitPrice").ToString(),
                                amount = item.Field<Decimal?>("Amount").ToString(),
                                TaskName = item.Field<String>("TaskName"),
                                Task_Bid_PkeyId = item.Field<Int64>("Task_Bid_PkeyId"),
                                damagecause = item.Field<String>("DamageCause"),
                                location = item.Field<String>("Task_Ext_Location"),
                                length = item.Field<String>("Task_Ext_Length"),
                                width = item.Field<String>("Task_Ext_Width"),
                                height = item.Field<String>("Task_Ext_Height"),
                                men = item.Field<String>("Task_Ext_Men"),
                                hours = item.Field<String>("Task_Ext_Hours"),
                            }).ToList();

                        for (int i = 0; i < ipl_Fb_BidUploadList.Count; i++)
                        {
                            Ipl_Fb_BidUploadJsonDTO ipl_Fb_BidUploadJsonDTO = new Ipl_Fb_BidUploadJsonDTO();
                            ipl_Fb_BidUploadJsonDTO.ordernumber = ipl_Fb_BidUploadList[i].ordernumber;
                            ipl_Fb_BidUploadJsonDTO.itemCode = ipl_Fb_BidUploadList[i].itemCode;
                            ipl_Fb_BidUploadJsonDTO.category = ipl_Fb_BidUploadList[i].category;
                            ipl_Fb_BidUploadJsonDTO.description = ipl_Fb_BidUploadList[i].description;
                            ipl_Fb_BidUploadJsonDTO.quantity = ipl_Fb_BidUploadList[i].quantity;
                            ipl_Fb_BidUploadJsonDTO.unit = ipl_Fb_BidUploadList[i].unit;
                            ipl_Fb_BidUploadJsonDTO.unitPrice = ipl_Fb_BidUploadList[i].unitPrice;
                            ipl_Fb_BidUploadJsonDTO.amount = ipl_Fb_BidUploadList[i].amount;
                            ipl_Fb_BidUploadJsonDTO.damagecause = ipl_Fb_BidUploadList[i].damagecause;
                            ipl_Fb_BidUploadJsonDTO.location = ipl_Fb_BidUploadList[i].location;
                            ipl_Fb_BidUploadJsonDTO.length = ipl_Fb_BidUploadList[i].length;
                            ipl_Fb_BidUploadJsonDTO.width = ipl_Fb_BidUploadList[i].width;
                            ipl_Fb_BidUploadJsonDTO.height = ipl_Fb_BidUploadList[i].height;
                            ipl_Fb_BidUploadJsonDTO.men = ipl_Fb_BidUploadList[i].men;
                            ipl_Fb_BidUploadJsonDTO.hours = ipl_Fb_BidUploadList[i].hours;
                            var json = JsonConvert.SerializeObject(ipl_Fb_BidUploadJsonDTO);
                            var data = new StringContent(json, Encoding.UTF8, "application/json");

                            HttpResponseMessage messge = client.PostAsync(url, data).Result;
                            string result = messge.Content.ReadAsStringAsync().Result;
                            if (messge.IsSuccessStatusCode)
                            {
                                ipl_Fb_BidUploadList[i].Status = "Success";
                                success = success + 1;
                                TaskBidMasterDTO taskBidMasterDTO = new TaskBidMasterDTO();
                                taskBidMasterDTO.Task_Bid_pkeyID = ipl_Fb_BidUploadList[i].Task_Bid_PkeyId;
                                taskBidMasterDTO.UserID = model.UserID;
                                taskBidMasterDTO.Task_Bid_WO_ID = model.WO_Id.GetValueOrDefault(0);
                                taskBidMasterDTO.Type = 6;
                                var updateTaskBid = taskBidMasterData.AddTaskBidData(taskBidMasterDTO);
                            }
                            else
                            {
                                ipl_Fb_BidUploadList[i].Status = "Failure";
                                var errormsg = JsonConvert.DeserializeObject<Client_Sync_DTO>(result);
                                ipl_Fb_BidUploadList[i].Message = errormsg.message;
                                fail = fail + 1;
                            }
                        }

                        objDataModel.Add(ipl_Fb_BidUploadList);
                        objDataModel.Add(success);
                        objDataModel.Add(fail);
                    }
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objDataModel;
        }

        public List<dynamic> UploadInvoiceSync(Client_Sync_DTO model)
        {
            List<dynamic> objDataModel = new List<dynamic>();
            int success = 0;
            int fail = 0;
            try
            {
                model.WI_ImportFrom = 4; // For FiveBrothers 

                List<Client_Sync_Credential> client_Sync_CredentialList = GetClientSyncCredentialList(model);

                if (client_Sync_CredentialList != null && client_Sync_CredentialList.Count > 0)
                {
                    client_Sync_CredentialList[0].WI_Password = securityHelper.GetMD5Hash(client_Sync_CredentialList[0].WI_Password);

                    string url = client_Sync_CredentialList[0].ApiUrl.Trim() + "invoice" + "?contractor=" + client_Sync_CredentialList[0].WI_LoginName + "&key=" + client_Sync_CredentialList[0].WI_Password;

                    byte[] credentials = UTF8Encoding.UTF8.GetBytes(client_Sync_CredentialList[0].WI_FB_LoginName + ":" + client_Sync_CredentialList[0].WI_FB_Password);
                    System.Net.Http.HttpClientHandler handler = new System.Net.Http.HttpClientHandler();// Added this condition to disbale SSL Validation 
                    handler.ServerCertificateCustomValidationCallback += (sender, cert, chain, sslPolicyErrors) => true; // Added this condition to disbale SSL Validation 
                    System.Net.Http.HttpClient client = new System.Net.Http.HttpClient(handler);

                    client.BaseAddress = new Uri(url);

                    client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(credentials));

                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                    DataSet ds = GetInvoiceSyncData(model);

                    if (ds.Tables.Count > 0)
                    {
                        var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();

                        List<Ipl_Fb_InvoiceUploadDTO> ipl_Fb_InvoiceUploadList =
                           (from item in myEnumerableFeaprd
                            select new Ipl_Fb_InvoiceUploadDTO
                            {
                                ordernumber = Convert.ToInt32(item.Field<String>("Ordernumber")),
                                itemCode = item.Field<String>("ItemCode"),
                                description = item.Field<String>("Description"),
                                quantity = item.Field<String>("Quantity"),
                                unitPrice = item.Field<Decimal?>("UnitPrice").ToString(),
                                amount = item.Field<Decimal?>("Amount").ToString(),
                                nodiscount = item.Field<Boolean?>("Discount"),
                                TaskName = item.Field<String>("TaskName"),
                                Inv_Child_PkeyId = item.Field<Int64>("Inv_Child_PkeyId"),
                            }).ToList();

                        for (int i = 0; i < ipl_Fb_InvoiceUploadList.Count; i++)
                        {
                            Ipl_Fb_InvoiceUploadJsonDTO ipl_Fb_InvoiceUploadJsonDTO = new Ipl_Fb_InvoiceUploadJsonDTO();
                            ipl_Fb_InvoiceUploadJsonDTO.ordernumber = ipl_Fb_InvoiceUploadList[i].ordernumber;
                            ipl_Fb_InvoiceUploadJsonDTO.itemCode = ipl_Fb_InvoiceUploadList[i].itemCode;
                            ipl_Fb_InvoiceUploadJsonDTO.description = ipl_Fb_InvoiceUploadList[i].description;
                            ipl_Fb_InvoiceUploadJsonDTO.quantity = ipl_Fb_InvoiceUploadList[i].quantity;
                            ipl_Fb_InvoiceUploadJsonDTO.unitPrice = ipl_Fb_InvoiceUploadList[i].unitPrice;
                            ipl_Fb_InvoiceUploadJsonDTO.amount = ipl_Fb_InvoiceUploadList[i].amount;
                            ipl_Fb_InvoiceUploadJsonDTO.nodiscount = ipl_Fb_InvoiceUploadList[i].nodiscount;

                            var json = JsonConvert.SerializeObject(ipl_Fb_InvoiceUploadJsonDTO);
                            var data = new StringContent(json, Encoding.UTF8, "application/json");

                            HttpResponseMessage messge = client.PostAsync(url, data).Result;
                            string result = messge.Content.ReadAsStringAsync().Result;
                            if (messge.IsSuccessStatusCode)
                            {
                                ipl_Fb_InvoiceUploadList[i].Status = "Success";
                                success = success + 1;
                                Invoice_Client_ChildDTO invoice_Client_ChildDTO = new Invoice_Client_ChildDTO();
                                invoice_Client_ChildDTO.Inv_Client_Ch_pkeyId = ipl_Fb_InvoiceUploadList[i].Inv_Child_PkeyId;
                                invoice_Client_ChildDTO.UserID = model.UserID;
                                invoice_Client_ChildDTO.Inv_Client_Ch_Wo_Id = model.WO_Id;
                                invoice_Client_ChildDTO.Type = 5;
                                var updateInvTask = invoice_Client_ChildData.AddInvoiceClientChildData(invoice_Client_ChildDTO);                                                             
                            }
                            else
                            {
                                ipl_Fb_InvoiceUploadList[i].Status = "Failure";
                                var errormsg = JsonConvert.DeserializeObject<Client_Sync_DTO>(result);
                                ipl_Fb_InvoiceUploadList[i].Message = errormsg.message;
                                fail = fail + 1;
                            }
                        }
                        objDataModel.Add(ipl_Fb_InvoiceUploadList);
                        objDataModel.Add(success);
                        objDataModel.Add(fail);
                    }
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objDataModel;
        }

        private DataSet GetInvoiceSyncData(Client_Sync_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Invoice_ClientSync]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@WorkOrderId", 1 + "#bigint#" + model.WO_Id);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ds;
        }
        public List<dynamic> UploadPreservation(Client_Sync_DTO model)
        {
            List<dynamic> objDataModel = new List<dynamic>();
            int success = 0;
            int fail = 0;
            try
            {
                model.WI_ImportFrom = 4; // For FiveBrothers 

                List<Client_Sync_Credential> client_Sync_CredentialList = GetClientSyncCredentialList(model);

                if (client_Sync_CredentialList != null && client_Sync_CredentialList.Count > 0)
                {
                    client_Sync_CredentialList[0].WI_Password = securityHelper.GetMD5Hash(client_Sync_CredentialList[0].WI_Password);

                    string url = client_Sync_CredentialList[0].ApiUrl.Trim() + "preservation" + "?contractor=" + client_Sync_CredentialList[0].WI_LoginName + "&key=" + client_Sync_CredentialList[0].WI_Password;

                    byte[] credentials = UTF8Encoding.UTF8.GetBytes(client_Sync_CredentialList[0].WI_FB_LoginName + ":" + client_Sync_CredentialList[0].WI_FB_Password);
                    System.Net.Http.HttpClientHandler handler = new System.Net.Http.HttpClientHandler();// Added this condition to disbale SSL Validation 
                    handler.ServerCertificateCustomValidationCallback += (sender, cert, chain, sslPolicyErrors) => true; // Added this condition to disbale SSL Validation 
                    System.Net.Http.HttpClient client = new System.Net.Http.HttpClient(handler);

                    client.BaseAddress = new Uri(url);

                    client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(credentials));

                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                    DataSet ds = GetPreservationSyncData(model);
                    string result = "";
                    if (ds.Tables.Count > 0)
                    {
                        var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();

                        List<Ipl_Fb_PreservationDTO> ipl_Fb_PreservationUploadList =
                           (from item in myEnumerableFeaprd
                            select new Ipl_Fb_PreservationDTO
                            {
                                ordernumber = Convert.ToInt32(item.Field<String>("ordernumber")),
                                uploadedDate = item.Field<DateTime?>("uploadedDate"),
                                dateCompleted = item.Field<DateTime?>("dateCompleted") == null ? item.Field<DateTime?>("uploadedDate") : item.Field<DateTime?>("dateCompleted"),
                                contractor = item.Field<String>("contractor"),
                                version = item.Field<String>("version"),
                                formType = item.Field<String>("formType"),
                                subcontnum = item.Field<String>("subcontnum"),
                                badAddress = item.Field<String>("badAddress"),
                                orderCompleted = item.Field<String>("orderCompleted"),
                                forSale = item.Field<String>("forSale"),
                                soldSign = item.Field<String>("soldSign"),
                                realtorPhone = item.Field<String>("realtorPhone"),
                                realtorName = item.Field<String>("realtorName"),
                                activeListing = item.Field<String>("activeListing"),
                                propertyType = item.Field<String>("propertyType"),
                                garage = item.Field<String>("garage"),
                                basement = item.Field<String>("basement"),
                                occupancy = item.Field<String>("occupancy"),
                                propertyMaintainedBy = item.Field<String>("propertyMaintainedBy"),
                                propertyMaintainedByOther = item.Field<String>("propertyMaintainedByOther"),
                                propertyMaintainedUtilities = item.Field<String>("propertyMaintainedUtilities"),
                                propertyMaintainedGrass = item.Field<String>("propertyMaintainedGrass"),
                                propertyMaintainedSnowRemoval = item.Field<String>("propertyMaintainedSnowRemoval"),
                                propertyMaintainedInteriorRepairs = item.Field<String>("propertyMaintainedInteriorRepairs"),
                                propertyMaintainedExteriorRepairs = item.Field<String>("propertyMaintainedExteriorRepairs"),
                                permitRequired = item.Field<String>("permitRequired"),
                                permitNumber = item.Field<String>("permitNumber"),
                                hoaPresent = item.Field<String>("hoaPresent"),
                                hoaName = item.Field<String>("hoaName"),
                                hoaPhone = item.Field<String>("hoaPhone"),
                                multiUnitProperty = item.Field<String>("multiUnitProperty"),
                                numberOfUnits = item.Field<String>("numberOfUnits"),
                                commonEntry = item.Field<String>("commonEntry"),
                                occupiedUnit1 = item.Field<String>("occupiedUnit1"),
                                occupiedUnit2 = item.Field<String>("occupiedUnit2"),
                                occupiedUnit3 = item.Field<String>("occupiedUnit3"),
                                occupiedUnit4 = item.Field<String>("occupiedUnit4"),
                                occupancyVerifiedOwner = item.Field<String>("occupancyVerifiedOwner"),
                                occupancyVerifiedPersonalsVisible = item.Field<String>("occupancyVerifiedPersonalsVisible"),
                                occupancyVerifiedNeighbor = item.Field<String>("occupancyVerifiedNeighbor"),
                                occupancyVerifiedUtilitiesOn = item.Field<String>("occupancyVerifiedUtilitiesOn"),
                                occupancyVerifiedVisual = item.Field<String>("occupancyVerifiedVisual"),
                                occupancyVerifiedTenant = item.Field<String>("occupancyVerifiedTenant"),
                                occupancyVerifiedMortgagor = item.Field<String>("occupancyVerifiedMortgagor"),
                                occupancyVerifiedUnknown = item.Field<String>("occupancyVerifiedUnknown"),
                                occupancyVerifiedOther = item.Field<String>("occupancyVerifiedOther"),
                                numberOfGarages = Convert.ToInt32(item.Field<String>("numberOfGarages")),
                                numberOfSheds = Convert.ToInt32(item.Field<String>("numberOfSheds")),
                                numberOfBarns = Convert.ToInt32(item.Field<String>("numberOfBarns")),
                                numberOfCarports = Convert.ToInt32(item.Field<String>("numberOfCarports")),
                                numberOfPoolHouse = Convert.ToInt32(item.Field<String>("numberOfPoolHouse")),
                                numberOfOtherbuildings = Convert.ToInt32(item.Field<String>("numberOfOtherbuildings")),
                                violationPosted = item.Field<String>("violationPosted"),
                                violationComment = item.Field<String>("violationComment"),
                                highVandalismArea = item.Field<String>("highVandalismArea"),
                                highVandalismComment = item.Field<String>("highVandalismComment"),
                                unusualCircumstances = item.Field<String>("unusualCircumstances"),
                                unusualCircumstancesComment = item.Field<String>("unusualCircumstancesComment"),
                                mobileHome = item.Field<String>("mobileHome"),
                                mobileHomeMake = item.Field<String>("mobileHomeMake"),
                                mobileHomeModel = item.Field<String>("mobileHomeModel"),
                                mobileHomeVIN1 = item.Field<String>("mobileHomeVIN1"),
                                mobileHomeVIN2 = item.Field<String>("mobileHomeVIN2"),
                                mobileHomeVIN3 = item.Field<String>("mobileHomeVIN3"),
                                mobileHomeAttachedToFound = item.Field<String>("mobileHomeAttachedToFound"),
                                mobileHomeTongue = item.Field<String>("mobileHomeTongue"),
                                mobileHomeSkirted = item.Field<String>("mobileHomeSkirted"),
                                mobileHomeSize = item.Field<String>("mobileHomeSize"),
                                mobileHomeHudTag1 = item.Field<String>("mobileHomeHudTag1"),
                                mobileHomeHudTag2 = item.Field<String>("mobileHomeHudTag2"),
                                mobileHomeLength = item.Field<String>("mobileHomeLength"),
                                mobileHomeWidth = item.Field<String>("mobileHomeWidth"),
                                mobileHomeYear = item.Field<String>("mobileHomeYear"),
                                secureOnArrival = item.Field<String>("secureOnArrival"),
                                notSecureOnArrivalDoorsMissing = item.Field<String>("notSecureOnArrivalDoorsMissing"),
                                notSecureOnArrivalDoorsBroken = item.Field<String>("notSecureOnArrivalDoorsBroken"),
                                notSecureOnArrivalDoorsOpen = item.Field<String>("notSecureOnArrivalDoorsOpen"),
                                notSecureOnArrivalWindowsMissing = item.Field<String>("notSecureOnArrivalDoorsOpen"),
                                notSecureOnArrivalWindowsBroken = item.Field<String>("notSecureOnArrivalWindowsBroken"),
                                notSecureOnArrivalWindowsOpen = item.Field<String>("notSecureOnArrivalWindowsOpen"),
                                notSecureOnArrivalLocksMissing = item.Field<String>("notSecureOnArrivalLocksMissing"),
                                notSecureOnArrivalLocksDamaged = item.Field<String>("notSecureOnArrivalLocksDamaged"),
                                notSecureOnArrivalBidsPending = item.Field<String>("notSecureOnArrivalBidsPending"),
                                secureOnDeparture = item.Field<String>("secureOnDeparture"),
                                notSecureOnDepartureDoorsMissing = item.Field<String>("notSecureOnDepartureDoorsMissing"),
                                notSecureOnDepartureDoorsBroken = item.Field<String>("notSecureOnDepartureDoorsBroken"),
                                notSecureOnDepartureBidsPending = item.Field<String>("notSecureOnDepartureBidsPending"),
                                notSecureOnDepartureWindowsMissing = item.Field<String>("notSecureOnDepartureWindowsMissing"),
                                notSecureOnDepartureWindowsBroken = item.Field<String>("notSecureOnDepartureWindowsBroken"),
                                notSecureOnDepartureLocksMissing = item.Field<String>("notSecureOnDepartureLocksMissing"),
                                notSecureOnDepartureLocksDamaged = item.Field<String>("notSecureOnDepartureLocksDamaged"),
                                numberOfFirstFloorWindows = Convert.ToInt32(item.Field<String>("numberOfFirstFloorWindows")),
                                boardedOnArrival = item.Field<String>("boardedOnArrival"),
                                boardedOnArrivalComment = item.Field<String>("boardedOnArrivalComment"),
                                previouslySecured = item.Field<String>("previouslySecured"),
                                propertySecured = item.Field<String>("propertySecured"),
                                propertyNotSecuredDescription = item.Field<String>("propertyNotSecuredDescription"),
                                performedBoarding = item.Field<String>("performedBoarding"),
                                installedKnoblocks = item.Field<String>("installedKnoblocks"),
                                knoblockCode = item.Field<String>("knoblockCode"),
                                knoblockLocation = item.Field<String>("knoblockLocation"),
                                knoblockQuantity = item.Field<String>("knoblockQuantity"),
                                installedPadlocks = item.Field<String>("installedPadlocks"),
                                padlockCode = item.Field<String>("padlockCode"),
                                padlockLocation = item.Field<String>("padlockLocation"),
                                padlockQuantity = item.Field<String>("padlockQuantity"),
                                installedSliderlocks = item.Field<String>("installedSliderlocks"),
                                installedSlidebolts = item.Field<String>("installedSlidebolts"),
                                installedDeadbolts = item.Field<String>("installedDeadbolts"),
                                deadboltCode = item.Field<String>("deadboltCode"),
                                deadboltLocation = item.Field<String>("deadboltLocation"),
                                deadboltQuantity = item.Field<String>("deadboltQuantity"),
                                installedLockbox = item.Field<String>("installedLockbox"),
                                lockboxCode = item.Field<String>("lockboxCode"),
                                lockboxComment = item.Field<String>("lockboxComment"),
                                waterOnArrival = item.Field<String>("waterOnArrival"),
                                waterOnDeparture = item.Field<String>("waterOnDeparture"),
                                gasOnArrival = item.Field<String>("gasOnArrival"),
                                gasOnDeparture = item.Field<String>("gasOnDeparture"),
                                electricOnArrival = item.Field<String>("electricOnArrival"),
                                electricOnDeparture = item.Field<String>("electricOnDeparture"),
                                utilityTransferred = item.Field<String>("utilityTransferred"),
                                utilityNotTransferredReason = item.Field<String>("utilityNotTransferredReason"),
                                waterCompanyAccountNumber = item.Field<String>("waterCompanyAccountNumber"),
                                waterCompanyName = item.Field<String>("waterCompanyName"),
                                waterCompanyAddress = item.Field<String>("waterCompanyAddress"),
                                waterCompanyPhone = item.Field<String>("waterCompanyPhone"),
                                gasCompanyAccountNumber = item.Field<String>("gasCompanyAccountNumber"),
                                gasCompanyName = item.Field<String>("gasCompanyName"),
                                gasCompanyAddress = item.Field<String>("gasCompanyAddress"),
                                gasCompanyPhone = item.Field<String>("gasCompanyPhone"),
                                electricCompanyAccountNumber = item.Field<String>("electricCompanyAccountNumber"),
                                electricCompanyName = item.Field<String>("electricCompanyName"),
                                electricCompanyAddress = item.Field<String>("electricCompanyAddress"),
                                electricCompanyPhone = item.Field<String>("electricCompanyPhone"),
                                sumpPump = item.Field<String>("sumpPump"),
                                sumpPumpStatus = item.Field<String>("sumpPumpStatus"),
                                sumpPumpComment = item.Field<String>("sumpPumpComment"),
                                mainBreaker = item.Field<String>("mainBreaker"),
                                damageFire = item.Field<String>("damageFire"),
                                damageNeglect = item.Field<String>("damageNeglect"),
                                damageVandal = item.Field<String>("damageVandal"),
                                damageFreeze = item.Field<String>("damageFreeze"),
                                damageStorm = item.Field<String>("damageStorm"),
                                damageFlood = item.Field<String>("damageFlood"),
                                damageWater = item.Field<String>("damageWater"),
                                damageWear = item.Field<String>("damageWear"),
                                damageUnfinishedRenovation = item.Field<String>("damageUnfinishedRenovation"),
                                damageStructural = item.Field<String>("damageStructural"),
                                damageExcessiveHumidity = item.Field<String>("damageExcessiveHumidity"),
                                damageRoofleak = item.Field<String>("damageRoofleak"),
                                damageRoofTarped = item.Field<String>("damageRoofTarped"),
                                damageMold = item.Field<String>("damageMold"),
                                damageSeepage = item.Field<String>("damageSeepage"),
                                damageFloodedBasement = item.Field<String>("damageFloodedBasement"),
                                damageFoundationCracks = item.Field<String>("damageFoundationCracks"),
                                damageWetCarpet = item.Field<String>("damageWetCarpet"),
                                damageWaterStains = item.Field<String>("damageWaterStains"),
                                damageFloorSafetyHazard = item.Field<String>("damageFloorSafetyHazard"),
                                damageCausingOtherDamage = item.Field<String>("damageCausingOtherDamage"),
                                damageCausingOtherSafetyIssues = item.Field<String>("damageCausingOtherSafetyIssues"),
                                damageHVAC = item.Field<String>("damageHVAC"),
                                damageElectirc = item.Field<String>("damageElectirc"),
                                damagePlumbing = item.Field<String>("damagePlumbing"),
                                damageUncappedWire = item.Field<String>("damageUncappedWire"),
                                roofShape = item.Field<String>("roofShape"),
                                roofLeak = item.Field<String>("roofLeak"),
                                roofLeakCause = item.Field<String>("roofLeakCause"),
                                roofLeakCauseOther = item.Field<String>("roofLeakCauseOther"),
                                roofLeakLocation = item.Field<String>("roofLeakLocation"),
                                roofDamage = item.Field<String>("roofDamage"),
                                roofDamageLocation = item.Field<String>("roofDamageLocation"),
                                waterStainOnCeiling = item.Field<String>("waterStainOnCeiling"),
                                waterStainOnCeilingCause = item.Field<String>("waterStainOnCeilingCause"),
                                waterStainOnCeilingLocation = item.Field<String>("waterStainOnCeilingLocation"),
                                performedRoofRepair = item.Field<String>("performedRoofRepair"),
                                performedRoofTarping = item.Field<String>("performedRoofTarping"),
                                interiorDebrisPresent = item.Field<String>("interiorDebrisPresent"),
                                interiorDebrisComment = item.Field<String>("interiorDebrisComment"),
                                interiorDebrisCubicYards = item.Field<String>("interiorDebrisCubicYards"),
                                interiorDebrisRemoved = item.Field<String>("interiorDebrisRemoved"),
                                exteriorDebrisPresent = item.Field<String>("exteriorDebrisPresent"),
                                exteriorDebrisComment = item.Field<String>("exteriorDebrisComment"),
                                exteriorDebrisCubicYards = item.Field<String>("exteriorDebrisCubicYards"),
                                exteriorDebrisRemoved = item.Field<String>("exteriorDebrisRemoved"),
                                broomSweptCondition = item.Field<String>("broomSweptCondition"),
                                broomSweptConditionComment = item.Field<String>("broomSweptConditionComment"),
                                debrisVisibleFromStreet = item.Field<String>("debrisVisibleFromStreet"),
                                debrisOnLawn = item.Field<String>("debrisOnLawn"),
                                abandonedVehicle = item.Field<String>("abandonedVehicle"),
                                abandonedVehicleComment = item.Field<String>("abandonedVehicleComment"),
                                dumpFacilityName = item.Field<String>("dumpFacilityName"),
                                dumpFacilityAddress = item.Field<String>("dumpFacilityAddress"),
                                dumpFacilityPhone = item.Field<String>("dumpFacilityPhone"),
                                dumpFacilityComment = item.Field<String>("dumpFacilityComment"),
                                meansOfDisposal = item.Field<String>("meansOfDisposal"),
                                interiorHazardsPresent = item.Field<String>("interiorHazardsPresent"),
                                interiorHazardsComment = item.Field<String>("interiorHazardsComment"),
                                interiorHazardsCubicYards = item.Field<String>("interiorHazardsCubicYards"),
                                exteriorHazardsPresent = item.Field<String>("exteriorHazardsPresent"),
                                exteriorHazardsComment = item.Field<String>("exteriorHazardsComment"),
                                exteriorHazardsCubicYards = item.Field<String>("exteriorHazardsCubicYards"),
                                winterizationIntactOnArrival = item.Field<String>("winterizationIntactOnArrival"),
                                winterizationNotIntactReason = item.Field<String>("winterizationNotIntactReason"),
                                winterizationCompleted = item.Field<String>("winterizationCompleted"),
                                winterizationNotCompletedReasonLowAllowable = item.Field<String>("winterizationNotCompletedReasonLowAllowable"),
                                winterizationNotCompletedReasonOutOfSeason = item.Field<String>("winterizationNotCompletedReasonOutOfSeason"),
                                winterizationNotCompletedReasonDamage = item.Field<String>("winterizationNotCompletedReasonDamage"),
                                winterizationNotCompletedReasonPlumbingMissing = item.Field<String>("winterizationNotCompletedReasonPlumbingMissing"),
                                winterizationNotCompletedReasonPlumbingDamaged = item.Field<String>("winterizationNotCompletedReasonPlumbingDamaged"),
                                winterizationNotCompletedReasonCommonWaterLine = item.Field<String>("winterizationNotCompletedReasonCommonWaterLine"),
                                winterizationNotCompletedReasonFrozen = item.Field<String>("winterizationNotCompletedReasonFrozen"),
                                winterizationNotCompletedReasonWaterNotOff = item.Field<String>("winterizationNotCompletedReasonWaterNotOff"),
                                winterizationNotCompletedReasonAlreadyWinterized = item.Field<String>("winterizationNotCompletedReasonAlreadyWinterized"),
                                winterizationNotCompletedReasonRealtorMaintained = item.Field<String>("winterizationNotCompletedReasonRealtorMaintained"),
                                winterizationNotCompletedReasonOther = item.Field<String>("winterizationNotCompletedReasonOther"),
                                winterizationNotCompletedReasonOtherComment = item.Field<String>("winterizationNotCompletedReasonOtherComment"),
                                winterizationComment = item.Field<String>("winterizationComment"),
                                winterizationSignPosted = item.Field<String>("winterizationSignPosted"),
                                commonWaterLine = item.Field<String>("commonWaterLine"),
                                antifreezeInToilet = item.Field<String>("antifreezeInToilet"),
                                waterheaterDrained = item.Field<String>("waterheaterDrained"),
                                waterTurnedOff = item.Field<String>("waterTurnedOff"),
                                linesBlown = item.Field<String>("linesBlown"),
                                systemHeldPressure = item.Field<String>("systemHeldPressure"),
                                disconnectedWaterMeter = item.Field<String>("disconnectedWaterMeter"),
                                heatingSystemType = item.Field<String>("heatingSystemType"),
                                boilerDrained = item.Field<String>("boilerDrained"),
                                zoneValvesOpened = item.Field<String>("zoneValvesOpened"),
                                antifreezeInBoiler = item.Field<String>("antifreezeInBoiler"),
                                wellPumpBreakerOff = item.Field<String>("wellPumpBreakerOff"),
                                pressureTankDrained = item.Field<String>("pressureTankDrained"),
                                supplyLineDisconnected = item.Field<String>("supplyLineDisconnected"),
                                mainValveShutOff = item.Field<String>("mainValveShutOff"),
                                mainValveShutOffComment = item.Field<String>("mainValveShutOffComment"),
                                fireSuppressionSystem = item.Field<String>("fireSuppressionSystem"),
                                personalPropertyPresent = item.Field<String>("personalPropertyPresent"),
                                interiorPersonalProperty = item.Field<String>("interiorPersonalProperty"),
                                interiorPersonalPropertyValue = item.Field<String>("interiorPersonalPropertyValue"),
                                interiorPersonalPropertyCubicYards = item.Field<String>("interiorPersonalPropertyCubicYards"),
                                exteriorPersonalProperty = item.Field<String>("exteriorPersonalProperty"),
                                exteriorPersonalPropertyValue = item.Field<String>("exteriorPersonalPropertyValue"),
                                exteriorPersonalPropertyCubicYards = item.Field<String>("exteriorPersonalPropertyCubicYards"),
                                poolPresent = item.Field<String>("poolPresent"),
                                poolType = item.Field<String>("poolType"),
                                poolCondition = item.Field<String>("poolCondition"),
                                poolDiameter = item.Field<String>("poolDiameter"),
                                poolLength = item.Field<String>("poolLength"),
                                poolWidth = item.Field<String>("poolWidth"),
                                poolWaterLevel = item.Field<String>("poolWaterLevel"),
                                poolFence = item.Field<String>("poolFence"),
                                poolFenceLocked = item.Field<String>("poolFenceLocked"),
                                poolDrained = item.Field<String>("poolDrained"),
                                poolRemoved = item.Field<String>("poolRemoved"),
                                poolDepressionLeft = item.Field<String>("poolDepressionLeft"),
                                poolCovered = item.Field<String>("poolCovered"),
                                poolSecuredPerGuidelines = item.Field<String>("poolSecuredPerGuidelines"),
                                poolNotSecuredReason = item.Field<String>("poolNotSecuredReason"),
                                hotTub = item.Field<String>("hotTub"),
                                hotTubCovered = item.Field<String>("hotTubCovered"),
                                hotTubSecured = item.Field<String>("hotTubSecured"),
                                grasscutCompleted = item.Field<String>("grasscutCompleted"),
                                grasscutNotCompletedReason = item.Field<String>("grasscutNotCompletedReason"),
                                propertyLotSize = item.Field<String>("propertyLotSize"),
                                grasscutAreaSize = item.Field<String>("grasscutAreaSize"),
                                shrubsTouchingHouseOnArrival = item.Field<String>("shrubsTouchingHouseOnArrival"),
                                treesTouchingHouseOnArrival = item.Field<String>("treesTouchingHouseOnArrival"),
                                performedTrimming = item.Field<String>("performedTrimming"),
                                trimmedPerGuidelines = item.Field<String>("trimmedPerGuidelines"),
                                shrubsTreesTouchingUponDeparture = item.Field<String>("shrubsTreesTouchingUponDeparture"),
                                Refrigerator = item.Field<String>("Refrigerator"),
                                microwave = item.Field<String>("microwave"),
                                stove = item.Field<String>("stove"),
                                walloven = item.Field<String>("walloven"),
                                dishwasher = item.Field<String>("dishwasher"),
                                washer = item.Field<String>("washer"),
                                dryer = item.Field<String>("dryer"),
                                airconditioner = item.Field<String>("airconditioner"),
                                waterheater = item.Field<String>("waterheater"),
                                furnace = item.Field<String>("furnace"),
                                watersoftener = item.Field<String>("watersoftener"),
                                dehumidifier = item.Field<String>("dehumidifier"),
                                conveyanceAllWorkCompleted = item.Field<String>("conveyanceAllWorkCompleted"),
                                conveyanceSecuredPerGuidelines = item.Field<String>("conveyanceSecuredPerGuidelines"),
                                conveyanceDamagesPresent = item.Field<String>("conveyanceDamagesPresent"),
                                conveyanceNewBidRequired = item.Field<String>("conveyanceNewBidRequired"),
                                conveyanceGrassCutNeeded = item.Field<String>("conveyanceGrassCutNeeded"),
                                conveyanceOvergrownShrubs = item.Field<String>("conveyanceOvergrownShrubs"),
                                conveyanceBroomSwept = item.Field<String>("conveyanceBroomSwept"),
                                conveyanceWinterizedPerGuidelines = item.Field<String>("conveyanceWinterizedPerGuidelines"),
                                conveyancePoolSecured = item.Field<String>("conveyancePoolSecured"),
                                conveyancePondSecured = item.Field<String>("conveyancePondSecured"),
                                conveyanceSumpPumpOperational = item.Field<String>("conveyanceSumpPumpOperational"),
                                conveyanceAnimalFree = item.Field<String>("conveyanceAnimalFree"),
                                conveyanceFenceSecured = item.Field<String>("conveyanceFenceSecured"),
                                conveyanceRoofLeak = item.Field<String>("conveyanceRoofLeak"),
                                conveyanceFoundationIntact = item.Field<String>("conveyanceFoundationIntact"),
                                conveyanceMoldFree = item.Field<String>("conveyanceMoldFree"),
                                conveyanceYardMaintained = item.Field<String>("conveyanceYardMaintained"),
                                conveyanceCondition = item.Field<String>("conveyanceCondition"),
                                notInConveyanceReasonDamages = item.Field<String>("notInConveyanceReasonDamages"),
                                notInConveyanceReasonDebris = item.Field<String>("notInConveyanceReasonDebris"),
                                notInConveyanceReasonRepairs = item.Field<String>("notInConveyanceReasonRepairs"),
                                notInConveyanceReasonHazards = item.Field<String>("notInConveyanceReasonHazards"),
                                notInConveyanceReasonOther = item.Field<String>("notInConveyanceReasonOther"),
                                notInConveyanceReasonOtherComment = item.Field<String>("notInConveyanceReasonOtherComment"),
                                generalComment = item.Field<String>("generalComment"),
                                femaDamages = item.Field<String>("femaDamages"),
                                femaPropertyDamageLevel = item.Field<String>("femaPropertyDamageLevel"),
                                femaNeighborhoodDamageLevel = item.Field<String>("femaNeighborhoodDamageLevel"),
                                femaTrailerPresent = item.Field<String>("femaTrailerPresent"),
                                femaDamageEstimate = item.Field<String>("femaDamageEstimate"),
                                femaDamageWind = item.Field<String>("femaDamageWind"),
                                femaDamageWater = item.Field<String>("femaDamageWater"),
                                femaDamageFire = item.Field<String>("femaDamageFire"),
                                femaDamageFlood = item.Field<String>("femaDamageFlood"),
                                propertyHabitable = item.Field<String>("propertyHabitable"),
                                numberOfBedrooms = Convert.ToInt32(item.Field<String>("numberOfBedrooms")),
                                numberOfBathrooms = Convert.ToInt32(item.Field<String>("numberOfBathrooms")),
                                mobileHomeAxles = item.Field<String>("mobileHomeAxles"),
                                conveyanceFloorsSoft = item.Field<String>("conveyanceFloorsSoft"),
                            }).ToList();

                        foreach (var ipl_Fb_Preservation in ipl_Fb_PreservationUploadList)                        
                        {                            
                            var json = JsonConvert.SerializeObject(ipl_Fb_Preservation);
                            var data = new StringContent(json, Encoding.UTF8, "application/json");

                            HttpResponseMessage messge = client.PostAsync(url, data).Result;
                            
                            if (messge.IsSuccessStatusCode)
                            {
                                result = "Preservation upload completed successfully";
                                success = success + 1;
                                PCR_FiveBrotherDTO pCR_FiveBrotherDTO = new PCR_FiveBrotherDTO();
                                pCR_FiveBrotherDTO.Type = 5;
                                pCR_FiveBrotherDTO.UserID = model.UserID;
                                pCR_FiveBrotherDTO.PCR_FiveBro_WO_ID = model.WO_Id.GetValueOrDefault(0);
                                var AddFiveBrother = pCR_FiveBrotherData.AddPCR_FiveBrother_Data(pCR_FiveBrotherDTO);
                            }
                            else
                            {
                                result = messge.Content.ReadAsStringAsync().Result;
                                fail = fail + 1;
                            }
                            
                        }
                        objDataModel.Add(result);
                        objDataModel.Add(success);
                        objDataModel.Add(fail);
                    }
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objDataModel;
        }
        private DataSet GetPreservationSyncData(Client_Sync_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Preservation_ClientSync]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@WorkOrderId", 1 + "#bigint#" + model.WO_Id);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ds;
        }
        public List<dynamic> UploadPhotoSync(Client_Sync_DTO model)
        {
            //1 before
            //2 during
            //3 after
            //4 bid
            //5 damage
            //6 custom
            //7 inspection

            GoogleCloudData googleCloudData = new GoogleCloudData();            
            List<dynamic> objDataModel = new List<dynamic>();
            int success = 0;
            int fail = 0;
            try
            {
                string SaveImgPath = System.Configuration.ConfigurationManager.AppSettings["ClientResultPhotosSave"];
               

                model.WI_ImportFrom = 4; // For FiveBrothers 

                List<Client_Sync_Credential> client_Sync_CredentialList = GetClientSyncCredentialList(model);

                if (client_Sync_CredentialList != null && client_Sync_CredentialList.Count > 0)
                {
                    client_Sync_CredentialList[0].WI_Password = securityHelper.GetMD5Hash(client_Sync_CredentialList[0].WI_Password);

                    string url = client_Sync_CredentialList[0].ApiUrl.Trim() + "file" + "?contractor=" + client_Sync_CredentialList[0].WI_LoginName + "&key=" + client_Sync_CredentialList[0].WI_Password;

                    byte[] credentials = UTF8Encoding.UTF8.GetBytes(client_Sync_CredentialList[0].WI_FB_LoginName + ":" + client_Sync_CredentialList[0].WI_FB_Password);
                    System.Net.Http.HttpClientHandler handler = new System.Net.Http.HttpClientHandler();// Added this condition to disbale SSL Validation 
                    handler.ServerCertificateCustomValidationCallback += (sender, cert, chain, sslPolicyErrors) => true; // Added this condition to disbale SSL Validation 
                    System.Net.Http.HttpClient client = new System.Net.Http.HttpClient(handler);

                    client.BaseAddress = new Uri(url);

                    client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(credentials));

                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                    DataSet ds = GetPhotoSyncData(model);

                    if (ds.Tables.Count > 0)
                    {
                        var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();

                        List<Ipl_Fb_FileDTO> ipl_Fb_PhotoUploadList =
                           (from item in myEnumerableFeaprd
                            select new Ipl_Fb_FileDTO
                            {
                                ordernumber = Convert.ToInt32(item.Field<String>("Ordernumber")),
                                contractor = item.Field<String>("contractor"),
                                formType = item.Field<String>("formType"),
                                categoryCode = item.Field<String>("categoryCode"),
                                itemCode = item.Field<String>("itemCode"),
                                description = item.Field<String>("description"),
                                beforeAfterFlag = item.Field<String>("beforeAfterFlag"),
                                fileDate = item.Field<DateTime?>("fileDate"),
                                latitude = Convert.ToDecimal(item.Field<String>("latitude")),
                                longitude = Convert.ToDecimal(item.Field<String>("longitude")),
                                fileData = item.Field<String>("fileData"),
                                fileType = item.Field<String>("fileType"),
                                locationCode = item.Field<String>("locationCode"),
                                attachmentID = item.Field<Int32>("attachmentID"),
                                Client_Result_Photo_ID = item.Field<Int64>("Client_Result_Photo_ID"),
                                Client_Result_Photo_FolderName = item.Field<String>("Client_Result_Photo_FolderName"),
                            }).ToList();

                        if (ipl_Fb_PhotoUploadList != null && ipl_Fb_PhotoUploadList.Count > 0)
                        {
                            foreach (var ipl_Fb_PhotoUpload in ipl_Fb_PhotoUploadList)
                            {
                                Ipl_Fb_FileJSONDTO ipl_Fb_FileJSONDTO = new Ipl_Fb_FileJSONDTO();
                                ipl_Fb_FileJSONDTO.ordernumber = ipl_Fb_PhotoUpload.ordernumber;
                                ipl_Fb_FileJSONDTO.contractor = client_Sync_CredentialList[0].WI_LoginName;
                                ipl_Fb_FileJSONDTO.formType = ipl_Fb_PhotoUpload.formType;
                                ipl_Fb_FileJSONDTO.categoryCode = ipl_Fb_PhotoUpload.categoryCode == null ? "" : ipl_Fb_PhotoUpload.categoryCode;
                                ipl_Fb_FileJSONDTO.itemCode = ipl_Fb_PhotoUpload.itemCode == null ? "" : ipl_Fb_PhotoUpload.itemCode;
                                ipl_Fb_FileJSONDTO.description = ipl_Fb_PhotoUpload.description == null ? "" : ipl_Fb_PhotoUpload.description;
                                ipl_Fb_FileJSONDTO.beforeAfterFlag = ipl_Fb_PhotoUpload.beforeAfterFlag;
                                ipl_Fb_FileJSONDTO.fileDate = ipl_Fb_PhotoUpload.fileDate;
                                ipl_Fb_FileJSONDTO.latitude = ipl_Fb_PhotoUpload.latitude;
                                ipl_Fb_FileJSONDTO.longitude = ipl_Fb_PhotoUpload.longitude;
                                var localPhoto = googleCloudData.SaveCloudPhotoInLocal(ipl_Fb_PhotoUpload.Client_Result_Photo_FolderName, ipl_Fb_PhotoUpload.fileType);
                                ipl_Fb_FileJSONDTO.fileData = imageGenerator.ImagefiletoBase64(SaveImgPath + ipl_Fb_PhotoUpload.Client_Result_Photo_FolderName + "\\" + ipl_Fb_PhotoUpload.fileType);
                                ipl_Fb_FileJSONDTO.fileType = Path.GetExtension(ipl_Fb_PhotoUpload.fileType);
                                ipl_Fb_FileJSONDTO.locationCode = ipl_Fb_PhotoUpload.locationCode;
                                ipl_Fb_FileJSONDTO.attachmentID = ipl_Fb_PhotoUpload.attachmentID;

                                var json = JsonConvert.SerializeObject(ipl_Fb_FileJSONDTO);
                                var data = new StringContent(json, Encoding.UTF8, "application/json");

                                HttpResponseMessage messge = client.PostAsync(url, data).Result;
                                
                                if (messge.IsSuccessStatusCode)
                                {
                                    ipl_Fb_PhotoUpload.Status = "Success";
                                    success = success + 1;
                                    Client_Result_PhotoDTO client_Result_PhotoDTO = new Client_Result_PhotoDTO();
                                    client_Result_PhotoDTO.Client_Result_Photo_ID = ipl_Fb_PhotoUpload.Client_Result_Photo_ID;
                                    client_Result_PhotoDTO.UserID = model.UserID;
                                    client_Result_PhotoDTO.Client_Result_Photo_Wo_ID = model.WO_Id;
                                    client_Result_PhotoDTO.Type = 8;
                                    var updatephoto = client_Result_PhotoData.AddClientResultPhotoData(client_Result_PhotoDTO);
                                }
                                else
                                {
                                    string result = messge.Content.ReadAsStringAsync().Result;
                                    ipl_Fb_PhotoUpload.Status = "Failure";
                                    var errormsg = JsonConvert.DeserializeObject<Client_Sync_DTO>(result);
                                    ipl_Fb_PhotoUpload.Message = errormsg.message;
                                    fail = fail + 1;
                                }
                            }
                            if (Directory.Exists(SaveImgPath + ipl_Fb_PhotoUploadList[0].Client_Result_Photo_FolderName))
                            {
                                Directory.Delete(SaveImgPath + ipl_Fb_PhotoUploadList[0].Client_Result_Photo_FolderName,true);
                            }
                            objDataModel.Add(ipl_Fb_PhotoUploadList);
                            objDataModel.Add(success);
                            objDataModel.Add(fail);
                        }

                       
                    }
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objDataModel;
        }
        private DataSet GetPhotoSyncData(Client_Sync_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Photo_ClientSync]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@WorkOrderId", 1 + "#bigint#" + model.WO_Id);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ds;
        }
        public List<dynamic> UploadGrassSync(Client_Sync_DTO model)
        {
            List<dynamic> objDataModel = new List<dynamic>();
            int success = 0;
            int fail = 0;
            try
            {
                model.WI_ImportFrom = 4; // For FiveBrothers 

                List<Client_Sync_Credential> client_Sync_CredentialList = GetClientSyncCredentialList(model);

                if (client_Sync_CredentialList != null && client_Sync_CredentialList.Count > 0)
                {
                    client_Sync_CredentialList[0].WI_Password = securityHelper.GetMD5Hash(client_Sync_CredentialList[0].WI_Password);

                    string url = client_Sync_CredentialList[0].ApiUrl.Trim() + "grass" + "?contractor=" + client_Sync_CredentialList[0].WI_LoginName + "&key=" + client_Sync_CredentialList[0].WI_Password;

                    byte[] credentials = UTF8Encoding.UTF8.GetBytes(client_Sync_CredentialList[0].WI_FB_LoginName + ":" + client_Sync_CredentialList[0].WI_FB_Password);
                    System.Net.Http.HttpClientHandler handler = new System.Net.Http.HttpClientHandler();// Added this condition to disbale SSL Validation 
                    handler.ServerCertificateCustomValidationCallback += (sender, cert, chain, sslPolicyErrors) => true; // Added this condition to disbale SSL Validation 
                    System.Net.Http.HttpClient client = new System.Net.Http.HttpClient(handler);

                    client.BaseAddress = new Uri(url);

                    client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(credentials));

                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                    DataSet ds = GetGrassSyncData(model);
                    string result = "";
                    if (ds.Tables.Count > 0)
                    {
                        var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();

                        List<Ipl_Fb_GrassDTO> ipl_Fb_GrassUploadList =
                           (from item in myEnumerableFeaprd
                            select new Ipl_Fb_GrassDTO
                            {
                               ordernumber = item.Field<Int64?>("ordernumber"),
                                uploadedDate = item.Field<DateTime?>("uploadDate"),
                                dateCompleted = item.Field<DateTime?>("dateCompleted") == null ? item.Field<DateTime?>("uploadDate") : item.Field<DateTime?>("dateCompleted"),
                                contractor = item.Field<String>("contractor"),
                                forRent = item.Field<String>("forRent"),
                                forSale = item.Field<String>("forSale"),
                                realtorPhone = item.Field<String>("realtorPhone"),
                                realtorName = item.Field<String>("realtorName"),
                                occupancy = item.Field<String>("occupancy"),
                                propertySecure = item.Field<String>("propertySecure"),
                                brokenOpenings = Convert.ToInt32(item.Field<String>("brokenOpenings")),
                                boardedOpenings = Convert.ToInt32(item.Field<String>("boardedOpenings")),
                                poolExists = item.Field<String>("poolExists"),
                                poolSecured = item.Field<String>("poolSecured"),
                                exteriorDamageFire = item.Field<String>("exteriorDamageFire"),
                                exteriorDamageNeglect = item.Field<String>("exteriorDamageNeglect"),
                                exteriorDamageVandal = item.Field<String>("exteriorDamageVandal"),
                                exteriorDamageFreeze = item.Field<String>("exteriorDamageFreeze"),
                                exteriorDamageStorm = item.Field<String>("exteriorDamageStorm"),
                                exteriorDamageFlood = item.Field<String>("exteriorDamageFlood"),
                                exteriorDamageRoofleak = item.Field<String>("exteriorDamageRoofleak"),
                                violationPosted = item.Field<String>("violationPosted"),
                                notes = item.Field<String>("notes"),
                                grasscutCompleted = item.Field<String>("grasscutCompleted"),
                                exteriorDebris = item.Field<String>("exteriorDebris"),
                                version = item.Field<String>("version"),
                                shrubsTouching = item.Field<String>("shrubsTouching"),
                                treesTouching = item.Field<String>("treesTouching"),
                                formType = item.Field<String>("formType"),
                                subcontnum = item.Field<String>("subcontnum"),
                                numberOfPhotos = item.Field<int>("numberOfPhotos"),
                                damageDescription = item.Field<String>("damageDescription"),
                                newDamages = item.Field<String>("newDamage"),
                                unableToCut = item.Field<String>("unableToCut"),
                                unableToCutOther = item.Field<String>("unableToCutOther"),
                                lotsize = item.Field<String>("lotsize"),
                            }).ToList();

                        foreach (var ipl_Fb_Grass in ipl_Fb_GrassUploadList)
                        {
                            var json = JsonConvert.SerializeObject(ipl_Fb_Grass);
                            var data = new StringContent(json, Encoding.UTF8, "application/json");

                            HttpResponseMessage messge = client.PostAsync(url, data).Result;

                            if (messge.IsSuccessStatusCode)
                            {
                                result = "Grass upload completed successfully";
                                success = success + 1;
                                PCR_Grass_Cut_MasterDTO pCR_Grass_Cut_MasterDTO = new PCR_Grass_Cut_MasterDTO();
                                pCR_Grass_Cut_MasterDTO.Type = 5;
                                pCR_Grass_Cut_MasterDTO.UserID = model.UserID;
                                pCR_Grass_Cut_MasterDTO.Grass_Cut_WO_ID = model.WO_Id.GetValueOrDefault(0);
                                var updateGrassSync = pCR_Grass_Cut_MasterData.AddPCR_Grass_Cut_Masterdata(pCR_Grass_Cut_MasterDTO);
                            }
                            else
                            {
                                result = messge.Content.ReadAsStringAsync().Result;
                                fail = fail + 1;
                            }

                        }
                        objDataModel.Add(result);
                        objDataModel.Add(success);
                        objDataModel.Add(fail);
                    }
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objDataModel;
        }
        private DataSet GetGrassSyncData(Client_Sync_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Grass_ClientSync]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@WorkOrderId", 1 + "#bigint#" + model.WO_Id);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ds;
        }

        public List<dynamic> UploadDamageSync(Client_Sync_DTO model)
        {
            List<dynamic> objDataModel = new List<dynamic>();
            int success = 0;
            int fail = 0;
            try
            {
                model.WI_ImportFrom = 4; // For FiveBrothers 

                List<Client_Sync_Credential> client_Sync_CredentialList = GetClientSyncCredentialList(model);

                if (client_Sync_CredentialList != null && client_Sync_CredentialList.Count > 0)
                {
                    client_Sync_CredentialList[0].WI_Password = securityHelper.GetMD5Hash(client_Sync_CredentialList[0].WI_Password);

                    string url = client_Sync_CredentialList[0].ApiUrl.Trim() + "damage" + "?contractor=" + client_Sync_CredentialList[0].WI_LoginName + "&key=" + client_Sync_CredentialList[0].WI_Password;

                    byte[] credentials = UTF8Encoding.UTF8.GetBytes(client_Sync_CredentialList[0].WI_FB_LoginName + ":" + client_Sync_CredentialList[0].WI_FB_Password);
                    System.Net.Http.HttpClientHandler handler = new System.Net.Http.HttpClientHandler();// Added this condition to disbale SSL Validation 
                    handler.ServerCertificateCustomValidationCallback += (sender, cert, chain, sslPolicyErrors) => true; // Added this condition to disbale SSL Validation 
                    System.Net.Http.HttpClient client = new System.Net.Http.HttpClient(handler);

                    client.BaseAddress = new Uri(url);

                    client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(credentials));

                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                    DataSet ds = GetDamageSyncData(model);
                    if (ds.Tables.Count > 0)
                    {
                        var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();

                        List<Ipl_Fb_DamageDTO> ipl_Fb_DamageList =
                           (from item in myEnumerableFeaprd
                            select new Ipl_Fb_DamageDTO
                            {
                                ordernumber = item.Field<Int64?>("ordernumber"),
                                attachmentId = item.Field<int>("attachmentId"),
                                damageditem = item.Field<String>("damageditem"),
                                cause = item.Field<String>("cause"),
                                description = item.Field<String>("description"),
                                status = item.Field<String>("status"),
                                location = item.Field<String>("location"),
                                building = item.Field<String>("building"),
                                room = item.Field<String>("room"),
                                quantity = item.Field<String>("quantity"),
                                estimate = item.Field<String>("estimate")
                            }).ToList();

                        for (int i = 0; i < ipl_Fb_DamageList.Count; i++)
                        {
                            Ipl_Fb_DamageJsonDTO ipl_Fb_DamageJsonDTO = new Ipl_Fb_DamageJsonDTO();
                            ipl_Fb_DamageJsonDTO.ordernumber = ipl_Fb_DamageList[i].ordernumber;
                            ipl_Fb_DamageJsonDTO.attachmentId = ipl_Fb_DamageList[i].attachmentId;
                            ipl_Fb_DamageJsonDTO.damageditem = ipl_Fb_DamageList[i].damageditem;
                            ipl_Fb_DamageJsonDTO.cause = ipl_Fb_DamageList[i].cause;
                            ipl_Fb_DamageJsonDTO.description = ipl_Fb_DamageList[i].description;
                            ipl_Fb_DamageJsonDTO.status = ipl_Fb_DamageList[i].status;
                            ipl_Fb_DamageJsonDTO.location = ipl_Fb_DamageList[i].location;
                            ipl_Fb_DamageJsonDTO.building = ipl_Fb_DamageList[i].building;
                            ipl_Fb_DamageJsonDTO.room = ipl_Fb_DamageList[i].room;
                            ipl_Fb_DamageJsonDTO.quantity = ipl_Fb_DamageList[i].quantity;
                            ipl_Fb_DamageJsonDTO.estimate = ipl_Fb_DamageList[i].estimate;

                            var json = JsonConvert.SerializeObject(ipl_Fb_DamageJsonDTO);
                            var data = new StringContent(json, Encoding.UTF8, "application/json");

                            HttpResponseMessage messge = client.PostAsync(url, data).Result;
                            string result = messge.Content.ReadAsStringAsync().Result;
                            if (messge.IsSuccessStatusCode)
                            {
                                ipl_Fb_DamageList[i].Status = "Success";
                                success = success + 1;
                                //Invoice_Client_ChildDTO invoice_Client_ChildDTO = new Invoice_Client_ChildDTO();
                                //invoice_Client_ChildDTO.Inv_Client_Ch_pkeyId = ipl_Fb_DamageList[i].Damage_PkeyId;
                                //invoice_Client_ChildDTO.UserID = model.UserID;
                                //invoice_Client_ChildDTO.Inv_Client_Ch_Wo_Id = model.WO_Id;
                                //invoice_Client_ChildDTO.Type = 5;
                                //var updateInvTask = invoice_Client_ChildData.AddInvoiceClientChildData(invoice_Client_ChildDTO);
                            }
                            else
                            {
                                ipl_Fb_DamageList[i].Status = "Failure";
                                var errormsg = JsonConvert.DeserializeObject<Client_Sync_DTO>(result);
                                ipl_Fb_DamageList[i].Message = errormsg.message;
                                fail = fail + 1;
                            }
                        }
                        objDataModel.Add(ipl_Fb_DamageList);
                        objDataModel.Add(success);
                        objDataModel.Add(fail);
                    }
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objDataModel;
        }

        private DataSet GetDamageSyncData(Client_Sync_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Grass_ClientSync]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@WorkOrderId", 1 + "#bigint#" + model.WO_Id);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ds;
        }

        public String ConvertImageURLToBase64(string url)
        {

            StringBuilder _sb = new StringBuilder();
            Byte[] _byte = this.GetImg(url);
            return Convert.ToBase64String(_byte, 0, _byte.Length);
            
        }
        private byte[] GetImg(string url)
        {
            Stream stream = null;
            byte[] buf;
            try
            {
                WebProxy myProxy = new WebProxy();
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(url);
                HttpWebResponse response = (HttpWebResponse)req.GetResponse();
                stream = response.GetResponseStream();

                using (BinaryReader br = new BinaryReader(stream))
                {
                    int len = (int)(response.ContentLength);
                    buf = br.ReadBytes(len);
                    br.Close();
                }
                stream.Close();
                response.Close();
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                buf = null;
            }
            return (buf);
        }
    }
}