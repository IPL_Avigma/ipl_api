﻿using Avigma.Repository.Lib;
using IPL.Models;
using IPL.Models.FiveBrothers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;

namespace IPL.Repository.data
{
    public class WorkOrderItem_FiveBrother_Data_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        SecurityHelper securityHelper = new SecurityHelper();
        Log log = new Log();
        FiveBrothers_Api_Data fiveBrothers_Api_Data = new FiveBrothers_Api_Data();

        public List<dynamic> AddCreateUpdate_WorkOrderItem_FiveBrotherData(WorkOrderItem_FiveBrother_Data_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_WorkOrderItem_FiveBrother_Data_Master]";
            WorkOrderItem_FiveBrother_Data workOrderItem_FiveBrother_Data = new WorkOrderItem_FiveBrother_Data();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@WorkOrder_Item_Fb_PkeyId", 1 + "#bigint#" + model.WorkOrder_Item_Fb_PkeyId);
                input_parameters.Add("@WorkOrder_Item_Fb_OrderNumber", 1 + "#bigint#" + model.WorkOrder_Item_Fb_OrderNumber);
                input_parameters.Add("@WorkOrder_Item_Fb_Description", 1 + "#varchar#" + model.WorkOrder_Item_Fb_Description);
                input_parameters.Add("@WorkOrder_Item_Fb_Amount", 1 + "#decimal#" + model.WorkOrder_Item_Fb_Amount);
                input_parameters.Add("@WorkOrder_Item_Fb_Quantity", 1 + "#bigint#" + model.WorkOrder_Item_Fb_Quantity);
                input_parameters.Add("@WorkOrder_Item_Fb_Import_From_Id", 1 + "#bigint#" + model.WorkOrder_Item_Fb_Import_From_Id);
                input_parameters.Add("@WorkOrder_Item_Fb_Client_Id", 1 + "#bigint#" + model.WorkOrder_Item_Fb_Client_Id);
                input_parameters.Add("@WorkOrder_Item_Fb_Company_Id", 1 + "#bigint#" + model.WorkOrder_Item_Fb_Company_Id);
                input_parameters.Add("@WorkOrder_Item_Fb_User_Id", 1 + "#bigint#" + model.WorkOrder_Item_Fb_User_Id);
                input_parameters.Add("@WorkOrder_Item_Fb_IsActive", 1 + "#bit#" + model.WorkOrder_Item_Fb_IsActive);
                input_parameters.Add("@WorkOrder_Item_Fb_IsDelete", 1 + "#bit#" + model.WorkOrder_Item_Fb_IsDelete);
                input_parameters.Add("@WorkOrder_Item_Fb_IsProcessed", 1 + "#int#" + model.WorkOrder_Item_Fb_IsProcessed);
                input_parameters.Add("@WorkOrder_Item_Fb_FiveBro_Id", 1 + "#bigint#" + model.WorkOrder_Item_Fb_FiveBro_Id);

                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@WorkOrder_Item_Fb_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    workOrderItem_FiveBrother_Data.WorkOrder_Item_Fb_PkeyId = "0";
                    workOrderItem_FiveBrother_Data.Status = "0";
                    workOrderItem_FiveBrother_Data.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    workOrderItem_FiveBrother_Data.WorkOrder_Item_Fb_PkeyId = Convert.ToString(objData[0]);
                    workOrderItem_FiveBrother_Data.Status = Convert.ToString(objData[1]);


                }
                objcltData.Add(workOrderItem_FiveBrother_Data);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;



        }

        private DataSet GetWorkOrderItem_FiveBrother_Data(WorkOrderItem_FiveBrother_Data_MasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_WorkOrderItem_FiveBrother_Data_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@WorkOrder_Item_Fb_PkeyId", 1 + "#bigint#" + model.WorkOrder_Item_Fb_PkeyId);
                input_parameters.Add("@WorkOrder_Item_Fb_Company_Id", 1 + "#bigint#" + model.WorkOrder_Item_Fb_Company_Id);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetWorkOrderItem_FiveBrother_DataDetails(WorkOrderItem_FiveBrother_Data_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetWorkOrderItem_FiveBrother_Data(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<WorkOrderItem_FiveBrother_Data_MasterDTO> WorkOrderItem_FiveBrother =
                   (from item in myEnumerableFeaprd
                    select new WorkOrderItem_FiveBrother_Data_MasterDTO
                    {
                        WorkOrder_Item_Fb_PkeyId = item.Field<Int64>("WorkOrder_Item_Fb_PkeyId"),
                        WorkOrder_Item_Fb_OrderNumber = item.Field<Int64?>("WorkOrder_Item_Fb_OrderNumber"),
                        WorkOrder_Item_Fb_Description = item.Field<String>("WorkOrder_Item_Fb_Description"),
                        WorkOrder_Item_Fb_Amount = item.Field<Decimal?>("WorkOrder_Item_Fb_Amount"),
                        WorkOrder_Item_Fb_Quantity = item.Field<Int64?>("WorkOrder_Item_Fb_Quantity"),
                        WorkOrder_Item_Fb_Import_From_Id = item.Field<Int64?>("WorkOrder_Item_Fb_Import_From_Id"),
                        WorkOrder_Item_Fb_Client_Id = item.Field<Int64?>("WorkOrder_Item_Fb_Client_Id"),
                        WorkOrder_Item_Fb_Company_Id = item.Field<Int64?>("WorkOrder_Item_Fb_Company_Id"),
                        WorkOrder_Item_Fb_User_Id = item.Field<Int64?>("WorkOrder_Item_Fb_User_Id"),
                        WorkOrder_Item_Fb_IsActive = item.Field<Boolean?>("WorkOrder_Item_Fb_IsActive"),
                        WorkOrder_Item_Fb_IsProcessed = item.Field<int?>("WorkOrder_Item_Fb_IsProcessed"),
                        WorkOrder_Item_Fb_FiveBro_Id = item.Field<Int64?>("WorkOrder_Item_Fb_FiveBro_Id"),


                    }).ToList();

                objDynamic.Add(WorkOrderItem_FiveBrother);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
        public List<dynamic> AddOrderItems(Int64 workOrderId, Int64? ordernumber, FBAPI_RequestDTO model)
        {
            List<dynamic> objDataModel = new List<dynamic>();
            model.ConPassword = securityHelper.GetMD5Hash(model.ConPassword);

            if (ordernumber != null)
            {
                string URL = model.ApiUrl.Trim() + "orderlineitems" + "?ordernumber=" + ordernumber + "&contractor=" + model.ConUserName + "&key=" + model.ConPassword;

                try
                {
                    byte[] credentials = UTF8Encoding.UTF8.GetBytes(model.AuthUserName + ":" + model.AuthPassword);
                    System.Net.Http.HttpClientHandler handler = new System.Net.Http.HttpClientHandler();// Added this condition to disbale SSL Validation 
                    handler.ServerCertificateCustomValidationCallback += (sender, cert, chain, sslPolicyErrors) => true; // Added this condition to disbale SSL Validation 
                    System.Net.Http.HttpClient client = new System.Net.Http.HttpClient(handler);

                    client.BaseAddress = new System.Uri(URL);

                    client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(credentials));

                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage messge = client.GetAsync(URL).Result;
                    if (messge.IsSuccessStatusCode)
                    {
                        string result = messge.Content.ReadAsStringAsync().Result;
                        List<OrderItemsJsonDTO> orderItemsJsonDTO = JsonConvert.DeserializeObject<List<OrderItemsJsonDTO>>(result);
                        if (orderItemsJsonDTO != null && orderItemsJsonDTO.Count > 0)
                        {
                            foreach (var orderItem in orderItemsJsonDTO)
                            {
                                WorkOrderItem_FiveBrother_Data_MasterDTO workOrderItem_FiveBrother_Data_MasterDTO = new WorkOrderItem_FiveBrother_Data_MasterDTO();
                                workOrderItem_FiveBrother_Data_MasterDTO.WorkOrder_Item_Fb_OrderNumber = orderItem.ordernumber;
                                workOrderItem_FiveBrother_Data_MasterDTO.WorkOrder_Item_Fb_Description = orderItem.description;
                                workOrderItem_FiveBrother_Data_MasterDTO.WorkOrder_Item_Fb_Amount = orderItem.amount;
                                workOrderItem_FiveBrother_Data_MasterDTO.WorkOrder_Item_Fb_Quantity = orderItem.quantity;
                                workOrderItem_FiveBrother_Data_MasterDTO.WorkOrder_Item_Fb_FiveBro_Id = workOrderId;
                                workOrderItem_FiveBrother_Data_MasterDTO.UserID = model.UserID;
                                workOrderItem_FiveBrother_Data_MasterDTO.WorkOrder_Item_Fb_IsActive = true;
                                workOrderItem_FiveBrother_Data_MasterDTO.WorkOrder_Item_Fb_IsDelete = false;
                                workOrderItem_FiveBrother_Data_MasterDTO.WorkOrder_Item_Fb_IsProcessed = 0;
                                workOrderItem_FiveBrother_Data_MasterDTO.Type = 1;
                                var objData = AddCreateUpdate_WorkOrderItem_FiveBrotherData(workOrderItem_FiveBrother_Data_MasterDTO);
                                objDataModel.Add(objData);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    log.logErrorMessage(ex.StackTrace);
                    log.logErrorMessage(ex.Message);
                }
            }            
            return objDataModel;
        }
    }
}