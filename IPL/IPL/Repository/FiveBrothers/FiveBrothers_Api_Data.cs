﻿using Avigma.Repository.Lib;
using IPL.Models.FiveBrothers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace IPL.Repository.data
{
    public class FiveBrothers_Api_Data
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        SecurityHelper securityHelper = new SecurityHelper();
        public List<dynamic> AddBidCodes(Client_Sync_DTO model)
        {
            List<dynamic> objDataModel = new List<dynamic>();

            //string URL = "https://test.fivecontractor.com/vendorsys/api/bidcodes" + "?contractor=" + model.ConUserName + "&key=" + model.ConPassword;

            try
            {
                model.WI_ImportFrom = 4; // For FiveBrothers 
                model.Type = 1;
                List<Client_Sync_Credential> client_Sync_CredentialList = GetClientSyncCredentialList(model);

                if (client_Sync_CredentialList != null && client_Sync_CredentialList.Count > 0)
                {
                    client_Sync_CredentialList[0].WI_Password = securityHelper.GetMD5Hash(client_Sync_CredentialList[0].WI_Password);

                    string url = client_Sync_CredentialList[0].ApiUrl.Trim() + "bidcodes" + "?contractor=" + client_Sync_CredentialList[0].WI_LoginName + "&key=" + client_Sync_CredentialList[0].WI_Password;

                    byte[] credentials = UTF8Encoding.UTF8.GetBytes(client_Sync_CredentialList[0].WI_FB_LoginName + ":" + client_Sync_CredentialList[0].WI_FB_Password);
                    System.Net.Http.HttpClientHandler handler = new System.Net.Http.HttpClientHandler();// Added this condition to disbale SSL Validation 
                    handler.ServerCertificateCustomValidationCallback += (sender, cert, chain, sslPolicyErrors) => true; // Added this condition to disbale SSL Validation 
                    System.Net.Http.HttpClient client = new System.Net.Http.HttpClient(handler);

                    client.BaseAddress = new Uri(url);

                    client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(credentials));

                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage messge = client.GetAsync(url).Result;
                    if (messge.IsSuccessStatusCode)
                    {
                        string result = messge.Content.ReadAsStringAsync().Result;
                        BidcodeJsonDTO bidcodeJsonDTO = JsonConvert.DeserializeObject<BidcodeJsonDTO>(result);
                        if (bidcodeJsonDTO.bidcategories != null && bidcodeJsonDTO.bidcategories.Count > 0)
                        {
                            foreach (var bidcategory in bidcodeJsonDTO.bidcategories)
                            {
                                Import_Client_BidCategory_MasteDTO import_Client_BidCategory_MasteDTO = new Import_Client_BidCategory_MasteDTO();
                                import_Client_BidCategory_MasteDTO.Import_Client_BidCategory_Name = bidcategory;
                                import_Client_BidCategory_MasteDTO.UserID = model.UserID;
                                import_Client_BidCategory_MasteDTO.Import_Client_BidCategory_IsActive = true;
                                import_Client_BidCategory_MasteDTO.Import_Client_BidCategory_IsDelete = false;
                                import_Client_BidCategory_MasteDTO.Type = 1;
                                var objData = Add_Import_Client_BidCategory_Master(import_Client_BidCategory_MasteDTO);
                                objDataModel.Add(objData);

                            }
                        }
                        if (bidcodeJsonDTO.damagecauses != null && bidcodeJsonDTO.damagecauses.Count > 0)
                        {
                            foreach (var damagecause in bidcodeJsonDTO.damagecauses)
                            {
                                Import_Client_TaskBid_DamageCaseDTO import_Client_TaskBid_DamageCaseDTO = new Import_Client_TaskBid_DamageCaseDTO();
                                import_Client_TaskBid_DamageCaseDTO.Import_Client_DamageCauses_Name = damagecause;
                                import_Client_TaskBid_DamageCaseDTO.UserID = model.UserID;
                                import_Client_TaskBid_DamageCaseDTO.Import_Client_DamageCauses_IsActive = true;
                                import_Client_TaskBid_DamageCaseDTO.Import_Client_DamageCauses_IsDelete = false;
                                import_Client_TaskBid_DamageCaseDTO.Type = 1;
                                var objData = AddTaskBid_DamageCaseData(import_Client_TaskBid_DamageCaseDTO);
                                objDataModel.Add(objData);
                            }
                        }
                    }

                }
                          
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objDataModel;
        }

        public List<dynamic> AddDamageCodes(Client_Sync_DTO model)
        {
            List<dynamic> objDataModel = new List<dynamic>();

            //string URL = "https://test.fivecontractor.com/vendorsys/api/damagecodes" + "?contractor=" + model.ConUserName + "&key=" + model.ConPassword;

            try
            {
                model.WI_ImportFrom = 4; // For FiveBrothers 
                model.Type = 1;
                List<Client_Sync_Credential> client_Sync_CredentialList = GetClientSyncCredentialList(model);

                if (client_Sync_CredentialList != null && client_Sync_CredentialList.Count > 0)
                {
                    client_Sync_CredentialList[0].WI_Password = securityHelper.GetMD5Hash(client_Sync_CredentialList[0].WI_Password);

                    string url = client_Sync_CredentialList[0].ApiUrl.Trim() + "damagecodes" + "?contractor=" + client_Sync_CredentialList[0].WI_LoginName + "&key=" + client_Sync_CredentialList[0].WI_Password;

                    byte[] credentials = UTF8Encoding.UTF8.GetBytes(client_Sync_CredentialList[0].WI_FB_LoginName + ":" + client_Sync_CredentialList[0].WI_FB_Password);
                    System.Net.Http.HttpClientHandler handler = new System.Net.Http.HttpClientHandler();// Added this condition to disbale SSL Validation 
                    handler.ServerCertificateCustomValidationCallback += (sender, cert, chain, sslPolicyErrors) => true; // Added this condition to disbale SSL Validation 
                    System.Net.Http.HttpClient client = new System.Net.Http.HttpClient(handler);

                    client.BaseAddress = new Uri(url);

                    client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(credentials));

                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage messge = client.GetAsync(url).Result;
                    if (messge.IsSuccessStatusCode)
                    {
                        string result = messge.Content.ReadAsStringAsync().Result;
                        DamageCodesJsonDTO damageCodesJsonDTO = JsonConvert.DeserializeObject<DamageCodesJsonDTO>(result);
                        if (damageCodesJsonDTO.damageditems != null && damageCodesJsonDTO.damageditems.Count > 0)
                        {
                            foreach (var damageditem in damageCodesJsonDTO.damageditems)
                            {
                                Import_Client_DamageItems_MasterDTO addImport_Client_DamageItemsData = new Import_Client_DamageItems_MasterDTO();
                                addImport_Client_DamageItemsData.Import_Client_DamageItem_Name = damageditem;
                                addImport_Client_DamageItemsData.UserID = model.UserID;
                                addImport_Client_DamageItemsData.Import_Client_DamageItem_IsActive = true;
                                addImport_Client_DamageItemsData.Import_Client_DamageItem_IsDelete = false;
                                addImport_Client_DamageItemsData.Type = 1;
                                var objData = AddImport_Client_DamageItemsData(addImport_Client_DamageItemsData);
                                objDataModel.Add(objData);

                            }
                        }
                        if (damageCodesJsonDTO.damagecauses != null && damageCodesJsonDTO.damagecauses.Count > 0)
                        {
                            foreach (var damagecause in damageCodesJsonDTO.damagecauses)
                            {
                                Import_Client_DamageCauses_MasterDTO import_Client_DamageCauses_MasterDTO = new Import_Client_DamageCauses_MasterDTO();
                                import_Client_DamageCauses_MasterDTO.Import_Client_DamageCauses_Name = damagecause;
                                import_Client_DamageCauses_MasterDTO.UserID = model.UserID;
                                import_Client_DamageCauses_MasterDTO.Import_Client_DamageCauses_IsActive = true;
                                import_Client_DamageCauses_MasterDTO.Import_Client_DamageCauses_IsDelete = false;
                                import_Client_DamageCauses_MasterDTO.Type = 1;
                                var objData = AddImport_Client_DamageCauses_MasterData(import_Client_DamageCauses_MasterDTO);
                                objDataModel.Add(objData);
                            }
                        }
                        if (damageCodesJsonDTO.buildings != null && damageCodesJsonDTO.buildings.Count > 0)
                        {
                            foreach (var building in damageCodesJsonDTO.buildings)
                            {
                                Import_Client_Buildings_MasterDTO import_Client_Buildings_MasterDTO = new Import_Client_Buildings_MasterDTO();
                                import_Client_Buildings_MasterDTO.Import_Client_Buildings_Name = building;
                                import_Client_Buildings_MasterDTO.UserID = model.UserID;
                                import_Client_Buildings_MasterDTO.Import_Client_Buildings_IsActive = true;
                                import_Client_Buildings_MasterDTO.Import_Client_Buildings_IsDelete = false;
                                import_Client_Buildings_MasterDTO.Type = 1;
                                var objData = AddImport_Client_Buildings_MasterData(import_Client_Buildings_MasterDTO);
                                objDataModel.Add(objData);
                            }
                        }
                        if (damageCodesJsonDTO.rooms != null && damageCodesJsonDTO.rooms.Count > 0)
                        {
                            foreach (var room in damageCodesJsonDTO.rooms)
                            {
                                Import_Client_Rooms_MasterDTO import_Client_Rooms_MasterDTO = new Import_Client_Rooms_MasterDTO();
                                import_Client_Rooms_MasterDTO.Import_Client_Rooms_Name = room;
                                import_Client_Rooms_MasterDTO.UserID = model.UserID;
                                import_Client_Rooms_MasterDTO.Import_Client_Rooms_IsActive = true;
                                import_Client_Rooms_MasterDTO.Import_Client_Rooms_IsDelete = false;
                                import_Client_Rooms_MasterDTO.Type = 1;
                                var objData = AddImport_Client_Rooms_MasterData(import_Client_Rooms_MasterDTO);
                                objDataModel.Add(objData);
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objDataModel;
        }

        public List<dynamic> AddInsurancecategories(Client_Sync_DTO model)
        {
            List<dynamic> objDataModel = new List<dynamic>();
            //string URL = "https://test.fivecontractor.com/vendorsys/api/insurancecategories" + "?contractor=" + model.ConUserName + "&key=" + model.ConPassword;

            try
            {
                model.WI_ImportFrom = 4; // For FiveBrothers 
                model.Type = 1;
                List<Client_Sync_Credential> client_Sync_CredentialList = GetClientSyncCredentialList(model);

                if (client_Sync_CredentialList != null && client_Sync_CredentialList.Count > 0)
                {
                    client_Sync_CredentialList[0].WI_Password = securityHelper.GetMD5Hash(client_Sync_CredentialList[0].WI_Password);

                    string url = client_Sync_CredentialList[0].ApiUrl.Trim() + "insurancecategories" + "?contractor=" + client_Sync_CredentialList[0].WI_LoginName + "&key=" + client_Sync_CredentialList[0].WI_Password;

                    byte[] credentials = UTF8Encoding.UTF8.GetBytes(client_Sync_CredentialList[0].WI_FB_LoginName + ":" + client_Sync_CredentialList[0].WI_FB_Password);
                    System.Net.Http.HttpClientHandler handler = new System.Net.Http.HttpClientHandler();// Added this condition to disbale SSL Validation 
                    handler.ServerCertificateCustomValidationCallback += (sender, cert, chain, sslPolicyErrors) => true; // Added this condition to disbale SSL Validation 
                    System.Net.Http.HttpClient client = new System.Net.Http.HttpClient(handler);

                    client.BaseAddress = new Uri(url);

                    client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(credentials));

                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage messge = client.GetAsync(url).Result;
                    if (messge.IsSuccessStatusCode)
                    {
                        string result = messge.Content.ReadAsStringAsync().Result;
                        List<InsurancecategoriesJsonDTO> insurancecategoriesJsonDTO = JsonConvert.DeserializeObject<List<InsurancecategoriesJsonDTO>>(result);
                        if (insurancecategoriesJsonDTO != null && insurancecategoriesJsonDTO.Count > 0)
                        {
                            foreach (var insurancecategory in insurancecategoriesJsonDTO)
                            {
                                Import_Client_InsuranceCategoriesDTO import_Client_InsuranceCategoriesDTO = new Import_Client_InsuranceCategoriesDTO();
                                import_Client_InsuranceCategoriesDTO.Import_Client_InsuranceCategories_Location = insurancecategory.location;
                                import_Client_InsuranceCategoriesDTO.Import_Client_InsuranceCategories_Category = insurancecategory.category;
                                import_Client_InsuranceCategoriesDTO.UserID = model.UserID;
                                import_Client_InsuranceCategoriesDTO.Import_Client_InsuranceCategories_IsActive = true;
                                import_Client_InsuranceCategoriesDTO.Import_Client_InsuranceCategories_IsDelete = false;
                                import_Client_InsuranceCategoriesDTO.Type = 1;
                                var objData = AddImport_Client_InsuranceCategoriesData(import_Client_InsuranceCategoriesDTO);
                                objDataModel.Add(objData);
                            }
                        }
                    }

                }

                
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objDataModel;
        }

        public List<dynamic> AddOrdertypecodes(Client_Sync_DTO model)
        {
            List<dynamic> objDataModel = new List<dynamic>();

            //string URL = "https://test.fivecontractor.com/vendorsys/api/ordertypecodes" + "?contractor=" + model.ConUserName + "&key=" + model.ConPassword;

            try
            {
                model.WI_ImportFrom = 4; // For FiveBrothers 
                model.Type = 1;
                List<Client_Sync_Credential> client_Sync_CredentialList = GetClientSyncCredentialList(model);

                if (client_Sync_CredentialList != null && client_Sync_CredentialList.Count > 0)
                {
                    client_Sync_CredentialList[0].WI_Password = securityHelper.GetMD5Hash(client_Sync_CredentialList[0].WI_Password);

                    string url = client_Sync_CredentialList[0].ApiUrl.Trim() + "ordertypecodes" + "?contractor=" + client_Sync_CredentialList[0].WI_LoginName + "&key=" + client_Sync_CredentialList[0].WI_Password;

                    byte[] credentials = UTF8Encoding.UTF8.GetBytes(client_Sync_CredentialList[0].WI_FB_LoginName + ":" + client_Sync_CredentialList[0].WI_FB_Password);
                    System.Net.Http.HttpClientHandler handler = new System.Net.Http.HttpClientHandler();// Added this condition to disbale SSL Validation 
                    handler.ServerCertificateCustomValidationCallback += (sender, cert, chain, sslPolicyErrors) => true; // Added this condition to disbale SSL Validation 
                    System.Net.Http.HttpClient client = new System.Net.Http.HttpClient(handler);

                    client.BaseAddress = new Uri(url);

                    client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(credentials));

                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage messge = client.GetAsync(url).Result;
                    if (messge.IsSuccessStatusCode)
                    {
                        string result = messge.Content.ReadAsStringAsync().Result;
                        List<OrdertypecodesJsonDTO> ordertypecodesJsonDTO = JsonConvert.DeserializeObject<List<OrdertypecodesJsonDTO>>(result);
                        if (ordertypecodesJsonDTO != null && ordertypecodesJsonDTO.Count > 0)
                        {
                            foreach (var ordertypecode in ordertypecodesJsonDTO)
                            {
                                Import_Client_OrderTypeCode_MasterDTO import_Client_OrderTypeCode_MasterDTO = new Import_Client_OrderTypeCode_MasterDTO();
                                import_Client_OrderTypeCode_MasterDTO.Import_Client_OrderTypeCodes_Code = ordertypecode.code;
                                import_Client_OrderTypeCode_MasterDTO.Import_Client_OrderTypeCodes_Description = ordertypecode.description;
                                import_Client_OrderTypeCode_MasterDTO.Import_Client_OrderTypeCodes_MainType = ordertypecode.maintype;
                                import_Client_OrderTypeCode_MasterDTO.UserID = model.UserID;
                                import_Client_OrderTypeCode_MasterDTO.Import_Client_OrderTypeCodes_IsActive = true;
                                import_Client_OrderTypeCode_MasterDTO.Import_Client_OrderTypeCodes_IsDelete = false;
                                import_Client_OrderTypeCode_MasterDTO.Type = 1;
                                var objData = AddImport_Client_OrderTypeCode_MasterData(import_Client_OrderTypeCode_MasterDTO);
                                objDataModel.Add(objData);
                            }                            
                        }
                    }
                }                
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objDataModel;
        }

        public List<dynamic> AddItemCodes(Client_Sync_DTO model)
        {
            List<dynamic> objDataModel = new List<dynamic>();

            //string URL = "https://test.fivecontractor.com/vendorsys/api/itemcodes" + "?contractor=" + model.ConUserName + "&key=" + model.ConPassword;

            try
            {
                model.WI_ImportFrom = 4; // For FiveBrothers 
                model.Type = 1;
                List<Client_Sync_Credential> client_Sync_CredentialList = GetClientSyncCredentialList(model);

                if (client_Sync_CredentialList != null && client_Sync_CredentialList.Count > 0)
                {
                    client_Sync_CredentialList[0].WI_Password = securityHelper.GetMD5Hash(client_Sync_CredentialList[0].WI_Password);

                    string url = client_Sync_CredentialList[0].ApiUrl.Trim() + "itemcodes" + "?contractor=" + client_Sync_CredentialList[0].WI_LoginName + "&key=" + client_Sync_CredentialList[0].WI_Password;

                    byte[] credentials = UTF8Encoding.UTF8.GetBytes(client_Sync_CredentialList[0].WI_FB_LoginName + ":" + client_Sync_CredentialList[0].WI_FB_Password);
                    System.Net.Http.HttpClientHandler handler = new System.Net.Http.HttpClientHandler();// Added this condition to disbale SSL Validation 
                    handler.ServerCertificateCustomValidationCallback += (sender, cert, chain, sslPolicyErrors) => true; // Added this condition to disbale SSL Validation 
                    System.Net.Http.HttpClient client = new System.Net.Http.HttpClient(handler);

                    client.BaseAddress = new Uri(url);

                    client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(credentials));

                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    HttpResponseMessage messge = client.GetAsync(url).Result;
                    if (messge.IsSuccessStatusCode)
                    {
                        string result = messge.Content.ReadAsStringAsync().Result;
                        List<ItemCodesJsonDTO> itemCodesJsonDTO = JsonConvert.DeserializeObject<List<ItemCodesJsonDTO>>(result);
                        if (itemCodesJsonDTO != null && itemCodesJsonDTO.Count > 0)
                        {
                            foreach (var itemCode in itemCodesJsonDTO)
                            {
                                Import_Client_ItemCode_MasterDTO import_Client_ItemCode_MasterDTO = new Import_Client_ItemCode_MasterDTO();
                                import_Client_ItemCode_MasterDTO.Import_Client_ItemCode_Code = itemCode.code;
                                import_Client_ItemCode_MasterDTO.Import_Client_ItemCode_Description = itemCode.description;
                                import_Client_ItemCode_MasterDTO.Import_Client_ItemCode_LoanType = itemCode.loantype;
                                import_Client_ItemCode_MasterDTO.Import_Client_ItemCode_Unit = itemCode.unit;
                                import_Client_ItemCode_MasterDTO.Import_Client_ItemCode_Price = itemCode.price;
                                import_Client_ItemCode_MasterDTO.Import_Client_ItemCode_Dimensions = itemCode.dimensions;
                                import_Client_ItemCode_MasterDTO.Import_Client_ItemCode_Manhours = itemCode.manhours;
                                import_Client_ItemCode_MasterDTO.UserID = model.UserID;
                                import_Client_ItemCode_MasterDTO.Import_Client_ItemCode_IsActive = true;
                                import_Client_ItemCode_MasterDTO.Import_Client_ItemCode_IsDelete = false;
                                import_Client_ItemCode_MasterDTO.Type = 1;
                                var objData = AddImport_Client_ItemCode_MasterData(import_Client_ItemCode_MasterDTO);
                                objDataModel.Add(objData);
                            }
                            AddEntry_Task_Configuration_Master(model);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objDataModel;
        }

        public List<dynamic> AddLocationCodes(Client_Sync_DTO model)
        {
            List<dynamic> objDataModel = new List<dynamic>();
           // string URL = "https://test.fivecontractor.com/vendorsys/api/locationcodes" + "?contractor=" + model.ConUserName + "&key=" + model.ConPassword;

            try
            {
                model.WI_ImportFrom = 4; // For FiveBrothers 
                model.Type = 1;
                List<Client_Sync_Credential> client_Sync_CredentialList = GetClientSyncCredentialList(model);

                if (client_Sync_CredentialList != null && client_Sync_CredentialList.Count > 0)
                {
                    client_Sync_CredentialList[0].WI_Password = securityHelper.GetMD5Hash(client_Sync_CredentialList[0].WI_Password);

                    string url = client_Sync_CredentialList[0].ApiUrl.Trim() + "locationcodes" + "?contractor=" + client_Sync_CredentialList[0].WI_LoginName + "&key=" + client_Sync_CredentialList[0].WI_Password;

                    byte[] credentials = UTF8Encoding.UTF8.GetBytes(client_Sync_CredentialList[0].WI_FB_LoginName + ":" + client_Sync_CredentialList[0].WI_FB_Password);
                    System.Net.Http.HttpClientHandler handler = new System.Net.Http.HttpClientHandler();// Added this condition to disbale SSL Validation 
                    handler.ServerCertificateCustomValidationCallback += (sender, cert, chain, sslPolicyErrors) => true; // Added this condition to disbale SSL Validation 
                    System.Net.Http.HttpClient client = new System.Net.Http.HttpClient(handler);

                    client.BaseAddress = new Uri(url);

                    client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(credentials));

                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage messge = client.GetAsync(url).Result;
                    if (messge.IsSuccessStatusCode)
                    {
                        string result = messge.Content.ReadAsStringAsync().Result;
                        List<LocationCodesJsonDTO> locationCodesJsonDTO = JsonConvert.DeserializeObject<List<LocationCodesJsonDTO>>(result);
                        if (locationCodesJsonDTO != null && locationCodesJsonDTO.Count > 0)
                        {
                            foreach (var locationCode in locationCodesJsonDTO)
                            {
                                Import_Client_LocationCode_MasterDTO import_Client_LocationCode_MasterDTO = new Import_Client_LocationCode_MasterDTO();
                                import_Client_LocationCode_MasterDTO.Import_Client_LocationCodes_Code = locationCode.code;
                                import_Client_LocationCode_MasterDTO.Import_Client_LocationCodes_Description = locationCode.description;
                                import_Client_LocationCode_MasterDTO.UserID = model.UserID;
                                import_Client_LocationCode_MasterDTO.Import_Client_LocationCodes_IsActive = true;
                                import_Client_LocationCode_MasterDTO.Import_Client_LocationCodes_IsDelete = false;
                                import_Client_LocationCode_MasterDTO.Type = 1;
                                var objData = AddImport_Client_LocationCode_MasterData(import_Client_LocationCode_MasterDTO);
                                objDataModel.Add(objData);
                            }
                        }
                    }
                }
                   
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objDataModel;
        }
        public List<dynamic> AddCategoryCodes(Client_Sync_DTO model)
        {
            List<dynamic> objDataModel = new List<dynamic>();

            //string URL = "https://test.fivecontractor.com/vendorsys/api/categorycodes" + "?contractor=" + model.ConUserName + "&key=" + model.ConPassword;

            try
            {
                model.WI_ImportFrom = 4; // For FiveBrothers 
                model.Type = 1;
                List<Client_Sync_Credential> client_Sync_CredentialList = GetClientSyncCredentialList(model);

                if (client_Sync_CredentialList != null && client_Sync_CredentialList.Count > 0)
                {
                    client_Sync_CredentialList[0].WI_Password = securityHelper.GetMD5Hash(client_Sync_CredentialList[0].WI_Password);

                    string url = client_Sync_CredentialList[0].ApiUrl.Trim() + "categorycodes" + "?contractor=" + client_Sync_CredentialList[0].WI_LoginName + "&key=" + client_Sync_CredentialList[0].WI_Password;

                    byte[] credentials = UTF8Encoding.UTF8.GetBytes(client_Sync_CredentialList[0].WI_FB_LoginName + ":" + client_Sync_CredentialList[0].WI_FB_Password);
                    System.Net.Http.HttpClientHandler handler = new System.Net.Http.HttpClientHandler();// Added this condition to disbale SSL Validation 
                    handler.ServerCertificateCustomValidationCallback += (sender, cert, chain, sslPolicyErrors) => true; // Added this condition to disbale SSL Validation 
                    System.Net.Http.HttpClient client = new System.Net.Http.HttpClient(handler);

                    client.BaseAddress = new Uri(url);

                    client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(credentials));

                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage messge = client.GetAsync(url).Result;
                    if (messge.IsSuccessStatusCode)
                    {
                        string result = messge.Content.ReadAsStringAsync().Result;
                        List<CategoryCodesJsonDTO> categoryCodesJsonDTO = JsonConvert.DeserializeObject<List<CategoryCodesJsonDTO>>(result);
                        if (categoryCodesJsonDTO != null && categoryCodesJsonDTO.Count > 0)
                        {
                            foreach (var categoryCodes in categoryCodesJsonDTO)
                            {
                                Import_Client_CategoryCode_MasterDTO import_Client_CategoryCode_MasterDTO = new Import_Client_CategoryCode_MasterDTO();
                                import_Client_CategoryCode_MasterDTO.Import_Client_CategoryCodes_Code = categoryCodes.code;
                                import_Client_CategoryCode_MasterDTO.Import_Client_CategoryCodes_Description = categoryCodes.description;
                                import_Client_CategoryCode_MasterDTO.Import_Client_CategoryCodes_MainCategory = categoryCodes.description;
                                import_Client_CategoryCode_MasterDTO.UserID = model.UserID;
                                import_Client_CategoryCode_MasterDTO.Import_Client_CategoryCodes_IsActive = true;
                                import_Client_CategoryCode_MasterDTO.Import_Client_CategoryCodes_IsDelete = false;
                                import_Client_CategoryCode_MasterDTO.Type = 1;
                                var objData = AddImport_Client_CategoryCodeData(import_Client_CategoryCode_MasterDTO);
                                objDataModel.Add(objData);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objDataModel;
        }

        public List<dynamic> AddOrders(Client_Sync_DTO model)
        {
            List<dynamic> objDataModel = new List<dynamic>();

            //string URL = "https://test.fivecontractor.com/vendorsys/api/orders" + "?contractor=" + model.ConUserName + "&key=" + model.ConPassword;

            try
            {
                model.WI_ImportFrom = 4; // For FiveBrothers 
                model.Type = 1;
                List<Client_Sync_Credential> client_Sync_CredentialList = GetClientSyncCredentialList(model);

                if (client_Sync_CredentialList != null && client_Sync_CredentialList.Count > 0)
                {
                    client_Sync_CredentialList[0].WI_Password = securityHelper.GetMD5Hash(client_Sync_CredentialList[0].WI_Password);

                    string url = client_Sync_CredentialList[0].ApiUrl.Trim() + "orders" + "?contractor=" + client_Sync_CredentialList[0].WI_LoginName + "&key=" + client_Sync_CredentialList[0].WI_Password;

                    byte[] credentials = UTF8Encoding.UTF8.GetBytes(client_Sync_CredentialList[0].WI_FB_LoginName + ":" + client_Sync_CredentialList[0].WI_FB_Password);
                    System.Net.Http.HttpClientHandler handler = new System.Net.Http.HttpClientHandler();// Added this condition to disbale SSL Validation 
                    handler.ServerCertificateCustomValidationCallback += (sender, cert, chain, sslPolicyErrors) => true; // Added this condition to disbale SSL Validation 
                    System.Net.Http.HttpClient client = new System.Net.Http.HttpClient(handler);

                    client.BaseAddress = new Uri(url);

                    client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(credentials));

                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage messge = client.GetAsync(url).Result;
                    if (messge.IsSuccessStatusCode)
                    {
                        string result = messge.Content.ReadAsStringAsync().Result;
                        List<OrdersJsonDTO> ordersJsonDTO = JsonConvert.DeserializeObject<List<OrdersJsonDTO>>(result);
                        if (ordersJsonDTO != null && ordersJsonDTO.Count > 0)
                        {
                            foreach (var orders in ordersJsonDTO)
                            {
                                Import_Client_Orders_MasterDTO import_Client_Orders_MasterDTO = new Import_Client_Orders_MasterDTO();
                                import_Client_Orders_MasterDTO.Import_Client_Orders_Order_Number = orders.ordernumber;
                                import_Client_Orders_MasterDTO.Import_Client_Orders_Address = orders.address;
                                import_Client_Orders_MasterDTO.Import_Client_Orders_City = orders.city;
                                import_Client_Orders_MasterDTO.Import_Client_Orders_State = orders.state;
                                import_Client_Orders_MasterDTO.Import_Client_Orders_Zip = orders.zip;
                                import_Client_Orders_MasterDTO.Import_Client_Orders_Name = orders.name;
                                import_Client_Orders_MasterDTO.Import_Client_Orders_DateOrdered = orders.dateOrdered;
                                import_Client_Orders_MasterDTO.Import_Client_Orders_EarliestDate = orders.earliestDate;
                                import_Client_Orders_MasterDTO.Import_Client_Orders_DueDate = orders.dueDate;
                                import_Client_Orders_MasterDTO.Import_Client_Orders_Photo_Requirements = orders.photoRequirements;
                                import_Client_Orders_MasterDTO.Import_Client_Orders_OrderInstructions = orders.orderInstructions;
                                import_Client_Orders_MasterDTO.Import_Client_Orders_PhotoInstructions = orders.photoInstructions;
                                import_Client_Orders_MasterDTO.Import_Client_Orders_DateCancelled = orders.dateCancelled;
                                import_Client_Orders_MasterDTO.Import_Client_Orders_FormType = orders.formType;
                                import_Client_Orders_MasterDTO.Import_Client_Orders_OrderType = orders.orderType;
                                import_Client_Orders_MasterDTO.Import_Client_Orders_Customer = orders.customer;
                                import_Client_Orders_MasterDTO.Import_Client_Orders_Contractor = orders.contractor;
                                import_Client_Orders_MasterDTO.Import_Client_Orders_Callingcard = orders.callingcard;
                                import_Client_Orders_MasterDTO.Import_Client_Orders_Loannumber = orders.loannumber;
                                import_Client_Orders_MasterDTO.Import_Client_Orders_Mortco = orders.mortco;
                                import_Client_Orders_MasterDTO.Import_Client_Orders_LoanType = orders.loanType;
                                import_Client_Orders_MasterDTO.Import_Client_Orders_Department = orders.department;
                                import_Client_Orders_MasterDTO.Import_Client_Orders_WorkValidation = orders.workValidation;
                                import_Client_Orders_MasterDTO.Import_Client_Orders_Version = orders.version;
                                import_Client_Orders_MasterDTO.Import_Client_Orders_Lotsize = orders.lotsize;
                                import_Client_Orders_MasterDTO.Import_Client_Orders_Lockbox = orders.lockbox;
                                import_Client_Orders_MasterDTO.Import_Client_Orders_Latitude = orders.latitude;
                                import_Client_Orders_MasterDTO.Import_Client_Orders_Longitude = orders.longitude;
                                import_Client_Orders_MasterDTO.Import_Client_Orders_OrderTypeCode = orders.orderTypeCode;
                                import_Client_Orders_MasterDTO.Import_Client_Orders_SubContractor = orders.subcontractor;
                                import_Client_Orders_MasterDTO.Import_Client_Orders_Previous = orders.previous;
                                import_Client_Orders_MasterDTO.UserID = model.UserID;
                                import_Client_Orders_MasterDTO.Import_Client_Orders_IsActive = true;
                                import_Client_Orders_MasterDTO.Import_Client_Orders_IsDelete = false;
                                import_Client_Orders_MasterDTO.Type = 1;
                                var objData = AddImport_Client_OrdersData(import_Client_Orders_MasterDTO);
                                objDataModel.Add(objData);
                            }
                        }
                    }
                }
                   
                
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objDataModel;
        }


        public List<dynamic> Add_Import_Client_BidCategory_Master(Import_Client_BidCategory_MasteDTO model)
        {
            List<dynamic> objAddData = new List<dynamic>();
            List<dynamic> objData = new List<dynamic>();

            string insertProcedure = "[CreateUpdate_Import_Client_BidCategory_Master]";
            Import_Client_BidCategory_Master import_Client_BidCategory_Master = new Import_Client_BidCategory_Master();

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Import_Client_BidCategory_PkeyId", 1 + "#bigint#" + model.Import_Client_BidCategory_PkeyId);
                input_parameters.Add("@Import_Client_BidCategory_Name", 1 + "#varchar#" + model.Import_Client_BidCategory_Name);
                input_parameters.Add("@Import_Client_BidCategory_Import_From_Id", 1 + "#bigint#" + model.Import_Client_BidCategory_Import_From_Id);
                input_parameters.Add("@Import_Client_BidCategory_Client_Id", 1 + "#bigint#" + model.Import_Client_BidCategory_Client_Id);
                input_parameters.Add("@Import_Client_BidCategory_Company_Id", 1 + "#bigint#" + model.Import_Client_BidCategory_Company_Id);
                input_parameters.Add("@Import_Client_BidCategory_User_Id", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Import_Client_BidCategory_IsActive", 1 + "#bit#" + model.Import_Client_BidCategory_IsActive);
                input_parameters.Add("@Import_Client_BidCategory_IsDelete", 1 + "#bit#" + model.Import_Client_BidCategory_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Import_Client_BidCategory_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    import_Client_BidCategory_Master.Import_Client_BidCategory_PkeyId = "0";
                    import_Client_BidCategory_Master.Status = "0";
                    import_Client_BidCategory_Master.ErrorMessage = "Error while Saviving in the Database";
                }
                else
                {
                    import_Client_BidCategory_Master.Import_Client_BidCategory_PkeyId = Convert.ToString(objData[0]);
                    import_Client_BidCategory_Master.Status = Convert.ToString(objData[1]);
                }
                objAddData.Add(import_Client_BidCategory_Master);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objAddData;
        }

        private DataSet Get_Import_Client_BidCategoryMaster(Import_Client_BidCategory_MasteDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Import_Client_BidCategory_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Import_Client_BidCategory_PkeyId", 1 + "#bigint#" + model.Import_Client_BidCategory_PkeyId);
                input_parameters.Add("@Import_Client_BidCategory_Company_Id", 1 + "#bigint#" + model.Import_Client_BidCategory_Company_Id);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ds;
        }

        public List<dynamic> Get_Import_Client_BidCategoryMasterDetails(Import_Client_BidCategory_MasteDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = Get_Import_Client_BidCategoryMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Import_Client_BidCategory_MasteDTO> Import_Client_BidCategory =
                   (from item in myEnumerableFeaprd
                    select new Import_Client_BidCategory_MasteDTO
                    {
                        Import_Client_BidCategory_PkeyId = item.Field<Int64>("Import_Client_BidCategory_PkeyId"),
                        Import_Client_BidCategory_Name = item.Field<String>("Import_Client_BidCategory_Name"),
                        Import_Client_BidCategory_Import_From_Id = item.Field<Int64?>("Import_Client_BidCategory_Import_From_Id"),
                        Import_Client_BidCategory_Client_Id = item.Field<Int64?>("Import_Client_BidCategory_Client_Id"),
                        Import_Client_BidCategory_Company_Id = item.Field<Int64?>("Import_Client_BidCategory_Company_Id"),
                        Import_Client_BidCategory_IsActive = item.Field<Boolean?>("Import_Client_BidCategory_IsActive")
                    }).ToList();

                objDynamic.Add(Import_Client_BidCategory);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objDynamic;
        }

        public List<dynamic> AddTaskBid_DamageCaseData(Import_Client_TaskBid_DamageCaseDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objAddData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_Import_Client_TaskBid_DamageCase_Master]";
            Import_Client_TaskBid_DamageCase import_Client_TaskBid_DamageCase = new Import_Client_TaskBid_DamageCase();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Import_Client_TaskBid_DamageCase_PkeyId", 1 + "#bigint#" + model.Import_Client_DamageCauses_PkeyId);
                input_parameters.Add("@Import_Client_TaskBid_DamageCase_Name", 1 + "#nvarchar#" + model.Import_Client_DamageCauses_Name);
                input_parameters.Add("@Import_Client_TaskBid_DamageCase_Import_From_Id", 1 + "#bigint#" + model.Import_Client_DamageCauses_Import_From_Id);
                input_parameters.Add("@Import_Client_TaskBid_DamageCase_Client_Id", 1 + "#bigint#" + model.Import_Client_DamageCauses_Client_Id);
                input_parameters.Add("@Import_Client_TaskBid_DamageCase_Company_Id", 1 + "#bigint#" + model.Import_Client_DamageCauses_Company_Id);
                input_parameters.Add("@Import_Client_TaskBid_DamageCase_User_Id", 1 + "#bigint#" + model.Import_Client_DamageCauses_UserID);
                input_parameters.Add("@Import_Client_TaskBid_DamageCase_IsActive", 1 + "#bit#" + model.Import_Client_DamageCauses_IsActive);
                input_parameters.Add("@Import_Client_TaskBid_DamageCase_IsDelete", 1 + "#bit#" + model.Import_Client_DamageCauses_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Import_Client_TaskBid_DamageCase_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    import_Client_TaskBid_DamageCase.Import_Client_DamageCauses_PkeyId = "0";
                    import_Client_TaskBid_DamageCase.Status = "0";
                    import_Client_TaskBid_DamageCase.ErrorMessage = "Error while Saviving in the Database";
                }
                else
                {
                    import_Client_TaskBid_DamageCase.Import_Client_DamageCauses_PkeyId = Convert.ToString(objData[0]);
                    import_Client_TaskBid_DamageCase.Status = Convert.ToString(objData[1]);
                }
                objAddData.Add(import_Client_TaskBid_DamageCase);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objAddData;
        }

        private DataSet GetImport_Client_TaskBid_DamageCaseMaster(Import_Client_TaskBid_DamageCaseDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Import_Client_TaskBid_DamageCase_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Import_Client_DamageCauses_PkeyId", 1 + "#bigint#" + model.Import_Client_DamageCauses_PkeyId);
                input_parameters.Add("@Import_Client_DamageCauses_Company_Id", 1 + "#int#" + model.Import_Client_DamageCauses_Company_Id);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ds;
        }

        public List<dynamic> GetImport_Client_TaskBid_DamageCaseDetails(Import_Client_TaskBid_DamageCaseDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetImport_Client_TaskBid_DamageCaseMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Import_Client_TaskBid_DamageCaseDTO> UserAddressDetails =
                   (from item in myEnumerableFeaprd
                    select new Import_Client_TaskBid_DamageCaseDTO
                    {
                        Import_Client_DamageCauses_PkeyId = item.Field<Int64>("Import_Client_DamageCauses_PkeyId"),
                        Import_Client_DamageCauses_Name = item.Field<String>("Import_Client_DamageCauses_Name"),
                        Import_Client_DamageCauses_Import_From_Id = item.Field<Int64?>("Import_Client_DamageCauses_Import_From_Id"),
                        Import_Client_DamageCauses_Client_Id = item.Field<Int64?>("Import_Client_DamageCauses_Client_Id"),
                        Import_Client_DamageCauses_Company_Id = item.Field<Int64?>("Import_Client_DamageCauses_Company_Id"),
                        Import_Client_DamageCauses_UserID = item.Field<Int64?>("Import_Client_DamageCauses_UserID"),
                        Import_Client_DamageCauses_IsActive = item.Field<Boolean?>("Import_Client_DamageCauses_IsActive")


                    }).ToList();

                objDynamic.Add(UserAddressDetails);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objDynamic;
        }
        public List<dynamic> AddImport_Client_DamageItemsData(Import_Client_DamageItems_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objAddData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_Import_Client_DamageItems_Master]";
            Import_Client_DamageItems_Master import_Client_DamageItems_Master = new Import_Client_DamageItems_Master();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Import_Client_DamageItem_PkeyID", 1 + "#bigint#" + model.Import_Client_DamageItem_PkeyID);
                input_parameters.Add("@Import_Client_DamageItem_Name", 1 + "#nvarchar#" + model.Import_Client_DamageItem_Name);
                input_parameters.Add("@Import_Client_DamageItem_Imp_From_Id", 1 + "#bigint#" + model.Import_Client_DamageItem_Imp_From_Id);
                input_parameters.Add("@Import_Client_DamageItem_ClientId", 1 + "#bigint#" + model.Import_Client_DamageItem_ClientId);
                input_parameters.Add("@Import_Client_DamageItem_CompanyId", 1 + "#bigint#" + model.Import_Client_DamageItem_CompanyId);
                input_parameters.Add("@Import_Client_DamageItem_UserId", 1 + "#bigint#" + model.Import_Client_DamageItem_UserId);
                input_parameters.Add("@Import_Client_DamageItem_IsActive", 1 + "#bit#" + model.Import_Client_DamageItem_IsActive);
                input_parameters.Add("@Import_Client_DamageItem_IsDelete", 1 + "#bit#" + model.Import_Client_DamageItem_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Import_Client_DamageItem_PkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    import_Client_DamageItems_Master.Import_Client_DamageItem_PkeyID = "0";
                    import_Client_DamageItems_Master.Status = "0";
                    import_Client_DamageItems_Master.ErrorMessage = "Error while Saviving in the Database";
                }
                else
                {
                    import_Client_DamageItems_Master.Import_Client_DamageItem_PkeyID = Convert.ToString(objData[0]);
                    import_Client_DamageItems_Master.Status = Convert.ToString(objData[1]);
                }
                objAddData.Add(import_Client_DamageItems_Master);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objAddData;
        }

        private DataSet GetImport_Client_DamageItemsMaster(Import_Client_DamageItems_MasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Import_Client_DamageItems_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Import_Client_DamageItem_PkeyID", 1 + "#bigint#" + model.Import_Client_DamageItem_PkeyID);
                input_parameters.Add("@Import_Client_DamageItem_CompanyId", 1 + "#int#" + model.Import_Client_DamageItem_CompanyId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ds;
        }

        public List<dynamic> GetImport_Client_DamageItemsDetails(Import_Client_DamageItems_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetImport_Client_DamageItemsMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Import_Client_DamageItems_MasterDTO> Import_Client_DamageItems =
                   (from item in myEnumerableFeaprd
                    select new Import_Client_DamageItems_MasterDTO
                    {
                        Import_Client_DamageItem_PkeyID = item.Field<Int64>("Import_Client_DamageItem_PkeyID"),
                        Import_Client_DamageItem_Name = item.Field<String>("Import_Client_DamageItem_Name"),
                        Import_Client_DamageItem_Imp_From_Id = item.Field<Int64?>("Import_Client_DamageItem_Imp_From_Id"),
                        Import_Client_DamageItem_ClientId = item.Field<Int64?>("Import_Client_DamageItem_ClientId"),
                        Import_Client_DamageItem_CompanyId = item.Field<Int64?>("Import_Client_DamageItem_CompanyId"),
                        Import_Client_DamageItem_UserId = item.Field<Int64?>("Import_Client_DamageItem_UserId"),
                        Import_Client_DamageItem_IsActive = item.Field<Boolean?>("Import_Client_DamageItem_IsActive")


                    }).ToList();

                objDynamic.Add(Import_Client_DamageItems);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objDynamic;
        }

        public List<dynamic> AddImport_Client_Rooms_MasterData(Import_Client_Rooms_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objAddData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_Import_Client_Rooms_Master]";
            Import_Client_Rooms_Master import_Client_Rooms_Master = new Import_Client_Rooms_Master();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Import_Client_Rooms_PkeyId", 1 + "#bigint#" + model.Import_Client_Rooms_PkeyId);
                input_parameters.Add("@Import_Client_Rooms_Name", 1 + "#nvarchar#" + model.Import_Client_Rooms_Name);
                input_parameters.Add("@Import_Client_Rooms_Import_From_Id", 1 + "#bigint#" + model.Import_Client_Rooms_Import_From_Id);
                input_parameters.Add("@Import_Client_Rooms_Client_Id", 1 + "#bigint#" + model.Import_Client_Rooms_Client_Id);
                input_parameters.Add("@Import_Client_Rooms_Company_Id", 1 + "#bigint#" + model.Import_Client_Rooms_Company_Id);
                input_parameters.Add("@Import_Client_Rooms_User_Id", 1 + "#bigint#" + model.Import_Client_Rooms_User_Id);
                input_parameters.Add("@Import_Client_Rooms_IsActive", 1 + "#bit#" + model.Import_Client_Rooms_IsActive);
                input_parameters.Add("@Import_Client_Rooms_IsDelete", 1 + "#bit#" + model.Import_Client_Rooms_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Import_Client_Rooms_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    import_Client_Rooms_Master.Import_Client_Rooms_PkeyId = "0";
                    import_Client_Rooms_Master.Status = "0";
                    import_Client_Rooms_Master.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    import_Client_Rooms_Master.Import_Client_Rooms_PkeyId = Convert.ToString(objData[0]);
                    import_Client_Rooms_Master.Status = Convert.ToString(objData[1]);
                }
                objAddData.Add(import_Client_Rooms_Master);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objAddData;
        }

        private DataSet GetImport_Client_RoomsMaster(Import_Client_Rooms_MasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Import_Client_Rooms_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Import_Client_Rooms_PkeyId", 1 + "#bigint#" + model.Import_Client_Rooms_PkeyId);
                input_parameters.Add("@Import_Client_Rooms_Company_Id", 1 + "#int#" + model.Import_Client_Rooms_Company_Id);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ds;
        }

        public List<dynamic> GetImport_Client_RoomsDetails(Import_Client_Rooms_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetImport_Client_RoomsMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Import_Client_Rooms_MasterDTO> Import_Client_Rooms =
                   (from item in myEnumerableFeaprd
                    select new Import_Client_Rooms_MasterDTO
                    {
                        Import_Client_Rooms_PkeyId = item.Field<Int64>("Import_Client_Rooms_PkeyId"),
                        Import_Client_Rooms_Name = item.Field<String>("Import_Client_Rooms_Name"),
                        Import_Client_Rooms_Import_From_Id = item.Field<Int64?>("Import_Client_Rooms_Import_From_Id"),
                        Import_Client_Rooms_Client_Id = item.Field<Int64?>("Import_Client_Rooms_Client_Id"),
                        Import_Client_Rooms_Company_Id = item.Field<Int64?>("Import_Client_Rooms_Company_Id"),
                        Import_Client_Rooms_User_Id = item.Field<Int64?>("Import_Client_Rooms_User_Id"),
                        Import_Client_Rooms_IsActive = item.Field<Boolean?>("Import_Client_Rooms_IsActive")


                    }).ToList();

                objDynamic.Add(Import_Client_Rooms);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objDynamic;
        }
        public List<dynamic> AddImport_Client_DamageCauses_MasterData(Import_Client_DamageCauses_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objAddData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_Import_Client_DamageCauses_Master]";
            Import_Client_DamageCauses_Master import_Client_DamageCauses_Master = new Import_Client_DamageCauses_Master();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Import_Client_DamageCauses_PkeyId", 1 + "#bigint#" + model.Import_Client_DamageCauses_PkeyId);
                input_parameters.Add("@Import_Client_DamageCauses_Name", 1 + "#nvarchar#" + model.Import_Client_DamageCauses_Name);
                input_parameters.Add("@Import_Client_DamageCauses_Import_From_Id", 1 + "#bigint#" + model.Import_Client_DamageCauses_Import_From_Id);
                input_parameters.Add("@Import_Client_DamageCauses_Client_Id", 1 + "#bigint#" + model.Import_Client_DamageCauses_Client_Id);
                input_parameters.Add("@Import_Client_DamageCauses_Company_Id", 1 + "#bigint#" + model.Import_Client_DamageCauses_Company_Id);
                input_parameters.Add("@Import_Client_DamageCauses_UserID", 1 + "#bigint#" + model.Import_Client_DamageCauses_UserID);
                input_parameters.Add("@Import_Client_DamageCauses_IsActive", 1 + "#bit#" + model.Import_Client_DamageCauses_IsActive);
                input_parameters.Add("@Import_Client_DamageCauses_IsDelete", 1 + "#bit#" + model.Import_Client_DamageCauses_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Import_Client_DamageCauses_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    import_Client_DamageCauses_Master.Import_Client_DamageCauses_PkeyId = "0";
                    import_Client_DamageCauses_Master.Status = "0";
                    import_Client_DamageCauses_Master.ErrorMessage = "Error while Saviving in the Database";
                }
                else
                {
                    import_Client_DamageCauses_Master.Import_Client_DamageCauses_PkeyId = Convert.ToString(objData[0]);
                    import_Client_DamageCauses_Master.Status = Convert.ToString(objData[1]);
                }
                objAddData.Add(import_Client_DamageCauses_Master);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objAddData;
        }

        private DataSet GetImport_Client_DamageCausesMaster(Import_Client_DamageCauses_MasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Import_Client_DamageCauses_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Import_Client_DamageCauses_PkeyId", 1 + "#bigint#" + model.Import_Client_DamageCauses_PkeyId);
                input_parameters.Add("@Import_Client_DamageCauses_Company_Id", 1 + "#int#" + model.Import_Client_DamageCauses_Company_Id);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ds;
        }

        public List<dynamic> GetImport_Client_DamageCausesMasterDetails(Import_Client_DamageCauses_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetImport_Client_DamageCausesMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Import_Client_DamageCauses_MasterDTO> Import_Client_DamageCauses =
                   (from item in myEnumerableFeaprd
                    select new Import_Client_DamageCauses_MasterDTO
                    {
                        Import_Client_DamageCauses_PkeyId = item.Field<Int64>("Import_Client_DamageCauses_PkeyId"),
                        Import_Client_DamageCauses_Name = item.Field<String>("Import_Client_DamageCauses_Name"),
                        Import_Client_DamageCauses_Import_From_Id = item.Field<Int64?>("Import_Client_DamageCauses_Import_From_Id"),
                        Import_Client_DamageCauses_Client_Id = item.Field<Int64?>("Import_Client_DamageCauses_Client_Id"),
                        Import_Client_DamageCauses_Company_Id = item.Field<Int64?>("Import_Client_DamageCauses_Company_Id"),
                        Import_Client_DamageCauses_UserID = item.Field<Int64?>("Import_Client_DamageCauses_UserID"),
                        Import_Client_DamageCauses_IsActive = item.Field<Boolean?>("Import_Client_DamageCauses_IsActive")


                    }).ToList();

                objDynamic.Add(Import_Client_DamageCauses);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objDynamic;
        }
        public List<dynamic> AddImport_Client_Buildings_MasterData(Import_Client_Buildings_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objAddData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_Import_Client_Buildings_Master]";
            Import_Client_Buildings_Master import_Client_Buildings_Master = new Import_Client_Buildings_Master();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Import_Client_Buildings_PkeyID", 1 + "#bigint#" + model.Import_Client_Buildings_PkeyID);
                input_parameters.Add("@Import_Client_Buildings_Name", 1 + "#nvarchar#" + model.Import_Client_Buildings_Name);
                input_parameters.Add("@Import_Client_Buildings_Import_From_Id", 1 + "#bigint#" + model.Import_Client_Buildings_Import_From_Id);
                input_parameters.Add("@Import_Client_Buildings_Client_Id", 1 + "#bigint#" + model.Import_Client_Buildings_Client_Id);
                input_parameters.Add("@Import_Client_Buildings_Company_Id", 1 + "#bigint#" + model.Import_Client_Buildings_Company_Id);
                input_parameters.Add("@Import_Client_Buildings_User_Id", 1 + "#bigint#" + model.Import_Client_Buildings_User_Id);
                input_parameters.Add("@Import_Client_Buildings_IsActive", 1 + "#bit#" + model.Import_Client_Buildings_IsActive);
                input_parameters.Add("@Import_Client_Buildings_IsDelete", 1 + "#bit#" + model.Import_Client_Buildings_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Import_Client_Buildings_PkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    import_Client_Buildings_Master.Import_Client_Buildings_PkeyID = "0";
                    import_Client_Buildings_Master.Status = "0";
                    import_Client_Buildings_Master.ErrorMessage = "Error while Saviving in the Database";
                }
                else
                {
                    import_Client_Buildings_Master.Import_Client_Buildings_PkeyID = Convert.ToString(objData[0]);
                    import_Client_Buildings_Master.Status = Convert.ToString(objData[1]);
                }
                objAddData.Add(import_Client_Buildings_Master);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objAddData;
        }

        private DataSet GetImport_Client_Buildings_Master(Import_Client_Buildings_MasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Import_Client_Buildings_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Import_Client_Buildings_PkeyID", 1 + "#bigint#" + model.Import_Client_Buildings_PkeyID);
                input_parameters.Add("@Import_Client_Buildings_Company_Id", 1 + "#int#" + model.Import_Client_Buildings_Company_Id);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ds;
        }        

        public List<dynamic> GetImport_Client_Buildings_MasterDetails(Import_Client_Buildings_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetImport_Client_Buildings_Master(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Import_Client_Buildings_MasterDTO> Import_Client_Buildings =
                   (from item in myEnumerableFeaprd
                    select new Import_Client_Buildings_MasterDTO
                    {
                        Import_Client_Buildings_PkeyID = item.Field<Int64>("Import_Client_Buildings_PkeyID"),
                        Import_Client_Buildings_Name = item.Field<String>("Import_Client_Buildings_Name"),
                        Import_Client_Buildings_Import_From_Id = item.Field<Int64?>("Import_Client_Buildings_Import_From_Id"),
                        Import_Client_Buildings_Client_Id = item.Field<Int64?>("Import_Client_Buildings_Client_Id"),
                        Import_Client_Buildings_Company_Id = item.Field<Int64?>("Import_Client_Buildings_Company_Id"),
                        Import_Client_Buildings_User_Id = item.Field<Int64?>("Import_Client_Buildings_User_Id"),
                        Import_Client_Buildings_IsActive = item.Field<Boolean?>("Import_Client_Buildings_IsActive")


                    }).ToList();

                objDynamic.Add(Import_Client_Buildings);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objDynamic;
        }

        public List<dynamic> AddImport_Client_InsuranceCategoriesData(Import_Client_InsuranceCategoriesDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objAddData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_Import_Client_InsuranceCategories_Master]";
            Import_Client_InsuranceCategories import_Client_InsuranceCategories = new Import_Client_InsuranceCategories();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Import_Client_InsuranceCategories_PkeyId", 1 + "#bigint#" + model.Import_Client_InsuranceCategories_PkeyId);
                input_parameters.Add("@Import_Client_InsuranceCategories_Location", 1 + "#nvarchar#" + model.Import_Client_InsuranceCategories_Location);
                input_parameters.Add("@Import_Client_InsuranceCategories_Category", 1 + "#nvarchar#" + model.Import_Client_InsuranceCategories_Category);
                input_parameters.Add("@Import_Client_InsuranceCategories_Import_From_Id", 1 + "#bigint#" + model.Import_Client_InsuranceCategories_Import_From_Id);
                input_parameters.Add("@Import_Client_InsuranceCategories_Client_Id", 1 + "#bigint#" + model.Import_Client_InsuranceCategories_Client_Id);
                input_parameters.Add("@Import_Client_InsuranceCategories_Company_Id", 1 + "#bigint#" + model.Import_Client_InsuranceCategories_Company_Id);
                input_parameters.Add("@Import_Client_InsuranceCategories_User_Id", 1 + "#bigint#" + model.Import_Client_InsuranceCategories_User_Id);
                input_parameters.Add("@Import_Client_InsuranceCategories_IsActive", 1 + "#bit#" + model.Import_Client_InsuranceCategories_IsActive);
                input_parameters.Add("@Import_Client_InsuranceCategories_IsDelete", 1 + "#bit#" + model.Import_Client_InsuranceCategories_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Import_Client_InsuranceCategories_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    import_Client_InsuranceCategories.Import_Client_InsuranceCategories_PkeyId = "0";
                    import_Client_InsuranceCategories.Status = "0";
                    import_Client_InsuranceCategories.ErrorMessage = "Error while Saviving in the Database";
                }
                else
                {
                    import_Client_InsuranceCategories.Import_Client_InsuranceCategories_PkeyId = Convert.ToString(objData[0]);
                    import_Client_InsuranceCategories.Status = Convert.ToString(objData[1]);
                }
                objAddData.Add(import_Client_InsuranceCategories);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objAddData;
        }

        private DataSet GetImport_Client_InsuranceCategoriesMaster(Import_Client_InsuranceCategoriesDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Import_Client_InsuranceCategories_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Import_Client_InsuranceCategories_PkeyId", 1 + "#bigint#" + model.Import_Client_InsuranceCategories_PkeyId);
                input_parameters.Add("@Import_Client_InsuranceCategories_Company_Id", 1 + "#bigint#" + model.Import_Client_InsuranceCategories_Company_Id);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ds;
        }

        public List<dynamic> GetImport_Client_InsuranceCategoriesDetails(Import_Client_InsuranceCategoriesDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetImport_Client_InsuranceCategoriesMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Import_Client_InsuranceCategoriesDTO> Import_Client_InsuranceCategories =
                   (from item in myEnumerableFeaprd
                    select new Import_Client_InsuranceCategoriesDTO
                    {
                        Import_Client_InsuranceCategories_PkeyId = item.Field<Int64>("Import_Client_InsuranceCategories_PkeyId"),
                        Import_Client_InsuranceCategories_Location = item.Field<String>("Import_Client_InsuranceCategories_Location"),
                        Import_Client_InsuranceCategories_Category = item.Field<String>("Import_Client_InsuranceCategories_Category"),
                        Import_Client_InsuranceCategories_Import_From_Id = item.Field<Int64?>("Import_Client_InsuranceCategories_Import_From_Id"),
                        Import_Client_InsuranceCategories_Client_Id = item.Field<Int64?>("Import_Client_InsuranceCategories_Client_Id"),
                        Import_Client_InsuranceCategories_Company_Id = item.Field<Int64?>("Import_Client_InsuranceCategories_Company_Id"),
                        Import_Client_InsuranceCategories_User_Id = item.Field<Int64?>("Import_Client_InsuranceCategories_User_Id"),
                        Import_Client_InsuranceCategories_IsActive = item.Field<Boolean?>("Import_Client_InsuranceCategories_IsActive")


                    }).ToList();

                objDynamic.Add(Import_Client_InsuranceCategories);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        public List<dynamic> AddImport_Client_OrderTypeCode_MasterData(Import_Client_OrderTypeCode_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objAddData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_Import_Client_OrderTypeCode_Master]";
            Import_Client_OrderTypeCode_Master import_Client_OrderTypeCode_Master = new Import_Client_OrderTypeCode_Master();
            
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Import_Client_OrderTypeCodes_PkeyID", 1 + "#bigint#" + model.Import_Client_OrderTypeCodes_PkeyID);
                input_parameters.Add("@Import_Client_OrderTypeCodes_Code", 1 + "#nvarchar#" + model.Import_Client_OrderTypeCodes_Code);
                input_parameters.Add("@Import_Client_OrderTypeCodes_Description", 1 + "#nvarchar#" + model.Import_Client_OrderTypeCodes_Description);
                input_parameters.Add("@Import_Client_OrderTypeCodes_MainType", 1 + "#nvarchar#" + model.Import_Client_OrderTypeCodes_MainType);
                input_parameters.Add("@Import_Client_OrderTypeCodes_Import_From_Id", 1 + "#bigint#" + model.Import_Client_OrderTypeCodes_Import_From_Id);
                input_parameters.Add("@Import_Client_OrderTypeCodes_Client_Id", 1 + "#bigint#" + model.Import_Client_OrderTypeCodes_Client_Id);
                input_parameters.Add("@Import_Client_OrderTypeCodes_Company_Id", 1 + "#bigint#" + model.Import_Client_OrderTypeCodes_Company_Id);
                input_parameters.Add("@Import_Client_OrderTypeCodes_User_Id", 1 + "#bigint#" + model.Import_Client_OrderTypeCodes_User_Id);
                input_parameters.Add("@Import_Client_OrderTypeCodes_IsActive", 1 + "#bit#" + model.Import_Client_OrderTypeCodes_IsActive);
                input_parameters.Add("@Import_Client_OrderTypeCodes_IsDelete", 1 + "#bit#" + model.Import_Client_OrderTypeCodes_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Import_Client_OrderTypeCodes_PkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    import_Client_OrderTypeCode_Master.Import_Client_OrderTypeCodes_PkeyID = "0";
                    import_Client_OrderTypeCode_Master.Status = "0";
                    import_Client_OrderTypeCode_Master.ErrorMessage = "Error while Saviving in the Database";
                }
                else
                {
                    import_Client_OrderTypeCode_Master.Import_Client_OrderTypeCodes_PkeyID = Convert.ToString(objData[0]);
                    import_Client_OrderTypeCode_Master.Status = Convert.ToString(objData[1]);
                }
                objAddData.Add(import_Client_OrderTypeCode_Master);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objAddData;
        }
        private DataSet GetImport_Client_OrderTypeCodesMaster(Import_Client_OrderTypeCode_MasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Import_Client_OrderTypeCodes_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Import_Client_OrderTypeCodes_PkeyID", 1 + "#bigint#" + model.Import_Client_OrderTypeCodes_PkeyID);
                input_parameters.Add("@Import_Client_OrderTypeCodes_Company_Id", 1 + "#bigint#" + model.Import_Client_OrderTypeCodes_Company_Id);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ds;
        }

        public List<dynamic> GetImport_Client_OrderTypeCodesDetails(Import_Client_OrderTypeCode_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetImport_Client_OrderTypeCodesMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Import_Client_OrderTypeCode_MasterDTO> Import_Client_OrderTypeCodes =
                   (from item in myEnumerableFeaprd
                    select new Import_Client_OrderTypeCode_MasterDTO
                    {
                        Import_Client_OrderTypeCodes_PkeyID = item.Field<Int64>("Import_Client_OrderTypeCodes_PkeyID"),
                        Import_Client_OrderTypeCodes_Code = item.Field<String>("Import_Client_OrderTypeCodes_Code"),
                        Import_Client_OrderTypeCodes_Description = item.Field<String>("Import_Client_OrderTypeCodes_Description"),
                        Import_Client_OrderTypeCodes_MainType = item.Field<String>("Import_Client_OrderTypeCodes_MainType"),
                        Import_Client_OrderTypeCodes_Import_From_Id = item.Field<Int64?>("Import_Client_OrderTypeCodes_Import_From_Id"),
                        Import_Client_OrderTypeCodes_Client_Id = item.Field<Int64?>("Import_Client_OrderTypeCodes_Client_Id"),
                        Import_Client_OrderTypeCodes_Company_Id = item.Field<Int64?>("Import_Client_OrderTypeCodes_Company_Id"),
                        Import_Client_OrderTypeCodes_User_Id = item.Field<Int64?>("Import_Client_OrderTypeCodes_User_Id"),
                        Import_Client_OrderTypeCodes_IsActive = item.Field<Boolean?>("Import_Client_OrderTypeCodes_IsActive")


                    }).ToList();

                objDynamic.Add(Import_Client_OrderTypeCodes);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objDynamic;
        }
        public List<dynamic> AddImport_Client_ItemCode_MasterData(Import_Client_ItemCode_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objAddData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_Import_Client_ItemCode_Master]";
            Import_Client_ItemCode_Master import_Client_ItemCode_Master = new Import_Client_ItemCode_Master();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Import_Client_ItemCode_PkeyId", 1 + "#bigint#" + model.Import_Client_ItemCode_PkeyId);
                input_parameters.Add("@Import_Client_ItemCode_Code", 1 + "#nvarchar#" + model.Import_Client_ItemCode_Code);
                input_parameters.Add("@Import_Client_ItemCode_Description", 1 + "#varchar#" + model.Import_Client_ItemCode_Description);
                input_parameters.Add("@Import_Client_ItemCode_LoanType", 1 + "#varchar#" + model.Import_Client_ItemCode_LoanType);
                input_parameters.Add("@Import_Client_ItemCode_Unit", 1 + "#varchar#" + model.Import_Client_ItemCode_Unit);
                input_parameters.Add("@Import_Client_ItemCode_Price", 1 + "#varchar#" + model.Import_Client_ItemCode_Price);
                input_parameters.Add("@Import_Client_ItemCode_Dimensions", 1 + "#varchar#" + model.Import_Client_ItemCode_Dimensions);
                input_parameters.Add("@Import_Client_ItemCode_Manhours", 1 + "#varchar#" + model.Import_Client_ItemCode_Manhours);
                input_parameters.Add("@Import_Client_ItemCode_Import_From_Id", 1 + "#bigint#" + model.Import_Client_ItemCode_Import_From_Id);
                input_parameters.Add("@Import_Client_ItemCode_Client_Id", 1 + "#bigint#" + model.Import_Client_ItemCode_Client_Id);
                input_parameters.Add("@Import_Client_ItemCode_Company_Id", 1 + "#bigint#" + model.Import_Client_ItemCode_Company_Id);
                input_parameters.Add("@Import_Client_ItemCode_User_Id", 1 + "#bigint#" + model.Import_Client_ItemCode_User_Id);
                input_parameters.Add("@Import_Client_ItemCode_IsActive", 1 + "#bit#" + model.Import_Client_ItemCode_IsActive);
                input_parameters.Add("@Import_Client_ItemCode_IsDelete", 1 + "#bit#" + model.Import_Client_ItemCode_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Import_Client_ItemCode_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    import_Client_ItemCode_Master.Import_Client_ItemCode_PkeyId = "0";
                    import_Client_ItemCode_Master.Status = "0";
                    import_Client_ItemCode_Master.ErrorMessage = "Error while Saviving in the Database";
                }
                else
                {
                    import_Client_ItemCode_Master.Import_Client_ItemCode_PkeyId = Convert.ToString(objData[0]);
                    import_Client_ItemCode_Master.Status = Convert.ToString(objData[1]);
                }
                objAddData.Add(import_Client_ItemCode_Master);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objAddData;
        }
        private DataSet GetImport_Client_ItemCodeMaster(Import_Client_ItemCode_MasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Import_Client_ItemCode_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Import_Client_ItemCode_PkeyId", 1 + "#bigint#" + model.Import_Client_ItemCode_PkeyId);
                input_parameters.Add("@Import_Client_ItemCode_Company_Id", 1 + "#bigint#" + model.Import_Client_ItemCode_Company_Id);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ds;
        }

        public List<dynamic> GetImport_Client_ItemCode_MasterDetails(Import_Client_ItemCode_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetImport_Client_ItemCodeMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Import_Client_ItemCode_MasterDTO> Import_Client_ItemCode =
                   (from item in myEnumerableFeaprd
                    select new Import_Client_ItemCode_MasterDTO
                    {
                        Import_Client_ItemCode_PkeyId = item.Field<Int64>("Import_Client_ItemCode_PkeyId"),
                        Import_Client_ItemCode_Code = item.Field<String>("Import_Client_ItemCode_Code"),
                        Import_Client_ItemCode_LoanType = item.Field<String>("Import_Client_ItemCode_LoanType"),
                        Import_Client_ItemCode_Unit = item.Field<String>("Import_Client_ItemCode_Unit"),
                        Import_Client_ItemCode_Price = item.Field<String>("Import_Client_ItemCode_Price"),
                        Import_Client_ItemCode_Dimensions = item.Field<String>("Import_Client_ItemCode_Dimensions"),
                        Import_Client_ItemCode_Manhours = item.Field<String>("Import_Client_ItemCode_Manhours"),
                        Import_Client_ItemCode_Import_From_Id = item.Field<Int64?>("Import_Client_ItemCode_Import_From_Id"),
                        Import_Client_ItemCode_Client_Id = item.Field<Int64?>("Import_Client_ItemCode_Client_Id"),
                        Import_Client_ItemCode_Company_Id = item.Field<Int64?>("Import_Client_ItemCode_Company_Id"),
                        Import_Client_ItemCode_User_Id = item.Field<Int64?>("Import_Client_ItemCode_User_Id"),
                        Import_Client_ItemCode_IsActive = item.Field<Boolean?>("Import_Client_ItemCode_IsActive")


                    }).ToList();

                objDynamic.Add(Import_Client_ItemCode);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objDynamic;
        }
        public List<dynamic> AddImport_Client_LocationCode_MasterData(Import_Client_LocationCode_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objAddData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_Import_Client_LocationCode_Master]";
            Import_Client_LocationCode_Master import_Client_LocationCode_Master = new Import_Client_LocationCode_Master();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Import_Client_LocationCodes_PkeyId", 1 + "#bigint#" + model.Import_Client_LocationCodes_PkeyId);
                input_parameters.Add("@Import_Client_LocationCodes_Code", 1 + "#nvarchar#" + model.Import_Client_LocationCodes_Code);
                input_parameters.Add("@Import_Client_LocationCodes_Description", 1 + "#varchar#" + model.Import_Client_LocationCodes_Description);

                input_parameters.Add("@Import_Client_LocationCodes_Import_From_Id", 1 + "#bigint#" + model.Import_Client_LocationCodes_Import_From_Id);
                input_parameters.Add("@Import_Client_LocationCodes_Client_Id", 1 + "#bigint#" + model.Import_Client_LocationCodes_Client_Id);
                input_parameters.Add("@Import_Client_LocationCodes_Company_Id", 1 + "#bigint#" + model.Import_Client_LocationCodes_Company_Id);
                input_parameters.Add("@Import_Client_LocationCodes_User_Id", 1 + "#bigint#" + model.Import_Client_LocationCodes_User_Id);
                input_parameters.Add("@Import_Client_LocationCodes_IsActive", 1 + "#bit#" + model.Import_Client_LocationCodes_IsActive);
                input_parameters.Add("@Import_Client_LocationCodes_IsDelete", 1 + "#bit#" + model.Import_Client_LocationCodes_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Import_Client_LocationCodes_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    import_Client_LocationCode_Master.Import_Client_LocationCodes_PkeyId = "0";
                    import_Client_LocationCode_Master.Status = "0";
                    import_Client_LocationCode_Master.ErrorMessage = "Error while Saviving in the Database";
                }
                else
                {
                    import_Client_LocationCode_Master.Import_Client_LocationCodes_PkeyId = Convert.ToString(objData[0]);
                    import_Client_LocationCode_Master.Status = Convert.ToString(objData[1]);
                }
                objAddData.Add(import_Client_LocationCode_Master);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objAddData;

        }
        private DataSet GetImport_Client_LocationCodesMaster(Import_Client_LocationCode_MasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Import_Client_LocationCodes_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Import_Client_LocationCodes_PkeyId", 1 + "#bigint#" + model.Import_Client_LocationCodes_PkeyId);
                input_parameters.Add("@Import_Client_LocationCodes_Company_Id", 1 + "#bigint#" + model.Import_Client_LocationCodes_Company_Id);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ds;
        }

        public List<dynamic> GetGetImport_Client_LocationCodesMasterDetails(Import_Client_LocationCode_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetImport_Client_LocationCodesMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Import_Client_LocationCode_MasterDTO> Import_Client_LocationCodes =
                   (from item in myEnumerableFeaprd
                    select new Import_Client_LocationCode_MasterDTO
                    {
                        Import_Client_LocationCodes_PkeyId = item.Field<Int64>("Import_Client_LocationCodes_PkeyId"),
                        Import_Client_LocationCodes_Code = item.Field<String>("Import_Client_LocationCodes_Code"),
                        Import_Client_LocationCodes_Description = item.Field<String>("Import_Client_LocationCodes_Description"),
                        Import_Client_LocationCodes_Import_From_Id = item.Field<Int64?>("Import_Client_LocationCodes_Import_From_Id"),
                        Import_Client_LocationCodes_Client_Id = item.Field<Int64?>("Import_Client_LocationCodes_Client_Id"),
                        Import_Client_LocationCodes_Company_Id = item.Field<Int64?>("Import_Client_LocationCodes_Company_Id"),
                        Import_Client_LocationCodes_User_Id = item.Field<Int64?>("Import_Client_LocationCodes_User_Id"),
                        Import_Client_LocationCodes_IsActive = item.Field<Boolean?>("Import_Client_LocationCodes_IsActive")


                    }).ToList();

                objDynamic.Add(Import_Client_LocationCodes);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

        public List<dynamic> AddImport_Client_CategoryCodeData(Import_Client_CategoryCode_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objAddData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_Import_Client_CategoryCode_Master]";
            Import_Client_CategoryCode import_Client_CategoryCode = new Import_Client_CategoryCode();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Import_Client_CategoryCodes_PkeyId", 1 + "#bigint#" + model.Import_Client_CategoryCodes_PkeyId);
                input_parameters.Add("@Import_Client_CategoryCodes_Code", 1 + "#nvarchar#" + model.Import_Client_CategoryCodes_Code);
                input_parameters.Add("@Import_Client_CategoryCodes_Description", 1 + "#nvarchar#" + model.Import_Client_CategoryCodes_Description);
                input_parameters.Add("@Import_Client_CategoryCodes_MainCategory", 1 + "#nvarchar#" + model.Import_Client_CategoryCodes_MainCategory);
                input_parameters.Add("@Import_Client_CategoryCodes_Import_From_Id", 1 + "#bigint#" + model.Import_Client_CategoryCodes_Import_From_Id);
                input_parameters.Add("@Import_Client_CategoryCodes_Client_Id", 1 + "#bigint#" + model.Import_Client_CategoryCodes_Client_Id);
                input_parameters.Add("@Import_Client_CategoryCodes_Company_Id", 1 + "#bigint#" + model.Import_Client_CategoryCodes_Company_Id);
                input_parameters.Add("@Import_Client_CategoryCodes_User_Id", 1 + "#bigint#" + model.Import_Client_CategoryCodes_User_Id);
                input_parameters.Add("@Import_Client_CategoryCodes_IsActive", 1 + "#bit#" + model.Import_Client_CategoryCodes_IsActive);
                input_parameters.Add("@Import_Client_CategoryCodes_IsDelete", 1 + "#bit#" + model.Import_Client_CategoryCodes_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Import_Client_CategoryCodes_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    import_Client_CategoryCode.Import_Client_CategoryCodes_PkeyId = "0";
                    import_Client_CategoryCode.Status = "0";
                    import_Client_CategoryCode.ErrorMessage = "Error while Saviving in the Database";
                }
                else
                {
                    import_Client_CategoryCode.Import_Client_CategoryCodes_PkeyId = Convert.ToString(objData[0]);
                    import_Client_CategoryCode.Status = Convert.ToString(objData[1]);
                }
                objAddData.Add(import_Client_CategoryCode);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objAddData;
        }

        private DataSet Get_Import_Client_CategoryCodeMaster(Import_Client_CategoryCode_MasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Import_Client_CategoryCode_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Import_Client_CategoryCodes_PkeyId", 1 + "#bigint#" + model.Import_Client_CategoryCodes_PkeyId);
                input_parameters.Add("@Import_Client_CategoryCodes_Company_Id", 1 + "#bigint#" + model.Import_Client_CategoryCodes_Company_Id);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ds;
        }

        public List<dynamic> Get_Import_Client_CategoryCodeMasterDetails(Import_Client_CategoryCode_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = Get_Import_Client_CategoryCodeMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Import_Client_CategoryCode_MasterDTO> Import_Client_CategoryCodedata =
                   (from item in myEnumerableFeaprd
                    select new Import_Client_CategoryCode_MasterDTO
                    {
                        Import_Client_CategoryCodes_PkeyId = item.Field<Int64>("Import_Client_CategoryCodes_PkeyId"),
                        Import_Client_CategoryCodes_Code = item.Field<String>("Import_Client_CategoryCodes_Code"),
                        Import_Client_CategoryCodes_Description = item.Field<String>("Import_Client_CategoryCodes_Description"),
                        Import_Client_CategoryCodes_MainCategory = item.Field<String>("Import_Client_CategoryCodes_MainCategory"),
                        Import_Client_CategoryCodes_Import_From_Id = item.Field<Int64?>("Import_Client_CategoryCodes_Import_From_Id"),
                        Import_Client_CategoryCodes_Client_Id = item.Field<Int64?>("Import_Client_CategoryCodes_Client_Id"),
                        Import_Client_CategoryCodes_Company_Id = item.Field<Int64?>("Import_Client_CategoryCodes_Company_Id"),
                        Import_Client_CategoryCodes_User_Id = item.Field<Int64?>("Import_Client_CategoryCodes_User_Id"),
                        Import_Client_CategoryCodes_IsActive = item.Field<Boolean?>("Import_Client_CategoryCodes_IsActive")


                    }).ToList();

                objDynamic.Add(Import_Client_CategoryCodedata);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objDynamic;
        }

        public List<dynamic> AddImport_Client_OrdersData(Import_Client_Orders_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objAddData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_Import_Client_Orders_Master]";
            Import_Client_Orders import_Client_Orders = new Import_Client_Orders();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Import_Client_Orders_PkeyId", 1 + "#bigint#" + model.Import_Client_Orders_PkeyId);
                input_parameters.Add("@Import_Client_Orders_Order_Number", 1 + "#bigint#" + model.Import_Client_Orders_Order_Number);
                input_parameters.Add("@Import_Client_Orders_Address", 1 + "#nvarchar#" + model.Import_Client_Orders_Address);
                input_parameters.Add("@Import_Client_Orders_City", 1 + "#nvarchar#" + model.Import_Client_Orders_City);
                input_parameters.Add("@Import_Client_Orders_State", 1 + "#nvarchar#" + model.Import_Client_Orders_State);
                input_parameters.Add("@Import_Client_Orders_Zip", 1 + "#nvarchar#" + model.Import_Client_Orders_Zip);
                input_parameters.Add("@Import_Client_Orders_Name", 1 + "#nvarchar#" + model.Import_Client_Orders_Name);
                input_parameters.Add("@Import_Client_Orders_DateOrdered", 1 + "#datetime#" + model.Import_Client_Orders_DateOrdered);
                input_parameters.Add("@Import_Client_Orders_EarliestDate", 1 + "#datetime#" + model.Import_Client_Orders_EarliestDate);
                input_parameters.Add("@Import_Client_Orders_DueDate", 1 + "#datetime#" + model.Import_Client_Orders_DueDate);
                input_parameters.Add("@Import_Client_Orders_Photo_Requirements", 1 + "#nvarchar#" + model.Import_Client_Orders_Photo_Requirements);
                input_parameters.Add("@Import_Client_Orders_OrderInstructions", 1 + "#nvarchar#" + model.Import_Client_Orders_OrderInstructions);
                input_parameters.Add("@Import_Client_Orders_PhotoInstructions", 1 + "#nvarchar#" + model.Import_Client_Orders_PhotoInstructions);
                input_parameters.Add("@Import_Client_Orders_DateCancelled", 1 + "#nvarchar#" + model.Import_Client_Orders_DateCancelled);
                input_parameters.Add("@Import_Client_Orders_FormType", 1 + "#nvarchar#" + model.Import_Client_Orders_FormType);
                input_parameters.Add("@Import_Client_Orders_OrderType", 1 + "#nvarchar#" + model.Import_Client_Orders_OrderType);
                input_parameters.Add("@Import_Client_Orders_Customer", 1 + "#nvarchar#" + model.Import_Client_Orders_Customer);
                input_parameters.Add("@Import_Client_Orders_Contractor", 1 + "#nvarchar#" + model.Import_Client_Orders_Contractor);
                input_parameters.Add("@Import_Client_Orders_Callingcard", 1 + "#nvarchar#" + model.Import_Client_Orders_Callingcard);
                input_parameters.Add("@Import_Client_Orders_Loannumber", 1 + "#nvarchar#" + model.Import_Client_Orders_Loannumber);
                input_parameters.Add("@Import_Client_Orders_Mortco", 1 + "#nvarchar#" + model.Import_Client_Orders_Mortco);
                input_parameters.Add("@Import_Client_Orders_LoanType", 1 + "#nvarchar#" + model.Import_Client_Orders_LoanType);
                input_parameters.Add("@Import_Client_Orders_Department", 1 + "#nvarchar#" + model.Import_Client_Orders_Department);
                input_parameters.Add("@Import_Client_Orders_WorkValidation", 1 + "#nvarchar#" + model.Import_Client_Orders_WorkValidation);
                input_parameters.Add("@Import_Client_Orders_Version", 1 + "#nvarchar#" + model.Import_Client_Orders_Version);
                input_parameters.Add("@Import_Client_Orders_Lotsize", 1 + "#nvarchar#" + model.Import_Client_Orders_Lotsize);
                input_parameters.Add("@Import_Client_Orders_Lockbox", 1 + "#nvarchar#" + model.Import_Client_Orders_Lockbox);
                input_parameters.Add("@Import_Client_Orders_Latitude", 1 + "#nvarchar#" + model.Import_Client_Orders_Latitude);
                input_parameters.Add("@Import_Client_Orders_Longitude", 1 + "#nvarchar#" + model.Import_Client_Orders_Longitude);
                input_parameters.Add("@Import_Client_Orders_OrderTypeCode", 1 + "#nvarchar#" + model.Import_Client_Orders_OrderTypeCode);
                input_parameters.Add("@Import_Client_Orders_SubContractor", 1 + "#nvarchar#" + model.Import_Client_Orders_SubContractor);
                input_parameters.Add("@Import_Client_Orders_Previous", 1 + "#bit#" + model.Import_Client_Orders_Previous);
                input_parameters.Add("@Import_Client_Orders_Import_From_Id", 1 + "#bigint#" + model.Import_Client_Orders_Import_From_Id);
                input_parameters.Add("@Import_Client_Orders_Client_Id", 1 + "#bigint#" + model.Import_Client_Orders_Client_Id);
                input_parameters.Add("@Import_Client_Orders_Company_Id", 1 + "#bigint#" + model.Import_Client_Orders_Company_Id);
                input_parameters.Add("@Import_Client_Orders_UserId", 1 + "#bigint#" + model.Import_Client_Orders_UserId);
                input_parameters.Add("@Import_Client_Orders_IsActive", 1 + "#bit#" + model.Import_Client_Orders_IsActive);
                input_parameters.Add("@Import_Client_Orders_IsDelete", 1 + "#bit#" + model.Import_Client_Orders_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Import_Client_Orders_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    import_Client_Orders.Import_Client_Orders_PkeyId = "0";
                    import_Client_Orders.Status = "0";
                    import_Client_Orders.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    import_Client_Orders.Import_Client_Orders_PkeyId = Convert.ToString(objData[0]);
                    import_Client_Orders.Status = Convert.ToString(objData[1]);


                }
                objAddData.Add(import_Client_Orders);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objAddData;


        }

        private DataSet GetImportClientOrdersMaster(Import_Client_Orders_MasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_Import_Client_Orders_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Import_Client_Orders_PkeyId", 1 + "#bigint#" + model.Import_Client_Orders_PkeyId);
                input_parameters.Add("@Import_Client_Orders_Company_Id", 1 + "#bigint#" + model.Import_Client_Orders_Company_Id);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ds;
        }

        public List<dynamic> Get_Import_Client_Orders_Details(Import_Client_Orders_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetImportClientOrdersMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<Import_Client_Orders_MasterDTO> Import_Client_Orders =
                   (from item in myEnumerableFeaprd
                    select new Import_Client_Orders_MasterDTO
                    {
                        Import_Client_Orders_PkeyId = item.Field<Int64>("Import_Client_Orders_PkeyId"),
                        Import_Client_Orders_Order_Number = item.Field<Int64?>("Import_Client_Orders_Order_Number"),
                        Import_Client_Orders_Address = item.Field<String>("Import_Client_Orders_Address"),
                        Import_Client_Orders_City = item.Field<String>("Import_Client_Orders_City"),
                        Import_Client_Orders_State = item.Field<String>("Import_Client_Orders_State"),
                        Import_Client_Orders_Zip = item.Field<String>("Import_Client_Orders_Zip"),
                        Import_Client_Orders_Name = item.Field<String>("Import_Client_Orders_Name"),
                        Import_Client_Orders_DateOrdered = item.Field<DateTime?>("Import_Client_Orders_DateOrdered"),
                        Import_Client_Orders_EarliestDate = item.Field<DateTime?>("Import_Client_Orders_EarliestDate"),
                        Import_Client_Orders_DueDate = item.Field<DateTime?>("Import_Client_Orders_DueDate"),
                        Import_Client_Orders_Photo_Requirements = item.Field<String>("Import_Client_Orders_Photo_Requirements"),
                        Import_Client_Orders_OrderInstructions = item.Field<String>("Import_Client_Orders_OrderInstructions"),
                        Import_Client_Orders_PhotoInstructions = item.Field<String>("Import_Client_Orders_PhotoInstructions"),
                        Import_Client_Orders_DateCancelled = item.Field<String>("Import_Client_Orders_DateCancelled"),
                        Import_Client_Orders_FormType = item.Field<String>("Import_Client_Orders_FormType"),
                        Import_Client_Orders_OrderType = item.Field<String>("Import_Client_Orders_OrderType"),
                        Import_Client_Orders_Customer = item.Field<String>("Import_Client_Orders_Customer"),
                        Import_Client_Orders_Contractor = item.Field<String>("Import_Client_Orders_Contractor"),
                        Import_Client_Orders_Callingcard = item.Field<String>("Import_Client_Orders_Callingcard"),
                        Import_Client_Orders_Loannumber = item.Field<String>("Import_Client_Orders_Loannumber"),
                        Import_Client_Orders_Mortco = item.Field<String>("Import_Client_Orders_Mortco"),
                        Import_Client_Orders_LoanType = item.Field<String>("Import_Client_Orders_LoanType"),
                        Import_Client_Orders_Department = item.Field<String>("Import_Client_Orders_Department"),
                        Import_Client_Orders_WorkValidation = item.Field<String>("Import_Client_Orders_WorkValidation"),
                        Import_Client_Orders_Version = item.Field<String>("Import_Client_Orders_Version"),
                        Import_Client_Orders_Lotsize = item.Field<String>("Import_Client_Orders_Lotsize"),
                        Import_Client_Orders_Lockbox = item.Field<String>("Import_Client_Orders_Lockbox"),
                        Import_Client_Orders_Latitude = item.Field<String>("Import_Client_Orders_Latitude"),
                        Import_Client_Orders_Longitude = item.Field<String>("Import_Client_Orders_Longitude"),
                        Import_Client_Orders_OrderTypeCode = item.Field<String>("Import_Client_Orders_OrderTypeCode"),
                        Import_Client_Orders_SubContractor = item.Field<String>("Import_Client_Orders_SubContractor"),
                        Import_Client_Orders_Previous = item.Field<Boolean?>("Import_Client_Orders_Previous"),
                        Import_Client_Orders_Import_From_Id = item.Field<Int64?>("Import_Client_Orders_Import_From_Id"),
                        Import_Client_Orders_Client_Id = item.Field<Int64?>("Import_Client_Orders_Client_Id"),
                        Import_Client_Orders_Company_Id = item.Field<Int64?>("Import_Client_Orders_Company_Id"),
                        Import_Client_Orders_UserId = item.Field<Int64?>("Import_Client_Orders_UserId"),
                        Import_Client_Orders_IsActive = item.Field<Boolean?>("Import_Client_Orders_IsActive")


                    }).ToList();

                objDynamic.Add(Import_Client_Orders);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objDynamic;
        }
        public List<Client_Sync_Credential> GetClientSyncCredentialList(Client_Sync_DTO model)
        {
            List<Client_Sync_Credential> client_Sync_CredentialList = null;
            try
            {
                DataSet crDs = GetClientSyncCredential(model);
                if (crDs.Tables.Count > 0)
                {
                    var myEnumerablerd = crDs.Tables[0].AsEnumerable();

                    client_Sync_CredentialList =
                       (from item in myEnumerablerd
                        select new Client_Sync_Credential
                        {
                            WI_ImportFrom = item.Field<Int64?>("WI_ImportFrom"),
                            Auto_IPL_Company_PkeyId = item.Field<Int64?>("Auto_IPL_Company_PkeyId"),
                            WI_LoginName = item.Field<String>("WI_LoginName"),
                            WI_Password = item.Field<String>("WI_Password"),
                            WI_FB_LoginName = item.Field<String>("WI_FB_LoginName"),
                            WI_FB_Password = item.Field<String>("WI_FB_Password"),
                            ApiUrl = item.Field<String>("ApiUrl"),
                        }).ToList();
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return client_Sync_CredentialList;
        }

        private DataSet GetClientSyncCredential(Client_Sync_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_WorkOrder_Import_Credential]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@WI_ImportFrom", 1 + "#bigint#" + model.WI_ImportFrom);
                input_parameters.Add("@UserId", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ds;
        }

        public List<dynamic> AddEntry_Task_Configuration_Master(Client_Sync_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objAddData = new List<dynamic>();
            string insertProcedure = "[AddEntry_Task_Configuration_Master]";
            Import_Client_OrderTypeCode_Master import_Client_OrderTypeCode_Master = new Import_Client_OrderTypeCode_Master();

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {                
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    import_Client_OrderTypeCode_Master.Import_Client_OrderTypeCodes_PkeyID = "0";
                    import_Client_OrderTypeCode_Master.Status = "0";
                    import_Client_OrderTypeCode_Master.ErrorMessage = "Error while Saviving in the Database";
                }
                else
                {
                    import_Client_OrderTypeCode_Master.Import_Client_OrderTypeCodes_PkeyID = Convert.ToString(objData[0]);
                    import_Client_OrderTypeCode_Master.Status = Convert.ToString(objData[1]);
                }
                objAddData.Add(import_Client_OrderTypeCode_Master);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objAddData;
        }

    }
}