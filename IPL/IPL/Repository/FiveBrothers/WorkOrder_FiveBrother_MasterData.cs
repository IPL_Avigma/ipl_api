﻿using Avigma.Repository.Lib;
using IPL.Models.FiveBrothers;
using IPL.Models;
using IPLApp.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using AutoMapper;
using System.Web;

namespace IPL.Repository.data
{
    public class WorkOrder_FiveBrother_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        SecurityHelper securityHelper = new SecurityHelper();
        WorkOrderItem_FiveBrother_Data_MasterData workOrderItem_FiveBrother_Data_MasterData = new WorkOrderItem_FiveBrother_Data_MasterData();

        public List<dynamic> AddWorkOrder_FiveBrotherData(WorkOrder_FiveBrother_MasterDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            List<dynamic> objcltData = new List<dynamic>();
            string insertProcedure = "[CreateUpdate_WorkOrder_FiveBrother_Master]";
            WorkOrder_FiveBrother workOrder_FiveBrother = new WorkOrder_FiveBrother();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@WorkOrder_FiveBrother_PkeyId", 1 + "#bigint#" + model.WorkOrder_FiveBrother_PkeyId);
                input_parameters.Add("@WorkOrder_FiveBrother_Number", 1 + "#bigint#" + model.WorkOrder_FiveBrother_Number);
                input_parameters.Add("@WorkOrder_FiveBrother_Address", 1 + "#varchar#" + model.WorkOrder_FiveBrother_Address);
                input_parameters.Add("@WorkOrder_FiveBrother_City", 1 + "#varchar#" + model.WorkOrder_FiveBrother_City);
                input_parameters.Add("@WorkOrder_FiveBrother_State", 1 + "#varchar#" + model.WorkOrder_FiveBrother_State);
                input_parameters.Add("@WorkOrder_FiveBrother_Zip", 1 + "#varchar#" + model.WorkOrder_FiveBrother_Zip);
                input_parameters.Add("@WorkOrder_FiveBrother_Name", 1 + "#varchar#" + model.WorkOrder_FiveBrother_Name);
                input_parameters.Add("@WorkOrder_FiveBrother_DateOrdered", 1 + "#datetime#" + model.WorkOrder_FiveBrother_DateOrdered);
                input_parameters.Add("@WorkOrder_FiveBrother_EarliestDate", 1 + "#datetime#" + model.WorkOrder_FiveBrother_EarliestDate);
                input_parameters.Add("@WorkOrder_FiveBrother_DueDate", 1 + "#datetime#" + model.WorkOrder_FiveBrother_DueDate);
                input_parameters.Add("@WorkOrder_FiveBrother_Photo_Requirements", 1 + "#varchar#" + model.WorkOrder_FiveBrother_Photo_Requirements);
                input_parameters.Add("@WorkOrder_FiveBrother_OrderInstructions", 1 + "#varchar#" + model.WorkOrder_FiveBrother_OrderInstructions);
                input_parameters.Add("@WorkOrder_FiveBrother_PhotoInstructions", 1 + "#varchar#" + model.WorkOrder_FiveBrother_PhotoInstructions);
                input_parameters.Add("@WorkOrder_FiveBrother_DateCancelled", 1 + "#varchar#" + model.WorkOrder_FiveBrother_DateCancelled);
                input_parameters.Add("@WorkOrder_FiveBrother_FormType", 1 + "#varchar#" + model.WorkOrder_FiveBrother_FormType);
                input_parameters.Add("@WorkOrder_FiveBrother_OrderType", 1 + "#varchar#" + model.WorkOrder_FiveBrother_OrderType);
                input_parameters.Add("@WorkOrder_FiveBrother_Customer", 1 + "#varchar#" + model.WorkOrder_FiveBrother_Customer);
                input_parameters.Add("@WorkOrder_FiveBrother_Contractor", 1 + "#varchar#" + model.WorkOrder_FiveBrother_Contractor);
                input_parameters.Add("@WorkOrder_FiveBrother_Callingcard", 1 + "#varchar#" + model.WorkOrder_FiveBrother_Callingcard);
                input_parameters.Add("@WorkOrder_FiveBrother_Loannumber", 1 + "#varchar#" + model.WorkOrder_FiveBrother_Loannumber);
                input_parameters.Add("@WorkOrder_FiveBrother_Mortco", 1 + "#varchar#" + model.WorkOrder_FiveBrother_Mortco);
                input_parameters.Add("@WorkOrder_FiveBrother_LoanType", 1 + "#varchar#" + model.WorkOrder_FiveBrother_LoanType);
                input_parameters.Add("@WorkOrder_FiveBrother_Department", 1 + "#varchar#" + model.WorkOrder_FiveBrother_Department);
                input_parameters.Add("@WorkOrder_FiveBrother_WorkValidation", 1 + "#varchar#" + model.WorkOrder_FiveBrother_WorkValidation);
                input_parameters.Add("@WorkOrder_FiveBrother_Version", 1 + "#varchar#" + model.WorkOrder_FiveBrother_Version);
                input_parameters.Add("@WorkOrder_FiveBrother_Lotsize", 1 + "#varchar#" + model.WorkOrder_FiveBrother_Lotsize);
                input_parameters.Add("@WorkOrder_FiveBrother_Lockbox", 1 + "#varchar#" + model.WorkOrder_FiveBrother_Lockbox);
                input_parameters.Add("@WorkOrder_FiveBrother_Latitude", 1 + "#varchar#" + model.WorkOrder_FiveBrother_Latitude);
                input_parameters.Add("@WorkOrder_FiveBrother_Longitude", 1 + "#varchar#" + model.WorkOrder_FiveBrother_Longitude);
                input_parameters.Add("@WorkOrder_FiveBrother_OrderTypeCode", 1 + "#varchar#" + model.WorkOrder_FiveBrother_OrderTypeCode);
                input_parameters.Add("@WorkOrder_FiveBrother_SubContractor", 1 + "#varchar#" + model.WorkOrder_FiveBrother_SubContractor);
                input_parameters.Add("@WorkOrder_FiveBrother_Previous", 1 + "#bit#" + model.WorkOrder_FiveBrother_Previous);
                input_parameters.Add("@WorkOrder_FiveBrother_Import_From_Id", 1 + "#bigint#" + model.WorkOrder_FiveBrother_Import_From_Id);
                input_parameters.Add("@WorkOrder_FiveBrother_Client_Id", 1 + "#bigint#" + model.WorkOrder_FiveBrother_Client_Id);
                input_parameters.Add("@WorkOrder_FiveBrother_Company_Id", 1 + "#bigint#" + model.WorkOrder_FiveBrother_Company_Id);
                input_parameters.Add("@WorkOrder_FiveBrother_UserId", 1 + "#bigint#" + model.WorkOrder_FiveBrother_UserId);
                input_parameters.Add("@WorkOrder_FiveBrother_IsActive", 1 + "#bit#" + model.WorkOrder_FiveBrother_IsActive);
                input_parameters.Add("@WorkOrder_FiveBrother_IsDelete", 1 + "#bit#" + model.WorkOrder_FiveBrother_IsDelete);
                input_parameters.Add("@WorkOrder_FiveBrother_IsProcessed", 1 + "#int#" + model.WorkOrder_FiveBrother_IsProcessed);
                input_parameters.Add("@WorkOrder_Import_FkeyId", 1 + "#bigint#" + model.WorkOrder_Import_FkeyId);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@WorkOrder_FiveBrother_PkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    workOrder_FiveBrother.WorkOrder_FiveBrother_PkeyId = "0";
                    workOrder_FiveBrother.Status = "0";
                    workOrder_FiveBrother.ErrorMessage = "Error while Saviving in the Database";

                }
                else
                {
                    workOrder_FiveBrother.WorkOrder_FiveBrother_PkeyId = Convert.ToString(objData[0]);
                    workOrder_FiveBrother.Status = Convert.ToString(objData[1]);


                }
                objcltData.Add(workOrder_FiveBrother);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objcltData;



        }

        private DataSet GetWorkOrder_FiveBrotherMaster(WorkOrder_FiveBrother_MasterDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Get_WorkOrder_FiveBrother_Master]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@WorkOrder_FiveBrother_PkeyId", 1 + "#bigint#" + model.WorkOrder_FiveBrother_PkeyId);
                input_parameters.Add("@WorkOrder_FiveBrother_Company_Id", 1 + "#bigint#" + model.WorkOrder_FiveBrother_Company_Id);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



            return ds;
        }

        public List<dynamic> GetWorkOrder_FiveBrotherDetails(WorkOrder_FiveBrother_MasterDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {


                DataSet ds = GetWorkOrder_FiveBrotherMaster(model);

                var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                List<WorkOrder_FiveBrother_MasterDTO> WorkOrder_FiveBrother =
                   (from item in myEnumerableFeaprd
                    select new WorkOrder_FiveBrother_MasterDTO
                    {
                        WorkOrder_FiveBrother_PkeyId = item.Field<Int64>("WorkOrder_FiveBrother_PkeyId"),
                        WorkOrder_FiveBrother_Number = item.Field<Int64?>("WorkOrder_FiveBrother_Number"),
                        WorkOrder_FiveBrother_Address = item.Field<String>("WorkOrder_FiveBrother_Address"),
                        WorkOrder_FiveBrother_City = item.Field<String>("WorkOrder_FiveBrother_City"),
                        WorkOrder_FiveBrother_State = item.Field<String>("WorkOrder_FiveBrother_State"),
                        WorkOrder_FiveBrother_Zip = item.Field<String>("WorkOrder_FiveBrother_Zip"),
                        WorkOrder_FiveBrother_Name = item.Field<String>("WorkOrder_FiveBrother_Name"),
                        WorkOrder_FiveBrother_DateOrdered = item.Field<DateTime?>("WorkOrder_FiveBrother_DateOrdered"),
                        WorkOrder_FiveBrother_EarliestDate = item.Field<DateTime?>("WorkOrder_FiveBrother_EarliestDate"),
                        WorkOrder_FiveBrother_DueDate = item.Field<DateTime?>("WorkOrder_FiveBrother_DueDate"),
                        WorkOrder_FiveBrother_Photo_Requirements = item.Field<String>("WorkOrder_FiveBrother_Photo_Requirements"),
                        WorkOrder_FiveBrother_OrderInstructions = item.Field<String>("WorkOrder_FiveBrother_OrderInstructions"),
                        WorkOrder_FiveBrother_PhotoInstructions = item.Field<String>("WorkOrder_FiveBrother_PhotoInstructions"),
                        WorkOrder_FiveBrother_DateCancelled = item.Field<String>("WorkOrder_FiveBrother_DateCancelled"),
                        WorkOrder_FiveBrother_FormType = item.Field<String>("WorkOrder_FiveBrother_FormType"),
                        WorkOrder_FiveBrother_OrderType = item.Field<String>("WorkOrder_FiveBrother_OrderType"),
                        WorkOrder_FiveBrother_Customer = item.Field<String>("WorkOrder_FiveBrother_Customer"),
                        WorkOrder_FiveBrother_Contractor = item.Field<String>("WorkOrder_FiveBrother_Contractor"),
                        WorkOrder_FiveBrother_Callingcard = item.Field<String>("WorkOrder_FiveBrother_Callingcard"),
                        WorkOrder_FiveBrother_Loannumber = item.Field<String>("WorkOrder_FiveBrother_Loannumber"),
                        WorkOrder_FiveBrother_Mortco = item.Field<String>("WorkOrder_FiveBrother_Mortco"),
                        WorkOrder_FiveBrother_LoanType = item.Field<String>("WorkOrder_FiveBrother_LoanType"),
                        WorkOrder_FiveBrother_Department = item.Field<String>("WorkOrder_FiveBrother_Department"),
                        WorkOrder_FiveBrother_WorkValidation = item.Field<String>("WorkOrder_FiveBrother_WorkValidation"),
                        WorkOrder_FiveBrother_Version = item.Field<String>("WorkOrder_FiveBrother_Version"),
                        WorkOrder_FiveBrother_Lotsize = item.Field<String>("WorkOrder_FiveBrother_Lotsize"),
                        WorkOrder_FiveBrother_Lockbox = item.Field<String>("WorkOrder_FiveBrother_Lockbox"),
                        WorkOrder_FiveBrother_Latitude = item.Field<String>("WorkOrder_FiveBrother_Latitude"),
                        WorkOrder_FiveBrother_Longitude = item.Field<String>("WorkOrder_FiveBrother_Longitude"),
                        WorkOrder_FiveBrother_OrderTypeCode = item.Field<String>("WorkOrder_FiveBrother_OrderTypeCode"),
                        WorkOrder_FiveBrother_SubContractor = item.Field<String>("WorkOrder_FiveBrother_SubContractor"),
                        WorkOrder_FiveBrother_Previous = item.Field<Boolean?>("WorkOrder_FiveBrother_Previous"),
                        WorkOrder_FiveBrother_Import_From_Id = item.Field<Int64?>("WorkOrder_FiveBrother_Import_From_Id"),
                        WorkOrder_FiveBrother_Client_Id = item.Field<Int64?>("WorkOrder_FiveBrother_Client_Id"),
                        WorkOrder_FiveBrother_Company_Id = item.Field<Int64?>("WorkOrder_FiveBrother_Company_Id"),
                        WorkOrder_FiveBrother_UserId = item.Field<Int64?>("WorkOrder_FiveBrother_UserId"),
                        WorkOrder_FiveBrother_IsActive = item.Field<Boolean?>("WorkOrder_FiveBrother_IsActive"),


                    }).ToList();

                objDynamic.Add(WorkOrder_FiveBrother);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
        public List<dynamic> AddOrders(FBAPI_RequestDTO model)
        {
            List<dynamic> objDataModel = new List<dynamic>();
            List<dynamic> objDataModelRet = new List<dynamic>();
            model.ConPassword = securityHelper.GetMD5Hash(model.ConPassword);
            ScrapperResponseDTO scrapperResponseDTO = new ScrapperResponseDTO();
            string URL = model.ApiUrl.Trim() + "orders" + "?contractor=" + model.ConUserName + "&key=" + model.ConPassword;
           
            try
            {
                byte[] credentials = UTF8Encoding.UTF8.GetBytes(model.AuthUserName + ":" + model.AuthPassword);
                System.Net.Http.HttpClientHandler handler = new System.Net.Http.HttpClientHandler();// Added this condition to disbale SSL Validation 
                handler.ServerCertificateCustomValidationCallback += (sender, cert, chain, sslPolicyErrors) => true; // Added this condition to disbale SSL Validation 
                System.Net.Http.HttpClient client = new System.Net.Http.HttpClient(handler);

                client.BaseAddress = new System.Uri(URL);

                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(credentials));

                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage messge = client.GetAsync(URL).Result;
                string statuscode = messge.StatusCode.ToString();
                if (messge.IsSuccessStatusCode)
                {
                    string result = messge.Content.ReadAsStringAsync().Result;
                    List<OrdersJsonDTO> ordersJsonDTO = JsonConvert.DeserializeObject<List<OrdersJsonDTO>>(result);
                    if (ordersJsonDTO != null && ordersJsonDTO.Count > 0)
                    {
                        scrapperResponseDTO.Responseval = "1";
                        foreach (var orders in ordersJsonDTO)
                        {
                            WorkOrder_FiveBrother_MasterDTO workOrder_FiveBrother_MasterDTO = new WorkOrder_FiveBrother_MasterDTO();
                            workOrder_FiveBrother_MasterDTO.WorkOrder_FiveBrother_Number = orders.ordernumber;
                            workOrder_FiveBrother_MasterDTO.WorkOrder_FiveBrother_Address = orders.address;
                            workOrder_FiveBrother_MasterDTO.WorkOrder_FiveBrother_City = orders.city;
                            workOrder_FiveBrother_MasterDTO.WorkOrder_FiveBrother_State = orders.state;
                            workOrder_FiveBrother_MasterDTO.WorkOrder_FiveBrother_Zip = orders.zip;
                            workOrder_FiveBrother_MasterDTO.WorkOrder_FiveBrother_Name = orders.name;
                            workOrder_FiveBrother_MasterDTO.WorkOrder_FiveBrother_DateOrdered = orders.dateOrdered;
                            workOrder_FiveBrother_MasterDTO.WorkOrder_FiveBrother_EarliestDate = orders.earliestDate;
                            workOrder_FiveBrother_MasterDTO.WorkOrder_FiveBrother_DueDate = orders.dueDate;
                            workOrder_FiveBrother_MasterDTO.WorkOrder_FiveBrother_Photo_Requirements = orders.photoRequirements;
                            workOrder_FiveBrother_MasterDTO.WorkOrder_FiveBrother_OrderInstructions = orders.orderInstructions;
                            workOrder_FiveBrother_MasterDTO.WorkOrder_FiveBrother_PhotoInstructions = orders.photoInstructions;
                            workOrder_FiveBrother_MasterDTO.WorkOrder_FiveBrother_DateCancelled = orders.dateCancelled;
                            workOrder_FiveBrother_MasterDTO.WorkOrder_FiveBrother_FormType = orders.formType;
                            workOrder_FiveBrother_MasterDTO.WorkOrder_FiveBrother_OrderType = orders.orderType;
                            workOrder_FiveBrother_MasterDTO.WorkOrder_FiveBrother_Customer = orders.customer;
                            workOrder_FiveBrother_MasterDTO.WorkOrder_FiveBrother_Contractor = orders.contractor;
                            workOrder_FiveBrother_MasterDTO.WorkOrder_FiveBrother_Callingcard = orders.callingcard;
                            workOrder_FiveBrother_MasterDTO.WorkOrder_FiveBrother_Loannumber = orders.loannumber;
                            workOrder_FiveBrother_MasterDTO.WorkOrder_FiveBrother_Mortco = orders.mortco;
                            workOrder_FiveBrother_MasterDTO.WorkOrder_FiveBrother_LoanType = orders.loanType;
                            workOrder_FiveBrother_MasterDTO.WorkOrder_FiveBrother_Department = orders.department;
                            workOrder_FiveBrother_MasterDTO.WorkOrder_FiveBrother_WorkValidation = orders.workValidation;
                            workOrder_FiveBrother_MasterDTO.WorkOrder_FiveBrother_Version = orders.version;
                            workOrder_FiveBrother_MasterDTO.WorkOrder_FiveBrother_Lotsize = orders.lotsize;
                            workOrder_FiveBrother_MasterDTO.WorkOrder_FiveBrother_Lockbox = orders.lockbox;
                            workOrder_FiveBrother_MasterDTO.WorkOrder_FiveBrother_Latitude = orders.latitude;
                            workOrder_FiveBrother_MasterDTO.WorkOrder_FiveBrother_Longitude = orders.longitude;
                            workOrder_FiveBrother_MasterDTO.WorkOrder_FiveBrother_OrderTypeCode = orders.orderTypeCode;
                            workOrder_FiveBrother_MasterDTO.WorkOrder_FiveBrother_SubContractor = orders.subcontractor;
                            workOrder_FiveBrother_MasterDTO.WorkOrder_FiveBrother_Previous = orders.previous;
                            workOrder_FiveBrother_MasterDTO.UserID = model.UserID;
                            workOrder_FiveBrother_MasterDTO.WorkOrder_Import_FkeyId = model.WI_Pkey_ID;
                            workOrder_FiveBrother_MasterDTO.WorkOrder_FiveBrother_IsActive = true;
                            workOrder_FiveBrother_MasterDTO.WorkOrder_FiveBrother_IsDelete = false;
                            workOrder_FiveBrother_MasterDTO.WorkOrder_FiveBrother_IsProcessed = 0;
                            workOrder_FiveBrother_MasterDTO.Type = 1;
                            var objData = AddWorkOrder_FiveBrotherData(workOrder_FiveBrother_MasterDTO);
                            var ObjItemData = workOrderItem_FiveBrother_Data_MasterData.AddOrderItems(Convert.ToInt64(objData[0].WorkOrder_FiveBrother_PkeyId), workOrder_FiveBrother_MasterDTO.WorkOrder_FiveBrother_Number, model);
                            objDataModel.Add(objData);
                        }
                    }
                    else
                    {
                        scrapperResponseDTO.Responseval = "4";
                        log.logDebugMessage("<-------------------No Five Brothers Open Work orders Found----------------->");
                        log.logDebugMessage("<-------------------"+ messge.IsSuccessStatusCode + "----------------->");
                        log.logDebugMessage("<-------------------" + messge.Content.ReadAsStringAsync().Result + "----------------->");
                    }

                }
                else
                {
                    scrapperResponseDTO.Responseval = "3";
                }
                string result1 = messge.Content.ReadAsStringAsync().Result;
                objDataModelRet.Add(objDataModel);
                objDataModelRet.Add(scrapperResponseDTO);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.InnerException.InnerException.Message);
                scrapperResponseDTO.Responseval = "3";
                objDataModelRet.Add(objDataModel);
                objDataModelRet.Add(scrapperResponseDTO);
            }
            return objDataModelRet;
        }
    }
}