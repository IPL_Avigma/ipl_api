﻿using Avigma.Repository.Lib;
using IPL.Repository.Account;
using IPL.Repository.Account.Reports_Templates;
using NReco.PdfGenerator;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace IPL.Models.Account
{
    public class Acc_Account_Payable_Receivable_Data
    {
        CommonTemplate template = new CommonTemplate();
        Account_Payable_Receivable_Reports_Templates account_Payable_template = new Account_Payable_Receivable_Reports_Templates();
        ReportsPDFGenerater PDF = new ReportsPDFGenerater();
        Log log = new Log();
        Acc_Ledger_Header_Data acc_Ledger_MasterData = new Acc_Ledger_Header_Data();
        public async Task<Custom_Response> GeneratePDFStringAccountPayableAgingSummary(Acc_Report_Filter filter)
        {
            Custom_Response custom = new Custom_Response();
            CultureInfo us = CultureInfo.GetCultureInfo("en-US");
            us = CultureInfo.CreateSpecificCulture("en-US");
            us.NumberFormat.CurrencyNegativePattern = 1;
            Thread.CurrentThread.CurrentCulture = us;
            Thread.CurrentThread.CurrentUICulture = us;
            try
            {
                StringBuilder html = new StringBuilder();
                html.Append(template.HtmlHeader.Replace("#ReportsTitle", "Account Payable Ageing Summary"));
                var acc_account_Payable = AccountPayableAgingSummaryReports(filter);
                if (acc_account_Payable != null && acc_account_Payable.Result != null)
                {
                    custom = acc_account_Payable.Result;
                    Acc_Account_Payable_Receivable_DTO acc_Account_ = new Acc_Account_Payable_Receivable_DTO();
                    acc_Account_ = custom.Data;
                    html.Append(account_Payable_template.AccountPayableReceivableCurrentTotalTitle);
                    StringBuilder Payable = new StringBuilder();
                    string PayableTable = "<table style='width:100%;border-bottom: 1px solid #6b6c72;'>";
                    foreach (var item in acc_Account_?.AccountPayableReceivable)
                    {
                        PayableTable += "<tr style='border-bottom: 0.2px solid #6b6c72'><td style='width: 70%;text-align: left;'>" + item.AccountName + "</td><td style='text-align: right;'>"+ string.Format(us, "{0:C}", Math.Round(item.Amount,2))+"</td><td style='text-align: right;'>" + string.Format(us, "{0:C}", Math.Round(item.Amount,2))+"</td></tr>";
                    }
                    PayableTable += "</table>";
                    string Total = "<table style='width:100%;border-bottom: 1px solid #6b6c72;'><tr><td style='width:70%;text-align: left;'><b> TOTAL </b></td><td style='text-align: right;'><b>" + string.Format(us, "{0:C}", Math.Round(acc_Account_.Total,2))+"</b></td><td style='text-align: right;'><b>" + string.Format(us, "{0:C}", Math.Round(acc_Account_.Total,2))+"</b></td></tr></table>";
                    Payable.Append(PayableTable + Total);
                   
                    html.Append(Payable.ToString());
                }
                html.Append(template.HtmlFooter);
                byte[] pdffile = PDF.GenerateRuntimePDF(html.ToString());
                custom.Data = pdffile;
                custom.HttpStatusCode = System.Net.HttpStatusCode.OK;
                return custom;
            }
            catch (Exception ex)
            {
                custom.Message = ex.Message;
                custom.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return custom;
            }
        }
        public async Task<Custom_Response> GeneratePDFStringAccountReceivableAgingSummary( Acc_Report_Filter filter)
        {
            Custom_Response custom = new Custom_Response();
            CultureInfo us = CultureInfo.GetCultureInfo("en-US");
            us = CultureInfo.CreateSpecificCulture("en-US");
            us.NumberFormat.CurrencyNegativePattern = 1;
            Thread.CurrentThread.CurrentCulture = us;
            Thread.CurrentThread.CurrentUICulture = us;
            try
            {
                StringBuilder html = new StringBuilder();
                html.Append(template.HtmlHeader.Replace("#ReportsTitle", "Account Receivable Ageing Summary"));
                var acc_account_Receivable = AccountReceivableAgingSummaryReports(filter);
                if (acc_account_Receivable != null && acc_account_Receivable.Result != null)
                {
                    custom = acc_account_Receivable.Result;
                    Acc_Account_Payable_Receivable_DTO acc_Account_ = new Acc_Account_Payable_Receivable_DTO();
                    acc_Account_ = custom.Data;
                    html.Append(account_Payable_template.AccountPayableReceivableCurrentTotalTitle);
                    StringBuilder Payable = new StringBuilder();
                    string PayableTable = "<table style='width:100%;border-bottom: 1px solid #6b6c72;'>";
                    foreach (var item in acc_Account_?.AccountPayableReceivable)
                    {
                        PayableTable += "<tr style='border-bottom: 0.2px solid #6b6c72'><td style='width: 70%;text-align: left;'>" + item.AccountName + "</td><td style='text-align: right;'>" + string.Format(us, "{0:C}", Math.Round(item.Amount,2)) + "</td><td style='text-align: right;'>" + string.Format(us, "{0:C}", Math.Round(item.Amount,2)) + "</td></tr>";
                    }
                    PayableTable += "</table>";
                    string Total = "<table style='width:100%;border-bottom: 1px solid #6b6c72;'><tr><td style='width:70%;text-align: left;'><b> TOTAL </b></td><td style='text-align: right;'><b>" +string.Format(us, "{0:C}", Math.Round(acc_Account_.Total,2)) + "</b></td><td style='text-align: right;'><b>" + string.Format(us, "{0:C}", Math.Round(acc_Account_.Total,2))+ "</b></td></tr></table>";
                    Payable.Append(PayableTable + Total);

                    html.Append(Payable.ToString());
                }
                html.Append(template.HtmlFooter);
                byte[] pdffile = PDF.GenerateRuntimePDF(html.ToString());
                custom.Data = pdffile;
                custom.HttpStatusCode = System.Net.HttpStatusCode.OK;
                return custom;
            }
            catch (Exception ex)
            {
                custom.Message = ex.Message;
                custom.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return custom;
            }
        }
       

        public async Task<Custom_Response> AccountPayableAgingSummaryReports( Acc_Report_Filter filter)
        {
            Custom_Response custom = new Custom_Response();
            Acc_Account_Payable_Receivable_DTO acc_Account_Payable = new Acc_Account_Payable_Receivable_DTO();
            try
            {
                Acc_Account_MasterData acc_Account_MasterData = new Acc_Account_MasterData();
                Acc_Ledger_Headers_DTO acc_Ledger_DTO = new Acc_Ledger_Headers_DTO();
                acc_Ledger_DTO.Type = 1;
                acc_Ledger_DTO.UserID = filter.UserId;
                IEnumerable<Acc_Ledger_Headers_DTO> acc_Ledger_Headers_DTOs = acc_Ledger_MasterData.GetLedgerEntries(acc_Ledger_DTO);
                List<AccountPayableReceivable> List = new List<AccountPayableReceivable>();
                var AccountPayabale = acc_Ledger_Headers_DTOs.Where(x => x.Acc_Account_Type == (int)AccountType.AccountPayable).GroupBy(x => new { x.Acc_AccountID, x.Acc_Account_Type, x.Acc_Account_Name, x.Acc_Account_Code, x.LedgH_CreatedOn }).Select(Line => new AccountPayableReceivable
                {
                    AccountId = Line.Key.Acc_AccountID,
                    AccountType = Line.Key.Acc_Account_Type,
                    AccountCode = Line.Key.Acc_Account_Code,
                    AccountName = Line.Key.Acc_Account_Name,
                    //Amount = Line.Sum(s => s.LedgE_DrCr == (int)DrOrCrSide.Dr ? (s.Acc_DrOrCr_Side == (int)DrOrCrSide.Dr ? s.DebitAmount : (s.DebitAmount)) : (s.Acc_DrOrCr_Side == (int)DrOrCrSide.Dr ? (s.CreditAmount) : s.CreditAmount)),
                    Amount = Line.Sum(s => s.LedgE_DrCr == (int)DrOrCrSide.Dr ? (s.Acc_DrOrCr_Side == (int)DrOrCrSide.Dr ? s.DebitAmount : -(s.DebitAmount)) : (s.Acc_DrOrCr_Side == (int)DrOrCrSide.Dr ? -(s.CreditAmount) : s.CreditAmount)),
                    CreatedDate = Line.Key.LedgH_CreatedOn
                });
                if (filter.StartDate != null && filter.EndDate != null)
                    AccountPayabale = AccountPayabale.Where(x => x.CreatedDate >= filter.StartDate && x.CreatedDate <= filter.EndDate);
                List = AccountPayabale.ToList();
                acc_Account_Payable.Total = List!=null? List.Sum(x => x.Amount):0;
                acc_Account_Payable.AccountPayableReceivable = List;
                custom.Data = acc_Account_Payable;
            }
            catch (Exception ex)
            {
                custom.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                custom.Message = ex.Message;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return custom;
        }
        public async Task<Custom_Response> AccountReceivableAgingSummaryReports( Acc_Report_Filter filter)
        {
            Custom_Response custom = new Custom_Response();
            Acc_Account_Payable_Receivable_DTO acc_Account_Receivable = new Acc_Account_Payable_Receivable_DTO();
            try
            {
                Acc_Account_MasterData acc_Account_MasterData = new Acc_Account_MasterData();
                Acc_Ledger_Headers_DTO acc_Ledger_DTO = new Acc_Ledger_Headers_DTO();
                acc_Ledger_DTO.Type = 1;
                acc_Ledger_DTO.UserID = filter.UserId;
                IEnumerable<Acc_Ledger_Headers_DTO> acc_Ledger_Headers_DTOs = acc_Ledger_MasterData.GetLedgerEntries(acc_Ledger_DTO);
                List<AccountPayableReceivable> List = new List<AccountPayableReceivable>();
                var AccountPayabale = acc_Ledger_Headers_DTOs.Where(x => x.Acc_Account_Type == (int)AccountType.AccountReceivable).GroupBy(x => new { x.Acc_AccountID, x.Acc_Account_Type, x.Acc_Account_Name, x.Acc_Account_Code, x.LedgH_CreatedOn }).Select(Line => new AccountPayableReceivable
                {
                    AccountId = Line.Key.Acc_AccountID,
                    AccountType = Line.Key.Acc_Account_Type,
                    AccountCode = Line.Key.Acc_Account_Code,
                    AccountName = Line.Key.Acc_Account_Name,
                    //Amount = Line.Sum(s => s.LedgE_DrCr == (int)DrOrCrSide.Dr ? (s.Acc_DrOrCr_Side == (int)DrOrCrSide.Dr ? s.DebitAmount : (s.DebitAmount)) : (s.Acc_DrOrCr_Side == (int)DrOrCrSide.Dr ? (s.CreditAmount) : s.CreditAmount)),
                    Amount = Line.Sum(s => s.LedgE_DrCr == (int)DrOrCrSide.Dr ? (s.Acc_DrOrCr_Side == (int)DrOrCrSide.Dr ? s.DebitAmount : -(s.DebitAmount)) : (s.Acc_DrOrCr_Side == (int)DrOrCrSide.Dr ? -(s.CreditAmount) : s.CreditAmount)),
                    CreatedDate = Line.Key.LedgH_CreatedOn
                });
                if (filter.StartDate != null && filter.EndDate != null)
                    AccountPayabale = AccountPayabale.Where(x => x.CreatedDate >= filter.StartDate && x.CreatedDate <= filter.EndDate);
                List = AccountPayabale.ToList();
                acc_Account_Receivable.Total = List != null ? List.Sum(x => x.Amount) : 0;
                acc_Account_Receivable.AccountPayableReceivable = List;
                custom.Data = acc_Account_Receivable;
            }
            catch (Exception ex)
            {
                custom.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                custom.Message = ex.Message;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return custom;
        }

    }
}