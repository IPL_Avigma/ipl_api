﻿using Avigma.Repository.Lib;
using IPL.Models.Account;
using IPL.Repository.Account.Reports_Templates;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace IPL.Repository.Account
{
    public class Acc_ProfitAndLossDetails_Data
    {
        Log log = new Log();
        Acc_Invoice_Data acc_Invoice_Data = new Acc_Invoice_Data();
        CommonTemplate template = new CommonTemplate();
        Profit_And_Loss_Details_Reports_Templates Profit_And_Loss_Details_Reports_Templates = new Profit_And_Loss_Details_Reports_Templates();
        ReportsPDFGenerater PDF = new ReportsPDFGenerater();
        Acc_Bill_Data acc_Bill_Data = new Acc_Bill_Data();
        Acc_Ledger_Header_Data acc_Ledger_MasterData = new Acc_Ledger_Header_Data();
        public async Task<Custom_Response> ProfitAndLossDetailsReports(Acc_Report_Filter filter)
        {
            Custom_Response custom = new Custom_Response();
            ProfitAndLossDetails_DTO profitAndLossDetails_DTO = new ProfitAndLossDetails_DTO();
            OrdinaryIncomeExpenses ordinaryIncomeExpenses = new OrdinaryIncomeExpenses();
            ProfitLossDetailsIncome profitLossDetailsIncome = new ProfitLossDetailsIncome();

            OtherIncomeExpenses otherIncomeExpenses = new OtherIncomeExpenses();
            try
            {

                List<DataList> InvoiceBillList = new List<DataList>();
                Acc_Invoice_DTO acc_Invoice_DTO = new Acc_Invoice_DTO();
                acc_Invoice_DTO.Type = 3;
                acc_Invoice_DTO.UserID = filter.UserId;
                if (filter.StartDate != null && filter.EndDate != null)
                    acc_Invoice_DTO.WhereClause += "AND inv.Invoice_CreatedOn >=CONVERT(nvarchar, '" + filter.StartDate.Value.ToString("yyyy/MM/dd") + "') and  inv.Invoice_CreatedOn <= CONVERT(nvarchar, '" + filter.EndDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59).ToString("yyyy/MM/dd h:mm tt") + "')";
                var getInvoice = acc_Invoice_Data.GetInvoiceMaster(acc_Invoice_DTO);
                if (getInvoice != null)
                {

                    foreach (var item in getInvoice.Where(x => x.Invoice_Total > 0))
                    {
                        InvoiceBillList.Add(new DataList()
                        {
                            Amount = item.Invoice_Total.Value,
                            CreatedDate = item.Invoice_Date != null ? item.Invoice_Date.Value : DateTime.Now,
                            TransactionType = "Invoice",
                            NO = item.Invoice_Number,
                            Memo_Description = item.Invoice_Message,
                            Name = item.Invoice_Custome_Name,

                        });
                    }
                }
                
                Acc_Bill_DTO acc_Bill_DTO = new Acc_Bill_DTO();
                acc_Bill_DTO.Type = 3;
                acc_Bill_DTO.UserID =filter.UserId;
                if (filter.StartDate != null && filter.EndDate != null)
                    acc_Bill_DTO.WhereClause = "AND bill.Bill_CreatedOn >=CONVERT(nvarchar, '" + filter.StartDate.Value.ToString("yyyy/MM/dd") + "') and bill.Bill_CreatedOn <= CONVERT(nvarchar, '" + filter.EndDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59).ToString("yyyy/MM/dd h:mm tt") + "')";
                var getBill = acc_Bill_Data.GetBillMaster(acc_Bill_DTO);
                if (getBill != null)
                {
                    foreach (var item in getBill.Where(x => x.Bill_Total > 0))
                    {
                        DataList dto = new DataList()
                        {
                            Amount = item.Bill_Total.Value,
                            CreatedDate = item.Bill_Date != null ? item.Bill_Date.Value : DateTime.Now,
                            TransactionType = "Bill",
                            NO = item.Bill_Number,
                            Memo_Description = item.Bill_Message,
                            Name = item.Bill_Vendor_Name,
                        };
                        InvoiceBillList.Add(dto);
                    }
                }
                decimal Balance = 0;
                foreach (var item in InvoiceBillList.ToList())
                {
                    item.Balance = Balance + item.Amount;
                    Balance += item.Amount;
                }
                profitLossDetailsIncome.InvocieBillDataList = InvoiceBillList;
                
               
                Acc_Account_MasterData acc_Account_MasterData = new Acc_Account_MasterData();
                Acc_Ledger_Headers_DTO acc_Ledger_DTO = new Acc_Ledger_Headers_DTO();
                acc_Ledger_DTO.Type = 3;
                acc_Ledger_DTO.UserID = filter.UserId;
                if (filter.StartDate != null && filter.EndDate != null)
                    acc_Ledger_DTO.WhereClause += "AND LedgH_CreatedOn >=CONVERT(nvarchar, '" + filter.StartDate.Value.ToString("yyyy/MM/dd") + "') and LedgH_CreatedOn <= CONVERT(nvarchar, '" + filter.EndDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59).ToString("yyyy/MM/dd h:mm tt") + "')";
                IEnumerable<Acc_Ledger_Headers_DTO> acc_Ledger_Headers_DTOs = acc_Ledger_MasterData.GetLedgerEntries(acc_Ledger_DTO);
                var ExpensesDataList = acc_Ledger_Headers_DTOs.Where(x => x.Acc_Account_Type == (int)AccountType.Expense || x.Acc_Account_Type == (int)AccountType.OtherExpense).GroupBy(x => new { x.Acc_AccountID, x.Acc_Account_Type, x.Acc_Account_Name, x.Acc_Account_Code, x.LedgH_CreatedOn, x.LedgH_pkeyId, x.LedgH_Decription, x.LedgE_DrCr, x.Acc_Detail_Type }).Select(Line => new DataList
                {
                    TransactionType = "Journal Entry",
                    ExpensesTypeName = Line.Key.Acc_Detail_Type,
                    NO = Line.Key.LedgH_pkeyId.ToString(),
                    Memo_Description = Line.Key.LedgH_Decription,
                    Name = Line.Key.Acc_Account_Name,
                    Amount = Line.Sum(s => s.LedgE_DrCr == (int)DrOrCrSide.Dr ? s.DebitAmount : 0),
                    Balance = Line.Sum(s => s.LedgE_DrCr == (int)DrOrCrSide.Dr ? s.DebitAmount : 0),
                    CreatedDate = Line.Key.LedgH_CreatedOn,
                    DrOrCrSide = Line.Key.LedgE_DrCr,
                }).Where(x => x.DrOrCrSide == (int)DrOrCrSide.Dr);

                var currentincome = acc_Ledger_Headers_DTOs.Where(x => (x.Acc_Account_Type == (int)
                AccountType.Income)).GroupBy(x => new { x.Acc_AccountID, x.Acc_Account_Type, x.Acc_Account_Name, x.Acc_Account_Code, x.LedgH_CreatedOn, x.LedgH_pkeyId, x.LedgH_Decription, x.LedgE_DrCr, x.Acc_Detail_Type }).Select(Line => new DataList
                {
                    TransactionType = "Journal Entry",
                    ExpensesTypeName = Line.Key.Acc_Detail_Type,
                    NO = Line.Key.Acc_AccountID.ToString(),
                    Memo_Description = Line.Key.LedgH_Decription,
                    Name = Line.Key.Acc_Account_Name,
                    Amount = Line.Sum(s => s.LedgE_DrCr == (int)DrOrCrSide.Dr ? s.DebitAmount : 0),
                    Balance = Line.Sum(s => s.LedgE_DrCr == (int)DrOrCrSide.Dr ? s.DebitAmount : 0),
                    CreatedDate = Line.Key.LedgH_CreatedOn,
                    DrOrCrSide = Line.Key.LedgE_DrCr,
                });

                currentincome = currentincome.Where(x => x.Amount > 0).ToList();
                profitLossDetailsIncome.Income = currentincome.ToList();
                profitLossDetailsIncome.TotalforSales = (InvoiceBillList != null ? InvoiceBillList.Sum(x => x.Amount) : 0);
                profitLossDetailsIncome.TotalforIncome = (InvoiceBillList != null ? InvoiceBillList.Sum(x => x.Amount) : 0)+ (currentincome!=null? currentincome.Sum(x=>x.Amount):0);
                profitLossDetailsIncome.ExpensesDataList = ExpensesDataList != null ? ExpensesDataList.ToList() : null;
                profitLossDetailsIncome.TotalforExpenses = profitLossDetailsIncome.ExpensesDataList != null ? profitLossDetailsIncome.ExpensesDataList.Sum(x => x.Amount) : 0;
                ordinaryIncomeExpenses.ProfitLossDetailsIncome = profitLossDetailsIncome;
                ordinaryIncomeExpenses.NetOrdinaryIncome = profitLossDetailsIncome.TotalforIncome - profitLossDetailsIncome.TotalforExpenses;
                profitAndLossDetails_DTO.OrdinaryIncomeExpenses = ordinaryIncomeExpenses;

                var IncomeDataList = acc_Ledger_Headers_DTOs.Where(x => x.Acc_Account_Type == (int)AccountType.OtherIncome).GroupBy(x => new { x.Acc_AccountID, x.Acc_Account_Type, x.Acc_Account_Name, x.Acc_Account_Code, x.LedgH_CreatedOn, x.LedgH_pkeyId, x.LedgH_Decription, x.LedgE_DrCr, x.Acc_Detail_Type }).Select(Line => new DataList
                {
                    TransactionType = "Journal Entry",
                    ExpensesTypeName = Line.Key.Acc_Detail_Type,
                    NO = Line.Key.LedgH_pkeyId.ToString(),
                    Memo_Description = Line.Key.LedgH_Decription,
                    Name = Line.Key.Acc_Account_Name,
                    Amount = Line.Sum(s => s.LedgE_DrCr == (int)DrOrCrSide.Dr ? s.DebitAmount : 0),
                    Balance = Line.Sum(s => s.LedgE_DrCr == (int)DrOrCrSide.Dr ? s.DebitAmount : 0),
                    CreatedDate = Line.Key.LedgH_CreatedOn,
                    DrOrCrSide = Line.Key.LedgE_DrCr,
                }).Where(x => x.DrOrCrSide == (int)DrOrCrSide.Dr);
                otherIncomeExpenses.OtherIncome = IncomeDataList.ToList();
                otherIncomeExpenses.TotalforOtherIncome = IncomeDataList != null ? IncomeDataList.Sum(x => x.Amount) : 0;
                profitAndLossDetails_DTO.OtherIncomeExpenses = otherIncomeExpenses;
                profitAndLossDetails_DTO.NetIncome = ordinaryIncomeExpenses.NetOrdinaryIncome - otherIncomeExpenses.TotalforOtherIncome;
                custom.Data = profitAndLossDetails_DTO;
                custom.HttpStatusCode = System.Net.HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                custom.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                custom.Message = ex.Message;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return custom;
        }
        public async Task<Custom_Response> GeneratePDFStringProfitLossDetails(Acc_Report_Filter filter)
        {
            Custom_Response custom = new Custom_Response();
            CultureInfo us = CultureInfo.GetCultureInfo("en-US");
            us = CultureInfo.CreateSpecificCulture("en-US");
            us.NumberFormat.CurrencyNegativePattern = 1;
            Thread.CurrentThread.CurrentCulture = us;
            Thread.CurrentThread.CurrentUICulture = us;
            try
            {
                StringBuilder html = new StringBuilder();
                html.Append(template.HtmlHeader.Replace("#ReportsTitle", "Profit And Loss Details"));
                var acc_ProfitAndLossDetails_DTO = ProfitAndLossDetailsReports(filter);
                if (acc_ProfitAndLossDetails_DTO != null && acc_ProfitAndLossDetails_DTO.Result != null)
                {
                    custom = acc_ProfitAndLossDetails_DTO.Result;
                    ProfitAndLossDetails_DTO acc_ProfitAndLoss_Details = new ProfitAndLossDetails_DTO();
                    acc_ProfitAndLoss_Details = custom.Data;
                    html.Append(Profit_And_Loss_Details_Reports_Templates.Header);
                    StringBuilder ProfitAndLoss = new StringBuilder();
                    string OrdinaryIncomeExpenses = "<div style='border-top: 1px solid #6e6b6b;padding:15px 10px; border-bottom: 1px solid #6e6b6b; font-weight: 600;width:100%;padding-bottom: 25px;'><div style='width:80%;float:left;text-align: left;'><span style ='float: left; margin-top: 15px;' ><i style='width: 0;border-left: 6px solid transparent;border-right: 6px solid transparent;border-top: 6px solid #A4A4A4;' ></i ></span> Ordinary Income/Expenses</div><div style='width:20%;float:left;text-align: left;'> " + string.Format(us, "{0:C}", Math.Round((decimal)acc_ProfitAndLoss_Details.OrdinaryIncomeExpenses.NetOrdinaryIncome,2)) + " </div></div>";
                    string Income = "<div style=';border-top: 1px solid #6e6b6b;border-bottom: 1px solid #6e6b6b; font-weight: 600;width:100%;padding-bottom: 25px;'><div style='padding-left:10px;width:80%;float:left;text-align: left;'><span style ='float: left; margin-top: 15px;margin-right: 10px' ><i style = 'width: 0;border-left: 6px solid transparent;border-right: 6px solid transparent;border-top: 6px solid #A4A4A4;' ></i ></span> Income</div><div style='width:20%;float:left;text-align: left;'> " +
                          string.Format(us, "{0:C}", Math.Round((decimal)acc_ProfitAndLoss_Details?.OrdinaryIncomeExpenses?.ProfitLossDetailsIncome?.TotalforIncome,2)) + " </div></div>";
                    string Sales = "<div style=';border-top: 1px solid #6e6b6b;padding:15px 10px; border-bottom: 1px solid #6e6b6b; font-weight: 600;width:100%;padding-bottom: 25px;'><div style='padding-left:13px;width:80%;float:left;text-align: left;'><span style ='float: left; margin-top: 15px;margin-right: 15px' ><i style = 'width: 0;border-left: 6px solid transparent;border-right: 6px solid transparent;border-top: 6px solid #A4A4A4;' ></i ></span> Sales</div><div style='width:20%;float:left;text-align: left;'> " +

                      string.Format(us, "{0:C}", Math.Round((decimal) acc_ProfitAndLoss_Details?.OrdinaryIncomeExpenses?.ProfitLossDetailsIncome?.TotalforIncome,2)) + " </div></div>";
                    ProfitAndLoss.Append(OrdinaryIncomeExpenses + Income +Sales);
                    string SalesList = "<table>";
                    foreach (var item in acc_ProfitAndLoss_Details?.OrdinaryIncomeExpenses?.ProfitLossDetailsIncome?.InvocieBillDataList)
                    {
                        SalesList += "<tr><td style='width: 10%'>"+item.CreatedDate.ToString("dd/MM/yyyy") +"</td><td style='width: 15%'>"+ item.TransactionType+"</td><td style='width: 10%'>"+ item.NO+"</td><td style='width: 17%'>"+item.Name+"</td><td style= 'width:15%'>"+ item.Memo_Description+"</td><td style='width: 8%'>" + string.Format(us, "{0:C}", Math.Round((decimal)item.Amount,2))+"</td><td style='width:10%; text-align: right'>" + string.Format(us, "{0:C}", Math.Round((decimal)item.Balance,2))+"</td></tr>";
                    }
                    SalesList= SalesList + "</table>";
                    string TotalforSales = "<div style=';border-top: 1px solid #6e6b6b;padding:15px 10px; border-bottom: 1px solid #6e6b6b; font-weight: 600;width:100%;padding-bottom: 25px;'><div style='padding-left:10px;width:80%;float:left;text-align: left;'><b> Total for Sales</b></div><div style='width:20%;float:left;text-align: left;'> " + string.Format(us, "{0:C}", Math.Round((decimal)acc_ProfitAndLoss_Details?.OrdinaryIncomeExpenses?.ProfitLossDetailsIncome?.TotalforIncome,2)) + " </div></div>";
                    string TotalforIncome = "<div style=';border-top: 1px solid #6e6b6b;padding:15px 10px; border-bottom: 1px solid #6e6b6b; font-weight: 600;width:100%;padding-bottom: 25px;'><div style='padding-left:10px;width:80%;float:left;text-align: left;'><b> Total for income</b></div><div style='width:20%;float:left;text-align: left;'> " + string.Format(us, "{0:C}", Math.Round((decimal)acc_ProfitAndLoss_Details?.OrdinaryIncomeExpenses?.ProfitLossDetailsIncome?.TotalforIncome,2)) + " </div></div>";
                    ProfitAndLoss.Append(SalesList+ TotalforSales+ TotalforIncome);

                    string Expenses = "<div style=';border-top: 1px solid #6e6b6b;border-bottom: 1px solid #6e6b6b; font-weight: 600;width:100%;padding-bottom: 25px;'><div style='padding-left:5px;width:80%;float:left;text-align: left;'><span style ='float: left; margin-top: 15px;margin-right: 10px' ><i style='width: 0;border-left: 6px solid transparent;border-right: 6px solid transparent;border-top: 6px solid #A4A4A4;' ></i ></span> Expenses</div><div style='width:20%;float:left;text-align: left;'> " +
                         string.Format(us, "{0:C}", Math.Round((decimal)acc_ProfitAndLoss_Details?.OrdinaryIncomeExpenses?.ProfitLossDetailsIncome?.TotalforExpenses, 2)) + " </div></div>";
                    ProfitAndLoss.Append(Expenses);
                  

                    string ExpenseList = "";
                    foreach (var item in acc_ProfitAndLoss_Details
                        .OrdinaryIncomeExpenses.ProfitLossDetailsIncome
                        .ExpensesDataList)
                    {
                        ExpenseList+= "<div style=';border-top: 1px solid #6e6b6b;border-bottom: 1px solid #6e6b6b; font-weight: 600;width:100%;padding-bottom: 25px;'><div style='padding-left:10px;width:80%;float:left;text-align: left;'><span style ='float: left; margin-top: 15px;margin-right: 10px' ><i style = 'width: 0;border-left: 6px solid transparent;border-right: 6px solid transparent;border-top: 6px solid #A4A4A4;' ></i ></span> "+item.ExpensesTypeName+"</div><div style='width:20%;float:left;text-align: left;'> " +
                          string.Format(us, "{0:C}", Math.Round((decimal)item.Amount, 2)) + " </div></div>";
                        ExpenseList += "<table><tr><td style='width: 10%'>" + item.CreatedDate.ToString("dd/MM/yyyy") + "</td><td style='width: 15%'>" + item.TransactionType + "</td><td style='width: 10%'>" + item.NO + "</td><td style='width: 15%'>" + item.Name + "</td><td style= 'width:15%'>" + item.Memo_Description + "</td><td style='width: 10%'>" + string.Format(us, "{0:C}", Math.Round((decimal)item.Amount, 2))+ "</td><td style='width:10%; text-align: right'>" + string.Format(us, "{0:C}", Math.Round((decimal)item.Balance, 2)) + "</td></tr>";
                        ExpenseList += "<tr><td colspan='5' style='width: 67%;text-align: left;padiing-left:5px'><b>Total for " + item.ExpensesTypeName + "</b></td><td colspan='2' style='text-align: right'><b>" + string.Format(us, "{0:C}", Math.Round((decimal)item.Amount, 2)) + "</b></td></tr></table>";
                    }
                    string TotalforExpenses = "<div style=';border-top: 1px solid #6e6b6b;padding:15px 10px; border-bottom: 1px solid #6e6b6b; font-weight: 600;width:100%;padding-bottom: 25px;'><div style='padding-left:10px;width:80%;float:left;text-align: left;'><b> Total for Expenses</b></div><div style='width:20%;float:left;text-align: left;'> " + string.Format(us, "{0:C}", Math.Round((decimal)acc_ProfitAndLoss_Details?.OrdinaryIncomeExpenses?.ProfitLossDetailsIncome?.TotalforExpenses, 2)) + " </div></div>";
                    string NetOrdinaryIncome = "<div style=';border-top: 1px solid #6e6b6b;padding:15px 10px; border-bottom: 1px solid #6e6b6b; font-weight: 600;width:100%;padding-bottom: 25px;'><div style='padding-left:10px;width:80%;float:left;text-align: left;'><b> Net Ordinary Income</b></div><div style='width:20%;float:left;text-align: left;'> " + string.Format(us, "{0:C}", Math.Round((decimal)acc_ProfitAndLoss_Details?.OrdinaryIncomeExpenses?.NetOrdinaryIncome, 2)) + " </div></div>";

                    string OtherIncomeExpenses = "<div style=';border-top: 1px solid #6e6b6b;border-bottom: 1px solid #6e6b6b; font-weight: 600;width:100%;padding-bottom: 25px;'><div style='padding-left:5px;width:80%;float:left;text-align: left;'><span style ='float: left; margin-top: 15px;margin-right: 10px' ><i style='width: 0;border-left: 6px solid transparent;border-right: 6px solid transparent;border-top: 6px solid #A4A4A4;' ></i ></span> Other Income/Expenses</div><div style='width:20%;float:left;text-align: left;'> " +
                          string.Format(us, "{0:C}", Math.Round((decimal)acc_ProfitAndLoss_Details?.OtherIncomeExpenses?.TotalforOtherIncome, 2)) + " </div></div>";
                    ProfitAndLoss.Append(ExpenseList + TotalforExpenses + NetOrdinaryIncome);

                    string OtherIncomeExpensesList = "";
                    foreach (var item in acc_ProfitAndLoss_Details.OtherIncomeExpenses.OtherIncome
                        )
                    {
                        OtherIncomeExpensesList += "<div style=';border-top: 1px solid #6e6b6b;border-bottom: 1px solid #6e6b6b; font-weight: 600;width:100%;padding-bottom: 25px;'><div style='padding-left:10px;width:80%;float:left;text-align: left;'><span style ='float: left; margin-top: 15px;margin-right: 10px' ><i style = 'width: 0;border-left: 6px solid transparent;border-right: 6px solid transparent;border-top: 6px solid #A4A4A4;' ></i ></span> " + item.ExpensesTypeName + "</div><div style='width:20%;float:left;text-align: left;'> " +
                          string.Format(us, "{0:C}", Math.Round((decimal)item.Amount, 2)) + " </div></div>";
                        OtherIncomeExpensesList += "<table><tr><td style='width: 10%'>" + item.CreatedDate.ToString("dd/MM/yyyy") + "</td><td style='width: 15%'>" + item.TransactionType + "</td><td style='width: 10%'>" + item.NO + "</td><td style='width: 15%'>" + item.Name + "</td><td style= 'width:15%'>" + item.Memo_Description + "</td><td style='width: 10%'>" + string.Format(us, "{0:C}", Math.Round((decimal)item.Amount, 2)) + "</td><td style='width:10%; text-align: right'>" + string.Format(us, "{0:C}", Math.Round((decimal)item.Balance, 2)) + "</td></tr>";
                        OtherIncomeExpensesList += "<tr><td colspan='5' style='width: 67%;text-align: left;padiing-left:5px'><b>Total for " + item.ExpensesTypeName + "</b></td><td colspan='2' style='text-align: right'><b>" + string.Format(us, "{0:C}", Math.Round((decimal)item.Amount, 2)) + "</b></td></tr></table>";
                    }
                    string TotalExpenses = "<div style=';border-top: 1px solid #6e6b6b;padding:15px 10px; border-bottom: 1px solid #6e6b6b; font-weight: 600;width:100%;padding-bottom: 25px;'><div style='padding-left:10px;width:80%;float:left;text-align: left;'><b> Total Expenses</b></div><div style='width:20%;float:left;text-align: left;'> " + string.Format(us, "{0:C}", Math.Round((decimal)acc_ProfitAndLoss_Details?.OtherIncomeExpenses.TotalforOtherIncome, 2)) + " </div></div>";
                    string NetIncome = "<div style=';border-top: 1px solid #6e6b6b;padding:15px 10px; border-bottom: 1px solid #6e6b6b; font-weight: 600;width:100%;padding-bottom: 25px;'><div style='padding-left:10px;width:80%;float:left;text-align: left;'><b> Net Income</b></div><div style='width:20%;float:left;text-align: left;'> " + string.Format(us, "{0:C}", Math.Round((decimal)acc_ProfitAndLoss_Details?.NetIncome, 2)) + " </div></div>";
                    
                    ProfitAndLoss.Append(OtherIncomeExpenses+OtherIncomeExpensesList + TotalExpenses + NetIncome);
                    html.Append(ProfitAndLoss.ToString() + "</div>");
                    html.Append(template.HtmlFooter);
                }
                byte[] pdffile = PDF.GenerateRuntimePDF(html.ToString());
                custom.Data = pdffile;
                custom.HttpStatusCode = System.Net.HttpStatusCode.OK;
                return custom;
            }
            catch (Exception ex)
            {
                custom.Message = ex.Message;
                custom.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return custom;
            }
        }

    }

}