﻿using Avigma.Repository.Lib;
using IPL.Repository.Account;
using IPL.Repository.Account.Reports_Templates;
using NReco.PdfGenerator;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace IPL.Models.Account
{
    public class Acc_BalanceSheet_Data
    {
        Log log = new Log();
        Acc_Ledger_Header_Data acc_Ledger_MasterData = new Acc_Ledger_Header_Data();
        Acc_Invoice_Data acc_Invoice_Data = new Acc_Invoice_Data();
        Acc_Bill_Data acc_Bill_Data = new Acc_Bill_Data();
        CommonTemplate template = new CommonTemplate();
        Balance_Sheet_Reports_Templates balance_Sheet_ = new Balance_Sheet_Reports_Templates();
        ReportsPDFGenerater PDF = new ReportsPDFGenerater();
        public async Task<Custom_Response> GeneratePDFStringBalanceSheet(Acc_Report_Filter filter)
        {
            Custom_Response custom = new Custom_Response();
            CultureInfo us = CultureInfo.GetCultureInfo("en-US");
            us = CultureInfo.CreateSpecificCulture("en-US");
            us.NumberFormat.CurrencyNegativePattern = 1;
            Thread.CurrentThread.CurrentCulture = us;
            Thread.CurrentThread.CurrentUICulture = us;

            try
            {
                StringBuilder html = new StringBuilder();
                html.Append(template.HtmlHeader.Replace("#ReportsTitle", "Balance Sheet"));

                var acc_BalanceSheet_DTO = BalanceSheetReports(filter);
                if (acc_BalanceSheet_DTO != null && acc_BalanceSheet_DTO.Result != null)
                {
                    Acc_New_Balanace_Sheet_DTO acc_New_Balanace_Sheet_DTO = new Acc_New_Balanace_Sheet_DTO();
                    acc_New_Balanace_Sheet_DTO = acc_BalanceSheet_DTO.Result;

                    html.Append(balance_Sheet_.TotalTitle);
                    StringBuilder Assets = new StringBuilder();
                    Assets.Append(balance_Sheet_.CurrentAssetTitle);
                    string CurrentAssets = "";
                    foreach (var item in acc_New_Balanace_Sheet_DTO.Assets?.CurrentAssets)
                    {
                        CurrentAssets += "<div style='color: #a4a4a4;display:flex;flex-wrap: wrap;width:100%;border-bottom: 1px solid #a4a4a4;padding-left: 3rem!important;'><div style='margin: 0px!important;text-align: left;padding: 0px!important;'>" + item.AccountName + "</div><div style='margin: 0px!important;padding: 0px!important;text-align: right;'>" + string.Format(us, "{0:C}", Math.Round(item.Amount, 2)) + "</div></div>";
                    }

                    string AccountsReceivable = balance_Sheet_.AccountsReceivable;
                    foreach (var item in acc_New_Balanace_Sheet_DTO.Assets?.AccountsReceivable)
                    {
                        AccountsReceivable += "<div style='color: #a4a4a4;display:flex;flex-wrap: wrap;width:100%;border-bottom: 1px solid #a4a4a4;padding-left: 3rem!important;'><div style='margin: 0px!important;text-align: left;padding: 0px!important;'>" + item.CustomerName + "</div><div style='margin: 0px!important;padding: 0px!important;text-align: right;'>" + string.Format(us, "{0:C}", Math.Round(item.Amount, 2)) + "</div></div>";
                    }
                    string TotalAccountReceivable = "<div style='color: #a4a4a4;display:flex;flex-wrap: wrap;justify - content: space-between;width:100%;padding-left: 3rem!important;padding-bottom: .25rem!important;padding-top: .25rem!important;border-top: 1px solid #a4a4a4;border-bottom: 1px solid #a4a4a4;'><div style='margin: 0;font-weight: bold;font-size: 1.3rem;text-align: left;' > Total Accounts Receivable (Debtors)</div><div style='font-weight: bold;text-align: right;' >" + string.Format(us, "{0:C}", Math.Round((decimal)acc_New_Balanace_Sheet_DTO.Assets?.TotalAccountsReceivable, 2)) + " </div></div>";

                    string TotalCurrentAssets = "<div style='color: #a4a4a4;display:flex;flex-wrap: wrap;justify - content: space-between;width:100%;padding-left: 3rem!important;padding-bottom: .25rem!important;padding-top: .25rem!important;border-top: 1px solid #a4a4a4;border-bottom: 1px solid #a4a4a4;'><div style='margin: 0;font-weight: bold;font-size: 1.3rem;text-align: left;' > Total Current Assets</div><div style='font-weight: bold;text-align: right;' >" + string.Format(us, "{0:C}", Math.Round((decimal)acc_New_Balanace_Sheet_DTO.Assets?.TotalCurrentAssets, 2)) + "</div></div>";
                    string TotalAsset = "<div style='color: #a4a4a4;display:flex;flex-wrap: wrap;justify - content: space-between;width:100%;padding-left: 3rem!important;padding-bottom: .25rem!important;padding-top: .25rem!important;border-top: 1px solid #a4a4a4;border-bottom: 1px solid #a4a4a4;'><div style= 'margin: 0;font-weight: bold;font-size: 1.3rem;text-align: left;' > Total Assets</div> <div style='font-weight: bold;text-align: right;' >" + string.Format(us, "{0:C}", Math.Round((decimal)(acc_New_Balanace_Sheet_DTO.Assets?.TotalAccountsReceivable + acc_New_Balanace_Sheet_DTO.Assets?.TotalCurrentAssets), 2)) + "</div></div>";
                    Assets.Append(CurrentAssets + AccountsReceivable + TotalAccountReceivable + TotalCurrentAssets + TotalAsset);
                    StringBuilder LiabilitiesAndEquity = new StringBuilder();
                    LiabilitiesAndEquity.Append(balance_Sheet_.LiabilitiesAndEquityTitle);
                    string CurrentLiability = balance_Sheet_.CurrentLiability;
                    foreach (var item in acc_New_Balanace_Sheet_DTO.LiabilitiesAndEquity.CurrentLiabilities)
                    {
                        CurrentLiability += "<div style='color: #a4a4a4;display:flex;flex-wrap: wrap;width:100%;border-bottom: 1px solid #a4a4a4;padding-left: 3rem!important;'><div style='margin: 0px!important;text-align: left;padding: 0px!important;'>" + item.AccountName + "</div><div style='margin: 0px!important;padding: 0px!important;text-align: right;'>" + string.Format(us, "{0:C}", Math.Round(item.Amount, 2)) + "</div></div>";
                    }
                    string TotalCurrentLiability = " <div style='color: #a4a4a4;display:flex;flex - wrap: wrap;justify - content: space - between; width: 100 %;padding - left: 3rem!important;  padding - bottom: .25rem!important;padding-top: .25rem!important; border-bottom: 1px solid #a4a4a4;'><div style= 'margin: 0;font-weight: bold;font-size: 1.3rem;text-align: left;' > Total Current Liability</div><div style='font-weight: bold;text-align: right;' >" + string.Format(us, "{0:C}", Math.Round((decimal)acc_New_Balanace_Sheet_DTO.LiabilitiesAndEquity?.TotalLiabilities, 2)) + "</div ></div >";
                    LiabilitiesAndEquity.Append(CurrentLiability + TotalCurrentLiability);
                    string Equity = "<div style ='padding-bottom: .25rem!important;padding-top: .25rem!important;border-bottom: 1px solid #a4a4a4;' ><span style = 'float: left; margin-top: 15px;margin-right: 10px' ><i style ='width: 0;border-left: 6px solid transparent;border-right: 6px solid transparent;border-top: 6px solid #A4A4A4;' ></i></span ><h6 style='color: #a4a4a4; margin: 0;font-size: 1.3rem;text-align: left;'> Equity</h6> </div>";
                    foreach (var item in acc_New_Balanace_Sheet_DTO.LiabilitiesAndEquity.Equity)
                    {
                        Equity += "<div style='color: #a4a4a4;display:flex;flex-wrap: wrap;width:100%;border-bottom: 1px solid #a4a4a4;padding-left: 3rem!important;'><div style='margin: 0px!important;text-align: left;padding: 0px!important;'>" + item.AccountName + "</div><div style='margin: 0px!important;padding: 0px!important;text-align: right;'>" + string.Format(us, "{0:C}", Math.Round(item.Amount, 2)) + "</div></div>";
                    }
                    string TotalEquity = " <div style='color: #a4a4a4;display:flex;flex - wrap: wrap;justify - content: space - between; width: 100 %;padding - left: 3rem!important;  padding - bottom: .25rem!important;padding-top: .25rem!important; border-bottom: 1px solid #a4a4a4;'><div style= 'margin: 0;font-weight: bold;font-size: 1.3rem;text-align: left;' > Total Equity</div><div style='font-weight: bold;text-align: right;' >" + string.Format(us, "{0:C}", Math.Round((decimal)acc_New_Balanace_Sheet_DTO.LiabilitiesAndEquity?.TotalEquity, 2)) + "</div></div>";
                    string TotalLiabilityEquity = " <div style='color: #a4a4a4;display:flex;flex - wrap: wrap;justify - content: space - between; width: 100 %;padding - left: 3rem!important;  padding - bottom: .25rem!important;padding-top: .25rem!important; border-bottom: 1px solid #a4a4a4;'><div style= 'margin: 0;font-weight: bold;font-size: 1.3rem;text-align: left;' >Total Liability and Equity</div><div style='font-weight: bold;text-align: right;' >" + string.Format(us, "{0:C}", Math.Round((decimal)acc_New_Balanace_Sheet_DTO.LiabilitiesAndEquity?.TotalLiabilitiesAndEquity, 2)) + "</div></div>";
                    LiabilitiesAndEquity.Append(Equity + TotalLiabilityEquity);
                    html.Append(Assets.ToString() + LiabilitiesAndEquity.ToString() + "</div>");
                    html.Append(template.HtmlFooter);
                    byte[] pdffile = PDF.GenerateRuntimePDF(html.ToString());
                    custom.Data = pdffile;
                    custom.HttpStatusCode = System.Net.HttpStatusCode.OK;
                }
                return custom;
            }
            catch (Exception ex)
            {
                custom.Message = ex.Message;
                custom.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return custom;
            }
        }


        public async Task<Acc_New_Balanace_Sheet_DTO> BalanceSheetReports(Acc_Report_Filter filter)
        {
            Acc_New_Balanace_Sheet_DTO balanceSheet = new Acc_New_Balanace_Sheet_DTO();
            Assets assets = new Assets();
            List<CurrentAssets> CurrentAssets = new List<CurrentAssets>();
            List<AccountsReceivable> AccountsReceivable = new List<AccountsReceivable>();

            LiabilitiesAndEquity liabilitiesAndEquity = new LiabilitiesAndEquity();
            List<CurrentLiabilities> CurrentLiabilities = new List<CurrentLiabilities>();
            List<Equity> Equity = new List<Equity>();
            try
            {
                Acc_Ledger_Headers_DTO acc_Ledger_DTO = new Acc_Ledger_Headers_DTO();
                acc_Ledger_DTO.Type = 3;
                acc_Ledger_DTO.UserID = filter.UserId;
                if (filter.StartDate != null && filter.EndDate != null)
                    acc_Ledger_DTO.WhereClause += "AND LedgH_CreatedOn >=CONVERT(nvarchar, '" + filter.StartDate.Value.ToString("yyyy/MM/dd") + "') and LedgH_CreatedOn <= CONVERT(nvarchar, '" + filter.EndDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59).ToString("yyyy/MM/dd h:mm tt") + "')";

                IEnumerable<Acc_Ledger_Headers_DTO> acc_Ledger_Headers_DTOs = acc_Ledger_MasterData.GetLedgerEntries(acc_Ledger_DTO);
                var currentAssets = acc_Ledger_Headers_DTOs.Where(x => (x.Acc_Account_Type == (int)
                AccountType.FixedAsset || x.Acc_Account_Type == (int)
                AccountType.OtherAsset || x.Acc_Account_Type == (int)
                AccountType.OthercurrentAsset)).GroupBy(x => new { x.Acc_AccountID, x.Acc_Account_Type, x.Acc_Account_Name, x.Acc_Account_Code, x.LedgH_CreatedOn }).Select(Line => new CurrentAssets
                {
                    AccountId = Line.Key.Acc_AccountID,
                    AccountType = Line.Key.Acc_Account_Type,
                    AccountCode = Line.Key.Acc_Account_Code,
                    AccountName = Line.Key.Acc_Account_Name,
                    Amount = Line.Sum(s => s.LedgE_DrCr == (int)DrOrCrSide.Dr ? (s.Acc_DrOrCr_Side == (int)DrOrCrSide.Dr ? s.DebitAmount : -(s.DebitAmount)) : (s.Acc_DrOrCr_Side == (int)DrOrCrSide.Dr ? -(s.CreditAmount) : s.CreditAmount)),
                    CreatedDate = Line.Key.LedgH_CreatedOn
                });
                CurrentAssets = currentAssets.ToList();
                assets.CurrentAssets = CurrentAssets;
                assets.TotalCurrentAssets = CurrentAssets.Sum(x => x.Amount);
                var liabilities = acc_Ledger_Headers_DTOs.Where(x => x.Acc_Account_Type == (int)AccountType.Liabilities).GroupBy(x => new { x.Acc_AccountID, x.Acc_Account_Type, x.Acc_Account_Name, x.Acc_Account_Code, x.LedgH_CreatedOn }).Select(Line => new CurrentLiabilities
                {
                    AccountId = Line.Key.Acc_AccountID,
                    AccountType = Line.Key.Acc_Account_Type,
                    AccountCode = Line.Key.Acc_Account_Code,
                    AccountName = Line.Key.Acc_Account_Name,
                    Amount = Line.Sum(s => s.LedgE_DrCr == (int)DrOrCrSide.Dr ? (s.Acc_DrOrCr_Side == (int)DrOrCrSide.Dr ? s.DebitAmount : -(s.DebitAmount)) : (s.Acc_DrOrCr_Side == (int)DrOrCrSide.Dr ? -(s.CreditAmount) : s.CreditAmount)),
                    CreatedDate = Line.Key.LedgH_CreatedOn
                });

                liabilitiesAndEquity.CurrentLiabilities = liabilities.ToList();
                var equities = acc_Ledger_Headers_DTOs.Where(x => x.Acc_Account_Type == (int)AccountType.Equity).GroupBy(x => new { x.Acc_AccountID, x.Acc_Account_Type, x.Acc_Account_Name, x.Acc_Account_Code, x.LedgH_CreatedOn }).Select(Line => new Equity
                {
                    AccountId = Line.Key.Acc_AccountID,
                    AccountType = Line.Key.Acc_Account_Type,
                    AccountCode = Line.Key.Acc_Account_Code,
                    AccountName = Line.Key.Acc_Account_Name,
                    Amount = Line.Sum(s => s.LedgE_DrCr == (int)DrOrCrSide.Dr ? (s.Acc_DrOrCr_Side == (int)DrOrCrSide.Dr ? s.DebitAmount : -(s.DebitAmount)) : (s.Acc_DrOrCr_Side == (int)DrOrCrSide.Dr ? -(s.CreditAmount) : s.CreditAmount)),
                    CreatedDate = Line.Key.LedgH_CreatedOn
                });


                liabilitiesAndEquity.Equity = Equity;
                Acc_Invoice_DTO acc_Invoice_DTO = new Acc_Invoice_DTO();
                acc_Invoice_DTO.Type = 3;
                if (filter.StartDate != null && filter.EndDate != null)
                    acc_Invoice_DTO.WhereClause += "AND inv.Invoice_CreatedOn >=CONVERT(nvarchar, '" + filter.StartDate.Value.ToString("yyyy/MM/dd") + "') and inv.Invoice_CreatedOn <= CONVERT(nvarchar, '" + filter.EndDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59).ToString("yyyy/MM/dd h:mm tt") + "')";
                var getInvoice = acc_Invoice_Data.GetInvoiceMaster(acc_Invoice_DTO);
                if (getInvoice != null)
                {
                    foreach (var item in getInvoice.Where(x => x.Invoice_Total > 0))
                    {
                        AccountsReceivable dto = new AccountsReceivable()
                        {
                            InvoiceId = item.Invoice_pkeyId,
                            Amount = item.Invoice_Total.Value,
                            customerId = item.Invoice_CustomeId,
                            CustomerName = "Accounts Receivable (Debtors)",
                            invoiceStatus = (InvoiceStatus)item.Invoice_Status.Value,
                            CreatedDate = item.Invoice_Date.Value,
                        };
                        AccountsReceivable.Add(dto);
                    }
                }
                Acc_Bill_DTO acc_Bill_DTO = new Acc_Bill_DTO();
                acc_Bill_DTO.Type = 3;
                acc_Bill_DTO.UserID = filter.UserId;
                if (filter.StartDate != null && filter.EndDate != null)
                    acc_Bill_DTO.WhereClause = "AND bill.Bill_CreatedOn >=CONVERT(nvarchar, '" + filter.StartDate.Value.ToString("yyyy/MM/dd") + "') and bill.Bill_CreatedOn <= CONVERT(nvarchar, '" + filter.EndDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59).ToString("yyyy/MM/dd h:mm tt") + "')";
                var getBill = acc_Bill_Data.GetBillMaster(acc_Bill_DTO);
                if (getBill != null)
                {
                    foreach (var item in getBill.Where(x => x.Bill_Total.Value > 0))
                    {
                        AccountsReceivable dto = new AccountsReceivable()
                        {
                            InvoiceId = item.Bill_pkeyId,
                            Amount = item.Bill_Total.Value,
                            customerId = item.Bill_VendorId,
                            CustomerName = "Accounts Receivable (Debtors)",
                            invoiceStatus = (InvoiceStatus)item.Bill_Status.Value,
                            CreatedDate = item.Bill_Date.Value,
                        };
                        AccountsReceivable.Add(dto);
                    }
                }
                //AccountsReceivable = AccountsReceivable.ToList();
                assets.AccountsReceivable = AccountsReceivable;
                assets.TotalAccountsReceivable = AccountsReceivable.Sum(x => x.Amount);
                balanceSheet.TotalAsset = assets.TotalAccountsReceivable + assets.TotalCurrentAssets;
                balanceSheet.Assets = assets;
                liabilitiesAndEquity.TotalLiabilities = liabilitiesAndEquity.CurrentLiabilities.Sum(x => x.Amount);
                Equity.Add(new Equity { AccountName = "Profit for the year", Amount = (balanceSheet.TotalAsset.Value - liabilitiesAndEquity.TotalLiabilities.Value) });

                liabilitiesAndEquity.TotalEquity = liabilitiesAndEquity.Equity.Sum(x => x.Amount);
                liabilitiesAndEquity.TotalLiabilitiesAndEquity = liabilitiesAndEquity.TotalLiabilities + liabilitiesAndEquity.TotalEquity;
                balanceSheet.LiabilitiesAndEquity = liabilitiesAndEquity;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return balanceSheet;
        }

    }
}