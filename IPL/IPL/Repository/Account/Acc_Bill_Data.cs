﻿using Avigma.Repository.Lib;
using IPL.HelperMessage;
using IPL.Models.Account;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Web;

namespace IPL.Repository.Account
{
    public class Acc_Bill_Data
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public IEnumerable<Acc_Bill_DTO> GetBillMaster(Acc_Bill_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Acc_Get_Bills]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Bill_Id", 1 + "#bigint#" + model.Bill_pkeyId);
                input_parameters.Add("@WhereClause", 1 + "#varchar#" + model.WhereClause);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                ds = obj.SelectSql(selectProcedure, input_parameters);

                try
                {
                    if (ds.Tables.Count > 0 && ds.Tables[0] != null)
                    {
                        var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                        List<Acc_Bill_DTO> BillDetail =
                           (from item in myEnumerableFeaprd
                            select new Acc_Bill_DTO
                            {
                                Bill_pkeyId = item.Field<Int64>("Bill_pkeyId"),
                                Bill_Number = item.Field<String>("Bill_Number"),
                                Bill_VendorId = item.Field<long>("Bill_VendorId"),
                                Bill_Vendor_Email = item.Field<string>("Bill_Vendor_Email"),
                                Bill_Date = item.Field<DateTime?>("Bill_Date"),
                                Bill_Sub_total = item.Field<decimal?>("Bill_Sub_total"),
                                Bill_Taxble_Amount = item.Field<decimal?>("Bill_Taxble_Amount"),
                                Bill_Total = item.Field<decimal?>("Bill_Total"),
                                Bill_Balance_Due = item.Field<decimal?>("Bill_Balance_Due"),
                                Bill_Vendor_Name = item.Field<string>("First_Name")+" "+ item.Field<string>("Last_Name"),
                                Bill_Status = item.Field<int?>("Bill_Status"),
                                Bill_Due_Date = item.Field<DateTime?>("Bill_Due_Date"),
                                Bill_Created = item.Field<String>("Bill_CreatedBy"),
                                Bill_Modified = item.Field<String>("Bill_ModifiedBy"),
                            }).ToList();
                        return BillDetail.AsEnumerable<Acc_Bill_DTO>();
                    }
                }
                catch (Exception ex)
                {
                    log.logErrorMessage(ex.StackTrace);
                    log.logErrorMessage(ex.Message);
                }
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }


            return null;
        }



        public Custom_Response AddBillData(Acc_Bill_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            Custom_Response custom_Response = new Custom_Response();

            string insertProcedure = "[Acc_CreateUpdate_Bill]";
            if (model.Bill_pkeyId == 0)
            {
                model.Type = 1;
            }
            else
            {
                model.Type = 2;
            }
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Bill_pkeyId", 1 + "#bigint#" + model.Bill_pkeyId);
                input_parameters.Add("@Bill_Number", 1 + "#nvarchar#" + model.Bill_Number);
                input_parameters.Add("@Bill_VendorId", 1 + "#bigint#" + model.Bill_VendorId);
                input_parameters.Add("@Bill_Vendor_Email", 1 + "#nvarchar#" + model.Bill_Vendor_Email);
                input_parameters.Add("@Bill_Send_to", 1 + "#bit#" + model.Bill_Send_to);
                input_parameters.Add("@Bill_Send_to_DateTime", 1 + "#datetime#" + model.Bill_Send_to_DateTime);
                input_parameters.Add("@Bill_CcBcc_Label", 1 + "#nvarchar#" + model.Bill_CcBcc_Label);
                input_parameters.Add("@Bill_Billing_Address", 1 + "#nvarchar#" + model.Bill_Billing_Address);
                input_parameters.Add("@Bill_Date", 1 + "#datetime#" + model.Bill_Date);
                input_parameters.Add("@Bill_Due_Date", 1 + "#datetime#" + model.Bill_Due_Date);
                input_parameters.Add("@Bill_PO_No", 1 + "#nvarchar#" + model.Bill_PO_No);
                input_parameters.Add("@Bill_Message", 1 + "#nvarchar#" + model.Bill_Message);
                input_parameters.Add("@Bill_Statement", 1 + "#nvarchar#" + model.Bill_Statement);
                input_parameters.Add("@Bill_Sub_total", 1 + "#decimal#" + model.Bill_Sub_total);
                input_parameters.Add("@Bill_Taxble_Amount", 1 + "#decimal#" + model.Bill_Taxble_Amount);
                input_parameters.Add("@Bill_Taxble_Sub_total", 1 + "#nvarchar#" + model.Bill_Taxble_Sub_total);
                input_parameters.Add("@Bill_Total", 1 + "#decimal#" + model.Bill_Total);
                input_parameters.Add("@Bill_Balance_Due", 1 + "#decimal#" + model.Bill_Balance_Due);
                input_parameters.Add("@Bill_IsActive", 1 + "#bit#" + true);
                input_parameters.Add("@Bill_IsDelete", 1 + "#bit#" + false);
                input_parameters.Add("@Bill_Terms", 1 + "#nvarchar#" + model.Bill_Terms);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Bill_pkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);
                input_parameters.Add("@Bill_Message_Out", 2 + "#varchar#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    custom_Response.Message = Message.ErrorWhileSaving;
                    custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                }
                else
                {
                    custom_Response.Message = Message.SuccessSave;
                    custom_Response.HttpStatusCode = HttpStatusCode.OK;
                    foreach (var item in model.Bill_Items)
                    {
                        if (model.Bill_pkeyId == 0)
                        {
                            item.Bill_Id = Convert.ToInt64(objData[0]);
                        }
                        else
                        {
                            item.Bill_Id = model.Bill_pkeyId;
                        }
                        item.Type = model.Type;
                        if (item.Bill_Items_PkeyId> 0)
                        {
                            item.Type = 2;
                        }
                        else
                        {
                            item.Type = 1;
                        }
                        item.UserID = model.UserID.GetValueOrDefault(0);
                        custom_Response = AddBillItemsData(item);
                        if (custom_Response.HttpStatusCode != HttpStatusCode.OK)
                        {
                            return custom_Response;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                custom_Response.Message = Message.ErrorWhileSaving;
                custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return custom_Response;
        }
        public Custom_Response AddBillPaymentData(Acc_Bill_Payment_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            Custom_Response custom_Response = new Custom_Response();

            string insertProcedure = "[Acc_CreateUpdate_Bill_Payment]";
            if (model.Bill_Pay_PkeyId == 0)
            {
                model.Type = 1;
            }
            else if (model.Bill_Pay_PkeyId > 0 && model.Type == 0)
            {
                model.Type = 2;
            }
            else
            {
                model.Type = 3;

            }
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Bill_Pay_PkeyId", 1 + "#bigint#" + model.Bill_Pay_PkeyId);
                input_parameters.Add("@Bill_Pay_Bill_Id", 1 + "#bigint#" + model.Bill_Pay_Bill_Id);
                input_parameters.Add("@Bill_Pay_Vendor_Id", 1 + "#bigint#" + model.Bill_Pay_Vendor_Id);
                input_parameters.Add("@Bill_Pay_Wo_Id", 1 + "#bigint#" + model.Bill_Pay_Wo_Id);
                input_parameters.Add("@Bill_Pay_Payment_Date", 1 + "#datetime#" + model.Bill_Pay_Payment_Date);
                input_parameters.Add("@Bill_Pay_Amount", 1 + "#decimal#" + model.Bill_Pay_Amount);
                input_parameters.Add("@Bill_Pay_CheckNumber", 1 + "#varchar#" + model.Bill_Pay_CheckNumber);
                input_parameters.Add("@Bill_Pay_Comment", 1 + "#varchar#" + model.Bill_Pay_Comment);
                input_parameters.Add("@Bill_Pay_Balance_Due", 1 + "#decimal#" + model.Bill_Pay_Balance_Due);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Bill_Payment_pkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);
                input_parameters.Add("@Bill_Message_Out", 2 + "#varchar#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    custom_Response.Message = Message.ErrorWhileSaving;
                    custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                }
                else
                {
                    custom_Response.Message = Message.SuccessSave;
                    custom_Response.HttpStatusCode = HttpStatusCode.OK;
                    if (model.Type == 3)
                    {
                        custom_Response.Message = Message.SuccessDelete;
                    }
                }
            }
            catch (Exception ex)
            {
                custom_Response.Message = Message.ErrorWhileSaving;
                custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return custom_Response;
        }
        public Custom_Response AddBillItemsData(Acc_Bill_ChildDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            Custom_Response custom_Response = new Custom_Response();

            string insertProcedure = "[Acc_CreateUpdate_Bill_Items]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Bill_Items_pkeyId", 1 + "#bigint#" + model.Bill_Items_PkeyId);
                input_parameters.Add("@Bill_Id", 1 + "#bigint#" + model.Bill_Id);
                input_parameters.Add("@Bill_Items_Task_Id", 1 + "#nvarchar#" + model.Task_Id);
                input_parameters.Add("@Bill_Items_QTY", 1 + "#varchar#" + model.QTY);
                input_parameters.Add("@Bill_Items_Amount", 1 + "#decimal#" + model.Amount);
                input_parameters.Add("@Bill_Items_Rate", 1 + "#decimal#" + model.Rate);
                input_parameters.Add("@Bill_Items_Tax", 1 + "#nvarchar#" + model.Tax);
                input_parameters.Add("@Bill_Items_Class", 1 + "#nvarchar#" + model.Class);
                input_parameters.Add("@Bill_Items_Description", 1 + "#nvarchar#" + model.Descp);
                input_parameters.Add("@Bill_Con_pkeyId", 1 + "#bigint#" + model.Bill_Con_Ch_pkeyId);
                input_parameters.Add("@Bill_Con_Ch_pkeyId", 1 + "#bigint#" + model.Bill_Con_Ch_pkeyId);
                input_parameters.Add("@Bill_Con_Ch_Wo_Id", 1 + "#bigint#" + model.Bill_Con_Ch_Wo_Id);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Bill_Items_pkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    custom_Response.Message = Message.ErrorWhileSaving;
                    custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                }
                else
                {
                    custom_Response.Message = Message.SuccessSave;
                    custom_Response.HttpStatusCode = HttpStatusCode.OK;
                }
            }
            catch (Exception ex)
            {
                custom_Response.Message = Message.ErrorWhileSaving;
                custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return custom_Response;
        }

        public Acc_Bill_DTO GetBill(Acc_Bill_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Acc_Get_Bill_Detail]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Bill_pkeyId", 1 + "#bigint#" + model.Bill_pkeyId);
                input_parameters.Add("@UserID", 1 + "#varchar#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);

                try
                {
                    if (ds.Tables.Count > 0 && ds.Tables[0] != null)
                    {
                        var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                        Acc_Bill_DTO BillDetail =
                       (from item in myEnumerableFeaprd
                        select new Acc_Bill_DTO
                        {
                            Bill_pkeyId = item.Field<Int64>("Bill_pkeyId"),
                            Bill_Number = item.Field<String>("Bill_Number"),
                            Bill_VendorId = item.Field<long>("Bill_VendorId"),
                            Bill_Vendor_Email = item.Field<string>("Bill_Vendor_Email"),
                            Bill_Send_to = item.Field<bool?>("Bill_Send_to"),
                            Bill_Send_to_DateTime = item.Field<DateTime?>("Bill_Send_to_DateTime"),
                            Bill_CcBcc_Label = item.Field<string>("Bill_CcBcc_Label"),
                            Bill_Billing_Address = item.Field<string>("Bill_Billing_Address"),
                            Bill_Terms = item.Field<string>("Bill_Terms"),
                            Bill_Date = item.Field<DateTime?>("Bill_Date"),
                            Bill_Due_Date = item.Field<DateTime?>("Bill_Due_Date"),
                            Bill_PO_No = item.Field<string>("Bill_PO_No"),
                            Bill_Statement = item.Field<string>("Bill_Statement"),
                            Bill_IsActive = item.Field<bool?>("Bill_IsActive"),
                            Bill_IsDelete = item.Field<bool?>("Bill_IsDelete"),
                            Bill_CreatedBy = item.Field<long>("Bill_CreatedBy"),
                            Bill_CreatedOn = item.Field<DateTime>("Bill_CreatedOn"),
                            Bill_ModifiedBy = item.Field<long?>("Bill_ModifiedBy"),
                            Bill_ModifiedOn = item.Field<DateTime?>("Bill_ModifiedOn"),
                            Bill_DeletedOn = item.Field<DateTime?>("Bill_DeletedOn"),
                            Bill_Message = item.Field<string>("Bill_Message"),
                            Bill_Sub_total = item.Field<decimal?>("Bill_Sub_total"),
                            Bill_Taxble_Sub_total = item.Field<string>("Bill_Taxble_Sub_total"),
                            Bill_Taxble_Amount = item.Field<decimal?>("Bill_Taxble_Amount"),
                            Bill_Total = item.Field<decimal?>("Bill_Total"),
                            Bill_Balance_Due = item.Field<decimal?>("Bill_Balance_Due"),
                            Bill_Con_pkeyId = item.Field<Int64?>("Bill_Con_pkeyId"),
                            Bill_Status = item.Field<int?>("Bill_Status"),
                        }).FirstOrDefault();
                        if (ds.Tables.Count > 1 && ds.Tables[1] != null)
                        {
                            var BillItemsList = ds.Tables[1].AsEnumerable();
                            List<Acc_Bill_ChildDTO> ItemsList =
                             (from item in BillItemsList
                              select new Acc_Bill_ChildDTO
                              {
                                  Bill_Items_PkeyId = item.Field<Int64>("Bill_Items_pkeyId"),
                                  Bill_Id = item.Field<Int64>("Bill_Id"),
                                  Task_Id = item.Field<Int64?>("Task_Id"),
                                  Tax = item.Field<bool?>("Tax"),
                                  Class = item.Field<string>("Class"),
                                  Descp = item.Field<string>("Descp"),
                                  Amount = item.Field<decimal?>("Amount"),
                                  QTY = item.Field<string>("QTY"),
                                  Rate = item.Field<decimal?>("Rate"),
                                  Bill_Con_pkeyId = item.Field<Int64?>("Bill_Con_pkeyId"),
                                  Bill_Con_Ch_pkeyId = item.Field<Int64?>("Bill_Con_Ch_pkeyId"),
                                  Bill_Con_Ch_Wo_Id = item.Field<Int64?>("Bill_Con_Ch_Wo_Id"),
                              }).ToList();
                            BillDetail.Bill_Items = ItemsList;
                        }
                        if (ds.Tables.Count > 2 && ds.Tables[2] != null)
                        {
                            var BillPaymentList = ds.Tables[2].AsEnumerable();
                            List<Acc_Bill_Payment_DTO> PaymentList =
                             (from item in BillPaymentList
                              select new Acc_Bill_Payment_DTO
                              {
                                  Bill_Pay_PkeyId = item.Field<Int64>("Bill_Pay_PkeyId"),
                                  Bill_Pay_Bill_Id = item.Field<Int64>("Bill_Pay_Bill_Id"),
                                  Bill_Pay_Vendor_Id = item.Field<Int64>("Bill_Pay_Vendor_Id"),
                                  Bill_Pay_Wo_Id = item.Field<Int64>("Bill_Pay_Wo_Id"),
                                  Bill_Pay_Payment_Date = item.Field<DateTime?>("Bill_Pay_Payment_Date"),
                                  Bill_Pay_Amount = item.Field<decimal?>("Bill_Pay_Amount"),
                                  Bill_Pay_CheckNumber = item.Field<string>("Bill_Pay_CheckNumber"),
                                  Bill_Pay_Comment = item.Field<string>("Bill_Pay_Comment"),
                                  Bill_Pay_Balance_Due = item.Field<decimal?>("Bill_Pay_Balance_Due"),
                                  Bill_Pay_EnteredBy = item.Field<Int64>("Bill_Pay_EnteredBy"),
                                  Bill_Pay_EnteredBy_Name = item.Field<string>("Bill_Pay_EnteredBy_Name"),
                              }).ToList();
                            BillDetail.Bill_Payments = PaymentList;
                        }
                        if (ds.Tables.Count > 3 && ds.Tables[3] != null)
                        {

                            var ReceivePaymentList = ds.Tables[3].AsEnumerable();
                            List<Receive_Bill_Item> PaymentList =
                             (from item in ReceivePaymentList
                              select new Receive_Bill_Item
                              {
                                  Bill_Rec_Bill_Id = item.Field<Int64>("Bill_Rec_Bill_Id"),
                                  PaymentDate = item.Field<DateTime>("Bill_Rec_CreatedOn"),
                                  Bill_Rec_Original_Amount = item.Field<decimal?>("Bill_Rec_Original_Amount"),
                                  Bill_Rec_Payment = item.Field<decimal?>("Bill_Rec_Payment"),

                              }).ToList();
                            BillDetail.Receive_Bill_Items = PaymentList;
                        }
                        return BillDetail;
                    }
                }
                catch (Exception ex)
                {
                    log.logErrorMessage(ex.StackTrace);
                    log.logErrorMessage(ex.Message);
                }
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }


            return null;
        }

        public Custom_Response GetBillByVendor(Acc_Bill_Receive_PaymentDTO model)
        {
            Custom_Response custom_Response = new Custom_Response();
            Acc_Bill_Receive_PaymentDTO result = new Acc_Bill_Receive_PaymentDTO();
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Acc_Get_Bill_By_VendorId]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Bill_VendorId", 1 + "#bigint#" + model.Bill_Rec_Vendor_Id);
                input_parameters.Add("@Bill_Id", 1 + "#bigint#" + model.Bill_pkeyId);
                input_parameters.Add("@Bill_Number", 1 + "#nvarchar#" + model.Bill_Number);
                input_parameters.Add("@UserID", 1 + "#varchar#" + model.UserID);
                input_parameters.Add("@WhereClause", 1 + "#varchar#" + model.WhereClause);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);

                try
                {
                    if (ds.Tables.Count > 0 && ds.Tables[0] != null)
                    {

                        var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                        List<Receive_Bill_Item> Receive_Bill_Items =
                          (from item in myEnumerableFeaprd
                           select new Receive_Bill_Item
                           {
                               //receive_Bill_Items_PkeyId = item.Field<Int64>("receive_Bill_Items_PkeyId"),
                               Bill_Rec_Bill_Id = item.Field<Int64>("Bill_pkeyId"),
                               Bill_VendorId = item.Field<Int64>("Bill_VendorId"),
                               Bill_Number = item.Field<string>("Bill_Number"),
                               DueDate = item.Field<DateTime?>("Bill_Due_Date"),
                               Bill_Total = item.Field<decimal?>("Bill_Total"),
                               Bill_Rec_Original_Amount = item.Field<decimal?>("Original_Amount"),
                               Bill_Rec_Payment = item.Field<decimal?>("Payment_Amount"),
                               Pending_Amount = item.Field<decimal?>("Pending_Amount"),
                               Bill_Rec_Deposit_To = item.Field<long?>("Bill_Rec_Deposit_To"),
                           }).ToList();
                        if (Receive_Bill_Items != null && Receive_Bill_Items.Count > 0)
                        {
                            foreach (var item in Receive_Bill_Items)
                            {
                                item.Bill_Rec_Original_Amount = item.Bill_Rec_Original_Amount == 0 ? item.Bill_Total : item.Bill_Rec_Original_Amount;
                                if (item.Bill_Rec_Payment > 0)
                                {

                                    item.Pending_Amount = item.Bill_Rec_Payment != 0 ? item.Bill_Rec_Original_Amount - item.Bill_Rec_Payment : item.Pending_Amount;
                                }
                                else
                                {
                                    item.Pending_Amount = item.Bill_Rec_Original_Amount;
                                }
                            }
                        }
                        result.Receive_Bill_Items = Receive_Bill_Items;
                        result.Bill_Rec_Vendor_Id = model.Bill_Rec_Vendor_Id;
                        custom_Response.Data = result;
                        custom_Response.HttpStatusCode = HttpStatusCode.OK;
                        return custom_Response;
                    }
                }
                catch (Exception ex)
                {
                    log.logErrorMessage(ex.StackTrace);
                    log.logErrorMessage(ex.Message);
                    custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                }
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
            }


            return custom_Response;
        }
        public Custom_Response CheckBillReceivePayment(Acc_Bill_Receive_PaymentDTO Data)
        {
            Custom_Response responce = new Custom_Response();
            try
            {
                Boolean IsValidAmount = false;
                List<Final_Bill_Receive_Payment_DTO> FinalList = new List<Final_Bill_Receive_Payment_DTO>();
                if (Data.Receive_Bill_Items != null && Data.Receive_Bill_Items.Count > 0)
                {

                    foreach (var item in Data.Receive_Bill_Items)
                    {
                        responce = CheckBillAmountValidOrNot((long)item.Bill_Rec_Bill_Id, (long)Data.UserID);
                        Receive_Bill_Item receive_Bill_Item = new Receive_Bill_Item();
                        receive_Bill_Item = responce.Data;
                        if (responce.HttpStatusCode == HttpStatusCode.OK && responce.Data != null)
                        {
                            if (receive_Bill_Item.Bill_Rec_Payment > 0)
                            {
                                receive_Bill_Item.Bill_Rec_Original_Amount = receive_Bill_Item.Bill_Rec_Original_Amount - receive_Bill_Item.Bill_Rec_Payment;
                                Data.Bill_Rec_PkeyId = (long)receive_Bill_Item.Bill_Rec_PkeyId;
                                Data.Bill_Rec_Deposit_To = receive_Bill_Item.Bill_Rec_Deposit_To>0?receive_Bill_Item.Bill_Rec_Deposit_To: Data.Bill_Rec_Deposit_To;
                            }
                            if (item.Pending_Amount == receive_Bill_Item.Bill_Rec_Original_Amount)
                            {
                                IsValidAmount = true;
                                FinalList.Add(new Final_Bill_Receive_Payment_DTO
                                {
                                    Bill_Rec_PkeyId = Data.Bill_Rec_PkeyId,
                                    Bill_Rec_Vendor_Id = Data.Bill_Rec_Vendor_Id,
                                    Bill_Rec_Bill_Id = item.Bill_Rec_Bill_Id,
                                    Bill_Rec_Payment_Date = Data.Bill_Rec_Payment_Date,
                                    Bill_Rec_Original_Amount = item.Bill_Rec_Original_Amount,
                                    Bill_Rec_Payment = item.Pending_Amount,
                                    Bill_Rec_Pending_Amount = item.Bill_Rec_Original_Amount - item.Pending_Amount,
                                    Bill_Rec_Payment_Method = Data.Bill_Rec_Payment_Method,
                                    Bill_Rec_Reference_No = Data.Bill_Rec_Reference_No,
                                    Bill_Rec_Deposit_To = Data.Bill_Rec_Deposit_To,
                                    Bill_Rec_EnteredBy = Data.UserID,
                                    UserID = Data.UserID,
                                });
                                ;
                            }
                            else
                            {
                                IsValidAmount = false;
                            }
                        }
                        else
                        {
                            IsValidAmount = false;
                        }
                    }

                }
                if (IsValidAmount)
                {
                    foreach (var item in FinalList)
                    {
                        responce = AddBillReceivePaymentData(item);
                        if (responce.HttpStatusCode != HttpStatusCode.OK)
                        {
                            responce.Message = Message.SuccessSave;
                            // return responce;
                        }
                    }
                }
                else
                {
                    responce.Message = Message.BillAmountNotMatch;
                    responce.HttpStatusCode = HttpStatusCode.NotAcceptable;
                    return responce;
                }
            }
            catch (Exception ex)
            {
                responce.Message = Message.ErrorWhileSaving;
                responce.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return responce;
        }
        public Custom_Response CheckBillAmountValidOrNot(long BillId, long UserID)
        {
            Custom_Response custom_Response = new Custom_Response();
            DataSet ds = null;
            string selectProcedure = "[Acc_Validate_Bill_Amount]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            input_parameters.Add("@Bill_Id", 1 + "#bigint#" + BillId);
            input_parameters.Add("@UserID", 1 + "#bigint#" + UserID);
            ds = obj.SelectSql(selectProcedure, input_parameters);

            try
            {
                if (ds.Tables.Count > 0 && ds.Tables[0] != null)
                {
                    var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                    Receive_Bill_Item BillDetail =
                       (from item in myEnumerableFeaprd
                        select new Receive_Bill_Item
                        {
                            Bill_Rec_PkeyId = item.Field<Int64>("Bill_Rec_PkeyId"),
                            Bill_Rec_Bill_Id = item.Field<Int64>("Bill_pkeyId"),
                            Bill_Rec_Deposit_To = item.Field<Int64>("Bill_Rec_Deposit_To"),
                            Bill_Number = item.Field<String>("Bill_Number"),
                            Bill_Rec_Original_Amount = item.Field<Decimal?>("Bill_Total"),
                            Bill_Status = item.Field<int>("Bill_Status"),
                            Bill_Rec_Payment = item.Field<Decimal?>("Bill_Rec_Payment"),
                        }).FirstOrDefault();
                    custom_Response.Data = BillDetail;
                    custom_Response.HttpStatusCode = HttpStatusCode.OK;
                    return custom_Response;
                }
            }
            catch (Exception ex)
            {
                custom_Response.Message = Message.GettingErrorWhileBindList;
                custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return custom_Response;
        }
        public Custom_Response AddBillReceivePaymentData(Final_Bill_Receive_Payment_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            Custom_Response custom_Response = new Custom_Response();

            string insertProcedure = "[Acc_CreateUpdate_Bill_Receive_Payment]";
            if (model.Bill_Rec_PkeyId == 0)
            {
                model.Type = 1;
            }
            else if (model.Bill_Rec_PkeyId > 0 && model.Type == 0)
            {
                model.Type = 2;
            }
            else
            {
                model.Type = 3;

            }
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Bill_Rec_PkeyId", 1 + "#bigint#" + model.Bill_Rec_PkeyId);
                input_parameters.Add("@Bill_Rec_Vendor_Id", 1 + "#bigint#" + model.Bill_Rec_Vendor_Id);
                input_parameters.Add("@Bill_Rec_Bill_Id", 1 + "#bigint#" + model.Bill_Rec_Bill_Id);
                input_parameters.Add("@Bill_Rec_Payment_Date", 1 + "#datetime#" + model.Bill_Rec_Payment_Date);
                input_parameters.Add("@Bill_Rec_Original_Amount", 1 + "#decimal#" + model.Bill_Rec_Original_Amount);
                input_parameters.Add("@Bill_Rec_Payment", 1 + "#decimal#" + model.Bill_Rec_Payment);
                input_parameters.Add("@Bill_Rec_Pending_Amount", 1 + "#decimal#" + model.Bill_Rec_Pending_Amount);
                input_parameters.Add("@Bill_Rec_Payment_Method", 1 + "#int#" + model.Bill_Rec_Payment_Method);
                input_parameters.Add("@Bill_Rec_Reference_No", 1 + "#varchar#" + model.Bill_Rec_Reference_No);
                input_parameters.Add("@Bill_Rec_Deposit_To", 1 + "#bigint#" + model.Bill_Rec_Deposit_To);
                input_parameters.Add("@Bill_Rec_Memo", 1 + "#varchar#" + model.Bill_Rec_Memo);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Bill_Rec_pkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);
                input_parameters.Add("@Bill_Message_Out", 2 + "#varchar#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    custom_Response.Message = Message.ErrorWhileSaving;
                    custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                }
                else
                {
                    custom_Response.Message = Message.SuccessSave;
                    custom_Response.HttpStatusCode = HttpStatusCode.OK;
                    if (model.Type == 3)
                    {
                        custom_Response.Message = Message.SuccessDelete;
                    }
                }
            }
            catch (Exception ex)
            {
                custom_Response.Message = Message.ErrorWhileSaving;
                custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return custom_Response;
        }
        public Custom_Response PaidBillList(Acc_Bill_Bank_Deposit_DTO model)
        {
            Custom_Response custom_Response = new Custom_Response();
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Acc_Get_Bill_Paid]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);

                try
                {
                    var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                    List<Acc_Bill_BankDeposit_Items> Receive_Bill_Items =
                      (from item in myEnumerableFeaprd
                       select new Acc_Bill_BankDeposit_Items
                       {
                           Bill_Id = item.Field<Int64>("Bill_Rec_Bill_Id"),
                           Bill_Rec_PkeyId = item.Field<Int64>("Bill_Rec_PkeyId"),
                           Bill_VendorId = item.Field<Int64>("Bill_VendorId"),
                           Payment_Date = item.Field<DateTime?>("Payment_Date"),
                           Amount = item.Field<decimal?>("Original_Amount"),
                           Original_Amount = item.Field<decimal?>("Original_Amount"),
                           Payment_Amount = item.Field<decimal?>("Payment_Amount"),
                           Pending_Amount = item.Field<decimal?>("Pending_Amount"),
                           Customer_Name = item.Field<string>("First_Name")+" "+ item.Field<string>("Last_Name"),
                           Payment_Method = 0,
                           //Ref_No = item.Field<string>("Ref_No"),
                           //Memo = item.Field<string>("Memo"),
                       }).ToList();

                    custom_Response.Data = Receive_Bill_Items;
                    custom_Response.HttpStatusCode = HttpStatusCode.OK;
                    return custom_Response;
                }
                catch (Exception ex)
                {
                    log.logErrorMessage(ex.StackTrace);
                    log.logErrorMessage(ex.Message);
                    custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                }
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
            }


            return custom_Response;
        }
        public Custom_Response BillBankDepositData(Acc_Bill_Receive_PaymentDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            Custom_Response custom_Response = new Custom_Response();

            string insertProcedure = "[Acc_Bill_Bank_Deposit]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Bill_Rec_PkeyId", 1 + "#bigint#" + model.Bill_Rec_PkeyId);
                input_parameters.Add("@Bill_Id", 1 + "#bigint#" + model.Bill_pkeyId);
                input_parameters.Add("@Bill_Rec_Payment_Method", 1 + "#int#" + model.Bill_Rec_Payment_Method);
                input_parameters.Add("@Bill_Rec_Reference_No", 1 + "#varchar#" + model.Bill_Rec_Reference_No);
                input_parameters.Add("@Bill_Rec_Deposit_To", 1 + "#bigint#" + model.Bill_Rec_Deposit_To);
                input_parameters.Add("@Bill_Rec_Memo", 1 + "#varchar#" + model.Bill_Rec_Memo);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Bill_Items_pkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    custom_Response.Message = Message.ErrorWhileSaving;
                    custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                }
                else
                {
                    custom_Response.Message = Message.SuccessSave;
                    custom_Response.HttpStatusCode = HttpStatusCode.OK;
                }
            }
            catch (Exception ex)
            {
                custom_Response.Message = Message.ErrorWhileSaving;
                custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return custom_Response;
        }
        public Acc_Journal_Reports_DTO GetBillMasterReports(Acc_Bill_DTO model)
        {
            Acc_Journal_Reports_DTO acc_Journal_Reports_DTO = new Acc_Journal_Reports_DTO();
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Acc_Get_Bill_For_Reports]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@ID", 1 + "#bigint#" + model.Bill_pkeyId);
                input_parameters.Add("@WhereClause", 1 + "#varchar#" + model.WhereClause);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                ds = obj.SelectSql(selectProcedure, input_parameters);

                try
                {
                    if (ds.Tables.Count > 0 && ds.Tables[0] != null)
                    {

                        var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                        List<Acc_Journal_Bill_Reports_DTO> InvoiceDetail =
                           (from item in myEnumerableFeaprd
                            select new Acc_Journal_Bill_Reports_DTO
                            {
                                Bill_pkeyId = item.Field<Int64>("Bill_pkeyId"),
                                Bill_Number = item.Field<String>("Bill_Number"),
                                Bill_VendorId = item.Field<long>("Bill_VendorId"),
                                Bill_Vendor_Email = item.Field<string>("Bill_Vendor_Email"),
                                Bill_Date = item.Field<DateTime?>("Bill_Date"),
                                Bill_Due_Date = item.Field<DateTime?>("Bill_Due_Date"),
                                Bill_Sub_total = item.Field<decimal?>("Bill_Sub_total"),
                                Bill_Taxble_Amount = item.Field<decimal?>("Bill_Taxble_Amount"),
                                Bill_Total = item.Field<decimal?>("Bill_Total"),
                                Bill_Balance_Due = item.Field<decimal?>("Bill_Balance_Due"),
                                Bill_Vendor_Name = item.Field<string>("Vendor_Company_Name"),
                                Bill_Status = item.Field<int?>("Bill_Status"),
                                Bill_CreatedOn = item.Field<DateTime>("Bill_CreatedOn"),
                                Bill_Rec_Payment = item.Field<decimal?>("Bill_Rec_Payment"),
                                DeposiToAccount = item.Field<string>("DeposiToAccount"),
                                Bill_Rec_PkeyId = item.Field<long?>("Bill_Rec_PkeyId"),
                                Bill_Message = item.Field<string>("Bill_Message"),
                                ReceivePayment_Memo = item.Field<string>("Bill_Rec_Memo"),
                                ReceivedPaymentDate = item.Field<DateTime?>("ReceivedPaymentDate"),
                            }).ToList();
                        acc_Journal_Reports_DTO.Acc_Journal_Bill_Reports_DTO= InvoiceDetail.ToList();
                        return acc_Journal_Reports_DTO;
                    }
                }
                catch (Exception ex)
                {
                    log.logErrorMessage(ex.StackTrace);
                    log.logErrorMessage(ex.Message);
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return null;
        }


    }
}