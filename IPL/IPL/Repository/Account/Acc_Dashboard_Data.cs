﻿using Avigma.Repository.Lib;
using IPL.HelperMessage;
using IPL.Models.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;

namespace IPL.Repository.Account
{
    public class Acc_Dashboard_Data
    {
        Log log = new Log();
        public async Task<dynamic> GetAccountStats()
        {
            Custom_Response custom_Response = new Custom_Response();
            try
            {
                Acc_Account_MasterData acc_Account_MasterData = new Acc_Account_MasterData();
                Acc_AccountMaster_DTO acc_AccountMaster_DTO = new Acc_AccountMaster_DTO();
                acc_AccountMaster_DTO.Type = 1;
                var accounts = acc_Account_MasterData.GetAccountMaster(acc_AccountMaster_DTO);

                var topLevelAccount = accounts.Where(x => x.Acc_Parent_Account_Id == 0 && x.Acc_Account_Type != (int)AccountType.Equity);
                List<Acc_Dashboard_DTO> acc_Dashboard_DTOs = new List<Acc_Dashboard_DTO>();
                foreach (var item in topLevelAccount)
                {
                    acc_Dashboard_DTOs.Add(new Acc_Dashboard_DTO
                    {
                        AccountCode = item.Acc_Account_Code,
                        AccountId = item.Acc_pkeyId,
                        AccountName = item.Acc_Account_Name,
                        Amount = item.Balance,
                        //Amount=acc_Ledger_Headers_DTOs.Where(x=>x.Acc_AccountParentID==item.Acc_pkeyId).Select(x=>(x.LedgE_DrCr==(int)DrOrCrSide.Dr?(item.Acc_DrOrCr_Side == (int)DrOrCrSide.Dr ? x.DebitAmount:-(x.DebitAmount)) :(item.Acc_DrOrCr_Side == (int)DrOrCrSide.Dr ? -(x.CreditAmount) : x.CreditAmount))).Sum()
                    });
                }
                custom_Response.Data = acc_Dashboard_DTOs;
                custom_Response.HttpStatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return await Task.FromResult(custom_Response);
        }
        public async Task<Custom_Response> GetDefaultDashBoardDetails(Acc_Dashboard_Input_DTO model)
        {
            Acc_Dashboard_Details_DTO Result = new Acc_Dashboard_Details_DTO();
            ProfitAndLoss ProfitAndLoss = new ProfitAndLoss();
            Expenses Expenses = new Expenses();
            Invoice Invoice = new Invoice();
            Custom_Response custom_Response = new Custom_Response();
            try
            {
                Acc_Account_MasterData acc_Account_MasterData = new Acc_Account_MasterData();
                Acc_Invoice_Data Acc_Invoice_Data = new Acc_Invoice_Data();
                Acc_AccountMaster_DTO acc_AccountMaster_DTO = new Acc_AccountMaster_DTO();
                acc_AccountMaster_DTO.Type = 1;
                acc_AccountMaster_DTO.UserID = model.UserID;
                var accounts = acc_Account_MasterData.GetAccountMaster(acc_AccountMaster_DTO);
                //Profit and Loss
                //if (model.IsProfitLossChanges)
                //{
                ProfitAndLoss.Income = accounts.Where(x => x.Acc_Account_Type ==(int)AccountType.Income && x.Acc_CreatedOn <= model.ProfitLossDate).Select(x => x.Balance).ToList().Sum();
                ProfitAndLoss.Expenses = accounts.Where(x => (x.Acc_Account_Type ==(int)AccountType.Expense|| x.Acc_Account_Type == (int)AccountType.OtherExpense) && x.Acc_CreatedOn <= model.ExpensesDate).Select(x => x.Balance).ToList().Sum();
                ProfitAndLoss.NetIncome = ProfitAndLoss.Income - ProfitAndLoss.Expenses;
                Result.ProfitAndLoss = ProfitAndLoss;
                //}
                //Expenses
                //if (model.IsExpensesTypeChanges)
                //{
                Expenses.TotalExpenses = accounts.Where(x => (x.Acc_Account_Type == (int)AccountType.Expense || x.Acc_Account_Type == (int)AccountType.OtherExpense) && x.Acc_CreatedOn <= model.ExpensesDate).Select(x => x.Balance).ToList().Sum();
                var ExpensesAccount = accounts.Where(x => (x.Acc_Account_Type == (int)AccountType.Expense || x.Acc_Account_Type == (int)AccountType.OtherExpense) && x.Acc_CreatedOn <= model.ExpensesDate).ToList();
                List<Acc_Dashboard_DTO> ExpensesList = new List<Acc_Dashboard_DTO>();
                foreach (var item in ExpensesAccount)
                {
                    ExpensesList.Add(new Acc_Dashboard_DTO
                    {
                        AccountCode = item.Acc_Account_Code,
                        AccountId = item.Acc_pkeyId,
                        AccountName = item.Acc_Account_Name,
                        Amount = item.Balance,
                    });
                }
                Expenses.List = ExpensesList;
                Result.Expenses = Expenses;
                //}
                //Income
                Acc_Invoice_DTO acc_Invoice_DTO = new Acc_Invoice_DTO();
                acc_Invoice_DTO.Type = 1;
                acc_Invoice_DTO.UserID = model.UserID;
                var InvoiceList = Acc_Invoice_Data.GetInvoiceMaster(acc_Invoice_DTO);
                if (InvoiceList != null)
                {
                    Invoice.TotalInvoice = InvoiceList.Select(x => x.Invoice_Total).Sum();
                    Invoice.PaidInvoice = InvoiceList.Where(x => x.Invoice_Status.Value == (int?)InvoiceStatus.Paid).Select(x => x.Invoice_Total).Sum();
                    if (Invoice.TotalInvoice > 0)
                    {
                        Invoice.TotalInvoicePercentage = Invoice.TotalInvoice / Invoice.TotalInvoice * 100 + "%";
                    }
                    Invoice.DepositInvoice = InvoiceList.Where(x => x.Invoice_Status.Value == (int?)InvoiceStatus.Deposit).Select(x => x.Invoice_Total).Sum();
                    decimal? ToatlPaidOrDeposit = Invoice.PaidInvoice + Invoice.DepositInvoice;
                    if (ToatlPaidOrDeposit > 0)
                    {
                        Invoice.PaidInvoicePercentage = Invoice.PaidInvoice / ToatlPaidOrDeposit * 100 + "%";
                        Invoice.DepositInvoicePercentage = Invoice.DepositInvoice / ToatlPaidOrDeposit * 100 + "%";
                    }
                    ;

                    foreach (var item in InvoiceList.Where(x => x.Invoice_Status.Value == (int?)InvoiceStatus.NotPaid))
                    {
                        if (item.Invoice_Due_Date != null)
                        {
                            int check = DateComapre(item.Invoice_Due_Date.Value);
                            if (check > 0)
                            {
                                Invoice.OverDueInvoice += item.Invoice_Total;
                            }
                            else
                            {
                                Invoice.DueInvoice += item.Invoice_Total;
                            }
                        }
                    }
                    decimal? TotalOpenOrOverDue = Invoice.DueInvoice + Invoice.OverDueInvoice;
                    if (TotalOpenOrOverDue > 0)
                    {

                       decimal DueInvoice =(decimal)Invoice.DueInvoice / (decimal)TotalOpenOrOverDue * 100;
                            Invoice.DueOverDueInvoicePercentage = DueInvoice + "%";

                    }
                    Result.Invoice = Invoice;
                    ProfitAndLoss.Income += (decimal)Invoice.TotalInvoice;
                    if (Invoice.TotalInvoice>0)
                    Result.ProfitAndLoss.NetIncome +=(decimal)Invoice.TotalInvoice;
                }
                custom_Response.Data = Result;
                custom_Response.HttpStatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                custom_Response.Data = null;
                custom_Response.Message = Message.GettingErrorWhileBindList;
                custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
            }
            return custom_Response;
        }
        public int DateComapre(DateTime? Date)
        {
            int Result = 0;
            DateTime CurrentDate = DateTime.Now;
            try
            {
                if (CurrentDate.Date < Date.Value.Date)
                {
                    Result = -1;
                }
                else if (CurrentDate.Date == Date.Value.Date)
                {
                    Result = 0;
                }
                else if (CurrentDate.Date > Date.Value.Date)
                {
                    Result = 1;
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return Result;
        }
    }
}