﻿using Avigma.Repository.Lib;
using IPL.Models.Account;
using IPL.Repository.Account.Reports_Templates;
using NReco.PdfGenerator;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace IPL.Repository.Account
{
    public class Acc_IncomeStatement_Data
    {
        Acc_Ledger_Header_Data acc_Ledger_MasterData = new Acc_Ledger_Header_Data();
        Log log = new Log();
        ReportsPDFGenerater PDF = new ReportsPDFGenerater();
        public async Task<IEnumerable<Acc_IncomeStatement_DTO>> IncomeStatement(Acc_Report_Filter filter)
        {
            var revenuesExpenses = new HashSet<Acc_IncomeStatement_DTO>();
            try
            {
                Acc_Ledger_Headers_DTO acc_Ledger_DTO = new Acc_Ledger_Headers_DTO();
                acc_Ledger_DTO.Type = 1;
                acc_Ledger_DTO.UserID = filter.UserId;
                IEnumerable<Acc_Ledger_Headers_DTO> acc_Ledger_Headers_DTOs = acc_Ledger_MasterData.GetLedgerEntries(acc_Ledger_DTO);

                var revenues = acc_Ledger_Headers_DTOs.Where(x => x.Acc_Account_Type == (int)AccountType.Income).GroupBy(x => new { x.Acc_AccountID, x.Acc_Account_Type, x.Acc_Account_Name, x.Acc_Account_Code,x.LedgH_CreatedOn }).Select(Line => new Acc_IncomeStatement_DTO
                {
                    AccountId = Line.Key.Acc_AccountID,
                    AccountCode = Line.Key.Acc_Account_Code,
                    AccountName = Line.Key.Acc_Account_Name,
                    Amount = Line.Sum(s => s.LedgE_DrCr == (int)DrOrCrSide.Dr ? (s.Acc_DrOrCr_Side == (int)DrOrCrSide.Dr ? s.DebitAmount : -(s.DebitAmount)) : (s.Acc_DrOrCr_Side == (int)DrOrCrSide.Dr ? -(s.CreditAmount) : s.CreditAmount)),
                    CreatedDate = Line.Key.LedgH_CreatedOn
                });
                if (filter.StartDate != null && filter.EndDate != null)
                    revenues = revenues.Where(x => x.CreatedDate >= filter.StartDate && x.CreatedDate <= filter.EndDate);

                var expenses = acc_Ledger_Headers_DTOs.Where(x => x.Acc_Account_Type == (int)AccountType.Expense || x.Acc_Account_Type == (int)AccountType.OtherExpense).GroupBy(x => new { x.Acc_AccountID, x.Acc_Account_Type, x.Acc_Account_Name, x.Acc_Account_Code,x.LedgH_CreatedOn }).Select(Line => new Acc_IncomeStatement_DTO
                {
                    AccountId = Line.Key.Acc_AccountID,
                    AccountCode = Line.Key.Acc_Account_Code,
                    AccountName = Line.Key.Acc_Account_Name,
                    Amount = Line.Sum(s => s.LedgE_DrCr == (int)DrOrCrSide.Dr ? (s.Acc_DrOrCr_Side == (int)DrOrCrSide.Dr ? s.DebitAmount : -(s.DebitAmount)) : (s.Acc_DrOrCr_Side == (int)DrOrCrSide.Dr ? -(s.CreditAmount) : s.CreditAmount)),
                    CreatedDate = Line.Key.LedgH_CreatedOn
                });
                if (filter.StartDate != null && filter.EndDate != null)
                    expenses = expenses.Where(x => x.CreatedDate >= filter.StartDate && x.CreatedDate <= filter.EndDate);
                foreach (var revenue in revenues)
                {
                    revenue.IsExpense = false;
                    revenuesExpenses.Add(revenue);
                }
                foreach (var expense in expenses)
                {
                    expense.IsExpense = true;
                    revenuesExpenses.Add(expense);
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return await Task.FromResult(revenuesExpenses);
        }
       
        public async Task<Custom_Response> GeneratePDFStringIncomeStatement(Acc_Report_Filter filter)
        {
            Custom_Response custom = new Custom_Response();
            CultureInfo us = CultureInfo.GetCultureInfo("en-US");
            us = CultureInfo.CreateSpecificCulture("en-US");
            us.NumberFormat.CurrencyNegativePattern = 1;
            Thread.CurrentThread.CurrentCulture = us;
            Thread.CurrentThread.CurrentUICulture = us;
            try
            {
                StringBuilder html = new StringBuilder();
                StringBuilder table = new StringBuilder();
                string theader = "";
                string tfooter = "";
                int index = 0;
                string tbody = "<tbody>";

                theader += "<table width = '100%' border='1' cellpadding='1' cellspacing='0' bgcolor='#efefef'>";
                table.Append(theader);
                tbody += "<tr> <td  bgcolor = '#CCCCCC' style='text-align:center;height:50px;  border: none;'><span class='MsoNormal1' style='text-align:center; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px'><b><u> </u></b></span>  <div align='center;' style='font - family: Verdana, Arial, Helvetica, sans - serif; font - size: 10px; '>    <span class='MsoNormal1' style='text - align:center; font - size: 35px'><b><u> Income  Statement</u></b></span></div> </td></tr>";
                tbody += "<tr><td ><table width='100%'  border='0'> <tr height ='30px'><td width='10%' bgcolor='#CCCCCC' style='font-size: 12px'><div align='center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'>&nbsp;Sr No </span></b></span></div><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'></span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align='center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'>&nbsp; Account code </span></b></span></div><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align='center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;' > &nbsp; Account Name </span ></b ></span ></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Credit</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td>  <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp;  Debit</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td>     </tr>";

                IEnumerable<Acc_IncomeStatement_DTO> acc_IncomeStatement_DTO = await IncomeStatement(filter);
                decimal TotalRevenue = 0;
                decimal TotalExpence = 0;
                foreach (var item in acc_IncomeStatement_DTO.ToList())
                {
                    switch (filter.ReportsType)
                    {
                        case Reports.IncomeStatement:

                            var sr = 1 + index++;

                            tbody += "<tr height ='30px'> <td style = 'text-align: center;'width='10%'>" + sr + "</td ><td style = 'text-align: center;'>" + getExpense(item.IsExpense) + "</td ><td style = 'text-align: center;'>" + item.AccountCode + "</td ><td style = 'text-align: center;'>" + item.AccountName + "</td><td style = 'text-align: center;'>" + string.Format(us, "{0:C}", item.Amount) + "%" + "</td></tr>";




                            if (item.IsExpense)
                                TotalExpence += item.Amount;
                            if (!item.IsExpense)
                                TotalRevenue += item.Amount;
                            break;
                          
                        default:
                            break;
                    }

                }
                tbody += @"<tr>
                                <td colspan = '4' style='text-align:right;border: solid black 1px'><b>Net Income</b></td>" +
                                    "<td style='text-align:center;border: solid black 1px'>" + string.Format(us, "{0:C}", TotalRevenue - TotalExpence) + "</td></tr>";
                tbody += @"<tr>
                                <td  colspan = '4' style='text-align:right;border: solid black 1px'><b>Total Revenue</b></td>" +
                                   "<td style='text-align:center;border: solid black 1px'>" + string.Format(us, "{0:C}", TotalRevenue) + "</td></tr>";
                tbody += @"<tr>
                                <td colspan = '4' style='text-align:right;border: solid black 1px'><b>Total Expense</b></td>" +
                                   "<td style='text-align:center;border: solid black 1px'>" + string.Format(us, "{0:C}", TotalExpence) + "</td></tr>";
                tbody += "</tbody>";
                table.Append(tbody);
                html.Append(table.ToString());
                byte[] pdffile = PDF.GenerateRuntimePDF(html.ToString());
                custom.Data = pdffile;
                custom.HttpStatusCode = System.Net.HttpStatusCode.OK;
                return custom;
            }
            catch (Exception ex)
            {
                custom.Message = ex.Message;
                custom.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return custom;
            }
        }
        public string getExpense(bool IsExpence)
        {
            return IsExpence ? "Expenses" : "Revenues";
        }
    }
}