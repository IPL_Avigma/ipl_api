﻿using Avigma.Repository.Lib;
using IPL.HelperMessage;
using IPL.Models.Account;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;

namespace IPL.Repository.Account
{
    public class Acc_Journal_Entry_Headers_Data
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        Acc_Account_MasterData acc_Account_MasterData = new Acc_Account_MasterData();
        public IEnumerable<Acc_Journal_Entry_Headers_DTO> GetJournalEntries(Acc_Journal_Entry_Headers_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Acc_Get_JournalEntries]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@ID", 1 + "#bigint#" + model.JrnlH_pkeyId);
                input_parameters.Add("@WhereClause", 1 + "#varchar#" + model.WhereClause);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                ds = obj.SelectSql(selectProcedure, input_parameters);
                try
                {
                    if (ds.Tables.Count > 0 && ds.Tables[0] != null)
                    {

                        var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                        List<Acc_Journal_Entry_Headers_DTO> AccountDetail = new List<Acc_Journal_Entry_Headers_DTO>();
                        Acc_Journal_Entry_Headers_DTO acc_Journal_Entry_Headers_DTO = null;
                        Int64 lastid = 0;
                        if (myEnumerableFeaprd.Count() > 0)
                        {
                            foreach (var item in myEnumerableFeaprd)
                            {
                                //Get data with journal entery(child data)
                                if (lastid != item.Field<Int64>("JrnlH_pkeyId"))
                                {
                                    if (acc_Journal_Entry_Headers_DTO != null)
                                    {
                                        AccountDetail.Add(acc_Journal_Entry_Headers_DTO);
                                    }
                                    lastid = item.Field<Int64>("JrnlH_pkeyId");
                                    acc_Journal_Entry_Headers_DTO = new Acc_Journal_Entry_Headers_DTO(item);

                                }
                                else
                                {
                                    acc_Journal_Entry_Headers_DTO.JrnlH_JournalEntry.Add(new Acc_Journal_Entry_Lines_DTO(item));
                                }
                                acc_Journal_Entry_Headers_DTO.JrnlH_CreatedByName = item.Field<string>("JrnlH_CreatedByName");
                                acc_Journal_Entry_Headers_DTO.JrnlH_ModifiedByName = item.Field<string>("JrnlH_ModifiedByName");
                            }
                            AccountDetail.Add(acc_Journal_Entry_Headers_DTO);

                            //check data for post
                            foreach (Acc_Journal_Entry_Headers_DTO item in AccountDetail)
                            {
                                if (item != null)
                                {

                                    //==1 is for debit
                                    item.DebitAmount = item.JrnlH_JournalEntry.Where(x => x.JrnlE_DrCr == 1).Select(x => x.JrnlE__Amount).Sum();
                                    //==1 is for credit
                                    item.CreditAmount = item.JrnlH_JournalEntry.Where(x => x.JrnlE_DrCr == 2).Select(x => x.JrnlE__Amount).Sum();

                                    item.JrnlH_ReadyForPosting = CheckDataForPost(item);
                                }
                            }
                        }
                        return AccountDetail.AsEnumerable<Acc_Journal_Entry_Headers_DTO>();
                    }
                }
                catch (Exception ex)
                {
                    log.logErrorMessage(ex.StackTrace);
                    log.logErrorMessage(ex.Message);
                }
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return null;
        }


        private bool CheckDataForPost(Acc_Journal_Entry_Headers_DTO acc_Journal_Entry_Headers_DTO)
        {
            if (!acc_Journal_Entry_Headers_DTO.JrnlH_Posted && acc_Journal_Entry_Headers_DTO.JrnlH_JournalEntry.Count >= 2 && acc_Journal_Entry_Headers_DTO.DebitAmount == acc_Journal_Entry_Headers_DTO.CreditAmount && acc_Journal_Entry_Headers_DTO.DebitAmount != 0)
            {
                return true;
            }
            return false;
        }

        public Custom_Response AddJournalData(Acc_Journal_Entry_Headers_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            Custom_Response custom_Response = new Custom_Response();

            string insertProcedure = "[Acc_CreateUpdate_Journal]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@JrnlH_pkeyId", 1 + "#bigint#" + model.JrnlH_pkeyId);
                input_parameters.Add("@JrnlH_Date", 1 + "#datetime#" + model.JrnlH_Date);
                input_parameters.Add("@JrnlH_ReferenceNo", 1 + "#varchar#" + model.JrnlH_ReferenceNo);
                input_parameters.Add("@JrnlH_Memo", 1 + "#varchar#" + model.JrnlH_Memo);
                input_parameters.Add("@JrnlH_Posted", 1 + "#bit#" + model.JrnlH_Posted);
                input_parameters.Add("@JrnlH_GeneralLedgerHeaderId", 1 + "#bigint#" + model.JrnlH_GeneralLedgerHeaderId);
                input_parameters.Add("@JrnlH_IsActive", 1 + "#bit#" + model.JrnlH_IsActive);
                input_parameters.Add("@JrnlH_IsDelete", 1 + "#bit#" + model.JrnlH_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Jrnl_pkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);
                input_parameters.Add("@Jrnl_Message", 2 + "#varchar#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    custom_Response.Message = Message.ErrorWhileSaving;
                    custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                }
                else
                {
                    custom_Response.Message = Message.SuccessSave;
                    if (model.Type == 3)
                    {
                        custom_Response.Message = Message.SuccessDelete;
                    }
                    custom_Response.HttpStatusCode = HttpStatusCode.OK;

                    foreach (var item in model.JrnlH_JournalEntry)
                    {
                        if (model.JrnlH_pkeyId == 0)
                        {
                            item.JrnlE_JournalEntryHeaderId = Convert.ToInt64(objData[0]);
                        }
                        else
                        {
                            item.JrnlE_JournalEntryHeaderId = model.JrnlH_pkeyId;
                        }
                        item.Type = model.Type;
                        item.JrnlE_pkeyId = model.JrnlH_pkeyId;
                        item.UserID = model.UserID;
                        custom_Response = AddJournalEntryData(item);
                        if (custom_Response.HttpStatusCode != HttpStatusCode.OK)
                        {
                            return custom_Response;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                custom_Response.Message = Message.ErrorWhileSaving;
                custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return custom_Response;
        }

        public Custom_Response AddJournalEntryData(Acc_Journal_Entry_Lines_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            Custom_Response custom_Response = new Custom_Response();

            string insertProcedure = "[Acc_CreateUpdate_Journal_Entry]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@JrnlE_pkeyId", 1 + "#bigint#" + model.JrnlE_pkeyId);
                input_parameters.Add("@JrnlE_DrCr", 1 + "#int#" + model.JrnlE_DrCr);
                input_parameters.Add("@JrnlE_Amount", 1 + "#decimal#" + model.JrnlE__Amount);
                input_parameters.Add("@JrnlE_Memo", 1 + "#varchar#" + model.JrnlE_Memo);
                input_parameters.Add("@JrnlE_Name", 1 + "#varchar#" + model.JrnlE_Name);
                input_parameters.Add("@JrnlE_Class", 1 + "#varchar#" + model.JrnlE_Class);
                input_parameters.Add("@JrnlE_JournalEntryHeaderId", 1 + "#bigint#" + model.JrnlE_JournalEntryHeaderId);
                input_parameters.Add("@JrnlE_AccountId", 1 + "#bigint#" + model.JrnlE_AccountId);
                input_parameters.Add("@JrnlE_IsActive", 1 + "#bit#" + model.JrnlE_IsActive);
                input_parameters.Add("@JrnlE_IsDelete", 1 + "#bit#" + model.JrnlE_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Jrnl_pkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    custom_Response.Message = Message.ErrorWhileSaving;
                    custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                }
                else
                {
                    custom_Response.Message = Message.SuccessSave;
                    custom_Response.HttpStatusCode = HttpStatusCode.OK;
                }
            }
            catch (Exception ex)
            {
                custom_Response.Message = Message.ErrorWhileSaving;
                custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return custom_Response;
        }

        public Custom_Response PostJournal(int Id, long UserId)
        {
            Custom_Response response = null;

            try
            {
                Acc_Journal_Entry_Headers_DTO acc_Journal_Entry_Headers_DTO = new Acc_Journal_Entry_Headers_DTO();
                acc_Journal_Entry_Headers_DTO.Type = 2;
                acc_Journal_Entry_Headers_DTO.UserID = UserId;
                acc_Journal_Entry_Headers_DTO.JrnlH_pkeyId = Id;
                IEnumerable<Acc_Journal_Entry_Headers_DTO> acc_Journal_Entry_Headers_DTOs = GetJournalEntries(acc_Journal_Entry_Headers_DTO);

                if (acc_Journal_Entry_Headers_DTOs.Count() > 0)
                {
                    acc_Journal_Entry_Headers_DTO = acc_Journal_Entry_Headers_DTOs.FirstOrDefault();
                    response = ValidateGeneralLedgerEntry(acc_Journal_Entry_Headers_DTO);
                    if (response.HttpStatusCode != HttpStatusCode.OK)
                    {
                        return response;
                    }
                    else
                    {
                        acc_Journal_Entry_Headers_DTO.JrnlH_Posted = true;
                        if (acc_Journal_Entry_Headers_DTO.JrnlH_GeneralLedgerHeaderId == null || acc_Journal_Entry_Headers_DTO.JrnlH_GeneralLedgerHeaderId == 0)
                        {
                            Acc_Ledger_Headers_DTO acc_Ledger_Headers_DTO = new Acc_Ledger_Headers_DTO();
                            acc_Ledger_Headers_DTO.Type = 1;
                            acc_Ledger_Headers_DTO.JournalId = acc_Journal_Entry_Headers_DTO.JrnlH_pkeyId;
                            acc_Ledger_Headers_DTO.LedgH_Decription = acc_Journal_Entry_Headers_DTO.JrnlH_ReferenceNo;
                            acc_Ledger_Headers_DTO.LedgH_Date = DateTime.Now;
                            acc_Ledger_Headers_DTO.UserID = UserId;
                            acc_Ledger_Headers_DTO.LedgerEntry = new List<Acc_Ledger_Lines_DTO>();
                            Acc_Ledger_Header_Data acc_Ledger_Header_Data = new Acc_Ledger_Header_Data();

                            foreach (var item in acc_Journal_Entry_Headers_DTO.JrnlH_JournalEntry)
                            {
                                Acc_Ledger_Lines_DTO acc_Ledger_Lines_DTO = new Acc_Ledger_Lines_DTO();
                                acc_Ledger_Lines_DTO.LedgE_AccountId = item.JrnlE_AccountId;
                                acc_Ledger_Lines_DTO.LedgE_DrCr = item.JrnlE_DrCr;
                                acc_Ledger_Lines_DTO.LedgE_Amount = item.JrnlE__Amount;
                                acc_Ledger_Headers_DTO.LedgerEntry.Add(acc_Ledger_Lines_DTO);
                            }
                            response = acc_Ledger_Header_Data.AddLedgerData(acc_Ledger_Headers_DTO);
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return response;
        }

        private Custom_Response ValidateGeneralLedgerEntry(Acc_Journal_Entry_Headers_DTO glEntry)
        {
            Custom_Response custom_Response = new Custom_Response();
            try
            {

                var totalDebit = glEntry.JrnlH_JournalEntry.Where(x => x.JrnlE_DrCr == (int)DrOrCrSide.Dr).Sum(x => x.JrnlE__Amount);
                var totalCredit = glEntry.JrnlH_JournalEntry.Where(x => x.JrnlE_DrCr == (int)DrOrCrSide.Cr).Sum(x => x.JrnlE__Amount);
                if (!((totalDebit - totalCredit) == 0))
                {
                    custom_Response.Message = Message.DebitCreditAreNotEqual;
                    custom_Response.HttpStatusCode = HttpStatusCode.BadRequest;
                    return custom_Response;
                }
                else if (totalCredit == 0 || totalDebit == 0)
                {
                    custom_Response.Message = Message.OneOrMoreLineZero;
                    custom_Response.HttpStatusCode = HttpStatusCode.BadRequest;
                    return custom_Response;
                }

                var duplicateAccounts = glEntry.JrnlH_JournalEntry
                  .GroupBy(gl => gl.JrnlE_AccountId)
                  .Where(gl => gl.Count() > 1);

                if (duplicateAccounts.Any())
                {
                    custom_Response.Message = Message.DuplicateIdCollection;
                    custom_Response.HttpStatusCode = HttpStatusCode.BadRequest;
                    return custom_Response;
                }
                var assetsAmountList = glEntry.JrnlH_JournalEntry.Where(x => (x.JrnlE_AccountType == (int)
                AccountType.FixedAsset || x.JrnlE_AccountType == (int)
                AccountType.OtherAsset || x.JrnlE_AccountType == (int)
                AccountType.OthercurrentAsset)).ToList();
                var liabilitiesAmountList = glEntry.JrnlH_JournalEntry.Where(x => x.JrnlE_AccountType == (int)AccountType.Liabilities).ToList();
                var equitiesAmountList = glEntry.JrnlH_JournalEntry.Where(x => x.JrnlE_AccountType == (int)AccountType.Equity).ToList();

                decimal assetsAmount = assetsAmountList != null ? assetsAmountList.Sum(a => a.JrnlE__Amount) : 0;
                decimal liabilitiesAmount = liabilitiesAmountList != null ? liabilitiesAmountList.Sum(a => a.JrnlE__Amount) : 0;
                decimal equitiesAmount = equitiesAmountList != null ? equitiesAmountList.Sum(a => a.JrnlE__Amount) : 0;

                if (!(assetsAmount == liabilitiesAmount + equitiesAmount))
                {
                    custom_Response.Message = Message.OneOfTheAccountNotEqual;
                    custom_Response.HttpStatusCode = HttpStatusCode.BadRequest;
                    return custom_Response;
                }
                custom_Response.HttpStatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return custom_Response;

        }


    }
}