﻿using Avigma.Repository.Lib;
using IPL.HelperMessage;
using IPL.Models.Account;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;

namespace IPL.Repository.Account
{
    public class Acc_Ledger_Header_Data
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        public IEnumerable<Acc_Ledger_Headers_DTO> GetLedgerEntries(Acc_Ledger_Headers_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Acc_Get_LedgerEntries]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@ID", 1 + "#bigint#" + model.LedgH_pkeyId);
                input_parameters.Add("@WhereClause", 1 + "#varchar#" + model.WhereClause);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                ds = obj.SelectSql(selectProcedure, input_parameters);
                try
                {
                    if (ds.Tables.Count > 0 && ds.Tables[0] != null)
                    {

                        var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                        List<Acc_Ledger_Headers_DTO> LedgerDetail = new List<Acc_Ledger_Headers_DTO>();
                        foreach (var item in myEnumerableFeaprd)
                        {
                            LedgerDetail.Add(new Acc_Ledger_Headers_DTO(item));
                        }
                        return LedgerDetail.OrderByDescending(x => x.LedgH_Date).ThenBy(x => x.LedgH_pkeyId).AsEnumerable<Acc_Ledger_Headers_DTO>();
                    }
                }
                catch (Exception ex)
                {
                    log.logErrorMessage(ex.StackTrace);
                    log.logErrorMessage(ex.Message);
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return null;
        }
        public Custom_Response AddLedgerData(Acc_Ledger_Headers_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            Custom_Response custom_Response = new Custom_Response();

            string insertProcedure = "[Acc_CreateUpdate_Ledger]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@LedgH_pkeyId", 1 + "#bigint#" + model.LedgH_pkeyId);
                input_parameters.Add("@LedgH_JournalId", 1 + "#bigint#" + model.JournalId);
                input_parameters.Add("@LedgH_Date", 1 + "#datetime#" + model.LedgH_Date);
                input_parameters.Add("@LedgH_Decription", 1 + "#varchar#" + model.LedgH_Decription);
                input_parameters.Add("@LedgH_IsActive", 1 + "#bit#" + model.LedgH_IsActive);
                input_parameters.Add("@LedgH_IsDelete", 1 + "#bit#" + model.LedgH_IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@LedgH_pkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);
                input_parameters.Add("@LedgH_Message", 2 + "#varchar#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    custom_Response.Message = Message.ErrorWhileSaving;
                    custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                }
                else
                {
                    custom_Response.Message = Message.SuccessSave;
                    custom_Response.HttpStatusCode = HttpStatusCode.OK;

                    foreach (var item in model.LedgerEntry)
                    {
                        item.LedgE_LedgerHeaderId = Convert.ToInt64(objData[0]);
                        item.Type = model.Type;
                        item.UserID = model.UserID;
                        custom_Response = AddLedgerEntryData(item);
                        if (custom_Response.HttpStatusCode != HttpStatusCode.OK)
                        {
                            return custom_Response;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                custom_Response.Message = Message.ErrorWhileSaving;
                custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return custom_Response;
        }




        public Custom_Response AddLedgerEntryData(Acc_Ledger_Lines_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            Custom_Response custom_Response = new Custom_Response();

            string insertProcedure = "[Acc_CreateUpdate_Ledger_Entry]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@LedgE_pkeyId", 1 + "#bigint#" + model.LedgE_pkeyId);
                input_parameters.Add("@LedgE_DrCr", 1 + "#int#" + model.LedgE_DrCr);
                input_parameters.Add("@LedgE_Amount", 1 + "#decimal#" + model.LedgE_Amount);
                input_parameters.Add("@LedgE_AccountId", 1 + "#varchar#" + model.LedgE_AccountId);
                input_parameters.Add("@LedgE_LedgerHeaderId", 1 + "#bigint#" + model.LedgE_LedgerHeaderId);
                input_parameters.Add("@LedgE_IsActive", 1 + "#bit#" + model.LedgE_IsActive);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@LedgE_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);
                input_parameters.Add("@LedgH_Message", 2 + "#varchar#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    custom_Response.Message = Message.ErrorWhileSaving;
                    custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                }
                else
                {
                    custom_Response.Message = Message.SuccessSave;
                    custom_Response.HttpStatusCode = HttpStatusCode.OK;
                }
            }
            catch (Exception ex)
            {
                custom_Response.Message = Message.ErrorWhileSaving;
                custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return custom_Response;
        }

    }
}