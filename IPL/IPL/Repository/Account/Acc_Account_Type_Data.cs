﻿using Avigma.Repository.Lib;
using IPL.Models.Account;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;

namespace IPL.Repository.Account
{
    public class Acc_Account_Type_Data
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public Custom_Response GetDropDownAccountTypeData(DropDownAccountTypeDTO model)
        {
            Custom_Response Custom_Response = new Custom_Response();
             DataSet ds = null;
            try
            {
                string selectProcedure = "[Acc_Get_DropDown_Account_Type]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
                if (ds.Tables.Count > 0 && ds.Tables[0] != null)
                {
                    var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                    List<AccountTypeDto> AccountTypeDetail =
                       (from item in myEnumerableFeaprd
                        select new AccountTypeDto
                        {
                            Acc_Type_pkeyId = item.Field<Int64>("Acc_Type_pkeyId"),
                            Acc_Account_Type = item.Field<String>("Acc_Account_Type"),

                        }).ToList();
                    Custom_Response.Data = AccountTypeDetail.AsEnumerable<AccountTypeDto>();
                    Custom_Response.HttpStatusCode = HttpStatusCode.OK;
                    return Custom_Response;
                }
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return null;
        }
        public Custom_Response GetDropDownAccountDetailsData(DropDownAccountTypeDTO model)
        {
            Custom_Response Custom_Response = new Custom_Response();
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Acc_Get_DropDown_Account_Details_ByAccount_Type_Id]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Acc_Account_Type_Id", 1 + "#int#" + model.Acc_TypeID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);
                if (ds.Tables.Count > 0 && ds.Tables[0] != null)
                {
                    var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                    List<AccountTypeDetailsDto> AccountTypeDetail =
                       (from item in myEnumerableFeaprd
                        select new AccountTypeDetailsDto
                        {
                            Acc_Type_pkeyId = item.Field<Int64>("Acc_Type_pkeyId"),
                            Acc_Detail_Type = item.Field<String>("Acc_Detail_Type"),
                            Acc_Account_Type_Description = item.Field<String>("Acc_Account_Type_Description"),

                        }).ToList();
                    Custom_Response.Data = AccountTypeDetail.AsEnumerable<AccountTypeDetailsDto>();
                    Custom_Response.HttpStatusCode = HttpStatusCode.OK;
                    return Custom_Response;
                }
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return null;
        }
    }
}