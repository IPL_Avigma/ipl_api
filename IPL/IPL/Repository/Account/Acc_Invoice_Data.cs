﻿using Avigma.Repository.Lib;
using IPL.HelperMessage;
using IPL.Models.Account;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Web;

namespace IPL.Repository.Account
{
    public class Acc_Invoice_Data
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public IEnumerable<Acc_Invoice_DTO> GetInvoiceMaster(Acc_Invoice_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Acc_Get_Invoices]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@ID", 1 + "#bigint#" + model.Invoice_pkeyId);
                input_parameters.Add("@WhereClause", 1 + "#varchar#" + model.WhereClause);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                ds = obj.SelectSql(selectProcedure, input_parameters);

                try
                {
                    if (ds.Tables.Count > 0 && ds.Tables[0] != null)
                    {

                        var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                        List<Acc_Invoice_DTO> InvoiceDetail =
                           (from item in myEnumerableFeaprd
                            select new Acc_Invoice_DTO
                            {
                                Invoice_pkeyId = item.Field<Int64>("Invoice_pkeyId"),
                                Invoice_Number = item.Field<String>("Invoice_Number"),
                                Invoice_CustomeId = item.Field<long?>("Invoice_CustomeId"),
                                Invoice_Custome_Email = item.Field<string>("Invoice_Custome_Email"),
                                Invoice_Date = item.Field<DateTime?>("Invoice_Date"),
                                Invoice_Due_Date = item.Field<DateTime?>("Invoice_Due_Date"),
                                Invoice_Sub_total = item.Field<decimal?>("Invoice_Sub_total"),
                                Invoice_Taxble_Amount = item.Field<decimal?>("Invoice_Taxble_Amount"),
                                Invoice_Total = item.Field<decimal?>("Invoice_Total"),
                                Invoice_Balance_Due = item.Field<decimal?>("Invoice_Balance_Due"),
                                Invoice_Custome_Name = item.Field<string>("Customer_Company_Name"),
                                Invoice_Status = item.Field<int?>("Invoice_Status"),
                                Invoice_CreatedOn= item.Field<DateTime?>("Invoice_CreatedOn"),
                                Inv_CreatedBy = item.Field<String>("Inv_CreatedBy"),
                                Inv_ModifiedBy = item.Field<String>("Inv_ModifiedBy"),
                            }).ToList();
                        return InvoiceDetail.AsEnumerable<Acc_Invoice_DTO>();
                    }
                }
                catch (Exception ex)
                {
                    log.logErrorMessage(ex.StackTrace);
                    log.logErrorMessage(ex.Message);
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return null;
        }
        public Acc_Journal_Reports_DTO GetInvoiceMasterReports(Acc_Invoice_DTO model)
        {
            DataSet ds = null;
            Acc_Journal_Reports_DTO acc_Journal_Reports_DTO = new Acc_Journal_Reports_DTO();
            try
            {
                string selectProcedure = "[Acc_Get_Invoices_For_Reports]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@ID", 1 + "#bigint#" + model.Invoice_pkeyId);
                input_parameters.Add("@WhereClause", 1 + "#varchar#" + model.WhereClause);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                ds = obj.SelectSql(selectProcedure, input_parameters);
                try
                {
                    if (ds.Tables.Count > 0 && ds.Tables[0] != null)
                    {

                        var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                        List<Acc_Journal_Invoice_Reports_DTO> InvoiceDetail =
                           (from item in myEnumerableFeaprd
                            select new Acc_Journal_Invoice_Reports_DTO
                            {
                                Invoice_pkeyId = item.Field<Int64>("Invoice_pkeyId"),
                                Invoice_Number = item.Field<String>("Invoice_Number"),
                                Invoice_CustomeId = item.Field<long>("Invoice_CustomeId"),
                                Invoice_Custome_Email = item.Field<string>("Invoice_Custome_Email"),
                                Invoice_Date = item.Field<DateTime?>("Invoice_Date"),
                                Invoice_Due_Date = item.Field<DateTime?>("Invoice_Due_Date"),
                                Invoice_Sub_total = item.Field<decimal?>("Invoice_Sub_total"),
                                Invoice_Taxble_Amount = item.Field<decimal?>("Invoice_Taxble_Amount"),
                                Invoice_Total = item.Field<decimal?>("Invoice_Total"),
                                Invoice_Balance_Due = item.Field<decimal?>("Invoice_Balance_Due"),
                                Invoice_Custome_Name = item.Field<string>("Customer_Company_Name"),
                                Invoice_Status = item.Field<int?>("Invoice_Status"),
                                Invoice_CreatedOn= item.Field<DateTime>("Invoice_CreatedOn"),
                                Invoice_Rec_Payment = item.Field<decimal?>("Invoice_Rec_Payment"),
                                DeposiToAccount = item.Field<string>("DeposiToAccount"),
                                Invoice_Rec_PkeyId= item.Field<long?>("Invoice_Rec_PkeyId"),
                                Invoice_Message= item.Field<string>("Invoice_Message"),
                                ReceivePayment_Memo = item.Field<string>("Invoice_Rec_Memo"),
                                ReceivedPaymentDate= item.Field<DateTime?>("ReceivedPaymentDate"),
                            }).ToList();
                        acc_Journal_Reports_DTO.Acc_Journal_Invoice_Reports_DTO = InvoiceDetail.ToList();
                        return acc_Journal_Reports_DTO;
                    }
                }
                catch (Exception ex)
                {
                    log.logErrorMessage(ex.StackTrace);
                    log.logErrorMessage(ex.Message);
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return null;
        }

        

        public Custom_Response AddInvoiceData(Acc_Invoice_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            Custom_Response custom_Response = new Custom_Response();

            string insertProcedure = "[Acc_CreateUpdate_Invoice]";
            if (model.Invoice_pkeyId == 0)
            {
                model.Type = 1;
                model.Invoice_Date =  DateTime.Now;
            }
            else
            {
                if (model.Invoice_pkeyId > 0 && (Boolean)!model.Invoice_IsDelete)
                    model.Type = 2;
                else
                    model.Type = 3;
            }
            if (model.Invoice_Items != null && model.Invoice_Items.Count > 0)
            {

                model.Invoice_Sub_total = (decimal)model.Invoice_Items.Select(x => x.Amount).ToList().Sum();
                model.Invoice_Taxble_Amount = model.Invoice_Taxble_Amount == null ? 0:model.Invoice_Taxble_Amount;
                model.Invoice_Total = (decimal)model.Invoice_Items.Select(x => x.Amount).ToList().Sum() + model.Invoice_Taxble_Amount;
                model.Invoice_Balance_Due = model.Invoice_Total;
            }

           Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Invoice_pkeyId", 1 + "#bigint#" + model.Invoice_pkeyId);
                input_parameters.Add("@Invoice_Number", 1 + "#nvarchar#" + model.Invoice_Number);
                input_parameters.Add("@Invoice_CustomeId", 1 + "#bigint#" + model.Invoice_CustomeId);
                input_parameters.Add("@Invoice_Custome_Email", 1 + "#nvarchar#" + model.Invoice_Custome_Email);
                input_parameters.Add("@Invoice_Send_to", 1 + "#bit#" + model.Invoice_Send_to);
                input_parameters.Add("@Invoice_Send_to_DateTime", 1 + "#datetime#" + model.Invoice_Send_to_DateTime);
                input_parameters.Add("@Invoice_CcBcc_Label", 1 + "#nvarchar#" + model.Invoice_CcBcc_Label);
                input_parameters.Add("@Invoice_Billing_Address", 1 + "#nvarchar#" + model.Invoice_Billing_Address);
                input_parameters.Add("@Invoice_Date", 1 + "#datetime#" + model.Invoice_Date);
                input_parameters.Add("@Invoice_Due_Date", 1 + "#datetime#" + model.Invoice_Due_Date);
                input_parameters.Add("@Invoice_PO_No", 1 + "#nvarchar#" + model.Invoice_PO_No);
                input_parameters.Add("@Invoice_Message", 1 + "#nvarchar#" + model.Invoice_Message);
                input_parameters.Add("@Invoice_Statement", 1 + "#nvarchar#" + model.Invoice_Statement);
                input_parameters.Add("@Invoice_Sub_total", 1 + "#decimal#" + model.Invoice_Sub_total);
                input_parameters.Add("@Invoice_Taxble_Amount", 1 + "#decimal#" + model.Invoice_Taxble_Amount);
                input_parameters.Add("@Invoice_Taxble_Sub_total", 1 + "#nvarchar#" + model.Invoice_Taxble_Sub_total);
                input_parameters.Add("@Invoice_Total", 1 + "#decimal#" + model.Invoice_Total);
                input_parameters.Add("@Invoice_Balance_Due", 1 + "#decimal#" + model.Invoice_Balance_Due);
                input_parameters.Add("@Invoice_IsActive", 1 + "#bit#" + true);
                input_parameters.Add("@Invoice_IsDelete", 1 + "#bit#" + false);
                input_parameters.Add("@Invoice_Terms", 1 + "#nvarchar#" + model.Invoice_Terms);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Invoice_pkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);
                input_parameters.Add("@Invoice_Message_Out", 2 + "#varchar#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    custom_Response.Message = Message.ErrorWhileSaving;
                    custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                }
                else
                {
                    if ((Boolean)!model.Invoice_IsDelete)
                    {

                        custom_Response.Message = Message.SuccessSave;
                        custom_Response.HttpStatusCode = HttpStatusCode.OK;
                        foreach (var item in model.Invoice_Items)
                        {
                            if (model.Invoice_pkeyId == 0)
                            {
                                item.Invoice_Id = Convert.ToInt64(objData[0]);
                            }
                            else
                            {
                                item.Invoice_Id = model.Invoice_pkeyId;
                            }
                            item.Type = model.Type;
                            if (item.Invoice_Items_PkeyId > 0)
                            {
                                item.Type = 2;
                            }
                            else
                            {
                                item.Type = 1;
                            }
                            item.UserID = model.UserID;
                            custom_Response = AddInvoiceItemsData(item);
                            if (custom_Response.HttpStatusCode != HttpStatusCode.OK)
                            {
                                return custom_Response;
                            }
                        }
                    }
                    else
                    {
                        custom_Response.Message = Message.SuccessDelete;
                        custom_Response.HttpStatusCode = HttpStatusCode.OK;
                    }
                }
            }
            catch (Exception ex)
            {
                custom_Response.Message = Message.ErrorWhileSaving;
                custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return custom_Response;
        }
        public Custom_Response AddInvoicePaymentData(Acc_Invoice_Payment_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            Custom_Response custom_Response = new Custom_Response();

            string insertProcedure = "[Acc_CreateUpdate_Invoice_Payment]";
            if (model.Invoice_Pay_PkeyId == 0)
            {
                model.Type = 1;
            }
            else if (model.Invoice_Pay_PkeyId > 0 && model.Type == 0)
            {
                model.Type = 2;
            }
            else
            {
                model.Type = 3;

            }
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Invoice_Pay_PkeyId", 1 + "#bigint#" + model.Invoice_Pay_PkeyId);
                input_parameters.Add("@Invoice_Pay_Invoice_Id", 1 + "#bigint#" + model.Invoice_Pay_Invoice_Id);
                input_parameters.Add("@Invoice_Pay_Customer_Id", 1 + "#bigint#" + model.Invoice_Pay_Customer_Id);
                input_parameters.Add("@Invoice_Pay_Wo_Id", 1 + "#bigint#" + model.Invoice_Pay_Wo_Id);
                input_parameters.Add("@Invoice_Pay_Payment_Date", 1 + "#datetime#" + model.Invoice_Pay_Payment_Date);
                input_parameters.Add("@Invoice_Pay_Amount", 1 + "#decimal#" + model.Invoice_Pay_Amount);
                input_parameters.Add("@Invoice_Pay_CheckNumber", 1 + "#varchar#" + model.Invoice_Pay_CheckNumber);
                input_parameters.Add("@Invoice_Pay_Comment", 1 + "#varchar#" + model.Invoice_Pay_Comment);
                input_parameters.Add("@Invoice_Pay_Balance_Due", 1 + "#decimal#" + model.Invoice_Pay_Balance_Due);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Invoice_Payment_pkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);
                input_parameters.Add("@Invoice_Message_Out", 2 + "#varchar#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    custom_Response.Message = Message.ErrorWhileSaving;
                    custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                }
                else
                {
                    custom_Response.Message = Message.SuccessSave;
                    custom_Response.HttpStatusCode = HttpStatusCode.OK;
                    if (model.Type == 3)
                    {
                        custom_Response.Message = Message.SuccessDelete;
                    }
                }
            }
            catch (Exception ex)
            {
                custom_Response.Message = Message.ErrorWhileSaving;
                custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return custom_Response;
        }
        public Custom_Response CheckInvoiceReceivePayment(Acc_Invoice_Receive_Payment_DTO Data)
        {
            Custom_Response responce = new Custom_Response();
            try
            {
                Boolean IsValidAmount = false;
                List<Final_Invoice_Receive_Payment_DTO> FinalList = new List<Final_Invoice_Receive_Payment_DTO>();
                if (Data.Receive_Invoice_Items != null && Data.Receive_Invoice_Items.Count > 0)
                {

                    foreach (var item in Data.Receive_Invoice_Items)
                    {
                        responce = CheckInvoiceAmountValidOrNot((long)item.Invoice_Rec_Invoice_Id,(long)Data.UserID);
                        Receive_Invoice_Item receive_Invoice_Item = new Receive_Invoice_Item();
                        receive_Invoice_Item = responce.Data;
                        if (responce.HttpStatusCode == HttpStatusCode.OK && responce.Data != null)
                        {
                            if (receive_Invoice_Item.Invoice_Rec_Payment > 0)
                            {
                                receive_Invoice_Item.Invoice_Rec_Original_Amount = receive_Invoice_Item.Invoice_Rec_Original_Amount - receive_Invoice_Item.Invoice_Rec_Payment;
                                Data.Invoice_Rec_PkeyId =(long) receive_Invoice_Item.Invoice_Rec_PkeyId;
                                Data.Invoice_Rec_Deposit_To= receive_Invoice_Item.Invoice_Rec_Deposit_To>0?receive_Invoice_Item.Invoice_Rec_Deposit_To: Data.Invoice_Rec_Deposit_To;
                            }
                            if (item.Pending_Amount == receive_Invoice_Item.Invoice_Rec_Original_Amount)
                            {
                                IsValidAmount = true;
                                FinalList.Add(new Final_Invoice_Receive_Payment_DTO
                                {
                                    Invoice_Rec_PkeyId = Data.Invoice_Rec_PkeyId,
                                    Invoice_Rec_Customer_Id = Data.Invoice_Rec_Customer_Id,
                                    Invoice_Rec_Invoice_Id = item.Invoice_Rec_Invoice_Id,
                                    Invoice_Rec_Payment_Date = Data.Invoice_Rec_Payment_Date,
                                    Invoice_Rec_Original_Amount = item.Invoice_Rec_Original_Amount,
                                    Invoice_Rec_Payment = item.Pending_Amount,
                                    Invoice_Rec_Pending_Amount = item.Invoice_Rec_Original_Amount - item.Pending_Amount,
                                    Invoice_Rec_Payment_Method = Data.Invoice_Rec_Payment_Method,
                                    Invoice_Rec_Reference_No = Data.Invoice_Rec_Reference_No,
                                    Invoice_Rec_Deposit_To = Data.Invoice_Rec_Deposit_To,
                                    Invoice_Rec_EnteredBy = Data.UserID,
                                    UserID = Data.UserID,
                                });
                                ;
                            }
                            else
                            {
                                IsValidAmount = false;
                            }
                        }
                        else
                        {
                            IsValidAmount = false;
                        }
                    }

                }
                if (IsValidAmount)
                {
                    foreach (var item in FinalList)
                    {
                        responce = AddInvoiceReceivePaymentData(item);
                        if (responce.HttpStatusCode != HttpStatusCode.OK)
                        {
                            responce.Message = Message.SuccessSave;
                           // return responce;
                        }
                    }
                }
                else
                {
                    responce.Message = Message.InvoiceAmountNotMatch;
                    responce.HttpStatusCode = HttpStatusCode.NotAcceptable;
                    return responce;
                }
            }
            catch (Exception ex)
            {
                responce.Message = Message.ErrorWhileSaving;
                responce.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return responce;
        }
        public Custom_Response AddInvoiceReceivePaymentData(Final_Invoice_Receive_Payment_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            Custom_Response custom_Response = new Custom_Response();

            string insertProcedure = "[Acc_CreateUpdate_Invoice_Receive_Payment]";
            if (model.Invoice_Rec_PkeyId == 0)
            {
                model.Type = 1;
            }
            else if (model.Invoice_Rec_PkeyId > 0 && model.Type == 0)
            {
                model.Type = 2;
            }
            else
            {
                model.Type = 3;

            }
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Invoice_Rec_PkeyId", 1 + "#bigint#" + model.Invoice_Rec_PkeyId);
                input_parameters.Add("@Invoice_Rec_Customer_Id", 1 + "#bigint#" + model.Invoice_Rec_Customer_Id);
                input_parameters.Add("@Invoice_Rec_Invoice_Id", 1 + "#bigint#" + model.Invoice_Rec_Invoice_Id);
                input_parameters.Add("@Invoice_Rec_Payment_Date", 1 + "#datetime#" + model.Invoice_Rec_Payment_Date);
                input_parameters.Add("@Invoice_Rec_Original_Amount", 1 + "#decimal#" + model.Invoice_Rec_Original_Amount);
                input_parameters.Add("@Invoice_Rec_Payment", 1 + "#decimal#" + model.Invoice_Rec_Payment);
                input_parameters.Add("@Invoice_Rec_Pending_Amount", 1 + "#decimal#" + model.Invoice_Rec_Pending_Amount);
                input_parameters.Add("@Invoice_Rec_Payment_Method", 1 + "#int#" + model.Invoice_Rec_Payment_Method);
                input_parameters.Add("@Invoice_Rec_Reference_No", 1 + "#varchar#" + model.Invoice_Rec_Reference_No);
                input_parameters.Add("@Invoice_Rec_Deposit_To", 1 + "#bigint#" + model.Invoice_Rec_Deposit_To);
                input_parameters.Add("@Invoice_Rec_Memo", 1 + "#varchar#" + model.Invoice_Rec_Memo);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Invoice_Rec_pkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);
                input_parameters.Add("@Invoice_Message_Out", 2 + "#varchar#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    custom_Response.Message = Message.ErrorWhileSaving;
                    custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                }
                else
                {
                    custom_Response.Message = Message.SuccessSave;
                    custom_Response.HttpStatusCode = HttpStatusCode.OK;
                    if (model.Type == 3)
                    {
                        custom_Response.Message = Message.SuccessDelete;
                    }
                }
            }
            catch (Exception ex)
            {
                custom_Response.Message = Message.ErrorWhileSaving;
                custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return custom_Response;
        }
        public Custom_Response CheckInvoiceAmountValidOrNot(long InvoiceId,long UserID)
        {
            Custom_Response custom_Response = new Custom_Response();
            DataSet ds = null;
            string selectProcedure = "[Acc_Validate_Invoice_Amount]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            input_parameters.Add("@Invoice_Id", 1 + "#bigint#" + InvoiceId);
            input_parameters.Add("@UserID", 1 + "#bigint#" + UserID);
            ds = obj.SelectSql(selectProcedure, input_parameters);

            try
            {
                if (ds.Tables.Count > 0 && ds.Tables[0] != null)
                {
                    var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                    Receive_Invoice_Item InvoiceDetail =
                       (from item in myEnumerableFeaprd
                        select new Receive_Invoice_Item
                        {
                            Invoice_Rec_PkeyId = item.Field<Int64>("Invoice_Rec_PkeyId"),
                            Invoice_Rec_Invoice_Id = item.Field<Int64>("Invoice_pkeyId"),
                            Invoice_Rec_Deposit_To = item.Field<Int64>("Invoice_Rec_Deposit_To"),
                            Invoice_Number = item.Field<String>("Invoice_Number"),
                            Invoice_Rec_Original_Amount = item.Field<Decimal?>("Invoice_Total"),
                            Invoice_Status = item.Field<int>("Invoice_Status"),
                            Invoice_Rec_Payment = item.Field<Decimal?>("Invoice_Rec_Payment"),
                        }).FirstOrDefault();
                    custom_Response.Data = InvoiceDetail;
                    custom_Response.HttpStatusCode = HttpStatusCode.OK;
                    return custom_Response;
                }
            }
            catch (Exception ex)
            {
                custom_Response.Message = Message.GettingErrorWhileBindList;
                custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return custom_Response;
        }
        public Custom_Response AddInvoiceItemsData(Acc_Invoice_ChildDTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            Custom_Response custom_Response = new Custom_Response();

            string insertProcedure = "[Acc_CreateUpdate_Invoice_Items]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Invoice_Items_pkeyId", 1 + "#bigint#" + model.Invoice_Items_PkeyId);
                input_parameters.Add("@Invoice_Id", 1 + "#bigint#" + model.Invoice_Id);
                input_parameters.Add("@Invoice_Items_Task_Id", 1 + "#nvarchar#" + model.Task_Id);
                input_parameters.Add("@Invoice_Items_QTY", 1 + "#varchar#" + model.QTY);
                input_parameters.Add("@Invoice_Items_Amount", 1 + "#decimal#" + model.Amount);
                input_parameters.Add("@Invoice_Items_Rate", 1 + "#decimal#" + model.Rate);
                input_parameters.Add("@Invoice_Items_Tax", 1 + "#nvarchar#" + model.Tax);
                input_parameters.Add("@Invoice_Items_Class", 1 + "#nvarchar#" + model.Class);
                input_parameters.Add("@Invoice_Items_Description", 1 + "#nvarchar#" + model.Descp);
                input_parameters.Add("@Inv_Client_pkeyId", 1 + "#bigint#" + model.Inv_Client_Ch_pkeyId);
                input_parameters.Add("@Inv_Client_Ch_pkeyId", 1 + "#bigint#" + model.Inv_Client_Ch_pkeyId);
                input_parameters.Add("@Inv_Client_Ch_Wo_Id", 1 + "#bigint#" + model.Inv_Client_Ch_Wo_Id);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Invoice_Items_pkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    custom_Response.Message = Message.ErrorWhileSaving;
                    custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                }
                else
                {
                    custom_Response.Message = Message.SuccessSave;
                    custom_Response.HttpStatusCode = HttpStatusCode.OK;
                }
            }
            catch (Exception ex)
            {
                custom_Response.Message = Message.ErrorWhileSaving;
                custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return custom_Response;
        }

        public Acc_Invoice_DTO GetInvoice(Acc_Invoice_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Acc_Get_Invoice_Detail]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Invoice_pkeyId", 1 + "#bigint#" + model.Invoice_pkeyId);
                input_parameters.Add("@UserID", 1 + "#varchar#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);

                try
                {
                    if (ds.Tables.Count > 0 && ds.Tables[0] != null)
                    {
                        var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                        Acc_Invoice_DTO InvoiceDetail =
                       (from item in myEnumerableFeaprd
                        select new Acc_Invoice_DTO
                        {
                            Invoice_pkeyId = item.Field<Int64>("Invoice_pkeyId"),
                            Invoice_Number = item.Field<String>("Invoice_Number"),
                            Invoice_CustomeId = item.Field<long>("Invoice_CustomeId"),
                            Invoice_Custome_Email = item.Field<string>("Invoice_Custome_Email"),
                            Invoice_Send_to = item.Field<bool?>("Invoice_Send_to"),
                            Invoice_Send_to_DateTime = item.Field<DateTime?>("Invoice_Send_to_DateTime"),
                            Invoice_CcBcc_Label = item.Field<string>("Invoice_CcBcc_Label"),
                            Invoice_Billing_Address = item.Field<string>("Invoice_Billing_Address"),
                            Invoice_Terms = item.Field<string>("Invoice_Terms"),
                            Invoice_Date = item.Field<DateTime?>("Invoice_Date"),
                            Invoice_Due_Date = item.Field<DateTime?>("Invoice_Due_Date"),
                            Invoice_PO_No = item.Field<string>("Invoice_PO_No"),
                            Invoice_Statement = item.Field<string>("Invoice_Statement"),
                            Invoice_IsActive = item.Field<bool?>("Invoice_IsActive"),
                            Invoice_IsDelete = item.Field<bool?>("Invoice_IsDelete"),
                            Invoice_CreatedBy = item.Field<long>("Invoice_CreatedBy"),
                            Invoice_CreatedOn = item.Field<DateTime>("Invoice_CreatedOn"),
                            Invoice_ModifiedBy = item.Field<long?>("Invoice_ModifiedBy"),
                            Invoice_ModifiedOn = item.Field<DateTime?>("Invoice_ModifiedOn"),
                            Invoice_DeletedOn = item.Field<DateTime?>("Invoice_DeletedOn"),
                            Invoice_Message = item.Field<string>("Invoice_Message"),
                            Invoice_Sub_total = item.Field<decimal?>("Invoice_Sub_total"),
                            Invoice_Taxble_Sub_total = item.Field<string>("Invoice_Taxble_Sub_total"),
                            Invoice_Taxble_Amount = item.Field<decimal?>("Invoice_Taxble_Amount"),
                            Invoice_Total = item.Field<decimal?>("Invoice_Total"),
                            Invoice_Balance_Due = item.Field<decimal?>("Invoice_Balance_Due"),
                            Inv_Client_pkeyId = item.Field<Int64?>("Inv_Client_pkeyId"),
                            Invoice_Status = item.Field<int?>("Invoice_Status"),
                        }).FirstOrDefault();

                        if (ds.Tables.Count > 1 && ds.Tables[1] != null)
                        {
                            var InvoiceItemsList = ds.Tables[1].AsEnumerable();
                            List<Acc_Invoice_ChildDTO> ItemsList =
                             (from item in InvoiceItemsList
                              select new Acc_Invoice_ChildDTO
                              {
                                  Invoice_Items_PkeyId = item.Field<Int64>("Invoice_Items_pkeyId"),
                                  Invoice_Id = item.Field<Int64>("Invoice_Id"),
                                  Task_Id = item.Field<Int64?>("Task_Id"),
                                  Tax = item.Field<bool?>("Tax"),
                                  Class = item.Field<string>("Class"),
                                  Descp = item.Field<string>("Descp"),
                                  Amount = item.Field<decimal?>("Amount"),
                                  QTY = item.Field<string>("QTY"),
                                  Rate = item.Field<decimal?>("Rate"),
                                  Inv_Client_pkeyId = item.Field<Int64?>("Inv_Client_pkeyId"),
                                  Inv_Client_Ch_pkeyId = item.Field<Int64?>("Inv_Client_Ch_pkeyId"),
                                  Inv_Client_Ch_Wo_Id = item.Field<Int64?>("Inv_Client_Ch_Wo_Id"),
                              }).ToList();
                            InvoiceDetail.Invoice_Items = ItemsList;
                        }
                        if (ds.Tables.Count > 2 && ds.Tables[2] != null)
                        {
                            var InvoicePaymentList = ds.Tables[2].AsEnumerable();
                            List<Acc_Invoice_Payment_DTO> PaymentList =
                             (from item in InvoicePaymentList
                              select new Acc_Invoice_Payment_DTO
                              {
                                  Invoice_Pay_PkeyId = item.Field<Int64>("Invoice_Pay_PkeyId"),
                                  Invoice_Pay_Invoice_Id = item.Field<Int64>("Invoice_Pay_Invoice_Id"),
                                  Invoice_Pay_Customer_Id = item.Field<Int64>("Invoice_Pay_Customer_Id"),
                                  Invoice_Pay_Wo_Id = item.Field<Int64>("Invoice_Pay_Wo_Id"),
                                  Invoice_Pay_Payment_Date = item.Field<DateTime?>("Invoice_Pay_Payment_Date"),
                                  Invoice_Pay_Amount = item.Field<decimal?>("Invoice_Pay_Amount"),
                                  Invoice_Pay_CheckNumber = item.Field<string>("Invoice_Pay_CheckNumber"),
                                  Invoice_Pay_Comment = item.Field<string>("Invoice_Pay_Comment"),
                                  Invoice_Pay_Balance_Due = item.Field<decimal?>("Invoice_Pay_Balance_Due"),
                                  Invoice_Pay_EnteredBy = item.Field<Int64>("Invoice_Pay_EnteredBy"),
                                  Invoice_Pay_EnteredBy_Name = item.Field<string>("Invoice_Pay_EnteredBy_Name"),
                              }).ToList();
                            InvoiceDetail.Invoice_Payments = PaymentList;
                        }
                        if (ds.Tables.Count > 3 && ds.Tables[3] != null)
                        {

                            var ReceivePaymentList = ds.Tables[3].AsEnumerable();
                            List<Receive_Invoice_Item> PaymentList =
                             (from item in ReceivePaymentList
                              select new Receive_Invoice_Item
                              {
                                  Invoice_Rec_Invoice_Id = item.Field<Int64>("Invoice_Rec_Invoice_Id"),
                                  PaymentDate = item.Field<DateTime>("Invoice_Rec_CreatedOn"),
                                  Invoice_Rec_Original_Amount = item.Field<decimal?>("Invoice_Rec_Original_Amount"),
                                  Invoice_Rec_Payment = item.Field<decimal?>("Invoice_Rec_Payment"),

                              }).ToList();
                            InvoiceDetail.Receive_Invoice_Items = PaymentList;
                        }
                        return InvoiceDetail;
                    }
                }
                catch (Exception ex)
                {
                    log.logErrorMessage(ex.StackTrace);
                    log.logErrorMessage(ex.Message);
                }
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }


            return null;
        }
        public Custom_Response GetInvoiceByCusomerId(Acc_Invoice_Receive_Payment_DTO model)
        {
            Custom_Response custom_Response = new Custom_Response();
            Acc_Invoice_Receive_Payment_DTO result = new Acc_Invoice_Receive_Payment_DTO();
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Acc_Get_Invoices_By_CustomerId]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Invoice_CustomeId", 1 + "#bigint#" + model.Invoice_Rec_Customer_Id);
                input_parameters.Add("@Invoice_Id", 1 + "#bigint#" + model.Invoice_pkeyId);
                input_parameters.Add("@Invoice_Number", 1 + "#nvarchar#" + model.Invoice_Number);
                input_parameters.Add("@UserID", 1 + "#varchar#" + model.UserID);
                input_parameters.Add("@WhereClause", 1 + "#varchar#" + model.WhereClause);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);

                try
                {
                    if (ds.Tables.Count > 0 && ds.Tables[0] != null)
                    {

                        var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                        List<Receive_Invoice_Item> Receive_Invoice_Items =
                          (from item in myEnumerableFeaprd
                           select new Receive_Invoice_Item
                           {
                           //Receive_Invoice_Items_PkeyId = item.Field<Int64>("Receive_Invoice_Items_PkeyId"),
                           Invoice_Rec_Invoice_Id = item.Field<Int64>("Invoice_pkeyId"),
                               Invoice_CustomeId = item.Field<Int64>("Invoice_CustomeId"),
                               Invoice_Number = item.Field<string>("Invoice_Number"),
                               DueDate = item.Field<DateTime?>("Invoice_Due_Date"),
                               Invoice_Total = item.Field<decimal?>("Invoice_Total"),
                               Invoice_Rec_Original_Amount = item.Field<decimal?>("Original_Amount"),
                               Invoice_Rec_Payment = item.Field<decimal?>("Payment_Amount"),
                               Pending_Amount = item.Field<decimal?>("Pending_Amount"),
                               Invoice_Rec_Deposit_To = item.Field<long?>("Invoice_Rec_Deposit_To"),
                           }).ToList();
                        if (Receive_Invoice_Items != null && Receive_Invoice_Items.Count > 0)
                        {
                            foreach (var item in Receive_Invoice_Items)
                            {
                                item.Invoice_Rec_Original_Amount = item.Invoice_Rec_Original_Amount == 0 ? item.Invoice_Total : item.Invoice_Rec_Original_Amount;
                                if (item.Invoice_Rec_Payment > 0)
                                {

                                    item.Pending_Amount = item.Invoice_Rec_Payment != 0 ? item.Invoice_Rec_Original_Amount - item.Invoice_Rec_Payment : item.Pending_Amount;
                                }
                                else
                                {
                                    item.Pending_Amount = item.Invoice_Rec_Original_Amount;
                                }
                            }
                        }
                        result.Receive_Invoice_Items = Receive_Invoice_Items;
                        result.Invoice_Rec_Customer_Id = model.Invoice_Rec_Customer_Id;
                        custom_Response.Data = result;
                        custom_Response.HttpStatusCode = HttpStatusCode.OK;
                        return custom_Response;
                    }
                }
                catch (Exception ex)
                {
                    log.logErrorMessage(ex.StackTrace);
                    log.logErrorMessage(ex.Message);
                    custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                }
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
            }


            return custom_Response;
        }
        public Custom_Response PaidInvoiceList(Acc_Invoice_Bank_Deposit_DTO model)
        {
            Custom_Response custom_Response = new Custom_Response();
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Acc_Get_Invoice_Paid]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);

                ds = obj.SelectSql(selectProcedure, input_parameters);

                try
                {
                    var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                    List<Acc_Invoice_BankDeposit_Items> Receive_Invoice_Items =
                      (from item in myEnumerableFeaprd
                       select new Acc_Invoice_BankDeposit_Items
                       {
                           Invoice_Id = item.Field<Int64>("Invoice_Rec_Invoice_Id"),
                           Invoice_Rec_PkeyId = item.Field<Int64>("Invoice_Rec_PkeyId"),
                           Invoice_CustomeId = item.Field<Int64>("Invoice_CustomeId"),
                           Payment_Date = item.Field<DateTime?>("Payment_Date"),
                           Amount = item.Field<decimal?>("Original_Amount"),
                           Original_Amount = item.Field<decimal?>("Original_Amount"),
                           Payment_Amount = item.Field<decimal?>("Payment_Amount"),
                           Pending_Amount = item.Field<decimal?>("Pending_Amount"),
                           Customer_Name = item.Field<string>("Customer_Name"),
                           Payment_Method =0,
                           //Ref_No = item.Field<string>("Ref_No"),
                           //Memo = item.Field<string>("Memo"),
                       }).ToList();
                   
                    custom_Response.Data = Receive_Invoice_Items;
                    custom_Response.HttpStatusCode = HttpStatusCode.OK;
                    return custom_Response;
                }
                catch (Exception ex)
                {
                    log.logErrorMessage(ex.StackTrace);
                    log.logErrorMessage(ex.Message);
                    custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                }
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
            }


            return custom_Response;
        }
        public Custom_Response InvoiceBankDepositData(Acc_Invoice_Receive_Payment_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            Custom_Response custom_Response = new Custom_Response();

            string insertProcedure = "[Acc_Invoice_Bank_Deposit]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Invoice_Rec_PkeyId", 1 + "#bigint#" + model.Invoice_Rec_PkeyId);
                input_parameters.Add("@Invoice_Id", 1 + "#bigint#" + model.Invoice_pkeyId);
                input_parameters.Add("@Invoice_Rec_Payment_Method", 1 + "#int#" + model.Invoice_Rec_Payment_Method);
                input_parameters.Add("@Invoice_Rec_Reference_No", 1 + "#varchar#" + model.Invoice_Rec_Reference_No);
                input_parameters.Add("@Invoice_Rec_Deposit_To", 1 + "#bigint#" + model.Invoice_Rec_Deposit_To);
                input_parameters.Add("@Invoice_Rec_Memo", 1 + "#varchar#" + model.Invoice_Rec_Memo);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Invoice_Items_pkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    custom_Response.Message = Message.ErrorWhileSaving;
                    custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                }
                else
                {
                    custom_Response.Message = Message.SuccessSave;
                    custom_Response.HttpStatusCode = HttpStatusCode.OK;
                }
            }
            catch (Exception ex)
            {
                custom_Response.Message = Message.ErrorWhileSaving;
                custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return custom_Response;
        }

    }
}