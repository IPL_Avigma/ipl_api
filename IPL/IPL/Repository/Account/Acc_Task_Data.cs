﻿using Avigma.Repository.Lib;
using IPL.HelperMessage;
using IPL.Models.Account;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;

namespace IPL.Repository.Account
{
    public class Acc_Task_Data
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        //Add app Company Details
        public Custom_Response AddTaskMasterData(Acc_Task_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            Custom_Response custom_Response = new Custom_Response();
            Acc_TaskMaster taskMaster = new Acc_TaskMaster();
            string insertProcedure = "[Acc_CreateUpdate_TaskMaster]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Acc_Task_pkeyId", 1 + "#bigint#" + model.Acc_Task_pkeyId);
                input_parameters.Add("@Task_Name", 1 + "#nvarchar#" + model.Task_Name);
                input_parameters.Add("@Task_Type", 1 + "#int#" + model.Task_Type);
                input_parameters.Add("@Task_Group", 1 + "#int#" + model.Task_Group);
                input_parameters.Add("@Task_UOM", 1 + "#int#" + model.Task_UOM);
                input_parameters.Add("@Task_Contractor_UnitPrice", 1 + "#decimal#" + model.Task_Contractor_UnitPrice);
                input_parameters.Add("@Task_Client_UnitPrice", 1 + "#decimal#" + model.Task_Client_UnitPrice);
                input_parameters.Add("@Task_Photo_Label_Name", 1 + "#nvarchar#" + model.Task_Photo_Label_Name);
                input_parameters.Add("@Task_AutoInvoiceComplete", 1 + "#bit#" + model.Task_AutoInvoiceComplete);
                input_parameters.Add("@Task_Work_Type_Group", 1 + "#nvarchar#" + model.Task_Work_Type_Group);
                input_parameters.Add("@Task_IsActive", 1 + "#bit#" + model.Task_IsActive);
                input_parameters.Add("@Task_System", 1 + "#int#" + model.Task_System);
                input_parameters.Add("@Task_pkeyID", 1 + "#bigint#" + model.Task_pkeyID);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Acc_Task_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    custom_Response.Message = Message.ErrorWhileSaving;
                    custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                }
                else
                {
                    custom_Response.Message = Message.SuccessSave;
                    custom_Response.HttpStatusCode = HttpStatusCode.OK;
                }

                return custom_Response;
            }
            catch (Exception ex)
            {
                custom_Response.Message = Message.ErrorWhileSaving;
                custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return custom_Response;
        }
        public List<dynamic> GetTaskMasterDetails(Acc_Task_DTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetTaskMaster(model);

                if (ds.Tables.Count > 0 && ds.Tables[0]!=null)
                {
                

                    if (ds.Tables[0].Rows.Count > 0)
                    {

                        var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                        List<Acc_Task_DTO> Acc_TaskDetails =
                           (from item in myEnumerableFeaprd
                            select new Acc_Task_DTO
                            {
                                Acc_Task_pkeyId = item.Field<Int64>("Acc_Task_pkeyId"),
                                Task_pkeyID = item.Field<Int64>("Task_pkeyID"),
                                Task_Name = item.Field<String>("Task_Name"),
                                Task_Type = item.Field<int?>("Task_Type"),
                                Task_TypeName = item.Field<String>("Task_TypeName"),
                                Task_Group = item.Field<int?>("Task_Group"),
                                Task_UOM = item.Field<int?>("Task_UOM"),
                                Task_Group_Name = item.Field<String>("Task_Group_Name"),
                                Task_Contractor_UnitPrice = item.Field<Decimal?>("Task_Contractor_UnitPrice"),
                                Task_Client_UnitPrice = item.Field<Decimal?>("Task_Client_UnitPrice"),
                                Task_Photo_Label_Name = item.Field<String>("Task_Photo_Label_Name"),
                                Task_IsActive = item.Field<Boolean?>("Task_IsActive"),
                                Task_AutoInvoiceComplete = item.Field<Boolean?>("Task_AutoInvoiceComplete"),
                                Task_CreatedBy = item.Field<String>("Task_CreatedBy"),
                                Task_ModifiedBy = item.Field<String>("Task_ModifiedBy"),
                            }).ToList();

                        objDynamic.Add(Acc_TaskDetails);

                    }
                }

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
        public List<dynamic> GetTaskFilterDetails(Acc_Task_DTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();


            string wherecondition = string.Empty;
            try
            {

                var Data = JsonConvert.DeserializeObject<Acc_Task_DTO>(model.FilterData);
                if (!string.IsNullOrEmpty(Data.Task_Name))
                {
                    wherecondition = " And Task_Name =    '" + Data.Task_Name + "'";
                }
                if (!string.IsNullOrEmpty(Data.Task_Photo_Label_Name))
                {
                    wherecondition = " And Task_Photo_Label_Name =    '" + Data.Task_Photo_Label_Name + "'";
                }
                if (Data.Task_Type != 0 && Data.Task_Type != null)
                {
                    wherecondition = "  And Task_Type = '" + Data.Task_Type + "'";
                }


                if (Data.Task_IsActive == true)
                {
                    wherecondition = wherecondition + "  And Task_IsActive =  '1'";
                }


                Acc_Task_DTO taskMasterDTO = new Acc_Task_DTO();

                taskMasterDTO.WhereClause = wherecondition;
                taskMasterDTO.Type = 3;
                taskMasterDTO.UserID = model.UserID;

                DataSet ds = GetTaskMaster(taskMasterDTO);
                if (ds.Tables.Count > 0 && ds.Tables[0] != null)
                {

                    var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                    List<Acc_Task_DTO> Taskinvfilter =
                       (from item in myEnumerableFeaprd
                        select new Acc_Task_DTO
                        {
                            Acc_Task_pkeyId = item.Field<Int64>("Acc_Task_pkeyId"),
                            Task_pkeyID = item.Field<Int64>("Task_pkeyID"),
                            Task_Name = item.Field<String>("Task_Name"),
                            Task_Type = item.Field<int?>("Task_Type"),
                            Task_TypeName = item.Field<String>("Task_TypeName"),
                            Task_Group = item.Field<int?>("Task_Group"),
                            Task_UOM = item.Field<int?>("Task_UOM"),
                            Task_Group_Name = item.Field<String>("Task_Group_Name"),
                            Task_Contractor_UnitPrice = item.Field<Decimal?>("Task_Contractor_UnitPrice"),
                            Task_Client_UnitPrice = item.Field<Decimal?>("Task_Client_UnitPrice"),
                            Task_Photo_Label_Name = item.Field<String>("Task_Photo_Label_Name"),
                            Task_IsActive = item.Field<Boolean?>("Task_IsActive"),
                            Task_AutoInvoiceComplete = item.Field<Boolean?>("Task_AutoInvoiceComplete"),

                        }).ToList();
                    objDynamic.Add(Taskinvfilter);
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
            }

            return objDynamic;
        }
        private DataSet GetTaskMaster(Acc_Task_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Acc_Get_TaskMaster]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();
                input_parameters.Add("@Acc_Task_pkeyId", 1 + "#bigint#" + model.Acc_Task_pkeyId);
                input_parameters.Add("@WhereClause", 1 + "#varchar#" + model.WhereClause);
                input_parameters.Add("@UserID", 1 + "#int#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ds;
        }
    }
}