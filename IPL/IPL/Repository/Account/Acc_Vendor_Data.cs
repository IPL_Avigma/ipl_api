﻿using Avigma.Repository.Lib;
using IPL.HelperMessage;
using IPL.Models.Account;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;

namespace IPL.Repository.Account
{
    public class Acc_Vendor_Data
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public Custom_Response AddVendorData(Acc_Vendor_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            Custom_Response custom_Response = new Custom_Response();
            string insertProcedure = "[Acc_CreateUpdate_Vendor]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Acc_Vendor_pkeyId", 1 + "#bigint#" + model.Acc_Vendor_pkeyId);
                input_parameters.Add("@Title", 1 + "#varchar#" + model.Title);
                input_parameters.Add("@First_Name", 1 + "#varchar#" + model.First_Name);
                input_parameters.Add("@Middle_Name", 1 + "#varchar#" + model.Middle_Name);
                input_parameters.Add("@Last_Name", 1 + "#varchar#" + model.Last_Name);
                input_parameters.Add("@Company_Name", 1 + "#varchar#" + model.Company_Name);
                input_parameters.Add("@Address", 1 + "#varchar#" + model.Address);
                input_parameters.Add("@City", 1 + "#varchar#" + model.City);
                input_parameters.Add("@StateId", 1 + "#int#" + model.StateId);
                input_parameters.Add("@ZipCode", 1 + "#bigint#" + model.ZipCode);
                input_parameters.Add("@Country", 1 + "#varchar#" + model.Country);
                input_parameters.Add("@Email", 1 + "#varchar#" + model.Email);
                input_parameters.Add("@Phone", 1 + "#varchar#" + model.Phone);
                input_parameters.Add("@Mobile", 1 + "#varchar#" + model.Mobile);
                input_parameters.Add("@FaxNumber", 1 + "#varchar#" + model.FaxNumber);
                input_parameters.Add("@IsActive", 1 + "#bit#" + model.IsActive);
                input_parameters.Add("@IsDelete", 1 + "#bit#" + model.IsDelete);
                input_parameters.Add("@PrintOnCheck", 1 + "#bit#" + model.PrintOnCheck);
                input_parameters.Add("@Website", 1 + "#varchar#" + model.Website);
                input_parameters.Add("@BusinessID_Social_No", 1 + "#nvarchar#" + model.BusinessID_Social_No);
                input_parameters.Add("@Default_Expence_Account_Id", 1 + "#nvarchar#" + model.Default_Expence_Account_Id);
                input_parameters.Add("@Track_Payment", 1 + "#bit#" + model.Track_Payment);
                input_parameters.Add("@User_pkeyID", 1 + "#bigint#" + model.User_pkeyID);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Acc_Vendor_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    custom_Response.Message = Message.ErrorWhileSaving;
                    custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                }
                else
                {
                    custom_Response.Message = Message.SuccessSave;
                    custom_Response.HttpStatusCode = HttpStatusCode.OK;
                }

                return custom_Response;
            }
            catch (Exception ex)
            {
                custom_Response.Message = Message.ErrorWhileSaving;
                custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return custom_Response;
        }
        private DataSet GetVendorMaster(Acc_Vendor_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Acc_Get_Vendor_List]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Acc_Vendor_pkeyId", 1 + "#bigint#" + model.Acc_Vendor_pkeyId);
                input_parameters.Add("@WhereClause", 1 + "#varchar#" + model.WhereClause);
                input_parameters.Add("@UserID", 1 + "#int#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ds;
        }
        public List<dynamic> GetVendorMasterDetails(Acc_Vendor_DTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetVendorMaster(model);

                if (ds.Tables.Count > 0 && ds.Tables[0]!=null)
                {
                        var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                        List<Acc_Vendor_DTO> AppClientDetails =
                           (from item in myEnumerableFeaprd
                            select new Acc_Vendor_DTO
                            {
                                Acc_Vendor_pkeyId = item.Field<Int64>("Acc_Vendor_pkeyId"),
                                User_pkeyID = item.Field<Int64?>("User_pkeyID"),
                                Title = item.Field<String>("Title"),
                                First_Name = item.Field<String>("First_Name"),
                                Middle_Name = item.Field<String>("Middle_Name"),
                                Last_Name = item.Field<String>("Last_Name"),
                                Company_Name = item.Field<String>("Company_Name"),
                                Address = item.Field<String>("Address"),
                                City = item.Field<String>("City"),
                                StateId = item.Field<int?>("StateId"),
                                ZipCode = item.Field<Int64?>("ZipCode"),
                                Country = item.Field<String>("Country"),
                                Email = item.Field<String>("Email"),
                                Mobile = item.Field<String>("Mobile"),
                                Phone = item.Field<String>("Phone"),
                                FaxNumber = item.Field<String>("FaxNumber"),
                                Website = item.Field<String>("Website"),
                                PrintOnCheck = item.Field<bool?>("PrintOnCheck"),
                                BusinessID_Social_No = item.Field<string>("BusinessID_Social_No"),
                                Track_Payment = item.Field<bool?>("Track_Payment"),
                                Default_Expence_Account_Id = item.Field<string>("Default_Expence_Account_Id"),
                                IsActive = item.Field<bool?>("IsActive"),
                                CreatedBy = item.Field<String>("CreatedBy"),
                                ModifiedBy = item.Field<String>("ModifiedBy"),
                            }).ToList();
                        objDynamic.Add(AppClientDetails);
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objDynamic;
        }
    }
}