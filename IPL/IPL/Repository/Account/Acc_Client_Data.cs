﻿using Avigma.Repository.Lib;
using IPL.HelperMessage;
using IPL.Models.Account;
using IPLApp.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;

namespace IPL.Repository.Account
{
    public class Acc_Data
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public Custom_Response CreateUpdateClient(Acc_Client model)
        {
            List<dynamic> objData = new List<dynamic>();
            Custom_Response custom_Response = new Custom_Response();
            string insertProcedure = "[Acc_CreateUpdate_Client]";
            Acc_Client acc_client = new Acc_Client();
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Acc_Client_pkeyId", 1 + "#bigint#" + model.Acc_Client_pkeyId);
                input_parameters.Add("@Company_Name", 1 + "#nvarchar#" + model.Company_Name);
                input_parameters.Add("@Discount", 1 + "#decimal#" + model.Discount);
                input_parameters.Add("@Contractor_Discount", 1 + "#decimal#" + model.Contractor_Discount);
                input_parameters.Add("@Billing_Address", 1 + "#varchar#" + model.Billing_Address);
                input_parameters.Add("@City", 1 + "#varchar#" + model.City);
                input_parameters.Add("@StateId", 1 + "#int#" + model.StateId);
                input_parameters.Add("@ZipCode", 1 + "#bigint#" + model.ZipCode);
                input_parameters.Add("@DateTimeOverlay", 1 + "#int#" + model.DateTimeOverlay);
                input_parameters.Add("@BackgroundProvider", 1 + "#varchar#" + model.BackgroundProvider);
                input_parameters.Add("@ContactType", 1 + "#varchar#" + model.ContactType);
                input_parameters.Add("@ContactName", 1 + "#varchar#" + model.ContactName);
                input_parameters.Add("@ContactEmail", 1 + "#varchar#" + model.ContactEmail);
                input_parameters.Add("@ContactPhone", 1 + "#varchar#" + model.ContactPhone);
                input_parameters.Add("@Comments", 1 + "#varchar#" + model.Comments);
                //input_parameters.Add("@Comments", 1 + "#varchar#" + model.Comments);
                input_parameters.Add("@Due_Date_Offset", 1 + "#bigint#" + model.Due_Date_Offset);
                input_parameters.Add("@Photo_Resize_width", 1 + "#bigint#" + model.Photo_Resize_width);
                input_parameters.Add("@Photo_Resize_height", 1 + "#bigint#" + model.Photo_Resize_height);
                input_parameters.Add("@Invoice_Total", 1 + "#decimal#" + model.Invoice_Total);
                input_parameters.Add("@Login", 1 + "#bit#" + model.Login);
                input_parameters.Add("@Login_Id", 1 + "#varchar#" + model.Login_Id);
                input_parameters.Add("@Password", 1 + "#varchar#" + model.Password);
                input_parameters.Add("@Rep_Id", 1 + "#varchar#" + model.Rep_Id);
                input_parameters.Add("@Lock_Order", 1 + "#bit#" + model.Lock_Order);
                input_parameters.Add("@IPL_Mobile", 1 + "#varchar#" + model.IPL_Mobile);
                input_parameters.Add("@Provider", 1 + "#int#" + model.Provider);
                input_parameters.Add("@Active", 1 + "#int#" + model.Active);
                input_parameters.Add("@ClientPhone", 1 + "#varchar#" + model.ClientPhone);
                input_parameters.Add("@FaxNumbar", 1 + "#varchar#" + model.FaxNumbar);
                input_parameters.Add("@Title", 1 + "#varchar#" + model.Title);
                input_parameters.Add("@PrintAsCheck", 1 + "#bit#" + model.PrintAsCheck);
                input_parameters.Add("@Country", 1 + "#varchar#" + model.Country);
                input_parameters.Add("@Other", 1 + "#varchar#" + model.Other);
                input_parameters.Add("@Website", 1 + "#varchar#" + model.Website);
                input_parameters.Add("@Client_pkeyID", 1 + "#bigint#" + model.Client_pkeyID);

                input_parameters.Add("@IsActive", 1 + "#bit#" + model.IsActive);
                input_parameters.Add("@IsDelete", 1 + "#bit#" + model.IsDelete);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@Acc_Client_pkeyID_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);


                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    custom_Response.Message = Message.ErrorWhileSaving;
                    custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;

                }
                else
                {
                    custom_Response.Message = Message.SuccessSave;
                    custom_Response.HttpStatusCode = HttpStatusCode.OK;


                }
            }
            catch (Exception ex)
            {
                custom_Response.Message = Message.ErrorWhileSaving;
                custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return custom_Response;
        }

        public List<dynamic> GetAccountClientList(ClientDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetAccountClient(model);
                if (ds.Tables.Count > 0 && ds.Tables[0] != null)
                {
                    var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                    List<Acc_Client> ClientDetails =
                       (from item in myEnumerableFeaprd
                        select new Acc_Client
                        {
                            Acc_Client_pkeyId = item.Field<Int64>("Acc_Client_pkeyId"),
                            Company_Name = item.Field<String>("Company_Name"),
                            Billing_Address = item.Field<String>("Billing_Address"),
                            City = item.Field<String>("City"),
                            IPL_StateName = item.Field<String>("IPL_StateName"),
                            ZipCode = item.Field<Int64?>("ZipCode"),
                            ContactName = item.Field<String>("ContactName"),
                            IsActive = item.Field<Boolean?>("IsActive"),
                            ContactEmail= item.Field<String>("ContactEmail"),
                            User_CreatedBy = item.Field<String>("User_CreatedBy"),
                            User_ModifiedBy = item.Field<String>("User_ModifiedBy"),
                        }).ToList();
                    objDynamic.Add(ClientDetails);
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }
        private DataSet GetAccountClient(ClientDTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Acc_Get_Client_List]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Acc_Client_pkeyId", 1 + "#bigint#" + model.Acc_Client_pkeyId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@WhereClause", 1 + "#nvarchar#" + model.WhereClause);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserId);
                input_parameters.Add("@MenuID", 1 + "#bigint#" + null);
                input_parameters.Add("@FilterData", 1 + "#nvarchar#" + null);
                ds = obj.SelectSql(selectProcedure, input_parameters);
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ds;
        }
        public List<dynamic> GetClientCompanyMasterDetails(ClientDTO model)
        {
            List<dynamic> objDynamic = new List<dynamic>();
            try
            {
                DataSet ds = GetAccountClient(model);
                if (ds.Tables.Count > 0 && ds.Tables[0] != null)
                {
                    var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                    List<Acc_Client> ClientDetails =
                       (from item in myEnumerableFeaprd
                        select new Acc_Client
                        {
                            Acc_Client_pkeyId = item.Field<Int64>("Acc_Client_pkeyId"),
                            Company_Name = item.Field<String>("Company_Name"),
                            Discount = item.Field<Decimal?>("Discount"),
                            Contractor_Discount = item.Field<Decimal?>("Contractor_Discount"),
                            Billing_Address = item.Field<String>("Billing_Address"),
                            City = item.Field<String>("City"),
                            StateId = item.Field<int?>("StateId"),
                            ZipCode = item.Field<Int64?>("ZipCode"),
                            DateTimeOverlay = item.Field<int?>("DateTimeOverlay"),
                            BackgroundProvider = item.Field<String>("BackgroundProvider"),
                            ContactType = item.Field<String>("ContactType"),
                            ContactName = item.Field<String>("ContactName"),
                            ContactEmail = item.Field<String>("ContactEmail"),
                            ContactPhone = item.Field<String>("ContactPhone"),
                            Comments = item.Field<String>("Comments"),
                            Due_Date_Offset = item.Field<Int64?>("Due_Date_Offset"),
                            Photo_Resize_width = item.Field<Int64?>("Photo_Resize_width"),
                            Photo_Resize_height = item.Field<Int64?>("Photo_Resize_height"),
                            Invoice_Total = item.Field<Decimal?>("Invoice_Total"),
                            Login = item.Field<Boolean?>("Login"),
                            Login_Id = item.Field<String>("Login_Id"),
                            Password = item.Field<String>("Password"),
                            Rep_Id = item.Field<String>("Rep_Id"),
                            Lock_Order = item.Field<Boolean?>("Lock_Order"),
                            IPL_Mobile = item.Field<String>("IPL_Mobile"),
                            Provider = item.Field<int?>("Provider"),
                            Active = item.Field<int?>("Active"),
                            ClientPhone = item.Field<String>("ClientPhone"),
                            FaxNumbar = item.Field<String>("FaxNumbar"),
                            IsActive = item.Field<Boolean?>("IsActive"),
                            IsDelete = item.Field<Boolean?>("IsDelete"),
                            Title = item.Field<string>("Title"),
                            PrintAsCheck = item.Field<bool?>("PrintAsCheck"),
                            Country = item.Field<string>("Country"),
                            Other = item.Field<string>("Other"),
                            Website = item.Field<string>("Website"),
                            Client_pkeyID = item.Field<Int64?>("Client_pkeyID"),
                            User_CreatedBy = item.Field<String>("User_CreatedBy"),
                            User_ModifiedBy = item.Field<String>("User_ModifiedBy"),
                        }).ToList();

                    objDynamic.Add(ClientDetails);
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return objDynamic;
        }

    }
}