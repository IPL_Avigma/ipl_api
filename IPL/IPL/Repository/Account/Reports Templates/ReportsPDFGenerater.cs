﻿using Avigma.Repository.Lib;
using NReco.PdfGenerator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Repository.Account.Reports_Templates
{
    public class ReportsPDFGenerater
    {
        Log log = new Log();
        public byte[] GenerateRuntimePDF(string html, string Header = null)
        {
            #region NReco.PdfGenerator
            try
            {
                HtmlToPdfConverter nRecohtmltoPdfObj = new HtmlToPdfConverter();

                if (Header != null)
                    nRecohtmltoPdfObj.PageHeaderHtml = Header;
                return nRecohtmltoPdfObj.GeneratePdf(html);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return null;
            }

            #endregion

        }
    }
}