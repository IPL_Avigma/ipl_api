﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Repository.Account.Reports_Templates
{
    public class Balance_Sheet_Reports_Templates
    {
        public string TotalTitle = "<div style='border-top: 1px solid #343a40; border-bottom: 1px solid #343a40; text-align: right; padding-top: .25rem; padding-bottom: .25rem;'><h5 style='font-weight: bold; margin: 0;font-size: 1.5rem'> Total </h5></div>";
        public string CurrentAssetTitle = "<div style='padding-left: 1rem;padding-right: 1rem;margin-bottom: .5rem;'><div style ='border-bottom: 1px solid #a4a4a4;padding-top: .25rem;adding-bottom: .25rem;'><span style ='float: left; margin-top: 15px;margin-right: 10px' ><i style = 'width: 0;border-left: 6px solid transparent;border-right: 6px solid transparent;border-top: 6px solid #A4A4A4;' ></i ></span ><h6 style ='color: #a4a4a4;margin: 0;font-size: 1.3rem;text-align: left;'> Assets </h6 ></div><div style='border-bottom: 1px solid #a4a4a4;padding-top: .25rem; padding-bottom: .25rem;'><span style='float: left; margin-top: 15px;margin-right: 10px' ><i style='width: 0;border-left: 6px solid transparent;border-right: 6px solid transparent;border-top: 6px solid #A4A4A4;'></i></span><h6 style ='color: #a4a4a4;margin:0;font-size:1.3rem;text-align: left;'>Current Assets</h6></div>";
        public string AccountsReceivable = "<div style='color: #a4a4a4;display:flex;flex-wrap: wrap; justify-content: space-between;width: 100%;border-bottom: 1px solid #a4a4a4;padding-left: 2rem!important;'><p style='margin: 0;font-weight: 600;position: relative;padding-left: 25px;text-align: left'><span style='float: left;position: absolute;top: 11px; left:0;text-align: left;'><i style='width:0; border-left: 6px solid transparent;border-right: 6px solid transparent; border-top: 6px solid #A4A4A4;'></i></span> Accounts Receivable(Debtors)</p></div>";

        public string LiabilitiesAndEquityTitle = "<div style ='padding-bottom: .25rem!important;padding-top: .25rem!important;border-bottom: 1px solid #a4a4a4;' ><span style = 'float: left; margin-top: 15px;margin-right: 10px' ><i style ='width: 0;border-left: 6px solid transparent;border-right: 6px solid transparent;border-top: 6px solid #A4A4A4;' ></i></span ><h6 style='color: #a4a4a4; margin: 0;font-size: 1.3rem;text-align: left;'> Liability and Equity</h6> </div>";
        public string CurrentLiability = "<div style ='padding-bottom: .25rem!important;padding-top: .25rem!important;border-bottom: 1px solid #a4a4a4;' ><span style = 'float: left; margin-top: 15px;margin-right: 10px' ><i style ='width: 0;border-left: 6px solid transparent;border-right: 6px solid transparent;border-top: 6px solid #A4A4A4;' ></i></span ><h6 style='color: #a4a4a4; margin: 0;font-size: 1.3rem;text-align: left;'>Current Liabilities</h6> </div>";
    }
}