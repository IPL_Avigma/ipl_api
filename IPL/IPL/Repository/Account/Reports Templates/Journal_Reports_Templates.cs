﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Repository.Account.Reports_Templates
{
    public class Journal_Reports_Templates
    {
        public string HtmlHeader = "<!DOCTYPE html><html lang='en'><head><meta charset='utf-8'><meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'><meta name='description' content=''><meta name ='author' content=''><title>IPL-Reports</title><style>*, ::after, ::before {box-sizing: border-box;}</style></head><body style ='font-family: sans-serif;' ><div style = 'max-width: 1140px;padding: 0 15px; margin: 0 auto;border: 1px solid #a4a4a4;'>";
        public string HtmlFooter = "</div></body></html>";
        public string Header = "<div style='text-align: center;'><h4 style='font-size: 2rem; margin-bottom: 10px; margin-top: 0;'>Journal</h4><span style='color: #6c757d;padding-bottom:15px;display:block;'> As of " + DateTime.Now.ToString("dd/MMM/yyyy") + " </span ><div style='border: 1px solid #6e6b6b;font-weight: 600;width:100%;padding:0 15px 25px; box-sizing:border-box; max-width:880px; margin: auto;'><div style='width:10%;float:left;text-align: left;'> Date</div><div style='width:10%;float:left;'>Type</div><div style='width:10%;float:left;'> No.</div> <div style='width:10%;float:left;'> Name</div><div style='width:20%;float:left;'> Memo / Description </div><div style='width:20%;float:left;'> Account</div><div style='width:10%;float:left;'> Amount</div><div style='width:10%;float:left;text-align: right;'> Balance </div></div></div>";
    }
}