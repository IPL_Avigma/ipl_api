﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Repository.Account.Reports_Templates
{
    public class Profit_And_Loss_Reports_Templates
    {
        public string TotalTitle = "<div style='border-top: 1px solid #343a40; border-bottom: 1px solid #343a40; text-align: right; padding-top: .25rem; padding-bottom: .25rem;'><h5 style='font-weight: bold; margin: 0;font-size: 1.5rem'> Total </h5></div>";
        public string IncomeTitle = "<div style='padding-left: 1rem;padding-right: 1rem;margin-bottom: .5rem;'><div style ='border-bottom: 1px solid #a4a4a4;padding-top: .25rem;padding-bottom: .25rem;'><span style ='float: left; margin-top: 15px;margin-right: 10px' ><i style = 'width: 0;border-left: 6px solid transparent;border-right: 6px solid transparent;border-top: 6px solid #A4A4A4;' ></i></span><h6 style ='color: #a4a4a4;margin: 0;font-size: 1.3rem;text-align: left;'> Income </h6></div>";
        public string ExpensesTitle = "<div style=';margin-bottom: .5rem;'><div style='border-bottom: 1px solid #a4a4a4;padding-top: .25rem;padding-bottom: .25rem;'><span style ='float: left; margin-top: 15px;margin-right: 10px' ><i style = 'width: 0;border-left: 6px solid transparent;border-right: 6px solid transparent;border-top: 6px solid #A4A4A4;' ></i></span><h6 style ='color: #a4a4a4;margin: 0;font-size: 1.3rem;text-align: left;'> Expenses </h6></div></div>";
       
    }
}