﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Repository.Account.Reports_Templates
{
    public class CommonTemplate
    {

        //public string HtmlHeader = "<!DOCTYPE html><html lang='en'><head><meta charset='utf-8'><meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'><meta name='description' content=''><meta name ='author' content=''><title>IPL-Reports</title><style>*, ::after, ::before {box-sizing: border-box;}</style></head><body style ='font-family: sans-serif;' ><div style = 'max-width: 1140px;padding: 0 15px; margin: 0 auto;border: 1px solid #a4a4a4;    padding: 40px 20px;'><div style='margin-bottom: 1rem;text-align: center;'><h4 style ='font-size: 2rem;    margin-bottom: 10px; margin - top: 0;'>#ReportsTitle</h4><span style = 'color: #6c757d;' > As of "+DateTime.Now.ToString("dd/MMM/yyyy")+" </span >";
        //public string HtmlFooter = "</div></body></html>";

        public string HtmlHeader = "<!DOCTYPE html><html lang='en'><head><meta charset='utf-8'><meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'><meta name='description' content=''><meta name ='author' content=''><title>IPL-Reports</title><style>*, ::after, ::before {box-sizing: border-box;}</style></head><body style ='font-family: sans-serif;' ><div style='margin-bottom: 1rem;text-align: center;'><h4 style ='font-size: 2rem;    margin-bottom: 10px; margin - top: 0;'>#ReportsTitle</h4><span style = 'color: #6c757d;' > As of " + DateTime.Now.ToString("dd/MMM/yyyy") + " </span >";

        public string HtmlFooter = "</body></html>";
    }
}