﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Repository.Account.Reports_Templates
{
    public class Profit_And_Loss_Comparison_Templates
    {
        public string TotalTitle = "<div style='width: 100%;top: 441px;left: 318px;color: #000; background-color: #fff;border-bottom: 1px solid #6e6b6b; border-top: 1px solid #6e6b6b;border-radius: 0; margin-top: 5px;box-shadow: 0 0 0 5px #fff; '><table style='border-collapse: collapse;border: none;table-layout: fixed;empty-cells: show; width: 100%; height: 100%;'  > <tbody> <tr><th style='color: #000;border-left: 1px dotted #c7c7c7;font-weight: 700;font-size: 14px;transition: none !important;'></th><th style='color: #000;border-left: 1px dotted #c7c7c7;font-weight: 700;font-size: 14px;transition: none !important; '></th><th></th></tr><tr><th rowspan='2'></th><th colspan='2' style='color: #000;border-left: 1px dotted #c7c7c7;font-weight: 700;font-size: 16px;transition: none !important;border-bottom: 1px solid #6e6b6b;text-align: center;'>Total</th></tr><tr><th rowspan='1' style='font-weight: 700;font-size: 16px;transition: none !important;'><div>#StartDate</div></th><th rowspan='1' style='color: #000;border-left: 1px dotted #c7c7c7;font-weight: 700;font-size: 16px;transition: none !important;'><div>#EndDate</div></th></tr></tbody></table></div>";
        public string IncomeTitle = "<div style='padding-left: 1rem;padding-right: 1rem;margin-bottom: .5rem;'><div style ='border-bottom: 1px solid #a4a4a4;padding-top: .25rem;padding-bottom: .25rem;'><span style='float: left; margin-top: 15px;margin-right: 10px' ><i style = 'width: 0;border-left: 6px solid transparent;border-right: 6px solid transparent;border-top: 6px solid #A4A4A4;' ></i></span><h6 style ='color: #a4a4a4;margin: 0;font-size: 1.3rem;text-align: left;'> Income </h6></div>";
        public string ExpensesTitle = "<div style='margin-bottom: .5rem;'><div style='border-bottom: 1px solid #a4a4a4;border-top: 1px solid #a4a4a4;padding-top: .25rem;padding-bottom: .25rem;'><span style ='float: left; margin-top: 15px;margin-right: 10px' ><i style = 'width: 0;border-left: 6px solid transparent;border-right: 6px solid transparent;border-top: 6px solid #A4A4A4;' ></i></span><h6 style ='color: #a4a4a4;margin: 0;font-size: 1.3rem;text-align: left;'> Expenses </h6></div></div>";
       
    }
}