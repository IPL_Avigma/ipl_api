﻿using Avigma.Repository.Lib;
using IPL.HelperMessage;
using IPL.Models.Account;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;

namespace IPL.Repository.Account
{
    public class Acc_Companies_Data
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();

        public Acc_Company_DTO GetCompanyDetail(Acc_Company_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Acc_Get_Companies]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@ID", 1 + "#bigint#" + model.Id);
                input_parameters.Add("@WhereClause", 1 + "#varchar#" + model.WhereClause);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                ds = obj.SelectSql(selectProcedure, input_parameters);
                try
                {
                    if (ds.Tables.Count > 0 && ds.Tables[0] != null)
                    {
                        var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                        List<Acc_Company_DTO> CompanyDetail = new List<Acc_Company_DTO>();
                        foreach (var item in myEnumerableFeaprd)
                        {
                            CompanyDetail.Add(new Acc_Company_DTO(item));
                        }
                        return CompanyDetail.FirstOrDefault();
                    }
                }
                catch (Exception ex)
                {
                    log.logErrorMessage(ex.StackTrace);
                    log.logErrorMessage(ex.Message);
                }
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }


            return null;
        }



        public Custom_Response AddCompanyData(Acc_Company_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            Custom_Response custom_Response = new Custom_Response();

            string insertProcedure = "[Acc_CreateUpdate_Company]";

            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Id", 1 + "#bigint#" + model.Id);
                input_parameters.Add("@CompanyName", 1 + "#varchar#" + model.CompanyName);
                input_parameters.Add("@ShortName", 1 + "#varchar#" + model.ShortName);
                input_parameters.Add("@CompanyCode", 1 + "#varchar#" + model.CompanyCode);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@pkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);
                input_parameters.Add("@LedgH_Message", 2 + "#varchar#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    custom_Response.Message = Message.ErrorWhileSaving;
                    custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                }
                else
                {
                    custom_Response.Message = Message.SuccessSave;
                    custom_Response.HttpStatusCode = HttpStatusCode.OK;
                    return custom_Response;
                }
            }
            catch (Exception ex)
            {
                custom_Response.Message = Message.ErrorWhileSaving;
                custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return custom_Response;
        }
    }
}