﻿using Avigma.Repository.Lib;
using IPL.Models.Account;
using IPL.Repository.Account.Reports_Templates;
using NReco.PdfGenerator;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace IPL.Repository.Account
{

    public class Acc_TrialBalance_Data
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Acc_Ledger_Header_Data acc_Ledger_MasterData = new Acc_Ledger_Header_Data();
        Log log = new Log();
        ReportsPDFGenerater PDF = new ReportsPDFGenerater();

        public async Task<IEnumerable<Acc_TrialBalance_DTO>> TrialBalance(Acc_Report_Filter filter)
        {
            Acc_Ledger_Headers_DTO acc_Ledger_DTO = new Acc_Ledger_Headers_DTO();
            acc_Ledger_DTO.Type = 1;
            acc_Ledger_DTO.UserID = filter.UserId;
            try
            {
            IEnumerable<Acc_Ledger_Headers_DTO> allLedgerEntry = await Task.Run(() => acc_Ledger_MasterData.GetLedgerEntries(acc_Ledger_DTO));

            var allDebitLines = allLedgerEntry.Where(x => x.LedgE_DrCr == (int)DrOrCrSide.Dr);
                if (allDebitLines != null)
                {

                    var allDr = allDebitLines
                      .GroupBy(l => new { l.Acc_AccountID, l.Acc_Account_Code, l.Acc_Account_Name, l.DebitAmount, l.CreditAmount,l.LedgH_CreatedOn })
                      .Select(l => new
                      {
                          AccountId = l.Key.Acc_AccountID,
                          AccountCode = l.Key.Acc_Account_Code,
                          AccountName = l.Key.Acc_Account_Name,
                          Debit = l.Key.DebitAmount,
                          Credit = l.Key.CreditAmount,
                          LedgH_CreatedOn=l.Key.LedgH_CreatedOn
                      });

                    var allCreditLines = allLedgerEntry.Where(x => x.LedgE_DrCr == (int)DrOrCrSide.Cr);
                    if (allCreditLines != null)
                    {

                        var allCr = allCreditLines
                         .GroupBy(l => new { l.Acc_AccountID, l.Acc_Account_Code, l.Acc_Account_Name, l.CreditAmount, l.DebitAmount,l.LedgH_CreatedOn })
                         .Select(l => new
                         {
                             AccountId = l.Key.Acc_AccountID,
                             AccountCode = l.Key.Acc_Account_Code,
                             AccountName = l.Key.Acc_Account_Name,
                             Credit = l.Key.CreditAmount,
                             Debit = l.Key.DebitAmount,
                             LedgH_CreatedOn = l.Key.LedgH_CreatedOn
                         });

                        var allDrcr = (from x in allDr
                                       select new Acc_TrialBalance_DTO
                                       {
                                           AccountId = x.AccountId,
                                           AccountCode = x.AccountCode,
                                           AccountName = x.AccountName,
                                           Debit = x.Debit,
                                           Credit = (decimal)0,
                                           CreatedDate=x.LedgH_CreatedOn
                                       }
                                    ).Concat(from y in allCr
                                             select new Acc_TrialBalance_DTO
                                             {
                                                 AccountId = y.AccountId,
                                                 AccountCode = y.AccountCode,
                                                 AccountName = y.AccountName,
                                                 Debit = (decimal)0,
                                                 Credit = y.Credit,
                                                 CreatedDate = y.LedgH_CreatedOn
                                             });

                        var sortedList = allDrcr
                        .OrderBy(tb => tb.AccountCode)
                        .ToList()
                        .Reverse<Acc_TrialBalance_DTO>();

                        var accounts = sortedList.ToList().GroupBy(a => a.AccountCode)
                                      .Select(tb => new Acc_TrialBalance_DTO()
                                      {
                                          AccountId = tb.First().AccountId,
                                          AccountCode = tb.First().AccountCode,
                                          AccountName = tb.First().AccountName,
                                          Credit = tb.Sum(x => x.Credit),
                                          Debit = tb.Sum(y => y.Debit),
                                          CreatedDate=tb.First().CreatedDate
                                      }).ToList();
                        if (filter.StartDate != null && filter.EndDate != null)
                            accounts = accounts.Where(x => x.CreatedDate >= filter.StartDate && x.CreatedDate <= filter.EndDate).ToList();
                        return await Task.FromResult(accounts);
                    }
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return null;
        }
       
        public async Task<Custom_Response> GeneratePDFStringTrialBalance(Acc_Report_Filter filter)
        {
            Custom_Response custom = new Custom_Response();
            CultureInfo us = CultureInfo.GetCultureInfo("en-US");
            us = CultureInfo.CreateSpecificCulture("en-US");
            us.NumberFormat.CurrencyNegativePattern = 1;
            Thread.CurrentThread.CurrentCulture = us;
            Thread.CurrentThread.CurrentUICulture = us;
            try
            {
                StringBuilder html = new StringBuilder();
                StringBuilder table = new StringBuilder();
                string theader = "";
                string tfooter = "";
                int index = 0;
                string tbody = "<tbody>";

                theader += "<table width = '100%' border='1' cellpadding='1' cellspacing='0' bgcolor='#efefef'>";
                table.Append(theader);
                tbody += "<tr> <td  bgcolor = '#CCCCCC' style='text-align:center;height:50px;  border: none;'><span class='MsoNormal1' style='text-align:center; font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px'><b><u> </u></b></span>  <div align='center;' style='font - family: Verdana, Arial, Helvetica, sans - serif; font - size: 10px; '>    <span class='MsoNormal1' style='text - align:center; font - size: 35px'><b><u> Trail Balance</u></b></span></div> </td></tr>";
                tbody += "<tr><td ><table width='100%'  border='0'> <tr height ='30px'><td width='10%' bgcolor='#CCCCCC' style='font-size: 12px'><div align='center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'>&nbsp;Sr No </span></b></span></div><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'></span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align='center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'>&nbsp; Account code </span></b></span></div><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style='mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align='center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;' > &nbsp; Account Name </span ></b ></span ></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td> <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp; Credit</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td>  <td  bgcolor='#CCCCCC' style='font-size: 12px'><div align = 'center'><span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family: Times New Roman;mso-fareast-language:EN-GB;font-size: 13px;'> &nbsp;  Debit</span></b></span></div> <span class='MsoNormal11' style='text-align:center;font-family: Verdana, Arial, Helvetica, sans-serif'><b><span style = 'mso-fareast-font-family:Times New Roman;mso-fareast-language:EN-GB'> </span></b></span></td>     </tr>";


             
                IEnumerable<Acc_TrialBalance_DTO> acc_TrialBalance_DTO = await TrialBalance(filter);
                decimal TotalCredit = 0;
                decimal TotalDebit = 0;
                foreach (var item in acc_TrialBalance_DTO.ToList())
                {
                    switch (filter.ReportsType)
                    {
                        case Reports.TrailBalance:

                            var sr = 1 + index++;

                            tbody += "<tr height ='30px'> <td style = 'text-align: center;'width='10%'>" + sr + "</td ><td style = 'text-align: center;'>" + item.AccountCode + "</td ><td style = 'text-align: center;'>" +  item.AccountName + "</td><td style = 'text-align: center;'>" + string.Format(us, "{0:C}", Math.Round(item.Credit, 2)) + "</td><td style = 'text-align: center;'>" + string.Format(us, "{0:C}", Math.Round(item.Debit, 2)) + "%" + "</td></tr>";
                            
                            TotalCredit += item.Credit;
                            TotalDebit += item.Debit;
                            break;
                            
                        default:
                            break;
                            
                    }
                }
                tbody += @"<tr><td style ='border:solid black 1px;text-align:center;'></td>
                           <td style ='border:solid black 1px;text-align:center;'></td>
                               <td style ='border:solid black 1px;text-align:center;'><b>Sum</b></td>
                               <td style ='border:solid black 1px;text-align:center;'>" + string.Format(us, "{0:C}", Math.Round(TotalCredit,2)) + "</td>" +
                                     "<td style='text-align:center;border: solid black 1px'>" + string.Format(us, "{0:C}", Math.Round(TotalDebit,2)) + "</td></tr>";
                tbody += "</tbody>";
                table.Append(tbody);
                byte[] pdffile = PDF.GenerateRuntimePDF(table.ToString());
                custom.Data = pdffile;
                custom.HttpStatusCode = System.Net.HttpStatusCode.OK;
                return custom;
            }
            catch (Exception ex)
            {
                custom.Message = ex.Message;
                custom.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return custom;
            }
        }

    }
}