﻿using Avigma.Repository.Lib;
using IPL.Models.Account;
using IPL.Repository.Account.Reports_Templates;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace IPL.Repository.Account
{
    public class Acc_Journal_Data
    {
        Log log = new Log();
        Acc_Invoice_Data acc_Invoice_Data = new Acc_Invoice_Data();
        CommonTemplate template = new CommonTemplate();
        Journal_Reports_Templates journal_Reports_Templates = new Journal_Reports_Templates();
        ReportsPDFGenerater PDF = new ReportsPDFGenerater();
        Acc_Ledger_Header_Data acc_Ledger_MasterData = new Acc_Ledger_Header_Data();
        Acc_Bill_Data acc_Bill_Data = new Acc_Bill_Data();
        public async Task<Custom_Response> JournalReports(Acc_Report_Filter filter)
        {
            Custom_Response custom = new Custom_Response();
            Acc_Journal_DTO journal_DTO = new Acc_Journal_DTO();
            try
            {
                List<Invoices> InvoicesList = new List<Invoices>();
                List<Journal> JournalList = new List<Journal>();
                Acc_Invoice_DTO acc_Invoice_DTO = new Acc_Invoice_DTO();
                acc_Invoice_DTO.Type = 3;
                acc_Invoice_DTO.UserID = filter.UserId;
                if (filter.StartDate != null && filter.EndDate != null)
                    acc_Invoice_DTO.WhereClause += " AND inv.Invoice_CreatedOn >=CONVERT(nvarchar, '" + filter.StartDate.Value.ToString("yyyy/MM/dd") + "') and  inv.Invoice_CreatedOn <= CONVERT(nvarchar, '" + filter.EndDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59).ToString("yyyy/MM/dd h:mm tt") + "')";
                var getInvoice = acc_Invoice_Data.GetInvoiceMasterReports(acc_Invoice_DTO);
                if (getInvoice != null)
                {

                    foreach (var item in getInvoice.Acc_Journal_Invoice_Reports_DTO.Where(x => x.Invoice_Total > 0))
                    {
                        List<string> Name = new List<string>();
                        List<string> MemoDescription = new List<string>();
                        List<decimal> DebitAmount = new List<decimal>();
                        List<decimal> CreditAmount = new List<decimal>();
                        DebitAmount.Add(item.Invoice_Total.Value);
                        DebitAmount.Add(0);
                        CreditAmount.Add(0);
                        CreditAmount.Add(item.Invoice_Total.Value);
                        MemoDescription.Add(item.Invoice_Message);
                        Name.Add("Accounts Receivable (Debtors)");
                        Name.Add("Sales");
                        JournalList.Add(new Journal()
                        {
                            CreditAmount = CreditAmount,
                            DebitAmount = DebitAmount,
                            CreatedDate = item.Invoice_Date != null ? item.Invoice_Date.Value : DateTime.Now,
                            TransactionType = "Invoice",
                            NO = item.Invoice_Number,
                            Memo_Description = MemoDescription,
                            Name = item.Invoice_Custome_Name,
                            AccountNames = Name,
                            TotalDebitAmount = DebitAmount.Sum(),
                            TotalCreditAmount = CreditAmount.Sum(),
                        });
                        if (item.Invoice_Status == (int)InvoiceStatus.Paid || item.Invoice_Status == (int)InvoiceStatus.Deposit || item.Invoice_Rec_PkeyId > 0)
                        {
                           
                            Name = new List<string>();
                            Name.Add("Undeposited Funds");
                            Name.Add("Accounts Receivable (Debtors)");
                            MemoDescription = new List<string>();
                            MemoDescription.Add(item.ReceivePayment_Memo);
                            JournalList.Add(new Journal()
                            {
                                CreditAmount = CreditAmount,
                                DebitAmount = DebitAmount,
                                CreatedDate = item.ReceivedPaymentDate != null ? item.ReceivedPaymentDate.Value : DateTime.Now,
                                TransactionType = "Payment",
                                Memo_Description = MemoDescription,
                                Name = item.Invoice_Custome_Name,
                                AccountNames = Name,
                                TotalDebitAmount = DebitAmount.Sum(),
                                TotalCreditAmount = CreditAmount.Sum(),
                            });
                        }
                        if (item.Invoice_Status == (int)InvoiceStatus.Deposit || (item.Invoice_Rec_PkeyId > 0 && item.DeposiToAccount != null && item.DeposiToAccount != ""))
                        {
                            Name = new List<string>();
                            Name.Add("Undeposited Funds");
                            Name.Add(item.DeposiToAccount);
                            MemoDescription = new List<string>();
                            MemoDescription.Add(item.ReceivePayment_Memo);
                            JournalList.Add(new Journal()
                            {
                                CreditAmount = CreditAmount,
                                DebitAmount = DebitAmount,
                                CreatedDate = item.Invoice_Date != null ? item.Invoice_Date.Value : DateTime.Now,
                                TransactionType = "Deposit",
                                Memo_Description = MemoDescription,
                                Name = item.Invoice_Custome_Name,
                                AccountNames = Name,
                                TotalDebitAmount = DebitAmount.Sum(),
                                TotalCreditAmount = CreditAmount.Sum(),
                            });
                        }
                    }
                }
                Acc_Bill_DTO acc_Bill_DTO = new Acc_Bill_DTO();
                acc_Bill_DTO.Type = 3;
                acc_Bill_DTO.UserID =filter.UserId;
                if (filter.StartDate != null && filter.EndDate != null)
                    acc_Bill_DTO.WhereClause = " AND bill.Bill_CreatedOn >=CONVERT(nvarchar, '" + filter.StartDate.Value.ToString("yyyy/MM/dd") + "') and bill.Bill_CreatedOn <= CONVERT(nvarchar, '" + filter.EndDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59).ToString("yyyy/MM/dd h:mm tt") + "')";
                var getBill = acc_Bill_Data.GetBillMasterReports(acc_Bill_DTO);
                if (getBill != null)
                {

                    foreach (var item in getBill.Acc_Journal_Bill_Reports_DTO.Where(x => x.Bill_Total > 0))
                    {
                        List<string> Name = new List<string>();
                        List<string> MemoDescription = new List<string>();
                        List<decimal> DebitAmount = new List<decimal>();
                        List<decimal> CreditAmount = new List<decimal>();
                        DebitAmount.Add(item.Bill_Total.Value);
                        DebitAmount.Add(0);
                        CreditAmount.Add(0);
                        CreditAmount.Add(item.Bill_Total.Value);
                        MemoDescription.Add(item.Bill_Message);
                        Name.Add("Accounts Receivable (Debtors)");
                        Name.Add("Sales");
                        JournalList.Add(new Journal()
                        {
                            CreditAmount = CreditAmount,
                            DebitAmount = DebitAmount,
                            CreatedDate = item.Bill_Date != null ? item.Bill_Date.Value : DateTime.Now,
                            TransactionType = "Bill",
                            NO = item.Bill_Number,
                            Memo_Description = MemoDescription,
                            Name = item.Bill_Vendor_Name,
                            AccountNames = Name,
                            TotalDebitAmount = DebitAmount.Sum(),
                            TotalCreditAmount = CreditAmount.Sum(),
                        });
                        if (item.Bill_Status == (int)InvoiceStatus.Paid || item.Bill_Status == (int)InvoiceStatus.Deposit || item.Bill_Rec_PkeyId > 0)
                        {

                            Name = new List<string>();
                            Name.Add("Undeposited Funds");
                            Name.Add("Accounts Receivable (Debtors)");
                            MemoDescription = new List<string>();
                            MemoDescription.Add(item.ReceivePayment_Memo);
                            JournalList.Add(new Journal()
                            {
                                CreditAmount = CreditAmount,
                                DebitAmount = DebitAmount,
                                CreatedDate = item.ReceivedPaymentDate != null ? item.ReceivedPaymentDate.Value : DateTime.Now,
                                TransactionType = "Payment",
                                Memo_Description = MemoDescription,
                                Name = item.Bill_Vendor_Name,
                                AccountNames = Name,
                                TotalDebitAmount = DebitAmount.Sum(),
                                TotalCreditAmount = CreditAmount.Sum(),
                            });
                        }
                        if (item.Bill_Status == (int)InvoiceStatus.Deposit || (item.Bill_Rec_PkeyId > 0 && item.DeposiToAccount != null && item.DeposiToAccount != ""))
                        {
                            Name = new List<string>();
                            Name.Add("Undeposited Funds");
                            Name.Add(item.DeposiToAccount);
                            MemoDescription = new List<string>();
                            MemoDescription.Add(item.ReceivePayment_Memo);
                            JournalList.Add(new Journal()
                            {
                                CreditAmount = CreditAmount,
                                DebitAmount = DebitAmount,
                                CreatedDate = item.Bill_Date != null ? item.Bill_Date.Value : DateTime.Now,
                                TransactionType = "Deposit",
                                Memo_Description = MemoDescription,
                                Name = item.Bill_Vendor_Name,
                                AccountNames = Name,
                                TotalDebitAmount = DebitAmount.Sum(),
                                TotalCreditAmount = CreditAmount.Sum(),
                            });
                        }
                    }
                }
                Acc_Journal_Entry_Headers_Data acc_Journal_Entry_Headers_Data = new Acc_Journal_Entry_Headers_Data();
                Acc_Journal_Entry_Headers_DTO model = new Acc_Journal_Entry_Headers_DTO();
                model.Type = 3;
                if (filter.StartDate != null && filter.EndDate != null)
                    model.WhereClause += " AND acc.JrnlH_Date >=CONVERT(nvarchar, '" + filter.StartDate.Value.ToString("yyyy/MM/dd") + "') and  acc.JrnlH_Date <= CONVERT(nvarchar, '" + filter.EndDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59).ToString("yyyy/MM/dd h:mm tt") + "')";

                var JournalEntry = acc_Journal_Entry_Headers_Data.GetJournalEntries(model);
                if (JournalEntry != null && JournalEntry.Any())
                {
                    foreach (var item in JournalEntry.ToList())
                    {
                        List<string> Name = new List<string>();
                        List<string> MemoDescription = new List<string>();
                        List<decimal> DebitAmount = new List<decimal>();
                        List<decimal> CreditAmount = new List<decimal>();
                        foreach (var name in item.JrnlH_JournalEntry)
                        {
                            Name.Add(name.AccountName);
                            DebitAmount.Add(name.JrnlE_DrCr == (int)DrOrCrSide.Dr ? name.JrnlE__Amount : 0);
                            CreditAmount.Add(name.JrnlE_DrCr == (int)DrOrCrSide.Cr ? name.JrnlE__Amount : 0);
                            MemoDescription.Add(name.JrnlE_Memo!=null?name.JrnlE_Memo:"-");
                        }
                        MemoDescription.Add("-");
                        JournalList.Add(new Journal
                        {
                            TransactionType = "Journal Entry",
                            CreatedDate = item.JrnlH_Date,
                            NO = item.JrnlH_pkeyId.ToString(),
                            AccountNames = Name,
                            DebitAmount = DebitAmount,
                            CreditAmount = CreditAmount,
                            TotalDebitAmount = DebitAmount.Sum(),
                            TotalCreditAmount = CreditAmount.Sum(),
                            Memo_Description = MemoDescription,
                        });
                    }
                }
                journal_DTO.Journal = JournalList;
                journal_DTO.DebitTotal =  (JournalList != null ? JournalList.Sum(x => x.TotalDebitAmount) : 0);
                journal_DTO.CreditTotal =  (JournalList != null ? JournalList.Sum(x => x.TotalCreditAmount) : 0);
               // journal_DTO.Journal = JournalList;
                custom.Data = journal_DTO;
                custom.HttpStatusCode = System.Net.HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                custom.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                custom.Message = ex.Message;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return custom;
        }
        public async Task<Custom_Response> GeneratePDFStringJournal( Acc_Report_Filter filter)
        {
            Custom_Response custom = new Custom_Response();
            CultureInfo us = CultureInfo.GetCultureInfo("en-US");
            us = CultureInfo.CreateSpecificCulture("en-US");
            us.NumberFormat.CurrencyNegativePattern = 1;
            Thread.CurrentThread.CurrentCulture = us;
            Thread.CurrentThread.CurrentUICulture = us;
            try
            {
                StringBuilder html = new StringBuilder();
                html.Append(journal_Reports_Templates.HtmlHeader);
                var acc_ProfitAndLossDetails_DTO = JournalReports(filter);
                if (acc_ProfitAndLossDetails_DTO != null && acc_ProfitAndLossDetails_DTO.Result != null)
                {
                    custom = acc_ProfitAndLossDetails_DTO.Result;
                    Acc_Journal_DTO journal_DTO = new Acc_Journal_DTO();
                    journal_DTO = custom.Data;
                    StringBuilder ProfitAndLoss = new StringBuilder();
                    string SalesList = "<table style='width:100%; border-collapse: collapse;'>";
                    foreach (var item in journal_DTO.Journal)
                    {
                        SalesList += "<tr><td style='width: 12%'>" + item.CreatedDate.ToString("dd/MM/yyyy") + "</td><td style='width: 10%'>" + item.TransactionType + "</td><td style='width: 10%'>" + item.NO + "</td><td style='width: 15%'>" + item.Name + "</td><td style= 'width:15%'>";
                        foreach (var memo in item.Memo_Description)
                        {
                            SalesList += "<p>" + memo + "</p>";
                        }
                        SalesList += "</td><td style= 'width:20%'>";
                        foreach (var name in item.AccountNames)
                        {
                            SalesList += "<p>" + name + "</p>";
                        }
                        SalesList += "<b><p>Total</p></b></td><td style='width: 8%'>";
                        foreach (var debit in item.DebitAmount)
                        {
                            SalesList += debit > 0 ? "<p>" + string.Format(us, "{0:C}", Math.Round(debit, 2)) + "</p>" : "<p style='text-align:center;'>-</p>";
                        }
                        SalesList += "<b><p>" + string.Format(us, "{0:C}", Math.Round(item.TotalDebitAmount, 2)) + "</p></b></td><td style='width: 8%'>";
                        foreach (var credit in item.CreditAmount)
                        {
                            SalesList += credit > 0 ? "<p>" + string.Format(us, "{0:C}", Math.Round(credit, 2)) + "</p>" : "<p style='text-align:center;'>-</p>";
                        }
                        SalesList += "<b><p>" + string.Format(us, "{0:C}", Math.Round(item.TotalCreditAmount, 2)) + "</p></b></td></tr>";
                    }

                    SalesList = SalesList + "</table>";
                    string TotalTable = "<table style='width:100%;border-top:1px solid black;border-bottom:1px solid black'><tr><td colspan='6' style='text-align:left;width:80%'><b>Total Income</b></td><td style='text-align:left;width:10%'><b>" + string.Format(us, "{0:C}", Math.Round(journal_DTO.DebitTotal, 2)) + "</b></td><td style='text-align:left;width:10%'><b>" + string.Format(us, "{0:C}", Math.Round(journal_DTO.CreditTotal, 2)) + "</b></td></tr></table>";
                    ProfitAndLoss.Append(SalesList + TotalTable);


                    html.Append(ProfitAndLoss.ToString());
                    html.Append(template.HtmlFooter);
                }
                byte[] pdffile = PDF.GenerateRuntimePDF(html.ToString(), journal_Reports_Templates.Header);
                custom.Data = pdffile;
                custom.HttpStatusCode = System.Net.HttpStatusCode.OK;
                return custom;
            }
            catch (Exception ex)
            {
                custom.Message = ex.Message;
                custom.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return custom;
            }
        }

    }

}