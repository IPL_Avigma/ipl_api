﻿using Avigma.Repository.Lib;
using IPL.HelperMessage;
using IPL.Models.Account;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;

namespace IPL.Repository.Account
{
    public class Acc_Account_MasterData
    {
        MyDataSourceFactory obj = new MyDataSourceFactory();
        Log log = new Log();
        public IEnumerable<Acc_AccountMaster_DTO> GetAccountMaster(Acc_AccountMaster_DTO model)
        {
            DataSet ds = null;
            try
            {
                string selectProcedure = "[Acc_Get_Account_Master_List]";
                //OLD SP [Acc_Get_Account_Master]
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();

                input_parameters.Add("@Account_Id", 1 + "#bigint#" + model.Acc_pkeyId);
                input_parameters.Add("@WhereClause", 1 + "#varchar#" + model.WhereClause);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                ds = obj.SelectSql(selectProcedure, input_parameters);

                try
                {
                    if (ds.Tables.Count > 0 && ds.Tables[0] != null)
                    {
                        var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                        List<Acc_AccountMaster_DTO> AccountDetail =
                           (from item in myEnumerableFeaprd
                            select new Acc_AccountMaster_DTO
                            {
                                Acc_pkeyId = item.Field<Int64>("Acc_pkeyId"),
                                Acc_Account_Name = item.Field<String>("Acc_Account_Name"),
                                Acc_Account_Code = item.Field<string>("Acc_Account_Code"),
                                Acc_DrOrCr_Side = item.Field<int>("Acc_DrOrCr_Side"),
                                Acc_Parent_Account_Id = item.Field<Int64?>("Acc_Parent_Account_Id"),
                                Acc_Account_Type = item.Field<int?>("Acc_Account_Type"),
                                Acc_Account_Details = item.Field<Int64?>("Acc_Account_Type_Detail"),
                                Acc_Account_Type_Name = item.Field<string>("Acc_Account_Type_Name"),
                                Acc_Account_Type_Detail_Name = item.Field<string>("Acc_Account_Type_Detail_Name"),
                                Acc_Account_Description = item.Field<string>("Acc_Account_Description"),
                                Balance = item.Field<Decimal>("Balance"),
                                Acc_CreatedOn= item.Field<DateTime>("CreatedDate"),
                            }).ToList();
                        return AccountDetail.AsEnumerable<Acc_AccountMaster_DTO>();
                    }
                }
                catch (Exception ex)
                {
                    log.logErrorMessage(ex.StackTrace);
                    log.logErrorMessage(ex.Message);
                }
            }

            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }


            return null;
        }

        public Custom_Response GetAllAccounts(long? Account_Id,long  UserId)
        {
            Custom_Response custom_Response = new Custom_Response();
            try
            {
                Acc_AccountMaster_DTO acc_AccountMaster_DTO = new Acc_AccountMaster_DTO();
                if (Account_Id != null && Account_Id > 0)
                {
                    acc_AccountMaster_DTO.Type = 2;
                    acc_AccountMaster_DTO.Acc_pkeyId = Account_Id.HasValue ? Account_Id.Value : 0;
                }
                else
                {
                    acc_AccountMaster_DTO.Type = 1;
                }
                acc_AccountMaster_DTO.UserID = UserId;
                IEnumerable<Acc_AccountMaster_DTO> allAccounts = GetAccountMaster(acc_AccountMaster_DTO);
                custom_Response.Data = allAccounts;
                custom_Response.HttpStatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                custom_Response.Message = Message.GettingErrorWhileBindList;
                custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return custom_Response;

        }


        public Custom_Response AddAccountData(Acc_AccountMaster_DTO model)
        {
            List<dynamic> objData = new List<dynamic>();
            Custom_Response custom_Response = new Custom_Response();

            string insertProcedure = "[Acc_CreateUpdate_Account]";
            if (model.Acc_pkeyId == 0)
            {
                model.Type = 1;
                //cheking account Number is unique 
                Acc_AccountMaster_DTO acc_AccountMaster_DTO = new Acc_AccountMaster_DTO();
                acc_AccountMaster_DTO.Type = 1;
                IEnumerable<Acc_AccountMaster_DTO> allAccounts = GetAccountMaster(acc_AccountMaster_DTO);

                if (allAccounts != null && allAccounts.ToList().Count > 0 && model.Acc_Account_Code != null && model.Acc_Account_Code != "")
                {
                    model.Acc_Account_Code = string.Join("", model.Acc_Account_Code.Split(default(string[]), StringSplitOptions.RemoveEmptyEntries));
                    var IsExist = allAccounts.Where(x => x.Acc_Account_Code == model.Acc_Account_Code).ToList();
                    if (IsExist != null && IsExist.Count > 0)
                    {
                        custom_Response.HttpStatusCode = HttpStatusCode.BadRequest;
                        custom_Response.Message = Message.AccountCodeIsExist;
                        return custom_Response;
                    }
                }
            }
            else
            {
                model.Type = 2;
                Acc_AccountMaster_DTO acc_AccountMaster_DTO = new Acc_AccountMaster_DTO();
                acc_AccountMaster_DTO.Type = 1;
                IEnumerable<Acc_AccountMaster_DTO> allAccounts = GetAccountMaster(acc_AccountMaster_DTO);

                if (allAccounts != null && allAccounts.ToList().Count > 0 && model.Acc_Account_Code != null && model.Acc_Account_Code != "")
                {
                    model.Acc_Account_Code = string.Join("", model.Acc_Account_Code.Split(default(string[]), StringSplitOptions.RemoveEmptyEntries));
                    var IsExist = allAccounts.Where(x => x.Acc_Account_Code == model.Acc_Account_Code).ToList();
                    if (IsExist != null && IsExist.Count > 0)
                    {
                        foreach (var item in IsExist)
                        {
                            if (item.Acc_pkeyId != model.Acc_pkeyId)
                            {
                                custom_Response.HttpStatusCode = HttpStatusCode.BadRequest;
                                custom_Response.Message = Message.AccountCodeIsExist;
                                return custom_Response;
                            }
                        }
                    }
                }
            }
            Dictionary<string, string> input_parameters = new Dictionary<string, string>();
            try
            {
                input_parameters.Add("@Id", 1 + "#bigint#" + model.Acc_pkeyId);
                input_parameters.Add("@Acc_Account_Type", 1 + "#bigint#" + model.Acc_Account_Type);
                input_parameters.Add("@Acc_Account_Type_Detail", 1 + "#bigint#" + model.Acc_Account_Details);
                input_parameters.Add("@Acc_Account_Name", 1 + "#varchar#" + model.Acc_Account_Name);
                input_parameters.Add("@Acc_Account_Code", 1 + "#varchar#" + model.Acc_Account_Code);
                input_parameters.Add("@Acc_Account_Description", 1 + "#varchar#" + model.Acc_Account_Description);
                input_parameters.Add("@Acc_Parent_Account_Id", 1 + "#bigint#" + model.Acc_Parent_Account_Id);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@pkeyId_Out", 2 + "#bigint#" + null);
                input_parameters.Add("@ReturnValue", 2 + "#int#" + null);
                input_parameters.Add("@LedgH_Message", 2 + "#varchar#" + null);

                objData = obj.SqlCRUD(insertProcedure, input_parameters);
                if (objData[1] == 0)
                {
                    custom_Response.Message = Message.ErrorWhileSaving;
                    custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                }
                else
                {
                    custom_Response.Message = Message.SuccessSave;
                    custom_Response.HttpStatusCode = HttpStatusCode.OK;
                    return custom_Response;
                }
            }
            catch (Exception ex)
            {
                custom_Response.Message = Message.ErrorWhileSaving;
                custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return custom_Response;
        }

        public Custom_Response GetAccountsByAccountId(Acc_AccountMaster_DTO model)
        {
            Custom_Response custom_Response = new Custom_Response();
            DataSet ds = null;
            Acc_AccountMaster_DTO acc_AccountMaster_DTO = new Acc_AccountMaster_DTO();
            acc_AccountMaster_DTO.Type = 1;
            try
            {
                string selectProcedure = "[Acc_Get_Account_Details_ByAccount_Id]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                input_parameters.Add("@Acc_pkeyId", 1 + "#bigint#" + model.Acc_pkeyId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                ds = obj.SelectSql(selectProcedure, input_parameters);
                if (ds.Tables.Count > 0 && ds.Tables[0] != null)
                {
                    var myEnumerableFeaprd = ds.Tables[0].AsEnumerable();
                    List<Acc_AccountMaster_DTO> acc_AccountMaster_DTOList =
                       (from item in myEnumerableFeaprd
                        select new Acc_AccountMaster_DTO
                        {
                            Acc_pkeyId = item.Field<Int64>("Acc_pkeyId"),
                            Acc_Account_Name = item.Field<String>("Acc_Account_Name"),
                            Acc_Account_Code = item.Field<String>("Acc_Account_Code"),
                            Acc_DrOrCr_Side = item.Field<int>("Acc_DrOrCr_Side"),
                            Acc_Account_Type = item.Field<int>("Acc_Account_Type"),
                            Acc_Parent_Account_Id = item.Field<Int64>("Acc_Parent_Account_Id"),
                            Acc_IsActive = item.Field<bool>("Acc_IsActive"),
                            Acc_Account_Details = item.Field<Int64>("Acc_Account_Details"),
                            Acc_Account_Description = item.Field<string>("Acc_Account_Description"),
                            Acc_Account_Type_Name = item.Field<string>("Acc_Account_Type_Name"),
                            Acc_Account_Type_Detail_Name = item.Field<string>("Acc_Account_Type_Detail_Name"),

                        }).ToList();
                    custom_Response.Data = acc_AccountMaster_DTOList.AsEnumerable<Acc_AccountMaster_DTO>();
                    custom_Response.HttpStatusCode = HttpStatusCode.OK;
                    return custom_Response;
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return custom_Response;
        }
        public Custom_Response GetAccountsActivityByAccountId(Acc_Account_Master_Activity_DTO model,COA_Filter Filter)
        {
            Custom_Response custom_Response = new Custom_Response();
            DataSet ds = null;
            Acc_Account_Master_Activity_DTO acc_AccountMaster_DTO = new Acc_Account_Master_Activity_DTO();
            acc_AccountMaster_DTO.Type = 1;
            try
            {
                string selectProcedure = "[Acc_Get_Invoice_Deposit]";
                Dictionary<string, string> input_parameters = new Dictionary<string, string>();
                input_parameters.Add("@Account_Id", 1 + "#bigint#" + model.Acc_pkeyId);
                input_parameters.Add("@Type", 1 + "#int#" + model.Type);
                input_parameters.Add("@UserID", 1 + "#bigint#" + model.UserID);
                List<Acc_Account_Master_Activity_DTO> list = new List<Acc_Account_Master_Activity_DTO>();
                ds = obj.SelectSql(selectProcedure, input_parameters);
                if (ds.Tables.Count>0 && ds.Tables[0] != null)
                {
                    var InvoicePaymentList = ds.Tables[0].AsEnumerable();
                   
                    List<Acc_Account_Master_Activity_DTO> acc_AccountMaster_DTOListInvoice =
                       (from item in InvoicePaymentList
                        select new Acc_Account_Master_Activity_DTO
                        {
                            Custome_Id = item.Field<Int64?>("Custome_Id"),
                            Invoice_Number = item.Field<String>("Invoice_Number"),
                            Invoice_Id = item.Field<Int64?>("Invoice_Id"),
                            Payment_Date = item.Field<DateTime?>("Payment_Date"),
                            Amount = item.Field<decimal?>("Amount"),
                            Reference_No = item.Field<string>("Reference_No"),
                            Memo = item.Field<string>("Memo"),
                            Customer_Name = item.Field<string>("Customer_Name"),
                            Type = 2,
                            CreatedDate= item.Field<DateTime>("CreatedDate"),
                        }).ToList();
                    if (Filter.StartDate != null && Filter.EndDate != null)
                    {
                        acc_AccountMaster_DTOListInvoice=acc_AccountMaster_DTOListInvoice.Where(x => x.CreatedDate >= Filter.StartDate && x.CreatedDate <= Filter.EndDate).ToList();
                    }
                    list.AddRange(acc_AccountMaster_DTOListInvoice);
                }
                if (ds.Tables.Count > 1 && ds.Tables[1] != null)
                {
                    var JournalPaymentList = ds.Tables[1].AsEnumerable();
                    List<Acc_Account_Master_Activity_DTO> acc_AccountMaster_DTOListJournal =
                       (from item in JournalPaymentList
                        select new Acc_Account_Master_Activity_DTO
                        {
                           // Custome_Id = item.Field<Int64?>("Custome_Id"),
                            Journal_Number = item.Field<Int64?>("Journal_Number"),
                            //Invoice_Id = item.Field<Int64?>("Invoice_Id"),
                            Payment_Date = item.Field<DateTime?>("Payment_Date"),
                            Amount = item.Field<decimal?>("Amount"),
                            Reference_No = item.Field<string>("Reference_No"),
                           // Memo = item.Field<string>("Memo"),
                           Account_Name = item.Field<string>("Acc_Account_Name"),
                            Type = 1,
                            CreatedDate = item.Field<DateTime>("CreatedDate"),
                        }).ToList();
                    if (Filter.StartDate != null && Filter.EndDate != null)
                    {
                        acc_AccountMaster_DTOListJournal = acc_AccountMaster_DTOListJournal.Where(x => x.CreatedDate >= Filter.StartDate && x.CreatedDate <= Filter.EndDate).ToList();
                    }
                    list.AddRange(acc_AccountMaster_DTOListJournal);

                    custom_Response.Data = list.AsEnumerable<Acc_Account_Master_Activity_DTO>();
                }
                
                custom_Response.HttpStatusCode = HttpStatusCode.OK;
                return custom_Response;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return custom_Response;
        }
    }
}