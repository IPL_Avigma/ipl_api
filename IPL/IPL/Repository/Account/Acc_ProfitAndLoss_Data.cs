﻿using Avigma.Repository.Lib;
using IPL.Repository.Account;
using IPL.Repository.Account.Reports_Templates;
using NReco.PdfGenerator;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace IPL.Models.Account
{
    public class Acc_ProfitAndLoss_Data
    {
        Log log = new Log();
        Acc_Invoice_Data acc_Invoice_Data = new Acc_Invoice_Data();
        Acc_Ledger_Header_Data acc_Ledger_MasterData = new Acc_Ledger_Header_Data();
        CommonTemplate template = new CommonTemplate();
        Profit_And_Loss_Reports_Templates profit_And_Loss = new Profit_And_Loss_Reports_Templates();
        Profit_And_Loss_Comparison_Templates profit_And_Loss_Comparison = new Profit_And_Loss_Comparison_Templates();
        ReportsPDFGenerater PDF = new ReportsPDFGenerater();
        Acc_Bill_Data acc_Bill_Data = new Acc_Bill_Data();
        public async Task<Custom_Response> GeneratePDFStringProfitLoss(Acc_Report_Filter filter)
        {
            Custom_Response custom = new Custom_Response();
            CultureInfo us = CultureInfo.GetCultureInfo("en-US");
            us = CultureInfo.CreateSpecificCulture("en-US");
            us.NumberFormat.CurrencyNegativePattern = 1;
            Thread.CurrentThread.CurrentCulture = us;
            Thread.CurrentThread.CurrentUICulture = us;
            try
            {
                StringBuilder html = new StringBuilder();
                html.Append(template.HtmlHeader.Replace("#ReportsTitle", "Profit And Loss"));
                var acc_ProfitAndLoss_DTO = ProfitAndLossReports(filter);
                if (acc_ProfitAndLoss_DTO != null && acc_ProfitAndLoss_DTO.Result != null)
                {
                    custom = acc_ProfitAndLoss_DTO.Result;
                    Acc_ProfitAndLoss_DTO acc_ProfitAndLoss_ = new Acc_ProfitAndLoss_DTO();
                    acc_ProfitAndLoss_ = custom.Data;
                    html.Append(profit_And_Loss.TotalTitle);
                    StringBuilder ProfitAndLoss = new StringBuilder();
                    ProfitAndLoss.Append(profit_And_Loss.IncomeTitle);
                    string sales = "<div style='color: #a4a4a4;display:flex;flex-wrap: wrap;width:100%;border-bottom: 1px solid #a4a4a4;padding-left: 2rem!important;'><div style='margin: 0px!important;text-align: left;padding: 0px!important;'>Sales</div><div style='margin: 0px!important;padding: 0px!important;text-align: right;'>" +
                       Math.Round(acc_ProfitAndLoss_.TotalSales, 2) + "</div></div>";
                    foreach (var item in acc_ProfitAndLoss_.Income)
                    {
                        sales += "<div style='color: #a4a4a4;display:flex;flex-wrap: wrap;width:100%;border-bottom: 1px solid #a4a4a4;padding-left: 2rem!important;'><div style='margin: 0px!important;text-align: left;padding: 0px!important;'>" + item.AccountName + "</div><div style='margin: 0px!important;padding: 0px!important;text-align: right;'>" + string.Format(us, "{0:C}", Math.Round(item.Amount, 2)) + "</div></div>";
                    }
                    string TotalIncome = "<div style='color: #a4a4a4;display:flex;flex-wrap: wrap;width:100%;border-bottom: 1px solid #a4a4a4;padding-left: 1rem!important;'><div style='margin: 0px!important;text-align: left;padding: 0px!important;'><b>Total Income</b></div><div style='margin: 0px!important;padding: 0px!important;text-align: right;'><b>" + string.Format(us, "{0:C}", Math.Round(acc_ProfitAndLoss_.TotalIncome, 2)) + "</b></div></div>";
                    string GROSSPROFIT = "<div style='color: #a4a4a4;display:flex;flex-wrap: wrap;width:100%;border-bottom: 1px solid #a4a4a4;padding-left: 1rem!important;'><div style='margin: 0px!important;text-align: left;padding: 0px!important;'><b>GROSS PROFIT</b></div><div style='margin: 0px!important;padding: 0px!important;text-align: right;'><b>" + string.Format(us, "{0:C}", Math.Round(acc_ProfitAndLoss_.GrossProfit, 2)) + "</b></div></div>";
                    ProfitAndLoss.Append(sales + TotalIncome + GROSSPROFIT);
                    ProfitAndLoss.Append(profit_And_Loss.ExpensesTitle);
                    string ExpesesList = "";
                    foreach (var item in acc_ProfitAndLoss_?.Expenses)
                    {
                        ExpesesList += "<div style='padding-left: 1rem!important;color: #a4a4a4;display:flex;flex-wrap: wrap;width:100%;border-bottom: 1px solid #a4a4a4;'><div style='text-align: left;'>" + item.AccountName + "</div><div style='text-align: right;'>" + string.Format(us, "{0:C}", Math.Round(item.Amount, 2)) + "</div></div>";
                    }
                    string TotalExpenses = "<div style='color: #a4a4a4;display:flex;flex-wrap: wrap;width:100%;border-bottom: 1px solid #a4a4a4;padding-left: 1rem!important;'><div style='margin: 0px!important;text-align: left;padding: 0px!important;'><b>Total Expenses</b></div><div style='margin: 0px!important;padding: 0px!important;text-align: right;'><b>" + string.Format(us, "{0:C}", Math.Round(acc_ProfitAndLoss_.TotalExpenses,2)) + "</b></div></div>";
                    string Profit = "<div style='color: #a4a4a4;display:flex;flex-wrap: wrap;width:100%;border-bottom: 1px solid #a4a4a4;padding-left: 1rem!important;'><div style='margin: 0px!important;text-align: left;padding: 0px!important;'>Profit</div><div style='margin: 0px!important;padding: 0px!important;text-align: right;'><b>" + string.Format(us, "{0:C}", Math.Round(acc_ProfitAndLoss_.Profit, 2)) + "</b></div></div></div>";
                    ProfitAndLoss.Append(ExpesesList + TotalExpenses + Profit);
                    html.Append(ProfitAndLoss.ToString() + "</div></div>");
                    html.Append(template.HtmlFooter);
                }
                byte[] pdffile = PDF.GenerateRuntimePDF(html.ToString());
                custom.Data = pdffile;
                custom.HttpStatusCode = System.Net.HttpStatusCode.OK;
                return custom;
            }
            catch (Exception ex)
            {
                custom.Message = ex.Message;
                custom.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return custom;
            }
        }

        public async Task<Custom_Response> ProfitAndLossReports(Acc_Report_Filter filter)
        {
            Custom_Response custom = new Custom_Response();
            Acc_ProfitAndLoss_DTO acc_ProfitAndLoss_ = new Acc_ProfitAndLoss_DTO();
            try
            {
                List<Income> InvoiceList = new List<Income>();
                Acc_Invoice_DTO acc_Invoice_DTO = new Acc_Invoice_DTO();
                acc_Invoice_DTO.Type = 3;
                acc_Invoice_DTO.UserID = filter.UserId;
                if (filter.StartDate != null && filter.EndDate != null)
                    acc_Invoice_DTO.WhereClause += "AND inv.Invoice_CreatedOn >=CONVERT(nvarchar, '" + filter.StartDate.Value.ToString("yyyy/MM/dd") + "') and  inv.Invoice_CreatedOn <= CONVERT(nvarchar, '" + filter.EndDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59).ToString("yyyy/MM/dd h:mm tt") + "')";
                var getInvoice = acc_Invoice_Data.GetInvoiceMaster(acc_Invoice_DTO);
                if (getInvoice != null)
                {
                    foreach (var item in getInvoice)
                    {
                        Income dto = new Income()
                        {
                            Amount = item.Invoice_Total.Value,
                            AccountName = item.Invoice_Custome_Name,
                            AccountType = (int)AccountType.Income,
                            CreatedDate = item.Invoice_CreatedOn
                        };
                        InvoiceList.Add(dto);
                    }
                }
                Acc_Bill_DTO acc_Bill_DTO = new Acc_Bill_DTO();
                acc_Bill_DTO.Type = 3;
                acc_Bill_DTO.UserID = filter.UserId;
                if (filter.StartDate != null && filter.EndDate != null)
                    acc_Bill_DTO.WhereClause = "AND bill.Bill_CreatedOn >=CONVERT(nvarchar, '" + filter.StartDate.Value.ToString("yyyy/MM/dd") + "') and bill.Bill_CreatedOn <= CONVERT(nvarchar, '" + filter.EndDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59).ToString("yyyy/MM/dd h:mm tt") + "')";
                var getBill = acc_Bill_Data.GetBillMaster(acc_Bill_DTO);
                if (getBill != null)
                {
                    foreach (var item in getBill.Where(x => x.Bill_Total > 0))
                    {
                        Income dto = new Income()
                        {
                            Amount = item.Bill_Total.Value,
                            AccountName = item.Bill_Vendor_Name,
                            AccountType = (int)AccountType.Income,
                            CreatedDate = item.Bill_Date.Value,
                        };
                        InvoiceList.Add(dto);
                    }
                }
                Acc_Ledger_Headers_DTO acc_Ledger_DTO = new Acc_Ledger_Headers_DTO();
                acc_Ledger_DTO.Type = 3;
                acc_Ledger_DTO.UserID = filter.UserId;
                if (filter.StartDate != null && filter.EndDate != null)
                    acc_Ledger_DTO.WhereClause += "AND LedgH_CreatedOn >=CONVERT(nvarchar, '" + filter.StartDate.Value.ToString("yyyy/MM/dd") + "') and LedgH_CreatedOn <= CONVERT(nvarchar, '" + filter.EndDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59).ToString("yyyy/MM/dd h:mm tt") + "')";
                IEnumerable<Acc_Ledger_Headers_DTO> acc_Ledger_Headers_DTOs = acc_Ledger_MasterData.GetLedgerEntries(acc_Ledger_DTO);
                var currentincome = acc_Ledger_Headers_DTOs.Where(x => (x.Acc_Account_Type == (int)
                AccountType.Income)).GroupBy(x => new { x.Acc_AccountID, x.Acc_Account_Type, x.Acc_Account_Name, x.Acc_Account_Code, x.LedgH_CreatedOn }).Select(Line => new Income
                {
                    AccountId = Line.Key.Acc_AccountID,
                    AccountType = Line.Key.Acc_Account_Type,
                    AccountCode = Line.Key.Acc_Account_Code,
                    AccountName = Line.Key.Acc_Account_Name,
                    Amount = Line.Sum(s => s.LedgE_DrCr == (int)DrOrCrSide.Dr ? (s.Acc_DrOrCr_Side == (int)DrOrCrSide.Dr ? s.DebitAmount : -(s.DebitAmount)) : (s.Acc_DrOrCr_Side == (int)DrOrCrSide.Dr ? -(s.CreditAmount) : s.CreditAmount)),
                    CreatedDate = Line.Key.LedgH_CreatedOn
                });

                currentincome = currentincome.Where(x => x.Amount > 0).ToList();
                acc_ProfitAndLoss_.Income = currentincome.ToList();
                var IncomeDataList = acc_Ledger_Headers_DTOs.Where(x => x.Acc_Account_Type == (int)AccountType.OtherIncome).GroupBy(x => new { x.Acc_AccountID, x.Acc_Account_Type, x.Acc_Account_Name, x.Acc_Account_Code, x.LedgH_CreatedOn }).Select(Line => new Income
                {
                    AccountId = Line.Key.Acc_AccountID,
                    AccountType = Line.Key.Acc_Account_Type,
                    AccountCode = Line.Key.Acc_Account_Code,
                    AccountName = Line.Key.Acc_Account_Name,
                    Amount = Line.Sum(s => s.LedgE_DrCr == (int)DrOrCrSide.Dr ? (s.Acc_DrOrCr_Side == (int)DrOrCrSide.Dr ? s.DebitAmount : -(s.DebitAmount)) : (s.Acc_DrOrCr_Side == (int)DrOrCrSide.Dr ? -(s.CreditAmount) : s.CreditAmount)),
                    CreatedDate = Line.Key.LedgH_CreatedOn
                });
                acc_ProfitAndLoss_.OtherIncome = IncomeDataList.ToList();
                acc_ProfitAndLoss_.TotalOtherIncome = IncomeDataList != null ? IncomeDataList.Sum(x => x.Amount) : 0;
                var Expenses = acc_Ledger_Headers_DTOs.Where(x => x.Acc_Account_Type == (int)AccountType.Expense || x.Acc_Account_Type == (int)AccountType.OtherExpense).GroupBy(x => new { x.Acc_AccountID, x.Acc_Account_Type, x.Acc_Account_Name, x.Acc_Account_Code, x.LedgH_CreatedOn }).Select(Line => new Expense
                {
                    AccountId = Line.Key.Acc_AccountID,
                    AccountType = Line.Key.Acc_Account_Type,
                    AccountCode = Line.Key.Acc_Account_Code,
                    AccountName = Line.Key.Acc_Account_Name,
                    Amount = Line.Sum(s => s.LedgE_DrCr == (int)DrOrCrSide.Dr ? (s.Acc_DrOrCr_Side == (int)DrOrCrSide.Dr ? s.DebitAmount : -(s.DebitAmount)) : (s.Acc_DrOrCr_Side == (int)DrOrCrSide.Dr ? -(s.CreditAmount) : s.CreditAmount)),
                    CreatedDate = Line.Key.LedgH_CreatedOn
                });
                List<Expense> ExpensesList = new List<Expense>();
                foreach (var item in Expenses.ToList())
                {
                    ExpensesList.Add(new Expense()
                    {
                        AccountId = item.AccountId,
                        AccountType = item.AccountType,
                        AccountCode = item.AccountCode,
                        AccountName = item.AccountName,
                        Amount = item.Amount,
                        CreatedDate = item.CreatedDate
                    });
                };
                Acc_Account_MasterData acc_Account_MasterData = new Acc_Account_MasterData();
                Acc_AccountMaster_DTO acc_AccountMaster_DTO = new Acc_AccountMaster_DTO();
                acc_AccountMaster_DTO.Type = 3;
                acc_AccountMaster_DTO.UserID = filter.UserId;
                if (filter.StartDate != null && filter.EndDate != null)
                    acc_AccountMaster_DTO.WhereClause += "AND aam.Acc_CreatedOn >=CONVERT(nvarchar, '" + filter.StartDate.Value.ToString("yyyy/MM/dd") + "') and  aam.Acc_CreatedOn <= CONVERT(nvarchar, '" + filter.EndDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59).ToString("yyyy/MM/dd h:mm tt") + "')";
                var accounts = acc_Account_MasterData.GetAccountMaster(acc_AccountMaster_DTO);


                acc_ProfitAndLoss_.TotalExpenses = accounts.Where(x => (x.Acc_Account_Type == (int)AccountType.Expense || x.Acc_Account_Type == (int)AccountType.OtherExpense)).Select(x => x.Balance).ToList().Sum();
                acc_ProfitAndLoss_.TotalSales = InvoiceList.Sum(x => x.Amount);
                acc_ProfitAndLoss_.TotalIncome = acc_ProfitAndLoss_.TotalSales + currentincome.Sum(x => x.Amount);
                acc_ProfitAndLoss_.Expenses = ExpensesList;
                acc_ProfitAndLoss_.TotalExpenses = ExpensesList.Sum(x => x.Amount);
                acc_ProfitAndLoss_.GrossProfit = acc_ProfitAndLoss_.TotalIncome;
                // acc_ProfitAndLoss_.Profit = acc_ProfitAndLoss_.TotalIncome;
                acc_ProfitAndLoss_.Profit = acc_ProfitAndLoss_.TotalIncome - acc_ProfitAndLoss_.TotalExpenses;
                custom.Data = acc_ProfitAndLoss_;
            }
            catch (Exception ex)
            {
                custom.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                custom.Message = ex.Message;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return custom;
        }

        public async Task<Custom_Response> ProfitAndLossComparisonReports(Acc_Report_Filter filter)
        {
            Custom_Response custom = new Custom_Response();
            Acc_ProfitAndLossComparison_DTO acc_ProfitAndLossComparison = new Acc_ProfitAndLossComparison_DTO();
            try
            {
                Acc_Account_MasterData acc_Account_MasterData = new Acc_Account_MasterData();
                Acc_AccountMaster_DTO acc_AccountMaster_DTO = new Acc_AccountMaster_DTO();
                acc_AccountMaster_DTO.Type = 1;
                acc_AccountMaster_DTO.UserID = filter.UserId;
                var accounts = acc_Account_MasterData.GetAccountMaster(acc_AccountMaster_DTO);
                var Oldaccounts = accounts;

                acc_ProfitAndLossComparison.TotalOldIncome = Oldaccounts.Where(x => x.Acc_Account_Type == (int)AccountType.Income && x.Acc_CreatedOn >= filter.StartDate.Value.AddYears(-1) && x.Acc_CreatedOn <= filter.EndDate.Value.AddYears(-1)).Select(x => x.Balance).ToList().Sum();

                acc_ProfitAndLossComparison.TotalOldExpenses = Oldaccounts.Where(x => (x.Acc_Account_Type == (int)AccountType.Expense || x.Acc_Account_Type == (int)AccountType.OtherExpense) && x.Acc_CreatedOn >= filter.StartDate.Value.AddYears(-1) && x.Acc_CreatedOn <= filter.EndDate.Value.AddYears(-1)).Select(x => x.Balance).ToList().Sum();
                acc_ProfitAndLossComparison.TotalIncome = acc_ProfitAndLossComparison.TotalIncome - acc_ProfitAndLossComparison.TotalExpenses;


                List<Income> InvoiceBillList = new List<Income>();
                Acc_Ledger_Headers_DTO acc_Ledger_DTO = new Acc_Ledger_Headers_DTO();
                acc_Ledger_DTO.Type = 1;
                acc_Ledger_DTO.UserID = filter.UserId;
                IEnumerable<Acc_Ledger_Headers_DTO> acc_Ledger_Headers_DTOs = acc_Ledger_MasterData.GetLedgerEntries(acc_Ledger_DTO);
                var currentincome = acc_Ledger_Headers_DTOs.Where(x => (x.Acc_Account_Type == (int)
                AccountType.Income || x.Acc_Account_Type == (int)
                AccountType.OtherIncome)).GroupBy(x => new { x.Acc_AccountID, x.Acc_Account_Type, x.Acc_Account_Name, x.Acc_Account_Code, x.LedgH_CreatedOn }).Select(Line => new Income
                {
                    AccountId = Line.Key.Acc_AccountID,
                    AccountType = Line.Key.Acc_Account_Type,
                    AccountCode = Line.Key.Acc_Account_Code,
                    AccountName = Line.Key.Acc_Account_Name,
                    Amount = Line.Sum(s => s.LedgE_DrCr == (int)DrOrCrSide.Dr ? (s.Acc_DrOrCr_Side == (int)DrOrCrSide.Dr ? s.DebitAmount : -(s.DebitAmount)) : (s.Acc_DrOrCr_Side == (int)DrOrCrSide.Dr ? -(s.CreditAmount) : s.CreditAmount)),
                    CreatedDate = Line.Key.LedgH_CreatedOn
                });

                var oldincome = currentincome;
                Acc_Invoice_DTO acc_Invoice_DTO = new Acc_Invoice_DTO()
                {
                    Type = 1,
                    UserID = filter.UserId
                };
                var getInvoice = acc_Invoice_Data.GetInvoiceMaster(acc_Invoice_DTO);

                if (getInvoice != null)
                {
                    foreach (var item in getInvoice)
                    {
                        Income dto = new Income()
                        {
                            Amount = item.Invoice_Total.Value,
                            AccountName = item.Invoice_Custome_Name,
                            AccountType = (int)AccountType.Income,
                            CreatedDate = item.Invoice_CreatedOn
                        };
                        InvoiceBillList.Add(dto);
                    }
                }
                Acc_Bill_DTO acc_Bill_DTO = new Acc_Bill_DTO();
                acc_Bill_DTO.Type = acc_Invoice_DTO.Type;
                acc_Bill_DTO.UserID = acc_Invoice_DTO.UserID;
                var getBill = acc_Bill_Data.GetBillMaster(acc_Bill_DTO);
                if (getBill != null)
                {
                    foreach (var item in getBill)
                    {
                        Income dto = new Income()
                        {
                            Amount = item.Bill_Total.Value,
                            AccountName = item.Bill_Vendor_Name,
                            AccountType = (int)AccountType.Income,
                            CreatedDate = item.Bill_Date.Value
                        };
                        InvoiceBillList.Add(dto);
                    }
                }
                if (filter.StartDate != null && filter.EndDate != null)
                {
                    var InvoiceOldList = InvoiceBillList.Where(x => x.CreatedDate >= filter.StartDate.Value.AddYears(-1) && x.CreatedDate <= filter.EndDate.Value.AddYears(-1)).ToList();
                    acc_ProfitAndLossComparison.TotalOldSales = InvoiceOldList.Sum(x => x.Amount);
                }
                if (filter.StartDate != null && filter.EndDate != null)
                {

                    InvoiceBillList = InvoiceBillList.Where(x => x.CreatedDate >= filter.StartDate && x.CreatedDate <= filter.EndDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59)).ToList();
                    acc_ProfitAndLossComparison.TotalSales = InvoiceBillList.Sum(x => x.Amount);
                }
                if (filter.StartDate != null && filter.EndDate != null)
                {
                    currentincome = currentincome.Where(x => x.CreatedDate >= filter.StartDate && x.CreatedDate <= filter.EndDate);
                }
                currentincome = currentincome.Where(x => x.Amount > 0).ToList();
                acc_ProfitAndLossComparison.Income = currentincome.ToList();
                List<Expense> ExpensesList = new List<Expense>();

                foreach (var item in accounts.Where(x => (x.Acc_Account_Type == (int)AccountType.Expense || x.Acc_Account_Type == (int)AccountType.OtherExpense) && x.Balance > 0).ToList())
                {
                    ExpensesList.Add(new Expense()
                    {
                        AccountId = item.Acc_pkeyId,
                        AccountType = item.Acc_Account_Type.Value,
                        AccountCode = item.Acc_Account_Code,
                        AccountName = item.Acc_Account_Name,
                        Amount = item.Balance,
                        CreatedDate = item.Acc_CreatedOn
                    });
                };
                if (filter.StartDate != null && filter.EndDate != null)
                {

                    accounts = accounts.Where(x => x.Acc_CreatedOn >= filter.StartDate && x.Acc_CreatedOn <= filter.EndDate);
                    acc_ProfitAndLossComparison.TotalIncome = currentincome.Sum(x => x.Amount) + InvoiceBillList.Sum(x => x.Amount);
                }
                if (filter.StartDate != null && filter.EndDate != null)
                {

                    acc_ProfitAndLossComparison.TotalOldIncome = oldincome.Where(x => x.CreatedDate >= filter.StartDate.Value.AddYears(-1) && x.CreatedDate <= filter.EndDate.Value.AddYears(-1)).Sum(x => x.Amount) + acc_ProfitAndLossComparison.TotalOldSales;
                }
                if (filter.StartDate != null && filter.EndDate != null)
                {
                    ExpensesList = ExpensesList.Where(x => x.CreatedDate >= filter.StartDate && x.CreatedDate <= filter.EndDate).ToList();
                }
                acc_ProfitAndLossComparison.TotalExpenses = ExpensesList.Sum(x => x.Amount);

                acc_ProfitAndLossComparison.Expenses = ExpensesList;
                acc_ProfitAndLossComparison.GrossProfit = acc_ProfitAndLossComparison.TotalIncome;
                acc_ProfitAndLossComparison.GrossOldProfit = acc_ProfitAndLossComparison.TotalOldIncome;
                acc_ProfitAndLossComparison.Profit = acc_ProfitAndLossComparison.TotalIncome;
                acc_ProfitAndLossComparison.OldProfit = acc_ProfitAndLossComparison.TotalOldIncome;
                //acc_ProfitAndLoss_.Profit = acc_ProfitAndLoss_.TotalIncome - acc_ProfitAndLoss_.TotalExpenses;
                custom.Data = acc_ProfitAndLossComparison;
            }
            catch (Exception ex)
            {
                custom.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                custom.Message = ex.Message;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return custom;
        }
        public async Task<Custom_Response> GeneratePDFStringProfitLossComparison( Acc_Report_Filter filter)
        {
            Custom_Response custom = new Custom_Response();
            CultureInfo us = CultureInfo.GetCultureInfo("en-US");
            us = CultureInfo.CreateSpecificCulture("en-US");
            us.NumberFormat.CurrencyNegativePattern = 1;
            Thread.CurrentThread.CurrentCulture = us;
            Thread.CurrentThread.CurrentUICulture = us;
            try
            {
                StringBuilder html = new StringBuilder();
                html.Append(template.HtmlHeader.Replace("#ReportsTitle", "Profit And Loss Comparison"));
           
                var acc_ProfitAndLossComparison_DTO = ProfitAndLossComparisonReports(filter);
                if (acc_ProfitAndLossComparison_DTO != null && acc_ProfitAndLossComparison_DTO.Result != null)
                {
                    custom = acc_ProfitAndLossComparison_DTO.Result;
                    Acc_ProfitAndLossComparison_DTO acc_ProfitAndLossComparison = new Acc_ProfitAndLossComparison_DTO();
                    acc_ProfitAndLossComparison = custom.Data;
                    string startDate = filter.StartDate.Value.ToString("MMM dd, yyyy") + " - " + filter.EndDate.Value.ToString("MMM dd, yyyy");
                    string endDate = filter.StartDate.Value.AddYears(-1).ToString("MMM dd, yyyy") + " - " + filter.EndDate.Value.AddYears(-1).ToString("MMM dd, yyyy");
                    html.Append(profit_And_Loss_Comparison.TotalTitle.Replace("#StartDate", startDate).Replace("#EndDate", endDate));
                    StringBuilder ProfitAndLossComparison = new StringBuilder();
                    ProfitAndLossComparison.Append(profit_And_Loss_Comparison.IncomeTitle);
                    string Sales = "<table style='width:100%; margin-left: 15px;'><tr><td style='width: 33.33%;text-align:left;'>Sales</td><td style='width: 33.33%'>" + string.Format(us, "{0:C}", Math.Round(acc_ProfitAndLossComparison.TotalSales, 2)) + "</td><td style='width: 33.33%'>" + string.Format(us, "{0:C}", Math.Round(acc_ProfitAndLossComparison.TotalOldSales, 2)) + "</td></tr>";
                    foreach (var item in acc_ProfitAndLossComparison.Income)
                    {
                        Sales += "<tr style='width:100%'><td style='width: 33.33%;text-align:left;'>" + item.AccountName + "</td><td style='width: 33.33%'>" + string.Format(us, "{0:C}", Math.Round(item.Amount, 2)) + "</td><td style='width: 33.33%'></td></tr>";
                    }
                    string TotalIncome = "<tr><td style='width: 33.33%;text-align:left;'><b>Total Income</b></td><td style='width: 33.33%'><b>" + string.Format(us, "{0:C}", Math.Round(acc_ProfitAndLossComparison.TotalIncome, 2)) + "</b></td><td style='width: 33.33%'><b>" + string.Format(us, "{0:C}", Math.Round(acc_ProfitAndLossComparison.TotalOldIncome, 2)) + "</b></td></tr>";
                    string GROSSPROFIT = "<tr style='border-bottom: 1px solid #c7c7c7;'><td style='width: 33.33%;text-align:left;'>GROSS PROFIT</td><td style='width: 33.33%'>" + string.Format(us, "{0:C}", Math.Round(acc_ProfitAndLossComparison.GrossProfit, 2)) + "</td><td style='width: 33.33%'>" + string.Format(us, "{0:C}", Math.Round(acc_ProfitAndLossComparison.GrossOldProfit, 2)) + "</td></tr></table>";
                    ProfitAndLossComparison.Append(Sales + TotalIncome + GROSSPROFIT);
                    ProfitAndLossComparison.Append(profit_And_Loss_Comparison.ExpensesTitle);
                    string ExpesesList = "<table cellpadding='0' cellspacing='0' style='width:100%; margin-left: 15px;'>";
                    foreach (var item in acc_ProfitAndLossComparison?.Expenses)
                    {
                        ExpesesList += "<tr style='width:100%'><td style='width: 33.33%;text-align:left;'>" + item.AccountName + "</td><td style='width: 33.33%'>" + string.Format(us, "{0:C}", Math.Round(item.Amount, 2)) + "</td><td style='width: 33.33%'></td></tr>";
                    }
                    string TotalExpenses = "<tr ><td style='width: 33.33%;text-align:left;border-bottom: 1px solid #a4a4a4;border-top: 1px solid #a4a4a4;'><b>Total Expenses</b></td><td style='width: 33.33%;border-bottom: 1px solid #a4a4a4;border-top: 1px solid #a4a4a4;'><b>" + string.Format(us, "{0:C}", Math.Round(acc_ProfitAndLossComparison.TotalExpenses, 2)) + "</b></td><td style='width: 33.33%;border-bottom: 1px solid #a4a4a4;border-top: 1px solid #a4a4a4;'><b>" + string.Format(us, "{0:C}", Math.Round(acc_ProfitAndLossComparison.TotalOldExpenses, 2)) + "</b></td></tr>";
                    string Profit = "<tr style='border-bottom: 1px solid #a4a4a4;'><td style='width: 33.33%;text-align:left;border-bottom: 1px solid #a4a4a4;'>Profit</td><td style='width: 33.33%;border-bottom: 1px solid #a4a4a4;'><b>" + string.Format(us, "{0:C}", Math.Round((acc_ProfitAndLossComparison.TotalIncome - acc_ProfitAndLossComparison.TotalExpenses), 2)) + "</b></td><td style='width: 33.33%;border-bottom: 1px solid #a4a4a4;'><b>" + string.Format(us, "{0:C}", Math.Round((acc_ProfitAndLossComparison.TotalOldIncome - acc_ProfitAndLossComparison.TotalOldExpenses), 2)) + "</b></td></tr>";
                    ProfitAndLossComparison.Append(ExpesesList + TotalExpenses + Profit);
                    html.Append(ProfitAndLossComparison.ToString() + "</table>");
                    html.Append(template.HtmlFooter);
                }
                byte[] pdffile = PDF.GenerateRuntimePDF(html.ToString());
                custom.Data = pdffile;
                custom.HttpStatusCode = System.Net.HttpStatusCode.OK;
                return custom;
            }
            catch (Exception ex)
            {
                custom.Message = ex.Message;
                custom.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return custom;
            }
        }
        public async Task<Custom_Response> ProfitAndLossByMonthReports(Acc_Report_Filter filter)
        {
            Custom_Response custom = new Custom_Response();
            Acc_ProfitAndLoss_By_MonthDTO acc_ProfitAndLoss_By = new Acc_ProfitAndLoss_By_MonthDTO();
            try
            {
                var start = new DateTime(filter.StartDate.Value.Ticks);
                var end = new DateTime(filter.EndDate.Value.Ticks);

                // set end-date to end of month
                end = new DateTime(end.Year, end.Month, DateTime.DaysInMonth(end.Year, end.Month));
                List<DateTime> GetAllMonth = new List<DateTime>();
                var GetAllMonths = Enumerable.Range(0, Int32.MaxValue)
                                    .Select(e => start.AddMonths(e))
                                    .TakeWhile(e => e <= end)
                                    .Select(e => e);
                int Count = 0;
                int Day = 1;
                foreach (var item in GetAllMonths.ToList())
                {
                    Day = Count == 0 ? filter.StartDate.Value.Day : 1;
                    if ((GetAllMonths.ToList().Count() - 1) == Count)
                    {
                        Day = filter.EndDate.Value.Day;
                    }
                    GetAllMonth.Add(new DateTime(item.Year, item.Month, Day));
                    Count++;
                }
                List<Income> InvoiceList = new List<Income>();
                Acc_Invoice_DTO acc_Invoice_DTO = new Acc_Invoice_DTO();
                acc_Invoice_DTO.Type = 3;
                acc_Invoice_DTO.UserID = filter.UserId;
                if (filter.StartDate != null && filter.EndDate != null)
                    acc_Invoice_DTO.WhereClause += "AND inv.Invoice_CreatedOn >=CONVERT(nvarchar, '" + filter.StartDate.Value.ToString("yyyy/MM/dd") + "') and  inv.Invoice_CreatedOn <= CONVERT(nvarchar, '" + filter.EndDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59).ToString("yyyy/MM/dd h:mm tt") + "')";
                var getInvoice = acc_Invoice_Data.GetInvoiceMaster(acc_Invoice_DTO);
                if (getInvoice != null)
                {

                    foreach (var item in getInvoice)
                    {
                        Income dto = new Income()
                        {
                            Amount = item.Invoice_Total.Value,
                            AccountName = item.Invoice_Custome_Name,
                            AccountType = (int)AccountType.Income,
                            CreatedDate = item.Invoice_CreatedOn
                        };
                        InvoiceList.Add(dto);

                    }

                }
                Acc_Bill_DTO acc_Bill_DTO = new Acc_Bill_DTO();
                acc_Bill_DTO.Type = 3;
                acc_Bill_DTO.UserID = filter.UserId;
                if (filter.StartDate != null && filter.EndDate != null)
                    acc_Bill_DTO.WhereClause = "AND bill.Bill_CreatedOn >=CONVERT(nvarchar, '" + filter.StartDate.Value.ToString("yyyy/MM/dd") + "') and bill.Bill_CreatedOn <= CONVERT(nvarchar, '" + filter.EndDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59).ToString("yyyy/MM/dd h:mm tt") + "')";
                var getBill = acc_Bill_Data.GetBillMaster(acc_Bill_DTO);
                if (getBill != null)
                {
                    foreach (var item in getBill.Where(x => x.Bill_Total > 0))
                    {
                        Income dto = new Income()
                        {
                            Amount = item.Bill_Total.Value,
                            AccountName = item.Bill_Vendor_Name,
                            AccountType = (int)AccountType.Income,
                            CreatedDate = item.Bill_Date.Value,
                        };
                        InvoiceList.Add(dto);
                    }
                }

                acc_ProfitAndLoss_By.TotalSales = InvoiceList.Sum(x => x.Amount);

                Acc_Ledger_Headers_DTO acc_Ledger_DTO = new Acc_Ledger_Headers_DTO();
                acc_Ledger_DTO.Type = 3;
                acc_Ledger_DTO.UserID = filter.UserId;
                if (filter.StartDate != null && filter.EndDate != null)
                    acc_Ledger_DTO.WhereClause += "AND LedgH_CreatedOn >=CONVERT(nvarchar, '" + filter.StartDate.Value.ToString("yyyy/MM/dd") + "') and LedgH_CreatedOn <= CONVERT(nvarchar, '" + filter.EndDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59).ToString("yyyy/MM/dd h:mm tt") + "')";
                IEnumerable<Acc_Ledger_Headers_DTO> acc_Ledger_Headers_DTOs = acc_Ledger_MasterData.GetLedgerEntries(acc_Ledger_DTO);
                var currentincome = acc_Ledger_Headers_DTOs.Where(x => (x.Acc_Account_Type == (int)
                AccountType.Income)).GroupBy(x => new { x.Acc_AccountID, x.Acc_Account_Type, x.Acc_Account_Name, x.Acc_Account_Code, x.LedgH_CreatedOn }).Select(Line => new Income
                {
                    AccountId = Line.Key.Acc_AccountID,
                    AccountType = Line.Key.Acc_Account_Type,
                    AccountCode = Line.Key.Acc_Account_Code,
                    AccountName = Line.Key.Acc_Account_Name,
                    Amount = Line.Sum(s => s.LedgE_DrCr == (int)DrOrCrSide.Dr ? (s.Acc_DrOrCr_Side == (int)DrOrCrSide.Dr ? s.DebitAmount : -(s.DebitAmount)) : (s.Acc_DrOrCr_Side == (int)DrOrCrSide.Dr ? -(s.CreditAmount) : s.CreditAmount)),
                    CreatedDate = Line.Key.LedgH_CreatedOn,
                    MonthByAmount = GetMonthByAmount(GetAllMonth.ToList(), Line.Key.LedgH_CreatedOn, Line.Sum(s => s.LedgE_DrCr == (int)DrOrCrSide.Dr ? (s.Acc_DrOrCr_Side == (int)DrOrCrSide.Dr ? s.DebitAmount : -(s.DebitAmount)) : (s.Acc_DrOrCr_Side == (int)DrOrCrSide.Dr ? -(s.CreditAmount) : s.CreditAmount))),
                });

                currentincome = currentincome.Where(x => x.Amount > 0).ToList();
                acc_ProfitAndLoss_By.Income = currentincome.ToList();

                var IncomeDataList = acc_Ledger_Headers_DTOs.Where(x => x.Acc_Account_Type == (int)AccountType.OtherIncome).GroupBy(x => new { x.Acc_AccountID, x.Acc_Account_Type, x.Acc_Account_Name, x.Acc_Account_Code, x.LedgH_CreatedOn }).Select(Line => new Income
                {
                    AccountId = Line.Key.Acc_AccountID,
                    AccountType = Line.Key.Acc_Account_Type,
                    AccountCode = Line.Key.Acc_Account_Code,
                    AccountName = Line.Key.Acc_Account_Name,
                    Amount = Line.Sum(s => s.LedgE_DrCr == (int)DrOrCrSide.Dr ? (s.Acc_DrOrCr_Side == (int)DrOrCrSide.Dr ? s.DebitAmount : -(s.DebitAmount)) : (s.Acc_DrOrCr_Side == (int)DrOrCrSide.Dr ? -(s.CreditAmount) : s.CreditAmount)),
                    CreatedDate = Line.Key.LedgH_CreatedOn,
                    MonthByAmount = GetMonthByAmount(GetAllMonth.ToList(), Line.Key.LedgH_CreatedOn, Line.Sum(s => s.LedgE_DrCr == (int)DrOrCrSide.Dr ? (s.Acc_DrOrCr_Side == (int)DrOrCrSide.Dr ? s.DebitAmount : -(s.DebitAmount)) : (s.Acc_DrOrCr_Side == (int)DrOrCrSide.Dr ? -(s.CreditAmount) : s.CreditAmount))),
                });
                acc_ProfitAndLoss_By.OtherIncome = IncomeDataList.ToList();
                acc_ProfitAndLoss_By.TotalOtherIncome = IncomeDataList != null ? IncomeDataList.Sum(x => x.Amount) : 0;
                var Expenses = acc_Ledger_Headers_DTOs.Where(x => x.Acc_Account_Type == (int)AccountType.Expense || x.Acc_Account_Type == (int)AccountType.OtherExpense).GroupBy(x => new { x.Acc_AccountID, x.Acc_Account_Type, x.Acc_Account_Name, x.Acc_Account_Code, x.LedgH_CreatedOn }).Select(Line => new Expense
                {
                    AccountId = Line.Key.Acc_AccountID,
                    AccountType = Line.Key.Acc_Account_Type,
                    AccountCode = Line.Key.Acc_Account_Code,
                    AccountName = Line.Key.Acc_Account_Name,
                    Amount = Line.Sum(s => s.LedgE_DrCr == (int)DrOrCrSide.Dr ? (s.Acc_DrOrCr_Side == (int)DrOrCrSide.Dr ? s.DebitAmount : -(s.DebitAmount)) : (s.Acc_DrOrCr_Side == (int)DrOrCrSide.Dr ? -(s.CreditAmount) : s.CreditAmount)),
                    CreatedDate = Line.Key.LedgH_CreatedOn
                });
                List<Expense> ExpensesList = new List<Expense>();
                foreach (var item in Expenses.ToList())
                {
                    ExpensesList.Add(new Expense()
                    {
                        AccountId = item.AccountId,
                        AccountType = item.AccountType,
                        AccountCode = item.AccountCode,
                        AccountName = item.AccountName,
                        Amount = item.Amount,
                        CreatedDate = item.CreatedDate,
                        MonthByAmount = GetMonthByAmount(GetAllMonth.ToList(), item.CreatedDate, item.Amount),
                    });
                };
                Acc_Account_MasterData acc_Account_MasterData = new Acc_Account_MasterData();
                Acc_AccountMaster_DTO acc_AccountMaster_DTO = new Acc_AccountMaster_DTO();
                acc_AccountMaster_DTO.Type = 3;
                acc_AccountMaster_DTO.UserID = filter.UserId;
                if (filter.StartDate != null && filter.EndDate != null)
                    acc_AccountMaster_DTO.WhereClause += "AND aam.Acc_CreatedOn >=CONVERT(nvarchar, '" + filter.StartDate.Value.ToString("yyyy/MM/dd") + "') and  aam.Acc_CreatedOn <= CONVERT(nvarchar, '" + filter.EndDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59).ToString("yyyy/MM/dd h:mm tt") + "')";
                var accounts = acc_Account_MasterData.GetAccountMaster(acc_AccountMaster_DTO);


                acc_ProfitAndLoss_By.TotalExpenses = accounts.Where(x => (x.Acc_Account_Type == (int)AccountType.Expense || x.Acc_Account_Type == (int)AccountType.OtherExpense)).Select(x => x.Balance).ToList().Sum();
                int index = 0;
                List<string> MonthsList = new List<string>();
                List<decimal> MonthByWiseTotalIncome = new List<decimal>();
                List<decimal> MonthByWiseTotalOtherIncome = new List<decimal>();
                List<decimal> MonthByWiseTotalSales = new List<decimal>();
                List<decimal> MonthByWiseTotalExpenses = new List<decimal>();
                List<decimal> MonthByWiseProfit = new List<decimal>();
                bool isFirstDateAdded = false;
                foreach (DateTime item in GetAllMonth.ToList())
                {
                    string month = string.Empty;
                    if (!isFirstDateAdded)
                    {
                        DateTime lastDate = new DateTime(filter.StartDate.Value.Year, filter.StartDate.Value.Month, 1).AddMonths(1).AddDays(-1);
                        if (filter.StartDate.Value.Day != 1)
                        {
                            month = filter.StartDate.Value.Day + "-" + lastDate.Day + item.ToString("MMM yyyy");
                        }
                        else
                        {
                            month = item.ToString("MMM yyyy");
                        }
                        isFirstDateAdded = true;
                    }
                    else
                    {
                        month = index == GetAllMonth.Count() - 1 ? "1-" + filter.EndDate.Value.Day + " " + item.ToString("MMM yyyy") : item.ToString("MMM yyyy");
                    }
                    MonthsList.Add(month);
                    index++;
                    decimal MonthByWiseTotalIncomeValue = InvoiceList.Where(x => x.CreatedDate >= new DateTime(item.Year, item.Month, 1) && x.CreatedDate <= item.AddMonths(1).AddDays(-1)).Sum(x => x.Amount);
                    decimal MonthByWiseTotalOtherIncomeValue = IncomeDataList.Where(x => x.CreatedDate >= new DateTime(item.Year, item.Month, 1) && x.CreatedDate <= item.AddMonths(1).AddDays(-1)).Sum(x => x.Amount);
                    decimal currentincomeValue = currentincome.Where(x => x.CreatedDate >= new DateTime(item.Year, item.Month, 1) && x.CreatedDate <= item.AddMonths(1).AddDays(-1)).Sum(x => x.Amount);
                    decimal MonthByWiseTotalExpensesValue = ExpensesList.Where(x => x.CreatedDate >= new DateTime(item.Year, item.Month, 1) && x.CreatedDate <= item.AddMonths(1).AddDays(-1)).Sum(x => x.Amount);
                    MonthByWiseTotalIncome.Add(MonthByWiseTotalIncomeValue + currentincomeValue);
                    MonthByWiseTotalSales.Add(MonthByWiseTotalIncomeValue);
                    MonthByWiseTotalOtherIncome.Add(MonthByWiseTotalOtherIncomeValue);
                    MonthByWiseTotalExpenses.Add(MonthByWiseTotalExpensesValue);
                    MonthByWiseProfit.Add((MonthByWiseTotalIncomeValue + currentincomeValue) - MonthByWiseTotalExpensesValue);
                }
                acc_ProfitAndLoss_By.SelectedMonthList = MonthsList;
                acc_ProfitAndLoss_By.MonthByWiseTotalIncome = MonthByWiseTotalIncome;
                acc_ProfitAndLoss_By.MonthByWiseTotalOtherIncome = MonthByWiseTotalOtherIncome;
                acc_ProfitAndLoss_By.MonthByWiseTotalSales = MonthByWiseTotalSales;
                acc_ProfitAndLoss_By.TotalIncome = acc_ProfitAndLoss_By.TotalSales + currentincome.Sum(x => x.Amount);
                acc_ProfitAndLoss_By.Expenses = ExpensesList;
                acc_ProfitAndLoss_By.TotalExpenses = ExpensesList.Sum(x => x.Amount);
                acc_ProfitAndLoss_By.MonthByWiseTotalExpenses = MonthByWiseTotalExpenses;
                acc_ProfitAndLoss_By.GrossProfit = acc_ProfitAndLoss_By.TotalIncome;
                // acc_ProfitAndLoss_.Profit = acc_ProfitAndLoss_.TotalIncome;
                acc_ProfitAndLoss_By.MonthByWiseProfit = MonthByWiseProfit;
                acc_ProfitAndLoss_By.Profit = acc_ProfitAndLoss_By.TotalIncome - acc_ProfitAndLoss_By.TotalExpenses;
                custom.Data = acc_ProfitAndLoss_By;
            }
            catch (Exception ex)
            {
                custom.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                custom.Message = ex.Message;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return custom;
        }
        public List<decimal> GetMonthByAmount(List<DateTime> SelectedMonth, DateTime CreatedDate, decimal Amount)
        {
            List<decimal> ListAmount = new List<decimal>();
            try
            {
                foreach (DateTime item in SelectedMonth.ToList())
                {
                    if (CreatedDate >= new DateTime(item.Year, item.Month, 1) && CreatedDate <= new DateTime(item.Year, item.Month, DateTime.DaysInMonth(item.Year, item.Month)))
                    {
                        ListAmount.Add(Amount);
                    }
                    else
                    {
                        ListAmount.Add(0);
                    }
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ListAmount;
        }
        public async Task<Custom_Response> GeneratePDFStringProfitLossByMonth(Acc_Report_Filter filter)
        {
            Custom_Response custom = new Custom_Response();
            CultureInfo us = CultureInfo.GetCultureInfo("en-US");
            us = CultureInfo.CreateSpecificCulture("en-US");
            us.NumberFormat.CurrencyNegativePattern = 1;
            Thread.CurrentThread.CurrentCulture = us;
            Thread.CurrentThread.CurrentUICulture = us;
            Custom_Response acc_ProfitAndLoss_By_Month = new Custom_Response();
            try
            {
                Acc_ProfitAndLoss_By_MonthDTO acc_ProfitAndLoss_By = new Acc_ProfitAndLoss_By_MonthDTO();
                StringBuilder html = new StringBuilder();
                html.Append(template.HtmlHeader.Replace("#ReportsTitle", "Profit And Loss By Month"));
                var acc_ProfitAndLoss_By_Month_Result = ProfitAndLossByMonthReports(filter);
                if (acc_ProfitAndLoss_By_Month_Result != null && acc_ProfitAndLoss_By_Month_Result.Result != null)
                {
                    acc_ProfitAndLoss_By = acc_ProfitAndLoss_By_Month_Result.Result.Data;
                    StringBuilder table = new StringBuilder();
                    table.Append("<table style='border:1px solid black;width:100%; border-collapse: collapse;'>");
                    string THead = "<tr><th style='width:15%; border-left: 1px dotted #c7c7c7; font-weight: 700;text-align: center;border-bottom:1px solid black;'></th>";
                    foreach (var item in acc_ProfitAndLoss_By.SelectedMonthList)
                    {
                        THead += "<th style='color: #000; border-left: 1px dotted #c7c7c7; font-weight: 700; text-align: center;border-bottom:1px solid black;'>" + item + "</th>";
                    }
                    THead += "<th style='width:10%; border-left: 1px dotted #c7c7c7; font-weight: 700; text-align: center;border-bottom:1px solid black;'>Total</th></tr>";
                    table.Append(THead);
                    string TrIncomeTitle = "<tr><td colspan='" + (acc_ProfitAndLoss_By.SelectedMonthList.Count() + 2) + "' style='width:15%;  text-align: left;'><b>Income</b></td></tr>";
                    table.Append(TrIncomeTitle);
                    string TrSales = "<tr><td style='width:15%; text-align: left;'>Sales</td>";
                    foreach (var item in acc_ProfitAndLoss_By.MonthByWiseTotalSales)
                    {
                        TrSales += "<td style='color: #000;  text-align: center;'>" + string.Format(us, "{0:C}", Math.Round(item, 2)) + "</td>";
                    }
                    TrSales += "<td style='width:10%;  text-align: center;'>" + string.Format(us, "{0:C}", Math.Round(acc_ProfitAndLoss_By.TotalSales, 2)) + "</td></tr>";
                    table.Append(TrSales);
                    string TrIncomeList = string.Empty;
                    foreach (var item in acc_ProfitAndLoss_By.Income)
                    {
                        TrIncomeList += "<tr><td style='color: #000;  text-align: left;'>" + item.AccountName + "</td>";
                        foreach (var amount in item.MonthByAmount)
                        {
                            TrIncomeList += "<td style='color: #000;  text-align: center;'>" + string.Format(us, "{0:C}", Math.Round(amount,2)) + "</td>";
                        }
                        TrIncomeList += "<td style='color: #000;  text-align: center;'>" + string.Format(us, "{0:C}", Math.Round(acc_ProfitAndLoss_By.TotalIncome, 2)) + "</td></tr>";
                    }
                    table.Append(TrIncomeList);
                    string TrTotalIncomeTitle = "<tr><td  style='width:15%; text-align: left;border-bottom:1px solid black;border-top:1px solid black;'><b>Total Income</b></td>";
                    foreach (var item in acc_ProfitAndLoss_By.MonthByWiseTotalIncome)
                    {
                        TrTotalIncomeTitle += "<td style='color: #000;  text-align: center;border-bottom:1px solid black;border-top:1px solid black;'><b>" + string.Format(us, "{0:C}", Math.Round(item, 2))+ "</b></td>";
                    }
                    TrTotalIncomeTitle += "<td style='width:10%;  text-align: center;border-bottom:1px solid black;border-top:1px solid black;'><b>" + string.Format(us, "{0:C}", Math.Round(acc_ProfitAndLoss_By.TotalIncome, 2)) + "</b></td></tr>";
                    table.Append(TrTotalIncomeTitle);
                    string TrGrossProfit = "<tr><td style='width:15%; text-align: left;'>GROSS PROFIT</td>";
                    foreach (var item in acc_ProfitAndLoss_By.MonthByWiseTotalIncome)
                    {
                        TrGrossProfit += "<td style='color: #000;  text-align: center;'>" + string.Format(us, "{0:C}", Math.Round(item, 2) )+ "</td>";
                    }
                    TrGrossProfit += "<td style='width:10%;  text-align: center;'>" + string.Format(us, "{0:C}", Math.Round(acc_ProfitAndLoss_By.GrossProfit, 2)) + "</td></tr>";
                    table.Append(TrGrossProfit);
                    string TrOtherIncomeTitle = "<tr><td colspan='" + (acc_ProfitAndLoss_By.SelectedMonthList.Count() + 2) + "' style='width:15%; text-align: left;'><b>Other Income</b></td></tr>";
                    table.Append(TrOtherIncomeTitle);
                    string TrOtherIncomeList = string.Empty;
                    foreach (var item in acc_ProfitAndLoss_By.OtherIncome)
                    {
                        TrOtherIncomeList += "<tr><td style='color: #000;  text-align: left;'>" + item.AccountName + "</td>";
                        foreach (var amount in item.MonthByAmount)
                        {
                            TrOtherIncomeList += "<td style='color: #000;  text-align: center;'>" + string.Format(us, "{0:C}", Math.Round(amount,2)) + "</td>";
                        }
                        TrOtherIncomeList += "<td style='color: #000;  text-align: center;'>" + string.Format(us, "{0:C}", Math.Round(acc_ProfitAndLoss_By.TotalOtherIncome, 2)) + "</td></tr>";
                    }
                    table.Append(TrOtherIncomeList);
                    string TrOtherIncometotal = "<tr><td style='width:15%; text-align: left;border-bottom:1px solid black;border-top:1px solid black;'><b>Other Income Total</b></td>";
                    foreach (var item in acc_ProfitAndLoss_By.MonthByWiseTotalOtherIncome)
                    {
                        TrOtherIncometotal += "<td style='color: #000;  text-align: center;border-bottom:1px solid black;border-top:1px solid black;'><b>" + string.Format(us, "{0:C}", Math.Round(item, 2)) + "</b></td>";
                    }
                    TrOtherIncometotal += "<td style='width:10%;  text-align: center;border-bottom:1px solid black;border-top:1px solid black;'><b>" + string.Format(us, "{0:C}", Math.Round(acc_ProfitAndLoss_By.TotalOtherIncome, 2)) + "</b></td></tr>";
                    table.Append(TrOtherIncometotal);

                    string TrExpeneseTitle = "<tr><td colspan='" + (acc_ProfitAndLoss_By.SelectedMonthList.Count() + 2) + "' style='width:15%; text-align: left;'><b>Expenses</b></td></tr>";
                    table.Append(TrExpeneseTitle);
                    string TrExpeneseList = string.Empty;
                    foreach (var item in acc_ProfitAndLoss_By.Expenses)
                    {
                        TrExpeneseList += "<tr><td style='color: #000;  text-align: left;'>" + item.AccountName + "</td>";
                        foreach (var amount in item.MonthByAmount)
                        {
                            TrExpeneseList += "<td style='color: #000;  text-align: center;'>" + string.Format(us, "{0:C}", Math.Round(amount,2)) + "</td>";
                        }
                        TrExpeneseList += "<td style='color: #000;  text-align: center;'>" + string.Format(us, "{0:C}", Math.Round(acc_ProfitAndLoss_By.TotalExpenses, 2)) + "</td></tr>";
                    }
                    table.Append(TrExpeneseList);
                    string TrExpeneseTotal = "<tr><td style='width:15%; text-align: left;border-bottom:1px solid black;border-top:1px solid black;'><b>Expenses Total</b></td>";
                    foreach (var item in acc_ProfitAndLoss_By.MonthByWiseTotalExpenses)
                    {
                        TrExpeneseTotal += "<td style='color: #000;  text-align: center;border-bottom:1px solid black;border-top:1px solid black;'><b>" + string.Format(us, "{0:C}", Math.Round(item, 2)) + "</b></td>";
                    }
                    TrExpeneseTotal += "<td style='width:10%;  text-align: center;border-bottom:1px solid black;border-top:1px solid black;'><b>" + string.Format(us, "{0:C}", Math.Round(acc_ProfitAndLoss_By.TotalOtherIncome, 2)) + "</b></td></tr>";
                    table.Append(TrExpeneseTotal);
                    string Profit = "<tr><td style='width:15%; text-align: left;border-bottom:1px solid black;'><b>Profit</b></td>";
                    foreach (var item in acc_ProfitAndLoss_By.MonthByWiseProfit)
                    {
                        Profit += "<td style='color: #000;  text-align: center;border-bottom:1px solid black;'><b>" + string.Format(us, "{0:C}", Math.Round(item, 2)) + "</b></td>";
                    }
                    Profit += "<td style='width:10%;  text-align: center;border-bottom:1px solid black;'><b>" + string.Format(us, "{0:C}", Math.Round(acc_ProfitAndLoss_By.Profit, 2)) + "</b></td></tr>";
                    table.Append(Profit);
                    table.Append("</table>");

                    html.Append(table.ToString());
                }
                html.Append(template.HtmlFooter);
                byte[] pdffile = PDF.GenerateRuntimePDF(html.ToString());
                custom.Data = pdffile;
                custom.HttpStatusCode = System.Net.HttpStatusCode.OK;
                return custom;
            }
            catch (Exception ex)
            {
                custom.Message = ex.Message;
                custom.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return custom;
            }
        }
        public async Task<Custom_Response> ProfitAndLossByCustomerReports(Acc_Report_Filter filter)
        {
            Custom_Response custom = new Custom_Response();
            Acc_ProfitAndLoss_By_CustomerDTO acc_ProfitAndLoss_By = new Acc_ProfitAndLoss_By_CustomerDTO();
            try
            {
                List<SelectCustomerItem> CustomerList = new List<SelectCustomerItem>();
                List<Income> InvoiceList = new List<Income>();
                Acc_Invoice_DTO acc_Invoice_DTO = new Acc_Invoice_DTO()
;                acc_Invoice_DTO.Type = 3;
;                acc_Invoice_DTO.UserID = filter.UserId;
                if (filter.StartDate != null && filter.EndDate != null)
                    acc_Invoice_DTO.WhereClause += "AND inv.Invoice_CreatedOn >=CONVERT(nvarchar, '" + filter.StartDate.Value.ToString("yyyy/MM/dd") + "') and  inv.Invoice_CreatedOn <= CONVERT(nvarchar, '" + filter.EndDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59).ToString("yyyy/MM/dd h:mm tt") + "')";
                var getInvoice = acc_Invoice_Data.GetInvoiceMaster(acc_Invoice_DTO);
                if (getInvoice != null)
                {

                    foreach (var item in getInvoice)
                    {
                        Income dto = new Income()
                        {
                            Amount = item.Invoice_Total.Value,
                            AccountName = item.Invoice_Custome_Name,
                            AccountType = (int)AccountType.Income,
                            CreatedDate = item.Invoice_CreatedOn,
                            CustomerId = item.Invoice_CustomeId,
                            CustomerName = item.Invoice_Custome_Name,
                        };


                        InvoiceList.Add(dto);
                        CustomerList.Add(new SelectCustomerItem { CustomerId = item.Invoice_CustomeId, Name = dto.AccountName != null && dto.AccountName != "" ? dto.AccountName : "NAN" });

                    }

                }
                List<decimal> CustomerByWiseTotalSales = new List<decimal>();
                CustomerList = CustomerList.GroupBy(x => x.CustomerId).Select(x => x.First()).ToList();
                foreach (var item in CustomerList.ToList())
                {
                    CustomerByWiseTotalSales.Add(InvoiceList.Where(x => x.CustomerId == item.CustomerId).Sum(x => x.Amount));
                }
                InvoiceList = InvoiceList.GroupBy(x => x.CustomerId).Select(x => x.First()).ToList();

                List<decimal> CustomerByWiseTotalIncome = new List<decimal>();
                List<decimal> CustomerByWiseProfit = new List<decimal>();
                foreach (var item in CustomerByWiseTotalSales.ToList())
                {
                    CustomerByWiseTotalIncome.Add(item);
                    CustomerByWiseProfit.Add(item);
                }
                acc_ProfitAndLoss_By.TotalSales = InvoiceList.Sum(x => x.Amount);
                acc_ProfitAndLoss_By.CustomerByWiseTotalSales = CustomerByWiseTotalSales;
                acc_ProfitAndLoss_By.CustomerByWiseTotalIncome = CustomerByWiseTotalIncome;
                acc_ProfitAndLoss_By.CustomerByWiseProfit = CustomerByWiseProfit;
                acc_ProfitAndLoss_By.TotalIncome = CustomerByWiseTotalIncome.Sum();
                acc_ProfitAndLoss_By.CustomerList = CustomerList;
                acc_ProfitAndLoss_By.Profit = acc_ProfitAndLoss_By.TotalIncome - acc_ProfitAndLoss_By.TotalExpenses;
                custom.Data = acc_ProfitAndLoss_By;
            }
            catch (Exception ex)
            {
                custom.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                custom.Message = ex.Message;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return custom;
        }
        public List<decimal> GetCustomerByAmount(List<DateTime> SelectedMonth, DateTime CreatedDate, decimal Amount)
        {
            List<decimal> ListAmount = new List<decimal>();
            try
            {
                foreach (DateTime item in SelectedMonth.ToList())
                {
                    if (CreatedDate >= new DateTime(item.Year, item.Month, 1) && CreatedDate <= new DateTime(item.Year, item.Month, DateTime.DaysInMonth(item.Year, item.Month)))
                    {
                        ListAmount.Add(Amount);
                    }
                    else
                    {
                        ListAmount.Add(0);
                    }
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ListAmount;
        }
        public async Task<Custom_Response> GeneratePDFStringProfitLossByCustomer( Acc_Report_Filter filter)
        {
            Custom_Response custom = new Custom_Response();
            CultureInfo us = CultureInfo.GetCultureInfo("en-US");
            us = CultureInfo.CreateSpecificCulture("en-US");
            us.NumberFormat.CurrencyNegativePattern = 1;
            Thread.CurrentThread.CurrentCulture = us;
            Thread.CurrentThread.CurrentUICulture = us;
            Custom_Response acc_ProfitAndLoss_By_Month = new Custom_Response();
            try
            {
                Acc_ProfitAndLoss_By_CustomerDTO acc_ProfitAndLoss_By = new Acc_ProfitAndLoss_By_CustomerDTO();
                StringBuilder html = new StringBuilder();
                html.Append(template.HtmlHeader.Replace("#ReportsTitle", "Profit And Loss By Customer"));
                var acc_ProfitAndLoss_By_Customer_Result = ProfitAndLossByCustomerReports(filter);
                if (acc_ProfitAndLoss_By_Customer_Result != null && acc_ProfitAndLoss_By_Customer_Result.Result != null)
                {
                    acc_ProfitAndLoss_By = acc_ProfitAndLoss_By_Customer_Result.Result.Data;
                    StringBuilder table = new StringBuilder();
                    table.Append("<table style='border:1px solid black;width:100%; border-collapse: collapse;'>");
                    string THead = "<tr><th style='width:15%; border-left: 1px dotted #c7c7c7; font-weight: 700;text-align: center;border-bottom:1px solid black;'></th>";
                    foreach (var item in acc_ProfitAndLoss_By.CustomerList)
                    {
                        THead += "<th style='color: #000; border-left: 1px dotted #c7c7c7; font-weight: 700; text-align: center;border-bottom:1px solid black;'>" + item.Name + "</th>";
                    }
                    THead += "<th style='width:10%; border-left: 1px dotted #c7c7c7; font-weight: 700; text-align: center;border-bottom:1px solid black;'>Total</th></tr>";
                    table.Append(THead);
                    string TrIncomeTitle = "<tr><td colspan='" + (acc_ProfitAndLoss_By.CustomerList.Count() + 2) + "' style='width:15%;  text-align: left;'><b>Income</b></td></tr>";
                    table.Append(TrIncomeTitle);
                    string TrSales = "<tr><td style='width:15%; text-align: left;'>Sales</td>";
                    foreach (var item in acc_ProfitAndLoss_By.CustomerByWiseTotalIncome)
                    {
                        TrSales += "<td style='color: #000;  text-align: center;'>" + string.Format(us, "{0:C}", Math.Round(item, 2)) + "</td>";
                    }
                    TrSales += "<td style='width:10%;  text-align: center;'>" + string.Format(us, "{0:C}", Math.Round(acc_ProfitAndLoss_By.TotalSales, 2)) + "</td></tr>";
                    table.Append(TrSales);
                    string TrTotalIncomeTitle = "<tr><td  style='width:15%; text-align: left;border-bottom:1px solid black;border-top:1px solid black;'><b>Total Income</b></td>";
                    foreach (var item in acc_ProfitAndLoss_By.CustomerByWiseTotalIncome)
                    {
                        TrTotalIncomeTitle += "<td style='color: #000;  text-align: center;border-bottom:1px solid black;border-top:1px solid black;'><b>" + string.Format(us, "{0:C}", Math.Round(item, 2)) + "</b></td>";
                    }
                    TrTotalIncomeTitle += "<td style='width:10%;  text-align: center;border-bottom:1px solid black;border-top:1px solid black;'><b>" + string.Format(us, "{0:C}", Math.Round(acc_ProfitAndLoss_By.TotalIncome, 2)) + "</b></td></tr>";
                    table.Append(TrTotalIncomeTitle);
                    string Profit = "<tr><td style='width:15%; text-align: left;border-bottom:1px solid black;'><b>Profit</b></td>";
                    foreach (var item in acc_ProfitAndLoss_By.CustomerByWiseProfit)
                    {
                        Profit += "<td style='color: #000;  text-align: center;border-bottom:1px solid black;'><b>" + string.Format(us, "{0:C}", Math.Round(item, 2)) + "</b></td>";
                    }
                    Profit += "<td style='width:10%;  text-align: center;border-bottom:1px solid black;'><b>" + string.Format(us, "{0:C}", Math.Round(acc_ProfitAndLoss_By.Profit, 2)) + "</b></td></tr>";
                    table.Append(Profit);
                    table.Append("</table>");

                    html.Append(table.ToString());
                }
                html.Append(template.HtmlFooter);
                byte[] pdffile = PDF.GenerateRuntimePDF(html.ToString());
                custom.Data = pdffile;
                custom.HttpStatusCode = System.Net.HttpStatusCode.OK;
                return custom;
            }
            catch (Exception ex)
            {
                custom.Message = ex.Message;
                custom.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return custom;
            }
        }
        public async Task<Custom_Response> ProfitAndLossByVendorReports(Acc_Report_Filter filter)
        {
            Custom_Response custom = new Custom_Response();
            Acc_ProfitAndLoss_By_VendorDTO acc_ProfitAndLoss_By = new Acc_ProfitAndLoss_By_VendorDTO();
            try
            {
                List<SelectVendorItem> VendorList = new List<SelectVendorItem>();
                List<Income> BillList = new List<Income>();
                Acc_Bill_DTO acc_Bill_DTO = new Acc_Bill_DTO();
                acc_Bill_DTO.Type = 3;
                acc_Bill_DTO.UserID = filter.UserId;
                if (filter.StartDate != null && filter.EndDate != null)
                    acc_Bill_DTO.WhereClause = "AND bill.Bill_CreatedOn >=CONVERT(nvarchar, '" + filter.StartDate.Value.ToString("yyyy/MM/dd") + "') and bill.Bill_CreatedOn <= CONVERT(nvarchar, '" + filter.EndDate.Value.AddHours(23).AddMinutes(59).AddSeconds(59).ToString("yyyy/MM/dd h:mm tt") + "')";
                var getBill = acc_Bill_Data.GetBillMaster(acc_Bill_DTO);
                if (getBill != null)
                {
                    foreach (var item in getBill.Where(x => x.Bill_Total.Value > 0))
                    {
                        Income dto = new Income()
                        {
                            Amount = item.Bill_Total.Value,
                            AccountName = item.Bill_Vendor_Name,
                            AccountType = (int)AccountType.Income,
                            CreatedDate = item.Bill_CreatedOn,
                            CustomerId = item.Bill_VendorId,
                            VendorName = item.Bill_Vendor_Name,
                            VendorId=item.Bill_VendorId,
                        };


                        BillList.Add(dto);
                        VendorList.Add(new SelectVendorItem { VendorId = item.Bill_VendorId, Name = dto.AccountName != null && dto.AccountName != "" ? dto.AccountName : "NAN" });
                    }
                }
                
                List<decimal> CustomerByWiseTotalSales = new List<decimal>();
                VendorList = VendorList.GroupBy(x => x.VendorId).Select(x => x.First()).ToList();
                foreach (var item in VendorList.ToList())
                {
                    CustomerByWiseTotalSales.Add(BillList.Where(x => x.VendorId == item.VendorId).Sum(x => x.Amount));
                }
                BillList = BillList.GroupBy(x => x.VendorId).Select(x => x.First()).ToList();

                List<decimal> VendorByWiseTotalIncome = new List<decimal>();
                List<decimal> VendorByWiseProfit = new List<decimal>();
                foreach (var item in CustomerByWiseTotalSales.ToList())
                {
                    VendorByWiseTotalIncome.Add(item);
                    VendorByWiseProfit.Add(item);
                }
                acc_ProfitAndLoss_By.TotalSales = BillList.Sum(x => x.Amount);
                acc_ProfitAndLoss_By.VendorByWiseTotalSales = CustomerByWiseTotalSales;
                acc_ProfitAndLoss_By.VendorByWiseTotalIncome = VendorByWiseTotalIncome;
                acc_ProfitAndLoss_By.VendorByWiseProfit = VendorByWiseProfit;
                acc_ProfitAndLoss_By.TotalIncome = VendorByWiseTotalIncome.Sum();
                acc_ProfitAndLoss_By.VendorList = VendorList;
                acc_ProfitAndLoss_By.Profit = acc_ProfitAndLoss_By.TotalIncome - acc_ProfitAndLoss_By.TotalExpenses;
                custom.Data = acc_ProfitAndLoss_By;
            }
            catch (Exception ex)
            {
                custom.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                custom.Message = ex.Message;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return custom;
        }
        public List<decimal> GetVendorByAmount(List<DateTime> SelectedMonth, DateTime CreatedDate, decimal Amount)
        {
            List<decimal> ListAmount = new List<decimal>();
            try
            {
                foreach (DateTime item in SelectedMonth.ToList())
                {
                    if (CreatedDate >= new DateTime(item.Year, item.Month, 1) && CreatedDate <= new DateTime(item.Year, item.Month, DateTime.DaysInMonth(item.Year, item.Month)))
                    {
                        ListAmount.Add(Amount);
                    }
                    else
                    {
                        ListAmount.Add(0);
                    }
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return ListAmount;
        }
        public async Task<Custom_Response> GeneratePDFStringProfitLossByVendor(Acc_Report_Filter filter)
        {
            Custom_Response custom = new Custom_Response();
            CultureInfo us = CultureInfo.GetCultureInfo("en-US");
            us = CultureInfo.CreateSpecificCulture("en-US");
            us.NumberFormat.CurrencyNegativePattern = 1;
            Thread.CurrentThread.CurrentCulture = us;
            Thread.CurrentThread.CurrentUICulture = us;
            Custom_Response acc_ProfitAndLoss_By_Month = new Custom_Response();
            try
            {
                Acc_ProfitAndLoss_By_VendorDTO acc_ProfitAndLoss_By = new Acc_ProfitAndLoss_By_VendorDTO();
                StringBuilder html = new StringBuilder();
                html.Append(template.HtmlHeader.Replace("#ReportsTitle", "Profit And Loss By Vendor"));
                var acc_ProfitAndLoss_By_Vendor_Result = ProfitAndLossByVendorReports(filter);
                if (acc_ProfitAndLoss_By_Vendor_Result != null && acc_ProfitAndLoss_By_Vendor_Result.Result != null)
                {
                    acc_ProfitAndLoss_By = acc_ProfitAndLoss_By_Vendor_Result.Result.Data;
                    StringBuilder table = new StringBuilder();
                    table.Append("<table style='border:1px solid black;width:100%; border-collapse: collapse;'>");
                    string THead = "<tr><th style='width:15%; border-left: 1px dotted #c7c7c7; font-weight: 700;text-align: center;border-bottom:1px solid black;'></th>";
                    foreach (var item in acc_ProfitAndLoss_By.VendorList)
                    {
                        THead += "<th style='color: #000; border-left: 1px dotted #c7c7c7; font-weight: 700; text-align: center;border-bottom:1px solid black;'>" + item.Name + "</th>";
                    }
                    THead += "<th style='width:10%; border-left: 1px dotted #c7c7c7; font-weight: 700; text-align: center;border-bottom:1px solid black;'>Total</th></tr>";
                    table.Append(THead);
                    string TrIncomeTitle = "<tr><td colspan='" + (acc_ProfitAndLoss_By.VendorList.Count() + 2) + "' style='width:15%;  text-align: left;'><b>Income</b></td></tr>";
                    table.Append(TrIncomeTitle);
                    string TrSales = "<tr><td style='width:15%; text-align: left;'>Sales</td>";
                    foreach (var item in acc_ProfitAndLoss_By.VendorByWiseTotalIncome)
                    {
                        TrSales += "<td style='color: #000;  text-align: center;'>" + string.Format(us, "{0:C}", Math.Round(item, 2)) + "</td>";
                    }
                    TrSales += "<td style='width:10%;  text-align: center;'>" + string.Format(us, "{0:C}", Math.Round(acc_ProfitAndLoss_By.TotalSales, 2)) + "</td></tr>";
                    table.Append(TrSales);
                    string TrTotalIncomeTitle = "<tr><td  style='width:15%; text-align: left;border-bottom:1px solid black;border-top:1px solid black;'><b>Total Income</b></td>";
                    foreach (var item in acc_ProfitAndLoss_By.VendorByWiseTotalIncome)
                    {
                        TrTotalIncomeTitle += "<td style='color: #000;  text-align: center;border-bottom:1px solid black;border-top:1px solid black;'><b>" + string.Format(us, "{0:C}", Math.Round(item, 2)) + "</b></td>";
                    }
                    TrTotalIncomeTitle += "<td style='width:10%;  text-align: center;border-bottom:1px solid black;border-top:1px solid black;'><b>" + string.Format(us, "{0:C}", Math.Round(acc_ProfitAndLoss_By.TotalIncome, 2)) + "</b></td></tr>";
                    table.Append(TrTotalIncomeTitle);
                    string Profit = "<tr><td style='width:15%; text-align: left;border-bottom:1px solid black;'><b>Profit</b></td>";
                    foreach (var item in acc_ProfitAndLoss_By.VendorByWiseProfit)
                    {
                        Profit += "<td style='color: #000;  text-align: center;border-bottom:1px solid black;'><b>" + string.Format(us, "{0:C}", Math.Round(item, 2)) + "</b></td>";
                    }
                    Profit += "<td style='width:10%;  text-align: center;border-bottom:1px solid black;'><b>" + string.Format(us, "{0:C}", Math.Round(acc_ProfitAndLoss_By.Profit, 2)) + "</b></td></tr>";
                    table.Append(Profit);
                    table.Append("</table>");

                    html.Append(table.ToString());
                }
                html.Append(template.HtmlFooter);
                byte[] pdffile = PDF.GenerateRuntimePDF(html.ToString());
                custom.Data = pdffile;
                custom.HttpStatusCode = System.Net.HttpStatusCode.OK;
                return custom;
            }
            catch (Exception ex)
            {
                custom.Message = ex.Message;
                custom.HttpStatusCode = System.Net.HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return custom;
            }
        }

    }
}
