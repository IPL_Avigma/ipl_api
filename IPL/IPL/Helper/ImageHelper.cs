﻿using System;
using System.Collections.Generic;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Drawing;
using System.Linq;
using System.Web;
using System.IO;

public static class ImageHelper
{
    public static Image ResizePhoto(string photoPath, int width, int height, string watermark_text)
    {
        Bitmap bmPhoto;
        using (FileStream stream = new FileStream(photoPath, FileMode.Open))
        {

            Image imgPhoto = Image.FromStream(stream);

            int sourceWidth = imgPhoto.Width;
            int sourceHeight = imgPhoto.Height;
            int sourceX = 0;
            int sourceY = 0;
            int destX = 0;
            int destY = 0;

            float nPercent = 0;
            float nPercentW = 0;
            float nPercentH = 0;

            nPercentW = ((float)width / (float)sourceWidth);
            nPercentH = ((float)height / (float)sourceHeight);
            if (nPercentH < nPercentW)
            {
                nPercent = nPercentH;
                destX = System.Convert.ToInt16((width -
                              (sourceWidth * nPercent)) / 2);
            }
            else
            {
                nPercent = nPercentW;
                destY = System.Convert.ToInt16((height -
                              (sourceHeight * nPercent)) / 2);
            }

            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            bmPhoto = new Bitmap(width, height, PixelFormat.Format24bppRgb);
            bmPhoto.SetResolution(imgPhoto.HorizontalResolution, imgPhoto.VerticalResolution);

            Graphics grPhoto = Graphics.FromImage(bmPhoto);
            grPhoto.Clear(Color.Black);
            grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;



            grPhoto.DrawImage(imgPhoto, new Rectangle(destX, destY, destWidth, destHeight), new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight), GraphicsUnit.Pixel);
            if (!string.IsNullOrEmpty(watermark_text))
            {
                AddWaterMark(watermark_text, bmPhoto, grPhoto);
            }
            grPhoto.Dispose();
            imgPhoto.Dispose();
            stream.Close();
            stream.Dispose();
        }
        return bmPhoto;
    }

    public static Image AddWaterMark(string photoPath, string watermark_text)
    {
        Bitmap bmPhoto;
        using (FileStream stream = new FileStream(photoPath, FileMode.Open))
        {
            bmPhoto = new Bitmap(stream);
            Graphics grPhoto = Graphics.FromImage(bmPhoto);
            AddWaterMark(watermark_text, bmPhoto, grPhoto);
            grPhoto.Dispose();
            stream.Close();
            stream.Dispose();
        }
        return bmPhoto;
    }
    private static Image AddWaterMark(string watermark_text, Bitmap bmPhoto, Graphics grPhoto)
    {
        Brush brush = new SolidBrush(Color.Black);
        Font font = new Font("Serif", 20, FontStyle.Bold, GraphicsUnit.Pixel);
        SizeF textSize = new SizeF();
        textSize = grPhoto.MeasureString(watermark_text, font);
        Point position = new Point((bmPhoto.Width - ((int)textSize.Width + 10)), (bmPhoto.Height - ((int)textSize.Height + 10)));
        //var rect = new RectangleF(position.X, position.Y, textSize.Width, textSize.Height);
        //grPhoto.FillRectangle(Brushes.Transparent, rect);
        grPhoto.DrawString(watermark_text, font, brush, position);
        return bmPhoto;
    }

    public static byte[] ImageToByteStream(Image img)
    {
        byte[] data = new byte[0];
        using (MemoryStream ms = new MemoryStream())
        {
            img.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
            ms.Seek(0, 0);
            data = ms.ToArray();
        }
        return data;
    }
}
