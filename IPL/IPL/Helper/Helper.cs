﻿using IPL.Models.Account;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

    public static class Helper
    {
        public static Int64 GetBigIntRowData(DataRow dataRow,string field)
        {
            if(dataRow.Table.Columns.Contains(field))
            {
                return dataRow.Field<Int64?>(field)!=null? dataRow.Field<Int64>(field):0;
            }
            else
            {
                return 0;
            }
        }
    public static int GetIntRowData(DataRow dataRow, string field)
    {
        if (dataRow.Table.Columns.Contains(field))
        {
            return dataRow.Field<int?>(field)!=null? dataRow.Field<int>(field):0;
        }
        else
        {
            return 0;
        }
    }
    public static DateTime GetDateTimeRowData(DataRow dataRow, string field)
    {
        if (dataRow.Table.Columns.Contains(field))
        {
            return dataRow.Field<DateTime>(field);
        }
        else
        {
            return new DateTime();
        }
    }
    public static string GetStringRowData(DataRow dataRow, string field)
        {
            if (dataRow.Table.Columns.Contains(field))
            {
                return dataRow.Field<string>(field);
            }
            else
            {
                return string.Empty;
            }
        }
        public static bool GetBoolRowData(DataRow dataRow, string field)
        {
            if (dataRow.Table.Columns.Contains(field))
            {
                return dataRow.Field<bool>(field);
            }
            else
            {
                return false;
            }
        }
        public static decimal GetDecimalRowData(DataRow dataRow, string field)
        {
            if (dataRow.Table.Columns.Contains(field))
            {
                return dataRow.Field<decimal>(field);
            }
            else
            {
                return 0;
            }
        }

    public static decimal GetBalance(int DrOrCr, decimal Debit, decimal Credit)
    {
        var balance = DrOrCr == (int)DrOrCrSide.Dr ? Debit - Credit : Credit - Debit;
        return balance;
    }
    public static decimal GetSign(int DrOrCr, decimal Debit, decimal Credit)
    {
        var balance = DrOrCr == (int)DrOrCrSide.Dr ? Debit - Credit : Credit - Debit;
        return balance;
    }

}
