﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.HelperMessage
{
    public static class Message
    {
        public const string ErrorWhileSaving= "Error while saving in the database";
        public const string SuccessSave = "Successfully saved in the database";
        public const string SuccessDelete = "Successfully deleted in the database";
        public const string OneOrMoreJournalEntry = "One or more journal entry lines has duplicate account.";
        public const string JournalEntryalreadyPost = "Journal Entry Already Post";
        public const string DebitCreditAreNotEqual = "Debit/Credit are not equal.";
        public const string OneOrMoreLineZero = "One or more line(s) amount is zero.";
        public const string DuplicateIdCollection = "Duplicate account id in a collection.";
        public const string OneOfTheAccountNotEqual = "One of the account not equal.";
        public const string AccountCodeIsExist = "Account Number Is Exist";
        public const string GettingErrorWhileBindList = "Getting Error While Bind List";
        public const string InvoiceAmountNotMatch = "Invoice Amount Not Match";
        public const string BillAmountNotMatch = "Bill Amount Not Match";
    }
}