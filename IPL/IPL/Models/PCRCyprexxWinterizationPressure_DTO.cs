﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class PCRCyprexxWinterizationPressure_DTO
    {
        public Int64 PCR_CW_PkeyID { get; set; }
        public String PCR_CW_Pressure_Test { get; set; }
        public String PCR_CW_Upload_photo { get; set; }
        public Boolean? PCR_CW_IsActive { get; set; }
        public Boolean? PCR_CW_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
    }
}