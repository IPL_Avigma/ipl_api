﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPLApp.Models
{
    public class IPLStateDTO
    {
        public Int64 IPL_StateID { get; set; }
        public String IPL_StateName { get; set; }
        public String WhereClause { get; set; }
        public String FilterData { get; set; }
        public Boolean? IPL_State_IsActive { get; set; }
        public Boolean? IPL_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public String IPL_State_CreatedBy  { get; set; }
        public String IPL_State_ModifiedBy { get; set; }
        public int Type { get; set; }
        public Boolean? IPL_State_IsDeleteAllow { get; set; }
    }
    public class IPLState
    {
        public String IPL_StateID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }

    public class IPLState_MobDTO
    {
       public Int64 IPL_StateID { get; set; }
       public String IPL_StateName { get; set; }
}
}