﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class PhotoHeaderTemplateDTO
    {
        public Int64 Photo_head_PkeyId { get; set; }
        public String Photo_head_HeaderTemp { get; set; }
        public Int64? Photo_head_Client_Company { get; set; }
        public String Photo_head_FileName_Temp { get; set; }
        public Boolean? Photo_head_Pdf_Temp { get; set; }
        public Boolean? Photo_head_IsActive { get; set; }
        public Boolean? Photo_head_IsDelete { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
        public String Client_Company_Name { get; set; }
        public string ViewUrl { get; set; }
        public String WhereClause { get; set; }
        public String FilterData { get; set; }
        public String Photo_head_CreatedBy { get; set; }
        public String Photo_head_ModifiedBy { get; set; }
    }
    public class PhotoHeaderTemplate
    {
        public String Photo_head_PkeyId { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}