﻿  using System;
 using System.Collections.Generic;
 using System.Linq;
 using System.Web;
 
 namespace IPL.Models
 {
     public class ClientResult_PropertyLoanSettings_MasterDTO
     {
         public Int64 CRPLS_PkeyID { get; set; }
         public Int64? CRPLS_WO_ID { get; set; }
         public Boolean? CRPLS_IsActive { get; set; }
         public Boolean? CRPLS_IsDelete { get; set; }
         public string CRPLS_BorrowerEmail { get; set; }
         public string CRPLS_BorrowerName { get; set; }
         public string CRPLS_BorrowerPhone { get; set; }
         public string CRPLS_LoanNumber { get; set; }
         public Int64? CRPLS_LoanStatus { get; set; }
         public Int64? CRPLS_LoanType { get; set; }
         public string CRPLS_PropertyMortgagee { get; set; }
         public string CRPLS_UnpaidPrincipalBalance { get; set; }
         public Int64 UserID { get; set; }
         public int Type { get; set; }
     }
     public class ClientResult_PropertyLoanSettings_Master
     {
             public String CRPLS_PkeyID { get; set; }
         public String Status { get; set; }
         public String ErrorMessage { get; set; }
     }
 }