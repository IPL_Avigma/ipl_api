﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPLApp.Models
{
    public class WorkOrderMaster_DTO
    {
        public Int64 Pkey_Id { get; set; }
        public String Category { get; set; }
        public String Wo { get; set; }
        public String address { get; set; }
        public String estimated_complete_date { get; set; }
        public String loan_info { get; set; }
        public String office_locked { get; set; }
        public String due_date { get; set; }
        public String ready_for_office_Contractor { get; set; }
        public String client_Due_date { get; set; }
        public String complete_date { get; set; }
        public String comments { get; set; }
        public String username { get; set; }
        public String follow_up_complete { get; set; }      
        public String broker_info { get; set; }
        public String lot_size { get; set; }
        public DateTime? received_date { get; set; }
        public String customer { get; set; }
        public String client_company { get; set; }
        public String freeze_property { get; set; }
        public String ppw { get; set; }
        public String start_date { get; set; }
        public String contractor { get; set; }
        public String ready_for_office { get; set; }
        public String missing_info { get; set; }
        public String BATF { get; set; }
        public String sub_contractor_follow_up { get; set; }
        public String work_type { get; set; }
        public String cancel_date { get; set; }
        public String is_Inspection { get; set; }
        public String assigned_admin { get; set; }
        public String lklg { get; set; }
        public Boolean? IsActive { get; set; }
        public Boolean? IsDelete { get; set; }
        public int Type { get; set; }
        public int UserId { get; set; }

        public string Mortgagor { get; set; }
    }
}