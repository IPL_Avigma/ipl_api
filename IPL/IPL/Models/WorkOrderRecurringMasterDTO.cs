﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class WorkOrderRecurringMasterDTO
    {
        public Int64 Wo_Rec_PkeyId { get; set; }
        public Int64? Wo_Rec_WoId { get; set; }
        public DateTime? Wo_Rec_ReceiveDate { get; set; }
        public DateTime? Wo_Rec_DueDate { get; set; }
        public Boolean? Wo_Rec_IsProcess { get; set; }
        public Boolean? Wo_Rec_IsActive { get; set; }
        public Boolean? Wo_Rec_IsDelete { get; set; }
        public Int64? UserId { get; set; }
        public int Type { get; set; }
    }
    public class WorkOrderRecurringMaster
    {
        public String Wo_Rec_PkeyId { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }

    public class ActionRecurringModel
    {
        public Int64 WorkOrderID { get; set; }
        public List<DateTime?> Recurs_ReceivedDateArray { get; set; }
        public List<DateTime?> Recurs_DueDateArray { get; set; }
        public Boolean? Recurring { get; set; }
        public Int32? Recurs_Day { get; set; }
        public Int64? Recurs_Period { get; set; }
        public Int64? Recurs_Limit { get; set; }
        public DateTime? Recurs_CutOffDate { get; set; }
        public Int64 UserID { get; set; }
    }
}