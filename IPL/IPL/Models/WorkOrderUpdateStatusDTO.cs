﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class WorkOrderUpdateStatusDTO
    {
        public Int64 workOrder_ID { get; set; }
        public String status { get; set; }
        public Boolean? IsActive { get; set; }
        public Int64 UserId { get; set; }
        public int Type { get; set; }
        public Int64? WN_UserId { get; set; }
        public String IPLNO { get; set; }
        public int? Wo_Start { get; set; }
        public String address1 { get; set; }
    }
    public class statusDetailsDTO
    {
        public Int64 workOrder_ID { get; set; }
        public int status { get; set; }
        public string Status_Name { get; set; }
        public DateTime? Received_Date { get; set; }
        public DateTime? Complete_Date { get; set; }
        public DateTime? Cancel_Date { get; set; }
        public DateTime? SentToClient_date { get; set; }
        public DateTime? OfficeApproved_date { get; set; }
        public DateTime? Field_complete_date { get; set; }
        public Int64? Status_UserID { get; set; }
    }
}