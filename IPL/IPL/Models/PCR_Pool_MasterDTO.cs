﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class PCR_Pool_MasterDTO
    {
        public Int64 PCR_Pool_pkeyId { get; set; }
        public Int64 PCR_Pool_MasterId { get; set; }
        public Int64 PCR_Pool_WO_Id { get; set; }
        public int? PCR_Pool_ValType { get; set; }
        public String PCR_Pool_Info_Pool_Present { get; set; }
        public String PCR_Pool_Diameter_Ft { get; set; }
        public String PCR_Pool_Length_Ft { get; set; }
        public String PCR_Pool_Width_Ft { get; set; }
        public String PCR_Pool_Condition_Good { get; set; }
       // public Boolean? PCR_Pool_Condition_Fair { get; set; }
       // public Boolean? PCR_Pool_Condition_Poor { get; set; }
       // public Boolean? PCR_Pool_Condition_Damaged { get; set; }
        public String PCR_Pool_Type_InGround { get; set; }
       // public Boolean? PCR_Pool_Type_Above_Ground { get; set; }
        public String PCR_Pool_Secure_On_This_Order { get; set; }
        public String PCR_Pool_Is_There_Fence { get; set; }
        public String PCR_Pool_Is_It_Locked { get; set; }
        public String PCR_Pool_Water_Level_Full { get; set; }
      //  public Boolean? PCR_Pool_Water_Level_Partially_Filled { get; set; }
      //  public Boolean? PCR_Pool_Water_Level_Empty { get; set; }
        public String PCR_Pool_Did_You_Drain_It { get; set; }
        public String PCR_Pool_Dismantled_Removed { get; set; }
        public String PCR_Pool_Is_There_Depression_Left { get; set; }
        public String PCR_Pool_Secured_Per_Guidelines { get; set; }
        public String PCR_Pool_Is_The_Pool_Converted_Prevents_Entry { get; set; }
        public String PCR_Pool_Hot_Tub_Present { get; set; }
        public String PCR_Pool_Bids_Drain_Shock_Install_Safety_Cover { get; set; }
        public String PCR_Pool_Bid_To_Install_Safety_Cover { get; set; }
        public String PCR_Pool_Bid_To_Drain { get; set; }
        public String PCR_Pool_Bid_To_Dismantle { get; set; }
        public String PCR_Pool_Drain_Remove { get; set; }
        public String PCR_Pool_Bid_To_Fill_Hole { get; set; }
        public String PCR_Pool_Size_Of_Hole { get; set; }
        public String PCR_Pool_Cubic_Yds_Of_Dirt { get; set; }
        public String PCR_Pool_Secure_This_Order_No_Secure_By_FiveBrothers { get; set; }
       //  public Boolean? PCR_Pool_Secure_This_Order_No_Bid_To_Secure { get; set; }
       // public Boolean? PCR_Pool_Secure_This_Order_No_Previously_Secure_By_Order { get; set; }
       // public Boolean? PCR_Pool_Secure_This_Order_No_Other { get; set; }
        public String PCR_Pool_Hot_Tub_Present_Yes_Covered_Drained { get; set; }
        public String PCR_Pool_Hot_Tub_Did_You_Secure { get; set; }
        public String PCR_Pool_Hot_Tub_Bids_Diameter_Ft { get; set; }
        public String PCR_Pool_Hot_Tub_Bids_Length_Ft { get; set; }
        public String PCR_Pool_Hot_Tub_Bids_Width_Ft { get; set; }
        public String PCR_Pool_Hot_Tub_Bids_Bid_To_Drain { get; set; }
        public String PCR_Pool_Hot_Tub_Bids_Bit_To_Install_Cover { get; set; }
        public String PCR_Pool_Hot_Tub_Bids_Drain_Secure { get; set; }
        public Boolean? PCR_Pool_IsActive { get; set; }
        public Boolean? PCR_Pool_IsDelete { get; set; }
        public Int64 UserID { get; set; }
        public Int64 fwo_pkyeId { get; set; }
        public int Type { get; set; }

    }

    public class PCR_Pool_Master
    {
        public String PCR_Pool_pkeyId { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}