﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class UOM_MasterDTO
    {
        public Int64 UOM_pkeyId { get; set; }
        public String UOM_Name { get; set; }
        public String FilterData { get; set; }
        public String WhereClause { get; set; }
        public String UOM_CreatedBy { get; set; }
        public String UOM_ModifiedBy { get; set; }
        public Boolean? UOM_IsActive { get; set; }
        public Boolean? UOM_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
    }
    public class UOM_Master
    {
        public String UOM_pkeyId { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}