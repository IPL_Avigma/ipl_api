﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Folder_State_MasterDTO
    {
        public Int64 Fold_State_PkeyId { get; set; }
        public Int64? Fold_State_Id { get; set; }
        public Int64? Fold_State_Folder_Id { get; set; }
        public Int64? Fold_State_File_Id { get; set; }
        public Int64? Fold_State_Auto_Assine_Id { get; set; }
        public Boolean? Fold_State_IsActive { get; set; }
        public Boolean? Fold_State_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int? Type { get; set; }
        public Int64? Folder_File_Master_FK_Id { get; set; }
    }
}