﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Filter_Admin_Loan_MasterDTO
    {
        public Int64 Loan_Filter_PkeyID { get; set; }
        public String Loan_Filter_LoanName { get; set; }
        public String Loan_Filter_CreatedBy { get; set; }
        public String Loan_Filter_ModifiedBy { get; set; }
        public Boolean? Loan_Filter_LoanIsActive { get; set; }
        public Boolean Loan_Filter_IsActive { get; set; }
        public Boolean? Loan_Filter_IsDelete { get; set; }
        public int? Type { get; set; }
        public Int64? UserId { get; set; }
    }
}