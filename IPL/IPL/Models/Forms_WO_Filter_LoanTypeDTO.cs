﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Forms_WO_Filter_LoanTypeDTO
    {
        public long WOF_LoanType_pkeyId { get; set; }
        //public long WOF_LoanTypeId { get; set; }
        public long Loan_pkeyId { get; set; }
        public long WOF_FilterId { get; set; }
        public long WOF_FormId { get; set; }
        public bool WOF_LoanType_IsActive { get; set; }
        public bool WOF_LoanType_IsDelete { get; set; }
        public long UserId { get; set; }
        public DateTime WOF_LoanType_CreatedOn { get; set; }  
        public DateTime WOF_LoanType_ModifiedOn { get; set; }    
        public DateTime WOF_LoanType_DeletedOn { get; set; }
    }
}