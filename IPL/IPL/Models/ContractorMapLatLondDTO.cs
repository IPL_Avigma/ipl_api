﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class ContractorMapLatLondDTO
    {
        public Int64 Zip_ID { get; set; }
        public Int64? Zip_zip { get; set; }
        public String Zip_lat { get; set; }
        public String Zip_lng { get; set; }
        public String Zip_city { get; set; }
        public String Zip_state_name { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
    }
}