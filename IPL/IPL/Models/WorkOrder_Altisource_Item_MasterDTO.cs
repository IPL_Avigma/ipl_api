﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class WorkOrder_Altisource_Item_MasterDTO
    {
        public Int64? WALTISOURCEINS_PkeyId { get; set; }
        public String WALTISOURCEINS_Ins_Name { get; set; }
        public String WALTISOURCEINS_Ins_Details { get; set; }
        public Int64? WALTISOURCEINS_Qty { get; set; }
        public Decimal? WALTISOURCEINS_Price { get; set; }
        public Decimal? WALTISOURCEINS_Total { get; set; }

        public Int64? WALTISOURCEINS_FkeyID { get; set; }
        public Boolean? WALTISOURCEINS_IsActive { get; set; }
        public Boolean? WALTISOURCEINS_IsDelete { get; set; }
        public String WALTISOURCEINS_Additional_Details { get; set; }


        public Int64 UserID { get; set; }
        public int Type { get; set; }
    }

    public class WorkOrder_Altisource_Item_Master
    {
        public String WALTISOURCEINS_PkeyId { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}