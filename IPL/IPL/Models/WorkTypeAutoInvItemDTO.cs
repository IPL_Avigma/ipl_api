﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPLApp.Models
{
    public class WorkTypeAutoInvItemDTO
    {
        public Int64 Auto_Inv_Itm_PkeyID { get; set; }
        public Int64? Auto_Inv_Itm_ID { get; set; }
        public Int64? Auto_Inv_Itm_Quantity { get; set; }
        public int? Auto_Inv_Itm_Flag { get; set; }
        public Int64? Auto_Inv_Itm_WorkTypeID { get; set; }
        public Boolean? Auto_Inv_Itm_IsActive { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
        public List<AutoInvItmStringArr> Auto_Inv_Itm_StringArr { get; set; }
    }
    public class AutoInvItmStringArr
    {
        public int Name { get; set; }
        public int LineItem { get; set; }
        public int Quntity { get; set; }
    }
    public class WorkTypeAutoInvItem
    {
        public String Auto_Inv_Itm_PkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}