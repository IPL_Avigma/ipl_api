﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IPL.Models;


namespace IPL.Models
{
    public class GetWorkorderNotificationDetailsDTO
    {
        public Int64 User_pkeyID { get; set; }
        public Int64 workOrder_ID { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
        public string message { get; set; }
        public string msgtitle { get; set; }
        public int from { get; set; }
        public int Action { get; set; }
    }
}