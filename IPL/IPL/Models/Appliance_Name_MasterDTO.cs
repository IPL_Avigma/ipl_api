﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Appliance_Name_MasterDTO
    {
        public Int64 App_pkeyId { get; set; }
        public String App_Apliance_Name { get; set; }
        public Boolean? App_IsActive { get; set; }
        public Boolean? App_IsDelete { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }

        public string WhereClause { get; set; }
        public int PageNumber { get; set; }
        public int Rowcount { get; set; }
        public string FilterData { get; set; }
    }
    public class Appliance_NameMaster
    {
        public String App_pkeyId { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }

    public class GroupRoleMasterDTO
    {
        public Int64 Group_DR_PkeyID { get; set; }
        public String Group_DR_Name { get; set; }
        public Boolean? Group_DR_IsActive { get; set; }
        public Boolean? Group_DR_IsDelete { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
        public string WhereClause { get; set; }
        public int PageNumber { get; set; }
        public int Rowcount { get; set; }
         public string FilterData { get; set; }
    }
    public class GroupRoleMaster
    {
        public String Group_DR_PkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }

    }

    public class NewApplianceNameMaster_Filter
    {
        public string App_Apliance_Name { get; set; }
       
    }
    public class NewGroupRoleMaster_Filter
    {
        public string Group_DR_Name { get; set; }

    }

}