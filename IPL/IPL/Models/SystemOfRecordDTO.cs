﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPLApp.Models
{
    public class SystemOfRecordDTO
    {
        public Int64 Sys_Of_Rcd_PkeyID { get; set; }
        public String Sys_Of_Rcd_Name { get; set; }
        public Boolean? Sys_Of_Rcd_IsActive { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
    }
    public class SystemOfRecord
    {
        public String Sys_Of_Rcd_PkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}