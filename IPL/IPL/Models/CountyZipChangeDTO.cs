﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class CountyZipChangeDTO
    {
        public Int64 Zip_ID { get; set; }
        public Int64? Zip_zip { get; set; }
        public String Zip_state_id { get; set; }
        public String Zip_county_name { get; set; }
        public Boolean? Cont_Coverage_Area_IsActive { get; set; }
        public Int64 UserID { get; set; }
        public Int64 IPL_StateID { get; set; }
        public int Type { get; set; }
        public String FilterData { get; set; }
        public String whereclause { get; set; }
        public Int64? Cont_Coverage_Area_UserID { get; set; }
        public Int64? County_ID { get; set; }
    }

    public class UserStateDTO
    {
        public Int64 IPL_StateID { get; set; }
        public String IPL_StateName { get; set; }
        public String whereclause { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
        public Int64 CompanyId { get; set; }
    }
     public class UserStateCountDTO
    {
        public Int64 ID { get; set; }
        public String COUNTY { get; set; }
        public int Type { get; set; }
    }
}