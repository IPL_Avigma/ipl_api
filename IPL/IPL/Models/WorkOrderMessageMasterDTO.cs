﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class WorkOrderMessageMasterDTO
    {
        public Int64 Msg_PkeyId { get; set; }
        public Int64 Msg_Wo_Id { get; set; }
        public Int64 Msg_From_UserId { get; set; }
        public Int64? Msg_To_UserId { get; set; }
        public Int64? Msg_To_UserId_A { get; set; }
        public Int64? Msg_To_UserId_B { get; set; }
        public String Msg_Message_text { get; set; }
        public String Msg_Time { get; set; }
        public int Msg_Status { get; set; }
        public String Msg_Message_Id { get; set; }
        public Boolean? Msg_IsActive { get; set; }
        public Boolean? Msg_IsDelete { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }

    }
}