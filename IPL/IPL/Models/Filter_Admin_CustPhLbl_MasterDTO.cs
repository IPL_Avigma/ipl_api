﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Filter_Admin_CustPhLbl_MasterDTO
    {
        public Int64 CustPhLbl_Filter_PkeyID { get; set; }
        public String CustPhLbl_Filter_CustPhLblName { get; set; }
        public Boolean? CustPhLbl_Filter_CustPhLblIsActive { get; set; }
        public Boolean CustPhLbl_Filter_IsActive { get; set; }
        public Boolean? CustPhLbl_Filter_IsDelete { get; set; }
        public int? Type { get; set; }
        public Int64? UserId { get; set; }
    }
}