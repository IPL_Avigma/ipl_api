﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Web;

namespace IPL.Models
{
    public class ContactTypeMasterDTO
    {
        public Int64 CT_PkeyId { get; set; }
        public string CT_Name { get; set; }
        public bool CT_IsActive { get; set; }
        public string CT_CreatedBy { get; set; }
        public string CT_ModifiedBy { get; set; }
        public Int64 UserId { get; set; }
        public int Type { get; set; }
    }
}