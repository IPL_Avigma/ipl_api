﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPLApp.Models
{
    public class LoginDTO
    {
        public string username { get; set; }
        public string password { get; set; }
        public string token { get; set; }
        public string URL { get; set; }
        public string FBUsername { get; set; }
        public string FBPassword { get; set; }
        public Int64 UserID { get; set; }
        public Int64 WI_Pkey_ID { get; set; }
        public Int64 importfrom { get; set; }
        public Boolean? image_download { get; set; }
        public String rep_code { get; set; }
        public String Scrapper_rep_code { get; set; }
    }

    public class RootLoginDTO
    {
        public string username { get; set; }
        public string token { get; set; }
    }

    public class ScrapperResponseDTO
    {
        public string Responseval { get; set; }
        public string ResponseMessage { get; set; }

    }

}