﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class WorkOrder_ServiceLink_History_MasterDTO
    {
        public Int64 WServiceLink_HS_PkeyID { get; set; }
        public String WServiceLink_HS_wo_id { get; set; }
        public String WServiceLink_HS_Address { get; set; }
        public String WServiceLink_HS_City { get; set; }
        public String WServiceLink_HS_State { get; set; }
        public String WServiceLink_HS_ZipCode { get; set; }
        public DateTime WServiceLink_HS_DueDate { get; set; }
        public String WServiceLink_HS_KeyCode { get; set; }
        public String WServiceLink_HS_LockBox { get; set; }

        public String WServiceLink_HS_ClientID { get; set; }
        public String WServiceLink_HS_Loan { get; set; }
        public String WServiceLink_HS_LoanType { get; set; }
        public String WServiceLink_HS_Owner { get; set; }
        public String WServiceLink_HS_CoverageID { get; set; }
        public String WServiceLink_HS_Investor { get; set; }
        public String WServiceLink_HS_services { get; set; }
        public String WServiceLink_HS_id { get; set; }
        public String WServiceLink_HS_username { get; set; }


        public String WServiceLink_HS_Comments { get; set; }
        public String WServiceLink_HS_gpsLatitude { get; set; }
        public String WServiceLink_HS_gpsLongitude { get; set; }
        public Int64 WServiceLink_HS_ImportMaster_Pkey { get; set; }
        public String WServiceLink_HS_Import_File_Name { get; set; }
        public String WServiceLink_HS_Import_FilePath { get; set; }
        public String WServiceLink_HS_Import_Pdf_Name { get; set; }
        public String WServiceLink_HS_Import_Pdf_Path { get; set; }
        public Boolean? WServiceLink_HS_IsActive { get; set; }
        public Boolean? WServiceLink_HS_IsDelete { get; set; }
        public String WServiceLink_HS_Service_ID { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
    }

    public class WorkOrder_ServiceLink_History_Master
    {
        public String WServiceLink_HS_PkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}