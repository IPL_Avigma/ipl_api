﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Forms_QuestionTypeDTO
    {
        public Int64 QuestionTypeId { get; set; }
        public string QuestionType { get; set; }
        public Int64 UserId { get; set; }
        public bool QueType_IsActive { get; set; }       
        public bool QueType_IsMultiple { get; set; }
        public string QueType_ControlType { get; set; }
    }
}