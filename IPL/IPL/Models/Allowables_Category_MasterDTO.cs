﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Allowables_Category_MasterDTO
    {
        public Int64 Allowables_Cat_PkeyId { get; set; }
        public String Allowables_Cat_Name { get; set; }
        public Boolean? Allowables_Cat_IsActive { get; set; }
        public Boolean? Allowables_Cat_IsDelete { get; set; }   
        public Int64? UserID { get; set; }   
        public int Type { get; set; }   
        public String FilterData { get; set; }   
        public String WhereClause { get; set; }   
        public String Allowables_Cat_CreatedBy { get; set; }   
        public String Allowables_Cat_ModifiedBy { get; set; }   
    }
    public class Allowables_Category
    {
        public String Allowables_Cat_PkeyId { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}