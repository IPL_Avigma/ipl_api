﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class PCR_MCS_Grass_Cut_Form_Master_DTO
    {
        public Int64 MG_PkeyID { get; set; }
        public Int64 MG_WO_ID { get; set; }
        public Int64 MG_Company_ID { get; set; }
        public string MG_Property_Info { get; set; }
        public string MG_Completion_Info { get; set; }
        public string MG_Access_Issue { get; set; }
        public string MG_Validation { get; set; }
        public string MG_Check_Ins { get; set; }
        public string MG_Notes { get; set; }
        public string MG_Expected_Completion_Date { get; set; }
        public bool MG_IsActive { get; set; }
        public bool MG_IsDelete { get; set; }
        public Int64 UserID { get; set; }
        public Int64 fwo_pkyeId { get; set; }
        public int Type { get; set; }
    }
}