﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class PCRCyprexxGrassChecklist_DTO
    {
        public Int64 CG_PkeyID { get; set; }
        public String CG_Property_Maintenance { get; set; }
        public String CG_General_Property_Info { get; set; }
        public String CG_Pool_Information { get; set; }
        public String CG_Utilities { get; set; }
        public String CG_Recommended_Services { get; set; }
        public String CG_General_Comments { get; set; }
        public String CG_Order_Completion { get; set; }
        public Boolean? CG_IsActive { get; set; }
        public Boolean? CG_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
    }
}