﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class WorkOrder_Cyprexx_MasterDTO
    {
        public Int64 WCyprexx_PkeyID { get; set; }
        public String WCyprexx_wo_id { get; set; }
        public String WCyprexx_address { get; set; }
        public String WCyprexx_City { get; set; }
        public String WCyprexx_State { get; set; }
        public String WCyprexx_Zip { get; set; }
        public String WCyprexx_Customer { get; set; }
        public String WCyprexx_Loan_Type { get; set; }
        public String WCyprexx_Lock_Code { get; set; }
        public String WCyprexx_Key_Code { get; set; }
        public DateTime? WCyprexx_Received_Date { get; set; }
        public DateTime? WCyprexx_Due_Date { get; set; }
        public String WCyprexx_Comments { get; set; }
        public String WCyprexx_gpsLatitude { get; set; }
        public String WCyprexx_gpsLongitude { get; set; }
        public Int64 WCyprexx_ImportMaster_Pkey { get; set; }
        public String WCyprexx_Import_File_Name { get; set; }
        public String WCyprexx_Import_FilePath { get; set; }
        public String WCyprexx_Import_Pdf_Name { get; set; }
        public String WCyprexx_Import_Pdf_Path { get; set; }
        public Boolean? WCyprexx_IsProcessed { get; set; }
        public Boolean? WCyprexx_IsActive { get; set; }
        public Boolean? WCyprexx_IsDelete { get; set; }
        public String WCyprexx_Cyprexx_ID { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
    }



    public class WorkOrder_CyprexxItem_MasterDTO
    {
        public String wo { get; set; }
        public string address { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zip { get; set; }
        public string customer { get; set; }
        public string loan_type { get; set; }
        public string lock_code { get; set; }
        public string key_code { get; set; }
        public DateTime? received_date { get; set; }
        public DateTime? due_date { get; set; }
        public string comments { get; set; }
        //public string work_order_item_details { get; set; }
        public List<CyprexxInstruction> work_order_item_details { get; set; }
    }

    public class CyprexxInstruction
    {
        public string instruction_name { get; set; }
        public string description { get; set; }
        public string additional_details { get; set; }
    }

    public class WorkOrder_Cyprexx_Master
    {
        public String WCyprexx_PkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}