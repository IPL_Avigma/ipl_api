﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPLApp.Models
{
    public class CustomerMasterDTO
    {
        public Int64 Cust_pkeyId { get; set; }
        public String Cust_Name { get; set; }
        public Boolean? Cust_IsActive { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
    }
    public class CustomerMaster
    {
        public String Cust_pkeyId { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}