﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class AdvanceReportMasterDTO
    {
        public Int64? ReportTypeId { get; set; }
        public Int64? GroupById { get; set; }
        public Int64? StatusId { get; set; }
        public Int64? ClientId { get; set; }
        public Int64? CustomerId { get; set; }
        public Int64? ContractorId { get; set; }
        public Int64? ProcessorId { get; set; }
        public Int64? CordinatorId { get; set; }
        public Int64 workOrder_ID { get; set; }
        public String IPLNO { get; set; }
        public String workOrderNumber { get; set; }
        public String Status_Name { get; set; }
        public Int64? Contractor { get; set; }
        public String address1 { get; set; }
        public String city { get; set; }
        public String state { get; set; }
        public Int64? zip { get; set; }
        public Int64? WorkType { get; set; }
        public DateTime? dueDate { get; set; }
        public DateTime? Field_complete_date { get; set; }
        public String Client_Invoice_Number { get; set; }
        public DateTime? Client_InvoiceDate { get; set; }
        public Decimal? Client_InvoiceTotal { get; set; }
        public String ContractorName { get; set; }
        public String Client_Company_Name { get; set; }
        public String whereClause { get; set; }
        public Boolean? IsActive { get; set; }
        public Boolean? IsDelete { get; set; }
        public int Type { get; set; }        
        public String CordinatorName { get; set; }
        public String ProcessorName { get; set; }
        public String IPL_StateName { get; set; }
        public String WT_WorkType { get; set; }
        public DateTime? InvoiceDateFrom { get; set; }
        public DateTime? InvoiceDateTo { get; set; }
        public DateTime? SentToClient_date { get; set; }
        public DateTime? OfficeApproved_date { get; set; }
        public String CategoryName { get; set; }
        public Decimal? Client_InvoicePaid { get; set; }
        public String Con_Invoice_Number { get; set; }
        public DateTime? Con_InvoiceDate { get; set; }
        public Decimal? Con_InvoiceTotal { get; set; }
        public Decimal? Con_InvoicePaid { get; set; }
        public Int64? Con_Pay_Invoice_Id { get; set; }
        public Int64? Client_Pay_Invoice_Id { get; set; }
        public DateTime? Con_InvoicePaid_Date { get; set; }
        public DateTime? Client_InvoicePaid_Date { get; set; }
        public Int64? UserId { get; set; }
        public Int64? WoFilterId { get; set; }
        public Int32? status { get; set; }
        public string Task_Name { get; set; }
        public List<FilterData> FilterData { get; set; }
        public Decimal? Client_Pay_Amount { get; set; }

        public DateTime? ReadyOfficeDateFrom { get; set; }
        public DateTime? ReadyOfficeDateTo { get; set; }

        public DateTime? SentToClientDateFrom { get; set; }
        public DateTime? SentToClientDateTo { get; set; }

        public DateTime? CompletedDateFrom { get; set; }
        public DateTime? CompletedDateTo { get; set; }

        public DateTime? CreatedDateFrom { get; set; }
        public DateTime? CreatedDateTo { get; set; }

        public DateTime? OfficeApproveDateFrom { get; set; }
        public DateTime? OfficeApproveDateTo { get; set; }

        public DateTime? ClientCheckDateFrom { get; set; }
        public DateTime? ClientCheckDateTo { get; set; }

        public DateTime? InvoiceDateFromstatus { get; set; }
        public DateTime? InvoiceDateTostatus { get; set; }
        public int? StausReportType { get; set; }
        public string WorkTypeMultiple { get; set; }
    }

    public class WorkTypeReport
    {
        public Int64 WT_pkeyID { get; set; }
        public string WT_WorkType { get; set; }
    }

        public class AdvanceFilterReportMasterDTO
    {
        public Int64 workOrder_ID { get; set; }
        public String IPLNO { get; set; }
        public String workOrderNumber { get; set; }
        public String Status_Name { get; set; }
        public Int64? Contractor { get; set; }
        public String address1 { get; set; }
        public String city { get; set; }
        public Int64? state { get; set; }
        public Int64? zip { get; set; }
        public Int64? WorkType { get; set; }
        public DateTime? dueDate { get; set; }
        public DateTime? Field_complete_date { get; set; }
        public String ContractorName { get; set; }
        public String Client_Company_Name { get; set; }
        public String CordinatorName { get; set; }
        public String ProcessorName { get; set; }
        public String IPL_StateName { get; set; }
        public String WT_WorkType { get; set; }
        public DateTime? SentToClient_date { get; set; }
        public DateTime? OfficeApproved_date { get; set; }
        public String CategoryName { get; set; }
        public DateTime? EstimatedDate { get; set; }
        public DateTime? clientDueDate { get; set; }
        public DateTime? Cancel_Date { get; set; }
        public DateTime? DateCreated { get; set; }
        public String Loan_Number { get; set; }
        public String Lotsize { get; set; }
        public String Lock_Code { get; set; }
        public Int32? status { get; set; }

    }
    public class FilterData
    {
        public Int64 Report_WO_Filter_Ch_FeildId { get; set; }
        public String Report_WO_Filter_Ch_FeildOperator { get; set; }
        public String Report_WO_Filter_Ch_FeildValue { get; set; }
        public DateTime? Report_WO_Filter_Ch_FeildDateValue { get; set; }
        public String Report_WO_Filter_Ch_FeildName { get; set; }
        public Int64? Report_WO_Filter_Ch_PkeyId { get; set; }
        public Int64? Report_WO_Filter_Ch_Master_FKeyId { get; set; }
        public Boolean? isDateFeild { get; set; }
    }
}