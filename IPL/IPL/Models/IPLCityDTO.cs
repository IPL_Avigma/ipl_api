﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPLApp.Models.data
{
    public class IPLCityDTO
    {
        public Int64 IPL_CityID { get; set; }
        public String IPL_CityName { get; set; }
        public Int64? IPL_StateID { get; set; }
        public Boolean? IPL_IsActive { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
    }
}