﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class ApplianceMasterDTO
    {
        public Int64 Appl_pkeyId { get; set; }
        public Int64 Appl_Wo_Id { get; set; }
        public Int64 Appl_App_Id { get; set; }
        public String Appl_Apliance_Name { get; set; }
        public String App_Apliance_Name { get; set; }
        public String Appl_Comment { get; set; }
        public int? Appl_Status_Id { get; set; }
        public Boolean? Appl_IsActive { get; set; }
        public Boolean? Appl_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int? Type { get; set; }
        public Int64 App_pkeyId { get; set; }
        public String str_Appl_Status_Id { get; set; }
    }

     public class Appliance_Name_Dto
    {
        public Int64 App_pkeyId { get; set; }
        public String App_Apliance_Name { get; set; }
        public Boolean? App_IsActive { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
    }
    public class RootObject_App
    {
        public List<ApplianceMasterDTO> ApplianceMasterDTO { get; set; }
        public Int64 Appl_Wo_Id { get; set; }
        public int Type { get; set; }
        public Int64 UserID { get; set; }
        public String str_Appl_Status_Id { get; set; }
        public string str_Task_Bid_WO_ID { get; set; }
    }
}