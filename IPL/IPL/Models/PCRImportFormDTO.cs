﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class PCRImportFormDTO
    {
        public OptionsPcrForms options_pcr_forms { get; set; }
        public List<OptionsPcrFormsQuestions> options_pcr_forms_questions { get; set; }
        public List<OptionsPcrFormsQuestionsValues> options_pcr_forms_questions_values { get; set; }
        public List<OptionsPcrFormsRules> options_pcr_forms_rules { get; set; }
        public List<OptionsPcrFormsRulesChecks> options_pcr_forms_rules_checks { get; set; }
        public List<OptionsPcrFormsPhotoRules> options_pcr_forms_photo_rules { get; set; }

    }
    public class KeyValueDTO
    {
        public String Key { get; set; }
        public String Value { get; set; }
    }
    public class ImportPCRFormDTO
    {
        public Int64 Imtr_FromId { get; set; }
        public String Imtr_FileName { get; set; }
        public String Imtr_FilePath { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
        public String Imtr_File { get; set; }


    }
    public class OptionsPcrForms
    {
        public Int64? pcr_form_id { get; set; }
        public String org_id { get; set; }
        public String pcr_form_name { get; set; }
        public String default_required { get; set; }
        public String form_photos_required { get; set; }
        public String transfer_prev_values { get; set; }
        public String client_new_wo { get; set; }
        public String global_form { get; set; }
        public String web_template { get; set; }
        public String fillable_pdf { get; set; }
        public String pcr_form_active { get; set; }
        public String pruvan_ver { get; set; }
        public String thirdparty_id { get; set; }
        public String form_deleted { get; set; }
        public String form_share_name { get; set; }
        public String pruvan_master_id { get; set; }
        public String insert_by { get; set; }
        public String insert_date { get; set; }
        public String publish_changes { get; set; }
        public String inspectorade_id { get; set; }
        public String cyprexx_id { get; set; }
        public String pruvan_id { get; set; }

    }

    public class OptionsPcrFormsQuestions
    {
        public Int64? pcr_question_id { get; set; }
        public Int64? pcr_form_id { get; set; }
        public String question_num { get; set; }
        public String question_name { get; set; }
        public String question_desc { get; set; }
        public String question_type { get; set; }
        public String question_data_type { get; set; }
        public String question_display_active { get; set; }
        public String question_required { get; set; }
        public String question_na_option { get; set; }
        public String question_flag_photos { get; set; }
        public String question_photos_required { get; set; }
        public String question_active { get; set; }
        public String question_deleted { get; set; }
        public String question_form_length { get; set; }
        public String question_order { get; set; }
        public String pdf_field_id { get; set; }
        public String thirdpartyfield_id { get; set; }
        public String question_share_name { get; set; }
        public String question_transfer_prev_values { get; set; }
        public String question_show_prev_values { get; set; }
        public String question_auto_transfer_prev_values { get; set; }
        public String question_field_rules_cnt { get; set; }
        public String question_action_rules_cnt { get; set; }
        public String question_photo_rules_cnt { get; set; }
        public String insert_by { get; set; }
        public String insert_date { get; set; }

    }

    public class OptionsPcrFormsQuestionsValues
    {
        public Int64? question_value_id { get; set; }
        public Int64? pcr_question_id { get; set; }
        public String question_value { get; set; }
        public String pdf_field_id { get; set; }
        public String question_value_active { get; set; }
        public String thirdparty_value_id { get; set; }
        public String question_share_value { get; set; }

    }

    public class OptionsPcrFormsRules
    {
        public Int64? pcr_form_rule_id { get; set; }
        public Int64? pcr_form_id { get; set; }
        public Int64? pcr_question_id { get; set; }
        public String rule_type { get; set; }
        public String rule_operator { get; set; }
        public String rule_value { get; set; }
        public String insert_by { get; set; }
        public String insert_date { get; set; }

    }

    public class OptionsPcrFormsRulesChecks
    {
        public Int64? pcr_rule_check_id { get; set; }
        public Int64? pcr_form_rule_id { get; set; }
        public Int64? pcr_question_id { get; set; }
        public String check_operator { get; set; }
        public String check_value { get; set; }
        public String insert_by { get; set; }
        public String insert_date { get; set; }

    }
    public class OptionsPcrFormsPhotoRules
    {
        public Int64? pcr_photo_rule_id { get; set; }
        public Int64? pcr_question_id { get; set; }
        public String rule_operator { get; set; }
        public String rule_value { get; set; }
        public String take_photo { get; set; }
        public String require_photo { get; set; }
        public String min_photo { get; set; }
        public String max_photo { get; set; }
        public String insert_by { get; set; }
        public String insert_date { get; set; }

    }
}