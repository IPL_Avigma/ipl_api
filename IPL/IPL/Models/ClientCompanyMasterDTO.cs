﻿using IPLApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPLApp.Models
{
    public class ClientCompanyMasterDTO
    {
        public Int64 Client_pkeyID { get; set; }
        public String Client_Company_Name { get; set; }
        public Decimal? Client_Discount { get; set; }
        public Decimal? Client_Contractor_Discount { get; set; }
        public String Client_Billing_Address { get; set; }
        public String Client_City { get; set; }
        public String IPL_StateName { get; set; }
        public int? Client_StateId { get; set; }
        public Int64? Client_ZipCode { get; set; }
        public int? Client_DateTimeOverlay { get; set; }
        public String Client_BackgroundProvider { get; set; }
        public String Client_ContactType { get; set; }
        public String Client_ContactName { get; set; }
        public String Client_ContactEmail { get; set; }
        public String Client_ContactPhone { get; set; }

        public String Client_Comments { get; set; }
        public Int64? Client_Due_Date_Offset { get; set; }
        public Int64? Client_Photo_Resize_width { get; set; }
        public Int64? Client_Photo_Resize_height { get; set; }
        public Decimal? Client_Invoice_Total { get; set; } // change to bool
        public Boolean? Client_Login { get; set; }
        public String Client_Login_Id { get; set; }
        public String Client_Password { get; set; }
        public String Client_Rep_Id { get; set; }
        public Boolean? Client_Lock_Order { get; set; }
        public String Client_Lock_Order_Reason { get; set; }
        public int? Client_IPL_Mobile { get; set; }
        public int? Client_Provider { get; set; }
        public int? Client_Active { get; set; }

        public String Client_ClientPhone { get; set; }
        public String Client_FaxNumbar { get; set; }
        public String Client_Website_Link { get; set; }

        public Boolean? Client_IsActive { get; set; }
        public Boolean? Client_IsDelete { get; set; }
        public Boolean? Client_IsDeleteAllow { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
        public String ErrorMessage { get; set; }
        public String Client_CreatedBy { get; set; }
        public String Client_ModifiedBy { get; set; }
        public List<CompaniesMultiAddress> CompaniesMultiAddress { get; set; }

        public List<ClientContactListDTO> ClientContactList { get; set; }

        //public List<ClientContactListDTO> ClientContactList { get; set; }

        //public Int64 Clnt_Con_List_pkeyID { get; set; }
        //public Int64 Clnt_Con_List_ClientComID { get; set; }
        //public String Clnt_Con_List_BillingDeptName { get; set; }
        //public String Clnt_Con_List_BillingDeptEmail { get; set; }
        //public String Clnt_Con_List_BillingDeptPhone { get; set; }
        //public String Clnt_Con_List_TechSuppName { get; set; }
        //public String Clnt_Con_List_TechSuppEmail { get; set; }
        //public String Clnt_Con_List_TechSuppPhone { get; set; }
        //public String Clnt_Con_List_Reg_CoordinatorName { get; set; }
        //public String Clnt_Con_List_Reg_CoordinatorEmail { get; set; }
        //public String Clnt_Con_List_Reg_CoordinatorPhone { get; set; }
        //public Boolean? Clnt_Con_List_IsActive { get; set; }
        //public Boolean? Clnt_Con_List_IsDelete { get; set; }



    }
    public class CompaniesMultiAddress
    {
        public string Address { get; set; }
        public string City { get; set; }
        public int State { get; set; }
        public int Zip { get; set; }
    }


    public class ClientCompanyMaster
    {
        public String Client_pkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
    public class ClientMasterdrop
    {
        public Int64 Client_pkeyID { get; set; }
        public String Client_Company_Name { get; set; }
    }

    public class ClientContactListDTO
    {
        public Int64 Clnt_Con_List_pkeyID { get; set; }
        public Int64 Clnt_Con_List_ClientComID { get; set; }
        public String Clnt_Con_List_Name { get; set; }
        public String Clnt_Con_List_Email { get; set; }
        public String Clnt_Con_List_Phone { get; set; }
        public String Clnt_Con_List_TypeName { get; set; }
        public String Clnt_Con_List_CreatedBy { get; set; }
        public String Clnt_Con_List_ModifiedBy { get; set; }
        public Boolean? Clnt_Con_List_IsActive { get; set; }
        public Boolean? Clnt_Con_List_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }

    }
    public class ClientContact
    {
        public String Clnt_Con_List_pkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
    public class ClientCompanyDTO
    {
        public Int64 Client_pkeyID { get; set; }
        public Int64? Client_StateId { get; set; }
        public String Client_Company_Name { get; set; }
        public String Client_Billing_Address { get; set; }
        public String Client_City { get; set; }
        public String IPL_StateName { get; set; }
        public Int64? Client_ZipCode { get; set; }
        public String Client_ContactName { get; set; }

        public String Client_Website_Link { get; set; }
        public Boolean? Client_IsActive { get; set; }
        public String WhereClause { get; set; }

        public Boolean? Client_IsDelete { get; set; }
        public Int64? UserId { get; set; }
        public int Type { get; set; }
        public String FilterData { get; set; }
        public String Client_CreatedBy { get; set; }
        public String Client_ModifiedBy { get; set; }
        public Int64? MenuId { get; set; }
        public SearchMasterDTO SearchMaster { get; set; }

        public string Client_ContactEmail { get; set; }
        public string Client_ContactPhone { get; set; }
        public string ViewUrl { get; set; }
        public Boolean? Client_IsDeleteAllow { get; set; }



    }

    public class ClientCompanyDTOForDropDowm
    {
        public Int64 Client_pkeyID { get; set; }
        public String Client_Company_Name { get; set; }
    }
    public class ClientSettingDTO
    {
        public Int64? Client_pkeyID { get; set; }
        public Int64 Client_Photo_Resize_height { get; set; }
        public Int64 Client_Photo_Resize_width { get; set; }
        public DateOverlapingEnum Client_DateTimeOverlay { get; set; }
    }
    public enum DateOverlapingEnum
    {
        DoNotPrint = 1,
        PrintDatetime = 2,
        PrintOnlyDate = 3,
    }
}