﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Forms_WO_Filters_CustomerDTO
    {
        public long WOF_Customer_pkeyId { get; set; }
        //public long WOF_CustomerId { get; set; }
        public long Cust_Num_pkeyId { get; set; }
        public long WOF_FilterId { get; set; }
        public long WOF_FormId { get; set; }
        public bool WOF_Customer_IsActive { get; set; }
        public bool WOF_Customer_IsDelete { get; set; }
        public long UserId { get; set; }
        public DateTime WOF_Customer_CreatedOn { get; set; }        
        public DateTime WOF_Customer_ModifiedOn { get; set; }     
        public DateTime WOF_Customer_DeletedOn { get; set; }
    }
}