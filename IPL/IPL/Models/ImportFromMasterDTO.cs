﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class ImportFromMasterDTO
    {
        public Int64 Import_Form_PkeyId { get; set; }
        public String Import_Form_Name { get; set; }
        public Boolean? Import_Form_IsActive { get; set; }
        public Boolean? Import_Form_IsDelete { get; set; }
        public String Import_Form_URL_Name { get; set; }
        public Boolean? Import_Form_PCRImportForm { get; set; }
        public Boolean? Import_Form_IsFixed { get; set; }
        public Boolean? IsImportFrom { get; set; }
        public String Import_BucketName { get; set; }
        public String Import_BucketFolderName { get; set; }
        public String Import_DestBucketFolderName { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }

        public string WhereClause { get; set; }
        public int PageNumber { get; set; }
        public int Rowcount { get; set; }
        public string FilterData { get; set; }
    }

    public class ImportFromMaster_Filter
    {
        public string Import_Form_Name { get; set; }
        public string Import_BucketName { get; set; }
    }
}