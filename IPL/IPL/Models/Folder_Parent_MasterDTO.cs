﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Folder_Parent_MasterDTO
    {
        public Int64 Fold_Pkey_Id { get; set; }
        public String Fold_Name { get; set; }
        public String Fold_Desc { get; set; }
        public Int64? Fold_Parent_Id { get; set; }
        public List<AutoAssinArray> AutoAssinArray { get; set; }
        public List<PermisionArray> PermisionArray { get; set; }
        public Boolean? Fold_IsActive { get; set; }
        public Boolean? Fold_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
        public String strAutoAssinArray { get; set; }
        public String strPermisionArray { get; set; }


        
        public Int64? Client_ID { get; set; }

    }
    public class FolderDataDTO
    {
        public String Fold_Pkey_Id { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
    public class AutoAssinArray
    {
        public List<TaskSettState> Task_sett_State { get; set; }
        public List<TaskSettCountry> Task_sett_Country { get; set; }
        public string Task_sett_Zip { get; set; }
        public List<TaskSettCustomer> Task_sett_Customer { get; set; }
     
        public List<TaskSettCompany> Task_sett_Company { get; set; }
        public List<TaskSettLone> Task_sett_Lone { get; set; }
  
        public List<TaskWorkTypeGroup> Task_Work_TypeGroup { get; set; }

        public List<WTTaskWorkType> WTTaskWorkType { get; set; }
        public bool? Task_sett_IsActive { get; set; }
        public bool? Task_sett_IsDelete { get; set; }
        public Int64 Fold_Auto_Assine_PkeyId { get; set; }
        public Int64 Instruction_Json_PkeyId { get; set; }
    }

    public class PermisionArray
    {
        public int Group_DR_PkeyID { get; set; }
        public string Grp_Name { get; set; }
        public Boolean? checkitem { get; set; }
        public Int64 Grp_pkeyID { get; set; }
    }


    public class Folder_Get_Parent_MasterDTO
    {
        public Int64 Fold_Pkey_Id { get; set; }
        public String Fold_Name { get; set; }
        public String Fold_Desc { get; set; }
        public Int64? Fold_Parent_Id { get; set; } 
        public Boolean? Fold_IsActive { get; set; }
        public Boolean? Fold_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
        public String strAutoAssinArray { get; set; }
        public String strPermisionArray { get; set; }
        public Int64 Client_ID { get; set; }
        public int Level { get; set; }
        public string Fold_CreatedBy { get; set; }
        public string Fold_ModifiedBy { get; set; }
        public string File_Data { get; set; }
        public List<Folder_Get_Parent_MasterDTO> lstFolder_Get_Parent_MasterDTO { get; set; }
        public List<Folder_File_MasterDTO> lstFolder_File_MasterDTO { get; set; }

    }


    public class Folder_Get_Parent_MasterDTO_Mobile
    {
        public Int64 Fold_Pkey_Id { get; set; }
        public String Fold_Name { get; set; }
        public String Fold_Desc { get; set; }
        public Int64? Fold_Parent_Id { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
        public int Level { get; set; }
        public string File_Data { get; set; }
        public List<Folder_Get_Parent_MasterDTO_Mobile> lstFolder_Get_Parent_MasterDTO { get; set; }
   

    }

}