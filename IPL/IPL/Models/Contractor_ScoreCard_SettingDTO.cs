﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Contractor_ScoreCard_SettingDTO
    {
        public Int64 Con_score_setting_pkeyId { get; set; }
        public int? Con_score_setting_picturequality { get; set; }
        public int? Con_score_setting_follow_inst { get; set; }
        public int? Con_score_setting_workquality { get; set; }
        public int? Con_score_setting_duadate { get; set; }
        public int? Con_score_setting_estdate { get; set; }
        public int? Con_score_setting_total { get; set; }
        public int? Con_score_setting_Info_needed { get; set; }
        public int? Con_score_setting_retern_property { get; set; }
        public int? Con_score_setting_escalated_penalty { get; set; }
        public int? Con_score_setting_number_day { get; set; }
        public Boolean? Con_score_setting_IsActive { get; set; }
        public Boolean? Con_score_setting_IsDelete { get; set; }
        public Int64? Con_score_setting_CompanyId { get; set; }
        public Int64? Con_score_setting_UserId { get; set; }
        public String Con_score_setting_CreatedBy { get; set; }
        public String Con_score_setting_ModifiedBy { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }

    }
    public class Contractor_ScoreCard
    {
        public String Con_score_setting_pkeyId { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
    public class ContractorAccountPayDTO
    {
        public Int64 Con_Account_Pay_PkeyID { get; set; }
        public int? Inv_Payout_Criteria { get; set; }
        public int? Payout_Frequency { get; set; }
        public DateTime? Next_Payout_End_Date { get; set; }
        public int? Sent_Contractor_Pay_Report { get; set; }
        public Int64? CompanyID { get; set; }
        public Boolean? IsActive { get; set; }
        public Boolean? IsDelete { get; set; }
        public String CreatetedBy { get; set; }
        public String ModifiedBy { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
    }
    public class FinalScrorCardDTO
    {
        public Int64 Wo_Con_FScore_PkeyID { get; set; }
        public String Wo_Con_FScore_Month { get; set; }
        public String Wo_Con_FScore_Year { get; set; }
        public String Cont_Name { get; set; }
        public String WhereClause { get; set; }
        public String FilterData { get; set; }
        public Int64? Wo_Con_FScore_ConID { get; set; }
        public Decimal? Wo_Con_FScore_Value { get; set; }
        public Boolean? Wo_Con_FScore_IsActive { get; set; }
        public Boolean? Wo_Con_FScore_IsDelete { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
        public Decimal? Wo_Con_Score_DueDate { get; set; }
        public Decimal? Wo_Con_Score_EstDate { get; set; }
        public Decimal? Wo_Con_Score_Follow_Inst { get; set; }
        public Decimal? Wo_Con_Score_Picturequality { get; set; }
        public Decimal? Wo_Con_Score_Work_Quality { get; set; }
        public Decimal? Wo_Con_Score_Info_Needed { get; set; }
        public int? Wo_Con_FScore_Wo_Count { get; set; }
        public Decimal? Wo_Con_FScore_Retern_Property { get; set; }
    }
    public class ChildScorecardDTO
    {
        public Int64 Wo_Con_Score_FScorePkeyId { get; set; }
        public String workOrderNumber { get; set; }
        public String IPLNO { get; set; }
        public String Cont_Name { get; set; }   
        public int? Wo_Con_Score_Contractor_Score { get; set; }   
        public int? Wo_Con_Score_Picturequality { get; set; }   
        public int? Wo_Con_Score_Follow_Inst { get; set; }   
        public int? Wo_Con_Score_Work_Quality { get; set; }   
        public int? Wo_Con_Score_DueDate { get; set; }   
        public int? Wo_Con_Score_EstDate { get; set; }   
        public int? Wo_Con_Score_Info_Needed { get; set; }   
        public int? Wo_Con_Score_Retern_Property { get; set; }   
        public DateTime? Wo_Con_Score_Contractor_Date { get; set; }   
        public String WT_WorkType { get; set; }
        public int Type { get; set; }
        public Int64? Wo_Con_Score_Wo_Id { get; set; }
        public Int64 UserID { get; set; }
    }

}