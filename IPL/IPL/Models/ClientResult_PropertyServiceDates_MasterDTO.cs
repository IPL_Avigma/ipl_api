﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class ClientResult_PropertyServiceDates_MasterDTO
    {
        public Int64 CRPSD_PkeyID { get; set; }
        public Int64? CRPSD_WO_ID { get; set; }
        public Boolean? CRPSD_IsActive { get; set; }
        public Boolean? CRPSD_IsDelete { get; set; }
        public DateTime? CRPSD_BoardingDate { get; set; }
        public DateTime? CRPSD_LastInspectedDate { get; set; }
        public DateTime? CRPSD_InspectionCycle { get; set; }
        public DateTime? CRPSD_LastInteriorCleanDate { get; set; }
        public DateTime? CRPSD_InitialInspectionComplete { get; set; }
        public DateTime? CRPSD_CleanOutComplete { get; set; } 
        public Int64 UserID { get; set; }
        public int Type { get; set; }
    }
    public class ClientResult_PropertyServiceDates_Master
    {
        public String CRPSD_PkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}