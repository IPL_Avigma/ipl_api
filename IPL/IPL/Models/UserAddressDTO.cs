﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPLApp.Models
{
    public class UserAddressDTO
    {
        public Int64 IPL_PkeyID { get; set; }
        public String IPL_Primary_radius { get; set; }
        public Int64? IPL_Primary_Zip_Code { get; set; }
        public Int64? IPL_Primary_Countries { get; set; }
        public String IPL_Primary_Latitude { get; set; }
        public String IPL_Primary_Longitude { get; set; }
        public String IPL_Secondary_radius { get; set; }
        public Int64? IPL_Secondary_Zip_Codes { get; set; }
        public Int64? IPL_Secondary_Countries { get; set; }
        public String IPL_Secondary_Latitude { get; set; }
        public String IPL_Secondary_Longitude { get; set; }
        public Int64? IPL_UserID { get; set; }
        public String IPL_Address { get; set; }
        public String IPL_City { get; set; }
        public String IPL_CreatedBy { get; set; }
        public String IPL_ModifiedBy { get; set; }
        public Int64? IPL_State { get; set; }
        public Boolean? IPL_IsActive { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
        public int IPL_Address_Val { get; set; }
        public Int64? IPL_County { get; set; }
        public List<StrAddressArrayDto> StrAddressArray { get; set; }

    }
    public class UserAddress
    {
        public String IPL_PkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
    public class StrAddressArrayDto
    {
        public Int64 IPL_PkeyID { get; set; }
        public String IPL_Primary_radius { get; set; }
        public Int64? IPL_Primary_Zip_Code { get; set; }
        public Int64? IPL_Primary_Countries { get; set; }
        public String IPL_Primary_Latitude { get; set; }
        public String IPL_Primary_Longitude { get; set; }
        public String IPL_Secondary_radius { get; set; }
        public Int64? IPL_Secondary_Zip_Codes { get; set; }
        public Int64? IPL_Secondary_Countries { get; set; }
        public String IPL_Secondary_Latitude { get; set; }
        public String IPL_Secondary_Longitude { get; set; }
        public Int64? IPL_UserID { get; set; }
        public String IPL_Address { get; set; }
        public String IPL_City { get; set; }
        public string IPL_State { get; set; }
        public Boolean? IPL_IsActive { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
        public int IPL_Address_Val { get; set; }
        public String IPL_County { get; set; }
        public Int64? IPL_StateID { get; set; }
        public Int64? ID { get; set; }
    }
    public class contractorMapDRD
    {
        public Int64 IPL_StateID { get; set; }
        public String IPL_StateName { get; set; }
        public Int64 CompanyId { get; set; }
        public Int64 UserId { get; set; }
        public int Type { get; set; }
    }
    public class ContractorCouny
    {
        public Int64 ID { get; set; }
        public String COUNTY { get; set; }
    }


}