﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class NewWorkOrderSettingDTO
    {
        public Int64 WO_Sett_pkeyID { get; set; }
        public Int64? WO_Sett_CompanyID { get; set; }
        public Boolean? WO_Sett_Allow_Dup_Num { get; set; }
        public Boolean? WO_Sett_Auto_Inc_GoBack { get; set; }
        public Boolean? WO_Sett_Auto_Inc_NeedInfo { get; set; }
        public Boolean? WO_Sett_Auto_Inc_Dup { get; set; }
        public Boolean? WO_Sett_Auto_Inc_Recurring { get; set; }
        public Boolean? WO_Sett_Auto_Assign { get; set; }
        public Boolean? WO_Sett_Detect_Pricing { get; set; }
        public Boolean? WO_Sett_Remove_Doller { get; set; }
        public Boolean? WO_Sett_IsDelete { get; set; }
        public Boolean? WO_Sett_IsActive { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
        public Int64? WO_Sett_UserId { get; set; }
        public String Wo_Sett_Comapny_SAlert { get; set; }
        public String Wo_Sett_Custom_Titlebar { get; set; }
        public Int64? Wo_Sett_Default_Time { get; set; }
        public String WO_Sett_CreatedBy { get; set; }
        public String WO_Sett_ModifiedBy { get; set; }
    }
    public class NewWorkOrderSetting
    {
        public String WO_Sett_pkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}