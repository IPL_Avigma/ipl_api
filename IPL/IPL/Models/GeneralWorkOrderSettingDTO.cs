﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class GeneralWorkOrderSettingDTO
    {
        public Int64 GW_Sett_pkeyID { get; set; }
        public Int64? GW_Sett_CompanyID { get; set; }
        public Boolean? GW_Sett_Field_Complete { get; set; }
        public Boolean? GW_Sett_Allow_Contractor { get; set; }
        public Boolean? GW_Sett_Assigned_Unread { get; set; }
        public Boolean? GW_Sett_Allow_Estimated { get; set; }
        public Boolean? GW_Sett_Require_Estimated { get; set; }
        public Boolean? GW_Sett_Sent_Ass_Cooradinator { get; set; }
        public Boolean? GW_Sett_Sent_Ass_Processor { get; set; }
        public Boolean? GW_Sett_Sent_Email_Multiple { get; set; }
        public Boolean? GW_Sett_StaffName { get; set; }
        public Boolean? GW_Sett_IsDelete { get; set; }
        public Boolean? GW_Sett_IsActive { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
        public Int64? GW_Sett_UserID { get; set; }
    }
    public class GeneralWorkOrderSetting
    {
        public String GW_Sett_pkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}