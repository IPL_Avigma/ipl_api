﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class WorkOrder_ServiceLink_Item_History_MasterDTO
    {
        public Int64? WSERVICEINS_HS_PkeyId { get; set; }
        public String WSERVICEINS_HS_Ins_Name { get; set; }
        public String WSERVICEINS_HS_Ins_Details { get; set; }
        public Int64? WSERVICEINS_HS_Qty { get; set; }
        public Decimal? WSERVICEINS_HS_Price { get; set; }
        public Decimal? WSERVICEINS_HS_Total { get; set; }
        public Boolean? WSERVICEINS_HS_IsProcessed { get; set; }
        public Int64? WSERVICEINS_HS_FkeyID { get; set; }
        public Boolean? WSERVICEINS_HS_IsActive { get; set; }
        public Boolean? WSERVICEINS_HS_IsDelete { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
    }

    public class WorkOrder_ServiceLink_Item_History_Master
    {
        public String WSERVICEINS_HS_PkeyId { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}