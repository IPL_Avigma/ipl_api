﻿using IPLApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;

namespace IPL.Models
{
    public class ReportMasterDTO
    {
        public Int64 workOrder_ID { get; set; }
        public String IPLNO { get; set; }
        public String workOrderNumber { get; set; }
        public Int64? Contractor { get; set; }
        public String address1 { get; set; }
        public String city { get; set; }
        public String state { get; set; }
        public Int64? zip { get; set; }
        public Int64? WorkType { get; set; }
        public DateTime? dueDate { get; set; }
        public DateTime? Field_complete_date { get; set; }
        public String Client_Invoice_Number { get; set; }
        public DateTime? Client_InvoiceDate { get; set; }
        public Decimal? Client_InvoiceTotal { get; set; }
        public String ContractorName { get; set; }
        public String Client_Company_Name { get; set; }
        public String whereClause { get; set; }
        public Boolean? IsActive { get; set; }
        public Boolean? IsDelete { get; set; }
        public int Type { get; set; }
        public Int64? Processor { get; set; }
        public Int64? Cordinator { get; set; }
        public String CordinatorName { get; set; }
        public String ProcessorName { get; set; }
        public String IPL_StateName { get; set; }
        public String WT_WorkType { get; set; }
        public int Valtype { get; set; }
        public DateTime? InvoiceDateFrom { get; set; }
        public DateTime? InvoiceDateTo { get; set; }
        public DateTime? ReadyOfficeDateFrom { get; set; }
        public DateTime? ReadyOfficeDateTo { get; set; }
        public DateTime? SentToClientDateFrom { get; set; }
        public DateTime? SentToClientDateTo { get; set; }
        //Added by Dipali
        public List<ReportAutoAssinArray> ReportAutoAssinArray { get; set; }
        public String InvoiceRangeStart { get; set; }
        public String InvoiceRangeEnd { get; set; }
        public String ClientInvoiceRangeStart { get; set; }
        public String ClientInvoiceRangeEnd { get; set; }
        public Boolean IsClientCheck { get; set; }
        public Boolean IsContractorCheck { get; set; }
        public DateTime? CompletedDateFrom { get; set; }
        public DateTime? CompletedDateTo { get; set; }
        public DateTime? CreatedDateFrom { get; set; }
        public DateTime? CreatedDateTo { get; set; }
        public DateTime? OfficeApproveDateFrom { get; set; }
        public DateTime? OfficeApproveDateTo { get; set; }
        public DateTime? ClientCheckDateFrom { get; set; }
        public DateTime? ClientCheckDateTo { get; set; }
        public DateTime? SentToClient_date { get; set; }
        public DateTime? OfficeApproved_date { get; set; }
        public String CategoryName { get; set; }
        public Decimal? Client_InvoicePaid { get; set; }
        public String Con_Invoice_Number { get; set; }
        public DateTime? Con_InvoiceDate { get; set; }
        public Decimal? Con_InvoiceTotal { get; set; }
        public Decimal? Con_InvoicePaid { get; set; }
        public Boolean RowCheckBox { get; set; }
        public Decimal? RowAmount { get; set; }
        public String RowComment { get; set; }
        public Int64? Con_Pay_Invoice_Id { get; set; }
        public Int64? Client_Pay_Invoice_Id { get; set; }
        public DateTime? Con_InvoicePaid_Date { get; set; }
        public DateTime? Client_InvoicePaid_Date { get; set; }
        public Int64? UserId { get; set; }

        public bool? Inv_Con_Inv_Followup { get; set; } // For Hold
        public Boolean Inv_Con_Inv_Approve { get; set; } // For Approve
        public Boolean Inv_Client_IsNoCharge { get; set; } // For No Charge

        public string ContractorInvoiceRangeStart { get; set; }
        public string ContractorInvoiceRangeEnd { get; set; }

        public bool? Inv_Client_Paid { get; set; }
        public DateTime? Inv_Client_Paid_Date { get; set; }
        public DateTime? Inv_Client_Paid_Date_Through { get; set; }
        public string Inv_Client_Paid_Check { get; set; }

        public bool? Inv_Contractor_Paid { get; set; }
       public DateTime? Inv_Contractor_Paid_Date { get; set; }
       public DateTime? Inv_Contractor_Paid_Date_Through { get; set; }
       public string Inv_Contractor_Paid_Check { get; set; }
      

    }

    public class ReportAutoAssinArray
    {
        public List<TaskSettState> Task_sett_State { get; set; }
        public List<TaskSettCustomer> Task_sett_Customer { get; set; }
        public List<TaskSettCompany> Task_sett_Company { get; set; }
        public List<TaskSettContractor> Task_sett_Contractor { get; set; }
        public List<TaskSettContractor> Task_sett_Admin { get; set; }
        public List<CategoryDTO> Task_sett_Category { get; set; }
    }

    public class CustomReportPDF
    {
        public dynamic Data { get; set; }
        public string Message { get; set; }
        public HttpStatusCode HttpStatusCode { get; set; }
    }

}