﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Filter_Admin_Task_MasterDTO
    {
        public Int64 Task_Filter_PkeyID { get; set; }
        public String Task_Filter_TaskName { get; set; }
        public Int32 Task_Filter_TaskType { get; set; }
        public String Task_Filter_TaskPhName { get; set; }
        public String Task_Filter_CreatedBy { get; set; }
        public String Task_Filter_ModifiedBy { get; set; }
        public Boolean? Task_Filter_TaskIsActive { get; set; }
        public Boolean Task_Filter_IsActive { get; set; }
        public Boolean? Task_Filter_IsDelete { get; set; }
        public int? Type { get; set; }
        public Int64? UserId { get; set; }
    }
}