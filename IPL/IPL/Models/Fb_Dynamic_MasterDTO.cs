﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Fb_Dynamic_MasterDTO
    {
        public Int64 Fb_Dynamic_pkeyID { get; set; }
        public Int64? Fb_Dynamic_ImportFrom_Id { get; set; }
        public Int64? Fb_Dynamic_Company_Id { get; set; }
        public String Fb_Dynamic_Tab_Name { get; set; }
        public Boolean Fb_Dynamic_Office_Results { get; set; }
        public Boolean Fb_Dynamic_FieldResults { get; set; }
        public Boolean? Fb_Dynamic_IsDelete { get; set; }
        public Boolean? Fb_Dynamic_IsActive { get; set; }
        public int? Type { get; set; }
        public Int64? UserId { get; set; }
        public Int64? Fb_Dynamic_WorkTypeId { get; set; }
        public Int64 workOrder_ID { get; set; }

    }

    public class Fb_Dynamic_Master
    {
        public String Fb_Dynamic_pkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
    public class Fb_Dynamic_MasterListDTO
    {
        public List<Fb_Dynamic_MasterDTO> Fb_Dynamic_List { get; set; }
    }
}