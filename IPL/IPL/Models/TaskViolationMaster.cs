﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class TaskViolationMaster
    {
        public Int64 Task_Violation_pkeyID { get; set; }
        public Int64? Task_Violation_WO_ID { get; set; }
        public String Task_Violation_Name { get; set; }
        public DateTime? Task_Violation_Date { get; set; }
        public DateTime? Task_Violation_Deadline { get; set; }
        public String Task_Violation_Id { get; set; }
        public DateTime? Task_Violation_Date_Discovered { get; set; }
        public Decimal? Task_Violation_Fine_Amount { get; set; }
        public String Task_Violation_Contact { get; set; }
        public String Task_Violation_Comment { get; set; }
        public Boolean? Task_Violation_IsActive { get; set; }
        public Boolean? Task_Violation_IsDelete { get; set; }
        public int Task_Violation_Status { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
    }
    public class TaskViolation
    {
        public String Task_Violation_pkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }



    public class TaskViolationMasterRootObject
    {
        public List<TaskViolationMaster> TaskViolationMaster { get; set; }
        public Int64 Task_Inv_WO_ID { get; set; }
        public int UserID { get; set; }
        public int Type { get; set; }
        public string str_Task_Bid_WO_ID { get; set; }
    }
}