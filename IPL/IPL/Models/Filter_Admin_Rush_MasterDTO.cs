﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Filter_Admin_Rush_MasterDTO
    {
        public Int64 Rush_Filter_PkeyID { get; set; }
        public String Rush_Filter_RushName { get; set; }
        public Boolean? Rush_Filter_RushIsActive { get; set; }
        public Boolean Rush_Filter_IsActive { get; set; }
        public Boolean? Rush_Filter_IsDelete { get; set; }
        public String Rush_Filter_CreatedBy { get; set; }
        public String Rush_Filter_ModifiedBy { get; set; }
        public int? Type { get; set; }
        public Int64? UserId { get; set; }
    }
}