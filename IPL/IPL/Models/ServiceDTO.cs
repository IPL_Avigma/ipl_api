﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPLApp.Models
{
    public class ServiceDTO
    {
        public Int64 serviceID { get; set; }
        public String serviceName { get; set; }
        public String survey { get; set; }
        public String instructions { get; set; }
        public String attribute1 { get; set; }
        public String attribute2 { get; set; }
        public String attribute3 { get; set; }
        public String attribute4 { get; set; }
        public String attribute5 { get; set; }
        public String attribute6 { get; set; }
        public String attribute7 { get; set; }
        public String attribute8 { get; set; }
        public String attribute9 { get; set; }
        public String attribute10 { get; set; }
        public String attribute11 { get; set; }
        public String attribute12 { get; set; }
        public String attribute13 { get; set; }
        public String attribute14 { get; set; }
        public String attribute15 { get; set; }
        public String source_service { get; set; }
        public Int64? source_service_id { get; set; }
        public Int64? surveyDynamic { get; set; }
        public Int64? workOrder_ID { get; set; }
        public Boolean? IsActive { get; set; }
        public int currUserId { get; set; }
        public int Type { get; set; }
        public String ErrorCode { get; set; }
        public Boolean? IsDelete { get; set; }
        public Boolean? IsProcess { get; set; }

    }




    public class RootService
    {
        public Int64 serviceID { get; set; }
        public String serviceName { get; set; }
        public String survey { get; set; }
        public String instructions { get; set; }
        public Int64? attribute1 { get; set; }
        public Int64? attribute2 { get; set; }
        public Int64? attribute3 { get; set; }
        public Int64? attribute4 { get; set; }
        public Int64? attribute5 { get; set; }
        public Int64? attribute6 { get; set; }
        public Int64? attribute7 { get; set; }
        public Int64? attribute8 { get; set; }
        public Int64? attribute9 { get; set; }
        public Int64? attribute10 { get; set; }
        public Int64? attribute11 { get; set; }
        public Int64? attribute12 { get; set; }
        public Int64? attribute13 { get; set; }
        public Int64? attribute14 { get; set; }
        public Int64? attribute15 { get; set; }
        public String source_service { get; set; }
        public Int64? source_service_id { get; set; }
        public Int64? surveyDynamic { get; set; }
        public Int64? workOrder_ID { get; set; }
        public Boolean? IsActive { get; set; }
        public int currUserId { get; set; }
        public int Type { get; set; }
        
    }


    public class ViewService
    {
        public Int64 serviceID { get; set; }
        public String serviceName { get; set; }
        public String survey { get; set; }
        public String instructions { get; set; }
        public Int64? attribute1 { get; set; }
        public Int64? attribute2 { get; set; }
        public Int64? attribute3 { get; set; }
        public Int64? attribute4 { get; set; }
        public Int64? attribute5 { get; set; }
        public Int64? attribute6 { get; set; }
        public Int64? attribute7 { get; set; }
        public Int64? attribute8 { get; set; }
        public Int64? attribute9 { get; set; }
        public Int64? attribute10 { get; set; }
        public Int64? attribute11 { get; set; }
        public Int64? attribute12 { get; set; }
        public Int64? attribute13 { get; set; }
        public Int64? attribute14 { get; set; }
        public Int64? attribute15 { get; set; }
        public String source_service { get; set; }
        public Int64? source_service_id { get; set; }
        public Int64? surveyDynamic { get; set; }
        public Int64? workOrder_ID { get; set; }

    }


    public class ServiceReturn
    {
        public String serviceID { get; set; }
        public String Status { get; set; }
    }
}