﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Folder_GroupRole_MasterDTO
    {
        public Int64 Fold_Role_Pkey_Id { get; set; }
        public Int64? Fold_Role_Parent_Id { get; set; }
        public Int64? Fold_Role_GroupRole_Id { get; set; }
        public Boolean? Fold_Role_IsActive { get; set; }
        public Boolean? Fold_Role_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int? Type { get; set; }
    }
}