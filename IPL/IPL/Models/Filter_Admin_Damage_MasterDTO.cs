﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Filter_Admin_Damage_MasterDTO
    {
        public Int64 Damage_Filter_PkeyID { get; set; }
        public String Damage_Filter_DamageName { get; set; }
        public String Damage_Filter_DamageIntExt { get; set; }
        public String Damage_Filter_DamageLocation { get; set; }
        public String Damage_Filter_DamageDesc { get; set; }
        public String Damage_Filter_CreatedBy { get; set; }
        public String Damage_Filter_ModifiedBy { get; set; }
        public Boolean? Damage_Filter_DamageIsActive { get; set; }
        public Boolean Damage_Filter_IsActive { get; set; }
        public Boolean? Damage_Filter_IsDelete { get; set; }
        public int? Type { get; set; }
        public Int64? UserId { get; set; }
    }
}