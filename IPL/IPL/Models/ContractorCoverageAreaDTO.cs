﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class ContractorCoverageAreaDTO
    {
        public Int64 Cont_Coverage_Area_PkeyId { get; set; }
        public Int64? Cont_Coverage_Area_UserID { get; set; }
        public String whereClause { get; set; }
        public String  COUNTY { get; set; }
        public String STATE_CODE { get; set; }
        public String Cont_Coverage_Area_CreatedBy { get; set; }
        public String Cont_Coverage_Area_ModifiedBy { get; set; }
        public Int64? Cont_Coverage_Area_State_Id { get; set; }
        public Int64? Cont_Coverage_Area_County_Id { get; set; }
        public Int64? Cont_Coverage_Area_Zip_Code { get; set; }
        public Boolean? Cont_Coverage_Area_IsActive { get; set; }
        public Boolean? Cont_Coverage_Area_IsDelete { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
        public List<StrAddressArray> StrAddressArray { get; set; }
        public Int64? Cont_Coverage_Area_UserAddressID { get; set; }
    }
    public class ContractorCoverageArea
    {
        public String Cont_Coverage_Area_PkeyId { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
    public class StrAddressArray
    {
        public int Zip_ID { get; set; }
        public int Zip_zip { get; set; }
        public string Zip_state_id { get; set; }
        public string Zip_county_name { get; set; }
        public int UserID { get; set; }
        public int Type { get; set; }
        public bool chkdata { get; set; }
    }
    public class ContCoverageAreaStateId
    {
        public int IPL_StateID { get; set; }
        public string IPL_StateName { get; set; }
        public int UserId { get; set; }
        public int Type { get; set; }
    }

    public class ContCoverageAreaCountyId
    {
        public Int64 ID { get; set; }
        public String COUNTY { get; set; }
    }
}