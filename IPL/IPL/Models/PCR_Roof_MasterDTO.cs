﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class PCR_Roof_MasterDTO
    {
        public Int64 PCR_Roof_pkeyId { get; set; }
        public Int64 PCR_Roof_MasterId { get; set; }
        public Int64 PCR_Roof_WO_Id { get; set; }
        public int? PCR_Roof_ValType { get; set; }
        public String PCR_Roof_Roof_Shape_Pitched_Roof { get; set; }
        public String PCR_Roof_Leak { get; set; }
        public String PCR_Roof_Leak_Case { get; set; }
        public String PCR_Roof_Leak_Other { get; set; }
        public String PCR_Roof_Location_Dimensions { get; set; }
        public String PCR_Roof_Roof_Damage { get; set; } //new
        public String PCR_Roof_Water_Strains { get; set; }
        public String PCR_Roof_Water_Strains_Case { get; set; }
        public String PCR_Roof_Water_Strains_Dimension { get; set; }
        public String PCR_Roof_Did_You_Perform_Roof_Repair { get; set; }
        public String PCR_Roof_Bid_To_Repair { get; set; }
        public String PCR_Roof_Did_You_Perform_Emergency_Traping { get; set; }
        public String PCR_Roof_Explain_Bid_Trap { get; set; }
        public int? PCR_Roof_Bid_To_Trap_Dimension_size_x { get; set; }
        public int? PCR_Roof_Bid_To_Trap_Dimension_size_y { get; set; }
        public int? PCR_Roof_Bid_To_Trap_Location { get; set; }
        public String PCR_Roof_Bid_To_Trap_Description { get; set; }
        public Decimal? PCR_Roof_Bid_To_Trap_Bid_Amount { get; set; }

        public int? PCR_Roof_Bid_To_Tar_Patch_Dimension_size_x { get; set; }
        public int? PCR_Roof_Bid_To_Tar_Patch_Dimension_size_y { get; set; }
        public int? PCR_Roof_Bid_To_Tar_Patch_Location { get; set; }
        public String PCR_Roof_Bid_To_Tar_Patch_dias { get; set; }
        public String PCR_Roof_Leak_Location_Dimension { get; set; }
        public Decimal? PCR_Roof_Bid_To_Tar_Patch_Bid_Amount { get; set; }

        public String PCR_Roof_Bid_To_Replace { get; set; }
        public String PCR_Roof_Reason_Cant_Repair_Due_To { get; set; }
        public String PCR_Roof_Reason_Cant_Repair_Due_To_TEXT { get; set; } //new
        public String PCR_Roof_Reason_Preventive_Due_To { get; set; }

        public String PCR_Roof_Reason_Leaking { get; set; }
        public String PCR_Roof_Reason_Other { get; set; }
        public String PCR_Roof_Bid_To_Description { get; set; }

        public Boolean? PCR_Roof_Bid_To_Location_Entire_Roof { get; set; }
        public Boolean? PCR_Roof_Bid_To_Location_Front { get; set; }
        public Boolean? PCR_Roof_Bid_To_Location_Back { get; set; }
        public Boolean? PCR_Roof_Bid_To_Location_Left_Side { get; set; } 
        public Boolean? PCR_Roof_Bid_To_Location_Right_Side { get; set; } 
        public Boolean? PCR_Roof_Building_House { get; set; } 
        public Boolean? PCR_Roof_Building_Garage { get; set; } 
        public Boolean? PCR_Roof_Building_Out_Building { get; set; } 
        public Boolean? PCR_Roof_Building_Pool_House { get; set; } 
        public Boolean? PCR_Roof_Building_Shed { get; set; } 
        public Boolean? PCR_Roof_Building_Bam { get; set; } 


        public String PCR_Roof_Item_Used_Roof_Type { get; set; }
        public int? PCR_Roof_Item_Used_DRD { get; set; }
        public int? PCR_Roof_Item_Used_Size { get; set; }
        public Decimal? PCR_Roof_Item_Used_Amount { get; set; }


        public String PCR_Roof_Item_Used_Felt_Type { get; set; }
        public int? PCR_Roof_Item_Used_Felt_Type_DRD { get; set; }
        public int? PCR_Roof_Item_Used_Felt_Type_Size { get; set; }
        public Decimal? PCR_Roof_Item_Used_Felt_Type_Amount { get; set; }
        public int? PCR_Roof_Item_Used_Sheathing_DRD { get; set; }
        public int? PCR_Roof_Item_Used_Sheathing_Size { get; set; }
        public Decimal? PCR_Roof_Item_Used_Sheathing_Amount { get; set; }




        public String PCR_Roof_Item_Used_Deck_Thikness { get; set; }
        public int? PCR_Roof_Item_Used_Deck_Thikness_DRD { get; set; }

        public String PCR_Roof_Item_Used_Drip_Edge { get; set; }
        public int? PCR_Roof_Item_Used_Drip_Edge_Size { get; set; }
        public Decimal? PCR_Roof_Item_Used_Drip_Edge_Amount { get; set; }

        public String PCR_Roof_Item_Used_Ice_Water_Barrier { get; set; }
        public int? PCR_Roof_Item_Used_Ice_Water_Barrier_Size { get; set; }
        public Decimal? PCR_Roof_Item_Used_Ice_Water_Barrier_Amount { get; set; }



        public String PCR_Roof_Item_Used_No_Of_Vents { get; set; }
        public String PCR_Roof_Item_Used_No_Of_Vents_Text { get; set; }
        public Decimal? PCR_Roof_Item_Used_No_Of_Vents_Amount { get; set; }

        public String PCR_Roof_Item_Used_Roof_Debris { get; set; }
        public int? PCR_Roof_Item_Used_Roof_Debris_Size { get; set; }
        public Decimal? PCR_Roof_Item_Used_Roof_Debris_Amount { get; set; }

        public String PCR_Roof_Item_Used_Dempster_Rental { get; set; }
        public int? PCR_Roof_Item_Used_Dempster_Rental_Size { get; set; }
        public Decimal? PCR_Roof_Item_Used_Dempster_Rental_Amount { get; set; }
        public Decimal? PCR_Bid_Amount { get; set; }
        public Boolean? PCR_Roof_IsActive { get; set; }
        public Boolean? PCR_Roof_IsDelete { get; set; }

        public Int64 UserID { get; set; }
        public int Type { get; set; }
        public Int64 fwo_pkyeId { get; set; }


    }
    public class PCR_Roof_Master
    {
        public String PCR_Roof_pkeyId { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}