﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class PCR_CyprexxWinterizationPressure_DTO
    {
        public Int64 PCR_CW_PkeyID { get; set; }
        public Int64 PCR_CW_WO_ID { get; set; }
        public Int64 PCR_CW_CompanyID { get; set; }
        public String PCR_CW_Pressure_Test { get; set; }
        public String PCR_CW_Upload_photo { get; set; }
        public Boolean? PCR_CW_IsActive { get; set; }
        public Boolean? PCR_CW_IsDelete { get; set; }
        public Int64 UserID { get; set; }
        public Int64 fwo_pkyeId { get; set; }
        public int Type { get; set; }
    }
}