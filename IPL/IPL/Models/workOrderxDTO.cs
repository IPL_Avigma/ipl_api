﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IPL.Models;
namespace IPLApp.Models
{
    public class workOrderxDTO
    {
        public Int64 workOrder_ID { get; set; }
        public String workOrderNumber { get; set; }
        public String workOrderInfo { get; set; }
        public String address1 { get; set; }
        public String fulladdress { get; set; }
        public String Processor_Name { get; set; }
        public String address2 { get; set; }
        public String city { get; set; }
        public String state { get; set; }
        public Int64? zip { get; set; }
        public String country { get; set; }
        public String options { get; set; }
        public String reference { get; set; }
        public String description { get; set; }
        public String instructions { get; set; }
        public String Mortgagor { get; set; }
        public String status { get; set; }
        public DateTime? dueDate { get; set; }
        public DateTime? startDate { get; set; }
        public String clientInstructions { get; set; }
        public String clientStatus { get; set; }
        public DateTime? clientDueDate { get; set; }
        public String gpsLatitude { get; set; }
        public String Back_Chk_ProviderName { get; set; }
        public String gpsLongitude { get; set; }
       
        public Boolean? IsActive { get; set; }
        public Boolean? IsDelete { get; set; }
        public int currUserId { get; set; }
        public int Type { get; set; }
        public String ErrorCode { get; set; }
        public String workOrderNumber_Org { get; set; }
        public String Loan_Type { get; set; }
        public Int64? Background_Provider { get; set; }



        public Int64? WorkType { get; set; }
        public String Company { get; set; }
        public String Com_Name { get; set; }
        public String Com_Phone { get; set; }
        public String Com_Email { get; set; }
        public String Contractor { get; set; }
        public DateTime? Received_Date { get; set; }
        public DateTime? Complete_Date { get; set; }
        public DateTime? Cancel_Date { get; set; }

        public String IPLNO { get; set; }
        public Int64? Category { get; set; }
        public String Loan_Info { get; set; }

        public Int64? Customer_Number { get; set; }
        public Int64? Cordinator { get; set; }
        public Int64? Processor { get; set; }
        public bool? BATF { get; set; }
        public bool? ISInspection { get; set; }
        public String Lotsize { get; set; }
        public Int64? Rush { get; set; }

        public String Lock_Code { get; set; }
        public String Broker_Info { get; set; }
        public String Comments { get; set; }

        public String SM_Name { get; set; }
        public String WT_WorkType { get; set; }
        public String Client_Company_Name { get; set; }
        public String Cont_Name { get; set; }
        public String Work_Type_Name { get; set; }
        public String Cust_Num_Number { get; set; }
        public String rus_Name { get; set; }
        public String Lock_Location { get; set; }
        public String Key_Code { get; set; }
        public String Gate_Code { get; set; }
        public String Loan_Number { get; set; }
        public string Cordinator_Name { get; set; }
        public String Search_Address { get; set; }

        public string ClientMetaData { get; set; }
        public string Office_Approved { get; set; }
        public string Sent_to_Client { get; set; }

        public string Client_Result_Photo_FilePath { get; set; }
        public string Client_Result_Photo_FileName { get; set; }
        public Int64 UserID { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? EstimatedDate { get; set; }



        public String Field_complete { get; set; }
        public DateTime? OfficeApproved_date { get; set; }
        public DateTime? SentToClient_date { get; set; }
        public DateTime? Field_complete_date { get; set; }
        public String Status_Name { get; set; }
        public String whereClause { get; set; }
        public String CORNT_User_LoginName { get; set; }
        public String CORNT_User_FirstName { get; set; }

        public String state_name { get; set; }

        public String Inst_Ch_Comand_Mobile { get; set; }
        public DateTime? assigned_date { get; set; }

        public Boolean? IsProcess { get; set; }
        public Int64? import_from { get; set; }
        public Int64? Work_Type_Cat_pkeyID { get; set; }

        public Boolean? Recurring { get; set; }
        public Int32? Recurs_Day { get; set; }
        public Int64? Recurs_Period { get; set; }
        public Int64? Recurs_Limit { get; set; }
        public DateTime? Recurs_CutOffDate { get; set; }
        public List<DateTime?> Recurs_ReceivedDateArray { get; set; }
        public List<DateTime?> Recurs_DueDateArray { get; set; }
        public Boolean? IsEdit { get; set; }
        public Int64 TaskCount { get; set; }
        public Int64 InstructionCount { get; set; }
        public int? PageNumber { get; set; }
        public int? NoofRows { get; set; }
        public int? Skip { get; set; }
        public Boolean? IsAlert { get; set; }
        public Boolean? IsLock { get; set; }
        public String User_Token_val { get; set; }
        
    }
    public class workOrderAppDTO
    {
        public Int64 workOrder_ID { get; set; }
        public String workOrderNumber { get; set; }
        public String workOrderInfo { get; set; }
        public String address1 { get; set; }
        public String fulladdress { get; set; }
        public String Processor_Name { get; set; }

        public String city { get; set; }
        public String state { get; set; }
        public Int64? zip { get; set; }
        public String country { get; set; }

        public String status { get; set; }
        public String dueDate { get; set; }
        public String startDate { get; set; }
        public String clientInstructions { get; set; }
        public String clientStatus { get; set; }
        public String clientDueDate { get; set; }
        public String gpsLatitude { get; set; }
        public String gpsLongitude { get; set; }


        public Boolean? IsActive { get; set; }
        public Boolean? IsDelete { get; set; }
        public int currUserId { get; set; }
        public int Type { get; set; }
        public String ErrorCode { get; set; }
        public String workOrderNumber_Org { get; set; }



        public Int64? WorkType { get; set; }
        public String Company { get; set; }
        public String Com_Name { get; set; }
        public String Com_Phone { get; set; }
        public String Com_Email { get; set; }
        public String Contractor { get; set; }
        public String Received_Date { get; set; }
        public String Complete_Date { get; set; }
        public String Cancel_Date { get; set; }

        public String IPLNO { get; set; }
        public Int64? Category { get; set; }
        public String Loan_Info { get; set; }

        public Int64? Customer_Number { get; set; }
        public Int64? Cordinator { get; set; }
        public Int64? Processor { get; set; }
        public bool? BATF { get; set; }
        public bool? ISInspection { get; set; }
        public String Lotsize { get; set; }
        public Int64? Rush { get; set; }

        public String Lock_Code { get; set; }
        public String Broker_Info { get; set; }
        public String Comments { get; set; }

        public String SM_Name { get; set; }
        public String WT_WorkType { get; set; }
        public String Client_Company_Name { get; set; }
        public String Cont_Name { get; set; }
        public String Work_Type_Name { get; set; }
        public String Cust_Num_Number { get; set; }
        public String rus_Name { get; set; }
        public String Lock_Location { get; set; }
        public String Key_Code { get; set; }
        public String Gate_Code { get; set; }
        public String Loan_Number { get; set; }
        public string Cordinator_Name { get; set; }
        public String Search_Address { get; set; }

        public string ClientMetaData { get; set; }
        public string Office_Approved { get; set; }
        public string Sent_to_Client { get; set; }

        public string Client_Result_Photo_FilePath { get; set; }
        public string Client_Result_Photo_FileName { get; set; }
        public Int64 UserID { get; set; }
        public DateTime? DateCreated { get; set; }
        public String EstimatedDate { get; set; }


        public String Field_complete { get; set; }
        public DateTime? OfficeApproved_date { get; set; }
        public DateTime? SentToClient_date { get; set; }
        public DateTime? Field_complete_date { get; set; }
        public String Status_Name { get; set; }
        public String whereClause { get; set; }

        public String CORNT_CellNumber { get; set; }
        public String CORNT_User_LoginName { get; set; }
        public String CORNT_User_FirstName { get; set; }

        public int Workstatustype { get; set; }

        public String assigned_date { get; set; }

        public String Loan_Type { get; set; }

        public Boolean? photostatus { get; set; }
        public int? Wo_Start { get; set; }
        public String CORNT_User_Email { get; set; }
        public String User_Token_val { get; set; }
    }


    public class workOrderListAppDTO
    {
        public Int64 workOrder_ID { get; set; }
        public String workOrderNumber { get; set; }

        public String address1 { get; set; }
        public String fulladdress { get; set; }
        public String Processor_Name { get; set; }

        public String city { get; set; }
        public String state { get; set; }
        public Int64? zip { get; set; }


        public String status { get; set; }
        public String dueDate { get; set; }
        public String startDate { get; set; }
        public String gpsLatitude { get; set; }
        public String gpsLongitude { get; set; }


        public Boolean? IsActive { get; set; }
        public Boolean? IsDelete { get; set; }
        public int currUserId { get; set; }
        public int Type { get; set; }
        public String ErrorCode { get; set; }
        public String workOrderNumber_Org { get; set; }



        public Int64? WorkType { get; set; }


        public String Received_Date { get; set; }

        public String EstimatedDate { get; set; }
        public String IPLNO { get; set; }
        public Int64? Category { get; set; }
        public String Loan_Info { get; set; }


        public bool? BATF { get; set; }



        public String SM_Name { get; set; }
        public String WT_WorkType { get; set; }
        public String Client_Company_Name { get; set; }
        public String Cont_Name { get; set; }
        public String Work_Type_Name { get; set; }
        public String Cust_Num_Number { get; set; }
        public String rus_Name { get; set; }
        public String Lock_Location { get; set; }
        public String Key_Code { get; set; }




        public string Client_Result_Photo_FilePath { get; set; }
        public string Client_Result_Photo_FileName { get; set; }
        public Int64 UserID { get; set; }


        public String CORNT_CellNumber { get; set; }
        public String Cordinator_Name { get; set; }

        public String CORNT_User_LoginName { get; set; }
        public String CORNT_User_FirstName { get; set; }

        public int Workstatustype { get; set; }
        public String assigned_date { get; set; }

        public String Lock_Code { get; set; }

        public String Loan_Number { get; set; }


        public String Loan_Type { get; set; }
        public Boolean? photostatus { get; set; }
        public int? Wo_Start { get; set; }
        public String CORNT_User_Email { get; set; }

    }

    public class workOrderListAppOffilneDTO
    {
        public Int64 workOrder_ID { get; set; }
        public String workOrderNumber { get; set; }

        public String address1 { get; set; }
        public String fulladdress { get; set; }
        public String Processor_Name { get; set; }

        public String city { get; set; }
        public String state { get; set; }
        public Int64? zip { get; set; }


        public String status { get; set; }
        public String dueDate { get; set; }
        public String startDate { get; set; }
        public String gpsLatitude { get; set; }
        public String gpsLongitude { get; set; }


        public Boolean? IsActive { get; set; }
        public Boolean? IsDelete { get; set; }
        public int currUserId { get; set; }
        public int Type { get; set; }
        public String ErrorCode { get; set; }
        public String workOrderNumber_Org { get; set; }



        public Int64? WorkType { get; set; }


        public String Received_Date { get; set; }

        public String EstimatedDate { get; set; }
        public String IPLNO { get; set; }
        public Int64? Category { get; set; }
        public String Loan_Info { get; set; }


        public bool? BATF { get; set; }



        public String SM_Name { get; set; }
        public String WT_WorkType { get; set; }
        public String Client_Company_Name { get; set; }
        public String Cont_Name { get; set; }
        public String Work_Type_Name { get; set; }
        public String Cust_Num_Number { get; set; }
        public String rus_Name { get; set; }
        public String Lock_Location { get; set; }
        public String Key_Code { get; set; }




        public string Client_Result_Photo_FilePath { get; set; }
        public string Client_Result_Photo_FileName { get; set; }
        public Int64 UserID { get; set; }


        public String CORNT_CellNumber { get; set; }
        public String Cordinator_Name { get; set; }

        public Boolean? GB_work { get; set; }

        public Boolean? Info_work { get; set; }

        public String CORNT_User_LoginName { get; set; }
        public String CORNT_User_FirstName { get; set; }
        public int Workstatustype { get; set; }
        public String assigned_date { get; set; }

        public String Lock_Code { get; set; }

        public Boolean? photostatus { get; set; }

        public Additional_Details Additional_Details { get; set; }
        public List<TaskInvoiceBid_MobApp> TaskInvoiceBid_MobApps { get; set; }
        public int? Wo_Start { get; set; }
        public String CORNT_User_Email { get; set;  }
        public String User_Token_val { get; set; }

    }

    public class Additional_Details
    {
        public List<Instruction_Master_ChildDTO> Instruction_Master_ChildDTOs { get; set; }
        public List<InstctionMasterDTO> InstructionMasterDTOs_Bid { get; set; }
        public List<InstctionMasterDTO> InstructionMasterDTOs_Inspection { get; set; }
    }

    public class workOrderListMapDTO
    {
        public Int64 workOrder_ID { get; set; }
        public String IPLNO { get; set; }
        public String address1 { get; set; }
        public String fulladdress { get; set; }


        public String city { get; set; }
        public String state { get; set; }
        public Int64? zip { get; set; }


        public String status { get; set; }
        public String dueDate { get; set; }

        public String gpsLatitude { get; set; }
        public String gpsLongitude { get; set; }

        public Boolean? photostatus { get; set; }
        public String CORNT_User_Email { get; set; }

    }
    public class CountWorkOrder
    {
        public int ViewAll { get; set; }

        public int ViewGoBack { get; set; }

        public int ViewInfo { get; set; }

        public int ViewDueDate { get; set; }

        public int Type { get; set; }

        public Int64 workOrder_ID { get; set; }
        public Int64 UserID { get; set; }

        public string User_Token_val { get; set; }

        public Decimal? ConScore { get; set; }

        public string IPL_Company_ID { get; set; }

        public Int64? GroupRoleId { get; set; }
        public string Score_Details { get; set; }
    }
    public class ViewworkOrder
    {
        public Int64 workOrder_ID { get; set; }
        public String workOrderNumber { get; set; }
        public String Status_Name { get; set; }
        public String status { get; set; }

        public String address1 { get; set; }

        public String city { get; set; }
        public String state { get; set; }
        public Int64? zip { get; set; }
        public String country { get; set; }



        public DateTime? dueDate { get; set; }
        public DateTime? startDate { get; set; }

        public DateTime? clientDueDate { get; set; }
        public String gpsLatitude { get; set; }
        public String gpsLongitude { get; set; }



        public String Loan_Number { get; set; }


        public String IPLNO { get; set; }

        public Int64 UserID { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? EstimatedDate { get; set; }
        public DateTime? Received_Date { get; set; }
        public DateTime? Complete_Date { get; set; }
        public DateTime? Cancel_Date { get; set; }
        public String Com_Name { get; set; }
        public String Loan_Info { get; set; }
        public String Lotsize { get; set; }
        public String Lock_Code { get; set; }
        public Int64? assigned_admin { get; set; }


        public String ContractorName { get; set; }

        public int photocount { get; set; }

        public int addresscount { get; set; }

        public DateTime? SentToClient_date { get; set; }

        public String WT_WorkType { get; set; }

        public String Main_Cat_Name { get; set; }

        public DateTime? Field_complete_date { get; set; }

        public String Cust_Num_Number { get; set; }

        public String CordinatorName { get; set; }

        public String ProcessorName { get; set; }

        public int? Days_Infield { get; set; }

        public String Inv_Client_Invoice_Number { get; set; }

        public Decimal? Inv_Client_Sub_Total { get; set; }

        public DateTime? Inv_Client_Inv_Date { get; set; }


        public String Inv_Con_Invoce_Num { get; set; }

        public Decimal? Inv_Con_Sub_Total { get; set; }

        public DateTime? Inv_Con_Inv_Date { get; set; }

        public String Mortgagor { get; set; }

        public String Loan_Type { get; set; }
        public String Main_Cat_Back_Color { get; set; }
        public String Client_Company_Name { get; set; }
        public Int64? import_from_id { get; set; }

        public String ViewUrl { get; set; }

        public bool? IsEdit { get; set; }
        public int ecdnotecount { get; set; }

        public string Inv_Con_Status { get; set; }
        public Int64? Inv_Con_pkeyId { get; set; }
        public Decimal? Contractor_Balance { get; set; }
        public Decimal? Contractor_Payment { get; set; }

        public string Inv_Client_Status { get; set; }
        public Int64? Inv_Client_pkeyId { get; set; }
        public Decimal? Client_Payment { get; set; }
        public Decimal? Client_Balance { get; set; }
        public String CountyName { get; set; }
        public string ImportID { get; set; }

        public string Import_ID { get; set; }
        public string ImportName { get; set; }

        public string Occupancy_Status { get; set; }
        public string Property_Status { get; set; }
        public string Loan_Status { get; set; }

        public DateTime? LastCutDate { get; set; }
        public DateTime? NextCutDate { get; set; }

        public string ContractorLoginName { get; set; }
        public string ContractorEmail { get; set; }
        public string ContractorCellNumber { get; set; }
        public string ContractorAddress { get; set; }

        public string CordinatorLoginName { get; set; }
        public string CordinatorEmail { get; set; }
        public string CordinatorCellNumber { get; set; }
        public string CordinatorAddress { get; set; }

        public string ProcessorLoginName { get; set; }
        public string ProcessorEmail { get; set; }
        public string ProcessorCellNumber { get; set; }
        public string ProcessorAddress { get; set; }
        public string ContractorBackColor { get; set; }
        public Boolean? IsAlert { get; set; }
        public Boolean? IsLock { get; set; }
        public string PA_Name { get; set; }
        public string Lock_Name { get; set; }


    }

    public class ReturnworkOrder
    {
        public String workOrder_ID { get; set; }
        public String Status { get; set; }
        public String Main_Cat_Back_Color { get; set; }
    }

    public class RootworkOrder
    {
        public Int64 workOrder_ID { get; set; }
        public String workOrderNumber { get; set; }
        public String workOrderInfo { get; set; }
        public String address1 { get; set; }
        public String address2 { get; set; }
        public String city { get; set; }
        public String Mortgagor { get; set; }
        public String state { get; set; }
        public String state_name { get; set; }
        public Int64? zip { get; set; }
        public Int64? Background_Provider { get; set; }
        public String country { get; set; }
        public String options { get; set; }
        public String reference { get; set; }
        public String description { get; set; }
        public String instructions { get; set; }
        public String status { get; set; }
        public DateTime? dueDate { get; set; }
        public DateTime? startDate { get; set; }
        public String clientInstructions { get; set; }
        public String clientStatus { get; set; }
        public DateTime? clientDueDate { get; set; }
        public String gpsLatitude { get; set; }
        public String gpsLongitude { get; set; }
        public String attribute7 { get; set; }
        public String attribute8 { get; set; }
        public Int64? attribute9 { get; set; }
        public Int64? attribute10 { get; set; }
        public Int64? attribute11 { get; set; }
        public Int64? attribute12 { get; set; }
        public Int64? attribute13 { get; set; }
        public Int64? attribute14 { get; set; }
        public Int64? attribute15 { get; set; }
        public String source_wo_provider { get; set; }
        public String source_wo_number { get; set; }
        public Int64? source_wo_id { get; set; }
        public Int64? controlConfig { get; set; }
        public Int64? services_Id { get; set; }
        public Boolean? IsActive { get; set; }
        public Boolean? IsDelete { get; set; }
        public int currUserId { get; set; }
        public int Type { get; set; }

        public Int64? WorkType { get; set; }
        public String Company { get; set; }
        public String Com_Name { get; set; }
        public String Com_Phone { get; set; }
        public String Com_Email { get; set; }
        public String Contractor { get; set; }
        public DateTime? Received_Date { get; set; }
        public DateTime? Complete_Date { get; set; }
        public DateTime? Cancel_Date { get; set; }

        public String IPLNO { get; set; }
        public Int64? Category { get; set; }
        public String Loan_Info { get; set; }

        public Int64? Customer_Number { get; set; }
        public Int64? Cordinator { get; set; }
        public Int64? Processor { get; set; }
        public bool? BATF { get; set; }
        public bool? ISInspection { get; set; }
        public String Lotsize { get; set; }
        public Int64? Rush { get; set; }

        public String Lock_Code { get; set; }
        public String Broker_Info { get; set; }
        public String Comments { get; set; }
        public String Lock_Location { get; set; }
        public String Key_Code { get; set; }
        public String Gate_Code { get; set; }
        public String Loan_Number { get; set; }
        public String Search_Address { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? EstimatedDate { get; set; }

        public Boolean? Recurring { get; set; }
        public Int32? Recurs_Day { get; set; }
        public Int64? Recurs_Period { get; set; }
        public Int64? Recurs_Limit { get; set; }
        public DateTime? Recurs_CutOffDate { get; set; }

        public List<DateTime?> Recurs_ReceivedDateArray { get; set; }
        public List<DateTime?> Recurs_DueDateArray { get; set; }
        public Boolean? IsEdit { get; set; }
    }

    public class ReturnWorkOrder
    {
        public String workOrder_ID { get; set; }
        public String Type { get; set; }
    }

    public class MetadataWorkOrder
    {
        public Int64 workOrder_ID { get; set; }
        public String workOrderNumber { get; set; }
        public String workOrderInfo { get; set; }
        public String address1 { get; set; }
        public String address2 { get; set; }
        public String state { get; set; }
        public Int64? zip { get; set; }
        public String country { get; set; }
        public String status { get; set; }
        public DateTime? dueDate { get; set; }
        public DateTime? startDate { get; set; }
        public String clientInstructions { get; set; }
        public String clientStatus { get; set; }
        public DateTime? clientDueDate { get; set; }
        public String finaladdress { get; set; }
        public String fulladdress { get; set; }
        public String SM_Name { get; set; }
        public String WT_WorkType { get; set; }
        public String Client_Company_Name { get; set; }
        public String Cont_Name { get; set; }
        public String Cordinator_Name { get; set; }
        public String Work_Type_Name { get; set; }
        public String Cust_Num_Number { get; set; }
        public String ClientMetaData { get; set; }
        public int? statusid { get; set; }
        public String ContractorAddress { get; set; }
        public String ClientAddress { get; set; }
        public String Loan_Number { get; set; }
        public String ClientName { get; set; }
        public String Loan_Info { get; set; }
        public decimal? Client_Discount { get; set; }
        public decimal? Client_Contractor_Discount { get; set; }



    }

    public class workoderaction
    {
        public String whereClause { get; set; }
        public String Assigned_Param { get; set; }
        public int Type { get; set; }
        public Int64 UserId { get; set; }
        public List<WorkOrderIDItems> workOrderIDItems { get; set; }

    }

    public class WorkoderActionItems
    {
        public String Arr_WorkOrderID { get; set; }

        public String WorkOrder_Action { get; set; }
        public int Type { get; set; }
        public Int64 UserId { get; set; }

    }

    public class WorkOrderIDItems
    {
        public Int64 WorkOrderID { get; set; }


    }
    public class GetdeleteWorkmasterDataDTO
    {
        public Int64 UserID { get; set; }
        public int Type { get; set; }
    }

}