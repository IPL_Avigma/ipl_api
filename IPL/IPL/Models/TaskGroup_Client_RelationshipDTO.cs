﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class TaskGroup_Client_RelationshipDTO
    {
        public Int64 TGC_PkeyID { get; set; }
        public Int64 TGC_GroupID { get; set; }
        public Int64 TGC_ClientID { get; set; }
        public Int64 TGC_CompanyID { get; set; }
        public Boolean? TGC_IsActive { get; set; }
        public Boolean? TGC_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
    }
}