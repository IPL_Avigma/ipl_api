﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace IPL.Models
{
    public class Client_Invoice_Print_MasterDTO
    {
        public Int64 Inv_Client_pkeyId { get; set; }
        public Int64 Inv_Client_WO_Id { get; set; }
        public String Inv_Client_Invoice_Number { get; set; }
        public DateTime? Inv_Client_Inv_Date { get; set; }
        public Decimal? Inv_Client_Sub_Total { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
    }
    public class Client_Child_Invoice_Print_DTO
    {
        public Int64 Inv_Client_Ch_pkeyId { get; set; }
        public String Inv_Client_Ch_Qty { get; set; }
        public Decimal? Inv_Client_Ch_Price { get; set; }
        public Decimal? Inv_Client_Ch_Total { get; set; }
        public Decimal? Inv_Client_Ch_Adj_Price { get; set; }
        public Decimal? Inv_Client_Ch_Adj_Total { get; set; }
        public String Inv_Client_Ch_Comment { get; set; }
        public Decimal? Inv_Client_Ch_Discount { get; set; }
        public String Task_Name { get; set; }
    }
    public class CustomContastPDF
    {
        public dynamic Data { get; set; }
        public string Message { get; set; }
        public string completedate { get; set; }
        public HttpStatusCode HttpStatusCode { get; set; }
    }
}