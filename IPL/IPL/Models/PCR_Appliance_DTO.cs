﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class PCR_Appliance_DTO
    {
        public Int64 PCR_Appliance_pkeyId { get; set; }
        public Int64 PCR_Appliance_MasterId { get; set; }
        public Int64 PCR_Appliance_WO_Id { get; set; }
        public int? PCR_Appliance_ValType { get; set; }
        public String PCR_Appliance_Refrigerator { get; set; }
   
        public String PCR_Appliance_Stove { get; set; }
      
        public String PCR_Appliance_Stove_Wall_Oven { get; set; }
       
        public String PCR_Appliance_Dishwasher { get; set; }
  
        public String PCR_Appliance_Build_In_Microwave { get; set; }
      
        public String PCR_Appliance_Dryer { get; set; }
        
        public String PCR_Appliance_Washer { get; set; }
      
        public String PCR_Appliance_Air_Conditioner { get; set; }
        
        public String PCR_Appliance_Hot_Water_Heater { get; set; }
        
        public String PCR_Appliance_Dehumidifier { get; set; }
        
        public String PCR_Appliance_Furnace { get; set; }
        
        public String PCR_Appliance_Water_Softener { get; set; }
        
        public String PCR_Appliance_Boiler { get; set; }
       
        public Boolean? PCR_Appliance_IsActive { get; set; }
        public Boolean? PCR_Appliance_IsDelete { get; set; }
        public Int64 UserID { get; set; }
        public Int64 fwo_pkyeId { get; set; }
        public int Type { get; set; }

    }
    public class PCRApplianceDetails
    {
        public String PCR_Appliance_pkeyId { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
       
    }
}