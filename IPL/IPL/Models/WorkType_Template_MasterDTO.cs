﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class WorkType_Template_MasterDTO
    {
        public Int64 WTT_pkeyID { get; set; }
        public String WTT_Template_Name { get; set; }
        public Int64 WTT_IPL_Company_PkeyId { get; set; }
        public String WTT_Template_Description { get; set; }
        public Boolean WTT_IsDeleteAllow { get; set; }
        public Boolean WTT_IsActive { get; set; }
        public Boolean WTT_Delete { get; set; }
        public int Type { get; set; }
        public Int64 UserID { get; set; }
        public String WTT_CreatedBy { get; set; }
        public String WTT_ModifiedBy { get; set; }
    }
}