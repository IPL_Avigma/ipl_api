﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPLApp.Models
{
    public class Loan_Status_MasterDTO
    {

        public Int64 LS_PkeyID { get; set; }
        public string LS_Name { get; set; }
        public Boolean? LS_IsActive { get; set; }
        public Boolean? LS_IsDelete { get; set; }
        public int Type { get; set; }
        public Int64 UserID { get; set; }
        public string FilterData { get; set; }
        public string WhereClause { get; set; }
    }

    public class Loan_Status_Master
    {
        public String LS_PkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}