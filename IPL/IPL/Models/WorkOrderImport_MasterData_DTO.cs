﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPLApp.Models
{
    //public class WorkOrderImport_MasterData_DTO
    //{
    //    public Int64 WI_Pkey_ID { get; set; }
    //    public Int64 WI_ImportFrom { get; set; }
    //    public Int64 WI_SetClientCompany { get; set; }
    //    public String WI_LoginName { get; set; }
    //    public String WI_Password { get; set; }
    //    public String WI_AlertEmail { get; set; }
    //    public String WI_FriendlyName { get; set; }
    //    public Int64 WI_SkipComments { get; set; }
    //    public Int64 WI_SkipLineItems { get; set; }
    //    public Int64 WI_SetCategory { get; set; }
    //    public Int64 WI_StateFilter { get; set; }
    //    public decimal? WI_Discount_Import { get; set; }
    //    public Boolean? WI_IsActive { get; set; }
    //    public Boolean? WI_IsDeleted { get; set; }
    //    public int Type { get; set; }
    //    public int UserId { get; set; }
    //}

    public class WorkOrderImport_MasterData_DTO
    {
        public Int64 WI_Pkey_ID { get; set; }
        public Int64? WI_ImportFrom { get; set; }
        public Int64? WI_SetClientCompany { get; set; }
        public String WI_LoginName { get; set; }
        public String WhereClause { get; set; }
        public String FilterData { get; set; }
        public String Client_Company_Name { get; set; }
        public String Main_Cat_Name { get; set; }
        public String Import_Form_Name { get; set; }
        public String WI_SkipCommentsDec { get; set; }
        public String WI_SkipLineItemsDec { get; set; }
        public String WI_Password { get; set; }
        public String WI_AlertEmail { get; set; }
        public String WI_FriendlyName { get; set; }
        public Boolean? WI_SkipComments { get; set; }
        public Boolean? WI_SkipLineItems { get; set; }
        public Int64? WI_SetCategory { get; set; }
        public Int64? WI_StateFilter { get; set; }
        public decimal? WI_Discount_Import { get; set; }
        public Boolean? WI_IsActive { get; set; }
        public Boolean? WI_IsDeleted { get; set; }
        public int Type { get; set; }
        public Int64? UserId { get; set; }

        public string Import_Form_URL_Name { get; set; }

        public Boolean? WI_ImageDownload { get; set; }
        public Int64? WI_Processor { get; set; }
        public Int64? WI_Coordinator { get; set; }
        public String WI_FB_LoginName { get; set; }
        public String WI_FB_Password { get; set; }
        public String ViewUrl  { get; set; }
        public String Import_BucketName { get; set; }
        public String Import_BucketFolderName { get; set; }
        public String Import_DestBucketFolderName { get; set; }
        public String WI_Res_Code { get; set; }
        public String WI_Changed_Order_Alert { get; set; }
        public String WI_Cancelled_Order_Alert { get; set; }
        public String WI_Createdby { get; set; }
        public String WI_Modifiedby { get; set; }
        public List<EmailAutoAssign> EmailAutoAssign { get; set; }
    }
    public class WorkOrderImport
    {
        public String WI_Pkey_ID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
    public class EmailAutoAssign
    {
        public List<WI_AlertEmail> WI_AlertEmail { get; set; }
        public List<WI_Changed_Order_Alert> WI_Changed_Order_Alert { get; set; }
        public List<WI_Cancelled_Order_Alert> WI_Cancelled_Order_Alert { get; set; }
    }
    public class WI_AlertEmail
    {
        public String User_FirstName { get; set; }
        public Int64 User_pkeyID { get; set; }
    }
    public class WI_Changed_Order_Alert
    {
        public String User_FirstName { get; set; }
        public Int64 User_pkeyID { get; set; }
    }
    public class WI_Cancelled_Order_Alert
    {
        public String User_FirstName { get; set; }
        public Int64 User_pkeyID { get; set; }
    }
}