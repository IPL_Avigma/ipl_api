﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Report_WO_Filter_MasterDTO
    {
        public Int64 Report_WO_Filter_PkeyId { get; set; }
        public String Report_WO_Filter_Name { get; set; }
        public Boolean? Report_WO_Filter_IsActive { get; set; }
        public Boolean? Report_WO_Filter_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
        public List<Report_WO_Filter_ChildDTO> ArrayWOFilter { get; set; }
        public Int64 Report_WO_Filter_ChId { get; set; }
    }
    public class Report_WO_Filter_Master
    {
        public String Report_WO_Filter_PkeyId { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}