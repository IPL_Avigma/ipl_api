﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class AllowableMasterDTO
    {
        public Int64 Allowable_PKeyId { get; set; }
        public String Allowable_Name { get; set; }
        public String WhereClause { get; set; }
        public String FilterData { get; set; }
        public DateTime? Allowable_StartDate { get; set; }
        public DateTime? Allowable_EndDate { get; set; }
        public Int64? Allowable_OverallAllowables { get; set; }
        public Int64? Allowable_CompanyId { get; set; }
        public Boolean? Allowable_IsActive { get; set; }
        public Boolean? Allowable_IsDelete { get; set; }
        public int? Type { get; set; }
        public Int64? UserID { get; set; }
        public String User_FirstName { get; set; }
        public String Allowable_CreatedBy { get; set; }
        public String Allowable_ModifiedBy { get; set; }
        public List<AllowablesArray> AllowablesArray { get; set; }
        public string ViewUrl { get; set; }
        public Int64? Allowable_Cust_ID { get; set; }
    }
    public class AllowableDTO
    {
        public String Allowable_PKeyId { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
    public class AllowablesArray
    {
        public Int64 Allow_Child_PkeyId { get; set; }
        public Int64? Allow_Child_Allowables_PkeyId { get; set; }
        public Int64? Allow_Child_Allowables_Cat_PkeyId { get; set; }
        public DateTime? Allow_Child_StartDate { get; set; }
        public DateTime? Allow_Child_EndDate { get; set; }
        public String Allow_Child_OverallAllowables { get; set; }
        public Boolean? Allow_Child_IsActive { get; set; }
        public Boolean? Allow_Child_IsDelete { get; set; }
    }


}