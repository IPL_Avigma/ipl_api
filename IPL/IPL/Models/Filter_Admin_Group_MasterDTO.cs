﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Filter_Admin_Group_MasterDTO
    {
        public Int64 Group_Filter_PkeyID { get; set; }
        public String Group_Filter_Group_Name { get; set; }
        public Boolean? Group_Filter_IsActive { get; set; }
        public Boolean? Group_Filter_GRPIsActive { get; set; }
        public Boolean? Group_Filter_IsDelete { get; set; }
        public Int64? Group_Filter_CompanyId { get; set; }
        public Int64? Group_Filter_UserId { get; set; }
        public Int64 UserId { get; set; }
        public int Type { get; set; }
    }
}