﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class HeaderMapDTO
    {
         public Int64 workOrder_ID { get; set; }
        public String address1 { get; set; }
        public String gpsLatitude { get; set; }
        public String gpsLongitude { get; set; }
        public int Type { get; set; }
    }
}