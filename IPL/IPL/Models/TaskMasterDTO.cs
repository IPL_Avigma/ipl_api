﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class TaskMasterDTO
    {
        public Int64 Task_pkeyID { get; set; }
        public String Task_Name { get; set; }
        public String WhereClause { get; set; }
        public String FilterData { get; set; }
        public int? Task_Type { get; set; }
        public String Task_Photo_Label_Name { get; set; }
        public int? Task_Group { get; set; }
        public int? Task_UOM { get; set; }
        public Decimal? Task_Contractor_UnitPrice { get; set; }
        public Decimal? Task_Client_UnitPrice { get; set; }
        public bool? Task_IsActive { get; set; }
        public bool? Task_IsDelete { get; set; }
        public int? UserID { get; set; }
        public int Type { get; set; }

        public string Task_TypeName { get; set; }
        public string Task_Group_Name { get; set; }

        public bool? Task_AutoInvoiceComplete { get; set; }
        public Int64 WorkOrderID { get; set; }
        public int Task_System { get; set; }
        public Boolean? Task_Flat_Free { get; set; }
        public Boolean? Task_Price_Edit { get; set; }
        public Boolean? Task_Disable_Default { get; set; }
        public Boolean? Allow_Selection { get; set; }
        public string ViewUrl  { get; set; }
        public string Task_CreatedBy { get; set; }
        public string Task_ModifiedBy { get; set; }
        public Boolean? Task_Auto_Assign { get; set; }
        public string Task_Auto_Assign_str { get; set; }
        public String Task_Work_Type_Group { get; set; }
    }

    public class TaskMaster
    {
        public String Task_pkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }

    public class Task_Status_Damage_Violation_Hazard
    {
        public Int64 Task_pkeyID { get; set; }
        public Int64? Task_WO_ID { get; set; }
        public Boolean? Task_IsActive { get; set; }
        public Boolean? Task_IsDelete { get; set; }
        public int? Task_Status { get; set; }
        public Int64? UserID { get; set; }
        public int? Type { get; set; }
        public int? Task_Type { get; set; }
        public string Task_Comma { get; set; }
        public string TaskArrayJson { get; set; }
    }

}