﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Service_Link_Form_Master_DTO
    {
        public Int64 SL_Pkey_ID { get; set; }
        public Int64 SL_WO_ID { get; set; }
        public Int64 SL_CompanyID { get; set; }
        public String SL_General_Information { get; set; }
        public String SL_Property_Condition_Report1 { get; set; }
        public String SL_Property_Condition_Report2 { get; set; }
        public String SL_Other_Result { get; set; }
        public String SL_Bids { get; set; }
        public String SL_Summary { get; set; }
        public Boolean SL_IsActive { get; set; }
        public Boolean SL_IsDelete { get; set; }
        public Int64 UserID { get; set; }
        public Int64 fwo_pkyeId { get; set; }
        public int Type { get; set; }
    }
}