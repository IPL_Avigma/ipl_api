﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Forms_ActionRules_MasterDTO
    {
        public Forms_ActionRules_MasterDTO()
        {         
            ActionRules = new HashSet<Forms_ActionRules>();
        }
        public Int64 ActionRuleId { get; set; }
        public string ActionRule_Operator { get; set; }
        public string ActionRule_Value { get; set; }
        public bool ActionRule_IsActive { get; set; }
        public DateTime ActionRule_CreatedOn { get; set; }
        public Int64 UserId { get; set; }
        public Int64 QuestionId { get; set; }
        public Int64 FormId { get; set; }
        public Int16 Type { get; set; }
        public bool ActionRule_IsDelete { get; set; }
        public string ActionRule_ValueText { get; set; }
        public ICollection<Forms_ActionRules> ActionRules { get; set; }
    }
}