﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class PCR_Grass_Cut_MasterDTO
    {

        public Int64 Grass_Cut_PkeyID { get; set; }
        public String Grass_Cut_Completed { get; set; }
        public String Grass_Cut_LotSize { get; set; }
        public String Grass_Cut_Unable_To_Cut { get; set; }
        public String Grass_Cut_Other { get; set; }
        public DateTime? Grass_Cut_Completion_Date { get; set; }
        public DateTime? Grass_Cut_EarliestDate { get; set; }
        public DateTime? Grass_Cut_LatestDate { get; set; }
        public String Grass_Cut_ForSale { get; set; }
        public String Grass_Cut_ForRent { get; set; }
        public String Grass_Cut_Realtor_Phone { get; set; }
        public String Grass_Cut_Realtor_Name { get; set; }
        public String Grass_Cut_New_Damage_Found { get; set; }
        public String Grass_Cut_Damage_Fire { get; set; }
        public String Grass_Cut_Damage_Neglect { get; set; }
        public String Grass_Cut_Damage_Vandal { get; set; }
        public String Grass_Cut_Damage_Freeze { get; set; }
        public String Grass_Cut_Damage_Storm { get; set; }
        public String Grass_Cut_Damage_Flood { get; set; }
        public String Grass_Cut_Roof_Leak { get; set; }
        public String Grass_Cut_Explain_New_Damage { get; set; }
        public String Grass_Cut_Occupancy { get; set; }
        public String Grass_Cut_Property_Secure { get; set; }
        public String Grass_Cut_Pool_Present { get; set; }
        public String Grass_Cut_Pool_Secured { get; set; }
        public String Grass_Cut_Violation_Posted { get; set; }
        public String Grass_Cut_Opening_Not_Boarded { get; set; }
        public String Grass_Cut_Opening_Boarded { get; set; }
        public String Grass_Cut_Debris_Present { get; set; }
        public String Grass_Cut_Trees_Touching_House { get; set; }
        public String Grass_Cut_Vines_Touching_House { get; set; }
        public String Grass_Cut_Explain_violations { get; set; }
        public Boolean? Grass_Cut_IsActive { get; set; }
        public Boolean? Grass_Cut_IsDelete { get; set; }
        public Boolean? Grass_Cut_IsSync { get; set; }
        public Int64? Grass_Cut_WO_ID { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
        public Int64 fwo_pkyeId { get; set; }

    }
    public class PCR_Grass_Cut
    {
        public String Grass_Cut_PkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}