﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class WorkOrder_CustomPhotoLabelDTO
    {
        public Int64 WorkOrderPhotoLabel_pkeyID { get; set; }
        public Int64 WorkOrderPhotoLabel_PhotoLabel_ID { get; set; }
        public Int64 WorkOrderPhotoLabel_WO_ID { get; set; }
        public Boolean? WorkOrderPhotoLabel_IsActive { get; set; }
        public Boolean? WorkOrderPhotoLabel_IsDelete { get; set; }
        public int Type { get; set; }
        public int? UserID { get; set; }

        public List<Custom_PhotoLabel_MasterDTO> Custom_PhotoLabel_MasterDTO { get; set; }
    }

    public class WorkOrder_CustomPhotoLabeOut
    {
        public String WorkOrderPhotoLabel_pkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}