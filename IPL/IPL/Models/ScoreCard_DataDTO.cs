﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class ScoreCard_DataDTO
    {
        public Int64 Scd_pkeyId { get; set; }
        public Int64? Scd_Wo_Id { get; set; }
        public Int64? Scd_Con_ID { get; set; }
        public String Scd_Comment { get; set; }
        public int? Scd_Status_Id { get; set; }
        public Boolean? Scd_IsActive { get; set; }
        public Boolean? Scd_IsDelete { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }

        public String ScoreCard_data { get; set; }
        public List<ScoreCard_DTO> ScoreCard_DTO { get; set; }
    }

    public class ScoreCard_Name_MasterDTO
    {
        public Int64 Sna_pkeyId { get; set; }
        public String Sna_Name { get; set; }
        public Boolean? Sna_IsActive { get; set; }
        public Boolean? Sna_IsDelete { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
    }


    public class ScoreCard_DTO
    {
        public Int64 Scdc_pkeyId { get; set; }
        public String Sna_Name { get; set; }
        public Int64? Sna_pkeyId { get; set; }

        public String str_Scdc_Status_Id { get; set; }

        public int Status_Id { get; set; }
      
        public Int64 UserID { get; set; }
        public int Type { get; set; }
    }
}