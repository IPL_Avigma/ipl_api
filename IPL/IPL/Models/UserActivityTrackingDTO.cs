﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class UserActivityTrackingDTO
    {
        public Int64 User_Track_PkeyId { get; set; }
        public Int64? User_Track_UserID { get; set; }
        public DateTime? User_Track_Captured_On { get; set; }
        public int? User_Track_Mouse_Captured_Count { get; set; }
        public int? User_Track_Keyboard_Captured_Count { get; set; }
        public String User_Track_Page_Url { get; set; }
        public String WhereClause { get; set; }
        public String FilterData { get; set; }
        public String User_Track_Screen_Shot_Name { get; set; }
        public DateTime? User_Track_Created_Date { get; set; }
        public String User_Track_Captured_From_Ip { get; set; }
        public String User_Track_Folder_Name { get; set; }
        public String User_Track_File_Path { get; set; }
        public String User_Track_File_Name { get; set; }
        public Int64? User_Track_Company_Id { get; set; }
        public Boolean? User_Track_IsActive { get; set; }
        public Boolean? User_Track_IsDelete { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
        public int? User_Track_Time_Frequency { get; set; }
        public int? PageNumber { get; set; }
        public int? NoofRows { get; set; }
       
    }
    public class UserActivityTrackingFilterDTO
    {
        public DateTime? fromDate { get; set; }
        public DateTime? toDate { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
        public Int64? User_Track_UserID { get; set; }
        public Boolean? User_Track_IsActive { get; set; }
        public int? PageNumber { get; set; }
        public int? NoofRows { get; set; }
    }


        public class UserActivityTracking
    {
        public String User_Track_PkeyId { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}