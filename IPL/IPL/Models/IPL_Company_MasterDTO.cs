﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class IPL_Company_MasterDTO
    {
        public Int64 IPL_Company_PkeyId { get; set; }
        public String IPL_Company_Name { get; set; }
        public String IPL_Company_Address { get; set; }
        public String IPL_Company_Mobile { get; set; }
        public String IPL_Company_City { get; set; }
        public String IPL_Company_State { get; set; }
        public Int64? IPL_Company_PinCode { get; set; }
        public Decimal? IPL_Company_Subscribe_Amount { get; set; }
        public DateTime? IPL_Company_Subscribe_date { get; set; }
        public String IPL_Company_Subscribe_Type { get; set; }
        public String IPL_Company_Subscribe_Valid { get; set; }
        public String IPL_Company_Subscribe_Users { get; set; }
        public int? IPL_Company_Subscribe_User_Count { get; set; }
        public Boolean? IPL_Company_IsActive { get; set; }
        public Boolean? IPL_Company_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
        public String IPL_Company_County { get; set; }
        public String IPL_Contact_Name { get; set; }
        public String IPL_Company_Email { get; set; }
        public String IPL_Company_Contact_Mobile { get; set; }
        public String IPL_Company_Phone { get; set; }
        public String IPL_Company_Company_Link { get; set; }
        public String IPL_Company_Contact_Email { get; set; }
        public String IPL_Company_ID { get; set; }


        public String User_FirstName { get; set; }
        public String User_LastName { get; set; }
        public String User_LoginName { get; set; }
        public String User_Password { get; set; }

        public string WhereClause { get; set; }
        public int PageNumber { get; set; }
        public int Rowcount { get; set; }

        public string FilterData { get; set; }
    }
    public class IPL_Company_Master
    {
        public String IPL_Company_PkeyId { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }

    public class NewIPLCompanyMaster_Filter
    {
        public string IPL_Company_Name { get; set; }
        public string IPL_Company_Address { get; set; }
        public string IPL_Company_State { get; set; }



    }
}