﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class PCR_WinterizationDTO
    {

        public Int64 PCR_Winterization_pkeyId { get; set; }
        public Int64? PCR_Winterization_MasterId { get; set; }
        public Int64? PCR_Winterization_WO_Id { get; set; }
        public int? PCR_Winterization_ValType { get; set; }
        public String PCR_Winterization_Upon_Arrival { get; set; }
        public String PCR_Winterization_Compleate_This_Order_Yes { get; set; }
        public String PCR_Winterization_Compleate_This_Order_No { get; set; }
        public String PCR_Winterization_Compleate_This_Order_Partial { get; set; }
        public String PCR_Winterization_Upon_Arrival_Never_Winterized { get; set; }
        public String PCR_Winterization_Upon_Arrival_Breached { get; set; }
        public Boolean? PCR_Winterization_Reason_Wint_NotCompleted_Allowable { get; set; }
        public Boolean? PCR_Winterization_Reason_Wint_NotCompleted_Upon_Arrival { get; set; }
        public Boolean? PCR_Winterization_Reason_Wint_NotCompleted_Out_Season { get; set; }
        public Boolean? PCR_Winterization_Reason_Wint_NotCompleted_TernedOff { get; set; }
        public Boolean? PCR_Winterization_Reason_Wint_NotCompleted_Prop_Damaged { get; set; }
        public Boolean? PCR_Winterization_Reason_Wint_NotCompleted_Plumbing_Damage { get; set; }
        public Boolean? PCR_Winterization_Reason_Wint_NotCompleted_Plumbing_IsMissing { get; set; }
        public Boolean? PCR_Winterization_Reason_Wint_NotCompleted_AllReady_Winterized { get; set; }
        public Boolean? PCR_Winterization_Reason_Wint_NotCompleted_Common_Water_Line { get; set; }
        public Boolean? PCR_Winterization_Reason_Wint_NotCompleted_Maintaining_Utilities { get; set; }
        public Boolean? PCR_Winterization_Reason_Wint_NotCompleted_Other { get; set; }
        public String PCR_Winterization_Heating_System { get; set; }
        public String PCR_Winterization_Heating_System_Well { get; set; }
        public String PCR_Winterization_Radiant_Heat_System { get; set; }
        public String PCR_Winterization_Radiant_Heat_System_Well { get; set; }
        public String PCR_Winterization_Steam_Heat_System { get; set; }
        public String PCR_Winterization_Steam_Heat_System_Well { get; set; }
        public String PCR_Winterization_Posted_Signs { get; set; }
        public String PCR_Winterization_Common_Water_Line { get; set; }
        public String PCR_Winterization_AntiFreeze_Toilet { get; set; }
        public String PCR_Winterization_Water_Heater_Drained { get; set; }
        public String PCR_Winterization_Water_Off_At_Curb { get; set; }
        public String PCR_Winterization_Blown_All_Lines { get; set; }
        public String PCR_Winterization_System_Held_Pressure { get; set; }
        public String PCR_Winterization_Disconnected_Water_Meter_Yes { get; set; }
        public String PCR_Winterization_Disconnected_Water_Meter_No_Shut_Valve { get; set; }
        public String PCR_Winterization_Disconnected_Water_Meter_No_Common_Water_Line { get; set; }
        public String PCR_Winterization_Disconnected_Water_Meter_No_Unable_To_Locate { get; set; }
        public String PCR_Winterization_Disconnected_Water_Meter_No_Prohibited_Ordinance { get; set; }
        public String PCR_Winterization_Disconnected_Water_Meter_No_Others { get; set; }
        public String PCR_Winterization_Radiant_Heat_Boiler_Drained { get; set; }
        public String PCR_Winterization_Radiant_Heat_Zone_Valves_Opened { get; set; }
        public String PCR_Winterization_Radiant_Heat_AntiFreeze_Boiler { get; set; }
        public String PCR_Winterization_If_Well_System_Breaker_Off { get; set; }
        public String PCR_Winterization_If_Well_System_Pressure_Tank_Drained { get; set; }
        public String PCR_Winterization_If_Well_System_Supply_Line_Disconnect { get; set; }
        public String PCR_Winterization_Interior_Main_Valve_Shut_Off { get; set; }
        public String PCR_Winterization_Interior_Main_Valve_Reason { get; set; }
        public String PCR_Winterization_Interior_Main_Valve_Fire_Suppression_System { get; set; }
        public String PCR_Winterization_To_Bid { get; set; }
        public String PCR_Winterization_To_Bit_Text { get; set; }
        public String PCR_Winterization_Winterize { get; set; }
        public String PCR_Winterization_Thaw { get; set; }
        public String PCR_Winterization_Description { get; set; }
        public int? PCR_Winterization_System_Type { get; set; }
        public String PCR_Winterization_Reason { get; set; }
        public Decimal? PCR_Winterization_Amount { get; set; }
        public String PCR_Winterization_Winterize_Men { get; set; }
        public String PCR_Winterization_Winterize_Hrs { get; set; }
        public Boolean? PCR_Winterization_IsActive { get; set; }
        public Boolean? PCR_Winterization_IsDelete { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
        public String PCR_Winterization_Reason_Wint_NotCompleted_Other_Text { get; set; }
        public String PCR_Winterization_Disconnected_Water_Meter_Other_Text { get; set; }
        public String PCR_Winterization_TextArea_Comment { get; set; }
        public Int64 fwo_pkyeId { get; set; }

    }
    public class PCR_Winterization_Master
    {
        public String PCR_Winterization_pkeyId { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}