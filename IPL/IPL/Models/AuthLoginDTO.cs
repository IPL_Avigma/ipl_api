﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class AuthLoginDTO
    {
        public Int64 User_pkeyID { get; set; }
        public string User_LoginName { get; set; }
        public string User_Password { get; set; }
        public string User_Token_val { get; set; }
        public string User_FirstName { get; set; }
        public string User_LastName { get; set; }
        public string User_Email { get; set; }
        public int Type { get; set; }
        public Boolean? User_IsPasswordChange { get; set; }

        public int? UserCount { get; set; }

        public int? UserID { get; set; }
        public Int64 GroupRoleId { get; set; }

        public string User_CellNumber { get; set; }
        public Boolean? User_Tracking { get; set; }
        public int? User_Tracking_Time { get; set; }
        public Boolean IsAdminLogin { get; set; }
        public string MacId { get; set; }
        public string IP { get; set; }

        public string IPL_Company_ID { get; set; }
        public string User_ImagePath { get; set; }
        public String User_Acc_Log_Device_Name { get; set; }
        public int User_Source { get; set; }

    }

    public class EmailTemplateDTO
    {
        public Int64 Email_Temp_PkeyId { get; set; }
        public String Email_Temp_HTML { get; set; }
        public String Email_Temp_Subject { get; set; }
        public Boolean? Email_Temp_IsActive { get; set; }
        public Boolean? Email_Temp_Delete { get; set; }
        public int Type { get; set; }
        public Int64 UserID { get; set; }
        public int Val_Type { get; set; }
    }
    public class StatusCountChart
    {
        public int Unassigned_count { get; set; }
        public int Assigned_count { get; set; }
        public int Field_Complete_count { get; set; }
        public int Office_Approved_count { get; set; }
        public int Total_count { get; set; }
        public String Client_Company_Name { get; set; }
    }


    public class WorkOrderCountChart
    {
        public int countval { get; set; }
        public String Client_Company_Name { get; set; }
    }

    public class EmailHeadingDTO
    {
        public Int64 Eml_PkeyId { get; set; }
        public String Eml_Name { get; set; }
        public Boolean? Eml_IsActive { get; set; }
    }
    public class usertrackingDto
    {
        public Int64 User_pkeyID { get; set; }
        public Boolean? User_Tracking { get; set; }
    }
    public class Pastworkordercount
    {
        public Int64 Status_ID { get; set; }
        public String Status_Name { get; set; }
        public int PastStatusCount { get; set; }
        public int FutureStatusCount { get; set; }
        public int TotalCount { get; set; }
    }
    public class InvoiceDeshboardDto
    {
        public String Client_Company_Name { get; set; }
        public Int64? Client_pkeyID { get; set; }
        public Int64? Firstval { get; set; }
        public Int64? Secondval { get; set; }
        public Int64? Thirdval { get; set; }
        public Int64? Fourthval { get; set; }
    }


}