﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class PropertyConditionReport_DTO
    {
        public Int64 PCR_PkeyID { get; set; }
        public Int64 PCR_WO_ID { get; set; }
        public Int64 PCR_Company_ID { get; set; }
        public Int64? PCR_UserID { get; set; }
        public String PCR_General { get; set; }
        public String PCR_Utilities { get; set; }
        public String PCR_Securing { get; set; }
        public String PCR_Winterization { get; set; }
        public String PCR_Bording { get; set; }
        public String PCR_Debris { get; set; }
        public String PCR_Roof { get; set; }
        public String PCR_Moisture { get; set; }
        public String PCR_Yard { get; set; }
        public String PCR_Damages { get; set; }
        public String PCR_Others { get; set; }
        public String PCR_PhotoCheckList { get; set; }
        public String PCR_Summary { get; set; }
        public Boolean? PCR_IsActive { get; set; }
        public Boolean? PCR_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public Int64 fwo_pkyeId { get; set; }
        public int Type { get; set; }

    }
    public class PCR_LoggerMessage
    {
        public String PCR_PkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}