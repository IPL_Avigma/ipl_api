﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class PCR_Debris_MasterDTO
    {
        public Int64 PCR_Debris_pkeyId { get; set; }
        public Int64 PCR_Debris_Master_Id { get; set; }
        public Int64 PCR_Debris_WO_Id { get; set; }
        public int? PCR_Debris_ValType { get; set; }

        public String PCR_Debris_Remove_Any_Interior_Debris { get; set; }
        public String PCR_Debris_Is_There_Interior_Debris_Present { get; set; }
        public String PCR_Debris_Describe { get; set; }
        public String PCR_Debris_Cubic_Yards { get; set; }

        public String PCR_Debris_Broom_Swept_Condition { get; set; }
        public String PCR_Debris_Broom_Swept_Condition_Describe { get; set; }

        public string PCR_Debris_Remove_Exterior_Debris { get; set; }
        public string PCR_Debris_Exterior_Debris_Present { get; set; }

        public String PCR_Debris_Exterior_Debris_Describe { get; set; }
        public String PCR_Debris_Exterior_Debris_Cubic_Yard { get; set; }

        public string PCR_Debris_Exterior_Debris_Visible_From_Street { get; set; }
        public string PCR_Debris_Exterior_On_The_Lawn { get; set; }
        public string PCR_Debris_Exterior_Vehicles_Present { get; set; }
        public String PCR_Debris_Exterior_Vehicles_Present_Describe { get; set; }

        //

        public String PCR_Debris_Dump_Recipt_Name { get; set; }
        public String PCR_Debris_Dump_Recipt_Address { get; set; }
        public String PCR_Debris_Dump_Recipt_Phone { get; set; }
        public String PCR_Debris_Dump_Recipt_Desc_what_was_Dump { get; set; }
        public String PCR_Debris_Dump_Recipt_Means_Of_Disposal { get; set; }

        public string PCR_Debris_InteriorHazards_Health_Present { get; set; }
        public String PCR_Debris_InteriorHazards_Health_Present_Describe { get; set; }
        public String PCR_Debris_InteriorHazards_Health_Present_Cubic_Yard { get; set; }

        public string PCR_Debris_Exterior_Hazards_Health_Present { get; set; }
        public String PCR_Debris_Exterior_Hazards_Health_Present_Describe { get; set; }
        public String PCR_Debris_Exterior_Hazards_Health_PresentCubic_Yards { get; set; }
        public Boolean? PCR_Debris_IsActive { get; set; }
        public Boolean? PCR_Debris_IsDelete { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
        public Int64 fwo_pkyeId { get; set; }



    }
    public class PCR_Debris_Master
    {
        public String PCR_Debris_pkeyId { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}