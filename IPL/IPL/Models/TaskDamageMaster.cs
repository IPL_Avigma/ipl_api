﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class TaskDamageMaster
    {
        public Int64 Task_Damage_pkeyID { get; set; }
        public Int64? Task_Damage_WO_ID { get; set; }
        public Int64? Task_Damage_Task_ID { get; set; }
        public Int64? Task_Damage_ID { get; set; }
        public String Task_Damage_Type { get; set; }
        public String Task_Damage_Int { get; set; }
        public String Task_Damage_Location { get; set; }
        public String Task_Damage_Qty { get; set; }
        public String Task_Damage_Estimate { get; set; }
        public String Task_Damage_Disc { get; set; }
        public Boolean? Task_Damage_IsActive { get; set; }
        public Boolean? Task_Damage_IsDelete { get; set; }

        public string Task_Damage_Other_Name { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
        public int? Task_Damage_Status { get; set; }
        public String Damage_Disc { get; set; }
        public bool? Task_Damage_PreTextHide { get; set; }
    }
    public class TaskDamage
    {
        public String Task_Damage_pkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }



    public class TaskDamageMasterRootObject
    {
        public List<TaskDamageMaster> TaskDamageMaster { get; set; }
        public Int64 Task_Inv_WO_ID { get; set; }
        public int UserID { get; set; }
        public int Type { get; set; }
        public string str_Task_Bid_WO_ID { get; set; }
        public Int64? Task_Damage_WO_ID { get; set; }
    }
}