﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPLApp.Models
{
    public class InvoiceItemMasterDTO
    {
        public Int64 Inv_Itm_pkeyID { get; set; }
        public String Inv_Itm_Name { get; set; }
        public Boolean? Inv_Itm_IsActive { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
    }
    public class InvoiceItemMaster
    {
        public String Inv_Itm_pkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}