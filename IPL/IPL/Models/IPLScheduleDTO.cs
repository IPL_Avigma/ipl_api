﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class IPLScheduleDTO
    {
        public string PPWusername { get; set; }
        public string PPWPassword { get; set; }

        public string FBUsername { get; set; }
        public string FBPassword { get; set; }

        public string URL { get; set; }
        public String WI_Res_Code { get; set; }

        public Int64 importfrom { get; set; }
        public Int64 WI_Pkey_ID { get; set; }

        public Boolean? ImageDownload { get; set; }
        public DateTime startDateStr { get; set; }

    }
}