﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class MainCategoroy_Configuration_MasterDTO
    {
        public Int64 MainCategoroy_Configuration_PkeyId { get; set; }
        public Int64? MainCategoroy_Configuration_MainCategoroy_Id { get; set; }
        public String Main_Cat_Name { get; set; }
        public Int64? MainCategoroy_Configuration_CategoryCode_Id { get; set; }
        public Int64? MainCategoroy_Configuration_Import_From_Id { get; set; }
        public Int64? MainCategoroy_Configuration_Client_Id { get; set; }
        public Int64? MainCategoroy_Configuration_Company_Id { get; set; }
        public Int64? MainCategoroy_Configuration_User_Id { get; set; }
        public Boolean? MainCategoroy_Configuration_IsActive { get; set; }
        public Boolean? MainCategoroy_Configuration_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
    }
    public class MainCategoroy_Configuration_Master
    {
        public String MainCategoroy_Configuration_PkeyId { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
    public class MainCategoroy_Configuration_ListDTO
    {
        public List<MainCategoroy_Configuration_MasterDTO> MainCategoroy_Configuration_List { get; set; }
    }
}