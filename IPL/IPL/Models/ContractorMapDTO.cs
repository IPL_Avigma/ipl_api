﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class ContractorMapDTO
    {
        public Int64 IPL_PkeyID { get; set; }
        public Int64? IPL_Primary_Zip_Code { get; set; }
        public String IPL_Primary_Latitude { get; set; }
        public String IPL_Primary_Longitude { get; set; }
        public Int64? IPL_UserID { get; set; }
        public String IPL_Address { get; set; }
        public String IPL_State { get; set; }
        public Boolean? IPL_IsActive { get; set; }
        public String Con_Cat_Name { get; set; }
        public String Con_Cat_Back_Color { get; set; }
        public String Con_Cat_Icon { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
        public String IPL_City { get; set; }
        public String FirstName { get; set; }
        public String User_Email { get; set; }
        public String User_CellNumber { get; set; }
        public Int64? User_Con_Cat_Id { get; set; }
    }

}