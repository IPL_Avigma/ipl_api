﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class WorkOrder_Cyprexx_History_MasterDTO
    {
        public Int64 WCyprexx_HS_PkeyID { get; set; }
        public String WCyprexx_HS_wo_id { get; set; }
        public String WCyprexx_HS_address { get; set; }
        public String WCyprexx_HS_City { get; set; }
        public String WCyprexx_HS_State { get; set; }
        public String WCyprexx_HS_Zip { get; set; }
        public String WCyprexx_HS_Customer { get; set; }
        public String WCyprexx_HS_Loan_Type { get; set; }
        public String WCyprexx_HS_Lock_Code { get; set; }
        public String WCyprexx_HS_Key_Code { get; set; }
        public DateTime? WCyprexx_HS_Received_Date { get; set; }
        public DateTime? WCyprexx_HS_Due_Date { get; set; }
        public String WCyprexx_HS_Comments { get; set; }
        public String WCyprexx_HS_gpsLatitude { get; set; }
        public String WCyprexx_HS_gpsLongitude { get; set; }
        public String WCyprexx_HS_ImportMaster_Pkey { get; set; }
        public String WCyprexx_HS_Import_File_Name { get; set; }
        public String WCyprexx_HS_Import_FilePath { get; set; }
        public String WCyprexx_HS_Import_Pdf_Name { get; set; }
        public String WCyprexx_HS_Import_Pdf_Path { get; set; }
        public Boolean? WCyprexx_HS_IsProcessed { get; set; }
        public Boolean? WCyprexx_HS_IsActive { get; set; }
        public Boolean? WCyprexx_HS_IsDelete { get; set; }
        public String WCyprexx_HS_Cyprexx_ID { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
    }

    public class WorkOrder_Cyprexx_History_Master
    {
        public String WCyprexx_HS_PkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}