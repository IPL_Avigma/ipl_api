﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPLApp.Models
{
    public class GroupMenuRelationDTO
    {
        public Int64 GroupMenuRelationID { get; set; }
        public Int64? GroupID { get; set; }
        public String Grp_Name { get; set; }
        public Int64? MenuID { get; set; }
        public String Ipre_MenuName { get; set; }
        public Boolean? IsActive { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
     
    }

    public class UserMenuAcessDTO
    {
        public Int64? MenuID { get; set; }
        public int count { get; set; }
        public Int64? UserID { get; set; }
    }
}