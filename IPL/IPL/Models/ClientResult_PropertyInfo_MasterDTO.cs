﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class ClientResult_PropertyInfo_MasterDTO
    {
        public Int64 CRPI_PkeyID { get; set; }
        public Int64? CRPI_WO_ID { get; set; }
        public String CRPI_LockCode { get; set; }
        public String CRPI_LockBox { get; set; }
        public String CRPI_LotSize { get; set; }
        public DateTime? CRPI_InitDefaultDate { get; set; }
        public DateTime? CRPI_PtvDate { get; set; }
        public DateTime? CRPI_InitSecureDate { get; set; }
        public DateTime? CRPI_DidRecDate { get; set; }
        public Boolean CRPI_IsActive { get; set; }
        public Boolean CRPI_IsDelete { get; set; }
        public DateTime? CRPI_OrCovDate { get; set; }
        public DateTime? CRPI_ExtReqDate { get; set; }
        public DateTime? CRPI_NewCovDate { get; set; }
        public Boolean? CRPI_ExtReq { get; set; }
        public Int64? CRPI_Occupanct_Status { get; set; }
        public Boolean? CRPI_Property_Locked { get; set; }
        //public Int64? CRPI_Property_Alert { get; set; }
        public String CRPI_Property_Alert { get; set; }
        public Int64? CRPI_Property_Type { get; set; }
        public String CRPI_GPS_Latitude { get; set; }
        public String CRPI_GPS_longitude { get; set; }
        public String CRPI_VPSCode { get; set; }
        public String CRPI_PropertyLockReason { get; set; }
        public Boolean? CRPI_Winterized { get; set; }
        public DateTime? CRPI_WinterizedDate { get; set; }
        public String CRPI_ConveyanceCondition { get; set; }
        public String CRPI_Client { get; set; }
        public String CRPI_Customer { get; set; }
        public DateTime? CRPI_OccupancyDate { get; set; }
        public String CRPI_Lot_Size_Pricing { get; set; }


        public bool? CRPI_Gason { get; set; }
        public bool? CRPI_Wateron { get; set; }
        public bool? CRPI_Elcton { get; set; }
        public string CRPI_GasLR { get; set; }
        public string CRPI_GasTS { get; set; }
        public string CRPI_WaterLR { get; set; }
        public string CRPI_WaterTS { get; set; }
        public string CRPI_ElctLR { get; set; }
        public string CRPI_ElctTS { get; set; }

        public string CRPI_Front_Of_HouseImagePath { get; set; }
        public string CRPI_Front_Of_HouseImageName { get; set; }
        public Int64? CRPI_Property_Status { get; set; }


        //Date Section 

        public DateTime? CRPI_Stop_Work_Date { get; set; }
        public string CRPI_Stop_Work_Reason { get; set; }
        public string CRPI_DaysInDefault { get; set; }
        public DateTime? CRPI_VPRExpirationDate { get; set; }
        public string CRPI_VPRFiled { get; set; }
        public DateTime? CRPI_ConfirmedSaleDate { get; set; }
        public DateTime? CRPI_REODate { get; set; }
        public DateTime? CRPI_FirstInspectionDate { get; set; }
        public DateTime? CRPI_LockChangeDate { get; set; }
        public DateTime? CRPI_LastGrasscutDate { get; set; }
        public DateTime? CRPI_ForeclosureSaleDate { get; set; }
        public DateTime? CRPI_DeedRecordedDate { get; set; }
        public DateTime? CRPI_RoutingDate { get; set; }
        public bool? CRPI_ICC { get; set; }
        public DateTime? CRPI_ICCDate { get; set; }
        public DateTime? CRPI_DateLoanFellOutOfICC { get; set; }
        public DateTime? CRPI_LatestICCDate { get; set; }
        public DateTime? CRPI_ConveyanceDueDate { get; set; }
        public DateTime? CRPI_ExtensionApprovalDate { get; set; }
        public DateTime? CRPI_NewConveyanceDueDate { get; set; }
        public DateTime? CRPI_OrgEstimatedClosingDate { get; set; }


        public Int64 UserID { get; set; }
        public int Type { get; set; }
    }
    public class ClientResult_PropertyInfo_Master
    {
        public String CRPI_PkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
    public class ClientResult_PropertyInfo_InvDTO
    {
        public Decimal? Inv_Con_Sub_Total { get; set; }
        public Decimal? Con_InvoicePaid { get; set; }
        public Decimal? Client_InvoiceTotal { get; set; }
        public Decimal? Client_InvoicePaid { get; set; }
    }
    public class ClientResult_PropertyInfo_BidDTO
    {
        public Int32 BidCount { get; set; }
        public Int32 PendingCount { get; set; }
        public Int32 ApproveCount { get; set; }
        public Int32 RejectCount { get; set; }
    }

    public class PropertyInfo_Alert_RelationShipMasterDTO
    {
        public Int64 PIAR_PkeyID { get; set; }
        public Int64 PIAR_CRPI_PkeyID { get; set; }
        public Int64 PIAR_PA_PkeyID { get; set; }
        public Int64 PIAR_AddressID { get; set; }
        public Boolean PIAR_IsActive { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
    }
}