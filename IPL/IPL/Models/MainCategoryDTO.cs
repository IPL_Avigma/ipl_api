﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class MainCategoryDTO
    {
        public Int64 Main_Cat_pkeyID { get; set; }
        public String Main_Cat_Name { get; set; }
        public String Main_Cat_CreatedBy { get; set; }
        public String Main_Cat_ModifiedBy { get; set; }
        public String Main_Cat_Back_Color { get; set; }
        public String Main_Cat_Con_Icon { get; set; }
        public Boolean? Main_Cat_Active { get; set; }
        public Boolean? Main_Cat_IsActive { get; set; }
        public Boolean? Main_Cat_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
        public String WhereClause { get; set; }
        public String FilterData { get; set; }
        public Boolean? Main_Cat_IsDeleteAllow { get; set; }
    }
    public class MainCategory
    {
        public String Main_Cat_pkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}