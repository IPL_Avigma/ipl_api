﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Forms_WO_Filter_WorkTypeDTO
    {
        public long WOF_WorkType_pkeyId { get; set; }
        //public long WOF_WorkTypeId { get; set; }
        public long WT_pkeyID { get; set; }
        public long WOF_FilterId { get; set; }
        public long WOF_FormId { get; set; }
        public bool WOF_WorkType_IsActive { get; set; }
        public bool WOF_WorkType_IsDelete { get; set; }
        public long UserId { get; set; }
        public DateTime WOF_WorkType_CreatedOn { get; set; }       
        public DateTime WOF_WorkType_ModifiedOn { get; set; }      
        public DateTime WOF_WorkType_DeletedOn { get; set; }
    }
}