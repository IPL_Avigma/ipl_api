﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Support_Setting_ReplyDTO
    {
        public Int64 Support_Rep_PkeyId { get; set; }
        public Int64? Support_Rep_Support_Id { get; set; }
        public String Support_Rep_Reply { get; set; }
        public int? Support_Rep_Status { get; set; }
        public Boolean? Support_Rep_IsActie { get; set; }
        public Boolean? Support_Rep_IsDelete { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
        public String Ipl_Ad_User_First_Name { get; set; }
        public String Ipl_Ad_User_Last_Name { get; set; }
        public DateTime? Support_Rep_CreatedOn { get; set; }
        public Boolean? Support_Rep_IsComment { get; set; }
        public Int64? Support_Rep_CommentId { get; set; }
    }
    public class Support_Setting_Reply
    {
        public String Support_Rep_PkeyId { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}