﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class MultipleClient_ch_InvoicePrintDTO
    {
        public Int64 Inv_Client_Ch_pkeyId { get; set; }
        public String Inv_Client_Ch_Qty { get; set; }
        public String Task_Name { get; set; }
        public Decimal? Inv_Client_Ch_Price { get; set; }
        public Decimal? Inv_Client_Ch_Total { get; set; }
        public Decimal? Inv_Client_Ch_Adj_Price { get; set; }
        public Decimal? Inv_Client_Ch_Adj_Total { get; set; }
        public int? Inv_Client_Ch_Discount { get; set; }
    }
    public class MultipleClientInvoice
    {
        public Int64 Inv_Client_pkeyId { get; set; }
        public Int64? Inv_Client_WO_Id { get; set; }
        public Decimal? Inv_Client_Sub_Total { get; set; }
        public int? Inv_Client_Client_Dis { get; set; }
        public Decimal? Inv_Client_Client_Total { get; set; }
        public DateTime? Inv_Client_Comp_Date { get; set; }
        public String Inv_Client_Invoice_Number { get; set; }
        public String Task_Name { get; set; }
        public DateTime? Inv_Client_Inv_Date { get; set; }
        public Decimal? Inv_Client_Discout_Amount { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
    }
    public class MultipleClientInvoiceWo
    {
        public String workOrderNumber { get; set; }
        public DateTime? Complete_Date { get; set; }
        public String Loan_Number { get; set; }
        public String Loan_Info { get; set; }
        public String fulladdress { get; set; }
        public String Work_Type_Name { get; set; }
        public String ClientMetaData { get; set; }
        public String ClientName { get; set; }      
        public String ClientAddress { get; set; }      
        public String IPLNO { get; set; }      
        public String ContractorAddress { get; set; }      
    }

    public class printAddressDTO
    {
        public Int64 Ipl_PkeyID { get; set; }
        public String Ipl_companyname { get; set; }
        public String Ipl_IPLAddress { get; set; }
        public String Ipl_IPLcity { get; set; }
        public String Ipl_IPLstate { get; set; }
        public String Ipl_IPLZip { get; set; }
        public String IPLContactnumber { get; set; }
    }
}