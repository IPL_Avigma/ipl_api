﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPLApp.Models
{
    public class LoanTypeMasterDTO
    {
        public Int64 Loan_pkeyId { get; set; }
        public String Loan_Type { get; set; }
        public String WhereClause { get; set; }
        public String FilterData { get; set; }
        public String Loan_CreatedBy { get; set; }
        public String Loan_ModifiedBy { get; set; }
        public Boolean? Loan_IsActive { get; set; }
        public Boolean? Loan_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
        public Boolean? Loan_IsDeleteAllow { get; set; }
    }
    public class LoanTypeMaster
    {
        public String Loan_pkeyId { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}