﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class PCR_FiveBrotherDTO
    {
        public Int64 PCR_FiveBro_id { get; set; }
        public Int64 PCR_FiveBro_WO_ID { get; set; }
        public String PCR_FiveBro_Propertyinfo { get; set; }
        public String PCR_FiveBro_Violations { get; set; }
        public String PCR_FiveBro_Securing { get; set; }
        public String PCR_FiveBro_Winterization { get; set; }
        public String PCR_FiveBro_Yard { get; set; }
        public String PCR_FiveBro_Debris_Hazards { get; set; }
        public String PCR_FiveBro_Roof { get; set; }
        public String PCR_FiveBro_Pool { get; set; }
        public String PCR_FiveBro_Utilities { get; set; }
        public String PCR_FiveBro_Appliances { get; set; }
        public String PCR_FiveBro_Damages { get; set; }
        public String PCR_FiveBro_Conveyance { get; set; }
        public Int64? PCR_FiveBro_Integration_Type { get; set; }
        public Boolean? PCR_FiveBro_IsIntegration { get; set; }
        public Boolean? PCR_FiveBro_IsActive { get; set; }
        public Boolean? PCR_FiveBro_IsDelete { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
        public Boolean? PCR_FiveBro_IsSync { get; set; }
        public string PCR_FiveBro_Json { get; set; }
        public int? PCR_FiveBro_Valtype { get; set; }

    }
    public class PCR_Fivebrother_Master
    {
        public String PCR_FiveBro_id { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}