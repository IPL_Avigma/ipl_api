﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Contractor_Client_Expense_Payment_DTO
    {
        public Int64   CCE_Pay_PkeyId { get; set; }
        public Int64?   CCE_Client_Pay_Invoice_Id { get; set; }
        public Int64?   CCE_Con_Pay_Invoice_Id { get; set; }
        public Int64?   CCE_Pay_Wo_Id { get; set; }
        public DateTime? CCE_Pay_Payment_Date { get; set; }
        public decimal? CCE_Pay_Amount { get; set; }
        public String CCE_Pay_Comment { get; set; }
        public Int64? CCE_Pay_EnteredBy { get; set; }
        public decimal? CCE_Pay_Balance_Due { get; set; }
        public Boolean? CCE_Pay_IsActive { get; set; }
        public Boolean? CCE_Pay_IsDelete { get; set; }
        public Int64? UserId { get; set; }
       
        public int Type { get; set; }
        public String CCE_Pay_Expense_Name { get; set; }
        public string WorkOrderID_mul { get; set; }
    }


    public class Contractor_Client_Expense_Payment
    {
        public String CCEP_Data { get; set; }
        public string WorkOrderID_mul { get; set; }
        public Int64? UserId { get; set; }
    }
    }