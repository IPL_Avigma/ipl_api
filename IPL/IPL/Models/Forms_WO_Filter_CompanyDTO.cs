﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Forms_WO_Filter_CompanyDTO
    {
        public long WOF_Company_pkeyId { get; set; }
        // public long WOF_CompanyId { get; set; }
        public long Client_pkeyID { get; set; }
        public long WOF_FilterId { get; set; }
        public long WOF_FormId { get; set; }
        public bool WOF_Company_IsActive { get; set; }
        public bool WOF_Company_IsDelete { get; set; }
        public long UserId { get; set; }
        public DateTime WOF_Company_CreatedOn { get; set; }
        public DateTime WOF_Company_ModifiedOn { get; set; }     
        public DateTime WOF_Company_DeletedOn { get; set; }        
    }
}