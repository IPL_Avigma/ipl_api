﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPLApp.Models
{
    public class WorkTypeCategoryDTO
    {
        public Int64 Work_Type_Cat_pkeyID { get; set; }
        public String Work_Type_Name { get; set; }
        public Boolean Work_Type_IsActive { get; set; }
        public Int64 Work_Type_Client_pkeyID { get; set; }
        public Int64 Work_Type_Template_pkeyID { get; set; }
        public String Client_Company_Name { get; set; }
        public String WTT_Template_Name { get; set; }
        public Boolean Work_Type_IsDeleteAllow { get; set; }
        public String Work_Type_CreatedBy { get; set; }
        public String Work_Type_ModifiedBy { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }

        public List<WorkTypeNameArr> Work_Type_NameArr { get; set; }
    }
    public class WorkTypeCategory
    {
        public String Work_Type_Cat_pkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }

    public class WorkTypeNameArr
    {
        public string Work_Type_Name { get; set; }
        public Int64 Work_Type_Cat_pkeyID { get; set; }

    }

    public class ImportFormDto
    {
        public Int64 Import_Form_PkeyId { get; set; }
        public String Import_Form_Name { get; set; }
        public Boolean? Import_Form_IsActive { get; set; }
    }

    public class StatusDetailsDTO
    {
        public Int64 Status_ID { get; set; }
        public String Status_Name { get; set; }
        public Boolean? Status_IsActive { get; set; }
        public Boolean? mydata { get; set; }
        public int Type { get; set; }
        public String whereclause { get; set; }
        public Boolean? Status_Filter { get; set; }
        public Boolean? Status_User { get; set; }
        public Int64 UserID { get; set; }

        public int? PageNumber { get; set; }
        public int? NoofRows { get; set; }
        public int? Skip { get; set; }
        public String FilterData { get; set; }

    }
    public class FilterDataDTO
    {
        public string field { get; set; }
        public string value { get; set; }
       
    }
        public class StatusMasterDto
    {
        public Int64 Status_ID { get; set; }
        public String Status_Name { get; set; }
        public Boolean? mydata { get; set; }
      
  

    }
}