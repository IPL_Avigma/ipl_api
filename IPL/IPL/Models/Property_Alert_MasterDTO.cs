﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPLApp.Models
{
    public class Property_Alert_MasterDTO
    {
        public Int64 PA_PkeyID { get; set; }
        public string PA_Name { get; set; }
        public Boolean? PA_IsActive { get; set; }
        public Boolean? PA_IsDelete { get; set; }
        public int Type { get; set; }
        public Int64 UserID { get; set; }
        public string WhereClause { get; set; }
        public string FilterData { get; set; }
    }

    public class Property_Alert_Master
    {
        public String PA_PkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
    }