﻿using IPL.Models.FiveBrothers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

//changes 
namespace IPL.Models
{
    public class Task_Configuration_MasterDTO
    {
        public Int64 Task_Configuration_PkeyId { get; set; }
        public Int64? Task_Configuration_Task_Id { get; set; }
        public String Task_Configuration_LoanType { get; set; }
        public Int64? Task_Configuration_ItemCode_Id { get; set; }
        public Int64? Task_Configuration_BidCategory_Id { get; set; }
        public Int64? Task_Configuration_BidDamage_Id { get; set; }
        public Int64? Task_Configuration_CategoryCode_Id { get; set; }
        public Int64? Task_Configuration_Import_From_Id { get; set; }
        public Int64? Task_Configuration_Client_Id { get; set; }
        public Int64? Task_Configuration_Company_Id { get; set; }
        public Int64? Task_Configuration_User_Id { get; set; }
        public Boolean? Task_Configuration_IsActive { get; set; }
        public Boolean? Task_Configuration_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
        public String Task_Configuration_TaskName { get; set; }
        public String Task_Configuration_ItemCode { get; set; }
        public String Task_Configuration_ItemCode_Desc { get; set; }
        public String Task_Configuration_ItemCode_Price { get; set; }

        public String Task_Configuration_CreatedBy { get; set; }
        public String Task_Configuration_ModifiedBy { get; set; }
        public List<Import_Client_ItemCode_MasterDTO> ItemCodeList { get; set; }
    }
    public class Task_Configuration
    {
        public String Task_Configuration_PkeyId { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
    public class Task_Configuration_ListDTO
    {
		 public List<Task_Configuration_MasterDTO> Task_Configuration_List { get; set; }
    }
}