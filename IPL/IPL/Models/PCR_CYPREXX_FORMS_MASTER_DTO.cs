﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class PCR_CYPREXX_FORMS_MASTER_DTO
    {
        public Int64 PCR_PCFM_pkeyId { get; set; }
        public Int64 PCR_PCFM_WO_Id { get; set; }
        public Int64 PCR_PCFM_CompanyID { get; set; }
        public String PCR_PCFM_General_Information { get; set; }
        public String PCR_PCFM_Property_Accessibility { get; set; }
        public String PCR_PCFM_Property_Type { get; set; }
        public String PCR_PCFM_Utilities { get; set; }

        public String PCR_PCFM_Occupancy_Information { get; set; }
        public String PCR_PCFM_Securing__Lock_Changes { get; set; }
        public String PCR_PCFM_Grage_shed_outbuilding_securing { get; set; }
        public String PCR_PCFM_Window_securing { get; set; }
        public String PCR_PCFM_Pool { get; set; }
        public String PCR_PCFM_Debris_Hazzards { get; set; }
        public String PCR_PCFM_Yard { get; set; }
        public String PCR_PCFM_Hazard_Abatement { get; set; }
        public String PCR_PCFM_Winterization { get; set; }
        public String PCR_PCFM_Damages { get; set; }
        public String PCR_PCFM_Signage { get; set; }
        public String PCR_PCFM_Canveyance { get; set; }
        public String PCR_PCFM_General_Comment { get; set; }
        public String PCR_PCFM_Vendor_Signature { get; set; }
        public Boolean? PCR_PCFM_IsActive { get; set; }
        public Boolean? PCR_PCFM_IsDelete { get; set; }
        public Int64 UserID { get; set; }
        public Int64 fwo_pkyeId { get; set; }
        public int Type { get; set; }
    }
}