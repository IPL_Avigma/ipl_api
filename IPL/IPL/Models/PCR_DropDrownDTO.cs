﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class PCR_DropDrownDTO
    {
        public Int64 PCR_Damage_Drd_pkeyID { get; set; }
        public String PCR_Damage_Drd_Name { get; set; }
        public Boolean? PCR_Damage_Drd_IsActive { get; set; }
        public Boolean? PCR_Damage_Drd_IsDelete { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
    }

    public class PCR_CauseDTO
    {
        public Int64 PCR_Cause_pkeyID { get; set; }
        public String PCR_Cause__Name { get; set; }
        public Boolean? PCR_Cause_IsActive { get; set; }
        public Boolean? PCR_Cause_IsDelete { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
    }

    public class PCR_BuildingDTO
    {
        public Int64 PCR_Building_pkeyID { get; set; }
        public String PCR_Building_Name { get; set; }
        public Boolean? PCR_Building_IsActive { get; set; }
        public Boolean? PCR_Building_IsDelete { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
    }
}