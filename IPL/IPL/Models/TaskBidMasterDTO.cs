﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class TaskBidMasterDTO
    {
        public Int64 Task_Bid_pkeyID { get; set; }
        public Int64? Task_Bid_TaskID { get; set; }
        public Int64 Task_Bid_WO_ID { get; set; }
        public String Task_Bid_Qty { get; set; }
        public Int64? Task_Bid_Uom_ID { get; set; }
        public Decimal? Task_Bid_Cont_Price { get; set; }
        public Decimal? Task_Bid_Cont_Total { get; set; }
        public Decimal? Task_Bid_Clnt_Price { get; set; }
        public Decimal? Task_Bid_Clnt_Total { get; set; }
        public String Task_Bid_Comments { get; set; }
        public Boolean? Task_Bid_Violation { get; set; }
        public Boolean? Task_Bid_damage { get; set; }
        public Boolean? Task_Bid_IsActive { get; set; }
        public Boolean? Task_Bid_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
        public int? Task_Bid_Status { get; set; }
        public bool? Task_Bid_PreTextHide { get; set; }
        public String Bid_Other_Task_Name { get; set; }
        public Boolean? Task_Bid_Hazards { get; set; }
        public int? Task_Bid_DamageItem { get; set; }
        public List<TaskPresetDTO> TaskPresetDTO { get; set; }
        public Int64 Task_Ext_pkeyID { get; set; }
        public Int64? Task_Ext_BidID { get; set; }
        public Int64? Task_Ext_WO_ID { get; set; }
        public String Task_Ext_Location { get; set; }
        public Int64? Task_Ext_DamageCauseId { get; set; }
        public String Task_Ext_Length { get; set; }
        public String Task_Ext_Width { get; set; }
        public String Task_Ext_Height { get; set; }
        public String Task_Ext_Men { get; set; }
        public String Task_Ext_Hours { get; set; }
        public Boolean? Task_Ext_IsActive { get; set; }
    }
    public class TaskBidMaster
    {
        public String Task_Bid_pkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }

    public class UpdateTaskBidstatusDTO
    {
        public String Task_Bid_Data { get; set; }
        public Int64 UserID { get; set; }
    }

    public class TaskBidMasterRootObject
    {
        public List<TaskBidMasterDTO> TaskBidMasterDTO { get; set; }
        public int Task_Bid_WO_ID { get; set; }
        public int UserID { get; set; }
        public int Type { get; set; }

        public string str_Task_Bid_WO_ID { get; set; }
    

    }
    public class TaskActionWorkOrder
    {
        public int Task_WO_ID { get; set; }
    }


    public class TaskBidforimg
    {
        public Int64 Task_Bid_TaskID { get; set; }
        public Int64? Task_Bid_WO_ID { get; set; }
        public String Task_Photo_Label_Name { get; set; }

        public Int64? Task_Bid_pkeyID { get; set; }
        public String ButtonName1 { get; set; }
        public String ButtonName2 { get; set; }
        public String ButtonName3 { get; set; }
        public String ButtonName4 { get; set; }
        public String ButtonName5 { get; set; }
        public String ButtonName6 { get; set; }
        public String ButtonName7 { get; set; }

        public int Task_Bid_Sys_Type { get; set; }

        public String Comments { get; set; }

        public Int64 UserID { get; set; }
        public String WO_IPLNO { get; set; }
        public int Type { get; set; }
    }


    public class TaskInvoiceBid_MobApp
    {
        public Int64 Task_Bid_TaskID { get; set; }
        public Int64? Task_Bid_WO_ID { get; set; }
        public String Task_Photo_Label_Name { get; set; }

        public String Task_Label_Name { get; set; }

        public Int64? Task_Bid_pkeyID { get; set; }
        public String ButtonName1 { get; set; }
        public String ButtonName2 { get; set; }
        public String ButtonName3 { get; set; }
        public String ButtonName4 { get; set; }
        public String ButtonName5 { get; set; }
        public String ButtonName6 { get; set; }
        public String ButtonName7 { get; set; }
        public String TMF_Task_FileName { get; set; }
        public String TMF_Task_localPath { get; set; }


        public String Task_Bid_Qty { get; set; }

        public Decimal? Task_Bid_Cont_Price { get; set; }
        public Decimal? Task_Bid_Cont_Total { get; set; }
        public Decimal? Task_Bid_Clnt_Price { get; set; }
        public Decimal? Task_Bid_Clnt_Total { get; set; }

        public Boolean? IsReject { get; set; }
        public String Comments { get; set; }
        public int Countval { get; set; }
        public int TaskStatus { get; set; }
        public int TaskStatusB { get; set; }
        public int TaskStatusD { get; set; }
        public int TaskStatusA { get; set; }
        public int TaskStatusM { get; set; }
        public int TaskStatusL { get; set; }

        public String Task_Photo_Setting_details { get; set; }

    }


    public class ContractorRejectTask
    {
        public Int64 TaskPkeyID { get; set; }

        public String TaskRemark { get; set; }

        public Int64 TaskRejectBy { get; set; }

        public int TaskType { get; set; }

        public int Type { get; set; }
        public Int64 UserID { get; set; }
        public Boolean Task_Bid_IsReject { get; set; }

    }


    public class ContractorRejectTaskStatus
    {


        public String Status { get; set; }

        public String ErrorMessage { get; set; }
    }

}