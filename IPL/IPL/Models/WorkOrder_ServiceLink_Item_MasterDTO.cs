﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class WorkOrder_ServiceLink_Item_MasterDTO
    {
        public Int64? WSERVICEINS_PkeyId { get; set; }
        public String WSERVICEINS_Ins_Name { get; set; }
        public String WSERVICEINS_Ins_Details { get; set; }
        public Int64? WSERVICEINS_Qty { get; set; }
        public Decimal? WSERVICEINS_Price { get; set; }
        public Decimal? WSERVICEINS_Total { get; set; }

        public Int64? WSERVICEINS_FkeyID { get; set; }
        public Boolean? WSERVICEINS_IsActive { get; set; }
        public Boolean? WSERVICEINS_IsDelete { get; set; }
        public String WSERVICEINS_Additional_Details { get; set; }


        public Int64 UserID { get; set; }
        public int Type { get; set; }
    }

    public class WorkOrder_ServiceLink_Item_Master
    {
        public String WSERVICEINS_PkeyId { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}