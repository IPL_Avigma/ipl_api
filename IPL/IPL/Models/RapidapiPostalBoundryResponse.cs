﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    //public class RapidapiPostalBoundryResponse
    //{
    //    public string type { get; set; }
    //    public List<Feature> features { get; set; }
    //    public string error { get; set; }
    //}

    public class RapidapiPostalBoundryResponse
    {
        public string type { get; set; }
        public List<Feature> features { get; set; }
        public string error { get; set; }
    }

    //public class Feature
    //{
    //    public string type { get; set; }
    //    public Properties properties { get; set; }
    //    public Geometry geometry { get; set; }
    //}

    public class Feature
    {
        public string type { get; set; }
        public Properties properties { get; set; }
        public Geometry geometry { get; set; }
    }

    //public class Properties
    //{
    //    public string zipCode { get; set; }
    //    public string country { get; set; }
    //    public string city { get; set; }
    //    public string county { get; set; }
    //    public string state { get; set; }
    //}

    public class Properties
    {
        public string zipCode { get; set; }
        public string country { get; set; }
        public string city { get; set; }
        public string county { get; set; }
        public string state { get; set; }
    }

    //public class Geometry
    //{
    //    public string type { get; set; }
    //    //public double[][][] coordinates { get; set; }
    //    //public List<List<List<double>>> coordinates { get; set; }
    //    public List<List<List<List<double>>>> coordinates { get; set; }
    //}

    //public class Geometry
    //{
    //    public string type { get; set; }
    //    public List<List<List<double>>> coordinates { get; set; }
    //}

    //public class Geometry
    //{
    //    public string type { get; set; }
    //    public List<List<List<double>>> coordinates { get; set; }
    //    public List<List<List<List<double>>>> coordinatesMultiPolygon { get; set; }
    //}

    public class Geometry
    {
        public string type { get; set; }
        public dynamic coordinates { get; set; }
    }
}
