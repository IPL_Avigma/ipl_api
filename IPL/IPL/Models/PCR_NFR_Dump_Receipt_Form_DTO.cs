﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class PCR_NFR_Dump_Receipt_Form_DTO
    {
        public Int64 NF_PkeyID { get; set; }
        public Int64 NF_NFR_WO_ID { get; set; }
        public Int64 NF_NFR_CompanyId { get; set; }
        public String NF_NFR_Dump_Receipt { get; set; }
        public Boolean? NF_IsActive { get; set; }
        public Boolean? NF_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public Int64 fwo_pkyeId { get; set; }
        public int Type { get; set; }
    }
}