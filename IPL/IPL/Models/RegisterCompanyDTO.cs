﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class RegisterCompanyDTO
    {
        public Int64 IPL_Company_PkeyId { get; set; }
        public String IPL_Company_Name { get; set; }
        public String IPL_Company_Address { get; set; }
        public String IPL_Company_Mobile { get; set; }
        public String IPL_Company_City { get; set; }
        public String IPL_Company_State { get; set; }
        public Int64? IPL_Company_PinCode { get; set; }
        public Boolean? IPL_Company_IsActive { get; set; }
        public Boolean? IPL_Company_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
        public String IPL_Company_County { get; set; }
        public String IPL_Contact_Name { get; set; }
        public String IPL_Company_Email { get; set; }
        public String IPL_Company_Company_Link { get; set; }
        public String IPL_Company_ID { get; set; }
        public String IPL_Company_Phone { get; set; }

        public Int64 User_pkeyID { get; set; }
        public String User_FirstName { get; set; }
        public String User_LastName { get; set; }
        public String User_Address { get; set; }
        public String User_City { get; set; }
        public int? User_State { get; set; }
        public Int64? User_Zip { get; set; }
        public String User_CellNumber { get; set; }
        public String User_CompanyName { get; set; }
        public String User_LoginName { get; set; }
        public String User_Password { get; set; }
        public String User_Email { get; set; }
        public Boolean? User_IsActive { get; set; }
        public Boolean? User_IsDelete { get; set; }
    }


}