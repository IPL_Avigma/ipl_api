﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class User_Access_log_MasterDTO
    {
        public Int64 User_Acc_Log_PkeyId { get; set; }
        public String User_Acc_Log_IP_Address { get; set; }
        public String User_Acc_Log_Logged_In { get; set; }
        public String User_Acc_Log_Logged_Out { get; set; }
        public String User_Acc_Log_MacD { get; set; }
        public String User_Acc_Log_UserName { get; set; }
        public String User_Acc_Log_Device_Name { get; set; }
        public Boolean? User_Acc_Log_IsActive { get; set; }
        public Boolean? User_Acc_Log_IsDelete { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
    }
    public class User_Access_log
    {
        public String User_Acc_Log_PkeyId { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
    public class User_Access_log_DeviceDTO
    {
        public Int64 User_Acc_Log_PkeyId { get; set; }
        public DateTime? User_Acc_Log_Logged_In { get; set; }
        public DateTime? User_Acc_Log_Logged_Out { get; set; }
        public String User_Acc_Log_UserName { get; set; }
        public String User_Acc_Log_Device_Name { get; set; }
        public String User_Acc_Log_CreatedBy { get; set; }
        public String User_Acc_Log_ModifiedBy { get; set; }
        public String User_Acc_Log_GpsLatitude { get; set; }
        public String User_Acc_Log_GpsLongitude { get; set; }
        public String User_Acc_Log_Event { get; set; }
    }
}