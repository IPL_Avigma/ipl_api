﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Task_Company_TBLDTO
    {
        public Int64 TC_pkeyID { get; set; }
        public Int64 TC_Task_pkeyID { get; set; }
        public Int64 TC_Task_sett_ID { get; set; }
        public Boolean? TC_IsActive { get; set; }
        public Boolean? TC_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }

        public Int64? TC_Task_sett_pkeyID { get; set; }
    }
    public class Task_Contractor_TBLDTO
    {
        public Int64 TCon_pkeyID { get; set; }
        public Int64? TCon_Task_pkeyID { get; set; }
        public Int64? TCon_Task_sett_ID { get; set; }
        public Boolean? TCon_IsActive { get; set; }
        public Boolean? TCon_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }

        public Int64? TCon_Task_sett_pkeyID { get; set; }
    }
    public class Task_Country_TBLDTO
    {
        public Int64 TCoun_pkeyID { get; set; }
        public Int64? TCoun_Task_pkeyID { get; set; }
        public Int64? TCoun_Task_sett_ID { get; set; }
        public Boolean? TCoun_IsActive { get; set; }
        public Boolean? TCoun_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
        public Int64? TCoun_Task_sett_pkeyID { get; set; }
    }
    public class Task_Cutomer_TBLDTO
    {
        public Int64 TCust_pkeyID { get; set; }
        public Int64? TCust_Task_pkeyID { get; set; }
        public Int64? TCust_Task_sett_ID { get; set; }
        public Boolean? TCust_IsActive { get; set; }
        public Boolean? TCust_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }

        public Int64? TCust_Task_sett_pkeyID { get; set; }
    }
    public class Task_Group_TBLDTO
    {
        public Int64 TG_pkeyID { get; set; }
        public Int64? TG_Task_pkeyID { get; set; }
        public Int64? TG_Task_sett_ID { get; set; }
        public Boolean? TG_IsActive { get; set; }
        public Boolean? TG_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
        public Int64? TG_Task_sett_pkeyID { get; set; }
    }
    public class Task_Loan_TBLDTO
    {
        public Int64 TLoan_pkeyID { get; set; }
        public Int64? TLoan_Task_pkeyID { get; set; }
        public Int64? TLoan_Task_sett_ID { get; set; }
        public Boolean? TLoan_IsActive { get; set; }
        public Boolean? TLoan_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
        public Int64? TLoan_Task_sett_pkeyID { get; set; }
    }
    public class Task_State_TBLDTO
    {
        public Int64 TS_pkeyID { get; set; }
        public Int64? TS_Task_pkeyID { get; set; }
        public Int64? TS_Task_sett_ID { get; set; }
        public Boolean? TS_IsActive { get; set; }
        public Boolean? TS_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }

        public Int64? TS_Task_sett_pkeyID { get; set; }
    }
    public class Task_WorkType_TBLDTO
    {
        public Int64 TW_pkeyID { get; set; }
        public Int64? TW_Task_pkeyID { get; set; }
        public Int64? TW_Task_sett_ID { get; set; }
        public Boolean? TW_IsActive { get; set; }
        public Boolean? TW_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
        public Int64? TW_Task_sett_pkeyID { get; set; }
    }
    public class Task_LotPrice_TBLDTO
    {
        public Int64 TLP_pkeyID { get; set; }
        public Int64? TLP_Task_pkeyID { get; set; }
        public Int64? TLP_Task_sett_ID { get; set; }
        public Boolean? TLP_IsActive { get; set; }
        public Boolean? TLP_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
        public Int64? TLP_Task_sett_pkeyID { get; set; }
    }
}