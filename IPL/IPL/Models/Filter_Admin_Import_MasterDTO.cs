﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Filter_Admin_Import_MasterDTO
    {
        public Int64 Import_Filter_PkeyID { get; set; }
        public String Import_Filter_ImpFromID { get; set; }
        public String Import_Filter_ImpName { get; set; }
        public String Import_Filter_ClientName { get; set; }
        public String Import_Filter_LoginName { get; set; }
        public String Import_Filter_CreatedBy { get; set; }
        public String Import_Filter_ModifiedBy { get; set; }
        public Boolean? Import_Filter_ImpIsActive { get; set; }
        public Boolean Import_Filter_IsDelete { get; set; }
        public Boolean? Import_Filter_IsActive { get; set; }
        public int? Type { get; set; }
        public Int64? UserId { get; set; }
    }
}