﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

//changes 
namespace IPL.Models.Account
{
    public class Acc_Bill_Receive_PaymentDTO
    {
        public Acc_Bill_Receive_PaymentDTO()
        {
        }
        public Acc_Bill_Receive_PaymentDTO(DataRow dataRow)
        {
            Bill_Rec_PkeyId = Helper.GetBigIntRowData(dataRow, "Bill_Rec_PkeyId");
            Bill_Rec_Vendor_Id = Helper.GetBigIntRowData(dataRow, "Bill_Rec_Vendor_Id");
            Bill_Rec_Payment_Date = Helper.GetDateTimeRowData(dataRow, "Bill_Rec_Payment_Date");
            Bill_Rec_Payment_Method = Helper.GetIntRowData(dataRow, "Bill_Rec_Payment_Method");
            Bill_Rec_Reference_No = Helper.GetStringRowData(dataRow, "Bill_Rec_Reference_No");
            Bill_Rec_Memo = Helper.GetStringRowData(dataRow, "Bill_Rec_Memo");
            Bill_Rec_Deposit_To = Helper.GetBigIntRowData(dataRow, "Bill_Rec_Deposit_To");
            Bill_Rec_Amount = Helper.GetDecimalRowData(dataRow, "Bill_Rec_Amount");
        }
        public Int64 Bill_Rec_PkeyId
        {
            get; set;
        }
        public Int64 Bill_pkeyId
        {
            get; set;
        }
        public string Bill_Number
        {
            get; set;
        }
        public Int64 Bill_Rec_Vendor_Id
        {
            get; set;
        }

        public DateTime? Bill_Rec_Payment_Date
        {
            get; set;
        }

        public int? Bill_Rec_Payment_Method
        {
            get; set;
        }
        public string Bill_Rec_Reference_No
        {
            get; set;
        }

        public long? Bill_Rec_Deposit_To
        {
            get; set;
        }
        public String Bill_Rec_Memo
        {
            get; set;
        }
        public decimal? Bill_Rec_Amount
        {
            get; set;
        }
        public Int64? UserID
        {
            get; set;
        }

        public int Type
        {
            get; set;
        }
        public String WhereClause
        {
            get; set;
        }

        public List<Receive_Bill_Item> Receive_Bill_Items
        {
            get;
            set;
        }
    }
    public class Receive_Bill_Item
    {
        public long? Bill_Rec_PkeyId
        {
            get; set;
        }
        public long? Bill_Rec_Bill_Id
        {
            get; set;
        }
        public Int64? Bill_VendorId
        {
            get; set;
        }
        public long? Bill_Rec_Deposit_To
        {
            get; set;
        }

        public string Bill_Number
        {
            get; set;
        }
        public DateTime? DueDate
        {
            get; set;
        }
        public DateTime? PaymentDate
        {
            get; set;
        }
        public decimal? Bill_Rec_Original_Amount
        {
            get; set;
        }
        public decimal? Bill_Rec_Payment
        {
            get; set;
        }
        public decimal? Bill_Total
        {
            get; set;
        }

        public decimal? Pending_Amount
        {
            get; set;
        }
        public int Bill_Status
        {
            get; set;
        }

    }
    public class Final_Bill_Receive_Payment_DTO
    {
        public long Bill_Rec_PkeyId
        {
            get; set;
        }
        public long? Bill_Rec_Vendor_Id
        {
            get; set;
        }
        public long? Bill_Rec_Bill_Id
        {
            get; set;
        }
        public DateTime? Bill_Rec_Payment_Date
        {
            get; set;
        }
        public decimal? Bill_Rec_Original_Amount
        {
            get; set;
        }
        public decimal? Bill_Rec_Payment
        {
            get; set;
        }
        public decimal? Bill_Rec_Pending_Amount
        {
            get; set;
        }
        public int? Bill_Rec_Payment_Method
        {
            get; set;
        }
        public string Bill_Rec_Reference_No
        {
            get; set;
        }
        public long? Bill_Rec_Deposit_To
        {
            get; set;
        }
        public string Bill_Rec_Memo
        {
            get; set;
        }
        public long? Bill_Rec_EnteredBy
        {
            get; set;
        }
        public bool? Bill_Rec_IsActive
        {
            get; set;
        }
        public bool? Bill_Rec_IsDelete
        {
            get; set;
        }
        public long Bill_Rec_CreatedBy
        {
            get; set;
        }
        public DateTime Bill_Rec_CreatedOn
        {
            get; set;
        }
        public long? Bill_Rec_ModifiedBy
        {
            get; set;
        }
        public DateTime? Bill_Rec_ModifiedOn
        {
            get; set;
        }
        public long? Bill_Rec_DeletedBy
        {
            get; set;
        }
        public DateTime? Bill_Rec_DeletedOn
        {
            get; set;
        }
        public int Type
        {
            get; set;
        }
        public String WhereClause
        {
            get; set;
        }
        public long? UserID
        {
            get; set;
        }
    }
}