﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models.Account
{
    public class Acc_Account_Payable_Receivable_DTO
    {
        public List<AccountPayableReceivable> AccountPayableReceivable
        {
            get; set;
        }
        public decimal Total
        {
            get; set;
        } = 0;
    }
    public class AccountPayableReceivable
    {
        public long AccountId
        {
            get; set;
        }
        public int AccountType
        {
            get; set;
        }
        public string AccountCode
        {
            get; set;
        }
        public string AccountName
        {
            get; set;
        }
        public decimal Amount
        {
            get; set;
        }
        public DateTime CreatedDate
        {
            get; set;
        }
    }
}