﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models.Account
{
    public class Acc_BalanceSheet_DTO
    {
        public long AccountId { get; set; }
        public int AccountType { get; set; }
        public string AccountCode { get; set; }
        public string AccountName { get; set; }
        public decimal Amount { get; set; } = 0;
        public DateTime CreatedDate
        {
            get;set;
        }
    }
    public class Acc_New_Balanace_Sheet_DTO
    {
        public Assets Assets
        {
            get; set;
        }
        public LiabilitiesAndEquity LiabilitiesAndEquity
        {
            get; set;
        }
        public decimal? TotalAsset
        {
            get; set;
        } = 0;
    }
    public class Assets
    {
        public List<CurrentAssets> CurrentAssets
        {
            get; set;
        }
        public decimal TotalCurrentAssets
        {
            get; set;
        } = 0;
        public List<AccountsReceivable> AccountsReceivable
        {
            get; set;
        }
        public decimal? TotalAccountsReceivable
        {
            get; set;
        } = 0;
    }
    public class CurrentAssets
    {
        public long AccountId
        {
            get; set;
        }
        public int AccountType
        {
            get; set;
        }
        public string AccountTypeName
        {
            get; set;
        }
        public string AccountCode
        {
            get; set;
        }
        public string AccountName
        {
            get; set;
        }
        public decimal Amount
        {
            get; set;
        } = 0;
        public DateTime CreatedDate
        {
            get; set;
        }
    }
    public class AccountsReceivable
    {
        public long InvoiceId
        {
            get; set;
        }

        public long? customerId
        {
            get; set;
        }
        public string CustomerName
        {
            get; set;
        }
        public InvoiceStatus invoiceStatus
        {
            get;set;
        }
        public DateTime CreatedDate
        {
            get;set;
        }
        public decimal Amount
        {
            get; set;
        } = 0;
    }
    public class LiabilitiesAndEquity
    {
        public List<CurrentLiabilities> CurrentLiabilities
        {
            get; set;
        }
        public List<Equity> Equity
        {
            get; set;
        }
        public decimal? TotalLiabilitiesAndEquity
        {
            get; set;
        } = 0;
        public decimal? TotalLiabilities
        {
            get; set;
        } = 0;
        public decimal? TotalEquity
        {
            get; set;
        } = 0;
    }
    public class CurrentLiabilities
    {
        public long AccountId
        {
            get; set;
        }
        public int AccountType
        {
            get; set;
        }
        public string AccountCode
        {
            get; set;
        }
        public string AccountName
        {
            get; set;
        }
        public decimal Amount
        {
            get; set;
        } = 0;
        public DateTime CreatedDate
        {
            get; set;
        }
    }
    public class Equity
    {
        public long AccountId
        {
            get; set;
        }
        public int AccountType
        {
            get; set;
        }
        public string AccountCode
        {
            get; set;
        }
        public string AccountName
        {
            get; set;
        }
        public decimal Amount
        {
            get; set;
        } = 0;
        public DateTime CreatedDate
        {
            get; set;
        }
    }
}