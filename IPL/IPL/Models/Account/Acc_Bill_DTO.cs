﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Models.Account
{
    public class Acc_Bill_DTO
    {
        public Acc_Bill_DTO()
        {
        }
        public Acc_Bill_DTO(DataRow dataRow)
        {
            Bill_pkeyId = Helper.GetBigIntRowData(dataRow, "Bill_pkeyId");
            Bill_Number = Helper.GetStringRowData(dataRow, "Bill_Number");
            Bill_VendorId = Helper.GetBigIntRowData(dataRow, "Bill_VendorId");
            Bill_Vendor_Name = Helper.GetStringRowData(dataRow, "Bill_Vendor_Name");
            Bill_Vendor_Email = Helper.GetStringRowData(dataRow, "Bill_Vendor_Email");
            Bill_Send_to = Helper.GetBoolRowData(dataRow, "Bill_Send_to");
            Bill_Send_to_DateTime = Helper.GetDateTimeRowData(dataRow, "Bill_Send_to_DateTime");
            Bill_CcBcc_Label = Helper.GetStringRowData(dataRow, "Inv_CcBcc_Label");
            Bill_Billing_Address = Helper.GetStringRowData(dataRow, "Bill_Billing_Address");
            Bill_Terms = Helper.GetStringRowData(dataRow, "Bill_Terms");
            Bill_Date = Helper.GetDateTimeRowData(dataRow, "Bill_Date");
            Bill_Due_Date = Helper.GetDateTimeRowData(dataRow, "Bill_Due_Date");
            Bill_PO_No = Helper.GetStringRowData(dataRow, "Bill_PO_No");
            Bill_Message = Helper.GetStringRowData(dataRow, "Bill_Message");
            Bill_Statement = Helper.GetStringRowData(dataRow, "Bill_Statement");
            Bill_Sub_total = Helper.GetDecimalRowData(dataRow, "Bill_Sub_total");
            Bill_Taxble_Amount = Helper.GetDecimalRowData(dataRow, "Bill_Taxble_Amount");
            Bill_Taxble_Sub_total = Helper.GetStringRowData(dataRow, "Bill_Taxble_Sub_total");
            Bill_Balance_Due = Helper.GetDecimalRowData(dataRow, "Bill_Balance_Due");
            Bill_Total = Helper.GetDecimalRowData(dataRow, "Bill_Total");
            UserID = Helper.GetBigIntRowData(dataRow, "UserID");
            Bill_Status = Helper.GetIntRowData(dataRow, "Bill_Status");
            Bill_Con_pkeyId = Helper.GetBigIntRowData(dataRow, "Bill_Con_pkeyId");
            Bill_Con_WO_Id = Helper.GetBigIntRowData(dataRow, "Bill_Con_WO_Id");
            Bill_Items = new List<Acc_Bill_ChildDTO>();
            Bill_Items.Add(new Acc_Bill_ChildDTO(dataRow));
            Bill_Payments = new List<Acc_Bill_Payment_DTO>();
            Bill_Payments.Add(new Acc_Bill_Payment_DTO(dataRow));
        }
        public Int64 Bill_pkeyId
        {
            get; set;
        }
        public String Bill_Number
        {
            get; set;
        }
        public Int64 Bill_VendorId
        {
            get; set;
        }
        public string Bill_Vendor_Name
        {
            get; set;
        }
        public String Bill_Vendor_Email
        {
            get; set;
        }
        public Boolean? Bill_Send_to
        {
            get; set;
        }
        public DateTime? Bill_Send_to_DateTime
        {
            get; set;
        }
        public string Bill_CcBcc_Label
        {
            get; set;
        }
        public String Bill_Billing_Address
        {
            get; set;
        }
        public string Bill_Terms
        {
            get; set;
        }
       
        public DateTime? Bill_Date
        {
            get; set;
        }
        public DateTime? Bill_Due_Date
        {
            get; set;
        }
        public String Bill_PO_No
        {
            get; set;
        }public String Bill_Message
        {
            get; set;
        }
        public String Bill_Statement
        {
            get; set;
        }
        public decimal? Bill_Sub_total
        {
            get; set;
        }
        public decimal? Bill_Taxble_Amount
        {
            get; set;
        }
        public String Bill_Taxble_Sub_total
        {
            get; set;
        }
        public decimal? Bill_Total
        {
            get; set;
        }
        public decimal? Bill_Balance_Due
        {
            get; set;
        }
        public Int64? UserID
        {
            get; set;
        }

        public bool? Bill_IsActive
        {
            get; set;
        }
        public bool? Bill_IsDelete
        {
            get; set;
        }
        public long Bill_CreatedBy
        {
            get; set;
        }
        public DateTime Bill_CreatedOn
        {
            get; set;
        }
        public long? Bill_ModifiedBy
        {
            get; set;
        }
        public DateTime? Bill_ModifiedOn
        {
            get; set;
        }
        public long? Bill_DeletedBy
        {
            get; set;
        }
        public DateTime? Bill_DeletedOn
        {
            get; set;
        }
        public int Type
        {
            get; set;
        }
        public String WhereClause
        {
            get; set;
        }
        public int? Bill_Status
        {
            get; set;
        }
        public List<Acc_Bill_ChildDTO> Bill_Items
        {
            get; set;
        }
        public List<Acc_Bill_Payment_DTO> Bill_Payments
        {
            get; set;
        }
        public Int64? Bill_Con_pkeyId
        {
            get;set;
        }
        public Int64? Bill_Con_WO_Id
        {
            get;set;
        }
        public List<Receive_Bill_Item> Receive_Bill_Items
        {
            get; set;
        }

        public String Bill_Created
        {
            get; set;
        }
        public String Bill_Modified
        {
            get; set;
        }
    }
}