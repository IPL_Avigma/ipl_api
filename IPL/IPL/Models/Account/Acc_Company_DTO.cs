﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Models.Account
{
    public class Acc_Company_DTO
    {
        public Acc_Company_DTO()
        {
        }
        public Acc_Company_DTO(DataRow dataRow)
        {
            Id = Helper.GetBigIntRowData(dataRow, "Id");
            CompanyName = Helper.GetStringRowData(dataRow, "CompanyName");
            ShortName = Helper.GetStringRowData(dataRow, "ShortName");
            CompanyCode = Helper.GetStringRowData(dataRow, "CompanyCode");
            UserID = Helper.GetBigIntRowData(dataRow, "UserID");
        }
        public Int64 Id { get; set; }
        public string CompanyName { get; set; }
        public string ShortName { get; set; }
        public string CompanyCode { get; set; }

        public string WhereClause { get; set; }
        public int Type { get; set; }
        public Int64? UserID
        {
            get; set;
        }
    }
}