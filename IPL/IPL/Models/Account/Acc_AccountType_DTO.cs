﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models.Account
{
    public class Acc_AccountType_DTO
    {
        public Int64 Acc_pkeyId { get; set; }
        public string Acc_Account_Type{ get; set; }
        public string Acc_Detail_Type { get; set; }
        public Int64? Acc_Parent_Account_Type_Id { get; set; }
        public bool Acc_IsActive { get; set; }
        public bool Acc_IsDelete { get; set; }
        public Int64 Acc_CreatedBy { get; set; }
        public DateTime Acc_CreatedOn { get; set; }
        public Int64 Acc_ModifiedBy { get; set; }
        public DateTime Acc_ModifiedOn { get; set; }
        public Int64 Acc_DeletedBy { get; set; }
        public DateTime Acc_DeletedOn { get; set; }

        public string WhereClause { get; set; }
        public int Type { get; set; }
    }
    public class DropDownAccountTypeDTO
    {
        public Int64 Acc_TypeID
        {
            get; set;
        }
        public Int64 UserID
        {
            get; set;
        }
        public int Type
        {
            get; set;
        }

    }
    public class AccountTypeDto
    {
        public Int64 Acc_Type_pkeyId
        {
            get; set;
        }
        public String Acc_Account_Type
        {
            get; set;
        }
    }
    public class AccountTypeDetailsDto
    {
        public Int64 Acc_Type_pkeyId
        {
            get; set;
        }
        public String Acc_Detail_Type
        {
            get; set;
        }
        public String Acc_Account_Type_Description
        {
            get; set;
        }
    }
}