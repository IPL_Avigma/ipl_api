﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Models.Account
{
    public class Acc_Ledger_Headers_DTO
    {
        public Acc_Ledger_Headers_DTO()
        {
        }
        public Acc_Ledger_Headers_DTO(DataRow dataRow)
        {
            LedgH_pkeyId = Helper.GetBigIntRowData(dataRow, "LedgH_pkeyId");
            LedgH_Date = Helper.GetDateTimeRowData(dataRow, "LedgH_Date");
            LedgH_Decription = Helper.GetStringRowData(dataRow, "LedgH_Decription");
            Acc_AccountParentID = Helper.GetBigIntRowData(dataRow, "Acc_Parent_Account_Id");
            Acc_AccountID = Helper.GetBigIntRowData(dataRow, "Acc_pkeyId");
            Acc_DrOrCr_Side = Helper.GetIntRowData(dataRow, "Acc_DrOrCr_Side");
            Acc_Account_Type = Helper.GetIntRowData(dataRow, "Acc_Account_Type");
            Acc_Account_Code = Helper.GetStringRowData(dataRow, "Acc_Account_Code");
            Acc_Account_Name = Helper.GetStringRowData(dataRow, "Acc_Account_Name");
            LedgE_DrCr = Helper.GetIntRowData(dataRow, "LedgE_DrCr");
            UserID = Helper.GetBigIntRowData(dataRow, "UserID");
            DebitAmount = (LedgE_DrCr == (int)DrOrCrSide.Dr ? Helper.GetDecimalRowData(dataRow, "LedgE_Amount"):0);
            CreditAmount = (LedgE_DrCr == (int)DrOrCrSide.Cr ? Helper.GetDecimalRowData(dataRow, "LedgE_Amount") : 0);
            LedgH_CreatedOn= Helper.GetDateTimeRowData(dataRow, "LedgH_CreatedOn");
            Acc_Detail_Type = Helper.GetStringRowData(dataRow, "Acc_Detail_Type");
            LedgH_Created = Helper.GetStringRowData(dataRow, "LedgH_Created");
            LedgH_Modified = Helper.GetStringRowData(dataRow, "LedgH_Modified");
            LedgH_CreatedByName = Helper.GetStringRowData(dataRow, "LedgH_CreatedByName");
            LedgH_ModifiedByName = Helper.GetStringRowData(dataRow, "LedgH_ModifiedByName");
        }
        public Int64 LedgH_pkeyId { get; set; }
        public DateTime? LedgH_Date { get; set; }
        public string LedgH_Decription { get; set; }
        public bool LedgH_IsActive { get; set; }
        public bool LedgH_IsDelete { get; set; }
        public Int64 LedgH_CreatedBy { get; set; }
        public DateTime LedgH_CreatedOn { get; set; }
        public Int64 LedgH_ModifiedBy { get; set; }
        public DateTime LedgH_ModifiedOn { get; set; }
        public Int64 LedgH_DeletedBy { get; set; }
        public DateTime LedgH_DeletedOn { get; set; }


        #region extra field
        public List<Acc_Ledger_Lines_DTO> LedgerEntry { get; set; }

        public string WhereClause { get; set; }
        public int Type { get; set; }
        public Int64 JournalId { get; set; }
        public Int64 Acc_AccountID { get; set; }
        public Int64 Acc_AccountParentID { get; set; }
        public int Acc_DrOrCr_Side { get; set; }
        public string Acc_Account_Code { get; set; }
        public string Acc_Account_Name { get; set; }
        public int Acc_Account_Type { get; set; }
        public decimal DebitAmount { get; set; }
        public decimal CreditAmount { get; set; }
        public int LedgE_DrCr { get; set; }

        public string LedgH_CreatedByName { get; set; }
        public string LedgH_ModifiedByName { get; set; }
        public Int64? UserID
        {
            get; set;
        }
        public string Acc_Detail_Type
        {
            get; set;
        }
        public String LedgH_Created { get; set; }
        public String LedgH_Modified { get; set; }

        #endregion
    }
}