﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IPL.Models.Account
{
    public enum DrOrCrSide
    {
        NA = 0,
        Dr = 1,
        Cr = 2
    }
    public enum AccountType
    {
        AccountReceivable = 1,
        OthercurrentAsset = 2,
        Bank=17,
        OtherAsset = 23,
        AccountPayable = 30,
        OtherCurrentLiabilities = 30,
        LongTermLiabilities=45,
        Equity=48,
        Income = 63,
        OtherIncome = 69,
        Expense = 79,
        OtherExpense = 107,
        FixedAsset = 131,
        Liabilities = 2,
        Revenue = 4,
        Temporary = 6,
        UndepositedFunds = 0,
        depositedFunds = 5,
        
      

    }
    public enum InvoiceStatus
    {
        NotPaid = 0,
        Paid = 1,
        Deposit = 2,


    }
    //public static class DashBoard
    //{
    //    public const string Income = "Income";
    //    public const string Expenses = "Expenses";
    //    public const string OtherExpenses = "Other Expenses";
    //}
    public enum Reports
    {
        [Display(Name = "BalanceSheet")]
        BalanceSheet = 1,
        [Display(Name = "ProfitLoss")]
        ProfitLoss = 2,
        [Display(Name = "TrailBalance")]
        TrailBalance = 3,
        [Display(Name = "IncomeStatement")]
        IncomeStatement = 4,
        [Display(Name = "AccountReceivable")]
        AccountReceivable = 5,
        [Display(Name = "AccountPayable")]
        AccountPayable = 6,
        [Display(Name = "ProfitLossDetails")]
        ProfitLossDetails = 7,
        [Display(Name = "ProfitLossComparison")]
        ProfitLossComparison = 8,
    }

}