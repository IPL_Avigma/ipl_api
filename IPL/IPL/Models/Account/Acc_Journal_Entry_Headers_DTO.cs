﻿using System;
using System.Collections.Generic;
using System.Data;

namespace IPL.Models.Account
{
    public class Acc_Journal_Entry_Headers_DTO
    {
        public Acc_Journal_Entry_Headers_DTO() { }
        public Acc_Journal_Entry_Headers_DTO(DataRow dataRow) {
            JrnlH_pkeyId = Helper.GetBigIntRowData(dataRow, "JrnlH_pkeyId");
            JrnlH_Date =Helper.GetDateTimeRowData(dataRow,"JrnlH_Date");
            JrnlH_ReferenceNo =Helper.GetStringRowData(dataRow,"JrnlH_ReferenceNo");
            JrnlH_Memo = Helper.GetStringRowData(dataRow, "JrnlH_Memo");
            JrnlH_Posted = Helper.GetBoolRowData(dataRow, "JrnlH_Posted");
            JrnlH_GeneralLedgerHeaderId = Helper.GetBigIntRowData(dataRow, "JrnlH_GeneralLedgerHeaderId");
            UserID = Helper.GetBigIntRowData(dataRow, "UserID");
            JrnlH_JournalEntry = new List<Acc_Journal_Entry_Lines_DTO>();
            JrnlH_JournalEntry.Add(new Acc_Journal_Entry_Lines_DTO(dataRow));
        }
        public Int64 JrnlH_pkeyId { get; set; }
        public DateTime JrnlH_Date { get; set; }
        public string JrnlH_ReferenceNo { get; set; }
        public string JrnlH_Memo { get; set; }
        public bool JrnlH_Posted { get; set; }
        public bool JrnlH_ReadyForPosting { get; set; }
        public Int64? JrnlH_GeneralLedgerHeaderId { get; set; }
        public bool JrnlH_IsActive { get; set; }
        public bool JrnlH_IsDelete { get; set; }
        public Int64 JrnlH_CreatedBy { get; set; }
        public DateTime JrnlH_CreatedOn { get; set; }
        public Int64 JrnlH_ModifiedBy { get; set; }
        public DateTime JrnlH_ModifiedOn { get; set; }
        public Int64 JrnlH_DeletedBy { get; set; }
        public DateTime JrnlH_DeletedOn { get; set; }
        public List<Acc_Journal_Entry_Lines_DTO> JrnlH_JournalEntry { get; set; }
        public decimal DebitAmount { get; set; }
        public decimal CreditAmount { get; set; }
        public string WhereClause { get; set; }
        public int Type { get; set; }

        public string JrnlH_CreatedByName { get; set; }
        public string JrnlH_ModifiedByName { get; set; }
        public Int64? UserID
        {
            get; set;
        }
    }
}