﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models.Account
{
    public class Acc_Report_Filter
    {
        public DateTime? StartDate
        {
            get; set;
        }
        public DateTime? EndDate
        {
            get; set;
        }
        public Reports ReportsType
        {
            get; set;
        }
        public long? UserId
        {
            get; set;
        }
        public int Type
        {
            get; set;
        }
    }
}