﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models.Account
{
    public class Acc_ProfitAndLoss_By_CustomerDTO
    {
        public List<SelectCustomerItem> CustomerList
        {
            get; set;
        }
        public List<Income> Income
        {
            get; set;
        }
        public List<Income> OtherIncome
        {
            get; set;
        }

        public decimal TotalIncome
        {
            get; set;
        } = 0;
        public List<decimal> CustomerByWiseTotalIncome
        {
            get; set;
        }
        public decimal TotalOtherIncome
        {
            get; set;
        } = 0;
        public List<decimal> CustomerByWiseTotalOtherIncome
        {
            get; set;
        }
        public decimal TotalSales
        {
            get; set;
        } = 0;
        public List<decimal> CustomerByWiseTotalSales
        {
            get; set;
        }
        public decimal GrossProfit
        {
            get; set;
        } = 0;
        public decimal Profit
        {
            get; set;
        } = 0;
        public List<decimal> CustomerByWiseProfit
        {
            get; set;
        }
        public List<Expense> Expenses
        {
            get; set;
        }
        public decimal TotalExpenses
        {
            get; set;
        } = 0;
        public List<decimal> CustomerByWiseTotalExpenses
        {
            get; set;
        } 
    }
    public class SelectCustomerItem
    {
        public long InvoiceId
        {
            get; set;
        }
        public long? CustomerId
        {
            get; set;
        }
        public string Name
        {
            get; set;
        }
    }
    
}