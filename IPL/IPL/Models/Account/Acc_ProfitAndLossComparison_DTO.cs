﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models.Account
{
    public class Acc_ProfitAndLossComparison_DTO
    {
        public List<Income> Income
        {
            get; set;
        }
        public decimal TotalIncome
        {
            get; set;
        } = 0;
        public decimal TotalOldIncome
        {
            get; set;
        } = 0;
        public decimal TotalSales
        {
            get; set;
        } = 0;
        public decimal TotalOldSales
        {
            get; set;
        } = 0;
        public decimal GrossProfit
        {
            get; set;
        } = 0;
        public decimal GrossOldProfit
        {
            get; set;
        } = 0;
        public decimal Profit
        {
            get; set;
        } = 0;
        public decimal OldProfit
        {
            get; set;
        } = 0;
        public List<Expense> Expenses
        {
            get; set;
        }
        public decimal TotalExpenses
        {
            get; set;
        } = 0;
        public decimal TotalOldExpenses
        {
            get; set;
        } = 0;
    }
}