﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Models.Account
{
    public class Acc_Ledger_Lines_DTO
    {
        public Acc_Ledger_Lines_DTO()
        {
        }
        public Acc_Ledger_Lines_DTO(DataRow dataRow)
        {
            LedgE_pkeyId = Helper.GetBigIntRowData(dataRow, "LedgE_pkeyId");
            LedgE_DrCr = Helper.GetIntRowData(dataRow, "LedgE_DrCr");
            LedgE_Amount = Helper.GetDecimalRowData(dataRow, "LedgE_Amount");
            LedgE_AccountId = Helper.GetBigIntRowData(dataRow, "LedgE_AccountId");
            LedgE_LedgerHeaderId = Helper.GetBigIntRowData(dataRow, "LedgE_LedgerHeaderId");
            UserID = Helper.GetBigIntRowData(dataRow, "UserID");
        }
        public Int64 LedgE_pkeyId { get; set; }
        public int LedgE_DrCr { get; set; }
        public decimal LedgE_Amount { get; set; }
        public Int64 LedgE_AccountId { get; set; }
        public Int64 LedgE_LedgerHeaderId { get; set; }

        public bool LedgE_IsActive { get; set; }
        public bool LedgE_IsDelete { get; set; }
        public Int64 LedgE_CreatedBy { get; set; }
        public DateTime LedgE_CreatedOn { get; set; }
        public Int64 LedgE_ModifiedBy { get; set; }
        public DateTime LedgE_ModifiedOn { get; set; }
        public Int64 LedgE_DeletedBy { get; set; }
        public DateTime LedgE_DeletedOn { get; set; }

        public string WhereClause { get; set; }
        public int Type { get; set; }
        public Int64? UserID
        {
            get; set;
        }

    }
}