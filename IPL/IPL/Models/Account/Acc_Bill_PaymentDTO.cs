﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Models.Account
{
    public class Acc_Bill_Payment_DTO
    {
        public Acc_Bill_Payment_DTO()
        {
        }
        public Acc_Bill_Payment_DTO(DataRow dataRow)
        {
            Bill_Pay_PkeyId = Helper.GetBigIntRowData(dataRow, "Bill_Pay_PkeyId");
            Bill_Pay_Bill_Id = Helper.GetBigIntRowData(dataRow, "Bill_Pay_Bill_Id");
            Bill_Pay_Vendor_Id = Helper.GetBigIntRowData(dataRow, "Bill_Pay_Vendor_Id");
            Bill_Pay_Wo_Id = Helper.GetBigIntRowData(dataRow, "Bill_Pay_Wo_Id");
            Bill_Pay_Payment_Date = Helper.GetDateTimeRowData(dataRow, "Bill_Pay_Payment_Date");
            Bill_Pay_Amount = Helper.GetDecimalRowData(dataRow, "Bill_Pay_Amount");
            Bill_Pay_CheckNumber = Helper.GetStringRowData(dataRow, "Bill_Pay_CheckNumber");
            Bill_Pay_Comment = Helper.GetStringRowData(dataRow, "Bill_Pay_Comment");
            Bill_Pay_Balance_Due = Helper.GetDecimalRowData(dataRow, "Bill_Pay_Balance_Due");
            Bill_Pay_EnteredBy = Helper.GetBigIntRowData(dataRow, "Bill_Pay_EnteredBy");
            Bill_Pay_EnteredBy_Name = Helper.GetStringRowData(dataRow, "Bill_Pay_EnteredBy_Name");
        }
        public Int64 Bill_Pay_PkeyId
        {
            get; set;
        }
        public Int64 Bill_Pay_Bill_Id
        {
            get; set;
        }
        public Int64 Bill_Pay_Vendor_Id
        {
            get; set;
        }
        public Int64 Bill_Pay_Wo_Id
        {
            get; set;
        }
       
        public DateTime? Bill_Pay_Payment_Date
        {
            get; set;
        }
     
        public decimal? Bill_Pay_Amount
        {
            get; set;
        }
        public string Bill_Pay_CheckNumber
        {
            get; set;
        }
      
        public decimal? Bill_Pay_Balance_Due
        {
            get; set;
        }
        public String Bill_Pay_Comment
        {
            get; set;
        }
        public Int64 Bill_Pay_EnteredBy
        {
            get; set;
        }
        public string Bill_Pay_EnteredBy_Name
        {
            get; set;
        }
        public Int64? UserID
        {
            get; set;
        }

        public bool? Bill_IsActive
        {
            get; set;
        }
        public bool? Bill_IsDelete
        {
            get; set;
        }
        public long Bill_CreatedBy
        {
            get; set;
        }
        public DateTime Bill_CreatedOn
        {
            get; set;
        }
        public long? Bill_ModifiedBy
        {
            get; set;
        }
        public DateTime? Bill_ModifiedOn
        {
            get; set;
        }
        public long? Bill_DeletedBy
        {
            get; set;
        }
        public DateTime? Bill_DeletedOn
        {
            get; set;
        }
        public int Type
        {
            get; set;
        }
        public String WhereClause
        {
            get; set;
        }
      
        public Int64? Inv_Client_pkeyId
        {
            get;set;
        }
        public Int64? Inv_Client_WO_Id
        {
            get;set;
        }
    }
}