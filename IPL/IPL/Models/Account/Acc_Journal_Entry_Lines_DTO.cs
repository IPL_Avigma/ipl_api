﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Models.Account
{
    public class Acc_Journal_Entry_Lines_DTO
    {
        public Acc_Journal_Entry_Lines_DTO()
        {

        }
    public Acc_Journal_Entry_Lines_DTO(DataRow dataRow)
        {
            JrnlE_pkeyId =Helper.GetBigIntRowData(dataRow,"JrnlE_pkeyId");
            JrnlE_AccountId =Helper.GetBigIntRowData(dataRow,"JrnlE_AccountId");
            JrnlE__Amount =Helper.GetDecimalRowData(dataRow,"JrnlE__Amount");
            JrnlE_DrCr =Helper.GetIntRowData(dataRow,"JrnlE_DrCr");
            JrnlE_Memo =Helper.GetStringRowData(dataRow,"JrnlE_Memo");
            JrnlE_Name = Helper.GetStringRowData(dataRow, "JrnlE_Name");
            JrnlE_Class = Helper.GetStringRowData(dataRow, "JrnlE_Class");
            JrnlE_AccountType = Helper.GetIntRowData(dataRow, "Acc_Account_Type");
            UserID = Helper.GetBigIntRowData(dataRow, "UserID");
            AccountName = Helper.GetStringRowData(dataRow, "Acc_Account_Name");
            Acc_CreatedBy = Helper.GetStringRowData(dataRow, "Acc_CreatedBy");
            Acc_ModifiedBy = Helper.GetStringRowData(dataRow, "Acc_ModifiedBy");
        }
public Int64 JrnlE_pkeyId { get; set; }
        public int JrnlE_DrCr { get; set; }
        public decimal JrnlE__Amount { get; set; }
        public string JrnlE_Memo { get; set; }
        public string JrnlE_Name{ get; set; }
        public string JrnlE_Class { get; set; }
        public Int64 JrnlE_AccountId { get; set; }
        public int JrnlE_AccountType { get; set; }
        public Int64 JrnlE_JournalEntryHeaderId { get; set; }


        public bool JrnlE_IsActive { get; set; }
        public bool JrnlE_IsDelete { get; set; }
        public Int64 JrnlE_CreatedBy { get; set; }
        public DateTime JrnlE_CreatedOn { get; set; }
        public Int64 JrnlE_ModifiedBy { get; set; }
        public DateTime JrnlE_ModifiedOn { get; set; }
        public Int64 JrnlE_DeletedBy { get; set; }
        public DateTime JrnlE_DeletedOn { get; set; }
        public string WhereClause { get; set; }
        public int Type { get; set; }
        public Int64? UserID
        {
            get; set;
        }
        public string AccountName
        {
            get; set;
        }
        public String Acc_CreatedBy { get; set; }
        public String Acc_ModifiedBy { get; set; }
    }
}