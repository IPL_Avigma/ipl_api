﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models.Account
{

    public class Acc_AccountMaster_DTO
    {
        public Int64 Acc_pkeyId { get; set; }
        public string Acc_Account_Name { get; set; }
        public string Acc_Account_Code { get; set; }
        public int Acc_DrOrCr_Side { get; set; }
        public int? Acc_Account_Type { get; set; }

        public decimal Balance
        {
            get; set;
        }
        public decimal DebitBalance
        {
            get; set;
        }
        public decimal CreditBalance
        {
            get; set;
        }
        public Int64? Acc_Account_Details { get; set; }
        public string Acc_Account_Type_Name { get; set; }
        public Int64? Acc_Parent_Account_Id
        { get; set; }
        public string Acc_Account_Type_Detail_Name
        {
            get; set;
        }
        public string Acc_Account_Description
        {
            get; set;
        }
        public bool Acc_IsActive { get; set; }
        public bool Acc_IsParent { get; set; }
        public bool Acc_IsDelete { get; set; }
        public Int64 Acc_CreatedBy { get; set; }
        public DateTime Acc_CreatedOn { get; set; }
        public Int64 Acc_ModifiedBy { get; set; }
        public DateTime Acc_ModifiedOn { get; set; }
        public Int64 Acc_DeletedBy { get; set; }
        public DateTime Acc_DeletedOn { get; set; }
        public Int64? UserID
        {
            get; set;
        }
        public string WhereClause { get; set; }
        public List<Acc_AccountMaster_DTO> Children
        { get; set; }
        public int Type { get; set; }
    }
}