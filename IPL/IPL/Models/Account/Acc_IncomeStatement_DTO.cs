﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models.Account
{
    public class Acc_IncomeStatement_DTO
    {
        public long AccountId { get; set; }
        public bool IsExpense { get; set; }
        public string AccountCode { get; set; }
        public string AccountName { get; set; }
        public decimal Amount { get; set; }
        public DateTime? CreatedDate
        {
            get; set;
        }
    }
}