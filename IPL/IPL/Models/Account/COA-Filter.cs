﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models.Account
{
    public class COA_Filter
    {
        public int Acc_pkeyId
        {
            get; set;
        }
        public DateTime? StartDate
        {
            get; set;
        }
        public DateTime? EndDate
        {
            get; set;
        }
    }
}