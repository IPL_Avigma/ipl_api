﻿using IPLApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models.Account
{
    public class Acc_Client
    {
        public Int64 Acc_Client_pkeyId
        {
            get; set;
        }
        public String Company_Name
        {
            get; set;
        }
        public Decimal? Discount
        {
            get; set;
        }
        public Decimal? Contractor_Discount
        {
            get; set;
        }
        public String Billing_Address
        {
            get; set;
        }
        public String City
        {
            get; set;
        }
        public String IPL_StateName
        {
            get; set;
        }
        public int? StateId
        {
            get; set;
        }
        public Int64? ZipCode
        {
            get; set;
        }
        public int? DateTimeOverlay
        {
            get; set;
        }
        public String BackgroundProvider
        {
            get; set;
        }
        public String ContactType
        {
            get; set;
        }
        public String ContactName
        {
            get; set;
        }
        public String ContactEmail
        {
            get; set;
        }
        public String ContactPhone
        {
            get; set;
        }

        public String Comments
        {
            get; set;
        }
        public Int64? Due_Date_Offset
        {
            get; set;
        }
        public Int64? Photo_Resize_width
        {
            get; set;
        }
        public Int64? Photo_Resize_height
        {
            get; set;
        }
        public Decimal? Invoice_Total
        {
            get; set;
        } // change to bool
        public Boolean? Login
        {
            get; set;
        }
        public String Login_Id
        {
            get; set;
        }
        public String Password
        {
            get; set;
        }
        public String Rep_Id
        {
            get; set;
        }
        public Boolean? Lock_Order
        {
            get; set;
        }
        public String IPL_Mobile
        {
            get; set;
        }
        public int? Provider
        {
            get; set;
        }
        public int? Active
        {
            get; set;
        }

        public String ClientPhone
        {
            get; set;
        }
        public String FaxNumbar
        {
            get; set;
        }

        public Boolean? IsActive
        {
            get; set;
        }
        public Boolean? IsDelete
        {
            get; set;
        }
        public Int64 UserID
        {
            get; set;
        }
        public int Type
        {
            get; set;
        }
        public String ErrorMessage
        {
            get; set;
        }
        public String Title
        {
            get; set;
        }
        public Boolean? PrintAsCheck
        {
            get; set;
        }
        public String Country
        {
            get; set;
        }
        public String Other
        {
            get; set;
        }
        public String Website
        {
            get; set;
        }
        public Int64? Client_pkeyID
        {
            get; set;
        }
        public String User_CreatedBy
        {
            get; set;
        }
        public String User_ModifiedBy
        {
            get; set;
        }
    }
    public class ClientDTO
    {
        public Int64 Acc_Client_pkeyId
        {
            get; set;
        }
        public Int64? Client_StateId
        {
            get; set;
        }
        public String Client_Company_Name
        {
            get; set;
        }
        public String Client_Billing_Address
        {
            get; set;
        }
        public String Client_City
        {
            get; set;
        }
        public String IPL_StateName
        {
            get; set;
        }
        public Int64? Client_ZipCode
        {
            get; set;
        }
        public String Client_ContactName
        {
            get; set;
        }
        public Boolean? Client_IsActive
        {
            get; set;
        }
        public String WhereClause
        {
            get; set;
        }

        public Boolean? Client_IsDelete
        {
            get; set;
        }
        public Int64? UserId
        {
            get; set;
        }
        public int Type
        {
            get; set;
        }
        public String FilterData
        {
            get; set;
        }
        public Int64? MenuId
        {
            get; set;
        }
        public SearchMasterDTO SearchMaster
        {
            get; set;
        }

        public string Client_ContactEmail
        {
            get; set;
        }
        public string Client_ContactPhone
        {
            get; set;
        }



    }
}