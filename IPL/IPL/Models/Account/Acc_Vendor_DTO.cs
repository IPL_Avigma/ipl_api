﻿using IPLApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models.Account
{
    public class Acc_Vendor_DTO
    {
        public Int64 Acc_Vendor_pkeyId
        {
            get; set;
        }
        public Int64? User_pkeyID
        {
            get; set;
        }
        public String Title
        {
            get; set;
        }  public String First_Name
        {
            get; set;
        }
        public String Middle_Name
        {
            get; set;
        }
        public String Last_Name
        {
            get; set;
        }
        public String Company_Name
        {
            get; set;
        }
        public String Address
        {
            get; set;
        }
        public string City
        {
            get; set;
        }
        public int? StateId
        {
            get; set;
        }
        public Int64? ZipCode
        {
            get; set;
        }
        public string Country
        {
            get; set;
        }
        public string Email
        {
            get; set;
        }
        public String Phone
        {
            get; set;
        }
        public String Mobile
        {
            get; set;
        }
        public String FaxNumber
        {
            get; set;
        }
        public bool? IsActive
        {
            get; set;
        }
        public bool? PrintOnCheck
        {
            get; set;
        }
        public bool? IsDelete
        {
            get; set;
        }
        public long? Invoice_DeletedBy
        {
            get; set;
        }
        public String Website
        {
            get; set;
        }
        public Int64 UserID
        {
            get; set;
        }
        public int Type
        {
            get; set;
        }
        public String WhereClause
        {
            get; set;
        }

        public String BusinessID_Social_No
        {
            get; set;
        }
        public Boolean? Track_Payment
        {
            get; set;
        }
        public String Default_Expence_Account_Id
        {
            get; set;
        }
        public int User_Source
        {
            get; set;
        }

        public String CreatedBy
        {
            get; set;
        }

        public String ModifiedBy
        {
            get; set;
        }
    }
  

   
}