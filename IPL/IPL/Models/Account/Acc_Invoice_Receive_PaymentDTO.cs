﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

//changes 
namespace IPL.Models.Account
{
    public class Acc_Invoice_Receive_Payment_DTO
    {
        public Acc_Invoice_Receive_Payment_DTO()
        {
        }
        public Acc_Invoice_Receive_Payment_DTO(DataRow dataRow)
        {
            Invoice_Rec_PkeyId = Helper.GetBigIntRowData(dataRow, "Invoice_Rec_PkeyId");
            Invoice_Rec_Customer_Id = Helper.GetBigIntRowData(dataRow, "Invoice_Rec_Customer_Id");
            Invoice_Rec_Payment_Date = Helper.GetDateTimeRowData(dataRow, "Invoice_Rec_Payment_Date");
            Invoice_Rec_Payment_Method = Helper.GetIntRowData(dataRow, "Invoice_Rec_Payment_Method");
            Invoice_Rec_Reference_No = Helper.GetStringRowData(dataRow, "Invoice_Rec_Reference_No");
            Invoice_Rec_Memo = Helper.GetStringRowData(dataRow, "Invoice_Rec_Memo");
            Invoice_Rec_Deposit_To = Helper.GetBigIntRowData(dataRow, "Invoice_Rec_Deposit_To");
            Invoice_Rec_Amount = Helper.GetDecimalRowData(dataRow, "Invoice_Rec_Amount");
        }
        public Int64 Invoice_Rec_PkeyId
        {
            get; set;
        }
        public Int64 Invoice_pkeyId
        {
            get; set;
        }
        public string Invoice_Number
        {
            get; set;
        }
        public Int64 Invoice_Rec_Customer_Id
        {
            get; set;
        }
       
        public DateTime? Invoice_Rec_Payment_Date
        {
            get; set;
        }
     
        public int? Invoice_Rec_Payment_Method
        {
            get; set;
        }
        public string Invoice_Rec_Reference_No
        {
            get; set;
        }
      
        public long? Invoice_Rec_Deposit_To
        {
            get; set;
        }
        public String Invoice_Rec_Memo
        {
            get; set;
        }
        public decimal? Invoice_Rec_Amount
        {
            get; set;
        }
        public Int64? UserID
        {
            get; set;
        }

        public int Type
        {
            get; set;
        }
        public String WhereClause
        {
            get; set;
        }
      
        public List<Receive_Invoice_Item> Receive_Invoice_Items
        {
            get;
            set;
        }
    }
    public class Receive_Invoice_Item
    {
        public long? Invoice_Rec_PkeyId
        {
            get; set;
        }
        public long? Invoice_Rec_Invoice_Id
        {
            get; set;
        }
        public Int64? Invoice_CustomeId
        {
            get; set;
        }
        public long? Invoice_Rec_Deposit_To
        {
            get; set;
        }
       
        public string Invoice_Number
        {
            get; set;
        }
        public DateTime? DueDate
        {
            get; set;
        }
        public DateTime? PaymentDate
        {
            get; set;
        }
        public decimal? Invoice_Rec_Original_Amount
        {
            get; set;
        }
        public decimal? Invoice_Rec_Payment
        {
            get; set;
        }
        public decimal? Invoice_Total
        {
            get; set;
        }

        public decimal? Pending_Amount
        {
            get; set;
        }
        public int Invoice_Status
        {
            get; set;
        }
       
    }
    public class Final_Invoice_Receive_Payment_DTO
    {
        public long Invoice_Rec_PkeyId
        {
            get; set;
        }
        public long? Invoice_Rec_Customer_Id
        {
            get; set;
        }
        public long? Invoice_Rec_Invoice_Id
        {
            get; set;
        }
        public DateTime? Invoice_Rec_Payment_Date
        {
            get; set;
        }
        public decimal? Invoice_Rec_Original_Amount
        {
            get; set;
        }
        public decimal? Invoice_Rec_Payment
        {
            get; set;
        }
        public decimal? Invoice_Rec_Pending_Amount
        {
            get; set;
        }
        public int? Invoice_Rec_Payment_Method
        {
            get; set;
        }
        public string Invoice_Rec_Reference_No
        {
            get; set;
        }
        public long? Invoice_Rec_Deposit_To
        {
            get; set;
        }
        public string Invoice_Rec_Memo
        {
            get; set;
        }
        public long? Invoice_Rec_EnteredBy
        {
            get; set;
        }
        public bool? Invoice_Rec_IsActive
        {
            get; set;
        }
        public bool? Invoice_Rec_IsDelete
        {
            get; set;
        }
        public long Invoice_Rec_CreatedBy
        {
            get; set;
        }
        public DateTime Invoice_Rec_CreatedOn
        {
            get; set;
        }
        public long? Invoice_Rec_ModifiedBy
        {
            get; set;
        }
        public DateTime? Invoice_Rec_ModifiedOn
        {
            get; set;
        }
        public long? Invoice_Rec_DeletedBy
        {
            get; set;
        }
        public DateTime? Invoice_Rec_DeletedOn
        {
            get; set;
        }
        public int Type
        {
            get; set;
        }
        public String WhereClause
        {
            get; set;
        }
        public long? UserID
        {
            get; set;
        }
    }
}