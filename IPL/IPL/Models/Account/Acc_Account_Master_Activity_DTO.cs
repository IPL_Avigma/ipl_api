﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models.Account
{
    public class Acc_Account_Master_Activity_DTO
    {
        public Int64 Acc_pkeyId
        {
            get; set;
        }
        public long? Custome_Id
        {
            get; set;
        }
        public string Invoice_Number
        {
            get; set;
        }
        public long? Journal_Number
        {
            get; set;
        }
        public long? Invoice_Id
        {
            get; set;
        }
        public DateTime? Payment_Date
        {
            get; set;
        }
        public decimal? Amount
        {
            get; set;
        }
        public string Reference_No
        {
            get; set;
        }
        public string Memo
        {
            get; set;
        }
        public string Customer_Name
        {
            get; set;
        }
        public string Account_Name
        {
            get; set;
        }
        public long UserID
        {
            get; set;
        }
        public int Type
        {
            get; set;
        }
        public DateTime  CreatedDate
        {
            get; set;
        }
    }
}