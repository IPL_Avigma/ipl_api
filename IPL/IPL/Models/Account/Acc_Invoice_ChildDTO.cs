﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Models.Account
{
    public class Acc_Invoice_ChildDTO
    {

        public Acc_Invoice_ChildDTO()
        {

        }
        public Acc_Invoice_ChildDTO(DataRow dataRow)
        {
            Invoice_Items_PkeyId = Helper.GetBigIntRowData(dataRow, "Invoice_Items_PkeyId");
            Invoice_Id = Helper.GetBigIntRowData(dataRow, "Invoice_Id");
            Task_Id = Helper.GetBigIntRowData(dataRow, "Task_Id");
            QTY = Helper.GetStringRowData(dataRow, "QTY");
            Rate = Helper.GetDecimalRowData(dataRow, "Rate");
            Tax = Helper.GetBoolRowData(dataRow, "Tax");
            Amount = Helper.GetDecimalRowData(dataRow, "Amount");
            Descp = Helper.GetStringRowData(dataRow, "Descp");
            Class = Helper.GetStringRowData(dataRow, "Class");
            Type = Helper.GetIntRowData(dataRow, "Type");
            UserID = Helper.GetBigIntRowData(dataRow, "UserID");
        }

        public Int64? Invoice_Items_PkeyId
        {
            get; set;
        }
        public Int64? Invoice_Id
        {
            get; set;
        }
        public Int64? Task_Id
        {
            get; set;
        }
        public string QTY
        {
            get; set;
        }
        public decimal? Rate
        {
            get; set;
        }
        public decimal? Amount
        {
            get; set;
        }
        public Boolean? Tax
        {
            get; set;
        }
       
        public String Descp
        {
            get; set;
        }
        public String Class
        {
            get; set;
        }
        public int Type
        {
            get; set;
        }
        public Int64? Inv_Client_Ch_pkeyId
        {
            get; set;
        }
        public Int64? Inv_Client_Ch_Wo_Id
        {
            get; set;
        }
        public Int64? Inv_Client_pkeyId
        {
            get; set;
        }
        public Int64? UserID { get; set; }
    }
}