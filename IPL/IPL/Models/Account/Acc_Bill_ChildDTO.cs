﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Models.Account
{
    public class Acc_Bill_ChildDTO
    {

        public Acc_Bill_ChildDTO()
        {

        }
        public Acc_Bill_ChildDTO(DataRow dataRow)
        {
            Bill_Items_PkeyId = Helper.GetBigIntRowData(dataRow, "Bill_Items_PkeyId");
            Bill_Id = Helper.GetBigIntRowData(dataRow, "Bill_Id");
            Task_Id = Helper.GetBigIntRowData(dataRow, "Task_Id");
            QTY = Helper.GetStringRowData(dataRow, "QTY");
            Rate = Helper.GetDecimalRowData(dataRow, "Rate");
            Tax = Helper.GetBoolRowData(dataRow, "Tax");
            Amount = Helper.GetDecimalRowData(dataRow, "Amount");
            Descp = Helper.GetStringRowData(dataRow, "Descp");
            Class = Helper.GetStringRowData(dataRow, "Class");
            Type = Helper.GetIntRowData(dataRow, "Type");
        }

        public Int64? Bill_Items_PkeyId
        {
            get; set;
        }
        public Int64? Bill_Id
        {
            get; set;
        }
        public Int64? Task_Id
        {
            get; set;
        }
        public string QTY
        {
            get; set;
        }
        public decimal? Rate
        {
            get; set;
        }
        public decimal? Amount
        {
            get; set;
        }
        public Boolean? Tax
        {
            get; set;
        }
       
        public String Descp
        {
            get; set;
        }
        public String Class
        {
            get; set;
        }
        public int Type
        {
            get; set;
        }
        public Int64 UserID
        {
            get; set;
        }
        public Int64? Bill_Con_Ch_pkeyId
        {
            get; set;
        }
        public Int64? Bill_Con_Ch_Wo_Id
        {
            get; set;
        }
        public Int64? Bill_Con_pkeyId
        {
            get; set;
        }
    }
}