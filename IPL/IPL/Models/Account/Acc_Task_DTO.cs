﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models.Account
{
    public class Acc_Task_DTO
    {
        public Int64 Acc_Task_pkeyId
        {
            get; set;
        }
        public String Task_Name
        {
            get; set;
        }
        public String WhereClause
        {
            get; set;
        }
        public String FilterData
        {
            get; set;
        }
        public int? Task_Type
        {
            get; set;
        }
      
        public String Task_Photo_Label_Name
        {
            get; set;
        }
        public int? Task_Group
        {
            get; set;
        }
        
        public int? Task_UOM
        {
            get; set;
        }
        public Decimal? Task_Contractor_UnitPrice
        {
            get; set;
        }
        public Decimal? Task_Client_UnitPrice
        {
            get; set;
        }
        public bool? Task_IsActive
        {
            get; set;
        }
        public bool? Task_IsDelete
        {
            get; set;
        }
        public Int64? UserID
        {
            get; set;
        }
        public int Type
        {
            get; set;
        }

        public string Task_TypeName
        {
            get; set;
        }
        public string Task_Group_Name
        {
            get; set;
        }

        public bool? Task_AutoInvoiceComplete
        {
            get; set;
        }
        public Int64 WorkOrderID
        {
            get; set;
        }
        public int Task_System
        {
            get; set;
        }
        public Int64 Task_pkeyID
        {
            get; set;
        }

        public String Task_CreatedBy
        {
            get; set;
        }
        public String Task_ModifiedBy
        {
            get; set;
        }
        public String Task_Work_Type_Group { get; set; }
    }
    public class Acc_TaskMaster
    {
        public String Acc_Task_pkeyID
        {
            get; set;
        }
        public String Status
        {
            get; set;
        }
        public String ErrorMessage
        {
            get; set;
        }
    }
}