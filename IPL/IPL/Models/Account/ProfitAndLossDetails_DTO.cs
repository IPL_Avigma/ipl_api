﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models.Account
{
    public class ProfitAndLossDetails_DTO
    {
        public OrdinaryIncomeExpenses OrdinaryIncomeExpenses
        {
            get; set;
        }
        public OtherIncomeExpenses OtherIncomeExpenses
        {
            get; set;
        }
        public decimal NetIncome
        {
            get; set;
        } = 0;
    }
    public class OrdinaryIncomeExpenses
    {
        public ProfitLossDetailsIncome ProfitLossDetailsIncome
        {
            get; set;
        }
        public decimal NetOrdinaryIncome
        {
            get; set;
        } = 0;
    }
    public class ProfitLossDetailsIncome
    {
        public List<DataList> InvocieBillDataList
        {
            get; set;
        }
        public decimal TotalforSales
        {
            get; set;
        }
        public List<DataList> Income
        {
            get; set;
        }
        public decimal TotalforIncome
        {
            get; set;
        }
        public List<DataList> ExpensesDataList
        {
            get; set;
        }
        public decimal TotalforExpenses
        {
            get; set;
        }
    }
    public class DataList
    {
        public DateTime CreatedDate
        {
            get; set;
        }
        public string TransactionType
        {
            get; set;
        }
        public string ExpensesTypeName
        {
            get; set;
        }
        public string NO
        {
            get; set;
        }
        public string Name
        {
            get; set;
        }
        public string Memo_Description
        {
            get; set;
        }
        public decimal Amount
        {
            get; set;
        }
        public decimal Balance
        {
            get; set;
        }
        public int DrOrCrSide
        {
            get; set;
        } = 0;
        public Boolean IsShow
        {
            get; set;
        } = true;
    }
    public class OtherIncomeExpenses
    {
        public List<DataList> OtherIncome
        {
            get; set;
        }
        public decimal TotalforOtherIncome
        {
            get; set;
        }
        public decimal NetOtherIncome
        {
            get; set;
        }
    }
}