﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Models.Account
{
    public class Acc_Invoice_Bank_Deposit_DTO
    {
        public Acc_Invoice_Bank_Deposit_DTO()
        {
        }
        public Acc_Invoice_Bank_Deposit_DTO(DataRow dataRow)
        {
            Deposit_To = Helper.GetBigIntRowData(dataRow, "Deposit_To");
            PaymentDate = Helper.GetDateTimeRowData(dataRow, "PaymentDate");
            Memo = Helper.GetStringRowData(dataRow, "Memo");
            UserID = Helper.GetBigIntRowData(dataRow, "UserID");
            Type = Helper.GetIntRowData(dataRow, "Type");
        }
        public long Deposit_To
        {
            get; set;
        }
        public DateTime? PaymentDate
        {
            get; set;
        }
        public string Memo
        {
            get; set;
        }
        public long UserID
        {
            get; set;
        }
        public int Type
        {
            get; set;
        }
        public List<Acc_Invoice_BankDeposit_Items> BankDeposit_Items
        {
            get;set;
        }
    }
    public class Acc_Invoice_BankDeposit_Items
    {
        public  long Invoice_Rec_PkeyId
        {
            get; set;
        }
        public long Invoice_Id
        {
            get; set;
        }
        public Int64 Invoice_CustomeId
        {
            get; set;
        }
        public DateTime? Payment_Date
        {
            get; set;
        }
        public decimal? Amount
        {
            get; set;
        }
        public decimal? Original_Amount
        {
            get; set;
        }
        public decimal? Payment_Amount
        {
            get; set;
        }
        public decimal? Pending_Amount
        {
            get; set;
        }
        public string Customer_Name
        {
            get; set;
        }
        public int? Payment_Method
        {
            get; set;

        }
        public string Ref_No
        {
            get; set;
        }
        public string Memo
        {
            get; set;
        }
    }
}