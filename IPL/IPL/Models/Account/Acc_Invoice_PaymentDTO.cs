﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

//changes 
namespace IPL.Models.Account
{
    public class Acc_Invoice_Payment_DTO
    {
        public Acc_Invoice_Payment_DTO()
        {
        }
        public Acc_Invoice_Payment_DTO(DataRow dataRow)
        {
            Invoice_Pay_PkeyId = Helper.GetBigIntRowData(dataRow, "Invoice_Pay_PkeyId");
            Invoice_Pay_Invoice_Id = Helper.GetBigIntRowData(dataRow, "Invoice_Pay_Invoice_Id");
            Invoice_Pay_Customer_Id = Helper.GetBigIntRowData(dataRow, "Invoice_Pay_Customer_Id");
            Invoice_Pay_Wo_Id = Helper.GetBigIntRowData(dataRow, "Invoice_Pay_Wo_Id");
            Invoice_Pay_Payment_Date = Helper.GetDateTimeRowData(dataRow, "Invoice_Pay_Payment_Date");
            Invoice_Pay_Amount = Helper.GetDecimalRowData(dataRow, "Invoice_Pay_Amount");
            Invoice_Pay_CheckNumber = Helper.GetStringRowData(dataRow, "Invoice_Pay_CheckNumber");
            Invoice_Pay_Comment = Helper.GetStringRowData(dataRow, "Invoice_Pay_Comment");
            Invoice_Pay_Balance_Due = Helper.GetDecimalRowData(dataRow, "Invoice_Pay_Balance_Due");
            Invoice_Pay_EnteredBy = Helper.GetBigIntRowData(dataRow, "Invoice_Pay_EnteredBy");
            Invoice_Pay_EnteredBy_Name = Helper.GetStringRowData(dataRow, "Invoice_Pay_EnteredBy_Name");
        }
        public Int64 Invoice_Pay_PkeyId
        {
            get; set;
        }
        public Int64 Invoice_Pay_Invoice_Id
        {
            get; set;
        }
        public Int64 Invoice_Pay_Customer_Id
        {
            get; set;
        }
        public Int64 Invoice_Pay_Wo_Id
        {
            get; set;
        }
       
        public DateTime? Invoice_Pay_Payment_Date
        {
            get; set;
        }
     
        public decimal? Invoice_Pay_Amount
        {
            get; set;
        }
        public string Invoice_Pay_CheckNumber
        {
            get; set;
        }
      
        public decimal? Invoice_Pay_Balance_Due
        {
            get; set;
        }
        public String Invoice_Pay_Comment
        {
            get; set;
        }
        public Int64 Invoice_Pay_EnteredBy
        {
            get; set;
        }
        public string Invoice_Pay_EnteredBy_Name
        {
            get; set;
        }
        public Int64? UserID
        {
            get; set;
        }

        public bool? Invoice_IsActive
        {
            get; set;
        }
        public bool? Invoice_IsDelete
        {
            get; set;
        }
        public long Invoice_CreatedBy
        {
            get; set;
        }
        public DateTime Invoice_CreatedOn
        {
            get; set;
        }
        public long? Invoice_ModifiedBy
        {
            get; set;
        }
        public DateTime? Invoice_ModifiedOn
        {
            get; set;
        }
        public long? Invoice_DeletedBy
        {
            get; set;
        }
        public DateTime? Invoice_DeletedOn
        {
            get; set;
        }
        public int Type
        {
            get; set;
        }
        public String WhereClause
        {
            get; set;
        }
      
        public Int64? Inv_Client_pkeyId
        {
            get;set;
        }
        public Int64? Inv_Client_WO_Id
        {
            get;set;
        }
    }
}