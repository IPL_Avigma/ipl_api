﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models.Account
{
    public class Acc_Dashboard_DTO
    {
        public Int64 AccountId
        {
            get; set;
        }
        public string AccountCode
        {
            get; set;
        }
        public string AccountName
        {
            get; set;
        }
        public decimal Amount
        {
            get; set;
        }
    }
    public class Acc_Dashboard_Input_DTO
    {
        public int ProfitLossType
        {
            get; set;
        }
        public DateTime? ProfitLossDate
        {
            get; set;
        }
        public Boolean IsProfitLossChanges
        {
            get; set;
        }
        public int ExpensesType
        {
            get; set;
        }
        public DateTime? ExpensesDate
        {
            get; set;
        }
        public Boolean IsExpensesTypeChanges
        {
            get; set;
        }
        public int IncomeType
        {
            get; set;
        }
        public DateTime? IncomeDate
        {
            get; set;
        }
        public Boolean IsIncomeTypeChanges
        {
            get; set;
        }
        public Int64? UserID
        {
            get; set;
        }
    }
    public class Acc_Dashboard_Details_DTO
    {

        public ProfitAndLoss ProfitAndLoss
        {
            get; set;
        }
        public Expenses Expenses
        {
            get; set;
        }

        public Invoice Invoice
        {
            get; set;
        }

    }
    public class ProfitAndLoss
    {
        public decimal? NetIncome
        {
            get; set;
        } = 0;
        public decimal? Income
        {
            get; set;
        } = 0;
        public decimal? Expenses
        {
            get; set;
        } = 0;
    }
    public class Expenses
    {
        public decimal? TotalExpenses
        {
            get; set;
        } = 0;
        public List<Acc_Dashboard_DTO> List
        {
            get; set;
        }
    }
    public class Invoice
    {
        public decimal? TotalInvoice
        {
            get; set;
        } = 0;
        public string TotalInvoicePercentage
        {
            get; set;
        } = 0 + "%";
        public decimal? DueInvoice
        {
            get; set;
        } = 0;
        public string DueInvoicePercentage
        {
            get; set;
        } = 0 + "%";
        public decimal? OverDueInvoice
        {
            get; set;
        } = 0;
        public string OverDueInvoicePercentage
        {
            get; set;
        } = 0 + "%";
        public string DueOverDueInvoicePercentage
        {
            get; set;
        } = 100 + "%";
        public decimal? PaidInvoice
        {
            get; set;
        } = 0;
        public string PaidInvoicePercentage
        {
            get; set;
        } = 0 + "%";
        public decimal? DepositInvoice
        {
            get; set;
        } = 0;
        public string DepositInvoicePercentage
        {
            get; set;
        } = 0 + "%";
    }
}