﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Models.Account
{
    public class Acc_Invoice_DTO
    {
        public Acc_Invoice_DTO()
        {
        }
        public Acc_Invoice_DTO(DataRow dataRow)
        {
            Invoice_pkeyId = Helper.GetBigIntRowData(dataRow, "Invoice_pkeyId");
            Invoice_Number = Helper.GetStringRowData(dataRow, "Invoice_Number");
            Invoice_CustomeId = Helper.GetBigIntRowData(dataRow, "Invoice_CustomeId");
            Invoice_Custome_Name = Helper.GetStringRowData(dataRow, "Invoice_Custome_Name");
            Invoice_Custome_Email = Helper.GetStringRowData(dataRow, "Invoice_Custome_Email");
            Invoice_Send_to = Helper.GetBoolRowData(dataRow, "Invoice_Send_to");
            Invoice_Send_to_DateTime = Helper.GetDateTimeRowData(dataRow, "Invoice_Send_to_DateTime");
            Invoice_CcBcc_Label = Helper.GetStringRowData(dataRow, "Inv_CcBcc_Label");
            Invoice_Billing_Address = Helper.GetStringRowData(dataRow, "Invoice_Billing_Address");
            Invoice_Terms = Helper.GetStringRowData(dataRow, "Invoice_Terms");
            Invoice_Date = Helper.GetDateTimeRowData(dataRow, "Invoice_Date");
            Invoice_Due_Date = Helper.GetDateTimeRowData(dataRow, "Invoice_Due_Date");
            Invoice_PO_No = Helper.GetStringRowData(dataRow, "Invoice_PO_No");
            Invoice_Message = Helper.GetStringRowData(dataRow, "Invoice_Message");
            Invoice_Statement = Helper.GetStringRowData(dataRow, "Invoice_Statement");
            Invoice_Sub_total = Helper.GetDecimalRowData(dataRow, "Invoice_Sub_total");
            Invoice_Taxble_Amount = Helper.GetDecimalRowData(dataRow, "Invoice_Taxble_Amount");
            Invoice_Taxble_Sub_total = Helper.GetStringRowData(dataRow, "Invoice_Taxble_Sub_total");
            Invoice_Balance_Due = Helper.GetDecimalRowData(dataRow, "Invoice_Balance_Due");
            Invoice_Total = Helper.GetDecimalRowData(dataRow, "Invoice_Total");
            UserID = Helper.GetBigIntRowData(dataRow, "UserID");
            Invoice_Status = Helper.GetIntRowData(dataRow, "Invoice_Status");
            Inv_Client_pkeyId = Helper.GetBigIntRowData(dataRow, "Inv_Client_pkeyId");
            Inv_Client_WO_Id = Helper.GetBigIntRowData(dataRow, "Inv_Client_WO_Id");
            Invoice_IsDelete = Helper.GetBoolRowData(dataRow, "Invoice_IsDelete");
            Invoice_IsActive = Helper.GetBoolRowData(dataRow, "Invoice_IsActive");
            Inv_Client_WO_Id = Helper.GetBigIntRowData(dataRow, "Inv_Client_WO_Id");
            Invoice_Items = new List<Acc_Invoice_ChildDTO>();
            Invoice_Items.Add(new Acc_Invoice_ChildDTO(dataRow));
            Invoice_Payments = new List<Acc_Invoice_Payment_DTO>();
            Invoice_Payments.Add(new Acc_Invoice_Payment_DTO(dataRow));
        }
        public Int64 Invoice_pkeyId
        {
            get; set;
        }
        public String Invoice_Number
        {
            get; set;
        }
        public Int64? Invoice_CustomeId
        {
            get; set;
        }
        public string Invoice_Custome_Name
        {
            get; set;
        }
        public String Invoice_Custome_Email
        {
            get; set;
        }
        public Boolean? Invoice_Send_to
        {
            get; set;
        }
        public DateTime? Invoice_Send_to_DateTime
        {
            get; set;
        }
        public string Invoice_CcBcc_Label
        {
            get; set;
        }
        public String Invoice_Billing_Address
        {
            get; set;
        }
        public string Invoice_Terms
        {
            get; set;
        }

        public DateTime? Invoice_Date
        {
            get; set;
        } = DateTime.Now;
        public DateTime? Invoice_Due_Date
        {
            get; set;
        }
        public String Invoice_PO_No
        {
            get; set;
        }public String Invoice_Message
        {
            get; set;
        }
        public String Invoice_Statement
        {
            get; set;
        }
        public decimal? Invoice_Sub_total
        {
            get; set;
        } = 0;
        public decimal? Invoice_Taxble_Amount
        {
            get; set;
        } = 0;
        public String Invoice_Taxble_Sub_total
        {
            get; set;
        }
        public decimal? Invoice_Total
        {
            get; set;
        } = 0;
        public decimal? Invoice_Balance_Due
        {
            get; set;
        } = 0;
        public Int64? UserID
        {
            get; set;
        }

        public bool? Invoice_IsActive
        {
            get; set;
        }
        public bool? Invoice_IsDelete
        {
            get; set;
        } = false;
        public long Invoice_CreatedBy
        {
            get; set;
        }
        public DateTime? Invoice_CreatedOn
        {
            get; set;
        }
        public long? Invoice_ModifiedBy
        {
            get; set;
        }
        public DateTime? Invoice_ModifiedOn
        {
            get; set;
        }
        public long? Invoice_DeletedBy
        {
            get; set;
        }
        public DateTime? Invoice_DeletedOn
        {
            get; set;
        }
        public int Type
        {
            get; set;
        }
        public String WhereClause
        {
            get; set;
        }
        public int? Invoice_Status
        {
            get; set;
            //Invoice_Status ==0 when create new invoice default value 0 
            //Invoice_Status ==1 paid invoice but not deposit any account
            //Invoice_Status ==2 deposit invoice selected account.
        }
        public List<Acc_Invoice_ChildDTO> Invoice_Items
        {
            get; set;
        }
        public List<Acc_Invoice_Payment_DTO> Invoice_Payments
        {
            get; set;
        }
        public Int64? Inv_Client_pkeyId
        {
            get;set;
        }
        public Int64? Inv_Client_WO_Id
        {
            get;set;
        } 
        public List<Receive_Invoice_Item> Receive_Invoice_Items
        {
            get;set;
        }

        public String Inv_CreatedBy
        {
            get; set;
        }

        public String Inv_ModifiedBy
        {
            get; set;
        }
    }
}