﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models.Account
{
    public class Acc_NodeData
    {
        public AccountData Data { get; set; }
        public List<Acc_NodeData> Children { get; set; }

        public Acc_NodeData()
        {
            Data = new AccountData();
            Children = new List<Acc_NodeData>();
        }
    }

    public class AccountData
    {
        public long Id { get; set; }
        public int AccountType { get; set; }
        public long? ParentAccountId { get; set; }
        public string AccountCode { get; set; }
        public string AccountName { get; set; }
        public decimal Balance { get; set; }
        public decimal DebitBalance { get; set; }
        public decimal CreditBalance { get; set; }
        public Int64? AccountDetailsId{  get; set;}
        public string AccountTypeName { get; set;}
        public string AccountTypeDetailName { get; set;}

    }
}