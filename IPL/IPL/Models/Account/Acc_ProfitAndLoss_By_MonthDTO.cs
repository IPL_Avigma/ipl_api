﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models.Account
{
    public class Acc_ProfitAndLoss_By_MonthDTO
    {
        public List<string> SelectedMonthList
        {
            get; set;
        }
        public List<Income> Income
        {
            get; set;
        }
        public List<Income> OtherIncome
        {
            get; set;
        }

        public decimal TotalIncome
        {
            get; set;
        } = 0;
        public List<decimal> MonthByWiseTotalIncome
        {
            get; set;
        }
        public decimal TotalOtherIncome
        {
            get; set;
        } = 0;
        public List<decimal> MonthByWiseTotalOtherIncome
        {
            get; set;
        }
        public decimal TotalSales
        {
            get; set;
        } = 0;
        public List<decimal> MonthByWiseTotalSales
        {
            get; set;
        }
        public decimal GrossProfit
        {
            get; set;
        } = 0;
        public decimal Profit
        {
            get; set;
        } = 0;
        public List<decimal> MonthByWiseProfit
        {
            get; set;
        }
        public List<Expense> Expenses
        {
            get; set;
        }
        public decimal TotalExpenses
        {
            get; set;
        } = 0;
        public List<decimal> MonthByWiseTotalExpenses
        {
            get; set;
        } 
    }
    
}