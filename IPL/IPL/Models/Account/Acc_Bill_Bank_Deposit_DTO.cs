﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IPL.Models.Account
{
    public class Acc_Bill_Bank_Deposit_DTO
    {
        public Acc_Bill_Bank_Deposit_DTO()
        {
        }
        public Acc_Bill_Bank_Deposit_DTO(DataRow dataRow)
        {
            Deposit_To = Helper.GetBigIntRowData(dataRow, "Deposit_To");
            PaymentDate = Helper.GetDateTimeRowData(dataRow, "PaymentDate");
            Memo = Helper.GetStringRowData(dataRow, "Memo");
            UserID = Helper.GetBigIntRowData(dataRow, "UserID");
            Type = Helper.GetIntRowData(dataRow, "Type");
        }
        public long Deposit_To
        {
            get; set;
        }
        public DateTime? PaymentDate
        {
            get; set;
        }
        public string Memo
        {
            get; set;
        }
        public long UserID
        {
            get; set;
        }
        public int Type
        {
            get; set;
        }
        public List<Acc_Bill_BankDeposit_Items> BankDeposit_Items
        {
            get;set;
        }
    }
    public class Acc_Bill_BankDeposit_Items
    {
        public  long Bill_Rec_PkeyId
        {
            get; set;
        }
        public long Bill_Id
        {
            get; set;
        }
        public Int64 Bill_VendorId
        {
            get; set;
        }
        public DateTime? Payment_Date
        {
            get; set;
        }
        public decimal? Amount
        {
            get; set;
        }
        public decimal? Original_Amount
        {
            get; set;
        }
        public decimal? Payment_Amount
        {
            get; set;
        }
        public decimal? Pending_Amount
        {
            get; set;
        }
        public string Customer_Name
        {
            get; set;
        }
        public int? Payment_Method
        {
            get; set;

        }
        public string Ref_No
        {
            get; set;
        }
        public string Memo
        {
            get; set;
        }
    }
}