﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models.Account
{
    public class Acc_Journal_Reports_DTO
    {
        public List<Acc_Journal_Invoice_Reports_DTO> Acc_Journal_Invoice_Reports_DTO
        {
            get; set;
        }
        public List<Acc_Journal_Bill_Reports_DTO> Acc_Journal_Bill_Reports_DTO
        {
            get; set;
        }
    }
    public class Acc_Journal_Invoice_Reports_DTO
    {
        public long Invoice_pkeyId
        {
            get; set;
        }
        public string Invoice_Number
        {
            get; set;
        }
        public long Invoice_CustomeId
        {
            get; set;
        }
        public string Invoice_Custome_Email
        {
            get; set;
        }
        public DateTime? Invoice_Date
        {
            get; set;
        }
        public DateTime? Invoice_Due_Date
        {
            get; set;
        }
        public decimal? Invoice_Sub_total
        {
            get; set;
        }
        public decimal? Invoice_Taxble_Amount
        {
            get; set;
        }
        public decimal? Invoice_Total
        {
            get; set;
        }
        public decimal? Invoice_Balance_Due
        {
            get; set;
        }
        public string Invoice_Custome_Name
        {
            get; set;
        }
        public int? Invoice_Status
        {
            get; set;
        }
        public DateTime? Invoice_CreatedOn
        {
            get; set;
        }
        public decimal? Invoice_Rec_Payment
        {
            get; set;
        }
        public string DeposiToAccount
        {
            get; set;
        }
        public string Invoice_Message
        {
            get; set;
        }
        public string ReceivePayment_Memo
        {
            get; set;
        }
        public long? Invoice_Rec_PkeyId
        {
            get; set;
        }
        public DateTime? ReceivedPaymentDate
        {
            get; set;
        }
    }
    public class Acc_Journal_Bill_Reports_DTO
    {
        public long Bill_pkeyId
        {
            get; set;
        }
        public string Bill_Number
        {
            get; set;
        }
        public long Bill_VendorId
        {
            get; set;
        }
        public string Bill_Vendor_Email
        {
            get; set;
        }
        public DateTime? Bill_Date
        {
            get; set;
        }
        public DateTime? Bill_Due_Date
        {
            get; set;
        }
        public decimal? Bill_Sub_total
        {
            get; set;
        }
        public decimal? Bill_Taxble_Amount
        {
            get; set;
        }
        public decimal? Bill_Total
        {
            get; set;
        }
        public decimal? Bill_Balance_Due
        {
            get; set;
        }
        public string Bill_Vendor_Name
        {
            get; set;
        }
        public int? Bill_Status
        {
            get; set;
        }
        public DateTime? Bill_CreatedOn
        {
            get; set;
        }
        public decimal? Bill_Rec_Payment
        {
            get; set;
        }
        public string DeposiToAccount
        {
            get; set;
        }
        public string Bill_Message
        {
            get; set;
        }
        public string ReceivePayment_Memo
        {
            get; set;
        }
        public long? Bill_Rec_PkeyId
        {
            get; set;
        }
        public DateTime? ReceivedPaymentDate
        {
            get; set;
        }
    }

}