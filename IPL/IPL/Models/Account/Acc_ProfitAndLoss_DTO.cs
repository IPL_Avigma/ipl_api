﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models.Account
{
    public class Acc_ProfitAndLoss_DTO
    {
        public List<Income> Income
        {
            get; set;
        }
        public List<Income> OtherIncome
        {
            get; set;
        }
        public decimal TotalIncome
        {
            get; set;
        } = 0;
        public decimal TotalOtherIncome
        {
            get; set;
        } = 0;
        public decimal TotalSales
        {
            get; set;
        } = 0;
        public decimal GrossProfit
        {
            get; set;
        } = 0;
        public decimal Profit
        {
            get; set;
        } = 0;
        public List<Expense> Expenses
        {
            get; set;
        }
        public decimal TotalExpenses
        {
            get; set;
        } = 0;
    }
    public class Income
    {
        public long AccountId
        {
            get; set;
        }
        public int AccountType
        {
            get; set;
        }
        public string AccountCode
        {
            get; set;
        }
        public string AccountName
        {
            get; set;
        }
        public decimal Amount
        {
            get; set;
        }
        public DateTime?  CreatedDate
        {
            get; set;
        }
        public List<decimal> MonthByAmount
        {
            get;set;
        }
        public long? CustomerId
        {
            get; set;
        }
        public string CustomerName
        {
            get; set;
        }
        public long? VendorId
        {
            get; set;
        }
        public string VendorName
        {
            get; set;
        }

    }
    public class Expense
    {
        public long AccountId
        {
            get; set;
        }
        public int AccountType
        {
            get; set;
        }
        public string AccountCode
        {
            get; set;
        }
        public string AccountName
        {
            get; set;
        }
        public decimal Amount
        {
            get; set;
        }
        public DateTime CreatedDate
        {
            get; set;
        }
        public List<decimal> MonthByAmount
        {
            get; set;
        }
    }
}