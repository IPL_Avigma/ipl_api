﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models.Account
{
    public class Acc_Journal_DTO
    {
        public List<Invoices> Invoices
        {
            get; set;
        }
        public List<Journal> Journal
        {
            get; set;
        }
        public decimal DebitTotal
        {
            get; set;
        } = 0;
        public decimal CreditTotal
        {
            get; set;
        } = 0;

    }
    public class Invoices
    {
        public DateTime CreatedDate
        {
            get; set;
        }
        public string TransactionType
        {
            get; set;
        }
        public string ExpensesTypeName
        {
            get; set;
        }
        public string NO
        {
            get; set;
        }
        public string Name
        {
            get; set;
        }
        public string Memo_Description
        {
            get; set;
        }
        public string DebitAccount
        {
            get; set;
        }
        public string CreditAccount
        {
            get; set;
        }
        public decimal DebitAmount
        {
            get; set;
        }
        public decimal CreditAmount
        {
            get; set;
        }
        public decimal TotalAmount
        {
            get; set;
        }
        public int DrOrCrSide
        {
            get; set;
        } = 0;
    }
    public class Journal
    {
        public DateTime CreatedDate
        {
            get; set;
        }
        public string TransactionType
        {
            get; set;
        }
        public string ExpensesTypeName
        {
            get; set;
        }
        public string NO
        {
            get; set;
        }
        public string Name
        {
            get; set;
        }
        public List<string> Memo_Description
        {
            get; set;
        }
        public List<string> AccountNames
        {
            get; set;
        }
    
        public List<decimal> DebitAmount
        {
            get; set;
        }
        public List<decimal> CreditAmount
        {
            get; set;
        }
        public decimal TotalDebitAmount
        {
            get; set;
        } = 0;
        public decimal TotalCreditAmount
        {
            get; set;
        } = 0;
        public int DrOrCrSide
        {
            get; set;
        } = 0;
    }

}