﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models.Account
{
    public class Acc_ProfitAndLoss_By_VendorDTO
    {
        public List<SelectVendorItem> VendorList
        {
            get; set;
        }
        public List<Income> Income
        {
            get; set;
        }
        public List<Income> OtherIncome
        {
            get; set;
        }

        public decimal TotalIncome
        {
            get; set;
        } = 0;
        public List<decimal> VendorByWiseTotalIncome
        {
            get; set;
        }
        public decimal TotalOtherIncome
        {
            get; set;
        } = 0;
        public List<decimal> VendorByWiseTotalOtherIncome
        {
            get; set;
        }
        public decimal TotalSales
        {
            get; set;
        } = 0;
        public List<decimal> VendorByWiseTotalSales
        {
            get; set;
        }
        public decimal GrossProfit
        {
            get; set;
        } = 0;
        public decimal Profit
        {
            get; set;
        } = 0;
        public List<decimal> VendorByWiseProfit
        {
            get; set;
        }
        public List<Expense> Expenses
        {
            get; set;
        }
        public decimal TotalExpenses
        {
            get; set;
        } = 0;
        public List<decimal> CustomerByWiseTotalExpenses
        {
            get; set;
        } 
    }
    public class SelectVendorItem
    {
        public long BillId
        {
            get; set;
        }
        public long VendorId
        {
            get; set;
        }
        public string Name
        {
            get; set;
        }
    }
    
}