﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
  public class IPL_Chat_PhotoDTO
  {
    public Int64 Wo_Msg_Doc_PkeyId { get; set; }
    public Int64 Wo_Msg_Doc_Wo_ID { get; set; }
    public Int64 Wo_Msg_Doc_Company_Id { get; set; }
    public Int64 workOrder_ID { get; set; }
    public Int64 UserID { get; set; }
    public int Type { get; set; }
    public int Skip { get; set; }
    public int Take { get; set; }
    public string SearchStr { get; set; }
  }
}