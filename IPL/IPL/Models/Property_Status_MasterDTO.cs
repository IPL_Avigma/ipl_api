﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Property_Status_MasterDTO
    {
        public Int64 PS_PkeyID { get; set; }
        public string PS_Name { get; set; }
        public Boolean? PS_IsActive { get; set; }
        public Boolean? PS_IsDelete { get; set; }
        public int Type { get; set; }
        public Int64 UserID { get; set; }
        public string WhereClause { get; set; }
        public string FilterData { get; set; }
    }
    public class Property_Status_Master
    {
        public String PS_PkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}