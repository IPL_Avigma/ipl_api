﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class PCR_Utilities_MasterDTO
    {
        public Int64 PCR_Utilities_pkeyId { get; set; }
        public Int64 PCR_Utilities_MasterId { get; set; }
        public Int64 PCR_Utilities_WO_Id { get; set; }
        public int PCR_Utilities_ValType { get; set; }

        public String PCR_Utilities_On_Arrival_Water { get; set; }
        public String PCR_Utilities_On_Departure_Water { get; set; }

        public String PCR_Utilities_On_Arrival_Gas { get; set; }
        public String PCR_Utilities_On_Departure_Gas { get; set; }

        public String PCR_Utilities_On_Arrival_Electric { get; set; }
        public String PCR_Utilities_On_Departure_Electric { get; set; }

        public String PCR_Utilities_Sump_Pump { get; set; }
        public String PCR_Utilities_Sump_Pump_Commend { get; set; }
        public String PCR_Utilities_Sump_Pump_Sump_Test { get; set; }

        public String PCR_Utilities_Main_Breaker_And_Operational { get; set; }
        public String PCR_Utilities_Sump_Pump_Missing_Bid_To_Replace { get; set; }

        public String PCR_Utilities_Transferred_Activated { get; set; }
        public String PCR_Utilities_Reason_UtilitiesNot_Transferred { get; set; }
        public String PCR_Utilities_Reason_UtilitiesNot_Transferred_Other_Notes { get; set; }

      
        public String PCR_Utilities_Transferred_Water_Co_Name { get; set; }
        public String PCR_Utilities_Transferred_Water_Address { get; set; }
        public String PCR_Utilities_Transferred_Water_Phone { get; set; }
        public String PCR_Utilities_Transferred_Water_Acct { get; set; }

        public String PCR_Utilities_Transferred_Gas_Co_Name { get; set; }
        public String PCR_Utilities_Transferred_Gas_Address { get; set; }
        public String PCR_Utilities_Transferred_Gas_Phone { get; set; }
        public String PCR_Utilities_Transferred_Gas_Acct { get; set; }

        public String PCR_Utilities_Transferred_Electric_Co_Name { get; set; }
        public String PCR_Utilities_Transferred_Electric_Address { get; set; }
        public String PCR_Utilities_Transferred_Electric_Phone { get; set; }
        public String PCR_Utilities_Transferred_Electric_Acct { get; set; }

    
        public Boolean? PCR_Utilities_IsActive { get; set; }
        public Boolean? PCR_Utilities_IsDelete { get; set; }
        public Int64 UserID { get; set; }
        public Int64 fwo_pkyeId { get; set; }
        public int Type { get; set; }

    }

    public class PCR_Utilities_Master
    {
        public String PCR_Utilities_pkeyId { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}