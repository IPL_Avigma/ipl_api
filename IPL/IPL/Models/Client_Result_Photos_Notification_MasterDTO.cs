﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace IPL.Models
{
    public class Client_Result_Photos_Notification_MasterDTO
    {
        public Int64 PN_Pkey_Id { get; set; }
        public Int64 PN_WoId { get; set; }
        public String WhereClause { get; set; }
        public Int64 PN_UserId { get; set; }
        public Int64 PN_IplCompanyId { get; set; }
        public int Type { get; set; }
    }
}