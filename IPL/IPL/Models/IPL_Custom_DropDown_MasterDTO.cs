﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class IPL_Custom_DropDown_MasterDTO
    {
        public Int64 Ipl_PkeyId { get; set; }
        public String Ipl_Name { get; set; }
    }
    public class IPL_Custom_DropDown_SecondDTO
    {
        public Int64 Ipl_Sec_PkeyId { get; set; }
        public String Ipl_Sec_Name { get; set; }
    }
    public class IPL_Custom_DropDown_ThirdDTO
    {
        public Int64 Ipl_Third_PkeyId { get; set; }
        public String Ipl_Third_Name { get; set; }
    }
    public class IPL_Custom_DropDown_FourthDTO
    {
        public Int64 Ipl_Fourth_PkeyId { get; set; }
        public String Ipl_Fourth_Name { get; set; }
    }
    public class CustomDRDMaster
    {
        public List<IPL_Custom_DropDown_MasterDTO> IPL_Custom_DropDown_MasterDTO { get; set; }
        public List<IPL_Custom_DropDown_SecondDTO> IPL_Custom_DropDown_SecondDTO { get; set; }
        public List<IPL_Custom_DropDown_ThirdDTO> IPL_Custom_DropDown_ThirdDTO { get; set; }
        public List<IPL_Custom_DropDown_FourthDTO> IPL_Custom_DropDown_FourthDTO { get; set; }
        public Int64 UserID { get; set; }
        public Int64 CompanyID { get; set; }
        public int Type { get; set; }
    }
    
}