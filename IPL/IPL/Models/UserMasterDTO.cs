﻿using IPL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPLApp.Models
{
    public class UserMasterDTO
    {
        public Int64 User_pkeyID { get; set; }
        public String User_FirstName { get; set; }
        public String User_LastName { get; set; }
        public String User_Address { get; set; }
        public String User_City { get; set; }
        public int? User_State { get; set; }
        public Int64? User_Zip { get; set; }
        public String User_CellNumber { get; set; }
        public String User_CompanyName { get; set; }
        public int? User_Sys_Record { get; set; }
        public String User_LoginName { get; set; }
        public String User_Password { get; set; }
        public String User_Email { get; set; }
        public int? User_Group { get; set; }
        public Boolean? User_Contractor { get; set; }
        public Boolean? User_Cordinator { get; set; }
        public Boolean? User_Processor { get; set; }
        public String User_OpenOrderDisCriteria { get; set; }
        public Boolean? User_PastWorkOrder { get; set; }
        public String User_PastOrderDisCriteria { get; set; }
        public String User_SelectOrderDisCriteria { get; set; }
        public String User_BackgroundCheckProvider { get; set; }
        public String User_BackgroundCheckId { get; set; }
        public String User_BackgroundDocPath { get; set; }
        public String User_BackgroundDocName { get; set; }
        public String User_Comments { get; set; }
        public Boolean? User_Assi_Admin { get; set; }
        public int? User_Active { get; set; }
        public int? User_WorkOrder { get; set; }
        public int? User_Wo_History { get; set; }
        public Decimal? User_Disc_percentage { get; set; }
        public int? User_Tme_Zone { get; set; }
        public Boolean? User_Auto_Assign { get; set; }
        public String User_Leg_FirstName { get; set; }
        public String User_Leg_LastName { get; set; }
        public String User_Leg_CellPhone { get; set; }
        public String User_Leg_Address { get; set; }
        public String User_Leg_Address1 { get; set; }
        public String User_Leg_City { get; set; }
        public int? User_Leg_State { get; set; }
        public String User_Leg_Notes { get; set; }
        public Boolean? User_Email_Note { get; set; }
        public Boolean? User_Emai_Reminders { get; set; }
        public Boolean? User_Email_New_Wo { get; set; }
        public Boolean? User_Email_UnAssigned_Wo { get; set; }
        public Boolean? User_Email_FollowUp { get; set; }
        public Boolean? User_Text_Note { get; set; }
        public Boolean? User_Text_Reminders { get; set; }
        public Boolean? User_Text_New_Wo { get; set; }
        public Boolean? User_Text_UnAssigned_Wo { get; set; }
        public Boolean? User_Text_FollowUp { get; set; }
        public Boolean? User_Alert_EmailReply { get; set; }
        public Boolean? User_Alert_Ready_Office { get; set; }
        public Int64? User_Misc_Contractor_Score { get; set; }
        public String User_Misc_Insurance_Expire { get; set; }
        public String User_Misc_Pruvan_Username { get; set; }
        public String User_Misc_PushKey { get; set; }
        public int? User_Misc_StartDate { get; set; }
        public String User_Misc_Device_Id { get; set; }
        public String User_Misc_ABC { get; set; }
        public String User_Misc_Service_Id { get; set; }
        public Boolean? User_IsActive { get; set; }
        public Boolean? User_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
        public Int64? User_Con_Cat_Id { get; set; }
        public Boolean? User_Tracking { get; set; }
        public int? User_Tracking_Time { get; set; }

        public Int64? User_SubContractor { get; set; }

        public List<StrAddressArray> StrAddressArray { get; set; }

        public Boolean? User_Email_Cancelled { get; set; }
        public Boolean? User_Email_New_Message { get; set; }
        public Boolean? User_Email_Field_Complete { get; set; }
        public Boolean? User_Email_Daily_Digest { get; set; }

        public Boolean? User_Text_Cancelled { get; set; }
        public Boolean? User_Text_New_Message { get; set; }
        public Boolean? User_Text_Field_Complete { get; set; }

        public String User_AssignClient { get; set; }


        public List<UserDocumentArray> UserDocumentArray { get; set; }


        public String User_ImagePath { get; set; }
        public String User_Token_val { get; set; }

        public Int64? User_State_strval { get; set; }

        public String User_lat { get; set; }

        public String User_long { get; set; }
        public String User_BusinessID_Social_No { get; set; }
        public Boolean? User_Track_Payment { get; set; }
        public String User_Default_Expence_Account_Id {  get; set; }
        public String User_CreatedBy {  get; set; }
        public String User_ModifiedBy {  get; set; }
        public int? User_Source{  get; set; }

        public Int64 Group_DR_PkeyID { get; set; }

        public String User_Sub_FirstName { get; set; }
        public string User_Sub_LastName { get; set; }
        public string User_Sub_Address { get; set; }
        public string User_Sub_City { get; set; }
        public int? User_Sub_State { get; set; }
        public int? User_Sub_County { get; set; }
        public Int64? User_Sub_Zip { get; set; }
        public String User_Sub_CellNumber { get; set; }
        public String User_Sub_LoginName { get; set; }
        public String User_Sub_Password { get; set; }
        public String User_Sub_Email { get; set; }
        public Boolean? User_Sub_IsActive { get; set; }
        public Boolean? User_Sub_Delete { get; set; }
        public int? User_Sub_GroupID { get; set; }
        public String WhereClause { get; set; }
        public int? Pagenumber { get; set; }
        public int? Rownumber { get; set; }


    }
    public class ProfileUserMaster
    {
        public Int64 User_pkeyID { get; set; }
        public String User_FirstName { get; set; }
        public String User_LastName { get; set; }
        public String User_LoginName { get; set; }
        public String User_Password { get; set; }
        public String User_CellNumber { get; set; }
        public String User_CompanyName { get; set; }

        public String User_ImagePath { get; set; }
        public String User_Token_val { get; set; }
        public String User_Address { get; set; }
        public String User_City { get; set; }

        public Int64? User_Zip { get; set; }
        public Int64? User_State_strval { get; set; }

        public String User_Email { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }

        public List<IPLState_MobDTO> IPLState_Mob_Data { get; set; }
    }


    public class UserMaster
    {
        public String User_pkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }


    public class StrAddressArray
    {
        public Int64 IPL_PkeyID { get; set; }
        public string IPL_Address { get; set; }
        public string IPL_City { get; set; }
        public Int64? IPL_State { get; set; }
        public Int64? IPL_County { get; set; }
        public String IPL_StateName { get; set; }
        public String IPL_CountyName { get; set; }

        public int? IPL_Primary_Zip_Code { get; set; }
    }
    public class UserFiltermasterDTO
    {
        public Int64 User_pkeyID { get; set; }
        public String User_FirstName { get; set; }
        public String User_LastName { get; set; }
        public String User_Address { get; set; }
        public String User_City { get; set; }
        public int? User_State { get; set; }
        public Int64? User_Zip { get; set; }
        public String User_CellNumber { get; set; }
        public String User_CompanyName { get; set; }
        public String User_LoginName { get; set; }
        public String User_Email { get; set; }
        public int? User_Group { get; set; }
        public Int64? User_Misc_Contractor_Score { get; set; }
        public Boolean? User_IsActive { get; set; }
        public Boolean? User_IsDelete { get; set; }
        public String FilterData { get; set; }
        public SearchMasterDTO SearchMaster { get; set; }

        public int Type { get; set; }

        public string User_Group_Name { get; set; }

        public Int64 UserID { get; set; }
        public String User_BusinessID_Social_No
        {
            get; set;
        }
        public Boolean? User_Track_Payment
        {
            get; set;
        }
        public String User_Default_Expence_Account_Id
        {
            get; set;
        }
        public String User_CreatedBy
        {
            get; set;
        }
        public String User_ModifiedBy
        {
            get; set;
        }
        public string ViewUrl { get; set; }
       
    }

    public class UserDocumentArray
    {
        public int User_Doc_pkeyID { get; set; }
        public string User_Doc_DocPath { get; set; }
        public string User_Doc_FileName { get; set; }
        public string User_Doc_ValType { get; set; }
        public DateTime? User_Doc_Exp_Date { get; set; }
        public DateTime? User_Doc_RecievedDate { get; set; }
        public DateTime? User_Doc_NotificationDate { get; set; }
        public bool? User_Doc_AlertUser { get; set; }
        public int User_Doc_UserID { get; set; }
        public bool? User_Doc_IsActive { get; set; }
        public bool? User_Doc_IsDelete { get; set; }
        public int? UserID { get; set; }
        public int? Type { get; set; }
    }
}