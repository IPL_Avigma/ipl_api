﻿using Quartz.Impl.Triggers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPLApp.Models
{
    public class WorkOrderHistoryDTO
    {
        public Int64 workOrder_ID { get; set; }
        public int Type { get; set; }
        public Int64? UserID { get; set; }
        public int? PageNumber { get; set; }
        public int? NoofRows { get; set; }
        public int? Skip { get; set; }

        public String FilterData { get; set; }
        public String FilterWhereClause { get; set; }

    }
    public class WorkOrderbidHistoryDTO
    {
        public Int64 Task_Bid_pkeyID { get; set; }
        public Int64? Task_Bid_TaskID { get; set; }
        public String Task_Name { get; set; }
        public Int64? Task_Bid_WO_ID { get; set; }
        public String Task_Bid_Qty { get; set; }
        public Int64? Task_Bid_Uom_ID { get; set; }
        public String UOM_Name { get; set; }
        public Decimal? Task_Bid_Cont_Price { get; set; }
        public Decimal? Task_Bid_Cont_Total { get; set; }
        public Decimal? Task_Bid_Clnt_Price { get; set; }
        public Decimal? Task_Bid_Clnt_Total { get; set; }
        public String Task_Bid_Comments { get; set; }
        public String Task_Bid_Violation_val { get; set; }
        public Boolean? Task_Bid_damage { get; set; }
        public String Task_Bid_damage_val { get; set; }
        public int? Task_Bid_Status { get; set; }
        public Int64? Task_Bid_Instr_pkeyId { get; set; }
        public String workOrderNumber { get; set; }
        public String IPLNO { get; set; }
        public String CORNT_User_FirstName { get; set; }
        public String Task_Bid_Status_val { get; set; }
        public int CountPhotos { get; set; }
        public string WT_WorkType { get; set; }
        public List<WorkOrderHistoryPhotoDTO> workOrderHistoryPhotoDTOs { get; set; }
        public DateTime? BIDDate { get; set; }

    }
    public class WorkOrderHistoryCompletionDTO
    {
        public Int64 Task_Inv_pkeyID { get; set; }
        public Int64? Task_Inv_TaskID { get; set; }
        public String Task_Name { get; set; }
        public Int64? Task_Inv_WO_ID { get; set; }
        public String Task_Inv_Qty { get; set; }
        public Int64? Task_Inv_Uom_ID { get; set; }
        public String UOM_Name { get; set; }
        public Decimal? Task_Inv_Cont_Price { get; set; }
        public Decimal? Task_Inv_Cont_Total { get; set; }
        public Decimal? Task_Inv_Clnt_Price { get; set; }
        public Decimal? Task_Inv_Clnt_Total { get; set; }
        public String Task_Inv_Comments { get; set; }
        public String Task_Inv_Violation_val { get; set; }
        public String Task_Inv_damage_val { get; set; }
        public String Task_Inv_Auto_Invoice_val { get; set; }
        public int? Task_Inv_Status { get; set; }
        public Int64? Task_Inv_Instr_pkeyId { get; set; }
        public DateTime? INVDate { get; set; }
        public String workOrderNumber { get; set; }
        public String IPLNO { get; set; }
        public String CORNT_User_FirstName { get; set; }
        public String Task_Inv_Status_val { get; set; }
        public int CountPhotos { get; set; }
        public List<WorkOrderHistoryPhotoDTO> workOrderHistoryPhotoDTOs { get; set; }
        public string WT_WorkType { get; set; }

    }
    public class WorkOrderHistoryDamageDTO
    {
        public Int64 Task_Damage_pkeyID { get; set; }
        public Int64? Task_Damage_WO_ID { get; set; }
        public Int64? Task_Damage_Task_ID { get; set; }
        public Int64? Task_Damage_ID { get; set; }
        public String Task_Damage_Type { get; set; }
        public String Task_Damage_Int { get; set; }
        public String Task_Damage_Int_val { get; set; }
        public String Task_Damage_Location { get; set; }
        public String Task_Damage_Qty { get; set; }
        public String Task_Damage_Estimate { get; set; }
        public String Task_Damage_Disc { get; set; }
        public String Damage_Type { get; set; }
        public int? Task_Damage_Status { get; set; }
        public DateTime? DamageDate { get; set; }
        public String IPLNO { get; set; }
        public String workOrderNumber { get; set; }
        public String CORNT_User_FirstName { get; set; }
        public String Task_Damage_Status_val { get; set; }
        public int CountPhotos { get; set; }
        public List<WorkOrderHistoryPhotoDTO> workOrderHistoryPhotoDTOs { get; set; }
        public string WT_WorkType { get; set; }
    }
    public class WorkOrderHistoryApplianceDTO
    {
        public Int64 Appl_pkeyId { get; set; }
        public Int64? Appl_Wo_Id { get; set; }
        public Int64? Appl_App_Id { get; set; }
        public String Appl_Apliance_Name { get; set; }
        public String Appl_Comment { get; set; }
        public int? Appl_Status_Id { get; set; }
        public DateTime? ApplDate { get; set; }
        public Boolean? Appl_IsActive { get; set; }
        public Boolean? Appl_IsDelete { get; set; }
        public String Appl_Status_Id_val { get; set; }
    }
    public class WorkOrderHistoryPastDTO
    {
        public Int64 workOrder_ID { get; set; }
        public String workOrderNumber { get; set; }
        public String workOrderInfo { get; set; }
        public String address1 { get; set; }
        public String address2 { get; set; }
        public String city { get; set; }
        public String state { get; set; }
        public Int64? zip { get; set; }
        public String country { get; set; }

        public String status { get; set; }
        public DateTime? dueDate { get; set; }
        public String strdueDate { get; set; }
        public DateTime? startDate { get; set; }

        public String clientStatus { get; set; }
        public DateTime? clientDueDate { get; set; }
        public String gpsLatitude { get; set; }
        public String gpsLongitude { get; set; }
        public String FullAddress { get; set; }
        public String Lock_Location { get; set; }
        public String Key_Code { get; set; }
        public String Gate_Code { get; set; }
        public String Loan_Number { get; set; }
        public String Search_Address { get; set; }
        public String Office_Approved { get; set; }
        public String Sent_to_Client { get; set; }
        public String IPLNO { get; set; }
        public String CORNT_User_FirstName { get; set; }
        public Boolean? IsDelete { get; set; }
        public Boolean? IsActive { get; set; }
        public String WT_WorkType { get; set; }
        public String CreatedBy { get; set; }
        public String ModifiedBy { get; set; }
        public Boolean? IsEdit { get; set; }
        public int CountPhotos { get; set; }
        public String CompanyName { get; set; }
    }

    public class WorkOrderHistoryPhotoDTO
    {
        public Int64 Client_Result_Photo_ID { get; set; }
        public Int64? Client_Result_Photo_Wo_ID { get; set; }
        public String Client_Result_Photo_FileName { get; set; }
        public String Client_Result_Photo_FilePath { get; set; }
        public String Client_Result_Photo_FileType { get; set; }
        public String Client_Result_Photo_FileSize { get; set; }
        public String Client_Result_Photo_TaskLable_Name { get; set; }
        public int? Client_Result_Photo_Lable_Status { get; set; }
        public String Client_Result_Photo_BucketName { get; set; }

        public String Client_Result_Photo_FolderName { get; set; }

        public int? Client_Result_Photo_StatusType { get; set; }

        public String Client_Result_Photo_UploadBy { get; set; }
        public DateTime? Client_Result_Photo_UploadTimestamp { get; set; }
        public DateTime? Client_Result_Photo_DateTimeOriginal { get; set; }
        public String Client_Result_Photo_GPSLatitude { get; set; }
        public String Client_Result_Photo_GPSLongitude { get; set; }
        public String Client_Result_Photo_Make { get; set; }
        public String Client_Result_Photo_Model { get; set; }
        public String Client_Result_Photo_UploadFrom { get; set; }
        public String Client_Result_Photo_FlaggedTo { get; set; }
        public String Client_Result_Photo_Caption { get; set; }


        public Int64? CRP_New_pkeyId { get; set; }
        public Int64? CRP_New_Bid_Id { get; set; }
        public Int64? CRP_New_Inv_Id { get; set; }
        public Int64? CRP_New_Damage_Id { get; set; }
        public Int64? CRP_WorkOrderPhotoLabel_ID { get; set; }
        public Int64? CRP_Inspection_Id { get; set; }
        public Int64? CRP_New_Task_Bid_pkeyID { get; set; }

        public int CRP_New_Status_Type { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
    }
}