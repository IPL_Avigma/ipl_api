﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class PropertyLockReasonMasterDTO
    {
        public Int64 LockReason_PkeyID { get; set; }
        public string LockReason_Name { get; set; }
        public Boolean? LockReason_IsActive { get; set; }
        public Boolean? LockReason_IsDelete { get; set; }
        public int Type { get; set; }
        public Int64 UserID { get; set; }
        public string WhereClause { get; set; }
        public string FilterData { get; set; }
    }
    public class PropertyLockReasonMaster
    {
        public String LockReason_PkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}