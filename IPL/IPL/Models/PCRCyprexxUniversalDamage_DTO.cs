﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class PCRCyprexxUniversalDamage_DTO
    {
        public Int64 PCR_CU_PkeyID { get; set; }
        public String PCR_CU_General_Info { get; set; }
        public String PCR_CU_Interior_Access_Information { get; set; }
        public String PCR_CU_Upload_Photos { get; set; }
        public Boolean? PCR_CU_IsActive { get; set; }
        public Boolean? PCR_CU_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
    }
}