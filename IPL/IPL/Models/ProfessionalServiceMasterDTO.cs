﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Web;

namespace IPL.Models
{
    public class ProfessionalServiceMasterDTO
    {
        public Int64 PS_PkeyId { get; set; }
        public string PS_ContactName { get; set; }
        public string PS_CompanyName { get; set; }
        public string PS_Address { get; set; }
        public string PS_Phone { get; set; }
        public string PS_Email { get; set; }
        public string PS_Website { get; set; }
        public string PS_Notes { get; set; }
        public Int64 PS_ContactType { get; set; }
        public string ContactType { get; set; }
        public bool PS_IsActive { get; set; }
        public string PS_CreatedBy { get; set; }
        public string PS_ModifiedBy { get; set; }
        public bool PS_IsDeleted { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
    }
}