﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Folder_LoneType_MasterDTO
    {
        public Int64 Fold_LoneType_PkeyId { get; set; }
        public Int64? Fold_LoneType_Id { get; set; }
        public Int64? Fold_LoneType_Folder_Id { get; set; }
        public Int64? Fold_LoneType_File_Id { get; set; }
        public Int64? Fold_LoneType_Auto_Assine_Id { get; set; }
        public Boolean? Fold_LoneType_IsActive { get; set; }
        public Boolean? Fold_LoneType_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int? Type { get; set; }
        public Int64? Folder_File_Master_FK_Id { get; set; }
    }
}