﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class WorkOrder_Altisource_MasterDTO
    {
        public Int64 WAltisource_PkeyID { get; set; }
        public String WAltisource_wo_id { get; set; }
        public String WAltisource_category { get; set; }
        public String WAltisource_address { get; set; }
        public String WAltisource_city { get; set; }
        public String WAltisource_state { get; set; }
        public String WAltisource_zip { get; set; }
        public DateTime? WAltisource_estimated_complete_date { get; set; }
        public String WAltisource_loan_number { get; set; }
        public String WAltisource_loan_type { get; set; }
        public DateTime? WAltisource_due_date { get; set; }
        public DateTime? WAltisource_client_Due_date { get; set; }
        public String WAltisource_username { get; set; }
        public String WAltisource_lot_size { get; set; }
        public DateTime? WAltisource_received_date { get; set; }
        public String WAltisource_customer { get; set; }
        public String WAltisource_client_company { get; set; }
        public String WAltisource_ppw { get; set; }
        public DateTime? WAltisource_start_date { get; set; }
        public String WAltisource_contractor { get; set; }
        public String WAltisource_missing_info { get; set; }
        public String WAltisource_work_type { get; set; }
        public String WAltisource_assigned_admin { get; set; }
        public String WAltisource_lock_code { get; set; }
        public String WAltisource_key_code { get; set; }
        public String WAltisource_wo_status { get; set; }
        public String WAltisource_mortgager { get; set; }
        public DateTime? WAltisource_ready_for_office_date { get; set; }
        public String WAltisource_id { get; set; }
        public String WAltisource_comments { get; set; }
        public String WAltisource_gpsLatitude { get; set; }
        public String WAltisource_gpsLongitude { get; set; }
        public Int64 WAltisource_ImportMaster_Pkey { get; set; }
        public String WAltisource_Import_File_Name { get; set; }
        public String WAltisource_Import_FilePath { get; set; }
        public String WAltisource_Import_Pdf_Name { get; set; }
        public String WAltisource_Import_Pdf_Path { get; set; }
        public Boolean? WAltisource_IsProcessed { get; set; }
        public Boolean? WAltisource_IsActive { get; set; }
        public Boolean? WAltisource_IsDelete { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
    }

    public class WorkOrder_AltisourceItem_MasterDTO
    {
        public String wo { get; set; }
        public String category { get; set; }
        public String address { get; set; }
        public String city { get; set; }
        public String state { get; set; }
        public String zip { get; set; }
        public DateTime? estimated_complete_date { get; set; }
        public String loan_number { get; set; }
        public String loan_type { get; set; }
        public DateTime? due_date { get; set; }
        public DateTime? client_Due_date { get; set; }
        public String username { get; set; }
        public String lot_size { get; set; }
        public DateTime? received_date { get; set; }
        public String customer { get; set; }
        public String client_company { get; set; }
        public String ppw { get; set; }
        public DateTime? start_date { get; set; }
        public String contractor { get; set; }
        public String missing_info { get; set; }
        public String work_type { get; set; }
        public String assigned_admin { get; set; }
        public String lock_code { get; set; }
        public String key_code { get; set; }
        public String wo_status { get; set; }
        public String mortgager { get; set; }
        public DateTime? ready_for_office_date { get; set; }
        public String wo_id { get; set; }
        public String comments { get; set; }
        public List<AltisourceInstruction> work_order_item_details { get; set; }

    }
    public class AltisourceInstruction
    {
        public string instruction_name { get; set; }
        public string Description { get; set; }
        public string additional_details { get; set; }
        public string Additional_Instructions { get; set; }
        public string Qty { get; set; }
        public string Price { get; set; }
        public string Total { get; set; }
    }
    public class WorkOrder_Altisource_Master
    {
        public String WAltisource_PkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}