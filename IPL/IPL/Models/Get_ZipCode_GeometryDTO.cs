﻿using System;

namespace IPL.Models
{
    public class Get_ZipCode_GeometryDTO
    {
        public Int64 PostalBoundry_pkeyId { get; set; }
        public string ZipCode { get; set; }
        public string GeometryData { get; set; }
    }
}