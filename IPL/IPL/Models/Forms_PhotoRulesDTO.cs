﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Forms_PhotoRulesDTO
    {
        public Int64 PhotoRuleId { get; set; }
        public string PhotoRule_Operator { get; set; }
        public Int64 PhotoRule_Value { get; set; }
        public Int64 PhotoRule_Min { get; set; }
        public Int64 PhotoRule_Max { get; set; }
        public bool PhotoRule_IsActive { get; set; }
        public Int64 UserId { get; set; }
        public Int64 QuestionId { get; set; }
        public Int64 FormId { get; set; }
        public Int16 Type { get; set; }
        public bool PhotoRule_IsDelete { get; set; }        

    }
}