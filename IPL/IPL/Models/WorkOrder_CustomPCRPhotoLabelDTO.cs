﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class WorkOrder_CustomPCRPhotoLabelDTO
    {
        public Int64 WorkOrderPCRPhotoLabel_pkeyID { get; set; }
        public Int64 WorkOrderPCRPhotoLabel_PhotoLabel_ID { get; set; }
        public Int64 WorkOrderPCRPhotoLabel_WO_ID { get; set; }
        public Boolean WorkOrderPCRPhotoLabel_IsActive { get; set; }
        public Boolean WorkOrderPCRPhotoLabel_IsDelete { get; set; }
        public int Type { get; set; }
        public Int64 UserID { get; set; }
        public String WorkOrderPCRPhotoLabel_CreatedBy { get; set; }
        public String WorkOrderPCRPhotoLabel_ModifiedBy { get; set; }

    }
}