﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class TaskPhotoSettingDTO
    {
        public Int64 TPS_pkeyID { get; set; }
        public Int64 TPS_TaskID { get; set; }
        public Boolean? TPS_MeasurementBidStatus { get; set; }
        public Boolean? TPS_MeasurementCompletionStatus { get; set; }
        public Boolean? TPS_LoadPhotosCompletionStatus { get; set; }
        public Decimal? TPS_PhotoRequirementMin { get; set; }
        public Decimal? TPS_PhotoRequirementMax { get; set; }
        public Boolean? TPS_PhotoRequirementStatus { get; set; }
        public Decimal? TPS_Inspection { get; set; }
        public Decimal? TPS_Bid { get; set; }
        public Decimal? TPS_BidMeasurement { get; set; }
        public Decimal? TPS_CompletionBefore { get; set; }
        public Decimal? TPS_CompletionDuring { get; set; }
        public Decimal? TPS_CompletionAfter { get; set; }
        public Decimal? TPS_CompletionLoad { get; set; }
        public Decimal? TPS_CompletionMeasurement { get; set; }
    }
    public class TaskPhotoSetting
    {
        public Int64 TPS_pkeyID { get; set; }
        public Int64 TPS_TaskID { get; set; }
        public Boolean? TPS_MeasurementBidStatus { get; set; }
        public Boolean? TPS_MeasurementCompletionStatus { get; set; }
        public Boolean? TPS_LoadPhotosCompletionStatus { get; set; }
        public Decimal? TPS_PhotoRequirementMin { get; set; }
        public Decimal? TPS_PhotoRequirementMax { get; set; }
        public Boolean? TPS_PhotoRequirementStatus { get; set; }
        public Decimal? TPS_Inspection { get; set; }
        public Decimal? TPS_Bid { get; set; }
        public Decimal? TPS_BidMeasurement { get; set; }
        public Decimal? TPS_CompletionBefore { get; set; }
        public Decimal? TPS_CompletionDuring { get; set; }
        public Decimal? TPS_CompletionAfter { get; set; }
        public Decimal? TPS_CompletionLoad { get; set; }
        public Decimal? TPS_CompletionMeasurement { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
    }
}