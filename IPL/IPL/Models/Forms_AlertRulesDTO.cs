﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Forms_AlertRulesDTO
    {
        public Int64 AlertRuleId { get; set; }
        public string AlertRule_Operator { get; set; }
        public Int64 AlertRule_Value { get; set; }       
        public bool AlertRule_IsActive { get; set; }
        public DateTime AlertRule_CreatedOn { get; set; }
        public Int64 UserId { get; set; }
        public Int64 QuestionId { get; set; }
        public Int64 FormId { get; set; }
        public Int16 Type { get; set; }
        public bool AlertRule_IsDelete { get; set; }

    }
}