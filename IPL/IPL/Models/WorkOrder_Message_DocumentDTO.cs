﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class WorkOrder_Message_DocumentDTO
    {
        public Int64 Wo_Msg_Doc_PkeyId { get; set; }
        public Int64? Wo_Msg_Doc_Wo_ID { get; set; }
        public String Wo_Msg_Doc_File_Path { get; set; }
        public String Wo_Msg_Doc_File_Size { get; set; }
        public String Wo_Msg_Doc_File_Name { get; set; }
        public String Wo_Msg_Doc_BucketName { get; set; }
        public String Wo_Msg_Doc_ProjectID { get; set; }
        public String Wo_Msg_Doc_Object_Name { get; set; }
        public String Wo_Msg_Doc_Folder_Name { get; set; }
        public String Wo_Msg_Doc_UploadedBy { get; set; }
        public Int64? Wo_Msg_Doc_Processor_ID { get; set; }
        public Int64? Wo_Msg_Doc_Contractor_ID { get; set; }
        public Int64? Wo_Msg_Doc_Cordinator_ID { get; set; }
        public Int64? Wo_Msg_Doc_Client_ID { get; set; }
        public String Wo_Msg_Doc_IPLNO { get; set; }
        public Int64? Wo_Folder_File_Master_FKId { get; set; }
        public Int64? Wo_Msg_Doc_Ch_ID { get; set; }
        public Int64? Wo_Msg_Doc_Company_Id { get; set; }
        public Boolean? Wo_Msg_Doc_IsActive { get; set; }
        public Boolean? Wo_Msg_Doc_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
        public int? Wo_Msg_Doc_Type { get; set; }


    }
    public class WorkOrder_Message_Document
    {
        public String Wo_Msg_Doc_PkeyId { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}