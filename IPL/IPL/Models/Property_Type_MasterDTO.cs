﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPLApp.Models
{
    public class Property_Type_MasterDTO
    {
        public Int64 PT_PkeyID { get; set; }
        public string PT_Name { get; set; }
        public Boolean? PT_IsActive { get; set; }
        public Boolean? PT_IsDelete { get; set; }
        public int Type { get; set; }
        public Int64 UserID { get; set; }
        public string WhereClause { get; set; }
        public string FilterData { get; set; }
    }

    public class Property_Type_Master
    {
        public String PT_PkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}