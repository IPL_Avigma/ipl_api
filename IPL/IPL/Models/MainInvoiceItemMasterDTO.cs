﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPLApp.Models
{
    public class MainInvoiceItemMasterDTO
    {
        public Int64 Item_pkeyID { get; set; }
        public String Item_Name { get; set; }
        public int? Item_Bid { get; set; }
        public Boolean? Item_AlwaysShowClientWo { get; set; }
        public Boolean? Item_RequiredClientWO { get; set; }
        public Int64? Item_LotSize { get; set; }
        public String Item_Through { get; set; }
        public int? Item_AutoInvoiceClient { get; set; }
        public int? Item_Active { get; set; }
        public Boolean? Item_IsActive { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
    }
    public class MainInvoiceItem
    {
        public String Item_pkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}