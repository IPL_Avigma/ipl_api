﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Client_Result_PhotoDTO
    {
        public Int64 Client_Result_Photo_ID { get; set; }
        public Int64? Client_Result_Photo_Wo_ID { get; set; }
        public String Client_Result_Photo_FileName { get; set; }
        public String Client_Result_Photo_FilePath { get; set; }
        public Boolean? Client_Result_Photo_IsActive { get; set; }
        public Boolean? Client_Result_Photo_IsDelete { get; set; }
        public String Client_Result_Photo_FileType { get; set; }
        public String Client_Result_Photo_FileSize { get; set; }
        public Int64? Client_Result_Photo_TaskId { get; set; }
        public Int64? Client_Result_Photo_Invoice_Id { get; set; }
        public Int64? Client_Result_Photo_Bid_Id { get; set; }
        public String Client_Result_Photo_TaskLable_Name { get; set; }
        public int? Client_Result_Photo_Lable_Status { get; set; }
        public String Client_Result_Photo_BucketName { get; set; }
        public String Client_Result_Photo_ProjectID { get; set; }
        public String Client_Result_Photo_objectName { get; set; }
        public String Client_Result_Photo_localPath { get; set; }
        public String Client_Result_Photo_folder { get; set; }
        public String Client_Result_Photo_FolderName { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
        public String IPLNO { get; set; }

        public int? Client_Result_Photo_StatusType { get; set; }
        public Int64? Client_Result_Photo_Task_Bid_pkeyID { get; set; }
        public Int64? Client_Result_Photo_Damage_ID { get; set; }
        public Int64? CRP_New_pkeyId { get; set; }

        public Int64 Client_Result_Photo_Ch_ID { get; set; }

        public Int64? Client_Result_Photo_WorkOrderPhotoLabel_ID { get; set; }
        public String Client_Result_Photo_UploadBy { get; set; }
        public DateTime? Client_Result_Photo_UploadTimestamp { get; set; }
        public DateTime? Client_Result_Photo_DateTimeOriginal { get; set; }
        public String Client_Result_Photo_GPSLatitude { get; set; }
        public String Client_Result_Photo_GPSLongitude { get; set; }
        public String Client_Result_Photo_Make { get; set; }
        public String Client_Result_Photo_Model { get; set; }
        public String Client_Result_Photo_UploadFrom { get; set; }
        public String Client_Result_Photo_FlaggedTo { get; set; }
        public String Client_Result_Photo_Caption { get; set; }

        public int? Client_Result_Photo_Type { get; set; }

        public int? Client_PageCalled { get; set; }

        public Int64 Client_TMF_Task_CustPkey { get; set; }

        public Int64 Client_TMF_Task_ClientPkey { get; set; }
        public string Client_Result_File_Desc { get; set; }
        public Boolean? User_Doc_AlertUser { get; set; }
        public String User_Doc_RecievedDate { get; set; }
        public String User_Doc_Exp_Date { get; set; }
        public String User_BackgroundCheckProvider { get; set; }
        public string WorkOrder_ID_Data { get; set; }
        public DateTime? Client_Result_Photo_CreatedOn { get; set; }
        public Boolean? Fold_Is_AutoAssign { get; set; }
        public Int64? Folder_File_Master_FKId { get; set; }
        public Int64? Wo_Office_Doc_Company_Id { get; set; }
        public Int64? Wo_Msg_Doc_Processor_ID { get; set; }
        public Int64? Wo_Msg_Doc_Contractor_ID { get; set; }
        public Int64? Wo_Msg_Doc_Cordinator_ID { get; set; }
        public Int64? Wo_Msg_Doc_Client_ID { get; set; }
        public String Wo_Msg_Doc_IPLNO { get; set; }
        public int? ContentType { get; set; }
        public int? ReqType { get; set; }
        public Int64? Client_Res_Photo_ID { get; set; }
        public List<Dataitem> Dataitems { get; set; }
        public String Client_Result_Photo_GPSAltitude { get; set; }
        public int? Client_Result_Photo_MeteringMode { get; set; }
        public int? Client_Result_Photo_Seq { get; set; }
        public string Client_Result_PhotoLabel_Name { get; set; }
    }

    public class Ret_UPload_Client_Result_PhotoDTO
    {
        public int Status { get; set; }
        public int Client_Result_Photo_ID { get; set; }
        public string Messsage { get; set; }


    }

    public class UPload_Client_Result_PhotoDTO
    {
        public Int64 Client_Result_Photo_ID { get; set; }
        public Int64? Client_Result_Photo_Wo_ID { get; set; }
        public String Client_Result_Photo_FileName { get; set; }
        public String Client_Result_Photo_FilePath { get; set; }
        public Boolean? Client_Result_Photo_IsActive { get; set; }

        public String Client_Result_Photo_FileType { get; set; }
        public String Client_Result_Photo_FileSize { get; set; }

        public int? Client_Result_Photo_StatusType { get; set; }

        public String Client_Result_Photo_BucketName { get; set; }
        public String Client_Result_Photo_objectName { get; set; }

        public String Client_Result_Photo_folder { get; set; }
        public String Client_Result_Photo_FolderName { get; set; }
        public String Client_Result_Photo_Make { get; set; }
        public String Client_Result_Photo_Model { get; set; }

        public Int64? UserID { get; set; }
        public int Type { get; set; }
        public String IPLNO { get; set; }

        public int? ContentType { get; set; }
        public int? ReqType { get; set; }

        public String WorkOrder_No { get; set; }

        public Int64 Inst_Doc_Inst_Ch_ID { get; set; }

        public int? Client_PageCalled { get; set; }

        public Int64 Client_TMF_Task_CustPkey { get; set; }

        public Int64 Client_TMF_Task_ClientPkey { get; set; }

        public Int64? Client_Result_Photo_Task_Bid_pkeyID { get; set; }

        public Int64? Client_Result_Photo_Ch_ID { get; set; }
        public String Api_Error { get; set; }

        public string Client_Result_File_Desc { get; set; }

        public String Client_Result_Photo_GPSLatitude { get; set; }
        public String Client_Result_Photo_GPSLongitude { get; set; }



        public Int64 User_pkeyID { get; set; }
        public String User_FirstName { get; set; }
        public String User_LastName { get; set; }
        public String User_Address { get; set; }
        public String User_City { get; set; }
        public String User_Password { get; set; }
        public Int64? User_Zip { get; set; }
        public String User_CellNumber { get; set; }
        public String User_CompanyName { get; set; }
        public String User_LoginName { get; set; }
        public String User_Email { get; set; }
        public String User_ImagePath { get; set; }
        public String User_Token_val { get; set; }

        public Int64? User_State_strval { get; set; }
        public String User_Doc_RecievedDate { get; set; }
        public String User_Doc_Exp_Date { get; set; }
        public Boolean? User_Doc_AlertUser { get; set; }
        public String User_BackgroundCheckProvider { get; set; }
        public String WorkOrder_ID_Data { get; set; }
        public Boolean? Fold_Is_AutoAssign { get; set; } // Added By Dipali
        public Int64? Folder_File_Master_FKId { get; set; }
        public Int64? Wo_Office_Doc_Company_Id { get; set; }
        public Int64? Wo_Msg_Doc_Processor_ID { get; set; }
        public Int64? Wo_Msg_Doc_Contractor_ID { get; set; }
        public Int64? Wo_Msg_Doc_Cordinator_ID { get; set; }
        public Int64? Wo_Msg_Doc_Client_ID { get; set; }
        public Int64? Wo_Msg_Doc_IPLNO { get; set; }

        public String Client_Result_Photo_GPSAltitude { get; set; }
        public int? MeteringMode { get; set; }
        public int? Client_Result_Photo_Seq { get; set; }
        public string Client_Result_PhotoLabel_Name { get; set; }
    }



    public class photosRoots
    {

        public string workOrderNumber { get; set; }
        public string IPLNO { get; set; }

        public string filePath { get; set; }

        public string fileName { get; set; }
        public int? Client_Result_Photo_StatusType { get; set; }
        public int Type { get; set; }
    }


    public class Rootclientresultbefore
    {
        public List<ImageArray> ImageArray { get; set; }
        public int Task_Bid_TaskID { get; set; }
        public Int64 Client_Result_Photo_Task_Bid_pkeyID { get; set; }
        public int Client_Result_Photo_StatusType { get; set; }
    }

    public class ImageArray
    {
        public int Client_Result_Photo_ID { get; set; }
    }

    public class TaskPhotoDetails
    {

        public Int64 Task_Bid_pkeyID { get; set; }
        public string Task_Photo_Label_Name { get; set; }



        public List<TaskPhotoButtonDetails> Items { get; set; }
    }
    public class TaskPhotoButtonDetails
    {
        public int Task_Photo_Button_Id { get; set; }
        public string Task_Photo_Button_Name { get; set; }



        public List<GetClientResultPhoto_DTO> Items { get; set; }
    }

    public class WorkOrder_HistoryData_DTO
    {
        public Int64 WorkOrder_ID { get; set; }
        public int Type { get; set; }
        public String WorkOrder_Data { get; set; }
        public Int64 UserID { get; set; }
        public String IPLNO { get; set; }
        public String workOrderNumber { get; set; }
        public String CreatedOn { get; set; }

    }

    public class ClientResultPhotoRef
    {
        public Int64 CRP_New_pkeyId { get; set; }
        public Int64? CRP_New_Photo_ID { get; set; }
        public Int64? CRP_New_Bid_Id { get; set; }
        public Int64? CRP_New_Inv_Id { get; set; }
        public Int64? CRP_New_Damage_Id { get; set; }
        public Int64? CRP_New_Status_Type { get; set; }
        public Int64? CRP_New_Task_Bid_pkeyID { get; set; }
        public Boolean? CRP_New_IsActive { get; set; }
        public Int64? CRP_New_Task_ID { get; set; }
        public Int64? CRP_WorkOrderPhotoLabel_ID { get; set; }
        public Int64? CRP_Inspection_Id { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
        public Int64 Client_Result_Photo_Wo_ID { get; set; }

    }

    public class GetClientResultPhoto_DTO
    {
        public Int64 Client_Result_Photo_ID { get; set; }
        public Int64? Client_Result_Photo_Wo_ID { get; set; }
        public String Client_Result_Photo_FileName { get; set; }
        public String Client_Result_Photo_FilePath { get; set; }
        public String Client_Result_Photo_FileType { get; set; }
        public String Client_Result_Photo_FileSize { get; set; }

        public Int64? Client_Result_Photo_TaskId { get; set; }
        public Int64? Client_Result_Photo_Invoice_Id { get; set; }
        public Int64? Client_Result_Photo_Bid_Id { get; set; }
        public String Client_Result_Photo_TaskLable_Name { get; set; }
        public Int64? CRP_New_pkeyId { get; set; }
        public DateTime? Client_Result_Photo_CreatedOn { get; set; }
        public String Client_Result_Photo_FolderName { get; set; }

        public int? Client_Result_Photo_StatusType { get; set; }
        public String Client_Result_Photo_UploadBy { get; set; }
        public String Client_Result_Photo_localPath { get; set; }
        public String Client_Result_Photo_objectName { get; set; }
        public String Client_Result_Photo_BucketName { get; set; }
        public String Client_Result_Photo_ProjectID { get; set; }
        public String Client_Result_Photo_folder { get; set; }
        public String Client_Result_Photo_GPSLatitude { get; set; }
        public String Client_Result_Photo_GPSLongitude { get; set; }
        public String Client_Result_Photo_Make { get; set; }
        public String Client_Result_Photo_Model { get; set; }
        public String Client_Result_Photo_UploadFrom { get; set; }
        public DateTime? Client_Result_Photo_UploadTimestamp { get; set; }
        public Boolean? Client_Result_Photo_IsActive { get; set; }
        public int Type { get; set; }
        public Int64 Client_TMF_Task_CustPkey { get; set; }
        public Int64 Client_TMF_Task_ClientPkey { get; set; }
        public string Client_Result_File_Desc { get; set; }
        public Int64? Client_Result_Photo_Damage_ID { get; set; }
        public Int64? Client_Result_Photo_WorkOrderPhotoLabel_ID { get; set; }
        public String IPLNO { get; set; }
        public int valtype { get; set; }
        public int? Client_Result_Photo_Seq { get; set; }
        public DateTime? Client_Result_Photo_DateTimeOriginal { get; set; }

    }

    public class Client_Result_Photo_HistoryDTO
    {
        public Int64 Client_Result_Photo_Wo_ID { get; set; }
        public String Client_Result_Photo_IPLNo { get; set; }
        public Boolean IsTabHidden { get; set; }

    }
    public class Dataitem
    {
        public string Client_Result_Photo_FileName { get; set; }
        public int Client_Result_Photo_ID { get; set; }
        public int CRP_New_pkeyId { get; set; }
    }
    public class Client_Result_Photo_HistoryMobileDTO
    {
        public List<TaskPhotoDetails> Bid { get; set; }
        public List<TaskPhotoDetails> Damage { get; set; }


    }

    public class Client_Result_Photo_Info
    {
        public Int64? Client_Result_Photo_ID { get; set; }
        public DateTime? Client_Result_Photo_UploadTimestamp { get; set; }
        public DateTime? Client_Result_Photo_DateTimeOriginal { get; set; }

        public String Working_Hour { get; set; }

        public decimal? Max_Distance { get; set; }

    }

    public class Client_Result_Photo_Temp
    {
        public Int64 Client_Res_Photo_PkeyID { get; set; }
        public Int64? Client_Res_Photo_ID { get; set; }
        public String IPLNO { get; set; }
        public Int64? Client_Result_Photo_Wo_ID { get; set; }
        public String Client_Result_Photo_FileName { get; set; }
        public String Client_Result_Photo_FilePath { get; set; }
        public int? Client_Result_Photo_Type { get; set; }
        public Int64? Client_Result_Photo_Ch_ID { get; set; }
        public int? ReqType { get; set; }
        public int? ContentType { get; set; }
        public String Client_Result_Photo_GPSLongitude { get; set; }
        public String Client_Result_Photo_GPSLatitude { get; set; }
        public string Client_Result_Photo_DateTimeOriginal { get; set; }
        public DateTime? Client_Result_Photo_DateTimeOriginal_Dt { get; set; }
        public int? Client_Result_Photo_StatusType { get; set; }
        public Int64? Client_Result_Photo_Task_Bid_pkeyID { get; set; }
        public String Client_Result_Photo_GPSAltitude { get; set; }
        public String Client_Result_Photo_Model { get; set; }
        public String Client_Result_Photo_Make { get; set; }
        public int? Client_Result_Photo_MeteringMode { get; set; }
        public int? Client_Result_Photo_Seq { get; set; }
        public int? MeteringMode { get; set; }
        public int Orginal_Type { get; set; }
        public Boolean? Client_Result_Photo_IsActive { get; set; }
        public Boolean? Client_Result_Photo_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
    }

    public class Client_Result_Photo_Data
    {
        public Int64? Client_Result_Photo_Wo_ID { get; set; }
        public String Client_Result_Photo_FilePath { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
    }
    public class StatusWIsePhoto_DTO
    {
        public Int64? Client_Result_Photo_ID { get; set; }
        public String Client_Result_Photo_FileName { get; set; }
        public string Client_Result_Photo_FilePath { get; set; }
        public int Client_Result_Photo_StatusType { get; set; }
        public string LableName { get; set; }
        public string TaskName { get; set; }
    }
    public class PhotoArchive_DTO
    {
        public String Filename { get; set; }
        public string FilePath { get; set; }
        public string TaskName { get; set; }
        public string LabelName { get; set; }
        public int Count { get; set; }
    }
    public class ClientResultPhotoDTO
    {
        public Int64? Client_Result_Photo_Wo_ID { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
        public string Client_Result_Photo_ID_Comma { get; set; }
        public string PhotoJsonArray { get; set; }
    }

}