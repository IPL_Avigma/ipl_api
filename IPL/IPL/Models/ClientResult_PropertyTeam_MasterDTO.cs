﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class ClientResult_PropertyTeam_MasterDTO
    {
        public Int64 CRPT_PkeyID { get; set; }
        public Int64? CRPT_WO_ID { get; set; }  
        public Boolean? CRPT_IsActive { get; set; }
        public Boolean? CRPT_IsDelete { get; set; }
        public Boolean? CRPT_InspectionVendor { get; set; }
        public string CRPT_PMVendor { get; set; }
        public string CRPT_PrimaryVendor { get; set; }
        public string CRPT_GeneralContractor { get; set; }
        public string CRPT_SalesSpecialist { get; set; }
        public string CRPT_Investor { get; set; }
        public string CRPT_InvestorCaseNumber { get; set; }
        public string CRPT_ServicerFamily { get; set; }
        public string  CRPT_ServicerLoan { get; set; }


        public Int64 UserID { get; set; }
        public int Type { get; set; }
    }
    public class ClientResult_PropertyTeam_Master
    {
        public String CRPT_PkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}