﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Message_User_MasterDTO
    {
        public Int64 MUM_PkeyID { get; set; }
        public Int64 MUM_User_PkeyID { get; set; }
        public Int64 MUM_User_Group_PkeyID { get; set; }
        public int MUM_User_Status { get; set; }
        public int MUM_User_Flag { get; set; }
        public Boolean MUM_User_IsActive { get; set; }
        public Boolean MUM_User_IsDelete { get; set; }
        public int Type { get; set; }
        public Int64 UserID { get; set; }
        public String FilterUserName { get; set; }
        public string User_FirstName { get; set; }
        public int Pagenumber { get; set; }
        public int Rownumber { get; set; }
    }
}