﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class EmailWorkOderDTO
    {
        public Int64 workOrder_ID { get; set; }
        public String workOrderNumber { get; set; }
        public String address1 { get; set; }
        public String city { get; set; }
        public String state { get; set; }
        public String zip { get; set; }
        public DateTime? dueDate { get; set; }
        public Int64? WorkType { get; set; }
        public Int64? Company { get; set; }
        public String Comments { get; set; }
        public String ContractorName { get; set; }
        public String Cordinator_Name { get; set; }
        public String Processor_Name { get; set; }
        public String ContractorEmail { get; set; }
        public String CordinatorEmail { get; set; }
        public String ProcessorEmail { get; set; }
        public String WT_WorkType { get; set; }
        public String Client_Company_Name { get; set; }
        public int? Val_Type { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
        public String User_CreatedBy { get; set; }
        public String User_FirstName { get; set; }
        public String Cordinator_Phone { get; set; }
        public String Inst_Ch_Comand_Mobile { get; set; }
        public String Client_Result_Photo_FilePath { get; set; }
        public String IPLNO { get; set; }

    }

    public class Email_Message_DTO
    {
        public String Subject { get; set; }
        public String Body { get; set; }
    }

    }