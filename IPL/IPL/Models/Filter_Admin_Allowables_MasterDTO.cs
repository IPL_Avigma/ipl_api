﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Filter_Admin_Allowables_MasterDTO
    {
        public Int64 Filter_Admin_Allowable_PkeyId { get; set; }
        public String Filter_Admin_Allowable_Name { get; set; }
        public String Filter_Admin_Allowable_CreatedBy { get; set; }
        public String Filter_Admin_Allowable_ModifiedBy { get; set; }
        public DateTime? Filter_Admin_Allowable_StartDate { get; set; }
        public DateTime? Filter_Admin_Allowable_EndDate { get; set; }
        public Int64? Filter_Admin_Allowable_OverallAllowables { get; set; }
        public Int64? Filter_Admin_Allowable_CompanyId { get; set; }
        public Int64? Filter_Admin_Allowable_UserId { get; set; }
        public Boolean? Filter_Admin_Allowable_IsActive { get; set; }
        public Boolean? Filter_Admin_Allowable_IsDelete { get; set; }
        public Boolean? Filter_Admin_Allowable_Isallowable { get; set; }
        public Int64 UserId { get; set; }
        public int Type { get; set; }


    }
}