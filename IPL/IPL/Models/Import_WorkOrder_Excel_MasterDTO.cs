﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Import_WorkOrder_Excel_MasterDTO
    {
        public Int64 IMP_PkeyId { get; set; }
        public String workOrderNumber { get; set; }
        public String WorkType { get; set; }
        public String address1 { get; set; }
        public String city { get; set; }
        public String state { get; set; }
        public String zip { get; set; }
        public String Company { get; set; }
        public String Contractor { get; set; }
        public String Cordinator { get; set; }
        public String Processor { get; set; }
        public String Category { get; set; }
        public String Loan_Info { get; set; }
        public String Loan_Number { get; set; }
        public String Customer_Number { get; set; }
        public String BATF { get; set; }
        public String ISInspection { get; set; }
        public String Lotsize { get; set; }
        public String Rush { get; set; }
        public String Lock_Code { get; set; }
        public String Lock_Location { get; set; }
        public String Key_Code { get; set; }
        public String Gate_Code { get; set; }
        public String Broker_Info { get; set; }
        public String Received_Date { get; set; }
        public String startDate { get; set; }
        public String dueDate { get; set; }
        public String clientDueDate { get; set; }
        public String Comments { get; set; }
        public int? import_type { get; set; }
        public String IMP_File_Path { get; set; }
        public String IMP_File_Name { get; set; }
        public Boolean? IMP_IsActive { get; set; }
        public Boolean? IMP_IsDelete { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
        public List<ExcelDataDTO> ExcelDataDTO { get; set; }

        public String gpsLatitude { get; set; }
        public String gpsLongitude { get; set; }

    }
    public class ExcelDataDTO
    {
        public Int64 UserID { get; set; }
        public int Type { get; set; }
        public String ExcelData { get; set; }

    }
}