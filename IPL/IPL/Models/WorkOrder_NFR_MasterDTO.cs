﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class WorkOrder_NFR_MasterDTO
    {
        public Int64 WNFR_PkeyID { get; set; }
        public String WNFR_Order { get; set; }
        public String WNFR_To { get; set; }
        public DateTime? WNFR_Date_Rec_Contact { get; set; }
        public DateTime? WNFR_Due_Date { get; set; }
        public String WNFR_MTG_Co { get; set; }
        public String WNFR_Account { get; set; }
        public String WNFR_LoanType { get; set; }
        public String WNFR_MTGR_Name { get; set; }
        public String WNFR_MTGR_Address { get; set; }
        public String WNFR_Requierment_Name { get; set; }
        public String WNFR_Requierment_Details { get; set; }
        public String WNFR_WorkType { get; set; }
        public String WNFR_gpsLatitude { get; set; }
        public String WNFR_gpsLongitude { get; set; }
        public Int64? WNFR_ImportMaster_Pkey { get; set; }
        public String WNFR_Import_File_Name { get; set; }
        public String WNFR_Import_FilePath { get; set; }
        public String WNFR_Import_Pdf_Name { get; set; }
        public String WNFR_Import_Pdf_Path { get; set; }
        public String WNFR_City { get; set; }
        public String WNFR_State { get; set; }
        public String WNFR_Zip { get; set; }
        public String WNFR_wo_id { get; set; }
        public String WNFR_username { get; set; }
        public String WNFR_keycode { get; set; }
        public String WNFR_lockbox { get; set; }
        public String WNFR_address { get; set; }
        public String WNFR_NFR_ID { get; set; }
        public Boolean? WNFR_IsProcessed { get; set; }
        public Boolean? WNFR_IsActive { get; set; }
        public Boolean? WNFR_IsDelete { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
    }

    public class WorkOrder_NFR_Master
    {
        public String WNFR_PkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}