﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class ZipMasterDTO
    {
        public Int64 Zip_ID { get; set; }
        public Int64? Zip_zip { get; set; }
        public String Zip_lat { get; set; }
        public String Zip_lng { get; set; }
        public String Zip_city { get; set; }
        public String Zip_state_id { get; set; }
        public String Zip_state_name { get; set; }
        public String Zip_zcta { get; set; }
        public String Zip_parent_zcta { get; set; }
        public String Zip_population { get; set; }
        public String Zip_density { get; set; }
        public String Zip_county_fips { get; set; }
        public String Zip_county_name { get; set; }
        public String Zip_county_weights { get; set; }
        public String Zip_county_names_all { get; set; }
        public String Zip_county_fips_all { get; set; }
        public String Zip_imprecise { get; set; }
        public String Zip_military { get; set; }
        public String Zip_timezone { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
        public Int64? ZipState_id { get; set; }
    }
}