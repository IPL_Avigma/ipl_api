﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPLApp.Models
{
    public class Occupancy_Status_MasterDTO
    {
     
        public Int64 OS_PkeyID { get; set; }
        public string OS_Name { get; set; }
        public Boolean? OS_IsActive { get; set; }
        public Boolean? OS_IsDelete { get; set; }
        public int Type { get; set; }
        public Int64 UserID { get; set; }
        public string WhereClause { get; set; }
        public string FilterData { get; set; }
    }

    public class Occupancy_Status_Master
    {
        public String OS_PkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}