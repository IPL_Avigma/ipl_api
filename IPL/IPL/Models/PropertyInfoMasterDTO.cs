﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class PropertyInfoMasterDTO
    {
        public Int64 PCR_Pro_PkeyID { get; set; }
        public Int64? PCR_Prop_WO_ID { get; set; }
        public Int64? PCR_Prop_MasterID { get; set; }
        public int? PCR_Prop_ValType { get; set; }
        public string PCR_Prop_ForSale { get; set; } //ch
        public string PCR_Prop_Sold { get; set; } //ch
        public String PCR_Prop_Broker_Phone { get; set; }
        public String PCR_Prop_Broker_Name { get; set; } //add 
        public String PCR_Prop_Maintained { get; set; } //add 
        public String PCR_Prop_Maintained_ByOther { get; set; } //ch
        public Boolean? PCR_Prop_Maintained_Items_Utilities { get; set; }
        public Boolean? PCR_Prop_Maintained_Items_Grass { get; set; }
        public Boolean? PCR_Prop_Maintained_Items_Snow_Removal { get; set; }
        public Boolean? PCR_Prop_Maintained_Items_Interior_Repaiars { get; set; }
        public Boolean? PCR_Prop_Maintained_Items_Exterior_Repairs { get; set; }
        public String PCR_Prop_Active_Listing { get; set; } //ch
        public String PCR_Prop_Basement_Present { get; set; } //ch
        public String PCR_Prop_Property_Type_Vacant_Land { get; set; }
        public Boolean? PCR_Prop_Property_Type_Single_Family { get; set; }
        public Boolean? PCR_Prop_Property_Type_Multi_Family { get; set; }
        public Boolean? PCR_Prop_Property_Type_Mobile_Home { get; set; }
        public Boolean? PCR_Prop_Property_Type_Condo { get; set; }
        public string PCR_Prop_Permit_Required { get; set; }//ch
        public string PCR_Prop_Permit_Number { get; set; }
        public String PCR_OurBuildings_Garages { get; set; }
        public String PCR_OurBuildings_Sheds { get; set; }
        public String PCR_OurBuildings_Caports { get; set; }
        public String PCR_OurBuildings_Bams { get; set; }
        public String PCR_OurBuildings_Pool_House { get; set; }
        public String PCR_OurBuildings_Other_Building { get; set; }
        public String PCR_Prop_Garage { get; set; }
        public string PCR_Prop_Condo_Association_Property { get; set; } //ch
        public string PCR_HOA_Name { get; set; }
        public string PCR_HOA_Phone { get; set; }
        public string PCR_Prop_No_Of_Unit { get; set; } //ch
        public string PCR_Prop_Common_Entry { get; set; }
        public String PCR_Prop_Unit1 { get; set; }
        public string PCR_Prop_Unit1_Occupied { get; set; } //ch
        public String PCR_Prop_Unit2 { get; set; }
        public string PCR_Prop_Unit2_Occupied { get; set; } //ch
        public String PCR_Prop_Unit3 { get; set; }
        public string PCR_Prop_Unit3_Occupied { get; set; } //ch
        public String PCR_Prop_Unit4 { get; set; }
        public string PCR_Prop_Unit4_Occupied { get; set; } //ch
        public string PCR_Prop_Property_Vacant { get; set; } // new 
        public string PCR_Prop_Property_Vacant_Notes { get; set; }
        public Boolean? PCR_Prop_Occupancy_Verified_Contact_Owner { get; set; }
        public Boolean? PCR_Prop_Occupancy_Verified_Personal_Visible { get; set; }
        public Boolean? PCR_Prop_Occupancy_Verified_Neighbor { get; set; }
        public Boolean? PCR_Prop_Occupancy_Verified_Utilities_On { get; set; }
        public Boolean? PCR_Prop_Occupancy_Verified_Visual { get; set; }
        public Boolean? PCR_Prop_Occupancy_Verified_Direct_Con_Tenant { get; set; }
        public Boolean? PCR_Prop_Occupancy_Verified_Direct_Con_Mortgagor { get; set; }
        public Boolean? PCR_Prop_Occupancy_Verified_Direct_Con_Unknown { get; set; }
        public Boolean? PCR_Prop_Owner_Maintaining_Property { get; set; }
        public Boolean? PCR_Prop_Other { get; set; }


        // use less


        //public Boolean? PCR_Prop_Maintained_Not_Maintained { get; set; }
        //public Boolean? PCR_Prop_Maintained_By_Broker { get; set; }
        //public Boolean? PCR_Prop_Maintained_ByBroker_HOA { get; set; }
        //public Boolean? PCR_Prop_Maintained_ByHOA { get; set; }
        //public Boolean? PCR_Prop_Garage_Attached { get; set; }
        //public Boolean? PCR_Prop_Garage_Carport { get; set; }
        //public Boolean? PCR_Prop_Garage_Detached { get; set; }
        //public Boolean? PCR_Prop_Garage_None { get; set; }
        //public Boolean? PCR_Prop_Property_Vacant_Yes { get; set; }
        //public Boolean? PCR_Prop_Property_Vacant_No { get; set; }
        //public Boolean? PCR_Prop_Property_Vacant_Land { get; set; }
        //public Boolean? PCR_Prop_Property_Vacant_Bad_Address { get; set; }
        //public String PCR_Prop_Property_Vacant_Bad_Address_Commend { get; set; }
        //public Boolean? PCR_Prop_Property_Vacant_Partial { get; set; }
       
        public Boolean? PRC_Prop_IsActive { get; set; }
        public Boolean? PRC_Prop_IsDelete { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
        public string PCR_Prop_Property { get; set; }
        public DateTime? PRC_Prop_dateCompleted { get; set; }
        public String PRC_Prop_PropertyVacantBadAddressProvide_dtls { get; set; }
        public String PRC_Prop_badAddress { get; set; }
        public String PRC_Prop_orderCompleted { get; set; }

        public Int64 fwo_pkyeId { get; set; }

    }
    public class PropertyInfoDTO
    {
        public String PCR_Pro_PkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}