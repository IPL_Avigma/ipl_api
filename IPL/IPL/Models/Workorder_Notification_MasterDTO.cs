﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Workorder_Notification_MasterDTO
    {
        public Int64 WN_Pkey_Id { get; set; }
        public Int64? WN_UserId { get; set; }
        public Int64? WN_WoId { get; set; }
        public String WN_Title { get; set; }
        public String WN_Message { get; set; }
        public Boolean? WN_IsRead { get; set; }
        public Boolean? WN_IsActive { get; set; }
        public Boolean? WN_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
    }
}