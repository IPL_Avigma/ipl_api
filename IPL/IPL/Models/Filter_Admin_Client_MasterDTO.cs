﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Filter_Admin_Client_MasterDTO
    {
        public Int64 Client_Filter_PkeyID { get; set; }
        public String Client_Filter_ClientName { get; set; }
        public String Client_Filter_Address { get; set; }
        public String Client_Filter_City { get; set; }
        public Int64? Client_Filter_State { get; set; }
        public String Client_Filter_CreatedBy { get; set; }
        public String Client_Filter_ModifiedBy { get; set; }
        public Boolean? Client_Filter_CLTIsActive { get; set; }
        public Boolean? Client_Filter_IsActive { get; set; }
        public Boolean? Client_Filter_IsDelete { get; set; }
        public Int64? Client_Filter_CompanyID { get; set; }
        public Int64? Client_Filter_UserID { get; set; }
        public int? Type { get; set; }
        public Int64? UserId { get; set; }
    }
}