﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPLApp.Models
{
    public class SearchMasterDTO
    {
        public String WhereClause { get; set; }
        public String FilterData { get; set; }
        public Int64? UserID { get; set; }
        public Int64? MenuID { get; set; }
    }

    public class FilterMasterDTO
    {
        public String FilterData { get; set; }
    }
}