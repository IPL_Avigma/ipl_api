﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class MSI_GrassPcr_Forms_Master_DTO
    {
        public Int64 MSI_Grass_PkeyId { get; set; }
        public Int64 MSI_Grass_WO_Id { get; set; }
        public Int64 MSI_Grass_CompanyID { get; set; }
        public String MSI_Grass_SubjectProperty { get; set; }
        public String MSI_Grass_ConditionReport { get; set; }
        public String MSI_Grass_BidItems { get; set; }
        public String MSI_Grass_PhotoManager { get; set; }
        public String MSI_Grass_Comments { get; set; }
        public String MSI_Grass_FinalReviews { get; set; }
        public Boolean? MSI_Grass_IsActive { get; set; }
        public Boolean? MSI_Grass_IsDelete { get; set; }
        public Int64 UserID { get; set; }
        public Int64 fwo_pkyeId { get; set; }
        public int Type { get; set; }
    }
}