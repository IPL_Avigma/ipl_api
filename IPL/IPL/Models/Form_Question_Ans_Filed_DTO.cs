﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Form_Question_Ans_Filed_DTO
    {
        public Int64 FileId { get; set; }
        public Int64 QuestionId { get; set; }
        public Int64 AnswerId { get; set; }
        public Int64 QuestionTypeId { get; set; }
        public Int64 OptionId { get; set; }
        public string OptionName { get; set; }
        public Int64 Que_PdfFiels { get; set; }
        public string FilePath { get; set; }
        public string AnswerValue { get; set; }
        public string FieldName { get; set; }
        public string FileName { get; set; }
    }
}