﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Forms_Question_MasterDTO
    {
        public Forms_Question_MasterDTO()
        {
            QuestionType = new HashSet<Forms_QuestionTypeDTO>();
            Options = new HashSet<Forms_Que_Options>();
            PhotoRules = new HashSet<Forms_PhotoRulesDTO>();
            AlertRules = new HashSet<Forms_AlertRulesDTO>();
            ActionRulesMaster = new HashSet<Forms_ActionRules_MasterDTO>();
            FieldRules = new HashSet<Forms_FieldRulesDTO>();
            Questions = new HashSet<Forms_Question_MasterDTO>();
            QuestionAnswers = new HashSet<Forms_Question_AnswersDTO>();
            PreviousAnswers = new HashSet<Forms_Question_AnswersDTO>();
            PCR_History_PreviousAnswers = new HashSet<Forms_Question_AnswersDTO>();
        }
        public Int64 QuestionId { get; set; }
        public string Question { get; set; }
        public Int64 QuestionTypeId { get; set; }
        public Int64 FormId { get; set; }
        public string Instructions { get; set; }
        public bool Que_Show { get; set; }
        public bool Que_OfficeResults { get; set; }
        public bool Que_Required { get; set; }
        public bool Que_FieldResults { get; set; }
        public bool Que_Alert { get; set; }
        public Int32 Que_ClonePrevAnswer { get; set; }
        public bool Que_IsActive { get; set; }
        public bool Que_Camera { get; set; }
        public bool Que_IsDelete { get; set; }
        public Int64 UserId { get; set; }
        public Int64 SequenceNo { get; set; }
        public Int32 TextAreaWidth { get; set; }
        public Int32 TextAreaHeight { get; set; }
        public Int32 TextBoxSize { get; set; }
        public bool QueType_IsMultiple { get; set; }
        public string UpdateField { get; set; }
        public bool Status { get; set; }
        public ICollection<Forms_QuestionTypeDTO> QuestionType { get; set; }
        public ICollection<Forms_Que_Options> Options { get; set; }
        public int Type { get; set; }
        public ICollection<Forms_PhotoRulesDTO> PhotoRules { get; set; }
        public ICollection<Forms_AlertRulesDTO> AlertRules { get; set; }
        public ICollection<Forms_ActionRules_MasterDTO> ActionRulesMaster { get; set; }
        public ICollection<Forms_FieldRulesDTO> FieldRules { get; set; }

        public ICollection<Forms_Question_MasterDTO> Questions { get; set; }
        public ICollection<Forms_Question_AnswersDTO> QuestionAnswers { get; set; }
        public ICollection<Forms_Question_AnswersDTO> PreviousAnswers { get; set; }
        public Int64 Id { get; set; } //universal id

        public Int64 Que_PdfFiels { get; set; }
        public Int64 Que_RefId { get; set; }
        public Int64 Que_ParentId { get; set; }
        public Boolean InvalidFR { get; set; }
        public Int64 InvalidFR_OptionId { get; set; }

        public string Que_Photo_Label_Name { get; set; }
        public int Que_Photo_max_count { get; set; }
        public int Que_Photo_min_count { get; set; }
        public int RuleCount { get; set; }

        public ICollection<Forms_Question_AnswersDTO> PCR_History_PreviousAnswers { get; set; }

    }
}