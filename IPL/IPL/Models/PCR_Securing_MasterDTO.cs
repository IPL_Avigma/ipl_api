﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class PCR_Securing_MasterDTO
    {
        public Int64 PCR_Securing_pkeyId { get; set; }
        public Int64 PCR_Securing_MasterId { get; set; }
        public Int64 PCR_Securing_WO_Id { get; set; }
        public int? PCR_Securing_ValType { get; set; }
        public string PCR_Securing_On_Arrival { get; set; }
        public string PCR_Securing_On_Departure { get; set; }
        public Boolean? PCR_Securing_Not_Secure_Reason_Missing_Doors { get; set; }
        public Boolean? PCR_Securing_Not_Secure_Reason_Door_Open { get; set; }
        public Boolean? PCR_Securing_Not_Secure_Reason_Missing_Locks { get; set; }
        public Boolean? PCR_Securing_Not_Secure_Reason_Broken_Windows { get; set; }
        public Boolean? PCR_Securing_Not_Secure_Reason_Missing_Window { get; set; }
        public Boolean? PCR_Securing_Not_Secure_Reason_Window_Open { get; set; }
        public Boolean? PCR_Securing_Not_Secure_Reason_Broken_Door { get; set; }
        public Boolean? PCR_Securing_Not_Secure_Reason_Bids_Pending { get; set; }
        public Boolean? PCR_Securing_Not_Secure_Reason_Damage_Locks { get; set; }
        public string PCR_Securing_Boarded_Arrival { get; set; }
        public string PCR_Securing_No_Of_First_Floor_Window { get; set; }
        public string PCR_Securing_More_Boarding_Still_Required_OR_Not { get; set; }
        public Boolean? PCR_Securing_IsActive { get; set; }
        public Boolean? PCR_Securing_IsDelete { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }


        public Boolean? PCR_Securing_Depart_Not_Secure_Reason_Missing_Doors { get; set; }
        public Boolean? PCR_Securing_Depart_Not_Secure_Reason_Door_Open { get; set; }
        public Boolean? PCR_Securing_Depart_Not_Secure_Reason_Missing_Locks { get; set; }
        public Boolean? PCR_Securing_Depart_Not_Secure_Reason_Broken_Windows { get; set; }
        public Boolean? PCR_Securing_Depart_Not_Secure_Reason_Missing_Window { get; set; }
        public Boolean? PCR_Securing_Depart_Not_Secure_Reason_Broken_Door { get; set; }
        public Boolean? PCR_Securing_Depart_Not_Secure_Reason_Bids_Pending { get; set; }
        public Boolean? PCR_Securing_Depart_Not_Secure_Reason_Damage_Locks { get; set; }

        public Boolean? PCR_Securing_Prop_No_More_Boarding_Required { get; set; }

        public Int64 fwo_pkyeId { get; set; }

    }
    public class PCR_Security_Master
    {
        public String PCR_Securing_pkeyId { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }

}