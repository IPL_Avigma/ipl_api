﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Contractor_Paid_Invoice_DateDTO
    {
        public Decimal? Inv_Con_Sub_Total { get; set; }
        public DateTime? Inv_Con_Inv_Date { get; set; }
        public String Con_Pay_CheckNumber { get; set; }
        public String WhereClause { get; set; }
        public Decimal? Con_Pay_Amount { get; set; }
        public Boolean? Inv_Con_IsActive { get; set; }
        public List<Contractor_Paid_InvoiceDTO> Contractor_Paid_InvoiceDTO { get; set; }
        public int? Type { get; set; }
        public Int64 UserID { get; set; }
        public int PageNumber { get; set; }
        public int NoofRows { get; set; }
        public DateTime? FromDatePaidInvoice { get; set; }
        public DateTime? ToDateDatePaidInvoice { get; set; }
        public List<Contractorarr> Contractorarr { get; set; }
        public DateTime? Con_Pay_Payment_Date { get; set; }
        public string Con_Pay_Comment { get; set; }
        public string ContractorName { get; set; }
    }

    public class Contractor_Paid_Invoice_ChildDTO
    {
        public Int64 Inv_Con_pkeyId { get; set; }
        public Int64 Inv_Con_Ch_ContractorId { get; set; }
        public Int64 Inv_Con_Ch_pkeyId { get; set; }
        public String Inv_Con_Ch_Qty { get; set; }
        public Decimal? Inv_Con_Ch_Price { get; set; }
        public Decimal? Inv_Con_Ch_Total { get; set; }
        public String Task_Name { get; set; }
        public int? Type { get; set; }
        public Decimal? Inv_Con_Ch_Discount { get; set; }
        public String Inv_Con_Ch_Comment { get; set; }

    }
    public class ContractorPendingInvoiceDTO
    {
        public List<Contractor_Paid_InvoiceDTO> CurrentlyDue { get; set; }
        public List<Contractor_Paid_InvoiceDTO> Completed { get; set; }
        public List<Contractor_Paid_InvoiceDTO> OnHold { get; set; }

    }


        public class Contractor_Paid_InvoiceDTO
    {
        public Int64 Inv_Con_pkeyId { get; set; }
        public Int64? Inv_Con_Invoice_Id { get; set; }
        public Int64? Inv_Con_TaskId { get; set; }
        public Int64? Inv_Con_Wo_ID { get; set; }
        public Decimal? Inv_Con_Sub_Total { get; set; }
        public Decimal? Inv_Con_ContTotal { get; set; }
        public Decimal? Con_Pay_Amount { get; set; }
        public String Inv_Con_Invoce_Num { get; set; }
        public DateTime? Inv_Con_Inv_Date { get; set; }
        public int? Inv_Con_Status { get; set; }
        public DateTime? dueDate { get; set; }
        public String IPLNO { get; set; }
        public String workOrderNumber { get; set; }
        public String address1 { get; set; }
        public String city { get; set; }
        public String SM_Name { get; set; }
        public Int64? zip { get; set; }
        public String ContractorName { get; set; }
        public String WT_WorkType { get; set; }
        public String Client_Company_Name { get; set; }
        public Int64? assigned_admin { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
        public List<Contractor_Paid_Invoice_ChildDTO> Contractor_Paid_Invoice_ChildDTO { get; set; }
        public Boolean? Inv_Con_Inv_Followup { get; set; }
        public DateTime? From_InvoiceDate { get; set; }
        public DateTime? To_InvoiceDate { get; set; }
        public String whereClause { get; set; }
        public List<Contractorarr> PendingTabCon { get; set; }
        public string Inv_Con_Inv_Comment { get; set; }

    }
    public class Contractorarr
    {
        public Int64 User_pkeyID { get; set; }
        public string User_FirstName { get; set; }

    }
}