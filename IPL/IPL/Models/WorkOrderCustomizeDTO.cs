﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPLApp.Models
{
    public class WorkOrderCustomizeDTO
    {

        public Int64 wo_Custo_pkeyID { get; set; }
        public String wo_Custo_DocType { get; set; }
        public DateTime? wo_Custo_RecievedDate { get; set; }
        public DateTime? wo_Custo_ExpDate { get; set; }
        public DateTime? wo_Custo_NotificationDate { get; set; }
        public Boolean? wo_Custo_AlertUser { get; set; }
        public String wo_Custo_DocPath { get; set; }
        public String wo_Custo_FileName { get; set; }
        public Int64? wo_Custo_UserId { get; set; }
        public Boolean? wo_Custo_IsActive { get; set; }
        public Boolean? wo_Custo_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }

    }
    public class WorkOrderCustomize
    {
        public String wo_Custo_pkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}