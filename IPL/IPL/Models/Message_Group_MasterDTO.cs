﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Message_Group_MasterDTO
    {

        public Int64 MGM_PkeyID { get; set; }
        public Int64 MGM_Group_UserID { get; set; }
        public String MGM_Group_Name { get; set; }
        public DateTime MGM_Group_Creation_Date { get; set; }
        public int MGM_Group_Status { get; set; }
        public String MGM_Group_Description { get; set; }
        public Boolean MGM_IsActive { get; set; }
        public Boolean MGM_IsDelete { get; set; }
        public int Type { get; set; }
        public Int64 UserID { get; set; }
        public String MGM_UserjsonData { get; set; }
        public String MGM_Group_id { get; set; }
        public String FilterName { get; set; }
        public int Pagenumber { get; set; }
        public int Rownumber { get; set; }
        public int MGM_Group_chat_type { get; set; }
        public string MGM_Group_id_out { get; set; }
        //public Int64 MGM_Group_CompanyId { get; set; }
    }
}