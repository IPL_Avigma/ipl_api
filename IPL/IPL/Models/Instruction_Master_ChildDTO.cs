﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Instruction_Master_ChildDTO
    {
        public Int64 Inst_Ch_pkeyId { get; set; }
        public Int64? Inst_Ch_Wo_Id { get; set; }
        public String Inst_Ch_Text { get; set; }
        public String Inst_Ch_Comand_Mobile { get; set; }
        public String Instr_Comand_Mobile { get; set; }
        public Boolean? Inst_Ch_IsActive { get; set; }
        public Boolean? Inst_Ch_Delete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
    }
    public class Instruction_Master_Child
    {
        public String Inst_Ch_pkeyId { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}