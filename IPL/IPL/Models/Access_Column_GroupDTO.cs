﻿using Realms.Sync;
using Remotion.Linq.Clauses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static Google.Cloud.Firestore.V1.StructuredQuery.Types;

namespace IPL.Models
{
    public class Access_Column_GroupDTO
    {
        public Int64 ACG_PkeyID { get; set; }
        public Int64? ACG_GroupID { get; set; }
        public Int64? ACG_ColumnID { get; set; }
        public Int64 ACG_CompanyID { get; set; }
        public Int64 ACG_UserID { get; set; }
        public Boolean? ACG_IsActive { get; set; }
        public Boolean? ACG_IsDelete { get; set; }
        public Int64? UserId { get; set; }
        public int Type { get; set; }
        public string Access_Colum_str { get; set; }

    }
    public class Access_Column_Group_Input
    {
        public Int64 ACG_PKeyID { get; set; }
        public int Type { get; set; }
        public String WhereClause { get; set; }
        public int PageNumber { get; set; }
        public int NoofRows { get; set; }
        public String Orderby { get; set; }
        public Int64 UserId { get; set; }

    }

    public class AccessColumnImport
    {
        public String ACG_Pkey_ID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }

    public class AccessColumnGroup
    {
        public Int64 Wo_Column_PkeyId { get; set; }
        public String Wo_Column_Name { get; set; }
        public Boolean Wo_Column_IsActive { get; set; }
        public String Keydata { get; set; }
        public String Wo_Column_parameter { get; set; }
        public int Wo_Sort_order { get; set; }
        public Int64 ACG_PKeyID { get; set; }

        public Boolean ACG_PKeyID_sel { get; set; }

        public Int64? ACG_GroupID { get; set; }

    }

}