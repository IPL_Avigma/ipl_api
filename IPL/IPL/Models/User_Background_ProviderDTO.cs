﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class User_Background_ProviderDTO
    {
        public Int64 Background_check_PkeyID { get; set; }
        public String Background_check_Drop_ID { get; set; }
        public String Background_check_ID { get; set; }
        public String Background_check_File_Name { get; set; }
        public String Background_check_File_Path { get; set; }
        public String Background_check_File_Type { get; set; }
        public Int64 Background_check_User_ID { get; set; }
        public String Back_Chk_ProviderName { get; set; }
        public Boolean? Background_check_IsActive { get; set; }
        public Boolean? Background_check_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
    }
}