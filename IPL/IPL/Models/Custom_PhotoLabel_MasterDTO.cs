﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Custom_PhotoLabel_MasterDTO
    {
        public Int64 PhotoLabel_pkeyID { get; set; }
        public string PhotoLabel_Name { get; set; }
        public int? PhotoLabel_IsCustom { get; set; }
        public Boolean? PhotoLabel_IsAutoAssign { get; set; }
        public Boolean? PhotoLabel_IsActive { get; set; }
        public Boolean? PhotoLabel_IsDelete { get; set; }
        public int Type { get; set; }
        public Int64 UserID { get; set; }
        public Boolean? Custom_label_Check { get; set; }
        public Int64 workOrder_ID { get; set; }
        public int PhotoLabel_Valtype { get; set; }
        public Int64 PhotoLabel_Client_Id { get; set; }
        public Int64 PhotoLabel_WorkType_Id { get; set; }
        public Int64 PhotoLabel_Customer_Id { get; set; }
        public Int64 PhotoLabel_Loan_Id { get; set; }
        public Int64? PhotoLabel_Group_Id { get; set; } //Added By dipali
        public String WhereClause { get; set; }
        public String FilterData { get; set; }
        public String PhotoLabel_CreatedBy { get; set; }
        public String PhotoLabel_ModifiedBy { get; set; }
        public List<AutoAssinArray> AutoAssinArray { get; set; }
        public Int64? CustomPhotoLabel_Filter_Master_PkeyId { get; set; }
        public string ViewUrl { get; set; }
    }

    public class Custom_PhotoLabelOut
    {
        public String PhotoLabel_pkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
        public String WorkOrderPhotoLabel_pkeyID { get; set; }
    }
}