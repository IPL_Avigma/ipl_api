﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Filter_Admin_UOM_MasterDTO
    {
        public Int64 UOM_Filter_PkeyID { get; set; }
        public String UOM_Filter_UOMName { get; set; }
        public String UOM_Filter_CreatedBy { get; set; }
        public String UOM_Filter_ModifiedBy { get; set; }
        public Boolean? UOM_Filter_UOMIsActive { get; set; }
        public Boolean UOM_Filter_IsActive { get; set; }
        public Boolean? UOM_Filter_IsDelete { get; set; }
        public int? Type { get; set; }
        public Int64? UserId { get; set; }
    }
}