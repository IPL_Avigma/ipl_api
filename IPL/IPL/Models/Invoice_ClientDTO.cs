﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Invoice_ClientDTO
    {
        public Int64? Inv_Client_pkeyId { get; set; }
        public Int64? Inv_Client_Invoice_Id { get; set; }
        public Int64? Inv_Client_WO_Id { get; set; }
        public Int64? Inv_Client_Task_Id { get; set; }
        public Int64? Inv_Client_Uom_Id { get; set; }
        public Decimal? Inv_Client_Sub_Total { get; set; }
        public int? Inv_Client_Client_Dis { get; set; }
        public Decimal? Inv_Client_Client_Total { get; set; }
        public String Inv_Client_Short_Note { get; set; }
        public Boolean? Inv_Client_Inv_Complete { get; set; }
        public Boolean? Inv_Client_Credit_Memo { get; set; }
        public DateTime? Inv_Client_Sent_Client { get; set; }
        public DateTime? Inv_Client_Inv_Date { get; set; }
        public DateTime? Inv_Client_Comp_Date { get; set; }
        public String Inv_Client_Invoice_Number { get; set; }
        public String Inv_Client_Internal_Note { get; set; }
        public int? Inv_Client_Status { get; set; }
        public Decimal? Inv_Client_Discout_Amount { get; set; }
        public Boolean? Inv_Client_IsActive { get; set; }
        public Boolean? Inv_Client_IsDelete { get; set; }

        public Int64? UserID { get; set; }
        public int Type { get; set; }

        public bool? Inv_Client_Auto_Invoice { get; set; }
        public Boolean? Inv_Client_Followup { get; set; }
        public DateTime? Inv_Client_Hold_Date { get; set; }
        public Boolean? Inv_Con_Inv_Approve { get; set; }
        public DateTime? Inv_Con_Inv_Approve_Date { get; set; }

        public bool? Inv_Client_IsNoCharge { get; set; }
        public DateTime? Inv_Client_NoChargeDate { get; set; }

        public string WorkOrderID_mul { get; set; }


        public List<Invoice_Client_ChildDTO> Invoice_Client_ChildDTO { get; set; }
    }
    public class Client_Pay_DTO
    {
        public Int64? ICP_PkeyID { get; set; }
        public String Client_Pay_CheckNumber { get; set; }
        public String Client_Pay_Payment_Date { get; set; }
        public Int64? Client_Pay_Invoice_Id { get; set; }
        public String Inv_Client_Inv_Date { get; set; }
        public Decimal Client_Pay_Amount { get; set; }
        public String Client_Pay_Comment { get; set; }
        public String IPLNO { get; set; }
        public String workOrderNumber { get; set; }
        public String Initial_Comments { get; set; }
        public Boolean Write_Off { get; set; }

        public String CheckNumber { get; set; }
        public String CheckDate { get; set; }

        public String ClientId { get; set; }

      


        public Boolean ICP_IsActive { get; set; }
        public Boolean ICP_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int? Type { get; set; }

        public Int64? workOrder_ID { get; set; }

        public Int64? Inv_Client_pkeyId { get; set; }

    }

    public class Client_Pay_DTO_List
    {
        public List<Client_Pay_DTO> Client_Pay_DTOs { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
    }
}