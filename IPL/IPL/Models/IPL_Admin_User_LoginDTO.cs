﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class IPL_Admin_User_LoginDTO
    {
        public Int64 Ipl_Ad_User_PkeyID { get; set; }
        public String Ipl_Ad_User_First_Name { get; set; }
        public String Ipl_Ad_User_Last_Name { get; set; }
        public string Ipl_Ad_User_Email { get; set; }
        public String Ipl_Ad_User_Address { get; set; }
        public String Ipl_Ad_User_City { get; set; }
        public String Ipl_Ad_User_State { get; set; }
        public String Ipl_Ad_User_Mobile { get; set; }
        public String Ipl_Ad_User_Login_Name { get; set; }
        public String Ipl_Ad_User_Password { get; set; }
        public Boolean? Ipl_Ad_User_IsActive { get; set; }
        public Boolean? Ipl_Ad_User_IsDelete { get; set; }
        public String Ipl_Ad_User_UserVal { get; set; }
        public String Ipl_Ad_User_Access_Device { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
        public string Ipl_Ad_User_IP { get; set; }
        public string Ipl_Ad_User_MacId { get; set; }

        public string WhereClause { get; set; }
        public int PageNumber { get; set; }
        public int Rowcount { get; set; }
        public string FilterData { get; set; }
    }

    public class NewIPLAdminUserLogin_Filter
    {
        public string Ipl_Ad_User_First_Name { get; set; }
        public string Ipl_Ad_User_Last_Name { get; set; }
        public string Ipl_Ad_User_Login_Name { get; set; }
        public string Ipl_Ad_User_Address { get; set; }
        public string Ipl_Ad_User_State { get; set; }


    }
}