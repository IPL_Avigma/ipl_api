﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class CyprexxJobDocumentationChecklist_DTO
    {
        public Int64 PCR_JD_PkeyID { get; set; }
        public Int64 PCR_JD_WO_ID { get; set; }
        public Int64 PCR_JD_CompanyId { get; set; }
        public String PCR_JD_Job_Info { get; set; }
        public String PCR_JD_Upload_Photos { get; set; }
        public Boolean? PCR_JD_IsActive { get; set; }
        public Boolean? PCR_JD_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public Int64 fwo_pkyeId { get; set; }
        public int Type { get; set; }
    }
}