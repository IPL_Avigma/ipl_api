﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class WorkOrderMasterImportPhotoDTO
    {
        public Int64 WAPI_PkeyID { get; set; }
        public Int64? WAPI_importpkey { get; set; }
        public Int64? WAPI_WI_PkeyID { get; set; }
        public String WAPI_Taskname { get; set; }
        public String WAPI_Filename { get; set; }
        public String WAPI_Filepath { get; set; }
        public int? WAPI_Labelid { get; set; }
        public Boolean? WAPI_IsActive { get; set; }
        public Boolean? WAPI_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
    }
    public class WoImportPhotoDTO
    {
        public String WAPI_PkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}