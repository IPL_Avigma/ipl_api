﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class MobileWorkOrderFilterDTO
    {
        public Int64 workOrder_ID { get; set; }
        public string IPLNO { get; set; }
        public string  WorkOrderNumber { get; set; }
        public string Address { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public int? Zip { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
        public string WhereClause { get; set; }
    }
}