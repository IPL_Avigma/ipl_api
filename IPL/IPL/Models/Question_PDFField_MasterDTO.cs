﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Question_PDFField_MasterDTO
    {
        public Int64 PF_pkeyId { get; set; }
        public String PF_Name { get; set; }
        public Int64? PF_FileId { get; set; }
        public Boolean? PF_IsActive { get; set; }
        public Boolean? PF_IsDelete { get; set; }
        public int? Type { get; set; }
        public Int64? UserId { get; set; }
    }
}