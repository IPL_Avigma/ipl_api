﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class PCR_Cyprexx_Property_Condition_Checklist_DTO
    {
        public Int64 PCR_PC_PkeyID { get; set; }
        public Int64 PCR_PC_WO_ID { get; set; }
        public Int64 PCR_PC_CompanyID { get; set; }
        public String PCR_PC_General_Property_Questions { get; set; }
        public String PCR_PC_Signature { get; set; }
        public Boolean PCR_PC_IsActive { get; set; }
        public Boolean PCR_PC_IsDelete { get; set; }
        public Int64 UserID { get; set; }
        public Int64 fwo_pkyeId { get; set; }
        public int Type { get; set; }
    }
}