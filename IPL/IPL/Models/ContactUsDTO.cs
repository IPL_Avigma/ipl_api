﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class ContactUsDTO
    {
        public Int64 Con_Us_PkeyId { get; set; }
        public String Con_Us_FName { get; set; }
        public String Con_Us_Email { get; set; }
        public String Con_Us_Contact_Number { get; set; }
        public String Con_Us_Message { get; set; }
        public String Con_Us_Subject { get; set; }
        public int? Con_Us_ValType { get; set; }
        public Boolean? Con_Us_IsActive { get; set; }
        public Boolean? Con_Us_IsDelete { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }

        public string WhereClause { get; set; }
        public int PageNumber { get; set; }
        public int Rowcount { get; set; }

        public string FilterData { get; set; }


    }

    public class NewContactUs_Filter
    {
        public string Con_Us_FName { get; set; }
        public string Con_Us_Subject { get; set; }
        public string Con_Us_Email { get; set; }



    }
}