﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Allowables_Child_Master_DTO
    {
        public Int64 Allow_Child_PkeyId { get; set; }
        public Int64? Allow_Child_Allowables_PkeyId { get; set; }
        public Int64? Allow_Child_Allowables_Cat_PkeyId { get; set; }
        public DateTime? Allow_Child_StartDate { get; set; }
        public DateTime? Allow_Child_EndDate { get; set; }
        public Decimal? Allow_Child_OverallAllowables { get; set; }
        public Int64? Allow_Child_CompanyId { get; set; }
        public Int64? Allow_Child_UserId { get; set; }
        public Boolean? Allow_Child_IsActive { get; set; }
        public Boolean? Allow_Child_IsDelete { get; set; }
        public int? Type { get; set; }
        public Int64? UserID { get; set; }
    }
}