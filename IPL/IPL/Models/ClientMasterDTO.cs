﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPLApp.Models
{
    public class ClientMasterDTO
    {
        public String Clnt_Con_List_Name { get; set; }

        public String Clnt_Con_List_Email { get; set; }
        public String Clnt_Con_List_Phone { get; set; }
        public String Clnt_Con_List_TypeName { get; set; }
        public Int64? Clnt_Con_List_ClientComID { get; set; }
        public int Type { get; set; }
        public Int64? client_pkyeId { get; set; }
    }
    public class ClientMaster
    {
        public String client_pkyeId { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}