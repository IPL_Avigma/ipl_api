﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPLApp.Models
{
    public class AppCompanyMasterDTO
    {

        public Int64 YR_Company_pkeyID { get; set; }
        public String YR_Company_Name { get; set; }
        public String YR_Company_Con_Name { get; set; }
        public String YR_Company_Email { get; set; }
        public String YR_Company_Phone { get; set; }
        public String YR_Company_Address { get; set; }
        public String YR_Company_City { get; set; }
        public int? YR_Company_State { get; set; }
        public Int64? YR_Company_Zip { get; set; }
        public String YR_Company_Logo { get; set; }
        public String YR_Company_Support_Email { get; set; }
        public String YR_Company_Support_Phone { get; set; }
        public String YR_Company_PDF_Heading { get; set; }
        public Boolean? YR_Company_IsActive { get; set; }
        public Boolean? YR_Company_IsDelete { get; set; }
        public String FilterData { get; set; }
     
        public SearchMasterDTO SearchMaster { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }

        public Int64? YR_Company_UserID { get; set; }
        public string ViewUrl { get; set; }
        public String YR_Company_App_logo { get; set; }
        public String YR_Company_CreatedBy { get; set; }
        public String YR_Company_ModifiedBy { get; set; }


    }
    public class AppCompanyMaster
    {
        public String YR_Company_pkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
    public class AppCompanyImagesDTO
    {
        public Int64 App_Com_Img_pkeyID { get; set; }
        public String App_Com_Img_FileName { get; set; }
        public String App_Com_Img_FilePath { get; set; }
        public Int64? App_Com_Img_CompanyID { get; set; }
        public Boolean? App_Com_Img_IsActive { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
    }
    public class AppCompanyImages
    {
        public String App_Com_Img_pkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}