﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPLApp.Models
{
    public class Survey_Question_MasterDTO
    {
        public Int64 que_pkey_Id { get; set; }
        public String que_type { get; set; }
        public String que_repeat { get; set; }
        public Boolean? que_expand { get; set; }
        public String que_name { get; set; }
        public String que_id { get; set; }
        public String que_prompt { get; set; }
        public Boolean? que_enableTotal { get; set; }
        public Boolean? que_anonymous { get; set; }
        public Boolean? que_enabledByDefault { get; set; }
        public String que_uuid { get; set; }
        public Boolean? que_IsActive { get; set; }
        public Int64? que_serviceID { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
    }
    public class Survey_Question_Master
    {
        public Int64 que_pkey_Id { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}