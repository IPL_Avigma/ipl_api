﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Contractor_Invoice_PaymentDTO
    {

        public Int64 Con_Pay_PkeyId { get; set; }
        public Int64? Con_Pay_Invoice_Id { get; set; }
        public Int64? Con_Pay_Wo_Id { get; set; }
        public DateTime? Con_Pay_Payment_Date { get; set; }
        public Decimal? Con_Pay_Amount { get; set; }
        public String Con_Pay_CheckNumber { get; set; }
        public String Con_Pay_Comment { get; set; }
        public String Con_Pay_EnteredBy { get; set; }
        public Decimal? Con_Pay_Balance_Due { get; set; }
        public Boolean? Con_Pay_IsActive { get; set; }
        public Boolean? Con_Pay_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
        public Int64 Pay_PkeyId { get; set; }
        public Int64 Pay_Wo_Id { get; set; }
    }
  
}