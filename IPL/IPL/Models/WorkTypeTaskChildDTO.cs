﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class WorkTypeTaskChildDTO
    {
        public Int64 WT_Task_pkeyID { get; set; }
        public Int64 WT_Task_ID { get; set; }
        public String WT_Task_Company { get; set; }
        public String WT_Task_State { get; set; }
        public String WT_Task_Customer { get; set; }
        public String WT_Task_LoneType { get; set; }
        public String WT_Task_WorkType { get; set; }
        public String WT_Task_WorkType_Group { get; set; }
        public DateTime? WT_Task_ClientDueDateTo { get; set; }
        public DateTime? WT_Task_ClientDueDateFrom { get; set; }
        public Boolean? WT_Task_IsActive { get; set; }
        public Boolean? WT_Task_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
        public List<WorkTypeTaskChildDTO> strworkTypeTaskChildDTO { get; set; }
    }
    public class WorkTypeTaskChildMaster
    {
        public String WT_Task_pkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}