﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class ActionResultsWorkOderDTO
    {
        public Int64? Assign_Contractor  { get; set; }
        public Int64? Assign_Coordinator   { get; set; }
        public Int64? Assign_Processor   { get; set; }
        public Int64? Client_Company { get; set; }
        public Int64? Work_Type { get; set; }
        public String  Due_Date { get; set; }
        public String Start_Date { get; set; }
        public String Client_Due_Date { get; set; }
        public String Recurring_Order { get; set; }
        public String  Comments { get; set; }
        public String Estimated_Date { get; set; }
        public String Send_Message { get; set; }
        public String  Task  { get; set; }
        public String  Instructions  { get; set; }
        public Int64? Category   { get; set; }
        public Int64? Background_Provider { get; set; }
        public String Assign_PCR { get; set; }
        public String Cancel_Work_Order { get; set; }
        public String Delete_Work_Order { get; set; }
        public String Mark_Client_Invoice_Paid { get; set; }
        public String Write_off_Invoice { get; set; }
        public String Mark_Contractor_Invoice_Paid { get; set; }
        public String Print_WO_Instructions { get; set; }
        public String Print_Client_Invoice { get; set; }
        public String Export_to_Excel { get; set; }
        public String Download_Photos { get; set; }
        public String Attach_Document { get; set; }
        public String Route { get; set; }

        public int Type { get; set; }
        public Int64 UserId { get; set; }
        public String WorkActionArray { get; set; }
    }
    public class ActionUpdateWorkOderDTO
    {
        public Int64 WorkOrder_ID { get; set; }
        public String Sql_Val { get; set; }

        public Int64 UserId { get; set; }

        public int Type { get; set; }
    }
}