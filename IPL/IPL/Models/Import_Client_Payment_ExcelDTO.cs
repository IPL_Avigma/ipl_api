﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Import_Client_Payment_ExcelDTO
    {

        public Int64 ICP_PkeyID { get; set; }
        public String Client_Pay_CheckNumber { get; set; }
        public String Client_Pay_Payment_Date { get; set; }
        public String Client_Pay_Invoice_Id { get; set; }
        public String Inv_Client_Inv_Date { get; set; }
        public String Client_Pay_Amount { get; set; }
        public String Client_Pay_Comment { get; set; }
        public String IPLNO { get; set; }
        public String workOrderNumber { get; set; }
        public String Initial_Comments { get; set; }
        public Boolean? Write_Off { get; set; }
        public Boolean? ICP_IsActive { get; set; }
        public Boolean? ICP_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
        public string ExcelData { get; set; }
        
    }
}