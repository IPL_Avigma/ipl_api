﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPLApp.Models
{
    public class WorkOrderItemDetail
    {
        public string Description { get; set; }
        public string Qty { get; set; }
        public string Price { get; set; }
        public string Total { get; set; }
        public string AdditionalInstructions { get; set; }
        public Int64? WorkOrderItem_PkeyId { get; set; }
        public Int64? WorkOrderMaster_API_Data_Pkey_Id { get; set; }
        public string Action { get; set; }

    }

public class PPW_WorkOrderMaster_API_DTO
    {
    public string category { get; set; }
    public string wo { get; set; }
    public string address { get; set; }
    public string estimated_complete_date { get; set; }
    public string loan_info { get; set; }
    public string office_locked { get; set; }
    public string due_date { get; set; }
    public string ready_for_office_Contractor { get; set; }
    public List<WorkOrderItemDetail> work_order_item_details { get; set; }
    public string complete_date { get; set; }
    public string comments { get; set; }
    public string client_Due_date { get; set; }
    public string username { get; set; }
    public string follow_up_complete { get; set; }
    public string broker_info { get; set; }
    public string lot_size { get; set; }
    public string received_date { get; set; }
    public string customer { get; set; }
    public string client_company { get; set; }
    public string freeze_property { get; set; }
    public string ppw { get; set; }
    public string start_date { get; set; }
    public string contractor { get; set; }
    public string ready_for_office { get; set; }
    public string missing_info { get; set; }
    public string BATF { get; set; }
    public string sub_contractor_follow_up { get; set; }
    public string work_type { get; set; }
    public string cancel_date { get; set; }
    public string is_Inspection { get; set; }
    public string assigned_admin { get; set; }
    public string lklg { get; set; }
    public string Imp_Wo_File_Name { get; set; }
    public string Imp_Wo_File_Path { get; set; }
    public string Mortgagor { get; set; }

   // new changes 
    public string city { get; set; }
    public string state { get; set; }
    public string zip { get; set; }
    public string loan_number { get; set; }
    public string loan_type { get; set; }
    public string lock_code { get; set; }
    public string key_code { get; set; }
    public string wo_status { get; set; }
    public string ready_for_office_date { get; set; }


    }
public class WorkOrderMaster_API_DTO
    {
        public Int64 Pkey_Id { get; set; }
        public Int64 Imrt_PkeyId { get; set; }
        public string Category { get; set; }
        public string Wo { get; set; }
        public string address { get; set; }
        public string estimated_complete_date { get; set; }
        public string loan_info { get; set; }
        public string office_locked { get; set; }
        public string due_date { get; set; }
        public string ready_for_office_Contractor { get; set; }
        public string client_Due_date { get; set; }
        public string complete_date { get; set; }
        public string comments { get; set; }
        public string username { get; set; }
        public string follow_up_complete { get; set; }
        public string broker_info { get; set; }
        public string lot_size { get; set; }
        public DateTime? received_date { get; set; }
        public string customer { get; set; }
        public string client_company { get; set; }
        public string freeze_property { get; set; }
        public string ppw { get; set; }
        public DateTime? start_date { get; set; }
        public string contractor { get; set; }
        public string ready_for_office { get; set; }
        public string missing_info { get; set; }
        public string BATF { get; set; }
        public string sub_contractor_follow_up { get; set; }
        public string work_type { get; set; }
        public DateTime? cancel_date { get; set; }
        public string is_Inspection { get; set; }
        public string assigned_admin { get; set; }
        public string lklg { get; set; }
        public Boolean? IsActive { get; set; }
        public int Type { get; set; }
        public int UserId { get; set; }

        public Int64? ImportMasterPkey { get; set; }
        public List<WorkOrderItemDetail> WorkOrderItemDetail { get; set; }

        public string Imp_Wo_File_Name { get; set; }
        public string Imp_Wo_File_Path { get; set; }

        public String gpsLatitude { get; set; }
        public String gpsLongitude { get; set; }

        public String Mortgagor { get; set; }
        public int PageNumber { get; set; }
        public int NoofRows { get; set; }
        public String WhereClause { get; set; }
        public Int64? Imrt_Import_From_ID { get; set; }
        public Int64? WorkType_Id { get; set; }
        public Int64? Contractor_Id { get; set; }
        public Int64? Coordinator_Id { get; set; }
        public Int64? Processor_Id { get; set; }

        public String Imrt_Wo_WTIDName { get; set; }
        public String Imrt_Wo_CatIDName { get; set; }
        public String Imrt_Wo_Number { get; set; }

        // new changes 
        public string city { get; set; }
        public string state { get; set; }
        public string zip { get; set; }
        public string loan_number { get; set; }
        public string loan_type { get; set; }
        public string lock_code { get; set; }
        public string key_code { get; set; }
        public string wo_status { get; set; }
        public string ready_for_office_date { get; set; }

    }
}