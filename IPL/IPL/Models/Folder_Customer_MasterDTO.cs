﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Folder_Customer_MasterDTO
    {
        public Int64 Fold_Customer_PkeyId { get; set; }
        public Int64? Fold_Customer_Id { get; set; }
        public Int64? Fold_Customer_Folder_Id { get; set; }
        public Int64? Fold_Customer_File_Id { get; set; }
        public Int64? Fold_Customer_Auto_Assine_Id { get; set; }
        public Boolean? Fold_Customer_IsActive { get; set; }
        public Boolean? Fold_Customer_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
        public Int64? Folder_File_Master_FK_Id { get; set; }
    }
}