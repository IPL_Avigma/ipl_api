﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class UserDocumentDTO
    {
        public Int64 User_Doc_pkeyID { get; set; }
        public String User_Doc_DocPath { get; set; }
        public String User_Doc_FileName { get; set; }
        public String User_Doc_ValType { get; set; }
        public DateTime? User_Doc_Exp_Date { get; set; }
        public DateTime? User_Doc_RecievedDate { get; set; }
        public DateTime? User_Doc_NotificationDate { get; set; }
        public Boolean? User_Doc_AlertUser { get; set; }
        public Int64? User_Doc_UserID { get; set; }
        public Boolean? User_Doc_IsActive { get; set; }
        public Boolean? User_Doc_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
    }
    public class UserDocument
    {
        public String User_Doc_pkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}