﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPLApp.Models
{
    public class Forms_Master_FilesDTO
    {
        public Int64 FMFI_Pkey { get; set; }
        public Int64? FMFI_FormId { get; set; }
        public String FMFI_File_FolderName { get; set; }
        public String FMFI_File_Name { get; set; }
        public String FMFI_File_Path { get; set; }
        public String FMFI_File_BucketName { get; set; }
        public String FMFI_File_ProjectID { get; set; }
        public String FMFI_File_Size { get; set; }
        public Int64? FMFI_UploadBy { get; set; }
        public Boolean? FMFI_IsDelete { get; set; }
        public Boolean? FMFI_IsActive { get; set; }
        public int? Type { get; set; }
        public Int64? UserId { get; set; }
    }
}