﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class WorkOrder_ServiceLink_MasterDTO
    {
        public Int64 WServiceLink_PkeyID { get; set; }
        public String WServiceLink_wo_id { get; set; }
        public String WServiceLink_Address { get; set; }
        public String WServiceLink_City { get; set; }
        public String WServiceLink_State { get; set; }
        public String WServiceLink_ZipCode { get; set; }
        public DateTime? WServiceLink_DueDate { get; set; }
        public String WServiceLink_KeyCode { get; set; }
        public String WServiceLink_LockBox { get; set; }

        public String WServiceLink_ClientID { get; set; }
        public String WServiceLink_Loan { get; set; }
        public String WServiceLink_LoanType { get; set; }
        public String WServiceLink_Owner { get; set; }
        public String WServiceLink_CoverageID { get; set; }
        public String WServiceLink_Investor { get; set; }
        public String WServiceLink_services { get; set; }
        public String WServiceLink_id { get; set; }
        public String WServiceLink_username { get; set; }


        public String WServiceLink_Comments { get; set; }
        public String WServiceLink_gpsLatitude { get; set; }
        public String WServiceLink_gpsLongitude { get; set; }
        public Int64 WServiceLink_ImportMaster_Pkey { get; set; }
        public String WServiceLink_Import_File_Name { get; set; }
        public String WServiceLink_Import_FilePath { get; set; }
        public String WServiceLink_Import_Pdf_Name { get; set; }
        public String WServiceLink_Import_Pdf_Path { get; set; }
        public Boolean? WServiceLink_IsActive { get; set; }
        public Boolean? WServiceLink_IsDelete { get; set; }
        public String WServiceLink_Service_ID { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
    }


    public class WorkOrder_ServiceLinkItem_MasterDTO
    {
        public String WO { get; set; }
        //public string address { get; set; }

        public string Address { get; set; }
        //public string city { get; set; }
        public string City { get; set; }
        //public string state { get; set; }
        public string State { get; set; }
        //public string zip { get; set; }
        public string ZipCode { get; set; }
        public string customer { get; set; }
        //public string loan_type { get; set; }
        public string Loan { get; set; }
        public string LoanType { get; set; }
        public string lock_code { get; set; }
        public string Owner { get; set; }
        //public string key_code { get; set; }
        public string KeyCode { get; set; }
        public string CoverageID { get; set; }
        public string LockBox { get; set; }
        public string Investor { get; set; }
        public string ClientID { get; set; }
        
        //public DateTime? received_date { get; set; }
        //public DateTime? due_date { get; set; }
        public DateTime? DueDate { get; set; }
        public string comments { get; set; }
        //public string work_order_item_details { get; set; }
        public List<services> services { get; set; }
        public string id { get; set; }
        public string username { get; set; }
    }

    public class services
    {
        public string name { get; set; }
        public string instructions { get; set; }
    }

        public class WorkOrder_ServiceLink_Master
    {
        public String WServiceLink_PkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}