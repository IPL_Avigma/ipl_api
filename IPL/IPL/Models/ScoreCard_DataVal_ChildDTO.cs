﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class ScoreCard_DataVal_ChildDTO
    {
        public Int64 Scdc_pkeyId { get; set; }
        public Int64? Scdc_Scd_pkeyId { get; set; }
        public Int64? Scdc_Sna_pkeyId { get; set; }
        public int? Scdc_Status_Id { get; set; }
        public Boolean? Scdc_IsActive { get; set; }
        public Boolean? Scdc_IsDelete { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
    }
}