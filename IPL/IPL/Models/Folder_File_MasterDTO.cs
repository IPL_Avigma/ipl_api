﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Folder_File_MasterDTO
    {
        public Int64 Fold_File_Pkey_Id { get; set; }
        public Int64? Fold_File_ParentId { get; set; }
        public Int64? Fold_File_Role_Folder_Id { get; set; }
        public String Fold_File_Name { get; set; }
        public String Fold_File_Local_Path { get; set; }
        public String Fold_File_Bucket_Name { get; set; }
        public String Fold_File_ProjectId { get; set; }
        public String Fold_File_Object_Name { get; set; }
        public String Fold_File_Folder_Name { get; set; }
        public Boolean? Fold_File_IsActive { get; set; }
        public Boolean? Fold_File_IsDelete { get; set; }
        public Boolean? Fold_Is_AutoAssign { get; set; }

        public String Fold_File_CreatedBy { get; set; }
        public String Fold_File_ModifiedBy { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
        public String Fold_File_Desc { get; set; }
        public List<AutoAssinArray> AutoAssinArray { get; set; }
    }
}