﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPLApp.Models
{
    public class WorkOrderFilterDTO
    {
      
        public Int64 WF_PkeyID { get; set; }

        public String WF_QueryName { get; set; }
        public Boolean? WF_IsLoad { get; set; }

        public String WF_Query { get; set; }

        public String WF_Json { get; set; }

        public Int64? UserID { get; set; }

        public int Type { get; set; }

        public Boolean? WF_IsActive { get; set; }
        public Boolean? WF_IsDelete { get; set; }
        public String WhereClause { get; set; }

        //public int? PageNumber { get; set; }
        //public int? NoofRows { get; set; }
        //public int? Skip { get; set; }

    }
    public class WorkOrderFilter
    {

        public Int64 WF_PkeyID { get; set; }

        public String WF_QueryName { get; set; }
        public Boolean? WF_IsLoad { get; set; }
    }

    }