﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Message_WorkOrder_User_DTO
    {
        public Int64 MWU_pkeyID { get; set; }
        public Int64 MWU_User_ID { get; set; }
        public Int64? MWU_workOrder_ID { get; set; }
        public Boolean? MWU_IsRead { get; set; }
        public int? MWU_Role { get; set; }
        public Boolean? MWU_IsActive { get; set; }
        public Boolean? MWU_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int? Type { get; set; }
        public List<Message_Admin_UserDTO> lstmessage_Admin_UserDTO { get; set; }
    }
    public class Message_Admin_UserDTO
    {
        public Int64 User_pkeyID { get; set; }
        public String User_FirstName { get; set; }
        public String User_LastName { get; set; }
        public String User_LoginName { get; set; }
        public Int64? MWU_workOrder_ID { get; set; }
        public Boolean? MWU_IsRead { get; set; }
        public Int64? UserID { get; set; }
        public int? Type { get; set; }
        public bool chkdata { get; set; }

    }
   
}