﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class TaskSettingChildDTO
    {
        public Int64 Task_sett_pkeyID { get; set; }
        public Int64 Task_sett_ID { get; set; }
        public String Task_sett_Company { get; set; }
        public String Task_sett_State { get; set; }
        public String Task_sett_Country { get; set; }
        public String Task_sett_Zip { get; set; }
        public String Task_sett_Contractor{ get; set; }
        public String Task_sett_Customer { get; set; }
        public String Task_sett_Lone { get; set; }
        public Decimal? Task_sett_Con_Unit_Price { get; set; }
        public Decimal? Task_sett_CLI_Unit_Price { get; set; }
        public Boolean? Task_sett_Flat_Free { get; set; }
        public Boolean? Task_sett_Price_Edit { get; set; }
        public string Task_Work_TypeGroup { get; set; }
        public Boolean? Task_sett_IsActive { get; set; }
        public Boolean? Task_sett_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
        public List<TaskPresetDTO> TaskPresetDTO { get; set; }
        public List<TaskSettingChildDTO> strTaskSettingChild { get; set; }
        public Boolean? Task_sett_Disable_Default { get; set; }
        public String Task_sett_WorkType { get; set; }
        public String Task_sett_LotPrice { get; set; }
        public String Task_sett_CreatedBy { get; set; }
        public String Task_sett_ModifiedBy { get; set; }
        public Decimal? Task_sett_LOT_Min { get; set; }
        public Decimal? Task_sett_LOT_Max { get; set; }
        public Int64? Task_sett_MapTaskFkeyId { get; set; }
    }
    public class TaskSettingChildMaster
    {
        public String Task_sett_pkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}