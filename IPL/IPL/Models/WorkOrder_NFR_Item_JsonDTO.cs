﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class WorkOrder_NFR_Item_JsonDTO
    {
        public String work_order { get; set; }
        public string to { get; set; }
        public DateTime? date_rep_contact { get; set; }
        public DateTime? date_due { get; set; }
        public string mtg_co { get; set; }
        public string account { get; set; }
        public string loan_type { get; set; }
        public string mtgr_name { get; set; }
        public string mtgr_address { get; set; }
        public string requirement_name { get; set; }
        public string requirement_details { get; set; }

        public string wo_id { get; set; }
        public string username { get; set; }
         public string keycode { get; set; }
        public string lockbox { get; set; }
        public string address { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zip { get; set; }
        
        public string nfr_id { get; set; }



        public List<NFRInstruction> instructions { get; set; }
    }

    public class NFRInstruction
    {
        public string instruction_name { get; set; }
        public string instruction_details { get; set; }
    }



}