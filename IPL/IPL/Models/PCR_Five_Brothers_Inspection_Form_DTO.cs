﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class PCR_Five_Brothers_Inspection_Form_DTO
    {
        public Int64 INS_PkeyID { get; set; }
        public Int64 INS_WO_ID { get; set; }
        public Int64 INS_Company_ID { get; set; }
        public string INS_Inspection { get; set; }
        public bool INS_IsActive { get; set; }
        public bool INS_IsDelete { get; set; }
        public Int64 UserID { get; set; }
        public Int64 fwo_pkyeId { get; set; }
        public int Type { get; set; }
    }
}