﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class PCR_MCS_Forms_Master_DTO
    {
        public Int64 MCS_PkeyID { get; set; }
        public Int64 MCS_WO_ID { get; set; }
        public Int64 MCS_CompanyID { get; set; }
        public String MCS_Property_Info { get; set; }
        public String MCS_Completion_Info { get; set; }
        public String MCS_Utilities { get; set; }
        public String MCS_VCL { get; set; }
        public String MCS_Check_Ins { get; set; }
        public bool MCS_IsActive { get; set; }
        public bool MCS_IsDelete { get; set; }
        public Int64 UserID { get; set; }
        public Int64 fwo_pkyeId { get; set; }
        public int Type { get; set; }
    }
}