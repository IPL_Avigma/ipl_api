﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class MassTemplateDTO
    {
        public Int64 Mass_Template_PkeyId { get; set; }
        public String Mass_Template_Subject { get; set; }
        public String Mass_Template_Contant { get; set; }
        public String WhereClause { get; set; }
        public Boolean? Mass_Template_IsActive { get; set; }
        public Boolean? Mass_Template_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
    }
}