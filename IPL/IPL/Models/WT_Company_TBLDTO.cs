﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class WT_Company_TBLDTO
    {
        public Int64 WC_pkeyID { get; set; }
        public Int64 WC_Task_pkeyID { get; set; }
        public Int64 WC_Task_sett_ID { get; set; }
        public Boolean WC_IsActive { get; set; }
        public Boolean WC_Isdelete { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
        public Int64? WC_WTTaskSett_Id { get; set; }
    }
    public class WT_Customer_TBLDTO
    {
        public Int64 WCust_pkeyID { get; set; }
        public Int64? WCust_Task_pkeyID { get; set; }
        public Int64? WCust_Task_sett_ID { get; set; }
        public Boolean? WCust_IsActive { get; set; }
        public Boolean? WCust_Isdelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
        public Int64? WCust_WTTaskSett_Id { get; set; }
    }
    public class WT_Loan_TBLDTO
    {
        public Int64 WL_pkeyID { get; set; }
        public Int64? WL_Task_pkeyID { get; set; }
        public Int64? WL_Task_sett_ID { get; set; }
        public Boolean? WL_IsActive { get; set; }
        public Boolean? WL_Isdelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
        public Int64? WL_WTTaskSett_Id { get; set; }
    }
    public class WT_State_TBLDTO
    {
        public Int64 WS_pkeyID { get; set; }
        public Int64? WS_Task_pkeyID { get; set; }
        public Int64? WS_Task_sett_ID { get; set; }
        public Boolean? WS_IsActive { get; set; }
        public Boolean? WS_Isdelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
        public Int64? WS_WTTaskSett_Id { get; set; }
    }
    public class WT_WorkType_TBLDTO
    {
        public Int64 Wwork_pkeyID { get; set; }
        public Int64? Wwork_Task_pkeyID { get; set; }
        public Int64? Wwork_Task_sett_ID { get; set; }
        public Boolean? Wwork_IsActive { get; set; }
        public Boolean? Wwork_Isdelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
        public Int64? Wwork_WTTaskSett_Id { get; set; }
    }
    public class WT_WorkType_Group_TBLDTO
    {
        public Int64 Wwork_Group_pkeyID { get; set; }
        public Int64? Wwork_Group_Task_pkeyID { get; set; }
        public Int64? Wwork_Group_Task_sett_ID { get; set; }
        public Boolean? Wwork_Group_IsActive { get; set; }
        public Boolean? Wwork_Group_Isdelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
        public Int64? Wwork_Group_WTTaskSett_Id { get; set; }
    }
}