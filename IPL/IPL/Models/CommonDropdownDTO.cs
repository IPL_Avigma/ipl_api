﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class CommonDropdownDTO
    {
        public Int64 UserId { get; set; }
        public int Type { get; set; }
        public Int64 FormId { get; set; }
    }
}