﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Invoice_ContractorDTO
    {
        public Int64 Inv_Con_pkeyId { get; set; }
        public Int64? Inv_Con_Invoice_Id { get; set; }
        public Int64? Inv_Con_TaskId { get; set; }
        public Int64? Inv_Con_Wo_ID { get; set; }
        public Int64? Inv_Con_Uom_Id { get; set; }
        public Decimal? Inv_Con_Sub_Total { get; set; }
        public int? Inv_Con_ContDiscount { get; set; }
        public Decimal? Inv_Con_ContTotal { get; set; }
        public String Inv_Con_Short_Note { get; set; }
        public Boolean? Inv_Con_Inv_Followup { get; set; }
        public String Inv_Con_Inv_Comment { get; set; }
        public String Inv_Con_Ref_ID { get; set; }
        public Boolean? Inv_Con_Followup_Com { get; set; }
        public String Inv_Con_Invoce_Num { get; set; }
        public DateTime? Inv_Con_Inv_Date { get; set; }
        public DateTime? Inv_Con_Inv_Hold_Date { get; set; }

        public int? Inv_Con_Status { get; set; }
        public Decimal? Inv_Con_DiscountAmount { get; set; }
        public Boolean? Inv_Con_IsActive { get; set; }
        public Boolean? Inv_Con_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }

        public bool? Task_Inv_Auto_Invoice { get; set; }

        public bool? Inv_Con_Auto_Invoice { get; set; }


        public List<Invoice_Contractor_ChildDTO> Invoice_Contractor_ChildDTO { get; set; }
        public DateTime? Inv_Con_Inv_Approve_Date { get; set; }
        public Boolean? Inv_Con_Inv_Approve { get; set; }
        public string WorkOrderID_mul { get; set; }
    }
    public class WorkOrder_Import_FilesDTO
    {
        public Int64 WIF_Pkey { get; set; }
        public Int64? WIF_workOrder_ID { get; set; }
        public String WIF_FileName { get; set; }
        public String WIF_FilePath { get; set; }
        public Boolean? WIF_IsActive { get; set; }
        public Boolean? WIF_IsDelete { get; set; }
        public Int64 UserId { get; set; }
        public int Type { get; set; }
    }
}