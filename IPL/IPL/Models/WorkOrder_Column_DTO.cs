﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class WorkOrder_Column_DTO
    {
     
        public Int64 Wo_Column_PkeyId { get; set; }
        public String Wo_Column_Name { get; set; }
        public String Wo_Column_parameter { get; set; }
        public String Keydata { get; set; }
        public Int64 Type { get; set; }
        public Int64 UserID { get; set; }
  
    }

    public class WorkOrderActionsDTO
    {         
        public Int64 Wo_Column_PkeyId { get; set; }
        public String Wo_Column_Name { get; set; }
        public Boolean? Wo_Column_IsActive { get; set; }
        public Boolean? Auto_Assine { get; set; }
        public int Type { get; set; }
        public Int64 UserID { get; set; }
        public Int64? Wo_Column_Parent_Id { get; set; }
        public int? Wo_Sort_order { get; set; }
    }

    public class WorkOrder_Column_Json_DTO
    {
        public Int64 WC_PeyID { get; set; }
        public Int64 WC_UserId { get; set; }
        public String WC_Right_Column_Json { get; set; }
        public String WC_Left_Column_Json { get; set; }
        public String WC_Query { get; set; }
        public String WC_FilterQuery { get; set; }
        public Boolean? WC_IsActive { get; set; }
        public Boolean? WC_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int val { get; set; }
        public int Type { get; set; }

        public String WC_Show_Column_Jsonarr { get; set; }
        public String WC_Hide_Column_Jsonarr { get; set; }
        public String WhereClause { get; set; }
        public Int64? Wc_Grid_ShortID { get; set; }
         public String WC_Sort_Data { get; set; }

    }
    public class WCShowColumnJsonarr
    {
        public int Wo_Column_PkeyId { get; set; }
        public string Wo_Column_Name { get; set; }
    }

    public class WCHideColumnJsonarr
    {
        public int Wo_Column_PkeyId { get; set; }
        public string Wo_Column_Name { get; set; }
    }

    public class RootObjectdata
    {
        public List<WCShowColumnJsonarr> WC_Show_Column_Jsonarr { get; set; }
        public List<WCHideColumnJsonarr> WC_Hide_Column_Jsonarr { get; set; }
        public int Type { get; set; }
    }
public class SaveFilterDTO
    {
        public String workOrderNumber { get; set; }
        public String status { get; set; }
        public String dueDate { get; set; }
        public String city { get; set; }
        public String state { get; set; }
        public Int64? zip { get; set; }
        public String IPLNO { get; set; }
        public String Lotsize { get; set; }
        public String Lock_Code { get; set; }
        public String Loan_Number { get; set; }
        public String Loan_Info { get; set; }
        public String address1 { get; set; }

        public String Client { get; set; }
        public Int64 WorkType { get; set; }
        public Int64 Cordinator { get; set; }
        public Int64 Processor { get; set; }
        public Int64 Contractor { get; set; }
        public String Category { get; set; }

        public Int64 Customer_Number { get; set; }
        public String Work_Type_Group { get; set; }
        public String County { get; set; }
    }
    public class WorkOrderActionDisplayDTO
    {
        public List<WorkOrderActionsDTO> ChildActionList { get; set; }
        public Int64 Wo_Column_PkeyId { get; set; }
        public String Wo_Column_Name { get; set; }
        public Boolean? Wo_Column_IsActive { get; set; }
        public Boolean? Auto_Assine { get; set; }
        public int Type { get; set; }
        public Int64 UserID { get; set; }
        public Int64? Wo_Column_Parent_Id { get; set; }
    }
}