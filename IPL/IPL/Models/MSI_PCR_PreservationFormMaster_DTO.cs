﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class MSI_PCR_PreservationFormMaster_DTO
    {
        public Int64 MSI_Preservation_PkeyId { get; set; }
        public Int64 MSI_Preservation_WO_ID { get; set; }
        public Int64 MSI_Preservation_CompanyId { get; set; }
        public String MSI_Preservation_SubjectProperty { get; set; }
        public String MSI_Preservation_ConditionReport { get; set; }
        public String MSI_Preservation_BidItems { get; set; }
        public String MSI_Preservation_PhotoManager { get; set; }
        public String MSI_Preservation_Comments { get; set; }
        public String MSI_Preservation_FinalReviews { get; set; }
        public Boolean MSI_Preservation_IsActive { get; set; }
        public Boolean MSI_Preservation_IsDelete { get; set; }
        public Int64 UserID { get; set; }
        public Int64 fwo_pkyeId { get; set; }
        public int Type { get; set; }
    }
}