﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Get_WorkColumnMasterDTO
    {
        public Int64 Type { get; set; }
        public Int64 UserId { get; set; }
        public Int64 CompanyId { get; set; }
        public Int64 Wo_Column_PkeyId { get; set; }
    }
}