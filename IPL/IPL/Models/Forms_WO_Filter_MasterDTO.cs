﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public partial class WO_Filters
    {
        public WO_Filters()
        {
            FiltersCompany = new HashSet<Forms_WO_Filter_CompanyDTO>();
            FiltersCustomer = new HashSet<Forms_WO_Filters_CustomerDTO>();
            FiltersLoanType = new HashSet<Forms_WO_Filter_LoanTypeDTO>();
            FiltersWorkType = new HashSet<Forms_WO_Filter_WorkTypeDTO>();
            FiltersWorkTypeGroup = new HashSet<Forms_WO_Filter_WorkTypeGroupDTO>();
        }
        public long WO_FilterId { get; set; }
        public string WO_Company { get; set; }
        public string WO_WorkType { get; set; }
        public string WO_Customer { get; set; }
        public string WO_LoanType { get; set; }
        public long WO_FormId { get; set; }
        public bool WO_IsActive { get; set; }
        public bool WO_IsDelete { get; set; }
        public long UserId { get; set; }
        public DateTime WO_CreatedOn { get; set; }        
        public DateTime WO_ModifiedOn { get; set; }       
        public DateTime WO_DeletedOn { get; set; }
        public int Type { get; set; }
        public ICollection<Forms_WO_Filter_CompanyDTO> FiltersCompany { get; set; }
        public ICollection<Forms_WO_Filters_CustomerDTO> FiltersCustomer { get; set; }
        public ICollection<Forms_WO_Filter_LoanTypeDTO> FiltersLoanType { get; set; }
        public ICollection<Forms_WO_Filter_WorkTypeDTO> FiltersWorkType { get; set; }
        public ICollection<Forms_WO_Filter_WorkTypeGroupDTO> FiltersWorkTypeGroup { get; set; }
    }
}