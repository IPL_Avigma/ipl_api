﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class BackgroundCheckinProvider_DTO
    {
        public Int64 Back_Chk_ProviderID { get; set; }
        public String Back_Chk_ProviderName { get; set; }
        public Boolean? Back_Chk_IsActive { get; set; }
        public Boolean? Back_Chk_IsDelete { get; set; }

        public String Back_Chk_CreatedBy { get; set; }
        public String Back_Chk_ModifiedBy { get; set; }
        public string FilterData { get; set; }
        public string WhereClause { get; set; }
        public Int64? UserID { get; set; }
        public int? Type { get; set; }
            
    }
}