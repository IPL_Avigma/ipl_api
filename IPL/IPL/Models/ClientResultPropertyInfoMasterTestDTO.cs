﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class ClientResultPropertyInfoMasterTestDTO
    {
        public Int64 CRPIM_PkeyID { get; set; }
        public Int64 CRPIM_WO_ID { get; set; }
        public Int64 CRPIM_AddressID { get; set; }
        public String CRPIM_Lock_Box_Code { get; set; }
        public String CRPIM_LotSize { get; set; }
        public String CRPIM_GPS_Latitude { get; set; }
        public String CRPIM_GPS_Longitude { get; set; }
        public Boolean CRPIM_IsActive { get; set; }
        public Boolean CRPIM_IsDelete { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
    }
}