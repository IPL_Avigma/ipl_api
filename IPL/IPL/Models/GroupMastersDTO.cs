﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPLApp.Models
{
    public class GroupMastersDTO
    {
        public Int64 Grp_pkeyID { get; set; }
        public Int64? GroupRoleId { get; set; }
        public String Grp_Name { get; set; }
        public String Group_DR_Name { get; set; }
        public String Grp_CreatedBy { get; set; }
        public String Grp_ModifiedBy { get; set; }
        public Boolean? Grp_IsActive { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }

        public Boolean? Grp_IsDelete { get; set; }
        public String FilterData { get; set; }
        public SearchMasterDTO SearchMaster { get; set; }
        public string ViewUrl { get; set; }
        public Boolean? Grp_IsDeleteAllow { get; set; }
    }
}