﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class User_AccessBy_CompanyID_DTO
    {
        public Int64 User_IPL_Company_PkeyId { get; set; }
        public Int64 User_Acc_Log_PkeyId { get; set; }
        public String  User_Acc_Log_IP_Address { get; set; }
        public String User_Acc_Log_Logged_In { get; set; }
        public String User_Acc_Log_Logged_Out { get; set; }
        public String User_Acc_Log_MacD { get; set; }
        public String User_Acc_Log_UserName { get; set; }
        public Int64? User_Acc_Log_UserID { get; set; }
        public String Name { get; set; }
        
        public Int64 UserID { get; set; }
        public int Type { get; set; }
    }
}