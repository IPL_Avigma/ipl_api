﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Filter_Admin_PhotoHeader_DTO
    {
        public Int64 Photo_Head_Filter_PkeyId { get; set; }
        public String Photo_Head_Filter_Header_Temp { get; set; }
        public Int64? Photo_Head_Filter_Client_Company { get; set; }
        public String Photo_Head_Filter_File_Name_temp { get; set; }
        public Boolean? Photo_Head_Filter_pdf_temp { get; set; }
        public Boolean? Photo_Head_Filter_IsActive { get; set; }
        public Boolean? Photo_Head_Filter_FilterActive { get; set; }
        public Boolean? Photo_Head_Filter_IsDelete { get; set; }
        public Int64? Photo_Head_Filter_UserId { get; set; }
        public Int64 UserId { get; set; }
        public int Type { get; set; }
    }
}