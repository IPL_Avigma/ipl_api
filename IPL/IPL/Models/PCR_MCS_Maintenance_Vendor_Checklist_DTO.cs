﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class PCR_MCS_Maintenance_Vendor_Checklist_DTO
    {
        public Int64 MVC_PkeyID { get; set; }
        public Int64 MVC_WO_ID { get; set; }
        public Int64 MVC_Company_ID { get; set; }
        public String MVC_Property_Info { get; set; }
        public String MVC_Completion_Info { get; set; }
        public String MVC_Utilities { get; set; }
        public String MVC_Damage { get; set; }
        public String MVC_Winterization_Info { get; set; }
        public String MVC_Violation { get; set; }
        public String MVC_Validation { get; set; }
        public String MVC_Check_Ins { get; set; }
        public String MVC_Notes { get; set; }
        public bool MVC_IsActive { get; set; }
        public bool MVC_IsDelete { get; set; }
        public Int64 UserID { get; set; }
        public Int64 fwo_pkyeId { get; set; }
        public int Type { get; set; }
    }
}