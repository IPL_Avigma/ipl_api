﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Forms_ActionRules
    {
        public Int64 ActionId { get; set; }
        public Int64 ActionRuleId { get; set; }
        public Int64 ActionQuestionId { get; set; }
        public string ActionValue { get; set; }
        public Int64 QuestionId { get; set; }
        public Int64 FormId { get; set; }
        public bool Action_IsActive { get; set; }
        public Int64 UserId { get; set; }
        public DateTime Action_CreatedOn { get; set; }
        public Int16 Type { get; set; }
        public bool Action_IsDelete { get; set; }
        public Int64 QuestionTypeId { get; set; }
        public ICollection<Forms_Que_Options> Options { get; set; }
        public string Question { get; set; }
        public string ActionQuestionText { get; set; }
    }
}