﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Work_Order_SettingDTO
    {
        public Int64 WOS_PkeyID { get; set; }
        public Boolean? WOS_Contractor_Work_Order_Rejection { get; set; }
        public Boolean? WOS_Work_Order_To_Previous_Contractor { get; set; }
        public Boolean? WOS_Contractor_Compancy_Name { get; set; }
        public Boolean? WOS_Contractor_Vendor_Id { get; set; }
        public String WOS_Contractor_list_Sorting { get; set; }
        public Boolean? WOS_EST_Completion_Date { get; set; }
        public Boolean? WOS_EST_Date_Allowed_Past { get; set; }
        public Boolean? WOS_EST_Date_List_Shows { get; set; }
        public int? WOS_New_Contractor_Priorty_WorkOrders { get; set; }

        public String WOS_Browser_Tab_Name { get; set; }
        public Boolean? WOS_Company_Name_On_Work_Order { get; set; }
        public int? WOS_Default_Timezone { get; set; }
        public Boolean? WOS_Client_Name_To_Office_Staff { get; set; }
        public Boolean? WOS_Remove_Pricing { get; set; }
        public Boolean? WOS_Highlight_Pricing { get; set; }
        public Boolean? WOS_Loan_On_Work_Orders { get; set; }
        public Boolean? WOS_Client_Company_On_Work_Orders { get; set; }
        public Boolean? WOS_Customer_On_Work_Orders { get; set; }
        public Boolean? WOS_Priorty_WorkOrder_Que { get; set; }

        public String WOS_Documents_Expiration { get; set; }
        public String WOS_Contractor_Availability { get; set; }
        public String WOS_Escalated_Work_Order { get; set; }
        public String WOS_Client_Cancelled_order { get; set; }
        public String WOS_Client_Workorder_Changed { get; set; }

  
        public Boolean? WOS_Work_Order_Assigned_Coordinator { get; set; }
        public Boolean? WOS_Work_Order_Assigned_Processor { get; set; }
        public Boolean? WOS_Work_Order_Email { get; set; }
        public int? WOS_Contractor_Alert_for_U_read { get; set; }
        public Boolean? WOS_Contractor_Late_Orders { get; set; }
        public Boolean? WOS_Contractor_Opens_Orders { get; set; }

        public Boolean? WOS_Allows_Duplicate_Work_Order { get; set; }
        public Boolean? WOS_Work_Orders_To_Previous_Contractor { get; set; }
        public Boolean? WOS_Show_Recomended_Contractorlist { get; set; }
        public String WOS_Customerdata { get; set; }
        public String WOS_Loan_Number { get; set; }
        public String WOS_LockBox_Code { get; set; }
        public String WOS_Key_Code { get; set; }
        public String WOS_LotSize { get; set; }

        public Boolean? WOS_Photos_be_Labeled { get; set; }
        public Boolean? WOS_Allow_Date_Stamps { get; set; }
        public Boolean? WOS_Allow_Metadata { get; set; }
        public int? WOS_Date_Stamp_Format { get; set; }
        public String WOS_Minimum_Number_of_Photos { get; set; }
        public Boolean? WOS_Bid_Photo_Labels { get; set; }
        public Boolean? WOS_Completion_Photo_Lables { get; set; }
        public Boolean? WOS_Damage_Photo_Lables { get; set; }
        public Boolean? WOS_Inspection_Photo_Lables { get; set; }
        public Boolean? WOS_Custom_Photo_Lables { get; set; }
        public Boolean? WOS_Allow_Invoice_Number { get; set; }
        public Boolean? WOS_Show_Completion_Comments { get; set; }
       
        public Boolean? WOS_Dafault_Transfer_Completion_Task { get; set; }
        public Boolean? WOS_Contractor_Invoice_Flat_Fee { get; set; }
        public Boolean? WOS_Client_Invoice_Flat_Fee { get; set; }
        public Boolean? WOS_Print_Company_Logo_Invoice { get; set; }
        public Boolean? WOS_Print_ABC_Number_on_Invoice { get; set; }
        public Boolean? WOS_Print_Invoice_Date { get; set; }
        public Boolean? WOS_Approved_Invoice { get; set; }
        public Boolean? WOS_Property_Alert_App { get; set; }
        public Boolean? WOS_Occpancy_App { get; set; }
        public Boolean? WOS_IsActive { get; set; }
        public Boolean? WOS_Is_Delete { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
    }

    public class Work_Order_Setting_Master
    {
        public String WOS_PkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}