﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Task_Master_Files_DTO
    {
        public Int64? TMF_Task_Pkey { get; set; }
        public Int64? TMF_Task_pkeyID { get; set; }
        public String TMF_Task_objectName { get; set; }
        public String TMF_Task_FolderName { get; set; }
        public String TMF_Task_FileName { get; set; }
        public String TMF_Task_localPath { get; set; }
        public String TMF_Task_BucketName { get; set; }
        public String TMF_Task_CreatedBy { get; set; }
        public String TMF_Task_ModifiedBy { get; set; }
        public Int64? TMF_Task_ProjectID { get; set; }
        public int? TMF_Task_FileSize { get; set; }
        public int? TMF_Task_FileType { get; set; }
        public DateTime? TMF_Task_UploadTimestamp { get; set; }
        public Int64? TMF_Task_UploadBy { get; set; }
        public Int64? TMF_Task_CustPkey { get; set; }
        public Int64? TMF_Task_ClientPkey { get; set; }
        public Boolean? TMF_Task_IsActive { get; set; }
        public Boolean? TMF_Task_IsDelete { get; set; }
        public Boolean? TMF_Status { get; set; }
        public int Type { get; set; }
        public Int64? UserId { get; set; }
        public IList<DocStatusArray> DocStatusArray { get; set; }
        public Boolean? chkflag { get; set; }
    }
    public class DocStatusArray
    {
        public int TMF_Task_Pkey { get; set; }
        public int TMF_Task_pkeyID { get; set; }
        public bool chkflag { get; set; }

    }
 

}