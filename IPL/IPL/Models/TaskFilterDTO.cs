﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class TaskFilterDTO
    {
        public Int64 Task_pkeyID { get; set; }
        public List<ArrayCustomPriceFilter> ArrayCustomPriceFilter { get; set; }
        public List<ArrayDocument> ArrayDocument { get; set; }
        public List<ArrayPreset> ArrayPreset { get; set; }
        public TaskPhotoSettingDTO TaskPhotoSetting { get; set; }
        public int UserID { get; set; }
        public int Type { get; set; }

        public String Task_File_Array { get; set; }
    }

    public class TaskSettState
    {
        public Int64 IPL_StateID { get; set; }
        public string IPL_StateName { get; set; }
    }

    public class TaskSettCountry
    {
        public Int64 ID { get; set; }
        public string COUNTY { get; set; }
    }

    public class TaskSettCustomer
    {
        public Int64 Cust_Num_pkeyId { get; set; }
        public string Cust_Num_Number { get; set; }
    }

    public class TaskSettContractor
    {
        public Int64 User_pkeyID { get; set; }
        public string User_FirstName { get; set; }
    }

    public class TaskSettCompany
    {
        public Int64 Client_pkeyID { get; set; }
        public string Client_Company_Name { get; set; }
    }

    public class TaskSettLone
    {
        public Int64 Loan_pkeyId { get; set; }
        public string Loan_Type { get; set; }
    }

    public class TaskWorkTypeGroup
    {
        public Int64 Work_Type_Cat_pkeyID { get; set; }
        public string Work_Type_Name { get; set; }
    }
    public class TaskSettWorkType
    {
        public Int64 WT_pkeyID { get; set; }
        public string WT_WorkType { get; set; }
    }
    public class TaskSettLotPriceFilter
    {
        public Int64 Lot_Pricing_PkeyID { get; set; }
        public  string Lot_Pricing_Name { get; set; }
    }

    public class ArrayCustomPriceFilter
    {

        public Int64 Task_sett_pkeyID { get; set; }
        public List<TaskSettState> Task_sett_State { get; set; }
        public List<TaskSettCountry> Task_sett_Country { get; set; }
        public string Task_sett_Zip { get; set; }
        public List<TaskSettCustomer> Task_sett_Customer { get; set; }
        public List<TaskSettContractor> Task_sett_Contractor { get; set; }
        public List<TaskSettCompany> Task_sett_Company { get; set; }
        public List<TaskSettLone> Task_sett_Lone { get; set; }
        public int? Task_sett_Con_Unit_Price { get; set; }
        public int? Task_sett_CLI_Unit_Price { get; set; }
        public bool? Task_sett_Flat_Free { get; set; }
        public bool? Task_sett_Price_Edit { get; set; }
        public bool? Task_sett_Disable_Default { get; set; }
        public List<TaskWorkTypeGroup> Task_Work_TypeGroup { get; set; }
        public List<TaskSettWorkType> Task_sett_WorkType { get; set; }
        public List<TaskSettLotPriceFilter> Task_sett_LotPrice { get; set; }
        public Decimal? Task_sett_LOT_Min { get; set; }
        public Decimal? Task_sett_LOT_Max { get; set; }


        public bool? Task_sett_IsActive { get; set; }
        public bool? Task_sett_IsDelete { get; set; }
    }

    public class WTTaskCompany
    {
        public Int64 Client_pkeyID { get; set; }
        public string Client_Company_Name { get; set; }
    }

    public class WTTaskState
    {
        public Int64 IPL_StateID { get; set; }
        public string IPL_StateName { get; set; }
    }

    public class WTTaskCustomer
    {
        public Int64 Cust_Num_pkeyId { get; set; }
        public string Cust_Num_Number { get; set; }
    }

    public class WTTaskLoneType
    {
        public Int64 Loan_pkeyId { get; set; }
        public string Loan_Type { get; set; }
    }

    public class WTTaskWorkType
    {
        public Int64 WT_pkeyID { get; set; }
        public string WT_WorkType { get; set; }
    }
    public class WTTaskWorkTypeGroup
    {
        public Int64 Work_Type_Cat_pkeyID { get; set; }
        public string Work_Type_Name { get; set; }
    }

    public class ArrayDocument
    {
        public Int64 WT_Task_pkeyID { get; set; }
        public List<WTTaskCompany> WT_Task_Company { get; set; }
        public List<WTTaskState> WT_Task_State { get; set; }
        public List<WTTaskCustomer> WT_Task_Customer { get; set; }
        public List<WTTaskLoneType> WT_Task_LoneType { get; set; }
        public List<WTTaskWorkType> WT_Task_WorkType { get; set; }
        public List<WTTaskWorkTypeGroup> WT_Task_WorkType_Group { get; set; }
        public DateTime? WT_Task_ClientDueDateTo { get; set; }
        public DateTime? WT_Task_ClientDueDateFrom { get; set; }
        public int? Task_sett_State { get; set; }
        public int? Task_sett_Customer { get; set; }
        public int? Task_sett_Company { get; set; }
        public int? Task_sett_Lone { get; set; }
        public int? Work_Type { get; set; }
        public bool? WT_Task_IsActive { get; set; }
        public bool? WT_Task_IsDelete { get; set; }
    }

    public class ArrayPreset
    {

        public Int64 Task_Preset_pkeyId { get; set; }

        public string Task_Preset_Text { get; set; }

        public bool? Task_Preset_IsActive { get; set; }
        public bool? Task_Preset_IsDelete { get; set; }
        public bool? Task_Preset_IsDefault { get; set; }
    }


}