﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Task_Invoice_MasterDTO
    {
        public Int64? Task_Inv_pkeyID { get; set; }
        public Int64? Task_Inv_TaskID { get; set; }
        public Int64? Task_Inv_WO_ID { get; set; }
        public String Task_Inv_Qty { get; set; }
        public Int64? Task_Inv_Uom_ID { get; set; }
        public Decimal? Task_Inv_Cont_Price { get; set; }
        public Decimal? Task_Inv_Cont_Total { get; set; }
        public Decimal? Task_Inv_Clnt_Price { get; set; }
        public Decimal? Task_Inv_Clnt_Total { get; set; }
        public String Task_Inv_Comments { get; set; }
        public Boolean? Task_Inv_Violation { get; set; }
        public Boolean? Task_Inv_damage { get; set; }
        public Boolean? Task_Inv_IsActive { get; set; }
        public Boolean? Task_Inv_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
        public int? Task_Inv_Status { get; set; }

        public bool? Task_Inv_Auto_Invoice { get; set; }
        public String Com_Other_Task_Name { get; set; }
        public Boolean? Task_Inv_Hazards { get; set; }

        public Int64 Task_Ext_pkeyID { get; set; }
        public Int64? Task_Ext_CompletionID { get; set; }
        public Int64? Task_Ext_WO_ID { get; set; }
        public String Task_Ext_Location { get; set; }
        public Int64? Task_Ext_DamageCauseId { get; set; }
        public String Task_Ext_Length { get; set; }
        public String Task_Ext_Width { get; set; }
        public String Task_Ext_Height { get; set; }
        public String Task_Ext_Men { get; set; }
        public String Task_Ext_Hours { get; set; }
        public Boolean? Task_Ext_IsActive { get; set; }

    }
    public class TaskInvMaster
    {
        public String Task_Inv_pkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}