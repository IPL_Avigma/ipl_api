﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class WorkOrder_MSI_MasterDTO
    {
        public Int64 WMSI_PkeyId { get; set; }
        public Int64? WMSI_Order { get; set; }
        public Int64? WMSI_Property_Id { get; set; }
        public String WMSI_OrderType { get; set; }
        public DateTime? WMSI_AssignedDate { get; set; }
        public DateTime? WMSI_DueDate { get; set; }
        public String WMSI_Priority { get; set; }
        public String WMSI_Vendor { get; set; }
        public String WMSI_Mortgagor { get; set; }
        public String WMSI_ClientCode { get; set; }
        public String WMSI_Loan { get; set; }
        public String WMSI_PosRequired { get; set; }
        public String WMSI_HoaContact { get; set; }
        public String WMSI_LoanType { get; set; }
        public String WMSI_sendtoAspen { get; set; }
        public String WMSI_HoaPhone { get; set; }
        public DateTime? WMSI_FtvDate { get; set; }
        public DateTime? WMSI_InitialSecureDate { get; set; }
        public DateTime? WMSI_LastWinterizationDate { get; set; }
        public DateTime? WMSI_LastInspectedDate { get; set; }
        public DateTime? WMSI_LastLockChangeDate { get; set; }
        public DateTime? WMSI_LastSnowRemovalDate { get; set; }
        public String WMSI_LastInspOccupancy { get; set; }
        public String WMSI_Lockbox { get; set; }
        public DateTime? WMSI_InitialGrassCutDate { get; set; }
        public String WMSI_PropertyType { get; set; }
        public String WMSI_Keycode { get; set; }
        public DateTime? WMSI_LastGrassCutDate { get; set; }
        public String WMSI_Color { get; set; }
        public String WMSI_SecurityAccessCode { get; set; }
        public String WMSI_LawnSizeSqFt { get; set; }
        public Boolean? WMSI_IsActive { get; set; }
        public Boolean? WMSI_IsDelete { get; set; }
        public Boolean? WMSI_IsProcessed { get; set; }
        public Int64? WorkOrder_Import_FkeyId { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
        public String gpsLatitude { get; set; }
        public String gpsLongitude { get; set; }
        public string Imp_Wo_File_Name { get; set; }
        public string Imp_Wo_File_Path { get; set; }
        public Int64? WMSI_ImportMaster_Pkey { get; set; }
        public String WMSI_gpsLatitude { get; set; }
        public String WMSI_gpsLongitude { get; set; }
        public String WMSI_Import_File_Name { get; set; }
        public String WMSI_Import_FilePath { get; set; }
        public String WMSI_Import_Pdf_Name { get; set; }
        public String WMSI_Import_Pdf_Path { get; set; }

        public String WMSI_address { get; set; }
        public String WMSI_City { get; set; }
        public String WMSI_State { get; set; }
        public String WMSI_zip { get; set; }
        public String WMSI_loan_or_investor_type { get; set; }
        public String WMSI_point_of_service_question { get; set; }
        public String WMSI_username { get; set; }
        public String WMSI_id { get; set; }
    }
    public class WorkOrder_MSI_Master
    {
        public String WMSI_PkeyId { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}