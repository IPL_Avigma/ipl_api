﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Forms_WO_Filter_WorkTypeGroupDTO
    {
        public long WOF_WorkTypeGroup_pkeyId { get; set; }
        //public long WOF_WorkTypeGroupId { get; set; }
        public long Work_Type_Cat_pkeyID { get; set; }
        public long WOF_FilterId { get; set; }
        public long WOF_FormId { get; set; }
        public bool WOF_WorkTypeGroup_IsActive { get; set; }
        public bool WOF_WorkTypeGroup_IsDelete { get; set; }
        public long UserId { get; set; }
        public DateTime WOF_WorkTypeGroup_CreatedOn { get; set; }    
        public DateTime WOF_WorkTypeGroup_ModifiedOn { get; set; }      
        public DateTime WOF_WorkTypeGroup_DeletedOn { get; set; }
    }
}