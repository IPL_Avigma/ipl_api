﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class WorkOrder_MCS_Item_MasterDTO
    {
        public Int64? WMCSINS_PkeyId { get; set; }
        public String WMCSINS_Ins_Name { get; set; }
        public String WMCSINS_Ins_Details { get; set; }
        public Int64? WMCSINS_Qty { get; set; }
        public Decimal? WMCSINS_Price { get; set; }
        public Decimal? WMCSINS_Total { get; set; }

        public Int64? WMCSINS_FkeyID { get; set; }
        public Boolean? WMCSINS_IsActive { get; set; }
        public Boolean? WMCSINS_IsDelete { get; set; }
        public String WMCSINS_Additional_Details { get; set; }


        public Int64 UserID { get; set; }
        public int Type { get; set; }
    }
    public class WorkOrder_MCS_Item_Master
    {
        public String WMCSINS_PkeyId { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}