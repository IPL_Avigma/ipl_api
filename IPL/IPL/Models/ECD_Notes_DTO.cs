﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{

    public class ECD_Notes
    {
        public Int64 ECD_Note_pkeyID { get; set; }
        public Int64 ECD_Note_WorkOrderId { get; set; }
        public string ECD_Note_Comment { get; set; }
        public DateTime ECD_Note_Date { get; set; }


        public Int64 UserID { get; set; }

        public int Type { get; set; }
    }
    public class ECD_Notes_DTO
    {
        public Int64 ECD_Note_pkeyID { get; set; }
        public Int64 ECD_Note_CompanyID { get; set; }
        public Int64 ECD_Note_WorkOrderId { get; set; }
        public string ECD_Note_Comment { get; set; }
        public DateTime ECD_Note_Date { get; set; }
        public DateTime ECD_Note_CreatedOn { get; set; }
        public Int64 ECD_Note_CreatedBy { get; set; }
        public string CreatedByUser { get; set; }
    }
    public class ECD_Notes_Filter
    {
        public Int64 ECD_Note_pkeyID { get; set; }
        public Int64 ECD_Note_WorkOrderId { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
    }
}