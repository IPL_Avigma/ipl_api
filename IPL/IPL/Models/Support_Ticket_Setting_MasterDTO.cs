﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Support_Ticket_Setting_MasterDTO
    {
        public Int64 Sup_Tickets_Pkey_ID { get; set; }
        public String Sup_Tickets_Phone { get; set; }
        public String Sup_Tickets_Email { get; set; }
        public String Sup_Tickets_Subject { get; set; }
        public String Sup_Tickets_Message { get; set; }
        public int? Sup_Tickets_Ticket_Status { get; set; }
        public Boolean? Sup_Tickets_IsActive { get; set; }
        public Boolean? Sup_Tickets_IsDelete { get; set; }
        public Int64? Sup_Tickets_CompanyID { get; set; }
        public Int64? Sup_Tickets_UserID { get; set; }
        public Int64 UserID { get; set; }
        public Int64? Sup_Tickets_Relation_Id { get; set; }
        public Int64? Sup_Tickets_ID { get; set; }
        public int Type { get; set; }
        public DateTime? AddDate { get; set; }
        public DateTime? LastUpdate { get; set; }
        public String Support_Docs_File_Path { get; set; }
        public String User_FirstName { get; set; }
        public String User_LastName { get; set; }
        public Boolean? Sup_Comment_Show { get; set; }
        public String Sup_Tickets_CreatedBy { get; set; }
        public String Sup_Tickets_ModifiedBy { get; set; }

        public string WhereClause { get; set; }
        public int PageNumber { get; set; }
        public int Rowcount { get; set; }
    }
    public class Support_Ticket_Setting
    {
        public String Sup_Tickets_Pkey_ID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}