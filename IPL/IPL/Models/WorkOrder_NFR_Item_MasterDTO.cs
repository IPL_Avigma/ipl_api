﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class WorkOrder_NFR_Item_MasterDTO
    {
        public Int64 WNFRINS_PkeyId { get; set; }
        public String WNFRINS_Ins_Name { get; set; }
        public String WNFRINS_Ins_Details { get; set; }
        public Int64? WNFRINS_Qty { get; set; }
        public Decimal? WNFRINS_Price { get; set; }
        public Decimal? WNFRINS_Total { get; set; }
        public Int64? WNFRINS_FkeyID { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
    }
    public class WorkOrder_NFR_Item_Master
    {
        public String WNFRINS_PkeyId { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}