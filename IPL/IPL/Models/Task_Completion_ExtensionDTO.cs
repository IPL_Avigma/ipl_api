﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Task_Completion_ExtensionDTO
    {
        public Int64 Task_Cmp_Ext_pkeyID { get; set; }
        public Int64? Task_Cmp_Ext_WO_ID { get; set; }
        public String Task_Cmp_Ext_Location { get; set; }
        public Int64? Task_Cmp_Ext_DamageCauseId { get; set; }
        public String Task_Cmp_Ext_Length { get; set; }
        public String Task_Cmp_Ext_Width { get; set; }
        public String Task_Cmp_Ext_Height { get; set; }
        public String Task_Cmp_Ext_Men { get; set; }
        public String Task_Cmp_Ext_Hours { get; set; }
        public Boolean? Task_Cmp_Ext_IsActive { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
        public Int64? Task_Cmp_Ext_CompletionID { get; set; }
    }
    public class TaskCompletionExtension
    {
        public String Task_Cmp_Ext_pkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}