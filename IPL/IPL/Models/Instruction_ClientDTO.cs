﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Instruction_ClientDTO
    {
        public Int64 Intruction_Client_PkeyId { get; set; }
        public Int64? Intruction_Client_CilentId { get; set; }
        public Int64? Intruction_Client_instId { get; set; }
        public Int64? Intruction_Client_InstJsonId { get; set; }
        public Boolean? Intruction_Client_IsActive { get; set; }
        public Boolean? Intruction_Client_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
    }

    public class Instruction_Json_DTO
    {
        public Int64 Instruction_Json_PkeyId { get; set; }
        public Int64? Instruction_Json_Inst_Id { get; set; }
        public String Instruction_Json_Client { get; set; }
        public String Instruction_Json_WorkType { get; set; }
        public String Instruction_Json_LoanType { get; set; }
        public String Instruction_Json_Customer { get; set; }
        public String Instruction_Json_Work_Group { get; set; }
        public String Instruction_Json_CreatedBy { get; set; }
        public String Instruction_Json_ModifiedBy { get; set; }
        public Boolean? Instruction_Json_IsActive { get; set; }
        public Boolean? Instruction_Json_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
    }

    public class Instruction_WorkType_DTO
    {
        public Int64 Instruction_workType_PkeyId { get; set; }
        public Int64? Instruction_workType_WT_Id { get; set; }
        public Int64? Instruction_workType_InstId { get; set; }
        public Int64? Instruction_workType_Inst_Json_Id { get; set; }
        public Boolean? Instruction_workType_IsActive { get; set; }
        public Boolean? Instruction_workType_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
    }

    public class Instruction_LoanType_DTO
    {
        public Int64 Instruction_Loan_PkeyId { get; set; }
        public Int64? Instruction_Loan_LoanId { get; set; }
        public Int64? Instruction_Loan_InstId { get; set; }
        public Int64? Instruction_Loan_Inst_JsonId { get; set; }
        public Boolean? Instruction_Loan_IsActive { get; set; }
        public Boolean? Instruction_Loan_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
    }

    public class Instruction_Customer_DTO
    {
        public Int64 Instruction_Cust_PkeyId { get; set; }
        public Int64? Instruction_Cust_CustId { get; set; }
        public Int64? Instruction_Cust_InstId { get; set; }
        public Int64? Instruction_Cust_InstJson_Id { get; set; }
        public Boolean? Instruction_Cust_IsActive { get; set; }
        public Boolean? Instruction_Cust_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
    }

    public class Instruction_WT_Group_DTO
    {
        public Int64 Instruction_Wt_Group_PkeyId { get; set; }
        public Int64? Instruction_Wt_Group_Wtg_Id { get; set; }
        public Int64? Instruction_Wt_Group_Inst_Id { get; set; }
        public Int64? Instruction_Wt_Group_InstJsonId { get; set; }
        public Boolean? Instruction_Wt_Group_IsActive { get; set; }
        public Boolean? Instruction_Wt_Group_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
    }

}