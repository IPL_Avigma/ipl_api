﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Filter_Admin_Allowables_Category_MasterDTO
    {
        public Int64 Allowables_Cat_Filter_PkeyId { get; set; }
        public String Allowables_Cat_Filter_Name { get; set; }
        public String Allowables_Cat_Filter_CreatedBy { get; set; }
        public String Allowables_Cat_Filter_ModifiedBy { get; set; }
        public Boolean? Allowables_Cat_Filter_IsActive { get; set; }
        public Boolean? Allowables_Cat_Filter_IsDelete { get; set; }
        public Int64? Allowables_Cat_Filter_CompanyId { get; set; }
        public Int64? Allowables_Cat_Filter_UserId { get; set; }
        public Int64 UserId { get; set; }
        public int Type { get; set; }
        public Boolean? Allowables_Cat_Filter_IsCateActive { get; set; }
    }
}