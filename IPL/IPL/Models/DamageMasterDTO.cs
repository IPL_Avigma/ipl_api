﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class DamageMasterDTO
    {
        public Int64 Damage_pkeyID { get; set; }
        public String Damage_Type { get; set; }
        public int? Damage_Int { get; set; }
        public String WhereClause { get; set; }
        public String FilterData { get; set; }
        public String Damage_Location { get; set; }
        public String Damage_Qty { get; set; }
        public Decimal? Damage_Estimate { get; set; }
        public String Damage_Disc { get; set; }
        public Boolean? Damage_IsActive { get; set; }
        public Boolean? Damage_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
        public string Damage_IntName { get; set; }
        public string Damage_CreatedBy { get; set; }
        public string Damage_ModifiedBy { get; set; }
        public string strDamage_pkeyID { get; set; }
        public string ViewUrl  { get; set; }
        public int Damage_Sys_Type { get; set; }
        public Boolean? Damage_IsDeleteAllow { get; set; }
    }
    public class DamageMaster
    {
        public String Damage_pkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }

    }
}