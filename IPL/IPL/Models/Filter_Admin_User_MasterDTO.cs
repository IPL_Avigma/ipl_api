﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Filter_Admin_User_MasterDTO
    {
        public Int64 User_Filter_PkeyID { get; set; }
        public String User_Filter_First_Name { get; set; }
        public String User_Filter_Last_Name { get; set; }
        public String User_Filter_Company { get; set; }
        public String User_Filter_Login_email { get; set; }
        public Int64? User_Filter_Group { get; set; }
        public String User_Filter_Mobile { get; set; }
        public String User_Filter_CreatedBy { get; set; }
        public String User_Filter_ModifiedBy { get; set; }
        public Boolean? User_Filter_USRIsActive { get; set; }
        public Boolean? User_Filter_IsActive { get; set; }
        public Boolean? User_Filter_IsDelete { get; set; }
        public Int64? User_Filter_Company_ID { get; set; }
        public Int64? User_Filter_UserId { get; set; }
        public Int64? UserId { get; set; }
        public int Type { get; set; }

    }
}