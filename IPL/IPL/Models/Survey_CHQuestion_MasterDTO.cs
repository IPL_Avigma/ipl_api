﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPLApp.Models
{
    public class Survey_CHQuestion_MasterDTO
    {
        public Int64 Sur_que_pkeyId { get; set; }
        public Int64 ParentSur_que_pkeyId { get; set; }

        public String Sur_que_name { get; set; }
        public String Sur_que_id { get; set; }
        public String Sur_que_uuid { get; set; }
        public String Sur_que_answerType { get; set; }
        public String Sur_que_text { get; set; }
        public String Sur_que_question { get; set; }
        public Boolean? Sur_que_required { get; set; }
        public String Sur_que_answerRequiresPics { get; set; }
        public String Sur_que_choices { get; set; }
        public String Sur_que_options { get; set; }
        public Boolean? Sur_que_enabledByDefault { get; set; }
        public Boolean? Sur_que_commentEnabled { get; set; }
        public String Sur_que_modeOptions { get; set; }
        public String Sur_que_mode { get; set; }
        public String Sur_que_answerEnables { get; set; }
        public Boolean? Sur_que_IsActive { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
    }
    public class Survey_CHQuestion_Master
    {
        public Int64 Sur_que_pkeyId { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}