﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Invoice_Contractor_ChildDTO
    {
        public Int64 Inv_Con_Ch_pkeyId { get; set; }
        public Int64? Inv_Con_Ch_ContractorId { get; set; }
        public Int64? Inv_Con_Ch_InvoiceId { get; set; }
        public Int64? Inv_Con_Ch_TaskId { get; set; }
        public Int64? Inv_Con_Ch_Wo_Id { get; set; }
        public Int64? Inv_Con_Ch_Uom_Id { get; set; }
        public String Inv_Con_Ch_Qty { get; set; }
        public Decimal? Inv_Con_Ch_Price { get; set; }
        public Decimal? Inv_Con_Ch_Total { get; set; }
        public Decimal? Inv_Con_Ch_Adj_Price { get; set; }
        public Decimal? Inv_Con_Ch_Adj_Total { get; set; }
        public String Inv_Con_Ch_Comment { get; set; }
        public Boolean? Inv_Con_Ch_IsActive { get; set; }
        public Boolean? Inv_Con_Ch_IsDelete { get; set; }

        public Int64? UserID { get; set; }
        public int Type { get; set; }
        public bool? Inv_Con_Ch_Auto_Invoice { get; set; }

        public Int64? Inv_Con_Ch_Client_ID { get; set; }
        public bool? Inv_Con_Ch_Flate_fee { get; set; }
        public Decimal? Inv_Con_Ch_Discount { get; set; }

        public string Inv_Task_Name { get; set; }

        public string Inv_Con_Ch_Other_Task_Name { get; set; }
    }
}