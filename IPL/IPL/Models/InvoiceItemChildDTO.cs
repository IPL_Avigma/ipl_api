﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPLApp.Models
{
    public class InvoiceItemChildDTO
    {
        public Int64 Inv_Chd_pkeyID { get; set; }
        public Int64? Inv_Chd_ClientID { get; set; }
        public Int64? Inv_Chd_StateID { get; set; }
        public Int64? Inv_Chd_CustomerID { get; set; }
        public Int64? Inv_Chd_LoanTypeID { get; set; }
        public Decimal? Inv_Chd_ContractorUnitPrice { get; set; }
        public Decimal? Inv_Chd_ClientUnitPrice { get; set; }
        public Int64? Inv_Chd_InvoiceID { get; set; }
        public Boolean? Inv_Chd_IsActive { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
    }
 public class  InvoiceItemChild
    {
        public String Inv_Chd_pkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}