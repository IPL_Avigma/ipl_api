﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Support_Ticket_Document_MasterDTO
    {
       public Int64 Support_Docs_PkeyID {get; set;}
       public Int64? Support_Docs_Ticket_ID {get; set;}
       public String Support_Docs_File_Path {get; set;}
      public String Support_Docs_File_Size {get; set;}
      public String Support_Docs_File_Name {get; set;}
      public String Support_Docs_Bucket_Name {get; set;}
      public String Support_Docs_Project_Id {get; set;}
      public String Support_Docs_Object_Name {get; set;}
      public String Support_Docs_Folder_Name {get; set;}
      public String Support_Docs_UploadedBy {get; set;}
      public Boolean? Support_Docs_IsActive {get; set;}
      public Boolean? Support_Docs_IsDelete {get; set;}
      public Int64? Support_Docs_User_ID {get; set;}
      public Int64? Support_Docs_Company_ID {get; set;}
      public Int64 UserID { get; set; }
      public int Type { get; set; }


    }
    public class Support_Ticket_Setting_Document
    {
        public String Support_Docs_PkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}