﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Forms_Question_AnswersDTO
    {
        public Int64 AnswerId { get; set; }
        public Int64 QuestionId { get; set; }
        public string Question { get; set; }
        public Int64 OptionId { get; set; }
        public string AnswerValue { get; set; }
        public string ActualAnswerValue { get; set; }
        public Int64 QuestionTypeId { get; set; }
        public Int64 UserId { get; set; }
        public DateTime Answer_CreatedOn { get; set; }
        public DateTime Answer_ModifiedOn { get; set; }
        public bool Answer_IsActive { get; set; }
        public bool Answer_IsDelete { get; set; }
        public Int64 FormId { get; set; }
        public int Type { get; set; }
        public Int64 WorkOrderId { get; set; }
        public string Address { get; set; }
        public string ModifiedBy { get; set; }
    }
}