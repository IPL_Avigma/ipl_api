﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class PCRDamageMasterDTO
    {
        public Int64 PCR_Damage_pkeyId { get; set; }
        public Int64 PCR_Damage_MasterId { get; set; }
        public Int64 PCR_Damage_WO_Id { get; set; }
        public int? PCR_Damage_ValType { get; set; }
        public String PCR_Damage_Fire_Smoke_Damage_Yes { get; set; }
        public String PCR_Damage_Mortgagor_Neglect_Yes { get; set; }
        public String PCR_Damage_Vandalism_Yes { get; set; }
        public String PCR_Damage_Freeze_Damage_Yes { get; set; }
        public String PCR_Damage_Storm_Damage_Yes { get; set; }
        public String PCR_Damage_Flood_Damage_Yes { get; set; }
        public String PCR_Damage_Water_Damage_Yes { get; set; }
        public String PCR_Damage_Wear_And_Tear_Yes { get; set; }
        public String PCR_Damage_Unfinished_Renovation_Yes { get; set; }
        public String PCR_Damage_Structural_Damage_Yes { get; set; }
        public String PCR_Damage_Excessive_Humidty_Yes { get; set; }
        public String PCR_Urgent_Damages_Roof_Leak_Yes { get; set; }
        public String PCR_Urgent_Damages_Roof_Traped_Yes { get; set; }
        public String PCR_Urgent_Damages_Mold_Damage_Yes { get; set; }
        public String PCR_Urgent_Damages_SeePage_Yes { get; set; }
        public String PCR_Urgent_Damages_Flooded_Basement_Yes { get; set; }
        public String PCR_Urgent_Damages_Foundation_Cracks_Yes { get; set; }
        public String PCR_Urgent_Damages_Wet_Carpet_Yes { get; set; }
        public String PCR_Urgent_Damages_Water_Stains_Yes { get; set; }
        public String PCR_Urgent_Damages_Floors_Safety_Yes { get; set; }
        public String PCR_Urgent_Damages_Other_Causing_Damage_Yes { get; set; }
        public String PCR_Urgent_Damages_Other_Safety_Issue_Yes { get; set; }
        public String PCR_System_Damages_HVAC_System_Damage_Yes { get; set; }
        public String PCR_System_Damages_Electric_Damage_Yes { get; set; }
        public String PCR_System_Damages_Plumbing_Damage_Yes { get; set; }
        public String PCR_System_Damages_Uncapped_Wire_Yes { get; set; }
        public String PCR_Damages_FEMA_Damages_Yes { get; set; }
        public String PCR_Damages_FEMA_Neighborhood_Level_Light { get; set; }
        public String PCR_Damages_FEMA_Trailer_Present { get; set; }
        public String PCR_Damages_FEMA_Property_Level_Light_Moderate { get; set; }
        public String PCR_Damages_Property_Habitable { get; set; }
        public Boolean? PCR_Damages_Property_Habitable_FEMA_Damage_Cause_By_Wind { get; set; }
        public Boolean? PCR_Damages_Property_Habitable_FEMA_Damage_Cause_By_Water { get; set; }
        public Boolean? PCR_Damages_Property_Habitable_FEMA_Damage_Cause_By_Fire { get; set; }
        public Boolean? PCR_Damages_Property_Habitable_FEMA_Damage_Cause_By_Flood { get; set; }
        public String PCR_Damages_FEMA_Damage_Estimate { get; set; }
        public int? PCR_Damages_Damage { get; set; }
        public int? PCR_Damages_Status { get; set; }
        public int? PCR_Damages_Cause { get; set; }
        public int? PCR_Damages_Int_Ext { get; set; }
        public int? PCR_Damages_Building { get; set; }
        public int? PCR_Damages_Room { get; set; }
        public String PCR_Damages_Description { get; set; }
        public String PCR_Damages_Qty { get; set; }
        public String PCR_Damages_Estimate { get; set; }
        public Boolean? PCR_Damages_IsActive { get; set; }
        public Boolean? PCR_Damages_IsDelete { get; set; }
        public Int64 UserID { get; set; }
        public Int64 fwo_pkyeId { get; set; }
        public int Type { get; set; }


    }
    public class PcrDamageDto
    {
        public String PCR_Damage_pkeyId { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}