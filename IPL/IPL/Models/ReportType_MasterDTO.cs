﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class ReportType_MasterDTO
    {
        public Int64 ReportType_PkeyId { get; set; }
        public Int64? ReportType_GroupById { get; set; }
        public String ReportType_Name { get; set; }
        public Boolean? ReportType_IsActive { get; set; }
        public Boolean? ReportType_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
    }
}