﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class TrainingVedioDTO
    {
        public Int64 Training_Vedio_pkeyID { get; set; }
        public String Training_Vedio_Name { get; set; }
        public String Training_Vedio_Path { get; set; }
        public String Training_Vedio_Size { get; set; }
        public String Training_Vedio_Folder { get; set; }
        public String Training_Vedio_Desc { get; set; }
        public Boolean? Training_Vedio_IsActive { get; set; }
        public Boolean? Training_Vedio_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }

        public string WhereClause { get; set; }
        public int PageNumber { get; set; }
        public int Rowcount { get; set; }
        public string FilterData { get; set; }
    }

    public class NewTrainingVedio_Filter
    {
        public string Training_Vedio_Name { get; set; }
        public string Training_Vedio_Path { get; set; }
        public string Training_Vedio_Desc { get; set; }
       


    }
}