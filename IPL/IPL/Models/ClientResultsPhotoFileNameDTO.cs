﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class ClientResultsPhotoFileNameDTO
    {
        public Int64 Client_Result_Photo_Wo_ID { get; set; }
        public String Client_Result_Photo_FileName { get; set; }
        public Int64 Client_Result_Photo_ID { get; set; }
        public String Client_Result_Photo_FolderName { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
    }
}