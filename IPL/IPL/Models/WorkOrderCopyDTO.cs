﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class WorkOrderCopyDTO
    {
        public Int64 workOrder_ID { get; set; }
        public int? WorkOderInfo { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
    }
    public class WorkOrderCopy
    {
        public String workOrder_ID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}