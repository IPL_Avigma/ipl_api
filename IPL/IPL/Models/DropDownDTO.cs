﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPLApp.Models
{
    public class DropDownDTO
    {
        public Int64 Cmp_ID { get; set; }
        public Int64 WT_pkeyID { get; set; }
        public Int64 YR_Company_pkeyID { get; set; }
     
        public String Cmp_Name { get; set; }
        public String WT_WorkType { get; set; }
        public String YR_Company_Name { get; set; }
      
        public Boolean Cmp_IsActive { get; set; }

       


    }
    public class DropDownMasterDTO
    {
        public Int64 WorkOrderID { get; set; }
        public string IPLNo { get; set; }
        public Int64 UserID { get; set; }
        public Int64 FilterID { get; set; }
        public int Type { get; set; }
        public int PageID { get; set; }
        public string WorkOrderID_mul { get; set; }
        public Int64 Imrt_PkeyId { get; set; }
        public Int64 WI_ImportFrom { get; set; }
    }
        public class WorkClientMaster
    {
        public Int64 Client_pkeyID { get; set; }
        public String Client_Company_Name { get; set; }
    }

    public class WorkTypeMasterDataValue
    {
        public Int64 WT_pkeyID { get; set; }
        public String WT_WorkType { get; set; }
    }

    public class CategoryDTO
    {
        public Int64 Cat_ID { get; set; }
        public String Cat_Name { get; set; }
    }
    public class USerMasterData
    {
        public Int64 User_pkeyID { get; set; }
        public String User_FirstName { get; set; }
        public String User_LoginName { get; set; }

    }
    public class CheckinProvider
    {
        public Int64 Back_Chk_ProviderID { get; set; }
        public String Back_Chk_ProviderName { get; set; }
        public Boolean Back_Chk_IsActive { get; set; }
    }


    public class CustomerNumber
    {
        public Int64 Cust_Num_pkeyId { get; set; }
        public String Cust_Num_Number { get; set; }
        
    }




    public class ContractorMaster
    {
        public Int64 Cont_ID { get; set; }
        public String Cont_Name { get; set; }
        public Boolean Cont_IsActive { get; set; }
    }
    public class WorkType
    {
        public Int64 Wrk_ID { get; set; }
        public String Wrk_Name { get; set; }
        public String Wrk_IsActive { get; set; }
    }
    public class StateMaster
    {
      
        public Int64 IPL_StateID { get; set; }
     
        public String IPL_StateName { get; set; }
    }

    public class StateData
    {
        public Int64 IPL_StateID { get; set; }

        public Int64 UserID { get; set; }
        public int Type { get; set; }

        public String StateMaster { get; set; }
        public String WhereClause { get; set; }
    }

    public class RushMaster
    {

        public Int64 rus_pkeyID { get; set; }

        public String rus_Name { get; set; }
    }

    public class GroupRoleDRDMaster
    {

        public Int64 Group_DR_PkeyID { get; set; }

        public String Group_DR_Name { get; set; }

        public bool checkitem { get; set; }
        public Int64 UserId { get; set; }
        public int Type { get; set; }
    }
    public class folderDrdDTO
    {
        public Int64 Fold_Pkey_Id { get; set; }
        public String Fold_Name { get; set; }
    }

    public class ImportClient
    {
        public Int64 WI_ImportFrom { get; set; }
        public Int64 WI_Pkey_ID { get; set; }
        public String WI_FriendlyName { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
    }
    public class DropDownWorkTypeMasterDTO
    {
        public Int64 WorkOrderID { get; set; }
        public Int64 UserID { get; set; }
        public Int64 ClientID { get; set; }
        public int Type { get; set; }

    }

    public class DropDownNewMasterDTO
    {
        public Int64 WorkOrderID { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
        public int PageID { get; set; }
        public string WhereClause { get; set; }
    }
}