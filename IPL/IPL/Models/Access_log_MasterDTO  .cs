﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPLApp.Models
{
    public class Access_log_MasterDTO
    {
        public Int64 Alm_Pkey { get; set; }
        public Int64? Alm_workOrder_ID { get; set; }
        public Int64? Alm_userID { get; set; }
        public String Alm_Remark { get; set; }
        public DateTime? Alm_AccessDate { get; set; }
        public DateTime? Alm_PrevStatusDate { get; set; }
        public int? Alm_Status { get; set; }
        public String Alm_log { get; set; }
        public Boolean? Alm_IsDelete { get; set; }
        public Boolean? Alm_IsActive { get; set; }
        public int? Type { get; set; }
        public Int64? UserId { get; set; }

        public String Status_Name { get; set; }

        public String LogUserName { get; set; }

        public String LogDate { get; set; }

    }


    public class NewAccessLogMaster
    {
        public Int64 Access_PkeyID { get; set; }
        public Int64 Access_WorkerOrderID { get; set; }
        public Int64? Access_CompanyID { get; set; }
        public String Access_Platform { get; set; }
        public string Access_User_Action { get; set; }
        public string Access_Log_data { get; set; }
        public string Access_Type_of_Log { get; set; }
        public string Access_Log_Details { get; set; }
        public DateTime Access_Date { get; set; }
        public Int64? Access_UserID { get; set; }
        public Int64 Access_Master_ID { get; set; }
        public bool Access_IsActive { get; set; }
        public string Access_CreatedBy { get; set; }
        public int Type { get; set; }
    }

    public class Access_User_Log_New_Master
    {
        public Int64 Acc_PkeyID { get; set; }
        public string FilterData { get; set; }
        public string WhereClause { get; set; }
        public int? Type { get; set; }
        public Int64? UserId { get; set; }
        public Int64? PageNumber { get; set; }
        public Int64? Rowcount { get; set; }


    }
    public class NewAccessLogMaster_Filter
    {
        public string Acc_UserAction { get; set; }
        public string Acc_Access_Log_Data { get; set; }
        public string Acc_Type_of_Log { get; set; }
        public string Acc_Log_Details { get; set; }

    }
}