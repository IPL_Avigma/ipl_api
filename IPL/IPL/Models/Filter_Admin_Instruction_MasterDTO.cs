﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Filter_Admin_Instruction_MasterDTO
    {
        public Int64 Ins_Filter_PkeyID { get; set; }
        public String Ins_Filter_InsName { get; set; }
        public String Ins_Filter_InsDesc { get; set; }
        public String Ins_Filter_CreatedBy { get; set; }
        public String Ins_Filter_ModifiedBy { get; set; }
        public Boolean? Ins_Filter_InsIsActive { get; set; }
        public Boolean Ins_Filter_IsDelete { get; set; }
        public Boolean? Ins_Filter_IsActive { get; set; }
        public int? Type { get; set; }
        public Int64? UserId { get; set; }
    }
}