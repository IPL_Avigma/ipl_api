﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class PruvanWorkOrderDTO
    {
        public Int64 workOrder_ID { get; set; }
        public String workOrderNumber { get; set; }
        public String workOrderInfo { get; set; }
        public String address1 { get; set; }
        public String fulladdress { get; set; }
        public String Processor_Name { get; set; }
        public String address2 { get; set; }
        public String city { get; set; }
        public String state { get; set; }
        public Int64? zip { get; set; }
        public String country { get; set; }
        public String options { get; set; }
        public String reference { get; set; }
        public String description { get; set; }
        public String instructions { get; set; }
        public String Mortgagor { get; set; }
        public String status { get; set; }
        public DateTime? dueDate { get; set; }
        public DateTime? startDate { get; set; }
        public String clientInstructions { get; set; }
        public String clientStatus { get; set; }
        public DateTime? clientDueDate { get; set; }
        public String gpsLatitude { get; set; }
        public String gpsLongitude { get; set; }
        public String attribute7 { get; set; }
        public String attribute8 { get; set; }
        public String attribute9 { get; set; }
        public String attribute10 { get; set; }
        public String attribute11 { get; set; }
        public String attribute12 { get; set; }
        public String attribute13 { get; set; }
        public String attribute14 { get; set; }
        public String attribute15 { get; set; }
        public String source_wo_provider { get; set; }
        public String source_wo_number { get; set; }
        public Int64? source_wo_id { get; set; }
        public Int64? controlConfig { get; set; }
        public Int64? services_Id { get; set; }
        public Boolean? IsActive { get; set; }
        public Boolean? IsDelete { get; set; }
        public int currUserId { get; set; }
        public int Type { get; set; }
        public String ErrorCode { get; set; }
        public String workOrderNumber_Org { get; set; }
        public String Loan_Type { get; set; }



        public Int64? WorkType { get; set; }
        public String Company { get; set; }
        public String Com_Name { get; set; }
        public String Com_Phone { get; set; }
        public String Com_Email { get; set; }
        public String Contractor { get; set; }
        public DateTime? Received_Date { get; set; }
        public DateTime? Complete_Date { get; set; }
        public DateTime? Cancel_Date { get; set; }

        public String IPLNO { get; set; }
        public Int64? Category { get; set; }
        public String Loan_Info { get; set; }

        public Int64? Customer_Number { get; set; }
        public Int64? Cordinator { get; set; }
        public Int64? Processor { get; set; }
        public bool? BATF { get; set; }
        public bool? ISInspection { get; set; }
        public String Lotsize { get; set; }
        public Int64? Rush { get; set; }

        public String Lock_Code { get; set; }
        public String Broker_Info { get; set; }
        public String Comments { get; set; }

        public String SM_Name { get; set; }
        public String WT_WorkType { get; set; }
        public String Client_Company_Name { get; set; }
        public String Cont_Name { get; set; }
        public String Work_Type_Name { get; set; }
        public String Cust_Num_Number { get; set; }
        public String rus_Name { get; set; }
        public String Lock_Location { get; set; }
        public String Key_Code { get; set; }
        public String Gate_Code { get; set; }
        public String Loan_Number { get; set; }
        public string Cordinator_Name { get; set; }
        public String Search_Address { get; set; }

        public string ClientMetaData { get; set; }
        public string Office_Approved { get; set; }
        public string Sent_to_Client { get; set; }

        public string Client_Result_Photo_FilePath { get; set; }
        public string Client_Result_Photo_FileName { get; set; }
        public Int64 UserID { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? EstimatedDate { get; set; }


        public String Field_complete { get; set; }
        public DateTime? OfficeApproved_date { get; set; }
        public DateTime? SentToClient_date { get; set; }
        public DateTime? Field_complete_date { get; set; }
        public String Status_Name { get; set; }
        public String whereClause { get; set; }
        public String CORNT_User_LoginName { get; set; }
        public String CORNT_User_FirstName { get; set; }

        public String state_name { get; set; }

        public String Inst_Ch_Comand_Mobile { get; set; }
        public DateTime? assigned_date { get; set; }

        public Boolean? IsProcess { get; set; }
        public Int64? WorkOrder_Import_FkeyId { get; set; }
    }
}