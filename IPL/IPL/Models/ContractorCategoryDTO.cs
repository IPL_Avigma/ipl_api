﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class ContractorCategoryDTO
    {
        public Int64 Con_Cat_PkeyID { get; set; }
        public String Con_Cat_Name { get; set; }
        public String Con_Cat_Back_Color { get; set; }
        public String Con_Cat_Icon { get; set; }
        public String WhereClause { get; set; }
        public String FilterData { get; set; }
        public String Con_Cat_CreatedBy { get; set; }
        public String Con_Cat_ModifiedBy { get; set; }
        public Boolean? Con_Cat_IsActive { get; set; }
        public Boolean? Con_Cat_IsDelete { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
        public Boolean?  Con_Cat_IsDeleteAllow { get; set;}
    }

    public class ContractorCategory
    {
        public String Con_Cat_PkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }

    public class ContractorCategorydrd
    {
        public Int64 Con_Cat_PkeyID { get; set; }
        public String Con_Cat_Name { get; set; }
    }
}