﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class MenuGroupRelationDTO
    {
        public Int64 Mgr_MenuRoleRelation_Id { get; set; }
       
        public Int64? Mgr_Group_Id { get; set; }
        public Int64? Mgr_Menu_Id { get; set; }
        public Boolean? Mgr_IsActive { get; set; }
        public Int64? Type { get; set; }
        public Int64? UserId { get; set; }
    }
    public class MenuGroupRelation
    {
        public String Mgr_MenuRoleRelation_Id { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}