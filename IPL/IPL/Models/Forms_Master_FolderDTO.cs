﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPLApp.Models
{
    public class Forms_Master_FolderDTO
    {
        public Int64 FMF_Pkey { get; set; }
        public String FMF_Name { get; set; }
        public Int64? FMF_CustPkey { get; set; }
        public Int64? FMF_ClientPkey { get; set; }
        public Boolean? FMF_IsActive { get; set; }
        public int? Type { get; set; }
        public Int64? UserId { get; set; }
    }
}