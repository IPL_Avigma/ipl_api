﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class WorkOrder_MSI_Item_JsonDTO
    {
        public Work_order work_order { get; set; }
        public List<InstructionsData> instructions { get; set; }
        public string vendor { get; set; }
        public string mortgagor { get; set; }
        public Client client { get; set; }
        public Property_info property_info { get; set; }
        public List<Point_of_service_question> point_of_service_question { get; set; }
        public string address { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zip { get; set; }
        public string username { get; set; }
        public string id { get; set; }
    }

    public class Work_order
    {
        public Int64? order { get; set; }
        public Int64? property_id { get; set; }
        public String order_type { get; set; }
        public DateTime? assigned_date { get; set; }
        public DateTime? due_date { get; set; }
        public String priority { get; set; }
    }
    public class InstructionsData
    {
        public string instruction_name { get; set; }
        public string description { get; set; }
        public string additional_details { get; set; }
    }

    public class Client
    {
        public String client_code { get; set; }
        public String loan { get; set; }
        public String pos_required { get; set; }
        public String loan_or_investor_type { get; set; } //
        public String send_to_aspen { get; set; }
        public String hoa_contact { get; set; }
        public String hoa_phone { get; set; }
    }
    public class Property_info
    {
        public DateTime? ftv_date { get; set; }
        public DateTime? initial_secure_date { get; set; }
        public DateTime? last_winterization_date { get; set; }
        public DateTime? last_inspected_date { get; set; }
        public DateTime? last_lock_change_date { get; set; }
        public DateTime? last_snow_removal_date { get; set; }
        public String last_insp_occupancy { get; set; }
        public String lockbox { get; set; }
        public DateTime? initial_grass_cut_date { get; set; }
        public String property_type { get; set; }
        public String keycode { get; set; }
        public DateTime? last_grass_cut_date { get; set; }
        public String color { get; set; }
        public String security_access_code { get; set; }
        public String lawn_size_sq_ft { get; set; }
    }


    public class Point_of_service_question
    {
        public string question { get; set; }
        public string answer { get; set; }
    }





}