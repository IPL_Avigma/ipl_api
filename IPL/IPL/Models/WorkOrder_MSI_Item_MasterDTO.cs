﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class WorkOrder_MSI_Item_MasterDTO
    {
        public Int64 WMSINS_PkeyId { get; set; }
        public string WMSINS_InstName { get; set; }
        public string WMSINS_Description { get; set; }
        public Int64? WMSINS_Qty { get; set; }
        public Decimal? WMSINS_Price { get; set; }
        public Decimal? WMSINS_Total { get; set; }
        public string WMSINS_Additional_Instructions { get; set; }
        public Int64? WMSINS_WMSI_FkeyId { get; set; }
        public Boolean? IsActive { get; set; }
        public int Type { get; set; }
        public Int64 UserID { get; set; }
    }
    public class WorkOrder_MSI_Item_Master
    {
        public String WMSINS_PkeyId { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}