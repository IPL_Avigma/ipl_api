﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class ViewMessageWorkOrder
    {
        public Int64 workOrder_ID { get; set; }
        public String workOrderNumber { get; set; }
        public String Status_Name { get; set; }
        public String status { get; set; }
        public String address1 { get; set; }
        public String city { get; set; }
        public String state { get; set; }
        public Int64? zip { get; set; }
        public DateTime? dueDate { get; set; }
        public String IPLNO { get; set; }
        public Int64 UserID { get; set; }
        public String Com_Name { get; set; }
        public Int64? assigned_admin { get; set; }
        public String ContractorName { get; set; }
        public String WT_WorkType { get; set; }
        public String Main_Cat_Name { get; set; }
        public String Cust_Num_Number { get; set; }
        public String CordinatorName { get; set; }
        public String ProcessorName { get; set; }
        public String Client_Company_Name { get; set; }
        public Int64? Cordinator { get; set; }
        public Int64? Processor { get; set; }
        public Int64? Contractor { get; set; }
        public String Company { get; set; }
        public Int64? Customer_Number { get; set; }
        public Int64? Category { get; set; }
        public Int64? WorkType { get; set; }
        public String ContractorLoginName { get; set; }
        public String CordinatorLoginName { get; set; }
        public String ProcessorLoginName { get; set; }
        public String lastUpdate { get; set; }
        public DateTime lastUpdateDate { get; set; }
        public String Contractor_ImagePath { get; set; }
        public String Contractor_Email { get; set; }
        public String Contractor_LoginName { get; set; }
        public String Cordinator_ImagePath { get; set; }
        public String Cordinator_Email { get; set; }
        public String Cordinator_LoginName { get; set; }
        public String Processor_ImagePath { get; set; }    
        public String Processor_Email { get; set; }
        public String Processor_LoginName { get; set; }
        public String Body { get; set; }
        public String Subject { get; set; }
        public int Msg_Count { get; set; }
        public String FullAddress { get; set; }
    }
}