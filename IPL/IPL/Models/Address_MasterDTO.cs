﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Address_MasterDTO
    {
        public Int64 ADD_PkeyId { get; set; }
        public String ADD_address { get; set; }
        public String ADD_city { get; set; }
        public Int16 ADD_state { get; set; }
        public Int16 ADD_zip { get; set; }
        public String ADD_county { get; set; }
        public string ADD_gpsLatitude { get; set; }
        public string ADD_gpsLongitude { get; set; }
        public string ADD_FinalAddress { get; set; }
        public Boolean ADD_IsActive { get; set; }
        public Boolean ADD_IsDelete { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
    }
}