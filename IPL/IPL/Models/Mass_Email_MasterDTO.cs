﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Mass_Email_MasterDTO
    {
        public Int64 Mass_Email_PkeyId { get; set; }
        public Int64? Mass_Email_Group_ID { get; set; }
        public String Mass_Email_From { get; set; }
        public String Mass_Email_Subject { get; set; }
        public String Mass_Email_Message { get; set; }
        public Int64? Mass_Email_Company_Id { get; set; }
        public Int64? Mass_Email_User_Id { get; set; }
        public DateTime? Mass_Email_Schedule_Time { get; set; }
        public Boolean? Mass_Email_IsActive { get; set; }
        public Boolean? Mass_Email_IsDelete { get; set; }
        public Boolean? Mass_Email_Sched_Flag { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
        public String filepath { get; set; }
        public String filename { get; set; }
        public String WhereClause { get; set; }
        public String Mass_Email_FirstName { get; set; }
        public String Mass_Email_Message_text { get; set; }
        public Int64? Mass_File_PkeyId { get; set; }

        //public List<dynamic> Attachmentarr { get; set; }
        //public List<dynamic> filenamearr { get; set; }
        //public List<dynamic> Extension { get; set; }
    }
    public class Mass_Email_Master
    {
        public String Mass_Email_PkeyId { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
    public class User_Group_masterDTO
    {
        public Int64 User_pkeyID { get; set; }
        public String User_FirstName { get; set; }
        public String User_Email { get; set; }
        public int User_Group { get; set; }
        public Boolean? User_IsActive { get; set; }
        public int Type { get; set; }
    }
    public class Attachmentarr
    {
        public String filepath { get; set; }
        public String filename { get; set; }
    }
}