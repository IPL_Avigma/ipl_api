﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPLApp.Models
{
    public class PresetInvoiceDTO
    {
        public Int64 Preset_pkeyID { get; set; }
        public String Preset_Name { get; set; }
        public Int64? Preset_InvoiceItemID { get; set; }
        public Boolean? Preset_IsActive { get; set; }
        public Int64? UserID  { get; set; }
        public int Type  { get; set; }
    }
    public class PresetInvoice
    {
        public String Preset_pkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}