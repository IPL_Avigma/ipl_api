﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class RushesMasterDTO
    {
        public Int64 rus_pkeyID { get; set; }
        public String rus_Name { get; set; }
        public String WhereClause { get; set; }
        public String FilterData { get; set; }
        public String rus_CreatedBy { get; set; }
        public String rus_ModifiedBy { get; set; }
        public Boolean? rus_Active { get; set; }
        public Boolean? rus_IsActive { get; set; }
        public Boolean? rus_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
    }
    public class RushesMaster
    {
        public String rus_pkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}