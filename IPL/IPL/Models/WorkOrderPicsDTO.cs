﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class WorkOrderPicsDTO
    {
        public Int64 Client_Result_Photo_ID { get; set; }
        public String Client_Result_Photo_FileName { get; set; }

        public string Client_Result_Photo_FolderName { get; set; }

        public Int64 WorkOrder_ID { get; set; }

        public String Client_Result_Photo_localPath { get; set; }
        public int Type { get; set; }

    }

    public class WorkOrderMoveCopyPhotoDTO
    {
        public Int64 Client_Result_Photo_Wo_ID { get; set; }
        public String Client_Result_Photo_IPLNo { get; set; }
        public int Client_Result_Photo_CM_Type { get; set; }
        public int Type { get; set; }
        public Int64 UserID { get; set; }
        public string WorkOrderLabelMoveCopyPhoto { get; set; }
        public List<WorkOrderLabelMoveCopyPhotoDTO>  workOrderLabelMoveCopyPhotoDTOs { get; set; }
        public Int64 Client_Result_Photo_TaskId { get; set; }
        public Int64 Client_Result_Photo_Task_Bid_pkeyID { get; set; }
        public int Client_Result_Photo_StatusType { get; set; }
    }

    public class WorkOrderLabelMoveCopyPhotoDTO
    {
        public Int64 Client_Result_Photo_ID { get; set; }
        public Int64 CRP_New_pkeyId { get; set; }
        public Int64 Client_Result_Photo_Bid_Id { get; set; }
        public Int64 Client_Result_Photo_TaskId { get; set; }
        public Int64 Client_Result_Photo_WorkOrderPhotoLabel_ID { get; set; }
        public Int64 Client_Result_Photo_Task_Bid_pkeyID { get; set; }
        public int Client_Result_Photo_StatusType { get; set; }
      

    }

    public class MoveCopy_ClientPhoto_DTO
    {
        public Int64 Client_Result_Photo_Wo_ID { get; set; }
        public String Client_Result_Photo_IPLNo { get; set; }
        public Int64? Client_Result_Photo_ID { get; set; }
        public Int64? CRP_New_pkeyId { get; set; }
        public int? Client_Result_Photo_StatusType { get; set; }
        public int? Client_Result_Photo_CM_Type { get; set; }
        public Int64? UserID { get; set; }
        public int? Type { get; set; }
        public Int64 Client_Result_Photo_TaskId { get; set; }
        public Int64 Client_Result_Photo_Task_Bid_pkeyID { get; set; }
    }
}