﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Filter_Admin_Customer_MasterDTO
    {
        public Int64 Cust_Filter_PkeyId { get; set; }
        public String Cust_Filter_Name { get; set; }
        public String Cust_Filter_CreatedBy { get; set; }
        public String Cust_Filter_ModifiedBy { get; set; }
        public Boolean? Cust_Filter_IsActive { get; set; }
        public Boolean? Cust_Filter_IsDelete { get; set; }
        public Int64? Cust_Filter_CompanyId { get; set; }
        public Int64? Cust_Filter_UserId { get; set; }
        public Int64 UserId { get; set; }
        public int Type { get; set; }
        public Boolean? Cust_Filter_IsCustActive { get; set; }
    }
}