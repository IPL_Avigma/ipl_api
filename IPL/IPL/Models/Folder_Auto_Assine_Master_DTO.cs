﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Folder_Auto_Assine_Master_DTO
    {
        public Int64 Fold_Auto_Assine_PkeyId { get; set; }
        public Int64? Fold_Parent_Id { get; set; }
        public String Fold_Auto_Assine_Client { get; set; }
        public String Fold_Auto_Assine_Customer { get; set; }
        public String Fold_Auto_Assine_LoneType { get; set; }
        public String Fold_Auto_Assine_WorkType { get; set; }
        public String Fold_Auto_Assine_WorkType_Group { get; set; }
        public String Fold_Auto_Assine_State { get; set; }
        public String Fold_Auto_Assine_County { get; set; }
        public Int64? Fold_Auto_Assine_Zip { get; set; }
        public Boolean? Fold_Auto_Assine_IsActive { get; set; }
        public Boolean? Fold_Auto_Assine_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
        public Int64? Folder_File_Master_FK_Id { get; set; }

    }
}