﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPLApp.Models
{
    public class Suggestion_Vote_DTO
    {
        public Int64 Sug_Vote_PkeyID { get; set; }
        public Int64 Sug_Vote_UserID { get; set; }
        public Int64 Sug_Vote_Sug_PkeyID { get; set; }
        public Boolean? Sug_Vote_Val { get; set; }
        public Boolean? Sug_Vote_IsActive { get; set; }
        public Boolean? Sug_Vote_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int? Type { get; set; }
    }
}