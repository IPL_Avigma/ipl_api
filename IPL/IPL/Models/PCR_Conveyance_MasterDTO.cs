﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class PCR_Conveyance_MasterDTO
    {
        public Int64 PCR_Conveyance_pkeyID { get; set; }
        public Int64 PCR_Conveyance_MasterID { get; set; }
        public Int64 PCR_Conveyance_Wo_ID { get; set; }
        public int? PCR_Conveyance_ValType { get; set; }
        public String PCR_Conveyance_Work_Order_Instruction { get; set; }
        public String PCR_Conveyance_Secured_Per_Guidelines { get; set; }
        public String PCR_Conveyance_Additional_Damage { get; set; }
        public String PCR_Conveyance_Bid_On_This_Visit { get; set; }
        public String PCR_Conveyance_Need_Maintenance { get; set; }
        public String PCR_Conveyance_Broom_Swept_Condition { get; set; }
        public String PCR_Conveyance_HUD_Guidelines { get; set; }
        public String PCR_Conveyance_Accidental_Entry { get; set; }
        public String PCR_Conveyance_Features_Are_Secure { get; set; }
        public String PCR_Conveyance_In_Place_Operational { get; set; }
        public String PCR_Conveyance_Property_Of_Animals { get; set; }
        public String PCR_Conveyance_Intact_Secure { get; set; }
        public String PCR_Conveyance_Water_Instruction { get; set; }
        public String PCR_Conveyance_Free_Of_Water { get; set; }
        public String PCR_Conveyance_Moisture_has_Eliminated { get; set; }
        public String PCR_Conveyance_Orderdinance { get; set; }
        public String PCR_Conveyance_Uneven { get; set; }
        public String PCR_Conveyance_Conveyance_Condition { get; set; }
        public Boolean? PCR_Conveyance_Damage { get; set; }
        public Boolean? PCR_Conveyance_Debris { get; set; }
        public Boolean? PCR_Conveyance_Repairs { get; set; }
        public Boolean? PCR_Conveyance_Hazards { get; set; }
        public Boolean? PCR_Conveyance_Other { get; set; }
        public String PCR_Conveyance_Describe { get; set; }
        public String PCR_Conveyance_Note { get; set; }
        public String PCR_Conveyance_Work_Order_Instruction_Reason { get; set; }
        public String PCR_Conveyance_Secured_Per_Guidelines_Reason { get; set; }
        public String PCR_Conveyance_HUD_Guidelines_Reasponce { get; set; }
        public Boolean? PCR_Conveyance_IsActive { get; set; }
        public Boolean? PCR_Conveyance_IsDelete { get; set; }
        public Int64 UserID { get; set; }
        public Int64 fwo_pkyeId { get; set; }
        public int Type { get; set; }
        public String PCR_Conveyance_Shrubs_or_tree { get; set; }

    }
    public class PCR_Conveyance
    {
        public String PCR_Conveyance_pkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}