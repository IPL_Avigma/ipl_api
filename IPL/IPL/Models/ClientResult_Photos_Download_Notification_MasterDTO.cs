﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class ClientResult_Photos_Download_Notification_MasterDTO
    {
        public Int64 PN_Pkey_Id { get; set; }
        public Int64? PN_UserId { get; set; }
        public String PN_Title { get; set; }
        public String PN_Message { get; set; }
        public String PN_DownloadLink { get; set; }
        public DateTime PN_ExpireOn { get; set; }
        public Boolean? PN_IsRead { get; set; }
        public Boolean? PN_IsActive { get; set; }
        public Boolean? PN_IsDelete { get; set; }
        public Int64? PN_CreatedBy { get; set; }
        public DateTime PN_CreatedOn { get; set; }
        public Int64 PN_ModifiedBy { get; set; }
        public DateTime PN_ModifiedOn { get; set; }
        public Int64 PN_DeletedBy { get; set; }
        public DateTime PN_DeletedOn { get; set; }
        public Int64 PN_IplCompanyId { get; set; }
        public Int64 PN_WoId { get; set; }
        public int Type { get; set; }
        public int PN_IsExpired { get; set; }
        public int PN_DN_Type { get; set; }
    }
    public class ClientResult_Photos_Download_FirebaseNotification_DTO
    {
        public String PN_Title { get; set; }
        public String PN_DownloadLink { get; set; }
        public Boolean PN_IsRead { get; set; }
        public Int64? PN_CreatedBy { get; set; }
        public Boolean PN_IsExpired { get; set; }
        public DateTime PN_ExpireOn { get; set; }
        public DateTime PN_CreatedOn { get; set; }
    }
}