﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Get_WorkOrderData_NotProcessedDTO
    {
       public Int64 Type { get; set; }
       public Int64 ImportFrom { get; set; }
       public Int64 UserID { get; set; }
       public Int64 Companyid { get; set; }
       public Int64 WI_Pkey_ID { get; set; }
       public Int64 Pkey_ID_Out { get; set; }
       public Int64 ReturnValue { get; set; }
    }
}