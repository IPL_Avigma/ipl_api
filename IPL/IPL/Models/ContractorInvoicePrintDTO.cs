﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace IPL.Models
{
    public class ContractorInvoicePrintDTO
    {
        public Int64 Inv_Con_pkeyId { get; set; }
        public Int64 Inv_Con_Wo_ID { get; set; }
        public String Inv_Con_Invoce_Num { get; set; }
        public DateTime? Inv_Con_Inv_Date { get; set; }
        public Decimal? Inv_Con_Sub_Total { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
    }
    public class ContractorInvoiceChildPrintDTO
    {
        public Int64 Inv_Con_Ch_pkeyId { get; set; }
        public String Inv_Con_Ch_Qty { get; set; }
        public Decimal? Inv_Con_Ch_Price { get; set; }
        public Decimal? Inv_Con_Ch_Total { get; set; }
        public Decimal? Inv_Con_Ch_Adj_Price { get; set; }
        public Decimal? Inv_Con_Ch_Adj_Total { get; set; }
        public String Inv_Con_Ch_Comment { get; set; }
        public Decimal? Inv_Con_Ch_Discount { get; set; }
        public String Task_Name { get; set; }

    }
    public class ContractorInvoiceWoDetailPrintDTO
    {
        public String workOrderNumber { get; set; }
        public String workOrderInfo { get; set; }
        public String address1 { get; set; }
        public String IPLNO { get; set; }
        public String city { get; set; }
        public String SM_Name { get; set; }    
        public Int64? zip { get; set; }    
        public String country { get; set; }    
        public DateTime? Complete_Date { get; set; }    
        public String Loan_Number { get; set; }    
        public String Loan_Info { get; set; }    
        public String fulladdress { get; set; }    
        public String WT_WorkType { get; set; }    
        public String Client_Company_Name { get; set; }    
        public String Cont_Name { get; set; }    
        public String Cordinator_Name { get; set; }    
        public String Work_Type_Name { get; set; }    
        public String Cust_Num_Number { get; set; }    
        public String ClientMetaData { get; set; }    
        public String ClientName { get; set; }      
        public String ClientAddress { get; set; }      
        public String ContractorAddress { get; set; }      
        public String IPL_Company_Name { get; set; }      
        public String CompanyAddress { get; set; }      
        public String IPL_Company_Mobile { get; set; }
        public String ClientZip { get; set; }
        public String ClientState { get; set; }
        public String ConZip { get; set; }
        public String ConState { get; set; }
        public String IPL_StateName { get; set; }
        public String IPL_Company_PinCode { get; set; }
        public String YR_Company_App_logo { get; set; }
    }
    public class DataTableResponse
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public object data { get; set; }

    }

    public class CustomContPDF
    {
        public dynamic Data { get; set; }
        public string Message { get; set; }
        public string completedate { get; set; }
        public HttpStatusCode HttpStatusCode { get; set; }
    }



}