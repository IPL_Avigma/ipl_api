﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPLApp.Models
{
    public class WorkOrderColumnDTO
    {
        public Int64 Wo_Column_pkeyID { get; set; }
        public String Wo_Column_Name { get; set; }
        public String Wo_Column_Table_ColumnName { get; set; }
        public Boolean? Wo_Column_IsActive { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
    }
}