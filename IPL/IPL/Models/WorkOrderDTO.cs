﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPLApp.Models
{
    public class WorkOrderDTO
    {

    }
    public class Service
    {
        public string serviceName { get; set; }
        public string survey { get; set; }
        public string instructions { get; set; }
        public string attribute1 { get; set; }
        public string attribute2 { get; set; }
        public string attribute3 { get; set; }
        public string attribute4 { get; set; }
        public string attribute5 { get; set; }
        public string attribute6 { get; set; }
        public string attribute7 { get; set; }
        public string attribute8 { get; set; }
        public string attribute9 { get; set; }
        public string attribute10 { get; set; }
        public string attribute11 { get; set; }
        public string attribute12 { get; set; }
        public string attribute13 { get; set; }
        public string attribute14 { get; set; }
        public string attribute15 { get; set; }
        public string source_service { get; set; }
        public string source_service_id { get; set; }
        public string surveyDynamic { get; set; }
    }

    public class WorkOrder
    {
        public string workOrderNumber { get; set; }
        public string workOrderInfo { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zip { get; set; }
        public string country { get; set; }
        public string reference { get; set; }
        public string description { get; set; }
        public string instructions { get; set; }
        public string status { get; set; }
        public int? dueDate { get; set; }
        public object startDate { get; set; }
        public string clientInstructions { get; set; }
        public string clientStatus { get; set; }
        public int? clientDueDate { get; set; }
        public object gpsLatitude { get; set; }
        public object gpsLongitude { get; set; }
        public string attribute7 { get; set; }
        public string attribute8 { get; set; }
        public string attribute9 { get; set; }
        public string attribute10 { get; set; }
        public string attribute11 { get; set; }
        public string attribute12 { get; set; }
        public string attribute13 { get; set; }
        public string attribute14 { get; set; }
        public string attribute15 { get; set; }
        public string source_wo_provider { get; set; }
        public string source_wo_number { get; set; }
        public string source_wo_id { get; set; }
        public object controlConfig { get; set; }
        public string options { get; set; }
        public List<Service> services { get; set; }
    }

    public class RootObject
    {
        public string error { get; set; }
        public int timestamp { get; set; }
        public List<WorkOrder> workOrders { get; set; }
    }

    
}