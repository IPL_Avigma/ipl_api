﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Report_WO_Filter_ChildDTO
    {
        public Int64 Report_WO_Filter_Ch_PkeyId { get; set; }
        public Int64? Report_WO_Filter_Ch_Master_FKeyId { get; set; }
        public String Report_WO_Filter_Ch_FeildName { get; set; }
        public Int64? Report_WO_Filter_Ch_FeildId { get; set; }
        public String Report_WO_Filter_Ch_FeildOperator { get; set; }
        public String Report_WO_Filter_Ch_FeildValue { get; set; }
        public Boolean? Report_WO_Filter_Ch_IsActive { get; set; }
        public Boolean? Report_WO_Filter_Ch_IsDelete { get; set; }
        public DateTime? Report_WO_Filter_Ch_FeildDateValue { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
    }
    public class Report_WO_Filter_Child_Array
    {
        public Int64? Data_Type { get; set; }
        public String Data_Type_Name { get; set; }
        public String Operator_Feild { get; set; }
        public String Value { get; set; }
        public DateTime? DateValue { get; set; }
    }
    public class Report_WO_Filter_Child
    {
        public String Report_WO_Filter_Ch_PkeyId { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}