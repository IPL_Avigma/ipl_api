﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Web;
using static log4net.Appender.RollingFileAppender;

namespace IPL.Models
{
    public class Filter_Admin_Common_Master_DTO
    {
        public Int64 Filter_PkeyID { get; set; }
        public int Filter_PageType { get; set; }
        public String Filter_FilterName { get; set; }
        public Boolean Filter_FilterIsActive { get; set; }
        public Boolean Filter_IsActive { get; set; }
        public Boolean Filter_IsDelete { get; set; }
        public Int64 Filter_CompanyId { get; set; }
        public int Type { get; set; }
        public Int64? UserId { get; set; }
    }
}