﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class PCR_Yard_MaintenanceDTO
    {
        public Int64 PCR_Yard_Maintenance_pkeyId { get; set; }
        public Int64 PCR_Yard_Maintenance_MasterId { get; set; }
        public Int64 PCR_Yard_Maintenance_WO_Id { get; set; }
        public int? PCR_Yard_Maintenance_ValType { get; set; }


        public String PCR_Yard_Maintenance_Grass_Cut_Completed { get; set; }
        public String PCR_Yard_Maintenance_Lot_Size { get; set; }
        public String PCR_Yard_Maintenance_Cuttable_Area { get; set; }
        public String PCR_Yard_Maintenance_Bit_To_Cut_Grass_Lot_Dimension_Lenght { get; set; }
        public String PCR_Yard_Maintenance_Bit_To_Cut_Grass_Lot_Dimension_Width { get; set; }
        public String PCR_Yard_Maintenance_Bit_To_Cut_Grass_Lot_Dimension_Height { get; set; }
        public String PCR_Yard_Maintenance_Bit_To_Cut_Grass_Bid_For_Inital_Cut { get; set; }
        public String PCR_Yard_Maintenance_Bit_To_Cut_Grass_Reason_For_Inital_Cut { get; set; }
        public String PCR_Yard_Maintenance_Bid_Recut { get; set; }
        public String PCR_Yard_Maintenance_Reason_For_Recut { get; set; }
        public String PCR_Yard_Maintenance_Trees_Cut_Back_Order { get; set; }
        public String PCR_Yard_Maintenance_Arrival_Shrubs_Touching_House { get; set; }
        public String PCR_Yard_Maintenance_Arrival_Trees_Touching_House { get; set; }
        public String PCR_Yard_Maintenance_Depature_Trees { get; set; }
        public String PCR_Yard_Maintenance_Were_Trimmed_Insurer_Guidlines { get; set; }

        public String PCR_Yard_Maintenance_Grass_Maintained_No { get; set; } // new

        public String PCR_Yard_Maintenance_Bid_To_Shrubs_Dimensions_Length { get; set; }
        public String PCR_Yard_Maintenance_Bid_To_Shrubs_Dimensions_Width { get; set; }
        public String PCR_Yard_Maintenance_Bid_To_Shrubs_Dimensions_Height { get; set; }
        public String PCR_Yard_Maintenance_Bid_To_Shrubs_Quantity { get; set; }
        public String PCR_Yard_Maintenance_Bid_To_Shrubs_Unit_Price { get; set; }
        public String PCR_Yard_Maintenance_Bid_To_Shrubs_Bid_Amount { get; set; }
        public String PCR_Yard_Maintenance_Bid_To_Shrubs_Location { get; set; }
        public Boolean? PCR_Yard_Maintenance_Bid_To_Shrubs_Reasons_Touching_House { get; set; }
        public Boolean? PCR_Yard_Maintenance_Bid_To_Shrubs_Reasons_Touching_Other_Structure { get; set; }
        public Boolean? PCR_Yard_Maintenance_Bid_To_Shrubs_Reasons_Within_Street_View { get; set; }
        public Boolean? PCR_Yard_Maintenance_Bid_To_Shrubs_Reasons_Affecting_Fencing { get; set; }
        public String PCR_Yard_Maintenance_Bid_To_Shrubs_Causing_Damage { get; set; }
        public String PCR_Yard_Maintenance_Bid_To_Shrubs_Describe { get; set; }

        public String PCR_Yard_Maintenance_Bid_To_Trim_Dimensions_Length { get; set; } // new
        public String PCR_Yard_Maintenance_Bid_To_Trim_Dimensions_Width { get; set; } // new
        public String PCR_Yard_Maintenance_Bid_To_Trim_Dimensions_Height { get; set; } // new
        public String PCR_Yard_Maintenance_Bid_To_Trim_Quantity { get; set; } // new
        public String PCR_Yard_Maintenance_Bid_To_Trim_Unit_Price { get; set; } // new
        public String PCR_Yard_Maintenance_Bid_To_Trim_Bid_Amount { get; set; } // new
        public String PCR_Yard_Maintenance_Bid_To_Trim_Location { get; set; } // new
        public Boolean? PCR_Yard_Maintenance_Bid_To_Trim_Reasons_Touching_House { get; set; } // new
        public Boolean? PCR_Yard_Maintenance_Bid_To_Trim_Reasons_Touching_Other_Structure { get; set; } // new
        public Boolean? PCR_Yard_Maintenance_Bid_To_Trim_Reasons_Within_Street_View { get; set; } // new
        public Boolean? PCR_Yard_Maintenance_Bid_To_Trim_Reasons_Affecting_Fencing { get; set; } // new
        public String PCR_Yard_Maintenance_Bid_To_Trim_Causing_Damage { get; set; } // new
        public String PCR_Yard_Maintenance_Bid_To_Trim_Describe { get; set; } // new
        public String PCR_Yard_Grass_LotSize { get; set; } // new


        public Boolean? PCR_Yard_Maintenance_IsActive { get; set; }
        public Boolean? PCR_Yard_Maintenance_IsDelete { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
        public Int64 fwo_pkyeId { get; set; }

    }
    public class PCR_Yard_Maintenance
    {
        public String PCR_Yard_Maintenance_pkeyId { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}