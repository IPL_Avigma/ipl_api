﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Instruction_DocumentDTO
    {
        public Int64 Inst_Doc_PkeyID { get; set; }
        public Int64? Inst_Doc_Inst_Ch_ID { get; set; }
        public Int64? Inst_Doc_Wo_ID { get; set; }
        public String Inst_Doc_File_Path { get; set; }
        public String Inst_Doc_File_Size { get; set; }
        public String Inst_Doc_File_Name { get; set; }
        public String Inst_Doc_BucketName { get; set; }
        public String Inst_Doc_ProjectID { get; set; }
        public String Inst_Doc_Object_Name { get; set; }
        public String Inst_Doc_Folder_Name { get; set; }
        public String Inst_Doc_UploadedBy { get; set; }
        public Boolean? Inst_Doc_IsActive { get; set; }
        public Boolean? Inst_Doc_IsDelete { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
        public Int64? Folder_File_Master_FKId { get; set; } // Added by Dipali

    }
    public class uploaddata
    {
        public String Inst_Doc_PkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}