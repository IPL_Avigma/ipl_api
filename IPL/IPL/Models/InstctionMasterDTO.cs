﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class InstctionMasterDTO
    {
        public Int64? Instr_pkeyId { get; set; }
        public Int64? Instr_Task_Id { get; set; }
        public Int64? Instr_Task_pkeyId { get; set; }
        public Int64? Instr_WO_Id { get; set; }
        public String Instr_Task_Name { get; set; }
        public String Inst_Task_Name { get; set; }
        public String Instr_Task_Comment { get; set; }
        public String Task_Name { get; set; }
        public int? Instr_Qty { get; set; }
        public Decimal? Instr_Contractor_Price { get; set; }
        public Decimal? Instr_Client_Price { get; set; }
        public Decimal? Instr_Contractor_Total { get; set; }
        public Decimal? Instr_Client_Total { get; set; }
        public int? Instr_Action { get; set; }
        public Boolean? Instr_IsActive { get; set; }
        public Boolean? Instr_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }

        public int InputType { get; set; }

        public String Instr_Details_Data { get; set; }
        public String Instr_Comand_Mobile { get; set; }
        public int? Instr_ValType { get; set; }

        public int? Instr_Qty_Text { get; set; }
        public Decimal? Instr_Price_Text { get; set; }
        public Decimal? Instr_Total_Text { get; set; }

        public Int64? Instr_Ch_pkeyId { get; set; }
        public String Inst_Comand_Mobile_details { get; set; }
        public String TMF_Task_localPath { get; set; }
        public String TMF_Task_FileName { get; set; }


        public int Inst_Task_Type_pkeyId { get; set; }
        public String Instr_Other_Task_Name { get; set; }

        public Boolean Task_Violation { get; set; }
        public Boolean Task_Hazards { get; set; }
        public Boolean Task_damage { get; set; }
    }

    public class InstctionMasterDTORootObject
    {
        public List<InstctionMasterDTO> InstctionMasterDTO { get; set; }

        public Instruction_Master_ChildDTO Instruction_Master_ChildDTO { get; set; }

        public Int64 Instr_WO_Id { get; set; }
        public Int64? Instr_Ch_pkeyId { get; set; }
        public int Type { get; set; }
        public Int64? UserID { get; set; }

    }
    public class Task_Instruction
    {
        public String Task_Instruction_Data { get; set; }
        public String WorkOrder_ID_Data { get; set; }
        public Int64 UserID { get; set; }
    }

    public class InstctionMaster
    {
        public String Instr_pkeyId { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
    public class InstctionMasterDropdownName
    {
        public string Task_pkeyID { get; set; }

        public Int64? Int_Task_pkeyID { get; set; }
        public Int64? Inst_Task_pkeyId { get; set; }
        public String Task_Name { get; set; }
        public Decimal? Task_Contractor_UnitPrice { get; set; }
        public Decimal? Task_Client_UnitPrice { get; set; }
        public int Type { get; set; }
        public Int64? UserID { get; set; }
        public int? Task_Type { get; set; }
        public Int64? WorkOrderID { get; set; }
        public Boolean? Task_Disable_Default { get; set; }
        public string Task_Photo_Label_Name { get; set; }
        public List<TaskPresetDrd> TaskPreset { get; set; }
    }


    public class TaskFilterPriceFlag
    {
        public int Flag { get; set; }
    }





    public class InstctionTaskMaster
    {
        public Int64 Inst_Task_pkeyId { get; set; }
        public Int64? Inst_Task_Type_pkeyId { get; set; }
        public Int64 Instruction_Json_PkeyId { get; set; }

        public String Inst_Task_Name { get; set; }
        public String Inst_Task_Desc { get; set; }
        public Boolean? Inst_Task_IsAutoAssign { get; set; }
        public Boolean? Inst_Task_IsActive { get; set; }
        public Boolean? Inst_Task_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
        public String whereClause { get; set; }
        public String FilterData { get; set; }
        public List<AutoAssinArray> AutoAssinArray { get; set; }
        public Int64? WorkOrderID { get; set; }
        public string ViewUrl { get; set; }
        public String Inst_Task_CreatedBy { get; set; }
        public String Inst_Task_ModifiedBy { get; set; }
    }
    public class InstctionTaskTypeMaster
    {
        public Int64 Ins_Type_pkeyId { get; set; }
        public String Ins_Type_Name { get; set; }
        public Boolean? Ins_Type_IsActive { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
    }


}