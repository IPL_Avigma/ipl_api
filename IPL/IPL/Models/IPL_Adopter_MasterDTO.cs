﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class IPL_Adopter_MasterDTO
    {
        public Int64 IPL_Adopter_PkeyId { get; set; }
        public String IPL_Adopter_Name { get; set; }
        public String IPL_Adopter_Company_Name { get; set; }
        public String IPL_Adopter_Email { get; set; }
        public String IPL_Adopter_Phone { get; set; }
        public String IPL_Adopter_Preservation_Ind { get; set; }
        public String IPL_Adopter_Mobile_App { get; set; }
        public String IPL_Adopter_Process_WO { get; set; }
        public Boolean? IPL_Adopter_IsActive { get; set; }
        public Boolean? IPL_Adopter_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
        public Boolean? IPL_Adop_Ref_flag { get; set; }
        public List<Refrencearray> Refrencearray { get; set; }

        public string WhereClause { get; set; }
        public int PageNumber { get; set; }
        public int Rowcount { get; set; }
        public string FilterData { get; set; }
    }
    public class IPL_Adopter_Master
    {
        public String IPL_Adopter_PkeyId { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
    public class IPL_Adop_Chk_MasterDTO
    {
        public Int64 IPL_Adop_Chk_PkeyID { get; set; }
        public String IPL_Adop_Chk_Name { get; set; }
        public Boolean? IPL_Adop_Chk_IsActive { get; set; }
        public Boolean? IPL_Adop_Chk_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
        public Boolean IPL_Adop_Chk_IsChecked { get; set; }
    }
    public class IPL_Adop_Ref_MasterDTO
    {
        public Int64 IPL_Adop_Ref_pkeyId { get; set; }
        public Int64? IPL_Adop_Ref_Adop_Id { get; set; }
        public Int64? IPL_Adop_Ref_Chk_Id { get; set; }
        public Int64? IPL_Adop_Chk_PkeyID { get; set; }
        public Boolean? IPL_Adop_Ref_flag { get; set; }
        public Boolean? IPL_Adop_Chk_IsChecked { get; set; }
        public Boolean? IPL_Adop_Ref_IsActive { get; set; }
        public Boolean? IPL_Adop_Ref_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int? Type { get; set; }
       
    }

    public class Refrencearray
    {
        public Int64 IPL_Adop_Chk_PkeyID { get; set; }
        public String IPL_Adop_Chk_Name { get; set; }
        public Boolean? IPL_Adop_Chk_IsActive { get; set; }
        public Boolean? IPL_Adop_Chk_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int? Type { get; set; }
        public Boolean? IPL_Adop_Chk_IsChecked { get; set; }
    }

    public class NewIPLAdopterMaster_Filter
    {
        public string IPL_Adopter_Name { get; set; }
        public string IPL_Adopter_Company_Name { get; set; }
        public string IPL_Adopter_Email { get; set; }


    }



}