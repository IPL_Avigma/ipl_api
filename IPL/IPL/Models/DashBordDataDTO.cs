﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class DashBordDataDTO
    {
        public Int64 Company { get; set; }
        public Int64? UserId { get; set; }
        public String whereClause { get; set; }
        public int? Type { get; set; }
        public Decimal? Client_InvoiceTotal { get; set; }
        public Decimal? Con_InvoiceTotal { get; set; }
        public Decimal? Client_Pay_Amount { get; set; }
    }
}