﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class TaskGroupMasterDTO
    {
        public Int64 Task_Group_pkeyID { get; set; }
        public string Task_Group_Name { get; set; }
        public string Task_Group_Client_data { get; set; }
        public string Client_Company_Name { get; set; }
        public string Task_Group_CreatedBy { get; set; }
        public string Task_Group_ModifiedBy { get; set; }
        public List<Task_GroupNameArr> Task_GroupNameArr { get; set; }
        public bool Task_Group_IsDeleteAllow { get; set; }
        public Boolean? Task_Group_IsActive { get; set; }
        public Boolean? Task_Group_IsDelete { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
    }

    public class Task_GroupNameArr
    {
        public Int64 Task_Group_pkeyID { get; set; }
        public string Task_Group_Name { get; set; }
        public string Task_Group_Client_data { get; set; }

    }

    public class Task_GroupClient
    {
        public Int64 Client_pkeyID { get; set; }
        public string Client_Company_Name { get; set; }
        

    }

}