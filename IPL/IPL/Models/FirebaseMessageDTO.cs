﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class FirebaseMessageDTO
    {
        public String message { get; set; }
        public String from { get; set; }
        //public DateTime time { get; set; }
        public object time { get; set; }
        public String avatar { get; set; }
        public String name { get; set; }
        public String threadtype { get; set; }
        public String threadid { get; set; }
        public String status { get; set; }

        public bool readByAdmin { get; set; }
        public bool readByContractor { get; set; }
        public bool readByCoordinator { get; set; }
        public bool readByProcessor { get; set; }
        public bool readByClient { get; set; }
        public int messagesType { get; set; } = 1;
    }
    public class FirebaseFullDetailsDTO
    {
        public string Details { get; set; }
    }

        public class FirebaseUserIPLDTO
    {
        public String IPLNO { get; set; }
        public String lastUpdate { get; set; }
        public String Details { get; set; }
    }
    public class FirebaseUserGroupDTO
    {
        public String id { get; set; }
        public String avatar { get; set; }
    }
    public class FirebaseUserIPLListDTO
    {
        public String IPLNO { get; set; }
        public String lastUpdate { get; set; }
        public DateTime lastUpdateDate { get; set; }
    }

    public class UserDetailDTO
    {
        public String User_LoginName { get; set; }
        public String UserName { get; set; }
        public String IPL_Company_ID { get; set; }
        public Int64 UserID { get; set; }
        public Int64 ContractorID { get; set; }
        public Int64 CordinatorID { get; set; }
        public Int64 ProcessorID { get; set; }
        public Int64 WoID { get; set; }
        public int Type { get; set; }
        public String UserToken { get; set; }
        public String IPLNO { get; set; }
        public String User_CreatedBy { get; set; }
        public String User_ModifiedBy { get; set; }
        public Int64 User_pkeyID { get; set; }
        public Int64? GroupRoleId { get; set; }
        public String address1 { get; set; }
        public String city { get; set; }
        public String state { get; set; }
        public Int64? zip { get; set; }

    }
    public class UserDetailListDTO
    {
        public List<UserDetailDTO> LoggedUserDetail { get; set; }
        public List<UserDetailDTO> ContractorDetail { get; set; }
        public List<UserDetailDTO> CordinatorDetail { get; set; }
        public List<UserDetailDTO> ProcessorDetail { get; set; }
        public Int64 Old_ContractorID { get; set; }
        public Int64 Old_CordinatorID { get; set; }
        public Int64 Old_ProcessorID { get; set; }
        public String IPLNo { get; set; }
        public Int32 status { get; set; }
        public List<UserDetailDTO> Old_ContractorDetail { get; set; }
        public List<UserDetailDTO> Old_CordinatorDetail { get; set; }
        public List<UserDetailDTO> Old_ProcessorDetail { get; set; }
        public List<UserDetailDTO> AdminUserDetail { get; set; }
        public String address1 { get; set; }
        public String city { get; set; }
        public String state { get; set; }
        public Int64? zip { get; set; }

    }
    public class FirebaseMessageWoDTO
    {
        public List<FirebaseMessageUIDTO> WoMessageList { get; set; }
    }
    public class FirebaseMessageUIDTO
    {
        public String message { get; set; }
        public String from { get; set; }
        //public String time { get; set; }
        public object time { get; set; }
        public String avatar { get; set; }
        public String name { get; set; }
        public String threadtype { get; set; }
        public String threadid { get; set; }
        public String status { get; set; }
        public string IPLNo { get; set; }
        public bool readByAdmin { get; set; }
        public bool readByContractor { get; set; }
        public bool readByCoordinator { get; set; }
        public bool readByProcessor { get; set; }
        public bool readByClient { get; set; }
        public int messagesType { get; set; } = 1;
    }
}