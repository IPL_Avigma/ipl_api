﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Mass_File_MasterDTO
    {
        public Int64? Mass_File_PkeyId { get; set; }
        public Int64? Mass_File_Email_ID { get; set; }
        public String Mass_File_FileName { get; set; }
        public String Mass_File_FilePth { get; set; }
        public Boolean? Mass_File_IsActive { get; set; }
        public Boolean? Mass_File_IsDelete { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
        public String Mass_File_Ext { get; set; }


    }
    public class Mass_File_Master
    {
        public String Mass_File_PkeyId { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}