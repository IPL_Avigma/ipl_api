﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Custom_PhotoLabel_Group_MasterDTO
    {
        public Int64 Custom_PhotoLabel_Group_pkeyID { get; set; }
        public String Custom_PhotoLabel_Group_Name { get; set; }
        public Boolean? Custom_PhotoLabel_Group_IsActive { get; set; }
        public Boolean? Custom_PhotoLabel_Group_IsDelete { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
        public List<Custom_PhotoLabel_Group_Arr> Custom_PhotoLabel_Group_Arr { get; set; }
    }
    public class Custom_PhotoLabel_Group_Master_Result
    {
        public String Custom_PhotoLabel_Group_pkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
    public class Custom_PhotoLabel_Group_Arr
    {
        public Int64 Custom_PhotoLabel_Group_pkeyID { get; set; }
        public String Custom_PhotoLabel_Group_Name { get; set; }

    }
}