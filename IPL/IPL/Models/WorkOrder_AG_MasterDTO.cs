﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class WorkOrder_AG_MasterDTO
    {
        public Int64 WAG_PkeyID { get; set; }
        public String WAG_wo_id { get; set; }
        public String WAG_address { get; set; }
        public String WAG_city { get; set; }
        public String WAG_state { get; set; }
        public String WAG_zip { get; set; }
        public String WAG_loan_number { get; set; }
        public String WAG_loan_type { get; set; }
        public DateTime? WAG_due_date { get; set; }
        public String WAG_username { get; set; }
        public String WAG_lot_size { get; set; }
        public DateTime? WAG_received_date { get; set; }
        public String WAG_customer { get; set; }
        public String WAG_work_type { get; set; }
        public String WAG_lock_code { get; set; }
        public String WAG_key_code { get; set; }
        public String WAG_mortgager { get; set; }
        public String WAG_id { get; set; }
        public String WAG_Comments { get; set; }
        public String WAG_gpsLatitude { get; set; }
        public String WAG_gpsLongitude { get; set; }
        public Int64 WAG_ImportMaster_Pkey { get; set; }
        public String WAG_Import_File_Name { get; set; }
        public String WAG_Import_FilePath { get; set; }
        public String WAG_Import_Pdf_Name { get; set; }
        public String WAG_Import_Pdf_Path { get; set; }
        public Boolean? WAG_IsProcessed { get; set; }
        public Boolean? WAG_IsActive { get; set; }
        public Boolean? WAG_IsDelete { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
    }
    public class WorkOrder_AGItem_MasterDTO
    {
        public String wo { get; set; }
        public String address { get; set; }
        public String city { get; set; }
        public String state { get; set; }
        public String zip { get; set; }
        public String loan_number { get; set; }
        public String loan_type { get; set; }
        public DateTime? due_date { get; set; }
        public String username { get; set; }
        public String lot_size { get; set; }
        public DateTime? received_date { get; set; }
        public String customer { get; set; }
        public String work_type { get; set; }
        public String lock_code { get; set; }
        public String key_code { get; set; }
        public String mortgager { get; set; }
        public String wo_id { get; set; }
        public string comments { get; set; }
        public List<AGInstruction> work_order_item_details { get; set; }

    }
    public class AGInstruction
    {
        public string instruction_name { get; set; }
        public string Description { get; set; }
        public string additional_details { get; set; }
        public string Additional_Instructions { get; set; }
        public string Qty { get; set; }
        public string Price { get; set; }
        public string Total { get; set; }
    }
    public class WorkOrder_AG_Master
    {
        public String WAG_PkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}