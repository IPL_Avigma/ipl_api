﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Filter_Admin_State_MasterDTO
    {
        public Int64 State_Filter_PkeyId { get; set; }
        public String State_Filter_Name { get; set; }
        public Boolean? State_Filter_IsActive { get; set; }
        public Boolean? State_Filter_IsDelete { get; set; }
        public Int64? State_Filter_CompanyId { get; set; }
        public Int64? State_Filter_UserId { get; set; }
        public Int64 UserId { get; set; }
        public String State_Filter_CreatedBy { get; set; }
        public String State_Filter_ModifiedBy { get; set; }
        public int Type { get; set; }
        public Boolean? State_Filter_IsStateActive { get; set; }
    }
}