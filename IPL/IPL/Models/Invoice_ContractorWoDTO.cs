﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Invoice_ContractorWoDTO
    {
        public Int64 Inv_Con_pkeyId { get; set; }
        public Int64? Inv_Con_Invoice_Id { get; set; }
        public Int64? Inv_Con_Wo_ID { get; set; }
        public Decimal? Inv_Con_Sub_Total { get; set; }
        public String Inv_Con_Invoce_Num { get; set; }
        public String IPLNO { get; set; }
        public Decimal? Pay_Amount { get; set; }
        public String Pay_Comment { get; set; }
    }
    public class Invoice_ContractorWo
    {
        public String WorkOrderID { get; set; }
    }
    public class Invoice_ContractorWoList
    {
        public List<Invoice_ContractorWo> WorkOrderIDList { get; set; }
        public Int64 UserId { get; set; }
        public int Type { get; set; }
        public string whereClause { get; set; }
    }
}