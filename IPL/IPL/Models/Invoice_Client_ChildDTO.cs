﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Invoice_Client_ChildDTO
    {
        public Int64? Inv_Client_Ch_pkeyId { get; set; }
        public Int64? Inv_Client_Ch_ClientID { get; set; }
        public Int64? Inv_Client_Ch_Wo_Id { get; set; }
        public Int64? Inv_Client_Ch_Task_Id { get; set; }
        public Int64? Inv_Client_Ch_Invoice_Id { get; set; }
        public Int64? Inv_Client_Ch_Uom_Id { get; set; }
        public String Inv_Client_Ch_Qty { get; set; }
        public Decimal? Inv_Client_Ch_Price { get; set; }
        public Decimal? Inv_Client_Ch_Total { get; set; }
        public Decimal? Inv_Client_Ch_Adj_Price { get; set; }
        public Decimal? Inv_Client_Ch_Adj_Total { get; set; }
        public String Inv_Client_Ch_Comment { get; set; }
        public Boolean? Inv_Client_Ch_IsActive { get; set; }
        public Boolean? Inv_Client_Ch_IsDelete { get; set; }

        public Int64? UserID   { get; set; }
        public int Type   { get; set; }

        public bool? Inv_Client_Ch_Flate_Fee { get; set; }
        public Decimal? Inv_Client_Ch_Discount { get; set; }

        public bool? Inv_Client_Ch_Auto_Invoice { get; set; }
        public string Inv_Client_Ch_Other_Task_Name { get; set; }
    }


    public class TaskInvoiceMasterRootObject
   {
        public List<Task_Invoice_MasterDTO> Task_Invoice_MasterDTO { get; set; }
        public Int64 Task_Inv_WO_ID { get; set; }
        public int UserID { get; set; }
        public int Type { get; set; }
        public string str_Task_Bid_WO_ID { get; set; }
    }
}