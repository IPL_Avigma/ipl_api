﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

//changes 
namespace IPL.Models
{
    public class WorkType_Configuration_MasterDTO
    {
        public Int64 WorkType_Configuration_PkeyId { get; set; }
        public Int64? WorkType_Configuration_WorkType_Id { get; set; }
        public String WorkType_Configuration_OrderType_Desc { get; set; }
        public Int64? WorkType_Configuration_OrderType_Id { get; set; }
        public Int64? WorkType_Configuration_Import_From_Id { get; set; }
        public Int64? WorkType_Configuration_Client_Id { get; set; }
        public Int64? WorkType_Configuration_Company_Id { get; set; }
        public Int64? WorkType_Configuration_User_Id { get; set; }
        public Boolean? WorkType_Configuration_IsActive { get; set; }
        public Boolean? WorkType_Configuration_IsDelete { get; set; }
        public String WorkType_OrderTypeCode_MainType { get; set; }

        public string WorkType_Configuration_CreatedBy { get; set; }
        public string WorkType_Configuration_ModifiedBy { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
    }
    public class WorkType_Configuration_Master
    {
        public String WorkType_Configuration_PkeyId { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
    public class WorkType_Configuration_ListDTO
    {
        public List<WorkType_Configuration_MasterDTO> WorkType_Configuration_List { get; set; }
    }
}