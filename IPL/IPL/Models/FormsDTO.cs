﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public partial class FormsDTO
    {
        public FormsDTO()
        {
            WO_Filters = new HashSet<WO_Filters>();
        }
        public long FormId { get; set; }
        public string FormName { get; set; }
        public bool IsRequired { get; set; }
        public bool OfficeResults { get; set; }
        public bool FieldResults { get; set; }
        public bool Form_IsActive { get; set; }

        public string Pdf_form { get; set; }
        public bool Form_IsDelete { get; set; }
        public Int64 UserId { get; set; }
        public DateTime? Form_CreatedOn { get; set; }
        public DateTime? Form_ModifiedOn { get; set; }
        public DateTime? Form_DeletedOn { get; set; }
        public int Type { get; set; }
        public ICollection<WO_Filters> WO_Filters { get; set; }
        public string lastEdited { get; set; }
        public Int64 WorkOrderId { get; set; }
        public string Address { get; set; }
        public string FormNumber_Id { get; set; }
        public string Form_IsPublished { get; set; }
        public int? Form_Version_No { get; set; }
        public int? FormAddedby { get; set; }
        public string whereClause { get; set; }
        public bool? Published { get; set; }
        public bool Form_IsAutoAssign { get; set; }
        public bool Form_IsPDFFile { get; set; }
        public string Form_PDFFilePath { get; set; }
    }

    public partial class FormsWoDTO
    {
        public Int64 Fwo_pkyeId { get; set; }
        public Int64 Fwo_PCRFormId { get; set; }
        public Int64 Fwo_WoId { get; set; }
        public bool Fwo_IsActive { get; set; }
        public Int64 UserId { get; set; }
        public int Type { get; set; }
        public bool Fwo_IsOfficeResult { get; set; }
        public bool Fwo_IsFieldResult { get; set; }
        public bool Fwo_IsPcrAdd { get; set; }
        public bool IsDynamicForm { get; set; }
    }
}