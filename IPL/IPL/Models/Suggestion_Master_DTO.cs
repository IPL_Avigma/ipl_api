﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPLApp.Models
{
    public class Suggestion_Master_DTO
    {
        public Int64 Sug_PkeyID { get; set; }
        public String Sug_Tittle { get; set; }
        public String Sug_Description { get; set; }
        public Int64 Sug_UserID { get; set; }
        public Boolean? Sug_IsActive { get; set; }
        public Boolean? Sug_IsDelete { get; set; }
        public Int64 UserID { get; set; }
        public int? Type { get; set; }
        public String WhereClause { get; set; }
        public int PageNumber { get; set; }
        public int NoofRows { get; set; }
        public int? votecount { get; set; }
    }
}