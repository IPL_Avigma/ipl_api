﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class TaskHazardMaster
    {
        public Int64 Task_Hazard_pkeyID { get; set; }
        public Int64? Task_Hazard_WO_ID { get; set; }
        public String Task_Hazard_Name { get; set; }
        public String Task_Hazard_Description { get; set; }
        public DateTime Task_Hazard_Date_Discovered { get; set; }
        public String Task_Hazard_Comment { get; set; }
        public Boolean? Task_Hazard_IsActive { get; set; }
        public Boolean? Task_Hazard_IsDelete { get; set; }
        public int Task_Hazard_Status { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
    }
    public class TaskHazard
    {
        public String Task_Hazard_pkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }



    public class TaskHazardMasterRootObject
    {
        public List<TaskHazardMaster> TaskHazardMaster { get; set; }
        public Int64 Task_Inv_WO_ID { get; set; }
        public int UserID { get; set; }
        public int Type { get; set; }
        public string str_Task_Bid_WO_ID { get; set; }
    }
}