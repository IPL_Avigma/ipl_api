﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPLApp.Models
{
    public class ServicesRootUI
    {
        public Meta meta { get; set; }
        public List<Question> questions { get; set; }
    }
    public class Meta
    {
        public string surveyTemplateId { get; set; }
        public double version { get; set; }
        public string name { get; set; }
    }

    public class AnswerEnables
    {
        public List<string> N { get; set; }
        public List<string> U { get; set; }
        public List<string> G { get; set; }
        public List<string> C { get; set; }
        public List<string> B { get; set; }
        public List<string> A { get; set; }
        public List<string> D { get; set; }
        public List<string> E { get; set; }
        public List<string> F { get; set; }
        public List<string> Z { get; set; }
    }

    public class AnswerEnables2
    {
        public List<string> Y { get; set; }
        public List<string> A { get; set; }
        public List<string> B { get; set; }
        public List<string> N { get; set; }
        public List<string> Y2 { get; set; }
        public List<string> __invalid_name__ { get; set; }
        public List<string> Shed { get; set; }
        public List<string> Barn { get; set; }
        public List<string> Garage { get; set; }
        public List<string> Other { get; set; }
    }

    public class Constraints
    {
        public int min { get; set; }
        public int max { get; set; }
    }

    public class Question2
    {
        public string name { get; set; }
        public string id { get; set; }
        public string uuid { get; set; }
        public string answerType { get; set; }
        public string text { get; set; }
        public string question { get; set; }
        public bool required { get; set; }
        public object answerRequiresPics { get; set; }
        public List<object> choices { get; set; }
        public List<object> options { get; set; }
        public bool enabledByDefault { get; set; }
        public bool commentEnabled { get; set; }
        public object modeOptions { get; set; }
        public AnswerEnables2 answerEnables { get; set; }
        public string mode { get; set; }
        public string hint { get; set; }
        public List<string> @default { get; set; }
        public Constraints constraints { get; set; }
    }

    public class Question
    {
        public string name { get; set; }
        public string id { get; set; }
        public string uuid { get; set; }
        public string answerType { get; set; }
        public string hint { get; set; }
        public string text { get; set; }
        public string question { get; set; }
        public bool required { get; set; }
        public object answerRequiresPics { get; set; }
        public List<object> choices { get; set; }
        public List<object> options { get; set; }
        public bool enabledByDefault { get; set; }
        public bool commentEnabled { get; set; }
        public object modeOptions { get; set; }
        public AnswerEnables answerEnables { get; set; }
        public string mode { get; set; }
        public string type { get; set; }
        public int? repeat { get; set; }
        public bool? expand { get; set; }
        public string prompt { get; set; }
        public bool? enableTotal { get; set; }
        public bool? anonymous { get; set; }
        public List<Question2> questions { get; set; }
    }


    public class SurveyDynamicDTO
    {
        public Int64 Surveypkey_id { get; set; }
        public Int64? ServicePkey_id { get; set; }

        public bool? anonymous { get; set; }
        public string answerEnables { get; set; }
        public string answerRequiresPics { get; set; }

        public string answerType { get; set; }
        public string choices { get; set; }
        public bool? commentEnabled { get; set; }
        public bool? enableTotal { get; set; }
        public bool? enabledByDefault { get; set; }
        public bool? expand { get; set; }
        public string hint { get; set; }
        public string id { get; set; }
        public string mode { get; set; }
        public string modeOptions { get; set; }
        public string name { get; set; }
        public string options { get; set; }
        public string prompt { get; set; }
        public string question { get; set; }
        public string questions { get; set; }
        public string repeat { get; set; }
        public string required { get; set; }
        public string text { get; set; }
        public string @type { get; set; }
        public string uuid { get; set; }

        public int? Type { get; set; }

        public bool? IsActive { get; set; }

    }



    public class ChildquestionDTO {

        public string answerEnables { get; set; }
        public string answerRequiresPics { get; set; }
        public string answerType { get; set; }
        public string choices { get; set; }
        public bool? commentEnabled { get; set; }
        public bool? enabledByDefault { get; set; }
        public string hint { get; set; }
        public string id { get; set; }
        public string mode { get; set; }
        public string modeOptions { get; set; }
        public string name { get; set; }
        public string options { get; set; }
        public string question { get; set; }
        public string required { get; set; }
        public string text { get; set; }
        public string uuid { get; set; }
        public bool? IsActive { get; set; }
        public int Type { get; set; }


    }
}