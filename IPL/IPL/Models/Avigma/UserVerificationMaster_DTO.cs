﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models.Avigma
{
    public class UserVerificationMaster_DTO
    {
        public Int64 UserVeriPKID { get; set; }
        public String User_LoginName { get; set; }
        public String UserVerificationID { get; set; }
        public Int64? UserID { get; set; }
        public String VerificationCode { get; set; }
        public Boolean? Verification_IsActive { get; set; }
        public Boolean? Verification_IsDelete { get; set; }
        public int? Type { get; set; }
        public Int64? VUserId { get; set; }
        
    }
}