﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Avigma.Models
{
    public class FileUploadDownloadDTO
    {
        public string zipPath { get; set; }
        public string extractPath { get; set; }

        public string DirectoryPath { get; set; }

        public Int64 importpkey { get; set; }

        public Int64 WI_PkeyID { get; set; }

        public string PPW_Number { get; set; }
        public String File_Ext { get; set; }
    }

    
}