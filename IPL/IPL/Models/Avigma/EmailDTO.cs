﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Avigma.Models
{
    public class EmailDTO
    {
        public long CustId { get; set; }
        public long UserId { get; set; }
        public string MainBody { get; set; }
        public String From { get; set; }
        public String To { get; set; }
        public String Subject { get; set; }
        public String Message { get; set; }
        public String CC { get; set; }
        public String BCC { get; set; }
        public String Attachment { get; set; }
        public String Password { get; set; }
        public String Username { get; set; }
        public bool IsBodyHtml { get; set; }
        public List<dynamic> Attachmentarr { get; set; }
        public List<dynamic> filenamearr { get; set; }
    }
    public class DyanmicEmailDTO
    {
        public object To { get; set; }
        public object From { get; set; }
        public object Cc { get; set; }
        public object Bcc { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public List<object> FileLinkes { get; set; }
    }
}