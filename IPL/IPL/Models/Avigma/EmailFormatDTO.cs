﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models.Avigma
{
    public class EmailFormatDTO
    {
        public string MainBody { get; set; }
        public String Subject { get; set; }
        public String Message { get; set; }
    }
}