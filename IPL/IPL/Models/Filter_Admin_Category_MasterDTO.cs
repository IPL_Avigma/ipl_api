﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Filter_Admin_Category_MasterDTO
    {
        public Int64 Category_Filter_PkeyID { get; set; }
        public String Category_Filter_CategoryName { get; set; }
        public Boolean? Category_Filter_CategoryIsActive { get; set; }
        public Boolean Category_Filter_IsActive { get; set; }
        public Boolean? Category_Filter_IsDelete { get; set; }
        public String Category_Filter_CreatedBy { get; set; }
        public String Category_Filter_ModifiedBy { get; set; }
        public int? Type { get; set; }
        public Int64? UserId { get; set; }
    }
}