﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class LotPricingFilterMasterDTO
    {
        public Int64 Lot_Pricing_PkeyID { get; set; }
        public Int64? Lot_Pricing_CompanyID { get; set; }
        public String Lot_Pricing_Name { get; set; }
        public Boolean? Lot_Pricing_IsDeleteAllow { get; set; }
        public Boolean? Lot_Pricing_IsActive { get; set; }
        public Boolean? Lot_Pricing_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public String whereClause { get; set; }
        public String FilterData { get; set; }
        public String Lot_Pricing_CreatedBy { get; set; }
        public String Lot_Pricing_ModifiedBy { get; set; }
        public int Type { get; set; }

    }

    public class ResonseMessages
    {
        public String ID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }

    }
}