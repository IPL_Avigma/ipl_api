﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPLApp.Models
{
    public class UserClientRelationDTO
    {
        public Int64 UCM_pkeyID { get; set; }
        public Int64? UCM_UserID { get; set; }
        public Int64? UCM_ClientID { get; set; }
        public Boolean? UCM_IsActive { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
    }
}