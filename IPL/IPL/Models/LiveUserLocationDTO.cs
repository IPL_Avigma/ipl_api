﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class LiveUserLocationDTO
    {
        public String Name { get; set; }
        public Boolean IsDeleteChecked { get; set; }
        public Int64 UserID { get; set; }
    }
    public class LiveUserLocationListDTO
    {
        public List<LiveUserLocationDTO> LiveUserLocationArray { get; set; }
    }

    public class FirebaseUserDTO
    {
        public Int64 UserID { get; set; }
        public int Type { get; set; }
        public String WhereClause { get; set; }
    }
    public class LiveUserLocationRouteDTO
    {
        public String Name { get; set; }
        public String CellNumber { get; set; }
        public String Latitude { get; set; }
        public String Longitude { get; set; }
    }
    public class LiveUserLocationFBDTO
    {
        public String name { get; set; }
        public String cellNumber { get; set; }
        public String latitude { get; set; }
        public String longitude { get; set; }
    }
}