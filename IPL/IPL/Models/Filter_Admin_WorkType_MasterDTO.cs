﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Filter_Admin_WorkType_MasterDTO
    {
        public Int64 WT_Filter_PkeyId { get; set; }
        public String WT_Filter_Name { get; set; }
        public Int64? WT_Filter_Group { get; set; }
        public Boolean? WT_Filter_WTIsActive { get; set; }
        public Boolean? WT_Filter_IsActive { get; set; }
        public Boolean? WT_Filter_IsDelete { get; set; }
        public Int64? WT_Filter_CompanyId { get; set; }
        public Int64? WT_Filter_UserID { get; set; }
        public Int64 UserId { get; set; }
        public int Type { get; set; }
    }
}