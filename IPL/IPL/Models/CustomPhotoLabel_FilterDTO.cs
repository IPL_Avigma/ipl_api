﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class CustomPhotoLabel_FilterDTO
    {
        public Int64 CustomPhotoLabel_Filter_Master_PkeyId { get; set; }
        public Int64? Custom_PhotoLabel_Master_FkeyId { get; set; }
        public String CustomPhotoLabel_Filter_Client { get; set; }
        public String CustomPhotoLabel_Filter_Customer { get; set; }
        public String CustomPhotoLabel_Filter_LoanType { get; set; }
        public String CustomPhotoLabel_Filter_WorkType { get; set; }
        public String CustomPhotoLabel_Filter_WorkTypeGroup { get; set; }
        public String CustomPhotoLabel_Filter_State { get; set; }
        public String CustomPhotoLabel_Filter_County { get; set; }
        public String CustomPhotoLabel_Filter_CreatedBy { get; set; }
        public String CustomPhotoLabel_Filter_ModifiedBy { get; set; }
        public Boolean? CustomPhotoLabel_Filter_IsActive { get; set; }
        public Boolean? CustomPhotoLabel_Filter_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
    }
    public class CustomPhotoLabel_Filter_ClientDTO
    {
        public Int64 CustomPhotoLabel_Filter_Client_PkeyId { get; set; }
        public Int64? CustomPhotoLabel_Filter_Client_CId { get; set; }
        public Int64? Custom_PhotoLabel_Master_FkeyId { get; set; }
        public Int64? CustomPhotoLabel_Filter_Master_FkeyId { get; set; }
        public Boolean? CustomPhotoLabel_Filter_Client_IsActive { get; set; }
        public Boolean? CustomPhotoLabel_Filter_Client_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
    }
    public class CustomPhotoLabel_Filter_CountyDTO
    {
        public Int64 CustomPhotoLabel_Filter_County_PkeyId { get; set; }
        public Int64? CustomPhotoLabel_Filter_County_CnId { get; set; }
        public Int64? Custom_PhotoLabel_Master_FkeyId { get; set; }
        public Int64? CustomPhotoLabel_Filter_Master_FkeyId { get; set; }
        public Boolean? CustomPhotoLabel_Filter_County_IsActive { get; set; }
        public Boolean? CustomPhotoLabel_Filter_County_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
    }
    public class CustomPhotoLabel_Filter_CustomerDTO
    {
        public Int64 CustomPhotoLabel_Filter_Customer_PkeyId { get; set; }
        public Int64? CustomPhotoLabel_Filter_Customer_CustId { get; set; }
        public Int64? Custom_PhotoLabel_Master_FkeyId { get; set; }
        public Int64? CustomPhotoLabel_Filter_Master_FkeyId { get; set; }
        public Boolean? CustomPhotoLabel_Filter_Customer_IsActive { get; set; }
        public Boolean? CustomPhotoLabel_Filter_Customer_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
    }
    public class CustomPhotoLabel_Filter_LoanTypeDTO
    {
        public Int64 CustomPhotoLabel_Filter_LoanType_PkeyId { get; set; }
        public Int64? CustomPhotoLabel_Filter_LoanType_LId { get; set; }
        public Int64? Custom_PhotoLabel_Master_FkeyId { get; set; }
        public Int64? CustomPhotoLabel_Filter_Master_FkeyId { get; set; }
        public Boolean? CustomPhotoLabel_Filter_LoanType_IsActive { get; set; }
        public Boolean? CustomPhotoLabel_Filter_LoanType_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
    }
    public class CustomPhotoLabel_Filter_StateDTO
    {
        public Int64 CustomPhotoLabel_Filter_State_PkeyId { get; set; }
        public Int64? CustomPhotoLabel_Filter_State_SId { get; set; }
        public Int64? Custom_PhotoLabel_Master_FkeyId { get; set; }
        public Int64? CustomPhotoLabel_Filter_Master_FkeyId { get; set; }
        public Boolean? CustomPhotoLabel_Filter_State_IsActive { get; set; }
        public Boolean? CustomPhotoLabel_Filter_State_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
    }
    public class CustomPhotoLabel_Filter_WorkTypeDTO
    {
        public Int64 CustomPhotoLabel_Filter_WorkType_PkeyId { get; set; }
        public Int64? CustomPhotoLabel_Filter_WorkType_WId { get; set; }
        public Int64? Custom_PhotoLabel_Master_FkeyId { get; set; }
        public Int64? CustomPhotoLabel_Filter_Master_FkeyId { get; set; }
        public Boolean? CustomPhotoLabel_Filter_WorkType_IsActive { get; set; }
        public Boolean? CustomPhotoLabel_Filter_WorkType_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
    }
    public class CustomPhotoLabel_Filter_WorkTypeGroupDTO
    {
        public Int64 CustomPhotoLabel_Filter_WorkTypeGroup_PkeyId { get; set; }
        public Int64? CustomPhotoLabel_Filter_WorkTypeGroup_WGId { get; set; }
        public Int64? Custom_PhotoLabel_Master_FkeyId { get; set; }
        public Int64? CustomPhotoLabel_Filter_Master_FkeyId { get; set; }
        public Boolean? CustomPhotoLabel_Filter_WorkTypeGroup_IsActive { get; set; }
        public Boolean? CustomPhotoLabel_Filter_WorkTypeGroup_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
    }
}