﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class WorkOrder_Cyprexx_Item_MasterDTO
    {
        public Int64? WCYPREXXINS_PkeyId { get; set; }
        public String WCYPREXXINS_Ins_Name { get; set; }
        public String WCYPREXXINS_Ins_Details { get; set; }
        public Int64? WCYPREXXINS_Qty { get; set; }
        public Decimal? WCYPREXXINS_Price { get; set; }
        public Decimal? WCYPREXXINS_Total { get; set; }
        
        public Int64? WCYPREXXINS_FkeyID { get; set; }
        public Boolean? WCYPREXXINS_IsActive { get; set; }
        public Boolean? WCYPREXXINS_IsDelete { get; set; }
        public String WCYPREXXINS_Additional_Details { get; set; }


        public Int64 UserID { get; set; }
        public int Type { get; set; }
    }

    public class WorkOrder_Cyprexx_Item_Master
    {
        public String WCYPREXXINS_PkeyId { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}