﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Import_Queue_Trans_DTO
    {
        public Int64 Imrt_PkeyId { get; set; }
        public String Imrt_Wo_ID { get; set; }
        public Int64? Imrt_Wo_Import_ID { get; set; }
        
        public String Client_Company_Name { get; set; }
        public String WI_FriendlyName { get; set; }
        public int? Imrt_Wo_Count { get; set; }
        public DateTime? Imrt_Last_Run { get; set; }
        public DateTime? Imrt_Next_Run { get; set; }
        public String Imrt_Status_Msg { get; set; }
        public String Imrt_Status_Msg_code { get; set; }
        public String Imrt_State_Filter { get; set; }
        public Boolean? Imrt_Active { get; set; }
        public Boolean? Imrt_IsDelete { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
        public String WhereClause { get; set; }
        public String FilterData { get; set; }
        public int PageNumber { get; set; }
        public int NoofRows { get; set; }
        public Int64 ClientId { get; set; }
        public Int64? Imrt_Import_From_ID { get; set; }
        public Int64? WI_ImportFrom { get; set; }
        public Boolean Imrt_NewOrderCheck { get; set; }
        public Boolean? image_download { get; set; }
        public Int64? Imrt_IPL_CompanyId { get; set; }
        public String Import_BucketName { get; set; }
        public String Import_BucketFolderName { get; set; }
        public String Import_DestBucketFolderName { get; set; }



    }
    public class Import_Queue_Trans_Details
    {
        public String Imrt_PkeyId { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }

    public class ImportWorkOrderNewDTO
    {
        public Int64 Pkey_Id { get; set; }
        public Int64 UserID { get; set; }
        public Int64? WorkType { get; set; }
        public String Contractor { get; set; }
        public Int64? Processor { get; set; }
        public Int64? cordinator { get; set; }
        public int Type { get; set; }
        public List<ImportWoArray> ImportWoArray { get; set; }
        public Int64? Imrt_Import_From_ID { get; set; }
        public Int64? Imrt_PkeyId { get; set; }
        public Int64? Imrt_Wo_Import_ID { get; set; }

    }
    public class ImportWorkOrderNew
    {
        public String Pkey_Id { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }

  

    public class ImportWoArray
    {
        public Int64 Pkey_Id { get; set; }
        public Int64? WorkType_Id { get; set; }
        public Int64? Contractor_Id { get; set; }
        public Int64? Coordinator_Id { get; set; }
        public Int64? Processor_Id { get; set; }
    }
}