﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPLApp.Models
{
    public class MenuMasterDTO
    {
        public Int64 Ipre_MenuID { get; set; }
        public String Ipre_MenuName { get; set; }
        public String Ipre_PageName { get; set; }
        public String Ipre_PageUrl { get; set; }
        public Boolean? Ipre_IsActive { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
        public Int64 Mgr_Group_Id { get; set; }
    }

    public class ParentLoginMenu
    {
        public Int64? MenuId { get; set; }
        public string MenuName { get; set; }
        public string URL { get; set; }
        public string IconTag { get; set; }

        public List<CommanLoginMenu> ChildMenu { get; set; }

    }
    public class CommanLoginMenu
    {
        public Int64? MenuId { get; set; }
        public string MenuName { get; set; }
        public string URL { get; set; }
        public string IconTag { get; set; }

        public Boolean IsAssignedMenu { get; set; }


    }

    public class Group_Masters_MenuDTO
    {
        public int Grp_pkeyID { get; set; }
        public string Grp_Name { get; set; }
        public Int64? GroupRoleId { get; set; }
        public List<CommanLoginMenu> MenuArray { get; set; }
        public bool Grp_IsActive { get; set; }
        public bool Grp_IsDelete { get; set; }
        public int UserID { get; set; }
        public int Type { get; set; }
    }

}