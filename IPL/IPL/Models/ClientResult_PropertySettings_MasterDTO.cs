﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class ClientResult_PropertySettings_MasterDTO
    {
        public Int64 CRPS_PkeyID { get; set; }
        public Int64? PS_WO_ID { get; set; }
        public Int64? CRPS_Client { get; set; }
        public Boolean? CRPS_Environmental_Flag { get; set; }
        public Int64? CRPS_Customer { get; set; }
        public Int64? CRPS_Property_Type { get; set; }
        public String CRPS_HOA_Name { get; set; }
        public String CRPS_HOA_Identifier { get; set; }
        public String CRPS_HOA_PhoneNo { get; set; }
        public String CRPS_Tax_Parcel_Number { get; set; }
        public String CRPS_Vacant_Land_Identifier { get; set; }
        public String CRPS_Property_Id { get; set; }
        public String CRPS_AssetID { get; set; }
        public Boolean? CRPS_Inspection_Required { get; set; }
        public Boolean? CRPS_VPR_Required { get; set; }
        public Boolean? CRPS_Recurring_Grass_Cuts { get; set; }
        public Boolean? PS_IsActive { get; set; }
        public Boolean? CRPS_IsDelete { get; set; }
        public Boolean? CRPS_Pool_On_Site { get; set; }
        public Boolean? CRPS_Sump_Pump_On_Site { get; set; }
        public Boolean? CRPS_Sump_Pump_Operational { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
    }
    public class ClientResult_PropertySetting_Master
    {
        public String CRPS_PkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}