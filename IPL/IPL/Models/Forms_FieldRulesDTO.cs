﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Forms_FieldRulesDTO
    {
        public Int64 FieldRuleId { get; set; }
        public string FieldRule_Operator { get; set; }
        public string FieldRule_Value { get; set; }
        public bool FieldRule_IsActive { get; set; }
        public DateTime FieldRule_CreatedOn { get; set; }
        public Int64 UserId { get; set; }
        public Int64 QuestionId { get; set; }
        public Int64 FormId { get; set; }
        public Int16 Type { get; set; }
        public bool FieldRule_IsDelete { get; set; }
        public string FieldRule_ValueText { get; set; }
    }
}