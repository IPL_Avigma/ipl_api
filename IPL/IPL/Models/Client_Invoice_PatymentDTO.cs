﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Client_Invoice_PatymentDTO
    {
        public Int64 Client_Pay_PkeyId { get; set; }
        public Int64? Client_Pay_Invoice_Id { get; set; }
        public Int64? Client_Pay_Wo_Id { get; set; }
        public DateTime? Client_Pay_Payment_Date { get; set; }
        public Decimal? Client_Pay_Amount { get; set; }
        public String Client_Pay_CheckNumber { get; set; }
        public String Client_Pay_Comment { get; set; }
        public String Client_Pay_EnteredBy { get; set; }
        public Decimal? Client_Pay_Balance_Due { get; set; }
        public Boolean? Client_Pay_IsActive { get; set; }
        public Boolean? Client_Pay_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
        public Int64 Pay_PkeyId { get; set; }
        public Int64 Pay_Wo_Id { get; set; }
    }

    public class Client_Invoice_MultiPatymentDTO
    {
        public Int64 Inv_Client_pkeyId { get; set; }
        public Int64? Inv_Client_Invoice_Id { get; set; }
        public Int64? Inv_Client_WO_Id { get; set; }

        public Decimal? Inv_Client_Sub_Total { get; set; }
        public String Inv_Client_Invoice_Number { get; set; }
        public String IPLNO { get; set; }
        public String FilterData { get; set; }
        public String whereclause { get; set; }
        public int Type { get; set; }
        public Decimal? Client_Amount { get; set; }
        public String Client_Comment { get; set; }
    }
    public class FilterDataDto
    {
        public int WorkOrderID { get; set; }
    }


}