﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class TaskPresetDTO
    {
        public Int64 Task_Preset_pkeyId { get; set; }
        public Int64 Task_Preset_ID { get; set; }
        public String Task_Preset_Text { get; set; }
        public Boolean? Task_Preset_IsActive { get; set; }
        public Boolean? Task_Preset_IsDelete { get; set; }
        public Boolean? Task_Preset_IsDefault { get; set; }

        public Int64? UserID { get; set; }
        public int Type { get; set; }
    }
    public class TaskPresetDrd
    {
        public Int64 Task_Preset_pkeyId { get; set; }
        public String Task_Preset_Text { get; set; }
    }
}