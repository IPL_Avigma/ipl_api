﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models.FiveBrothers
{
    public class Import_Client_Buildings_MasterDTO
    {
        public Int64 Import_Client_Buildings_PkeyID { get; set; }
        public String Import_Client_Buildings_Name { get; set; }
        public Int64? Import_Client_Buildings_Import_From_Id { get; set; }
        public Int64? Import_Client_Buildings_Client_Id { get; set; }
        public Int64? Import_Client_Buildings_Company_Id { get; set; }
        public Int64? Import_Client_Buildings_User_Id { get; set; }
        public Boolean? Import_Client_Buildings_IsActive { get; set; }
        public Boolean? Import_Client_Buildings_IsDelete { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
    }
    public class Import_Client_Buildings_Master
    {
        public String Import_Client_Buildings_PkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}