﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models.FiveBrothers
{
    public class Ipl_Fb_GrassDTO
    {
        public Int64? ordernumber { get; set; }
        public DateTime? uploadedDate { get; set; }
        public DateTime? dateCompleted { get; set; }
        public String contractor { get; set; }
        public String forRent { get; set; }
        public String forSale { get; set; }
        public String realtorPhone { get; set; }
        public String realtorName { get; set; }
        public String occupancy { get; set; }
        public String propertySecure { get; set; }
        public int? brokenOpenings { get; set; }
        public int? boardedOpenings { get; set; }
        public String poolExists { get; set; }
        public String poolSecured { get; set; }
        public String exteriorDamageFire { get; set; }
        public String exteriorDamageNeglect { get; set; }
        public String exteriorDamageVandal { get; set; }
        public String exteriorDamageFreeze { get; set; }
        public String exteriorDamageStorm { get; set; }
        public String exteriorDamageFlood { get; set; }
        public String exteriorDamageRoofleak { get; set; }
        public String violationPosted { get; set; }
        public String notes { get; set; }
        public String grasscutCompleted { get; set; }
        public String exteriorDebris { get; set; }
        public String version { get; set; }
        public String shrubsTouching { get; set; }
        public String treesTouching { get; set; }
        public String formType { get; set; }
        public String subcontnum { get; set; }
        public int? numberOfPhotos { get; set; }
        public String damageDescription { get; set; }
        public String newDamages { get; set; }
        public String unableToCut { get; set; }
        public String unableToCutOther { get; set; }
        public String lotsize { get; set; }
    }
}