﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models.FiveBrothers
{
    public class Import_Client_TaskBid_DamageCaseDTO
    {
        public Int64 Import_Client_DamageCauses_PkeyId { get; set; }
        public String Import_Client_DamageCauses_Name { get; set; }
        public Int64? Import_Client_DamageCauses_Import_From_Id { get; set; }
        public Int64? Import_Client_DamageCauses_Client_Id { get; set; }
        public Int64? Import_Client_DamageCauses_Company_Id { get; set; }
        public Int64? Import_Client_DamageCauses_UserID { get; set; }
        public Boolean? Import_Client_DamageCauses_IsActive { get; set; }
        public Boolean? Import_Client_DamageCauses_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
    }
    public class Import_Client_TaskBid_DamageCase
    {
        public String Import_Client_DamageCauses_PkeyId { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}