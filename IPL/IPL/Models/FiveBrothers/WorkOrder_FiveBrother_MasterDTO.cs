﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models.FiveBrothers
{
    public class WorkOrder_FiveBrother_MasterDTO
    {
        public Int64 WorkOrder_FiveBrother_PkeyId { get; set; }
        public Int64? WorkOrder_FiveBrother_Number { get; set; }
        public String WorkOrder_FiveBrother_Address { get; set; }
        public String WorkOrder_FiveBrother_City { get; set; }
        public String WorkOrder_FiveBrother_State { get; set; }
        public String WorkOrder_FiveBrother_Zip { get; set; }
        public String WorkOrder_FiveBrother_Name { get; set; }
        public DateTime? WorkOrder_FiveBrother_DateOrdered { get; set; }
        public DateTime? WorkOrder_FiveBrother_EarliestDate { get; set; }
        public DateTime? WorkOrder_FiveBrother_DueDate { get; set; }
        public String WorkOrder_FiveBrother_Photo_Requirements { get; set; }
        public String WorkOrder_FiveBrother_OrderInstructions { get; set; }
        public String WorkOrder_FiveBrother_PhotoInstructions { get; set; }
        public String WorkOrder_FiveBrother_DateCancelled { get; set; }
        public String WorkOrder_FiveBrother_FormType { get; set; }
        public String WorkOrder_FiveBrother_OrderType { get; set; }
        public String WorkOrder_FiveBrother_Customer { get; set; }
        public String WorkOrder_FiveBrother_Contractor { get; set; }
        public String WorkOrder_FiveBrother_Callingcard { get; set; }
        public String WorkOrder_FiveBrother_Loannumber { get; set; }
        public String WorkOrder_FiveBrother_Mortco { get; set; }
        public String WorkOrder_FiveBrother_LoanType { get; set; }
        public String WorkOrder_FiveBrother_Department { get; set; }
        public String WorkOrder_FiveBrother_WorkValidation { get; set; }
        public String WorkOrder_FiveBrother_Version { get; set; }
        public String WorkOrder_FiveBrother_Lotsize { get; set; }
        public String WorkOrder_FiveBrother_Lockbox { get; set; }
        public String WorkOrder_FiveBrother_Latitude { get; set; }
        public String WorkOrder_FiveBrother_Longitude { get; set; }
        public String WorkOrder_FiveBrother_OrderTypeCode { get; set; }
        public String WorkOrder_FiveBrother_SubContractor { get; set; }
        public Boolean? WorkOrder_FiveBrother_Previous { get; set; }
        public Int64? WorkOrder_FiveBrother_Import_From_Id { get; set; }
        public Int64? WorkOrder_FiveBrother_Client_Id { get; set; }
        public Int64? WorkOrder_FiveBrother_Company_Id { get; set; }
        public Int64? WorkOrder_FiveBrother_UserId { get; set; }
        public Boolean? WorkOrder_FiveBrother_IsActive { get; set; }
        public Boolean? WorkOrder_FiveBrother_IsDelete { get; set; }
        public int? WorkOrder_FiveBrother_IsProcessed { get; set; }
        public Int64 UserID { get; set; }
        public  int Type { get; set; }
        public Int64? WorkOrder_Import_FkeyId { get; set; }

    }

    public class WorkOrder_FiveBrother
    {
        public String WorkOrder_FiveBrother_PkeyId { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}