﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models.FiveBrothers
{
    public class FBJsonResponseDTO
    {
    }
    public class BidcodeJsonDTO
    {
        public List<String> bidcategories { get; set; }
        public List<String> damagecauses { get; set; }
    }
    public class DamageCodesJsonDTO
    {
        public List<String> damageditems { get; set; }
        public List<String> damagecauses { get; set; }
        public List<String> buildings { get; set; }
        public List<String> rooms { get; set; }
    }
    public class InsurancecategoriesJsonDTO
    {
        public String location { get; set; }
        public String category { get; set; }
    }
    public class OrdertypecodesJsonDTO
    {
        public String code { get; set; }
        public String description { get; set; }
        public String maintype { get; set; }
    }
    public class ItemCodesJsonDTO
    {
        public String code { get; set; }
        public String description { get; set; }
        public String loantype { get; set; }
        public String unit { get; set; }
        public String price { get; set; }
        public String dimensions { get; set; }
        public String manhours { get; set; }
    }
    public class LocationCodesJsonDTO
    {
        public String code { get; set; }
        public String description { get; set; }
    }
    public class CategoryCodesJsonDTO
    {
        public String code { get; set; }
        public String description { get; set; }
        public String maincategory { get; set; }
    }
    public class OrdersJsonDTO
    {
        public Int64? ordernumber { get; set; }
        public String address { get; set; }
        public String city { get; set; }
        public String state { get; set; }
        public String zip { get; set; }
        public String name { get; set; }
        public DateTime? dateOrdered { get; set; }
        public DateTime? earliestDate { get; set; }
        public DateTime? dueDate { get; set; }
        public String photoRequirements { get; set; }
        public String orderInstructions { get; set; }
        public String photoInstructions { get; set; }
        public String dateCancelled { get; set; }
        public String formType { get; set; }
        public String orderType { get; set; }
        public String customer { get; set; }
        public String contractor { get; set; }
        public String callingcard { get; set; }
        public String loannumber { get; set; }
        public String mortco { get; set; }
        public String loanType { get; set; }
        public String department { get; set; }
        public String workValidation { get; set; }
        public String version { get; set; }
        public String lotsize { get; set; }
        public String lockbox { get; set; }
        public String latitude { get; set; }
        public String longitude { get; set; }
        public String orderTypeCode { get; set; }
        public String subcontractor { get; set; }
        public Boolean? previous { get; set; }
    }
    public class OrderItemsJsonDTO
    {
        public Int64? ordernumber { get; set; }
        public String description { get; set; }
        public Decimal? amount { get; set; }
        public Int64? quantity { get; set; }
    }

}