﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models.FiveBrothers
{
    public class Import_Client_CategoryCode_MasterDTO
    {
        public Int64 Import_Client_CategoryCodes_PkeyId { get; set; }
        public String Import_Client_CategoryCodes_Code { get; set; }
        public String Import_Client_CategoryCodes_Description { get; set; }
        public String Import_Client_CategoryCodes_MainCategory { get; set; }
        public Int64? Import_Client_CategoryCodes_Import_From_Id { get; set; }
        public Int64? Import_Client_CategoryCodes_Client_Id { get; set; }
        public Int64? Import_Client_CategoryCodes_Company_Id { get; set; }
        public Int64? Import_Client_CategoryCodes_User_Id { get; set; }
        public Boolean? Import_Client_CategoryCodes_IsActive { get; set; }
        public Boolean? Import_Client_CategoryCodes_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
    }
    public class Import_Client_CategoryCode
    {
        public String Import_Client_CategoryCodes_PkeyId { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}