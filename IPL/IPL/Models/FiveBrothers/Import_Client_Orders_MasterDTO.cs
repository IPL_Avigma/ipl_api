﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models.FiveBrothers
{
    public class Import_Client_Orders_MasterDTO
    {
        public Int64 Import_Client_Orders_PkeyId { get; set; }
        public Int64? Import_Client_Orders_Order_Number { get; set; }
        public String Import_Client_Orders_Address { get; set; }
        public String Import_Client_Orders_City { get; set; }
        public String Import_Client_Orders_State { get; set; }
        public String Import_Client_Orders_Zip { get; set; }
        public String Import_Client_Orders_Name { get; set; }
        public DateTime? Import_Client_Orders_DateOrdered { get; set; }
        public DateTime? Import_Client_Orders_EarliestDate { get; set; }
        public DateTime? Import_Client_Orders_DueDate { get; set; }
        public String Import_Client_Orders_Photo_Requirements { get; set; }
        public String Import_Client_Orders_OrderInstructions { get; set; }
        public String Import_Client_Orders_PhotoInstructions { get; set; }
        public String Import_Client_Orders_DateCancelled { get; set; }
        public String Import_Client_Orders_FormType { get; set; }
        public String Import_Client_Orders_OrderType { get; set; }
        public String Import_Client_Orders_Customer { get; set; }
        public String Import_Client_Orders_Contractor { get; set; }
        public String Import_Client_Orders_Callingcard { get; set; }
        public String Import_Client_Orders_Loannumber { get; set; }
        public String Import_Client_Orders_Mortco { get; set; }
        public String Import_Client_Orders_LoanType { get; set; }
        public String Import_Client_Orders_Department { get; set; }
        public String Import_Client_Orders_WorkValidation { get; set; }
        public String Import_Client_Orders_Version { get; set; }
        public String Import_Client_Orders_Lotsize { get; set; }
        public String Import_Client_Orders_Lockbox { get; set; }
        public String Import_Client_Orders_Latitude { get; set; }
        public String Import_Client_Orders_Longitude { get; set; }
        public String Import_Client_Orders_OrderTypeCode { get; set; }
        public String Import_Client_Orders_SubContractor { get; set; }
        public Boolean? Import_Client_Orders_Previous { get; set; }
        public Int64? Import_Client_Orders_Import_From_Id { get; set; }
        public Int64? Import_Client_Orders_Client_Id { get; set; }
        public Int64? Import_Client_Orders_Company_Id { get; set; }
        public Int64? Import_Client_Orders_UserId { get; set; }
        public Boolean? Import_Client_Orders_IsActive { get; set; }
        public Boolean? Import_Client_Orders_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
    }

    public class Import_Client_Orders
    {
        public String Import_Client_Orders_PkeyId { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}