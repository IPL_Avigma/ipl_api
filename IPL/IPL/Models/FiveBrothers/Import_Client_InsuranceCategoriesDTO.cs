﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models.FiveBrothers
{
    public class Import_Client_InsuranceCategoriesDTO
    {
        public Int64 Import_Client_InsuranceCategories_PkeyId { get; set; }
        public String Import_Client_InsuranceCategories_Location { get; set; }
        public String Import_Client_InsuranceCategories_Category { get; set; }
        public Int64? Import_Client_InsuranceCategories_Import_From_Id { get; set; }
        public Int64? Import_Client_InsuranceCategories_Client_Id { get; set; }
        public Int64? Import_Client_InsuranceCategories_Company_Id { get; set; }
        public Int64? Import_Client_InsuranceCategories_User_Id { get; set; }
        public Boolean? Import_Client_InsuranceCategories_IsActive { get; set; }
        public Boolean? Import_Client_InsuranceCategories_IsDelete { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
    }
    public class Import_Client_InsuranceCategories
    {
        public String Import_Client_InsuranceCategories_PkeyId { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}