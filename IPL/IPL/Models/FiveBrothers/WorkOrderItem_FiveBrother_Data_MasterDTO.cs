﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class WorkOrderItem_FiveBrother_Data_MasterDTO
    {
        public Int64 WorkOrder_Item_Fb_PkeyId { get; set; }
        public Int64? WorkOrder_Item_Fb_OrderNumber { get; set; }
        public String WorkOrder_Item_Fb_Description { get; set; }
        public Decimal? WorkOrder_Item_Fb_Amount { get; set; }
        public Int64? WorkOrder_Item_Fb_Quantity { get; set; }
        public Int64? WorkOrder_Item_Fb_Import_From_Id { get; set; }
        public Int64? WorkOrder_Item_Fb_Client_Id { get; set; }
        public Int64? WorkOrder_Item_Fb_Company_Id { get; set; }
        public Int64? WorkOrder_Item_Fb_User_Id { get; set; }
        public Boolean? WorkOrder_Item_Fb_IsActive { get; set; }
        public Boolean? WorkOrder_Item_Fb_IsDelete { get; set; }
        public int? WorkOrder_Item_Fb_IsProcessed { get; set; }
        public Int64? WorkOrder_Item_Fb_FiveBro_Id { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
    }
    public class WorkOrderItem_FiveBrother_Data
    {
        public String WorkOrder_Item_Fb_PkeyId { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}