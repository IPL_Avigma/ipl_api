﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models.FiveBrothers
{
    public class Ipl_Fb_AspensigninDTO
    {
        public int? ordernumber { get; set; }
        public String ordertype { get; set; }
        public String signindata { get; set; }
        public String contractor { get; set; } 
        public String version { get; set; } 
    }
}