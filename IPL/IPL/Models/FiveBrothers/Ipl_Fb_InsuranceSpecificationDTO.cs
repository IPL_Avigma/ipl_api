﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models.FiveBrothers
{
    public class Ipl_Fb_InsuranceSpecificationDTO
    {
        public int? ordernumber { get; set; }
        public String description { get; set; }
        public String notes { get; set; }
        public String location { get; set; }
        public String category { get; set; }
        public String repairAmount { get; set; }
        public String percentageCompleted { get; set; }
        public String finalAmount { get; set; }
    }
}