﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models.FiveBrothers
{
    public class Ipl_Fb_InvoiceUploadDTO
    {
        public int? ordernumber { get; set; }        
        public String itemCode { get; set; }
        public String description { get; set; }
        public String location { get; set; }
        public String quantity { get; set; }
        public String unit { get; set; }
        public String unitPrice { get; set; }
        public String amount { get; set; }
        public int attachmentId { get; set; }
        public Boolean? nodiscount { get; set; }
        public String Status { get; set; }
        public String Message { get; set; }
        public String TaskName { get; set; }
        public Int64 Inv_Child_PkeyId { get; set; }
    }
    public class Ipl_Fb_InvoiceUploadJsonDTO
    {
        public int? ordernumber { get; set; }
        public String itemCode { get; set; }
        public String description { get; set; }
        public String location { get; set; }
        public String quantity { get; set; }
        public String unit { get; set; }
        public String unitPrice { get; set; }
        public String amount { get; set; }
        public int attachmentId { get; set; }
        public Boolean? nodiscount { get; set; }
        public String Status { get; set; }
        public String Message { get; set; }
        public String TaskName { get; set; }
        public Int64 Inv_Child_PkeyId { get; set; }
    }
}