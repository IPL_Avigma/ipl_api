﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models.FiveBrothers
{
    public class FBAPI_RequestDTO
    {
        public String ApiUrl { get; set; }
        public String ConUserName { get; set; }
        public String ConPassword { get; set; }
        public String AuthUserName { get; set; }
        public String AuthPassword { get; set; }
        public Int64 UserID { get; set; }
        public Int64 WI_Pkey_ID { get; set; }
        public int Type { get; set; }
    }
}