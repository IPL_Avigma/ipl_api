﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models.FiveBrothers
{
    public class Ipl_Fb_BidUploadDTO
    {
        public String ordernumber { get; set; }
        public String itemCode { get; set; }
        public String category { get; set; }
        public String description { get; set; }
        public String location { get; set; }
        public String reason { get; set; }
        public String damagecause { get; set; }
        public String length { get; set; }
        public String width { get; set; }
        public String height { get; set; }
        public String men { get; set; }
        public String hours { get; set; }
        public String quantity { get; set; }
        public String unit { get; set; }
        public String unitPrice { get; set; }
        public String amount { get; set; }
        public int attachmentId { get; set; }
        public String Status { get; set; }
        public String Message { get; set; }
        public String TaskName { get; set; }
        public Int64 Task_Bid_PkeyId { get; set; }
    }
    public class Ipl_Fb_BidUploadJsonDTO
    {
        public String ordernumber { get; set; }
        public String itemCode { get; set; }
        public String category { get; set; }
        public String description { get; set; }
        public String location { get; set; }
        public String reason { get; set; }
        public String damagecause { get; set; }
        public String length { get; set; }
        public String width { get; set; }
        public String height { get; set; }
        public String men { get; set; }
        public String hours { get; set; }
        public String quantity { get; set; }
        public String unit { get; set; }
        public String unitPrice { get; set; }
        public String amount { get; set; }
        public int attachmentId { get; set; }
    }
    public class Client_Sync_DTO
    {
        public Int64? WO_Id { get; set; }
        public Int64? WI_ImportFrom { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
        public String message { get; set; }
    }
    public class Client_Sync_Credential
    {
        public Int64? WI_ImportFrom { get; set; }
        public Int64? Auto_IPL_Company_PkeyId { get; set; }
        public String WI_LoginName { get; set; }
        public String WI_Password { get; set; }
        public String WI_FB_LoginName { get; set; }
        public String WI_FB_Password { get; set; }
        public String ApiUrl { get; set; }
    }
}