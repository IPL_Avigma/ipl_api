﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models.FiveBrothers
{
    public class Import_Client_BidCategory_MasteDTO
    {
        public Int64 Import_Client_BidCategory_PkeyId { get; set; }
        public String Import_Client_BidCategory_Name { get; set; }
        public Int64? Import_Client_BidCategory_Import_From_Id { get; set; }
        public Int64? Import_Client_BidCategory_Client_Id { get; set; }
        public Int64? Import_Client_BidCategory_Company_Id { get; set; }
        public Boolean? Import_Client_BidCategory_IsActive { get; set; }
        public Boolean? Import_Client_BidCategory_IsDelete { get; set; }
        public Int64? Import_Client_BidCategory_User_Id { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
    }
    public class Import_Client_BidCategory_Master
    {
        public String Import_Client_BidCategory_PkeyId { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}