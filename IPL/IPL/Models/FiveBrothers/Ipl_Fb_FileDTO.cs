﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models.FiveBrothers
{
    public class Ipl_Fb_FileDTO
    {
        public int ordernumber { get; set; }
        public String contractor { get; set; }
        public String formType { get; set; }
        public String categoryCode { get; set; }
        public String itemCode { get; set; }
        public String description { get; set; }
        public String beforeAfterFlag { get; set; }
        public DateTime? fileDate { get; set; }
        public Decimal? latitude { get; set; }
        public Decimal? longitude { get; set; }
        public String fileData { get; set; }
        public String fileType { get; set; }
        public String locationCode { get; set; }
        public int? attachmentID { get; set; }
        public String Status { get; set; }
        public String Message { get; set; }
        public Int64 Client_Result_Photo_ID { get; set; }
        public String Client_Result_Photo_FolderName { get; set; }
    }
    public class Ipl_Fb_FileJSONDTO
    {
        public int ordernumber { get; set; }
        public String contractor { get; set; }
        public String formType { get; set; }
        public String categoryCode { get; set; }
        public String itemCode { get; set; }
        public String description { get; set; }
        public String beforeAfterFlag { get; set; }
        public DateTime? fileDate { get; set; }
        public Decimal? latitude { get; set; }
        public Decimal? longitude { get; set; }
        public String fileData { get; set; }
        public String fileType { get; set; }
        public String locationCode { get; set; }
        public int? attachmentID { get; set; }
    }    
}