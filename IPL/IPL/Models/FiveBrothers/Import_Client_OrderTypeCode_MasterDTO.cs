﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models.FiveBrothers
{
    public class Import_Client_OrderTypeCode_MasterDTO
    {
        public Int64 Import_Client_OrderTypeCodes_PkeyID { get; set; }
        public String Import_Client_OrderTypeCodes_Code { get; set; }
        public String Import_Client_OrderTypeCodes_Description { get; set; }
        public String Import_Client_OrderTypeCodes_MainType { get; set; }
        public String Import_Client_OrderTypeCodes_CreatedBy { get; set; }
        public String Import_Client_OrderTypeCodes_ModifiedBy { get; set; }
        public Int64? Import_Client_OrderTypeCodes_Import_From_Id { get; set; }
        public Int64? Import_Client_OrderTypeCodes_Client_Id { get; set; }
        public Int64? Import_Client_OrderTypeCodes_Company_Id { get; set; }
        public Int64? Import_Client_OrderTypeCodes_User_Id { get; set; }
        public Boolean? Import_Client_OrderTypeCodes_IsActive { get; set; }
        public Boolean? Import_Client_OrderTypeCodes_IsDelete { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
    }
    public class Import_Client_OrderTypeCode_Master
    {
        public String Import_Client_OrderTypeCodes_PkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}