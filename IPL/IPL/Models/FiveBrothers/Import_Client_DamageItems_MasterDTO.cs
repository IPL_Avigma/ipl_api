﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models.FiveBrothers
{
    public class Import_Client_DamageItems_MasterDTO
    {
        public Int64 Import_Client_DamageItem_PkeyID { get; set; }
        public String Import_Client_DamageItem_Name { get; set; }
        public Int64? Import_Client_DamageItem_Imp_From_Id { get; set; }
        public Int64? Import_Client_DamageItem_ClientId { get; set; }
        public Int64? Import_Client_DamageItem_CompanyId { get; set; }
        public Int64? Import_Client_DamageItem_UserId { get; set; }
        public Boolean? Import_Client_DamageItem_IsActive { get; set; }
        public Boolean? Import_Client_DamageItem_IsDelete { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
    }
    public class Import_Client_DamageItems_Master
    {
        public String Import_Client_DamageItem_PkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
    public class Import_Client_DamageItems_Drd
    {
        public Int64 Import_Client_DamageItem_PkeyID { get; set; }
        public String Import_Client_DamageItem_Name { get; set; }
    }
}