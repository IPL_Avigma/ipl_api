﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models.FiveBrothers
{
    public class Ipl_Fb_InspectionDTO
    {
        public int? ordernumber { get; set; }
        public DateTime? uploadedDate { get; set; }
        public DateTime? dateCompleted { get; set; }
        public String contractor { get; set; }
        public String forRent { get; set; }
        public String propertyValue { get; set; }
        public String forSale { get; set; }
        public String soldSign { get; set; }
        public String realtorPhone { get; set; }
        public String realtorName { get; set; }
        public String badAddress { get; set; }
        public String propertyGuarded { get; set; }
        public String Occupancy { get; set; }
        public String occupancyVerified { get; set; }
        public String occupancyVerifiedOther { get; set; } 
        public String occupancyUnit1 { get; set; } 
        public String occupancyUnit2 { get; set; } 
        public String occupancyUnit3 { get; set; } 
        public String occupancyUnit4 { get; set; } 
        public String exteriorCondition { get; set; } 
        public String propertySecure { get; set; } 
        
    }
}