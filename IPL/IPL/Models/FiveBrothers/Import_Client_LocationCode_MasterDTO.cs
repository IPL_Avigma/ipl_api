﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models.FiveBrothers
{
    public class Import_Client_LocationCode_MasterDTO
    {
        public Int64 Import_Client_LocationCodes_PkeyId { get; set; }
        public String Import_Client_LocationCodes_Code { get; set; }
        public String Import_Client_LocationCodes_Description { get; set; }
        public Int64? Import_Client_LocationCodes_Import_From_Id { get; set; }
        public Int64? Import_Client_LocationCodes_Client_Id { get; set; }
        public Int64? Import_Client_LocationCodes_Company_Id { get; set; }
        public Int64? Import_Client_LocationCodes_User_Id { get; set; }
        public Boolean? Import_Client_LocationCodes_IsActive { get; set; }
        public Boolean? Import_Client_LocationCodes_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
    }
    public class Import_Client_LocationCode_Master
    {
        public String Import_Client_LocationCodes_PkeyId { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}