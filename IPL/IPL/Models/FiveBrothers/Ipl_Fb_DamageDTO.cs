﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models.FiveBrothers
{
    public class Ipl_Fb_DamageJsonDTO
    {
        public Int64? ordernumber { get; set; }
        public int attachmentId { get; set; }
        public String damageditem { get; set; }
        public String cause { get; set; }
        public String description { get; set; }
        public String status { get; set; }
        public String location { get; set; }
        public String building { get; set; }
        public String room { get; set; }
        public String quantity { get; set; }
        public String estimate { get; set; }

    }

    public class Ipl_Fb_DamageDTO
    {
        public Int64? Damage_PkeyId { get; set; }
        public Int64? ordernumber { get; set; }
        public int attachmentId { get; set; }
        public String damageditem { get; set; }
        public String cause { get; set; }
        public String description { get; set; }
        public String status { get; set; }
        public String location { get; set; }
        public String building { get; set; }
        public String room { get; set; }
        public String quantity { get; set; }
        public String estimate { get; set; }
        public String Status { get; set; }
        public String Message { get; set; }

    }
}