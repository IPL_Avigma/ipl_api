﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models.FiveBrothers
{
    public class Import_Client_ItemCode_MasterDTO
    {
        public Int64 Import_Client_ItemCode_PkeyId { get; set; }
        public String Import_Client_ItemCode_Code { get; set; }
        public String Import_Client_ItemCode_Description { get; set; }
        public String Import_Client_ItemCode_LoanType { get; set; }
        public String Import_Client_ItemCode_Unit { get; set; }
        public String Import_Client_ItemCode_Price { get; set; }
        public String Import_Client_ItemCode_Dimensions { get; set; }
        public String Import_Client_ItemCode_Manhours { get; set; }
        public Int64? Import_Client_ItemCode_Import_From_Id { get; set; }
        public Int64? Import_Client_ItemCode_Client_Id { get; set; }
        public Int64? Import_Client_ItemCode_Company_Id { get; set; }
        public Int64? Import_Client_ItemCode_User_Id { get; set; }
        public Boolean? Import_Client_ItemCode_IsActive { get; set; }
        public Boolean? Import_Client_ItemCode_IsDelete { get; set; }
        public String Import_Client_ItemCode_LoanId { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
    }
    public class Import_Client_ItemCode_Master
    {
        public String Import_Client_ItemCode_PkeyId { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}