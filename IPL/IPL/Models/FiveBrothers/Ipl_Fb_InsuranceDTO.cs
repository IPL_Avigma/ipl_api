﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models.FiveBrothers
{
    public class Ipl_Fb_InsuranceDTO
    {
        public int? ordernumber { get; set; }
        public DateTime? uploadDate { get; set; }
        public String contractor { get; set; }
        public String appointmentStatus { get; set; }
        public DateTime? appointmentDate { get; set; }
        public String occupancy { get; set; }
        public String contactName { get; set; }
        public String contactHomePhone { get; set; }
        public String contactWorkPhone { get; set; }
        public String contactEmail { get; set; }
        public String materialsPresent { get; set; }
        public String permitsPosted { get; set; }
        public String specificationsProvided { get; set; }
        public String specificationsUsed { get; set; }
        public String pricesIncluded { get; set; }
        public String metMortgagor { get; set; }
        public String metContractor { get; set; }
        public String metOccupant { get; set; }
        public String walkAroundOnly { get; set; }
        public String observedGoodWorkmanship { get; set; }
        public String buildingCodesMet { get; set; }
        public String buildingCodeNotes { get; set; }
        public String completedByLicensedBuilder { get; set; }
        public String contactAgreesAssessment { get; set; }
        public String signName { get; set; }
        public String notes { get; set; }
        public String percentageCompleted { get; set; }
        public String formType { get; set; }
        public String subcontnum { get; set; }
        public String numberOfPhotos { get; set; }
        public String version { get; set; }
        public String visibleDamages { get; set; }
        public String vacantLand { get; set; }                        
        public String roofDamage { get; set; }                        
    }
}