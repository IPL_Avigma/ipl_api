﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class UserAccessLog
    {
        public Int64 User_Acc_Log_PkeyId { get; set; }
        public string User_Acc_Log_IP_Address { get; set; }
        public string User_Acc_Log_MacD { get; set; }
        public Boolean? User_Acc_Log_IsActive { get; set; }
        public Boolean? User_Acc_Log_IsDelete { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
    }
    public class UserAccessLogmaster
    {
        public String User_Acc_Log_UserName { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}