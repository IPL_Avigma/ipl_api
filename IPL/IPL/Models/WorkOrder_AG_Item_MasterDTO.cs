﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class WorkOrder_AG_Item_MasterDTO
    {
        public Int64? WAGINS_PkeyId { get; set; }
        public String WAGINS_Ins_Name { get; set; }
        public String WAGINS_Ins_Details { get; set; }
        public Int64? WAGINS_Qty { get; set; }
        public Decimal? WAGINS_Price { get; set; }
        public Decimal? WAGINS_Total { get; set; }

        public Int64? WAGINS_FkeyID { get; set; }
        public Boolean? WAGINS_IsActive { get; set; }
        public Boolean? WAGINS_IsDelete { get; set; }
        public String WAGINS_Additional_Details { get; set; }


        public Int64 UserID { get; set; }
        public int Type { get; set; }
    }
    public class WorkOrder_AG_Item_Master
    {
        public String WAGINS_PkeyId { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}