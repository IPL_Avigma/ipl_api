﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Filter_Admin_Form_Master_DTO
    {
        public Int64 Form_Filter_PkeyId { get; set; }
        public String Form_Filter_FormName { get; set; }
        public Boolean? Form_Filter_FormIsActive { get; set; }
        public Boolean? Form_Filter_FormIsRequired { get; set; }
        public Boolean? Form_Filter_FormIsFieldResult { get; set; }
        public Boolean? Form_Filter_FormIsOfficeResult { get; set; }
        public Boolean? Form_Filter_FormIsPublished { get; set; }
        public Int64? Form_Filter_FormAddedby { get; set; }
        public int? Form_Filter_FormVersion { get; set; }
        public Boolean? Form_Filter_IsActive { get; set; }
        public Boolean? Form_Filter_IsDelete { get; set; }
        public Int64? Form_Filter_CompanyID { get; set; }
        public Int64? Form_Filter_UserID { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
        public string Form_Filter_Where { get; set; }

    }
}