﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class ClientResultFohDTO
    {
        public Int64 Client_Result_Photo_ID { get; set; }
        public Int64? Client_Result_Photo_Wo_ID { get; set; }
        public Int64? CRP_New_pkeyId { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
        public string PhotoArrayJson { get; set; }
    }
}