﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models.RepairBase
{
    public class RepairBase_Profit_OverHead_DTO
    {
        public Int64 Rep_PKeyID { get; set; }
        public Int64? Rep_OrderId { get; set; }
        public int? Rep_Profit { get; set; }
        public int? Rep_Overhead { get; set; }
        public Int64? Rep_Userid { get; set; }
        public Int64? Rep_CompanyID { get; set; }
        public Boolean? Rep_IsActive { get; set; }
        public Boolean? Rep_IsDelete { get; set; }
        public int? Type { get; set; }
        public Int64? UserId { get; set; }
    }
}