﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models.RepairBase
{
    public class RepairBase_Document_Maste_DTO
    {
        public Int64 Rep_Base_PKeyID { get; set; }
        public Int64? Rep_Base_OrderId { get; set; }
        public Int64? Rep_Base_RepairAreaId { get; set; }
        public Int64? Rep_Base_RepairId { get; set; }
        public String Rep_Base_Doc_File_Path { get; set; }
        public String Rep_Base_Doc_File_Size { get; set; }
        public String Rep_Base_Doc_File_Name { get; set; }
        public String Rep_Base_Doc_BucketName { get; set; }
        public String Rep_Base_Doc_ProjectID { get; set; }
        public String Rep_Base_Doc_Object_Name { get; set; }
        public String Rep_Base_Doc_Folder_Name { get; set; }
        public String Rep_Base_Doc_UploadedBy { get; set; }
        public Int64? Rep_Base_Doc_RepUserID_ID { get; set; }
        public Int64? Rep_Base_Doc_Company_Id { get; set; }
        public int? Rep_Base_Doc_Type { get; set; }
        public Boolean? Rep_Base_IsActive { get; set; }
        public Boolean? Rep_Base_IsDelete { get; set; }
        public int? Type { get; set; }
        public Int64? UserId { get; set; }
    }
}