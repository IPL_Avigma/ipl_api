﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models.RepairBase
{
    public class RepairBase_User_Master_DTO
    {
        public Int64 Rep_UM_PKeyID { get; set; }
        public String Rep_UM_Username { get; set; }
        public String Rep_UM_Password { get; set; }
        public String Rep_UM_ApiAuthorization { get; set; }
        public String Rep_UM_Apikey { get; set; }
        public int? Rep_UM_ProductType { get; set; }
        public String Rep_UM_Token { get; set; }
        public Int64? Rep_Um_Token_Count { get; set; }
        public Boolean? Rep_UM_Token_IsValid { get; set; }
        public DateTime? Rep_UM_Token_StartDate { get; set; }
        public DateTime? Rep_UM_Token_ExpiryDate { get; set; }
        public Boolean? Rep_UM_IsActive { get; set; }
        public Boolean? Rep_UM_IsDelete { get; set; }
        public int? Type { get; set; }
        public Int64? UserId { get; set; }
        public String Rep_UM_TestApiUrl { get; set; }
        public String Rep_UM_LiveApiUrl { get; set; }
        public String Rep_UM_CreatedBy { get; set; }
        public String Rep_UM_ModifiedBy { get; set; }
    }
}