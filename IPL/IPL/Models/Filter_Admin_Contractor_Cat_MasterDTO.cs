﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Filter_Admin_Contractor_Cat_MasterDTO
    {
        public Int64 ConCat_Filter_PkeyId { get; set; }
        public String ConCat_Filter_Name { get; set; }
        public String ConCat_Filter_CreatedBy { get; set; }
        public String ConCat_Filter_ModifiedBy { get; set; }
        public Boolean? ConCat_Filter_CatIsActive { get; set; }
        public Boolean? ConCat_Filter_IsActive { get; set; }
        public Boolean? ConCat_Filter_IsDelete { get; set; }
        public Int64? ConCat_Filter_CompanyId { get; set; }
        public Int64? ConCat_Filter_UserId { get; set; }
        public Int64 UserId { get; set; }
        public int Type { get; set; }
    }
}