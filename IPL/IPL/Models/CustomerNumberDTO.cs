﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class CustomerNumberDTO
    {
        public Int64 Cust_Num_pkeyId { get; set; }
        public String Cust_Num_Number { get; set; }
        public String FilterData { get; set; }
        public String WhereClause { get; set; }
        public String Cust_Num_CreatedBy { get; set; }
        public String Cust_Num_ModifiedBy { get; set; }
        public Boolean? Cust_Num_Active { get; set; }
        public Boolean? Cust_Num_IsDelete { get; set; }
        public Boolean? Cust_Num_IsActive { get; set; }
        public Int64? UserID { get; set; }
        public int Type { get; set; }
        public Boolean? Cust_Num_IsDeleteAllow { get; set; }
    }
    public class CustomerNumberDetails
    {
        public String Cust_Num_pkeyId { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}