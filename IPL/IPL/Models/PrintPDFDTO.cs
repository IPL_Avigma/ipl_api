﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace IPL.Models
{
    public class PrintPDFDTO
    {
        public Int64 workOrder_ID { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }

        public bool IsOfficeResult { get; set; }
        public bool IsFiledResult { get; set; }
    }

    public class WorkOrdersMaster
    {
        public Int64? workOrder_ID { get; set; }
        public String workOrderNumber { get; set; }
        public String workOrderInfo { get; set; }
        public String address1 { get; set; }
        public String address2 { get; set; }
        public String city { get; set; }
        public String state { get; set; }
        public String zip { get; set; }
        public String country { get; set; }
        //public DateTime? dueDate { get; set; }
        //public DateTime? startDate { get; set; }
        public String Lotsize { get; set; }
        public String Lock_Code { get; set; }
        public String Key_Code { get; set; }
        public String Mortgagor { get; set; }
        public String dueDate { get; set; }
        public String Received_Date { get; set; }
        public String assigned_date { get; set; }
        public String Loan_Number { get; set; }
        public String startDate { get; set; }
        public String WorkType { get; set; }
        public String WT_WorkType { get; set; }
        public String Company { get; set; }
        public String Com_Phone { get; set; }
        public String Comments { get; set; }
        public String stateName { get; set; }
        public String ContractorName { get; set; }
        public String Cordinator_Name { get; set; }
        public String Processor_Name { get; set; }
        public String ContractorEmail { get; set; }
        public String CordinatorEmail { get; set; }
        public String ProcessorEmail { get; set; }
        public String Cordinator_Phone { get; set; }
        public String status { get; set; }
        public String Contractor { get; set; }
        public String Cordinator { get; set; }
        public String Processor { get; set; }
        public String User_FirstName { get; set; }
        public String IPLNO { get; set; }
        public String Client_Result_Photo_FileName { get; set; }
        public String Client_Result_Photo_FilePath { get; set; }
        public String Inst_Ch_Comand_Mobile { get; set; }
        public String Inst_Task_Name { get; set; }
        public String Inst_Comand_Mobile_details { get; set; }
        public Decimal GrandTotalAmt { get; set; }
        public Decimal GrandTotalAmt2 { get; set; }
        public int Type { get; set; }
        public string Cust_Num_Number { get; set; }
        public List<WorkOrderTaskMaster> workOrderTaskMastersList { get; set; }
        public bool IsOfficeResult { get; set; }
        public bool IsFiledResult { get; set; }

        public Int64 UserId { get; set; }

    }

    public class WorkOrderTaskMaster : WorkOrderInstructionMaster
    {
        public String Task_Name { get; set; }
        public String Task_Bid_Qty { get; set; }
        public Decimal? Task_Bid_Cont_Price { get; set; }
        public Decimal? Task_Bid_Cont_Total { get; set; }
        public Decimal? Task_Bid_Clnt_Price { get; set; }
        public Decimal? Task_Bid_Clnt_Total { get; set; }
        public String Task_Bid_Comments { get; set; }
        public String Task_Bid_Violation { get; set; }
        public String Task_Bid_Status { get; set; }
        public String Task_Bid_Hazards { get; set; }
        public String Task_Ext_Location { get; set; }


    }

    public class WorkOrderInstructionMaster
    {

        public String Instr_Qty { get; set; }
        public Decimal? Instr_Contractor_Price { get; set; }
        public Decimal? Instr_Contractor_Total { get; set; }
        public String Inst_Comand_Mobile_details { get; set; }

        //Instructon Task
        public String Task_Inv_Qty { get; set; }
        public Decimal? Task_Inv_Cont_Price { get; set; }
        public Decimal? Task_Inv_Cont_Total { get; set; }

        public Decimal? Task_Inv_Clnt_Price { get; set; }
        public Decimal? Task_Inv_Clnt_Total { get; set; }
        public String Task_Inv_Comments { get; set; }


        //Instruction 
        public int Instr_ValType { get; set; }
        public String Instr_Qty_Text { get; set; }
        public Decimal? Instr_Price_Text { get; set; }
        public Decimal? Instr_Total_Text { get; set; }


        // Damage
        public String Task_Damage_Qty { get; set; }
        public String Task_Damage_Disc { get; set; }
        public String Damage_Type { get; set; }
        public String Task_Damage_Type { get; set; }
        public String Task_Damage_Int { get; set; }
        public String Task_Damage_Location { get; set; }
        public String Task_Damage_Estimate { get; set; }

        //Violation
        public String Task_Violation_Name { get; set; }
        public Decimal? Task_Violation_Fine_Amount { get; set; }
        public DateTime Task_Violation_Date { get; set; }
        public DateTime Task_Violation_Deadline { get; set; }
        public String Task_Violation_Id { get; set; }
        public DateTime Task_Violation_Date_Discovered { get; set; }
        public String Task_Violation_Contact { get; set; }
        public String Task_Violation_Comment { get; set; }

        //Hazard
        public String Task_Hazard_Name { get; set; }
        public String Task_Hazard_Description { get; set; }
        public DateTime Task_Hazard_Date_Discovered { get; set; }
        public String Task_Hazard_Comment { get; set; }


    }
}