﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class PCR_Violation_MasterDTO
    {

        public Int64 PCR_Violation_pkeyId { get; set; }
        public Int64 PCR_Violation_MasterID { get; set; }
        public Int64 PCR_Violation_WO_ID { get; set; }
        public int? PCR_Violation_ValType { get; set; }
        public String PCR_Violation_Any_Citation { get; set; }
        public String PCR_Violation_Describe_Citation { get; set; }
        public String PCR_Violation_High_Vandalism_Area { get; set; }
        public String PCR_Violation_Describe_High_Vandalism_Reason { get; set; }
        public String PCR_Violation_Any_Unusual_Circumstances { get; set; }
        public String PCR_Violation_Attached_Proof_Path { get; set; }
        public int? PCR_Violation_Attached_Proof_Size { get; set; }
        public String PCR_Violation_Describe { get; set; }
        public String PCR_Violation_Attached_NoticesPosted_FilePath { get; set; }
        public String PCR_Violation_Attached_NoticesPosted_FileName { get; set; }
        public Boolean? PCR_Violation_IsActive { get; set; }
        public Boolean? PCR_Violation_IsDelete { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
        public Int64 fwo_pkyeId { get; set; }

    }
    public class PCR_Violation_Master
    {
        public String PCR_Violation_pkeyId { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}