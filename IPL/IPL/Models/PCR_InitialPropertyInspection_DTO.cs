﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class PCR_InitialPropertyInspection_DTO
    {
        public Int64 PCR_PkeyID { get; set; }
        public Int64 PCR_WO_ID { get; set; }
        public Int64 PCR_CompanyID { get; set; }
        public String PCR_General { get; set; }
        public Boolean? PCR_IsActive { get; set; }
        public Boolean? PCR_IsDelete { get; set; }
        public Int64 UserID { get; set; }
        public Int64 fwo_pkyeId { get; set; }
        public int Type { get; set; }
    }
}