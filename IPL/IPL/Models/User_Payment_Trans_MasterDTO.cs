﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class User_Payment_Trans_MasterDTO
    {
        public Int64 User_Pay_PkeyID { get; set; }
        public String User_Pay_Ac_Holder_Name { get; set; }
        public Int64 User_Pay_Payment_Type { get; set; }
        public String User_Pay_Card_Number { get; set; }
        public String User_Pay_Cvv_Number { get; set; }
        public Int64 User_Pay_Expiry_Month { get; set; }
        public DateTime User_Pay_Expiry_Year { get; set; }
        public String User_Pay_Key { get; set; }
        public String User_Pay_Merchant_ID { get; set; }
        public Int64? User_Pay_Status { get; set; }
        public Boolean? User_Pay_IsActive { get; set; }
        public Boolean? User_Pay_IsDelete { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
    }
    public class User_Payment_Trans
    {
        public String User_Pay_PkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}