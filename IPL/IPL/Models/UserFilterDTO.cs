﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPLApp.Models
{
    public class UserFilterDTO
    {
        public Int64 usr_filter_pkeyID { get; set; }
        public String usr_filter_WhereData { get; set; }
        public String usr_filter_FilterData { get; set; }
        public Int64? usr_filter_MenuId { get; set; }
        public Int64? usr_filter_UserID { get; set; }
        public Boolean? usr_filter_IsActive { get; set; }
        public int? Type { get; set; }
        public String Client_Filter_CreatedBy { get; set; }
        public String Client_Filter_ModifiedBy { get; set; }

    }
    public class UserFilter
    {
        public String usr_filter_pkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
}