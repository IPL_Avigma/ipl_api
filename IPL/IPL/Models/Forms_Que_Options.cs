﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Forms_Que_Options
    {
        public Int64 OptionId { get; set; }
        public string OptionName { get; set; }
        public Int64 Option_QuestionId { get; set; }
        public bool Option_IsActive { get; set; }
        public bool Option_IsDelete { get; set; }
        public Int64 UserId { get; set; }
        public Int32 Type { get; set; }
        public string AnswerValue { get; set; }
        public List<OptionsPcrFormsRules> ActionRuleList { get; set; }
        public Int64 Option_RefId { get; set; }
        public Int64 Option_FormId { get; set; }
    }
}