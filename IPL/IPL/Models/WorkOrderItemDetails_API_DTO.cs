﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPLApp.Models
{
    public class WorkOrderItemDetails_API_DTO
    {
        public Int64 WorkOrderItem_PkeyId { get; set; }
        public string Description { get; set; }
        public string Qty { get; set; }
        public string Price { get; set; }
        public string Total { get; set; }
        public string Additional_Instructions { get; set; }
        public Int64? WorkOrderMaster_API_Data_Pkey_Id { get; set; }
        public Boolean? IsActive { get; set; }
        public int Type { get; set; }
        public int UserID { get; set; }
        public string Action { get; set; }
    }
}