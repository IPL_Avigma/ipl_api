﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class CreateBucketDTO
    {
        public string BucketName { get; set; }
        public string ProjectID { get; set; }
    }
    public class DownloadImageDTO
    {
        public String BucketName { get; set; }
        public String objectName { get; set; }
        public String localPath { get; set; }
        public String IPLFolder { get; set; }
    }
    public class CreateFolderDTO
    {
        public String BucketName { get; set; }
        public String folder { get; set; }
        public String FolderName { get; set; }
    }
    public class FileUploadBucketDTO
    {
        public string BucketName { get; set; }
        public string SharedKeyFilePath { get; set; }
        public string UploadFilePath { get; set; }
        public string UploadFileType { get; set; }

        public string filePath { get; set; }
        public string IPLNO { get; set; }
        public String objectName { get; set; }
        public String localPath { get; set; }

        public String ImageZiplocalPath { get; set; }
        public Boolean? image_download { get; set; }
        public Int64 WI_Pkey_ID { get; set; }
        public String Import_BucketName { get; set; }
        public String Import_BucketFolderName { get; set; }
        public String Import_DestBucketFolderName { get; set; }
        public String rep_code { get; set; }
        public String Scrapper_rep_code { get; set; }
        public Int64 UserID { get; set; }
        public string FolderName { get; set; }
    }

    public class MovedFilesDTO
    {
        public String BucketName { get; set; }
        public String destObjectName { get; set; }
        public String sourceObjectName { get; set; }
         
    }
}