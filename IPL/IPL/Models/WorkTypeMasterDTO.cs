﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPLApp.Models
{
    public class WorkTypeMasterDTO
    {
        public Int64 WT_pkeyID { get; set; }
        public String WT_WorkType { get; set; }
        public Int64? WT_CategoryID { get; set; }
        public List<WT_CategoryMultiple> WT_CategoryMultiple { get; set; }
        public Boolean? WT_YardMaintenance { get; set; }
        public int? WT_Active { get; set; }
        public Boolean? WT_Always_recurring { get; set; }
        public String WT_Recurs_Every { get; set; }
        public Int64? WT_Recurs_WeekID { get; set; }
        public String WT_Limit_to { get; set; }
        public DateTime? WT_Cutoff_Date { get; set; }
        public Int64? WT_WO_ItemID { get; set; }
        public Int64? WT_Contractor_AssignmentID { get; set; }
        public Int64? WT_Ready_for_FieldID { get; set; }
        public Boolean? WT_IsInspection { get; set; }
        public Boolean? WT_AutoInvoice { get; set; }
        public Boolean? WT_IsActive { get; set; }
        // public Int64? WT_Ready_Quantity { get; set; 
        public string Work_Category_Name { get; set; }
        public Boolean? Work_Type_IsDelete { get; set; }
        public String FilterData { get; set; }
        public SearchMasterDTO SearchMaster { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }

        public Boolean? WT_assign_upon_comple { get; set; }
        public String Client_Work_Type_Name { get; set; }
        public String WT_CreatedBy { get; set; }
        public String WT_Type_ModifiedBy { get; set; }
        public Int64? WT_Template_Id { get; set; }
        public Boolean? Allow_Selection { get; set; }

        public string WT_CategoryJson { get; set; }
        public int CategoryCount { get; set; }
    }
    public class WorkTypeMaster
    {
        public String WT_pkeyID { get; set; }
        public String Status { get; set; }
        public String ErrorMessage { get; set; }
    }
    public class WT_CategoryMultiple
    {
        public Int64 Work_Type_Cat_pkeyID { get; set; }
        public String Work_Type_Name { get; set; }
    }

    public class WorkTypeGroupRelationDTO
    {
        public Int64 WTGroupRel_pkeyId { get; set; }
        public Int64 WTGroupRel_WorkTypeId { get; set; }
        public Int64 WTGroupRel_WorkGroupId { get; set; }
        public Int64 UserID { get; set; }
        public int Type { get; set; }
    }
}