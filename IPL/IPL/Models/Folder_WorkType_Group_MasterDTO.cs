﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPL.Models
{
    public class Folder_WorkType_Group_MasterDTO
    {
        public Int64 Fold_WorkType_Group_PkeyId { get; set; }
        public Int64? Fold_WorkType_Group_Id { get; set; }
        public Int64? Fold_WorkType_Group_Folder_Id { get; set; }
        public Int64? Fold_WorkType_Group_File_Id { get; set; }
        public Int64? Fold_WorkType_Group_Auto_Assine_Id { get; set; }
        public Boolean? Fold_WorkType_Group_IsActive { get; set; }
        public Boolean? Fold_WorkType_Group_IsDelete { get; set; }
        public Int64? UserID { get; set; }
        public int? Type { get; set; }
        public Int64? Folder_File_Master_FK_Id { get; set; }
    }
}