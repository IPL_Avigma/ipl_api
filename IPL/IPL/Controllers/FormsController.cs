﻿using AdminDemo.Repositotry.Data;
using Avigma.Repository.Lib;
using IPL.Models;
using IPL.Repository.data;
using IPLApp.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace IPL.Controllers
{
    [RoutePrefix("api/Forms")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class FormsController : BaseController
    {
        FormData formData = new FormData();
        Forms_Master_FilesData forms_Master_FilesData = new Forms_Master_FilesData();
        Question_PDFField_MasterData question_PDFField_MasterData = new Question_PDFField_MasterData();
        Filter_Admin_Form_Master_Data filter_Admin_Form_Master_Data = new Filter_Admin_Form_Master_Data();
        Log log = new Log();
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Form"></param>
        /// <returns></returns>
        ///  
        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("PostFormDetails")]
        public async Task<List<dynamic>> PostFormDetails([FromBody] Models.FormsDTO Form)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                //save pcr form
                Form.UserId = LoggedInUSerId; //set userid
                var FormDetails = await Task.Run(() => formData.Add_Form(Form));
                return FormDetails;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("PostQuestionDetails")]
        public async Task<List<dynamic>> PostQuestionDetails([FromBody] Models.Forms_Question_MasterDTO Question)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                //save question
                Question.UserId = LoggedInUSerId; //set userid
                var FormDetails = await Task.Run(() => formData.Add_Question(Question));
                return FormDetails;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("UpdateQuestion")]
        public async Task<List<dynamic>> UpdateQuestion([FromBody] Models.Forms_Question_MasterDTO Question)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                //save question
                Question.UserId = LoggedInUSerId; //set userid
                var FormDetails = await Task.Run(() => formData.Update_Question(Question));
                return FormDetails;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }


        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("PostActionRules")]
        public async Task<List<dynamic>> PostActionRules([FromBody] List<Models.Forms_ActionRules_MasterDTO> ActionRules)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                //save ActionRules

                foreach (Models.Forms_ActionRules_MasterDTO ActionRulesMaster in ActionRules)
                {
                    ActionRulesMaster.UserId = LoggedInUSerId; //set userid
                    foreach (Models.Forms_ActionRules action in ActionRulesMaster.ActionRules)
                    {
                        action.UserId = LoggedInUSerId;
                    }
                }
                var ActionRulesDetails = await Task.Run(() => formData.Add_Forms_ActionRules(ActionRules));
                return ActionRulesDetails;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("PostOptions")]
        public async Task<List<dynamic>> PostOptions([FromBody] List<Forms_Que_Options> Que_Options)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                //save ActionRules

                if (Que_Options != null && Que_Options.Count > 0)
                {
                    Que_Options.ForEach(q => q.UserId = LoggedInUSerId);
                }
                var OptionsDetails = await Task.Run(() => formData.Add_Options(Que_Options));
                return OptionsDetails;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }



        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("PostAlertRules")]
        public async Task<List<dynamic>> PostAlertRules([FromBody] List<Models.Forms_AlertRulesDTO> AlertRules)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                //save ActionRules

                if (AlertRules != null && AlertRules.Count > 0)
                {
                    AlertRules.ForEach(q => q.UserId = LoggedInUSerId);
                }
                var AlertRulesDetails = await Task.Run(() => formData.Add_Forms_AlertRules(AlertRules));
                return AlertRulesDetails;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }


        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("PostFieldRules")]
        public async Task<List<dynamic>> PostFieldRules([FromBody] List<Models.Forms_FieldRulesDTO> FieldRules)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                //save FieldRules
                if (FieldRules != null && FieldRules.Count > 0)
                {
                    FieldRules.ForEach(q => q.UserId = LoggedInUSerId);
                }

                var FieldRulesDetails = await Task.Run(() => formData.Add_Forms_FieldRules(FieldRules));
                return FieldRulesDetails;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("PostPhotoRules")]
        public async Task<List<dynamic>> PostPhotoRules([FromBody] List<Models.Forms_PhotoRulesDTO> PhotoRules)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                //save PhotoRules
                if (PhotoRules != null && PhotoRules.Count > 0)
                {
                    PhotoRules.ForEach(q => q.UserId = LoggedInUSerId);
                }
                var PhotoRulesDetails = await Task.Run(() => formData.Add_Forms_PhotoRules(PhotoRules));
                return PhotoRulesDetails;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }
        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("PostQuestionAnswer")]
        public async Task<List<dynamic>> AddUpdateQuestionAnswer([FromBody] List<Models.Forms_Question_AnswersDTO> QuestionAnswers)
        {
            List<dynamic> objdynamic = new List<dynamic>();
            try
            {
                if (QuestionAnswers != null && QuestionAnswers.Count > 0)
                {
                    QuestionAnswers.ForEach(q => q.UserId = LoggedInUSerId);
                }

                var QuestionAnswerDetails = await Task.Run(() => formData.AddUpdateQuestionAnswer(QuestionAnswers));
                return QuestionAnswerDetails;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamic.Add(ex.Message);
                return objdynamic;
            }
        }

        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("GetFormMaster")]
        public async Task<List<dynamic>> GetFormMaster([FromBody] Models.FormsDTO Form)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                Form.UserId = LoggedInUSerId; //set userid
                var FormDetails = await Task.Run(() => formData.Get_Form_Master(Form));
                return FormDetails;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("GetFormMasterFilter")]
        public async Task<List<dynamic>> GetFormMasterFilter([FromBody] Models.FormsDTO Form)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                Form.UserId = LoggedInUSerId; //set userid
                var FormDetails = await Task.Run(() => formData.Get_Filter_FormData(Form));
                return FormDetails;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }


        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("GetFormQuestion")]
        public async Task<List<dynamic>> GetFormQuestion([FromBody] Models.Forms_Question_MasterDTO Question)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                Question.UserId = LoggedInUSerId; //set userid
                var QuestionDetails = await Task.Run(() => formData.Get_Form_Question(Question));
                return QuestionDetails;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("GetPreviewData")]
        public async Task<List<dynamic>> GetPreviewData([FromBody] Models.FormsDTO model)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                model.UserId = LoggedInUSerId; //set userid
                var FormPreviewData = await Task.Run(() => formData.Get_Form_PreviewPage(model));
                return FormPreviewData;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }


        /// <summary>
        /// Type = 3
        /// </summary>
        /// <param name="Form"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("DeleteForm")]
        public async Task<List<dynamic>> DeleteForm([FromBody] Models.FormsDTO Form)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                Form.UserId = LoggedInUSerId; //set userid
                var FormDetails = await Task.Run(() => formData.DeleteForm(Form));
                return FormDetails;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("DeleteEditQuestionItems")]
        public async Task<List<dynamic>> DeleteEditQuestionItems([FromBody] Models.Forms_Question_MasterDTO Question)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                Question.UserId = LoggedInUSerId; //set userid
                var DeleteEditQuestionItemDetails = await Task.Run(() => formData.DeleteEditQuestionItems(Question));
                return DeleteEditQuestionItemDetails;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("CopyForm")]
        public async Task<List<dynamic>> CopyForm([FromBody] Models.FormsDTO Form)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                Form.UserId = LoggedInUSerId; //set userid
                var FormDetails = await Task.Run(() => formData.CopyForm(Form));
                return FormDetails;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

        /// <summary>
        /// Type = 3
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("DeleteFilter")]
        public async Task<List<dynamic>> DeleteFilter([FromBody] Models.WO_Filters filter)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                filter.UserId = LoggedInUSerId; //set userid
                var FormDetails = await Task.Run(() => formData.DeleteFilter(filter));
                return FormDetails;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }


        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("UpdateFormStatus")]
        public async Task<List<dynamic>> UpdateFormStatus([FromBody] Models.FormsDTO form)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                form.UserId = LoggedInUSerId; //set userid
                var FormDetails = await Task.Run(() => formData.UpdateFormStatus(form));
                return FormDetails;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }




        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("UpdateQuestionStatus")]
        public async Task<List<dynamic>> UpdateQuestionStatus([FromBody] Models.Forms_Question_MasterDTO question)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                question.UserId = LoggedInUSerId; //set userid
                var FormDetails = await Task.Run(() => formData.UpdateQuestionStatus(question));
                return FormDetails;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }
        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("GetFormDocument")]
        public async Task<List<dynamic>> GetFormDocument([FromBody] Forms_Master_FilesDTO fileDoc)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                fileDoc.UserId = LoggedInUSerId; //set userid
                var documentDetails = await Task.Run(() => forms_Master_FilesData.Get_Forms_Master_FilesDetails(fileDoc));
                return documentDetails;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }
        [Authorize]
        [HttpPost]
        [Route("UpdateFormDocument")]
        public async Task<List<dynamic>> UpdateFileMasterData([FromBody] Forms_Master_FilesDTO fileDoc)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            Folder_File_MasterData folder_File_MasterData = new Folder_File_MasterData();

            try
            {
                fileDoc.UserId = LoggedInUSerId;
                var UpdateFile = await Task.Run(() => forms_Master_FilesData.AddCreateUpdate_Forms_Master_Files(fileDoc));
                return UpdateFile;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("ImportPCRForm")]
        public async Task<List<dynamic>> ImportPCRFormData([FromBody] ImportPCRFormDTO importPCRFormDTO)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            Folder_File_MasterData folder_File_MasterData = new Folder_File_MasterData();

            try
            {
                importPCRFormDTO.UserID = LoggedInUSerId;
                var importForm = await Task.Run(() => forms_Master_FilesData.ImportPCRFormDetail(importPCRFormDTO));
                return importForm;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("GetWoFormMaster")]
        public async Task<List<dynamic>> GetWoFormMaster([FromBody] Models.FormsDTO Form)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                Form.UserId = LoggedInUSerId; //set userid
                var FormDetails = await Task.Run(() => formData.Get_Wo_Form_Master(Form));
                return FormDetails;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

        [Authorize]
        [HttpPost]
        [Route("DeleteWoFormMaster")]
        public async Task<List<dynamic>> DeleteWoFormMaster([FromBody] FormsWoDTO model)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                model.UserId = LoggedInUSerId;
                var UpdateFile = await Task.Run(() => formData.Add_Form_Wo_Relation(model));
                return UpdateFile;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }


        }

        [Authorize]
        [HttpPost]
        [Route("AddUpdateQuePDFField")]
        public async Task<List<dynamic>> AddUpdateQuePDFFieldData([FromBody] Forms_Master_FilesDTO fileDoc)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                fileDoc.UserId = LoggedInUSerId;
                var UpdateFile = await Task.Run(() => question_PDFField_MasterData.AddUpdateQuePDFField(fileDoc));
                return UpdateFile;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("GetFBFormList")]
        public async Task<List<dynamic>> GetFBFormList([FromBody] Fb_Dynamic_MasterDTO model)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                model.UserId = LoggedInUSerId; //set userid
                var documentDetails = await Task.Run(() => formData.Get_FB_FormsDetails(model));
                return documentDetails;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("UpdateFBFormStatus")]
        public async Task<List<dynamic>> UpdateFBFormStatus([FromBody] Fb_Dynamic_MasterDTO model)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                model.UserId = LoggedInUSerId; //set userid
                var FormDetails = await Task.Run(() => formData.UpdateFBFormStatus(model));
                return FormDetails;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

        [Authorize]
        [HttpPost]
        [Route("AddFormWoRelation")]
        public async Task<List<dynamic>> AddFormWoRelation([FromBody] FormsWoDTO model)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                model.UserId = LoggedInUSerId;
                var UpdateFile = await Task.Run(() => formData.Add_Form_Wo_Relation(model));
                return UpdateFile;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        // post workType configuration List
        [Authorize]
        [HttpPost]
        [Route("PostFbDynamicDetail")]
        public async Task<List<dynamic>> PostWorkTypeConfigurationDetails()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Fb_Dynamic_MasterListDTO>(s);
                Data.Fb_Dynamic_List.ForEach(d => d.UserId = Convert.ToInt32(LoggedInUSerId));

                objdynamicobj = await Task.Run(() => formData.PostFb_Dynamic_Master(Data));
                return objdynamicobj;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }



        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("PostFilterAdminFormMaster")]
        public async Task<List<dynamic>> PostFilterAdminFormMaster()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Filter_Admin_Form_Master_DTO>(s);
                Data.UserID = LoggedInUSerId;


                var CreateUpdate_Filter_Admin_Form_Master_DataDetails = await Task.Run(() => filter_Admin_Form_Master_Data.CreateUpdate_Filter_Admin_Form_Master_DataDetails(Data));

                return CreateUpdate_Filter_Admin_Form_Master_DataDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage("CreateUpdateFilterAdminFormMaster");
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }
        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("GetFilterAdminFormMaster")]
        public async Task<List<dynamic>> GetFilterAdminFormMaster()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Filter_Admin_Form_Master_DTO>(s);
                Data.UserID = LoggedInUSerId;

                var Get_Filter_Admin_Form_MasterDetails = await Task.Run(() => filter_Admin_Form_Master_Data.Get_Filter_Admin_Form_MasterDetails(Data));

                return Get_Filter_Admin_Form_MasterDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage("GetFilterAdminFormMaster");
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

        [Authorize]
        [HttpPost]
        [Route("GeneratePdfDocumentForForm")]
        public async Task<List<dynamic>> GeneratePdfDocumentForForm()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<FormsWoDTO>(s);
                Data.UserId = LoggedInUSerId;

                var Get_Filter_Admin_Form_MasterDetails = await Task.Run(() => formData.GeneratePdfDocumentForForm(Data));

                return Get_Filter_Admin_Form_MasterDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage("GetFilterAdminFormMaster");
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

    }
}
