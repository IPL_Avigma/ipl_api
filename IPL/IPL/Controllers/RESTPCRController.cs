﻿using IPL.Models;
using IPL.Repository.data;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using Avigma.Repository.Lib;

namespace IPL.Controllers
{
    [RoutePrefix("api/RESTPCR")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class RESTPCRController : BaseController
    {
        PropertyInfoMasterData propertyInfoMasterData = new PropertyInfoMasterData();


        PCR_FiveBrotherData pCR_FiveBrotherData = new PCR_FiveBrotherData();
        PCR_Violation_MasterData pCR_Violation_MasterData = new PCR_Violation_MasterData();
        PCR_Securing_MasterData pCR_Securing_MasterData = new PCR_Securing_MasterData();
        PCR_Appliance_Data pCR_Appliance_Data = new PCR_Appliance_Data();
        PCR_WinterizationData pCR_WinterizationData = new PCR_WinterizationData();
        PCR_Yard_MaintenanceData pCR_Yard_MaintenanceData = new PCR_Yard_MaintenanceData();
        PCR_Pool_MasterData pCR_Pool_MasterData = new PCR_Pool_MasterData();
        PCR_Debris_MasterData pCR_Debris_MasterData = new PCR_Debris_MasterData();
        PCR_Utilities_MasterData pCR_Utilities_MasterData = new PCR_Utilities_MasterData();
        PCR_Conveyance_MasterData pCR_Conveyance_MasterData = new PCR_Conveyance_MasterData();
        PCRDamageMasterData pCRDamageMasterData = new PCRDamageMasterData();
        PCR_Roof_MasterData pCR_Roof_MasterData = new PCR_Roof_MasterData();
        PCR_CYPREXX_FORMS_MASTERData pCR_CYPREXX_FORMS_MASTER = new PCR_CYPREXX_FORMS_MASTERData();
        PCR_Grass_Cut_MasterData pCR_Grass_Cut_MasterData = new PCR_Grass_Cut_MasterData();
        MSI_GrassPcr_Forms_MasterData mSI_GrassPcr_Forms_MasterData = new MSI_GrassPcr_Forms_MasterData();
        PCR_Cyprexx_Grass_Checklist_Data pCR_Cyprexx_Grass_Checklist_Data = new PCR_Cyprexx_Grass_Checklist_Data();
        PCR_CyprexxWinterizationPressureData pCR_CyprexxWinterizationPressureData = new PCR_CyprexxWinterizationPressureData();
        PCR_CyprexxUniversalDamageChecklist_Data cyprexxUniversalDamageChecklist_Data = new PCR_CyprexxUniversalDamageChecklist_Data();
        MSI_PCR_PreservationFormMaster_Data mSI_PCR_PreservationFormMaster_Data = new MSI_PCR_PreservationFormMaster_Data();
        Service_Link_Form_Master_Data service_Link_Form_Master_Data = new Service_Link_Form_Master_Data();
        PCR_MCS_Forms_Master_Data pCR_MCS_Forms_Master_Data = new PCR_MCS_Forms_Master_Data();
        PCR_Five_Brothers_Inspection_Form_Data PCR_Five_Brothers_Inspection_Form = new PCR_Five_Brothers_Inspection_Form_Data();
        PCR_MCS_Grass_Cut_Form_Master_Data pCR_MCS_Grass_Cut_Form_Master_Data = new PCR_MCS_Grass_Cut_Form_Master_Data();
        PCR_MCS_Maintenance_Vendor_Checklist_Data pCR_MCS_Maintenance_Vendor_Checklist_Data = new PCR_MCS_Maintenance_Vendor_Checklist_Data();
        PropertyConditionReport_Data propertyCondition = new PropertyConditionReport_Data();
        PCR_NFR_Dump_Receipt_Form_Data _NFR_Dump_Receipt_Form_Data = new PCR_NFR_Dump_Receipt_Form_Data();
        CyprexxJobDocumentationChecklist_Data _CyprexxJobDocumentationChecklist_Data = new CyprexxJobDocumentationChecklist_Data();
        PCR_InitialPropertyInspection_Data pCR_InitialPropertyInspection_Data = new PCR_InitialPropertyInspection_Data();
        Log log = new Log();
        //For Property Information Post method
        [Authorize]
        [HttpPost]
        [Route("PostPropertyInfoData")]
        public async Task<List<dynamic>> PostPropertyInfoData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();


                var Data = JsonConvert.DeserializeObject<PropertyInfoMasterDTO>(s);

                PropertyInfoMasterDTO propertyInfoMasterDTO = new PropertyInfoMasterDTO();

                propertyInfoMasterDTO.PCR_Pro_PkeyID = Data.PCR_Pro_PkeyID;
                propertyInfoMasterDTO.PCR_Prop_WO_ID = Data.PCR_Prop_WO_ID;
                propertyInfoMasterDTO.PCR_Prop_MasterID = Data.PCR_Prop_MasterID;
                propertyInfoMasterDTO.PCR_Prop_ValType = Data.PCR_Prop_ValType;
                propertyInfoMasterDTO.PCR_Prop_ForSale = Data.PCR_Prop_ForSale;
                propertyInfoMasterDTO.PCR_Prop_Sold = Data.PCR_Prop_Sold;
                propertyInfoMasterDTO.PCR_Prop_Broker_Phone = Data.PCR_Prop_Broker_Phone;
                propertyInfoMasterDTO.PCR_Prop_Broker_Name = Data.PCR_Prop_Broker_Name;
                propertyInfoMasterDTO.PCR_Prop_Maintained = Data.PCR_Prop_Maintained;
                propertyInfoMasterDTO.PCR_Prop_Maintained_ByOther = Data.PCR_Prop_Maintained_ByOther;
                propertyInfoMasterDTO.PCR_Prop_Maintained_Items_Utilities = Data.PCR_Prop_Maintained_Items_Utilities;
                propertyInfoMasterDTO.PCR_Prop_Maintained_Items_Grass = Data.PCR_Prop_Maintained_Items_Grass;
                propertyInfoMasterDTO.PCR_Prop_Maintained_Items_Snow_Removal = Data.PCR_Prop_Maintained_Items_Snow_Removal;
                propertyInfoMasterDTO.PCR_Prop_Maintained_Items_Interior_Repaiars = Data.PCR_Prop_Maintained_Items_Interior_Repaiars;
                propertyInfoMasterDTO.PCR_Prop_Maintained_Items_Exterior_Repairs = Data.PCR_Prop_Maintained_Items_Exterior_Repairs;
                propertyInfoMasterDTO.PCR_Prop_Active_Listing = Data.PCR_Prop_Active_Listing;
                propertyInfoMasterDTO.PCR_Prop_Basement_Present = Data.PCR_Prop_Basement_Present;
                propertyInfoMasterDTO.PCR_OurBuildings_Garages = Data.PCR_OurBuildings_Garages;
                propertyInfoMasterDTO.PCR_OurBuildings_Sheds = Data.PCR_OurBuildings_Sheds;
                propertyInfoMasterDTO.PCR_OurBuildings_Caports = Data.PCR_OurBuildings_Caports;
                propertyInfoMasterDTO.PCR_OurBuildings_Bams = Data.PCR_OurBuildings_Bams;
                propertyInfoMasterDTO.PCR_OurBuildings_Pool_House = Data.PCR_OurBuildings_Pool_House;
                propertyInfoMasterDTO.PCR_OurBuildings_Other_Building = Data.PCR_OurBuildings_Other_Building;
                propertyInfoMasterDTO.PCR_Prop_Property_Type_Vacant_Land = Data.PCR_Prop_Property_Type_Vacant_Land;
                propertyInfoMasterDTO.PCR_Prop_Property_Type_Single_Family = Data.PCR_Prop_Property_Type_Single_Family;
                propertyInfoMasterDTO.PCR_Prop_Property_Type_Multi_Family = Data.PCR_Prop_Property_Type_Multi_Family;
                propertyInfoMasterDTO.PCR_Prop_Property_Type_Mobile_Home = Data.PCR_Prop_Property_Type_Mobile_Home;
                propertyInfoMasterDTO.PCR_Prop_Property_Type_Condo = Data.PCR_Prop_Property_Type_Condo;
                propertyInfoMasterDTO.PCR_Prop_Permit_Required = Data.PCR_Prop_Permit_Required;
                propertyInfoMasterDTO.PCR_Prop_Condo_Association_Property = Data.PCR_Prop_Condo_Association_Property;
                propertyInfoMasterDTO.PCR_HOA_Name = Data.PCR_HOA_Name;
                propertyInfoMasterDTO.PCR_HOA_Phone = Data.PCR_HOA_Phone;
                propertyInfoMasterDTO.PCR_Prop_No_Of_Unit = Data.PCR_Prop_No_Of_Unit;
                propertyInfoMasterDTO.PCR_Prop_Common_Entry = Data.PCR_Prop_Common_Entry;
                propertyInfoMasterDTO.PCR_Prop_Garage = Data.PCR_Prop_Garage;
                propertyInfoMasterDTO.PCR_Prop_Unit1 = Data.PCR_Prop_Unit1;
                propertyInfoMasterDTO.PCR_Prop_Unit1_Occupied = Data.PCR_Prop_Unit1_Occupied;
                propertyInfoMasterDTO.PCR_Prop_Unit2 = Data.PCR_Prop_Unit2;
                propertyInfoMasterDTO.PCR_Prop_Unit2_Occupied = Data.PCR_Prop_Unit2_Occupied;
                propertyInfoMasterDTO.PCR_Prop_Unit3 = Data.PCR_Prop_Unit3;
                propertyInfoMasterDTO.PCR_Prop_Unit3_Occupied = Data.PCR_Prop_Unit3_Occupied;
                propertyInfoMasterDTO.PCR_Prop_Unit4 = Data.PCR_Prop_Unit4;
                propertyInfoMasterDTO.PCR_Prop_Unit4_Occupied = Data.PCR_Prop_Unit4_Occupied;
                propertyInfoMasterDTO.PCR_Prop_Property_Vacant = Data.PCR_Prop_Property_Vacant;
                propertyInfoMasterDTO.PCR_Prop_Property_Vacant_Notes = Data.PCR_Prop_Property_Vacant_Notes;
                propertyInfoMasterDTO.PCR_Prop_Occupancy_Verified_Personal_Visible = Data.PCR_Prop_Occupancy_Verified_Personal_Visible;
                propertyInfoMasterDTO.PCR_Prop_Occupancy_Verified_Neighbor = Data.PCR_Prop_Occupancy_Verified_Neighbor;
                propertyInfoMasterDTO.PCR_Prop_Occupancy_Verified_Utilities_On = Data.PCR_Prop_Occupancy_Verified_Utilities_On;
                propertyInfoMasterDTO.PCR_Prop_Occupancy_Verified_Visual = Data.PCR_Prop_Occupancy_Verified_Visual;
                propertyInfoMasterDTO.PCR_Prop_Occupancy_Verified_Direct_Con_Tenant = Data.PCR_Prop_Occupancy_Verified_Direct_Con_Tenant;
                propertyInfoMasterDTO.PCR_Prop_Occupancy_Verified_Direct_Con_Mortgagor = Data.PCR_Prop_Occupancy_Verified_Direct_Con_Mortgagor;
                propertyInfoMasterDTO.PCR_Prop_Occupancy_Verified_Direct_Con_Unknown = Data.PCR_Prop_Occupancy_Verified_Direct_Con_Unknown;
                propertyInfoMasterDTO.PCR_Prop_Owner_Maintaining_Property = Data.PCR_Prop_Owner_Maintaining_Property;
                propertyInfoMasterDTO.PCR_Prop_Other = Data.PCR_Prop_Other;
                propertyInfoMasterDTO.PRC_Prop_IsActive = Data.PRC_Prop_IsActive;
                propertyInfoMasterDTO.PRC_Prop_IsDelete = Data.PRC_Prop_IsDelete;
                propertyInfoMasterDTO.UserID = Data.UserID;
                propertyInfoMasterDTO.Type = Data.Type;
                propertyInfoMasterDTO.PCR_Prop_Property = Data.PCR_Prop_Property;
                propertyInfoMasterDTO.PCR_Prop_Permit_Number = Data.PCR_Prop_Permit_Number;
                propertyInfoMasterDTO.UserID = LoggedInUSerId;

                propertyInfoMasterDTO.PRC_Prop_dateCompleted = Data.PRC_Prop_dateCompleted;
                propertyInfoMasterDTO.PRC_Prop_badAddress = Data.PRC_Prop_badAddress;
                propertyInfoMasterDTO.PRC_Prop_orderCompleted = Data.PRC_Prop_orderCompleted;
                propertyInfoMasterDTO.PRC_Prop_PropertyVacantBadAddressProvide_dtls = Data.PRC_Prop_PropertyVacantBadAddressProvide_dtls;
                propertyInfoMasterDTO.fwo_pkyeId = Data.fwo_pkyeId;


                var returnVal = await Task.Run(() => propertyInfoMasterData.AddPCR_Propert_Info_Data(propertyInfoMasterDTO));
                return returnVal;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //For Property Information Get method
        [Authorize]
        [HttpPost]
        [Route("GetPropertyInfoData")]
        public async Task<List<dynamic>> GetPropertyInfoData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();


                var Data = JsonConvert.DeserializeObject<PropertyInfoMasterDTO>(s);

                PropertyInfoMasterDTO propertyInfoMasterDTO = new PropertyInfoMasterDTO();

                propertyInfoMasterDTO.PCR_Pro_PkeyID = Data.PCR_Pro_PkeyID;
                propertyInfoMasterDTO.PCR_Prop_WO_ID = Data.PCR_Prop_WO_ID;
                propertyInfoMasterDTO.Type = Data.Type;

                var GetProperty = await Task.Run(() => propertyInfoMasterData.GetProperty_Info_Master_Details(propertyInfoMasterDTO));
                return GetProperty;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        //For PCR Five Brothers Post method // JSon Only Save
        [Authorize]
        [HttpPost]
        [Route("PostPCRFiveBrotherData")]
        public async Task<List<dynamic>> PostPCRFiveBrotherData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();


                var Data = JsonConvert.DeserializeObject<PCR_FiveBrotherDTO>(s);
                Data.UserID = LoggedInUSerId;
                PCR_FiveBrotherDTO pCR_FiveBrotherDTO = new PCR_FiveBrotherDTO();

                pCR_FiveBrotherDTO.PCR_FiveBro_id = Data.PCR_FiveBro_id;
                pCR_FiveBrotherDTO.PCR_FiveBro_Json = Data.PCR_FiveBro_Json;
                pCR_FiveBrotherDTO.PCR_FiveBro_Valtype = Data.PCR_FiveBro_Valtype;
                pCR_FiveBrotherDTO.PCR_FiveBro_WO_ID = Data.PCR_FiveBro_WO_ID;
                //pCR_FiveBrotherDTO.PCR_FiveBro_Propertyinfo = Data.PCR_FiveBro_Propertyinfo;
                //pCR_FiveBrotherDTO.PCR_FiveBro_Violations = Data.PCR_FiveBro_Violations;
                //pCR_FiveBrotherDTO.PCR_FiveBro_Securing = Data.PCR_FiveBro_Securing;
                //pCR_FiveBrotherDTO.PCR_FiveBro_Winterization = Data.PCR_FiveBro_Winterization;
                //pCR_FiveBrotherDTO.PCR_FiveBro_Yard = Data.PCR_FiveBro_Yard;
                //pCR_FiveBrotherDTO.PCR_FiveBro_Debris_Hazards = Data.PCR_FiveBro_Debris_Hazards;
                //pCR_FiveBrotherDTO.PCR_FiveBro_Roof = Data.PCR_FiveBro_Roof;
                //pCR_FiveBrotherDTO.PCR_FiveBro_Pool = Data.PCR_FiveBro_Pool;
                //pCR_FiveBrotherDTO.PCR_FiveBro_Utilities = Data.PCR_FiveBro_Utilities;
                //pCR_FiveBrotherDTO.PCR_FiveBro_Appliances = Data.PCR_FiveBro_Appliances;
                //pCR_FiveBrotherDTO.PCR_FiveBro_Damages = Data.PCR_FiveBro_Damages;
                //pCR_FiveBrotherDTO.PCR_FiveBro_Conveyance = Data.PCR_FiveBro_Conveyance;
                //pCR_FiveBrotherDTO.PCR_FiveBro_Integration_Type = Data.PCR_FiveBro_Integration_Type;
                //pCR_FiveBrotherDTO.PCR_FiveBro_IsIntegration = Data.PCR_FiveBro_IsIntegration;
                pCR_FiveBrotherDTO.PCR_FiveBro_IsActive = Data.PCR_FiveBro_IsActive;
                //pCR_FiveBrotherDTO.PCR_FiveBro_IsDelete = Data.PCR_FiveBro_IsDelete;
                pCR_FiveBrotherDTO.UserID = Data.UserID;
                pCR_FiveBrotherDTO.Type = Data.Type;



                var AddFiveBrother = await Task.Run(() => pCR_FiveBrotherData.AddPCR_FiveBrother_Data(pCR_FiveBrotherDTO));
                return AddFiveBrother;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        //For PCR Five Brothers Get method
        [Authorize]
        [HttpPost]
        [Route("GetPCRFiveBrotherData")]
        public async Task<List<dynamic>> GetPCRFiveBrotherData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();


                var Data = JsonConvert.DeserializeObject<PCR_FiveBrotherDTO>(s);

                PCR_FiveBrotherDTO pCR_FiveBrotherDTO = new PCR_FiveBrotherDTO();

                pCR_FiveBrotherDTO.PCR_FiveBro_id = Data.PCR_FiveBro_id;
                pCR_FiveBrotherDTO.PCR_FiveBro_WO_ID = Data.PCR_FiveBro_WO_ID;
                pCR_FiveBrotherDTO.Type = Data.Type;



                var GetFiveBrother = await Task.Run(() => pCR_FiveBrotherData.GetPCRfivebrotherDetails(pCR_FiveBrotherDTO));
                return GetFiveBrother;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }




        //For PCR Violation post method
        [Authorize]
        [HttpPost]
        [Route("PostPCRViolationData")]
        public async Task<List<dynamic>> PostPCRViolationData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();


                var Data = JsonConvert.DeserializeObject<PCR_Violation_MasterDTO>(s);
                Data.UserID = LoggedInUSerId;

                PCR_Violation_MasterDTO pCR_Violation_MasterDTO = new PCR_Violation_MasterDTO();


                pCR_Violation_MasterDTO.PCR_Violation_pkeyId = Data.PCR_Violation_pkeyId;
                pCR_Violation_MasterDTO.PCR_Violation_MasterID = Data.PCR_Violation_MasterID;
                pCR_Violation_MasterDTO.PCR_Violation_WO_ID = Data.PCR_Violation_WO_ID;
                pCR_Violation_MasterDTO.PCR_Violation_ValType = Data.PCR_Violation_ValType;
                pCR_Violation_MasterDTO.PCR_Violation_Any_Citation = Data.PCR_Violation_Any_Citation;
                pCR_Violation_MasterDTO.PCR_Violation_Describe_Citation = Data.PCR_Violation_Describe_Citation;
                pCR_Violation_MasterDTO.PCR_Violation_High_Vandalism_Area = Data.PCR_Violation_High_Vandalism_Area;
                pCR_Violation_MasterDTO.PCR_Violation_Describe_High_Vandalism_Reason = Data.PCR_Violation_Describe_High_Vandalism_Reason;
                pCR_Violation_MasterDTO.PCR_Violation_Any_Unusual_Circumstances = Data.PCR_Violation_Any_Unusual_Circumstances;
                pCR_Violation_MasterDTO.PCR_Violation_Attached_Proof_Path = Data.PCR_Violation_Attached_Proof_Path;
                pCR_Violation_MasterDTO.PCR_Violation_Attached_Proof_Size = Data.PCR_Violation_Attached_Proof_Size;
                pCR_Violation_MasterDTO.PCR_Violation_Describe = Data.PCR_Violation_Describe;
                pCR_Violation_MasterDTO.PCR_Violation_Attached_NoticesPosted_FilePath = Data.PCR_Violation_Attached_NoticesPosted_FilePath;
                pCR_Violation_MasterDTO.PCR_Violation_Attached_NoticesPosted_FileName = Data.PCR_Violation_Attached_NoticesPosted_FileName;
                pCR_Violation_MasterDTO.PCR_Violation_IsActive = Data.PCR_Violation_IsActive;
                pCR_Violation_MasterDTO.PCR_Violation_IsDelete = Data.PCR_Violation_IsDelete;
                pCR_Violation_MasterDTO.UserID = Data.UserID;
                pCR_Violation_MasterDTO.Type = Data.Type;
                pCR_Violation_MasterDTO.fwo_pkyeId = Data.fwo_pkyeId;



                var AddViolation = await Task.Run(() => pCR_Violation_MasterData.AddPCR_Violation_Data(pCR_Violation_MasterDTO));
                return AddViolation;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //For PCR Violation Get method
        [Authorize]
        [HttpPost]
        [Route("GetPCRViolationData")]
        public async Task<List<dynamic>> GetPCRViolationData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();


                var Data = JsonConvert.DeserializeObject<PCR_Violation_MasterDTO>(s);

                PCR_Violation_MasterDTO pCR_Violation_MasterDTO = new PCR_Violation_MasterDTO();


                pCR_Violation_MasterDTO.PCR_Violation_pkeyId = Data.PCR_Violation_pkeyId;
                pCR_Violation_MasterDTO.PCR_Violation_WO_ID = Data.PCR_Violation_WO_ID;
                pCR_Violation_MasterDTO.Type = Data.Type;



                var GetViolation = await Task.Run(() => pCR_Violation_MasterData.GetPCRPCRViolationDetails(pCR_Violation_MasterDTO));
                return GetViolation;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        //For PCR Securing Post method
        [Authorize]
        [HttpPost]
        [Route("PostPCRSecuringData")]
        public async Task<List<dynamic>> PostPCRSecuringData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();


                var Data = JsonConvert.DeserializeObject<PCR_Securing_MasterDTO>(s);
                Data.UserID = LoggedInUSerId;

                PCR_Securing_MasterDTO pCR_Securing_MasterDTO = new PCR_Securing_MasterDTO();

                pCR_Securing_MasterDTO.PCR_Securing_pkeyId = Data.PCR_Securing_pkeyId;
                pCR_Securing_MasterDTO.PCR_Securing_MasterId = Data.PCR_Securing_MasterId;
                pCR_Securing_MasterDTO.PCR_Securing_WO_Id = Data.PCR_Securing_WO_Id;
                pCR_Securing_MasterDTO.PCR_Securing_ValType = Data.PCR_Securing_ValType;
                pCR_Securing_MasterDTO.PCR_Securing_On_Arrival = Data.PCR_Securing_On_Arrival;
                pCR_Securing_MasterDTO.PCR_Securing_On_Departure = Data.PCR_Securing_On_Departure;
                pCR_Securing_MasterDTO.PCR_Securing_Not_Secure_Reason_Missing_Doors = Data.PCR_Securing_Not_Secure_Reason_Missing_Doors;
                pCR_Securing_MasterDTO.PCR_Securing_Not_Secure_Reason_Door_Open = Data.PCR_Securing_Not_Secure_Reason_Door_Open;
                pCR_Securing_MasterDTO.PCR_Securing_Not_Secure_Reason_Missing_Locks = Data.PCR_Securing_Not_Secure_Reason_Missing_Locks;
                pCR_Securing_MasterDTO.PCR_Securing_Not_Secure_Reason_Broken_Windows = Data.PCR_Securing_Not_Secure_Reason_Broken_Windows;
                pCR_Securing_MasterDTO.PCR_Securing_Not_Secure_Reason_Missing_Window = Data.PCR_Securing_Not_Secure_Reason_Missing_Window;
                pCR_Securing_MasterDTO.PCR_Securing_Not_Secure_Reason_Window_Open = Data.PCR_Securing_Not_Secure_Reason_Window_Open;
                pCR_Securing_MasterDTO.PCR_Securing_Not_Secure_Reason_Broken_Door = Data.PCR_Securing_Not_Secure_Reason_Broken_Door;
                pCR_Securing_MasterDTO.PCR_Securing_Not_Secure_Reason_Bids_Pending = Data.PCR_Securing_Not_Secure_Reason_Bids_Pending;
                pCR_Securing_MasterDTO.PCR_Securing_Not_Secure_Reason_Damage_Locks = Data.PCR_Securing_Not_Secure_Reason_Damage_Locks;

                pCR_Securing_MasterDTO.PCR_Securing_Depart_Not_Secure_Reason_Missing_Doors = Data.PCR_Securing_Depart_Not_Secure_Reason_Missing_Doors;
                pCR_Securing_MasterDTO.PCR_Securing_Depart_Not_Secure_Reason_Door_Open = Data.PCR_Securing_Depart_Not_Secure_Reason_Door_Open;
                pCR_Securing_MasterDTO.PCR_Securing_Depart_Not_Secure_Reason_Missing_Locks = Data.PCR_Securing_Depart_Not_Secure_Reason_Missing_Locks;
                pCR_Securing_MasterDTO.PCR_Securing_Depart_Not_Secure_Reason_Broken_Windows = Data.PCR_Securing_Depart_Not_Secure_Reason_Broken_Windows;
                pCR_Securing_MasterDTO.PCR_Securing_Depart_Not_Secure_Reason_Missing_Window = Data.PCR_Securing_Depart_Not_Secure_Reason_Missing_Window;
                pCR_Securing_MasterDTO.PCR_Securing_Depart_Not_Secure_Reason_Broken_Door = Data.PCR_Securing_Depart_Not_Secure_Reason_Broken_Door;
                pCR_Securing_MasterDTO.PCR_Securing_Depart_Not_Secure_Reason_Bids_Pending = Data.PCR_Securing_Depart_Not_Secure_Reason_Bids_Pending;
                pCR_Securing_MasterDTO.PCR_Securing_Depart_Not_Secure_Reason_Damage_Locks = Data.PCR_Securing_Depart_Not_Secure_Reason_Damage_Locks;

                pCR_Securing_MasterDTO.PCR_Securing_Boarded_Arrival = Data.PCR_Securing_Boarded_Arrival;
                pCR_Securing_MasterDTO.PCR_Securing_No_Of_First_Floor_Window = Data.PCR_Securing_No_Of_First_Floor_Window;
                pCR_Securing_MasterDTO.PCR_Securing_More_Boarding_Still_Required_OR_Not = Data.PCR_Securing_More_Boarding_Still_Required_OR_Not;
                pCR_Securing_MasterDTO.PCR_Securing_IsActive = Data.PCR_Securing_IsActive;
                pCR_Securing_MasterDTO.PCR_Securing_IsDelete = Data.PCR_Securing_IsDelete;
                pCR_Securing_MasterDTO.UserID = Data.UserID;
                pCR_Securing_MasterDTO.Type = Data.Type;
                pCR_Securing_MasterDTO.fwo_pkyeId = Data.fwo_pkyeId;



                var AddSecuring = await Task.Run(() => pCR_Securing_MasterData.AddPCR_Securing_Data(pCR_Securing_MasterDTO));
                return AddSecuring;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //For PCR Securing Get method
        [Authorize]
        [HttpPost]
        [Route("GetPCRSecuringData")]
        public async Task<List<dynamic>> GetPCRSecuringData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();


                var Data = JsonConvert.DeserializeObject<PCR_Securing_MasterDTO>(s);

                PCR_Securing_MasterDTO pCR_Securing_MasterDTO = new PCR_Securing_MasterDTO();

                pCR_Securing_MasterDTO.PCR_Securing_pkeyId = Data.PCR_Securing_pkeyId;
                pCR_Securing_MasterDTO.PCR_Securing_WO_Id = Data.PCR_Securing_WO_Id;
                pCR_Securing_MasterDTO.Type = Data.Type;



                var GetSecuring = await Task.Run(() => pCR_Securing_MasterData.GetPCRSecuringDetails(pCR_Securing_MasterDTO));
                return GetSecuring;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        //For PCR Appliance Post method
        [Authorize]
        [HttpPost]
        [Route("PostPCRApplianceData")]
        public async Task<List<dynamic>> PostPCRApplianceData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();


                var Data = JsonConvert.DeserializeObject<PCR_Appliance_DTO>(s);
                Data.UserID = LoggedInUSerId;

                PCR_Appliance_DTO pCR_Appliance_DTO = new PCR_Appliance_DTO();

                pCR_Appliance_DTO.PCR_Appliance_pkeyId = Data.PCR_Appliance_pkeyId;
                pCR_Appliance_DTO.PCR_Appliance_MasterId = Data.PCR_Appliance_MasterId;
                pCR_Appliance_DTO.PCR_Appliance_WO_Id = Data.PCR_Appliance_WO_Id;
                pCR_Appliance_DTO.PCR_Appliance_ValType = Data.PCR_Appliance_ValType;

                pCR_Appliance_DTO.PCR_Appliance_Refrigerator = Data.PCR_Appliance_Refrigerator;
                pCR_Appliance_DTO.PCR_Appliance_Stove = Data.PCR_Appliance_Stove;
                pCR_Appliance_DTO.PCR_Appliance_Stove_Wall_Oven = Data.PCR_Appliance_Stove_Wall_Oven;
                pCR_Appliance_DTO.PCR_Appliance_Dishwasher = Data.PCR_Appliance_Dishwasher;
                pCR_Appliance_DTO.PCR_Appliance_Build_In_Microwave = Data.PCR_Appliance_Build_In_Microwave;
                pCR_Appliance_DTO.PCR_Appliance_Dryer = Data.PCR_Appliance_Dryer;
                pCR_Appliance_DTO.PCR_Appliance_Washer = Data.PCR_Appliance_Washer;
                pCR_Appliance_DTO.PCR_Appliance_Air_Conditioner = Data.PCR_Appliance_Air_Conditioner;
                pCR_Appliance_DTO.PCR_Appliance_Hot_Water_Heater = Data.PCR_Appliance_Hot_Water_Heater;
                pCR_Appliance_DTO.PCR_Appliance_Dehumidifier = Data.PCR_Appliance_Dehumidifier;
                pCR_Appliance_DTO.PCR_Appliance_Furnace = Data.PCR_Appliance_Furnace;
                pCR_Appliance_DTO.PCR_Appliance_Water_Softener = Data.PCR_Appliance_Water_Softener;
                pCR_Appliance_DTO.PCR_Appliance_Boiler = Data.PCR_Appliance_Boiler;

                pCR_Appliance_DTO.PCR_Appliance_IsActive = Data.PCR_Appliance_IsActive;
                pCR_Appliance_DTO.PCR_Appliance_IsDelete = Data.PCR_Appliance_IsDelete;
                pCR_Appliance_DTO.UserID = Data.UserID;
                pCR_Appliance_DTO.fwo_pkyeId = Data.fwo_pkyeId;
                pCR_Appliance_DTO.Type = Data.Type;



                var AddPRCAppliance = await Task.Run(() => pCR_Appliance_Data.AddPCR_Appliance_Data(pCR_Appliance_DTO));
                return AddPRCAppliance;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //For PCR Appliance Get method
        [Authorize]
        [HttpPost]
        [Route("GetPCRApplianceData")]
        public async Task<List<dynamic>> GetPCRApplianceData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();


                var Data = JsonConvert.DeserializeObject<PCR_Appliance_DTO>(s);

                PCR_Appliance_DTO pCR_Appliance_DTO = new PCR_Appliance_DTO();

                pCR_Appliance_DTO.PCR_Appliance_pkeyId = Data.PCR_Appliance_pkeyId;
                pCR_Appliance_DTO.PCR_Appliance_WO_Id = Data.PCR_Appliance_WO_Id;
                pCR_Appliance_DTO.Type = Data.Type;



                var GetPRCAppliance = await Task.Run(() => pCR_Appliance_Data.GetPCRApplianceDetails(pCR_Appliance_DTO));
                return GetPRCAppliance;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        ////For PCR Winterization Post method
        [Authorize]
        [HttpPost]
        [Route("PostPCRWinterizationData")]
        public async Task<List<dynamic>> PostPCRWinterizationData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();


                var Data = JsonConvert.DeserializeObject<PCR_WinterizationDTO>(s);
                Data.UserID = LoggedInUSerId;

                PCR_WinterizationDTO pCR_WinterizationDTO = new PCR_WinterizationDTO();


                pCR_WinterizationDTO.PCR_Winterization_pkeyId = Data.PCR_Winterization_pkeyId;
                pCR_WinterizationDTO.PCR_Winterization_MasterId = Data.PCR_Winterization_MasterId;
                pCR_WinterizationDTO.PCR_Winterization_WO_Id = Data.PCR_Winterization_WO_Id;
                pCR_WinterizationDTO.PCR_Winterization_ValType = Data.PCR_Winterization_ValType;
                pCR_WinterizationDTO.PCR_Winterization_Upon_Arrival = Data.PCR_Winterization_Upon_Arrival;
                pCR_WinterizationDTO.PCR_Winterization_Compleate_This_Order_Yes = Data.PCR_Winterization_Compleate_This_Order_Yes;
                pCR_WinterizationDTO.PCR_Winterization_Compleate_This_Order_No = Data.PCR_Winterization_Compleate_This_Order_No;
                pCR_WinterizationDTO.PCR_Winterization_Compleate_This_Order_Partial = Data.PCR_Winterization_Compleate_This_Order_Partial;
                pCR_WinterizationDTO.PCR_Winterization_Upon_Arrival_Never_Winterized = Data.PCR_Winterization_Upon_Arrival_Never_Winterized;
                pCR_WinterizationDTO.PCR_Winterization_Upon_Arrival_Breached = Data.PCR_Winterization_Upon_Arrival_Breached;
                pCR_WinterizationDTO.PCR_Winterization_Reason_Wint_NotCompleted_Allowable = Data.PCR_Winterization_Reason_Wint_NotCompleted_Allowable;
                pCR_WinterizationDTO.PCR_Winterization_Reason_Wint_NotCompleted_Upon_Arrival = Data.PCR_Winterization_Reason_Wint_NotCompleted_Upon_Arrival;
                pCR_WinterizationDTO.PCR_Winterization_Reason_Wint_NotCompleted_Out_Season = Data.PCR_Winterization_Reason_Wint_NotCompleted_Out_Season;
                pCR_WinterizationDTO.PCR_Winterization_Reason_Wint_NotCompleted_TernedOff = Data.PCR_Winterization_Reason_Wint_NotCompleted_TernedOff;
                pCR_WinterizationDTO.PCR_Winterization_Reason_Wint_NotCompleted_Prop_Damaged = Data.PCR_Winterization_Reason_Wint_NotCompleted_Prop_Damaged;
                pCR_WinterizationDTO.PCR_Winterization_Reason_Wint_NotCompleted_Plumbing_Damage = Data.PCR_Winterization_Reason_Wint_NotCompleted_Plumbing_Damage;
                pCR_WinterizationDTO.PCR_Winterization_Reason_Wint_NotCompleted_Plumbing_IsMissing = Data.PCR_Winterization_Reason_Wint_NotCompleted_Plumbing_IsMissing;
                pCR_WinterizationDTO.PCR_Winterization_Reason_Wint_NotCompleted_AllReady_Winterized = Data.PCR_Winterization_Reason_Wint_NotCompleted_AllReady_Winterized;
                pCR_WinterizationDTO.PCR_Winterization_Reason_Wint_NotCompleted_Common_Water_Line = Data.PCR_Winterization_Reason_Wint_NotCompleted_Common_Water_Line;
                pCR_WinterizationDTO.PCR_Winterization_Reason_Wint_NotCompleted_Maintaining_Utilities = Data.PCR_Winterization_Reason_Wint_NotCompleted_Maintaining_Utilities;
                pCR_WinterizationDTO.PCR_Winterization_Reason_Wint_NotCompleted_Other = Data.PCR_Winterization_Reason_Wint_NotCompleted_Other;
                pCR_WinterizationDTO.PCR_Winterization_Heating_System = Data.PCR_Winterization_Heating_System;
                pCR_WinterizationDTO.PCR_Winterization_Heating_System_Well = Data.PCR_Winterization_Heating_System_Well;
                pCR_WinterizationDTO.PCR_Winterization_Radiant_Heat_System = Data.PCR_Winterization_Radiant_Heat_System;
                pCR_WinterizationDTO.PCR_Winterization_Radiant_Heat_System_Well = Data.PCR_Winterization_Radiant_Heat_System_Well;
                pCR_WinterizationDTO.PCR_Winterization_Steam_Heat_System = Data.PCR_Winterization_Steam_Heat_System;
                pCR_WinterizationDTO.PCR_Winterization_Steam_Heat_System_Well = Data.PCR_Winterization_Steam_Heat_System_Well;
                pCR_WinterizationDTO.PCR_Winterization_Posted_Signs = Data.PCR_Winterization_Posted_Signs;
                pCR_WinterizationDTO.PCR_Winterization_Common_Water_Line = Data.PCR_Winterization_Common_Water_Line;
                pCR_WinterizationDTO.PCR_Winterization_AntiFreeze_Toilet = Data.PCR_Winterization_AntiFreeze_Toilet;
                pCR_WinterizationDTO.PCR_Winterization_Water_Heater_Drained = Data.PCR_Winterization_Water_Heater_Drained;
                pCR_WinterizationDTO.PCR_Winterization_Water_Off_At_Curb = Data.PCR_Winterization_Water_Off_At_Curb;
                pCR_WinterizationDTO.PCR_Winterization_Blown_All_Lines = Data.PCR_Winterization_Blown_All_Lines;
                pCR_WinterizationDTO.PCR_Winterization_System_Held_Pressure = Data.PCR_Winterization_System_Held_Pressure;
                pCR_WinterizationDTO.PCR_Winterization_Disconnected_Water_Meter_Yes = Data.PCR_Winterization_Disconnected_Water_Meter_Yes;
                pCR_WinterizationDTO.PCR_Winterization_Disconnected_Water_Meter_No_Shut_Valve = Data.PCR_Winterization_Disconnected_Water_Meter_No_Shut_Valve;
                pCR_WinterizationDTO.PCR_Winterization_Disconnected_Water_Meter_No_Common_Water_Line = Data.PCR_Winterization_Disconnected_Water_Meter_No_Common_Water_Line;
                pCR_WinterizationDTO.PCR_Winterization_Disconnected_Water_Meter_No_Unable_To_Locate = Data.PCR_Winterization_Disconnected_Water_Meter_No_Unable_To_Locate;
                pCR_WinterizationDTO.PCR_Winterization_Disconnected_Water_Meter_No_Prohibited_Ordinance = Data.PCR_Winterization_Disconnected_Water_Meter_No_Prohibited_Ordinance;
                pCR_WinterizationDTO.PCR_Winterization_Disconnected_Water_Meter_No_Others = Data.PCR_Winterization_Disconnected_Water_Meter_No_Others;
                pCR_WinterizationDTO.PCR_Winterization_Radiant_Heat_Boiler_Drained = Data.PCR_Winterization_Radiant_Heat_Boiler_Drained;
                pCR_WinterizationDTO.PCR_Winterization_Radiant_Heat_Zone_Valves_Opened = Data.PCR_Winterization_Radiant_Heat_Zone_Valves_Opened;
                pCR_WinterizationDTO.PCR_Winterization_Radiant_Heat_AntiFreeze_Boiler = Data.PCR_Winterization_Radiant_Heat_AntiFreeze_Boiler;
                pCR_WinterizationDTO.PCR_Winterization_If_Well_System_Breaker_Off = Data.PCR_Winterization_If_Well_System_Breaker_Off;
                pCR_WinterizationDTO.PCR_Winterization_If_Well_System_Pressure_Tank_Drained = Data.PCR_Winterization_If_Well_System_Pressure_Tank_Drained;
                pCR_WinterizationDTO.PCR_Winterization_If_Well_System_Supply_Line_Disconnect = Data.PCR_Winterization_If_Well_System_Supply_Line_Disconnect;
                pCR_WinterizationDTO.PCR_Winterization_Interior_Main_Valve_Shut_Off = Data.PCR_Winterization_Interior_Main_Valve_Shut_Off;
                pCR_WinterizationDTO.PCR_Winterization_Interior_Main_Valve_Reason = Data.PCR_Winterization_Interior_Main_Valve_Reason;
                pCR_WinterizationDTO.PCR_Winterization_Interior_Main_Valve_Fire_Suppression_System = Data.PCR_Winterization_Interior_Main_Valve_Fire_Suppression_System;
                pCR_WinterizationDTO.PCR_Winterization_To_Bid = Data.PCR_Winterization_To_Bid;
                pCR_WinterizationDTO.PCR_Winterization_To_Bit_Text = Data.PCR_Winterization_To_Bit_Text;
                pCR_WinterizationDTO.PCR_Winterization_Winterize = Data.PCR_Winterization_Winterize;
                pCR_WinterizationDTO.PCR_Winterization_Thaw = Data.PCR_Winterization_Thaw;
                pCR_WinterizationDTO.PCR_Winterization_Description = Data.PCR_Winterization_Description;
                pCR_WinterizationDTO.PCR_Winterization_System_Type = Data.PCR_Winterization_System_Type;
                pCR_WinterizationDTO.PCR_Winterization_Reason = Data.PCR_Winterization_Reason;
                pCR_WinterizationDTO.PCR_Winterization_Amount = Data.PCR_Winterization_Amount;
                pCR_WinterizationDTO.PCR_Winterization_Winterize_Men = Data.PCR_Winterization_Winterize_Men;
                pCR_WinterizationDTO.PCR_Winterization_Winterize_Hrs = Data.PCR_Winterization_Winterize_Hrs;
                pCR_WinterizationDTO.PCR_Winterization_IsActive = Data.PCR_Winterization_IsActive;
                pCR_WinterizationDTO.PCR_Winterization_IsDelete = Data.PCR_Winterization_IsDelete;
                pCR_WinterizationDTO.PCR_Winterization_Reason_Wint_NotCompleted_Other_Text = Data.PCR_Winterization_Reason_Wint_NotCompleted_Other_Text;
                pCR_WinterizationDTO.PCR_Winterization_Disconnected_Water_Meter_Other_Text = Data.PCR_Winterization_Disconnected_Water_Meter_Other_Text;
                pCR_WinterizationDTO.PCR_Winterization_TextArea_Comment = Data.PCR_Winterization_TextArea_Comment;
                pCR_WinterizationDTO.UserID = Data.UserID;
                pCR_WinterizationDTO.Type = Data.Type;
                pCR_WinterizationDTO.fwo_pkyeId = Data.fwo_pkyeId;



                var AddWinterization = await Task.Run(() => pCR_WinterizationData.AddPCR_Winterization_Data(pCR_WinterizationDTO));
                return AddWinterization;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        ////For PCR Winterization Get method
        [Authorize]
        [HttpPost]
        [Route("GetPCRWinterizationData")]
        public async Task<List<dynamic>> GetPCRWinterizationData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();


                var Data = JsonConvert.DeserializeObject<PCR_WinterizationDTO>(s);

                PCR_WinterizationDTO pCR_WinterizationDTO = new PCR_WinterizationDTO();


                pCR_WinterizationDTO.PCR_Winterization_pkeyId = Data.PCR_Winterization_pkeyId;
                pCR_WinterizationDTO.PCR_Winterization_WO_Id = Data.PCR_Winterization_WO_Id;
                pCR_WinterizationDTO.Type = Data.Type;



                var GetWinterization = await Task.Run(() => pCR_WinterizationData.GetPCRPCRPCRWinterizationDetails(pCR_WinterizationDTO));
                return GetWinterization;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }



        //For PCR Yard Post method
        [Authorize]
        [HttpPost]
        [Route("PostPCRYardData")]
        public async Task<List<dynamic>> PostPCRYardData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();


                var Data = JsonConvert.DeserializeObject<PCR_Yard_MaintenanceDTO>(s);
                Data.UserID = LoggedInUSerId;

                PCR_Yard_MaintenanceDTO pCR_Yard_MaintenanceDTO = new PCR_Yard_MaintenanceDTO();


                pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_pkeyId = Data.PCR_Yard_Maintenance_pkeyId;
                pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_MasterId = Data.PCR_Yard_Maintenance_MasterId;
                pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_WO_Id = Data.PCR_Yard_Maintenance_WO_Id;
                pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_ValType = Data.PCR_Yard_Maintenance_ValType;

                pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Grass_Cut_Completed = Data.PCR_Yard_Maintenance_Grass_Cut_Completed;
                pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Lot_Size = Data.PCR_Yard_Maintenance_Lot_Size;
                pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Cuttable_Area = Data.PCR_Yard_Maintenance_Cuttable_Area;
                pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Bit_To_Cut_Grass_Lot_Dimension_Lenght = Data.PCR_Yard_Maintenance_Bit_To_Cut_Grass_Lot_Dimension_Lenght;
                pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Bit_To_Cut_Grass_Lot_Dimension_Width = Data.PCR_Yard_Maintenance_Bit_To_Cut_Grass_Lot_Dimension_Width;
                pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Bit_To_Cut_Grass_Lot_Dimension_Height = Data.PCR_Yard_Maintenance_Bit_To_Cut_Grass_Lot_Dimension_Height;
                pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Bit_To_Cut_Grass_Bid_For_Inital_Cut = Data.PCR_Yard_Maintenance_Bit_To_Cut_Grass_Bid_For_Inital_Cut;
                pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Bit_To_Cut_Grass_Reason_For_Inital_Cut = Data.PCR_Yard_Maintenance_Bit_To_Cut_Grass_Reason_For_Inital_Cut;
                pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Bid_Recut = Data.PCR_Yard_Maintenance_Bid_Recut;
                pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Reason_For_Recut = Data.PCR_Yard_Maintenance_Reason_For_Recut;
                pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Trees_Cut_Back_Order = Data.PCR_Yard_Maintenance_Trees_Cut_Back_Order;
                pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Arrival_Shrubs_Touching_House = Data.PCR_Yard_Maintenance_Arrival_Shrubs_Touching_House;
                pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Arrival_Trees_Touching_House = Data.PCR_Yard_Maintenance_Arrival_Trees_Touching_House;
                pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Depature_Trees = Data.PCR_Yard_Maintenance_Depature_Trees;
                pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Were_Trimmed_Insurer_Guidlines = Data.PCR_Yard_Maintenance_Were_Trimmed_Insurer_Guidlines;

                pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Grass_Maintained_No = Data.PCR_Yard_Maintenance_Grass_Maintained_No;

                pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Bid_To_Shrubs_Dimensions_Length = Data.PCR_Yard_Maintenance_Bid_To_Shrubs_Dimensions_Length;
                pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Bid_To_Shrubs_Dimensions_Width = Data.PCR_Yard_Maintenance_Bid_To_Shrubs_Dimensions_Width;
                pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Bid_To_Shrubs_Dimensions_Height = Data.PCR_Yard_Maintenance_Bid_To_Shrubs_Dimensions_Height;
                pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Bid_To_Shrubs_Quantity = Data.PCR_Yard_Maintenance_Bid_To_Shrubs_Quantity;
                pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Bid_To_Shrubs_Unit_Price = Data.PCR_Yard_Maintenance_Bid_To_Shrubs_Unit_Price;
                pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Bid_To_Shrubs_Bid_Amount = Data.PCR_Yard_Maintenance_Bid_To_Shrubs_Bid_Amount;
                pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Bid_To_Shrubs_Location = Data.PCR_Yard_Maintenance_Bid_To_Shrubs_Location;
                pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Bid_To_Shrubs_Reasons_Touching_House = Data.PCR_Yard_Maintenance_Bid_To_Shrubs_Reasons_Touching_House;
                pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Bid_To_Shrubs_Reasons_Touching_Other_Structure = Data.PCR_Yard_Maintenance_Bid_To_Shrubs_Reasons_Touching_Other_Structure;
                pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Bid_To_Shrubs_Reasons_Within_Street_View = Data.PCR_Yard_Maintenance_Bid_To_Shrubs_Reasons_Within_Street_View;
                pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Bid_To_Shrubs_Reasons_Affecting_Fencing = Data.PCR_Yard_Maintenance_Bid_To_Shrubs_Reasons_Affecting_Fencing;
                pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Bid_To_Shrubs_Causing_Damage = Data.PCR_Yard_Maintenance_Bid_To_Shrubs_Causing_Damage;
                pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Bid_To_Shrubs_Describe = Data.PCR_Yard_Maintenance_Bid_To_Shrubs_Describe;

                pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Bid_To_Trim_Dimensions_Length = Data.PCR_Yard_Maintenance_Bid_To_Trim_Dimensions_Length;
                pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Bid_To_Trim_Dimensions_Width = Data.PCR_Yard_Maintenance_Bid_To_Trim_Dimensions_Width;
                pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Bid_To_Trim_Dimensions_Height = Data.PCR_Yard_Maintenance_Bid_To_Trim_Dimensions_Height;
                pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Bid_To_Trim_Quantity = Data.PCR_Yard_Maintenance_Bid_To_Trim_Quantity;
                pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Bid_To_Trim_Unit_Price = Data.PCR_Yard_Maintenance_Bid_To_Trim_Unit_Price;
                pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Bid_To_Trim_Bid_Amount = Data.PCR_Yard_Maintenance_Bid_To_Trim_Bid_Amount;
                pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Bid_To_Trim_Location = Data.PCR_Yard_Maintenance_Bid_To_Trim_Location;
                pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Bid_To_Trim_Reasons_Touching_House = Data.PCR_Yard_Maintenance_Bid_To_Trim_Reasons_Touching_House;
                pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Bid_To_Trim_Reasons_Touching_Other_Structure = Data.PCR_Yard_Maintenance_Bid_To_Trim_Reasons_Touching_Other_Structure;
                pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Bid_To_Trim_Reasons_Within_Street_View = Data.PCR_Yard_Maintenance_Bid_To_Trim_Reasons_Within_Street_View;
                pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Bid_To_Trim_Reasons_Affecting_Fencing = Data.PCR_Yard_Maintenance_Bid_To_Trim_Reasons_Affecting_Fencing;
                pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Bid_To_Trim_Causing_Damage = Data.PCR_Yard_Maintenance_Bid_To_Trim_Causing_Damage;
                pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Bid_To_Trim_Describe = Data.PCR_Yard_Maintenance_Bid_To_Trim_Describe;
                pCR_Yard_MaintenanceDTO.PCR_Yard_Grass_LotSize = Data.PCR_Yard_Grass_LotSize;

                pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_IsActive = Data.PCR_Yard_Maintenance_IsActive;
                pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_IsDelete = Data.PCR_Yard_Maintenance_IsDelete;
                pCR_Yard_MaintenanceDTO.UserID = Data.UserID;
                pCR_Yard_MaintenanceDTO.fwo_pkyeId = Data.fwo_pkyeId;
                pCR_Yard_MaintenanceDTO.Type = Data.Type;



                var AddYard = await Task.Run(() => pCR_Yard_MaintenanceData.AddPCR_Yard_Maintenance_Data(pCR_Yard_MaintenanceDTO));
                return AddYard;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        //For PCR Yard Get method
        [Authorize]
        [HttpPost]
        [Route("GetPCRYardData")]
        public async Task<List<dynamic>> GetPCRYardData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();


                var Data = JsonConvert.DeserializeObject<PCR_Yard_MaintenanceDTO>(s);

                PCR_Yard_MaintenanceDTO pCR_Yard_MaintenanceDTO = new PCR_Yard_MaintenanceDTO();


                pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_pkeyId = Data.PCR_Yard_Maintenance_pkeyId;
                pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_WO_Id = Data.PCR_Yard_Maintenance_WO_Id;
                pCR_Yard_MaintenanceDTO.Type = Data.Type;



                var GetYard = await Task.Run(() => pCR_Yard_MaintenanceData.GetPCYardDetails(pCR_Yard_MaintenanceDTO));
                return GetYard;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }



        //For PCR Pool Post method
        [Authorize]
        [HttpPost]
        [Route("PostPCRPoolData")]
        public async Task<List<dynamic>> PostPCRPoolData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();


                var Data = JsonConvert.DeserializeObject<PCR_Pool_MasterDTO>(s);
                Data.UserID = LoggedInUSerId;

                PCR_Pool_MasterDTO pCR_Pool_MasterDTO = new PCR_Pool_MasterDTO();

                pCR_Pool_MasterDTO.PCR_Pool_pkeyId = Data.PCR_Pool_pkeyId;
                pCR_Pool_MasterDTO.PCR_Pool_MasterId = Data.PCR_Pool_MasterId;
                pCR_Pool_MasterDTO.PCR_Pool_WO_Id = Data.PCR_Pool_WO_Id;
                pCR_Pool_MasterDTO.PCR_Pool_ValType = Data.PCR_Pool_ValType;
                pCR_Pool_MasterDTO.PCR_Pool_Info_Pool_Present = Data.PCR_Pool_Info_Pool_Present;
                pCR_Pool_MasterDTO.PCR_Pool_Diameter_Ft = Data.PCR_Pool_Diameter_Ft;
                pCR_Pool_MasterDTO.PCR_Pool_Length_Ft = Data.PCR_Pool_Length_Ft;
                pCR_Pool_MasterDTO.PCR_Pool_Width_Ft = Data.PCR_Pool_Width_Ft;
                pCR_Pool_MasterDTO.PCR_Pool_Condition_Good = Data.PCR_Pool_Condition_Good;
                // pCR_Pool_MasterDTO.PCR_Pool_Condition_Fair = Data.PCR_Pool_Condition_Fair;
                // pCR_Pool_MasterDTO.PCR_Pool_Condition_Poor = Data.PCR_Pool_Condition_Poor;
                // pCR_Pool_MasterDTO.PCR_Pool_Condition_Damaged = Data.PCR_Pool_Condition_Damaged;
                pCR_Pool_MasterDTO.PCR_Pool_Type_InGround = Data.PCR_Pool_Type_InGround;
                // pCR_Pool_MasterDTO.PCR_Pool_Type_Above_Ground = Data.PCR_Pool_Type_Above_Ground;
                pCR_Pool_MasterDTO.PCR_Pool_Secure_On_This_Order = Data.PCR_Pool_Secure_On_This_Order;
                pCR_Pool_MasterDTO.PCR_Pool_Is_There_Fence = Data.PCR_Pool_Is_There_Fence;
                pCR_Pool_MasterDTO.PCR_Pool_Is_It_Locked = Data.PCR_Pool_Is_It_Locked;
                pCR_Pool_MasterDTO.PCR_Pool_Water_Level_Full = Data.PCR_Pool_Water_Level_Full;
                // pCR_Pool_MasterDTO.PCR_Pool_Water_Level_Partially_Filled = Data.PCR_Pool_Water_Level_Partially_Filled;
                // pCR_Pool_MasterDTO.PCR_Pool_Water_Level_Empty = Data.PCR_Pool_Water_Level_Empty;
                pCR_Pool_MasterDTO.PCR_Pool_Did_You_Drain_It = Data.PCR_Pool_Did_You_Drain_It;
                pCR_Pool_MasterDTO.PCR_Pool_Dismantled_Removed = Data.PCR_Pool_Dismantled_Removed;
                pCR_Pool_MasterDTO.PCR_Pool_Is_There_Depression_Left = Data.PCR_Pool_Is_There_Depression_Left;
                pCR_Pool_MasterDTO.PCR_Pool_Secured_Per_Guidelines = Data.PCR_Pool_Secured_Per_Guidelines;
                pCR_Pool_MasterDTO.PCR_Pool_Is_The_Pool_Converted_Prevents_Entry = Data.PCR_Pool_Is_The_Pool_Converted_Prevents_Entry;
                pCR_Pool_MasterDTO.PCR_Pool_Hot_Tub_Present = Data.PCR_Pool_Hot_Tub_Present;
                pCR_Pool_MasterDTO.PCR_Pool_Bids_Drain_Shock_Install_Safety_Cover = Data.PCR_Pool_Bids_Drain_Shock_Install_Safety_Cover;
                pCR_Pool_MasterDTO.PCR_Pool_Bid_To_Install_Safety_Cover = Data.PCR_Pool_Bid_To_Install_Safety_Cover;
                pCR_Pool_MasterDTO.PCR_Pool_Bid_To_Drain = Data.PCR_Pool_Bid_To_Drain;
                pCR_Pool_MasterDTO.PCR_Pool_Bid_To_Dismantle = Data.PCR_Pool_Bid_To_Dismantle;
                pCR_Pool_MasterDTO.PCR_Pool_Drain_Remove = Data.PCR_Pool_Drain_Remove;
                pCR_Pool_MasterDTO.PCR_Pool_Bid_To_Fill_Hole = Data.PCR_Pool_Bid_To_Fill_Hole;
                pCR_Pool_MasterDTO.PCR_Pool_Size_Of_Hole = Data.PCR_Pool_Size_Of_Hole;
                pCR_Pool_MasterDTO.PCR_Pool_Cubic_Yds_Of_Dirt = Data.PCR_Pool_Cubic_Yds_Of_Dirt;
                pCR_Pool_MasterDTO.PCR_Pool_Secure_This_Order_No_Secure_By_FiveBrothers = Data.PCR_Pool_Secure_This_Order_No_Secure_By_FiveBrothers;
                // pCR_Pool_MasterDTO.PCR_Pool_Secure_This_Order_No_Bid_To_Secure = Data.PCR_Pool_Secure_This_Order_No_Bid_To_Secure;
                // pCR_Pool_MasterDTO.PCR_Pool_Secure_This_Order_No_Previously_Secure_By_Order = Data.PCR_Pool_Secure_This_Order_No_Previously_Secure_By_Order;
                //  pCR_Pool_MasterDTO.PCR_Pool_Secure_This_Order_No_Other = Data.PCR_Pool_Secure_This_Order_No_Other;
                pCR_Pool_MasterDTO.PCR_Pool_Hot_Tub_Present_Yes_Covered_Drained = Data.PCR_Pool_Hot_Tub_Present_Yes_Covered_Drained;
                pCR_Pool_MasterDTO.PCR_Pool_Hot_Tub_Did_You_Secure = Data.PCR_Pool_Hot_Tub_Did_You_Secure;
                pCR_Pool_MasterDTO.PCR_Pool_Hot_Tub_Bids_Diameter_Ft = Data.PCR_Pool_Hot_Tub_Bids_Diameter_Ft;
                pCR_Pool_MasterDTO.PCR_Pool_Hot_Tub_Bids_Length_Ft = Data.PCR_Pool_Hot_Tub_Bids_Length_Ft;
                pCR_Pool_MasterDTO.PCR_Pool_Hot_Tub_Bids_Width_Ft = Data.PCR_Pool_Hot_Tub_Bids_Width_Ft;
                pCR_Pool_MasterDTO.PCR_Pool_Hot_Tub_Bids_Bid_To_Drain = Data.PCR_Pool_Hot_Tub_Bids_Bid_To_Drain;
                pCR_Pool_MasterDTO.PCR_Pool_Hot_Tub_Bids_Bit_To_Install_Cover = Data.PCR_Pool_Hot_Tub_Bids_Bit_To_Install_Cover;
                pCR_Pool_MasterDTO.PCR_Pool_Hot_Tub_Bids_Drain_Secure = Data.PCR_Pool_Hot_Tub_Bids_Drain_Secure;
                pCR_Pool_MasterDTO.PCR_Pool_IsActive = Data.PCR_Pool_IsActive;
                pCR_Pool_MasterDTO.PCR_Pool_IsDelete = Data.PCR_Pool_IsDelete;
                pCR_Pool_MasterDTO.UserID = Data.UserID;
                pCR_Pool_MasterDTO.fwo_pkyeId = Data.fwo_pkyeId;
                pCR_Pool_MasterDTO.Type = Data.Type;



                var AddPool = await Task.Run(() => pCR_Pool_MasterData.AddPCR_Pool_Data(pCR_Pool_MasterDTO));
                return AddPool;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //For PCR Pool Get method
        [Authorize]
        [HttpPost]
        [Route("GetPCRPoolData")]
        public async Task<List<dynamic>> GetPCRPoolData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();


                var Data = JsonConvert.DeserializeObject<PCR_Pool_MasterDTO>(s);

                PCR_Pool_MasterDTO pCR_Pool_MasterDTO = new PCR_Pool_MasterDTO();

                pCR_Pool_MasterDTO.PCR_Pool_pkeyId = Data.PCR_Pool_pkeyId;
                pCR_Pool_MasterDTO.PCR_Pool_WO_Id = Data.PCR_Pool_WO_Id;
                pCR_Pool_MasterDTO.Type = Data.Type;



                var GetPool = await Task.Run(() => pCR_Pool_MasterData.GetPCRPoolDetails(pCR_Pool_MasterDTO));
                return GetPool;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        //For PCR Debris Post method
        [Authorize]
        [HttpPost]
        [Route("PostPCRDebrisData")]
        public async Task<List<dynamic>> PostPCRDebrisData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();


                var Data = JsonConvert.DeserializeObject<PCR_Debris_MasterDTO>(s);
                Data.UserID = LoggedInUSerId;

                PCR_Debris_MasterDTO pCR_Debris_MasterDTO = new PCR_Debris_MasterDTO();

                pCR_Debris_MasterDTO.PCR_Debris_pkeyId = Data.PCR_Debris_pkeyId;
                pCR_Debris_MasterDTO.PCR_Debris_Master_Id = Data.PCR_Debris_Master_Id;
                pCR_Debris_MasterDTO.PCR_Debris_WO_Id = Data.PCR_Debris_WO_Id;
                pCR_Debris_MasterDTO.PCR_Debris_ValType = Data.PCR_Debris_ValType;
                //pCR_Debris_MasterDTO.PCR_Debris_Interior_Debris = Data.PCR_Debris_Interior_Debris;
                pCR_Debris_MasterDTO.PCR_Debris_Is_There_Interior_Debris_Present = Data.PCR_Debris_Is_There_Interior_Debris_Present;
                pCR_Debris_MasterDTO.PCR_Debris_Remove_Any_Interior_Debris = Data.PCR_Debris_Remove_Any_Interior_Debris;
                pCR_Debris_MasterDTO.PCR_Debris_Describe = Data.PCR_Debris_Describe;
                pCR_Debris_MasterDTO.PCR_Debris_Cubic_Yards = Data.PCR_Debris_Cubic_Yards;
                pCR_Debris_MasterDTO.PCR_Debris_Broom_Swept_Condition = Data.PCR_Debris_Broom_Swept_Condition;
                pCR_Debris_MasterDTO.PCR_Debris_Broom_Swept_Condition_Describe = Data.PCR_Debris_Broom_Swept_Condition_Describe;
                //pCR_Debris_MasterDTO.PCR_Debris_Exterior_Debris = Data.PCR_Debris_Exterior_Debris;
                pCR_Debris_MasterDTO.PCR_Debris_Remove_Exterior_Debris = Data.PCR_Debris_Remove_Exterior_Debris;
                pCR_Debris_MasterDTO.PCR_Debris_Exterior_Debris_Present = Data.PCR_Debris_Exterior_Debris_Present;
                pCR_Debris_MasterDTO.PCR_Debris_Exterior_Debris_Describe = Data.PCR_Debris_Exterior_Debris_Describe;
                pCR_Debris_MasterDTO.PCR_Debris_Exterior_Debris_Cubic_Yard = Data.PCR_Debris_Exterior_Debris_Cubic_Yard;
                pCR_Debris_MasterDTO.PCR_Debris_Exterior_Debris_Visible_From_Street = Data.PCR_Debris_Exterior_Debris_Visible_From_Street;
                pCR_Debris_MasterDTO.PCR_Debris_Exterior_On_The_Lawn = Data.PCR_Debris_Exterior_On_The_Lawn;
                pCR_Debris_MasterDTO.PCR_Debris_Exterior_Vehicles_Present = Data.PCR_Debris_Exterior_Vehicles_Present;
                pCR_Debris_MasterDTO.PCR_Debris_Exterior_Vehicles_Present_Describe = Data.PCR_Debris_Exterior_Vehicles_Present_Describe;
                pCR_Debris_MasterDTO.PCR_Debris_Dump_Recipt_Name = Data.PCR_Debris_Dump_Recipt_Name;
                pCR_Debris_MasterDTO.PCR_Debris_Dump_Recipt_Address = Data.PCR_Debris_Dump_Recipt_Address;
                pCR_Debris_MasterDTO.PCR_Debris_Dump_Recipt_Phone = Data.PCR_Debris_Dump_Recipt_Phone;
                pCR_Debris_MasterDTO.PCR_Debris_Dump_Recipt_Desc_what_was_Dump = Data.PCR_Debris_Dump_Recipt_Desc_what_was_Dump;
                pCR_Debris_MasterDTO.PCR_Debris_Dump_Recipt_Means_Of_Disposal = Data.PCR_Debris_Dump_Recipt_Means_Of_Disposal;
                pCR_Debris_MasterDTO.PCR_Debris_InteriorHazards_Health_Present = Data.PCR_Debris_InteriorHazards_Health_Present;
                pCR_Debris_MasterDTO.PCR_Debris_InteriorHazards_Health_Present_Describe = Data.PCR_Debris_InteriorHazards_Health_Present_Describe;
                pCR_Debris_MasterDTO.PCR_Debris_InteriorHazards_Health_Present_Cubic_Yard = Data.PCR_Debris_InteriorHazards_Health_Present_Cubic_Yard;
                pCR_Debris_MasterDTO.PCR_Debris_Exterior_Hazards_Health_Present = Data.PCR_Debris_Exterior_Hazards_Health_Present;
                pCR_Debris_MasterDTO.PCR_Debris_Exterior_Hazards_Health_Present_Describe = Data.PCR_Debris_Exterior_Hazards_Health_Present_Describe;
                pCR_Debris_MasterDTO.PCR_Debris_Exterior_Hazards_Health_PresentCubic_Yards = Data.PCR_Debris_Exterior_Hazards_Health_PresentCubic_Yards;
                pCR_Debris_MasterDTO.PCR_Debris_IsActive = Data.PCR_Debris_IsActive;
                pCR_Debris_MasterDTO.UserID = Data.UserID;
                pCR_Debris_MasterDTO.fwo_pkyeId = Data.fwo_pkyeId;
                pCR_Debris_MasterDTO.Type = Data.Type;



                var AddPRCDebris = await Task.Run(() => pCR_Debris_MasterData.AddPCR_Debris_Data(pCR_Debris_MasterDTO));
                return AddPRCDebris;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //For PCR Debris Get method
        [Authorize]
        [HttpPost]
        [Route("GetPCRDebrisData")]
        public async Task<List<dynamic>> GetPCRDebrisData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();


                var Data = JsonConvert.DeserializeObject<PCR_Debris_MasterDTO>(s);

                PCR_Debris_MasterDTO pCR_Debris_MasterDTO = new PCR_Debris_MasterDTO();

                pCR_Debris_MasterDTO.PCR_Debris_pkeyId = Data.PCR_Debris_pkeyId;
                pCR_Debris_MasterDTO.PCR_Debris_WO_Id = Data.PCR_Debris_WO_Id;
                pCR_Debris_MasterDTO.Type = Data.Type;



                var GetPRCDebris = await Task.Run(() => pCR_Debris_MasterData.GetPCRDebrisDetails(pCR_Debris_MasterDTO));
                return GetPRCDebris;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        //For PCR Utilities Post method
        [Authorize]
        [HttpPost]
        [Route("PostPCRUtilitiesData")]
        public async Task<List<dynamic>> PostPCRUtilitiesData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();


                var Data = JsonConvert.DeserializeObject<PCR_Utilities_MasterDTO>(s);
                Data.UserID = LoggedInUSerId;

                PCR_Utilities_MasterDTO pCR_Utilities_MasterDTO = new PCR_Utilities_MasterDTO();

                pCR_Utilities_MasterDTO.PCR_Utilities_pkeyId = Data.PCR_Utilities_pkeyId;
                pCR_Utilities_MasterDTO.PCR_Utilities_MasterId = Data.PCR_Utilities_MasterId;
                pCR_Utilities_MasterDTO.PCR_Utilities_WO_Id = Data.PCR_Utilities_WO_Id;
                pCR_Utilities_MasterDTO.PCR_Utilities_ValType = Data.PCR_Utilities_ValType;
                pCR_Utilities_MasterDTO.PCR_Utilities_On_Arrival_Water = Data.PCR_Utilities_On_Arrival_Water;
                pCR_Utilities_MasterDTO.PCR_Utilities_On_Arrival_Gas = Data.PCR_Utilities_On_Arrival_Gas;
                pCR_Utilities_MasterDTO.PCR_Utilities_On_Arrival_Electric = Data.PCR_Utilities_On_Arrival_Electric;
                pCR_Utilities_MasterDTO.PCR_Utilities_On_Departure_Water = Data.PCR_Utilities_On_Departure_Water;
                pCR_Utilities_MasterDTO.PCR_Utilities_On_Departure_Gas = Data.PCR_Utilities_On_Departure_Gas;
                pCR_Utilities_MasterDTO.PCR_Utilities_On_Departure_Electric = Data.PCR_Utilities_On_Departure_Electric;

                pCR_Utilities_MasterDTO.PCR_Utilities_Sump_Pump = Data.PCR_Utilities_Sump_Pump;
                pCR_Utilities_MasterDTO.PCR_Utilities_Sump_Pump_Commend = Data.PCR_Utilities_Sump_Pump_Commend;
                pCR_Utilities_MasterDTO.PCR_Utilities_Sump_Pump_Sump_Test = Data.PCR_Utilities_Sump_Pump_Sump_Test;
                pCR_Utilities_MasterDTO.PCR_Utilities_Main_Breaker_And_Operational = Data.PCR_Utilities_Main_Breaker_And_Operational;
                pCR_Utilities_MasterDTO.PCR_Utilities_Sump_Pump_Missing_Bid_To_Replace = Data.PCR_Utilities_Sump_Pump_Missing_Bid_To_Replace;
                pCR_Utilities_MasterDTO.PCR_Utilities_Transferred_Activated = Data.PCR_Utilities_Transferred_Activated;
                pCR_Utilities_MasterDTO.PCR_Utilities_Reason_UtilitiesNot_Transferred = Data.PCR_Utilities_Reason_UtilitiesNot_Transferred;
                pCR_Utilities_MasterDTO.PCR_Utilities_Reason_UtilitiesNot_Transferred_Other_Notes = Data.PCR_Utilities_Reason_UtilitiesNot_Transferred_Other_Notes;

                pCR_Utilities_MasterDTO.PCR_Utilities_Transferred_Water_Co_Name = Data.PCR_Utilities_Transferred_Water_Co_Name;
                pCR_Utilities_MasterDTO.PCR_Utilities_Transferred_Water_Address = Data.PCR_Utilities_Transferred_Water_Address;
                pCR_Utilities_MasterDTO.PCR_Utilities_Transferred_Water_Phone = Data.PCR_Utilities_Transferred_Water_Phone;
                pCR_Utilities_MasterDTO.PCR_Utilities_Transferred_Water_Acct = Data.PCR_Utilities_Transferred_Water_Acct;
                pCR_Utilities_MasterDTO.PCR_Utilities_Transferred_Gas_Co_Name = Data.PCR_Utilities_Transferred_Gas_Co_Name;
                pCR_Utilities_MasterDTO.PCR_Utilities_Transferred_Gas_Address = Data.PCR_Utilities_Transferred_Gas_Address;
                pCR_Utilities_MasterDTO.PCR_Utilities_Transferred_Gas_Phone = Data.PCR_Utilities_Transferred_Gas_Phone;
                pCR_Utilities_MasterDTO.PCR_Utilities_Transferred_Gas_Acct = Data.PCR_Utilities_Transferred_Gas_Acct;
                pCR_Utilities_MasterDTO.PCR_Utilities_Transferred_Electric_Co_Name = Data.PCR_Utilities_Transferred_Electric_Co_Name;
                pCR_Utilities_MasterDTO.PCR_Utilities_Transferred_Electric_Address = Data.PCR_Utilities_Transferred_Electric_Address;
                pCR_Utilities_MasterDTO.PCR_Utilities_Transferred_Electric_Phone = Data.PCR_Utilities_Transferred_Electric_Phone;
                pCR_Utilities_MasterDTO.PCR_Utilities_Transferred_Electric_Acct = Data.PCR_Utilities_Transferred_Electric_Acct;

                pCR_Utilities_MasterDTO.PCR_Utilities_IsActive = Data.PCR_Utilities_IsActive;
                pCR_Utilities_MasterDTO.PCR_Utilities_IsDelete = Data.PCR_Utilities_IsDelete;
                pCR_Utilities_MasterDTO.UserID = Data.UserID;
                pCR_Utilities_MasterDTO.fwo_pkyeId = Data.fwo_pkyeId;
                pCR_Utilities_MasterDTO.Type = Data.Type;


                var AddUtilities = await Task.Run(() => pCR_Utilities_MasterData.AddPCR_Utilities_Data(pCR_Utilities_MasterDTO));
                return AddUtilities;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //For PCR Utilities Get method
        [Authorize]
        [HttpPost]
        [Route("GetPCRUtilitiesData")]
        public async Task<List<dynamic>> GetPCRUtilitiesData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();


                var Data = JsonConvert.DeserializeObject<PCR_Utilities_MasterDTO>(s);

                PCR_Utilities_MasterDTO pCR_Utilities_MasterDTO = new PCR_Utilities_MasterDTO();

                pCR_Utilities_MasterDTO.PCR_Utilities_pkeyId = Data.PCR_Utilities_pkeyId;
                pCR_Utilities_MasterDTO.PCR_Utilities_WO_Id = Data.PCR_Utilities_WO_Id;
                pCR_Utilities_MasterDTO.Type = Data.Type;



                var GetUtilities = await Task.Run(() => pCR_Utilities_MasterData.GetPCRUtilitisDetails(pCR_Utilities_MasterDTO));
                return GetUtilities;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }



        ////For PCR Damage Post method
        [Authorize]
        [HttpPost]
        [Route("PostPCRDamageData")]
        public async Task<List<dynamic>> PostPCRDamageData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            string s = string.Empty;
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                  s = reader.ReadToEnd();


                var Data = JsonConvert.DeserializeObject<PCRDamageMasterDTO>(s);
                Data.UserID = LoggedInUSerId;

                PCRDamageMasterDTO pCRDamageMasterDTO = new PCRDamageMasterDTO();

                pCRDamageMasterDTO.PCR_Damage_pkeyId = Data.PCR_Damage_pkeyId;
                pCRDamageMasterDTO.PCR_Damage_MasterId = Data.PCR_Damage_MasterId;
                pCRDamageMasterDTO.PCR_Damage_WO_Id = Data.PCR_Damage_WO_Id;
                pCRDamageMasterDTO.PCR_Damage_ValType = Data.PCR_Damage_ValType;
                pCRDamageMasterDTO.PCR_Damage_Fire_Smoke_Damage_Yes = Data.PCR_Damage_Fire_Smoke_Damage_Yes;

                pCRDamageMasterDTO.PCR_Damage_Mortgagor_Neglect_Yes = Data.PCR_Damage_Mortgagor_Neglect_Yes;
                pCRDamageMasterDTO.PCR_Damage_Vandalism_Yes = Data.PCR_Damage_Vandalism_Yes;
                pCRDamageMasterDTO.PCR_Damage_Freeze_Damage_Yes = Data.PCR_Damage_Freeze_Damage_Yes;
                pCRDamageMasterDTO.PCR_Damage_Storm_Damage_Yes = Data.PCR_Damage_Storm_Damage_Yes;
                pCRDamageMasterDTO.PCR_Damage_Flood_Damage_Yes = Data.PCR_Damage_Flood_Damage_Yes;
                pCRDamageMasterDTO.PCR_Damage_Water_Damage_Yes = Data.PCR_Damage_Water_Damage_Yes;
                pCRDamageMasterDTO.PCR_Damage_Wear_And_Tear_Yes = Data.PCR_Damage_Wear_And_Tear_Yes;
                pCRDamageMasterDTO.PCR_Damage_Unfinished_Renovation_Yes = Data.PCR_Damage_Unfinished_Renovation_Yes;
                pCRDamageMasterDTO.PCR_Damage_Structural_Damage_Yes = Data.PCR_Damage_Structural_Damage_Yes;
                pCRDamageMasterDTO.PCR_Damage_Excessive_Humidty_Yes = Data.PCR_Damage_Excessive_Humidty_Yes;
                pCRDamageMasterDTO.PCR_Urgent_Damages_Roof_Leak_Yes = Data.PCR_Urgent_Damages_Roof_Leak_Yes;
                pCRDamageMasterDTO.PCR_Urgent_Damages_Roof_Traped_Yes = Data.PCR_Urgent_Damages_Roof_Traped_Yes;
                pCRDamageMasterDTO.PCR_Urgent_Damages_Mold_Damage_Yes = Data.PCR_Urgent_Damages_Mold_Damage_Yes;
                pCRDamageMasterDTO.PCR_Urgent_Damages_SeePage_Yes = Data.PCR_Urgent_Damages_SeePage_Yes;
                pCRDamageMasterDTO.PCR_Urgent_Damages_Flooded_Basement_Yes = Data.PCR_Urgent_Damages_Flooded_Basement_Yes;
                pCRDamageMasterDTO.PCR_Urgent_Damages_Foundation_Cracks_Yes = Data.PCR_Urgent_Damages_Foundation_Cracks_Yes;
                pCRDamageMasterDTO.PCR_Urgent_Damages_Wet_Carpet_Yes = Data.PCR_Urgent_Damages_Wet_Carpet_Yes;
                pCRDamageMasterDTO.PCR_Urgent_Damages_Water_Stains_Yes = Data.PCR_Urgent_Damages_Water_Stains_Yes;
                pCRDamageMasterDTO.PCR_Urgent_Damages_Floors_Safety_Yes = Data.PCR_Urgent_Damages_Floors_Safety_Yes;
                pCRDamageMasterDTO.PCR_Urgent_Damages_Other_Causing_Damage_Yes = Data.PCR_Urgent_Damages_Other_Causing_Damage_Yes;
                pCRDamageMasterDTO.PCR_Urgent_Damages_Other_Safety_Issue_Yes = Data.PCR_Urgent_Damages_Other_Safety_Issue_Yes;
                pCRDamageMasterDTO.PCR_System_Damages_HVAC_System_Damage_Yes = Data.PCR_System_Damages_HVAC_System_Damage_Yes;
                pCRDamageMasterDTO.PCR_System_Damages_Electric_Damage_Yes = Data.PCR_System_Damages_Electric_Damage_Yes;
                pCRDamageMasterDTO.PCR_System_Damages_Plumbing_Damage_Yes = Data.PCR_System_Damages_Plumbing_Damage_Yes;
                pCRDamageMasterDTO.PCR_System_Damages_Uncapped_Wire_Yes = Data.PCR_System_Damages_Uncapped_Wire_Yes;
                pCRDamageMasterDTO.PCR_Damages_FEMA_Damages_Yes = Data.PCR_Damages_FEMA_Damages_Yes;
                pCRDamageMasterDTO.PCR_Damages_FEMA_Neighborhood_Level_Light = Data.PCR_Damages_FEMA_Neighborhood_Level_Light;
                pCRDamageMasterDTO.PCR_Damages_FEMA_Trailer_Present = Data.PCR_Damages_FEMA_Trailer_Present;
                pCRDamageMasterDTO.PCR_Damages_FEMA_Property_Level_Light_Moderate = Data.PCR_Damages_FEMA_Property_Level_Light_Moderate;
                pCRDamageMasterDTO.PCR_Damages_Property_Habitable = Data.PCR_Damages_Property_Habitable;
                pCRDamageMasterDTO.PCR_Damages_Property_Habitable_FEMA_Damage_Cause_By_Wind = Data.PCR_Damages_Property_Habitable_FEMA_Damage_Cause_By_Wind;
                pCRDamageMasterDTO.PCR_Damages_Property_Habitable_FEMA_Damage_Cause_By_Water = Data.PCR_Damages_Property_Habitable_FEMA_Damage_Cause_By_Water;
                pCRDamageMasterDTO.PCR_Damages_Property_Habitable_FEMA_Damage_Cause_By_Fire = Data.PCR_Damages_Property_Habitable_FEMA_Damage_Cause_By_Fire;
                pCRDamageMasterDTO.PCR_Damages_Property_Habitable_FEMA_Damage_Cause_By_Flood = Data.PCR_Damages_Property_Habitable_FEMA_Damage_Cause_By_Flood;
                pCRDamageMasterDTO.PCR_Damages_FEMA_Damage_Estimate = Data.PCR_Damages_FEMA_Damage_Estimate;
                pCRDamageMasterDTO.PCR_Damages_Damage = Data.PCR_Damages_Damage;
                pCRDamageMasterDTO.PCR_Damages_Status = Data.PCR_Damages_Status;
                pCRDamageMasterDTO.PCR_Damages_Cause = Data.PCR_Damages_Cause;
                pCRDamageMasterDTO.PCR_Damages_Int_Ext = Data.PCR_Damages_Int_Ext;
                pCRDamageMasterDTO.PCR_Damages_Building = Data.PCR_Damages_Building;
                pCRDamageMasterDTO.PCR_Damages_Room = Data.PCR_Damages_Room;
                pCRDamageMasterDTO.PCR_Damages_Description = Data.PCR_Damages_Description;
                pCRDamageMasterDTO.PCR_Damages_Qty = Data.PCR_Damages_Qty;
                pCRDamageMasterDTO.PCR_Damages_Estimate = Data.PCR_Damages_Estimate;
                pCRDamageMasterDTO.PCR_Damages_IsActive = Data.PCR_Damages_IsActive;
                pCRDamageMasterDTO.PCR_Damages_IsDelete = Data.PCR_Damages_IsDelete;
                pCRDamageMasterDTO.UserID = Data.UserID;
                pCRDamageMasterDTO.fwo_pkyeId = Data.fwo_pkyeId;
                pCRDamageMasterDTO.Type = Data.Type;



                var AddPRCDamage = await Task.Run(() => pCRDamageMasterData.AddPCR_Damage_Data(pCRDamageMasterDTO));
                return AddPRCDamage;

            }
            catch (Exception ex)
            {
                log.logErrorMessage("PostPCRDamageData---->Start");
                log.logErrorMessage(s);
                log.logErrorMessage("PostPCRDamageData---->End");
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //For PCR Damage Get method
        [Authorize]
        [HttpPost]
        [Route("GetPCRDamageData")]
        public async Task<List<dynamic>> GetPCRDamageData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();


                var Data = JsonConvert.DeserializeObject<PCRDamageMasterDTO>(s);

                PCRDamageMasterDTO pCRDamageMasterDTO = new PCRDamageMasterDTO();

                pCRDamageMasterDTO.PCR_Damage_pkeyId = Data.PCR_Damage_pkeyId;
                pCRDamageMasterDTO.PCR_Damage_WO_Id = Data.PCR_Damage_WO_Id;
                pCRDamageMasterDTO.Type = Data.Type;



                var GetPRCDamage = await Task.Run(() => pCRDamageMasterData.GetPCRDamageDetails(pCRDamageMasterDTO));
                return GetPRCDamage;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        ////For PCR Conveyance Post method
        [Authorize]
        [HttpPost]
        [Route("PostPCRConveyanceData")]
        public async Task<List<dynamic>> PostPCRConveyanceData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();


                var Data = JsonConvert.DeserializeObject<PCR_Conveyance_MasterDTO>(s);
                Data.UserID = LoggedInUSerId;

                PCR_Conveyance_MasterDTO pCR_Conveyance_MasterDTO = new PCR_Conveyance_MasterDTO();


                pCR_Conveyance_MasterDTO.PCR_Conveyance_pkeyID = Data.PCR_Conveyance_pkeyID;
                pCR_Conveyance_MasterDTO.PCR_Conveyance_MasterID = Data.PCR_Conveyance_MasterID;
                pCR_Conveyance_MasterDTO.PCR_Conveyance_Wo_ID = Data.PCR_Conveyance_Wo_ID;
                pCR_Conveyance_MasterDTO.PCR_Conveyance_ValType = Data.PCR_Conveyance_ValType;
                pCR_Conveyance_MasterDTO.PCR_Conveyance_Work_Order_Instruction = Data.PCR_Conveyance_Work_Order_Instruction;
                pCR_Conveyance_MasterDTO.PCR_Conveyance_Secured_Per_Guidelines = Data.PCR_Conveyance_Secured_Per_Guidelines;
                pCR_Conveyance_MasterDTO.PCR_Conveyance_Additional_Damage = Data.PCR_Conveyance_Additional_Damage;
                pCR_Conveyance_MasterDTO.PCR_Conveyance_Bid_On_This_Visit = Data.PCR_Conveyance_Bid_On_This_Visit;
                pCR_Conveyance_MasterDTO.PCR_Conveyance_Need_Maintenance = Data.PCR_Conveyance_Need_Maintenance;
                pCR_Conveyance_MasterDTO.PCR_Conveyance_Broom_Swept_Condition = Data.PCR_Conveyance_Broom_Swept_Condition;
                pCR_Conveyance_MasterDTO.PCR_Conveyance_HUD_Guidelines = Data.PCR_Conveyance_HUD_Guidelines;
                pCR_Conveyance_MasterDTO.PCR_Conveyance_Accidental_Entry = Data.PCR_Conveyance_Accidental_Entry;
                pCR_Conveyance_MasterDTO.PCR_Conveyance_Features_Are_Secure = Data.PCR_Conveyance_Features_Are_Secure;
                pCR_Conveyance_MasterDTO.PCR_Conveyance_In_Place_Operational = Data.PCR_Conveyance_In_Place_Operational;
                pCR_Conveyance_MasterDTO.PCR_Conveyance_Property_Of_Animals = Data.PCR_Conveyance_Property_Of_Animals;
                pCR_Conveyance_MasterDTO.PCR_Conveyance_Intact_Secure = Data.PCR_Conveyance_Intact_Secure;
                pCR_Conveyance_MasterDTO.PCR_Conveyance_Water_Instruction = Data.PCR_Conveyance_Water_Instruction;
                pCR_Conveyance_MasterDTO.PCR_Conveyance_Free_Of_Water = Data.PCR_Conveyance_Free_Of_Water;
                pCR_Conveyance_MasterDTO.PCR_Conveyance_Moisture_has_Eliminated = Data.PCR_Conveyance_Moisture_has_Eliminated;
                pCR_Conveyance_MasterDTO.PCR_Conveyance_Orderdinance = Data.PCR_Conveyance_Orderdinance;
                pCR_Conveyance_MasterDTO.PCR_Conveyance_Uneven = Data.PCR_Conveyance_Uneven;
                pCR_Conveyance_MasterDTO.PCR_Conveyance_Conveyance_Condition = Data.PCR_Conveyance_Conveyance_Condition;
                pCR_Conveyance_MasterDTO.PCR_Conveyance_Damage = Data.PCR_Conveyance_Damage;
                pCR_Conveyance_MasterDTO.PCR_Conveyance_Debris = Data.PCR_Conveyance_Debris;
                pCR_Conveyance_MasterDTO.PCR_Conveyance_Repairs = Data.PCR_Conveyance_Repairs;
                pCR_Conveyance_MasterDTO.PCR_Conveyance_Hazards = Data.PCR_Conveyance_Hazards;
                pCR_Conveyance_MasterDTO.PCR_Conveyance_Other = Data.PCR_Conveyance_Other;
                pCR_Conveyance_MasterDTO.PCR_Conveyance_Describe = Data.PCR_Conveyance_Describe;
                pCR_Conveyance_MasterDTO.PCR_Conveyance_Note = Data.PCR_Conveyance_Note;
                pCR_Conveyance_MasterDTO.PCR_Conveyance_Work_Order_Instruction_Reason = Data.PCR_Conveyance_Work_Order_Instruction_Reason;
                pCR_Conveyance_MasterDTO.PCR_Conveyance_Secured_Per_Guidelines_Reason = Data.PCR_Conveyance_Secured_Per_Guidelines_Reason;
                pCR_Conveyance_MasterDTO.PCR_Conveyance_HUD_Guidelines_Reasponce = Data.PCR_Conveyance_HUD_Guidelines_Reasponce;
                pCR_Conveyance_MasterDTO.PCR_Conveyance_IsActive = Data.PCR_Conveyance_IsActive;
                pCR_Conveyance_MasterDTO.PCR_Conveyance_IsDelete = Data.PCR_Conveyance_IsDelete;
                pCR_Conveyance_MasterDTO.UserID = Data.UserID;
                pCR_Conveyance_MasterDTO.fwo_pkyeId = Data.fwo_pkyeId;
                pCR_Conveyance_MasterDTO.Type = Data.Type;
                pCR_Conveyance_MasterDTO.PCR_Conveyance_Shrubs_or_tree = Data.PCR_Conveyance_Shrubs_or_tree;




                var AddConveyance = await Task.Run(() => pCR_Conveyance_MasterData.AddPCR_Conveyance_Data(pCR_Conveyance_MasterDTO));
                return AddConveyance;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        //For PCR Conveyance Get method
        [Authorize]
        [HttpPost]
        [Route("GetPCRConveyanceData")]
        public async Task<List<dynamic>> GetPCRConveyanceData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();


                var Data = JsonConvert.DeserializeObject<PCR_Conveyance_MasterDTO>(s);

                PCR_Conveyance_MasterDTO pCR_Conveyance_MasterDTO = new PCR_Conveyance_MasterDTO();


                pCR_Conveyance_MasterDTO.PCR_Conveyance_pkeyID = Data.PCR_Conveyance_pkeyID;
                pCR_Conveyance_MasterDTO.PCR_Conveyance_Wo_ID = Data.PCR_Conveyance_Wo_ID;
                pCR_Conveyance_MasterDTO.Type = Data.Type;



                var GetConveyance = await Task.Run(() => pCR_Conveyance_MasterData.GetPCRConveyanceDetails(pCR_Conveyance_MasterDTO));
                return GetConveyance;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }



        //For PCR Roof Add method
        [Authorize]
        [HttpPost]
        [Route("PostPCRRoofData")]
        public async Task<List<dynamic>> PostPCRRoofData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();


                var Data = JsonConvert.DeserializeObject<PCR_Roof_MasterDTO>(s);
                Data.UserID = LoggedInUSerId;

                PCR_Roof_MasterDTO pCR_Roof_MasterDTO = new PCR_Roof_MasterDTO();

                pCR_Roof_MasterDTO.PCR_Roof_pkeyId = Data.PCR_Roof_pkeyId;
                pCR_Roof_MasterDTO.PCR_Roof_MasterId = Data.PCR_Roof_MasterId;
                pCR_Roof_MasterDTO.PCR_Roof_WO_Id = Data.PCR_Roof_WO_Id;
                pCR_Roof_MasterDTO.PCR_Roof_ValType = Data.PCR_Roof_ValType;
                pCR_Roof_MasterDTO.PCR_Roof_Roof_Shape_Pitched_Roof = Data.PCR_Roof_Roof_Shape_Pitched_Roof;
                pCR_Roof_MasterDTO.PCR_Roof_Leak = Data.PCR_Roof_Leak;
                pCR_Roof_MasterDTO.PCR_Roof_Leak_Case = Data.PCR_Roof_Leak_Case;
                pCR_Roof_MasterDTO.PCR_Roof_Leak_Other = Data.PCR_Roof_Leak_Other;
                pCR_Roof_MasterDTO.PCR_Roof_Location_Dimensions = Data.PCR_Roof_Location_Dimensions;
                pCR_Roof_MasterDTO.PCR_Roof_Roof_Damage = Data.PCR_Roof_Roof_Damage;//new
                pCR_Roof_MasterDTO.PCR_Roof_Water_Strains = Data.PCR_Roof_Water_Strains;
                pCR_Roof_MasterDTO.PCR_Roof_Water_Strains_Case = Data.PCR_Roof_Water_Strains_Case;
                pCR_Roof_MasterDTO.PCR_Roof_Water_Strains_Dimension = Data.PCR_Roof_Water_Strains_Dimension;
                pCR_Roof_MasterDTO.PCR_Roof_Did_You_Perform_Roof_Repair = Data.PCR_Roof_Did_You_Perform_Roof_Repair;
                pCR_Roof_MasterDTO.PCR_Roof_Bid_To_Repair = Data.PCR_Roof_Bid_To_Repair;
                pCR_Roof_MasterDTO.PCR_Roof_Did_You_Perform_Emergency_Traping = Data.PCR_Roof_Did_You_Perform_Emergency_Traping;
                pCR_Roof_MasterDTO.PCR_Roof_Explain_Bid_Trap = Data.PCR_Roof_Explain_Bid_Trap;
                pCR_Roof_MasterDTO.PCR_Roof_Bid_To_Trap_Dimension_size_x = Data.PCR_Roof_Bid_To_Trap_Dimension_size_x;
                pCR_Roof_MasterDTO.PCR_Roof_Bid_To_Trap_Dimension_size_y = Data.PCR_Roof_Bid_To_Trap_Dimension_size_y;
                pCR_Roof_MasterDTO.PCR_Roof_Bid_To_Trap_Location = Data.PCR_Roof_Bid_To_Trap_Location;
                pCR_Roof_MasterDTO.PCR_Roof_Bid_To_Trap_Description = Data.PCR_Roof_Bid_To_Trap_Description;
                pCR_Roof_MasterDTO.PCR_Roof_Bid_To_Trap_Bid_Amount = Data.PCR_Roof_Bid_To_Trap_Bid_Amount;

                pCR_Roof_MasterDTO.PCR_Roof_Bid_To_Tar_Patch_Dimension_size_x = Data.PCR_Roof_Bid_To_Tar_Patch_Dimension_size_x;
                pCR_Roof_MasterDTO.PCR_Roof_Bid_To_Tar_Patch_Dimension_size_y = Data.PCR_Roof_Bid_To_Tar_Patch_Dimension_size_y;
                pCR_Roof_MasterDTO.PCR_Roof_Bid_To_Tar_Patch_Location = Data.PCR_Roof_Bid_To_Tar_Patch_Location;
                pCR_Roof_MasterDTO.PCR_Roof_Bid_To_Tar_Patch_dias = Data.PCR_Roof_Bid_To_Tar_Patch_dias;
                pCR_Roof_MasterDTO.PCR_Roof_Bid_To_Tar_Patch_Bid_Amount = Data.PCR_Roof_Bid_To_Tar_Patch_Bid_Amount;

                pCR_Roof_MasterDTO.PCR_Roof_Bid_To_Replace = Data.PCR_Roof_Bid_To_Replace;
                pCR_Roof_MasterDTO.PCR_Roof_Reason_Cant_Repair_Due_To = Data.PCR_Roof_Reason_Cant_Repair_Due_To;
                pCR_Roof_MasterDTO.PCR_Roof_Reason_Cant_Repair_Due_To_TEXT = Data.PCR_Roof_Reason_Cant_Repair_Due_To_TEXT;//new
                pCR_Roof_MasterDTO.PCR_Roof_Reason_Preventive_Due_To = Data.PCR_Roof_Reason_Preventive_Due_To;

                pCR_Roof_MasterDTO.PCR_Roof_Reason_Leaking = Data.PCR_Roof_Reason_Leaking;
                pCR_Roof_MasterDTO.PCR_Roof_Reason_Other = Data.PCR_Roof_Reason_Other;
                pCR_Roof_MasterDTO.PCR_Roof_Bid_To_Description = Data.PCR_Roof_Bid_To_Description;
                pCR_Roof_MasterDTO.PCR_Roof_Leak_Location_Dimension = Data.PCR_Roof_Leak_Location_Dimension;

                pCR_Roof_MasterDTO.PCR_Roof_Bid_To_Location_Entire_Roof = Data.PCR_Roof_Bid_To_Location_Entire_Roof;
                pCR_Roof_MasterDTO.PCR_Roof_Bid_To_Location_Front = Data.PCR_Roof_Bid_To_Location_Front;
                pCR_Roof_MasterDTO.PCR_Roof_Bid_To_Location_Back = Data.PCR_Roof_Bid_To_Location_Back;
                pCR_Roof_MasterDTO.PCR_Roof_Bid_To_Location_Left_Side = Data.PCR_Roof_Bid_To_Location_Left_Side;
                pCR_Roof_MasterDTO.PCR_Roof_Bid_To_Location_Right_Side = Data.PCR_Roof_Bid_To_Location_Right_Side;
                pCR_Roof_MasterDTO.PCR_Roof_Building_House = Data.PCR_Roof_Building_House;
                pCR_Roof_MasterDTO.PCR_Roof_Building_Garage = Data.PCR_Roof_Building_Garage;
                pCR_Roof_MasterDTO.PCR_Roof_Building_Out_Building = Data.PCR_Roof_Building_Out_Building;
                pCR_Roof_MasterDTO.PCR_Roof_Building_Pool_House = Data.PCR_Roof_Building_Pool_House;
                pCR_Roof_MasterDTO.PCR_Roof_Building_Shed = Data.PCR_Roof_Building_Shed;
                pCR_Roof_MasterDTO.PCR_Roof_Building_Bam = Data.PCR_Roof_Building_Bam;


                pCR_Roof_MasterDTO.PCR_Roof_Item_Used_Roof_Type = Data.PCR_Roof_Item_Used_Roof_Type;
                pCR_Roof_MasterDTO.PCR_Roof_Item_Used_DRD = Data.PCR_Roof_Item_Used_DRD;
                pCR_Roof_MasterDTO.PCR_Roof_Item_Used_Size = Data.PCR_Roof_Item_Used_Size;
                pCR_Roof_MasterDTO.PCR_Roof_Item_Used_Amount = Data.PCR_Roof_Item_Used_Amount;


                pCR_Roof_MasterDTO.PCR_Roof_Item_Used_Felt_Type = Data.PCR_Roof_Item_Used_Felt_Type;
                pCR_Roof_MasterDTO.PCR_Roof_Item_Used_Felt_Type_DRD = Data.PCR_Roof_Item_Used_Felt_Type_DRD;
                pCR_Roof_MasterDTO.PCR_Roof_Item_Used_Felt_Type_Size = Data.PCR_Roof_Item_Used_Felt_Type_Size;
                pCR_Roof_MasterDTO.PCR_Roof_Item_Used_Felt_Type_Amount = Data.PCR_Roof_Item_Used_Felt_Type_Amount;
                pCR_Roof_MasterDTO.PCR_Roof_Item_Used_Sheathing_DRD = Data.PCR_Roof_Item_Used_Sheathing_DRD;
                pCR_Roof_MasterDTO.PCR_Roof_Item_Used_Sheathing_Size = Data.PCR_Roof_Item_Used_Sheathing_Size;
                pCR_Roof_MasterDTO.PCR_Roof_Item_Used_Sheathing_Amount = Data.PCR_Roof_Item_Used_Sheathing_Amount;

                pCR_Roof_MasterDTO.PCR_Roof_Item_Used_Deck_Thikness = Data.PCR_Roof_Item_Used_Deck_Thikness;
                pCR_Roof_MasterDTO.PCR_Roof_Item_Used_Deck_Thikness_DRD = Data.PCR_Roof_Item_Used_Deck_Thikness_DRD;

                pCR_Roof_MasterDTO.PCR_Roof_Item_Used_Drip_Edge = Data.PCR_Roof_Item_Used_Drip_Edge;
                pCR_Roof_MasterDTO.PCR_Roof_Item_Used_Drip_Edge_Size = Data.PCR_Roof_Item_Used_Drip_Edge_Size;
                pCR_Roof_MasterDTO.PCR_Roof_Item_Used_Drip_Edge_Amount = Data.PCR_Roof_Item_Used_Drip_Edge_Amount;

                pCR_Roof_MasterDTO.PCR_Roof_Item_Used_Ice_Water_Barrier = Data.PCR_Roof_Item_Used_Ice_Water_Barrier;
                pCR_Roof_MasterDTO.PCR_Roof_Item_Used_Ice_Water_Barrier_Size = Data.PCR_Roof_Item_Used_Ice_Water_Barrier_Size;
                pCR_Roof_MasterDTO.PCR_Roof_Item_Used_Ice_Water_Barrier_Amount = Data.PCR_Roof_Item_Used_Ice_Water_Barrier_Amount;

                pCR_Roof_MasterDTO.PCR_Roof_Item_Used_No_Of_Vents = Data.PCR_Roof_Item_Used_No_Of_Vents;
                pCR_Roof_MasterDTO.PCR_Roof_Item_Used_No_Of_Vents_Text = Data.PCR_Roof_Item_Used_No_Of_Vents_Text;
                pCR_Roof_MasterDTO.PCR_Roof_Item_Used_No_Of_Vents_Amount = Data.PCR_Roof_Item_Used_No_Of_Vents_Amount;

                pCR_Roof_MasterDTO.PCR_Roof_Item_Used_Roof_Debris = Data.PCR_Roof_Item_Used_Roof_Debris;
                pCR_Roof_MasterDTO.PCR_Roof_Item_Used_Roof_Debris_Size = Data.PCR_Roof_Item_Used_Roof_Debris_Size;
                pCR_Roof_MasterDTO.PCR_Roof_Item_Used_Roof_Debris_Amount = Data.PCR_Roof_Item_Used_Roof_Debris_Amount;

                pCR_Roof_MasterDTO.PCR_Roof_Item_Used_Dempster_Rental = Data.PCR_Roof_Item_Used_Dempster_Rental;
                pCR_Roof_MasterDTO.PCR_Roof_Item_Used_Dempster_Rental_Size = Data.PCR_Roof_Item_Used_Dempster_Rental_Size;
                pCR_Roof_MasterDTO.PCR_Roof_Item_Used_Dempster_Rental_Amount = Data.PCR_Roof_Item_Used_Dempster_Rental_Amount;
                pCR_Roof_MasterDTO.PCR_Bid_Amount = Data.PCR_Bid_Amount;
                pCR_Roof_MasterDTO.PCR_Roof_IsActive = Data.PCR_Roof_IsActive;
                pCR_Roof_MasterDTO.PCR_Roof_IsDelete = Data.PCR_Roof_IsDelete;
                pCR_Roof_MasterDTO.UserID = Data.UserID;
                pCR_Roof_MasterDTO.fwo_pkyeId = Data.fwo_pkyeId;
                pCR_Roof_MasterDTO.Type = Data.Type;




                var AddRoof = await Task.Run(() => pCR_Roof_MasterData.AddPCR_Roof_Data(pCR_Roof_MasterDTO));
                return AddRoof;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        //For PCR Roof Get method
        [Authorize]
        [HttpPost]
        [Route("GetPCRRoofData")]
        public async Task<List<dynamic>> GetPCRRoofData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();


                var Data = JsonConvert.DeserializeObject<PCR_Roof_MasterDTO>(s);

                PCR_Roof_MasterDTO pCR_Roof_MasterDTO = new PCR_Roof_MasterDTO();

                pCR_Roof_MasterDTO.PCR_Roof_pkeyId = Data.PCR_Roof_pkeyId;
                pCR_Roof_MasterDTO.PCR_Roof_WO_Id = Data.PCR_Roof_WO_Id;
                pCR_Roof_MasterDTO.Type = Data.Type;



                var GetRoof = await Task.Run(() => pCR_Roof_MasterData.GetPCRRoofDetails(pCR_Roof_MasterDTO));
                return GetRoof;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //for grass cut pcr 
        [Authorize]
        [HttpPost]
        [Route("PostPCRGrassCutData")]
        public async Task<List<dynamic>> PostPCRGrassCutData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<PCR_Grass_Cut_MasterDTO>(s);

                Data.UserID = LoggedInUSerId;

                var addGrass = await Task.Run(() => pCR_Grass_Cut_MasterData.AddPCR_Grass_Cut_Masterdata(Data));
                return addGrass;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("GetPCRGrassCutData")]
        public async Task<List<dynamic>> GetPCRGrassCutData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<PCR_Grass_Cut_MasterDTO>(s);

                Data.UserID = LoggedInUSerId;

                var GetGrass = await Task.Run(() => pCR_Grass_Cut_MasterData.GetPCR_Grass_Cut_MasterDetails(Data));
                return GetGrass;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("GetPCRCYPREXXFORMSMASTERData")]
        public async Task<List<dynamic>> GetPCRCYPREXX_FORMS_MASTERData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<PCR_CYPREXX_FORMS_MASTER_DTO>(s);

                Data.UserID = LoggedInUSerId;

                var GetPcr = await Task.Run(() => pCR_CYPREXX_FORMS_MASTER.GetPCRCYPREXXFORMSMasterDetails(Data));
                return GetPcr;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [Authorize]
        [HttpPost]
        [Route("SavePcrCyperxxFORMSMASTERData")]
        public async Task<List<dynamic>> SavePcrCyperxx_FORMS_MASTERData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<PCR_CYPREXX_FORMS_MASTER_DTO>(s);

                Data.UserID = LoggedInUSerId;

                var GetPcr = await Task.Run(() => pCR_CYPREXX_FORMS_MASTER.AddPCRCYPREXXFORMSMasterData(Data));
                return GetPcr;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("GetMsiGrassPcrFORMSMASTERData")]
        public async Task<List<dynamic>> GetMsiGrassPcr_FORMS_MASTERData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<MSI_GrassPcr_Forms_Master_DTO>(s);

                Data.UserID = LoggedInUSerId;

                var GetPcr = await Task.Run(() => mSI_GrassPcr_Forms_MasterData.GetMsiGrassPcrFORMSMasterDetails(Data));
                return GetPcr;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [Authorize]
        [HttpPost]
        [Route("SaveMsiGrassPcrFormsMasterData")]
        public async Task<List<dynamic>> SaveMsiGrassPcr_FORMS_MasterData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<MSI_GrassPcr_Forms_Master_DTO>(s);

                Data.UserID = LoggedInUSerId;

                var GetPcr = await Task.Run(() => mSI_GrassPcr_Forms_MasterData.AddMsiGrassPCRCFORMSMasterData(Data));
                return GetPcr;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        [Authorize]
        [HttpPost]
        [Route("GetCyprexxGrassCheckListData")]
        public async Task<List<dynamic>> GetCyprexxGrassCheckListData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<PCR_Cyprexx_Grass_Checklist_DTO>(s);

                Data.UserID = LoggedInUSerId;

                var GetPcr = await Task.Run(() => pCR_Cyprexx_Grass_Checklist_Data.GetCyprexxGrassCheckListDetails(Data));
                return GetPcr;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [Authorize]
        [HttpPost]
        [Route("PostCyprexxGrassCheckData")]
        public async Task<List<dynamic>> PostCyprexxGrassCheckData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<PCR_Cyprexx_Grass_Checklist_DTO>(s);

                Data.UserID = LoggedInUSerId;

                var GetPcr = await Task.Run(() => pCR_Cyprexx_Grass_Checklist_Data.PostCyprexxGrassCheckData(Data));
                return GetPcr;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }




        [Authorize]
        [HttpPost]
        [Route("GetCyprexxWinterizationPressure")]
        public async Task<List<dynamic>> GetCyprexxWinterizationPressure()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<PCR_CyprexxWinterizationPressure_DTO>(s);

                Data.UserID = LoggedInUSerId;

                var GetPcr = await Task.Run(() => pCR_CyprexxWinterizationPressureData.GetCyprexxWinterizationPressureDetails(Data));
                return GetPcr;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [Authorize]
        [HttpPost]
        [Route("PostCyprexxWinterizationPressure")]
        public async Task<List<dynamic>> PostCyprexxWinterizationPressure()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<PCR_CyprexxWinterizationPressure_DTO>(s);

                Data.UserID = LoggedInUSerId;

                var GetPcr = await Task.Run(() => pCR_CyprexxWinterizationPressureData.PostCyprexxWinterizationPressure(Data));
                return GetPcr;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }



        [Authorize]
        [HttpPost]
        [Route("GetCyprexxUniversalDamageChecklist")]
        public async Task<List<dynamic>> GetCyprexxUniversalDamageChecklist()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<PCR_Cyprexx_Universal_Damage_DTO>(s);

                Data.UserID = LoggedInUSerId;

                var GetPcr = await Task.Run(() => cyprexxUniversalDamageChecklist_Data.GetCyprexxUniversalDamageChecklistDetails(Data));
                return GetPcr;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [Authorize]
        [HttpPost]
        [Route("PostCyprexxUniversalDamageChecklist")]
        public async Task<List<dynamic>> PostCyprexxUniversalDamageChecklist()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<PCR_Cyprexx_Universal_Damage_DTO>(s);

                Data.UserID = LoggedInUSerId;

                var GetPcr = await Task.Run(() => cyprexxUniversalDamageChecklist_Data.PostCyprexxUniversalDamageChecklist(Data));
                return GetPcr;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }



        [Authorize]
        [HttpPost]
        [Route("GetMsiPreservationPcrForm")]
        public async Task<List<dynamic>> GetMsiPreservationPcrForm()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<MSI_PCR_PreservationFormMaster_DTO>(s);

                Data.UserID = LoggedInUSerId;

                var GetPcr = await Task.Run(() => mSI_PCR_PreservationFormMaster_Data.GetMsiPreservationPcrFormDetails(Data));
                return GetPcr;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [Authorize]
        [HttpPost]
        [Route("PostMsiPreservationPcrForm")]
        public async Task<List<dynamic>> PostMsiPreservationPcrForm()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<MSI_PCR_PreservationFormMaster_DTO>(s);

                Data.UserID = LoggedInUSerId;

                var GetPcr = await Task.Run(() => mSI_PCR_PreservationFormMaster_Data.PostMsiPreservationPcrForm(Data));
                return GetPcr;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }



        [Authorize]
        [HttpPost]
        [Route("GetServiceLinkForm")]
        public async Task<List<dynamic>> GetServiceLinkForm()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<Service_Link_Form_Master_DTO>(s);

                Data.UserID = LoggedInUSerId;

                var GetPcr = await Task.Run(() => service_Link_Form_Master_Data.GetServiceLinkFormDetails(Data));
                return GetPcr;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [Authorize]
        [HttpPost]
        [Route("PostServiceLinkForm")]
        public async Task<List<dynamic>> PostServiceLinkForm()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<Service_Link_Form_Master_DTO>(s);

                Data.UserID = LoggedInUSerId;

                var GetPcr = await Task.Run(() => service_Link_Form_Master_Data.PostServiceLinkForm(Data));
                return GetPcr;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        [Authorize]
        [HttpPost]
        [Route("GetMCSFormMaster")]
        public async Task<List<dynamic>> GetMCSFormMaster()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<PCR_MCS_Forms_Master_DTO>(s);

                Data.UserID = LoggedInUSerId;

                var GetPcr = await Task.Run(() => pCR_MCS_Forms_Master_Data.GetMCSFormMasterDetails(Data));
                return GetPcr;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [Authorize]
        [HttpPost]
        [Route("PostMCSFormMaster")]
        public async Task<List<dynamic>> PostMCSFormMaster()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<PCR_MCS_Forms_Master_DTO>(s);

                Data.UserID = LoggedInUSerId;

                var GetPcr = await Task.Run(() => pCR_MCS_Forms_Master_Data.PostMCSFormMaster(Data));
                return GetPcr;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        [Authorize]
        [HttpPost]
        [Route("GetPCRInspectionForm")]
        public async Task<List<dynamic>> GetPCRInspectionForm()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<PCR_Five_Brothers_Inspection_Form_DTO>(s);

                Data.UserID = LoggedInUSerId;

                var GetPcr = await Task.Run(() => PCR_Five_Brothers_Inspection_Form.GetPCRInspectionFormDetails(Data));
                return GetPcr;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [Authorize]
        [HttpPost]
        [Route("PostPCRInspectionForm")]
        public async Task<List<dynamic>> PostPCRInspectionForm()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<PCR_Five_Brothers_Inspection_Form_DTO>(s);

                Data.UserID = LoggedInUSerId;

                var GetPcr = await Task.Run(() => PCR_Five_Brothers_Inspection_Form.PostPCRInspectionForm(Data));
                return GetPcr;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        [Authorize]
        [HttpPost]
        [Route("GetMCSGrassCutFormMaster")]
        public async Task<List<dynamic>> GetMCSGrassCutFormMaster()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<PCR_MCS_Grass_Cut_Form_Master_DTO>(s);

                Data.UserID = LoggedInUSerId;

                var GetPcr = await Task.Run(() => pCR_MCS_Grass_Cut_Form_Master_Data.GetMCSGrassCutFormMasterDetails(Data));
                return GetPcr;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [Authorize]
        [HttpPost]
        [Route("PostMCSGrassCutFormMaster")]
        public async Task<List<dynamic>> PostMCSGrassCutFormMaster()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<PCR_MCS_Grass_Cut_Form_Master_DTO>(s);

                Data.UserID = LoggedInUSerId;

                var GetPcr = await Task.Run(() => pCR_MCS_Grass_Cut_Form_Master_Data.PostMCSGrassCutFormMaster(Data));
                return GetPcr;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        [Authorize]
        [HttpPost]
        [Route("PostMcsMaintenanceVendorChecklistFormMaster")]
        public async Task<List<dynamic>> PostMcsMaintenanceVendorChecklistFormMaster()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<PCR_MCS_Maintenance_Vendor_Checklist_DTO>(s);

                Data.UserID = LoggedInUSerId;

                var GetPcr = await Task.Run(() => pCR_MCS_Maintenance_Vendor_Checklist_Data.PostMcsMaintenanceVendorChecklistFormMaster(Data));
                return GetPcr;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [Authorize]
        [HttpPost]
        [Route("GetMcsMaintenanceVendorChecklistFormMaster")]
        public async Task<List<dynamic>> GetMcsMaintenanceVendorChecklistFormMaster()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<PCR_MCS_Maintenance_Vendor_Checklist_DTO>(s);

                Data.UserID = LoggedInUSerId;

                var GetPcr = await Task.Run(() => pCR_MCS_Maintenance_Vendor_Checklist_Data.GetMcsMaintenanceVendorChecklistFormMasterDetail(Data));
                return GetPcr;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("Post_NRF_PropertyConditionReportData")]
        public async Task<List<dynamic>> PostPropertyConditionReport_Data()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();


                var Data = JsonConvert.DeserializeObject<PropertyConditionReport_DTO>(s);

                PropertyConditionReport_DTO propertyInfoMasterDTO = new PropertyConditionReport_DTO();
                Data.UserID = LoggedInUSerId;
                propertyInfoMasterDTO.PCR_PkeyID = Data.PCR_PkeyID;
                propertyInfoMasterDTO.PCR_WO_ID = Data.PCR_WO_ID;
                propertyInfoMasterDTO.PCR_UserID = Data.UserID;
                propertyInfoMasterDTO.PCR_General = Data.PCR_General;
                propertyInfoMasterDTO.PCR_Utilities = Data.PCR_Utilities;
                propertyInfoMasterDTO.PCR_Securing = Data.PCR_Securing;
                propertyInfoMasterDTO.PCR_Winterization = Data.PCR_Winterization;
                propertyInfoMasterDTO.PCR_Bording = Data.PCR_Bording;
                propertyInfoMasterDTO.PCR_Debris = Data.PCR_Debris;
                propertyInfoMasterDTO.PCR_Roof = Data.PCR_Roof;
                propertyInfoMasterDTO.PCR_Moisture = Data.PCR_Moisture;
                propertyInfoMasterDTO.PCR_Yard = Data.PCR_Yard;
                propertyInfoMasterDTO.PCR_Damages = Data.PCR_Damages;
                propertyInfoMasterDTO.PCR_Others = Data.PCR_Others;
                propertyInfoMasterDTO.PCR_PhotoCheckList = Data.PCR_PhotoCheckList;
                propertyInfoMasterDTO.PCR_Summary = Data.PCR_Summary;
                propertyInfoMasterDTO.PCR_IsActive = Data.PCR_IsActive;
                propertyInfoMasterDTO.PCR_IsDelete = Data.PCR_IsDelete;
                propertyInfoMasterDTO.Type = Data.Type;
                propertyInfoMasterDTO.UserID = LoggedInUSerId;
                propertyInfoMasterDTO.fwo_pkyeId = Data.fwo_pkyeId;
                var returnVal = await Task.Run(() => propertyCondition.CreatePropertyConditionReport_Data(propertyInfoMasterDTO));
                return returnVal;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //For Property Information Get method
        [Authorize]
        [HttpPost]
        [Route("Get_NRF_PropertyConditionReportData")]
        public async Task<List<dynamic>> GetPropertyConditionReport_Data()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();


                var Data = JsonConvert.DeserializeObject<PropertyConditionReport_DTO>(s);

                PropertyConditionReport_DTO propertyInfoMasterDTO = new PropertyConditionReport_DTO();

                propertyInfoMasterDTO.PCR_PkeyID = Data.PCR_PkeyID;
                propertyInfoMasterDTO.PCR_WO_ID = Data.PCR_WO_ID;
                propertyInfoMasterDTO.UserID = LoggedInUSerId;
                propertyInfoMasterDTO.Type = Data.Type;

                var GetProperty = await Task.Run(() => propertyCondition.GetPropertyConditionReportDetails(propertyInfoMasterDTO));
                return GetProperty;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        [Authorize]
        [HttpPost]
        [Route("PostNfrDumpReceiptFormMasterData")]
        public async Task<List<dynamic>> PostNfrDumpReceiptFormMasterData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();


                var Data = JsonConvert.DeserializeObject<PCR_NFR_Dump_Receipt_Form_DTO>(s);
                Data.UserID = LoggedInUSerId;

                var returnVal = await Task.Run(() => _NFR_Dump_Receipt_Form_Data.CreateUpdate_Nfr_Dump_ReceiptData(Data));
                return returnVal;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //For Property Information Get method
        [Authorize]
        [HttpPost]
        [Route("GetNfrDumpReceiptFormMasterData")]
        public async Task<List<dynamic>> GetNfrDumpReceiptFormMasterData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();


                var Data = JsonConvert.DeserializeObject<PCR_NFR_Dump_Receipt_Form_DTO>(s);
                Data.UserID = LoggedInUSerId;

                var GetProperty = await Task.Run(() => _NFR_Dump_Receipt_Form_Data.GetNfr_Dump_ReceipDetails(Data));
                return GetProperty;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("PostCyprexxJobDocumentationChecklist")]
        public async Task<List<dynamic>> PostCyprexxJobDocumentationChecklist()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();


                var Data = JsonConvert.DeserializeObject<CyprexxJobDocumentationChecklist_DTO>(s);
                Data.UserID = LoggedInUSerId;

                var returnVal = await Task.Run(() => _CyprexxJobDocumentationChecklist_Data.CreateUpdate_CyprexxJobDocumentationChecklist(Data));
                return returnVal;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //For Property Information Get method
        [Authorize]
        [HttpPost]
        [Route("GetCyprexxJobDocumentationChecklist")]
        public async Task<List<dynamic>> GetCyprexxJobDocumentationChecklist()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();


                var Data = JsonConvert.DeserializeObject<CyprexxJobDocumentationChecklist_DTO>(s);
                Data.UserID = LoggedInUSerId;

                var GetProperty = await Task.Run(() => _CyprexxJobDocumentationChecklist_Data.GetCyprexxJobDocumentationChecklistData(Data));
                return GetProperty;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("PostInitialPropertyInspection")]
        public async Task<List<dynamic>> PostInitialPropertyInspection()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<PCR_InitialPropertyInspection_DTO>(s);

                Data.UserID = LoggedInUSerId;

                var GetPcr = await Task.Run(() => pCR_InitialPropertyInspection_Data.CreateUpdatePCRInitialPropertyInspection(Data));
                return GetPcr;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [Authorize]
        [HttpPost]
        [Route("GetInitialPropertyInspection")]
        public async Task<List<dynamic>> GetInitialPropertyInspection()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<PCR_InitialPropertyInspection_DTO>(s);

                Data.UserID = LoggedInUSerId;

                var GetPcr = await Task.Run(() => pCR_InitialPropertyInspection_Data.GetInitialPropertyInspectionDetails(Data));
                return GetPcr;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        //PropertyInfoMasterData propertyInfoMasterData = new PropertyInfoMasterData();
        //PCR_Appliance_Data pCR_Appliance_Data = new PCR_Appliance_Data();
        //
        //
        //
        //
        //PCR_FiveBrotherData pCR_FiveBrotherData = new PCR_FiveBrotherData();
        //PCR_Securing_MasterData pCR_Securing_MasterData = new PCR_Securing_MasterData();
        //
        //PCR_Violation_MasterData pCR_Violation_MasterData = new PCR_Violation_MasterData();
        //PCR_WinterizationData pCR_WinterizationData = new PCR_WinterizationData();
        //
        //


        ////For Property Information Post method
        //[HttpPost]
        //[Route("PostPropertyInfoData")]
        //public async Task<List<dynamic>> PostPropertyInfoData()
        //{
        //    List<dynamic> objdynamicobj = new List<dynamic>();

        //    try
        //    {
        //        System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
        //        System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
        //        System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
        //        string s = reader.ReadToEnd();


        //        var Data = JsonConvert.DeserializeObject<PropertyInfoMasterDTO>(s);

        //        PropertyInfoMasterDTO propertyInfoMasterDTO = new PropertyInfoMasterDTO();

        //        propertyInfoMasterDTO.PCR_Pro_PkeyID = Data.PCR_Pro_PkeyID;
        //        propertyInfoMasterDTO.PCR_Prop_WO_ID = Data.PCR_Prop_WO_ID;
        //        propertyInfoMasterDTO.PCR_Prop_MasterID = Data.PCR_Prop_MasterID;
        //        propertyInfoMasterDTO.PCR_Prop_ValType = Data.PCR_Prop_ValType;
        //        propertyInfoMasterDTO.PCR_Prop_ForSale = Data.PCR_Prop_ForSale;
        //        propertyInfoMasterDTO.PCR_Prop_Sold = Data.PCR_Prop_Sold;
        //        propertyInfoMasterDTO.PCR_Prop_Broker_Phone = Data.PCR_Prop_Broker_Phone;
        //        propertyInfoMasterDTO.PCR_Prop_Maintained_Not_Maintained = Data.PCR_Prop_Maintained_Not_Maintained;
        //        propertyInfoMasterDTO.PCR_Prop_Maintained_By_Broker = Data.PCR_Prop_Maintained_By_Broker;
        //        propertyInfoMasterDTO.PCR_Prop_Maintained_ByBroker_HOA = Data.PCR_Prop_Maintained_ByBroker_HOA;
        //        propertyInfoMasterDTO.PCR_Prop_Maintained_ByHOA = Data.PCR_Prop_Maintained_ByHOA;
        //        propertyInfoMasterDTO.PCR_Prop_Maintained_ByOther = Data.PCR_Prop_Maintained_ByOther;
        //        propertyInfoMasterDTO.PCR_Prop_Maintained_Items_Utilities = Data.PCR_Prop_Maintained_Items_Utilities;
        //        propertyInfoMasterDTO.PCR_Prop_Maintained_Items_Grass = Data.PCR_Prop_Maintained_Items_Grass;
        //        propertyInfoMasterDTO.PCR_Prop_Maintained_Items_Snow_Removal = Data.PCR_Prop_Maintained_Items_Snow_Removal;
        //        propertyInfoMasterDTO.PCR_Prop_Maintained_Items_Interior_Repaiars = Data.PCR_Prop_Maintained_Items_Interior_Repaiars;
        //        propertyInfoMasterDTO.PCR_Prop_Maintained_Items_Exterior_Repairs = Data.PCR_Prop_Maintained_Items_Exterior_Repairs;
        //        propertyInfoMasterDTO.PCR_Prop_Active_Listing = Data.PCR_Prop_Active_Listing;
        //        propertyInfoMasterDTO.PCR_Prop_Basement_Present = Data.PCR_Prop_Basement_Present;
        //        propertyInfoMasterDTO.PCR_Prop_Out_Buildings_Garges = Data.PCR_Prop_Out_Buildings_Garges;
        //        propertyInfoMasterDTO.PCR_Prop_Bams = Data.PCR_Prop_Bams;
        //        propertyInfoMasterDTO.PCR_Prop_Sheds = Data.PCR_Prop_Sheds;
        //        propertyInfoMasterDTO.PCR_Prop_Pool_House = Data.PCR_Prop_Pool_House;
        //        propertyInfoMasterDTO.PCR_Prop_Carports = Data.PCR_Prop_Carports;
        //        propertyInfoMasterDTO.PCR_Prop_Other_Building = Data.PCR_Prop_Other_Building;
        //        propertyInfoMasterDTO.PCR_Prop_Property_Type_Vacant_Land = Data.PCR_Prop_Property_Type_Vacant_Land;
        //        propertyInfoMasterDTO.PCR_Prop_Property_Type_Single_Family = Data.PCR_Prop_Property_Type_Single_Family;
        //        propertyInfoMasterDTO.PCR_Prop_Property_Type_Multi_Family = Data.PCR_Prop_Property_Type_Multi_Family;
        //        propertyInfoMasterDTO.PCR_Prop_Property_Type_Mobile_Home = Data.PCR_Prop_Property_Type_Mobile_Home;
        //        propertyInfoMasterDTO.PCR_Prop_Property_Type_Condo = Data.PCR_Prop_Property_Type_Condo;
        //        propertyInfoMasterDTO.PCR_Prop_Permit_Required = Data.PCR_Prop_Permit_Required;
        //        propertyInfoMasterDTO.PCR_Prop_Condo_Association_Property = Data.PCR_Prop_Condo_Association_Property;
        //        propertyInfoMasterDTO.PCR_Prop_No_Of_Unit = Data.PCR_Prop_No_Of_Unit;
        //        propertyInfoMasterDTO.PCR_Prop_Common_Entry = Data.PCR_Prop_Common_Entry;
        //        propertyInfoMasterDTO.PCR_Prop_Unit1 = Data.PCR_Prop_Unit1;
        //        propertyInfoMasterDTO.PCR_Prop_Unit1_Occupied = Data.PCR_Prop_Unit1_Occupied;
        //        propertyInfoMasterDTO.PCR_Prop_Unit2 = Data.PCR_Prop_Unit2;
        //        propertyInfoMasterDTO.PCR_Prop_Unit2_Occupied = Data.PCR_Prop_Unit2_Occupied;
        //        propertyInfoMasterDTO.PCR_Prop_Unit3 = Data.PCR_Prop_Unit3;
        //        propertyInfoMasterDTO.PCR_Prop_Unit3_Occupied = Data.PCR_Prop_Unit3_Occupied;
        //        propertyInfoMasterDTO.PCR_Prop_Unit4 = Data.PCR_Prop_Unit4;
        //        propertyInfoMasterDTO.PCR_Prop_Unit4_Occupied = Data.PCR_Prop_Unit4_Occupied;
        //        propertyInfoMasterDTO.PCR_Prop_Garage_Attached = Data.PCR_Prop_Garage_Attached;
        //        propertyInfoMasterDTO.PCR_Prop_Garage_Carport = Data.PCR_Prop_Garage_Carport;
        //        propertyInfoMasterDTO.PCR_Prop_Garage_Detached = Data.PCR_Prop_Garage_Detached;
        //        propertyInfoMasterDTO.PCR_Prop_Garage_None = Data.PCR_Prop_Garage_None;
        //        propertyInfoMasterDTO.PCR_Prop_Property_Vacant_Yes = Data.PCR_Prop_Property_Vacant_Yes;
        //        propertyInfoMasterDTO.PCR_Prop_Property_Vacant_No = Data.PCR_Prop_Property_Vacant_No;
        //        propertyInfoMasterDTO.PCR_Prop_Property_Vacant_Land = Data.PCR_Prop_Property_Vacant_Land;
        //        propertyInfoMasterDTO.PCR_Prop_Property_Vacant_Bad_Address = Data.PCR_Prop_Property_Vacant_Bad_Address;
        //        propertyInfoMasterDTO.PCR_Prop_Property_Vacant_Bad_Address_Commend = Data.PCR_Prop_Property_Vacant_Bad_Address_Commend;
        //        propertyInfoMasterDTO.PCR_Prop_Property_Vacant_Partial = Data.PCR_Prop_Property_Vacant_Partial;
        //        propertyInfoMasterDTO.PCR_Prop_Occupancy_Verified_Contact_Owner = Data.PCR_Prop_Occupancy_Verified_Contact_Owner;
        //        propertyInfoMasterDTO.PCR_Prop_Occupancy_Verified_Personal_Visible = Data.PCR_Prop_Occupancy_Verified_Personal_Visible;
        //        propertyInfoMasterDTO.PCR_Prop_Occupancy_Verified_Neighbor = Data.PCR_Prop_Occupancy_Verified_Neighbor;
        //        propertyInfoMasterDTO.PCR_Prop_Occupancy_Verified_Utilities_On = Data.PCR_Prop_Occupancy_Verified_Utilities_On;
        //        propertyInfoMasterDTO.PCR_Prop_Occupancy_Verified_Visual = Data.PCR_Prop_Occupancy_Verified_Visual;
        //        propertyInfoMasterDTO.PCR_Prop_Occupancy_Verified_Direct_Con_Tenant = Data.PCR_Prop_Occupancy_Verified_Direct_Con_Tenant;
        //        propertyInfoMasterDTO.PCR_Prop_Occupancy_Verified_Direct_Con_Mortgagor = Data.PCR_Prop_Occupancy_Verified_Direct_Con_Mortgagor;
        //        propertyInfoMasterDTO.PCR_Prop_Occupancy_Verified_Direct_Con_Unknown = Data.PCR_Prop_Occupancy_Verified_Direct_Con_Unknown;
        //        propertyInfoMasterDTO.PCR_Prop_Owner_Maintaining_Property = Data.PCR_Prop_Owner_Maintaining_Property;
        //        propertyInfoMasterDTO.PCR_Prop_Other = Data.PCR_Prop_Other;
        //        propertyInfoMasterDTO.PRC_Prop_IsActive = Data.PRC_Prop_IsActive;
        //        propertyInfoMasterDTO.PRC_Prop_IsDelete = Data.PRC_Prop_IsDelete;
        //        propertyInfoMasterDTO.UserID = Data.UserID;
        //        propertyInfoMasterDTO.Type = Data.Type;



        //        var returnVal = await Task.Run(() => propertyInfoMasterData.AddPCR_Propert_Info_Data(propertyInfoMasterDTO));
        //        return returnVal;

        //    }
        //    catch (Exception ex)
        //    {

        //        objdynamicobj.Add(ex.Message);
        //        return objdynamicobj;
        //    }

        //}

        ////For Property Information Get method
        //[HttpPost]
        //[Route("GetPropertyInfoData")]
        //public async Task<List<dynamic>> GetPropertyInfoData()
        //{
        //    List<dynamic> objdynamicobj = new List<dynamic>();

        //    try
        //    {
        //        System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
        //        System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
        //        System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
        //        string s = reader.ReadToEnd();


        //        var Data = JsonConvert.DeserializeObject<PropertyInfoMasterDTO>(s);

        //        PropertyInfoMasterDTO propertyInfoMasterDTO = new PropertyInfoMasterDTO();

        //        propertyInfoMasterDTO.PCR_Pro_PkeyID = Data.PCR_Pro_PkeyID;
        //        propertyInfoMasterDTO.PCR_Prop_WO_ID = Data.PCR_Prop_WO_ID;
        //        propertyInfoMasterDTO.Type = Data.Type;



        //        var GetProperty = await Task.Run(() => propertyInfoMasterData.GetProperty_Info_Master_Details(propertyInfoMasterDTO));
        //        return GetProperty;

        //    }
        //    catch (Exception ex)
        //    {

        //        objdynamicobj.Add(ex.Message);
        //        return objdynamicobj;
        //    }

        //}



        ////For PCR Damage Post method
        //[HttpPost]
        //[Route("PostPCRDamageData")]
        //public async Task<List<dynamic>> PostPCRDamageData()
        //{
        //    List<dynamic> objdynamicobj = new List<dynamic>();

        //    try
        //    {
        //        System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
        //        System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
        //        System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
        //        string s = reader.ReadToEnd();


        //        var Data = JsonConvert.DeserializeObject<PCRDamageMasterDTO>(s);

        //        PCRDamageMasterDTO pCRDamageMasterDTO = new PCRDamageMasterDTO();

        //        pCRDamageMasterDTO.PCR_Damage_pkeyId = Data.PCR_Damage_pkeyId;
        //        pCRDamageMasterDTO.PCR_Damage_MasterId = Data.PCR_Damage_MasterId;
        //        pCRDamageMasterDTO.PCR_Damage_WO_Id = Data.PCR_Damage_WO_Id;
        //        pCRDamageMasterDTO.PCR_Damage_ValType = Data.PCR_Damage_ValType;
        //        pCRDamageMasterDTO.PCR_Damage_Fire_Smoke_Damage_Yes = Data.PCR_Damage_Fire_Smoke_Damage_Yes;
        //        pCRDamageMasterDTO.PCR_Damage_Fire_Smoke_Damage_No = Data.PCR_Damage_Fire_Smoke_Damage_No;
        //        pCRDamageMasterDTO.PCR_Damage_Mortgagor_Neglect_Yes = Data.PCR_Damage_Mortgagor_Neglect_Yes;
        //        pCRDamageMasterDTO.PCR_Damage_Mortgagor_Neglect_No = Data.PCR_Damage_Mortgagor_Neglect_No;
        //        pCRDamageMasterDTO.PCR_Damage_Vandalism_Yes = Data.PCR_Damage_Vandalism_Yes;
        //        pCRDamageMasterDTO.PCR_Damage_Vandalism_No = Data.PCR_Damage_Vandalism_No;
        //        pCRDamageMasterDTO.PCR_Damage_Freeze_Damage_Yes = Data.PCR_Damage_Freeze_Damage_Yes;
        //        pCRDamageMasterDTO.PCR_Damage_Freeze_Damage_No = Data.PCR_Damage_Freeze_Damage_No;
        //        pCRDamageMasterDTO.PCR_Damage_Storm_Damage_Yes = Data.PCR_Damage_Storm_Damage_Yes;
        //        pCRDamageMasterDTO.PCR_Damage_Storm_Damage_No = Data.PCR_Damage_Storm_Damage_No;
        //        pCRDamageMasterDTO.PCR_Damage_Flood_Damage_Yes = Data.PCR_Damage_Flood_Damage_Yes;
        //        pCRDamageMasterDTO.PCR_Damage_Flood_Damage_No = Data.PCR_Damage_Flood_Damage_No;
        //        pCRDamageMasterDTO.PCR_Damage_Water_Damage_Yes = Data.PCR_Damage_Water_Damage_Yes;
        //        pCRDamageMasterDTO.PCR_Damage_Water_Damage_No = Data.PCR_Damage_Water_Damage_No;
        //        pCRDamageMasterDTO.PCR_Damage_Wear_And_Tear_Yes = Data.PCR_Damage_Wear_And_Tear_Yes;
        //        pCRDamageMasterDTO.PCR_Damage_Wear_And_Tear_No = Data.PCR_Damage_Wear_And_Tear_No;
        //        pCRDamageMasterDTO.PCR_Damage_Unfinished_Renovation_Yes = Data.PCR_Damage_Unfinished_Renovation_Yes;
        //        pCRDamageMasterDTO.PCR_Damage_Unfinished_Renovation_No = Data.PCR_Damage_Unfinished_Renovation_No;
        //        pCRDamageMasterDTO.PCR_Damage_Structural_Damage_Yes = Data.PCR_Damage_Structural_Damage_Yes;
        //        pCRDamageMasterDTO.PCR_Damage_Structural_Damage_No = Data.PCR_Damage_Structural_Damage_No;
        //        pCRDamageMasterDTO.PCR_Damage_Excessive_Humidty_Yes = Data.PCR_Damage_Excessive_Humidty_Yes;
        //        pCRDamageMasterDTO.PCR_Damage_Excessive_Humidty_No = Data.PCR_Damage_Excessive_Humidty_No;
        //        pCRDamageMasterDTO.PCR_Urgent_Damages_Roof_Leak_Yes = Data.PCR_Urgent_Damages_Roof_Leak_Yes;
        //        pCRDamageMasterDTO.PCR_Urgent_Damages_Roof_Leak_No = Data.PCR_Urgent_Damages_Roof_Leak_No;
        //        pCRDamageMasterDTO.PCR_Urgent_Damages_Roof_Traped_Yes = Data.PCR_Urgent_Damages_Roof_Traped_Yes;
        //        pCRDamageMasterDTO.PCR_Urgent_Damages_Roof_Traped_No = Data.PCR_Urgent_Damages_Roof_Traped_No;
        //        pCRDamageMasterDTO.PCR_Urgent_Damages_Mold_Damage_Yes = Data.PCR_Urgent_Damages_Mold_Damage_Yes;
        //        pCRDamageMasterDTO.PCR_Urgent_Damages_Mold_Damage_No = Data.PCR_Urgent_Damages_Mold_Damage_No;
        //        pCRDamageMasterDTO.PCR_Urgent_Damages_SeePage_Yes = Data.PCR_Urgent_Damages_SeePage_Yes;
        //        pCRDamageMasterDTO.PCR_Urgent_Damages_SeePage_No = Data.PCR_Urgent_Damages_SeePage_No;
        //        pCRDamageMasterDTO.PCR_Urgent_Damages_Flooded_Basement_Yes = Data.PCR_Urgent_Damages_Flooded_Basement_Yes;
        //        pCRDamageMasterDTO.PCR_Urgent_Damages_Flooded_Basement_No = Data.PCR_Urgent_Damages_Flooded_Basement_No;
        //        pCRDamageMasterDTO.PCR_Urgent_Damages_Foundation_Cracks_Yes = Data.PCR_Urgent_Damages_Foundation_Cracks_Yes;
        //        pCRDamageMasterDTO.PCR_Urgent_Damages_Foundation_Cracks_No = Data.PCR_Urgent_Damages_Foundation_Cracks_No;
        //        pCRDamageMasterDTO.PCR_Urgent_Damages_Wet_Carpet_Yes = Data.PCR_Urgent_Damages_Wet_Carpet_Yes;
        //        pCRDamageMasterDTO.PCR_Urgent_Damages_Wet_Carpet_No = Data.PCR_Urgent_Damages_Wet_Carpet_No;
        //        pCRDamageMasterDTO.PCR_Urgent_Damages_Water_Stains_Yes = Data.PCR_Urgent_Damages_Water_Stains_Yes;
        //        pCRDamageMasterDTO.PCR_Urgent_Damages_Water_Stains_No = Data.PCR_Urgent_Damages_Water_Stains_No;
        //        pCRDamageMasterDTO.PCR_Urgent_Damages_Floors_Safety_Yes = Data.PCR_Urgent_Damages_Floors_Safety_Yes;
        //        pCRDamageMasterDTO.PCR_Urgent_Damages_Floors_Safety_No = Data.PCR_Urgent_Damages_Floors_Safety_No;
        //        pCRDamageMasterDTO.PCR_Urgent_Damages_Other_Causing_Damage_Yes = Data.PCR_Urgent_Damages_Other_Causing_Damage_Yes;
        //        pCRDamageMasterDTO.PCR_Urgent_Damages_Other_Causing_Damage_No = Data.PCR_Urgent_Damages_Other_Causing_Damage_No;
        //        pCRDamageMasterDTO.PCR_Urgent_Damages_Other_Safety_Issue_Yes = Data.PCR_Urgent_Damages_Other_Safety_Issue_Yes;
        //        pCRDamageMasterDTO.PCR_Urgent_Damages_Other_Safety_Issue_No = Data.PCR_Urgent_Damages_Other_Safety_Issue_No;
        //        pCRDamageMasterDTO.PCR_System_Damages_HVAC_System_Damage_Yes = Data.PCR_System_Damages_HVAC_System_Damage_Yes;
        //        pCRDamageMasterDTO.PCR_System_Damages_HVAC_System_Damage_No = Data.PCR_System_Damages_HVAC_System_Damage_No;
        //        pCRDamageMasterDTO.PCR_System_Damages_Electric_Damage_Yes = Data.PCR_System_Damages_Electric_Damage_Yes;
        //        pCRDamageMasterDTO.PCR_System_Damages_Electric_Damage_No = Data.PCR_System_Damages_Electric_Damage_No;
        //        pCRDamageMasterDTO.PCR_System_Damages_Plumbing_Damage_Yes = Data.PCR_System_Damages_Plumbing_Damage_Yes;
        //        pCRDamageMasterDTO.PCR_System_Damages_Plumbing_Damage_No = Data.PCR_System_Damages_Plumbing_Damage_No;
        //        pCRDamageMasterDTO.PCR_System_Damages_Uncapped_Wire_Yes = Data.PCR_System_Damages_Uncapped_Wire_Yes;
        //        pCRDamageMasterDTO.PCR_System_Damages_Uncapped_Wire_No = Data.PCR_System_Damages_Uncapped_Wire_No;
        //        pCRDamageMasterDTO.PCR_Damages_FEMA_Damages_Yes = Data.PCR_Damages_FEMA_Damages_Yes;
        //        pCRDamageMasterDTO.PCR_Damages_FEMA_Damages_No = Data.PCR_Damages_FEMA_Damages_No;
        //        pCRDamageMasterDTO.PCR_Damages_FEMA_Neighborhood_Level_Light = Data.PCR_Damages_FEMA_Neighborhood_Level_Light;
        //        pCRDamageMasterDTO.PCR_Damages_FEMA_Neighborhood_Level_Moderate = Data.PCR_Damages_FEMA_Neighborhood_Level_Moderate;
        //        pCRDamageMasterDTO.PCR_Damages_FEMA_Neighborhood_Level_Severe = Data.PCR_Damages_FEMA_Neighborhood_Level_Severe;
        //        pCRDamageMasterDTO.PCR_Damages_FEMA_Neighborhood_Level_Not_Affected = Data.PCR_Damages_FEMA_Neighborhood_Level_Not_Affected;
        //        pCRDamageMasterDTO.PCR_Damages_FEMA_Trailer_Present = Data.PCR_Damages_FEMA_Trailer_Present;
        //        pCRDamageMasterDTO.PCR_Damages_FEMA_Property_Level_Light_Moderate = Data.PCR_Damages_FEMA_Property_Level_Light_Moderate;
        //        pCRDamageMasterDTO.PCR_Damages_FEMA_Property_Level_Light_Severe = Data.PCR_Damages_FEMA_Property_Level_Light_Severe;
        //        pCRDamageMasterDTO.PCR_Damages_FEMA_Property_Level_Light_Not_Affected = Data.PCR_Damages_FEMA_Property_Level_Light_Not_Affected;
        //        pCRDamageMasterDTO.PCR_Damages_Property_Habitable = Data.PCR_Damages_Property_Habitable;
        //        pCRDamageMasterDTO.PCR_Damages_Property_Habitable_FEMA_Damage_Cause_By_Wind = Data.PCR_Damages_Property_Habitable_FEMA_Damage_Cause_By_Wind;
        //        pCRDamageMasterDTO.PCR_Damages_Property_Habitable_FEMA_Damage_Cause_By_Water = Data.PCR_Damages_Property_Habitable_FEMA_Damage_Cause_By_Water;
        //        pCRDamageMasterDTO.PCR_Damages_Property_Habitable_FEMA_Damage_Cause_By_Fire = Data.PCR_Damages_Property_Habitable_FEMA_Damage_Cause_By_Fire;
        //        pCRDamageMasterDTO.PCR_Damages_Property_Habitable_FEMA_Damage_Cause_By_Flood = Data.PCR_Damages_Property_Habitable_FEMA_Damage_Cause_By_Flood;
        //        pCRDamageMasterDTO.PCR_Damages_FEMA_Damage_Estimate = Data.PCR_Damages_FEMA_Damage_Estimate;
        //        pCRDamageMasterDTO.PCR_Damages_Damage = Data.PCR_Damages_Damage;
        //        pCRDamageMasterDTO.PCR_Damages_Status = Data.PCR_Damages_Status;
        //        pCRDamageMasterDTO.PCR_Damages_Cause = Data.PCR_Damages_Cause;
        //        pCRDamageMasterDTO.PCR_Damages_Int_Ext = Data.PCR_Damages_Int_Ext;
        //        pCRDamageMasterDTO.PCR_Damages_Building = Data.PCR_Damages_Building;
        //        pCRDamageMasterDTO.PCR_Damages_Room = Data.PCR_Damages_Room;
        //        pCRDamageMasterDTO.PCR_Damages_Description = Data.PCR_Damages_Description;
        //        pCRDamageMasterDTO.PCR_Damages_Qty = Data.PCR_Damages_Qty;
        //        pCRDamageMasterDTO.PCR_Damages_Estimate = Data.PCR_Damages_Estimate;
        //        pCRDamageMasterDTO.PCR_Damages_IsActive = Data.PCR_Damages_IsActive;
        //        pCRDamageMasterDTO.PCR_Damages_IsDelete = Data.PCR_Damages_IsDelete;
        //        pCRDamageMasterDTO.UserID = Data.UserID;
        //        pCRDamageMasterDTO.Type = Data.Type;



        //        var AddPRCDamage = await Task.Run(() => pCRDamageMasterData.AddPCR_Damage_Data(pCRDamageMasterDTO));
        //        return AddPRCDamage;

        //    }
        //    catch (Exception ex)
        //    {

        //        objdynamicobj.Add(ex.Message);
        //        return objdynamicobj;
        //    }

        //}

        ////For PCR Damage Get method
        //[HttpPost]
        //[Route("GetPCRDamageData")]
        //public async Task<List<dynamic>> GetPCRDamageData()
        //{
        //    List<dynamic> objdynamicobj = new List<dynamic>();

        //    try
        //    {
        //        System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
        //        System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
        //        System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
        //        string s = reader.ReadToEnd();


        //        var Data = JsonConvert.DeserializeObject<PCRDamageMasterDTO>(s);

        //        PCRDamageMasterDTO pCRDamageMasterDTO = new PCRDamageMasterDTO();

        //        pCRDamageMasterDTO.PCR_Damage_pkeyId = Data.PCR_Damage_pkeyId;
        //        pCRDamageMasterDTO.PCR_Damage_WO_Id = Data.PCR_Damage_WO_Id;
        //        pCRDamageMasterDTO.Type = Data.Type;



        //        var GetPRCDamage = await Task.Run(() => pCRDamageMasterData.GetPCRDamageDetails(pCRDamageMasterDTO));
        //        return GetPRCDamage;

        //    }
        //    catch (Exception ex)
        //    {

        //        objdynamicobj.Add(ex.Message);
        //        return objdynamicobj;
        //    }

        //}

        ////For PCR Debris Post method
        //[HttpPost]
        //[Route("PostPCRDebrisData")]
        //public async Task<List<dynamic>> PostPCRDebrisData()
        //{
        //    List<dynamic> objdynamicobj = new List<dynamic>();

        //    try
        //    {
        //        System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
        //        System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
        //        System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
        //        string s = reader.ReadToEnd();


        //        var Data = JsonConvert.DeserializeObject<PCR_Debris_MasterDTO>(s);

        //        PCR_Debris_MasterDTO pCR_Debris_MasterDTO = new PCR_Debris_MasterDTO();

        //        pCR_Debris_MasterDTO.PCR_Debris_pkeyId = Data.PCR_Debris_pkeyId;
        //        pCR_Debris_MasterDTO.PCR_Debris_Master_Id = Data.PCR_Debris_Master_Id;
        //        pCR_Debris_MasterDTO.PCR_Debris_WO_Id = Data.PCR_Debris_WO_Id;
        //        pCR_Debris_MasterDTO.PCR_Debris_ValType = Data.PCR_Debris_ValType;
        //        pCR_Debris_MasterDTO.PCR_Debris_Interior_Debris = Data.PCR_Debris_Interior_Debris;
        //        pCR_Debris_MasterDTO.PCR_Debris_Is_There_Interior_Debris_Present = Data.PCR_Debris_Is_There_Interior_Debris_Present;
        //        pCR_Debris_MasterDTO.PCR_Debris_Remove_Any_Interior_Debris = Data.PCR_Debris_Remove_Any_Interior_Debris;
        //        pCR_Debris_MasterDTO.PCR_Debris_Describe = Data.PCR_Debris_Describe;
        //        pCR_Debris_MasterDTO.PCR_Debris_Cubic_Yards = Data.PCR_Debris_Cubic_Yards;
        //        pCR_Debris_MasterDTO.PCR_Debris_Broom_Swept_Condition = Data.PCR_Debris_Broom_Swept_Condition;
        //        pCR_Debris_MasterDTO.PCR_Debris_Broom_Swept_Condition_Describe = Data.PCR_Debris_Broom_Swept_Condition_Describe;
        //        pCR_Debris_MasterDTO.PCR_Debris_Exterior_Debris = Data.PCR_Debris_Exterior_Debris;
        //        pCR_Debris_MasterDTO.PCR_Debris_Remove_Exterior_Debris = Data.PCR_Debris_Remove_Exterior_Debris;
        //        pCR_Debris_MasterDTO.PCR_Debris_Exterior_Debris_Present = Data.PCR_Debris_Exterior_Debris_Present;
        //        pCR_Debris_MasterDTO.PCR_Debris_Exterior_Debris_Describe = Data.PCR_Debris_Exterior_Debris_Describe;
        //        pCR_Debris_MasterDTO.PCR_Debris_Exterior_Debris_Cubic_Yard = Data.PCR_Debris_Exterior_Debris_Cubic_Yard;
        //        pCR_Debris_MasterDTO.PCR_Debris_Exterior_Debris_Visible_From_Street = Data.PCR_Debris_Exterior_Debris_Visible_From_Street;
        //        pCR_Debris_MasterDTO.PCR_Debris_Exterior_On_The_Lawn = Data.PCR_Debris_Exterior_On_The_Lawn;
        //        pCR_Debris_MasterDTO.PCR_Debris_Exterior_Vehicles_Present = Data.PCR_Debris_Exterior_Vehicles_Present;
        //        pCR_Debris_MasterDTO.PCR_Debris_Exterior_Vehicles_Present_Describe = Data.PCR_Debris_Exterior_Vehicles_Present_Describe;
        //        pCR_Debris_MasterDTO.PCR_Debris_Dump_Recipt_Name = Data.PCR_Debris_Dump_Recipt_Name;
        //        pCR_Debris_MasterDTO.PCR_Debris_Dump_Recipt_Address = Data.PCR_Debris_Dump_Recipt_Address;
        //        pCR_Debris_MasterDTO.PCR_Debris_Dump_Recipt_Phone = Data.PCR_Debris_Dump_Recipt_Phone;
        //        pCR_Debris_MasterDTO.PCR_Debris_Dump_Recipt_Desc_what_was_Dump = Data.PCR_Debris_Dump_Recipt_Desc_what_was_Dump;
        //        pCR_Debris_MasterDTO.PCR_Debris_Dump_Recipt_Means_Of_Disposal = Data.PCR_Debris_Dump_Recipt_Means_Of_Disposal;
        //        pCR_Debris_MasterDTO.PCR_Debris_InteriorHazards_Health_Present = Data.PCR_Debris_InteriorHazards_Health_Present;
        //        pCR_Debris_MasterDTO.PCR_Debris_InteriorHazards_Health_Present_Describe = Data.PCR_Debris_InteriorHazards_Health_Present_Describe;
        //        pCR_Debris_MasterDTO.PCR_Debris_InteriorHazards_Health_Present_Cubic_Yard = Data.PCR_Debris_InteriorHazards_Health_Present_Cubic_Yard;
        //        pCR_Debris_MasterDTO.PCR_Debris_Exterior_Hazards_Health_Present = Data.PCR_Debris_Exterior_Hazards_Health_Present;
        //        pCR_Debris_MasterDTO.PCR_Debris_Exterior_Hazards_Health_Present_Describe = Data.PCR_Debris_Exterior_Hazards_Health_Present_Describe;
        //        pCR_Debris_MasterDTO.PCR_Debris_Exterior_Hazards_Health_PresentCubic_Yards = Data.PCR_Debris_Exterior_Hazards_Health_PresentCubic_Yards;
        //        pCR_Debris_MasterDTO.PCR_Debris_IsActive = Data.PCR_Debris_IsActive;
        //        pCR_Debris_MasterDTO.UserID = Data.UserID;
        //        pCR_Debris_MasterDTO.Type = Data.Type;



        //        var AddPRCDebris = await Task.Run(() => pCR_Debris_MasterData.AddPCR_Debris_Data(pCR_Debris_MasterDTO));
        //        return AddPRCDebris;

        //    }
        //    catch (Exception ex)
        //    {

        //        objdynamicobj.Add(ex.Message);
        //        return objdynamicobj;
        //    }

        //}

        ////For PCR Debris Get method
        //[HttpPost]
        //[Route("GetPCRDebrisData")]
        //public async Task<List<dynamic>> GetPCRDebrisData()
        //{
        //    List<dynamic> objdynamicobj = new List<dynamic>();

        //    try
        //    {
        //        System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
        //        System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
        //        System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
        //        string s = reader.ReadToEnd();


        //        var Data = JsonConvert.DeserializeObject<PCR_Debris_MasterDTO>(s);

        //        PCR_Debris_MasterDTO pCR_Debris_MasterDTO = new PCR_Debris_MasterDTO();

        //        pCR_Debris_MasterDTO.PCR_Debris_pkeyId = Data.PCR_Debris_pkeyId;
        //        pCR_Debris_MasterDTO.PCR_Debris_WO_Id = Data.PCR_Debris_WO_Id;
        //        pCR_Debris_MasterDTO.Type = Data.Type;



        //        var GetPRCDebris = await Task.Run(() => pCR_Debris_MasterData.GetPCRDebrisDetails(pCR_Debris_MasterDTO));
        //        return GetPRCDebris;

        //    }
        //    catch (Exception ex)
        //    {

        //        objdynamicobj.Add(ex.Message);
        //        return objdynamicobj;
        //    }

        //}

        ////For PCR Pool Post method
        //[HttpPost]
        //[Route("PostPCRPoolData")]
        //public async Task<List<dynamic>> PostPCRPoolData()
        //{
        //    List<dynamic> objdynamicobj = new List<dynamic>();

        //    try
        //    {
        //        System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
        //        System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
        //        System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
        //        string s = reader.ReadToEnd();


        //        var Data = JsonConvert.DeserializeObject<PCR_Pool_MasterDTO>(s);

        //        PCR_Pool_MasterDTO pCR_Pool_MasterDTO = new PCR_Pool_MasterDTO();

        //        pCR_Pool_MasterDTO.PCR_Pool_pkeyId = Data.PCR_Pool_pkeyId;
        //        pCR_Pool_MasterDTO.PCR_Pool_MasterId = Data.PCR_Pool_MasterId;
        //        pCR_Pool_MasterDTO.PCR_Pool_WO_Id = Data.PCR_Pool_WO_Id;
        //        pCR_Pool_MasterDTO.PCR_Pool_ValType = Data.PCR_Pool_ValType;
        //        pCR_Pool_MasterDTO.PCR_Pool_Info_Pool_Present = Data.PCR_Pool_Info_Pool_Present;
        //        pCR_Pool_MasterDTO.PCR_Pool_Diameter_Ft = Data.PCR_Pool_Diameter_Ft;
        //        pCR_Pool_MasterDTO.PCR_Pool_Length_Ft = Data.PCR_Pool_Length_Ft;
        //        pCR_Pool_MasterDTO.PCR_Pool_Width_Ft = Data.PCR_Pool_Width_Ft;
        //        pCR_Pool_MasterDTO.PCR_Pool_Condition_Good = Data.PCR_Pool_Condition_Good;
        //        pCR_Pool_MasterDTO.PCR_Pool_Condition_Fair = Data.PCR_Pool_Condition_Fair;
        //        pCR_Pool_MasterDTO.PCR_Pool_Condition_Poor = Data.PCR_Pool_Condition_Poor;
        //        pCR_Pool_MasterDTO.PCR_Pool_Condition_Damaged = Data.PCR_Pool_Condition_Damaged;
        //        pCR_Pool_MasterDTO.PCR_Pool_Type_InGround = Data.PCR_Pool_Type_InGround;
        //        pCR_Pool_MasterDTO.PCR_Pool_Type_Above_Ground = Data.PCR_Pool_Type_Above_Ground;
        //        pCR_Pool_MasterDTO.PCR_Pool_Secure_On_This_Order = Data.PCR_Pool_Secure_On_This_Order;
        //        pCR_Pool_MasterDTO.PCR_Pool_Is_There_Fence = Data.PCR_Pool_Is_There_Fence;
        //        pCR_Pool_MasterDTO.PCR_Pool_Is_It_Locked = Data.PCR_Pool_Is_It_Locked;
        //        pCR_Pool_MasterDTO.PCR_Pool_Water_Level_Full = Data.PCR_Pool_Water_Level_Full;
        //        pCR_Pool_MasterDTO.PCR_Pool_Water_Level_Partially_Filled = Data.PCR_Pool_Water_Level_Partially_Filled;
        //        pCR_Pool_MasterDTO.PCR_Pool_Water_Level_Empty = Data.PCR_Pool_Water_Level_Empty;
        //        pCR_Pool_MasterDTO.PCR_Pool_Did_You_Drain_It = Data.PCR_Pool_Did_You_Drain_It;
        //        pCR_Pool_MasterDTO.PCR_Pool_Dismantled_Removed = Data.PCR_Pool_Dismantled_Removed;
        //        pCR_Pool_MasterDTO.PCR_Pool_Is_There_Depression_Left = Data.PCR_Pool_Is_There_Depression_Left;
        //        pCR_Pool_MasterDTO.PCR_Pool_Secured_Per_Guidelines = Data.PCR_Pool_Secured_Per_Guidelines;
        //        pCR_Pool_MasterDTO.PCR_Pool_Is_The_Pool_Converted_Prevents_Entry = Data.PCR_Pool_Is_The_Pool_Converted_Prevents_Entry;
        //        pCR_Pool_MasterDTO.PCR_Pool_Hot_Tub_Present = Data.PCR_Pool_Hot_Tub_Present;
        //        pCR_Pool_MasterDTO.PCR_Pool_Bids_Drain_Shock_Install_Safety_Cover = Data.PCR_Pool_Bids_Drain_Shock_Install_Safety_Cover;
        //        pCR_Pool_MasterDTO.PCR_Pool_Bid_To_Install_Safety_Cover = Data.PCR_Pool_Bid_To_Install_Safety_Cover;
        //        pCR_Pool_MasterDTO.PCR_Pool_Bid_To_Drain = Data.PCR_Pool_Bid_To_Drain;
        //        pCR_Pool_MasterDTO.PCR_Pool_Bid_To_Dismantle = Data.PCR_Pool_Bid_To_Dismantle;
        //        pCR_Pool_MasterDTO.PCR_Pool_Drain_Remove = Data.PCR_Pool_Drain_Remove;
        //        pCR_Pool_MasterDTO.PCR_Pool_Bid_To_Fill_Hole = Data.PCR_Pool_Bid_To_Fill_Hole;
        //        pCR_Pool_MasterDTO.PCR_Pool_Size_Of_Hole = Data.PCR_Pool_Size_Of_Hole;
        //        pCR_Pool_MasterDTO.PCR_Pool_Cubic_Yds_Of_Dirt = Data.PCR_Pool_Cubic_Yds_Of_Dirt;
        //        pCR_Pool_MasterDTO.PCR_Pool_Secure_This_Order_No_Secure_By_FiveBrothers = Data.PCR_Pool_Secure_This_Order_No_Secure_By_FiveBrothers;
        //        pCR_Pool_MasterDTO.PCR_Pool_Secure_This_Order_No_Bid_To_Secure = Data.PCR_Pool_Secure_This_Order_No_Bid_To_Secure;
        //        pCR_Pool_MasterDTO.PCR_Pool_Secure_This_Order_No_Previously_Secure_By_Order = Data.PCR_Pool_Secure_This_Order_No_Previously_Secure_By_Order;
        //        pCR_Pool_MasterDTO.PCR_Pool_Secure_This_Order_No_Other = Data.PCR_Pool_Secure_This_Order_No_Other;
        //        pCR_Pool_MasterDTO.PCR_Pool_Hot_Tub_Present_Yes_Covered_Drained = Data.PCR_Pool_Hot_Tub_Present_Yes_Covered_Drained;
        //        pCR_Pool_MasterDTO.PCR_Pool_Hot_Tub_Did_You_Secure = Data.PCR_Pool_Hot_Tub_Did_You_Secure;
        //        pCR_Pool_MasterDTO.PCR_Pool_Hot_Tub_Bids_Diameter_Ft = Data.PCR_Pool_Hot_Tub_Bids_Diameter_Ft;
        //        pCR_Pool_MasterDTO.PCR_Pool_Hot_Tub_Bids_Length_Ft = Data.PCR_Pool_Hot_Tub_Bids_Length_Ft;
        //        pCR_Pool_MasterDTO.PCR_Pool_Hot_Tub_Bids_Width_Ft = Data.PCR_Pool_Hot_Tub_Bids_Width_Ft;
        //        pCR_Pool_MasterDTO.PCR_Pool_Hot_Tub_Bids_Bid_To_Drain = Data.PCR_Pool_Hot_Tub_Bids_Bid_To_Drain;
        //        pCR_Pool_MasterDTO.PCR_Pool_Hot_Tub_Bids_Bit_To_Install_Cover = Data.PCR_Pool_Hot_Tub_Bids_Bit_To_Install_Cover;
        //        pCR_Pool_MasterDTO.PCR_Pool_Hot_Tub_Bids_Drain_Secure = Data.PCR_Pool_Hot_Tub_Bids_Drain_Secure;
        //        pCR_Pool_MasterDTO.PCR_Pool_IsActive = Data.PCR_Pool_IsActive;
        //        pCR_Pool_MasterDTO.PCR_Pool_IsDelete = Data.PCR_Pool_IsDelete;
        //        pCR_Pool_MasterDTO.UserID = Data.UserID;
        //        pCR_Pool_MasterDTO.Type = Data.Type;



        //        var AddPool = await Task.Run(() => pCR_Pool_MasterData.AddPCR_Pool_Data(pCR_Pool_MasterDTO));
        //        return AddPool;

        //    }
        //    catch (Exception ex)
        //    {

        //        objdynamicobj.Add(ex.Message);
        //        return objdynamicobj;
        //    }

        //}

        ////For PCR Pool Get method
        //[HttpPost]
        //[Route("GetPCRPoolData")]
        //public async Task<List<dynamic>> GetPCRPoolData()
        //{
        //    List<dynamic> objdynamicobj = new List<dynamic>();

        //    try
        //    {
        //        System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
        //        System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
        //        System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
        //        string s = reader.ReadToEnd();


        //        var Data = JsonConvert.DeserializeObject<PCR_Pool_MasterDTO>(s);

        //        PCR_Pool_MasterDTO pCR_Pool_MasterDTO = new PCR_Pool_MasterDTO();

        //        pCR_Pool_MasterDTO.PCR_Pool_pkeyId = Data.PCR_Pool_pkeyId;
        //        pCR_Pool_MasterDTO.PCR_Pool_WO_Id = Data.PCR_Pool_WO_Id;
        //        pCR_Pool_MasterDTO.Type = Data.Type;



        //        var GetPool = await Task.Run(() => pCR_Pool_MasterData.GetPCRPoolDetails(pCR_Pool_MasterDTO));
        //        return GetPool;

        //    }
        //    catch (Exception ex)
        //    {

        //        objdynamicobj.Add(ex.Message);
        //        return objdynamicobj;
        //    }

        //}


        ////For PCR Roof Add method
        //[HttpPost]
        //[Route("PostPCRRoofData")]
        //public async Task<List<dynamic>> PostPCRRoofData()
        //{
        //    List<dynamic> objdynamicobj = new List<dynamic>();

        //    try
        //    {
        //        System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
        //        System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
        //        System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
        //        string s = reader.ReadToEnd();


        //        var Data = JsonConvert.DeserializeObject<PCR_Roof_MasterDTO>(s);

        //        PCR_Roof_MasterDTO pCR_Roof_MasterDTO = new PCR_Roof_MasterDTO();

        //        pCR_Roof_MasterDTO.PCR_Roof_pkeyId = Data.PCR_Roof_pkeyId;
        //        pCR_Roof_MasterDTO.PCR_Roof_MasterId = Data.PCR_Roof_MasterId;
        //        pCR_Roof_MasterDTO.PCR_Roof_WO_Id = Data.PCR_Roof_WO_Id;
        //        pCR_Roof_MasterDTO.PCR_Roof_ValType = Data.PCR_Roof_ValType;
        //        pCR_Roof_MasterDTO.PCR_Roof_Roof_Shape_Pitched_Roof = Data.PCR_Roof_Roof_Shape_Pitched_Roof;
        //        pCR_Roof_MasterDTO.PCR_Roof_Roof_Shape_Flat_Roof = Data.PCR_Roof_Roof_Shape_Flat_Roof;
        //        pCR_Roof_MasterDTO.PCR_Roof_Leak = Data.PCR_Roof_Leak;
        //        pCR_Roof_MasterDTO.PCR_Roof_Location_Dimensions = Data.PCR_Roof_Location_Dimensions;
        //        pCR_Roof_MasterDTO.PCR_Roof_Water_Strains = Data.PCR_Roof_Water_Strains;
        //        pCR_Roof_MasterDTO.PCR_Roof_On_Ceiling_Previously_Reported = Data.PCR_Roof_On_Ceiling_Previously_Reported;
        //        pCR_Roof_MasterDTO.PCR_Roof_Did_You_Perform_Roof_Repair = Data.PCR_Roof_Did_You_Perform_Roof_Repair;
        //        pCR_Roof_MasterDTO.PCR_Roof_Did_You_Perform_Emergency_Traping = Data.PCR_Roof_Did_You_Perform_Emergency_Traping;
        //        pCR_Roof_MasterDTO.PCR_Roof_Bid_To_Trap_Dimension_size_x = Data.PCR_Roof_Bid_To_Trap_Dimension_size_x;
        //        pCR_Roof_MasterDTO.PCR_Roof_Bid_To_Trap_Dimension_size_y = Data.PCR_Roof_Bid_To_Trap_Dimension_size_y;
        //        pCR_Roof_MasterDTO.PCR_Roof_Bid_To_Trap_Location = Data.PCR_Roof_Bid_To_Trap_Location;
        //        pCR_Roof_MasterDTO.PCR_Roof_Bid_To_Trap_Description = Data.PCR_Roof_Bid_To_Trap_Description;
        //        pCR_Roof_MasterDTO.PCR_Roof_Bid_To_Trap_Bid_Amount = Data.PCR_Roof_Bid_To_Trap_Bid_Amount;
        //        pCR_Roof_MasterDTO.PCR_Roof_Bid_To_Tar_Patch_Dimension_size_x = Data.PCR_Roof_Bid_To_Tar_Patch_Dimension_size_x;
        //        pCR_Roof_MasterDTO.PCR_Roof_Bid_To_Tar_Patch_Dimension_size_y = Data.PCR_Roof_Bid_To_Tar_Patch_Dimension_size_y;
        //        pCR_Roof_MasterDTO.PCR_Roof_Bid_To_Tar_Patch_Location = Data.PCR_Roof_Bid_To_Tar_Patch_Location;
        //        pCR_Roof_MasterDTO.PCR_Roof_Bid_To_Tar_Patch_Bid_Amount = Data.PCR_Roof_Bid_To_Tar_Patch_Bid_Amount;
        //        pCR_Roof_MasterDTO.PCR_Roof_Bid_To_Replace = Data.PCR_Roof_Bid_To_Replace;
        //        pCR_Roof_MasterDTO.PCR_Roof_Bid_To_Repair = Data.PCR_Roof_Bid_To_Repair;
        //        pCR_Roof_MasterDTO.PCR_Roof_Bid_Reason = Data.PCR_Roof_Bid_Reason;
        //        pCR_Roof_MasterDTO.PCR_Roof_Bid_To_Description = Data.PCR_Roof_Bid_To_Description;
        //        pCR_Roof_MasterDTO.PCR_Roof_Bid_To_Location_Entire_Roof = Data.PCR_Roof_Bid_To_Location_Entire_Roof;
        //        pCR_Roof_MasterDTO.PCR_Roof_Bid_To_Location_Front = Data.PCR_Roof_Bid_To_Location_Front;
        //        pCR_Roof_MasterDTO.PCR_Roof_Bid_To_Location_Back = Data.PCR_Roof_Bid_To_Location_Back;
        //        pCR_Roof_MasterDTO.PCR_Roof_Bid_To_Location_Left_Side = Data.PCR_Roof_Bid_To_Location_Left_Side;
        //        pCR_Roof_MasterDTO.PCR_Roof_Bid_To_Location_Right_Side = Data.PCR_Roof_Bid_To_Location_Right_Side;
        //        pCR_Roof_MasterDTO.PCR_Roof_Building_House = Data.PCR_Roof_Building_House;
        //        pCR_Roof_MasterDTO.PCR_Roof_Building_Garage = Data.PCR_Roof_Building_Garage;
        //        pCR_Roof_MasterDTO.PCR_Roof_Building_Out_Building = Data.PCR_Roof_Building_Out_Building;
        //        pCR_Roof_MasterDTO.PCR_Roof_Building_Pool_House = Data.PCR_Roof_Building_Pool_House;
        //        pCR_Roof_MasterDTO.PCR_Roof_Building_Shed = Data.PCR_Roof_Building_Shed;
        //        pCR_Roof_MasterDTO.PCR_Roof_Building_Bam = Data.PCR_Roof_Building_Bam;
        //        pCR_Roof_MasterDTO.PCR_Roof_Item_Used_Roof_Type = Data.PCR_Roof_Item_Used_Roof_Type;
        //        pCR_Roof_MasterDTO.PCR_Roof_Item_Used_DRD = Data.PCR_Roof_Item_Used_DRD;
        //        pCR_Roof_MasterDTO.PCR_Roof_Item_Used_Size = Data.PCR_Roof_Item_Used_Size;
        //        pCR_Roof_MasterDTO.PCR_Roof_Item_Used_Amount = Data.PCR_Roof_Item_Used_Amount;
        //        pCR_Roof_MasterDTO.PCR_Roof_Item_Used_Felt_Type = Data.PCR_Roof_Item_Used_Felt_Type;
        //        pCR_Roof_MasterDTO.PCR_Roof_Item_Used_Felt_Type_DRD = Data.PCR_Roof_Item_Used_Felt_Type_DRD;
        //        pCR_Roof_MasterDTO.PCR_Roof_Item_Used_Felt_Type_Size = Data.PCR_Roof_Item_Used_Felt_Type_Size;
        //        pCR_Roof_MasterDTO.PCR_Roof_Item_Used_Felt_Type_Amount = Data.PCR_Roof_Item_Used_Felt_Type_Amount;
        //        pCR_Roof_MasterDTO.PCR_Roof_Item_Used_Sheathing = Data.PCR_Roof_Item_Used_Sheathing;
        //        pCR_Roof_MasterDTO.PCR_Roof_Item_Used_Sheathing_DRD = Data.PCR_Roof_Item_Used_Sheathing_DRD;
        //        pCR_Roof_MasterDTO.PCR_Roof_Item_Used_Sheathing_Size = Data.PCR_Roof_Item_Used_Sheathing_Size;
        //        pCR_Roof_MasterDTO.PCR_Roof_Item_Used_Sheathing_Amount = Data.PCR_Roof_Item_Used_Sheathing_Amount;
        //        pCR_Roof_MasterDTO.PCR_Roof_Item_Used_Deck_Thikness = Data.PCR_Roof_Item_Used_Deck_Thikness;
        //        pCR_Roof_MasterDTO.PCR_Roof_Item_Used_Deck_Thikness_DRD = Data.PCR_Roof_Item_Used_Deck_Thikness_DRD;
        //        pCR_Roof_MasterDTO.PCR_Roof_Item_Used_Drip_Edge = Data.PCR_Roof_Item_Used_Drip_Edge;
        //        pCR_Roof_MasterDTO.PCR_Roof_Item_Used_Drip_Edge_Size = Data.PCR_Roof_Item_Used_Drip_Edge_Size;
        //        pCR_Roof_MasterDTO.PCR_Roof_Item_Used_Drip_Edge_Amount = Data.PCR_Roof_Item_Used_Drip_Edge_Amount;
        //        pCR_Roof_MasterDTO.PCR_Roof_Item_Used_Ice_Water_Barrier = Data.PCR_Roof_Item_Used_Ice_Water_Barrier;
        //        pCR_Roof_MasterDTO.PCR_Roof_Item_Used_Ice_Water_Barrier_Size = Data.PCR_Roof_Item_Used_Ice_Water_Barrier_Size;
        //        pCR_Roof_MasterDTO.PCR_Roof_Item_Used_Ice_Water_Barrier_Amount = Data.PCR_Roof_Item_Used_Ice_Water_Barrier_Amount;
        //        pCR_Roof_MasterDTO.PCR_Roof_Item_Used_No_Of_Vents = Data.PCR_Roof_Item_Used_No_Of_Vents;
        //        pCR_Roof_MasterDTO.PCR_Roof_Item_Used_No_Of_Vents_Text = Data.PCR_Roof_Item_Used_No_Of_Vents_Text;
        //        pCR_Roof_MasterDTO.PCR_Roof_Item_Used_No_Of_Vents_Amount = Data.PCR_Roof_Item_Used_No_Of_Vents_Amount;
        //        pCR_Roof_MasterDTO.PCR_Roof_Item_Used_Roof_Debris = Data.PCR_Roof_Item_Used_Roof_Debris;
        //        pCR_Roof_MasterDTO.PCR_Roof_Item_Used_Roof_Debris_Size = Data.PCR_Roof_Item_Used_Roof_Debris_Size;
        //        pCR_Roof_MasterDTO.PCR_Roof_Item_Used_Roof_Debris_Amount = Data.PCR_Roof_Item_Used_Roof_Debris_Amount;
        //        pCR_Roof_MasterDTO.PCR_Roof_Item_Used_Dempster_Rental = Data.PCR_Roof_Item_Used_Dempster_Rental;
        //        pCR_Roof_MasterDTO.PCR_Roof_Item_Used_Dempster_Rental_Size = Data.PCR_Roof_Item_Used_Dempster_Rental_Size;
        //        pCR_Roof_MasterDTO.PCR_Roof_Item_Used_Dempster_Rental_Amount = Data.PCR_Roof_Item_Used_Dempster_Rental_Amount;
        //        pCR_Roof_MasterDTO.PCR_Bid_Amount = Data.PCR_Bid_Amount;
        //        pCR_Roof_MasterDTO.PCR_Roof_Leak_Case = Data.PCR_Roof_Leak_Case;
        //        pCR_Roof_MasterDTO.PCR_Roof_Leak_Stom = Data.PCR_Roof_Leak_Stom;
        //        pCR_Roof_MasterDTO.PCR_Roof_Leak_Wear_Tear = Data.PCR_Roof_Leak_Wear_Tear;
        //        pCR_Roof_MasterDTO.PCR_Roof_Leak_Other = Data.PCR_Roof_Leak_Other;
        //        pCR_Roof_MasterDTO.PCR_Roof_Leak_Location_Dimension = Data.PCR_Roof_Leak_Location_Dimension;
        //        pCR_Roof_MasterDTO.PCR_Roof_Damage_No_Paint = Data.PCR_Roof_Damage_No_Paint;
        //        pCR_Roof_MasterDTO.PCR_Roof_Perform_Roof_Repair_Explain_Traped_Bid_Repair = Data.PCR_Roof_Perform_Roof_Repair_Explain_Traped_Bid_Repair;
        //        pCR_Roof_MasterDTO.PCR_Roof_Perform_Roof_Repair_Explain_Bid_Reapair = Data.PCR_Roof_Perform_Roof_Repair_Explain_Bid_Reapair;
        //        pCR_Roof_MasterDTO.PCR_Roof_Perform_Roof_Repair_Explain_Bid_Reapair_Replace = Data.PCR_Roof_Perform_Roof_Repair_Explain_Bid_Reapair_Replace;
        //        pCR_Roof_MasterDTO.PCR_Roof_Perform_Roof_Repair_Explain_Bid_Trap_Repair = Data.PCR_Roof_Perform_Roof_Repair_Explain_Bid_Trap_Repair;
        //        pCR_Roof_MasterDTO.PCR_Roof_Explain_Bid_Trap = Data.PCR_Roof_Explain_Bid_Trap;
        //        pCR_Roof_MasterDTO.PCR_Roof_Explain_Already_Traped = Data.PCR_Roof_Explain_Already_Traped;
        //        pCR_Roof_MasterDTO.PCR_Roof_Reason_Cant_Repair_Due_To = Data.PCR_Roof_Reason_Cant_Repair_Due_To;
        //        pCR_Roof_MasterDTO.PCR_Roof_Reason_Preventive_Due_To = Data.PCR_Roof_Reason_Preventive_Due_To;
        //        pCR_Roof_MasterDTO.PCR_Roof_Reason_Leaking = Data.PCR_Roof_Reason_Leaking;
        //        pCR_Roof_MasterDTO.PCR_Roof_Reason_Other = Data.PCR_Roof_Reason_Other;
        //        pCR_Roof_MasterDTO.PCR_Roof_Reason_Other_Text = Data.PCR_Roof_Reason_Other_Text;
        //        pCR_Roof_MasterDTO.PCR_Roof_IsActive = Data.PCR_Roof_IsActive;
        //        pCR_Roof_MasterDTO.PCR_Roof_IsDelete = Data.PCR_Roof_IsDelete;
        //        pCR_Roof_MasterDTO.UserID = Data.UserID;
        //        pCR_Roof_MasterDTO.Type = Data.Type;



        //        var AddRoof = await Task.Run(() => pCR_Roof_MasterData.AddPCR_Roof_Data(pCR_Roof_MasterDTO));
        //        return AddRoof;

        //    }
        //    catch (Exception ex)
        //    {

        //        objdynamicobj.Add(ex.Message);
        //        return objdynamicobj;
        //    }

        //}


        ////For PCR Roof Get method
        //[HttpPost]
        //[Route("GetPCRRoofData")]
        //public async Task<List<dynamic>> GetPCRRoofData()
        //{
        //    List<dynamic> objdynamicobj = new List<dynamic>();

        //    try
        //    {
        //        System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
        //        System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
        //        System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
        //        string s = reader.ReadToEnd();


        //        var Data = JsonConvert.DeserializeObject<PCR_Roof_MasterDTO>(s);

        //        PCR_Roof_MasterDTO pCR_Roof_MasterDTO = new PCR_Roof_MasterDTO();

        //        pCR_Roof_MasterDTO.PCR_Roof_pkeyId = Data.PCR_Roof_pkeyId;
        //        pCR_Roof_MasterDTO.PCR_Roof_WO_Id = Data.PCR_Roof_WO_Id;
        //        pCR_Roof_MasterDTO.Type = Data.Type;



        //        var GetRoof = await Task.Run(() => pCR_Roof_MasterData.GetPCRRoofDetails(pCR_Roof_MasterDTO));
        //        return GetRoof;

        //    }
        //    catch (Exception ex)
        //    {

        //        objdynamicobj.Add(ex.Message);
        //        return objdynamicobj;
        //    }

        //}


        ////For PCR Five Brothers Post method
        //[HttpPost]
        //[Route("PostPCRFiveBrotherData")]
        //public async Task<List<dynamic>> PostPCRFiveBrotherData()
        //{
        //    List<dynamic> objdynamicobj = new List<dynamic>();

        //    try
        //    {
        //        System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
        //        System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
        //        System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
        //        string s = reader.ReadToEnd();


        //        var Data = JsonConvert.DeserializeObject<PCR_FiveBrotherDTO>(s);

        //        PCR_FiveBrotherDTO pCR_FiveBrotherDTO = new PCR_FiveBrotherDTO();

        //        pCR_FiveBrotherDTO.PCR_FiveBro_id = Data.PCR_FiveBro_id;
        //        pCR_FiveBrotherDTO.PCR_FiveBro_WO_ID = Data.PCR_FiveBro_WO_ID;
        //        pCR_FiveBrotherDTO.PCR_FiveBro_Propertyinfo = Data.PCR_FiveBro_Propertyinfo;
        //        pCR_FiveBrotherDTO.PCR_FiveBro_Violations = Data.PCR_FiveBro_Violations;
        //        pCR_FiveBrotherDTO.PCR_FiveBro_Securing = Data.PCR_FiveBro_Securing;
        //        pCR_FiveBrotherDTO.PCR_FiveBro_Winterization = Data.PCR_FiveBro_Winterization;
        //        pCR_FiveBrotherDTO.PCR_FiveBro_Yard = Data.PCR_FiveBro_Yard;
        //        pCR_FiveBrotherDTO.PCR_FiveBro_Debris_Hazards = Data.PCR_FiveBro_Debris_Hazards;
        //        pCR_FiveBrotherDTO.PCR_FiveBro_Roof = Data.PCR_FiveBro_Roof;
        //        pCR_FiveBrotherDTO.PCR_FiveBro_Pool = Data.PCR_FiveBro_Pool;
        //        pCR_FiveBrotherDTO.PCR_FiveBro_Utilities = Data.PCR_FiveBro_Utilities;
        //        pCR_FiveBrotherDTO.PCR_FiveBro_Appliances = Data.PCR_FiveBro_Appliances;
        //        pCR_FiveBrotherDTO.PCR_FiveBro_Damages = Data.PCR_FiveBro_Damages;
        //        pCR_FiveBrotherDTO.PCR_FiveBro_Conveyance = Data.PCR_FiveBro_Conveyance;
        //        pCR_FiveBrotherDTO.PCR_FiveBro_Integration_Type = Data.PCR_FiveBro_Integration_Type;
        //        pCR_FiveBrotherDTO.PCR_FiveBro_IsIntegration = Data.PCR_FiveBro_IsIntegration;
        //        pCR_FiveBrotherDTO.PCR_FiveBro_IsActive = Data.PCR_FiveBro_IsActive;
        //        pCR_FiveBrotherDTO.PCR_FiveBro_IsDelete = Data.PCR_FiveBro_IsDelete;
        //        pCR_FiveBrotherDTO.UserID = Data.UserID;
        //        pCR_FiveBrotherDTO.Type = Data.Type;



        //        var AddFiveBrother = await Task.Run(() => pCR_FiveBrotherData.AddPCR_FiveBrother_Data(pCR_FiveBrotherDTO));
        //        return AddFiveBrother;

        //    }
        //    catch (Exception ex)
        //    {

        //        objdynamicobj.Add(ex.Message);
        //        return objdynamicobj;
        //    }

        //}


        ////For PCR Five Brothers Get method
        //[HttpPost]
        //[Route("GetPCRFiveBrotherData")]
        //public async Task<List<dynamic>> GetPCRFiveBrotherData()
        //{
        //    List<dynamic> objdynamicobj = new List<dynamic>();

        //    try
        //    {
        //        System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
        //        System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
        //        System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
        //        string s = reader.ReadToEnd();


        //        var Data = JsonConvert.DeserializeObject<PCR_FiveBrotherDTO>(s);

        //        PCR_FiveBrotherDTO pCR_FiveBrotherDTO = new PCR_FiveBrotherDTO();

        //        pCR_FiveBrotherDTO.PCR_FiveBro_id = Data.PCR_FiveBro_id;
        //        pCR_FiveBrotherDTO.PCR_FiveBro_WO_ID = Data.PCR_FiveBro_WO_ID;
        //        pCR_FiveBrotherDTO.Type = Data.Type;



        //        var GetFiveBrother = await Task.Run(() => pCR_FiveBrotherData.GetPCRfivebrotherDetails(pCR_FiveBrotherDTO));
        //        return GetFiveBrother;

        //    }
        //    catch (Exception ex)
        //    {

        //        objdynamicobj.Add(ex.Message);
        //        return objdynamicobj;
        //    }

        //}



        ////For PCR Utilities Post method
        //[HttpPost]
        //[Route("PostPCRUtilitiesData")]
        //public async Task<List<dynamic>> PostPCRUtilitiesData()
        //{
        //    List<dynamic> objdynamicobj = new List<dynamic>();

        //    try
        //    {
        //        System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
        //        System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
        //        System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
        //        string s = reader.ReadToEnd();


        //        var Data = JsonConvert.DeserializeObject<PCR_Utilities_MasterDTO>(s);

        //        PCR_Utilities_MasterDTO pCR_Utilities_MasterDTO = new PCR_Utilities_MasterDTO();

        //        pCR_Utilities_MasterDTO.PCR_Utilities_pkeyId = Data.PCR_Utilities_pkeyId;
        //        pCR_Utilities_MasterDTO.PCR_Utilities_MasterId = Data.PCR_Utilities_MasterId;
        //        pCR_Utilities_MasterDTO.PCR_Utilities_WO_Id = Data.PCR_Utilities_WO_Id;
        //        pCR_Utilities_MasterDTO.PCR_Utilities_ValType = Data.PCR_Utilities_ValType;
        //        pCR_Utilities_MasterDTO.PCR_Utilities_On_Arrival_Water = Data.PCR_Utilities_On_Arrival_Water;
        //        pCR_Utilities_MasterDTO.PCR_Utilities_On_Arrival_Gas = Data.PCR_Utilities_On_Arrival_Gas;
        //        pCR_Utilities_MasterDTO.PCR_Utilities_On_Arrival_Electric = Data.PCR_Utilities_On_Arrival_Electric;
        //        pCR_Utilities_MasterDTO.PCR_Utilities_On_Departure_Water = Data.PCR_Utilities_On_Departure_Water;
        //        pCR_Utilities_MasterDTO.PCR_Utilities_On_Departure_Gas = Data.PCR_Utilities_On_Departure_Gas;
        //        pCR_Utilities_MasterDTO.PCR_Utilities_On_Departure_Electric = Data.PCR_Utilities_On_Departure_Electric;
        //        pCR_Utilities_MasterDTO.PCR_Utilities_Sump_Pump_Yes = Data.PCR_Utilities_Sump_Pump_Yes;
        //        pCR_Utilities_MasterDTO.PCR_Utilities_Sump_Pump_No = Data.PCR_Utilities_Sump_Pump_No;
        //        pCR_Utilities_MasterDTO.PCR_Utilities_Sump_Pump_Missing = Data.PCR_Utilities_Sump_Pump_Missing;
        //        pCR_Utilities_MasterDTO.PCR_Utilities_Sump_Pump_Unable_To_Check = Data.PCR_Utilities_Sump_Pump_Unable_To_Check;
        //        pCR_Utilities_MasterDTO.PCR_Utilities_Sump_Pump_Commend = Data.PCR_Utilities_Sump_Pump_Commend;
        //        pCR_Utilities_MasterDTO.PCR_Utilities_Main_Breaker_And_Operational = Data.PCR_Utilities_Main_Breaker_And_Operational;
        //        pCR_Utilities_MasterDTO.PCR_Utilities_Transferred = Data.PCR_Utilities_Transferred;
        //        pCR_Utilities_MasterDTO.PCR_Utilities_Activated = Data.PCR_Utilities_Activated;
        //        pCR_Utilities_MasterDTO.PCR_Utilities_No = Data.PCR_Utilities_No;
        //        pCR_Utilities_MasterDTO.PCR_Utilities_Reason_UtilitiesNot_Transferred_Authorization_Letter_Needed = Data.PCR_Utilities_Reason_UtilitiesNot_Transferred_Authorization_Letter_Needed;
        //        pCR_Utilities_MasterDTO.PCR_Utilities_Reason_UtilitiesNot_Transferred_Must_Contact_Util_Co = Data.PCR_Utilities_Reason_UtilitiesNot_Transferred_Must_Contact_Util_Co;
        //        pCR_Utilities_MasterDTO.PCR_Utilities_Reason_UtilitiesNot_Transferred_Not_Required = Data.PCR_Utilities_Reason_UtilitiesNot_Transferred_Not_Required;
        //        pCR_Utilities_MasterDTO.PCR_Utilities_Reason_UtilitiesNot_Transferred_Other = Data.PCR_Utilities_Reason_UtilitiesNot_Transferred_Other;
        //        pCR_Utilities_MasterDTO.PCR_Utilities_Sump_Pump_Yes_Sump_Works = Data.PCR_Utilities_Sump_Pump_Yes_Sump_Works;
        //        pCR_Utilities_MasterDTO.PCR_Utilities_Sump_Pump_Yes_Sump_Tested_With_Generator = Data.PCR_Utilities_Sump_Pump_Yes_Sump_Tested_With_Generator;
        //        pCR_Utilities_MasterDTO.PCR_Utilities_Sump_Pump_Yes_Sump_Dont_Work_Bid_To_Replace = Data.PCR_Utilities_Sump_Pump_Yes_Sump_Dont_Work_Bid_To_Replace;
        //        pCR_Utilities_MasterDTO.PCR_Utilities_Sump_Pump_Yes_Sump_Dont_Work_Bid_To_Replace_Sump = Data.PCR_Utilities_Sump_Pump_Yes_Sump_Dont_Work_Bid_To_Replace_Sump;
        //        pCR_Utilities_MasterDTO.PCR_Utilities_Sump_Pump_Missing_Replace_Sump = Data.PCR_Utilities_Sump_Pump_Missing_Replace_Sump;
        //        pCR_Utilities_MasterDTO.PCR_Utilities_Sump_Pump_Unable_To_Check_Text = Data.PCR_Utilities_Sump_Pump_Unable_To_Check_Text;
        //        pCR_Utilities_MasterDTO.PCR_Utilities_Transferred_Water_Co_Name = Data.PCR_Utilities_Transferred_Water_Co_Name;
        //        pCR_Utilities_MasterDTO.PCR_Utilities_Transferred_Water_Address = Data.PCR_Utilities_Transferred_Water_Address;
        //        pCR_Utilities_MasterDTO.PCR_Utilities_Transferred_Water_Phone = Data.PCR_Utilities_Transferred_Water_Phone;
        //        pCR_Utilities_MasterDTO.PCR_Utilities_Transferred_Water_Acct = Data.PCR_Utilities_Transferred_Water_Acct;
        //        pCR_Utilities_MasterDTO.PCR_Utilities_Transferred_Gas_Co_Name = Data.PCR_Utilities_Transferred_Gas_Co_Name;
        //        pCR_Utilities_MasterDTO.PCR_Utilities_Transferred_Gas_Address = Data.PCR_Utilities_Transferred_Gas_Address;
        //        pCR_Utilities_MasterDTO.PCR_Utilities_Transferred_Gas_Phone = Data.PCR_Utilities_Transferred_Gas_Phone;
        //        pCR_Utilities_MasterDTO.PCR_Utilities_Transferred_Gas_Acct = Data.PCR_Utilities_Transferred_Gas_Acct;
        //        pCR_Utilities_MasterDTO.PCR_Utilities_Transferred_Electric_Co_Name = Data.PCR_Utilities_Transferred_Electric_Co_Name;
        //        pCR_Utilities_MasterDTO.PCR_Utilities_Transferred_Electric_Address = Data.PCR_Utilities_Transferred_Electric_Address;
        //        pCR_Utilities_MasterDTO.PCR_Utilities_Transferred_Electric_Phone = Data.PCR_Utilities_Transferred_Electric_Phone;
        //        pCR_Utilities_MasterDTO.PCR_Utilities_Transferred_Electric_Acct = Data.PCR_Utilities_Transferred_Electric_Acct;
        //        pCR_Utilities_MasterDTO.PCR_Utilities_Transferred_Activated_Water_Co_Name = Data.PCR_Utilities_Transferred_Activated_Water_Co_Name;
        //        pCR_Utilities_MasterDTO.PCR_Utilities_Transferred_Activated_Water_Address = Data.PCR_Utilities_Transferred_Activated_Water_Address;
        //        pCR_Utilities_MasterDTO.PCR_Utilities_Transferred_Activated_Water_Phone = Data.PCR_Utilities_Transferred_Activated_Water_Phone;
        //        pCR_Utilities_MasterDTO.PCR_Utilities_Transferred_Activated_Water_Acct = Data.PCR_Utilities_Transferred_Activated_Water_Acct;
        //        pCR_Utilities_MasterDTO.PCR_Utilities_Transferred_Activated_Gas_Co_Name = Data.PCR_Utilities_Transferred_Activated_Gas_Co_Name;
        //        pCR_Utilities_MasterDTO.PCR_Utilities_Transferred_Activated_Gas_Address = Data.PCR_Utilities_Transferred_Activated_Gas_Address;
        //        pCR_Utilities_MasterDTO.PCR_Utilities_Transferred_Activated_Gas_Phone = Data.PCR_Utilities_Transferred_Activated_Gas_Phone;
        //        pCR_Utilities_MasterDTO.PCR_Utilities_Transferred_Activated_Gas_Acct = Data.PCR_Utilities_Transferred_Activated_Gas_Acct;
        //        pCR_Utilities_MasterDTO.PCR_Utilities_Transferred_Activated_Electric_Co_Name = Data.PCR_Utilities_Transferred_Activated_Electric_Co_Name;
        //        pCR_Utilities_MasterDTO.PCR_Utilities_Transferred_Activated_Electric_Address = Data.PCR_Utilities_Transferred_Activated_Electric_Address;
        //        pCR_Utilities_MasterDTO.PCR_Utilities_Transferred_Activated_Electric_Phone = Data.PCR_Utilities_Transferred_Activated_Electric_Phone;
        //        pCR_Utilities_MasterDTO.PCR_Utilities_Transferred_Activated_Electric_Acct = Data.PCR_Utilities_Transferred_Activated_Electric_Acct;
        //        pCR_Utilities_MasterDTO.PCR_Utilities_IsActive = Data.PCR_Utilities_IsActive;
        //        pCR_Utilities_MasterDTO.PCR_Utilities_IsDelete = Data.PCR_Utilities_IsDelete;
        //        pCR_Utilities_MasterDTO.UserID = Data.UserID;
        //        pCR_Utilities_MasterDTO.Type = Data.Type;



        //        var AddUtilities = await Task.Run(() => pCR_Utilities_MasterData.AddPCR_Utilities_Data(pCR_Utilities_MasterDTO));
        //        return AddUtilities;

        //    }
        //    catch (Exception ex)
        //    {

        //        objdynamicobj.Add(ex.Message);
        //        return objdynamicobj;
        //    }

        //}

        ////For PCR Utilities Get method
        //[HttpPost]
        //[Route("GetPCRUtilitiesData")]
        //public async Task<List<dynamic>> GetPCRUtilitiesData()
        //{
        //    List<dynamic> objdynamicobj = new List<dynamic>();

        //    try
        //    {
        //        System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
        //        System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
        //        System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
        //        string s = reader.ReadToEnd();


        //        var Data = JsonConvert.DeserializeObject<PCR_Utilities_MasterDTO>(s);

        //        PCR_Utilities_MasterDTO pCR_Utilities_MasterDTO = new PCR_Utilities_MasterDTO();

        //        pCR_Utilities_MasterDTO.PCR_Utilities_pkeyId = Data.PCR_Utilities_pkeyId;
        //        pCR_Utilities_MasterDTO.PCR_Utilities_WO_Id = Data.PCR_Utilities_WO_Id;
        //        pCR_Utilities_MasterDTO.Type = Data.Type;



        //        var GetUtilities = await Task.Run(() => pCR_Utilities_MasterData.GetPCRUtilitisDetails(pCR_Utilities_MasterDTO));
        //        return GetUtilities;

        //    }
        //    catch (Exception ex)
        //    {

        //        objdynamicobj.Add(ex.Message);
        //        return objdynamicobj;
        //    }

        //}

        ////For PCR Violation post method
        //[HttpPost]
        //[Route("PostPCRViolationData")]
        //public async Task<List<dynamic>> PostPCRViolationData()
        //{
        //    List<dynamic> objdynamicobj = new List<dynamic>();

        //    try
        //    {
        //        System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
        //        System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
        //        System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
        //        string s = reader.ReadToEnd();


        //        var Data = JsonConvert.DeserializeObject<PCR_Violation_MasterDTO>(s);

        //        PCR_Violation_MasterDTO pCR_Violation_MasterDTO = new PCR_Violation_MasterDTO();


        //        pCR_Violation_MasterDTO.PCR_Violation_pkeyId = Data.PCR_Violation_pkeyId;
        //        pCR_Violation_MasterDTO.PCR_Violation_MasterID = Data.PCR_Violation_MasterID;
        //        pCR_Violation_MasterDTO.PCR_Violation_WO_ID = Data.PCR_Violation_WO_ID;
        //        pCR_Violation_MasterDTO.PCR_Violation_ValType = Data.PCR_Violation_ValType;
        //        pCR_Violation_MasterDTO.PCR_Violation_Any_Citation = Data.PCR_Violation_Any_Citation;
        //        pCR_Violation_MasterDTO.PCR_Violation_Describe_Citation = Data.PCR_Violation_Describe_Citation;
        //        pCR_Violation_MasterDTO.PCR_Violation_High_Vandalism_Area = Data.PCR_Violation_High_Vandalism_Area;
        //        pCR_Violation_MasterDTO.PCR_Violation_Describe_High_Vandalism_Reason = Data.PCR_Violation_Describe_High_Vandalism_Reason;
        //        pCR_Violation_MasterDTO.PCR_Violation_Any_Unusual_Circumstances = Data.PCR_Violation_Any_Unusual_Circumstances;
        //        pCR_Violation_MasterDTO.PCR_Violation_Attached_Proof_Path = Data.PCR_Violation_Attached_Proof_Path;
        //        pCR_Violation_MasterDTO.PCR_Violation_Attached_Proof_Size = Data.PCR_Violation_Attached_Proof_Size;
        //        pCR_Violation_MasterDTO.PCR_Violation_Describe = Data.PCR_Violation_Describe;
        //        pCR_Violation_MasterDTO.PCR_Violation_IsActive = Data.PCR_Violation_IsActive;
        //        pCR_Violation_MasterDTO.PCR_Violation_IsDelete = Data.PCR_Violation_IsDelete;
        //        pCR_Violation_MasterDTO.UserID = Data.UserID;
        //        pCR_Violation_MasterDTO.Type = Data.Type;



        //        var AddViolation = await Task.Run(() => pCR_Violation_MasterData.AddPCR_Violation_Data(pCR_Violation_MasterDTO));
        //        return AddViolation;

        //    }
        //    catch (Exception ex)
        //    {

        //        objdynamicobj.Add(ex.Message);
        //        return objdynamicobj;
        //    }

        //}

        ////For PCR Violation Get method
        //[HttpPost]
        //[Route("GetPCRViolationData")]
        //public async Task<List<dynamic>> GetPCRViolationData()
        //{
        //    List<dynamic> objdynamicobj = new List<dynamic>();

        //    try
        //    {
        //        System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
        //        System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
        //        System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
        //        string s = reader.ReadToEnd();


        //        var Data = JsonConvert.DeserializeObject<PCR_Violation_MasterDTO>(s);

        //        PCR_Violation_MasterDTO pCR_Violation_MasterDTO = new PCR_Violation_MasterDTO();


        //        pCR_Violation_MasterDTO.PCR_Violation_pkeyId = Data.PCR_Violation_pkeyId;
        //        pCR_Violation_MasterDTO.PCR_Violation_WO_ID = Data.PCR_Violation_WO_ID;
        //        pCR_Violation_MasterDTO.Type = Data.Type;



        //        var GetViolation = await Task.Run(() => pCR_Violation_MasterData.GetPCRPCRViolationDetails(pCR_Violation_MasterDTO));
        //        return GetViolation;

        //    }
        //    catch (Exception ex)
        //    {

        //        objdynamicobj.Add(ex.Message);
        //        return objdynamicobj;
        //    }

        //}

        ////For PCR Winterization Post method
        //[HttpPost]
        //[Route("PostPCRWinterizationData")]
        //public async Task<List<dynamic>> PostPCRWinterizationData()
        //{
        //    List<dynamic> objdynamicobj = new List<dynamic>();

        //    try
        //    {
        //        System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
        //        System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
        //        System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
        //        string s = reader.ReadToEnd();


        //        var Data = JsonConvert.DeserializeObject<PCR_WinterizationDTO>(s);

        //        PCR_WinterizationDTO pCR_WinterizationDTO = new PCR_WinterizationDTO();


        //        pCR_WinterizationDTO.PCR_Winterization_pkeyId = Data.PCR_Winterization_pkeyId;
        //        pCR_WinterizationDTO.PCR_Winterization_MasterId = Data.PCR_Winterization_MasterId;
        //        pCR_WinterizationDTO.PCR_Winterization_WO_Id = Data.PCR_Winterization_WO_Id;
        //        pCR_WinterizationDTO.PCR_Winterization_ValType = Data.PCR_Winterization_ValType;
        //        pCR_WinterizationDTO.PCR_Winterization_Upon_Arrival = Data.PCR_Winterization_Upon_Arrival;
        //        pCR_WinterizationDTO.PCR_Winterization_Compleate_This_Order_Yes = Data.PCR_Winterization_Compleate_This_Order_Yes;
        //        pCR_WinterizationDTO.PCR_Winterization_Compleate_This_Order_No = Data.PCR_Winterization_Compleate_This_Order_No;
        //        pCR_WinterizationDTO.PCR_Winterization_Compleate_This_Order_Partial = Data.PCR_Winterization_Compleate_This_Order_Partial;
        //        pCR_WinterizationDTO.PCR_Winterization_Upon_Arrival_Never_Winterized = Data.PCR_Winterization_Upon_Arrival_Never_Winterized;
        //        pCR_WinterizationDTO.PCR_Winterization_Upon_Arrival_Breached = Data.PCR_Winterization_Upon_Arrival_Breached;
        //        pCR_WinterizationDTO.PCR_Winterization_Reason_Wint_NotCompleted_Allowable = Data.PCR_Winterization_Reason_Wint_NotCompleted_Allowable;
        //        pCR_WinterizationDTO.PCR_Winterization_Reason_Wint_NotCompleted_Upon_Arrival = Data.PCR_Winterization_Reason_Wint_NotCompleted_Upon_Arrival;
        //        pCR_WinterizationDTO.PCR_Winterization_Reason_Wint_NotCompleted_Out_Season = Data.PCR_Winterization_Reason_Wint_NotCompleted_Out_Season;
        //        pCR_WinterizationDTO.PCR_Winterization_Reason_Wint_NotCompleted_TernedOff = Data.PCR_Winterization_Reason_Wint_NotCompleted_TernedOff;
        //        pCR_WinterizationDTO.PCR_Winterization_Reason_Wint_NotCompleted_Prop_Damaged = Data.PCR_Winterization_Reason_Wint_NotCompleted_Prop_Damaged;
        //        pCR_WinterizationDTO.PCR_Winterization_Reason_Wint_NotCompleted_Plumbing_Damage = Data.PCR_Winterization_Reason_Wint_NotCompleted_Plumbing_Damage;
        //        pCR_WinterizationDTO.PCR_Winterization_Reason_Wint_NotCompleted_Plumbing_IsMissing = Data.PCR_Winterization_Reason_Wint_NotCompleted_Plumbing_IsMissing;
        //        pCR_WinterizationDTO.PCR_Winterization_Reason_Wint_NotCompleted_AllReady_Winterized = Data.PCR_Winterization_Reason_Wint_NotCompleted_AllReady_Winterized;
        //        pCR_WinterizationDTO.PCR_Winterization_Reason_Wint_NotCompleted_Common_Water_Line = Data.PCR_Winterization_Reason_Wint_NotCompleted_Common_Water_Line;
        //        pCR_WinterizationDTO.PCR_Winterization_Reason_Wint_NotCompleted_Maintaining_Utilities = Data.PCR_Winterization_Reason_Wint_NotCompleted_Maintaining_Utilities;
        //        pCR_WinterizationDTO.PCR_Winterization_Reason_Wint_NotCompleted_Other = Data.PCR_Winterization_Reason_Wint_NotCompleted_Other;
        //        pCR_WinterizationDTO.PCR_Winterization_Heating_System = Data.PCR_Winterization_Heating_System;
        //        pCR_WinterizationDTO.PCR_Winterization_Heating_System_Well = Data.PCR_Winterization_Heating_System_Well;
        //        pCR_WinterizationDTO.PCR_Winterization_Radiant_Heat_System = Data.PCR_Winterization_Radiant_Heat_System;
        //        pCR_WinterizationDTO.PCR_Winterization_Radiant_Heat_System_Well = Data.PCR_Winterization_Radiant_Heat_System_Well;
        //        pCR_WinterizationDTO.PCR_Winterization_Steam_Heat_System = Data.PCR_Winterization_Steam_Heat_System;
        //        pCR_WinterizationDTO.PCR_Winterization_Steam_Heat_System_Well = Data.PCR_Winterization_Steam_Heat_System_Well;
        //        pCR_WinterizationDTO.PCR_Winterization_Posted_Signs = Data.PCR_Winterization_Posted_Signs;
        //        pCR_WinterizationDTO.PCR_Winterization_Common_Water_Line = Data.PCR_Winterization_Common_Water_Line;
        //        pCR_WinterizationDTO.PCR_Winterization_AntiFreeze_Toilet = Data.PCR_Winterization_AntiFreeze_Toilet;
        //        pCR_WinterizationDTO.PCR_Winterization_Water_Heater_Drained = Data.PCR_Winterization_Water_Heater_Drained;
        //        pCR_WinterizationDTO.PCR_Winterization_Water_Off_At_Curb = Data.PCR_Winterization_Water_Off_At_Curb;
        //        pCR_WinterizationDTO.PCR_Winterization_Blown_All_Lines = Data.PCR_Winterization_Blown_All_Lines;
        //        pCR_WinterizationDTO.PCR_Winterization_System_Held_Pressure = Data.PCR_Winterization_System_Held_Pressure;
        //        pCR_WinterizationDTO.PCR_Winterization_Disconnected_Water_Meter_Yes = Data.PCR_Winterization_Disconnected_Water_Meter_Yes;
        //        pCR_WinterizationDTO.PCR_Winterization_Disconnected_Water_Meter_No_Shut_Valve = Data.PCR_Winterization_Disconnected_Water_Meter_No_Shut_Valve;
        //        pCR_WinterizationDTO.PCR_Winterization_Disconnected_Water_Meter_No_Common_Water_Line = Data.PCR_Winterization_Disconnected_Water_Meter_No_Common_Water_Line;
        //        pCR_WinterizationDTO.PCR_Winterization_Disconnected_Water_Meter_No_Unable_To_Locate = Data.PCR_Winterization_Disconnected_Water_Meter_No_Unable_To_Locate;
        //        pCR_WinterizationDTO.PCR_Winterization_Disconnected_Water_Meter_No_Prohibited_Ordinance = Data.PCR_Winterization_Disconnected_Water_Meter_No_Prohibited_Ordinance;
        //        pCR_WinterizationDTO.PCR_Winterization_Disconnected_Water_Meter_No_Others = Data.PCR_Winterization_Disconnected_Water_Meter_No_Others;
        //        pCR_WinterizationDTO.PCR_Winterization_Radiant_Heat_Boiler_Drained = Data.PCR_Winterization_Radiant_Heat_Boiler_Drained;
        //        pCR_WinterizationDTO.PCR_Winterization_Radiant_Heat_Zone_Valves_Opened = Data.PCR_Winterization_Radiant_Heat_Zone_Valves_Opened;
        //        pCR_WinterizationDTO.PCR_Winterization_Radiant_Heat_AntiFreeze_Boiler = Data.PCR_Winterization_Radiant_Heat_AntiFreeze_Boiler;
        //        pCR_WinterizationDTO.PCR_Winterization_If_Well_System_Breaker_Off = Data.PCR_Winterization_If_Well_System_Breaker_Off;
        //        pCR_WinterizationDTO.PCR_Winterization_If_Well_System_Pressure_Tank_Drained = Data.PCR_Winterization_If_Well_System_Pressure_Tank_Drained;
        //        pCR_WinterizationDTO.PCR_Winterization_If_Well_System_Supply_Line_Disconnect = Data.PCR_Winterization_If_Well_System_Supply_Line_Disconnect;
        //        pCR_WinterizationDTO.PCR_Winterization_Interior_Main_Valve_Shut_Off = Data.PCR_Winterization_Interior_Main_Valve_Shut_Off;
        //        pCR_WinterizationDTO.PCR_Winterization_Interior_Main_Valve_Reason = Data.PCR_Winterization_Interior_Main_Valve_Reason;
        //        pCR_WinterizationDTO.PCR_Winterization_Interior_Main_Valve_Fire_Suppression_System = Data.PCR_Winterization_Interior_Main_Valve_Fire_Suppression_System;
        //        pCR_WinterizationDTO.PCR_Winterization_To_Bid = Data.PCR_Winterization_To_Bid;
        //        pCR_WinterizationDTO.PCR_Winterization_To_Bit_Text = Data.PCR_Winterization_To_Bit_Text;
        //        pCR_WinterizationDTO.PCR_Winterization_Winterize = Data.PCR_Winterization_Winterize;
        //        pCR_WinterizationDTO.PCR_Winterization_Thaw = Data.PCR_Winterization_Thaw;
        //        pCR_WinterizationDTO.PCR_Winterization_Description = Data.PCR_Winterization_Description;
        //        pCR_WinterizationDTO.PCR_Winterization_System_Type = Data.PCR_Winterization_System_Type;
        //        pCR_WinterizationDTO.PCR_Winterization_Reason = Data.PCR_Winterization_Reason;
        //        pCR_WinterizationDTO.PCR_Winterization_Amount = Data.PCR_Winterization_Amount;
        //        pCR_WinterizationDTO.PCR_Winterization_Winterize_Men = Data.PCR_Winterization_Winterize_Men;
        //        pCR_WinterizationDTO.PCR_Winterization_Winterize_Hrs = Data.PCR_Winterization_Winterize_Hrs;
        //        pCR_WinterizationDTO.PCR_Winterization_IsActive = Data.PCR_Winterization_IsActive;
        //        pCR_WinterizationDTO.PCR_Winterization_IsDelete = Data.PCR_Winterization_IsDelete;
        //        pCR_WinterizationDTO.UserID = Data.UserID;
        //        pCR_WinterizationDTO.Type = Data.Type;



        //        var AddWinterization = await Task.Run(() => pCR_WinterizationData.AddPCR_Winterization_Data(pCR_WinterizationDTO));
        //        return AddWinterization;

        //    }
        //    catch (Exception ex)
        //    {

        //        objdynamicobj.Add(ex.Message);
        //        return objdynamicobj;
        //    }

        //}

        ////For PCR Winterization Get method
        //[HttpPost]
        //[Route("GetPCRWinterizationData")]
        //public async Task<List<dynamic>> GetPCRWinterizationData()
        //{
        //    List<dynamic> objdynamicobj = new List<dynamic>();

        //    try
        //    {
        //        System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
        //        System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
        //        System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
        //        string s = reader.ReadToEnd();


        //        var Data = JsonConvert.DeserializeObject<PCR_WinterizationDTO>(s);

        //        PCR_WinterizationDTO pCR_WinterizationDTO = new PCR_WinterizationDTO();


        //        pCR_WinterizationDTO.PCR_Winterization_pkeyId = Data.PCR_Winterization_pkeyId;
        //        pCR_WinterizationDTO.PCR_Winterization_WO_Id = Data.PCR_Winterization_WO_Id;
        //        pCR_WinterizationDTO.Type = Data.Type;



        //        var GetWinterization = await Task.Run(() => pCR_WinterizationData.AddPCR_Winterization_Data(pCR_WinterizationDTO));
        //        return GetWinterization;

        //    }
        //    catch (Exception ex)
        //    {

        //        objdynamicobj.Add(ex.Message);
        //        return objdynamicobj;
        //    }

        //}

        ////For PCR Yard Post method
        //[HttpPost]
        //[Route("PostPCRYardData")]
        //public async Task<List<dynamic>> PostPCRYardData()
        //{
        //    List<dynamic> objdynamicobj = new List<dynamic>();

        //    try
        //    {
        //        System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
        //        System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
        //        System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
        //        string s = reader.ReadToEnd();


        //        var Data = JsonConvert.DeserializeObject<PCR_Yard_MaintenanceDTO>(s);

        //        PCR_Yard_MaintenanceDTO pCR_Yard_MaintenanceDTO = new PCR_Yard_MaintenanceDTO();


        //        pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_pkeyId = Data.PCR_Yard_Maintenance_pkeyId;
        //        pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_MasterId = Data.PCR_Yard_Maintenance_MasterId;
        //        pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_WO_Id = Data.PCR_Yard_Maintenance_WO_Id;
        //        pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_ValType = Data.PCR_Yard_Maintenance_ValType;
        //        pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Grass_Cut_Completed = Data.PCR_Yard_Maintenance_Grass_Cut_Completed;
        //        pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Lot_Size = Data.PCR_Yard_Maintenance_Lot_Size;
        //        pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Cuttable_Area = Data.PCR_Yard_Maintenance_Cuttable_Area;
        //        pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Bit_To_Cut_Grass_Lot_Dimension_Lenght = Data.PCR_Yard_Maintenance_Bit_To_Cut_Grass_Lot_Dimension_Lenght;
        //        pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Bit_To_Cut_Grass_Lot_Dimension_Width = Data.PCR_Yard_Maintenance_Bit_To_Cut_Grass_Lot_Dimension_Width;
        //        pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Bit_To_Cut_Grass_Lot_Dimension_Height = Data.PCR_Yard_Maintenance_Bit_To_Cut_Grass_Lot_Dimension_Height;
        //        pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Bit_To_Cut_Grass_Bid_For_Inital_Cut = Data.PCR_Yard_Maintenance_Bit_To_Cut_Grass_Bid_For_Inital_Cut;
        //        pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Bit_To_Cut_Grass_Reason_For_Inital_Cut = Data.PCR_Yard_Maintenance_Bit_To_Cut_Grass_Reason_For_Inital_Cut;
        //        pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Bid_Recut = Data.PCR_Yard_Maintenance_Bid_Recut;
        //        pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Reason_For_Recut = Data.PCR_Yard_Maintenance_Reason_For_Recut;
        //        pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Trees_Cut_Back_Order = Data.PCR_Yard_Maintenance_Trees_Cut_Back_Order;
        //        pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Arrival_Shrubs_Touching_House = Data.PCR_Yard_Maintenance_Arrival_Shrubs_Touching_House;
        //        pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Arrival_Trees_Touching_House = Data.PCR_Yard_Maintenance_Arrival_Trees_Touching_House;
        //        pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Depature_Trees = Data.PCR_Yard_Maintenance_Depature_Trees;
        //        pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Grass_Maintained_HOA = Data.PCR_Yard_Maintenance_Grass_Maintained_HOA;
        //        pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_No_Needed_Desert_Landscaping = Data.PCR_Yard_Maintenance_No_Needed_Desert_Landscaping;
        //        pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Debrnis_The_Way_Of_Grass = Data.PCR_Yard_Maintenance_Debrnis_The_Way_Of_Grass;
        //        pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Unable_To_Cut_Per_Regs = Data.PCR_Yard_Maintenance_Unable_To_Cut_Per_Regs;
        //        pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Not_Required = Data.PCR_Yard_Maintenance_Not_Required;
        //        pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Not_Needed_Dead_Grass = Data.PCR_Yard_Maintenance_Not_Needed_Dead_Grass;
        //        pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Not_Needed_No_Grass = Data.PCR_Yard_Maintenance_Not_Needed_No_Grass;
        //        pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Weather = Data.PCR_Yard_Maintenance_Weather;
        //        pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Bid_To_Trim_Shrubs_Dimensions_Length = Data.PCR_Yard_Maintenance_Bid_To_Trim_Shrubs_Dimensions_Length;
        //        pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Bid_To_Trim_Shrubs_Dimensions_Width = Data.PCR_Yard_Maintenance_Bid_To_Trim_Shrubs_Dimensions_Width;
        //        pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Bid_To_Trim_Shrubs_Dimensions_Height = Data.PCR_Yard_Maintenance_Bid_To_Trim_Shrubs_Dimensions_Height;
        //        pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Bid_To_Trim_Shrubs_Quantity = Data.PCR_Yard_Maintenance_Bid_To_Trim_Shrubs_Quantity;
        //        pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Bid_To_Trim_Shrubs_Unit_Price = Data.PCR_Yard_Maintenance_Bid_To_Trim_Shrubs_Unit_Price;
        //        pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Bid_To_Trim_Shrubs_Bid_Amount = Data.PCR_Yard_Maintenance_Bid_To_Trim_Shrubs_Bid_Amount;
        //        pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Bid_To_Trim_Shrubs_Location = Data.PCR_Yard_Maintenance_Bid_To_Trim_Shrubs_Location;
        //        pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Bid_To_Trim_Shrubs_Reasons_Touching_House = Data.PCR_Yard_Maintenance_Bid_To_Trim_Shrubs_Reasons_Touching_House;
        //        pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Bid_To_Trim_Shrubs_Reasons_Touching_Other_Structure = Data.PCR_Yard_Maintenance_Bid_To_Trim_Shrubs_Reasons_Touching_Other_Structure;
        //        pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Bid_To_Trim_Shrubs_Reasons_Within_Street_View = Data.PCR_Yard_Maintenance_Bid_To_Trim_Shrubs_Reasons_Within_Street_View;
        //        pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Bid_To_Trim_Shrubs_Reasons_Affecting_Fencing = Data.PCR_Yard_Maintenance_Bid_To_Trim_Shrubs_Reasons_Affecting_Fencing;
        //        pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Causing_Damage = Data.PCR_Yard_Maintenance_Causing_Damage;
        //        pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Describe = Data.PCR_Yard_Maintenance_Describe;
        //        pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_Were_Trimmed_Insurer_Guidlines = Data.PCR_Yard_Maintenance_Were_Trimmed_Insurer_Guidlines;
        //        pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_IsActive = Data.PCR_Yard_Maintenance_IsActive;
        //        pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_IsDelete = Data.PCR_Yard_Maintenance_IsDelete;
        //        pCR_Yard_MaintenanceDTO.UserID = Data.UserID;
        //        pCR_Yard_MaintenanceDTO.Type = Data.Type;



        //        var AddYard = await Task.Run(() => pCR_Yard_MaintenanceData.AddPCR_Yard_Maintenance_Data(pCR_Yard_MaintenanceDTO));
        //        return AddYard;

        //    }
        //    catch (Exception ex)
        //    {

        //        objdynamicobj.Add(ex.Message);
        //        return objdynamicobj;
        //    }

        //}


        ////For PCR Yard Get method
        //[HttpPost]
        //[Route("GetPCRYardData")]
        //public async Task<List<dynamic>> GetPCRYardData()
        //{
        //    List<dynamic> objdynamicobj = new List<dynamic>();

        //    try
        //    {
        //        System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
        //        System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
        //        System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
        //        string s = reader.ReadToEnd();


        //        var Data = JsonConvert.DeserializeObject<PCR_Yard_MaintenanceDTO>(s);

        //        PCR_Yard_MaintenanceDTO pCR_Yard_MaintenanceDTO = new PCR_Yard_MaintenanceDTO();


        //        pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_pkeyId = Data.PCR_Yard_Maintenance_pkeyId;
        //        pCR_Yard_MaintenanceDTO.PCR_Yard_Maintenance_WO_Id = Data.PCR_Yard_Maintenance_WO_Id;
        //        pCR_Yard_MaintenanceDTO.Type = Data.Type;



        //        var GetYard = await Task.Run(() => pCR_Yard_MaintenanceData.GetPCYardDetails(pCR_Yard_MaintenanceDTO));
        //        return GetYard;

        //    }
        //    catch (Exception ex)
        //    {

        //        objdynamicobj.Add(ex.Message);
        //        return objdynamicobj;
        //    }

        //}

        ////For PCR Conveyance Post method
        //[HttpPost]
        //[Route("PostPCRConveyanceData")]
        //public async Task<List<dynamic>> PostPCRConveyanceData()
        //{
        //    List<dynamic> objdynamicobj = new List<dynamic>();

        //    try
        //    {
        //        System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
        //        System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
        //        System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
        //        string s = reader.ReadToEnd();


        //        var Data = JsonConvert.DeserializeObject<PCR_Conveyance_MasterDTO>(s);

        //        PCR_Conveyance_MasterDTO pCR_Conveyance_MasterDTO = new PCR_Conveyance_MasterDTO();


        //        pCR_Conveyance_MasterDTO.PCR_Conveyance_pkeyID = Data.PCR_Conveyance_pkeyID;
        //        pCR_Conveyance_MasterDTO.PCR_Conveyance_MasterID = Data.PCR_Conveyance_MasterID;
        //        pCR_Conveyance_MasterDTO.PCR_Conveyance_Wo_ID = Data.PCR_Conveyance_Wo_ID;
        //        pCR_Conveyance_MasterDTO.PCR_Conveyance_ValType = Data.PCR_Conveyance_ValType;
        //        pCR_Conveyance_MasterDTO.PCR_Conveyance_Work_Order_Instruction = Data.PCR_Conveyance_Work_Order_Instruction;
        //        pCR_Conveyance_MasterDTO.PCR_Conveyance_Secured_Per_Guidelines = Data.PCR_Conveyance_Secured_Per_Guidelines;
        //        pCR_Conveyance_MasterDTO.PCR_Conveyance_Additional_Damage = Data.PCR_Conveyance_Additional_Damage;
        //        pCR_Conveyance_MasterDTO.PCR_Conveyance_Bid_On_This_Visit = Data.PCR_Conveyance_Bid_On_This_Visit;
        //        pCR_Conveyance_MasterDTO.PCR_Conveyance_Need_Maintenance = Data.PCR_Conveyance_Need_Maintenance;
        //        pCR_Conveyance_MasterDTO.PCR_Conveyance_Broom_Swept_Condition = Data.PCR_Conveyance_Broom_Swept_Condition;
        //        pCR_Conveyance_MasterDTO.PCR_Conveyance_HUD_Guidelines = Data.PCR_Conveyance_HUD_Guidelines;
        //        pCR_Conveyance_MasterDTO.PCR_Conveyance_Accidental_Entry = Data.PCR_Conveyance_Accidental_Entry;
        //        pCR_Conveyance_MasterDTO.PCR_Conveyance_Features_Are_Secure = Data.PCR_Conveyance_Features_Are_Secure;
        //        pCR_Conveyance_MasterDTO.PCR_Conveyance_In_Place_Operational = Data.PCR_Conveyance_In_Place_Operational;
        //        pCR_Conveyance_MasterDTO.PCR_Conveyance_Property_Of_Animals = Data.PCR_Conveyance_Property_Of_Animals;
        //        pCR_Conveyance_MasterDTO.PCR_Conveyance_Intact_Secure = Data.PCR_Conveyance_Intact_Secure;
        //        pCR_Conveyance_MasterDTO.PCR_Conveyance_Water_Instruction = Data.PCR_Conveyance_Water_Instruction;
        //        pCR_Conveyance_MasterDTO.PCR_Conveyance_Free_Of_Water = Data.PCR_Conveyance_Free_Of_Water;
        //        pCR_Conveyance_MasterDTO.PCR_Conveyance_Moisture_has_Eliminated = Data.PCR_Conveyance_Moisture_has_Eliminated;
        //        pCR_Conveyance_MasterDTO.PCR_Conveyance_Orderdinance = Data.PCR_Conveyance_Orderdinance;
        //        pCR_Conveyance_MasterDTO.PCR_Conveyance_Uneven = Data.PCR_Conveyance_Uneven;
        //        pCR_Conveyance_MasterDTO.PCR_Conveyance_Conveyance_Condition = Data.PCR_Conveyance_Conveyance_Condition;
        //        pCR_Conveyance_MasterDTO.PCR_Conveyance_Damage = Data.PCR_Conveyance_Damage;
        //        pCR_Conveyance_MasterDTO.PCR_Conveyance_Debris = Data.PCR_Conveyance_Debris;
        //        pCR_Conveyance_MasterDTO.PCR_Conveyance_Repairs = Data.PCR_Conveyance_Repairs;
        //        pCR_Conveyance_MasterDTO.PCR_Conveyance_Hazards = Data.PCR_Conveyance_Hazards;
        //        pCR_Conveyance_MasterDTO.PCR_Conveyance_Other = Data.PCR_Conveyance_Other;
        //        pCR_Conveyance_MasterDTO.PCR_Conveyance_Describe = Data.PCR_Conveyance_Describe;
        //        pCR_Conveyance_MasterDTO.PCR_Conveyance_Note = Data.PCR_Conveyance_Note;
        //        pCR_Conveyance_MasterDTO.PCR_Conveyance_Work_Order_Instruction_Reason = Data.PCR_Conveyance_Work_Order_Instruction_Reason;
        //        pCR_Conveyance_MasterDTO.PCR_Conveyance_Secured_Per_Guidelines_Reason = Data.PCR_Conveyance_Secured_Per_Guidelines_Reason;
        //        pCR_Conveyance_MasterDTO.PCR_Conveyance_HUD_Guidelines_Reasponce = Data.PCR_Conveyance_HUD_Guidelines_Reasponce;
        //        pCR_Conveyance_MasterDTO.PCR_Conveyance_IsActive = Data.PCR_Conveyance_IsActive;
        //        pCR_Conveyance_MasterDTO.PCR_Conveyance_IsDelete = Data.PCR_Conveyance_IsDelete;
        //        pCR_Conveyance_MasterDTO.UserID = Data.UserID;
        //        pCR_Conveyance_MasterDTO.Type = Data.Type;



        //        var AddConveyance = await Task.Run(() => pCR_Conveyance_MasterData.AddPCR_Conveyance_Data(pCR_Conveyance_MasterDTO));
        //        return AddConveyance;

        //    }
        //    catch (Exception ex)
        //    {

        //        objdynamicobj.Add(ex.Message);
        //        return objdynamicobj;
        //    }

        //}


        ////For PCR Conveyance Get method
        //[HttpPost]
        //[Route("GetPCRConveyanceData")]
        //public async Task<List<dynamic>> GetPCRConveyanceData()
        //{
        //    List<dynamic> objdynamicobj = new List<dynamic>();

        //    try
        //    {
        //        System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
        //        System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
        //        System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
        //        string s = reader.ReadToEnd();


        //        var Data = JsonConvert.DeserializeObject<PCR_Conveyance_MasterDTO>(s);

        //        PCR_Conveyance_MasterDTO pCR_Conveyance_MasterDTO = new PCR_Conveyance_MasterDTO();


        //        pCR_Conveyance_MasterDTO.PCR_Conveyance_pkeyID = Data.PCR_Conveyance_pkeyID;
        //        pCR_Conveyance_MasterDTO.PCR_Conveyance_Wo_ID = Data.PCR_Conveyance_Wo_ID;
        //        pCR_Conveyance_MasterDTO.Type = Data.Type;



        //        var GetConveyance = await Task.Run(() => pCR_Conveyance_MasterData.GetPCRConveyanceDetails(pCR_Conveyance_MasterDTO));
        //        return GetConveyance;

        //    }
        //    catch (Exception ex)
        //    {

        //        objdynamicobj.Add(ex.Message);
        //        return objdynamicobj;
        //    }

        //}
    }
}
