﻿using Avigma.Repository.Lib;
using IPL.Models;
using IPL.Repository.data;
using IPL.Repository.Lib;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace IPL.Controllers
{
    [RoutePrefix("api/Geometary")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]  
    public class GeometaryController : BaseController
    {
        Log log = new Log();
        Master_Postal_BoundriesData postalBoundaryData= new Master_Postal_BoundriesData();

        [Authorize]
        [HttpPost]
        [Route("GetPostalCodeBoundary")]
        public async Task<RapidapiPostalBoundryResponse> GetPostalCodeBoundary()
        {
            var response = new RapidapiPostalBoundryResponse();
            try
            {
                Contractor_Paid_InvoiceData contractor_Paid_InvoiceData = new Contractor_Paid_InvoiceData();
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string postalCodes = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<List<string>>(postalCodes);
                
                var postalBoundries = await Task.Run(() => postalBoundaryData.GetPostalBoundriesDetails(Data));
                response.features = new List<Feature>();
                response.features.AddRange(postalBoundries);
                return response;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                response.error =ex.Message;
                return response;
            }
        }
    }
}
