﻿using IPL.Models;
using IPL.Repository.data;
using Avigma.Repository.Lib;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using IPLApp.Models;
using IPLApp.Repository.data;

namespace IPL.Controllers
{
    [RoutePrefix("api/UserActivityTracting")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UserActivityTractingController : BaseController
    {
        UserActivityTrackingData UserActivityTrackingData = new UserActivityTrackingData();
        UserMasterData userMasterData = new UserMasterData();
        Log log = new Log();
        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("PostUserActivityTrackingDetails")]
        public async Task<List<dynamic>> PostUserActivityTrackingDetails()
        {

            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                //log.logDebugMessage(s);

                var Data = JsonConvert.DeserializeObject<UserActivityTrackingDTO>(s);
                Data.Type = 1;

                if (LoggedInUSerId == 0)
                {
                    Data.UserID = Convert.ToInt64(Data.User_Track_UserID);
                }
                else
                {
                    Data.UserID = LoggedInUSerId;
                    Data.User_Track_UserID = LoggedInUSerId;

                }
                Data.User_Track_IsActive = true;
                Data.User_Track_IsDelete = false;
                string strURL = System.Configuration.ConfigurationManager.AppSettings["IntegrationURL"] + "download";
                FileUploadDownload fileUploadDownload = new FileUploadDownload();
                string ReqData = "{\r\n \"FileName\": \"" + Data.User_Track_File_Name + "\",\r\n \"FolderName\": \"" + Data.User_Track_Folder_Name + "\"\r\n}";
                string strPath = fileUploadDownload.CallAPI(strURL, ReqData);
                Data.User_Track_File_Path = strPath;
                var AddUserTracking = await Task.Run(() => UserActivityTrackingData.AddUserActivityTrackingDetails(Data));

                return AddUserTracking;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }



        [Authorize]
        [HttpPost]
        [Route("GetUserActivityTrackingDetails")]
        public async Task<List<dynamic>> GetUserActivityTrackingDetails()
        {

            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<UserActivityTrackingDTO>(s);

                var GetUserTracking = await Task.Run(() => UserActivityTrackingData.GetUserActivityTrackingDetails(Data));

                return GetUserTracking;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }



        [Authorize]
        [HttpPost]
        [Route("GetActivityTrackingFilterDetails")]
        public async Task<List<dynamic>> GetActivityTrackingFilterDetails()
        {

            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                //log.logDebugMessage(s);
                var Data = JsonConvert.DeserializeObject<UserActivityTrackingFilterDTO>(s);

                var GetUserTracking = await Task.Run(() => UserActivityTrackingData.GetActivityTrackingFilterDetails(Data));

                return GetUserTracking;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("DeleteActivityTrackingPhoto")]
        public async Task<List<dynamic>> DeleteActivityTrackingPhoto()
        {

            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
               // log.logDebugMessage(s);
                var Data = JsonConvert.DeserializeObject<UserActivityTrackingDTO>(s);
                Data.UserID = LoggedInUSerId;
                Data.Type = 4;
                var DelUserTracking = await Task.Run(() => UserActivityTrackingData.AddUserActivityTrackingDetails(Data));

                return DelUserTracking;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        [Authorize]
        [HttpPost]
        [Route("GetUserFilterList")]
        public async Task<List<dynamic>> GetUserFilterList()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<UserFiltermasterDTO>(s);
                UserFiltermasterDTO userFiltermasterDTO = new UserFiltermasterDTO();
                Data.Type = 6;
                Data.UserID = LoggedInUSerId;
                objdynamicobj = await Task.Run(() => userMasterData.GetUserfilterDetails(Data));





            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

            return objdynamicobj;

        }
    }
}
