﻿using IPLApp.Models;
using IPLApp.Repository.data;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using Avigma.Repository.Lib;

namespace IPL.Controllers
{
    [RoutePrefix("api/Access")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class Access_logController : BaseController
    {
        Access_log_MasterData access_log_MasterData = new Access_log_MasterData();
        Log log = new Log();



        [Authorize]
        [HttpPost]
        [Route("PostAccessLogData")]

        public async Task<List<dynamic>> PostAccessLogData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Access_log_MasterDTO>(s);
                Data.Alm_userID = LoggedInUSerId;



                var GetLog = await Task.Run(() => access_log_MasterData.AddAccess_log_Master(Data));
                return GetLog;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }




        [Authorize]
        [HttpPost]
        [Route("GetAccessLogData")]

        public async Task<List<dynamic>> GetAccessLogData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Access_log_MasterDTO>(s);



                var GetLog = await Task.Run(() => access_log_MasterData.Get_Access_log_MasterDetails(Data));
                return GetLog;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

        [Authorize]
        [HttpPost]
        [Route("PostNewAccessLogData")]

        public async Task<List<dynamic>> PostNewAccessLogData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<NewAccessLogMaster>(s);
                Data.Access_UserID = LoggedInUSerId;

                var GetLog = await Task.Run(() => access_log_MasterData.AddNewAccessLogMaster(Data));
                return GetLog;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }


        [Authorize]
        [HttpPost]
        [Route("GetNewAccessLogData")]

        public async Task<List<dynamic>> GetNewAccessLogData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<NewAccessLogMaster>(s);



                var GetLog = await Task.Run(() => access_log_MasterData.Get_New_Access_log_MasterDetails(Data));
                return GetLog;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("GetAccessUserLogNewMaster")]

        public async Task<List<dynamic>> GetAccessUserLogNewMaster()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Access_User_Log_New_Master>(s);



                var GetLog = await Task.Run(() => access_log_MasterData.Get_AccessUserLogNewMaster(Data));
                return GetLog;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }
    }
}
