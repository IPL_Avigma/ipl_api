﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using Avigma.Models;
using Avigma.Repository.Lib;
using IPL.Models;
using IPL.Repository.data;
using IPLApp.Models;
using IPLApp.Repository.data;
using Newtonsoft.Json;


namespace IPL.Controllers
{
    [RoutePrefix("api/WOCTASK")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class WOCTASKController : BaseController
    {
        UOM_Master_Data uOM_Master_Data = new UOM_Master_Data();
        TaskBidMasterData taskBidMasterData = new TaskBidMasterData();
        Task_Invoice_MasterData task_Invoice_MasterData = new Task_Invoice_MasterData();
        TaskDamageMasterData taskDamageMasterData = new TaskDamageMasterData();
        TaskViolationMasterData taskViolationMasterData = new TaskViolationMasterData();
        TaskHazardMasterData taskHazardMasterData = new TaskHazardMasterData();
        ClientResultDataMaster clientResultDataMaster = new ClientResultDataMaster();
        Invoice_ContractorData invoice_ContractorData = new Invoice_ContractorData();
        InstructionMasterData instructionMasterData = new InstructionMasterData();
        Custom_PhotoLabel_MasterData custom_PhotoLabel_MasterData = new Custom_PhotoLabel_MasterData();
        WorkOrder_CustomPhotoLabelData workOrder_CustomPhotoLabelData = new WorkOrder_CustomPhotoLabelData();
        Instruction_Master_ChildData instruction_Master_ChildData = new Instruction_Master_ChildData();
        ApplianceMasterData applianceMasterData = new ApplianceMasterData();
        Contractor_Client_Expense_Payment_Data contractor_Client_Expense_Payment = new Contractor_Client_Expense_Payment_Data();
        Contractor_ScoreCard_SettingData contractor_ScoreCard_SettingData = new Contractor_ScoreCard_SettingData();
        Custom_PhotoLabel_Group_MasterData custom_PhotoLabel_Group_MasterData = new Custom_PhotoLabel_Group_MasterData(); //Added By dipali
        WorkOrder_Office_DocumentData workOrder_Office_DocumentData = new WorkOrder_Office_DocumentData();
        Filter_Admin_InstructionData filter_Admin_InstructionData = new Filter_Admin_InstructionData();
        Filter_Admin_UOMData filter_Admin_UOMData = new Filter_Admin_UOMData();
        Filter_Admin_TaskData filter_Admin_TaskData = new Filter_Admin_TaskData();
        Filter_Admin_RushData filter_Admin_RushData = new Filter_Admin_RushData();
        Filter_Admin_CategoryData filter_Admin_CategoryData = new Filter_Admin_CategoryData();
        Filter_Admin_CustPhLblData filter_Admin_CustPhLblData = new Filter_Admin_CustPhLblData();
        ContractorClientPrintData contractorClientPrintData = new ContractorClientPrintData();
        Appliance_Name_MasterData appliance_Name_MasterData = new Appliance_Name_MasterData();
        PhotoHeaderTemplateData photoHeaderTemplateData = new PhotoHeaderTemplateData();
        UserMasterData userMasterData = new UserMasterData();
        Log log = new Log();


        //post Uom details
        [Authorize]
        [HttpPost]
        [Route("PostUomData")]
        public async Task<List<dynamic>> PostUomData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            UOM_MasterDTO uOM_MasterDTO = new UOM_MasterDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<UOM_MasterDTO>(s);
                Data.UserID = LoggedInUSerId;

                uOM_MasterDTO.UOM_pkeyId = Data.UOM_pkeyId;
                uOM_MasterDTO.UOM_Name = Data.UOM_Name;
                uOM_MasterDTO.UOM_IsActive = Data.UOM_IsActive;
                uOM_MasterDTO.UOM_IsDelete = Data.UOM_IsDelete;
                uOM_MasterDTO.UserID = Data.UserID;
                uOM_MasterDTO.Type = Data.Type;

                var AddUom = await Task.Run(() => uOM_Master_Data.AddUOMMasterData(uOM_MasterDTO));
                return AddUom;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        // get UOM Details
        [Authorize]
        [HttpPost]
        [Route("GetUomData")]
        public async Task<List<dynamic>> GetUomData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            UOM_MasterDTO uOM_MasterDTO = new UOM_MasterDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<UOM_MasterDTO>(s);
                //Data.UserID = LoggedInUSerId;

                //uOM_MasterDTO.UOM_pkeyId = Data.UOM_pkeyId;
                //uOM_MasterDTO.Type = Data.Type;
                //uOM_MasterDTO.UserID = Data.UserID;
                //var GetUom = await Task.Run(() => uOM_Master_Data.GetUOM_MasterDetails(uOM_MasterDTO));
                //return GetUom;


                switch (Data.Type)
                {
                    case 2:
                        {
                            Data.UserID = LoggedInUSerId;
                            objdynamicobj = await Task.Run(() => uOM_Master_Data.GetUOM_MasterDetails(Data));
                            break;
                        }
                    case 1:
                        {
                            Data.UserID = LoggedInUSerId;
                            objdynamicobj = await Task.Run(() => uOM_Master_Data.GetUOM_MasterDetails(Data));

                            break;
                        }
                    case 3:
                        {
                            Data.UserID = LoggedInUSerId;
                            objdynamicobj = await Task.Run(() => uOM_Master_Data.GetUomFilterDetails(Data));

                            break;
                        }
                }

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
            return objdynamicobj;

        }


        //post Task Bid details
        [Authorize]
        [HttpPost]
        [Route("PostTaskBidData")]
        public async Task<List<dynamic>> PostTaskBidData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<TaskBidMasterRootObject>(s);
                Data.UserID = Convert.ToInt32(LoggedInUSerId);



                var AddBidData = await Task.Run(() => clientResultDataMaster.ClientResultBidTaskPost(Data));
                return AddBidData;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //Get Task Bid details
        [Authorize]
        [HttpPost]
        [Route("GetTaskBidData")]
        public async Task<List<dynamic>> GetTaskBidData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            TaskBidMasterDTO taskBidMasterDTO = new TaskBidMasterDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<TaskBidMasterDTO>(s);
                Data.UserID = LoggedInUSerId;

                taskBidMasterDTO.Task_Bid_pkeyID = Data.Task_Bid_pkeyID;
                taskBidMasterDTO.Type = Data.Type;

                var GetBidData = await Task.Run(() => taskBidMasterData.GetTaskBidDetails(taskBidMasterDTO));
                return GetBidData;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //post Task Invoice details
        [Authorize]
        [HttpPost]
        [Route("PostTaskInvoiceData")]
        public async Task<List<dynamic>> PostTaskInvoiceData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            Task_Invoice_MasterDTO task_Invoice_MasterDTO = new Task_Invoice_MasterDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Task_Invoice_MasterDTO>(s);
                Data.UserID = LoggedInUSerId;

                task_Invoice_MasterDTO.Task_Inv_pkeyID = Data.Task_Inv_pkeyID;
                task_Invoice_MasterDTO.Task_Inv_TaskID = Data.Task_Inv_TaskID;
                task_Invoice_MasterDTO.Task_Inv_WO_ID = Data.Task_Inv_WO_ID;
                task_Invoice_MasterDTO.Task_Inv_Qty = Data.Task_Inv_Qty;
                task_Invoice_MasterDTO.Task_Inv_Uom_ID = Data.Task_Inv_Uom_ID;
                task_Invoice_MasterDTO.Task_Inv_Cont_Price = Data.Task_Inv_Cont_Price;
                task_Invoice_MasterDTO.Task_Inv_Cont_Total = Data.Task_Inv_Cont_Total;
                task_Invoice_MasterDTO.Task_Inv_Clnt_Price = Data.Task_Inv_Clnt_Price;
                task_Invoice_MasterDTO.Task_Inv_Clnt_Total = Data.Task_Inv_Clnt_Total;
                task_Invoice_MasterDTO.Task_Inv_Comments = Data.Task_Inv_Comments;
                task_Invoice_MasterDTO.Task_Inv_Violation = Data.Task_Inv_Violation;
                task_Invoice_MasterDTO.Task_Inv_damage = Data.Task_Inv_damage;
                task_Invoice_MasterDTO.Task_Inv_IsActive = Data.Task_Inv_IsActive;
                task_Invoice_MasterDTO.Task_Inv_IsDelete = Data.Task_Inv_IsDelete;
                task_Invoice_MasterDTO.UserID = Data.UserID;
                task_Invoice_MasterDTO.Type = Data.Type;

                var AddInvoiceData = await Task.Run(() => task_Invoice_MasterData.AddTaskInvoiceData(task_Invoice_MasterDTO));
                return AddInvoiceData;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //Get Task Invoice details
        [Authorize]
        [HttpPost]
        [Route("GetTaskInvoiceData")]
        public async Task<List<dynamic>> GetTaskInvoiceData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            Task_Invoice_MasterDTO task_Invoice_MasterDTO = new Task_Invoice_MasterDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Task_Invoice_MasterDTO>(s);
                Data.UserID = LoggedInUSerId;

                task_Invoice_MasterDTO.Task_Inv_pkeyID = Data.Task_Inv_pkeyID;
                task_Invoice_MasterDTO.Type = Data.Type;
                task_Invoice_MasterDTO.UserID = Data.UserID;
                var GetInvoiceData = await Task.Run(() => task_Invoice_MasterData.GetTaskInvoiceDetails(task_Invoice_MasterDTO));
                return GetInvoiceData;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //post Task Damage details
        [Authorize]
        [HttpPost]
        [Route("PostTaskDamageData")]
        public async Task<List<dynamic>> PostTaskDamageData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            TaskDamageMaster taskDamageMaster = new TaskDamageMaster();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<TaskDamageMaster>(s);
                Data.UserID = LoggedInUSerId;

                taskDamageMaster.Task_Damage_pkeyID = Data.Task_Damage_pkeyID;
                taskDamageMaster.Task_Damage_WO_ID = Data.Task_Damage_WO_ID;
                taskDamageMaster.Task_Damage_Task_ID = Data.Task_Damage_Task_ID;
                taskDamageMaster.Task_Damage_ID = Data.Task_Damage_ID;
                taskDamageMaster.Task_Damage_Type = Data.Task_Damage_Type;
                taskDamageMaster.Task_Damage_Int = Data.Task_Damage_Int;
                taskDamageMaster.Task_Damage_Location = Data.Task_Damage_Location;
                taskDamageMaster.Task_Damage_Qty = Data.Task_Damage_Qty;
                taskDamageMaster.Task_Damage_Estimate = Data.Task_Damage_Estimate;
                taskDamageMaster.Task_Damage_Disc = Data.Task_Damage_Disc;
                taskDamageMaster.Task_Damage_IsActive = Data.Task_Damage_IsActive;
                taskDamageMaster.Task_Damage_IsDelete = Data.Task_Damage_IsDelete;
                taskDamageMaster.UserID = Data.UserID;
                taskDamageMaster.Type = Data.Type;

                var AddTaskDamageData = await Task.Run(() => taskDamageMasterData.AddTaskDamageMasterData(taskDamageMaster));
                return AddTaskDamageData;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //post Task Damage details
        [Authorize]
        [HttpPost]
        [Route("GetTaskDamageData")]
        public async Task<List<dynamic>> GetTaskDamageData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            TaskDamageMaster taskDamageMaster = new TaskDamageMaster();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<TaskDamageMaster>(s);
                Data.UserID = LoggedInUSerId;

                taskDamageMaster.Task_Damage_pkeyID = Data.Task_Damage_pkeyID;
                taskDamageMaster.Type = Data.Type;
                taskDamageMaster.UserID = Data.UserID;
                var GetTaskDamageData = await Task.Run(() => taskDamageMasterData.GetTaskDamageMasterDetails(taskDamageMaster));
                return GetTaskDamageData;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        //post IPL Auto genrate details
        [Authorize]
        [HttpPost]
        [Route("PostIPLAutoData")]
        public async Task<List<dynamic>> PostIPLAutoData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            IPLAutoGenrateIDDTO iPLAutoGenrateIDDTO = new IPLAutoGenrateIDDTO();
            IPLAutoFillIDData iPLAutoFillIDData = new IPLAutoFillIDData();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<IPLAutoGenrateIDDTO>(s);



                iPLAutoGenrateIDDTO.Type = Data.Type;

                var AddIplData = await Task.Run(() => iPLAutoFillIDData.AddIPLAutoGenrateNo(iPLAutoGenrateIDDTO));
                return AddIplData;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }




        // get ClientResultTaskBidGetWorkOrderGet work order id  all 
        [Authorize]
        [HttpPost]
        [Route("ClientResultTaskBidGetWorkOrderGet")]
        public async Task<List<dynamic>> ClientResultTaskBidGetWorkOrderGet()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            TaskBidMasterDTO taskBidMasterDTO = new TaskBidMasterDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<TaskBidMasterDTO>(s);
                Data.UserID = LoggedInUSerId;

                taskBidMasterDTO.Task_Bid_WO_ID = Data.Task_Bid_WO_ID;
                taskBidMasterDTO.Type = Data.Type;
                taskBidMasterDTO.UserID = Data.UserID;


                Task_Invoice_MasterDTO task_Invoice_MasterDTO = new Task_Invoice_MasterDTO();
                task_Invoice_MasterDTO.Task_Inv_WO_ID = Data.Task_Bid_WO_ID;
                task_Invoice_MasterDTO.Type = Data.Type;
                task_Invoice_MasterDTO.UserID = Data.UserID;

                TaskDamageMaster taskDamageMaster = new TaskDamageMaster();

                taskDamageMaster.Task_Damage_WO_ID = Data.Task_Bid_WO_ID;
                taskDamageMaster.Type = Data.Type;
                taskDamageMaster.UserID = Data.UserID;

                ApplianceMasterDTO applianceMasterDTO = new ApplianceMasterDTO();

                //applianceMasterDTO.Appl_pkeyId = Data.Appl_pkeyId;
                applianceMasterDTO.Appl_Wo_Id = Data.Task_Bid_WO_ID;
                applianceMasterDTO.Type = Data.Type;
                applianceMasterDTO.UserID = Data.UserID;

                WorkOrder_Office_DocumentDTO workOrder_Office_DocumentDTO = new WorkOrder_Office_DocumentDTO(); // Added by dipali
                workOrder_Office_DocumentDTO.UserID = LoggedInUSerId;
                workOrder_Office_DocumentDTO.Wo_Office_Doc_Wo_ID = Data.Task_Bid_WO_ID;
                workOrder_Office_DocumentDTO.Type = 2;

                //var GetTask = await Task.Run(() => taskBidMasterData.GetTaskBidDetails(taskBidMasterDTO));
                //objdynamicobj.Add(GetTask);
                //var GetInvoice = await Task.Run(() => task_Invoice_MasterData.GetTaskInvoiceDetails(task_Invoice_MasterDTO));
                //objdynamicobj.Add(GetInvoice);
                //var GetDamage = await Task.Run(() => taskDamageMasterData.GetTaskDamageMasterDetails(taskDamageMaster));
                //objdynamicobj.Add(GetDamage);
                //var GetApplinceMaster = await Task.Run(() => applianceMasterData.GetApplianceMasterDetails(applianceMasterDTO));
                //objdynamicobj.Add(GetApplinceMaster);
                //var officeDocumentList = await Task.Run(() => workOrder_Office_DocumentData.GetWorkOrderOfficeDocumentDetails(workOrder_Office_DocumentDTO));
                //objdynamicobj.Add(officeDocumentList);


                TaskViolationMaster taskViolationMaster = new TaskViolationMaster();
                taskViolationMaster.Task_Violation_WO_ID = Data.Task_Bid_WO_ID;
                taskViolationMaster.Type = Data.Type;
                taskViolationMaster.UserID = Data.UserID;

                TaskHazardMaster taskHazardMaster = new TaskHazardMaster();
                taskHazardMaster.Task_Hazard_WO_ID = Data.Task_Bid_WO_ID;
                taskHazardMaster.Type = Data.Type;
                taskHazardMaster.UserID = Data.UserID;

                var GetTask = taskBidMasterData.GetTaskBidDetails(taskBidMasterDTO);
                objdynamicobj.Add(GetTask);
                var GetInvoice = task_Invoice_MasterData.GetTaskInvoiceDetails(task_Invoice_MasterDTO);
                objdynamicobj.Add(GetInvoice);
                var GetDamage = taskDamageMasterData.GetTaskDamageMasterDetails(taskDamageMaster);
                objdynamicobj.Add(GetDamage);
                var GetApplinceMaster = applianceMasterData.GetApplianceMasterDetails(applianceMasterDTO);
                objdynamicobj.Add(GetApplinceMaster);
                var officeDocumentList = await Task.Run(() => workOrder_Office_DocumentData.GetWorkOrderOfficeDocumentDetails(workOrder_Office_DocumentDTO));
                objdynamicobj.Add(officeDocumentList);


                var GetViolation = taskViolationMasterData.GetTaskViolationMasterDetails(taskViolationMaster);
                objdynamicobj.Add(GetViolation);

                var GetHazard = taskHazardMasterData.GetTaskHazardMasterDetails(taskHazardMaster);
                objdynamicobj.Add(GetHazard);

                return objdynamicobj;


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }




        //post Task Invoice mutiple details
        [Authorize]
        [HttpPost]
        [Route("ClientResultTaskInvoicePost")]
        public async Task<List<dynamic>> ClientResultTaskInvoicePost()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<TaskInvoiceMasterRootObject>(s);
                Data.UserID = Convert.ToInt32(LoggedInUSerId);



                var AddBidData = await Task.Run(() => clientResultDataMaster.ClientResultInvoicePost(Data));
                return AddBidData;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        //post Task Damage mutiple details client result
        [Authorize]
        [HttpPost]
        [Route("ClientResultTaskDamagePost")]
        public async Task<List<dynamic>> ClientResultTaskDamagePost()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<TaskDamageMasterRootObject>(s);
                Data.UserID = Convert.ToInt32(LoggedInUSerId);



                var AddBidData = await Task.Run(() => clientResultDataMaster.ClientResultDamagePost(Data));
                return AddBidData;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }



        // get client result invoice contractor and client data WOID for auto complete invoice on
        [Authorize]

        [HttpPost]
        [Route("GetClientResultInvoiceWOIDAutoCom")]
        public async Task<List<dynamic>> GetClientResultInvoiceWOIDAutoCom()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                Invoice_ContractorDTO invoice_ContractorDTO = new Invoice_ContractorDTO();

                var Data = JsonConvert.DeserializeObject<Invoice_ContractorDTO>(s);
                Data.UserID = Convert.ToInt64(LoggedInUSerId);

                invoice_ContractorDTO.Inv_Con_Wo_ID = Data.Inv_Con_Wo_ID;
                invoice_ContractorDTO.Type = Data.Type;
                invoice_ContractorDTO.UserID = LoggedInUSerId;

                var ViewInvoiceByAutoComplete = await Task.Run(() => invoice_ContractorData.GetInvoiceContractor_ClientData_Details(Data));
                return ViewInvoiceByAutoComplete;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }



        // Client result Contractor Invoice Post
        [Authorize]
        [HttpPost]
        [Route("ClientResultContractorInvoice")]
        public async Task<List<dynamic>> ClientResultContractorInvoice()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            string s = string.Empty;

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Invoice_ContractorDTO>(s);

                Data.UserID = LoggedInUSerId;

                var ReturnPkey = await Task.Run(() => clientResultDataMaster.ClientResultInvoiceContractorPost(Data));
                return ReturnPkey;


            }
            catch (Exception ex)
            {
                log.logErrorMessage("ClientResultContractorInvoice----Data----Start ");
                log.logErrorMessage(s);
                log.logErrorMessage("ClientResultContractorInvoice----Data----End ");
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        // Client result Contractor Invoice Post Multiple
        [Authorize]
        [HttpPost]
        [Route("ClientResultContractorInvoice_Multiple")]
        public async Task<List<dynamic>> ClientResultContractorInvoice_Multiple()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Invoice_ContractorDTO>(s);

                Data.UserID = LoggedInUSerId;
                if (!string.IsNullOrEmpty(Data.WorkOrderID_mul))
                {
                    List<Int64> workorderIdList = Data.WorkOrderID_mul.Split(',')?.Select(Int64.Parse).ToList();
                    foreach (var item in workorderIdList)
                    {
                        Data.Inv_Con_Wo_ID = item;
                        var ReturnPkey = await Task.Run(() => clientResultDataMaster.ClientResultInvoiceContractorPost(Data));
                        objdynamicobj.Add(ReturnPkey);
                    }

                }
                return objdynamicobj;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        // Client result Clinet Invoice Post
        [Authorize]
        [HttpPost]
        [Route("ClientResultClientInvoice")]
        public async Task<List<dynamic>> ClientResultClientInvoice()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Invoice_ClientDTO>(s);
                Data.UserID = LoggedInUSerId;


                var ReturnPkey = await Task.Run(() => clientResultDataMaster.ClientResultInvoiceClientPost(Data));
                return ReturnPkey;


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        // Client result Clinet Invoice Post Multiple
        [Authorize]
        [HttpPost]
        [Route("ClientResultClientInvoice_Multiple")]
        public async Task<List<dynamic>> ClientResultClientInvoice_Multiple()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Invoice_ClientDTO>(s);
                Data.UserID = LoggedInUSerId;

                if (!string.IsNullOrEmpty(Data.WorkOrderID_mul))
                {
                    List<Int64> workorderIdList = Data.WorkOrderID_mul.Split(',')?.Select(Int64.Parse).ToList();
                    foreach (var item in workorderIdList)
                    {
                        Data.Inv_Client_WO_Id = item;
                        var ReturnPkey = await Task.Run(() => clientResultDataMaster.ClientResultInvoiceClientPost(Data));
                        objdynamicobj.Add(ReturnPkey);
                    }

                }
                return objdynamicobj;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }



        //get bid task for photo  data 
        [Authorize]
        [HttpPost]
        [Route("GettaskbidPhotos")]
        public async Task<List<dynamic>> GettaskbidPhotos()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            TaskBidforimg taskBidforimg = new TaskBidforimg();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                TaskBidForPhotoData taskBidForPhotoData = new TaskBidForPhotoData();

                var Data = JsonConvert.DeserializeObject<TaskBidforimg>(s);
                Data.UserID = LoggedInUSerId;

                taskBidforimg.Task_Bid_TaskID = Data.Task_Bid_TaskID;
                taskBidforimg.Task_Bid_WO_ID = Data.Task_Bid_WO_ID;
                taskBidforimg.UserID = Data.UserID;
                var GettaskPhoto = await Task.Run(() => taskBidForPhotoData.GetbitfortaskDetails(taskBidforimg));
                return GettaskPhoto;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //get new correction bid task for photo  data 
        [Authorize]
        [HttpPost]
        [Route("GettaskbidPhotosdetails")]
        public async Task<List<dynamic>> GettaskbidPhotosdetails()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            TaskBidforimg taskBidforimg = new TaskBidforimg();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                TaskBidForPhotoData taskBidForPhotoData = new TaskBidForPhotoData();

                var Data = JsonConvert.DeserializeObject<TaskBidforimg>(s);
                Data.UserID = LoggedInUSerId;

                taskBidforimg.Task_Bid_TaskID = Data.Task_Bid_TaskID;
                taskBidforimg.Task_Bid_WO_ID = Data.Task_Bid_WO_ID;
                taskBidforimg.Task_Bid_Sys_Type = Data.Task_Bid_Sys_Type;
                taskBidforimg.UserID = Data.UserID;

                var GettaskPhoto = await Task.Run(() => taskBidForPhotoData.GetInvoiceBidfortaskMobApp(taskBidforimg));
                return GettaskPhoto;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }





        //get instruction Type data
        [Authorize]
        [HttpPost]
        [Route("GetInstructionType")]
        public async Task<List<dynamic>> GetInstructionType()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                InstctionTaskTypeMaster instctionTaskTypeMaster = new InstctionTaskTypeMaster();


                var Data = JsonConvert.DeserializeObject<InstctionTaskTypeMaster>(s);
                Data.UserID = LoggedInUSerId;

                instctionTaskTypeMaster.Ins_Type_pkeyId = Data.Ins_Type_pkeyId;
                instctionTaskTypeMaster.Type = Data.Type;
                instctionTaskTypeMaster.UserID = Data.UserID;

                var GetinstructionType = await Task.Run(() => instructionMasterData.GetInstructionTaskTypeDetails(instctionTaskTypeMaster));
                return GetinstructionType;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //get instruction Task data
        [Authorize]
        [HttpPost]
        [Route("GetInstructionTask")]
        public async Task<List<dynamic>> GetInstructionTask()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                InstctionTaskMaster instctionTaskMaster = new InstctionTaskMaster();


                var Data = JsonConvert.DeserializeObject<InstctionTaskMaster>(s);
                Data.UserID = LoggedInUSerId;

                instctionTaskMaster.Inst_Task_pkeyId = Data.Inst_Task_pkeyId;
                instctionTaskMaster.Inst_Task_Type_pkeyId = Data.Inst_Task_Type_pkeyId;
                instctionTaskMaster.WorkOrderID = Data.WorkOrderID;
                instctionTaskMaster.FilterData = Data.FilterData;
                instctionTaskMaster.Type = Data.Type;
                instctionTaskMaster.UserID = Data.UserID;

                var GetinstructionType = await Task.Run(() => instructionMasterData.GetInstructionTaskDetails(instctionTaskMaster));
                return GetinstructionType;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //get auto instruction master data
        [HttpPost]
        [Route("GetAutoInstructionTask")]
        public async Task<List<dynamic>> GetAutoInstructionTask()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                InstctionTaskMaster instctionTaskMaster = new InstctionTaskMaster();


                var Data = JsonConvert.DeserializeObject<InstctionTaskMaster>(s);
                Data.UserID = LoggedInUSerId;

                var AutoGetinstruction = await Task.Run(() => instructionMasterData.GetAutoInstructionTaskDetails(Data));
                return AutoGetinstruction;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //add auto intruction task master
        [Authorize]
        [HttpPost]
        [Route("PostAutoInstructionTask")]
        public async Task<List<dynamic>> PostAutoInstructionTask()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                InstctionTaskMaster instctionTaskMaster = new InstctionTaskMaster();


                var Data = JsonConvert.DeserializeObject<InstctionTaskMaster>(s);
                Data.UserID = LoggedInUSerId;

                var PostinstructionTask = await Task.Run(() => instructionMasterData.AddUpdateInstructionTaskDetails(Data));
                return PostinstructionTask;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("DeleteAutoInstructionTask")]
        public async Task<List<dynamic>> DeleteAutoInstructionTask()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();



                var Data = JsonConvert.DeserializeObject<InstctionTaskMaster>(s);
                Data.UserID = LoggedInUSerId;

                var PostinstructionTask = await Task.Run(() => instructionMasterData.AddAutoInstructionTaskData(Data));
                return PostinstructionTask;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //get instruction Task Name 
        [Authorize]
        [HttpPost]
        [Route("GetInstructionTaskName")]
        public async Task<List<dynamic>> GetInstructionTaskName()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                InstctionMasterDropdownName instctionMasterDropdownName = new InstctionMasterDropdownName();


                var Data = JsonConvert.DeserializeObject<InstctionMasterDropdownName>(s);


                instctionMasterDropdownName.Inst_Task_pkeyId = Data.Inst_Task_pkeyId;
                instctionMasterDropdownName.WorkOrderID = Data.WorkOrderID;
                instctionMasterDropdownName.Type = Data.Type;
                instctionMasterDropdownName.UserID = LoggedInUSerId;


                var GetinstructionType = await Task.Run(() => instructionMasterData.GetInstructionDRDDetails(instctionMasterDropdownName));
                return GetinstructionType;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        //Add instruction 
        [Authorize]
        [HttpPost]
        [Route("PostInstruction")]
        public async Task<List<dynamic>> PostInstruction()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();



                var Data = JsonConvert.DeserializeObject<InstctionMasterDTORootObject>(s);
                Data.UserID = LoggedInUSerId;


                var Addinstruction = await Task.Run(() => instructionMasterData.AddInstructionDetails(Data));
                return Addinstruction;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("PostInstructionMultiple")]
        public async Task<List<dynamic>> PostInstructionMultiple()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();



                var Data = JsonConvert.DeserializeObject<Task_Instruction>(s);
                Data.UserID = LoggedInUSerId;


                var Addinstruction = await Task.Run(() => instructionMasterData.AddMutipleTask(Data));
                return Addinstruction;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }



        //Delete  instruction 
        [Authorize]
        [HttpPost]
        [Route("PostDeleteInstruction")]
        public async Task<List<dynamic>> PostDeleteInstruction()
        {
            InstructionMasterData instructionMasterData = new InstructionMasterData();
            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();



                var Data = JsonConvert.DeserializeObject<InstctionMasterDTO>(s);
                Data.UserID = LoggedInUSerId;

                InstctionMasterDTO instctionMasterDTO = new InstctionMasterDTO();
                instctionMasterDTO.Instr_pkeyId = Data.Instr_pkeyId;
                instctionMasterDTO.Type = Data.Type;
                instctionMasterDTO.UserID = Data.UserID;


                var deleteinstruction = await Task.Run(() => instructionMasterData.AddInstructionData(instctionMasterDTO));
                return deleteinstruction;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //Delete  instruction 
        [Authorize]
        [HttpPost]
        [Route("PostDeleteInstructionchild")]
        public async Task<List<dynamic>> PostDeleteInstructionchild()
        {
            InstructionMasterData instructionMasterData = new InstructionMasterData();
            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();



                var Data = JsonConvert.DeserializeObject<Instruction_Master_ChildDTO>(s);
                Data.UserID = LoggedInUSerId;

                Instruction_Master_ChildDTO instruction_Master_ChildDTO = new Instruction_Master_ChildDTO();

                instruction_Master_ChildDTO.Inst_Ch_pkeyId = Data.Inst_Ch_pkeyId;
                instruction_Master_ChildDTO.Type = Data.Type;
                instruction_Master_ChildDTO.UserID = Data.UserID;

                var deleteinstructionchild = await Task.Run(() => instruction_Master_ChildData.AddInstructionChildData(instruction_Master_ChildDTO));
                return deleteinstructionchild;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }



        //Get instruction  Main
        [Authorize]
        [HttpPost]
        [Route("GetInstruction")]
        public async Task<List<dynamic>> GetInstruction()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<InstctionMasterDTO>(s);
                Data.UserID = LoggedInUSerId;

                InstctionMasterDTO instctionMasterDTO = new InstctionMasterDTO();

                instctionMasterDTO.Instr_pkeyId = Data.Instr_pkeyId;
                instctionMasterDTO.Instr_WO_Id = Data.Instr_WO_Id;
                instctionMasterDTO.Type = Data.Type;
                instctionMasterDTO.UserID = Data.UserID;

                var Addinstruction = await Task.Run(() => instructionMasterData.GetInstructionMasterDetails(instctionMasterDTO));
                return Addinstruction;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }






        //post Custom photo label single data
        [Authorize]
        [HttpPost]
        [Route("postCustomPhotoLabel")]
        public async Task<List<dynamic>> postCustomPhotoLabel()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var custom_PhotoLabel_MasterDTO = JsonConvert.DeserializeObject<Custom_PhotoLabel_MasterDTO>(s);
                custom_PhotoLabel_MasterDTO.UserID = Convert.ToInt32(LoggedInUSerId);

                //Custom_PhotoLabel_MasterDTO custom_PhotoLabel_MasterDTO = new Custom_PhotoLabel_MasterDTO();

                //custom_PhotoLabel_MasterDTO.PhotoLabel_pkeyID = Data.PhotoLabel_pkeyID;
                //custom_PhotoLabel_MasterDTO.PhotoLabel_Name = Data.PhotoLabel_Name;
                //custom_PhotoLabel_MasterDTO.PhotoLabel_IsCustom = Data.PhotoLabel_IsCustom;
                //custom_PhotoLabel_MasterDTO.PhotoLabel_IsActive = Data.PhotoLabel_IsActive;
                //custom_PhotoLabel_MasterDTO.PhotoLabel_IsDelete = Data.PhotoLabel_IsDelete;
                //custom_PhotoLabel_MasterDTO.PhotoLabel_Valtype = Data.PhotoLabel_Valtype;
                //custom_PhotoLabel_MasterDTO.PhotoLabel_Client_Id = Data.PhotoLabel_Client_Id;
                //custom_PhotoLabel_MasterDTO.PhotoLabel_WorkType_Id = Data.PhotoLabel_WorkType_Id;
                //custom_PhotoLabel_MasterDTO.PhotoLabel_Customer_Id = Data.PhotoLabel_Customer_Id;
                //custom_PhotoLabel_MasterDTO.PhotoLabel_Loan_Id = Data.PhotoLabel_Loan_Id;
                //custom_PhotoLabel_MasterDTO.PhotoLabel_Group_Id = Data.PhotoLabel_Group_Id; //Added By dipali
                //custom_PhotoLabel_MasterDTO.Type = Data.Type;
                //custom_PhotoLabel_MasterDTO.workOrder_ID = Data.workOrder_ID;
                //custom_PhotoLabel_MasterDTO.UserID = Data.UserID;

                var AddCustom = await Task.Run(() => custom_PhotoLabel_MasterData.AddCustomPhotoLableData(custom_PhotoLabel_MasterDTO));

                if (custom_PhotoLabel_MasterDTO.PhotoLabel_IsCustom == 0)
                {

                    custom_PhotoLabel_MasterDTO.Type = 4;//for WO which check on UI show
                    custom_PhotoLabel_MasterDTO.UserID = LoggedInUSerId;
                    var GetCustom = await Task.Run(() => custom_PhotoLabel_MasterData.GetCustomlabelDetails(custom_PhotoLabel_MasterDTO));
                    objdynamicobj.Add(GetCustom);
                }


                objdynamicobj.Add(AddCustom);


                return objdynamicobj;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }



        //get Custom photo label all get
        [Authorize]
        [HttpPost]
        [Route("GetCustomPhotoLabel")]
        public async Task<List<dynamic>> GetCustomPhotoLabel()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Custom_PhotoLabel_MasterDTO>(s);
                Data.UserID = LoggedInUSerId;

                Custom_PhotoLabel_MasterDTO custom_PhotoLabel_MasterDTO = new Custom_PhotoLabel_MasterDTO();

                switch (Data.Type)
                {
                    case 1:
                        {
                            objdynamicobj = await Task.Run(() => custom_PhotoLabel_MasterData.GetCustomlabelDetails(Data));
                            break;
                        }
                    case 2:
                        {

                            objdynamicobj = await Task.Run(() => custom_PhotoLabel_MasterData.GetCustomlabelDetails(Data));
                            break;
                        }
                    case 3:
                        {
                            objdynamicobj = await Task.Run(() => custom_PhotoLabel_MasterData.GetCustomlabelDetails(Data));

                            break;
                        }
                    case 4:
                        {
                            objdynamicobj = await Task.Run(() => custom_PhotoLabel_MasterData.GetCustomlabelDetails(Data));

                            break;
                        }
                    case 5:
                        {
                            objdynamicobj = await Task.Run(() => custom_PhotoLabel_MasterData.GetCustomPhotoFilterDetails(Data));

                            break;
                        }
                }



                return objdynamicobj;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        //post Custom photo label All data
        [Authorize]
        [HttpPost]
        [Route("PostWOCustomPhotoLabel")]
        public async Task<List<dynamic>> PostWOCustomPhotoLabel()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<WorkOrder_CustomPhotoLabelDTO>(s);
                Data.UserID = Convert.ToInt32(LoggedInUSerId);


                var GetCustom = await Task.Run(() => workOrder_CustomPhotoLabelData.AddWorkOrderPhotoLableDataMutiple(Data));

                objdynamicobj.Add(GetCustom);

                return objdynamicobj;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }




        //Add Instruction child data 
        [Authorize]
        [HttpPost]
        [Route("PostInstructionchild")]
        public async Task<List<dynamic>> PostInstructionchild()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                Instruction_Master_ChildDTO instruction_Master_ChildDTO = new Instruction_Master_ChildDTO();


                var Data = JsonConvert.DeserializeObject<Instruction_Master_ChildDTO>(s);
                Data.UserID = LoggedInUSerId;

                instruction_Master_ChildDTO.Inst_Ch_pkeyId = Data.Inst_Ch_pkeyId;
                instruction_Master_ChildDTO.Inst_Ch_Wo_Id = Data.Inst_Ch_Wo_Id;
                instruction_Master_ChildDTO.Inst_Ch_Text = Data.Inst_Ch_Text;
                instruction_Master_ChildDTO.Inst_Ch_Delete = Data.Inst_Ch_Delete;
                instruction_Master_ChildDTO.UserID = Data.UserID;
                instruction_Master_ChildDTO.Type = Data.Type;
                instruction_Master_ChildDTO.UserID = Data.UserID;


                var Addinstructionchild = await Task.Run(() => instruction_Master_ChildData.AddInstructionChildData(instruction_Master_ChildDTO));
                return Addinstructionchild;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //Get Instruction child data 
        [Authorize]
        [HttpPost]
        [Route("GetInstructionchild")]
        public async Task<List<dynamic>> GetInstructionchild()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                Instruction_Master_ChildDTO instruction_Master_ChildDTO = new Instruction_Master_ChildDTO();


                var Data = JsonConvert.DeserializeObject<Instruction_Master_ChildDTO>(s);
                Data.UserID = LoggedInUSerId;

                instruction_Master_ChildDTO.Inst_Ch_pkeyId = Data.Inst_Ch_pkeyId;
                instruction_Master_ChildDTO.Type = Data.Type;
                instruction_Master_ChildDTO.UserID = Data.UserID;


                var Getinstructionchild = await Task.Run(() => instruction_Master_ChildData.GetInstructionChildMasterDetails(instruction_Master_ChildDTO));
                return Getinstructionchild;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //Add Appliance master details
        [Authorize]
        [HttpPost]
        [Route("PostApplicantData")]

        public async Task<List<dynamic>> PostApplicantData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                ApplianceMasterDTO applianceMasterDTO = new ApplianceMasterDTO();
                var Data = JsonConvert.DeserializeObject<RootObject_App>(s);
                Data.UserID = LoggedInUSerId;



                var AddApplinceMaster = await Task.Run(() => applianceMasterData.AddApplianceMasterdata(Data));
                return AddApplinceMaster;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }


        //Get Appliance master details
        [Authorize]
        [HttpPost]
        [Route("GetApplicantData")]

        public async Task<List<dynamic>> GetApplicantData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                ApplianceMasterDTO applianceMasterDTO = new ApplianceMasterDTO();
                var Data = JsonConvert.DeserializeObject<ApplianceMasterDTO>(s);
                Data.UserID = LoggedInUSerId;

                applianceMasterDTO.Appl_pkeyId = Data.Appl_pkeyId;
                applianceMasterDTO.Appl_Wo_Id = Data.Appl_Wo_Id;
                applianceMasterDTO.Type = Data.Type;
                applianceMasterDTO.UserID = Data.UserID;

                var GetApplinceMaster = await Task.Run(() => applianceMasterData.GetApplianceMasterDetails(applianceMasterDTO));
                return GetApplinceMaster;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

        #region Created by Pradeep Yadav On 03/07/2020
        [Authorize]
        [HttpPost]
        [Route("CreateUpdateContractorPayment")]
        public async Task<List<dynamic>> CreateUpdateContractorPayment()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var contractor_Client_Expense_Payment_DTO = JsonConvert.DeserializeObject<Contractor_Client_Expense_Payment>(s);
                contractor_Client_Expense_Payment_DTO.UserId = LoggedInUSerId;
                var MasterDetails = await Task.Run(() =>
                contractor_Client_Expense_Payment.AddContractor_Client_Expense_Payment_Data(contractor_Client_Expense_Payment_DTO));
                return MasterDetails;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }


        [Authorize]
        [HttpPost]
        [Route("CreateUpdateContractorPayment_Multiple")]
        public async Task<List<dynamic>> CreateUpdateContractorPayment_Multiple()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var contractor_Client_Expense_Payment_DTO = JsonConvert.DeserializeObject<Contractor_Client_Expense_Payment>(s);
                contractor_Client_Expense_Payment_DTO.UserId = LoggedInUSerId;
                var MasterDetails = await Task.Run(() =>
                contractor_Client_Expense_Payment.AddContractor_Client_Expense_Payment_Data_Multiple(contractor_Client_Expense_Payment_DTO));
                return MasterDetails;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

        [Authorize]
        [HttpPost]
        [Route("CreateUpdateContractorSingleExpensePayment")]
        public async Task<List<dynamic>> CreateUpdateContractorSingleExpensePayment()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var contractor_Client_Expense_Payment_DTO = JsonConvert.DeserializeObject<Contractor_Client_Expense_Payment_DTO>(s);
                contractor_Client_Expense_Payment_DTO.UserId = LoggedInUSerId;
                var MasterDetails = await Task.Run(() =>
                contractor_Client_Expense_Payment.AddUpdate_SingleContractor_Client_Expense_Payment_Data(contractor_Client_Expense_Payment_DTO));
                return MasterDetails;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

        [Authorize]
        [HttpPost]
        [Route("GetContractorClientExpensePaymentData")]
        public async Task<List<dynamic>> GetContractorClientExpensePaymentData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var User_ContractorPaymentDTO = JsonConvert.DeserializeObject<Contractor_Client_Expense_Payment_DTO>(s);
                var geteusermaster = await Task.Run(() =>
                contractor_Client_Expense_Payment.Get_Contractor_Client_Expense_PaymentDetails(User_ContractorPaymentDTO));
                return geteusermaster;
            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }
        #endregion

        //Get print instruction  
        [Authorize]
        [HttpPost]
        [Route("MultipleInstructionPrint")]
        public async Task<List<dynamic>> MultipleInstructionPrint()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<WorkoderActionItems>(s);
                Data.UserId = LoggedInUSerId;

                var printinstruction = await Task.Run(() => instructionMasterData.PrintMultipleInstruction(Data));
                return printinstruction;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //Get print client invoice
        // [Authorize]
        [HttpPost]
        [Route("MultipleClientInvoicePrint")]
        public async Task<List<dynamic>> MultipleClientInvoicePrint()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            MultipleClientInvoicePrintData multipleClientInvoicePrintData = new MultipleClientInvoicePrintData();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<WorkoderActionItems>(s);
                Data.UserId = LoggedInUSerId;

                var printclientinvoice = await Task.Run(() => multipleClientInvoicePrintData.PrintMultipleClientInvoice(Data));
                return printclientinvoice;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("PostContractorScoreCardSetting")]
        public async Task<List<dynamic>> PostContractorScoreCardSetting()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Contractor_ScoreCard_SettingDTO>(s);
                Data.Con_score_setting_UserId = LoggedInUSerId;
                Data.UserID = LoggedInUSerId;

                var ScorecardSetting = await Task.Run(() => contractor_ScoreCard_SettingData.AddContractorScorecardSettingData(Data));
                return ScorecardSetting;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("GetContractorScoreCardSetting")]
        public async Task<List<dynamic>> GetContractorScoreCardSetting()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Contractor_ScoreCard_SettingDTO>(s);
                Data.Con_score_setting_UserId = LoggedInUSerId;
                Data.UserID = LoggedInUSerId;

                var ScorecardSetting = await Task.Run(() => contractor_ScoreCard_SettingData.GetContractorScoreCardSettingDetails(Data));
                return ScorecardSetting;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [Authorize]
        [HttpPost]
        [Route("PostContractorAccountPaySetting")]
        public async Task<List<dynamic>> PostContractorAccountPaySetting()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<ContractorAccountPayDTO>(s);
                Data.UserID = LoggedInUSerId;

                var AccountSetting = await Task.Run(() => contractor_ScoreCard_SettingData.AddContractorAccountPaySettingData(Data));
                return AccountSetting;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [Authorize]
        [HttpPost]
        [Route("GetContractorAccountPaySetting")]
        public async Task<List<dynamic>> GetContractorAccountPaySetting()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<ContractorAccountPayDTO>(s);
                Data.UserID = LoggedInUSerId;

                var GetAccountSetting = await Task.Run(() => contractor_ScoreCard_SettingData.GetContractorAccountPaySettingDetails(Data));
                return GetAccountSetting;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        //get Custom photo label group //Added By dipali
        [Authorize]
        [HttpPost]
        [Route("GetCustomPhotoLabelGroupData")]
        public async Task<List<dynamic>> GetCustomPhotoLabelGroupData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var data = JsonConvert.DeserializeObject<Custom_PhotoLabel_Group_MasterDTO>(s);
                data.UserID = LoggedInUSerId;
                objdynamicobj = await Task.Run(() => custom_PhotoLabel_Group_MasterData.GetCustomPhotoLabelGroupData(data));

                return objdynamicobj;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

        //post Custom photo label group
        [Authorize]
        [HttpPost]
        [Route("CreateUpdateCustomPhotoLabelGroupData")]
        public async Task<List<dynamic>> CreateUpdateCustomPhotoLabelGroupData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var data = JsonConvert.DeserializeObject<Custom_PhotoLabel_Group_MasterDTO>(s);
                data.UserID = LoggedInUSerId;
                objdynamicobj = await Task.Run(() => custom_PhotoLabel_Group_MasterData.CreateUpdateCustomPhotoLabelGroupData(data));

                return objdynamicobj;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }
        // delete Custom photo label group
        [Authorize]
        [HttpPost]
        [Route("DeleteCustomPhotoLabelGroupData")]
        public async Task<List<dynamic>> DeleteCustomPhotoLabelGroupData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var data = JsonConvert.DeserializeObject<Custom_PhotoLabel_Group_MasterDTO>(s);
                data.UserID = LoggedInUSerId;
                objdynamicobj = await Task.Run(() => custom_PhotoLabel_Group_MasterData.CreateUpdateCustomPhotoLabelGroup(data));

                return objdynamicobj;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

        // preset task
        [Authorize]
        [HttpPost]
        [Route("GetTaskPreset")]
        public async Task<List<dynamic>> GetTaskPreset()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            TaskPresetData taskPresetData = new TaskPresetData();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var data = JsonConvert.DeserializeObject<TaskPresetDTO>(s);
                data.UserID = LoggedInUSerId;
                objdynamicobj = await Task.Run(() => taskPresetData.GetTaskPresetChildDetails(data));

                return objdynamicobj;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

        [Authorize]
        [HttpPost]
        [Route("AddUpdateFilterAdminInstruction")]
        public async Task<List<dynamic>> AddUpdateFilterAdminInstruction([FromBody] Filter_Admin_Instruction_MasterDTO model)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                model.UserId = LoggedInUSerId;
                var UpdateFile = await Task.Run(() => filter_Admin_InstructionData.AddUpdate_Filter_Admin_Instruction(model));
                return UpdateFile;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }


        [Authorize]
        [HttpPost]
        [Route("AddUpdateFilterAdminUOM")]
        public async Task<List<dynamic>> AddUpdateFilterAdminUOM([FromBody] Filter_Admin_UOM_MasterDTO model)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                model.UserId = LoggedInUSerId;
                var UpdateFile = await Task.Run(() => filter_Admin_UOMData.AddUpdate_Filter_Admin_UOM(model));
                return UpdateFile;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

        [Authorize]
        [HttpPost]
        [Route("AddUpdateFilterAdminTask")]
        public async Task<List<dynamic>> AddUpdateFilterAdminTask([FromBody] Filter_Admin_Task_MasterDTO model)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                model.UserId = LoggedInUSerId;
                var UpdateFile = await Task.Run(() => filter_Admin_TaskData.AddUpdate_Filter_Admin_Task(model));
                return UpdateFile;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

        [Authorize]
        [HttpPost]
        [Route("AddUpdateFilterAdminRush")]
        public async Task<List<dynamic>> AddUpdateFilterAdminRush([FromBody] Filter_Admin_Rush_MasterDTO model)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                model.UserId = LoggedInUSerId;
                var UpdateFile = await Task.Run(() => filter_Admin_RushData.AddUpdate_Filter_Admin_Rush(model));
                return UpdateFile;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

        [Authorize]
        [HttpPost]
        [Route("AddUpdateFilterAdminCategory")]
        public async Task<List<dynamic>> AddUpdateFilterAdminCategory([FromBody] Filter_Admin_Category_MasterDTO model)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                model.UserId = LoggedInUSerId;
                var UpdateFile = await Task.Run(() => filter_Admin_CategoryData.AddUpdate_Filter_Admin_Category(model));
                return UpdateFile;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

        [Authorize]
        [HttpPost]
        [Route("AddUpdateFilterAdminCustPhLbl")]
        public async Task<List<dynamic>> AddUpdateFilterAdminCustPhLbl([FromBody] Filter_Admin_CustPhLbl_MasterDTO model)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                model.UserId = LoggedInUSerId;
                var UpdateFile = await Task.Run(() => filter_Admin_CustPhLblData.AddUpdate_Filter_Admin_CustPhLbl(model));
                return UpdateFile;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

        [Authorize]
        [HttpPost]
        [Route("GetWoContractorInvoice")]
        public async Task<List<dynamic>> GetWoContractorInvoiceDetail()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Invoice_ContractorWoList>(s);
                Data.UserId = LoggedInUSerId;
                objdynamicobj = await Task.Run(() => invoice_ContractorData.GetWoContractorInvoiceDetail(Data));
                return objdynamicobj;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //contractor print
        [Authorize]
        [HttpPost]
        [Route("ContractorInvoicePdfDetailsData")]
        public async Task<HttpResponseMessage> ContractorInvoicePdfDetailsData()
        {

            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<ContractorInvoicePrintDTO>(s);
                CustomReportPDF customReportPDF = new CustomReportPDF();
                Data.UserID = Convert.ToInt32(LoggedInUSerId);

                var Returnkey = await Task.Run(() => contractorClientPrintData.GeneratePDFContractorInvoicePrint(Data));

                Returnkey.HttpStatusCode = HttpStatusCode.OK;
                byte[] respbytes = Returnkey.Data;
                response.Content = new ByteArrayContent(respbytes);
                response.Content.Headers.ContentLength = respbytes.LongLength;
                response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = "Contractor" + ".pdf";
                response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/pdf");

                return response;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return null;
            }
        }

        //client print
        [Authorize]
        [HttpPost]
        [Route("ClientInvoicePdfDetailsData")]
        public async Task<HttpResponseMessage> ClientInvoicePdfDetailsData()
        {
            Client_Invoice_Print_MasterData client_Invoice_Print_MasterData = new Client_Invoice_Print_MasterData();
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Client_Invoice_Print_MasterDTO>(s);
                CustomReportPDF customReportPDF = new CustomReportPDF();
                Data.UserID = Convert.ToInt32(LoggedInUSerId);

                var Returnkey = await Task.Run(() => client_Invoice_Print_MasterData.GeneratePDFClientInvoicePrint(Data));

                Returnkey.HttpStatusCode = HttpStatusCode.OK;
                byte[] respbytes = Returnkey.Data;
                response.Content = new ByteArrayContent(respbytes);
                response.Content.Headers.ContentLength = respbytes.LongLength;
                response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = "Client" + ".pdf";
                response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/pdf");

                return response;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return null;
            }
        }

        [Authorize]
        [HttpPost]
        [Route("PostApplincesName")]
        public async Task<List<dynamic>> PostApplincesName()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Appliance_Name_MasterDTO>(s);
                Data.UserID = LoggedInUSerId;
                objdynamicobj = await Task.Run(() => appliance_Name_MasterData.AddAAppliancNameData(Data));
                return objdynamicobj;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("GetApplincesName")]
        public async Task<List<dynamic>> GetApplincesName()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Appliance_Name_MasterDTO>(s);
                Data.UserID = LoggedInUSerId;
                objdynamicobj = await Task.Run(() => appliance_Name_MasterData.GetAppliance_NamenDetails(Data));
                return objdynamicobj;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("PostGroupRoleData")]
        public async Task<List<dynamic>> PostGroupRoleData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<GroupRoleMasterDTO>(s);
                Data.UserID = LoggedInUSerId;
                objdynamicobj = await Task.Run(() => appliance_Name_MasterData.AddgrouproleData(Data));
                return objdynamicobj;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("GetGroupRoleData")]
        public async Task<List<dynamic>> GetGroupRoleData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<GroupRoleMasterDTO>(s);
                Data.UserID = LoggedInUSerId;
                objdynamicobj = await Task.Run(() => appliance_Name_MasterData.GetGroupRoleDetails(Data));
                return objdynamicobj;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("GetPhotoTransferBidTask")]
        public async Task<List<dynamic>> GetPhotoTransferBidTaskList()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                TaskBidForPhotoData taskBidForPhotoData = new TaskBidForPhotoData();

                var Data = JsonConvert.DeserializeObject<TaskBidforimg>(s);
                Data.UserID = LoggedInUSerId;

                var GettaskPhoto = await Task.Run(() => taskBidForPhotoData.GetPhotoTransferBidTaskList(Data));
                return GettaskPhoto;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]

        [Route("ClientResultsPhotoData")]
        public async Task<HttpResponseMessage> ClientResultsPhotoData()
        {
            ClientResultsPhotosPdf clientResultsPhotosPdf = new ClientResultsPhotosPdf();
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);



            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();


                var Data = JsonConvert.DeserializeObject<GetClientResultPhoto_DTO>(s);
                CustomReportPDF customReportPDF = new CustomReportPDF();

                var Returnkey = await Task.Run(() => clientResultsPhotosPdf.GeneratePDFClientResultsPhotos(Data));

                Returnkey.HttpStatusCode = HttpStatusCode.OK;
                byte[] respbytes = Returnkey.Data;
                response.Content = new ByteArrayContent(respbytes);
                response.Content.Headers.ContentLength = respbytes.LongLength;
                response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = "Client" + ".pdf";
                response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/pdf");

                return response;



            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return null;
            }
        }

        [Authorize]
        [HttpPost]
        [Route("PostPhotoHeaderTemplate")]
        public async Task<List<dynamic>> PostPhotoHeaderTemplate()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<PhotoHeaderTemplateDTO>(s);
                Data.UserID = LoggedInUSerId;

                var PostPhotoheader = await Task.Run(() => photoHeaderTemplateData.AddPhotoheaderData(Data));
                return PostPhotoheader;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("GetPhotoHeaderTemplate")]
        public async Task<List<dynamic>> GetPhotoHeaderTemplate()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<PhotoHeaderTemplateDTO>(s);
                Data.UserID = LoggedInUSerId;
                switch (Data.Type)
                {
                    case 1:
                        {
                            objdynamicobj = await Task.Run(() => photoHeaderTemplateData.GetPhotoHeaderTempDetails(Data));
                            break;
                        }
                    case 2:
                        {

                            objdynamicobj = await Task.Run(() => photoHeaderTemplateData.GetPhotoHeaderTempDetails(Data));
                            break;
                        }
                    case 3:
                        {
                            objdynamicobj = await Task.Run(() => photoHeaderTemplateData.GetPhotoHeaderFilterDetails(Data));

                            break;
                        }

                }


                return objdynamicobj;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("AdminFilterPhotoHeaderTemplate")]
        public async Task<List<dynamic>> AdminFilterPhotoHeaderTemplate()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            Filter_Admin_PhotoHeaderData filter_Admin_PhotoHeaderData = new Filter_Admin_PhotoHeaderData();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Filter_Admin_PhotoHeader_DTO>(s);
                Data.UserId = LoggedInUSerId;

                var PostPhotoheader = await Task.Run(() => filter_Admin_PhotoHeaderData.AddUpdate_Filter_Admin_PhotoHeader(Data));
                return PostPhotoheader;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [Authorize]
        [HttpPost]
        [Route("UpdateBackgroundCheckData")]
        public async Task<List<dynamic>> UpdateBackgroundCheckData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<UserMasterDTO>(s);
                Data.UserID = LoggedInUSerId;

                var background = await Task.Run(() => userMasterData.UpdateBackgrounduserMasterdetails(Data));
                return background;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        #region Task Violation and Hazard post

        [Authorize]
        [HttpPost]
        [Route("ClientResultTaskViolationPost")]
        public async Task<List<dynamic>> ClientResultTaskViolationPost()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<TaskViolationMasterRootObject>(s);
                Data.UserID = Convert.ToInt32(LoggedInUSerId);



                var AddViolation = await Task.Run(() => clientResultDataMaster.ClientResultViolationPost(Data));
                return AddViolation;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        [Authorize]
        [HttpPost]
        [Route("ClientResultTaskHazardPost")]
        public async Task<List<dynamic>> ClientResultTaskHazardPost()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<TaskHazardMasterRootObject>(s);
                Data.UserID = Convert.ToInt32(LoggedInUSerId);



                var addhazard = await Task.Run(() => clientResultDataMaster.ClientResultHazardPost(Data));
                return addhazard;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        #endregion
    }
}
