﻿using Avigma.Repository.Lib;
using Google.Apis.Auth.OAuth2;
using Google.Cloud.Storage.V1;

using Google.Apis.Storage.v1;


using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Cors;
using System.Web.Mvc;
using System.Net.Http;
using IPL.Models;
using IPL.Repository.data;
using iTextSharp.text.pdf.parser;
using System.Drawing.Imaging;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.WebControls;
using static iTextSharp.text.pdf.AcroFields;
using IPLApp.Models;
using Microsoft.Ajax.Utilities;
using IPLApp.Repository.data;
using System.Data;
using System.Net;
using RestSharp;
using Google.Apis.Storage.v1.Data;
using Google.Apis.Services;

namespace IPL.Controllers
{
    [RoutePrefix("api/downloadPhotoZip")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class DownloadPhotoZipController : BaseController
    {
        public string bucketName = System.Configuration.ConfigurationManager.AppSettings["BucketName"];
        public string jsonpath = System.Configuration.ConfigurationManager.AppSettings["GooglebucketJsom"];
        Log log = new Log();

        [Authorize]
        [HttpPost]
        [Route("PostZipDownloadHook")]

        public async Task<List<dynamic>> PostZipDownloadHook()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<dynamic>(s);


                JObject result = JObject.Parse(s);
                string folderPath = HttpContext.Current.Server.MapPath(@"~/Document/") + "DownloadForZip\\";

                var tasks = new[] { Task.Run(() => ProcessDownloadZip(result, folderPath)) };
                MessageResponse m = new MessageResponse();
                m.StatusCode = 200;
                m.Message = "Download is in progress! We will notify you once download is completed!";
                objdynamicobj.Add(m);
                // without the await here, this should be hit before the order processing is complete
                return objdynamicobj;

            }
            catch (Exception ex)
            {
                MessageResponse m = new MessageResponse();
                m.StatusCode = 200;
                m.Message = "Download Error : " + ex.ToString();
                objdynamicobj.Add(m);
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                // without the await here, this should be hit before the order processing is complete
                return objdynamicobj;
            }
            //return objdynamicobj;
        }

        #region old_code
        //public void ProcessDownloadZip(JObject result, string folderPathRaw)
        //{
        //    var filesToZipArray = result["Files"].Value<JArray>();
        //    string folderName = result["FolderName"].Value<string>();
        //    Int64 workOrderId = result["Client_Result_Photo_Wo_ID"].Value<Int64>();
        //    List<string> filesToZip = filesToZipArray.ToObject<List<string>>();
        //    try
        //    {
        //        string folderPath = folderPathRaw + folderName;

        //        if (!Directory.Exists(folderPath))
        //        {
        //            Directory.CreateDirectory(folderPath);
        //        }
        //        var strpath = string.Empty;
        //        for (int i = 0; i < filesToZip.Count; i++)
        //        {
        //            strpath = folderPath + "/" + filesToZip[i];
        //            DownloadFile(folderName, filesToZip[i], strpath);
        //        }

        //        //create zip 

        //        using (var stream = File.OpenWrite(folderPath + "\\" + folderName + ".zip"))
        //        using (ZipArchive archive = new ZipArchive(stream, System.IO.Compression.ZipArchiveMode.Create))
        //        {
        //            var files = Directory.GetFiles(folderPath).Where(name => !name.EndsWith(".zip"));
        //            //folderPathstring[] files = System.IO.Directory.GetFiles(folderPath);                    

        //            foreach (var item in files)
        //            {
        //                FileInfo fi = new FileInfo(item);
        //                archive.CreateEntryFromFile(fi.FullName, fi.Name, CompressionLevel.Optimal);
        //            }
        //        }

        //        string SaveImgPath = System.Configuration.ConfigurationManager.AppSettings["DownloadZip"];

        //        Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", jsonpath);

        //        FileUploadDownload fileUploadDownload = new FileUploadDownload();

        //        var credential = GoogleCredential.GetApplicationDefault();
        //        var storage = StorageClient.Create();

        //        var storageClient = StorageClient.Create(credential);


        //        string UploadFilePath = folderName;
        //        string UploadFileType = "";


        //        string url = string.Empty;
        //        using (var fileStream = new FileStream(folderPath + "\\" + folderName + ".zip", FileMode.Open,
        //            FileAccess.Read, FileShare.Read))
        //        {
        //            var response = storage.UploadObject(bucketName, UploadFilePath + "/" + folderName + ".zip", UploadFileType, fileStream);
        //            url = DownloadURL(folderName, folderName + ".zip");
        //            //Console.Write("signed url - " + url);
        //        }

        //        //send notification 
        //        ClientResult_Photos_Download_Notification_MasterDTO notification = new ClientResult_Photos_Download_Notification_MasterDTO();
        //        notification.PN_Pkey_Id = 0;
        //        notification.PN_UserId = LoggedInUSerId;
        //        notification.PN_IsRead = false;
        //        notification.PN_IsDelete = false;
        //        notification.PN_Title = "Your file is ready to download with IPL No - " + folderName + Environment.NewLine + "Link will be valid for 2 hour.";
        //        notification.PN_Message = "";
        //        notification.PN_DownloadLink = url;
        //        notification.Type = 1;
        //        notification.PN_IsActive = true;
        //        notification.PN_IsDelete = false;
        //        notification.PN_CreatedBy = LoggedInUSerId;

        //        notification.PN_WoId = workOrderId;

        //        ClientResult_Photos_Download_Notification_MasterData notificationMaster = new ClientResult_Photos_Download_Notification_MasterData();

        //        notificationMaster.AddDownloadPhotosNotificationMaster(notification);

        //        //remove unwanted data clutter 
        //        Directory.Delete(folderPath, true);

        //    }
        //    catch (Exception ex)
        //    {
        //        log.logErrorMessage(ex.StackTrace);
        //        log.logErrorMessage(ex.Message);
        //    }
        //}
        #endregion

        public async void ProcessDownloadZip(JObject result, string folderPathRaw)
        {
            string folderPath = string.Empty;
            try
            {
                var filesToZipArray = result["Files"].Value<JArray>();
                string folderName = result["FolderName"].Value<string>();
                Int64 workOrderId = result["Client_Result_Photo_Wo_ID"].Value<Int64>();
                int PN_DN_Type = result["PN_DN_Type"].Value<int>();
                List<string> filesToZip = filesToZipArray.ToObject<List<string>>();
                folderPath = folderPathRaw + folderName;

                if (PN_DN_Type == 2)
                {
                    #region get Photos Label List and also Photos setting
                    Client_Result_PhotoData client_Result_PhotoData = new Client_Result_PhotoData();
                    Client_Result_Photo_Data client_Result_Photo_Data = new Client_Result_Photo_Data();
                    client_Result_Photo_Data.Client_Result_Photo_Wo_ID = workOrderId;
                    client_Result_Photo_Data.Type = 1;

                    var getDynamicPhotosDetails = client_Result_PhotoData.GetPhotosListStatusTypeWise(client_Result_Photo_Data);
                    var photosListInStatus = getDynamicPhotosDetails[0] as List<StatusWIsePhoto_DTO>;
                    var photoSettings = getDynamicPhotosDetails[1] as ClientSettingDTO;
                    #endregion

                    #region Create Directory
                    if (!Directory.Exists(folderPath))
                    {
                        Directory.CreateDirectory(folderPath);
                    }
                    #endregion

                    #region Download photos from Google Storage
                    var strpath = string.Empty;
                    for (int i = 0; i < filesToZip.Count; i++)
                    {
                        strpath = folderPath + "\\" + filesToZip[i];
                        DownloadFile(folderName, filesToZip[i], strpath);
                    }
                    #endregion

                    #region Create Zip  File and Resize Photo if Needed

                    using (var stream = File.OpenWrite(folderPath + "\\" + folderName + ".zip"))
                    using (ZipArchive archive = new ZipArchive(stream, System.IO.Compression.ZipArchiveMode.Create))
                    {
                        var directoryInfo = new DirectoryInfo(folderPath);
                        //var files = Directory.GetFiles(folderPath).Where(name => !name.EndsWith(".zip"));
                        var files = directoryInfo.GetFiles()
                                            .Where(file => !file.Extension.Equals(".zip", StringComparison.OrdinalIgnoreCase))
                                            .ToList();
                        #region Update File Name 
                        int i = 1;
                        List<PhotoArchive_DTO> photoArchives = new List<PhotoArchive_DTO>();
                        //foreach (var item in files)
                        foreach (var item in filesToZip)
                        {
                            //FileInfo fi = new FileInfo(item);
                            string fileNameval = item;
                            FileInfo fi = files.FirstOrDefault(f => f.Name.Equals(fileNameval, StringComparison.OrdinalIgnoreCase));
                            var phoptoExist = photosListInStatus.Where(x => x.Client_Result_Photo_FileName == fi.Name).ToList();
                            if (phoptoExist.Count > 0)
                            {

                                foreach (var photo in phoptoExist)
                                {

                                    if (photoArchives.Any(x => x.TaskName == photo.TaskName && x.LabelName == photo.LableName))
                                    {
                                          i = photoArchives.Where(x => x.TaskName == photo.TaskName && x.LabelName == photo.LableName).OrderByDescending(x => x.Count).FirstOrDefault().Count + 1;
                                      //  i = photoArchives.Where(x => x.TaskName == photo.TaskName && x.LabelName == photo.LableName).OrderBy(x => x.Count).FirstOrDefault().Count + 1;
                                    }

                                    string fileName = $"{photo.TaskName} {photo.LableName}{i}.jpg";
                                  //  string fileName = photo.Client_Result_Photo_FileName;
                                    fi.MoveTo(fi.Directory.FullName + "\\" + fileName);
                                    if (photoArchives.Count == 0)
                                    {
                                        photoArchives.Add(new PhotoArchive_DTO
                                        {
                                            FilePath = fi.FullName,
                                            Filename = fi.Name,
                                            TaskName = photo.TaskName,
                                            LabelName = photo.LableName,
                                            Count = i
                                        });
                                    }
                                    else if (photoArchives.Any(x => x.Filename != fileName))
                                    {
                                        photoArchives.Add(new PhotoArchive_DTO
                                        {
                                            FilePath = fi.FullName,
                                            Filename = fi.Name,
                                            TaskName = photo.TaskName,
                                            LabelName = photo.LableName,
                                            Count = i
                                        });
                                    }
                                }
                                i = 1;
                            }
                            else
                            {
                                photoArchives.Add(new PhotoArchive_DTO
                                {
                                    FilePath = fi.FullName,
                                    Filename = fi.Name
                                });
                                i = 1;
                            }
                        }
                        #endregion

                        #region Photo Resize and Add file into Zip File
                        foreach (var item in photoArchives)
                        {
                            if (File.Exists(item.FilePath))
                            {
                                ImageResizer(photoSettings, item.FilePath);
                                FileInfo fi = new FileInfo(item.FilePath);
                                archive.CreateEntryFromFile(fi.FullName, fi.Name, CompressionLevel.Optimal);
                            }
                        }
                        #endregion
                    }
                    #endregion

                    #region upload Zip file to Storage
                    string SaveImgPath = System.Configuration.ConfigurationManager.AppSettings["DownloadZip"];

                    Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", jsonpath);

                    FileUploadDownload fileUploadDownload = new FileUploadDownload();

                    var credential = GoogleCredential.GetApplicationDefault();
                    var storage = StorageClient.Create();

                    var storageClient = StorageClient.Create(credential);


                    string UploadFilePath = folderName;
                    string UploadFileType = "";


                    string url = string.Empty;
                    using (var fileStream = new FileStream(folderPath + "\\" + folderName + ".zip", FileMode.Open, FileAccess.Read, FileShare.Read))
                    {
                        var response = storage.UploadObject(bucketName, UploadFilePath + "/" + folderName + ".zip", UploadFileType, fileStream);
                        url = DownloadURL(folderName, folderName + ".zip");
                    }
                    #endregion

                    #region Send notification 
                    //send notification 
                    ClientResult_Photos_Download_Notification_MasterDTO notification = new ClientResult_Photos_Download_Notification_MasterDTO();
                    notification.PN_Pkey_Id = 0;
                    notification.PN_UserId = LoggedInUSerId;
                    notification.PN_IsRead = false;
                    notification.PN_IsDelete = false;
                    notification.PN_Title = "Your file is ready to download with IPL No - " + folderName + Environment.NewLine + "Link will be valid for 2 hour.";
                    notification.PN_Message = "";
                    notification.PN_DownloadLink = url;
                    notification.Type = 1;
                    notification.PN_IsActive = true;
                    notification.PN_IsDelete = false;
                    notification.PN_CreatedBy = LoggedInUSerId;
                    notification.PN_DN_Type = PN_DN_Type;
                    notification.PN_WoId = workOrderId;

                    ClientResult_Photos_Download_Notification_MasterData notificationMaster = new ClientResult_Photos_Download_Notification_MasterData();

                    notificationMaster.AddDownloadPhotosNotificationMaster(notification);
                    await notificationMaster.AddFirbase_DownloadNotification(notification);  //Send Firebase Notification 

                    #endregion

                    #region New Access Log
                    Access_log_MasterData access_Log_MasterData = new Access_log_MasterData();
                    NewAccessLogMaster newAccessLogMaster = new NewAccessLogMaster();
                    newAccessLogMaster.Access_WorkerOrderID = workOrderId;
                    newAccessLogMaster.Access_Master_ID = 20;
                    newAccessLogMaster.Access_UserID = LoggedInUSerId;
                    newAccessLogMaster.Type = 1;
                    access_Log_MasterData.AddNewAccessLogMaster(newAccessLogMaster);
                    #endregion


                    //remove unwanted data clutter 
                    Directory.Delete(folderPath, true);
                }
                else
                {


                    #region Checking the photos are already downloaded
                    Get_Client_Result_Photos_Notification_MasterData get_Client_Result_Photos_Notification_MasterData = new Get_Client_Result_Photos_Notification_MasterData();
                    Client_Result_Photos_Notification_MasterDTO client_Result_Photos_Notification_MasterDTO = new Client_Result_Photos_Notification_MasterDTO();
                    client_Result_Photos_Notification_MasterDTO.Type = 1;
                    client_Result_Photos_Notification_MasterDTO.PN_WoId = workOrderId;
                    DataSet ds = get_Client_Result_Photos_Notification_MasterData.GetClientResultPhotosNotificationMaster(client_Result_Photos_Notification_MasterDTO);
                    string urlvalue = string.Empty;
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            urlvalue = ds.Tables[0].Rows[i]["PN_DownloadLink"].ToString();
                        }
                        #region Send notification 
                        //send notification 

                        ClientResult_Photos_Download_Notification_MasterDTO notification = new ClientResult_Photos_Download_Notification_MasterDTO();
                        notification.PN_Pkey_Id = 0;
                        notification.PN_UserId = LoggedInUSerId;
                        notification.PN_IsRead = false;
                        notification.PN_IsDelete = false;
                        notification.PN_Title = "Your file is ready to download with IPL No - " + folderName + Environment.NewLine + "Link will be valid for 2 hour.";
                        notification.PN_Message = "";
                        notification.PN_DownloadLink = urlvalue;
                        notification.Type = 1;
                        notification.PN_IsActive = true;
                        notification.PN_IsDelete = false;
                        notification.PN_CreatedBy = LoggedInUSerId;
                        notification.PN_DN_Type = PN_DN_Type;
                        notification.PN_WoId = workOrderId;

                        ClientResult_Photos_Download_Notification_MasterData notificationMaster = new ClientResult_Photos_Download_Notification_MasterData();

                        notificationMaster.AddDownloadPhotosNotificationMaster(notification);
                        await notificationMaster.AddFirbase_DownloadNotification(notification);  //Send Firebase Notification 

                        #endregion

                        #region New Access Log
                        Access_log_MasterData access_Log_MasterData = new Access_log_MasterData();
                        NewAccessLogMaster newAccessLogMaster = new NewAccessLogMaster();
                        newAccessLogMaster.Access_WorkerOrderID = workOrderId;
                        newAccessLogMaster.Access_Master_ID = 20;
                        newAccessLogMaster.Access_UserID = LoggedInUSerId;
                        newAccessLogMaster.Type = 1;
                        access_Log_MasterData.AddNewAccessLogMaster(newAccessLogMaster);
                        #endregion

                        #endregion


                    }
                    else
                    {



                        #region get Photos Label List and also Photos setting
                        Client_Result_PhotoData client_Result_PhotoData = new Client_Result_PhotoData();
                        Client_Result_Photo_Data client_Result_Photo_Data = new Client_Result_Photo_Data();
                        client_Result_Photo_Data.Client_Result_Photo_Wo_ID = workOrderId;
                        client_Result_Photo_Data.Type = 1;

                        var getDynamicPhotosDetails = client_Result_PhotoData.GetPhotosListStatusTypeWise(client_Result_Photo_Data);
                        var photosListInStatus = getDynamicPhotosDetails[0] as List<StatusWIsePhoto_DTO>;
                        var photoSettings = getDynamicPhotosDetails[1] as ClientSettingDTO;
                        #endregion

                        #region Create Directory
                        if (!Directory.Exists(folderPath))
                        {
                            Directory.CreateDirectory(folderPath);
                        }
                        #endregion

                        #region Download photos from Google Storage
                        var strpath = string.Empty;
                        for (int i = 0; i < filesToZip.Count; i++)
                        {
                            strpath = folderPath + "\\" + filesToZip[i];
                            DownloadFile(folderName, filesToZip[i], strpath);
                        }
                        #endregion

                        #region Create Zip  File and Resize Photo if Needed

                        using (var stream = File.OpenWrite(folderPath + "\\" + folderName + ".zip"))
                        using (ZipArchive archive = new ZipArchive(stream, System.IO.Compression.ZipArchiveMode.Create))
                        {
                            var directoryInfo = new DirectoryInfo(folderPath);
                            // var files = Directory.GetFiles(folderPath).Where(name => !name.EndsWith(".zip"));
                            var files = directoryInfo.GetFiles()
                          .Where(file => !file.Extension.Equals(".zip", StringComparison.OrdinalIgnoreCase))
                          .ToList();
                            #region Update File Name 
                            int i = 1;
                            List<PhotoArchive_DTO> photoArchives = new List<PhotoArchive_DTO>();
                            foreach (var item in filesToZip)
                            {
                                string fileNameval = item;
                                FileInfo fi = files.FirstOrDefault(f => f.Name.Equals(fileNameval, StringComparison.OrdinalIgnoreCase));
                                //FileInfo fi = new FileInfo(item);
                                var phoptoExist = photosListInStatus.Where(x => x.Client_Result_Photo_FileName == fi.Name).ToList();

                                if (phoptoExist.Count > 0)
                                {

                                    foreach (var photo in phoptoExist)
                                    {

                                        if (photoArchives.Any(x => x.TaskName == photo.TaskName && x.LabelName == photo.LableName))
                                        {
                                            i = photoArchives.Where(x => x.TaskName == photo.TaskName && x.LabelName == photo.LableName).OrderByDescending(x => x.Count).FirstOrDefault().Count + 1;
                                          //  i = photoArchives.Where(x => x.TaskName == photo.TaskName && x.LabelName == photo.LableName).OrderBy(x => x.Count).FirstOrDefault().Count + 1;
                                        }

                                        string fileName = $"{photo.TaskName} {photo.LableName}{i}.jpg";
                                      //  string fileName = photo.Client_Result_Photo_FileName;
                                        fi.MoveTo(fi.Directory.FullName + "\\" + fileName);
                                        if (photoArchives.Count == 0)
                                        {
                                            photoArchives.Add(new PhotoArchive_DTO
                                            {
                                                FilePath = fi.FullName,
                                                Filename = fi.Name,
                                                TaskName = photo.TaskName,
                                                LabelName = photo.LableName,
                                                Count = i
                                            });
                                        }
                                        else if (photoArchives.Any(x => x.Filename != fileName))
                                        {
                                            photoArchives.Add(new PhotoArchive_DTO
                                            {
                                                FilePath = fi.FullName,
                                                Filename = fi.Name,
                                                TaskName = photo.TaskName,
                                                LabelName = photo.LableName,
                                                Count = i
                                            });
                                        }
                                    }
                                    i = 1;
                                }
                                else
                                {
                                    photoArchives.Add(new PhotoArchive_DTO
                                    {
                                        FilePath = fi.FullName,
                                        Filename = fi.Name
                                    });
                                    i = 1;
                                }
                            }
                            #endregion

                            #region Photo Resize and Add file into Zip File
                            foreach (var item in photoArchives)
                            {
                                if (File.Exists(item.FilePath))
                                {
                                    ImageResizer(photoSettings, item.FilePath);
                                    FileInfo fi = new FileInfo(item.FilePath);
                                    archive.CreateEntryFromFile(fi.FullName, fi.Name, CompressionLevel.Optimal);
                                }
                            }
                            #endregion
                        }
                        #endregion

                        #region upload Zip file to Storage
                        string SaveImgPath = System.Configuration.ConfigurationManager.AppSettings["DownloadZip"];

                        Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", jsonpath);

                        FileUploadDownload fileUploadDownload = new FileUploadDownload();

                        var credential = GoogleCredential.GetApplicationDefault();
                        var storage = StorageClient.Create();

                        var storageClient = StorageClient.Create(credential);


                        string UploadFilePath = folderName;
                        string UploadFileType = "";


                        string url = string.Empty;
                        using (var fileStream = new FileStream(folderPath + "\\" + folderName + ".zip", FileMode.Open, FileAccess.Read, FileShare.Read))
                        {
                            var response = storage.UploadObject(bucketName, UploadFilePath + "/" + folderName + ".zip", UploadFileType, fileStream);
                            url = DownloadURL(folderName, folderName + ".zip");
                        }
                        #endregion

                        #region Send notification 
                        //send notification 
                        ClientResult_Photos_Download_Notification_MasterDTO notification = new ClientResult_Photos_Download_Notification_MasterDTO();
                        notification.PN_Pkey_Id = 0;
                        notification.PN_UserId = LoggedInUSerId;
                        notification.PN_IsRead = false;
                        notification.PN_IsDelete = false;
                        notification.PN_Title = "Your file is ready to download with IPL No - " + folderName + Environment.NewLine + "Link will be valid for 2 hour.";
                        notification.PN_Message = "";
                        notification.PN_DownloadLink = url;
                        notification.Type = 1;
                        notification.PN_IsActive = true;
                        notification.PN_IsDelete = false;
                        notification.PN_CreatedBy = LoggedInUSerId;
                        notification.PN_DN_Type = PN_DN_Type;
                        notification.PN_WoId = workOrderId;

                        ClientResult_Photos_Download_Notification_MasterData notificationMaster = new ClientResult_Photos_Download_Notification_MasterData();

                        notificationMaster.AddDownloadPhotosNotificationMaster(notification);
                        await notificationMaster.AddFirbase_DownloadNotification(notification);  //Send Firebase Notification 

                        #endregion

                        #region New Access Log
                        Access_log_MasterData access_Log_MasterData = new Access_log_MasterData();
                        NewAccessLogMaster newAccessLogMaster = new NewAccessLogMaster();
                        newAccessLogMaster.Access_WorkerOrderID = workOrderId;
                        newAccessLogMaster.Access_Master_ID = 20;
                        newAccessLogMaster.Access_UserID = LoggedInUSerId;
                        newAccessLogMaster.Type = 1;
                        access_Log_MasterData.AddNewAccessLogMaster(newAccessLogMaster);
                        #endregion


                        //remove unwanted data clutter 
                        Directory.Delete(folderPath, true);
                    }
                }

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                //remove unwanted data clutter 
                if (folderPath != null)
                    Directory.Delete(folderPath, true);
            }
        }

        public void DownloadFile(string newFolderName, string fileName, string filePathToDownload)
        {
            string jsonpath = System.Configuration.ConfigurationManager.AppSettings["GooglebucketJsom"];

            var bucketName = System.Configuration.ConfigurationManager.AppSettings["BucketName"];

            string SaveImgPath = System.Configuration.ConfigurationManager.AppSettings["DownloadZip"];

            Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", jsonpath);

            FileUploadDownload fileUploadDownload = new FileUploadDownload();

            var credential = GoogleCredential.GetApplicationDefault();
            var storage = StorageClient.Create();


            #region Image Download code  
            // Image Download code starts

            string folderPath = SaveImgPath + newFolderName; // IPL NO

            try
            {
                using (var fileStream = File.Create(filePathToDownload))
                {
                    storage.DownloadObject(bucketName, newFolderName + "/" + fileName, fileStream);
                    fileStream.Dispose();
                    fileStream.Close();
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage("error in file download --->" + newFolderName + "---->"+fileName);
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            #endregion
        }

        //public void DownloadFile(DownloadPhotoModel downloadPhotoModel)
        //{
        //    string jsonpath = System.Configuration.ConfigurationManager.AppSettings["GooglebucketJsom"];

        //    var bucketName = System.Configuration.ConfigurationManager.AppSettings["BucketName"];

        //    string SaveImgPath = System.Configuration.ConfigurationManager.AppSettings["DownloadZip"];

        //    Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", jsonpath);

        //    FileUploadDownload fileUploadDownload = new FileUploadDownload();

        //    var credential = GoogleCredential.GetApplicationDefault();
        //    var storage = StorageClient.Create();


        //    #region Image Download code  
        //    //Image Download code starts 

        //    //string folderPath = SaveImgPath + newFolderName; // IPL NO

        //    try
        //    {
        //        string filePathToDownload = downloadPhotoModel.FolderPath + "\\" + downloadPhotoModel.FileName;
        //        using (var fileStream = File.Create(filePathToDownload))
        //        {
        //            storage.DownloadObject(bucketName, downloadPhotoModel.BucketfolderName + "/" + downloadPhotoModel.FileName, fileStream);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        // Console.Write("error in file download -" + ex.ToString());
        //        log.logErrorMessage(ex.StackTrace);
        //        log.logErrorMessage(ex.Message);
        //    }

        //    #endregion
        //}


        private string CallAPI(string URL, string DATA)
        {
            string responseval = string.Empty;

            try
            {
                var client = new RestClient(URL);
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Connection", "keep-alive");
                request.AddHeader("Content-Length", "70");
                request.AddHeader("Accept-Encoding", "gzip, deflate");
                request.AddHeader("Host", "us-central1-rare-lambda-245821.cloudfunctions.net");
                request.AddHeader("Postman-Token", "9426244d-41e4-486e-aa06-55ed1abbeec0,1df2c66a-58d9-4e0e-b804-7585fdd70002");
                request.AddHeader("Cache-Control", "no-cache");
                request.AddHeader("Accept", "*/*");
                request.AddHeader("User-Agent", "PostmanRuntime/7.20.1");
                request.AddHeader("Content-Type", "application/json");
                ///request.AddParameter("undefined", "{\r\n \"FileName\": \"Test.jpg\",\r\n \"FolderName\": \"Name of Folder in IPL\"\r\n}", ParameterType.RequestBody);
                request.AddParameter("undefined", DATA, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                //log.logDebugMessage("Request FireBase");
                //log.logDebugMessage(request.ToString());
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    responseval = response.Content;
                }
                else
                {
                    responseval = "Error While Retving";
                    log.logErrorMessage(" API Response" + response.StatusCode.ToString());
                }


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                responseval = "Error";
            }

            return responseval;

        }
      
        public string DownloadURL(string folderName, string fileName)
        {
            string url = string.Empty;
            try
            {
                //UrlSigner urlSigner = UrlSigner.FromServiceAccountPath(jsonpath);
                ////DateTimeOffset DateTimeOffset = DateTimeOffset.MaxValue;
                //TimeSpan expiration = TimeSpan.FromDays(365 * 5);
                //// V4 is the default signing version.
                //url = urlSigner.Sign(bucketName, folderName + "/" + fileName, expiration, HttpMethod.Get);

                string strURL = System.Configuration.ConfigurationManager.AppSettings["IntegrationURL"] + "download";
                string ReqData = "{\r\n \"FileName\": \"" + fileName + "\",\r\n \"FolderName\": \"" + folderName + "\"\r\n}";
                url = CallAPI(strURL, ReqData);


                //// Console.WriteLine($"curl '{url}'");
                ///


            }
            catch (Exception ex)
            {

                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }

            return url;
        }


        private void ImageResizer(ClientSettingDTO photoSettings, string strpath)
        {
            try
            {
                if (photoSettings != null)
                {
                    if (photoSettings.Client_Photo_Resize_width > 0 && photoSettings.Client_Photo_Resize_height > 0)
                    {
                        string datetimeStamp = null;

                        if (photoSettings.Client_DateTimeOverlay == DateOverlapingEnum.PrintDatetime)
                            datetimeStamp = DateTime.Now.ToString("MM/dd/yyyy hh:mm tt");
                        if (photoSettings.Client_DateTimeOverlay == DateOverlapingEnum.PrintOnlyDate)
                            datetimeStamp = DateTime.Now.ToString("MM/dd/yyyy");

                        var Imagedata = ImageHelper.ResizePhoto(strpath, Convert.ToInt32(photoSettings.Client_Photo_Resize_width), Convert.ToInt32(photoSettings.Client_Photo_Resize_height), datetimeStamp);
                        if (File.Exists(strpath))
                            File.Delete(strpath);
                        Stream stream = new MemoryStream(ImageHelper.ImageToByteStream(Imagedata));
                        Imagedata.Save(strpath, ImageFormat.Jpeg);
                    }
                    else if (photoSettings.Client_DateTimeOverlay > 0)
                    {
                        string datetimeStamp = null;

                        if (photoSettings.Client_DateTimeOverlay == DateOverlapingEnum.PrintDatetime)
                            datetimeStamp = DateTime.Now.ToString("MM/dd/yyyy hh:mm tt");
                        if (photoSettings.Client_DateTimeOverlay == DateOverlapingEnum.PrintOnlyDate)
                            datetimeStamp = DateTime.Now.ToString("MM/dd/yyyy");

                        var Imagedata = ImageHelper.AddWaterMark(strpath, datetimeStamp);
                        if (File.Exists(strpath))
                            File.Delete(strpath);
                        Stream stream = new MemoryStream(ImageHelper.ImageToByteStream(Imagedata));
                        Imagedata.Save(strpath, ImageFormat.Jpeg);
                    }
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage("error in file download -");
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }



        }
    }

    class MessageResponse
    {
        public int StatusCode { get; set; }
        public string Message { get; set; }
    }
}