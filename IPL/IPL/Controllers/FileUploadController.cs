﻿using Avigma.Models;
using Avigma.Repository.Lib;
using IPL.Models;
using IPL.Repository.data;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace IPL.Controllers
{
    [RoutePrefix("api/FileUpload")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class FileUploadController : BaseController
    {
        Log log = new Log();
        EmailData emailData = new EmailData();
        PrintPDFData printPDFData = new PrintPDFData();

        [HttpPost]
        [AllowAnonymous]
        [Route("EmailFileUpload1")]

        public async Task<HttpResponseMessage> EmailFileUpload()
        {

            System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
            System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
            System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
            string s = reader.ReadToEnd();

            Dictionary<string, object> dict = new Dictionary<string, object>();
            string strfilepath = String.Empty;
            try
            {

                var httpRequest = HttpContext.Current.Request;

                string formobj = HttpContext.Current.Server.UrlDecode(httpRequest.Form.AllKeys[0]);

                //var Data = JsonConvert.DeserializeObject<EmailDTO>(formobj);

                string strpath = System.Configuration.ConfigurationManager.AppSettings["EmailUploadDocPath"];

                foreach (string file in httpRequest.Files)
                {
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created);
                

                    var postedFile = httpRequest.Files[file];


                    if (postedFile != null && postedFile.ContentLength > 0)
                    {

                        int MaxContentLength = 1024 * 1024 * 2; 

                        IList<string> AllowedFileExtensions = new List<string> { ".pdf", ".ppt", ".pptx", ".PDF", ".DOC", ".DOCX", ".png", ".jpeg", ".jpg" };
                        var ext = postedFile.FileName.Substring(postedFile.FileName.LastIndexOf('.'));
                        var extension = ext.ToLower();
                        if (!AllowedFileExtensions.Contains(extension))
                        {
                            var message = string.Format("Please Upload file of type .pdf,.doc,.docx.,.img file");

                            dict.Add("Message", message);
                            return Request.CreateResponse(HttpStatusCode.Created, dict);
                        }
                        else if (postedFile.ContentLength > MaxContentLength)
                        {

                            var message = string.Format("Please Upload a file upto 2 mb.");

                            dict.Add("Message", message);
                            return Request.CreateResponse(HttpStatusCode.Created, dict);
                        }
                        else
                        {

                            string fileName = Guid.NewGuid() + Path.GetExtension(extension);
                           
                            strfilepath = strpath + fileName; 
                            string orginalName = postedFile.FileName; 

                            var filePath = HttpContext.Current.Server.MapPath(strpath + fileName);
                            postedFile.SaveAs(filePath);


                           // var msgx = await Task.Run(() => emailData.SendAttachEmail(Data));

                        }
                    }

                    var message1 = string.Format("Document Updated Successfully.");
                  
                    String result = message1 + "#" + strfilepath;
                    var msg = await Task.Run(() => Request.CreateErrorResponse(HttpStatusCode.Created, result));
                    return msg;
                }
                var res = string.Format("Please Upload a document.");
                dict.Add("Message", res);
                return Request.CreateResponse(HttpStatusCode.Created, dict);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                var res = string.Format("Server Error, To Large File Size");
                dict.Add("Message", res);
                return Request.CreateResponse(HttpStatusCode.Created, dict);
            }




        }

        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("CreateWorkOrderPdf")]
        //public async Task<List<dynamic>> CreateWork_OrderPdf()
        public async Task<HttpResponseMessage> CreateWork_OrderPdf()
        {
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage("----------CreateWorkOrderPdf Start--------------");
                log.logDebugMessage(s);
                log.logDebugMessage("----------CreateWorkOrderPdf End--------------");
                var Data = JsonConvert.DeserializeObject<PrintPDFDTO>(s);
                //Data.UserID=LoginUset
                Data.UserID = LoggedInUSerId;
                var context = HttpContext.Current;
                var Path = HttpContext.Current.Server.MapPath(@"~/Document/PdfDocuments/");
                string pdfpath = Path;

                //var pdfworkorder = await Task.Run(() => printPDFData.GenerateWork_Order_Pdf(Data, context, pdfpath)).ConfigureAwait(true); ;

                //return pdfworkorder;
                var Returnkey = await Task.Run(() => printPDFData.GenerateWork_Order_Pdf(Data, context, pdfpath)).ConfigureAwait(true);
                Returnkey.HttpStatusCode = HttpStatusCode.OK;
                byte[] respbytes = Returnkey.Data.Result.Data;
                response.Content = new ByteArrayContent(respbytes);
                response.Content.Headers.ContentLength = respbytes.LongLength;
                response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = "Contractor" + ".pdf";
                response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/pdf");

                return response;

            }
            catch (Exception ex)
            {
                log.logErrorMessage("CreateUpdateUserGenerateOrder");
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return null;//objdynamicobj;
            }
            //return response;
        }

    }
}
