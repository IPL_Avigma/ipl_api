﻿using IPL.Models.FiveBrothers;
using IPL.Repository.data;
using IPL.Repository.FiveBrothers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using Avigma.Repository.Lib;

namespace IPL.Controllers
{
    [RoutePrefix("api/FBAPI")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class FBAPIController : BaseController
    {
        FiveBrothers_Api_Data fiveBrothers_Api_Data = new FiveBrothers_Api_Data();
        WorkOrder_FiveBrother_MasterData workOrder_FiveBrother_MasterData = new WorkOrder_FiveBrother_MasterData();
        FiveBrothers_Sync_Data fiveBrothers_Sync_Data = new FiveBrothers_Sync_Data();
        Log log = new Log();


        [Authorize]
        [HttpPost]
        [Route("AddDamageCodes")]
        public async Task<List<dynamic>> AddDamageCodes()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var reqData = JsonConvert.DeserializeObject<Client_Sync_DTO>(s);
                reqData.UserID = LoggedInUSerId;

                objdynamicobj = await Task.Run(() => fiveBrothers_Api_Data.AddDamageCodes(reqData));

                return objdynamicobj;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

        [Authorize]
        [HttpPost]
        [Route("AddOrdertypecodes")]
        public async Task<List<dynamic>> AddOrdertypecodes()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var reqData = JsonConvert.DeserializeObject<Client_Sync_DTO>(s);
                reqData.UserID = LoggedInUSerId;

                objdynamicobj = await Task.Run(() => fiveBrothers_Api_Data.AddOrdertypecodes(reqData));

                return objdynamicobj;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

        [Authorize]
        [HttpPost]
        [Route("AddItemCodes")]
        public async Task<List<dynamic>> AddItemCodes()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var reqData = JsonConvert.DeserializeObject<Client_Sync_DTO>(s);
                reqData.UserID = LoggedInUSerId;

                objdynamicobj = await Task.Run(() => fiveBrothers_Api_Data.AddItemCodes(reqData));

                return objdynamicobj;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

        [Authorize]
        [HttpPost]
        [Route("AddLocationCodes")]
        public async Task<List<dynamic>> AddLocationCodes()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var reqData = JsonConvert.DeserializeObject<Client_Sync_DTO>(s);
                reqData.UserID = LoggedInUSerId;

                objdynamicobj = await Task.Run(() => fiveBrothers_Api_Data.AddLocationCodes(reqData));

                return objdynamicobj;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }
        [Authorize]
        [HttpPost]
        [Route("AddCategoryCodes")]
        public async Task<List<dynamic>> AddCategoryCodes()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var reqData = JsonConvert.DeserializeObject<Client_Sync_DTO>(s);
                reqData.UserID = LoggedInUSerId;

                objdynamicobj = await Task.Run(() => fiveBrothers_Api_Data.AddCategoryCodes(reqData));

                return objdynamicobj;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

        [Authorize]
        [HttpPost]
        [Route("AddBidCodes")]
        public async Task<List<dynamic>> AddBidCodes()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var reqData = JsonConvert.DeserializeObject<Client_Sync_DTO>(s);
                reqData.UserID = LoggedInUSerId;

                objdynamicobj = await Task.Run(() => fiveBrothers_Api_Data.AddBidCodes(reqData));

                return objdynamicobj;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

        [Authorize]
        [HttpPost]
        [Route("AddInsurancecategories")]
        public async Task<List<dynamic>> AddInsurancecategories()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var reqData = JsonConvert.DeserializeObject<Client_Sync_DTO>(s);
                reqData.UserID = LoggedInUSerId;

                objdynamicobj = await Task.Run(() => fiveBrothers_Api_Data.AddInsurancecategories(reqData));

                return objdynamicobj;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

        [Authorize]
        [HttpPost]
        [Route("AddOrders")]
        public async Task<List<dynamic>> AddOrders()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var reqData = JsonConvert.DeserializeObject<Client_Sync_DTO>(s);
                reqData.UserID = LoggedInUSerId;

               // objdynamicobj = await Task.Run(() => workOrder_FiveBrother_MasterData.AddOrders(reqData));

                return objdynamicobj;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

        // Bid Sync
        [Authorize]
        [HttpPost]
        [Route("UploadBidSync")]
        public async Task<List<dynamic>> UploadBidSync()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            TaskPresetData taskPresetData = new TaskPresetData();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var data = JsonConvert.DeserializeObject<Client_Sync_DTO>(s);
                data.UserID = LoggedInUSerId;
                objdynamicobj = await Task.Run(() => fiveBrothers_Sync_Data.UploadBidSync(data));

                return objdynamicobj;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

        // Invoice Sync
        [Authorize]
        [HttpPost]
        [Route("UploadInvoiceSync")]
        public async Task<List<dynamic>> UploadInvoiceSync()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            TaskPresetData taskPresetData = new TaskPresetData();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var data = JsonConvert.DeserializeObject<Client_Sync_DTO>(s);
                data.UserID = LoggedInUSerId;
                objdynamicobj = await Task.Run(() => fiveBrothers_Sync_Data.UploadInvoiceSync(data));

                return objdynamicobj;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

        // Preservation Sync
        [Authorize]
        [HttpPost]
        [Route("UploadPreservationSync")]
        public async Task<List<dynamic>> UploadPreservationSync()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            TaskPresetData taskPresetData = new TaskPresetData();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var data = JsonConvert.DeserializeObject<Client_Sync_DTO>(s);
                data.UserID = LoggedInUSerId;
                objdynamicobj = await Task.Run(() => fiveBrothers_Sync_Data.UploadPreservation(data));

                return objdynamicobj;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

        // Photo Sync
        [Authorize]
        [HttpPost]
        [Route("UploadPhotoSync")]
        public async Task<List<dynamic>> UploadPhotoSync()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            TaskPresetData taskPresetData = new TaskPresetData();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var data = JsonConvert.DeserializeObject<Client_Sync_DTO>(s);
                data.UserID = LoggedInUSerId;
                objdynamicobj = await Task.Run(() => fiveBrothers_Sync_Data.UploadPhotoSync(data));

                return objdynamicobj;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

        // Grass Sync
        [Authorize]
        [HttpPost]
        [Route("UploadGrassSync")]
        public async Task<List<dynamic>> UploadGrassSync()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            TaskPresetData taskPresetData = new TaskPresetData();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var data = JsonConvert.DeserializeObject<Client_Sync_DTO>(s);
                data.UserID = LoggedInUSerId;
                objdynamicobj = await Task.Run(() => fiveBrothers_Sync_Data.UploadGrassSync(data));

                return objdynamicobj;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

        // Damage Sync
        [Authorize]
        [HttpPost]
        [Route("UploadDamageSync")]
        public async Task<List<dynamic>> UploadDamageSync()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            TaskPresetData taskPresetData = new TaskPresetData();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var data = JsonConvert.DeserializeObject<Client_Sync_DTO>(s);
                data.UserID = LoggedInUSerId;
                objdynamicobj = await Task.Run(() => fiveBrothers_Sync_Data.UploadDamageSync(data));

                return objdynamicobj;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

    }
}
