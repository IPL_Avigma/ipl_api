﻿using Avigma.Repository.Lib;
using IPL.Models.RepairBase;
using IPL.Repository.RepairBase;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace IPL.Controllers
{
    [RoutePrefix("api/RepairBase")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class RepairBaseController : BaseController
    {
        Log log = new Log();
        RepairBase_Document_Maste_Data repairBase_Document_Maste_Data = new RepairBase_Document_Maste_Data();
        RepairBase_Profit_OverHead_Data repairBase_Profit_OverHead_Data = new RepairBase_Profit_OverHead_Data();
        RepairBase_User_Master_Data repairBase_User_Master_Data = new RepairBase_User_Master_Data();

        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("AddRepairBaseDocumentMasteData")]
        public async Task<List<dynamic>> AddRepairBaseDocumentMasteData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage(s);
                var Data = JsonConvert.DeserializeObject<RepairBase_Document_Maste_DTO>(s);
                Data.Type = 1;
                Data.UserId = LoggedInUSerId;
                Data.Rep_Base_IsActive = true;
                Data.Rep_Base_IsDelete = false;

                string strURL = System.Configuration.ConfigurationManager.AppSettings["IntegrationURL"] + "download";
                FileUploadDownload fileUploadDownload = new FileUploadDownload();
                string ReqData = "{\r\n \"FileName\": \"" + Data.Rep_Base_Doc_File_Name + "\",\r\n \"FolderName\": \"" + Data.Rep_Base_Doc_Folder_Name + "\"\r\n}";
                string strPath = fileUploadDownload.CallAPI(strURL, ReqData);
                Data.Rep_Base_Doc_File_Path = strPath;
                var repairbaseDetails = await Task.Run(() => repairBase_Document_Maste_Data.AddRepairBase_Document_Maste_Data(Data));

                return repairbaseDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }
        [Authorize]
        [HttpPost]
        [Route("GetRepairBaseDocumentMasteData")]
        public async Task<List<dynamic>> GetRepairBaseDocumentMasteData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<RepairBase_Document_Maste_DTO>(s);
                Data.UserId = LoggedInUSerId;
                var repairbaseDetails = await Task.Run(() => repairBase_Document_Maste_Data.Get_RepairBase_Document_masterDetails(Data));
                return repairbaseDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("AddRepairBaseProfitOverHeadData")]
        public async Task<List<dynamic>> AddRepairBaseProfitOverHeadData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<RepairBase_Profit_OverHead_DTO>(s);
                Data.UserId = LoggedInUSerId;
                Data.Rep_Userid = LoggedInUSerId;
                var repairprofileDetails = await Task.Run(() => repairBase_Profit_OverHead_Data.AddRepairBase_Profit_OverHead_Data(Data));

                return repairprofileDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

        [Authorize]
        [HttpPost]
        [Route("GetRepairBaseProfitOverHeadData")]
        public async Task<List<dynamic>> GetRepairBaseProfitOverHeadData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<RepairBase_Profit_OverHead_DTO>(s);
                Data.UserId = LoggedInUSerId;
                var repairprofileDetails = await Task.Run(() => repairBase_Profit_OverHead_Data.Get_RepairBase_Profit_OverHeadDetails(Data));
                return repairprofileDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        [Authorize]
        [HttpPost]
        [Route("AddRepairBaseUserMasterData")]
        public async Task<List<dynamic>> AddRepairBaseUserMasterData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<RepairBase_User_Master_DTO>(s);
                Data.UserId = LoggedInUSerId;

                var repairuserDetails = await Task.Run(() => repairBase_User_Master_Data.AddRepairBase_User_Master_Data(Data));

                return repairuserDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

        [Authorize]
        [HttpPost]
        [Route("GetRepairBaseUserMasterData")]
        public async Task<List<dynamic>> GetRepairBaseUserMasterData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<RepairBase_User_Master_DTO>(s);
                Data.UserId = LoggedInUSerId;
                var repairuserDetails = await Task.Run(() => repairBase_User_Master_Data.Get_RepairBase_User_MasterDetails(Data));
                return repairuserDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
    }
}
