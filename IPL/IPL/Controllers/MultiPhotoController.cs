﻿using Avigma.Repository.Lib;
using IPL.Models;
using IPLApp.Models;
using IPL.Repository.data;
using IPLApp.Repository.data;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Results;
using AutoMapper;
using RestSharp;
using AdminDemo.Repositotry.Data;

namespace IPL.Controllers
{
    [RoutePrefix("api/MultiPhoto")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class MultiPhotoController : BaseController
    {
        Log log = new Log();
        Client_Result_PhotoData client_Result_PhotoData = new Client_Result_PhotoData();
        GoogleCloudData googleCloudData = new GoogleCloudData();
        ClientResultsPhotoFileNameData clientResultsPhotoFileNameData = new ClientResultsPhotoFileNameData();




        //Upload  image   client work order 

        [HttpPost]

        [AllowAnonymous]
        [Route("ClientResultPhtotos")]

        public async Task<HttpResponseMessage> ClientResultPhtotos()
        {

            System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
            System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
            System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
            string s = reader.ReadToEnd();

            Dictionary<string, object> dict = new Dictionary<string, object>();
            string strfilepath = String.Empty;
            try
            {

                var httpRequest = HttpContext.Current.Request;


                //var str = httpRequest.Form.AllKeys[0];

                string formobj = HttpContext.Current.Server.UrlDecode(httpRequest.Form.AllKeys[0]);

                var Data = JsonConvert.DeserializeObject<photosRoots>(formobj);

                string strpath = System.Configuration.ConfigurationManager.AppSettings["UploadDocPathClientResultPhotos"];

                string BucketName = System.Configuration.ConfigurationManager.AppSettings["BucketName"];

                string UploadPhotos = System.Configuration.ConfigurationManager.AppSettings["UploadPhotos"];
                string UploadPhotosPathSet = System.Configuration.ConfigurationManager.AppSettings["UploadPhotosPathSet"];

                string DownLoadPhotos = System.Configuration.ConfigurationManager.AppSettings["DownLoadPhotos"];
                string DownLoadPhotosPathSet = System.Configuration.ConfigurationManager.AppSettings["DownLoadPhotosPathSet"];



                foreach (string file in httpRequest.Files)
                {
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created);


                    string[] PkID = file.Split('#');

                    string ModelpkeyId = PkID[0];

                    var postedFile = httpRequest.Files[file];


                    if (postedFile != null && postedFile.ContentLength > 0)
                    {

                        int MaxContentLength = 1024 * 1024 * 2; //Size = 2 MB  

                        IList<string> AllowedFileExtensions = new List<string> { ".pdf", ".ppt", ".pptx", ".PDF", ".DOC", ".DOCX", ".png", ".jpeg", ".jpg" };
                        var ext = postedFile.FileName.Substring(postedFile.FileName.LastIndexOf('.'));
                        var extension = ext.ToLower();
                        if (!AllowedFileExtensions.Contains(extension))
                        {

                            var message = string.Format("Please Upload file of type .pdf,.doc,.docx.,.img file");

                            dict.Add("Message", message);
                            return Request.CreateResponse(HttpStatusCode.Created, dict);
                        }
                        else if (postedFile.ContentLength > MaxContentLength)
                        {

                            var message = string.Format("Please Upload a file upto 2 mb.");

                            dict.Add("Message", message);
                            return Request.CreateResponse(HttpStatusCode.Created, dict);
                        }
                        else
                        {



                            string fileName = Guid.NewGuid() + Path.GetExtension(extension);

                            //Int64 Stp_pkeyId = (Convert.ToInt64(UserpkeyId)); // dB user Email
                            strfilepath = strpath + fileName; // dB pic path
                            string orginalName = postedFile.FileName; // dB pic name

                            var filePath = HttpContext.Current.Server.MapPath(strpath + fileName);//"~/Userimage/"+ extension
                            postedFile.SaveAs(filePath);

                            var filePathUploadPhotos = HttpContext.Current.Server.MapPath(UploadPhotos + Data.IPLNO + "/" + fileName);
                            // upload file create
                            if (!Directory.Exists(UploadPhotosPathSet + Data.IPLNO))
                            {

                                Directory.CreateDirectory(UploadPhotosPathSet + Data.IPLNO);
                            }
                            postedFile.SaveAs(filePathUploadPhotos);

                            var filePathDownLoadPhotos = HttpContext.Current.Server.MapPath(DownLoadPhotos + Data.IPLNO + "/" + fileName);


                            // download file create
                            if (!Directory.Exists(DownLoadPhotosPathSet + Data.IPLNO))
                            {

                                Directory.CreateDirectory(DownLoadPhotosPathSet + Data.IPLNO);
                            }
                            postedFile.SaveAs(filePathDownLoadPhotos);

                            Client_Result_PhotoDTO client_Result_PhotoDTO = new Client_Result_PhotoDTO();

                            client_Result_PhotoDTO.Client_Result_Photo_Wo_ID = Convert.ToInt64(ModelpkeyId);
                            client_Result_PhotoDTO.Client_Result_Photo_FilePath = strfilepath;
                            client_Result_PhotoDTO.Client_Result_Photo_FileName = orginalName;
                            client_Result_PhotoDTO.Client_Result_Photo_IsActive = true;
                            client_Result_PhotoDTO.Client_Result_Photo_FileType = extension;
                            client_Result_PhotoDTO.Client_Result_Photo_FileSize = Convert.ToString(postedFile.ContentLength);
                            client_Result_PhotoDTO.Client_Result_Photo_folder = Data.IPLNO;
                            client_Result_PhotoDTO.Client_Result_Photo_objectName = orginalName;
                            client_Result_PhotoDTO.Client_Result_Photo_FolderName = Data.IPLNO;
                            client_Result_PhotoDTO.Client_Result_Photo_BucketName = BucketName;
                            client_Result_PhotoDTO.Client_Result_Photo_localPath = DownLoadPhotos + Data.IPLNO + "/" + fileName;
                            client_Result_PhotoDTO.Type = Data.Type;
                            client_Result_PhotoDTO.Client_Result_Photo_StatusType = Data.Client_Result_Photo_StatusType;


                            photosRoots photosRoots = new photosRoots();

                            photosRoots.fileName = fileName;
                            photosRoots.filePath = filePath;
                            photosRoots.IPLNO = Data.IPLNO; ;

                            var msgx = await Task.Run(() => client_Result_PhotoData.AddClientResultPhotoData(client_Result_PhotoDTO));


                            var msgxx = await Task.Run(() => PostgoogleUploadImage(photosRoots));



                        }
                    }

                    var message1 = string.Format("Document Updated Successfully.");
                    //var message2 = strfilepath;
                    String result = message1 + "#" + strfilepath;
                    var msg = await Task.Run(() => Request.CreateErrorResponse(HttpStatusCode.Created, result));
                    return msg;
                }
                var res = string.Format("Please Upload a document.");
                dict.Add("Message", res);
                return Request.CreateResponse(HttpStatusCode.Created, dict);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                var res = string.Format("Server Error, To Large File Size");
                dict.Add("Message", res);
                return Request.CreateResponse(HttpStatusCode.Created, dict);
            }




        }




        //get Client result Image  data 

        [HttpPost]
        [Route("GetCLientResultPhotos")]
        public async Task<List<dynamic>> GetCLientResultPhotos()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            GetClientResultPhoto_DTO getClientResultPhoto_DTO = new GetClientResultPhoto_DTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<GetClientResultPhoto_DTO>(s);
                getClientResultPhoto_DTO.Client_Result_Photo_ID = Data.Client_Result_Photo_ID;
                getClientResultPhoto_DTO.Client_Result_Photo_Wo_ID = Data.Client_Result_Photo_Wo_ID;
                getClientResultPhoto_DTO.IPLNO = Data.IPLNO;
                getClientResultPhoto_DTO.Type = Data.Type;



                var GetClientPhoto = await Task.Run(() => client_Result_PhotoData.GetClientResultPhotoDetails(getClientResultPhoto_DTO,1));



                // temp check 
                if (GetClientPhoto[0].Count != 0)
                {

                    objdynamicobj.Add(GetClientPhoto);


                    var GetClientPhotox = await Task.Run(() => PostgoogleImageDownload(GetClientPhoto, getClientResultPhoto_DTO));


                    objdynamicobj.Add(GetClientPhotox);
                }


                return objdynamicobj;



            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        // upload on google drive photo

        [HttpPost]
        [Route("Postgooglecloud")]
        public async Task<List<dynamic>> Postgooglecloud()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<CreateBucketDTO>(s);

                CreateBucketDTO createBucketDTO = new CreateBucketDTO();
                createBucketDTO.BucketName = "clientresult";


                var AddBucket = await Task.Run(() => googleCloudData.CreateBucket(createBucketDTO));
                return AddBucket;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }



        /// <summary>
        ///  post data google bucket UI to Cloud
        /// </summary>
        /// <returns></returns>
        //[HttpPost]
        //[Route("PostgoogleUploadImage")]
        public async Task<List<dynamic>> PostgoogleUploadImage(photosRoots model)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {

                string jsonpath = System.Configuration.ConfigurationManager.AppSettings["GooglebucketJsom"];
                string BucketName = System.Configuration.ConfigurationManager.AppSettings["BucketName"];

                FileUploadBucketDTO fileUploadBucketDTO = new FileUploadBucketDTO();
                fileUploadBucketDTO.BucketName = BucketName;
                fileUploadBucketDTO.UploadFilePath = model.fileName;
                fileUploadBucketDTO.filePath = model.filePath;
                fileUploadBucketDTO.IPLNO = model.IPLNO;

                fileUploadBucketDTO.SharedKeyFilePath = jsonpath;

                var AddDoc = await Task.Run(() => googleCloudData.UploadDocument(fileUploadBucketDTO));
                return AddDoc;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        /// <summary>
        /// get google photos save to local sys
        /// </summary>
        /// <returns></returns>
        [Route("PostgoogleImageDownload")]
        public async Task<List<dynamic>> PostgoogleImageDownload(dynamic GetClientPhoto, GetClientResultPhoto_DTO model)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {

                DownloadImageDTO downloadImageDTO = new DownloadImageDTO();
                downloadImageDTO.IPLFolder = Convert.ToString(model.IPLNO);

                var Download = await Task.Run(() => googleCloudData.DownloadObject(GetClientPhoto, downloadImageDTO));
                return Download;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [HttpPost]
        [Route("PostAddFolder")]
        public async Task<List<dynamic>> PostAddFolder()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<CreateFolderDTO>(s);
                CreateFolderDTO createFolderDTO = new CreateFolderDTO();
                createFolderDTO.BucketName = "rare-lambda-245821clientresult";
                createFolderDTO.FolderName = "workorder";


                var AddFolderdata = await Task.Run(() => googleCloudData.AddFolder(createFolderDTO));
                return AddFolderdata;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [HttpPost]
        [Route("PostDeleteImage")]
        public async Task<List<dynamic>> PostDeleteImage()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<FileUploadBucketDTO>(s);
                FileUploadBucketDTO fileUploadBucketDTO = new FileUploadBucketDTO();
                fileUploadBucketDTO.BucketName = "rare-lambda-245821clientresult";


                var Download = await Task.Run(() => googleCloudData.DeleteBucket(fileUploadBucketDTO));
                return Download;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [HttpPost]
        [Route("GetStorageListImage")]
        public async Task<List<dynamic>> GetStorageListImage()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<FileUploadBucketDTO>(s);
                FileUploadBucketDTO fileUploadBucketDTO = new FileUploadBucketDTO();
                fileUploadBucketDTO.BucketName = "rare-lambda-245821clientresult";


                //var GetImage = await Task.Run(() => googleCloudData.ListObjects(fileUploadBucketDTO, 1));

                var GetImage = await Task.Run(() => googleCloudData.ListPPWNewObjects(fileUploadBucketDTO, 1));
                return GetImage;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }







        //get Client result Image  data second api call clean data 
        [Authorize]
        [HttpPost]
        [Route("GetCLientResultPhotosMaster")]
        public async Task<List<dynamic>> GetCLientResultPhotosMaster()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            GetClientResultPhoto_DTO getClientResultPhoto_DTO = new GetClientResultPhoto_DTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<GetClientResultPhoto_DTO>(s);
                getClientResultPhoto_DTO.Client_Result_Photo_ID = Data.Client_Result_Photo_ID;
                getClientResultPhoto_DTO.Client_Result_Photo_Wo_ID = Data.Client_Result_Photo_Wo_ID;
                getClientResultPhoto_DTO.Type = Data.Type;

                var GetClientPhoto = await Task.Run(() => client_Result_PhotoData.GetClientResultPhotoDetails(getClientResultPhoto_DTO,1));

                return GetClientPhoto;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }



        // 
        //update client results photo
        [HttpPost]
        [Route("PostUpdateclientphoto")]
        public async Task<List<dynamic>> PostUpdateclientphoto()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Rootclientresultbefore>(s);

                Rootclientresultbefore rootclientresultbefore = new Rootclientresultbefore();
                ImageArray imageArray = new ImageArray();
                rootclientresultbefore.ImageArray = Data.ImageArray;
                rootclientresultbefore.Task_Bid_TaskID = Data.Task_Bid_TaskID;
                rootclientresultbefore.Client_Result_Photo_StatusType = Data.Client_Result_Photo_StatusType;
                Client_Result_PhotoDTO client_Result_PhotoDTO = new Client_Result_PhotoDTO();
                for (int i = 0; i < Data.ImageArray.Count; i++)
                {
                    client_Result_PhotoDTO.Client_Result_Photo_ID = Data.ImageArray[i].Client_Result_Photo_ID;
                    client_Result_PhotoDTO.Client_Result_Photo_StatusType = Data.Client_Result_Photo_StatusType;
                    client_Result_PhotoDTO.Client_Result_Photo_TaskId = Data.Task_Bid_TaskID;
                    client_Result_PhotoDTO.Client_Result_Photo_Task_Bid_pkeyID = Data.Client_Result_Photo_Task_Bid_pkeyID;
                    client_Result_PhotoDTO.Type = 5;
                    objdynamicobj = await Task.Run(() => client_Result_PhotoData.AddClientResultPhotoData(client_Result_PhotoDTO));
                }


                return objdynamicobj;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        // get single img for work order 

        [HttpPost]
        [Route("GetSingleImagedata")]
        public async Task<List<dynamic>> GetSingleImagedata()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                Client_Result_PhotoDTO client_Result_PhotoDTO = new Client_Result_PhotoDTO();


                var Data = JsonConvert.DeserializeObject<Client_Result_PhotoDTO>(s);
                client_Result_PhotoDTO.Client_Result_Photo_StatusType = Data.Client_Result_Photo_StatusType;
                client_Result_PhotoDTO.Client_Result_Photo_Wo_ID = Data.Client_Result_Photo_Wo_ID;


                var GetsinglePhoto = await Task.Run(() => client_Result_PhotoData.GetSingleImageInstructionDetails(client_Result_PhotoDTO));

                return GetsinglePhoto;




            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        private string CallAPI(string URL, string DATA)
        {
            string responseval = string.Empty;

            try
            {
                var client = new RestClient(URL);
                var request = new RestRequest(Method.POST);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Connection", "keep-alive");
                request.AddHeader("Content-Length", "70");
                request.AddHeader("Accept-Encoding", "gzip, deflate");
                request.AddHeader("Host", "us-central1-rare-lambda-245821.cloudfunctions.net");
                request.AddHeader("Postman-Token", "9426244d-41e4-486e-aa06-55ed1abbeec0,1df2c66a-58d9-4e0e-b804-7585fdd70002");
                request.AddHeader("Cache-Control", "no-cache");
                request.AddHeader("Accept", "*/*");
                request.AddHeader("User-Agent", "PostmanRuntime/7.20.1");
                request.AddHeader("Content-Type", "application/json");
                ///request.AddParameter("undefined", "{\r\n \"FileName\": \"Test.jpg\",\r\n \"FolderName\": \"Name of Folder in IPL\"\r\n}", ParameterType.RequestBody);
                request.AddParameter("undefined", DATA, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                //log.logDebugMessage("Request FireBase");
                //log.logDebugMessage(request.ToString());
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    responseval = response.Content;
                }
                else
                {
                    responseval = "Error While Retving";
                    log.logErrorMessage(" API Response" + response.StatusCode.ToString());
                }


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                responseval = "Error";
            }

            return responseval;

        }


        //Upload  image   client work order 
        [AllowAnonymous]
        [Authorize]
        [HttpPost]
       
        [Route("UploadClientResultPhtotos")]

        public async Task<List<dynamic>> UploadClientResultPhtotos()
        {
            string strURL = System.Configuration.ConfigurationManager.AppSettings["IntegrationURL"] + "download";
            string ReqData = string.Empty;
            System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
            System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
            System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
            string s = reader.ReadToEnd();
            List<dynamic> objdynamicobj = new List<dynamic>();
            Dictionary<string, object> dict = new Dictionary<string, object>();
            string strfilepath = String.Empty;
            Ret_UPload_Client_Result_PhotoDTO ret_UPload_Client_Result_PhotoDTO = new Ret_UPload_Client_Result_PhotoDTO();
            try
            {
                log.logInfoMessage("FireBaseIntegration  ----- Start");
                log.logInfoMessage(s);


                var Data = JsonConvert.DeserializeObject<UPload_Client_Result_PhotoDTO>(s);



                if (!string.IsNullOrEmpty(Data.Api_Error))
                {
                    log.logErrorMessage("Error While Uploading Document from Nodejs" + " _  " + Data.Api_Error);
                    log.logDebugMessage("FireBaseIntegration Error  ----- End");
                }
                else
                {

                    switch (Data.ContentType)
                    {
                        case 1:
                        case 2:
                            {
                                var config = new MapperConfiguration(cfg =>
                                {
                                    cfg.CreateMap<UPload_Client_Result_PhotoDTO, Client_Result_PhotoDTO>();
                                });

                                IMapper mapper = config.CreateMapper();
                                if (!string.IsNullOrEmpty(Data.IPLNO))
                                {
                                    Data.Client_Result_Photo_FolderName = Data.IPLNO.ToString();
                                }
                                var dest = mapper.Map<UPload_Client_Result_PhotoDTO, Client_Result_PhotoDTO>(Data);
                                dest.Client_Result_Photo_MeteringMode = Data.MeteringMode;
                                dest.Client_Result_Photo_objectName = Data.Client_Result_Photo_FileName;
                                string URLValue = string.Empty;
                                #region comment
                                //if (!string.IsNullOrWhiteSpace(Data.Client_Result_Photo_FilePath))
                                //{
                                //    dest.Client_Result_Photo_localPath = Data.Client_Result_Photo_FilePath;
                                //    dest.Client_Result_Photo_FilePath = Data.Client_Result_Photo_FilePath;
                                //    URLValue = Data.Client_Result_Photo_FilePath;
                                //}
                                //else
                                //{
                                //    //ReqData = "{\r\n\"FileName\" : "+ Data.Client_Result_Photo_FileName +",\t\r\n\"FolderName\":"+ Data.Client_Result_Photo_FolderName +"\r\n}";
                                //    ReqData = "{\r\n \"FileName\": \"" + Data.Client_Result_Photo_FileName + "\",\r\n \"FolderName\": \"" + Data.Client_Result_Photo_FolderName + "\"\r\n}";
                                //    URLValue = CallAPI(strURL, ReqData);
                                //   dest.Client_Result_Photo_localPath = URLValue;
                                //   dest.Client_Result_Photo_FilePath = URLValue;
                                //}
                                //ReqData = "{\r\n\"FileName\" : "+ Data.Client_Result_Photo_FileName +",\t\r\n\"FolderName\":"+ Data.Client_Result_Photo_FolderName +"\r\n}";
                                #endregion

                                #region uncomment while uploading in live
                                //ReqData = "{\r\n \"FileName\": \"" + Data.Client_Result_Photo_FileName + "\",\r\n \"FolderName\": \"" + Data.Client_Result_Photo_FolderName + "\"\r\n}";
                                //URLValue = CallAPI(strURL, ReqData);
                                //dest.Client_Result_Photo_localPath = URLValue;
                                //dest.Client_Result_Photo_FilePath = URLValue;
                                #endregion
                                dest.Client_Result_Photo_localPath = Data.Client_Result_Photo_FilePath;
                                dest.Client_Result_Photo_FilePath = Data.Client_Result_Photo_FilePath;

                                switch (Data.ContentType)
                                {
                                    case 1:
                                        {

                                            switch (Data.Client_PageCalled)
                                            {

                                                case 9: // Multiple WorkOrder Message  document
                                                    {
                                                        // log.logDebugMessage(" workOrder Message Docs");
                                                        WorkOrder_Message_DocumentData workOrder_Message_DocumentData = new WorkOrder_Message_DocumentData();
                                                        dest.UserID = LoggedInUSerId;
                                                        dest.ContentType = Data.ContentType;
                                                        var Val = workOrder_Message_DocumentData.AddUpdateWorkOrderMessageDocsData(dest);

                                                        try
                                                        {

                                                            objdynamicobj.Add(Val);
                                                            objdynamicobj.Add(URLValue);
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            log.logErrorMessage("Val issue 9");
                                                            log.logErrorMessage(ex.Message);
                                                            log.logErrorMessage(ex.StackTrace);

                                                        }

                                                        break;
                                                    }
                                                case 11: // upload Company App Logo
                                                    {
                                                        // log.logDebugMessage("App Logo");
                                                        AppCompanyMasterData appCompanyMasterData = new AppCompanyMasterData();
                                                        dest.UserID = LoggedInUSerId;
                                                        dest.ContentType = Data.ContentType;
                                                        dest.Type = 5;
                                                        var Val = appCompanyMasterData.AddUpdateCompanyApplogoData(dest);

                                                        try
                                                        {

                                                            objdynamicobj.Add(Val);
                                                            objdynamicobj.Add(URLValue);
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            log.logErrorMessage("Val issue 11");
                                                            log.logErrorMessage(ex.Message);
                                                            log.logErrorMessage(ex.StackTrace);

                                                        }

                                                        break;
                                                    }

                                                case 3:
                                                    {
                                                        AppCompanyMasterData appCompanyMasterData = new AppCompanyMasterData();
                                                        if (dest.Type == 1)
                                                        {
                                                            dest.Client_Result_Photo_UploadTimestamp = System.DateTime.Now;
                                                            dest.Client_Result_Photo_DateTimeOriginal = dest.Client_Result_Photo_UploadTimestamp;
                                                        }
                                                        dest.UserID = LoggedInUSerId;
                                                        var Val = appCompanyMasterData.AddUpdateCompanyPhotoData(dest);
                                                        if (Val.Count > 0)
                                                        {
                                                            ret_UPload_Client_Result_PhotoDTO.Status = Convert.ToInt32(Val[1]);
                                                            ret_UPload_Client_Result_PhotoDTO.Client_Result_Photo_ID = Val[0];
                                                            ret_UPload_Client_Result_PhotoDTO.Messsage = "Success";
                                                        }

                                                        objdynamicobj.Add(ret_UPload_Client_Result_PhotoDTO);


                                                        break;
                                                    }

                                                case 6: // background document
                                                    {
                                                        //log.logDebugMessage("Background User Docs");
                                                        UserMasterData userMasterData = new UserMasterData();
                                                        dest.UserID = LoggedInUSerId;
                                                        var Val = userMasterData.AddBackgroundDocumentMaster(dest);

                                                        try
                                                        {
                                                            if (Val.Count > 0)
                                                            {
                                                                ret_UPload_Client_Result_PhotoDTO.Status = Val[1];
                                                                ret_UPload_Client_Result_PhotoDTO.Client_Result_Photo_ID = Val[0];
                                                                ret_UPload_Client_Result_PhotoDTO.Messsage = "Success";
                                                            }
                                                            objdynamicobj.Add(ret_UPload_Client_Result_PhotoDTO);
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            log.logErrorMessage("Val issue 6");
                                                            log.logErrorMessage(ex.Message);
                                                            log.logErrorMessage(ex.StackTrace);

                                                        }

                                                        break;
                                                    }

                                                default:
                                                    {

                                                        //log.logDebugMessage(" photo Start");
                                                        if (dest.Type == 1)
                                                        {
                                                            dest.Client_Result_Photo_UploadTimestamp = System.DateTime.Now;
                                                            dest.Client_Result_Photo_DateTimeOriginal = dest.Client_Result_Photo_UploadTimestamp;
                                                        }
                                                        else if (dest.Client_Result_Photo_UploadTimestamp == null)
                                                        {
                                                            dest.Client_Result_Photo_UploadTimestamp = System.DateTime.Now;
                                                            dest.Client_Result_Photo_DateTimeOriginal = dest.Client_Result_Photo_UploadTimestamp;
                                                        }



                                                        dest.Client_Result_Photo_Type = 1; // for Photos
                                                                                           //log.logDebugMessage("PkeyVal for 7" + dest.Client_Result_Photo_Task_Bid_pkeyID);
                                                        dest.UserID = LoggedInUSerId;
                                                        dest.Client_Result_Photo_UploadBy = LoggedInUSerId.ToString();
                                                        var Val = await Task.Run(() => client_Result_PhotoData.AddClientResultPhotoData(dest));
                                                        if (Val.Count > 0)
                                                        {
                                                            ret_UPload_Client_Result_PhotoDTO.Status = Val[1];
                                                            ret_UPload_Client_Result_PhotoDTO.Client_Result_Photo_ID = Val[0];
                                                            ret_UPload_Client_Result_PhotoDTO.Messsage = "Success";
                                                        }

                                                        objdynamicobj.Add(ret_UPload_Client_Result_PhotoDTO);
                                                        break;
                                                    }
                                            }

                                            break;
                                        }
                                    case 2:
                                        {
                                            // log.logDebugMessage("Document");

                                            switch (Data.Client_PageCalled)
                                            {
                                                case 1: // From Instruction Page
                                                    {
                                                        InstructionDocumentData instructionDocumentData = new InstructionDocumentData();
                                                        dest.UserID = LoggedInUSerId;
                                                        var Val = await Task.Run(() => instructionDocumentData.AddInstructionDocumentData(dest));
                                                        if (Val.Count > 0)
                                                        {
                                                            ret_UPload_Client_Result_PhotoDTO.Status = Val[1];
                                                            ret_UPload_Client_Result_PhotoDTO.Client_Result_Photo_ID = Val[0];
                                                            ret_UPload_Client_Result_PhotoDTO.Messsage = "Success";
                                                        }

                                                        objdynamicobj.Add(ret_UPload_Client_Result_PhotoDTO);
                                                        break;
                                                    }


                                                case 2: // Task Master
                                                    {
                                                        // log.logDebugMessage("Task_Master");
                                                        Task_Master_Files_Data task_Master_Files_Data = new Task_Master_Files_Data();
                                                        Task_Master_Files_DTO task_Master_Files_DTO = new Task_Master_Files_DTO();
                                                        dest.UserID = LoggedInUSerId;
                                                        var Val = task_Master_Files_Data.AddUpdateTaskMasterData(dest);

                                                        //  log.logDebugMessage("Val count" + Val.Count);

                                                        try
                                                        {
                                                            if (Val.Count > 0)
                                                            {
                                                                ret_UPload_Client_Result_PhotoDTO.Messsage = "Success";
                                                                ret_UPload_Client_Result_PhotoDTO.Client_Result_Photo_ID = Val[0];
                                                            }
                                                            if (Val.Count > 1)
                                                            {
                                                                ret_UPload_Client_Result_PhotoDTO.Status = Val[1];
                                                            }
                                                            objdynamicobj.Add(ret_UPload_Client_Result_PhotoDTO);
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            log.logErrorMessage("Val issue 2");
                                                            log.logErrorMessage(ex.Message);
                                                            log.logErrorMessage(ex.StackTrace);
                                                            

                                                        }

                                                        break;

                                                    }
                                                case 3:
                                                    {
                                                        if (dest.Type == 1)
                                                        {
                                                            dest.Client_Result_Photo_UploadTimestamp = System.DateTime.Now;
                                                            dest.Client_Result_Photo_DateTimeOriginal = dest.Client_Result_Photo_UploadTimestamp;
                                                        }
                                                        dest.Client_Result_Photo_Type = 2; // for Documents
                                                        dest.UserID = LoggedInUSerId;
                                                        var Val = client_Result_PhotoData.AddClientResultPhotoData(dest);
                                                        if (Val.Count > 0)
                                                        {
                                                            ret_UPload_Client_Result_PhotoDTO.Status = Val[1];
                                                            ret_UPload_Client_Result_PhotoDTO.Client_Result_Photo_ID = Val[0];
                                                            ret_UPload_Client_Result_PhotoDTO.Messsage = "Success";
                                                        }

                                                        objdynamicobj.Add(ret_UPload_Client_Result_PhotoDTO);


                                                        break;
                                                    }

                                                case 4: //Forms and Docs
                                                    {
                                                        // log.logDebugMessage("Forms and Docs");
                                                        Folder_File_MasterData folder_File_MasterData = new Folder_File_MasterData();
                                                        dest.UserID = LoggedInUSerId;
                                                        // log.logDebugMessage("UserId===" + dest.UserID);
                                                        var Val = folder_File_MasterData.AddFormsDocFolder_File_Master(dest);

                                                        try
                                                        {
                                                            if (Val.Count > 0)
                                                            {
                                                                ret_UPload_Client_Result_PhotoDTO.Status = Val[1];
                                                                ret_UPload_Client_Result_PhotoDTO.Client_Result_Photo_ID = Val[0];
                                                                ret_UPload_Client_Result_PhotoDTO.Messsage = "Success";
                                                            }
                                                            objdynamicobj.Add(ret_UPload_Client_Result_PhotoDTO);
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            log.logErrorMessage("Val issue 4");
                                                            log.logErrorMessage(ex.Message);
                                                            log.logErrorMessage(ex.StackTrace);

                                                        }

                                                        break;
                                                    }

                                                case 5: //user document
                                                    {
                                                        // log.logDebugMessage("User Docs");
                                                        UserMasterData userMasterData = new UserMasterData();
                                                        dest.UserID = LoggedInUSerId;
                                                        var Val = userMasterData.AddUserDocumentMaster(dest);
                                                      
                                                        try
                                                        {
                                                            if (Val.Count > 0)
                                                            {
                                                                if (!string.IsNullOrWhiteSpace(((IPL.Models.UserDocument)Val[0]).Status))
                                                                {
                                                                    ret_UPload_Client_Result_PhotoDTO.Status = Convert.ToInt32(((IPL.Models.UserDocument)Val[0]).Status);
                                                                }
                                                                if (!string.IsNullOrWhiteSpace(((IPL.Models.UserDocument)Val[0]).User_Doc_pkeyID))
                                                                {
                                                                    ret_UPload_Client_Result_PhotoDTO.Client_Result_Photo_ID = Convert.ToInt32(((IPL.Models.UserDocument)Val[0]).User_Doc_pkeyID);
                                                                }
                                                                
                                                                ret_UPload_Client_Result_PhotoDTO.Messsage = "Success";
                                                            }
                                                            objdynamicobj.Add(ret_UPload_Client_Result_PhotoDTO);
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            log.logErrorMessage("Val issue 5");
                                                            log.logErrorMessage(ex.Message);
                                                            log.logErrorMessage(ex.StackTrace);

                                                        }

                                                        break;
                                                    }



                                                case 7: // Multiple Instruction docu,ment document
                                                    {
                                                        //  log.logDebugMessage(" Instruction Document Docs");
                                                        InstructionDocumentData instructionDocumentData = new InstructionDocumentData();
                                                        dest.UserID = LoggedInUSerId;
                                                        var Val = instructionDocumentData.AddUpdateInstructionDocsData(dest);

                                                        try
                                                        {
                                                            if (Val.Count > 0)
                                                            {
                                                                ret_UPload_Client_Result_PhotoDTO.Status = Val[1];
                                                                ret_UPload_Client_Result_PhotoDTO.Client_Result_Photo_ID = Val[0];
                                                                ret_UPload_Client_Result_PhotoDTO.Messsage = "Success";
                                                            }
                                                            objdynamicobj.Add(ret_UPload_Client_Result_PhotoDTO);
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            log.logErrorMessage("Val issue 7");
                                                            log.logErrorMessage(ex.Message);
                                                            log.logErrorMessage(ex.StackTrace);

                                                        }

                                                        break;
                                                    }
                                                case 8: // Multiple WorkOrder Office  document
                                                    {
                                                        // log.logDebugMessage(" workOrder Office Docs");
                                                        WorkOrder_Office_DocumentData workOrder_Office_DocumentData = new WorkOrder_Office_DocumentData();
                                                        dest.UserID = LoggedInUSerId;
                                                        var Val = workOrder_Office_DocumentData.AddUpdateWorkOrderOfficeDocsData(dest);

                                                        try
                                                        {

                                                            objdynamicobj.Add(Val);
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            log.logErrorMessage("Val issue 8");
                                                            log.logErrorMessage(ex.Message);
                                                            log.logErrorMessage(ex.StackTrace);

                                                        }

                                                        break;
                                                    }

                                                case 9: // Multiple WorkOrder Message  document
                                                    {
                                                        // log.logDebugMessage(" workOrder Message Docs");
                                                        WorkOrder_Message_DocumentData workOrder_Message_DocumentData = new WorkOrder_Message_DocumentData();
                                                        dest.UserID = LoggedInUSerId;
                                                        dest.ContentType = Data.ContentType;
                                                        var Val = workOrder_Message_DocumentData.AddUpdateWorkOrderMessageDocsData(dest);

                                                        try
                                                        {

                                                            objdynamicobj.Add(Val);
                                                            objdynamicobj.Add(URLValue);
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            log.logErrorMessage("Val issue 9");
                                                            log.logErrorMessage(ex.Message);
                                                            log.logErrorMessage(ex.StackTrace);

                                                        }

                                                        break;
                                                    }
                                                case 10: // upload Form Document
                                                    {
                                                        // log.logDebugMessage(" Form Docs");
                                                        Forms_Master_FilesData forms_Master_FilesData = new Forms_Master_FilesData();
                                                        dest.UserID = LoggedInUSerId;
                                                        dest.ContentType = Data.ContentType;
                                                        var Val = forms_Master_FilesData.AddUpdateFormDocument(dest);

                                                        try
                                                        {

                                                            objdynamicobj.Add(Val);
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            log.logErrorMessage("Val issue 10");
                                                            log.logErrorMessage(ex.Message);
                                                            log.logErrorMessage(ex.StackTrace);

                                                        }

                                                        break;
                                                    }

                                                case 11: // Mass MeMo
                                                    {
                                                        

                                                        try
                                                        {
                                                            URLValue = dest.Client_Result_Photo_FilePath;
                                                            objdynamicobj.Add(URLValue);
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            log.logErrorMessage("Val issue 11");
                                                            log.logErrorMessage(ex.Message);
                                                            log.logErrorMessage(ex.StackTrace);

                                                        }

                                                        break;
                                                    }

                                                case 12:
                                                    {
                                                        try
                                                        {
                                                            Client_Result_Photo_Data client_Result_Photo_Data = new Client_Result_Photo_Data();
                                                            if (!string.IsNullOrEmpty(Data.IPLNO))
                                                            {
                                                                Data.Client_Result_Photo_FolderName = Data.IPLNO.ToString();
                                                            }

                                                            ReqData = "{\r\n \"FileName\": \"" + Data.Client_Result_Photo_FileName + "\",\r\n \"FolderName\": \"" + Data.Client_Result_Photo_FolderName + "\"\r\n}";
                                                            URLValue = CallAPI(strURL, ReqData);
                                                            client_Result_Photo_Data.Client_Result_Photo_FilePath = URLValue;
                                                            objdynamicobj.Add(client_Result_Photo_Data);
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            log.logErrorMessage("Val issue 12");
                                                            log.logErrorMessage(ex.Message);
                                                            log.logErrorMessage(ex.StackTrace);

                                                        }
                                                        
                                                        break;
                                                    }
                                                case 13:
                                                    {
                                                        //  log.logDebugMessage("Email Call from node js---------------------------------->");
                                                        //   log.logDebugMessage("Email Call from node js Filename---------------------------------->" + Data.Client_Result_Photo_FileName);
                                                        Client_Result_Photo_Data client_Result_Photo_Data = new Client_Result_Photo_Data();
                                                      

                                                        client_Result_Photo_Data.Client_Result_Photo_FilePath = URLValue;
                                                        objdynamicobj.Add(client_Result_Photo_Data);
                                                        break;
                                                    }

                                            }

                                            #region comment

                                            //if (dest.Client_Result_Photo_Ch_ID == 0)
                                            //{
                                            //    dest.Client_Result_Photo_Type = 2;
                                            //    var Val = await Task.Run(() => client_Result_PhotoData.AddClientResultPhotoData(dest));
                                            //    if (Val.Count > 0)
                                            //    {
                                            //        ret_UPload_Client_Result_PhotoDTO.Status = Val[1];
                                            //        ret_UPload_Client_Result_PhotoDTO.Client_Result_Photo_ID = Val[0];
                                            //        ret_UPload_Client_Result_PhotoDTO.Messsage = "Success";
                                            //    }

                                            //}
                                            //else
                                            //{
                                            //    InstructionDocumentData instructionDocumentData = new InstructionDocumentData();
                                            //    var Val = await Task.Run(() => instructionDocumentData.AddInstructionDocumentData(dest));
                                            //    if (Val.Count > 0)
                                            //    {
                                            //        ret_UPload_Client_Result_PhotoDTO.Status = Val[1];
                                            //        ret_UPload_Client_Result_PhotoDTO.Client_Result_Photo_ID = Val[0];
                                            //        ret_UPload_Client_Result_PhotoDTO.Messsage = "Success";
                                            //    }

                                            //}


                                            #endregion

                                            break;
                                        }
                                }

                                break;
                            }
                                
                        case 3:
                            {
                                var config = new MapperConfiguration(cfg =>
                                {
                                    cfg.CreateMap<UPload_Client_Result_PhotoDTO, ProfileUserMaster>();
                                });

                                IMapper mapper = config.CreateMapper();
                                var dest = mapper.Map<UPload_Client_Result_PhotoDTO, ProfileUserMaster>(Data);
                                if (!string.IsNullOrEmpty(Data.IPLNO))
                                {
                                    Data.Client_Result_Photo_FolderName = Data.IPLNO.ToString();
                                }
                                //ReqData = "{\r\n\"FileName\" : "+ Data.Client_Result_Photo_FileName +",\t\r\n\"FolderName\":"+ Data.Client_Result_Photo_FolderName +"\r\n}";
                                ReqData = "{\r\n \"FileName\": \"" + Data.Client_Result_Photo_FileName + "\",\r\n \"FolderName\": \"" + Data.Client_Result_Photo_FolderName + "\"\r\n}";
                                string URLValue = CallAPI(strURL, ReqData);

                                dest.User_ImagePath = URLValue;
                                UserMasterData userMasterData = new UserMasterData();
                                dest.UserID = LoggedInUSerId;
                                objdynamicobj = userMasterData.UpdateProfile(dest);
                                break;
                            }

                        case 4: // For photos from PPW 
                            {
                                // log.logDebugMessage("PPW Photos Call from node js---------------------------------->");
                                // log.logDebugMessage("PPW Photos Call from node js Filename---------------------------------->" + Data.Client_Result_Photo_FileName);

                                if (!string.IsNullOrEmpty(Data.IPLNO))
                                {
                                    Data.Client_Result_Photo_FolderName = Data.IPLNO.ToString();
                                }
                                // log.logDebugMessage("PPW Photos Call from node js Filename---------------------------------->" + Data.Client_Result_Photo_FolderName);
                                //ReqData = "{\r\n\"FileName\" : "+ Data.Client_Result_Photo_FileName +",\t\r\n\"FolderName\":"+ Data.Client_Result_Photo_FolderName +"\r\n}";
                                ReqData = "{\r\n \"FileName\": \"" + Data.Client_Result_Photo_FileName + "\",\r\n \"FolderName\": \"" + Data.Client_Result_Photo_FolderName + "\"\r\n}";
                                string URLValue = CallAPI(strURL, ReqData);
                                Data.Client_Result_Photo_FilePath = URLValue;
                                WorkOrderMasterImportPhotoData workOrderMasterImportPhotoData = new WorkOrderMasterImportPhotoData();
                                objdynamicobj = workOrderMasterImportPhotoData.AddUpdateWorkOrderMasterImportPhotoData(Data);
                                break;
                            }
                        case 5:
                        case 6:
                            {
                                //  log.logDebugMessage("Email Call from node js---------------------------------->");
                                //   log.logDebugMessage("Email Call from node js Filename---------------------------------->" + Data.Client_Result_Photo_FileName);

                                if (!string.IsNullOrEmpty(Data.IPLNO))
                                {
                                    Data.Client_Result_Photo_FolderName = Data.IPLNO.ToString();
                                }
                                //   log.logDebugMessage("Email Call from node js Filename---------------------------------->" + Data.Client_Result_Photo_FolderName);
                                //ReqData = "{\r\n\"FileName\" : "+ Data.Client_Result_Photo_FileName +",\t\r\n\"FolderName\":"+ Data.Client_Result_Photo_FolderName +"\r\n}";
                                ReqData = "{\r\n \"FileName\": \"" + Data.Client_Result_Photo_FileName + "\",\r\n \"FolderName\": \"" + Data.Client_Result_Photo_FolderName + "\"\r\n}";
                                string URLValue = CallAPI(strURL, ReqData);
                                Data.Client_Result_Photo_FilePath = URLValue;
                                Mass_File_MasterData mass_File_MasterData = new Mass_File_MasterData();
                                objdynamicobj = mass_File_MasterData.AddMassAttachedData(Data);
                                break;
                            }

                        case 7:
                            {
                                //  log.logDebugMessage("Email Call from node js---------------------------------->");
                                //   log.logDebugMessage("Email Call from node js Filename---------------------------------->" + Data.Client_Result_Photo_FileName);
                                Client_Result_Photo_Data client_Result_Photo_Data = new Client_Result_Photo_Data();
                                if (!string.IsNullOrEmpty(Data.IPLNO))
                                {
                                    Data.Client_Result_Photo_FolderName = Data.IPLNO.ToString();
                                }
                                //   log.logDebugMessage("Email Call from node js Filename---------------------------------->" + Data.Client_Result_Photo_FolderName);
                                //ReqData = "{\r\n\"FileName\" : "+ Data.Client_Result_Photo_FileName +",\t\r\n\"FolderName\":"+ Data.Client_Result_Photo_FolderName +"\r\n}";
                                ReqData = "{\r\n \"FileName\": \"" + Data.Client_Result_Photo_FileName + "\",\r\n \"FolderName\": \"" + Data.Client_Result_Photo_FolderName + "\"\r\n}";
                                string URLValue = CallAPI(strURL, ReqData);

                                client_Result_Photo_Data.Client_Result_Photo_FilePath = URLValue;
                                objdynamicobj.Add(client_Result_Photo_Data);
                                break;
                            }
                    }




                    #region Old
                    //if (dest.Type == 1)
                    //{
                    //    dest.Client_Result_Photo_UploadTimestamp = System.DateTime.Now;et
                    //    dest.Client_Result_Photo_DateTimeOriginal = dest.Client_Result_Photo_UploadTimestamp;
                    //    dest.Client_Result_Photo_IsActive = true;
                    //}
                    //var Val = await Task.Run(() => client_Result_PhotoData.AddClientResultPhotoData(dest));
                    //if (Val.Count > 0)
                    //{
                    //    ret_UPload_Client_Result_PhotoDTO.Status = Val[1];
                    //    ret_UPload_Client_Result_PhotoDTO.Client_Result_Photo_ID = Val[0];
                    //    ret_UPload_Client_Result_PhotoDTO.Messsage = "Success";
                    //}
                    //objdynamicobj.Add(ret_UPload_Client_Result_PhotoDTO);
                    #endregion


                    //log.logDebugMessage("FireBaseIntegration Sucess ----- End");
                }

                return objdynamicobj;



            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                ret_UPload_Client_Result_PhotoDTO.Status = 0;
                ret_UPload_Client_Result_PhotoDTO.Messsage = ex.Message;


                objdynamicobj.Add(ret_UPload_Client_Result_PhotoDTO);
                return objdynamicobj;
            }


        }




        // test 
        [HttpPost]

        [AllowAnonymous]
        [Route("ClientResultPhtotosHttpRequest")]

        public async Task<HttpResponseMessage> ClientResultPhtotosHttpRequest()
        {

            System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
            System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
            System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
            string s = reader.ReadToEnd();
            string strURL = System.Configuration.ConfigurationManager.AppSettings["IntegrationURL"] + "download";
            List<dynamic> objdynamicobj = new List<dynamic>();
            string ReqData = string.Empty;
            Dictionary<string, object> dict = new Dictionary<string, object>();
            string strfilepath = String.Empty;
            Ret_UPload_Client_Result_PhotoDTO ret_UPload_Client_Result_PhotoDTO = new Ret_UPload_Client_Result_PhotoDTO();

            try
            {

                var httpRequest = HttpContext.Current.Request;


                //var str = httpRequest.Form.AllKeys[0];

                string formobj = HttpContext.Current.Server.UrlDecode(httpRequest.Form.AllKeys[0]);

                //var Data = JsonConvert.DeserializeObject<photosRoots>(formobj);

                var Data = JsonConvert.DeserializeObject<UPload_Client_Result_PhotoDTO>(formobj);
                ReqData = "{\r\n \"FileName\": \"" + Data.Client_Result_Photo_FileName + "\",\r\n \"FolderName\": \"" + Data.Client_Result_Photo_FolderName + "\"\r\n}";
                string URLValue = CallAPI(strURL, ReqData);
                var config = new MapperConfiguration(cfg =>
                {
                    cfg.CreateMap<UPload_Client_Result_PhotoDTO, Client_Result_PhotoDTO>();
                });

                IMapper mapper = config.CreateMapper();

                //ReqData = "{\r\n\"FileName\" : "+ Data.Client_Result_Photo_FileName +",\t\r\n\"FolderName\":"+ Data.Client_Result_Photo_FolderName +"\r\n}";

                var dest = mapper.Map<UPload_Client_Result_PhotoDTO, Client_Result_PhotoDTO>(Data);

                dest.Client_Result_Photo_localPath = URLValue;
                dest.Client_Result_Photo_objectName = Data.Client_Result_Photo_FileName;
                dest.Client_Result_Photo_FilePath = URLValue;

                switch (Data.ContentType)
                {
                    case 1:
                        {

                            if (dest.Type == 1)
                            {
                                dest.Client_Result_Photo_UploadTimestamp = System.DateTime.Now;
                                dest.Client_Result_Photo_DateTimeOriginal = dest.Client_Result_Photo_UploadTimestamp;
                            }
                            var Val = await Task.Run(() => client_Result_PhotoData.AddClientResultPhotoData(dest));
                            if (Val.Count > 0)
                            {
                                ret_UPload_Client_Result_PhotoDTO.Status = Val[1];
                                ret_UPload_Client_Result_PhotoDTO.Client_Result_Photo_ID = Val[0];
                                ret_UPload_Client_Result_PhotoDTO.Messsage = "Success";
                            }

                            objdynamicobj.Add(ret_UPload_Client_Result_PhotoDTO);
                            break;
                        }
                    case 2:
                        {
                            InstructionDocumentData instructionDocumentData = new InstructionDocumentData();
                            var Val = await Task.Run(() => instructionDocumentData.AddInstructionDocumentData(dest));
                            if (Val.Count > 0)
                            {
                                ret_UPload_Client_Result_PhotoDTO.Status = Val[1];
                                ret_UPload_Client_Result_PhotoDTO.Client_Result_Photo_ID = Val[0];
                                ret_UPload_Client_Result_PhotoDTO.Messsage = "Success";
                            }

                            objdynamicobj.Add(ret_UPload_Client_Result_PhotoDTO);
                            break;
                        }
                }


                //return objdynamicobj;
                var message1 = string.Format("Document Updated Successfully..!");
                //var message2 = strfilepath;
                //String result = message1;
                var msg = await Task.Run(() => Request.CreateErrorResponse(HttpStatusCode.Created, message1));
                return msg;


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                var res = string.Format("Server Error, To Large File Size");
                dict.Add("Message", res);
                return Request.CreateResponse(HttpStatusCode.Created, dict);
            }

        }


        //photo file name
        [Authorize]
        [AllowAnonymous]
        [HttpPost]
        [Route("GetPhotoFileName")]
        public async Task<List<dynamic>> GetPhotoFileName()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                //log.logDebugMessage("-----------GetPhotoFileName called-----------");
                //log.logDebugMessage(s);
                var Data = JsonConvert.DeserializeObject<ClientResultsPhotoFileNameDTO>(s);

                var GetImagename = await Task.Run(() => clientResultsPhotoFileNameData.Get_Client_Result_Photo_Name_Details(Data));
                return GetImagename;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("AddClientResultFoHData")]

        public async Task<List<dynamic>> AddClientResultFoHData()
        {
            ClientResultFoHData clientResultFoHData = new ClientResultFoHData();
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<ClientResultFohDTO>(s);
                Data.UserID = LoggedInUSerId;

                if (!string.IsNullOrEmpty(Data.PhotoArrayJson))
                {
                    List<dynamic> dynamicsReturn = new List<dynamic>();
                    List<ClientResultFohDTO> unlableArrayJson = JsonConvert.DeserializeObject<List<ClientResultFohDTO>>(Data.PhotoArrayJson);
                    foreach (var clientResult in unlableArrayJson)
                    {
                        if (clientResult.CRP_New_pkeyId > 0)
                        {
                            clientResult.Type = Data.Type;
                            clientResult.UserID = Data.UserID;
                            clientResult.Client_Result_Photo_Wo_ID = Data.Client_Result_Photo_Wo_ID;
                            var AddFoH = await Task.Run(() => clientResultFoHData.Add_Client_Photo_FoH_Master(clientResult));
                            dynamicsReturn.Add(AddFoH);
                        }
                    }
                    return dynamicsReturn;
                }
                else
                {
                    var AddFoH = await Task.Run(() => clientResultFoHData.Add_Client_Photo_FoH_Master(Data));
                    return AddFoH;
                }
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("MoveCopyClientPhotoData")]

        public async Task<List<dynamic>> MoveCopyClientPhotoData()
        {

            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<WorkOrderMoveCopyPhotoDTO>(s);
                Data.UserID = LoggedInUSerId;

                var Addmcp = await Task.Run(() => client_Result_PhotoData.AddMoveCopy_ClientPhoto_Data(Data));
                return Addmcp;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

        //workorderupdatepic data
        [Authorize]
        [HttpPost]
        [Route("UpdateCLientResultPhotosMaster")]
        public async Task<List<dynamic>> UpdateCLientResultPhotosMaster()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            WorkOrderPicsData workOrderPicsData = new WorkOrderPicsData();
            WorkOrderPicsDTO workOrderPicsDTO = new WorkOrderPicsDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<GetClientResultPhoto_DTO>(s);
                workOrderPicsDTO.Client_Result_Photo_ID = Data.Client_Result_Photo_ID;
                workOrderPicsDTO.WorkOrder_ID = Convert.ToInt64(Data.Client_Result_Photo_Wo_ID);
                workOrderPicsDTO.Type = Data.Type;

                var updateClientPhoto = await Task.Run(() => workOrderPicsData.UpdateByWorkorderID(workOrderPicsDTO));

                return updateClientPhoto;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //update photo time And date
        [Authorize]
        [HttpPost]
        [Route("UpdateCLientResultPhotosTimeStamp")]
        public async Task<List<dynamic>> UpdateCLientResultPhotosTimeStamp()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Client_Result_PhotoDTO>(s);
                Data.Client_Result_Photo_DateTimeOriginal = Convert.ToDateTime(Data.Client_Result_Photo_DateTimeOriginal);
                Data.Client_Result_Photo_UploadTimestamp = Convert.ToDateTime(Data.Client_Result_Photo_UploadTimestamp);
                Data.UserID = LoggedInUSerId;

                var updateClientPhoto = await Task.Run(() => client_Result_PhotoData.UpdateClientResultPhoto_Data(Data));

                return updateClientPhoto;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //client result photo history
        [Authorize]
        [HttpPost]
        [Route("GetCLientResultPhotosMasterHistory")]
        public async Task<List<dynamic>> GetCLientResultPhotosMasterHistory()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            GetClientResultPhoto_DTO getClientResultPhoto_DTO = new GetClientResultPhoto_DTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<GetClientResultPhoto_DTO>(s);
                getClientResultPhoto_DTO.Client_Result_Photo_ID = Data.Client_Result_Photo_ID;
                getClientResultPhoto_DTO.Client_Result_Photo_Wo_ID = Data.Client_Result_Photo_Wo_ID;
                getClientResultPhoto_DTO.Type = Data.Type;

                var GetClientPhoto = await Task.Run(() => client_Result_PhotoData.GetClientResultPhotosHistoryDetails(getClientResultPhoto_DTO));

                return GetClientPhoto;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //client result photo Info
        [Authorize]
        [HttpPost]
        [Route("GetClientResultPhotoInfo")]
        public async Task<List<dynamic>> GetClientResultPhotoInfo()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<GetClientResultPhoto_DTO>(s);
                var GetClientPhoto = await Task.Run(() => client_Result_PhotoData.GetClientResultPhotoInfoData(Data));

                return GetClientPhoto;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //client result photo with text
        [AllowAnonymous]
        [HttpPost]
        [Route("DownLoadTextImageData")]
        public async Task<List<dynamic>> DownLoadTextImageData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<GetClientResultPhoto_DTO>(s);
                var GetClientPhoto = await Task.Run(() => client_Result_PhotoData.DownLoadTextImage());

                return GetClientPhoto;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("ClientResultPhotoTemp")]
        public async Task<List<dynamic>> ClientResultPhotoTemp()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {

                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                Ret_UPload_Client_Result_PhotoDTO ret_UPload_Client_Result_PhotoDTO = new Ret_UPload_Client_Result_PhotoDTO();
                string s = reader.ReadToEnd();
                log.logInfoMessage("-------------------ClientResultPhotoTemp start-----------");
                log.logInfoMessage(s);
                log.logInfoMessage("-------------------ClientResultPhotoTemp end-----------");
                var Data = JsonConvert.DeserializeObject<Client_Result_Photo_Temp>(s);
                Data.UserID = LoggedInUSerId;
                var Val = await Task.Run(() => client_Result_PhotoData.AddUpdatePhotoTempData(Data));
                if (Val.Count > 0)
                {
                    ret_UPload_Client_Result_PhotoDTO.Status = Val[1];
                    ret_UPload_Client_Result_PhotoDTO.Client_Result_Photo_ID = Val[0];
                    ret_UPload_Client_Result_PhotoDTO.Messsage = "Success";
                }
                objdynamicobj.Add(ret_UPload_Client_Result_PhotoDTO);
                return objdynamicobj;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("UpdateByWorkorderIDFromAPI")]
        public async Task<List<dynamic>> UpdateByWorkorderIDFromAPI()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            WorkOrderPicsData workOrderPicsData = new WorkOrderPicsData();
            WorkOrderPicsDTO workOrderPicsDTO = new WorkOrderPicsDTO();

            try
            {

                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);

                string s = reader.ReadToEnd();
                log.logInfoMessage("-------------------UpdateByWorkorderIDFromAPI start-----------");
                log.logInfoMessage(s);
                log.logInfoMessage("-------------------UpdateByWorkorderIDFromAPI end-----------");
                var Data = JsonConvert.DeserializeObject<WorkOrderPicsDTO>(s);
                var Val = await Task.Run(() => workOrderPicsData.UpdateByWorkorderIDFromAPI(Data));

                return Val;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [Authorize]
        [HttpPost]
        [Route("PostDeleteClientResultsPhotos")]
        public async Task<List<dynamic>> PostDeleteClientResultsPhotos()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<ClientResultFohDTO>(s);
                Data.UserID = LoggedInUSerId;


                if (!string.IsNullOrEmpty(Data.PhotoArrayJson))
                {
                    List<ClientResultFohDTO> selectedPhotoList = JsonConvert.DeserializeObject<List<ClientResultFohDTO>>(Data.PhotoArrayJson);
                    ClientResultPhotoDTO clientResultPhotoDTO = new ClientResultPhotoDTO();
                    clientResultPhotoDTO.Client_Result_Photo_Wo_ID = Data.Client_Result_Photo_Wo_ID;
                    clientResultPhotoDTO.Client_Result_Photo_ID_Comma = string.Join(",", selectedPhotoList.Where(x => x.Client_Result_Photo_ID > 0).Select(x => x.Client_Result_Photo_ID.ToString()).ToArray());
                    clientResultPhotoDTO.UserID = LoggedInUSerId;
                    clientResultPhotoDTO.Type = 1;
                    objdynamicobj = await Task.Run(() => client_Result_PhotoData.DeletedClientResultPhotos(clientResultPhotoDTO));

                }
                return objdynamicobj;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("GetdeleteWorkmasterDataDetails")]
        public async Task<List<dynamic>> GetdeleteWorkmasterDataDetails()
        {

            List<dynamic> objdynamicobj = new List<dynamic>();
            WorkOrderMasterData workOrderMasterData = new WorkOrderMasterData();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<GetdeleteWorkmasterDataDTO>(s);
                Data.UserID = LoggedInUSerId;


                var GetBidPhoto = await Task.Run(() => workOrderMasterData.GetdeleteWorkmasterDataDetails(Data));

                return GetBidPhoto;


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


    }
}
