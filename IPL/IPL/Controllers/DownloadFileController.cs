﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Avigma.Repository.Lib;
using ICSharpCode.SharpZipLib.Zip;
using Newtonsoft.Json;

namespace IPL.Controllers
{
    public class DownloadFileController : Controller
    {
        // GET: DownloadFile
        Log log = new Log();
        public FileResult DownloadZipFile(string ImgPath)
        {

            try
            {

                if (ImgPath == null)
                {

                    return null;
                }

                //Install-Package SharpZipLib -Version 1.2.0
                var fileName = string.Format("{0}_ImageFiles.zip", DateTime.Today.Date.ToString("dd-MM-yyyy") + "_1");
                var tempOutPutPath = Server.MapPath(Url.Content("/Document/")) + fileName;

                using (ZipOutputStream s = new ZipOutputStream(System.IO.File.Create(tempOutPutPath)))
                {
                    s.SetLevel(9); // 0-9, 9 being the highest compression  

                    byte[] buffer = new byte[4096];

                    var ImageList = new List<string>();

                    string[] imgPathArray = ImgPath.Split(',');

                    foreach (var item in imgPathArray)
                    {
                        ImageList.Add(Server.MapPath(item));
                    }

                    //ImageList.Add(Server.MapPath("//Document//Download//191158//f6278467-7640-4af2-b1a0-5cde1e09d3b9.jpg"));
                    for (int i = 0; i < ImageList.Count; i++)
                    {
                        ZipEntry entry = new ZipEntry(Path.GetFileName(ImageList[i]));
                        entry.DateTime = DateTime.Now;
                        entry.IsUnicodeText = true;
                        s.PutNextEntry(entry);

                        using (FileStream fs = System.IO.File.OpenRead(ImageList[i]))
                        {
                            int sourceBytes;
                            do
                            {
                                sourceBytes = fs.Read(buffer, 0, buffer.Length);
                                s.Write(buffer, 0, sourceBytes);
                            } while (sourceBytes > 0);
                        }
                    }
                    s.Finish();
                    s.Flush();
                    s.Close();

                }

                byte[] finalResult = System.IO.File.ReadAllBytes(tempOutPutPath);
                if (System.IO.File.Exists(tempOutPutPath))
                    System.IO.File.Delete(tempOutPutPath);

                if (finalResult == null || !finalResult.Any())
                    throw new Exception(String.Format("No Files found with Image"));

                return File(finalResult, "application/zip", fileName);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return null;

            }
        }

        public FileStreamResult SingleDownload(string ImgPath)
        {

            try
            {
                var ext = ImgPath.Substring(ImgPath.LastIndexOf('.'));
                Stream Returnstreamval = null;
                HttpWebRequest aRequest = (HttpWebRequest)WebRequest.Create(ImgPath);
                HttpWebResponse aResponse = (HttpWebResponse)aRequest.GetResponse();
                var fileName = string.Format("{0}_ImageFiles" + ext, DateTime.Today.Date.ToString("dd-MM-yyyy") + "_1");
                Returnstreamval = aResponse.GetResponseStream();
                return File(Returnstreamval, "image/jpeg", fileName);

            }
            catch (Exception ex)
            {

                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return null;
            }
        }
    }
}