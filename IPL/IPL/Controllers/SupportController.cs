﻿using Avigma.Repository.Lib;
using IPL.Models;
using IPL.Repository.data;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace IPL.Controllers
{
    [RoutePrefix("api/Support")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]

    public class SupportController : BaseController
    {
        Log log = new Log();
        ContactUsData contactUsData = new ContactUsData();
        Support_Ticket_Setting_MasterData spport_Ticket_Setting_MasterData = new Support_Ticket_Setting_MasterData();
        Support_Ticket_Document_MasterData support_Ticket_Document_MasterData = new Support_Ticket_Document_MasterData();
        Support_Setting_ReplyData support_Setting_ReplyData = new Support_Setting_ReplyData();
        TrainingVedioData trainingVedioData = new TrainingVedioData();


        [Authorize]
        [HttpPost]
        [Route("PostContactDetails")]

        public async Task<List<dynamic>> PostContactDetails()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<ContactUsDTO>(s);
                 Data.UserID = LoggedInUSerId;


                var AddContact = await Task.Run(() => contactUsData.AddContactDetailsMaster(Data));
                return AddContact;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }


        [Authorize]
        [HttpPost]
        [Route("GetContactDetails")]

        public async Task<List<dynamic>> GetContactDetails()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<ContactUsDTO>(s);
                Data.UserID = LoggedInUSerId;


                var GetContact = await Task.Run(() => contactUsData.Get_Contact_Details_Master(Data));
                return GetContact;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }
        [Authorize]
        [HttpPost]
        [Route("PostIPLAutoSupportticket")]
        public async Task<List<dynamic>> PostIPLAutoSupportticket()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            IPLAutoGenrateIDDTO iPLAutoGenrateIDDTO = new IPLAutoGenrateIDDTO();
            IPLAutoFillIDData iPLAutoFillIDData = new IPLAutoFillIDData();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<IPLAutoGenrateIDDTO>(s);

                iPLAutoGenrateIDDTO.Type = Data.Type;

                var AddAutoticket = await Task.Run(() => iPLAutoFillIDData.AddIPLAutoGenrateSupportTicket(iPLAutoGenrateIDDTO));
                return AddAutoticket;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        //Support ticket Add data
        [Authorize]
        [HttpPost]
        [Route("PostSupportTicketData")]
        public async Task<List<dynamic>> PostSupportTicketData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Support_Ticket_Setting_MasterDTO>(s);
                Data.UserID = Convert.ToInt64(LoggedInUSerId);
                var GetSupportTicket = await Task.Run(() => spport_Ticket_Setting_MasterData.AddupdateSupportTicketDetails(Data));
                return GetSupportTicket;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [Authorize]
        [HttpPost]
        [Route("GetSupportTicketData")]
        public async Task<List<dynamic>> GetSupportTicketData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Support_Ticket_Setting_MasterDTO>(s);
                Data.UserID = Convert.ToInt64(LoggedInUSerId);
                switch (Data.Type)
                {
                    case 1:
                        {
                            objdynamicobj = await Task.Run(() => spport_Ticket_Setting_MasterData.GetSupportTicketDetails(Data));
                            break;
                        }
                    case 2:
                        {

                            objdynamicobj = await Task.Run(() => spport_Ticket_Setting_MasterData.GetSupportTicketDetails(Data));
                            break;
                        }
                    case 3:
                        {
                            objdynamicobj = await Task.Run(() => spport_Ticket_Setting_MasterData.GetSingleSupportTicketDetails(Data));

                            break;
                        }

                }
               
             
                return objdynamicobj;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        //reply support data
        //Support ticket Add data
        [Authorize]
        [HttpPost]
        [Route("PostSupportTicketReplyData")]
        public async Task<List<dynamic>> PostSupportTicketReplyData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Support_Setting_ReplyDTO>(s);
                Data.UserID = Convert.ToInt64(LoggedInUSerId);
                var postSupportTicketreply = await Task.Run(() => support_Setting_ReplyData.AddupdateSupportTicketReply(Data));
                return postSupportTicketreply;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [Authorize]
        [HttpPost]
        [Route("GetSupportTicketReplyData")]
        public async Task<List<dynamic>> GetSupportTicketReplyData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Support_Setting_ReplyDTO>(s);
                Data.UserID = Convert.ToInt64(LoggedInUSerId);
                var GetSupportTicketreply = await Task.Run(() => support_Setting_ReplyData.GetSupportTicketDetails(Data));
                return GetSupportTicketreply;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //support document post
        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("AddSupportTicketDocument")]
        public async Task<List<dynamic>> AddSupportTicketDocument()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage(s);
                var Data = JsonConvert.DeserializeObject<Support_Ticket_Document_MasterDTO>(s);
                Data.Type = 1;
                Data.UserID = LoggedInUSerId;
                Data.Support_Docs_IsActive = true;
                Data.Support_Docs_IsDelete = false;

                string strURL = System.Configuration.ConfigurationManager.AppSettings["IntegrationURL"] + "download";
                FileUploadDownload fileUploadDownload = new FileUploadDownload();
                string ReqData = "{\r\n \"FileName\": \"" + Data.Support_Docs_File_Name + "\",\r\n \"FolderName\": \"" + Data.Support_Docs_Folder_Name + "\"\r\n}";
                string strPath = fileUploadDownload.CallAPI(strURL, ReqData);
                Data.Support_Docs_File_Path = strPath;
                var repairbaseDetails = await Task.Run(() => support_Ticket_Document_MasterData.AddupdateSupportTicket_DocumentDetails(Data));

                return repairbaseDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

        [Authorize]
        [HttpPost]
        [Route("GetTrainingVedioData")]
        public async Task<List<dynamic>> GetTrainingVedioData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<TrainingVedioDTO>(s);
                 Data.UserID = Convert.ToInt64(LoggedInUSerId);
                var GetTraining = await Task.Run(() => trainingVedioData.Get_Get_TrainingVedoMasterDetails(Data));
                return GetTraining;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [Authorize]
        [HttpPost]
        [Route("PostTrainingVedioData")]
        public async Task<List<dynamic>> PostTrainingVedioData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<TrainingVedioDTO>(s);
                Data.UserID = Convert.ToInt64(LoggedInUSerId);
                var AddTraining = await Task.Run(() => trainingVedioData.AddVedioTrainingMaster(Data));
                return AddTraining;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [Authorize]
        [HttpPost]
        [Route("DeleteTrainingVedioData")]
        public async Task<List<dynamic>> DeleteTrainingVedioData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<TrainingVedioDTO>(s);
                Data.UserID = Convert.ToInt64(LoggedInUSerId);
                var deltraining = await Task.Run(() => trainingVedioData.AddVedioTrainingMaster(Data));
                return deltraining;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
    }
}
