﻿using IPL.Models;
using IPL.Repository.data;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using Avigma.Repository.Lib;

namespace IPL.Controllers
{
    [RoutePrefix("api/Report")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ReportController : BaseController
    {
        ReportMasterData reportMasterData = new ReportMasterData();
        Client_Invoice_PatymentData client_Invoice_PatymentData = new Client_Invoice_PatymentData();
        Contractor_Invoice_PaymentData contractor_Invoice_PaymentData = new Contractor_Invoice_PaymentData();
        AdvanceReportData advanceReportData = new AdvanceReportData();
        Report_WO_Filter_MasterData report_WO_Filter_MasterData = new Report_WO_Filter_MasterData();
        Log log = new Log();

        [Authorize]
        [HttpPost]
        [Route("GetReportDetail")]
        public async Task<List<dynamic>> GetReportDetail()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<ReportMasterDTO>(s);
                //Added by Dipali
                Data.UserId = LoggedInUSerId;
                var getReport = await Task.Run(() => reportMasterData.GetReportDetails(Data));
                objdynamicobj.Add(getReport);
                //if (Data.IsClientCheck)
                //{
                //    Data.Valtype = 1;
                //    var getReport = await Task.Run(() => reportMasterData.GetReportDetails(Data));
                //    objdynamicobj.Add(getReport);
                //}
                //if (Data.IsContractorCheck)
                //{
                //    Data.Valtype = 2;
                //    var getReport = await Task.Run(() => reportMasterData.GetReportDetails(Data));
                //    objdynamicobj.Add(getReport);
                //}
                return objdynamicobj;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        // Added By Dipali
        [Authorize]
        [HttpPost]
        [Route("ClientPaymentDataList")]
        public async Task<List<dynamic>> ClientPaymentDataList()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<List<Client_Invoice_PatymentDTO>>(s);
                Data.ForEach(d => d.UserID = LoggedInUSerId);

                objdynamicobj = await Task.Run(() => client_Invoice_PatymentData.AddClientPaymentDataList(Data));

                return objdynamicobj;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }
        [Authorize]
        [HttpPost]
        [Route("ContractorPaymentDataList")]
        public async Task<List<dynamic>> ContractorPaymentDataList()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<List<Contractor_Invoice_PaymentDTO>>(s);
                Data.ForEach(d => d.UserID = LoggedInUSerId);
                Data.ForEach(d => d.Con_Pay_EnteredBy = LoggedInUSerId.ToString());
                objdynamicobj = await Task.Run(() => contractor_Invoice_PaymentData.AddContractorPaymentDataList(Data));

                return objdynamicobj;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }
        [Authorize]
        [HttpPost]
        [Route("GetAdvanceReportDropDown")]
        public async Task<List<dynamic>> GetAdvanceReportDropDownDetail()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<ReportType_MasterDTO>(s);
                Data.UserID = LoggedInUSerId;
                var getReport = await Task.Run(() => advanceReportData.GetReportTypeMasterDaata(Data));
                objdynamicobj.Add(getReport);
                return objdynamicobj;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [Authorize]
        [HttpPost]
        [Route("GetAdvanceReportDetail")]
        public async Task<List<dynamic>> GetAdvanceReportDetail()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<AdvanceReportMasterDTO>(s);
                Data.UserId = LoggedInUSerId;
                var getReport = await Task.Run(() => advanceReportData.GetAdvanceReportDetails(Data));
                return getReport;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

        [Authorize]
        [HttpPost]
        [Route("PostWorkOrderFilterData")]
        public async Task<List<dynamic>> PostWorkOrderFilterData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Report_WO_Filter_MasterDTO>(s);
                Data.UserID = Convert.ToInt32(LoggedInUSerId);

                var Returnkey = await Task.Run(() => advanceReportData.AddWorkOrderFilterData(Data));
                return Returnkey;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }
        [Authorize]
        [HttpPost]
        [Route("GetReportWOFilterChildDetail")]
        public async Task<List<dynamic>> GetReportWOFilterChildDetail()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<AdvanceReportMasterDTO>(s);

                Report_WO_Filter_ChildDTO report_WO_Filter_ChildDTO = new Report_WO_Filter_ChildDTO();
                report_WO_Filter_ChildDTO.UserID = LoggedInUSerId;
                report_WO_Filter_ChildDTO.Type = Data.Type;
                report_WO_Filter_ChildDTO.Report_WO_Filter_Ch_Master_FKeyId = Data.WoFilterId;
                var getReport = await Task.Run(() => report_WO_Filter_MasterData.GetReportWOFilterChildData(report_WO_Filter_ChildDTO));
                return getReport;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }
        [Authorize]
        [HttpPost]
        [Route("DeleteWorkOrderFilterChildData")]
        public async Task<List<dynamic>> DeleteWorkOrderFilterChildData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Report_WO_Filter_MasterDTO>(s);
                Data.UserID = Convert.ToInt32(LoggedInUSerId);

                var Returnkey = await Task.Run(() => advanceReportData.DeleteWorkOrderFilterChildData(Data));
                return Returnkey;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

        [Authorize]
        [HttpPost]
        [Route("ReportPdfDetailsData")]
        public async Task<HttpResponseMessage> ReportPdfDetailsData()
        {

            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<ReportMasterDTO>(s);
                CustomReportPDF customReportPDF = new CustomReportPDF();
                Data.UserId = Convert.ToInt32(LoggedInUSerId);

                var Returnkey = await Task.Run(() => reportMasterData.GeneratePDFStringreportdetails(Data));
                Returnkey.HttpStatusCode = HttpStatusCode.OK;
                byte[] respbytes = Returnkey.Data;
                response.Content = new ByteArrayContent(respbytes);
                response.Content.Headers.ContentLength = respbytes.LongLength;
                response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = "ReportDetails" + ".pdf";
                response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/pdf");

                return response;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return response;
            }
        }

        // Advance report pdf
        [Authorize]
        [HttpPost]
        [Route("AdvanceReportPdfDetailsData")]
        public async Task<HttpResponseMessage> AdvanceReportPdfDetailsData()
        {

            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<AdvanceReportMasterDTO>(s);
                CustomReportPDF customReportPDF = new CustomReportPDF();
                Data.UserId = Convert.ToInt32(LoggedInUSerId);

                var Returnkey = await Task.Run(() => advanceReportData.GeneratePDFAdvanceReportdetails(Data));
                Returnkey.HttpStatusCode = HttpStatusCode.OK;
                byte[] respbytes = Returnkey.Data;
                response.Content = new ByteArrayContent(respbytes);
                response.Content.Headers.ContentLength = respbytes.LongLength;
                response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = "ReportDetails" + ".pdf";
                response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/pdf");

                return response;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return response;
            }
        }

        [Authorize]
        [HttpPost]
        [Route("GetAdvanceProTimeReportDetail")]
        public async Task<List<dynamic>> GetAdvanceProTimeReportDetail()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<AdvanceReportMasterDTO>(s);
                Data.UserId = LoggedInUSerId;
                var getReport = await Task.Run(() => advanceReportData.GetAdvanceProTimeReportDetails(Data));
                return getReport;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

    }
}
