﻿using Avigma.Repository.Lib;
using IPL.HelperMessage;
using IPL.Models.Account;
using IPL.Repository.Account;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace IPL.Controllers.Account
{
    [RoutePrefix("api/Task")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class TaskController : BaseController
    {
        Acc_Task_Data acc_Task_Data = new Acc_Task_Data();
        Log log = new Log();
        [Authorize]
        [HttpPost]
        [Route("CreateUpdateAccTask")]
        public async Task<Custom_Response> CreateUpdateAccTask()
        {
            Custom_Response responce = new Custom_Response();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Acc_Task_DTO>(s);
                Data.UserID = Convert.ToInt32(LoggedInUSerId);
                 responce = await Task.Run(() => acc_Task_Data.AddTaskMasterData(Data));
                if (responce.HttpStatusCode != HttpStatusCode.OK)
                {
                    return responce;
                }

            }
            catch (Exception ex)
            {
                responce.Message = Message.ErrorWhileSaving;
                responce.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return responce;
            }
            return responce;

        }
        [Authorize]
        [HttpPost]
        [Route("GetTaskMasterList")]
        public async Task<Custom_Response> GetTaskMasterList()
        {
            Custom_Response custom_Response = new Custom_Response();
             Acc_Task_DTO taskMasterDTO = new Acc_Task_DTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Acc_Task_DTO>(s);
                Data.UserID = LoggedInUSerId;
                switch (Data.Type)
                {
                    case 1:
                        {
                            custom_Response.Data = await Task.Run(() => acc_Task_Data.GetTaskMasterDetails(Data));
                            break;
                        }
                    case 2:
                        {
                            custom_Response.Data = await Task.Run(() => acc_Task_Data.GetTaskMasterDetails(Data));
                            break;
                        }
                    case 3:
                        {
                            custom_Response.Data = await Task.Run(() => acc_Task_Data.GetTaskFilterDetails(Data));
                            break;
                        }
                    case 4:
                        {
                            custom_Response.Data = await Task.Run(() => acc_Task_Data.GetTaskFilterDetails(Data));
                            break;
                        }
                }

                custom_Response.HttpStatusCode = HttpStatusCode.OK;
                return custom_Response;

            }
            catch (Exception ex)
            {
                custom_Response.Message = Message.GettingErrorWhileBindList;
                custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return custom_Response;
            }
        }
        [HttpGet]
        [Route("GetAccTask")]
        [Authorize]
        public async Task<Custom_Response> GetAccTask(int Id)
        {
            Custom_Response custom_Response = new Custom_Response();
            try
            {
                Acc_Task_DTO acc_Task_DTO = new Acc_Task_DTO();
                acc_Task_DTO.Type = 2;
                acc_Task_DTO.Acc_Task_pkeyId = Id;
                acc_Task_DTO.UserID = LoggedInUSerId;
                custom_Response.Data = await Task.Run(() => acc_Task_Data.GetTaskMasterDetails(acc_Task_DTO));
                custom_Response.HttpStatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                custom_Response.Message = Message.GettingErrorWhileBindList;
                custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return custom_Response;
            }
            return custom_Response;
        }
        [HttpGet]
        [Route("DeleteAccTask")]
        [Authorize]
        public async Task<Custom_Response> DeleteAccTask(int Id)
        {
            Custom_Response custom_Response = new Custom_Response();
            try
            {
                Acc_Task_DTO acc_Task_DTO = new Acc_Task_DTO();
                acc_Task_DTO.Type = 4;
                acc_Task_DTO.Acc_Task_pkeyId = Id;
                acc_Task_DTO.UserID = LoggedInUSerId;
                custom_Response.Data = await Task.Run(() => acc_Task_Data.AddTaskMasterData(acc_Task_DTO));
                custom_Response.HttpStatusCode = HttpStatusCode.OK;
                custom_Response.Message = Message.SuccessDelete;
            }
            catch (Exception  ex)
            {
                custom_Response.Message = Message.GettingErrorWhileBindList;
                custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return custom_Response;
            }
            return custom_Response;
        }

    }
}
