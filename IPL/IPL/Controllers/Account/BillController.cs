﻿using Avigma.Repository.Lib;
using IPL.HelperMessage;
using IPL.Models.Account;
using IPL.Repository.Account;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace IPL.Controllers.Account
{
    [RoutePrefix("api/Bill")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]

    public class BillController : BaseController
    {
        Acc_Bill_Data acc_Bill_Data = new Acc_Bill_Data();
        Log log = new Log();
        [HttpGet]
        [Route("GetBillList")]
        [Authorize]
        public async Task<Custom_Response> GetBillList()
        {
            Custom_Response custom_Response = new Custom_Response();

            try
            {
                Acc_Bill_DTO acc_Bill_DTO = new Acc_Bill_DTO();
                acc_Bill_DTO.Type = 1;
                acc_Bill_DTO.UserID = LoggedInUSerId;
                custom_Response.Data = await Task.Run(() => acc_Bill_Data.GetBillMaster(acc_Bill_DTO));
                custom_Response.HttpStatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                custom_Response.Message = Message.GettingErrorWhileBindList;
                custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return custom_Response;
            }
            return custom_Response;
        }
        [HttpGet]
        [Route("GetBill")]
        [Authorize]
        public async Task<Custom_Response> GetBill(int BillId)
        {
            Custom_Response custom_Response = new Custom_Response();
            try
            {
                Acc_Bill_DTO acc_Bill_DTO = new Acc_Bill_DTO();
                acc_Bill_DTO.Type = 2;
                acc_Bill_DTO.UserID = LoggedInUSerId;
                acc_Bill_DTO.Bill_pkeyId = BillId;
                custom_Response.Data = await Task.Run(() => acc_Bill_Data.GetBill(acc_Bill_DTO));
                custom_Response.HttpStatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                custom_Response.Message = Message.GettingErrorWhileBindList;
                custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return custom_Response;
            }
            return custom_Response;
        }

        [HttpPost]
        [Route("CreateUpdateBill")]
        [Authorize]
        public async Task<Custom_Response> CreateUpdateBill()
        {
            Custom_Response responce = new Custom_Response();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Acc_Bill_DTO>(s);
                Data.UserID = LoggedInUSerId;
                responce = await Task.Run(() => acc_Bill_Data.AddBillData(Data));
                if (responce.HttpStatusCode != HttpStatusCode.OK)
                {
                    return responce;
                }
            }
            catch (Exception ex)
            {
                responce.Message = Message.ErrorWhileSaving;
                responce.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return responce;
            }
            return responce;
        }
        [HttpPost]
        [Route("CreateUpdateBillPayment")]
        [Authorize]
        public async Task<Custom_Response> CreateUpdateBillPayment()
        {
            Custom_Response responce = new Custom_Response();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Acc_Bill_Payment_DTO>(s);
                Data.UserID = LoggedInUSerId;
                responce = await Task.Run(() => acc_Bill_Data.AddBillPaymentData(Data));
                if (responce.HttpStatusCode != HttpStatusCode.OK)
                {
                    return responce;
                }
            }
            catch (Exception ex)
            {
                responce.Message = Message.ErrorWhileSaving;
                responce.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return responce;
            }
            return responce;
        }
        [HttpGet]
        [Route("GetBillByVendorId")]
        [Authorize]
        public async Task<Custom_Response> GetBillByVendorId(long VendorId, string Bill_Number)
        {
            Custom_Response custom_Response = new Custom_Response();
            try
            {
                Acc_Bill_Receive_PaymentDTO  acc_Bill_Receive_Payment = new Acc_Bill_Receive_PaymentDTO();
                if (Bill_Number != null && !string.IsNullOrEmpty(Bill_Number))
                {
                    acc_Bill_Receive_Payment.Type = 2;
                    acc_Bill_Receive_Payment.Bill_Number = Bill_Number;
                }
                else
                {
                    acc_Bill_Receive_Payment.Type = 1;
                }
                acc_Bill_Receive_Payment.UserID =LoggedInUSerId;
                acc_Bill_Receive_Payment.Bill_Rec_Vendor_Id = VendorId;
                custom_Response = await Task.Run(() => acc_Bill_Data.GetBillByVendor(acc_Bill_Receive_Payment));
                return custom_Response;
            }
            catch (Exception ex)
            {
                custom_Response.Message = Message.ErrorWhileSaving;
                custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return custom_Response;
            }
        }
        [HttpPost]
        [Route("CreateUpdateBillReceivePayment")]
        [Authorize]
        public async Task<Custom_Response> CreateUpdateBillReceivePayment()
        {
            Custom_Response responce = new Custom_Response();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Acc_Bill_Receive_PaymentDTO>(s);
                Data.UserID = LoggedInUSerId;
                responce = await Task.Run(() => acc_Bill_Data.CheckBillReceivePayment(Data));

            }
            catch (Exception ex)
            {
                responce.Message = Message.ErrorWhileSaving;
                responce.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return responce;
            }
            return responce;
        }
        [HttpGet]
        [Route("GetPaidBillList")]
        [Authorize]
        public async Task<Custom_Response> GetPaidBillList()
        {
            Custom_Response custom_Response = new Custom_Response();

            try
            {
                Acc_Bill_Bank_Deposit_DTO acc_Bill_Bank_Deposit_DTO = new Acc_Bill_Bank_Deposit_DTO();
                acc_Bill_Bank_Deposit_DTO.UserID = LoggedInUSerId;
                custom_Response = await Task.Run(() => acc_Bill_Data.PaidBillList(acc_Bill_Bank_Deposit_DTO));
                return custom_Response;

            }
            catch (Exception ex)
            {
                custom_Response.Message = Message.GettingErrorWhileBindList;
                custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return custom_Response;
            }
        }
        [HttpPost]
        [Route("BillBankDeposit")]
        [Authorize]
        public async Task<Custom_Response> BillBankDeposit()
        {
            Custom_Response responce = new Custom_Response();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Acc_Bill_Bank_Deposit_DTO>(s);
                if (Data.Deposit_To > 0)
                {
                    Data.UserID = LoggedInUSerId;
                    foreach (var item in Data.BankDeposit_Items)
                    {
                        Acc_Bill_Receive_PaymentDTO dto = new Acc_Bill_Receive_PaymentDTO();
                        dto.Bill_Rec_PkeyId = item.Bill_Rec_PkeyId;
                        dto.Bill_pkeyId = item.Bill_Id;
                        dto.Bill_Rec_Payment_Date = Data.PaymentDate;
                        dto.Bill_Rec_Amount = item.Amount;
                        dto.Bill_Rec_Payment_Method = item.Payment_Method;
                        dto.Bill_Rec_Reference_No = item.Ref_No;
                        dto.Bill_Rec_Deposit_To = Data.Deposit_To;
                        dto.Bill_Rec_Memo = item.Memo;
                        dto.UserID = LoggedInUSerId;
                        dto.Type = 1;
                        responce = await Task.Run(() => acc_Bill_Data.BillBankDepositData(dto));
                    }
                }
                else
                {
                    responce.Message = Message.ErrorWhileSaving;
                    responce.HttpStatusCode = HttpStatusCode.InternalServerError;

                    return responce;
                }

            }
            catch (Exception ex)
            {
                responce.Message = Message.ErrorWhileSaving;
                responce.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return responce;
            }
            return responce;
        }


    }
}