﻿using Avigma.Repository.Lib;
using IPL.HelperMessage;
using IPL.Models.Account;
using IPL.Repository.Account;
using IPLApp.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace IPL.Controllers.Account
{
    [RoutePrefix("api/Client")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ClientController : BaseController
    {
        Acc_Data acc_Client_Data = new Acc_Data();
        Log log = new Log();
        [HttpPost]
        [Route("CreateUpdateClient")]
        [Authorize]
        public async Task<Custom_Response> CreateUpdateClient()
        {
            Custom_Response responce = new Custom_Response();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Acc_Client>(s);
                Data.UserID = LoggedInUSerId;
                responce = await Task.Run(() => acc_Client_Data.CreateUpdateClient(Data));
                if (responce.HttpStatusCode != HttpStatusCode.OK)
                {
                    return responce;
                }
            }
            catch (Exception ex)
            {
                responce.Message = Message.ErrorWhileSaving;
                responce.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return responce;
            }
            return responce;
        }
        [Authorize]
        [HttpPost]
        [Route("GetClientList")]
        public async Task<List<dynamic>> GetClientList()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            ClientDTO clientCompanyDTO = new ClientDTO();
            SearchMasterDTO searchMasterDTO = new SearchMasterDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<ClientDTO>(s);
                ClientDTO ClientDTOObj = new ClientDTO();
                ClientDTOObj.Acc_Client_pkeyId = Data.Acc_Client_pkeyId;
                ClientDTOObj.Type = Data.Type;
                ClientDTOObj.UserId = LoggedInUSerId;
                clientCompanyDTO.UserId = LoggedInUSerId;
                switch (Data.Type)
                {
                    case 2:
                        {
                            objdynamicobj = await Task.Run(() => acc_Client_Data.GetClientCompanyMasterDetails(ClientDTOObj));
                            break;
                        }
                    case 3:
                        {
                            objdynamicobj = await Task.Run(() => acc_Client_Data.GetAccountClientList(ClientDTOObj));

                            break;
                        }
                    //case 4:
                    //    {
                    //        objdynamicobj = await Task.Run(() => acc_Client_Data.GetAccountClientList(ClientDTOObj));

                    //        break;
                    //    }
                }
               



            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return objdynamicobj;
            }

            return objdynamicobj;

        }
    }
}
