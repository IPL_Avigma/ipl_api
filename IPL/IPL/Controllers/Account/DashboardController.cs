﻿using Avigma.Repository.Lib;
using IPL.HelperMessage;
using IPL.Models.Account;
using IPL.Repository.Account;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace IPL.Controllers.Account
{
    [RoutePrefix("api/Dashboard")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]

    public class DashboardController : BaseController
    {
        Acc_Dashboard_Data acc_DashBoard = new Acc_Dashboard_Data();
        Log log = new Log();
        //[HttpGet]
        //[Route("GetDashborad")]
        //[Authorize]
        //public async Task<dynamic> GetDashborad()
        //{
        //    return await Task.Run(() => acc_DashBoard.GetAccountStats());
        //}
        [HttpPost]
        [Route("GetDefaultDashboradDetails")]
        [Authorize]
        public async Task<Custom_Response> GetDefaultDashboradDetails()
        {
            Custom_Response responce = new Custom_Response();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Acc_Dashboard_Input_DTO>(s);
                Data.UserID = LoggedInUSerId;
                responce = await acc_DashBoard.GetDefaultDashBoardDetails(Data);
            }
            catch (Exception ex)
            {
                responce.Message = Message.ErrorWhileSaving;
                responce.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return responce;
            }
            return responce;
        }

    }
}