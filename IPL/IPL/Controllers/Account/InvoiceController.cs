﻿using Avigma.Repository.Lib;
using IPL.HelperMessage;
using IPL.Models.Account;
using IPL.Repository.Account;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace IPL.Controllers.Account
{
    [RoutePrefix("api/Invoice")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]

    public class InvoiceController : BaseController
    {
        Acc_Invoice_Data acc_Invoice_Data = new Acc_Invoice_Data();
        Log log = new Log();
        [HttpGet]
        [Route("GetInvoiceList")]
        [Authorize]
        public async Task<Custom_Response> GetInvoiceList()
        {
            Custom_Response custom_Response = new Custom_Response();
            try
            {
                Acc_Invoice_DTO acc_Invoice_DTO = new Acc_Invoice_DTO();
                acc_Invoice_DTO.Type = 1;
                acc_Invoice_DTO.UserID = LoggedInUSerId;
                custom_Response.Data = await Task.Run(() => acc_Invoice_Data.GetInvoiceMaster(acc_Invoice_DTO));
                custom_Response.HttpStatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                custom_Response.Message = Message.GettingErrorWhileBindList;
                custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return custom_Response;
            }
            return custom_Response;
        }

        [HttpGet]
        [Route("GetInvoice")]
        [Authorize]
        public async Task<Custom_Response> GetInvoice(int InvoiceId)
        {
            Custom_Response custom_Response = new Custom_Response();
            try
            {
                Acc_Invoice_DTO acc_Invoice_DTO = new Acc_Invoice_DTO();
                acc_Invoice_DTO.Type = 2;
                acc_Invoice_DTO.Invoice_pkeyId = InvoiceId;
                acc_Invoice_DTO.UserID = LoggedInUSerId;
                custom_Response.Data = await Task.Run(() => acc_Invoice_Data.GetInvoice(acc_Invoice_DTO));
                custom_Response.HttpStatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                custom_Response.Message = Message.GettingErrorWhileBindList;
                custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return custom_Response;
            }
            return custom_Response;
        }

        [HttpPost]
        [Route("CreateUpdateInvoice")]
        [Authorize]
        public async Task<Custom_Response> CreateUpdateInvoice()
        {
            Custom_Response responce = new Custom_Response();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Acc_Invoice_DTO>(s);
                Data.UserID = LoggedInUSerId;
                responce = await Task.Run(() => acc_Invoice_Data.AddInvoiceData(Data));
                if (responce.HttpStatusCode != HttpStatusCode.OK)
                {
                    return responce;
                }
            }
            catch (Exception ex)
            {
                responce.Message = Message.ErrorWhileSaving;
                responce.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return responce;
            }
            return responce;
        }
        [HttpPost]
        [Route("CreateUpdateInvoicePayment")]
        [Authorize]
        public async Task<Custom_Response> CreateUpdateInvoicePayment()
        {
            Custom_Response responce = new Custom_Response();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Acc_Invoice_Payment_DTO>(s);
                Data.UserID = LoggedInUSerId;
                responce = await Task.Run(() => acc_Invoice_Data.AddInvoicePaymentData(Data));
                if (responce.HttpStatusCode != HttpStatusCode.OK)
                {
                    return responce;
                }
            }
            catch (Exception ex)
            {
                responce.Message = Message.ErrorWhileSaving;
                responce.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return responce;
            }
            return responce;
        }
        [HttpPost]
        [Route("CreateUpdateInvoiceReceivePayment")]
        [Authorize]
        public async Task<Custom_Response> CreateUpdateInvoiceReceivePayment()
        {
            Custom_Response responce = new Custom_Response();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Acc_Invoice_Receive_Payment_DTO>(s);
                Data.UserID = LoggedInUSerId;
                responce = await Task.Run(() => acc_Invoice_Data.CheckInvoiceReceivePayment(Data));
                
            }
            catch (Exception ex)
            {
                responce.Message = Message.ErrorWhileSaving;
                responce.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return responce;
            }
            return responce;
        }
        [HttpGet]
        [Route("GetInvoiceByCustomerId")]
        [Authorize]
        public async Task<Custom_Response> GetInvoiceByCustomerId(long CustomerId, string Invoice_Number)
        {
            Custom_Response custom_Response = new Custom_Response();
            try
            {
                Acc_Invoice_Receive_Payment_DTO acc_Invoice_Receive_Payment_DTO = new Acc_Invoice_Receive_Payment_DTO();
                if (Invoice_Number != null && !string.IsNullOrEmpty(Invoice_Number))
                {
                    acc_Invoice_Receive_Payment_DTO.Type = 2;
                    acc_Invoice_Receive_Payment_DTO.Invoice_Number = Invoice_Number;
                }
                else
                {
                    acc_Invoice_Receive_Payment_DTO.Type = 1;
                }

                acc_Invoice_Receive_Payment_DTO.UserID = LoggedInUSerId;
                acc_Invoice_Receive_Payment_DTO.Invoice_Rec_Customer_Id = CustomerId;
                custom_Response = await Task.Run(() => acc_Invoice_Data.GetInvoiceByCusomerId(acc_Invoice_Receive_Payment_DTO));
                return custom_Response;
            }
            catch (Exception ex)
            {
                custom_Response.Message = Message.ErrorWhileSaving;
                custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return custom_Response;
            }
        }
        [HttpGet]
        [Route("GetPaidInvoiceList")]
        [Authorize]
        public async Task<Custom_Response> GetPaidInvoiceList()
        {
            Custom_Response custom_Response = new Custom_Response();

            try
            {
                Acc_Invoice_Bank_Deposit_DTO acc_Invoice_Bank_Deposit_DTO = new Acc_Invoice_Bank_Deposit_DTO();
                acc_Invoice_Bank_Deposit_DTO.UserID = LoggedInUSerId;
                custom_Response = await Task.Run(() => acc_Invoice_Data.PaidInvoiceList(acc_Invoice_Bank_Deposit_DTO));
                return custom_Response;

            }
            catch (Exception ex)
            {
                custom_Response.Message = Message.GettingErrorWhileBindList;
                custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return custom_Response;
            }
        }
        [HttpPost]
        [Route("InvoiceBankDeposit")]
        [Authorize]
        public async Task<Custom_Response> InvoiceBankDeposit()
        {
            Custom_Response responce = new Custom_Response();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Acc_Invoice_Bank_Deposit_DTO>(s);
                if (Data.Deposit_To > 0)
                {
                    Data.UserID = LoggedInUSerId;
                    foreach (var item in Data.BankDeposit_Items)
                    {
                        Acc_Invoice_Receive_Payment_DTO dto = new Acc_Invoice_Receive_Payment_DTO();
                        dto.Invoice_Rec_PkeyId = item.Invoice_Rec_PkeyId;
                        dto.Invoice_pkeyId = item.Invoice_Id;
                        dto.Invoice_Rec_Payment_Date = Data.PaymentDate;
                        dto.Invoice_Rec_Amount = item.Amount;
                        dto.Invoice_Rec_Payment_Method = item.Payment_Method;
                        dto.Invoice_Rec_Reference_No = item.Ref_No;
                        dto.Invoice_Rec_Deposit_To = Data.Deposit_To;
                        dto.Invoice_Rec_Memo = item.Memo;
                        dto.UserID = LoggedInUSerId;
                        dto.Type = 1;
                        responce = await Task.Run(() => acc_Invoice_Data.InvoiceBankDepositData(dto));
                    }
                }
                else
                {
                    responce.Message = Message.ErrorWhileSaving;
                    responce.HttpStatusCode = HttpStatusCode.InternalServerError;
                 
                    return responce;
                }
                
            }
            catch (Exception ex)
            {
                responce.Message = Message.ErrorWhileSaving;
                responce.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return responce;
            }
            return responce;
        }
    }
}