﻿using Avigma.Repository.Lib;
using IPL.HelperMessage;
using IPL.Models.Account;
using IPL.Repository.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace IPL.Controllers.Account
{
    [RoutePrefix("api/Ledger")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class LedgerController : BaseController
    {
        Acc_Ledger_Header_Data acc_Ledger_MasterData = new Acc_Ledger_Header_Data();
        Log log = new Log();
        [HttpGet]
        [Route("GetLedgerList")]
        [Authorize]
        public async Task<Custom_Response> GetLedgerList()
        {
            Custom_Response custom_Response = new Custom_Response();
            try
            {
                Acc_Ledger_Headers_DTO acc_Ledger_DTO = new Acc_Ledger_Headers_DTO();
                acc_Ledger_DTO.Type = 1;
                acc_Ledger_DTO.UserID = LoggedInUSerId;
                custom_Response.Data = await Task.Run(() => acc_Ledger_MasterData.GetLedgerEntries(acc_Ledger_DTO));
                custom_Response.HttpStatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                custom_Response.Message = Message.GettingErrorWhileBindList;
                custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return custom_Response;
            }
            return custom_Response;
        }
        [HttpGet]
        [Route("GetLedger")]
        [Authorize]
        public async Task<Custom_Response> GetLedger(int id)
        {
            Custom_Response custom_Response = new Custom_Response();
            try
            {
                Acc_Ledger_Headers_DTO acc_Ledger_DTO = new Acc_Ledger_Headers_DTO();
                acc_Ledger_DTO.Type = 1;
                acc_Ledger_DTO.LedgH_pkeyId = id;
                acc_Ledger_DTO.UserID = LoggedInUSerId;
                custom_Response.Data = await Task.Run(() => acc_Ledger_MasterData.GetLedgerEntries(acc_Ledger_DTO).FirstOrDefault());
                custom_Response.HttpStatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                custom_Response.Message = Message.GettingErrorWhileBindList;
                custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return custom_Response;
            }
            return custom_Response;
        }
    }
}