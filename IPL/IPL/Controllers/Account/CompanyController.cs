﻿using Avigma.Repository.Lib;
using IPL.HelperMessage;
using IPL.Models.Account;
using IPL.Repository.Account;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace IPL.Controllers.Account
{
    [RoutePrefix("api/Company")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
  
    public class CompanyController : BaseController
    {
        Acc_Companies_Data acc_Companies_Data = new Acc_Companies_Data();
        Log log = new Log();
        [HttpGet]
        [Route("GetCompanyDetails")]
        [Authorize]
        public async Task<Custom_Response> GetCompanyDetails()
        {
            Custom_Response custom_Response = new Custom_Response();
            try
            {
                Acc_Company_DTO acc_Company_DTO = new Acc_Company_DTO();
                acc_Company_DTO.Type = 1;
                acc_Company_DTO.UserID = LoggedInUSerId;
                custom_Response.Data = await Task.Run(() => acc_Companies_Data.GetCompanyDetail(acc_Company_DTO));
                custom_Response.HttpStatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                custom_Response.Message = Message.ErrorWhileSaving;
                custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return custom_Response;
            }
            return custom_Response;
        }
     
        [HttpPost]
        [Route("PostCompany")]
        [Authorize]
        public async Task<Custom_Response> PostCompany()
        {

            Custom_Response custom_Response = new Custom_Response();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                Acc_Company_DTO acc_Company_DTO = JsonConvert.DeserializeObject<Acc_Company_DTO>(s);
                acc_Company_DTO.Type = 2;
                acc_Company_DTO.UserID = LoggedInUSerId;

                custom_Response = await Task.Run(() => acc_Companies_Data.AddCompanyData(acc_Company_DTO));
                return custom_Response;
            }
            catch (Exception ex)
            {
                custom_Response.Message = Message.ErrorWhileSaving;
                custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return custom_Response;
            }
        }

    }
}