﻿using Avigma.Repository.Lib;
using IPL.HelperMessage;
using IPL.Models.Account;
using IPL.Repository.Account;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace IPL.Controllers.Account
{

    [RoutePrefix("api/Journal")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class JournalController : BaseController
    {
        Acc_Journal_Entry_Headers_Data acc_Journal_MasterData = new Acc_Journal_Entry_Headers_Data();
        Log log = new Log();
        [HttpGet]
        [Route("GetJournalList")]
        [Authorize]
        public async Task<Custom_Response> GetJournalList()
        {
            Custom_Response custom_Response = new Custom_Response();
            try
            {
                Acc_Journal_Entry_Headers_DTO acc_Jounal_DTO = new Acc_Journal_Entry_Headers_DTO();
                acc_Jounal_DTO.Type = 1;
                acc_Jounal_DTO.UserID = LoggedInUSerId;
                IEnumerable<Acc_Journal_Entry_Headers_DTO> List = await Task.Run(() => acc_Journal_MasterData.GetJournalEntries(acc_Jounal_DTO));
                custom_Response.Data = List;
                custom_Response.HttpStatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                custom_Response.Message = Message.GettingErrorWhileBindList;
                custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return custom_Response;
            }
            return custom_Response;
        }

        [HttpGet]
        [Route("GetJournalById")]
        [Authorize]
        public async Task<Custom_Response> GetJournalById(int Journal_Id)
        {
            Custom_Response custom_Response = new Custom_Response();
            try
            {
                Acc_Journal_Entry_Headers_DTO acc_Jounal_DTO = new Acc_Journal_Entry_Headers_DTO();
                acc_Jounal_DTO.Type = 2;
                acc_Jounal_DTO.JrnlH_pkeyId = Journal_Id;
                acc_Jounal_DTO.UserID = LoggedInUSerId;
                custom_Response.Data = await Task.Run(() => acc_Journal_MasterData.GetJournalEntries(acc_Jounal_DTO).FirstOrDefault());
                custom_Response.HttpStatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                custom_Response.Message = Message.GettingErrorWhileBindList;
                custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return custom_Response;
            }
            return custom_Response;
        }
        [HttpPost]
        [Route("CreateUpdateJournal")]
        [Authorize]
        public async Task<Custom_Response> CreateUpdateJournal()
        {
            Custom_Response custom_Response = new Custom_Response();
            try
            {
            System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
            System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
            System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
            string s = reader.ReadToEnd();

            var Data = JsonConvert.DeserializeObject<Acc_Journal_Entry_Headers_DTO>(s);
            Data.UserID = LoggedInUSerId;
            //if (Data.JrnlH_JournalEntry != null)
            //{
            //    var anyDuplicate = Data.JrnlH_JournalEntry.GroupBy(x => x.JrnlE_AccountId).Any(g => g.Count() > 1);
            //    if (anyDuplicate)
            //    {
            //        custom_Response.HttpStatusCode = HttpStatusCode.BadRequest;
            //        custom_Response.Message = Message.OneOrMoreJournalEntry;
            //        return custom_Response;
            //    }
            //}

            var isNew = Data.JrnlH_pkeyId == 0;

            if(isNew)
            {
                Data.Type = 1;//1 = Add
                Data.JrnlH_IsDelete = false;
               return await Task.Run(() => acc_Journal_MasterData.AddJournalData(Data));
            }
            else
            {
                Data.Type = 2;//2 = Edit
                    if (!Data.JrnlH_Posted)
                        return await Task.Run(() => acc_Journal_MasterData.AddJournalData(Data));
                    else
                    {

                        custom_Response.HttpStatusCode = HttpStatusCode.BadRequest;
                        custom_Response.Message = Message.JournalEntryalreadyPost;
                        return custom_Response;
                    }

                }
            }
            catch (Exception ex)
            {
                custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                custom_Response.Message = Message.ErrorWhileSaving;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return custom_Response;
            }
        }

        [HttpPost]
        [Route("PostJournal")]
        [Authorize]
        public async Task<Custom_Response> PostJournal()
        {
            Custom_Response custom_Response = new Custom_Response();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<int>(s);
                return await Task.Run(() => acc_Journal_MasterData.PostJournal(Data,LoggedInUSerId));
            }
            catch (Exception ex)
            {
                custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                custom_Response.Message = Message.ErrorWhileSaving;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return custom_Response;
            }
        }
        [HttpPost]
        [Route("DeleteJournal")]
        [Authorize]
        public async Task<Custom_Response> DeleteJournal()
        {
            Custom_Response custom_Response = new Custom_Response();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<Acc_Journal_Entry_Headers_DTO>(s);
                Data.UserID = LoggedInUSerId;
                Data.Type = 3;//3 = Delete
                return await Task.Run(() => acc_Journal_MasterData.AddJournalData(Data));
            }
            catch (Exception ex)
            {
                custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                custom_Response.Message = Message.ErrorWhileSaving;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return custom_Response;
            }
        }
    }
}