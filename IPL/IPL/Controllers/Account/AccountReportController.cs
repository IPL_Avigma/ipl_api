﻿using Avigma.Repository.Lib;
using IPL.HelperMessage;
using IPL.Models.Account;
using IPL.Repository.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace IPL.Controllers.Account
{
    [RoutePrefix("api/AccountReport")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]

    public class AccountReportController : BaseController
    {
        Log log = new Log();


        #region TrialBalance
        [HttpPost]
        [Route("GetTrialBalance")]
        [Authorize]
        public async Task<dynamic> GetTrialBalance(Acc_Report_Filter filter)
        {
            Custom_Response custom_Response = new Custom_Response();
            try
            {
                Acc_TrialBalance_Data acc_TrialBalance_Data = new Acc_TrialBalance_Data();
                filter.UserId = LoggedInUSerId;
                custom_Response.Data = await Task.Run(() => acc_TrialBalance_Data.TrialBalance(filter));
                custom_Response.HttpStatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                custom_Response.Message = Message.GettingErrorWhileBindList;
                custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return custom_Response;
            }
            return custom_Response;
        }
        [HttpGet]
        [Route("GetTrialBalancePDF")]
        [Authorize]
        public async Task<HttpResponseMessage> GetTrialBalancePDF(DateTime? StartDate, DateTime? EndDate, int ReportType)
        {
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            try
            {
                Acc_Report_Filter filter = new Acc_Report_Filter()
                {
                    StartDate = StartDate,
                    EndDate = EndDate,
                    ReportsType = (Reports)ReportType,
                    UserId=LoggedInUSerId,
                };
                Custom_Response custom_Response = new Custom_Response();
                Acc_TrialBalance_Data acc_TrialBalance_Data = new Acc_TrialBalance_Data();
                custom_Response = await Task.Run(() => acc_TrialBalance_Data.GeneratePDFStringTrialBalance(filter));
                custom_Response.HttpStatusCode = HttpStatusCode.OK;
                byte[] respbytes = custom_Response.Data;
                response.Content = new ByteArrayContent(respbytes);
                response.Content.Headers.ContentLength = respbytes.LongLength;
                //response.Content = new StreamContent(new MemoryStream(orderBL.DownloadPDF(OrderRequestId, PdfType)));
                response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = Reports.TrailBalance.ToString() + ".pdf";
                response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/pdf");
                //return File(new MemoryStream(orderBL.DownloadPDF(OrderRequestId, PdfType), "application/pdf");
                return response;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

            }
            return response;
        }

        #endregion

        #region BalanceSheet
        [HttpPost]
        [Route("GetBalanceSheetReports")]
        [Authorize]
        public async Task<dynamic> GetBalanceSheetReports(Acc_Report_Filter filter)
        {
            Custom_Response custom_Response = new Custom_Response();
            try
            {
                Acc_BalanceSheet_Data acc_BalanceSheet_Data = new Acc_BalanceSheet_Data();
                filter.UserId = LoggedInUSerId;
                filter.Type = 3;
                custom_Response.Data = await Task.Run(() => acc_BalanceSheet_Data.BalanceSheetReports(filter));
                custom_Response.HttpStatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                custom_Response.Message = Message.GettingErrorWhileBindList;
                custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return custom_Response;
            }
            return custom_Response;
        }
        [HttpGet]
        [Route("GetBalanceSheetPDF")]
        [Authorize]
        public async Task<HttpResponseMessage> GetBalanceSheetPDF(DateTime? StartDate, DateTime? EndDate, int ReportType)
        {
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            try
            {
                Acc_Report_Filter filter = new Acc_Report_Filter()
                {
                    StartDate = StartDate,
                    EndDate = EndDate,
                    ReportsType = (Reports)ReportType,
                    Type = 3,
                    UserId = LoggedInUSerId,
                };
                Custom_Response custom_Response = new Custom_Response();
                Acc_BalanceSheet_Data acc_BalanceSheet_Data = new Acc_BalanceSheet_Data();
                custom_Response = await Task.Run(() => acc_BalanceSheet_Data.GeneratePDFStringBalanceSheet(filter));
                custom_Response.HttpStatusCode = HttpStatusCode.OK;
                byte[] respbytes = custom_Response.Data;
                response.Content = new ByteArrayContent(respbytes);
                response.Content.Headers.ContentLength = respbytes.LongLength;
                //response.Content = new StreamContent(new MemoryStream(orderBL.DownloadPDF(OrderRequestId, PdfType)));
                response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = Reports.BalanceSheet.ToString() + ".pdf";
                response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/pdf");
                //return File(new MemoryStream(orderBL.DownloadPDF(OrderRequestId, PdfType), "application/pdf");
                return response;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

            }
            return response;
        }

        #endregion

        #region IncomeStatement
        [HttpPost]
        [Route("GetIncomeStatement")]
        [Authorize]

        public async Task<dynamic> GetIncomeStatement(Acc_Report_Filter filter)
        {
            Custom_Response custom_Response = new Custom_Response();

            try
            {
                Acc_IncomeStatement_Data acc_IncomeStatement_Data = new Acc_IncomeStatement_Data();
                filter.UserId = LoggedInUSerId;
                custom_Response.Data = await Task.Run(() => acc_IncomeStatement_Data.IncomeStatement(filter));
                custom_Response.HttpStatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                custom_Response.Message = Message.GettingErrorWhileBindList;
                custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return custom_Response;
            }
            return custom_Response;
        }

        [HttpGet]
        [Route("GetIncomeStatementPDF")]
        [Authorize]
        public async Task<HttpResponseMessage> GetIncomeStatementPDF(DateTime? StartDate, DateTime? EndDate, int ReportType)
        {
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            try
            {

                Acc_Report_Filter filter = new Acc_Report_Filter()
                {
                    StartDate = StartDate,
                    EndDate = EndDate,
                    ReportsType = (Reports)ReportType,
                    UserId=LoggedInUSerId,
                };
                Custom_Response custom_Response = new Custom_Response();
                Acc_IncomeStatement_Data acc_IncomeStatement_Data = new Acc_IncomeStatement_Data();
                custom_Response = await Task.Run(() => acc_IncomeStatement_Data.GeneratePDFStringIncomeStatement(filter));
                custom_Response.HttpStatusCode = HttpStatusCode.OK;
                byte[] respbytes = custom_Response.Data;
                response.Content = new ByteArrayContent(respbytes);
                response.Content.Headers.ContentLength = respbytes.LongLength;
                //response.Content = new StreamContent(new MemoryStream(orderBL.DownloadPDF(OrderRequestId, PdfType)));
                response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = Reports.IncomeStatement.ToString() + ".pdf";
                response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/pdf");
                //return File(new MemoryStream(orderBL.DownloadPDF(OrderRequestId, PdfType), "application/pdf");
                return response;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

            }
            return response;
        }
        #endregion

        #region ProfitAndLoss

        [HttpPost]
        [Route("GetProfitAndLossReports")]
        [Authorize]
        public async Task<dynamic> GetProfitAndLossReports(Acc_Report_Filter filter)
        {
            Custom_Response custom_Response = new Custom_Response();
            try
            {
                Acc_ProfitAndLoss_Data acc_ProfitAndLoss_Data = new Acc_ProfitAndLoss_Data();
                filter.UserId = LoggedInUSerId;
                custom_Response = await Task.Run(() => acc_ProfitAndLoss_Data.ProfitAndLossReports(filter));
                custom_Response.HttpStatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                custom_Response.Message = Message.GettingErrorWhileBindList;
                custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return custom_Response;
            }
            return custom_Response;
        }
        [HttpGet]
        [Route("GetProfitLossPDF")]
        [Authorize]
        public async Task<HttpResponseMessage> GetProfitLossPDF(DateTime? StartDate, DateTime? EndDate, int ReportType)
        {
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            try
            {
                Acc_Report_Filter filter = new Acc_Report_Filter()
                {
                    StartDate = StartDate,
                    EndDate = EndDate,
                    ReportsType = (Reports)ReportType,
                    UserId=LoggedInUSerId,
                };
                Custom_Response custom_Response = new Custom_Response();
                Acc_ProfitAndLoss_Data acc_ProfitAndLoss_Data = new Acc_ProfitAndLoss_Data();
                custom_Response = await Task.Run(() => acc_ProfitAndLoss_Data.GeneratePDFStringProfitLoss(filter));
                custom_Response.HttpStatusCode = HttpStatusCode.OK;
                byte[] respbytes = custom_Response.Data;
                response.Content = new ByteArrayContent(respbytes);
                response.Content.Headers.ContentLength = respbytes.LongLength;
                //response.Content = new StreamContent(new MemoryStream(orderBL.DownloadPDF(OrderRequestId, PdfType)));
                response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = Reports.ProfitLoss.ToString() + ".pdf";
                response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/pdf");
                //return File(new MemoryStream(orderBL.DownloadPDF(OrderRequestId, PdfType), "application/pdf");
                return response;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

            }
            return response;
        }

        #endregion
        #region ProfitAndLosscomparison

        [HttpPost]
        [Route("GetProfitAndLossComparisonReports")]
        [Authorize]
        public async Task<dynamic> GetProfitAndLossComparisonReports(Acc_Report_Filter filter)
        {
            Custom_Response custom_Response = new Custom_Response();
            try
            {
                Acc_ProfitAndLoss_Data acc_ProfitAndLoss_Data = new Acc_ProfitAndLoss_Data();
                filter.UserId = LoggedInUSerId;
                filter.Type = 3;
                custom_Response = await Task.Run(() => acc_ProfitAndLoss_Data.ProfitAndLossComparisonReports(filter));
                custom_Response.HttpStatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                custom_Response.Message = Message.GettingErrorWhileBindList;
                custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return custom_Response;
            }
            return custom_Response;
        }
        [HttpGet]
        [Route("GetProfitLossComparisonPDF")]
        [Authorize]
        public async Task<HttpResponseMessage> GetProfitLossComparisonPDF(DateTime? StartDate, DateTime? EndDate, int ReportType)
        {
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            try
            {
               
                Acc_Report_Filter filter = new Acc_Report_Filter()
                {
                    StartDate = StartDate,
                    EndDate = EndDate,
                    ReportsType = (Reports)ReportType,
                    Type=1,
                    UserId=LoggedInUSerId,
                };
                Custom_Response custom_Response = new Custom_Response();
                Acc_ProfitAndLoss_Data acc_ProfitAndLoss_Data = new Acc_ProfitAndLoss_Data();
                custom_Response = await Task.Run(() => acc_ProfitAndLoss_Data.GeneratePDFStringProfitLossComparison(filter));
                custom_Response.HttpStatusCode = HttpStatusCode.OK;
                byte[] respbytes = custom_Response.Data;
                response.Content = new ByteArrayContent(respbytes);
                response.Content.Headers.ContentLength = respbytes.LongLength;
                //response.Content = new StreamContent(new MemoryStream(orderBL.DownloadPDF(OrderRequestId, PdfType)));
                response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = Reports.ProfitLoss.ToString() + ".pdf";
                response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/pdf");
                //return File(new MemoryStream(orderBL.DownloadPDF(OrderRequestId, PdfType), "application/pdf");
                return response;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

            }
            return response;
        }

        #endregion
        #region Account Payable and Receivable

        [HttpPost]
        [Route("GetAccountPayableReports")]
        [Authorize]
        public async Task<dynamic> GetAccountPayableReports(Acc_Report_Filter filter)
        {
            Custom_Response custom_Response = new Custom_Response();
            try
            {
                Acc_Account_Payable_Receivable_Data acc_Account_Payable = new Acc_Account_Payable_Receivable_Data();
                filter.UserId = LoggedInUSerId;
                custom_Response = await Task.Run(() => acc_Account_Payable.AccountPayableAgingSummaryReports( filter));
                custom_Response.HttpStatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                custom_Response.Message = Message.GettingErrorWhileBindList;
                custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return custom_Response;
            }
            return custom_Response;
        }
        [HttpGet]
        [Route("GetAccountPayableReportsPDF")]
        [Authorize]
        public async Task<HttpResponseMessage> GetAccountPayableReportsPDF(DateTime? StartDate, DateTime? EndDate, int ReportType)
        {
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            try
            {
              
                Acc_Report_Filter filter = new Acc_Report_Filter()
                {
                    StartDate = StartDate,
                    EndDate = EndDate,
                    ReportsType = (Reports)ReportType,
                    UserId=LoggedInUSerId,
                };
                Custom_Response custom_Response = new Custom_Response();
                Acc_Account_Payable_Receivable_Data acc_Account_Payable = new Acc_Account_Payable_Receivable_Data();
                custom_Response = await Task.Run(() => acc_Account_Payable.GeneratePDFStringAccountPayableAgingSummary(filter));
                custom_Response.HttpStatusCode = HttpStatusCode.OK;
                byte[] respbytes = custom_Response.Data;
                response.Content = new ByteArrayContent(respbytes);
                response.Content.Headers.ContentLength = respbytes.LongLength;
                //response.Content = new StreamContent(new MemoryStream(orderBL.DownloadPDF(OrderRequestId, PdfType)));
                response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = Reports.AccountPayable.ToString() + ".pdf";
                response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/pdf");
                //return File(new MemoryStream(orderBL.DownloadPDF(OrderRequestId, PdfType), "application/pdf");
                return response;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

            }
            return response;
        }

        [HttpPost]
        [Route("GetAccountReceivableReports")]
        [Authorize]
        public async Task<dynamic> GetAccountReceivableReports(Acc_Report_Filter filter)
        {
            Custom_Response custom_Response = new Custom_Response();
            try
            {
                Acc_Account_Payable_Receivable_Data acc_Account_Payable = new Acc_Account_Payable_Receivable_Data();
                filter.UserId = LoggedInUSerId;
                custom_Response = await Task.Run(() => acc_Account_Payable.AccountReceivableAgingSummaryReports(filter));
                custom_Response.HttpStatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                custom_Response.Message = Message.GettingErrorWhileBindList;
                custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return custom_Response;
            }
            return custom_Response;
        }
        [HttpGet]
        [Route("GetAccountReceivableReportsPDF")]
        [Authorize]
        public async Task<HttpResponseMessage> GetAccountReceivableReportsPDF(DateTime? StartDate, DateTime? EndDate, int ReportType)
        {
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            try
            {
              
                Acc_Report_Filter filter = new Acc_Report_Filter()
                {
                    StartDate = StartDate,
                    EndDate = EndDate,
                    ReportsType = (Reports)ReportType,
                    UserId=LoggedInUSerId,
                };
                Custom_Response custom_Response = new Custom_Response();
                Acc_Account_Payable_Receivable_Data acc_Account_Payable = new Acc_Account_Payable_Receivable_Data();
                custom_Response = await Task.Run(() => acc_Account_Payable.GeneratePDFStringAccountReceivableAgingSummary(filter));
                custom_Response.HttpStatusCode = HttpStatusCode.OK;
                byte[] respbytes = custom_Response.Data;
                response.Content = new ByteArrayContent(respbytes);
                response.Content.Headers.ContentLength = respbytes.LongLength;
                //response.Content = new StreamContent(new MemoryStream(orderBL.DownloadPDF(OrderRequestId, PdfType)));
                response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = Reports.AccountReceivable.ToString() + ".pdf";
                response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/pdf");
                //return File(new MemoryStream(orderBL.DownloadPDF(OrderRequestId, PdfType), "application/pdf");
                return response;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

            }
            return response;
        }
        #endregion

        #region Profit and Loss Details
        [HttpPost]
        [Route("GetProfitAndLossDetailsReports")]
        [Authorize]

        public async Task<dynamic> GetProfitAndLossDetailsReports(Acc_Report_Filter filter)
        {
            Custom_Response custom_Response = new Custom_Response();
            try
            {
                Acc_ProfitAndLossDetails_Data acc_ProfitAndLossDetails = new Acc_ProfitAndLossDetails_Data();
                filter.UserId = LoggedInUSerId;
                custom_Response = await Task.Run(() => acc_ProfitAndLossDetails.ProfitAndLossDetailsReports(filter));
                custom_Response.HttpStatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                custom_Response.Message = Message.GettingErrorWhileBindList;
                custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return custom_Response;
            }
            return custom_Response;
        }
        [HttpGet]
        [Route("GetProfitAndLossDetailsPDF")]
        [Authorize]
        public async Task<HttpResponseMessage> GetProfitAndLossDetailsPDF(DateTime? StartDate, DateTime? EndDate, int ReportType)
        {
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            try
            {
                Acc_Report_Filter filter = new Acc_Report_Filter()
                {
                    StartDate = StartDate,
                    EndDate = EndDate,
                    ReportsType = (Reports)ReportType,
                    UserId=LoggedInUSerId,
                };
                Custom_Response custom_Response = new Custom_Response();
                Acc_ProfitAndLossDetails_Data acc_ProfitAndLossDetails = new Acc_ProfitAndLossDetails_Data();
                custom_Response = await Task.Run(() => acc_ProfitAndLossDetails.GeneratePDFStringProfitLossDetails( filter));
                custom_Response.HttpStatusCode = HttpStatusCode.OK;
                byte[] respbytes = custom_Response.Data;
                response.Content = new ByteArrayContent(respbytes);
                response.Content.Headers.ContentLength = respbytes.LongLength;
                //response.Content = new StreamContent(new MemoryStream(orderBL.DownloadPDF(OrderRequestId, PdfType)));
                response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = Reports.ProfitLoss.ToString() + ".pdf";
                response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/pdf");
                //return File(new MemoryStream(orderBL.DownloadPDF(OrderRequestId, PdfType), "application/pdf");
                return response;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

            }
            return response;
        }

        #endregion
        #region Journal
        [HttpPost]
        [Route("GetJournalReports")]
        [Authorize]
        public async Task<dynamic> GetJournalReports(Acc_Report_Filter filter)
        {
            Custom_Response custom_Response = new Custom_Response();
            try
            {
                Acc_Journal_Data acc_Journal = new Acc_Journal_Data();
                filter.UserId = LoggedInUSerId;
                custom_Response = await Task.Run(() => acc_Journal.JournalReports( filter));
                custom_Response.HttpStatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                custom_Response.Message = Message.GettingErrorWhileBindList;
                custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return custom_Response;
            }
            return custom_Response;
        }
        [HttpGet]
        [Route("GetJournalPDF")]
        [Authorize]
        public async Task<HttpResponseMessage> GetJournalPDF(DateTime? StartDate, DateTime? EndDate, int ReportType)
        {
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            try
            {
                Acc_Report_Filter filter = new Acc_Report_Filter()
                {
                    StartDate = StartDate,
                    EndDate = EndDate,
                    ReportsType = (Reports)ReportType,
                    UserId =LoggedInUSerId
                };
                Custom_Response custom_Response = new Custom_Response();
                Acc_Journal_Data acc_Journal = new Acc_Journal_Data();
                custom_Response = await Task.Run(() => acc_Journal.GeneratePDFStringJournal(filter));
                custom_Response.HttpStatusCode = HttpStatusCode.OK;
                byte[] respbytes = custom_Response.Data;
                response.Content = new ByteArrayContent(respbytes);
                response.Content.Headers.ContentLength = respbytes.LongLength;
                //response.Content = new StreamContent(new MemoryStream(orderBL.DownloadPDF(OrderRequestId, PdfType)));
                response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = Reports.ProfitLoss.ToString() + ".pdf";
                response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/pdf");
                //return File(new MemoryStream(orderBL.DownloadPDF(OrderRequestId, PdfType), "application/pdf");
                return response;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

            }
            return response;
        }

        #endregion

        #region Profit and loss By Month
        [HttpPost]
        [Route("GetProfitAndLossByMonthReports")]
        [Authorize]
        public async Task<dynamic> GetProfitAndLossByMonthReports(Acc_Report_Filter filter)
        {
            Custom_Response custom_Response = new Custom_Response();
            try
            {
                Acc_ProfitAndLoss_Data acc_ProfitAndLoss_Data = new Acc_ProfitAndLoss_Data();
                filter.UserId = LoggedInUSerId;
                custom_Response = await Task.Run(() => acc_ProfitAndLoss_Data.ProfitAndLossByMonthReports(filter));
                custom_Response.HttpStatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                custom_Response.Message = Message.GettingErrorWhileBindList;
                custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return custom_Response;
            }
            return custom_Response;
        }
        [HttpGet]
        [Route("GetProfitLossByMonthPDF")]
        [Authorize]
        public async Task<HttpResponseMessage> GetProfitLossByMonthPDF(DateTime? StartDate, DateTime? EndDate, int ReportType)
        {
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            try
            {
               
                Acc_Report_Filter filter = new Acc_Report_Filter()
                {
                    StartDate = StartDate,
                    EndDate = EndDate,
                    ReportsType = (Reports)ReportType,
                    UserId= LoggedInUSerId
                };
                Custom_Response custom_Response = new Custom_Response();
                Acc_ProfitAndLoss_Data acc_ProfitAndLoss_Data = new Acc_ProfitAndLoss_Data();
                custom_Response = await Task.Run(() => acc_ProfitAndLoss_Data.GeneratePDFStringProfitLossByMonth( filter));
                custom_Response.HttpStatusCode = HttpStatusCode.OK;
                byte[] respbytes = custom_Response.Data;
                response.Content = new ByteArrayContent(respbytes);
                response.Content.Headers.ContentLength = respbytes.LongLength;
                //response.Content = new StreamContent(new MemoryStream(orderBL.DownloadPDF(OrderRequestId, PdfType)));
                response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = Reports.ProfitLoss.ToString() + ".pdf";
                response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/pdf");
                //return File(new MemoryStream(orderBL.DownloadPDF(OrderRequestId, PdfType), "application/pdf");
                return response;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

            }
            return response;
        }

        #endregion
        #region Profit and loss By Customer
        [HttpPost]
        [Route("GetProfitAndLossByCustomerReports")]
        [Authorize]
        public async Task<dynamic> GetProfitAndLossByCustomerReports(Acc_Report_Filter filter)
        {
            Custom_Response custom_Response = new Custom_Response();
            try
            {
                Acc_ProfitAndLoss_Data acc_ProfitAndLoss_Data = new Acc_ProfitAndLoss_Data();
                filter.UserId = LoggedInUSerId;
                custom_Response = await Task.Run(() => acc_ProfitAndLoss_Data.ProfitAndLossByCustomerReports(filter));
                custom_Response.HttpStatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                custom_Response.Message = Message.GettingErrorWhileBindList;
                custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return custom_Response;
            }
            return custom_Response;
        }
        [HttpGet]
        [Route("GetProfitLossByCustomerPDF")]
        [Authorize]
        public async Task<HttpResponseMessage> GetProfitLossByCustomerPDF(DateTime? StartDate, DateTime? EndDate, int ReportType)
        {
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            try
            {
           
                Acc_Report_Filter filter = new Acc_Report_Filter()
                {
                    StartDate = StartDate,
                    EndDate = EndDate,
                    ReportsType = (Reports)ReportType,
                    UserId=LoggedInUSerId,
                };
                Custom_Response custom_Response = new Custom_Response();
                Acc_ProfitAndLoss_Data acc_ProfitAndLoss_Data = new Acc_ProfitAndLoss_Data();
                custom_Response = await Task.Run(() => acc_ProfitAndLoss_Data.GeneratePDFStringProfitLossByCustomer(filter));
                custom_Response.HttpStatusCode = HttpStatusCode.OK;
                byte[] respbytes = custom_Response.Data;
                response.Content = new ByteArrayContent(respbytes);
                response.Content.Headers.ContentLength = respbytes.LongLength;
                //response.Content = new StreamContent(new MemoryStream(orderBL.DownloadPDF(OrderRequestId, PdfType)));
                response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = Reports.ProfitLoss.ToString() + ".pdf";
                response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/pdf");
                //return File(new MemoryStream(orderBL.DownloadPDF(OrderRequestId, PdfType), "application/pdf");
                return response;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

            }
            return response;
        }

        #endregion
        #region Profit and loss By Vendor
        [HttpPost]
        [Route("GetProfitAndLossByVendorReports")]
        [Authorize]
        public async Task<dynamic> GetProfitAndLossByVendorReports(Acc_Report_Filter filter)
        {
            Custom_Response custom_Response = new Custom_Response();
            try
            {
                Acc_ProfitAndLoss_Data acc_ProfitAndLoss_Data = new Acc_ProfitAndLoss_Data();
                filter.UserId = LoggedInUSerId;
                custom_Response = await Task.Run(() => acc_ProfitAndLoss_Data.ProfitAndLossByVendorReports(filter));
                custom_Response.HttpStatusCode = HttpStatusCode.OK;
            }
            catch (Exception ex)
            {
                custom_Response.Message = Message.GettingErrorWhileBindList;
                custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return custom_Response;
            }
            return custom_Response;
        }
        [HttpGet]
        [Route("GetProfitLossByVendorPDF")]
        [Authorize]
        public async Task<HttpResponseMessage> GetProfitLossByVendorPDF(DateTime? StartDate, DateTime? EndDate, int ReportType)
        {
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            try
            {

                Acc_Report_Filter filter = new Acc_Report_Filter()
                {
                    StartDate = StartDate,
                    EndDate = EndDate,
                    ReportsType = (Reports)ReportType,
                    UserId = LoggedInUSerId,
                };
                Custom_Response custom_Response = new Custom_Response();
                Acc_ProfitAndLoss_Data acc_ProfitAndLoss_Data = new Acc_ProfitAndLoss_Data();
                custom_Response = await Task.Run(() => acc_ProfitAndLoss_Data.GeneratePDFStringProfitLossByVendor(filter));
                custom_Response.HttpStatusCode = HttpStatusCode.OK;
                byte[] respbytes = custom_Response.Data;
                response.Content = new ByteArrayContent(respbytes);
                response.Content.Headers.ContentLength = respbytes.LongLength;
                //response.Content = new StreamContent(new MemoryStream(orderBL.DownloadPDF(OrderRequestId, PdfType)));
                response.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = Reports.ProfitLoss.ToString() + ".pdf";
                response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/pdf");
                //return File(new MemoryStream(orderBL.DownloadPDF(OrderRequestId, PdfType), "application/pdf");
                return response;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

            }
            return response;
        }

        #endregion
    }
}