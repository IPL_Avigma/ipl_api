﻿using Avigma.Repository.Lib;
using IPL.HelperMessage;
using IPL.Models.Account;
using IPL.Repository.Account;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace IPL.Controllers.Account
{
    [RoutePrefix("api/Account")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]

    public class AccountController : BaseController
    {
        Acc_Account_MasterData acc_Account_MasterData = new Acc_Account_MasterData();
        Log log = new Log();
        [HttpGet]
        [Route("GetAccountList")]
        [Authorize]
        public async Task<Custom_Response> GetAccountList()
        {
            Custom_Response responce = new Custom_Response();
            try
            {
                responce = await Task.Run(() => acc_Account_MasterData.GetAllAccounts(null,LoggedInUSerId));

                if (responce.HttpStatusCode == HttpStatusCode.OK)
                {
                    List<Acc_AccountMaster_DTO> OldAccountList = new List<Acc_AccountMaster_DTO>();
                    List<Acc_AccountMaster_DTO> NewAccountList = new List<Acc_AccountMaster_DTO>();
                    OldAccountList = responce.Data;

                    var Listdata = OldAccountList;
                    foreach (var item in OldAccountList)
                    {
                        if (!NewAccountList.Where(x => x.Acc_pkeyId == item.Acc_pkeyId).Any())
                        {
                            NewAccountList.Add(item);
                        }
                        else
                        {
                            Acc_AccountMaster_DTO dto = new Acc_AccountMaster_DTO()
                            {
                                Acc_Account_Code = item.Acc_Account_Code,
                                Acc_Account_Description = item.Acc_Account_Description,
                                Acc_Account_Details = item.Acc_Account_Details,
                                Acc_Account_Name = item.Acc_Account_Name,
                                DebitBalance = item.DebitBalance,
                                Acc_Account_Type = item.Acc_Account_Type,
                                Acc_Account_Type_Detail_Name = item.Acc_Account_Type_Detail_Name,
                                Acc_Account_Type_Name = item.Acc_Account_Type_Name,
                                Acc_CreatedBy = item.Acc_CreatedBy,
                                Acc_CreatedOn = item.Acc_CreatedOn,
                                Acc_DrOrCr_Side = item.Acc_DrOrCr_Side,
                                Acc_IsActive = item.Acc_IsActive,
                                Acc_IsParent = item.Acc_IsParent,
                                Acc_Parent_Account_Id = item.Acc_Parent_Account_Id,
                                Acc_pkeyId = item.Acc_pkeyId,
                                CreditBalance = item.CreditBalance,
                                Balance = Listdata.Where(x => x.Acc_pkeyId == item.Acc_pkeyId).Sum(x => x.Balance),
                            };

                            NewAccountList.Remove(NewAccountList.Where(x => x.Acc_pkeyId == item.Acc_pkeyId).FirstOrDefault());
                            NewAccountList.Add(dto);
                        }
                    }
                    responce.Data = NewAccountList;
                    return responce;
                }
            }
            catch (Exception ex)
            {
                responce.Message = Message.GettingErrorWhileBindList;
                responce.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return responce;
            }
            return responce;
        }
        [HttpGet]
        [Route("GetAccountChildByAccountId")]
        [Authorize]
        public async Task<Custom_Response> GetAccountChildByAccountId(long AccountId)
        {
            Custom_Response responce = new Custom_Response();
            try
            {
                responce = await Task.Run(() => acc_Account_MasterData.GetAllAccounts(AccountId,LoggedInUSerId));
                if (responce.HttpStatusCode != HttpStatusCode.OK)
                {
                    return responce;
                }
            }
            catch (Exception ex)
            {
                responce.Message = Message.GettingErrorWhileBindList;
                responce.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return responce;
            }
            return responce;
        }

        [HttpGet]
        [Route("GetAccountDetailsByAccountId")]
        [Authorize]
        public async Task<dynamic> GetAccountDetailsByAccountId(int AccountId)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                Acc_AccountMaster_DTO acc_AccountMaster_DTO = new Acc_AccountMaster_DTO();
                acc_AccountMaster_DTO.UserID = LoggedInUSerId;
                acc_AccountMaster_DTO.Type = 1;
                acc_AccountMaster_DTO.Acc_pkeyId = AccountId;
                var GetAccount = await Task.Run(() => acc_Account_MasterData.GetAccountsByAccountId(acc_AccountMaster_DTO));
                return GetAccount;
            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return objdynamicobj;
            }
        }

        [HttpPost]
        [Route("CreateUpdateAccount")]
        [Authorize]
        public async Task<Custom_Response> CreateUpdateAccount()
        {
            Custom_Response responce = new Custom_Response();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Acc_AccountMaster_DTO>(s);
                Data.UserID = LoggedInUSerId;
                responce = await Task.Run(() => acc_Account_MasterData.AddAccountData(Data));
                if (responce.HttpStatusCode != HttpStatusCode.OK)
                {
                    return responce;
                }
            }
            catch (Exception ex)
            {
                responce.Message = Message.ErrorWhileSaving;
                responce.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return responce;
            }
            return responce;
        }
        [HttpPost]
        [Route("GetAccountActivityByAccountId")]
        [Authorize]
        public async Task<dynamic> GetAccountActivityByAccountId(COA_Filter Filter)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                Acc_Account_Master_Activity_DTO acc_AccountMaster_DTO = new Acc_Account_Master_Activity_DTO();
                acc_AccountMaster_DTO.UserID = LoggedInUSerId;
                acc_AccountMaster_DTO.Type = 1;
                acc_AccountMaster_DTO.Acc_pkeyId = Filter.Acc_pkeyId;
                var GetAccount = await Task.Run(() => acc_Account_MasterData.GetAccountsActivityByAccountId(acc_AccountMaster_DTO, Filter));
                return GetAccount;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }
    }
}