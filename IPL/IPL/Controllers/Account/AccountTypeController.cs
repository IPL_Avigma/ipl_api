﻿using Avigma.Repository.Lib;
using IPL.Models.Account;
using IPL.Repository.Account;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace IPL.Controllers.Account
{
    [RoutePrefix("api/AccountType")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]

    public class AccountTypeController : BaseController
    {
        Acc_Account_Type_Data acc_Account_Type_Data = new Acc_Account_Type_Data();
        Log log = new Log();
        [HttpGet]
        [Route("GetDropDownAccountType")]
        [Authorize]
        public async Task<dynamic> GetDropDownAccountType()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                DropDownAccountTypeDTO dropDownAccountTypeDTO = new DropDownAccountTypeDTO();
                dropDownAccountTypeDTO.UserID = LoggedInUSerId;
                dropDownAccountTypeDTO.Type = 1;

                var Getdropdown = await Task.Run(() => acc_Account_Type_Data.GetDropDownAccountTypeData(dropDownAccountTypeDTO));
                return Getdropdown;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [HttpGet]
        [Route("GetDropDownAccountDetailsByAccountTypeId")]
        [Authorize]
        public async Task<dynamic> GetDropDownAccountDetailsByAccountTypeId(int AccountTypeId)
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                DropDownAccountTypeDTO dropDownAccountTypeDTO = new DropDownAccountTypeDTO();
                dropDownAccountTypeDTO.UserID = LoggedInUSerId;
                dropDownAccountTypeDTO.Type = 1;
                dropDownAccountTypeDTO.Acc_TypeID = AccountTypeId;

                var Getdropdown = await Task.Run(() => acc_Account_Type_Data.GetDropDownAccountDetailsData(dropDownAccountTypeDTO));
                return Getdropdown;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
    }
}