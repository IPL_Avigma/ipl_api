﻿using Avigma.Repository.Lib;
using IPL.HelperMessage;
using IPL.Models.Account;
using IPL.Repository.Account;
using IPLApp.Models;
using IPLApp.Repository.data;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;

namespace IPL.Controllers.Account
{
    [RoutePrefix("api/User")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UserController : BaseController
    {
        Acc_Vendor_Data acc_Vendor_Data = new Acc_Vendor_Data();
        Log log = new Log();
        [Authorize]
        [HttpPost]
        [Route("CreateUpdateAccVendor")]
        public async Task<Custom_Response> CreateUpdateAccVendor()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            Custom_Response custom_Response = new Custom_Response();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<Acc_Vendor_DTO>(s);
                Data.UserID = LoggedInUSerId;
                custom_Response = await Task.Run(() => acc_Vendor_Data.AddVendorData(Data));
                return custom_Response;

            }
            catch (Exception ex)
            {
                custom_Response.Message = Message.ErrorWhileSaving;
                custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return custom_Response;
            }

        }
        [Authorize]
        [HttpPost]
        [Route("GetVendorFilterList")]
        public async Task<Custom_Response> GetVendorFilterList()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            Custom_Response custom_Response = new Custom_Response();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<Acc_Vendor_DTO>(s);
                Data.UserID = LoggedInUSerId;
                switch (Data.Type)
                {
                    case 1:
                        {

                            custom_Response.Data = await Task.Run(() => acc_Vendor_Data.GetVendorMasterDetails(Data));
                            break;
                        }
                    case 2:
                        {
                            custom_Response.Data = await Task.Run(() => acc_Vendor_Data.GetVendorMasterDetails(Data));
                            break;
                        }
                    case 3:
                        {
                            custom_Response.Data = await Task.Run(() => acc_Vendor_Data.GetVendorMasterDetails(Data));
                            break;
                        }
                       
                }
                custom_Response.HttpStatusCode = HttpStatusCode.OK;
                return custom_Response;
            }
            catch (Exception ex)
            {
                custom_Response.Message = Message.GettingErrorWhileBindList;
                custom_Response.HttpStatusCode = HttpStatusCode.InternalServerError;
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                return custom_Response;
            }


        }
    }
}
