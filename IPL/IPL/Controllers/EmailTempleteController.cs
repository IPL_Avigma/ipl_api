﻿using IPL.Models;
using IPL.Repository.data;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using Avigma.Repository.Lib;

namespace IPL.Controllers
{
    [RoutePrefix("api/Email")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class EmailTempleteController : BaseController
    {
        EmailTemplateData emailTemplateDta = new EmailTemplateData();
        WorkOrder_Column_Data workOrder_Column_Data = new WorkOrder_Column_Data();
        Log log = new Log();


        [HttpPost]
        [Route("GetEmailDRDDetail")]
        public async Task<List<dynamic>> GetEmailDRDDetail()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Getemailheading = await Task.Run(() => emailTemplateDta.GetEmailheadingDetail());
                return Getemailheading;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("GetEmailBodyDetail")]
        public async Task<List<dynamic>> GetEmailBodyDetail()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<EmailTemplateDTO>(s);
                Data.UserID = LoggedInUSerId;
                var Getemailbody = await Task.Run(() => emailTemplateDta.GetEmailTempMasterDetails(Data));
                return Getemailbody;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("GetEmailMeataDetail")]
        public async Task<List<dynamic>> GetEmailMeataDetail()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<WorkOrder_Column_DTO>(s);
                Data.UserID = LoggedInUSerId;
                var Getemailmeta = await Task.Run(() => workOrder_Column_Data.GetEmailMetaData(Data));
                return Getemailmeta;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //update template
        [Authorize]
        [HttpPost]
        [Route("UpdateEmailTemplateDetail")]
        public async Task<List<dynamic>> UpdateEmailTemplateDetail()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<EmailTemplateDTO>(s);
                Data.UserID = LoggedInUSerId;
                var Getemailmeta = await Task.Run(() => emailTemplateDta.AddEmailTempData(Data));
                return Getemailmeta;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

    }
}
