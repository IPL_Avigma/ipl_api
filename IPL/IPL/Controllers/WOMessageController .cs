﻿using Avigma.Repository.Lib;
using IPL.Models;
using IPLApp.Models;
using IPL.Repository.data;
using IPLApp.Repository.data;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Results;
using AutoMapper;
using RestSharp;
using Newtonsoft.Json.Linq;
using System.Web.Script.Serialization;

namespace IPL.Controllers
{
    [RoutePrefix("api/WOMessage")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class WOMessageController : BaseController
    {
        Log log = new Log();
        Client_Result_PhotoData client_Result_PhotoData = new Client_Result_PhotoData();
        IPL_Chat_PhotoData iPL_Chat_PhotoData = new IPL_Chat_PhotoData();
        GoogleCloudData googleCloudData = new GoogleCloudData();
        ClientResultsPhotoFileNameData clientResultsPhotoFileNameData = new ClientResultsPhotoFileNameData();
        workOrderxDTO workOrder = new workOrderxDTO();
        
        WorkOrderMasterData workOrderMasterData = new WorkOrderMasterData();
        WorkOrderMessageMasterData workOrderMessageMasterData = new WorkOrderMessageMasterData();
        Message_WorkOrder_User_Data message_WorkOrder_User_Data = new Message_WorkOrder_User_Data();
        
        
        //add ipl chat photo
        //[HttpPost]
        //[Route("PostUpdateIPLChatphoto")]
        //public async Task<List<dynamic>> PostUpdateIPLChatphoto()
        //{
        //  List<dynamic> objdynamicobj = new List<dynamic>();
        //  try
        //  {
        //    System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
        //    System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
        //    System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
        //    string s = reader.ReadToEnd();

        //    var Data = JsonConvert.DeserializeObject<RootIPLchatbefore>(s);

        //    RootIPLchatbefore rootIPLchatbefore = new RootIPLchatbefore();
        //    ImageArray imageArray = new ImageArray();
        //    rootIPLchatbefore.ImageArray = Data.ImageArray;
        //    rootIPLchatbefore.Task_Bid_TaskID = Data.Task_Bid_TaskID;
        //    rootIPLchatbefore.IPL_Chat_Photo_StatusType = Data.IPL_Chat_Photo_StatusType;
        //    IPL_Chat_PhotoDTO iPL_Chat_PhotoDTO = new IPL_Chat_PhotoDTO();
        //    for (int i = 0; i < Data.ImageArray.Count; i++)
        //    {
        //      iPL_Chat_PhotoDTO.IPL_Chat_Photo_ID = Data.ImageArray[i].IPL_Chat_Photo_ID;
        //      iPL_Chat_PhotoDTO.IPL_Chat_Photo_StatusType = Data.IPL_Chat_Photo_StatusType;
        //      iPL_Chat_PhotoDTO.IPL_Chat_Photo_TaskId = Data.Task_Bid_TaskID;
        //      iPL_Chat_PhotoDTO.IPL_Chat_Photo_Task_Bid_pkeyID = Data.IPL_Chat_Photo_Task_Bid_pkeyID;
        //      iPL_Chat_PhotoDTO.Type = 5;
        //      objdynamicobj = await Task.Run(() => iPL_Chat_PhotoData.AddIPLChatPhotoData(iPL_Chat_PhotoDTO));
        //    }
        //    return objdynamicobj;
        //  }
        //  catch (Exception ex)
        //  {
        //    objdynamicobj.Add(ex.Message);
        //    return objdynamicobj;
        //  }

        //}

        //file details
        [Authorize]
        [AllowAnonymous]
        [HttpPost]
        [Route("GetIPLChatFile")]
        public async Task<List<dynamic>> GetIPLChatFile()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<WorkOrder_Message_DocumentDTO>(s);
                var GetFile = await Task.Run(() => iPL_Chat_PhotoData.GetIPLChatFileDetails(Data));
                objdynamicobj.Add(GetFile);
                return objdynamicobj;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //image details
        [Authorize]
        [AllowAnonymous]
        [HttpPost]
        [Route("GetIPLChatImage")]
        public async Task<List<dynamic>> GetIPLChatImage()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                //log.logDebugMessage("-----------GetPhotoFileName called-----------");
                //log.logDebugMessage(s);
                var Data = JsonConvert.DeserializeObject<IPL_Chat_PhotoDTO>(s);
                var GetImage = await Task.Run(() => iPL_Chat_PhotoData.GetIPLChatImageDetails(Data));
                objdynamicobj.Add(GetImage);
                return objdynamicobj;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        // This Api Responsiable For Showing the all WorkOrder Details In Grid Formate .
        //get work order data
        [Authorize]
        [AllowAnonymous]
        [HttpPost]
        [Route("GetWorkOrderData")]
        public async Task<dynamic> GetWorkOrderData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                //JObject jObject = JObject.Parse(s);
                //string displayName = (string)jObject.SelectToken("Take");
                var Data = JsonConvert.DeserializeObject<IPL_Chat_PhotoDTO>(s);
                workOrder.workOrder_ID = Data.workOrder_ID;
                workOrder.Type = Data.Type;
                workOrder.UserID = LoggedInUSerId;
                var Getworkorder = await Task.Run(() => iPL_Chat_PhotoData.GetWorkOrderData(workOrder, Data.Skip, Data.Take));
                var json = new JavaScriptSerializer().Serialize(Getworkorder);               
                JArray textArray = JArray.Parse(json);
                return textArray;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return null;
            }

        }



        // This Api Responsiable For Showing the all WorkOrder Details In Grid Formate .
        //get work order data
        [Authorize]
        [AllowAnonymous]
        [HttpPost]
        [Route("GetAllWorkOrderData")]
        public async Task<dynamic> GetAllWorkOrderData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                //JObject jObject = JObject.Parse(s);
                //string displayName = (string)jObject.SelectToken("Take");
                var Data = JsonConvert.DeserializeObject<IPL_Chat_PhotoDTO>(s);
                workOrder.workOrder_ID = Data.workOrder_ID;
                workOrder.Type = Data.Type;
                workOrder.UserID = LoggedInUSerId;
                var Getworkorder = await Task.Run(() => iPL_Chat_PhotoData.GetWorkOrderData(workOrder));
                var json = new JavaScriptSerializer().Serialize(Getworkorder);
                JArray textArray = JArray.Parse(json);
                return textArray;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return null;
            }

        }

        // This Api Responsiable For Showing the all WorkOrder Details In Grid Formate .
        //get work order data
        [Authorize]
        [AllowAnonymous]
        [HttpPost]
        [Route("GetWorkOrderDataOnSearch")]
        public async Task<dynamic> GetWorkOrderDataOnSearch()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<IPL_Chat_PhotoDTO>(s);
                workOrder.workOrder_ID = Data.workOrder_ID;
                workOrder.Type = Data.Type;
                workOrder.UserID = LoggedInUSerId;
                var Getworkorder = await Task.Run(() => iPL_Chat_PhotoData.GetWorkOrderDataOnSearch(workOrder, Data.SearchStr));
                var json = new JavaScriptSerializer().Serialize(Getworkorder);
                JArray textArray = JArray.Parse(json);
                return textArray;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return null;
            }

        }

        //get work order data
        [Authorize]
        [AllowAnonymous]
        [HttpPost]
        [Route("GetMessageWorkOrderListData")]
        public async Task<dynamic> GetMessageWorkOrderListData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                //JObject jObject = JObject.Parse(s);
                //string displayName = (string)jObject.SelectToken("Take");
                var Data = JsonConvert.DeserializeObject<IPL_Chat_PhotoDTO>(s);
                workOrder.workOrder_ID = Data.workOrder_ID;
                workOrder.Type = Data.Type;
                workOrder.UserID = LoggedInUSerId;
                var Getworkorder = await Task.Run(() => iPL_Chat_PhotoData.GetMessageWorkOrderListData(workOrder));
                var json = new JavaScriptSerializer().Serialize(Getworkorder);
                JArray textArray = JArray.Parse(json);
                return textArray;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return null;
            }

        }

        //filter get message

        [Authorize]
        [HttpPost]
        [Route("GetMessageWorkOrderListFilterData")]
        public async Task<List<dynamic>> GetMessageWorkOrderListFilterData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<workOrderxDTO>(s);
                workOrder.IPLNO = Data.IPLNO;
                workOrder.Type = Data.Type;
                workOrder.UserID = LoggedInUSerId;

                var GettaskPhoto = await Task.Run(() => iPL_Chat_PhotoData.GetMessageWorkOrderListFilterData(workOrder));
                return GettaskPhoto;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        // add firebaseMessage
        [Authorize]
        [HttpPost]
        [Route("AddFirebaseWoMessageData")]
        public async Task<List<dynamic>> DeleteFirebaseLocationDetails()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<FirebaseMessageWoDTO>(s);
                //Data.UserID = LoggedInUSerId;
                objdynamicobj = await Task.Run(() => workOrderMessageMasterData.AddFirebaseMessageData(Data, LoggedInUSerId));
                return objdynamicobj;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("SendMessageNotofication")]
        public async Task<List<dynamic>> SendMessageNotoficationDetails()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<workOrderxDTO>(s);
                Data.UserID = LoggedInUSerId;
                var Updatedata = await Task.Run(() => workOrderMessageMasterData.SendMessageNotoficationData(Data));
                return Updatedata;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("CreateMessage_WorkOrder_User")]
        public async Task<List<dynamic>> CreaterMessage_WorkOrder_User()
        {

            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<Message_WorkOrder_User_DTO>(s);
                Data.UserID = LoggedInUSerId;

                var Create_Update_Message_WorkOrder_User = await Task.Run(() => message_WorkOrder_User_Data.Create_Update_Message_WorkOrder_User(Data));

                return Create_Update_Message_WorkOrder_User;

            }
            catch (Exception ex)
            {
                log.logErrorMessage("CreateMessage_WorkOrder_User");
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }
        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("GetMessageWorkOrderUser")]
        public async Task<List<dynamic>> GetMessageWorkOrderUser()
        {

            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<Message_WorkOrder_User_DTO>(s);
                Data.UserID = LoggedInUSerId;

                var Get_Message_WorkOrder_UserDetails = await Task.Run(() => message_WorkOrder_User_Data.Get_Message_WorkOrder_UserDetails(Data));

                return Get_Message_WorkOrder_UserDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage("GetMessageWorkOrderUser");
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("GetMessageAdminUser")]
        public async Task<List<dynamic>> GetMessageAdminUser()
        {

            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<Message_Admin_UserDTO>(s);
                Data.UserID = LoggedInUSerId;

                var Get_Message_AdminUserDetails = await Task.Run(() => message_WorkOrder_User_Data.Get_Message_AdminUserDetails(Data));

                return Get_Message_AdminUserDetails;

            }
            catch (Exception ex)
            {
                log.logErrorMessage("GetMessageAdminUser");
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }


        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("AddMessageGroupMaster")]
        public async Task<List<dynamic>> AddMessageGroupMaster()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            Message_Group_MasterData message_Group_MasterData = new Message_Group_MasterData();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Message_Group_MasterDTO>(s);
                Data.UserID = LoggedInUSerId;
                Data.MGM_Group_UserID = LoggedInUSerId;


                var AddCity = await Task.Run(() => message_Group_MasterData.AddMessageGroupMaster(Data));
                return AddCity;


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("GetUserDropdownData")]
        public async Task<List<dynamic>> GetUserDropdownData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            UserMasterData userMasterData = new UserMasterData();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<UserFiltermasterDTO>(s);
                UserFiltermasterDTO userFiltermasterDTO = new UserFiltermasterDTO();
                Data.UserID = LoggedInUSerId;
                objdynamicobj = await Task.Run(() => userMasterData.GetUserfilterDetails(Data));
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

            return objdynamicobj;

        }

          [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("GetMessageChatGroupMaster")]
        public async Task<List<dynamic>> GetMessageChatGroupMaster()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            Message_Group_MasterData message_Group_MasterData = new Message_Group_MasterData();
            
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<Message_Group_MasterDTO>(s);
                Data.UserID = LoggedInUSerId;
                objdynamicobj = await Task.Run(() => message_Group_MasterData.GetMessageChatGroupMaster(Data));
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

            return objdynamicobj;

        }

        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("GetMessageChatGroupDrodown")]
        public async Task<List<dynamic>> GetMessageChatGroupDrodown()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            Message_Group_MasterData message_Group_MasterData = new Message_Group_MasterData();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<Message_User_MasterDTO>(s);
                Data.UserID = LoggedInUSerId;
                objdynamicobj = await Task.Run(() => message_Group_MasterData.GetMessageChatGroupDrodown(Data));
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

            return objdynamicobj;

        }

        //filter get message

        
    }

}




