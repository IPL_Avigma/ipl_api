﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Cors;
using Newtonsoft.Json;
using Avigma.Repository.Lib;
using IPL.Models;
using IPL.Repository.data;
using IPLApp.Models;
using IPLApp.Repository.data;

namespace IPL.Controllers
{
    [RoutePrefix("api/WorkOrderImport")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class WorkOrderImportController : BaseController
    {
        Log log = new Log();
        GoogleCloudData GoogleCloudData = new GoogleCloudData();
        WorkOrderMaster_ImportAPI_Data workOrderMaster_ImportAPI_Data = new WorkOrderMaster_ImportAPI_Data();
        WorkOrderCopyData workOrderCopyData = new WorkOrderCopyData();
        Import_Queue_TransData import_Queue_TransData = new Import_Queue_TransData();
        ImportFromMasterData importFromMasterData = new ImportFromMasterData();
        WorkOrderMaster_API_Data WorkOrderMaster_API_Data = new WorkOrderMaster_API_Data();
        Access_Column_GroupData accessColumnGroup = new Access_Column_GroupData();
        WorkOrderDataNotProcessedData WorkOrderDataNotProcessedData = new WorkOrderDataNotProcessedData();

        //get Client result Image  data 
        [Authorize]
        [HttpPost]
        [Route("ImportWorkOrdersFromCloud")]
        public async Task<List<dynamic>> ImportWorkOrdersFromCloud()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            FileUploadBucketDTO fileUploadBucketDTO = new FileUploadBucketDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

              
                FileUploadBucketDTO model = new FileUploadBucketDTO();



                var GetClientPhoto = await Task.Run(() => GoogleCloudData.ListObjects(model,1));







                return objdynamicobj;



            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }



        // for Get workorder Queue 
        [Authorize]
        [HttpPost]
        [Route("GetWorkOrderQueuedata")]
        public async Task<List<dynamic>> GetWorkOrderQueuedata()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
          

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<WorkOrderMaster_API_DTO>(s);

                WorkOrderMaster_API_DTO workOrderMaster_API_DTO = new WorkOrderMaster_API_DTO();

                workOrderMaster_API_DTO.Pkey_Id = Data.Pkey_Id;
                workOrderMaster_API_DTO.Imrt_PkeyId = Data.Imrt_PkeyId;
                workOrderMaster_API_DTO.Type = Data.Type;
                workOrderMaster_API_DTO.PageNumber = Data.PageNumber;
                workOrderMaster_API_DTO.NoofRows = Data.NoofRows;
                workOrderMaster_API_DTO.WhereClause = Data.WhereClause;
                workOrderMaster_API_DTO.Imrt_Import_From_ID = Data.Imrt_Import_From_ID;

                workOrderMaster_API_DTO.Imrt_Wo_Number = Data.Imrt_Wo_Number;
                workOrderMaster_API_DTO.Imrt_Wo_WTIDName = Data.Imrt_Wo_WTIDName;
                workOrderMaster_API_DTO.Imrt_Wo_CatIDName = Data.Imrt_Wo_CatIDName;

                var Getwoqueue = await Task.Run(() => workOrderMaster_ImportAPI_Data.GetWorkOrderQueueMasterDetails(workOrderMaster_API_DTO));

                return Getwoqueue;



            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        // for Get workorder Queue 
        [Authorize]
        [HttpPost]
        [Route("GetImportWorkOrderQueuedata")]
        public async Task<List<dynamic>> GetImportWorkOrderQueuedata()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<WorkOrderMaster_API_DTO>(s);

                WorkOrderMaster_API_DTO workOrderMaster_API_DTO = new WorkOrderMaster_API_DTO();

                workOrderMaster_API_DTO.Pkey_Id = Data.Pkey_Id;
                workOrderMaster_API_DTO.Type = Data.Type;

                var Getimwoqueue = await Task.Run(() => workOrderMaster_ImportAPI_Data.GetImportWorkOrderQueueMasterDetails(workOrderMaster_API_DTO));

                return Getimwoqueue;



            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //for Copy Work Oder details
		[Authorize]
        [HttpPost]
        [Route("PostCopyWorkOrderData")]
        public async Task<List<dynamic>> PostCopyWorkOrderData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<WorkOrderCopyDTO>(s);

                WorkOrderCopyDTO workOrderCopyDTO = new WorkOrderCopyDTO();

                workOrderCopyDTO.workOrder_ID = Data.workOrder_ID;
                workOrderCopyDTO.WorkOderInfo = Data.WorkOderInfo;
                workOrderCopyDTO.Type = Data.Type;
                workOrderCopyDTO.UserID = LoggedInUSerId;

                var Copyworkoder = await Task.Run(() => workOrderCopyData.AddCopyWorkOrderData(workOrderCopyDTO));

                return Copyworkoder;



            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        //for Import Work Oder details click on import button
		[Authorize]
        [HttpPost]
        [Route("PostImportWorkOderDetailData")]
        public async Task<List<dynamic>> PostImportWorkOderDetailData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<ImportWorkOrderNewDTO>(s);


                ImportWorkOrderNewDTO importWorkOrderNewDTO = new ImportWorkOrderNewDTO();

                for (int i = 0; i < Data.ImportWoArray.Count; i++)
                {
                    importWorkOrderNewDTO.Pkey_Id = Data.ImportWoArray[i].Pkey_Id;
                    importWorkOrderNewDTO.WorkType = Convert.ToInt64(Data.ImportWoArray[i].WorkType_Id);
                    importWorkOrderNewDTO.Contractor = Convert.ToString(Data.ImportWoArray[i].Contractor_Id);
                    importWorkOrderNewDTO.Processor = Convert.ToInt64(Data.ImportWoArray[i].Processor_Id);
                    importWorkOrderNewDTO.cordinator = Convert.ToInt64(Data.ImportWoArray[i].Coordinator_Id);
                    importWorkOrderNewDTO.Type = 1;
                    importWorkOrderNewDTO.UserID = LoggedInUSerId;
                    importWorkOrderNewDTO.Imrt_Import_From_ID = Data.Imrt_Import_From_ID;

                    objdynamicobj = await Task.Run(() => import_Queue_TransData.AddImportWorkOrderData(importWorkOrderNewDTO));
                    
                }
                Import_Queue_Trans_DTO import_Queue_Trans_DTO = new Import_Queue_Trans_DTO();
                import_Queue_Trans_DTO.Imrt_PkeyId = Data.Imrt_PkeyId.GetValueOrDefault(0);
                import_Queue_Trans_DTO.Imrt_Import_From_ID = Data.Imrt_Import_From_ID;
                import_Queue_Trans_DTO.Imrt_Wo_Import_ID = Data.Imrt_Wo_Import_ID;
                import_Queue_Trans_DTO.Type = 2;
                import_Queue_Trans_DTO.UserID = LoggedInUSerId;

                var objTransData = import_Queue_TransData.AddUpdateImportQueueTransData(import_Queue_Trans_DTO);

                return objdynamicobj;



            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //import user name

        [Authorize]
        [HttpPost]
        [Route("AddImportFromMaster")]
        public async Task<List<dynamic>> AddImportFromMaster()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<ImportFromMasterDTO>(s);
                Data.UserID = LoggedInUSerId;
                var AddImport = await Task.Run(() => importFromMasterData.AddImportFromMasterData(Data));

                return AddImport;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("GetImportFromMaster")]
        public async Task<List<dynamic>> GetImportFromMaster()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<ImportFromMasterDTO>(s);
                var GetImport = await Task.Run(() => importFromMasterData.Get_ImportFromMasterDetails(Data));

                return GetImport;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("GetPPWDataNotProcessed")]
        public async Task<List<dynamic>> GetPPWDataNotProcessed()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<WorkOrderDataNotProcessedDTO>(s);
                var GetImport = await Task.Run(() => WorkOrderDataNotProcessedData.GetWorkOrderDataNotProcessedDetails(Data));

                return GetImport;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        // Access Column Group


        [Authorize]
        [HttpPost]
        [Route("AddAccessColumnGroup")]
        public async Task<List<dynamic>> AddAcessColumnGroup()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<Access_Column_GroupDTO>(s);
                Data.UserId = LoggedInUSerId;
                var AddACG = await Task.Run(() => accessColumnGroup.CreateUpdateAccessColumnGroup(Data));

                return AddACG;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("GetAccessColumnGroup")]
        public async Task<List<dynamic>> GetAccessColumnGroup()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<Access_Column_Group_Input>(s);
                Data.UserId = LoggedInUSerId;
                var GetImport = await Task.Run(() => accessColumnGroup.GetaccessColumnGroupDetails(Data));

                return GetImport;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


    }
}
