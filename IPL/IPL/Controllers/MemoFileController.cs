﻿using Avigma.Models;
using Avigma.Repository.Lib;
using IPL.Models;
using IPL.Repository.data;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace IPL.Controllers
{
    [RoutePrefix("api/FileUpload")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class MemoFileController : BaseController
    {
        Log log = new Log();
        Mass_Email_MasterData mass_Email_MasterData = new Mass_Email_MasterData();
     

       // [Authorize]
       // [HttpPost]
       //// [AllowAnonymous]
       // [Route("EmailFileUpload")]

       // public async Task<HttpResponseMessage> EmailFileUpload()
       // {

       //     System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
       //     System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
       //     System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
       //     string s = reader.ReadToEnd();
          
       //     List<dynamic> Attachmentarr = new List<dynamic>();
       //     List<dynamic> filenamearr = new List<dynamic>();
       //     List<dynamic> Extension = new List<dynamic>();
       //     Dictionary<string, object> dict = new Dictionary<string, object>();
       //     string strfilepath = String.Empty;
       //     try
       //     {

       //         var httpRequest = HttpContext.Current.Request;

       //         string formobj = HttpContext.Current.Server.UrlDecode(httpRequest.Form.AllKeys[0]);

       //         //var Data = JsonConvert.DeserializeObject<Mass_Email_MasterDTO>(formobj);

       //         string strpath = System.Configuration.ConfigurationManager.AppSettings["EmailUploadDocPath"];
       //         Mass_Email_MasterDTO mass_Email_MasterDTO = new Mass_Email_MasterDTO();
       //         foreach (string file in httpRequest.Files)
       //         {
       //             HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created);
                

       //             var postedFile = httpRequest.Files[file];


       //             if (postedFile != null && postedFile.ContentLength > 0)
       //             {

       //                 int MaxContentLength = 1024 * 1024 * 2; 

       //                 IList<string> AllowedFileExtensions = new List<string> { ".pdf", ".ppt", ".pptx", ".PDF", ".DOC", ".DOCX", ".png", ".jpeg", ".jpg" };
       //                 var ext = postedFile.FileName.Substring(postedFile.FileName.LastIndexOf('.'));
       //                 var extension = ext.ToLower();
       //                 if (!AllowedFileExtensions.Contains(extension))
       //                 {
       //                     var message = string.Format("Please Upload file of type .pdf,.doc,.docx.,.img file");

       //                     dict.Add("Message", message);
       //                     return Request.CreateResponse(HttpStatusCode.Created, dict);
       //                 }
       //                 else if (postedFile.ContentLength > MaxContentLength)
       //                 {

       //                     var message = string.Format("Please Upload a file upto 2 mb.");

       //                     dict.Add("Message", message);
       //                     return Request.CreateResponse(HttpStatusCode.Created, dict);
       //                 }
       //                 else
       //                 {

       //                     string fileName = Guid.NewGuid() + Path.GetExtension(extension);
                           
       //                     strfilepath = strpath + fileName; 
       //                     string orginalName = postedFile.FileName;

       //                     //var filePath = HttpContext.Current.Server.MapPath(strpath + fileName);
       //                     var filePath = strpath + orginalName;

       //                     if (File.Exists(filePath))
       //                     {
       //                         File.Delete(filePath);
       //                     }
       //                     postedFile.SaveAs(filePath);

       //                     filenamearr.Add(orginalName);
       //                     Attachmentarr.Add(filePath);
       //                     Extension.Add(ext.Replace(".",""));


       //                     mass_Email_MasterDTO.Mass_Email_PkeyId = Convert.ToInt64(httpRequest.Form.GetValues(0)[0]);
       //                     mass_Email_MasterDTO.Mass_Email_Group_ID = Convert.ToInt64(httpRequest.Form.GetValues(1)[0]);
       //                     DateTime dDate;
       //                     string strchkval = httpRequest.Form.GetValues(2)[0];
       //                     if(!string.IsNullOrEmpty(strchkval))
       //                     {
       //                         mass_Email_MasterDTO.Mass_Email_Schedule_Time = DateTime.Parse(new string(strchkval.Take(24).ToArray()));
       //                     }
                            
       //                     //if (DateTime.TryParse(strchkval, out dDate))
       //                     //{
       //                     //    String.Format("{0:dd/MM/yyyy}", dDate, System.Globalization.CultureInfo.InvariantCulture);
       //                     //}

       //                     //mass_Email_MasterDTO.Mass_Email_Schedule_Time = dDate;
       //                     mass_Email_MasterDTO.Mass_Email_Subject = httpRequest.Form.GetValues(3)[0];
       //                     mass_Email_MasterDTO.Mass_Email_From = httpRequest.Form.GetValues(4)[0];
       //                     mass_Email_MasterDTO.Mass_Email_Message = httpRequest.Form.GetValues(5)[0];
       //                     mass_Email_MasterDTO.Mass_Email_User_Id = LoggedInUSerId;
       //                     mass_Email_MasterDTO.Mass_Email_IsActive = Convert.ToBoolean(httpRequest.Form.GetValues(6)[0]);
       //                     mass_Email_MasterDTO.Mass_Email_IsDelete = Convert.ToBoolean(httpRequest.Form.GetValues(7)[0]);


       //                 }
       //             }

       //             var message1 = string.Format("Document Updated Successfully.");
                  
       //             String result = message1 + "#" + strfilepath;
       //             //var msg = await Task.Run(() => Request.CreateErrorResponse(HttpStatusCode.Created, result));
                    
       //         }

                
       //         mass_Email_MasterDTO.Attachmentarr = Attachmentarr;
       //         mass_Email_MasterDTO.filenamearr = filenamearr;
       //         mass_Email_MasterDTO.Extension = Extension;
       //         mass_Email_MasterDTO.Type = 1;
       //         var msgx = await Task.Run(() => mass_Email_MasterData.EmailMassDetails(mass_Email_MasterDTO));

       //         var res = string.Format("Docment Email Sucessfully");
       //         dict.Add("ResponseType", "1");
       //         dict.Add("Message", res);
       //         return Request.CreateResponse(HttpStatusCode.Created, dict);


       //     }
       //     catch (Exception ex)
       //     {
       //         log.logErrorMessage(ex.StackTrace);
       //         log.logErrorMessage(ex.Message);
       //         var res = string.Format("Server Error Please check the File");
       //         dict.Add("ResponseType", "0");
       //         dict.Add("Message", res);
       //         return Request.CreateResponse(HttpStatusCode.Created, dict);
       //     }




       // }


        [Authorize]
        [HttpPost]
        [Route("EmailFileUpload")]
        public async Task<List<dynamic>> EmailFileUpload()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {

                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Mass_Email_MasterDTO>(s);
                string strfilepath = String.Empty;


                Data.Mass_Email_User_Id = LoggedInUSerId;
            
                Data.UserID = LoggedInUSerId;
                //Data.Type = 1;
                var msgx = await Task.Run(() => mass_Email_MasterData.EmailMassDetails(Data));
                objdynamicobj.Add(msgx);
            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
            }
            return objdynamicobj;
        }

        //get email data 
        [Authorize]
        [HttpPost]
        [Route("GetMassEmailData")]
        public async Task<List<dynamic>> GetMassEmailData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Mass_Email_MasterDTO>(s);
                Data.UserID = Convert.ToInt32(LoggedInUSerId);
                var GetEmail = await Task.Run(() => mass_Email_MasterData.GetMassEmailDetaills(Data));
                return GetEmail;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //get memo contractor details
        [Authorize]
        [HttpPost]
        [Route("GetmemoContractor")]
        public async Task<List<dynamic>> GetmemoContractor()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<User_Group_masterDTO>(s);
               
                var Getmemo = await Task.Run(() => mass_Email_MasterData.User_Group_ChangeDetaills(Data));
                return Getmemo;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
    }
}
