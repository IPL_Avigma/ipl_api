﻿using IPL.Models;
using IPL.Repository.data;
using IPLApp.Models;
using IPLApp.Repository.data;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using Avigma.Repository.Lib;

namespace IPL.Controllers
{
    [RoutePrefix("api/Suggestion")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class SuggestionBoxController : BaseController
    {
        Log log = new Log();
        Suggestion_MasterData suggestion_MasterData = new Suggestion_MasterData();
        Suggestion_VoteData suggestion_VoteData = new Suggestion_VoteData();
        MassTemplateData massTemplateData = new MassTemplateData();
        

        [Authorize]
        [HttpPost]
        [Route("PostSuggestionBoxData")]
        public async Task<List<dynamic>> PostSuggestionBoxData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Suggestion_Master_DTO>(s);
                Data.UserID = Convert.ToInt32(LoggedInUSerId);
                Data.Sug_UserID = Convert.ToInt32(LoggedInUSerId);
                var AddSugges = await Task.Run(() => suggestion_MasterData.AddUpdateUserSuggestion_Master_Data(Data));
                return AddSugges;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [Authorize]
        [HttpPost]
        [Route("GetSuggestionBoxData")]
        public async Task<List<dynamic>> GetSuggestionBoxData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Suggestion_Master_DTO>(s);
                Data.UserID = Convert.ToInt32(LoggedInUSerId);

                var GetSugges = await Task.Run(() => suggestion_MasterData.Get_Suggestion_MasterDetails(Data));
                return GetSugges;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("AddSuggestionVoteData")]
        public async Task<List<dynamic>> AddSuggestionVoteData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Suggestion_Vote_DTO>(s);
                Data.UserID = Convert.ToInt32(LoggedInUSerId);
                Data.Sug_Vote_UserID = Convert.ToInt32(LoggedInUSerId);
                var AddSugges = await Task.Run(() => suggestion_VoteData.AddUpdateSuggestion_Vote_Data(Data));
                return AddSugges;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("AddMassTemplateData")]
        public async Task<List<dynamic>> AddMassTemplateData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<MassTemplateDTO>(s);
                Data.UserID = Convert.ToInt32(LoggedInUSerId);
                var AddMassTemp = await Task.Run(() => massTemplateData.AddMassTemplate(Data));
                return AddMassTemp;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("GetMassTemplateData")]
        public async Task<List<dynamic>> GetMassTemplateData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<MassTemplateDTO>(s);
                Data.UserID = Convert.ToInt32(LoggedInUSerId);
                var GetMassTemp = await Task.Run(() => massTemplateData.GetMassTemplateDetails(Data));
                return GetMassTemp;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
    }

}
