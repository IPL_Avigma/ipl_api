﻿using IPL.Models;
using IPL.Repository.data;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using Avigma.Repository.Lib;

namespace IPL.Controllers
{
    [RoutePrefix("api/ContactUs")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ContactUsController : BaseController
    {
        ContactUsData contactUsData = new ContactUsData();
        IPL_Adopter_MasterData iPL_Adopter_MasterData = new IPL_Adopter_MasterData();
        Log log = new Log();

        [HttpPost]
        [Route("PostWebContactDetails")]

        public async Task<List<dynamic>> PostWebContactDetails()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<ContactUsDTO>(s);

                var AddContact = await Task.Run(() => contactUsData.AddContactDetailsMaster(Data));
                return AddContact;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }
        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("PostIPLAdopterDetails")]

        public async Task<List<dynamic>> PostIPLAdopterDetails()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<IPL_Adopter_MasterDTO>(s);
                Data.IPL_Adopter_IsActive = true;
                Data.IPL_Adopter_IsDelete = false;
                Data.UserID = LoggedInUSerId;

                var AddAdopter = await Task.Run(() => iPL_Adopter_MasterData.AddupdateIPL_AdopterDetails(Data));
                return AddAdopter;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }
        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("GetcheckboxDetails")]

        public async Task<List<dynamic>> GetcheckboxDetails()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<IPL_Adop_Chk_MasterDTO>(s);
                Data.Type = 1;
                Data.UserID = LoggedInUSerId;

                var GetChk = await Task.Run(() => iPL_Adopter_MasterData.GetIPL_Adop_Chk_Master(Data));
                return GetChk;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("GetIPLAdopterDetails")]

        public async Task<List<dynamic>> GetIPLAdopterDetails()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<IPL_Adopter_MasterDTO>(s);
               
                Data.UserID = LoggedInUSerId;

                var Getdetail = await Task.Run(() => iPL_Adopter_MasterData.GetIPL_AdopterDetails(Data));
                return Getdetail;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }
        [Authorize]
        [HttpPost]
        [Route("DeleteIPLAdopterDetails")]

        public async Task<List<dynamic>> DeleteIPLAdopterDetails()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<IPL_Adopter_MasterDTO>(s);

                Data.UserID = LoggedInUSerId;

                var Deletedetail = await Task.Run(() => iPL_Adopter_MasterData.DeleteIPL_AdopterDetails(Data));
                return Deletedetail;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }
    }
}
