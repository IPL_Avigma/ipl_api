﻿using IPL.Models;
using IPL.Repository.data;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using Avigma.Repository.Lib;

namespace IPL.Controllers
{
    [RoutePrefix("api/ContractorReport")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ContractorReportController : BaseController
    {
        Contractor_Paid_InvoiceData contractor_Paid_InvoiceData = new Contractor_Paid_InvoiceData();
        ContractorReportPendingData contractorReportPendingData = new ContractorReportPendingData();
        Log log = new Log();

        [Authorize]
        [HttpPost]
        [Route("GetContractorPaidInvoice")]
        public async Task<List<dynamic>> GetContractorPaidInvoice()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Contractor_Paid_Invoice_DateDTO>(s);
                Data.UserID = LoggedInUSerId;

                var getconpaid = await Task.Run(() => contractor_Paid_InvoiceData.GetContractorInvoice_Details(Data));
                return getconpaid;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //pending 
        [Authorize]
        [HttpPost]
        [Route("GetContractorPendingInvoice")]
        public async Task<List<dynamic>> GetContractorPendingInvoice()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Contractor_Paid_InvoiceDTO>(s);
                Data.UserID = LoggedInUSerId;

                var getconpending = await Task.Run(() => contractorReportPendingData.GetContractorInvoice_Details(Data));
                return getconpending;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //currently due 
        [Authorize]
        [HttpPost]
        [Route("GetCurrentlyDueContractorReport")]
        public async Task<List<dynamic>> GetCurrentlyDueContractorReport()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Contractor_Paid_InvoiceDTO>(s);
                Data.UserID = LoggedInUSerId;

                var getconpending = await Task.Run(() => contractorReportPendingData.GetCurrentlyDueContractorReport(Data));
                return getconpending;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //Pending Report Data
        [Authorize]
        [HttpPost]
        [Route("GetPendingReport")]
        public async Task<List<dynamic>> GetPendingReport()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Contractor_Paid_InvoiceDTO>(s);
                Data.UserID = LoggedInUSerId;

                var getconpending = await Task.Run(() => contractorReportPendingData.GetPendingReport(Data));
                return getconpending;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
    }
}
