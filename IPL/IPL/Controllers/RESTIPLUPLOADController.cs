﻿using Avigma.Repository.Lib;
using IPL.Controllers;
using IPL.Models;
using IPL.Repository.data;
using IPLApp.Models;
using IPLApp.Repository.data;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace IPLApp.Controllers
{
    [RoutePrefix("api/RESTIPLUPLOAD")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class RESTIPLUPLOADController : BaseController
    {
        AppCompanyMasterData appCompanyMasterData = new AppCompanyMasterData();
        UserMasterData userMasterData = new UserMasterData();
        UserDocumentData userDocumentData = new UserDocumentData();
        User_Background_checkData user_Background_checkData = new User_Background_checkData();

        Log log = new Log();

        //Upload  All Format 
        //[Authorize]
        //[HttpPost]
        //[Route("PostDocumentData")]
        //[AllowAnonymous]
        //public async Task<HttpResponseMessage> PostDocumentData()
        //{

        //    System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
        //    System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
        //    System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
        //    string s = reader.ReadToEnd();

        //    Dictionary<string, object> dict = new Dictionary<string, object>();
        //    string strfilepath = String.Empty;
        //    try
        //    {
        //        // mutiple file here check only
        //        System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;

        //        var httpRequest = HttpContext.Current.Request;

        //        foreach (string file in httpRequest.Files)
        //        {
        //            HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created);


        //            string[] PkID = file.Split('#');

        //            string ModelpkeyId = PkID[0];

        //            var postedFile = httpRequest.Files[file];
        //            if (postedFile != null && postedFile.ContentLength > 0)
        //            {

        //                int MaxContentLength = 1024 * 1024 * 2; //Size = 2 MB  

        //                IList<string> AllowedFileExtensions = new List<string> { ".pdf", ".ppt", ".pptx", ".PDF", ".DOC", ".DOCX", ".png", ".jpeg", ".jpg" };
        //                var ext = postedFile.FileName.Substring(postedFile.FileName.LastIndexOf('.'));
        //                var extension = ext.ToLower();
        //                if (!AllowedFileExtensions.Contains(extension))
        //                {

        //                    var message = string.Format("Please Upload file of type .pdf,.doc,.docx.,.img file");

        //                    dict.Add("Message", message);
        //                    return Request.CreateResponse(HttpStatusCode.Created, dict);
        //                }
        //                else if (postedFile.ContentLength > MaxContentLength)
        //                {

        //                    var message = string.Format("Please Upload a file upto 2 mb.");

        //                    dict.Add("Message", message);
        //                    return Request.CreateResponse(HttpStatusCode.Created, dict);
        //                }
        //                else
        //                {

        //                    string strpath = System.Configuration.ConfigurationManager.AppSettings["UploadDocPath"];

        //                    string fileName = Guid.NewGuid() + Path.GetExtension(extension);

        //                    //Int64 Stp_pkeyId = (Convert.ToInt64(UserpkeyId)); // dB user Email
        //                    strfilepath = strpath + fileName; // dB pic path
        //                    string orginalName = postedFile.FileName; // dB pic name

        //                    var filePath = HttpContext.Current.Server.MapPath(strpath + fileName);//"~/Userimage/"+ extension

        //                    postedFile.SaveAs(filePath);

        //                    AppCompanyImagesDTO appCompanyImagesDTO = new AppCompanyImagesDTO();



        //                    appCompanyImagesDTO.App_Com_Img_CompanyID = Convert.ToInt64(ModelpkeyId);
        //                    appCompanyImagesDTO.App_Com_Img_FilePath = strfilepath;
        //                    appCompanyImagesDTO.App_Com_Img_FileName = orginalName;
        //                    appCompanyImagesDTO.App_Com_Img_IsActive = true;
        //                    appCompanyImagesDTO.Type = 1;

        //                    var msgx = await Task.Run(() => appCompanyMasterData.AddAppCompanyImageData(appCompanyImagesDTO));



        //                }
        //            }

        //            var message1 = string.Format("Document Updated Successfully.");
        //            //var message2 = strfilepath;
        //            String result = message1 + "#" + strfilepath;
        //            var msg = await Task.Run(() => Request.CreateErrorResponse(HttpStatusCode.Created, result));
        //            return msg;
        //        }
        //        var res = string.Format("Please Upload a document.");
        //        dict.Add("Message", res);
        //        return Request.CreateResponse(HttpStatusCode.Created, dict);

        //    }
        //    catch (Exception ex)
        //    {
        //        log.logErrorMessage(ex.StackTrace);
        //        log.logErrorMessage(ex.Message);
        //        var res = string.Format("Server Error, To Large File Size");
        //        dict.Add("Message", res);
        //        return Request.CreateResponse(HttpStatusCode.Created, dict);
        //    }




        //}





        // this api fetch all  AppCompanyInfo records get image or document.
        //Get AppCompanyInfo Image  master
        [Authorize]
        [HttpPost]
        [Route("GetAppCompanyInfoImageData")]
        public async Task<List<dynamic>> GetAppCompanyInfoImageData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            AppCompanyImagesDTO appCompanyMasterDTO = new AppCompanyImagesDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<AppCompanyImagesDTO>(s);
                appCompanyMasterDTO.App_Com_Img_pkeyID = Data.App_Com_Img_pkeyID;
                appCompanyMasterDTO.App_Com_Img_CompanyID = Data.App_Com_Img_CompanyID;
                appCompanyMasterDTO.Type = Data.Type;

                var GetAppComapany = await Task.Run(() => appCompanyMasterData.GetAppCompanyImageDetails(appCompanyMasterDTO));
                return GetAppComapany;

            }
            catch (Exception ex)
            {
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }




        [Authorize]
        [HttpPost]
        [Route("PostUserDocumentData")]
        [AllowAnonymous]
        public async Task<HttpResponseMessage> PostUserDocumentData()
        {
            log.logErrorMessage("PostUserDocumentData");
            System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
            System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
            System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
            string s = reader.ReadToEnd();

            Dictionary<string, object> dict = new Dictionary<string, object>();
            string strfilepath = String.Empty;
            try
            {
                var httpRequest = HttpContext.Current.Request;
                foreach (string file in httpRequest.Files)
                {
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created);


                    string[] PkID = file.Split('#');

                    string ModelpkeyId = PkID[0];

                    var postedFile = httpRequest.Files[file];
                    #region comment
                    //if (postedFile != null && postedFile.ContentLength > 0)
                    //{

                    //    int MaxContentLength = 1024 * 1024 * 2; //Size = 2 MB  

                    //    IList<string> AllowedFileExtensions = new List<string> { ".pdf", ".ppt", ".pptx", ".PDF", ".DOC", ".DOCX", ".png", ".jpeg", ".jpg" };
                    //    var ext = postedFile.FileName.Substring(postedFile.FileName.LastIndexOf('.'));
                    //    var extension = ext.ToLower();
                    //    if (!AllowedFileExtensions.Contains(extension))
                    //    {
                    //        var message = string.Format("Please Upload file of type .pdf,.doc,.docx.,.img file");
                    //        dict.Add("Message", message);
                    //        return Request.CreateResponse(HttpStatusCode.Created, dict);
                    //    }
                    //    else if (postedFile.ContentLength > MaxContentLength)
                    //    {
                    //        var message = string.Format("Please Upload a file upto 2 mb.");
                    //        dict.Add("Message", message);
                    //        return Request.CreateResponse(HttpStatusCode.Created, dict);
                    //    }
                    //    else
                    //    {
                    //        string strpath = System.Configuration.ConfigurationManager.AppSettings["UploadDocPathUserDocuments"];
                    //        string fileName = Guid.NewGuid() + Path.GetExtension(extension);
                    //        strfilepath = strpath + fileName; // dB pic path
                    //        string orginalName = postedFile.FileName; // dB pic name
                    //        var filePath = HttpContext.Current.Server.MapPath(strpath + fileName);

                    //        postedFile.SaveAs(filePath);
                    //    }
                    //}
                    #endregion
                    var message1 = string.Format("Document Updated Successfully.");
                    String result = message1 + "#" + strfilepath;
                    var msg = await Task.Run(() => Request.CreateErrorResponse(HttpStatusCode.Created, result));
                    return msg;
                }
                var res = string.Format("Please Upload a document.");
                dict.Add("Message", res);
                return Request.CreateResponse(HttpStatusCode.Created, dict);
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                var res = string.Format("Server Error, To Large File Size");
                dict.Add("Message", res);
                return Request.CreateResponse(HttpStatusCode.Created, dict);
            }




        }




        //Upload  All Format  ImageBackground
        [Authorize]
        [HttpPost]
        [Route("PostUserDocumentUserImageBackground")]
        [AllowAnonymous]
        public async Task<HttpResponseMessage> PostUserDocumentUserImageBackground()
        {

            log.logDebugMessage("PostUserDocumentUserImageBackground API Called");
            System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
            System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
            System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
            string s = reader.ReadToEnd();

            Dictionary<string, object> dict = new Dictionary<string, object>();
            string strfilepath = String.Empty;
            try
            {

                var httpRequest = HttpContext.Current.Request;

                System.Web.HttpFileCollection hfc = System.Web.HttpContext.Current.Request.Files;

                foreach (string file in httpRequest.Files)
                {
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created);


                    string[] PkID = file.Split('#');

                    string ModelpkeyId = PkID[0];


                    var postedFile = httpRequest.Files[file];
                    #region comment
                    //if (postedFile != null && postedFile.ContentLength > 0)
                    //{

                    //    int MaxContentLength = 1024 * 1024 * 2; //Size = 2 MB  

                    //    IList<string> AllowedFileExtensions = new List<string> { ".pdf", ".ppt", ".pptx", ".PDF", ".DOC", ".DOCX", ".png", ".jpeg", ".jpg" };
                    //    var ext = postedFile.FileName.Substring(postedFile.FileName.LastIndexOf('.'));
                    //    var extension = ext.ToLower();
                    //    if (!AllowedFileExtensions.Contains(extension))
                    //    {

                    //        var message = string.Format("Please Upload file of type .pdf,.doc,.docx.,.img file");

                    //        dict.Add("Message", message);
                    //        return Request.CreateResponse(HttpStatusCode.Created, dict);
                    //    }
                    //    else if (postedFile.ContentLength > MaxContentLength)
                    //    {

                    //        var message = string.Format("Please Upload a file upto 2 mb.");

                    //        dict.Add("Message", message);
                    //        return Request.CreateResponse(HttpStatusCode.Created, dict);
                    //    }
                    //    else
                    //    {

                    //        string strpath = System.Configuration.ConfigurationManager.AppSettings["UploadDocPathUserDocuments"];

                    //        string fileName = Guid.NewGuid() + Path.GetExtension(extension);

                    //        //Int64 Stp_pkeyId = (Convert.ToInt64(UserpkeyId)); // dB user Email
                    //        strfilepath = strpath + fileName; // dB pic path
                    //        string orginalName = postedFile.FileName; // dB pic name

                    //        var filePath = HttpContext.Current.Server.MapPath(strpath + fileName);//"~/Userimage/"+ extension

                    //        postedFile.SaveAs(filePath);

                    //        UserMasterDTO userMasterDTO = new UserMasterDTO();



                    //        userMasterDTO.User_pkeyID = Convert.ToInt64(ModelpkeyId);
                    //        userMasterDTO.User_BackgroundDocPath = strfilepath;
                    //        userMasterDTO.User_BackgroundDocName = orginalName;
                    //        userMasterDTO.Type = 5;

                    //        var returnVal = await Task.Run(() => userMasterData.AddUserMasterdetails(userMasterDTO));



                    //    }
                    //}
                    #endregion
                    var message1 = string.Format("Document Updated Successfully.");
                    //var message2 = strfilepath;
                    String result = message1 + "#" + strfilepath;
                    var msg = await Task.Run(() => Request.CreateErrorResponse(HttpStatusCode.Created, result));
                    return msg;
                }
                var res = string.Format("Please Upload a document.");
                dict.Add("Message", res);
                return Request.CreateResponse(HttpStatusCode.Created, dict);

                


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                var res = string.Format("Server Error, To Large File Size");
                dict.Add("Message", res);
                return Request.CreateResponse(HttpStatusCode.Created, dict);
            }




        }


        // this api fetch all  user records get image or document.
        //Get user document Image  master
        [Authorize]
        [HttpPost]
        [Route("GetUserDocumentMasterData")]
        public async Task<List<dynamic>> GetUserDocumentMasterData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            UserDocumentDTO userDocumentDTO = new UserDocumentDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<UserDocumentDTO>(s);
                userDocumentDTO.User_Doc_pkeyID = Data.User_Doc_pkeyID;
                userDocumentDTO.User_Doc_UserID = Data.User_Doc_UserID;
                userDocumentDTO.Type = Data.Type;
                userDocumentDTO.UserID = LoggedInUSerId;
                var GetAppComapany = await Task.Run(() => userDocumentData.GetUserDocumentDetails(userDocumentDTO));
                return GetAppComapany;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        // this api  user records delete  document. document pkey id wise 
        //Delete dcoument user Master 
        [Authorize]
        [HttpPost]
        [Route("DeleteUserDocumentMasterData")]
        public async Task<List<dynamic>> DeleteUserDocumentMasterData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            UserDocumentDTO userDocumentDTO = new UserDocumentDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<UserDocumentDTO>(s);
                userDocumentDTO.User_Doc_pkeyID = Data.User_Doc_pkeyID;
                userDocumentDTO.Type = Data.Type;
                userDocumentDTO.UserID = LoggedInUSerId;
                var GetAppComapany = await Task.Run(() => userDocumentData.AddUserDocumentData(userDocumentDTO));
                return GetAppComapany;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }




        [Authorize]
        [HttpPost]

        //[AllowAnonymous]
        [Route("Save")]

        public async Task<HttpResponseMessage> Save()
        {

            System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
            System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
            System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
            string s = reader.ReadToEnd();

            Dictionary<string, object> dict = new Dictionary<string, object>();
            string strfilepath = String.Empty;
            try
            {

                var httpRequest = HttpContext.Current.Request;

                foreach (string file in httpRequest.Files)
                {
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created);


                    string[] PkID = file.Split('#');

                    string ModelpkeyId = PkID[0];

                    var postedFile = httpRequest.Files[file];
                    if (postedFile != null && postedFile.ContentLength > 0)
                    {

                        int MaxContentLength = 1024 * 1024 * 2; //Size = 2 MB  

                        IList<string> AllowedFileExtensions = new List<string> { ".pdf", ".ppt", ".pptx", ".PDF", ".DOC", ".DOCX", ".png", ".jpeg", ".jpg" };
                        var ext = postedFile.FileName.Substring(postedFile.FileName.LastIndexOf('.'));
                        var extension = ext.ToLower();
                        if (!AllowedFileExtensions.Contains(extension))
                        {

                            var message = string.Format("Please Upload file of type .pdf,.doc,.docx.,.img file");

                            dict.Add("Message", message);
                            return Request.CreateResponse(HttpStatusCode.Created, dict);
                        }
                        else if (postedFile.ContentLength > MaxContentLength)
                        {

                            var message = string.Format("Please Upload a file upto 2 mb.");

                            dict.Add("Message", message);
                            return Request.CreateResponse(HttpStatusCode.Created, dict);
                        }
                        else
                        {

                            string strpath = System.Configuration.ConfigurationManager.AppSettings["UploadingFiles"];

                            string fileName = Guid.NewGuid() + Path.GetExtension(extension);

                            //Int64 Stp_pkeyId = (Convert.ToInt64(UserpkeyId)); // dB user Email
                            strfilepath = strpath + fileName; // dB pic path
                            string orginalName = postedFile.FileName; // dB pic name

                            var filePath = HttpContext.Current.Server.MapPath(strpath + fileName);//"~/Userimage/"+ extension

                            postedFile.SaveAs(filePath);

                            //Client_Result_PhotoDTO client_Result_PhotoDTO = new Client_Result_PhotoDTO();



                            //client_Result_PhotoDTO.Client_Result_Photo_Wo_ID = 1;
                            //client_Result_PhotoDTO.Client_Result_Photo_FilePath = strfilepath;
                            //client_Result_PhotoDTO.Client_Result_Photo_FileName = orginalName;
                            //client_Result_PhotoDTO.Client_Result_Photo_IsActive = true;
                            //client_Result_PhotoDTO.Type = 1;

                            //var msgx = await Task.Run(() => client_Result_PhotoData.AddClientResultPhotoData(client_Result_PhotoDTO));



                        }
                    }

                    var message1 = string.Format("Document Updated Successfully.");
                    //var message2 = strfilepath;
                    String result = message1 + "#" + strfilepath;
                    var msg = await Task.Run(() => Request.CreateErrorResponse(HttpStatusCode.Created, result));
                    return msg;
                }
                var res = string.Format("Please Upload a document.");
                dict.Add("Message", res);
                return Request.CreateResponse(HttpStatusCode.Created, dict);

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                var res = string.Format("Server Error, To Large File Size");
                dict.Add("Message", res);
                return Request.CreateResponse(HttpStatusCode.Created, dict);
            }




        }

        //Get user Background Image  master
        [Authorize]
        [HttpPost]
        [Route("GetUserBackgroundMasterData")]
        public async Task<List<dynamic>> GetUserBackgroundMasterData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            User_Background_ProviderDTO User_Background_ProviderDTO = new User_Background_ProviderDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<User_Background_ProviderDTO>(s);
                User_Background_ProviderDTO.Background_check_PkeyID = Data.Background_check_PkeyID;
                User_Background_ProviderDTO.Background_check_User_ID = Data.Background_check_User_ID;
                User_Background_ProviderDTO.Type = Data.Type;

                var GetAppComapany = await Task.Run(() => user_Background_checkData.GetUserBackgroundDetails(User_Background_ProviderDTO));
                return GetAppComapany;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        //Delete dcoument user Master 
        [Authorize]
        [HttpPost]
        [Route("DeleteUserBackgroundMasterData")]
        public async Task<List<dynamic>> DeleteUserBackgroundMasterData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            User_Background_ProviderDTO user_Background_ProviderDTO = new User_Background_ProviderDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<User_Background_ProviderDTO>(s);
                user_Background_ProviderDTO.Background_check_PkeyID = Data.Background_check_PkeyID;
                user_Background_ProviderDTO.Type = Data.Type;

                var GetAppComapany = await Task.Run(() => user_Background_checkData.CreateUpdateUserBackgroundcheckData(user_Background_ProviderDTO));
                return GetAppComapany;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


    }
}
