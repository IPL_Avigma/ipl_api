﻿using Avigma.Repository.Lib;
using IPL.Models;
using IPLApp.Models;
using IPL.Repository.data;
using IPLApp.Repository.data;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using Avigma.Models;

namespace IPL.Controllers
{
    [RoutePrefix("api/Mobile")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class MobileIPLController : BaseController
    {
        Log log = new Log();
        IPLStateData iPLStateData = new IPLStateData();
        InstructionMasterData instructionMasterData = new InstructionMasterData();
        WorkOrderMaster_Details_MobileApp_Data workOrderMaster_Details_MobileApp_Data = new WorkOrderMaster_Details_MobileApp_Data();
        ClientResultDataMaster clientResultDataMaster = new ClientResultDataMaster();
        Invoice_ContractorData invoice_ContractorData = new Invoice_ContractorData();
        TaskBidMasterData taskBidMasterData = new TaskBidMasterData();
        UserLoginMaster userLoginMaster = new UserLoginMaster();
        WorkOrderMasterData WorkOrderMasterDataOBJ = new WorkOrderMasterData();
        UserMasterData userMasterData = new UserMasterData();
        IPL_Custom_DropDown_MasterData iPL_Custom_DropDown_MasterData = new IPL_Custom_DropDown_MasterData();
        Contractor_Paid_InvoiceData contractor_Paid_InvoiceData = new Contractor_Paid_InvoiceData();
        ContractorReportPendingData contractorReportPendingData = new ContractorReportPendingData();
        MobileWorkOrderFilterData mobileWorkOrderFilterData = new MobileWorkOrderFilterData();
        Folder_File_Child_MasterData folder_File_Child_MasterData = new Folder_File_Child_MasterData();
        Mass_Email_MasterData mass_Email_MasterData = new Mass_Email_MasterData();
        Contractor_ScoreCard_SettingData contractor_ScoreCard_SettingData = new Contractor_ScoreCard_SettingData();
        UserDocumentData userDocumentData = new UserDocumentData();
        User_Background_checkData user_Background_checkData = new User_Background_checkData();
        // for get work order, instruction child , and instruction master data


        [Authorize]

        [HttpPost]
        [Route("PostUserLoginDatailsData")]
        public async Task<List<dynamic>> PostUserLoginDatailsData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
              
                log.logInfoMessage(s);
                var Data = JsonConvert.DeserializeObject<AuthLoginDTO>(s);

                Data.UserID = Convert.ToInt32(LoggedInUSerId);
                Data.User_pkeyID = Convert.ToInt32(LoggedInUSerId);
                Data.Type = 4;
              
                var Getinvestorlogin = await Task.Run(() => userLoginMaster.GetLoginDetails(Data));
              
                return Getinvestorlogin;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        //[AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("GetStateDetail")]
        public async Task<List<dynamic>> GetStateDetail()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            IPLStateDTO iPLStateDTO = new IPLStateDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<IPLStateDTO>(s);
                Data.UserID = LoggedInUSerId;
                objdynamicobj = await Task.Run(() => iPLStateData.GetStateDetails(Data));
             
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
            return objdynamicobj;
        }


        [Authorize]
        [HttpPost]
        [Route("GetMobleWorkOrderData")]

        public async Task<List<dynamic>> GetMobleWorkOrderData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                //log.logInfoMessage("GetMobleWorkOrderData-----------> Start");
                //log.logInfoMessage(s);
                //log.logInfoMessage("GetMobleWorkOrderData-----------> End");
                var Data = JsonConvert.DeserializeObject<workOrderxDTO>(s);
                Data.UserID = LoggedInUSerId;
               var mobile = await Task.Run(() => workOrderMaster_Details_MobileApp_Data.GetWorkOrderDataDetailsForMobile(Data));
                return mobile;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }


         [Authorize]
        [HttpPost]
        [Route("UpdateEstimateDateWorkOrderData")]
        public async Task<List<dynamic>> UpdateEstimateDateWorkOrderData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
               

                var Data = JsonConvert.DeserializeObject<RootworkOrder>(s);

                workOrderxDTO workOrderDTO = new workOrderxDTO();

                workOrderDTO.EstimatedDate = Data.EstimatedDate;
                workOrderDTO.workOrder_ID = Data.workOrder_ID;
                workOrderDTO.Type = 7;
                workOrderDTO.UserID = LoggedInUSerId;
                workOrderDTO.currUserId = Convert.ToInt32(LoggedInUSerId);


                var returndelVal = await Task.Run(() => WorkOrderMasterDataOBJ.AddWorkorderDataAsync(workOrderDTO));
                return returndelVal;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


         
        [Authorize]
        [HttpPost]
        [Route("GetClientPhotoMasterByTaskData")]
        public async Task<List<dynamic>> GetClientPhotoMasterByTaskData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();


                var Data = JsonConvert.DeserializeObject<ClientResultPhotoRef>(s);
                Data.Type = 1;
                Data.UserID = LoggedInUSerId;
                Client_Result_PhotoData client_Result_PhotoData = new Client_Result_PhotoData();


                var returndelVal = await Task.Run(() => client_Result_PhotoData.Get_ClientPhotoMasterByTaskData(Data));
                return returndelVal;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //get instruction Task Name 
        [Authorize]
        [HttpPost]
        [Route("GetInstructionTaskName")]
        public async Task<List<dynamic>> GetInstructionTaskName()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                InstctionMasterDropdownName instctionMasterDropdownName = new InstctionMasterDropdownName();


                var Data = JsonConvert.DeserializeObject<InstctionMasterDropdownName>(s);
                Data.UserID = LoggedInUSerId;
                instctionMasterDropdownName.Inst_Task_pkeyId = Data.Inst_Task_pkeyId;
                instctionMasterDropdownName.Type = Data.Type;
                instctionMasterDropdownName.UserID = Data.UserID;
                instctionMasterDropdownName.WorkOrderID = Data.WorkOrderID;


                        var GetinstructionType = await Task.Run(() => instructionMasterData.GetInstructionForMobile(instctionMasterDropdownName));
                return GetinstructionType;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.Message);
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //post instruction for ,mobile

        [Authorize]
        [HttpPost]
        [Route("PostInstructionmobile")]
        public async Task<List<dynamic>> PostInstructionmobile()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
            


                var Data = JsonConvert.DeserializeObject<InstctionMasterDTO>(s);

                Data.UserID = LoggedInUSerId;
              

                var Addinstruction = await Task.Run(() => instructionMasterData.AddInstructionMobileData(Data));
                return Addinstruction;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //post Invoice Data 
        [Authorize]
        [HttpPost]
        [Route("PostInvoiceDetailsmobile")]
        public async Task<List<dynamic>> PostInvoiceDetailsmobile()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();



                var Data = JsonConvert.DeserializeObject<Invoice_ContractorDTO>(s);
                Data.UserID = LoggedInUSerId;


                var Addinvoice = await Task.Run(() => clientResultDataMaster.ClientResultInvoiceContractorPost(Data));
                return Addinvoice;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //Get Invoice Data 
        [Authorize]
        [HttpPost]
        [Route("GetInvoiceDetailsmobile")]
        public async Task<List<dynamic>> GetInvoiceDetailsmobile()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();



                var Data = JsonConvert.DeserializeObject<Invoice_ContractorDTO>(s);

                Data.UserID = LoggedInUSerId;

                Invoice_ContractorDTO invoice_ContractorDTO = new Invoice_ContractorDTO();

                invoice_ContractorDTO.Inv_Con_Wo_ID = Data.Inv_Con_Wo_ID;
                invoice_ContractorDTO.UserID = Data.UserID;


                var Getinvoice = await Task.Run(() => invoice_ContractorData.GetInvoiceContractor_Mobile_Details(Data));
                return Getinvoice;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        //get bid task for photo  data 
        [Authorize]
        [HttpPost]
        [Route("GettaskbidPhotosmobile")]
        public async Task<List<dynamic>> GettaskbidPhotosmobile()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            TaskBidforimg taskBidforimg = new TaskBidforimg();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                TaskBidForPhotoData taskBidForPhotoData = new TaskBidForPhotoData();

                var Data = JsonConvert.DeserializeObject<TaskBidforimg>(s);
              
                taskBidforimg.Task_Bid_TaskID = Data.Task_Bid_TaskID;
                taskBidforimg.Task_Bid_WO_ID = Data.Task_Bid_WO_ID;
                taskBidforimg.Task_Bid_Sys_Type = Data.Task_Bid_Sys_Type;
                taskBidforimg.UserID = LoggedInUSerId;

                var Gettask = await Task.Run(() => taskBidForPhotoData.GetInvoiceBidfortaskMobApp(taskBidforimg));
                return Gettask;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //get bid task for photo  data 
        [Authorize]
        [HttpPost]
        [Route("PostContractorRejectTask")]
        public async Task<List<dynamic>> PostContractorRejectTask()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
 

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                

                var Data = JsonConvert.DeserializeObject<ContractorRejectTask>(s);
                Data.TaskRejectBy = LoggedInUSerId;
                Data.Type = 1;
                Data.UserID = LoggedInUSerId;


                var Gettask = await Task.Run(() => taskBidMasterData.Contractor_RejectTask(Data));
                return Gettask;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        //get bid task for photo  data 
        //[AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("GetUserProfile")]
        public async Task<List<dynamic>> GetUserProfile()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage("<-------------------GetUserProfile start------------------>");
                log.logDebugMessage(s);
                log.logDebugMessage("<-------------------GetUserProfile end------------------>");
                var Data = JsonConvert.DeserializeObject<ProfileUserMaster>(s);
                Data.UserID = LoggedInUSerId;
                Data.User_pkeyID = LoggedInUSerId;


                var Gettask = await Task.Run(() => userMasterData.GetUserProfile(Data));
                return Gettask;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("UpdateProfile")]
        public async Task<List<dynamic>> UpdateProfile()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage("<-------------------UpdateProfile start------------------>");
                log.logDebugMessage(s);
                log.logDebugMessage("<-------------------UpdateProfile end------------------>");
                var Data = JsonConvert.DeserializeObject<ProfileUserMaster>(s);
                Data.UserID = LoggedInUSerId;
                Data.User_pkeyID = LoggedInUSerId;


                var Gettask = await Task.Run(() => userMasterData.UpdateProfile(Data));
                return Gettask;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

       // [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("UpdateWorkOrderStatusDetails")]
        public async Task<List<dynamic>> UpdateWorkOrderStatusDetails()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            Ret_UPload_Client_Result_PhotoDTO ret_UPload_Client_Result_PhotoDTO = new Ret_UPload_Client_Result_PhotoDTO();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage("<-------------------UpdateWorkOrderStatusDetails start------------------>");
                log.logDebugMessage(s);
                log.logDebugMessage("<-------------------UpdateWorkOrderStatusDetails End------------------>");
                UpdateWorkOrderMAsterStatusData updateWorkOrderMAsterStatusData = new UpdateWorkOrderMAsterStatusData();

                var Data = JsonConvert.DeserializeObject<WorkOrderUpdateStatusDTO>(s);
                Data.status = "5";
                Data.Type = 1;
                Data.UserId = LoggedInUSerId;

                var Updatedata = await Task.Run(() => updateWorkOrderMAsterStatusData.UpdateWorkOrderStatus(Data));
                ret_UPload_Client_Result_PhotoDTO.Status = 1;
                ret_UPload_Client_Result_PhotoDTO.Messsage = "Sucess";
                objdynamicobj.Add(ret_UPload_Client_Result_PhotoDTO);
                return objdynamicobj;


            }
            catch (Exception ex)
            {


                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                ret_UPload_Client_Result_PhotoDTO.Status = 0;
                ret_UPload_Client_Result_PhotoDTO.Messsage = ex.Message;
                objdynamicobj.Add(ret_UPload_Client_Result_PhotoDTO);
                return objdynamicobj;
            }

        }



        [Authorize]
        [HttpPost]
        [Route("GetWorkOrderCountForMobile")]
        public async Task<List<dynamic>> GetWorkOrderCountForMobile()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<CountWorkOrder>(s);
                Data.UserID = LoggedInUSerId;
                Data.Type = 1;
                var Gettask = await Task.Run(() => workOrderMaster_Details_MobileApp_Data.GetWorkOrderCountForMobile(Data));
                return Gettask;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("GetCustomDropDownList")]
        public async Task<List<dynamic>> GetCustomDropDownList()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();


            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<CustomDRDMaster>(s);
                Data.UserID = LoggedInUSerId;
                Data.Type = 1;
                Data.CompanyID = 0;
                var GetDRD = await Task.Run(() => iPL_Custom_DropDown_MasterData.GetCustomDRDDetails(Data));
                return GetDRD;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("AddCustomPhotoLableDataForMobile")]
        public async Task<List<dynamic>> AddCustomPhotoLableDataForMobile()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            Custom_PhotoLabel_MasterData custom_PhotoLabel_MasterData = new Custom_PhotoLabel_MasterData();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<Custom_PhotoLabel_MasterDTO>(s);
                Data.UserID = Convert.ToInt32(LoggedInUSerId);
            
                var GetDRD = await Task.Run(() => custom_PhotoLabel_MasterData.AddCustomPhotoLableDataForMobile(Data));
                return GetDRD;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

      //[AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("GetContractorPaidInvoice")]
        public async Task<List<dynamic>> GetContractorPaidInvoice()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var data = JsonConvert.DeserializeObject<Contractor_Paid_Invoice_DateDTO>(s);
                data.UserID = LoggedInUSerId;

                
                objdynamicobj = await Task.Run(() => contractor_Paid_InvoiceData.GetContractorInvoiceDetailForMobileUser(data));

                return objdynamicobj;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

     
        [Authorize]
        [HttpPost]
        [Route("GetContractorPendingInvoice")]
        public async Task<List<dynamic>> GetContractorPendingInvoice()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var data = JsonConvert.DeserializeObject<Contractor_Paid_InvoiceDTO>(s);
                data.UserID = LoggedInUSerId;

                objdynamicobj = await Task.Run(() => contractorReportPendingData.GetPendingReportForMobileUser(data));

                return objdynamicobj;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }
        // AddUpdate UserAccessLog Logout
        [Authorize]
        [HttpPost]
        [Route("UserLogout")]
        public async Task<List<dynamic>> UserLogout()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var data = JsonConvert.DeserializeObject<UserAccessLog>(s);
                data.UserID = LoggedInUSerId;
                data.Type = 1;
                objdynamicobj = await Task.Run(() => userLoginMaster.AddUserAccessLog_Logout_Mobile(data));

                return objdynamicobj;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

        [Authorize]
        [HttpPost]
        [Route("GetClientResultPhotosHistoryMobileApp")]
        public async Task<List<dynamic>> GetClientResultPhotosHistoryMobileData()
        {

            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<Client_Result_PhotoDTO>(s);
                Data.UserID = LoggedInUSerId;

                Client_Result_PhotoData client_Result_PhotoData = new Client_Result_PhotoData();
                var GetBidPhoto = await Task.Run(() => client_Result_PhotoData.GetClientResultPhotosHistoryMobileApp(Data));

                return GetBidPhoto;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        [Authorize]
        [HttpPost]
        [Route("GetHistoryDataForMobile")]
        public async Task<List<dynamic>> GetHistoryDataForMobile()
        {

            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<MobileWorkOrderFilterDTO>(s);
                Data.UserID = LoggedInUSerId;

                
                var GetBidPhoto = await Task.Run(() => mobileWorkOrderFilterData.GetHistoryDataForMobileDetails(Data));

                return GetBidPhoto;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }




      
        [Authorize]
        [HttpPost]
        [Route("Get_FileFolderDataForMobile")]
        public async Task<List<dynamic>> Get_FileFolderDataForMobile()
        {

            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<Folder_Get_Parent_MasterDTO>(s);
                Data.UserID = LoggedInUSerId;


                var GetBidPhoto = await Task.Run(() => folder_File_Child_MasterData.Get_FileFolderDataForMobile(Data));

                return GetBidPhoto;
                

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


      
        [Authorize]
        [HttpPost]
        [Route("GetMassEmailDataForMobile")]
        public async Task<List<dynamic>> GetMassEmailDataForMobile()
        {

            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<Mass_Email_MasterDTO>(s);
                Data.UserID = LoggedInUSerId;


                var GetBidPhoto = await Task.Run(() => mass_Email_MasterData.GetMassEmailDataForMobile(Data));

                return GetBidPhoto;
              

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("GetWorkOrderFinalContractorScoreForMobile")]
        public async Task<List<dynamic>> GetWorkOrderFinalContractorScoreForMobile()
        {

            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<FinalScrorCardDTO>(s);
                Data.UserID = LoggedInUSerId;


                var GetBidPhoto = await Task.Run(() => contractor_ScoreCard_SettingData.GetWorkOrderFinalContractorScoreForMobile(Data));

                return GetBidPhoto;


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        [Authorize]
        [HttpPost]
        [Route("UpdateWorkorderStatusForMobile")]
        public async Task<List<dynamic>> UpdateWorkorderStatusForMobile()
        {

            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<WorkOrderUpdateStatusDTO>(s);
                UpdateWorkOrderMAsterStatusData updateWorkOrderMAsterStatusData = new UpdateWorkOrderMAsterStatusData();
                Data.UserId = LoggedInUSerId;


                var GetBidPhoto = await Task.Run(() => updateWorkOrderMAsterStatusData.UpdateWorkorderStatusForMobile(Data));

                return GetBidPhoto;


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //post Task Damage mutiple details client result
       // [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("ClientResultTaskDamagePost")]
        public async Task<List<dynamic>> ClientResultTaskDamagePost()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage("--------------------Mobile ClientResultTaskDamagePost Start --------------------");
                log.logDebugMessage(s);
                log.logDebugMessage("--------------------Mobile ClientResultTaskDamagePost End --------------------");
                var Data = JsonConvert.DeserializeObject<TaskDamageMasterRootObject>(s);
                Data.UserID = Convert.ToInt32(LoggedInUSerId);



                var AddBidData = await Task.Run(() => clientResultDataMaster.ClientResultDamagePost(Data));
                return AddBidData;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //Task Violation
        //[AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("ClientResultTaskViolationPost")]
        public async Task<List<dynamic>> ClientResultTaskViolationPost()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<TaskViolationMasterRootObject>(s);
                Data.UserID = Convert.ToInt32(LoggedInUSerId);



                var AddViolation = await Task.Run(() => clientResultDataMaster.ClientResultViolationPost(Data));
                return AddViolation;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        //Task Hazard
        //[AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("ClientResultTaskHazardPost")]
        public async Task<List<dynamic>> ClientResultTaskHazardPost()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<TaskHazardMasterRootObject>(s);
                Data.UserID = Convert.ToInt32(LoggedInUSerId);



                var addhazard = await Task.Run(() => clientResultDataMaster.ClientResultHazardPost(Data));
                return addhazard;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        #region Sub Contractor

        [Authorize]
        [HttpPost]
        [Route("AddUsersubcontractorMasterdetails")]
        public async Task<List<dynamic>> AddUsersubcontractorMasterdetails()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            UserMasterData UserMasterData = new UserMasterData();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage("AddUsersubcontractorMasterdetails");
                log.logDebugMessage(s);
                log.logDebugMessage("AddUsersubcontractorMasterdetails");
                var Data = JsonConvert.DeserializeObject<UserMasterDTO>(s);
                Data.UserID = LoggedInUSerId;
                Data.User_SubContractor = LoggedInUSerId;
                var AddCity = await Task.Run(() => UserMasterData.AddUsersubcontractorMasterdetails(Data));
                return AddCity;


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        [Authorize]
        [HttpPost]
        [Route("GetUsersubcontractorMasterdetails")]
        public async Task<List<dynamic>> GetUsersubcontractorMasterdetails()
        {

            List<dynamic> objdynamicobj = new List<dynamic>();
            UserMasterData UserMasterData = new UserMasterData();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage("GetUsersubcontractorMasterdetails");
                log.logDebugMessage(s);
                log.logDebugMessage("GetUsersubcontractorMasterdetails");
                var Data = JsonConvert.DeserializeObject<UserMasterDTO>(s);
                Data.UserID = LoggedInUSerId;
                Data.User_SubContractor = LoggedInUSerId;

                var GetBidPhoto = await Task.Run(() => UserMasterData.GetUsersubcontractorMasterdetails(Data));

                return GetBidPhoto;


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        // this api fetch all  user records get image or document.
        //Get user document Image  master
        [Authorize]
        [HttpPost]
        [Route("GetUserDocumentMasterData")]
        public async Task<List<dynamic>> GetUserDocumentMasterData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            UserDocumentDTO userDocumentDTO = new UserDocumentDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage("GetUserDocumentMasterData");
                log.logDebugMessage(s);
                log.logDebugMessage("GetUserDocumentMasterData");

                var Data = JsonConvert.DeserializeObject<UserDocumentDTO>(s);
                userDocumentDTO.User_Doc_pkeyID = Data.User_Doc_pkeyID;
                userDocumentDTO.User_Doc_UserID = Data.User_Doc_UserID;
                userDocumentDTO.Type = Data.Type;
                userDocumentDTO.User_Doc_UserID = LoggedInUSerId;
                userDocumentDTO.UserID = LoggedInUSerId;
                var GetAppComapany = await Task.Run(() => userDocumentData.GetUserDocumentDetails(userDocumentDTO));
                return GetAppComapany;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }



        // this api  user records Add/update/delete  document. document pkey id wise 
        //Add/update/delete dcoument user Master 
        [Authorize]
        [HttpPost]
        [Route("CreateUpdateUserDocumentMaster")]
        public async Task<List<dynamic>> CreateUpdateUserDocumentMaster()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            UserDocumentDTO userDocumentDTO = new UserDocumentDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage("CreateUpdateUserDocumentMaster");
                log.logDebugMessage(s);
                log.logDebugMessage("CreateUpdateUserDocumentMaster");
                var Data = JsonConvert.DeserializeObject<UserDocumentDTO>(s);
                Data.UserID = LoggedInUSerId;
                Data.User_Doc_UserID = LoggedInUSerId;
                var GetAppComapany = await Task.Run(() => userDocumentData.AddUserDocumentData(Data));
                return GetAppComapany;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        //Get user Background Image  master
        [Authorize]
        [HttpPost]
        [Route("GetUserBackgroundMasterData")]
        public async Task<List<dynamic>> GetUserBackgroundMasterData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            User_Background_ProviderDTO User_Background_ProviderDTO = new User_Background_ProviderDTO();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage("GetUserBackgroundMasterData");
                log.logDebugMessage(s);
                log.logDebugMessage("GetUserBackgroundMasterData");
                var Data = JsonConvert.DeserializeObject<User_Background_ProviderDTO>(s);
                User_Background_ProviderDTO.Background_check_PkeyID = Data.Background_check_PkeyID;
                User_Background_ProviderDTO.Background_check_User_ID = LoggedInUSerId;
                User_Background_ProviderDTO.Type = Data.Type;
                User_Background_ProviderDTO.UserID = LoggedInUSerId;
                var GetAppComapany = await Task.Run(() => user_Background_checkData.GetUserBackgroundDetails(User_Background_ProviderDTO));
                return GetAppComapany;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        [Authorize]
        [HttpPost]
        [Route("AddUserBackgroundcheckDataDetails")]
        public async Task<List<dynamic>> AddUserBackgroundcheckDataDetails()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            User_Background_checkData User_Background_checkData = new User_Background_checkData();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage("AddUserBackgroundcheckDataDetails");
                log.logDebugMessage(s);
                log.logDebugMessage("AddUserBackgroundcheckDataDetails");
                var Data = JsonConvert.DeserializeObject<User_Background_ProviderDTO>(s);
                Data.UserID = LoggedInUSerId;
                Data.Background_check_User_ID = LoggedInUSerId;
                var AddCity = await Task.Run(() => User_Background_checkData.AddUserBackgroundcheckDataDetails(Data));
                return AddCity;


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
       // [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("GetContractorStateDropDown")]
        public async Task<List<dynamic>> GetContractorStateDropDown()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            Form_Page_DropDownData form_Page_DropDownData = new Form_Page_DropDownData();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage("GetContractorStateDropDown");
                log.logDebugMessage(s);
                log.logDebugMessage("GetContractorStateDropDown");
                var Data = JsonConvert.DeserializeObject<contractorMapDRD>(s);
                Data.UserId = LoggedInUSerId;
                var Getstatedropdown = await Task.Run(() => userMasterData.GetContractorMapAreaDRDDetails(Data));
                return Getstatedropdown;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
       // [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("GetContractorCountyZIPDropDown")]
        public async Task<List<dynamic>> GetContractorCountyDropDown()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage("GetContractorCountyZIPDropDown");
                log.logDebugMessage(s);
                log.logDebugMessage("GetContractorCountyZIPDropDown");

                var Data = JsonConvert.DeserializeObject<CountyZipChangeDTO>(s);
                Data.UserID = LoggedInUSerId;
                var Getcountydropdown = await Task.Run(() => userMasterData.GetContractorcountyDRDDetails(Data));
                return Getcountydropdown;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        //[AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("UserCoveragedetail")]
        public async Task<List<dynamic>> UserCoveragedetail()

        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage("UserCoveragedetail");
                log.logDebugMessage(s);
                log.logDebugMessage("UserCoveragedetail");
                var Data = JsonConvert.DeserializeObject<ContractorCoverageAreaDTO>(s);
                Data.UserID = LoggedInUSerId;
                Data.Cont_Coverage_Area_UserID = Data.UserID;
                var addressarr = await Task.Run(() => userMasterData.AddContractorMapAddress(Data));
                return addressarr;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

      //  [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("GetContractorCoverageData")]
        public async Task<List<dynamic>> GetContractorCoverageData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                log.logDebugMessage("GetContractorCoverageData");
                log.logDebugMessage(s);
                log.logDebugMessage("GetContractorCoverageData");
                var Data = JsonConvert.DeserializeObject<CountyZipChangeDTO>(s);
                Data.UserID = LoggedInUSerId;
                Data.Cont_Coverage_Area_UserID = Data.UserID;
                var Getcountydropdown = await Task.Run(() => userMasterData.GetContractorcountyDRDDetails(Data));
                return Getcountydropdown;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        #endregion

        //[AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("AddUser_Access_logdataDetails")]
        public async Task<List<dynamic>> AddUser_Access_logdataDetails()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            Insert_User_Access_log_MasterData insert_User_Access_log_MasterData = new Insert_User_Access_log_MasterData();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Insert_User_Access_log_MasterDTO>(s);
                Data.UserID = LoggedInUSerId;
                Data.User_Acc_Log_UserID = Data.UserID;



                var AddCity = await Task.Run(() => insert_User_Access_log_MasterData.AddUser_Access_logdataDetails(Data));
                return AddCity;


            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        //[AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("GetContractorScoreCardSetting")]
        public async Task<List<dynamic>> GetContractorScoreCardSetting()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Contractor_ScoreCard_SettingDTO>(s);
                Data.Con_score_setting_UserId = LoggedInUSerId;
                Data.UserID = LoggedInUSerId;

                var ScorecardSetting = await Task.Run(() => contractor_ScoreCard_SettingData.GetContractorScoreCardSettingDetails(Data));
                return ScorecardSetting;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("CreateLog")]
        public async Task<List<dynamic>> CreateLog()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<LogDTO>(s);
             
                Data.UserID = LoggedInUSerId;
                log.logMobileInfoMessage("*******************Start*******************************");
                log.logMobileInfoMessage(Data.UserID.ToString());
                log.logMobileInfoMessage(Data.Val);
                log.logMobileInfoMessage("*******************End*******************************");
                await Task.Run(() => objdynamicobj.Add(1));
                return objdynamicobj;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        //Delete Mobile USer
        //[AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("DeleAccount")]
        public async Task<List<dynamic>> DeleAccount()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<UserMasterDTO>(s);
                Data.UserID = LoggedInUSerId;
                Data.User_pkeyID = LoggedInUSerId;
                Data.User_IsActive = false;
                Data.Type = 3;
                var Deletuser = await Task.Run(() => userMasterData.AddUserMasterdetails(Data));
                return Deletuser;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }



        //Delete Mobile USer
        //[AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("UpdateUserTokenFromMobile")]
        public async Task<List<dynamic>> UpdateUserTokenFromMobile()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<UserMasterDTO>(s);
                Data.UserID = LoggedInUSerId;
                Data.User_pkeyID = LoggedInUSerId;
                var updateuser = await Task.Run(() => userMasterData.AddUserMasterdetails(Data));
                return updateuser;

            }
            catch (Exception ex)
            {

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }



    }
}
