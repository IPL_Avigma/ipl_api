﻿using IPL.Models;
using IPL.Repository.data;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using Avigma.Repository.Lib;
namespace IPL.Controllers
{
    [RoutePrefix("api/formdocs")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class FormAndDocsController : BaseController
    {
        Folder_Parent_MasterData folder_Parent_MasterData = new Folder_Parent_MasterData();
        Folder_File_Child_MasterData Folder_File_Child_MasterData = new Folder_File_Child_MasterData();
        Log log = new Log();
        //add folder 
        [Authorize]
        [HttpPost]
        [Route("PostAddFolderDetails")]
        public async Task<List<dynamic>> PostAddFolderDetails()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Folder_Parent_MasterDTO>(s);
                Data.UserID = LoggedInUSerId;

                var AddFolder = await Task.Run(() => folder_Parent_MasterData.AddUpdateFolderFilterDetails(Data));
                return AddFolder;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //Get folder 
        [Authorize]
        [HttpPost]
        [Route("GetParentFolderDetails")]
        public async Task<List<dynamic>> GetParentFolderDetails()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Folder_Parent_MasterDTO>(s);
                Data.UserID = LoggedInUSerId;

                var GetFolder = await Task.Run(() => folder_Parent_MasterData.GetFolderMasterDetails(Data));
                return GetFolder;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        //Get folder 


        [Authorize]

        [HttpPost]
        [Route("GetFileFolderData")]
        public async Task<List<dynamic>> GetFileFolderData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
        
                var Data = JsonConvert.DeserializeObject<Folder_Get_Parent_MasterDTO>(s);
                Data.UserID = LoggedInUSerId;

                var GetFolder = await Task.Run(() => Folder_File_Child_MasterData.Get_FileFolderData(Data));
                return GetFolder;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        [Authorize]
        [HttpPost]
        [Route("GetEditFileData")]
        public async Task<List<dynamic>> GetEditFileData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            Folder_File_MasterData folder_File_MasterData = new Folder_File_MasterData();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Folder_File_MasterDTO>(s);
                Data.UserID = LoggedInUSerId;

                var GetFile = await Task.Run(() => folder_File_MasterData.GetFolderFileMasterDetails(Data));
                return GetFile;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("UpdateFileMasterData")]
        public async Task<List<dynamic>> UpdateFileMasterData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            Folder_File_MasterData folder_File_MasterData = new Folder_File_MasterData();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Folder_File_MasterDTO>(s);
                Data.UserID = LoggedInUSerId;

                var UpdateFile = await Task.Run(() => folder_File_MasterData.AddUpdateFileMasterDetails(Data));
                return UpdateFile;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("AddUpdateFileAutoAssignReference")]
        public async Task<List<dynamic>> AddUpdateFileAutoAssignReference()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            Folder_File_MasterData folder_File_MasterData = new Folder_File_MasterData();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var data = JsonConvert.DeserializeObject<Folder_File_MasterDTO>(s);
                data.UserID = LoggedInUSerId;

                var UpdateFile = await Task.Run(() => folder_File_MasterData.AddUpdate_File_AutoAssignReference(data.Fold_File_Pkey_Id,data));
                return UpdateFile;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }



    }
}
