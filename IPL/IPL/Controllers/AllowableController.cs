﻿using IPL.Models;
using IPL.Repository.data;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using Avigma.Repository.Lib;

namespace IPL.Controllers
{
    [RoutePrefix("api/Allowables")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AllowableController : BaseController
    {

        Allowables_Category_MasterData allowables_Category_MasterData = new Allowables_Category_MasterData();
        AllowableMasterData allowableMasterData = new AllowableMasterData();
        Log log = new Log();

        [Authorize]
        [HttpPost]
        [Route("PostAllowablesCategory")]
        public async Task<List<dynamic>> PostAllowablesCategory()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<Allowables_Category_MasterDTO>(s);
                Data.UserID = LoggedInUSerId;
                var AddAllowablesCategory = await Task.Run(() => allowables_Category_MasterData.AddAllowablesCategoryData(Data));
                return AddAllowablesCategory;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("GetAllowablesCategory")]
        public async Task<List<dynamic>> GetAllowablesCategory()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<Allowables_Category_MasterDTO>(s);
                Data.UserID = LoggedInUSerId;
                switch (Data.Type)
                {

                    case 1:
                        {

                            objdynamicobj = await Task.Run(() => allowables_Category_MasterData.GetAllowablesCategoryDetails(Data));

                            break;
                        }
                    case 3:
                        {
                            objdynamicobj = await Task.Run(() => allowables_Category_MasterData.GetAllowablesFilterDetails(Data));

                            break;
                        }
                }



            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
            return objdynamicobj;

        }

        [Authorize]
        [HttpPost]
        [Route("AddUpdateFilterAdminAllowablesCategory")]
        public async Task<List<dynamic>> AddUpdateFilterAdminAllowablesCategory()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<Filter_Admin_Allowables_Category_MasterDTO>(s);
                Data.UserId = LoggedInUSerId;
                var Updateallowcate = await Task.Run(() => allowables_Category_MasterData.AddUpdate_Filter_Admin_AllowablesCategory(Data));
                return Updateallowcate;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("AddUpdateAllowables")]
        public async Task<List<dynamic>> AddUpdateAllowables()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<AllowableMasterDTO>(s);
                Data.UserID = LoggedInUSerId;
                var Updateallowables = await Task.Run(() => allowableMasterData.AddAllowableMaster_Data(Data));
                return Updateallowables;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("GetAllowables")]
        public async Task<List<dynamic>> GetAllowables()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<AllowableMasterDTO>(s);
                Data.UserID = LoggedInUSerId;

                switch (Data.Type)
                {

                    case 1:
                        {

                            objdynamicobj = await Task.Run(() => allowableMasterData.Get_AllowableMasterDetails(Data));

                            break;
                        }
                    case 2:
                        {

                            objdynamicobj = await Task.Run(() => allowableMasterData.Get_AllowablesSingleDetails(Data));

                            break;
                        }
                    case 3:
                        {
                            objdynamicobj = await Task.Run(() => allowableMasterData.GetFilterAllowablesDetails(Data));

                            break;
                        }
                }

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
            return objdynamicobj;
        }

        [Authorize]
        [HttpPost]
        [Route("GetAllowablesCategoryDRD")]
        public async Task<List<dynamic>> GetAllowablesCategoryDRD()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<Allowables_Category_MasterDTO>(s);
                Data.UserID = LoggedInUSerId;
                Data.Type = 1;
                var drdallowables = await Task.Run(() => allowables_Category_MasterData.GetAllowablesCategoryDRD(Data));
                return drdallowables;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("AllowablesFilterSave")]
        public async Task<List<dynamic>> AllowablesFilterSave()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<Filter_Admin_Allowables_MasterDTO>(s);
                Data.UserId = LoggedInUSerId;
               
                var filterallowables = await Task.Run(() => allowableMasterData.AddUpdate_Filter_Admin_Allowables(Data));
                return filterallowables;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        [Authorize]
        [HttpPost]
        [Route("DeleteAllowablesChildDetails")]
        public async Task<List<dynamic>> DeleteAllowablesChildDetails()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            Allowables_Child_Master_Data allowables_Child_Master_Data = new Allowables_Child_Master_Data();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<Allowables_Child_Master_DTO>(s);
                Data.UserID = LoggedInUSerId;

                var delallowables = await Task.Run(() => allowables_Child_Master_Data.AddupdateAllowables_Child_Master_Data(Data));
                return delallowables;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

    }
}
