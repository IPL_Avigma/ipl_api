﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using Avigma.Repository.Lib;
using IPL.Models;
using IPL.Repository.data;
using IPLApp.Models;
using Newtonsoft.Json;
using System.Configuration;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Microsoft.Owin.Security.OAuth;
using Owin;
using IPL.Repository.Lib;
using IPL.Models.Avigma;

namespace IPL.Controllers
{

    [RoutePrefix("api/RESTAuthentication")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class RESTAuthenticationController : BaseController
    {

        // private IAppBuilder _app;
        //public RESTAuthenticationController(IAppBuilder app) {
        //    _app = app;
        //}
    
        UserLoginMaster UserLoginMasterObj = new UserLoginMaster();
        UserLoginPasswordForget UserLoginPasswordForgetObj = new UserLoginPasswordForget();
        IPL_Admin_User_LoginData iPL_Admin_User_LoginData = new IPL_Admin_User_LoginData();
        UserVerificationMaster_Data userVerification = new UserVerificationMaster_Data();
        Log log = new Log();

        // User Login Here api


        //[Authorize]

        [HttpPost]
        [Route("PostUserLoginData")]
        public async Task<List<dynamic>> PostUserLoginData()
        {     
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<AuthLoginDTO>(s);

                AuthLoginDTO loginDTO = new AuthLoginDTO();
                loginDTO.User_LoginName = Data.User_LoginName;
                loginDTO.User_Password = Data.User_Password;
                loginDTO.User_pkeyID = LoggedInUSerId;
                loginDTO.User_Token_val = Data.User_Token_val;
                if (Data.IsAdminLogin)
                {
                    loginDTO.Type = 3;
                }
                else
                {
                    loginDTO.Type = 2;
                }
              

                var Getinvestorlogin = await Task.Run(() => UserLoginMasterObj.GetLoginDetails(loginDTO));
                //if (Getinvestorlogin[0][0].User_pkeyID != null)
                //{
                //    Startup startup = new Startup();
                //    _app = app;
                //    startup.ConfigureOAuth(_app);

                //}
                return Getinvestorlogin;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //admin login

        [HttpPost]
        [Route("PostAdminLoginData")]
        public async Task<List<dynamic>> PostAdminLoginData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<IPL_Admin_User_LoginDTO>(s);

                IPL_Admin_User_LoginDTO iPL_Admin_User_LoginDTO = new IPL_Admin_User_LoginDTO();
                iPL_Admin_User_LoginDTO.Ipl_Ad_User_Login_Name = Data.Ipl_Ad_User_Login_Name;
                iPL_Admin_User_LoginDTO.Ipl_Ad_User_Password = Data.Ipl_Ad_User_Password;
                iPL_Admin_User_LoginDTO.UserID = 0;
                iPL_Admin_User_LoginDTO.Ipl_Ad_User_UserVal = Data.Ipl_Ad_User_UserVal;
                iPL_Admin_User_LoginDTO.Type = 2;

                var Getinvestorlogin = await Task.Run(() => iPL_Admin_User_LoginData.GetAuthenticateAdminLoginDetails(iPL_Admin_User_LoginDTO));
              
                return Getinvestorlogin;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //add admin user
        [Authorize]
        [HttpPost]
        [Route("PostAdminUserData")]
        public async Task<List<dynamic>> PostAdminUserData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<IPL_Admin_User_LoginDTO>(s);

                Data.UserID = LoggedInUSerId;
                var postuserad = await Task.Run(() => iPL_Admin_User_LoginData.AddAdminUserData(Data));

                return postuserad;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //add admin user
        [Authorize]
        [HttpPost]
        [Route("DeleteAdminUserData")]
        public async Task<List<dynamic>> DeleteAdminUserData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<IPL_Admin_User_LoginDTO>(s);
                Data.UserID = LoggedInUSerId;

                var postuserad = await Task.Run(() => iPL_Admin_User_LoginData.AddAdminUserData(Data));

                return postuserad;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //get user details
        [Authorize]
        [HttpPost]
        [Route("GetAdminUserData")]
        public async Task<List<dynamic>> GetAdminUserData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<IPL_Admin_User_LoginDTO>(s);


                var Getuser = await Task.Run(() => iPL_Admin_User_LoginData.GetUserDetails(Data));
                return Getuser;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        // get profile data
        [Authorize]
        [HttpPost]
        [Route("GetAdminProfileUserData")]
        public async Task<List<dynamic>> GetAdminProfileUserData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<IPL_Admin_User_LoginDTO>(s);
                if(Data.Type !=4)
                {
                    Data.Ipl_Ad_User_PkeyID = LoggedInUSerId;
                }
                
               
                Data.UserID = LoggedInUSerId;

                var Getuser = await Task.Run(() => iPL_Admin_User_LoginData.GetUserDetails(Data));
                return Getuser;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //for deshboard chartdata
        [Authorize]
        [HttpPost]
        [Route("PostChartData")]
        public async Task<List<dynamic>> PostChartData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

               // var Data = JsonConvert.DeserializeObject<AuthLoginDTO>(s);

                AuthLoginDTO loginDTO = new AuthLoginDTO();
              
               loginDTO.User_pkeyID = LoggedInUSerId;
               // loginDTO.User_pkeyID = Data.User_pkeyID;

                loginDTO.Type = 2;

                var Getchart = await Task.Run(() => UserLoginMasterObj.GetLoginDetails(loginDTO));
              
                return Getchart;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }






        // User forgot password user id se create auto generate, email sent and save dBase entry passord, flag set 0
     //   [Authorize]
        [HttpPost]
        [Route("PostUserForgotPasswordData")]
        public async Task<List<dynamic>> PostUserForgotPasswordData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
               
                string s = reader.ReadToEnd();
                //log.logDebugMessage("PostUserForgotPasswordData  Start");
                //log.logDebugMessage(s);
                //log.logDebugMessage("PostUserForgotPasswordData End");

                var Data = JsonConvert.DeserializeObject<AuthLoginDTO>(s);

                AuthLoginDTO loginDTO = new AuthLoginDTO();
                loginDTO.User_LoginName = Data.User_LoginName;
                loginDTO.User_Source = Data.User_Source;


                var Getinvestorlogin = await Task.Run(() => UserLoginPasswordForgetObj.GetUserEamil(loginDTO));
                return Getinvestorlogin;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }



        // User Change password user pkey  flag set 1
        //[Authorize]
        [HttpPost]
        [Route("PostUserChangePasswordData")]
        public async Task<List<dynamic>> PostUserChangePasswordData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<AuthLoginDTO>(s);

                UserMasterDTO userMasterDTO = new UserMasterDTO();

                userMasterDTO.User_pkeyID = Data.User_pkeyID;
                userMasterDTO.User_Password = Data.User_Password;
                userMasterDTO.User_Token_val = Data.User_Token_val;
                userMasterDTO.Type = Data.Type;


                var Getinvestorlogin = await Task.Run(() => UserLoginPasswordForgetObj.PasswordUserMasterdetails(userMasterDTO));
                return Getinvestorlogin;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }



        // Check user name
        [Authorize]
        [HttpPost]
        [Route("PostCheckUserNameData")]
        public async Task<List<dynamic>> PostCheckUserNameData()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<AuthLoginDTO>(s);

                AuthLoginDTO loginDTO = new AuthLoginDTO();
                loginDTO.User_LoginName = Data.User_LoginName;


                var Getinvestorlogin = await Task.Run(() => UserLoginPasswordForgetObj.CheckUsername(loginDTO));
                return Getinvestorlogin;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }




        // change password get user pkey set password get
        [Authorize]
        [HttpPost]
        [Route("GetUserLoginPassword")]
        public async Task<List<dynamic>> GetUserLoginPassword()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<AuthLoginDTO>(s);

                AuthLoginDTO loginDTO = new AuthLoginDTO();
                loginDTO.User_pkeyID = LoggedInUSerId; 


                var Getinvestorlogin = await Task.Run(() => UserLoginPasswordForgetObj.GetPassword(loginDTO));
                return Getinvestorlogin;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }



        // change password page se
        [Authorize]
        [HttpPost]
        [Route("ChangeUserLoginPassword")]
        public async Task<List<dynamic>> ChangeUserLoginPassword()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<AuthLoginDTO>(s);

                AuthLoginDTO loginDTO = new AuthLoginDTO();
                loginDTO.User_pkeyID = LoggedInUSerId;
                loginDTO.User_Password = Data.User_Password;
                loginDTO.UserID = 0;
                loginDTO.Type = 1;


                var Getinvestorlogin = await Task.Run(() => UserLoginPasswordForgetObj.PasswordChangeUser(loginDTO));
                return Getinvestorlogin;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //Pradeep

        //[Authorize]
        [HttpPost]
        [Route("GetUserViryficationDetails")]
        public async Task<List<dynamic>> GetUserViryficationDetails()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<UserVerificationMaster_DTO>(s);


                var Getuser = await Task.Run(() => userVerification.ChangeUser_Password(Data));
                return Getuser;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        // Update UserAccessLog
        [Authorize]
        [HttpPost]
        [Route("AddUserAccessLogLogout")]
        public async Task<List<dynamic>> AddUserAccessLogLogout()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var data = JsonConvert.DeserializeObject<UserAccessLog>(s);
                data.UserID = LoggedInUSerId;
                data.Type = 1;
                objdynamicobj = await Task.Run(() => UserLoginMasterObj.AddUserAccessLog_Logout(data));

                return objdynamicobj;
            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }
        }

        [HttpPost]
        [Route("PostCompanyUserChangePassword")]
        public async Task<List<dynamic>> PostCompanyUserChangePassword()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<AuthLoginDTO>(s);

                UserMasterDTO userMasterDTO = new UserMasterDTO();

                userMasterDTO.User_pkeyID = Data.User_pkeyID;
                userMasterDTO.User_Password = Data.User_Password;
                userMasterDTO.User_Token_val = Data.User_Token_val;
                userMasterDTO.Type = Data.Type;


                var Getinvestorlogin = await Task.Run(() => UserLoginPasswordForgetObj.PasswordUserMasterdetails(userMasterDTO));
                return Getinvestorlogin;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("GetDashBoardClientDetailsData")]
        public async Task<List<dynamic>> GetDashBoardClientDetailsData()
        {

            List<dynamic> objdynamicobj = new List<dynamic>();
            DashBoardData DashBoardData = new DashBoardData();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<DashBordDataDTO>(s);
                Data.UserId = LoggedInUSerId;
                var GetclientPay = await Task.Run(() => DashBoardData.GetDashBoardClientDetailsData(Data));

                return GetclientPay;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

    }
}
