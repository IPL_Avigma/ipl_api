﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using Avigma.Repository.Lib;
using IPL.Models;
using IPL.Models.FiveBrothers;
using IPL.Repository.data;
using IPLApp.Models;
using IPLApp.Repository.data;
using Newtonsoft.Json;


namespace IPL.Controllers
{
    [RoutePrefix("api/RESTIPLDROPDOWN")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class RESTIPLDROPDOWNController : BaseController
    {
        DropDownData dropDownData = new DropDownData();
        PCR_DropDrownData PCR_DropDrownData = new PCR_DropDrownData();
        StatusMasterDRD statusMasterDRD = new StatusMasterDRD();
        UserMasterData userMasterData = new UserMasterData();
        Ipl_Company_RegisterData ipl_Company_RegisterData = new Ipl_Company_RegisterData();
        Log log = new Log();

        //[AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("GetDropDownDataWorkOder")]
        public async Task<List<dynamic>> GetDropDownDataWorkOder()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
               
                var dropDownMasterDTO = JsonConvert.DeserializeObject<DropDownMasterDTO>(s);
                dropDownMasterDTO.UserID = LoggedInUSerId;
                dropDownMasterDTO.Type = dropDownMasterDTO.Type == 0 ? 1 : dropDownMasterDTO.Type;

                //var Getdropdown = await Task.Run(() => dropDownData.GetDropdownWorkOrder(dropDownMasterDTO));
                var Getdropdown = await Task.Run(() => dropDownData.GetDropdownWorkOrdernew(dropDownMasterDTO)); // Uncomment it when implemented new from Angular
                return Getdropdown;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }




        // get drop down client
        //[AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("GetDropDownDataClientResult")]
        public async Task<List<dynamic>> GetDropDownDataClientResult()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<DropDownMasterDTO>(s);
                DropDownMasterDTO dropDownMasterDTO = new DropDownMasterDTO();
                dropDownMasterDTO.UserID = LoggedInUSerId;
                dropDownMasterDTO.WorkOrderID = Data.WorkOrderID;
                dropDownMasterDTO.WorkOrderID_mul = Data.WorkOrderID_mul;
                if (Data.WorkOrderID != 0)
                {
                    if (Data.Type == 3)
                        dropDownMasterDTO.Type = 3;
                    else
                        dropDownMasterDTO.Type = 1;
                    var Getdropdown = await Task.Run(() => dropDownData.GetClientRsultMasterDetails(dropDownMasterDTO));
                    return Getdropdown;
                }
                else if (!string.IsNullOrEmpty(Data.WorkOrderID_mul))
                {
                    dropDownMasterDTO.Type = 2;
                    var Getdropdown = await Task.Run(() => dropDownData.GetClientRsultMasterDetails(dropDownMasterDTO));
                    return Getdropdown;
                }
                else
                {
                    string message = "WorkOrderID getting 0.. Please check" + LoggedInUSerId;
                    objdynamicobj.Add(message);
                    return objdynamicobj;
                }



            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        // get drop down UOM data 
        //[AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("GetDropDownUOM")]
        public async Task<List<dynamic>> GetDropDownUOM()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                UOM_MasterDTO uOM_MasterDTO = new UOM_MasterDTO();
                UOM_Master_Data uOM_Master_Data = new UOM_Master_Data();

                uOM_MasterDTO.Type = 1;
                uOM_MasterDTO.UserID = LoggedInUSerId;

                var Getdropdown = await Task.Run(() => uOM_Master_Data.GetUOM_MasterDetails(uOM_MasterDTO));
                return Getdropdown;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        // Get PCR DropDown Details
        // [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("GetPCRDataDropDown")]
        public async Task<List<dynamic>> GetPCRDataDropDown()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                DropDownMasterDTO dropDownMasterDTO = new DropDownMasterDTO();
                dropDownMasterDTO.UserID = LoggedInUSerId;
                dropDownMasterDTO.Type = 1;
                var Getpcrdropdown = await Task.Run(() => PCR_DropDrownData.GetPCRDropdownDetails(dropDownMasterDTO));
                return Getpcrdropdown;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //get status DropDown
        // [AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("GetStatusDropDown")]
        public async Task<List<dynamic>> GetStatusDropDown()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<StatusDetailsDTO>(s);
                Data.UserID = LoggedInUSerId;



                var Getstatusdropdown = await Task.Run(() => statusMasterDRD.GetStatusMasterDetails(Data));
                return Getstatusdropdown;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }



        [Authorize]
        [AllowAnonymous]

        [HttpPost]
        [Route("GetFormDropDown")]
        public async Task<List<dynamic>> GetFormDropDown()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            Form_Page_DropDownData form_Page_DropDownData = new Form_Page_DropDownData();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<CommonDropdownDTO>(s);
                Data.UserId = LoggedInUSerId;
                var Getformdropdown = await Task.Run(() => form_Page_DropDownData.GetDropdownFormPage(Data));
                return Getformdropdown;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("GetContractorStateDropDown")]
        public async Task<List<dynamic>> GetContractorStateDropDown()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();
            Form_Page_DropDownData form_Page_DropDownData = new Form_Page_DropDownData();
            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<contractorMapDRD>(s);
                Data.UserId = LoggedInUSerId;
                var Getstatedropdown = await Task.Run(() => userMasterData.GetContractorMapAreaDRDDetails(Data));
                return Getstatedropdown;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
        [Authorize]
        [HttpPost]
        [Route("GetContractorCountyDropDown")]
        public async Task<List<dynamic>> GetContractorCountyDropDown()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<CountyZipChangeDTO>(s);
                Data.UserID = LoggedInUSerId;
                var Getcountydropdown = await Task.Run(() => userMasterData.GetContractorcountyDRDDetails(Data));
                return Getcountydropdown;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("GetContractorCountyDropDownList")]
        public async Task<List<dynamic>> GetContractorCountyDropDownList()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<Folder_Parent_MasterDTO>(s);
                Data.Type = 2;

                //Data.UserID = LoggedInUSerId;
                var Getcountydropdown = await Task.Run(() => userMasterData.GetContractorcountyDRDDetails(Data));
                return Getcountydropdown;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        // get drop down task configuration
        //[AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("GetTaskConfigurationDropDown")]
        public async Task<List<dynamic>> GetTaskConfigurationDropDown()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<DropDownMasterDTO>(s);
                Data.UserID = LoggedInUSerId;
                // This is temp Need to be removed after consulting angular develoepr
                if (Data.Type == 2)
                {
                    Data.Type = 2;
                }
                else
                {
                    Data.Type = 1;
                }
                ///////////////////
                Data.WorkOrderID = Data.WorkOrderID;

                var Getdropdown = await Task.Run(() => dropDownData.GetTaskConfigurationDropDown(Data));
                return Getdropdown;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //damage Items Dropdown


        [Authorize]
        [HttpPost]
        [Route("GetDamageItemsDropDown")]
        public async Task<List<dynamic>> GetDamageItemsDropDown()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<Import_Client_DamageItems_Drd>(s);


                var Getdropdown = await Task.Run(() => dropDownData.GetImportDamageItemsDropDown());
                return Getdropdown;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("GetUserStateDrD")]
        public async Task<List<dynamic>> GetUserStateDrD()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<UserStateDTO>(s);
                Data.UserID = LoggedInUSerId;
                var GetState = await Task.Run(() => userMasterData.GetUserStateDetails(Data));
                return GetState;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("GetUserCountyDrD")]
        public async Task<List<dynamic>> GetUserCountyDrD()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<UserStateDTO>(s);
                Data.UserID = LoggedInUSerId;
                var GetState = await Task.Run(() => userMasterData.GetUserUserCountyDetails(Data));
                return GetState;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }


        [AllowAnonymous]
        [HttpPost]
        [Route("GetUserregisterStateDrD")]
        public async Task<List<dynamic>> GetUserregisterStateDrD()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<UserStateDTO>(s);

                var GetState = await Task.Run(() => ipl_Company_RegisterData.GetregisterStateDetails(Data));
                return GetState;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [AllowAnonymous]
        [HttpPost]
        [Route("GetUserregisterCountyDrD")]
        public async Task<List<dynamic>> GetUserregisterCountyDrD()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();
                var Data = JsonConvert.DeserializeObject<UserStateDTO>(s);

                var GetState = await Task.Run(() => ipl_Company_RegisterData.GetUserregisterCountyDetails(Data));
                return GetState;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);

                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("GetWorkOrderIPLNumberlist")]
        public async Task<List<dynamic>> GetWorkOrderIPLNumberlist()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var Data = JsonConvert.DeserializeObject<DropDownMasterDTO>(s);
                Data.UserID = LoggedInUSerId;


                var Getdropdown = await Task.Run(() => dropDownData.GetWorkOrderIPLNumberlist(Data));
                return Getdropdown;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        //[AllowAnonymous]
        [Authorize]
        [HttpPost]
        [Route("Createworktypeviewdependency")]
        public async Task<List<dynamic>> Createworktypeviewdependency()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var dropDownWorkTypeMasterDTO = JsonConvert.DeserializeObject<DropDownWorkTypeMasterDTO>(s);
                dropDownWorkTypeMasterDTO.UserID = LoggedInUSerId;
                var Getdropdown = await Task.Run(() => dropDownData.Createworktypeviewdependency(dropDownWorkTypeMasterDTO)); // Uncomment it when implemented new from Angular
                return Getdropdown;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }

        [Authorize]
        [HttpPost]
        [Route("CreateTasktypeviewdependency")]
        public async Task<List<dynamic>> CreateTasktypeviewdependency()
        {
            List<dynamic> objdynamicobj = new List<dynamic>();

            try
            {
                System.IO.Stream body = System.Web.HttpContext.Current.Request.InputStream;
                System.Text.Encoding encoding = System.Web.HttpContext.Current.Request.ContentEncoding;
                System.IO.StreamReader reader = new System.IO.StreamReader(body, encoding);
                string s = reader.ReadToEnd();

                var dropDownWorkTypeMasterDTO = JsonConvert.DeserializeObject<DropDownWorkTypeMasterDTO>(s);
                dropDownWorkTypeMasterDTO.UserID = LoggedInUSerId;
                var Getdropdown = await Task.Run(() => dropDownData.CreateTasktypeviewdependency(dropDownWorkTypeMasterDTO)); // Uncomment it when implemented new from Angular
                return Getdropdown;

            }
            catch (Exception ex)
            {
                log.logErrorMessage(ex.StackTrace);
                log.logErrorMessage(ex.Message);
                objdynamicobj.Add(ex.Message);
                return objdynamicobj;
            }

        }
    }
}
